package am_grade_house_mapping;

import am_house.Am_houseRepository;
import common.ApiResponse;
import common.BaseServlet;
import common.CommonDAOService;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import permission.MenuConstants;
import user.UserDTO;
import util.CommonDTO;
import util.HttpRequestUtils;
import util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * Servlet implementation class Am_grade_house_mappingServlet
 */
@WebServlet("/Am_grade_house_mappingServlet")
@MultipartConfig
public class Am_grade_house_mappingServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Am_grade_house_mappingServlet.class);


    @Override
    public String getTableName() {
        return Am_grade_house_mappingDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Am_grade_house_mappingServlet";
    }

    @Override
    public CommonDAOService<? extends CommonDTO> getCommonDAOService() {
        return Am_grade_house_mappingDAO.getInstance();
    }

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
        Am_grade_house_mappingDTO am_grade_house_mappingDTO;

        if (Boolean.TRUE.equals(addFlag)) {
            am_grade_house_mappingDTO = new Am_grade_house_mappingDTO();
            am_grade_house_mappingDTO.insertionDate = am_grade_house_mappingDTO.lastModificationTime
                    = System.currentTimeMillis();
            am_grade_house_mappingDTO.insertedByUserId = userDTO.ID;
            am_grade_house_mappingDTO.insertedByOrganogramId = userDTO.organogramID;
        } else {
            am_grade_house_mappingDTO = Am_grade_house_mappingRepository.getInstance().getAm_grade_house_mappingDTOByID
                    (Long.parseLong(request.getParameter("iD")));
            if (am_grade_house_mappingDTO == null) {
                throw new Exception("Grade House Mapping not found");
            }
            am_grade_house_mappingDTO.lastModificationTime = System.currentTimeMillis();
            am_grade_house_mappingDTO.modifiedBy = userDTO.ID;
        }

        am_grade_house_mappingDTO.amHouseClassCat = Integer.parseInt(request.getParameter("amHouseClassCat"));
        CategoryLanguageModel categoryLanguageModel = CatRepository.getInstance().getCategoryLanguageModel
                ("am_house_class", am_grade_house_mappingDTO.amHouseClassCat);
        if (categoryLanguageModel == null) {
            throw new Exception("House class is invalid");
        }

        am_grade_house_mappingDTO.minimum_basic_salary =
                Double.parseDouble(request.getParameter("minimumBasicSalary"));


        am_grade_house_mappingDTO.job_grade_ids = Jsoup.clean(request.getParameter("jobGradeIds"),
                Whitelist.simpleText());
        if (am_grade_house_mappingDTO.job_grade_ids.isEmpty()) {
            throw new Exception("Grade required");
        }

        if (Boolean.TRUE.equals(addFlag)) {
            if (!Am_grade_house_mappingRepository.getInstance().getAm_grade_house_mappingDTOByam_house_class_cat(am_grade_house_mappingDTO.amHouseClassCat).isEmpty()) {
                throw new Exception("Mapping exists for this class");
            }
            getCommonDAOService().add(am_grade_house_mappingDTO);
        } else {
            getCommonDAOService().update(am_grade_house_mappingDTO);
        }

        return am_grade_house_mappingDTO;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String actionType = request.getParameter("actionType");
        if ("getApplyForHigherClass".equals(actionType)) {
            try {
                String idString = request.getParameter("id");
                Long id = Long.parseLong(idString);
                int gradeId = Employee_recordsRepository.getInstance().getGradeByEmployeeRecordId(id);
                String text = "";
                int classCat = Integer.parseInt(request.getParameter("classCat"));
                List<Am_grade_house_mappingDTO> am_grade_house_mappingDTOS = Am_grade_house_mappingRepository.getInstance().
                        getAm_grade_house_mappingDTOByam_house_class_cat(classCat);
                int min = Integer.MAX_VALUE;
                for (Am_grade_house_mappingDTO am_grade_house_mappingDTO : am_grade_house_mappingDTOS) {
                    if (am_grade_house_mappingDTO.job_grade_ids.contains(String.valueOf(gradeId))) {
                        text = gradeId + "_0";
                        break;
                    } else {
                        String[] arr = am_grade_house_mappingDTO.job_grade_ids.split(",");
                        for (int i = 0; i < arr.length; i++) {
                            int parserInt = Integer.parseInt(arr[i].trim());
                            if (gradeId > parserInt) {
                                min = Math.min(gradeId - parserInt, min);
                            }
                        }
                        if (min < Integer.MAX_VALUE) {
                            text = gradeId + "_" + min;
                        } else {
                            text = gradeId + "_-1";
                        }
                    }
                }
                ApiResponse.sendSuccessResponse(response, text);
            } catch (Exception ex) {
                logger.error(ex);
                ApiResponse.sendErrorResponse(response, ex.getMessage());
            }
            return;
        }
        super.doGet(request, response);
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.AM_GRADE_HOUSE_MAPPING_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.AM_GRADE_HOUSE_MAPPING_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.AM_GRADE_HOUSE_MAPPING_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Am_grade_house_mappingServlet.class;
    }

}

