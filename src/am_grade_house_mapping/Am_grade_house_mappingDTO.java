package am_grade_house_mapping;
import java.util.*; 
import util.*; 


public class Am_grade_house_mappingDTO extends CommonDTO
{

	public int amHouseClassCat = -1;
	public String job_grade_ids = "";
	public long insertedByUserId = -1;
	public long insertedByOrganogramId = -1;
	public long modifiedBy = -1;
	public long insertionDate = -1;
	public double minimum_basic_salary = 0.0;
	
	
    @Override
	public String toString() {
            return "$Am_grade_house_mappingDTO[" +
            " iD = " + iD +
            " amHouseClassCat = " + amHouseClassCat +
            " job_grade_ids = " + job_grade_ids +
			" minimum_basic_salary = " + minimum_basic_salary +
            " insertedByUserId = " + insertedByUserId +
            " insertedByOrganogramId = " + insertedByOrganogramId +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " searchColumn = " + searchColumn +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}