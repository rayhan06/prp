package am_grade_house_mapping;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pb.CatRepository;
import pb.CategoryLanguageModel;
import repository.Repository;
import repository.RepositoryManager;
import util.UtilCharacter;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


public class Am_grade_house_mappingRepository implements Repository {
	Am_grade_house_mappingDAO am_grade_house_mappingDAO;
	Gson gson = new Gson();
	
	public void setDAO(Am_grade_house_mappingDAO am_grade_house_mappingDAO)
	{
		this.am_grade_house_mappingDAO = am_grade_house_mappingDAO;
	}
	
	
	static Logger logger = Logger.getLogger(Am_grade_house_mappingRepository.class);
	Map<Long, Am_grade_house_mappingDTO>mapOfAm_grade_house_mappingDTOToiD;
	Map<Integer, Set<Am_grade_house_mappingDTO> >mapOfAm_grade_house_mappingDTOToamHouseClassCat;

  
	private Am_grade_house_mappingRepository(){
		am_grade_house_mappingDAO = Am_grade_house_mappingDAO.getInstance();
		mapOfAm_grade_house_mappingDTOToiD = new ConcurrentHashMap<>();
		mapOfAm_grade_house_mappingDTOToamHouseClassCat = new ConcurrentHashMap<>();

		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        static final Am_grade_house_mappingRepository INSTANCE = new Am_grade_house_mappingRepository();
    }

    public static Am_grade_house_mappingRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Am_grade_house_mappingDTO> am_grade_house_mappingDTOs = am_grade_house_mappingDAO.getAllDTOs(reloadAll);
			for(Am_grade_house_mappingDTO am_grade_house_mappingDTO : am_grade_house_mappingDTOs) {
				Am_grade_house_mappingDTO oldAm_grade_house_mappingDTO = getAm_grade_house_mappingDTOByIDWithoutClone(am_grade_house_mappingDTO.iD);
				if( oldAm_grade_house_mappingDTO != null ) {
					mapOfAm_grade_house_mappingDTOToiD.remove(oldAm_grade_house_mappingDTO.iD);
				
					if(mapOfAm_grade_house_mappingDTOToamHouseClassCat.containsKey(oldAm_grade_house_mappingDTO.amHouseClassCat)) {
						mapOfAm_grade_house_mappingDTOToamHouseClassCat.get(oldAm_grade_house_mappingDTO.amHouseClassCat).remove(oldAm_grade_house_mappingDTO);
					}
					if(mapOfAm_grade_house_mappingDTOToamHouseClassCat.get(oldAm_grade_house_mappingDTO.amHouseClassCat).isEmpty()) {
						mapOfAm_grade_house_mappingDTOToamHouseClassCat.remove(oldAm_grade_house_mappingDTO.amHouseClassCat);
					}
					
					
				}
				if(am_grade_house_mappingDTO.isDeleted == 0) 
				{
					
					mapOfAm_grade_house_mappingDTOToiD.put(am_grade_house_mappingDTO.iD, am_grade_house_mappingDTO);
				
					if( ! mapOfAm_grade_house_mappingDTOToamHouseClassCat.containsKey(am_grade_house_mappingDTO.amHouseClassCat)) {
						mapOfAm_grade_house_mappingDTOToamHouseClassCat.put(am_grade_house_mappingDTO.amHouseClassCat, new HashSet<>());
					}
					mapOfAm_grade_house_mappingDTOToamHouseClassCat.get(am_grade_house_mappingDTO.amHouseClassCat).add(am_grade_house_mappingDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public List<Am_grade_house_mappingDTO> getAm_grade_house_mappingList() {
		return clone(new ArrayList<>(this.mapOfAm_grade_house_mappingDTOToiD.values()));
	}
	
	
	public Am_grade_house_mappingDTO getAm_grade_house_mappingDTOByIDWithoutClone( long ID){
		return mapOfAm_grade_house_mappingDTOToiD.get(ID);
	}

	public Am_grade_house_mappingDTO getAm_grade_house_mappingDTOByID( long ID){
		return clone(mapOfAm_grade_house_mappingDTOToiD.get(ID));
	}
	
	
	public List<Am_grade_house_mappingDTO> getAm_grade_house_mappingDTOByam_house_class_cat(int am_house_class_cat) {
		return clone(new ArrayList<>( mapOfAm_grade_house_mappingDTOToamHouseClassCat.getOrDefault
				(am_house_class_cat,new HashSet<>())));
	}

	
	@Override
	public String getTableName() {
		return "am_grade_house_mapping";
	}

	public Am_grade_house_mappingDTO clone(Am_grade_house_mappingDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Am_grade_house_mappingDTO.class);
	}

	public List<Am_grade_house_mappingDTO> clone(List<Am_grade_house_mappingDTO> dtoList) {
		return dtoList
				.stream()
				.map(this::clone)
				.collect(Collectors.toList());
	}

	public String getMsg(long gradeId, String language){
		String msg = "";
		if(gradeId > 0){
			List<Am_grade_house_mappingDTO> dtos = getAm_grade_house_mappingList();
			List<Long> ids;
			CategoryLanguageModel model;
			for (Am_grade_house_mappingDTO am_grade_house_mappingDTO : dtos) {
				if (am_grade_house_mappingDTO.job_grade_ids != null) {
					ids = Arrays.stream(am_grade_house_mappingDTO.job_grade_ids.split(","))
							.map(String::trim)
							.map(Long::parseLong)
							.collect(Collectors.toList());

					if(ids.contains(gradeId)){
						model = CatRepository.getInstance().getCategoryLanguageModel
								("job_grade_type", (int)gradeId);
						if(model != null){
							String bn = "সর্বোচ্চ " + model.banglaText +  " বাসা বরাদ্দ প্রাপ্তির যোগ্য ";
							String en = "Highest eligibility for " + model.englishText + " house";
							msg = UtilCharacter.getDataByLanguage(language, bn, en);
						}
						break;
					}
				}
			}
		}

		return msg;
	}
}


