package am_grade_house_mapping;

import common.CommonDAOService;
import employee_records.Employee_recordsDTO;
import employee_records.Employee_recordsRepository;
import org.apache.log4j.Logger;
import pb.*;
import recruitment_job_description.Recruitment_job_descriptionDTO;
import recruitment_job_description.Recruitment_job_descriptionRepository;
import util.CommonDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Am_grade_house_mappingDAO  implements CommonDAOService<Am_grade_house_mappingDTO>
{

	private static final Logger logger = Logger.getLogger(Am_grade_house_mappingDAO.class);
	private final Map<String,String> searchMap = new HashMap<>();

	private static final String addQuery = "INSERT INTO {tableName} (inserted_by_user_id,inserted_by_organogram_id," +
			"insertion_date, modified_by, search_column, am_house_class_cat, job_grade_ids," +
			" minimum_basic_salary,  lastModificationTime, isDeleted,ID) VALUES (?,?,?,?,?,?,?,?, ?, ?, ?)";

	private static final String updateQuery = "UPDATE {tableName} SET inserted_by_user_id = ?,inserted_by_organogram_id=?," +
			" insertion_date=?, modified_by = ?, search_column=?,am_house_class_cat=?, job_grade_ids = ?," +
			" minimum_basic_salary = ?, lastModificationTime=? WHERE ID = ?";

	private static class LazyLoader{
		static final Am_grade_house_mappingDAO INSTANCE = new Am_grade_house_mappingDAO();
	}

	public static Am_grade_house_mappingDAO getInstance(){
		return Am_grade_house_mappingDAO.LazyLoader.INSTANCE;
	}

	private Am_grade_house_mappingDAO() {
		searchMap.put("am_house_class_cat"," and (am_house_class_cat = ?)");
		searchMap.put("job_grade_cat"," and (job_grade_ids like ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	
	public void setSearchColumn(Am_grade_house_mappingDTO am_grade_house_mappingDTO)
	{
		am_grade_house_mappingDTO.searchColumn = "";
		CategoryLanguageModel model = CatRepository.getInstance().getCategoryLanguageModel
				("am_house_class",am_grade_house_mappingDTO.amHouseClassCat);
		if(model != null){
			am_grade_house_mappingDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
		}

//		model = CatRepository.getInstance().getCategoryLanguageModel
//				("job_grade",am_grade_house_mappingDTO.job_grade_ids);
//		if(model != null){
//			am_grade_house_mappingDTO.searchColumn += model.englishText+" "+model.banglaText+" ";
//		}
	}


	@Override
	public void set(PreparedStatement ps, Am_grade_house_mappingDTO am_grade_house_mappingDTO, boolean isInsert) throws SQLException {
		setSearchColumn(am_grade_house_mappingDTO);
		int index = 0;
		ps.setObject(++index, am_grade_house_mappingDTO.insertedByUserId);
		ps.setObject(++index, am_grade_house_mappingDTO.insertedByOrganogramId);
		ps.setObject(++index, am_grade_house_mappingDTO.insertionDate);
		ps.setObject(++index, am_grade_house_mappingDTO.modifiedBy);
		ps.setObject(++index, am_grade_house_mappingDTO.searchColumn);
		ps.setObject(++index, am_grade_house_mappingDTO.amHouseClassCat);
		ps.setObject(++index, am_grade_house_mappingDTO.job_grade_ids);
		ps.setObject(++index, am_grade_house_mappingDTO.minimum_basic_salary);
		ps.setObject(++index, am_grade_house_mappingDTO.lastModificationTime);
		if (isInsert) {
			ps.setObject(++index, 0);
		}
		ps.setObject(++index, am_grade_house_mappingDTO.iD);
	}

	@Override
	public Am_grade_house_mappingDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Am_grade_house_mappingDTO am_grade_house_mappingDTO = new Am_grade_house_mappingDTO();
			am_grade_house_mappingDTO.iD = rs.getLong("ID");
			am_grade_house_mappingDTO.amHouseClassCat = rs.getInt("am_house_class_cat");
			am_grade_house_mappingDTO.job_grade_ids = rs.getString("job_grade_ids");
			am_grade_house_mappingDTO.minimum_basic_salary = rs.getDouble("minimum_basic_salary");
			am_grade_house_mappingDTO.insertedByUserId = rs.getLong("inserted_by_user_id");
			am_grade_house_mappingDTO.insertedByOrganogramId = rs.getLong("inserted_by_organogram_id");
			am_grade_house_mappingDTO.modifiedBy = rs.getLong("modified_by");
			am_grade_house_mappingDTO.insertionDate = rs.getLong("insertion_date");
			am_grade_house_mappingDTO.searchColumn = rs.getString("search_column");
			am_grade_house_mappingDTO.isDeleted = rs.getInt("isDeleted");
			am_grade_house_mappingDTO.lastModificationTime = rs.getLong("lastModificationTime");
			return am_grade_house_mappingDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}

	@Override
	public String getTableName() {
		return "am_grade_house_mapping";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	@Override
	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_grade_house_mappingDTO) commonDTO,addQuery,true);
	}

	@Override
	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Am_grade_house_mappingDTO) commonDTO,updateQuery,false);
	}


	public String getBuildOptionsOfGrades(String language,String selectedValues){
		List<OptionDTO> optionDTOList = CatRepository.getDTOs("job_grade_type")
				.stream()
				.map(e->new OptionDTO(e.languageTextEnglish,e.languageTextEnglish,String.valueOf(e.value)))
				.collect(Collectors.toList());
		return Utils.buildOptionsMultipleSelection(optionDTOList,language,selectedValues);
	}
}
	