package complain_action;

import common.BaseServlet;
import common.NameDao;
import common.NameRepository;
import common.NameInterface;
import language.LC;
import permission.MenuConstants;
import sessionmanager.SessionConstants;
import user.UserDTO;
import util.CommonDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@WebServlet("/Complain_actionServlet")
@MultipartConfig
public class Complain_actionServlet extends BaseServlet implements NameInterface {
	private final Complain_actionDAO complainActionDAO = Complain_actionDAO.getInstance();

	public String commonPartOfDispatchURL(){
		return  "common/name";
	}
	@Override
	public String getTableName() {
		return complainActionDAO.getTableName();
	}

	@Override
	public String getServletName() {
		return "Complain_actionServlet";
	}

	@Override
	public NameDao getCommonDAOService() {
		return complainActionDAO;
	}

	@Override
	public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {
		return addT(request,addFlag,userDTO,complainActionDAO);
	}

	@Override
	public int[] getAddPageMenuConstants() {
		return new int[] {MenuConstants.COMPLAIN_ACTION_ADD};
	}

	@Override
	public int[] getEditPageMenuConstants() {
		return new int[] {MenuConstants.COMPLAIN_ACTION_UPDATE};
	}

	@Override
	public int[] getSearchMenuConstants() {
		return new int[] {MenuConstants.COMPLAIN_ACTION_SEARCH};
	}

	@Override
	public Class<? extends HttpServlet> getClazz() {
		return Complain_actionServlet.class;
	}

	@Override
	public int getSearchTitleValue() {
		return LC.COMPLAIN_ACTION_SEARCH_COMPLAIN_ACTION_SEARCH_FORMNAME;
	}

	@Override
	public String get_p_navigatorName() {
		return SessionConstants.NAV_COMPLAIN_ACTION;
	}

	@Override
	public String get_p_dtoCollectionName() {
		return SessionConstants.VIEW_COMPLAIN_ACTION;
	}

	@Override
	public NameRepository getNameRepository() {
		return Complain_actionRepository.getInstance();
	}

	@Override
	public int getAddTitleValue() {
		return LC.COMPLAIN_ACTION_ADD_COMPLAIN_ACTION_ADD_FORMNAME;
	}

	@Override
	public int getEditTitleValue() {
		return LC.COMPLAIN_ACTION_EDIT_COMPLAIN_ACTION_EDIT_FORMNAME;
	}

	@Override
	public void init(HttpServletRequest request) throws ServletException {
		setCommonAttributes(request,getServletName(),getCommonDAOService());
	}
}