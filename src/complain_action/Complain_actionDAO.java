package complain_action;

import common.NameDao;

public class Complain_actionDAO extends NameDao {

    private Complain_actionDAO() {
        super("complain_action");
    }

    private static class LazyLoader{
        static final Complain_actionDAO INSTANCE = new Complain_actionDAO();
    }

    public static Complain_actionDAO getInstance(){
        return LazyLoader.INSTANCE;
    }
}
