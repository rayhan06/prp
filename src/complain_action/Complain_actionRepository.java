package complain_action;

import common.NameRepository;

public class Complain_actionRepository extends NameRepository {

	private static class LazyLoader {
		static Complain_actionRepository INSTANCE = new Complain_actionRepository();
	}

	public static Complain_actionRepository getInstance() {
		return LazyLoader.INSTANCE;
	}

	private Complain_actionRepository() {
		super(Complain_actionDAO.getInstance(), Complain_actionRepository.class);
	}
}
