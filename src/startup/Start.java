package startup;


import api_authentication_and_log.APIAgentDAO;
import bank_name.Bank_nameRepository;
import board.BoardRepository;
import card_info.CardApprovalRepository;
import card_info.CardEmployeeInfoRepository;
import card_info.CardEmployeeOfficeInfoRepository;
import complain_action.Complain_actionRepository;
import config.GlobalConfigurationRepository;
import education_level.DegreeExamRepository;
import education_level.Education_levelRepository;
import election_constituency.Election_constituencyRepository;
import election_details.Election_detailsRepository;
import election_wise_mp.Election_wise_mpRepository;
import employee_offices.EmployeeOfficeRepository;
import employee_records.EmployeeKiller;
import employee_records.Employee_recordsRepository;
import geolocation.*;
import incident.IncidentRepository;
import ipRestriction.IPRestrictionRepository;
import language.LM;
import nationality.NationalityRepository;
import occupation.OccupationRepository;
import office_unit_organograms.OfficeUnitOrganogramsRepository;
import office_units.Office_unitsRepository;
import offices.OfficesRepository;
import pb.CatRepository;
import permission.MenuRepository;
import political_party.Political_partyRepository;
import profession.ProfessionRepository;
import relation.RelationRepository;
import religion.ReligionRepository;
import repository.RepositoryManager;
import result_exam.Result_examRepository;
import role.PermissionRepository;
import theme.ThemeRepository;
import ticket_issues.TicketIssueSubTypeRepository;
import ticket_issues.Ticket_issuesRepository;
import user.UserRepository;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@WebListener
public class Start implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

    	user.LoginSessionListener loginSessionListener = new user.LoginSessionListener();
        System.out.println(Thread.currentThread());
        ExecutorService executorService = Executors.newSingleThreadExecutor(r->{
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            System.out.println("Single thread name : "+thread.getName());
            return thread;
        });
        EmployeeKiller.getInstance();
        executorService.execute(()->{
            System.out.println("executorService is start working : "+Thread.currentThread());
            RepositoryManager.getInstance();
            LM.getInstance();
            IPRestrictionRepository.getInstance();
            Election_detailsRepository.getInstance();
            Political_partyRepository.getInstance();
            Election_constituencyRepository.getInstance();
            Election_wise_mpRepository.getInstance();
            GlobalConfigurationRepository.getInstance();
            UserRepository.getInstance();
            MenuRepository.getInstance();
            PermissionRepository.getInstance();
            CatRepository.getInstance();
            ThemeRepository.getInstance();
            Bank_nameRepository.getInstance();
            Complain_actionRepository.getInstance();
            GeoCountryRepository.getInstance();
            IncidentRepository.getInstance();
            NationalityRepository.getInstance();
            OccupationRepository.getInstance();
            ProfessionRepository.getInstance();
            RelationRepository.getInstance();
            ReligionRepository.getInstance();
            //OfficesRepository.getInstance();
            Office_unitsRepository.getInstance();
            BoardRepository.getInstance();
            Education_levelRepository.getInstance();
            Result_examRepository.getInstance();
            GeoLocationRepository.getInstance();
            GeoLocationTypeRepository.getInstance();
            GeoDivisionRepository.getInstance();
            GeoDistrictRepository.getInstance();
            GeoThanaRepository.getInstance();
            EmployeeOfficeRepository.getInstance();
            DegreeExamRepository.getInstance();
            Employee_recordsRepository.getInstance();
            OfficeUnitOrganogramsRepository.getInstance();
            CardEmployeeInfoRepository.getInstance();
            CardEmployeeOfficeInfoRepository.getInstance();
            CardApprovalRepository.getInstance();
            APIAgentDAO.getInstance();
            Ticket_issuesRepository.getInstance();
            TicketIssueSubTypeRepository.getInstance();
            try {
                Class.forName("training_calender.Training_calenderRepository");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println("executorService is shut down : "+Thread.currentThread());
        });
        executorService.shutdown();
        //SlowWorker.getInstance().start();
        System.out.println("Start END...");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
