package startup;

public interface RunLater {
    void call();
}
