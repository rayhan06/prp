package startup;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class SlowWorker extends Thread {
    private static SlowWorker instance;
    private final BlockingQueue<RunLater> slowQueue;

    public static SlowWorker getInstance() {
        if (instance == null) {
            synchronized (SlowWorker.class) {
                if (instance == null) {
                    instance = new SlowWorker();
                }
            }
        }
        return instance;
    }

    private SlowWorker() {
    	setDaemon(true);
        this.slowQueue = new LinkedBlockingQueue<>();
    }

    public void invoke(RunLater runLater) {
        this.slowQueue.add(runLater);
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.println("Start SlowWorker");
                try {
                    RunLater runLater = slowQueue.take();
                    runLater.call();
                } catch (InterruptedException e) {
                    sleep(1000);
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
