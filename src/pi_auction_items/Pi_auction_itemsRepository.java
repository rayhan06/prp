package pi_auction_items;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Pi_auction_itemsRepository implements Repository {
	Pi_auction_itemsDAO pi_auction_itemsDAO = null;
	
	static Logger logger = Logger.getLogger(Pi_auction_itemsRepository.class);
	Map<Long, Pi_auction_itemsDTO>mapOfPi_auction_itemsDTOToiD;
	Gson gson;

  
	private Pi_auction_itemsRepository(){
		pi_auction_itemsDAO = Pi_auction_itemsDAO.getInstance();
		mapOfPi_auction_itemsDTOToiD = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Pi_auction_itemsRepository INSTANCE = new Pi_auction_itemsRepository();
    }

    public static Pi_auction_itemsRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Pi_auction_itemsDTO> pi_auction_itemsDTOs = pi_auction_itemsDAO.getAllDTOs(reloadAll);
			for(Pi_auction_itemsDTO pi_auction_itemsDTO : pi_auction_itemsDTOs) {
				Pi_auction_itemsDTO oldPi_auction_itemsDTO = getPi_auction_itemsDTOByiD(pi_auction_itemsDTO.iD);
				if( oldPi_auction_itemsDTO != null ) {
					mapOfPi_auction_itemsDTOToiD.remove(oldPi_auction_itemsDTO.iD);
				
					
				}
				if(pi_auction_itemsDTO.isDeleted == 0) 
				{
					
					mapOfPi_auction_itemsDTOToiD.put(pi_auction_itemsDTO.iD, pi_auction_itemsDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public Pi_auction_itemsDTO clone(Pi_auction_itemsDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Pi_auction_itemsDTO.class);
	}
	
	
	public List<Pi_auction_itemsDTO> getPi_auction_itemsList() {
		List <Pi_auction_itemsDTO> pi_auction_itemss = new ArrayList<Pi_auction_itemsDTO>(this.mapOfPi_auction_itemsDTOToiD.values());
		return pi_auction_itemss;
	}
	
	public List<Pi_auction_itemsDTO> copyPi_auction_itemsList() {
		List <Pi_auction_itemsDTO> pi_auction_itemss = getPi_auction_itemsList();
		return pi_auction_itemss
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Pi_auction_itemsDTO getPi_auction_itemsDTOByiD( long iD){
		return mapOfPi_auction_itemsDTOToiD.get(iD);
	}
	
	public Pi_auction_itemsDTO copyPi_auction_itemsDTOByiD( long iD){
		return clone(mapOfPi_auction_itemsDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return pi_auction_itemsDAO.getTableName();
	}
}


