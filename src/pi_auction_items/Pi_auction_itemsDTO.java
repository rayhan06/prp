package pi_auction_items;
import java.util.*; 
import util.*; 


public class Pi_auction_itemsDTO extends CommonDTO
{

	public long piAuctionId = -1;
	public long piPurchaseItemsId = -1;
    public String productName = "";
    public String supplierCompanyName = "";
	public long buyingDate = System.currentTimeMillis();
	public long fiscalYearId = -1;
	public double unitBuyingPrice = -1;
	public long stock = -1;
	public long sellingUnit = -1;
	public double unitSellingPrice = -1;
	public long insertionDate = -1;
    public String insertedBy = "";
    public String modifiedBy = "";
	public long officeUnitId = -1;

	public long itemId = -1;
	
	
    @Override
	public String toString() {
            return "$Pi_auction_itemsDTO[" +
            " iD = " + iD +
            " piAuctionId = " + piAuctionId +
            " piPurchaseItemsId = " + piPurchaseItemsId +
            " productName = " + productName +
            " supplierCompanyName = " + supplierCompanyName +
            " buyingDate = " + buyingDate +
            " fiscalYearId = " + fiscalYearId +
            " unitBuyingPrice = " + unitBuyingPrice +
            " stock = " + stock +
            " sellingUnit = " + sellingUnit +
            " unitSellingPrice = " + unitSellingPrice +
            " insertionDate = " + insertionDate +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            " searchColumn = " + searchColumn +
			" officeUnitId = " + officeUnitId +
			" itemId = " + itemId +
            "]";
    }

}