package pi_auction_items;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import common.ConnectionAndStatementUtil;
import org.apache.log4j.Logger;
import pi_package_vendor.PiPackageVendorChildrenDTO;
import util.*;
import pb.*;
import vm_requisition.CommonApprovalStatus;

public class Pi_auction_itemsDAO  implements CommonDAOService<Pi_auction_itemsDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private static final String getPiAuctionItemsByAuctionId =
			"SELECT * FROM pi_auction_items WHERE  pi_auction_id = %d AND office_unit_id = %d " +
					" AND isDeleted = 0 ORDER BY lastModificationTime DESC";

	private Pi_auction_itemsDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"pi_auction_id",
			"pi_purchase_items_id",
			"product_name",
			"supplier_company_name",
			"buying_date",
			"fiscal_year_id",
			"unit_buying_price",
			"stock",
			"selling_unit",
			"unit_selling_price",
			"insertion_date",
			"inserted_by",
			"modified_by",
			"search_column",
			"office_unit_id",
			"item_id",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("product_name"," and (product_name like ?)");
		searchMap.put("supplier_company_name"," and (supplier_company_name like ?)");
		searchMap.put("buying_date_start"," and (buying_date >= ?)");
		searchMap.put("buying_date_end"," and (buying_date <= ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Pi_auction_itemsDAO INSTANCE = new Pi_auction_itemsDAO();
	}

	public static Pi_auction_itemsDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Pi_auction_itemsDTO pi_auction_itemsDTO)
	{
		pi_auction_itemsDTO.searchColumn = "";
		pi_auction_itemsDTO.searchColumn += pi_auction_itemsDTO.productName + " ";
		pi_auction_itemsDTO.searchColumn += pi_auction_itemsDTO.supplierCompanyName + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Pi_auction_itemsDTO pi_auction_itemsDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(pi_auction_itemsDTO);
		if(isInsert)
		{
			ps.setObject(++index,pi_auction_itemsDTO.iD);
		}
		ps.setObject(++index,pi_auction_itemsDTO.piAuctionId);
		ps.setObject(++index,pi_auction_itemsDTO.piPurchaseItemsId);
		ps.setObject(++index,pi_auction_itemsDTO.productName);
		ps.setObject(++index,pi_auction_itemsDTO.supplierCompanyName);
		ps.setObject(++index,pi_auction_itemsDTO.buyingDate);
		ps.setObject(++index,pi_auction_itemsDTO.fiscalYearId);
		ps.setObject(++index,pi_auction_itemsDTO.unitBuyingPrice);
		ps.setObject(++index,pi_auction_itemsDTO.stock);
		ps.setObject(++index,pi_auction_itemsDTO.sellingUnit);
		ps.setObject(++index,pi_auction_itemsDTO.unitSellingPrice);
		ps.setObject(++index,pi_auction_itemsDTO.insertionDate);
		ps.setObject(++index,pi_auction_itemsDTO.insertedBy);
		ps.setObject(++index,pi_auction_itemsDTO.modifiedBy);
		ps.setObject(++index,pi_auction_itemsDTO.searchColumn);
		ps.setObject(++index,pi_auction_itemsDTO.officeUnitId);
		ps.setObject(++index,pi_auction_itemsDTO.itemId);
		if(isInsert)
		{
			ps.setObject(++index,pi_auction_itemsDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,pi_auction_itemsDTO.iD);
		}
	}
	
	@Override
	public Pi_auction_itemsDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Pi_auction_itemsDTO pi_auction_itemsDTO = new Pi_auction_itemsDTO();
			int i = 0;
			pi_auction_itemsDTO.iD = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.piAuctionId = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.piPurchaseItemsId = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.productName = rs.getString(columnNames[i++]);
			pi_auction_itemsDTO.supplierCompanyName = rs.getString(columnNames[i++]);
			pi_auction_itemsDTO.buyingDate = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.fiscalYearId = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.unitBuyingPrice = rs.getDouble(columnNames[i++]);
			pi_auction_itemsDTO.stock = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.sellingUnit = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.unitSellingPrice = rs.getDouble(columnNames[i++]);
			pi_auction_itemsDTO.insertionDate = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.insertedBy = rs.getString(columnNames[i++]);
			pi_auction_itemsDTO.modifiedBy = rs.getString(columnNames[i++]);
			pi_auction_itemsDTO.searchColumn = rs.getString(columnNames[i++]);
			pi_auction_itemsDTO.officeUnitId = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.itemId = rs.getLong(columnNames[i++]);
			pi_auction_itemsDTO.isDeleted = rs.getInt(columnNames[i++]);
			pi_auction_itemsDTO.lastModificationTime = rs.getLong(columnNames[i++]);

			return pi_auction_itemsDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Pi_auction_itemsDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "pi_auction_items";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_auction_itemsDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Pi_auction_itemsDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }

	public List<Pi_auction_itemsDTO> getPiAuctionItemsByAuctionId(long auctionId,long officeUnitId) {
		String sql = String.format(getPiAuctionItemsByAuctionId, auctionId, officeUnitId);
		return ConnectionAndStatementUtil.getListOfT(sql, this::buildObjectFromResultSet);
	}


				
}
	