package pi_auction_items;
import fiscal_year.Fiscal_yearRepository;
//import org.intellij.lang.annotations.Language;
import pb.Utils;
import pi_package_vendor.PiPackageVendorChildrenDTO;
import pi_package_vendor.PiPackageVendorChildrenRepository;
import pi_purchase.Pi_purchaseDTO;
import pi_unique_item.PiStageEnum;
import pi_unique_item.PiUniqueItemAssignmentDAO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsDTO;
import pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository;
import procurement_goods.Procurement_goodsDTO;
import procurement_goods.Procurement_goodsRepository;
import util.HttpRequestUtils;
import util.StringUtils;

public class PiProductModel {
    public String iD = "";
    public String productName = "";
    public String supplierCompanyName = "";
    public String buyingDate = "";
    public String fiscalYearId = "";

    public String unitBuyingPrice = "";
    public String stock = "";
    public String officeUnitId="";

    public String hiddenBuyingDate = "";
    public String hiddenFiscalYear = "";
    public String itemId = "-1";

    public PiProductModel(){ }
    public PiProductModel(Pi_purchaseDTO dto, String language){
        boolean isLanguageEnglish = "English".equalsIgnoreCase(language);
        Procurement_goodsDTO procurement_goodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(dto.itemId);
        Pi_vendor_auctioneer_detailsDTO pi_vendor_auctioneer_detailsDTO = Pi_vendor_auctioneer_detailsRepository.getInstance().getPi_vendor_auctioneer_detailsDTOByiD(dto.vendorId);
        iD = String.valueOf(dto.iD);
        itemId = String.valueOf(dto.itemId);
        productName = isLanguageEnglish?procurement_goodsDTO.nameEn:procurement_goodsDTO.nameBn;
        supplierCompanyName = isLanguageEnglish?pi_vendor_auctioneer_detailsDTO.nameEn:pi_vendor_auctioneer_detailsDTO.nameBn;
        buyingDate = StringUtils.getFormattedDate(HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language,dto.purchaseDate);
        hiddenBuyingDate = String.valueOf(dto.purchaseDate);
        fiscalYearId = Fiscal_yearRepository.getInstance().getText(dto.fiscalYearId, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language);
        hiddenFiscalYear = String.valueOf(dto.fiscalYearId);
        unitBuyingPrice = String.valueOf(dto.unitPrice);

        int initialStockInt= PiUniqueItemAssignmentDAO.getInstance()
                .getItemCountInStockByItemIdAndPurchaseId(dto.itemId, dto.iD);
//        int initialStockInt= PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStageAndPurchaseIdAllover(dto.officeUnitId, dto.itemId,dto.iD,
//                PiStageEnum.IN_STOCK.getValue());
//			int distributedAmountInt=PiUniqueItemAssignmentDAO.getInstance().getTotalItemCountWithStage(officeUnitId, purchaseDTO.itemId,
//					PiStageEnum.OUT_STOCK.getValue(),purchaseDTO.purchaseDate);
        stock= Utils.getDigits(initialStockInt,"English");

        officeUnitId = String.valueOf(dto.officeUnitId);


    }
    


    @Override
    public String toString() {
        return "PiProductModel{" +
                "iD='" + iD + '\'' +
                ", productName='" + productName + '\'' +
                ", supplierCompanyName='" + supplierCompanyName + '\'' +
                ", buyingDate='" + buyingDate + '\'' +
                ", fiscalYearId='" + fiscalYearId + '\'' +
                ", unitBuyingPrice='" + unitBuyingPrice + '\'' +
                ", stock='" + stock + '\'' +
                '}';
    }
}
