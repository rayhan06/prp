package pi_auction_items;

import com.google.gson.Gson;
import common.ApiResponse;
import common.BaseServlet;
import economic_sub_code.EconomicSubCodeModel;
import economic_sub_code.Economic_sub_codeRepository;
import fiscal_year.Fiscal_yearRepository;
import language.LC;
import language.LM;
import login.LoginDTO;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import pb.ErrorMessage;
import pb.Utils;
import permission.MenuConstants;
import pi_package_vendor.PiPackageVendorChildrenRepository;
import pi_purchase.Pi_purchaseDAO;
import pi_purchase.Pi_purchaseDTO;
import sessionmanager.SessionConstants;
import user.UserDTO;
import user.UserRepository;
import util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static permission.MenuConstants.*;


/**
 * Servlet implementation class Pi_auction_itemsServlet
 */
@WebServlet("/Pi_auction_itemsServlet")
@MultipartConfig
public class Pi_auction_itemsServlet extends BaseServlet
{
	private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Pi_auction_itemsServlet.class);

    @Override
    public String getTableName() {
        return Pi_auction_itemsDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Pi_auction_itemsServlet";
    }

    @Override
    public Pi_auction_itemsDAO getCommonDAOService() {
        return Pi_auction_itemsDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[] {MenuConstants.PI_AUCTION_ITEMS_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[] {MenuConstants.PI_AUCTION_ITEMS_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[] {MenuConstants.PI_AUCTION_ITEMS_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Pi_auction_itemsServlet.class;
    }
    private final Gson gson = new Gson();


	@Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub
		try
		{
			request.setAttribute("failureMessage", "");
			System.out.println("%%%% addPi_auction_items");
			String language = userDTO.languageID == SessionConstants.ENGLISH? "English":"Bangla";
			Pi_auction_itemsDTO pi_auction_itemsDTO;
			SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			if(addFlag == true)
			{
				pi_auction_itemsDTO = new Pi_auction_itemsDTO();
			}
			else
			{
				pi_auction_itemsDTO = Pi_auction_itemsDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
			}

			String Value = "";

			Value = request.getParameter("piAuctionId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("piAuctionId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_auction_itemsDTO.piAuctionId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("piPurchaseItemsId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("piPurchaseItemsId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_auction_itemsDTO.piPurchaseItemsId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("productName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("productName = " + Value);
			if(Value != null)
			{
				pi_auction_itemsDTO.productName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("supplierCompanyName");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("supplierCompanyName = " + Value);
			if(Value != null)
			{
				pi_auction_itemsDTO.supplierCompanyName = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("buyingDate");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("buyingDate = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				try
				{
					Date d = f.parse(Value);
					pi_auction_itemsDTO.buyingDate = d.getTime();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception(LM.getText(LC.PI_AUCTION_ITEMS_ADD_BUYINGDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
				}
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("fiscalYearId");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("fiscalYearId = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_auction_itemsDTO.fiscalYearId = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitBuyingPrice");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitBuyingPrice = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_auction_itemsDTO.unitBuyingPrice = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("stock");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("stock = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_auction_itemsDTO.stock = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("sellingUnit");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("sellingUnit = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_auction_itemsDTO.sellingUnit = Long.parseLong(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("unitSellingPrice");

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("unitSellingPrice = " + Value);
			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				pi_auction_itemsDTO.unitSellingPrice = Double.parseDouble(Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			if(addFlag)
			{
				pi_auction_itemsDTO.insertionDate = TimeConverter.getToday();
			}


			Value = request.getParameter("insertedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("insertedBy = " + Value);
			if(Value != null)
			{
				pi_auction_itemsDTO.insertedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("modifiedBy");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("modifiedBy = " + Value);
			if(Value != null)
			{
				pi_auction_itemsDTO.modifiedBy = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			Value = request.getParameter("searchColumn");

			if(Value != null)
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}
			System.out.println("searchColumn = " + Value);
			if(Value != null)
			{
				pi_auction_itemsDTO.searchColumn = (Value);
			}
			else
			{
				System.out.println("FieldName has a null Value, not updating" + " = " + Value);
			}

			System.out.println("Done adding  addPi_auction_items dto = " + pi_auction_itemsDTO);
			long returnedID = -1;


			if(addFlag == true)
			{
				returnedID = Pi_auction_itemsDAO.getInstance().add(pi_auction_itemsDTO);
			}
			else
			{
				returnedID = Pi_auction_itemsDAO.getInstance().update(pi_auction_itemsDTO);
			}


			return pi_auction_itemsDTO;

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public  void  multipleAddT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception
	{
		// TODO Auto-generated method stub
		String[] paramValues = request.getParameterValues("piPurchaseItemsId");
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		for (int i = 0; i < paramValues.length; i++)
		{

			Pi_auction_itemsDTO pi_auction_itemsDTO = new Pi_auction_itemsDTO();
			pi_auction_itemsDTO.insertionDate = pi_auction_itemsDTO.lastModificationTime = System.currentTimeMillis();
			pi_auction_itemsDTO.insertedBy = pi_auction_itemsDTO.modifiedBy = String.valueOf(userDTO.ID);


			String Value = "";

			Value = request.getParameter("piPurchaseItemsId");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide product");
			}
			pi_auction_itemsDTO.piPurchaseItemsId = Long.parseLong(Value);

			Value = request.getParameter("productName");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide product name");
			}
			pi_auction_itemsDTO.productName = (Value);

			Value = request.getParameter("supplierCompanyName");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide supplier company name");
			}
			pi_auction_itemsDTO.supplierCompanyName = (Value);


			Value = request.getParameter("buyingDate");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide buying date");
			}
//			Date d = f.parse(Value);
//			pi_auction_itemsDTO.buyingDate = d.getTime();
			pi_auction_itemsDTO.buyingDate = System.currentTimeMillis();

			Value = request.getParameter("fiscalYearId");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide fiscal year");
			}
			pi_auction_itemsDTO.fiscalYearId = 1000;

			Value = request.getParameter("unitBuyingPrice");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide unit buying price");
			}
			pi_auction_itemsDTO.unitBuyingPrice = Double.parseDouble(Value);

			

			Value = request.getParameter("stock");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide stock");
			}
			pi_auction_itemsDTO.stock = Long.parseLong(Value);

			Value = request.getParameter("sellingUnit");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide selling unit");
			}
			pi_auction_itemsDTO.sellingUnit = Long.parseLong(Value);


			Value = request.getParameter("unitSellingPrice");
			if (Value == null ||Value.equals("") || Value.equals("-1")) {
				throw new Exception("Please provide unit selling price");
			}
			pi_auction_itemsDTO.unitSellingPrice = Double.parseDouble(Value);

			if(Value != null && !Value.equalsIgnoreCase(""))
			{
				Value = Jsoup.clean(Value,Whitelist.simpleText());
			}

			logger.debug("Done adding  addPi_auction_items dto = " + pi_auction_itemsDTO);
			Pi_auction_itemsDAO.getInstance().add(pi_auction_itemsDTO);
		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}

		try {
			String actionType = request.getParameter("actionType");
			switch (actionType) {
				case "buildProductModalTableData":
					long productId = Long.parseLong(request.getParameter("productId"));
					long officeUnitId = Long.parseLong(request.getParameter("officeUnitId"));
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					/*List<Pi_purchaseDTO> pi_purchaseDTOS = Pi_purchaseDAO.getInstance().
							getPurchaseDetailsByItemId(officeUnitId,productId);*/

					List<Pi_purchaseDTO> pi_purchaseDTOS = Pi_purchaseDAO.getInstance().
							getPurchaseDetailsByOnlyItemId(productId);



					List<PiProductModel> modelList = null;

					if(pi_purchaseDTOS!=null &&  !pi_purchaseDTOS.isEmpty()){
						modelList = pi_purchaseDTOS
								.stream()
								.map(dto -> new PiProductModel(dto, HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language))
								.collect(Collectors.toList());
					}



					response.getWriter().println(gson.toJson(modelList));
					return;


				default:
					super.doGet(request, response);
					return;
			}
		} catch (Exception ex) {
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LoginDTO loginDTO = (LoginDTO) request.getSession().getAttribute(SessionConstants.USER_LOGIN);
		UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
		if (userDTO == null) {
			request.getRequestDispatcher("common/error_page.jsp").forward(request, response);
			return;
		}
		try {
			switch (request.getParameter("actionType")) {
				case "getSelectedProductData":
					if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_AUCTION_ITEMS_SEARCH)) {
						request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(INVENTORY_AND_PURCHASE_MANAGEMENT,
								PI_AUCTION_MANAGEMENT,
								PI_AUCTION_CREATE_NEW_LIST)));
						request.getRequestDispatcher("pi_auction_items/pi_auction_itemsPreviewSearch.jsp").forward(request, response);

						return;
					}
					break;
				case "multiple_ajax_add":

					if (Utils.checkPermission(String.valueOf(userDTO.employee_record_id), userDTO, MenuConstants.PI_AUCTION_ITEMS_ADD)) {
						request.getRequestDispatcher("pi_auction_items/pi_auction_itemsPreviewSearch.jsp").forward(request, response);

						return;
					}
					break;

			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		}
		request.getRequestDispatcher("common/error_page.jsp").forward(request, response);


	}
}

