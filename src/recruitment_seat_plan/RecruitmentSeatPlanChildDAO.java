package recruitment_seat_plan;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class RecruitmentSeatPlanChildDAO  implements CommonDAOService<RecruitmentSeatPlanChildDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private RecruitmentSeatPlanChildDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"recruitment_test_name_id",
			"recruitment_job_description_id",
			"recruitment_job_specific_exam_type_id",
			"recruitment_seat_plan_id",
			"exam_date",
			"exam_time",
			"recruitment_exam_venue_id",
			"recruitment_exam_venue_item_id",
			"building",
			"floor",
			"room_no",
			"capacity",
			"start_roll",
			"end_roll",
			"total",
			"isRangeAutoCount",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("exam_date_start"," and (exam_date >= ?)");
		searchMap.put("exam_date_end"," and (exam_date <= ?)");
		searchMap.put("building"," and (building like ?)");
		searchMap.put("floor"," and (floor like ?)");
		searchMap.put("room_no"," and (room_no like ?)");
		searchMap.put("capacity"," and (capacity like ?)");
		searchMap.put("start_roll"," and (start_roll like ?)");
		searchMap.put("end_roll"," and (end_roll like ?)");
		searchMap.put("total"," and (total like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final RecruitmentSeatPlanChildDAO INSTANCE = new RecruitmentSeatPlanChildDAO();
	}

	public static RecruitmentSeatPlanChildDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(RecruitmentSeatPlanChildDTO recruitmentseatplanchildDTO)
	{
		recruitmentseatplanchildDTO.searchColumn = "";
		recruitmentseatplanchildDTO.searchColumn += recruitmentseatplanchildDTO.building + " ";
		recruitmentseatplanchildDTO.searchColumn += recruitmentseatplanchildDTO.floor + " ";
		recruitmentseatplanchildDTO.searchColumn += recruitmentseatplanchildDTO.roomNo + " ";
		recruitmentseatplanchildDTO.searchColumn += recruitmentseatplanchildDTO.capacity + " ";
		recruitmentseatplanchildDTO.searchColumn += recruitmentseatplanchildDTO.startRoll + " ";
		recruitmentseatplanchildDTO.searchColumn += recruitmentseatplanchildDTO.endRoll + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, RecruitmentSeatPlanChildDTO recruitmentseatplanchildDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitmentseatplanchildDTO);
		if(isInsert)
		{
			ps.setObject(++index,recruitmentseatplanchildDTO.iD);
		}
		ps.setObject(++index,recruitmentseatplanchildDTO.recruitmentTestNameId);
		ps.setObject(++index,recruitmentseatplanchildDTO.recruitmentJobDescriptionId);
		ps.setObject(++index,recruitmentseatplanchildDTO.recruitmentJobSpecificExamTypeId);
		ps.setObject(++index,recruitmentseatplanchildDTO.recruitmentSeatPlanId);
		ps.setObject(++index,recruitmentseatplanchildDTO.examDate);
		ps.setObject(++index,recruitmentseatplanchildDTO.examTime);
		ps.setObject(++index,recruitmentseatplanchildDTO.recruitmentExamVenueId);
		ps.setObject(++index,recruitmentseatplanchildDTO.recruitmentExamVenueItemId);
		ps.setObject(++index,recruitmentseatplanchildDTO.building);
		ps.setObject(++index,recruitmentseatplanchildDTO.floor);
		ps.setObject(++index,recruitmentseatplanchildDTO.roomNo);
		ps.setObject(++index,recruitmentseatplanchildDTO.capacity);
		ps.setObject(++index,recruitmentseatplanchildDTO.startRoll);
		ps.setObject(++index,recruitmentseatplanchildDTO.endRoll);
		ps.setObject(++index,recruitmentseatplanchildDTO.total);
		ps.setObject(++index,recruitmentseatplanchildDTO.isRangeAutoCount);
		ps.setObject(++index,recruitmentseatplanchildDTO.searchColumn);
		ps.setObject(++index,recruitmentseatplanchildDTO.insertedBy);
		ps.setObject(++index,recruitmentseatplanchildDTO.modifiedBy);
		ps.setObject(++index,recruitmentseatplanchildDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,recruitmentseatplanchildDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,recruitmentseatplanchildDTO.iD);
		}
	}
	
	@Override
	public RecruitmentSeatPlanChildDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			RecruitmentSeatPlanChildDTO recruitmentseatplanchildDTO = new RecruitmentSeatPlanChildDTO();
			int i = 0;
			recruitmentseatplanchildDTO.iD = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.recruitmentTestNameId = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.recruitmentJobDescriptionId = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.recruitmentJobSpecificExamTypeId = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.recruitmentSeatPlanId = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.examDate = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.examTime = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.recruitmentExamVenueId = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.recruitmentExamVenueItemId = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.building = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.floor = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.roomNo = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.capacity = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.startRoll = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.endRoll = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.total = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.isRangeAutoCount = rs.getBoolean(columnNames[i++]);
			recruitmentseatplanchildDTO.searchColumn = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.insertedBy = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.modifiedBy = rs.getString(columnNames[i++]);
			recruitmentseatplanchildDTO.insertionDate = rs.getLong(columnNames[i++]);
			recruitmentseatplanchildDTO.isDeleted = rs.getInt(columnNames[i++]);
			recruitmentseatplanchildDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return recruitmentseatplanchildDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public RecruitmentSeatPlanChildDTO getDTOByID (long id)
	{
		return getDTOFromID(id);
	}

	@Override
	public String getTableName() {
		return "recruitment_seat_plan_child";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((RecruitmentSeatPlanChildDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((RecruitmentSeatPlanChildDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	