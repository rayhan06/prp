package recruitment_seat_plan;

import java.io.IOException;
import java.io.*;
import java.text.SimpleDateFormat;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;


import job_applicant_application.Job_applicant_applicationDAO;
import org.apache.log4j.Logger;

import login.LoginDTO;
import permission.MenuConstants;
import recruitment_exam_venue.RecruitmentExamVenueItemDTO;
import recruitment_exam_venue.RecruitmentExamVenueItemRepository;
import recruitment_exam_venue.Recruitment_exam_venueDAO;
import recruitment_exam_venue.Recruitment_exam_venueDTO;
import role.PermissionRepository;

import sessionmanager.SessionConstants;
import user.UserDTO;
import util.*;

import javax.servlet.http.*;
import java.util.*;


import geolocation.GeoLocationDAO2;
import language.LC;
import language.LM;
import common.ApiResponse;
import common.BaseServlet;
import com.google.gson.Gson;
import pb.*;
import pbReport.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;


/**
 * Servlet implementation class Recruitment_seat_planServlet
 */
@WebServlet("/Recruitment_seat_planServlet")
@MultipartConfig
public class Recruitment_seat_planServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
    public static Logger logger = Logger.getLogger(Recruitment_seat_planServlet.class);

    @Override
    public String getTableName() {
        return Recruitment_seat_planDAO.getInstance().getTableName();
    }

    @Override
    public String getServletName() {
        return "Recruitment_seat_planServlet";
    }

    @Override
    public Recruitment_seat_planDAO getCommonDAOService() {
        return Recruitment_seat_planDAO.getInstance();
    }

    @Override
    public int[] getAddPageMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_SEAT_PLAN_ADD};
    }

    @Override
    public int[] getEditPageMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_SEAT_PLAN_UPDATE};
    }

    @Override
    public int[] getSearchMenuConstants() {
        return new int[]{MenuConstants.RECRUITMENT_SEAT_PLAN_SEARCH};
    }

    @Override
    public Class<? extends HttpServlet> getClazz() {
        return Recruitment_seat_planServlet.class;
    }

    RecruitmentSeatPlanChildDAO recruitmentSeatPlanChildDAO = RecruitmentSeatPlanChildDAO.getInstance();
    private final Gson gson = new Gson();

    Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();

    @Override
    public CommonDTO addT(HttpServletRequest request, Boolean addFlag, UserDTO userDTO) throws Exception {

        request.setAttribute("failureMessage", "");
        String language = userDTO.languageID == SessionConstants.ENGLISH ? "English" : "Bangla";
        Recruitment_seat_planDTO recruitment_seat_planDTO;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
        boolean isLanEng = Language.equalsIgnoreCase("English");

        if (addFlag) {
            recruitment_seat_planDTO = new Recruitment_seat_planDTO();
            recruitment_seat_planDTO.insertionDate = recruitment_seat_planDTO.lastModificationTime = System.currentTimeMillis();
            recruitment_seat_planDTO.insertedBy = recruitment_seat_planDTO.modifiedBy = String.valueOf(userDTO.ID);
        } else {
            recruitment_seat_planDTO = Recruitment_seat_planDAO.getInstance().getDTOFromID(Long.parseLong(request.getParameter("iD")));
            if (recruitment_seat_planDTO == null) {
                throw new Exception(isLanEng ? "Recruitment exam seat plan information is not found" : "এক্সাম সিট প্ল্যান তথ্য খুঁজে পাওয়া যায়নি");
            }
            recruitment_seat_planDTO.lastModificationTime = System.currentTimeMillis();
            recruitment_seat_planDTO.modifiedBy = String.valueOf(userDTO.ID);
        }

        String Value = "";

        if(addFlag){

            Value = request.getParameter("recruitmentTestNameId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            if (Value != null && !Value.equalsIgnoreCase("")) {
                recruitment_seat_planDTO.recruitmentTestNameId = Long.parseLong(Value);
            } else {

            }

            Value = request.getParameter("recruitmentJobDescriptionId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            if (Value != null && !Value.equalsIgnoreCase("")) {
                recruitment_seat_planDTO.recruitmentJobDescriptionId = Long.parseLong(Value);
            } else {

            }

            Value = request.getParameter("recruitmentJobSpecificExamTypeId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            if (Value != null && !Value.equalsIgnoreCase("")) {
                recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId = Long.parseLong(Value);
            } else {

            }

            Value = request.getParameter("recruitmentExamVenueId");

            if (Value != null && !Value.equalsIgnoreCase("")) {
                Value = Jsoup.clean(Value, Whitelist.simpleText());
            }
            if (Value != null && !Value.equalsIgnoreCase("")) {
                recruitment_seat_planDTO.recruitmentExamVenueId = Long.parseLong(Value);
            } else {

            }

        }



        Value = request.getParameter("examDate");

        if (Value != null && !Value.equalsIgnoreCase("")) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null && !Value.equalsIgnoreCase("")) {
            try {
                Date d = f.parse(Value);
                recruitment_seat_planDTO.examDate = d.getTime();
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception(LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_EXAMDATE, language) + " " + ErrorMessage.getInvalidMessage(language));
            }
        } else {

        }

        Value = request.getParameter("examTime");

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        }
        if (Value != null) {
            recruitment_seat_planDTO.examTime = (Value);
        } else {
        }


        List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOList = createRecruitmentSeatPlanChildDTOListByRequest(request, language,recruitment_seat_planDTO);

        if (addFlag) {
             Recruitment_seat_planDAO.getInstance().add(recruitment_seat_planDTO);
        } else {
            Recruitment_seat_planDAO.getInstance().update(recruitment_seat_planDTO);
        }


        if (addFlag)
        {
            if (recruitmentSeatPlanChildDTOList != null) {
                for (RecruitmentSeatPlanChildDTO recruitmentSeatPlanChildDTO : recruitmentSeatPlanChildDTOList) {
                    recruitmentSeatPlanChildDTO.recruitmentSeatPlanId = recruitment_seat_planDTO.iD;
                    recruitmentSeatPlanChildDAO.add(recruitmentSeatPlanChildDTO);
                    if(!recruitmentSeatPlanChildDTO.startRoll.isEmpty() && !recruitmentSeatPlanChildDTO.endRoll.isEmpty()) {
                        job_applicant_applicationDAO.updateSeatPlan(recruitmentSeatPlanChildDTO);
                    }

                }
            }

        }
        else {
            List<Long> childIdsFromRequest = recruitmentSeatPlanChildDAO.getChildIdsFromRequest(request, "recruitmentSeatPlanChild");
            //delete the removed children
            recruitmentSeatPlanChildDAO.deleteChildrenNotInList("recruitment_seat_plan", "recruitment_seat_plan_child", recruitment_seat_planDTO.iD, childIdsFromRequest);
            List<Long> childIDsInDatabase = recruitmentSeatPlanChildDAO.getChilIds("recruitment_seat_plan", "recruitment_seat_plan_child", recruitment_seat_planDTO.iD);


            if (childIdsFromRequest != null) {
                for (int i = 0; i < childIdsFromRequest.size(); i++) {
                    Long childIDFromRequest = childIdsFromRequest.get(i);
                    if (childIDsInDatabase.contains(childIDFromRequest)) {
                        RecruitmentSeatPlanChildDTO recruitmentSeatPlanChildDTO = createRecruitmentSeatPlanChildDTOByRequestAndIndex(request, false, i, language,recruitment_seat_planDTO);
                        recruitmentSeatPlanChildDTO.recruitmentSeatPlanId = recruitment_seat_planDTO.iD;
                        recruitmentSeatPlanChildDAO.update(recruitmentSeatPlanChildDTO);
                        if(!recruitmentSeatPlanChildDTO.startRoll.isEmpty() && !recruitmentSeatPlanChildDTO.endRoll.isEmpty()) {
                            job_applicant_applicationDAO.updateSeatPlan(recruitmentSeatPlanChildDTO);
                        }
                    } else {
                        RecruitmentSeatPlanChildDTO recruitmentSeatPlanChildDTO = createRecruitmentSeatPlanChildDTOByRequestAndIndex(request, true, i, language,recruitment_seat_planDTO);
                        recruitmentSeatPlanChildDTO.recruitmentSeatPlanId = recruitment_seat_planDTO.iD;
                        recruitmentSeatPlanChildDAO.add(recruitmentSeatPlanChildDTO);
                        if(!recruitmentSeatPlanChildDTO.startRoll.isEmpty() && !recruitmentSeatPlanChildDTO.endRoll.isEmpty()) {
                            job_applicant_applicationDAO.updateSeatPlan(recruitmentSeatPlanChildDTO);
                        }
                    }
                }
            } else {
                recruitmentSeatPlanChildDAO.deleteChildrenByParent(recruitment_seat_planDTO.iD, "recruitment_seat_plan_id");
            }

        }
        return recruitment_seat_planDTO;


    }

    private List<RecruitmentSeatPlanChildDTO> createRecruitmentSeatPlanChildDTOListByRequest(HttpServletRequest request,
                                                                                             String language,Recruitment_seat_planDTO recruitment_seat_planDTO) throws Exception {
        List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOList = new ArrayList<>();
        if (request.getParameterValues("recruitmentSeatPlanChild.iD") != null) {
            int recruitmentSeatPlanChildItemNo = request.getParameterValues("recruitmentSeatPlanChild.iD").length;


            for (int index = 0; index < recruitmentSeatPlanChildItemNo; index++) {
                RecruitmentSeatPlanChildDTO recruitmentSeatPlanChildDTO = createRecruitmentSeatPlanChildDTOByRequestAndIndex(request, true, index, language,recruitment_seat_planDTO);
                recruitmentSeatPlanChildDTOList.add(recruitmentSeatPlanChildDTO);
            }

            return recruitmentSeatPlanChildDTOList;
        }
        return null;
    }

    private RecruitmentSeatPlanChildDTO createRecruitmentSeatPlanChildDTOByRequestAndIndex(HttpServletRequest request,
                                                                                           boolean addFlag, int index,
                                                                                           String language, Recruitment_seat_planDTO recruitment_seat_planDTO) throws Exception {

        RecruitmentSeatPlanChildDTO recruitmentSeatPlanChildDTO;
        if (addFlag) {
            recruitmentSeatPlanChildDTO = new RecruitmentSeatPlanChildDTO();
        } else {
            recruitmentSeatPlanChildDTO = recruitmentSeatPlanChildDAO.getDTOByID(Long.parseLong(request.getParameterValues("recruitmentSeatPlanChild.iD")[index]));
        }
        String path = getServletContext().getRealPath("/img2/");
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

        String Value = "";

        recruitmentSeatPlanChildDTO.recruitmentTestNameId = recruitment_seat_planDTO.recruitmentTestNameId;
        recruitmentSeatPlanChildDTO.recruitmentJobDescriptionId = recruitment_seat_planDTO.recruitmentJobDescriptionId;
        recruitmentSeatPlanChildDTO.recruitmentJobSpecificExamTypeId = recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId;
        recruitmentSeatPlanChildDTO.recruitmentExamVenueId = recruitment_seat_planDTO.recruitmentExamVenueId;
        recruitmentSeatPlanChildDTO.examDate = recruitment_seat_planDTO.examDate;
        recruitmentSeatPlanChildDTO.examTime = recruitment_seat_planDTO.examTime;

        Value = request.getParameterValues("recruitmentSeatPlanChild.recruitmentExamVenueItemId")[index];
        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_RECRUITMENTEXAMVENUEITEMID, language) + " " + ErrorMessage.getInvalidMessage(language));
        }
        recruitmentSeatPlanChildDTO.recruitmentExamVenueItemId = Long.parseLong(Value);

        RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO = RecruitmentExamVenueItemRepository.getInstance().
                getRecruitmentExamVenueItemDTOByiD(recruitmentSeatPlanChildDTO.recruitmentExamVenueItemId);

        if(recruitmentExamVenueItemDTO != null){
            recruitmentSeatPlanChildDTO.building = recruitmentExamVenueItemDTO.building;
            recruitmentSeatPlanChildDTO.floor = recruitmentExamVenueItemDTO.floor;
            recruitmentSeatPlanChildDTO.roomNo = recruitmentExamVenueItemDTO.roomNo;
            recruitmentSeatPlanChildDTO.capacity = recruitmentExamVenueItemDTO.capacity;
        }


        Value = request.getParameterValues("recruitmentSeatPlanChild.startRoll")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_STARTROLL, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentSeatPlanChildDTO.startRoll = (Value);
        Value = request.getParameterValues("recruitmentSeatPlanChild.endRoll")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_ENDROLL, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentSeatPlanChildDTO.endRoll = (Value);
        Value = request.getParameterValues("recruitmentSeatPlanChild.total")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_TOTAL, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentSeatPlanChildDTO.total = (Value);
        Value = request.getParameterValues("recruitmentSeatPlanChild.isRangeAutoHidden")[index];

        if (Value != null) {
            Value = Jsoup.clean(Value, Whitelist.simpleText());
        } else {
            throw new Exception(LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENT_SEAT_PLAN_CHILD_ISRANGEAUTOCOUNT, language) + " " + ErrorMessage.getInvalidMessage(language));
        }

        recruitmentSeatPlanChildDTO.isRangeAutoCount = Value.equals("1");

        recruitmentSeatPlanChildDTO.lastModificationTime = System.currentTimeMillis();


        return recruitmentSeatPlanChildDTO;

    }
}

