package recruitment_seat_plan;
import java.util.*; 
import util.*; 


public class RecruitmentSeatPlanChildDTO extends CommonDTO
{

	public long recruitmentTestNameId = -1;
	public long recruitmentJobDescriptionId = -1;
	public long recruitmentJobSpecificExamTypeId = -1;
    public long recruitmentExamVenueId = -1;
	public long examDate = System.currentTimeMillis();
    public String examTime = "";

    public long recruitmentSeatPlanId = -1;

	public long recruitmentExamVenueItemId = -1;
	
    public String building = "";
    public String floor = "";
    public String roomNo = "";
    public String capacity = "";

    public String startRoll = "";
    public String endRoll = "";
    public String total = "";
	public boolean isRangeAutoCount = false;
	
    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	public List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$RecruitmentSeatPlanChildDTO[" +
            " iD = " + iD +
            " recruitmentTestNameId = " + recruitmentTestNameId +
            " recruitmentJobDescriptionId = " + recruitmentJobDescriptionId +
            " recruitmentJobSpecificExamTypeId = " + recruitmentJobSpecificExamTypeId +
            " recruitmentSeatPlanId = " + recruitmentSeatPlanId +
            " examDate = " + examDate +
            " examTime = " + examTime +
            " recruitmentExamVenueId = " + recruitmentExamVenueId +
            " recruitmentExamVenueItemId = " + recruitmentExamVenueItemId +
            " building = " + building +
            " floor = " + floor +
            " roomNo = " + roomNo +
            " capacity = " + capacity +
            " startRoll = " + startRoll +
            " endRoll = " + endRoll +
            " total = " + total +
            " isRangeAutoCount = " + isRangeAutoCount +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}