package recruitment_seat_plan;
import java.util.*; 
import util.*; 


public class Recruitment_seat_planDTO extends CommonDTO
{

	public long recruitmentTestNameId = -1;
	public long recruitmentJobDescriptionId = -1;
	public long recruitmentJobSpecificExamTypeId = -1;
	public long recruitmentExamVenueId = -1;
	public long examDate = System.currentTimeMillis();
    public String examTime = "";

    public String insertedBy = "";
    public String modifiedBy = "";
	public long insertionDate = -1;
	
	public List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOList = new ArrayList<>();
	
    @Override
	public String toString() {
            return "$Recruitment_seat_planDTO[" +
            " iD = " + iD +
            " recruitmentTestNameId = " + recruitmentTestNameId +
            " recruitmentJobDescriptionId = " + recruitmentJobDescriptionId +
            " recruitmentJobSpecificExamTypeId = " + recruitmentJobSpecificExamTypeId +
            " recruitmentExamVenueId = " + recruitmentExamVenueId +
            " examDate = " + examDate +
            " examTime = " + examTime +
            " searchColumn = " + searchColumn +
            " insertedBy = " + insertedBy +
            " modifiedBy = " + modifiedBy +
            " insertionDate = " + insertionDate +
            " isDeleted = " + isDeleted +
            " lastModificationTime = " + lastModificationTime +
            "]";
    }

}