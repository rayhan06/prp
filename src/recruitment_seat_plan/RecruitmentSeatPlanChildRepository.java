package recruitment_seat_plan;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class RecruitmentSeatPlanChildRepository implements Repository {
	RecruitmentSeatPlanChildDAO recruitmentseatplanchildDAO = null;
	
	static Logger logger = Logger.getLogger(RecruitmentSeatPlanChildRepository.class);
	Map<Long, RecruitmentSeatPlanChildDTO>mapOfRecruitmentSeatPlanChildDTOToiD;
	Map<Long, Set<RecruitmentSeatPlanChildDTO> >mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId;
	Map<Long, Set<RecruitmentSeatPlanChildDTO> >mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId;
	Map<Long, Set<RecruitmentSeatPlanChildDTO> >mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId;
	Map<Long, Set<RecruitmentSeatPlanChildDTO> >mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId;
	Map<Long, Set<RecruitmentSeatPlanChildDTO> >mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId;
	Map<Long, Set<RecruitmentSeatPlanChildDTO> >mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId;
	Gson gson;

  
	private RecruitmentSeatPlanChildRepository(){
		recruitmentseatplanchildDAO = RecruitmentSeatPlanChildDAO.getInstance();
		mapOfRecruitmentSeatPlanChildDTOToiD = new ConcurrentHashMap<>();
		mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId = new ConcurrentHashMap<>();
		mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId = new ConcurrentHashMap<>();
		mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId = new ConcurrentHashMap<>();
		mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId = new ConcurrentHashMap<>();
		mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId = new ConcurrentHashMap<>();
		mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static RecruitmentSeatPlanChildRepository INSTANCE = new RecruitmentSeatPlanChildRepository();
    }

    public static RecruitmentSeatPlanChildRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<RecruitmentSeatPlanChildDTO> recruitmentseatplanchildDTOs = recruitmentseatplanchildDAO.getAllDTOs(reloadAll);
			for(RecruitmentSeatPlanChildDTO recruitmentseatplanchildDTO : recruitmentseatplanchildDTOs) {
				RecruitmentSeatPlanChildDTO oldRecruitmentSeatPlanChildDTO = getRecruitmentSeatPlanChildDTOByiD(recruitmentseatplanchildDTO.iD);
				if( oldRecruitmentSeatPlanChildDTO != null ) {
					mapOfRecruitmentSeatPlanChildDTOToiD.remove(oldRecruitmentSeatPlanChildDTO.iD);
				
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId.containsKey(oldRecruitmentSeatPlanChildDTO.recruitmentTestNameId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId.get(oldRecruitmentSeatPlanChildDTO.recruitmentTestNameId).remove(oldRecruitmentSeatPlanChildDTO);
					}
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId.get(oldRecruitmentSeatPlanChildDTO.recruitmentTestNameId).isEmpty()) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId.remove(oldRecruitmentSeatPlanChildDTO.recruitmentTestNameId);
					}
					
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId.containsKey(oldRecruitmentSeatPlanChildDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId.get(oldRecruitmentSeatPlanChildDTO.recruitmentJobDescriptionId).remove(oldRecruitmentSeatPlanChildDTO);
					}
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId.get(oldRecruitmentSeatPlanChildDTO.recruitmentJobDescriptionId).isEmpty()) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId.remove(oldRecruitmentSeatPlanChildDTO.recruitmentJobDescriptionId);
					}
					
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId.containsKey(oldRecruitmentSeatPlanChildDTO.recruitmentJobSpecificExamTypeId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId.get(oldRecruitmentSeatPlanChildDTO.recruitmentJobSpecificExamTypeId).remove(oldRecruitmentSeatPlanChildDTO);
					}
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId.get(oldRecruitmentSeatPlanChildDTO.recruitmentJobSpecificExamTypeId).isEmpty()) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId.remove(oldRecruitmentSeatPlanChildDTO.recruitmentJobSpecificExamTypeId);
					}
					
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId.containsKey(oldRecruitmentSeatPlanChildDTO.recruitmentExamVenueId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId.get(oldRecruitmentSeatPlanChildDTO.recruitmentExamVenueId).remove(oldRecruitmentSeatPlanChildDTO);
					}
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId.get(oldRecruitmentSeatPlanChildDTO.recruitmentExamVenueId).isEmpty()) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId.remove(oldRecruitmentSeatPlanChildDTO.recruitmentExamVenueId);
					}
					
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId.containsKey(oldRecruitmentSeatPlanChildDTO.recruitmentExamVenueItemId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId.get(oldRecruitmentSeatPlanChildDTO.recruitmentExamVenueItemId).remove(oldRecruitmentSeatPlanChildDTO);
					}
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId.get(oldRecruitmentSeatPlanChildDTO.recruitmentExamVenueItemId).isEmpty()) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId.remove(oldRecruitmentSeatPlanChildDTO.recruitmentExamVenueItemId);
					}

					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId.containsKey(oldRecruitmentSeatPlanChildDTO.recruitmentSeatPlanId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId.get(oldRecruitmentSeatPlanChildDTO.recruitmentSeatPlanId).remove(oldRecruitmentSeatPlanChildDTO);
					}
					if(mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId.get(oldRecruitmentSeatPlanChildDTO.recruitmentSeatPlanId).isEmpty()) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId.remove(oldRecruitmentSeatPlanChildDTO.recruitmentSeatPlanId);
					}
					
					
				}
				if(recruitmentseatplanchildDTO.isDeleted == 0) 
				{
					
					mapOfRecruitmentSeatPlanChildDTOToiD.put(recruitmentseatplanchildDTO.iD, recruitmentseatplanchildDTO);
				
					if( ! mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId.containsKey(recruitmentseatplanchildDTO.recruitmentTestNameId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId.put(recruitmentseatplanchildDTO.recruitmentTestNameId, new HashSet<>());
					}
					mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId.get(recruitmentseatplanchildDTO.recruitmentTestNameId).add(recruitmentseatplanchildDTO);
					
					if( ! mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId.containsKey(recruitmentseatplanchildDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId.put(recruitmentseatplanchildDTO.recruitmentJobDescriptionId, new HashSet<>());
					}
					mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId.get(recruitmentseatplanchildDTO.recruitmentJobDescriptionId).add(recruitmentseatplanchildDTO);
					
					if( ! mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId.containsKey(recruitmentseatplanchildDTO.recruitmentJobSpecificExamTypeId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId.put(recruitmentseatplanchildDTO.recruitmentJobSpecificExamTypeId, new HashSet<>());
					}
					mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId.get(recruitmentseatplanchildDTO.recruitmentJobSpecificExamTypeId).add(recruitmentseatplanchildDTO);
					
					if( ! mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId.containsKey(recruitmentseatplanchildDTO.recruitmentExamVenueId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId.put(recruitmentseatplanchildDTO.recruitmentExamVenueId, new HashSet<>());
					}
					mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId.get(recruitmentseatplanchildDTO.recruitmentExamVenueId).add(recruitmentseatplanchildDTO);
					
					if( ! mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId.containsKey(recruitmentseatplanchildDTO.recruitmentExamVenueItemId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId.put(recruitmentseatplanchildDTO.recruitmentExamVenueItemId, new HashSet<>());
					}
					mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId.get(recruitmentseatplanchildDTO.recruitmentExamVenueItemId).add(recruitmentseatplanchildDTO);

					if( ! mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId.containsKey(recruitmentseatplanchildDTO.recruitmentSeatPlanId)) {
						mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId.put(recruitmentseatplanchildDTO.recruitmentSeatPlanId, new HashSet<>());
					}
					mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId.get(recruitmentseatplanchildDTO.recruitmentSeatPlanId).add(recruitmentseatplanchildDTO);
					
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}
	
	public RecruitmentSeatPlanChildDTO clone(RecruitmentSeatPlanChildDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, RecruitmentSeatPlanChildDTO.class);
	}
	
	
	public List<RecruitmentSeatPlanChildDTO> getRecruitmentSeatPlanChildList() {
		List <RecruitmentSeatPlanChildDTO> recruitmentseatplanchilds = new ArrayList<RecruitmentSeatPlanChildDTO>(this.mapOfRecruitmentSeatPlanChildDTOToiD.values());
		return recruitmentseatplanchilds;
	}
	
	public List<RecruitmentSeatPlanChildDTO> copyRecruitmentSeatPlanChildList() {
		List <RecruitmentSeatPlanChildDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanChildList();
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public RecruitmentSeatPlanChildDTO getRecruitmentSeatPlanChildDTOByiD( long iD){
		return mapOfRecruitmentSeatPlanChildDTOToiD.get(iD);
	}
	
	public RecruitmentSeatPlanChildDTO copyRecruitmentSeatPlanChildDTOByiD( long iD){
		return clone(mapOfRecruitmentSeatPlanChildDTOToiD.get(iD));
	}
	
	
	public List<RecruitmentSeatPlanChildDTO> getRecruitmentSeatPlanChildDTOByrecruitmentTestNameId(long recruitmentTestNameId) {
		return new ArrayList<>( mapOfRecruitmentSeatPlanChildDTOTorecruitmentTestNameId.getOrDefault(recruitmentTestNameId,new HashSet<>()));
	}
	
	public List<RecruitmentSeatPlanChildDTO> copyRecruitmentSeatPlanChildDTOByrecruitmentTestNameId(long recruitmentTestNameId)
	{
		List <RecruitmentSeatPlanChildDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanChildDTOByrecruitmentTestNameId(recruitmentTestNameId);
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<RecruitmentSeatPlanChildDTO> getRecruitmentSeatPlanChildDTOByrecruitmentJobDescriptionId(long recruitmentJobDescriptionId) {
		return new ArrayList<>( mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobDescriptionId.getOrDefault(recruitmentJobDescriptionId,new HashSet<>()));
	}
	
	public List<RecruitmentSeatPlanChildDTO> copyRecruitmentSeatPlanChildDTOByrecruitmentJobDescriptionId(long recruitmentJobDescriptionId)
	{
		List <RecruitmentSeatPlanChildDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanChildDTOByrecruitmentJobDescriptionId(recruitmentJobDescriptionId);
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<RecruitmentSeatPlanChildDTO> getRecruitmentSeatPlanChildDTOByrecruitmentJobSpecificExamTypeId(long recruitmentJobSpecificExamTypeId) {
		return new ArrayList<>( mapOfRecruitmentSeatPlanChildDTOTorecruitmentJobSpecificExamTypeId.getOrDefault(recruitmentJobSpecificExamTypeId,new HashSet<>()));
	}
	
	public List<RecruitmentSeatPlanChildDTO> copyRecruitmentSeatPlanChildDTOByrecruitmentJobSpecificExamTypeId(long recruitmentJobSpecificExamTypeId)
	{
		List <RecruitmentSeatPlanChildDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanChildDTOByrecruitmentJobSpecificExamTypeId(recruitmentJobSpecificExamTypeId);
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<RecruitmentSeatPlanChildDTO> getRecruitmentSeatPlanChildDTOByrecruitmentExamVenueId(long recruitmentExamVenueId) {
		return new ArrayList<>( mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueId.getOrDefault(recruitmentExamVenueId,new HashSet<>()));
	}
	
	public List<RecruitmentSeatPlanChildDTO> copyRecruitmentSeatPlanChildDTOByrecruitmentExamVenueId(long recruitmentExamVenueId)
	{
		List <RecruitmentSeatPlanChildDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanChildDTOByrecruitmentExamVenueId(recruitmentExamVenueId);
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	public List<RecruitmentSeatPlanChildDTO> getRecruitmentSeatPlanChildDTOByrecruitmentExamVenueItemId(long recruitmentExamVenueItemId) {
		return new ArrayList<>( mapOfRecruitmentSeatPlanChildDTOTorecruitmentExamVenueItemId.getOrDefault(recruitmentExamVenueItemId,new HashSet<>()));
	}
	
	public List<RecruitmentSeatPlanChildDTO> copyRecruitmentSeatPlanChildDTOByrecruitmentExamVenueItemId(long recruitmentExamVenueItemId)
	{
		List <RecruitmentSeatPlanChildDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanChildDTOByrecruitmentExamVenueItemId(recruitmentExamVenueItemId);
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public List<RecruitmentSeatPlanChildDTO> getRecruitmentSeatPlanChildDTOByrecruitmentSeatPlanId(long recruitmentSeatPlanId) {
		return new ArrayList<>( mapOfRecruitmentSeatPlanChildDTOTorecruitmentSeatPlanId.getOrDefault(recruitmentSeatPlanId,new HashSet<>()));
	}

	public List<RecruitmentSeatPlanChildDTO> copyRecruitmentSeatPlanChildDTOByrecruitmentSeatPlanId(long recruitmentSeatPlanId)
	{
		List <RecruitmentSeatPlanChildDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanChildDTOByrecruitmentSeatPlanId(recruitmentSeatPlanId);
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	
	@Override
	public String getTableName() {
		return recruitmentseatplanchildDAO.getTableName();
	}
}


