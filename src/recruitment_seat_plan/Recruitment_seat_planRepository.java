package recruitment_seat_plan;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

import repository.Repository;
import repository.RepositoryManager;


public class Recruitment_seat_planRepository implements Repository {
	Recruitment_seat_planDAO recruitment_seat_planDAO = null;
	
	static Logger logger = Logger.getLogger(Recruitment_seat_planRepository.class);
	Map<Long, Recruitment_seat_planDTO>mapOfRecruitment_seat_planDTOToiD;
	Map<Long, Set<Recruitment_seat_planDTO> >mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId;
	Map<Long, Set<Recruitment_seat_planDTO> >mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId;
	Gson gson;

  
	private Recruitment_seat_planRepository(){
		recruitment_seat_planDAO = Recruitment_seat_planDAO.getInstance();
		mapOfRecruitment_seat_planDTOToiD = new ConcurrentHashMap<>();
		mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId = new ConcurrentHashMap<>();
		mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId = new ConcurrentHashMap<>();
		gson = new Gson();
		RepositoryManager.getInstance().addRepository(this);
	}

	private static class LazyLoader{
        final static Recruitment_seat_planRepository INSTANCE = new Recruitment_seat_planRepository();
    }

    public static Recruitment_seat_planRepository getInstance() {
        return LazyLoader.INSTANCE;
    }

	public void reload(boolean reloadAll){
		try {
			List<Recruitment_seat_planDTO> recruitment_seat_planDTOs = recruitment_seat_planDAO.getAllDTOs(reloadAll);
			for(Recruitment_seat_planDTO recruitment_seat_planDTO : recruitment_seat_planDTOs) {
				Recruitment_seat_planDTO oldRecruitment_seat_planDTO = getRecruitment_seat_planDTOByiD(recruitment_seat_planDTO.iD);
				if( oldRecruitment_seat_planDTO != null ) {
					mapOfRecruitment_seat_planDTOToiD.remove(oldRecruitment_seat_planDTO.iD);

					if(mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId.containsKey(oldRecruitment_seat_planDTO.recruitmentTestNameId)) {
						mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId.get(oldRecruitment_seat_planDTO.recruitmentTestNameId).remove(oldRecruitment_seat_planDTO);
					}
					if(mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId.get(oldRecruitment_seat_planDTO.recruitmentTestNameId).isEmpty()) {
						mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId.remove(oldRecruitment_seat_planDTO.recruitmentTestNameId);
					}

					if(mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId.containsKey(oldRecruitment_seat_planDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId.get(oldRecruitment_seat_planDTO.recruitmentJobDescriptionId).remove(oldRecruitment_seat_planDTO);
					}
					if(mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId.get(oldRecruitment_seat_planDTO.recruitmentJobDescriptionId).isEmpty()) {
						mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId.remove(oldRecruitment_seat_planDTO.recruitmentJobDescriptionId);
					}
				
					
				}
				if(recruitment_seat_planDTO.isDeleted == 0) 
				{
					
					mapOfRecruitment_seat_planDTOToiD.put(recruitment_seat_planDTO.iD, recruitment_seat_planDTO);

					if( ! mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId.containsKey(recruitment_seat_planDTO.recruitmentTestNameId)) {
						mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId.put(recruitment_seat_planDTO.recruitmentTestNameId, new HashSet<>());
					}
					mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId.get(recruitment_seat_planDTO.recruitmentTestNameId).add(recruitment_seat_planDTO);

					if( ! mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId.containsKey(recruitment_seat_planDTO.recruitmentJobDescriptionId)) {
						mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId.put(recruitment_seat_planDTO.recruitmentJobDescriptionId, new HashSet<>());
					}
					mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId.get(recruitment_seat_planDTO.recruitmentJobDescriptionId).add(recruitment_seat_planDTO);
				
				}
			}
			
		} catch (Exception e) {
			logger.debug("FATAL", e);
		}
	}

	public List<Recruitment_seat_planDTO> getRecruitmentSeatPlanDTOByRecruitmentTestNameId(long recruitmentTestNameId) {
		return new ArrayList<>( mapOfRecruitmentSeatPlanDTOTorecruitmentTestNameId.getOrDefault(recruitmentTestNameId,new HashSet<>()));
	}

	public List<Recruitment_seat_planDTO> copyRecruitmentSeatPlanChildDTOByrecruitmentTestNameId(long recruitmentTestNameId)
	{
		List <Recruitment_seat_planDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanDTOByRecruitmentTestNameId(recruitmentTestNameId);
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}

	public List<Recruitment_seat_planDTO> getRecruitmentSeatPlanDTOByRecruitmentJobDescriptionId(long recruitmentJobDescriptionId) {
		return new ArrayList<>( mapOfRecruitmentSeatPlanDTOToRecruitmentJobDescriptionId.getOrDefault(recruitmentJobDescriptionId,new HashSet<>()));
	}

	public List<Recruitment_seat_planDTO> copyRecruitmentSeatPlanChildDTOByRecruitmentJobDescriptionId(long recruitmentJobDescriptionId)
	{
		List <Recruitment_seat_planDTO> recruitmentseatplanchilds = getRecruitmentSeatPlanDTOByRecruitmentJobDescriptionId(recruitmentJobDescriptionId);
		return recruitmentseatplanchilds
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	public Recruitment_seat_planDTO clone(Recruitment_seat_planDTO dto) {
		String raw = gson.toJson(dto);
		return gson.fromJson(raw, Recruitment_seat_planDTO.class);
	}
	
	
	public List<Recruitment_seat_planDTO> getRecruitment_seat_planList() {
		List <Recruitment_seat_planDTO> recruitment_seat_plans = new ArrayList<Recruitment_seat_planDTO>(this.mapOfRecruitment_seat_planDTOToiD.values());
		return recruitment_seat_plans;
	}
	
	public List<Recruitment_seat_planDTO> copyRecruitment_seat_planList() {
		List <Recruitment_seat_planDTO> recruitment_seat_plans = getRecruitment_seat_planList();
		return recruitment_seat_plans
				.stream()
				.map(dto -> clone(dto))
				.collect(Collectors.toList());
	}
	
	
	
	public Recruitment_seat_planDTO getRecruitment_seat_planDTOByiD( long iD){
		return mapOfRecruitment_seat_planDTOToiD.get(iD);
	}
	
	public Recruitment_seat_planDTO copyRecruitment_seat_planDTOByiD( long iD){
		return clone(mapOfRecruitment_seat_planDTOToiD.get(iD));
	}

	
	@Override
	public String getTableName() {
		return recruitment_seat_planDAO.getTableName();
	}
}


