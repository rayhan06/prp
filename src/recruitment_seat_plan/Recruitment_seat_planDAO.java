package recruitment_seat_plan;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.SQLException;
import common.CommonDAOService;
import org.apache.log4j.Logger;
import util.*;
import pb.*;

public class Recruitment_seat_planDAO  implements CommonDAOService<Recruitment_seat_planDTO>
{
	
	Logger logger = Logger.getLogger(getClass());
	
	private static String addQuery = "";
	private static String updateQuery = "";
	private String[] columnNames = null;

	private Recruitment_seat_planDAO()
	{
		columnNames = new String[] 
		{
			"ID",
			"recruitment_test_name_id",
			"recruitment_job_description_id",
			"recruitment_job_specific_exam_type_id",
			"recruitment_exam_venue_id",
			"exam_date",
			"exam_time",
			"search_column",
			"inserted_by",
			"modified_by",
			"insertion_date",
			"isDeleted",
			"lastModificationTime"
		};
		updateQuery = getUpdateQuery(columnNames);
        addQuery = getAddQuery(columnNames);
		
		searchMap.put("exam_date_start"," and (exam_date >= ?)");
		searchMap.put("exam_date_end"," and (exam_date <= ?)");
		searchMap.put("exam_time"," and (exam_time like ?)");
		searchMap.put("insertion_date_start"," and (insertion_date >= ?)");
		searchMap.put("insertion_date_end"," and (insertion_date <= ?)");
		searchMap.put("AnyField"," and (search_column like ?)");
	}

	private final Map<String,String> searchMap = new HashMap<>();

    private static class LazyLoader
    {
    	static final Recruitment_seat_planDAO INSTANCE = new Recruitment_seat_planDAO();
	}

	public static Recruitment_seat_planDAO getInstance()
	{
    	return LazyLoader.INSTANCE;
	}
	
	public void setSearchColumn(Recruitment_seat_planDTO recruitment_seat_planDTO)
	{
		recruitment_seat_planDTO.searchColumn = "";
		recruitment_seat_planDTO.searchColumn += recruitment_seat_planDTO.examTime + " ";
	}
	
	@Override
	public void set(PreparedStatement ps, Recruitment_seat_planDTO recruitment_seat_planDTO, boolean isInsert) throws SQLException
	{
		int index = 0;
		long lastModificationTime = System.currentTimeMillis();	
		setSearchColumn(recruitment_seat_planDTO);
		if(isInsert)
		{
			ps.setObject(++index,recruitment_seat_planDTO.iD);
		}
		ps.setObject(++index,recruitment_seat_planDTO.recruitmentTestNameId);
		ps.setObject(++index,recruitment_seat_planDTO.recruitmentJobDescriptionId);
		ps.setObject(++index,recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId);
		ps.setObject(++index,recruitment_seat_planDTO.recruitmentExamVenueId);
		ps.setObject(++index,recruitment_seat_planDTO.examDate);
		ps.setObject(++index,recruitment_seat_planDTO.examTime);
		ps.setObject(++index,recruitment_seat_planDTO.searchColumn);
		ps.setObject(++index,recruitment_seat_planDTO.insertedBy);
		ps.setObject(++index,recruitment_seat_planDTO.modifiedBy);
		ps.setObject(++index,recruitment_seat_planDTO.insertionDate);
		if(isInsert)
		{
			ps.setObject(++index,recruitment_seat_planDTO.isDeleted);
		}
		ps.setObject(++index, lastModificationTime);
		if(!isInsert)
		{
			ps.setObject(++index,recruitment_seat_planDTO.iD);
		}
	}
	
	@Override
	public Recruitment_seat_planDTO buildObjectFromResultSet(ResultSet rs)
	{
		try
		{
			Recruitment_seat_planDTO recruitment_seat_planDTO = new Recruitment_seat_planDTO();
			int i = 0;
			recruitment_seat_planDTO.iD = rs.getLong(columnNames[i++]);
			recruitment_seat_planDTO.recruitmentTestNameId = rs.getLong(columnNames[i++]);
			recruitment_seat_planDTO.recruitmentJobDescriptionId = rs.getLong(columnNames[i++]);
			recruitment_seat_planDTO.recruitmentJobSpecificExamTypeId = rs.getLong(columnNames[i++]);
			recruitment_seat_planDTO.recruitmentExamVenueId = rs.getLong(columnNames[i++]);
			recruitment_seat_planDTO.examDate = rs.getLong(columnNames[i++]);
			recruitment_seat_planDTO.examTime = rs.getString(columnNames[i++]);
			recruitment_seat_planDTO.searchColumn = rs.getString(columnNames[i++]);
			recruitment_seat_planDTO.insertedBy = rs.getString(columnNames[i++]);
			recruitment_seat_planDTO.modifiedBy = rs.getString(columnNames[i++]);
			recruitment_seat_planDTO.insertionDate = rs.getLong(columnNames[i++]);
			recruitment_seat_planDTO.isDeleted = rs.getInt(columnNames[i++]);
			recruitment_seat_planDTO.lastModificationTime = rs.getLong(columnNames[i++]);
			return recruitment_seat_planDTO;
		}
		catch (SQLException ex)
		{
			logger.error(ex);
			return null;
		}
	}
		
	public Recruitment_seat_planDTO getDTOByID (long id)
	{
		Recruitment_seat_planDTO recruitment_seat_planDTO = null;
		try 
		{
			recruitment_seat_planDTO = getDTOFromID(id);
			if(recruitment_seat_planDTO != null)
			{
				RecruitmentSeatPlanChildDAO recruitmentSeatPlanChildDAO = RecruitmentSeatPlanChildDAO.getInstance();				
				List<RecruitmentSeatPlanChildDTO> recruitmentSeatPlanChildDTOList = (List<RecruitmentSeatPlanChildDTO>)recruitmentSeatPlanChildDAO.getDTOsByParent("recruitment_seat_plan_id", recruitment_seat_planDTO.iD);
				recruitment_seat_planDTO.recruitmentSeatPlanChildDTOList = recruitmentSeatPlanChildDTOList;
			}
		}
		catch (Exception e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return recruitment_seat_planDTO;
	}

	@Override
	public String getTableName() {
		return "recruitment_seat_plan";
	}

	@Override
	public Map<String, String> getSearchMap() {
		return searchMap;
	}

	public long add(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Recruitment_seat_planDTO) commonDTO,addQuery,true);
	}

	public long update(CommonDTO commonDTO) throws Exception {
		return executeAddOrUpdateQuery((Recruitment_seat_planDTO) commonDTO,updateQuery,false);
	}
	
	@Override
	public String getLastModifierUser(){
		return "modified_by";
    }
				
}
	