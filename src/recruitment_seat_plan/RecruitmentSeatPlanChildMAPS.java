package recruitment_seat_plan;
import java.util.*; 
import util.*;


public class RecruitmentSeatPlanChildMAPS extends CommonMaps
{	
	public RecruitmentSeatPlanChildMAPS(String tableName)
	{
		


		java_SQL_map.put("recruitment_test_name_id".toLowerCase(), "recruitmentTestNameId".toLowerCase());
		java_SQL_map.put("recruitment_job_description_id".toLowerCase(), "recruitmentJobDescriptionId".toLowerCase());
		java_SQL_map.put("recruitment_job_specific_exam_type_id".toLowerCase(), "recruitmentJobSpecificExamTypeId".toLowerCase());
		java_SQL_map.put("exam_date".toLowerCase(), "examDate".toLowerCase());
		java_SQL_map.put("exam_time".toLowerCase(), "examTime".toLowerCase());
		java_SQL_map.put("recruitment_exam_venue_id".toLowerCase(), "recruitmentExamVenueId".toLowerCase());
		java_SQL_map.put("recruitment_exam_venue_item_id".toLowerCase(), "recruitmentExamVenueItemId".toLowerCase());
		java_SQL_map.put("building".toLowerCase(), "building".toLowerCase());
		java_SQL_map.put("floor".toLowerCase(), "floor".toLowerCase());
		java_SQL_map.put("room_no".toLowerCase(), "roomNo".toLowerCase());
		java_SQL_map.put("capacity".toLowerCase(), "capacity".toLowerCase());
		java_SQL_map.put("start_roll".toLowerCase(), "startRoll".toLowerCase());
		java_SQL_map.put("end_roll".toLowerCase(), "endRoll".toLowerCase());
		java_SQL_map.put("total".toLowerCase(), "total".toLowerCase());
		java_SQL_map.put("isRangeAutoCount".toLowerCase(), "isRangeAutoCount".toLowerCase());

		java_Text_map.put("ID".toLowerCase(), "iD".toLowerCase());
		java_Text_map.put("Recruitment Test Name Id".toLowerCase(), "recruitmentTestNameId".toLowerCase());
		java_Text_map.put("Recruitment Job Description Id".toLowerCase(), "recruitmentJobDescriptionId".toLowerCase());
		java_Text_map.put("Recruitment Job Specific Exam Type Id".toLowerCase(), "recruitmentJobSpecificExamTypeId".toLowerCase());
		java_Text_map.put("Exam Date".toLowerCase(), "examDate".toLowerCase());
		java_Text_map.put("Exam Time".toLowerCase(), "examTime".toLowerCase());
		java_Text_map.put("Recruitment Exam Venue Id".toLowerCase(), "recruitmentExamVenueId".toLowerCase());
		java_Text_map.put("Recruitment Exam Venue Item Id".toLowerCase(), "recruitmentExamVenueItemId".toLowerCase());
		java_Text_map.put("Building".toLowerCase(), "building".toLowerCase());
		java_Text_map.put("Floor".toLowerCase(), "floor".toLowerCase());
		java_Text_map.put("Room No".toLowerCase(), "roomNo".toLowerCase());
		java_Text_map.put("Capacity".toLowerCase(), "capacity".toLowerCase());
		java_Text_map.put("Start Roll".toLowerCase(), "startRoll".toLowerCase());
		java_Text_map.put("End Roll".toLowerCase(), "endRoll".toLowerCase());
		java_Text_map.put("Total".toLowerCase(), "total".toLowerCase());
		java_Text_map.put("IsRangeAutoCount".toLowerCase(), "isRangeAutoCount".toLowerCase());
		java_Text_map.put("Range".toLowerCase(), "searchColumn".toLowerCase());
		java_Text_map.put("Inserted By".toLowerCase(), "insertedBy".toLowerCase());
		java_Text_map.put("Modified By".toLowerCase(), "modifiedBy".toLowerCase());
		java_Text_map.put("Insertion Date".toLowerCase(), "insertionDate".toLowerCase());
		java_Text_map.put("IsDeleted".toLowerCase(), "isDeleted".toLowerCase());
		java_Text_map.put("LastModificationTime".toLowerCase(), "lastModificationTime".toLowerCase());
			
	}

}