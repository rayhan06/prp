<%@page pageEncoding="UTF-8" %>
<script src="${pageContext.request.contextPath}/assets/js/metronic/pages/components/extended/sweetalert2.js"
        type="text/javascript"></script>

<script>
    let utilIsBangla = false;
    if (document.getElementById('header_language_id') != null) {
        let headerLang = $('#header_language_id').val();
        if (headerLang && headerLang.toLowerCase() === 'bangla') {
            utilIsBangla = true;
        }
    }

    function deleteDialog(message, successFunction, cancelFunction) {
        if (!message) {
            message = utilIsBangla ? "আপনি কি রেকর্ড (গুলি) মুছে ফেলার ব্যাপারে নিশ্চিত?"
                : "Are you sure you want to delete the record(s)?";
        }
        let deleteMsg = utilIsBangla ? 'আপনি কি মুছে ফেলতে চান?' : 'Do you want to delete?';
        let deleteButton = utilIsBangla ? 'মুছুন' : 'Delete';
        let cancelButton = utilIsBangla ? 'বাতিল' : 'Cancel';
        messageDialog(deleteMsg, message, 'error', true, deleteButton, cancelButton, successFunction, cancelFunction);
    }

    function deleteWarningDialog(successFunction, cancelFunction) {
        let msg = utilIsBangla ? 'মুছে ফেলার জন্য রেকর্ড নির্বাচন করুন!' : 'Select rows to delete!';
        let okButton = utilIsBangla ? 'ঠিক আছে' : 'OK';
        messageDialog('', msg, 'warning', false, okButton, 'Cancel', successFunction, cancelFunction);
    }

    function submitDialog(message, successFunction, cancelFunction) {
        if (!message) {
            message = "You won't be able to revert this!";
        }
        messageDialog('Do you want to submit?', message, 'success', true, 'Submit', 'Cancel', successFunction, cancelFunction);
    }

    function animationMessageDialogWithTitle(title, message, successFunction, cancelFunction) {
        animationMessageDialog(title, message, successFunction, cancelFunction);
    }

    function animationMessageDialogWithoutTitle(message, successFunction, cancelFunction) {
        animationMessageDialog('', message, successFunction, cancelFunction);
    }

    function animationMessageDialog(title, message, successFunction, cancelFunction) {
        swal.fire({
            title: title,
            html: $('<div>')
                .addClass('some-class')
                .text(message),
            animation: false,
            customClass: 'animated tada'
        }).then(function (result) {
            if (result.value) {
                successFunction();
            } else {
                if (cancelFunction) {
                    cancelFunction();
                }
            }
        });
    }

    function warningMessageDialogWithTitle(title, message, successFunction, cancelFunction) {
        warningMessageDialog(title, message, successFunction, cancelFunction);
    }

    function warningMessageDialogWithoutTitle(message, successFunction, cancelFunction) {
        warningMessageDialog('', message, successFunction, cancelFunction);
    }

    function warningMessageDialog(title, message, successFunction, cancelFunction) {
        messageDialog(title, message, 'warning', false, 'OK', 'Cancel', successFunction, cancelFunction);
    }

    function messageDialog(title, message, type, showCancelButton, confirmButtonText, cancelButtonText, successFunction, cancelFunction) {
        swal.fire({
            title: title,
            text: message,
            type: type,
            showCancelButton: showCancelButton,
            confirmButtonText: confirmButtonText,
            cancelButtonText: cancelButtonText
        }).then(function (result) {
            if (result.value) {
                if (successFunction && typeof (successFunction) == 'function') {
                    successFunction();
                }
            } else {
                if (cancelFunction && typeof (cancelFunction) == 'function') {
                    cancelFunction();
                }
            }
        });
    }

    function select2SingleSelector(selector, language) {
        select2Selector(selector, language, false, true);
    }

    function select2MultiSelector(selector, language) {
        select2Selector(selector, language, true, true);
    }

    function select2SingleSelectorWithoutClearOption(selector, language) {
        select2Selector(selector, language, false, false);
    }

    function select2Selector(selector, language, multiple, clearOption) {
        language = language.toLowerCase();
        let placeHolder;
        if (language === 'english') {
            placeHolder = '<%=LM.getText(LC.GLOBAL_TYPE_TO_SELECT,"English")%>'
        } else {
            placeHolder = '<%=LM.getText(LC.GLOBAL_TYPE_TO_SELECT,"Bangla")%>'
        }
        $(selector).select2({
            multiple: multiple,
            allowClear: clearOption,
            maximumSelectionSize: 1,
            placeholder: placeHolder,
            width: '100%'
        }).on('change', function () {
            let errorSelector = selector + '-error';
            if ($(errorSelector)) {
                if ($(this).val()) {
                    $(errorSelector).hide();
                } else {
                    $(errorSelector).show();
                }
            }
        });
    }

    function dialogMessageWithTextBox(message, placeholder, confirmButtonText, cancelButtonText, displayMsgIfInputIsEmpty, successFunction, cancelFunction) {
        Swal.fire({
            confirmButtonText: confirmButtonText,
            cancelButtonText: cancelButtonText,
            showCancelButton: true,
            input: 'textarea',
            title: message,
            inputPlaceholder: placeholder,
            html: $('<div>')
                .addClass('some-class'),
            animation: false,
            customClass: 'animated tada'
        }).then((result) => {
            console.log(result);
            if (result.dismiss) {
                if (cancelFunction && typeof (cancelFunction) == 'function') {
                    cancelFunction();
                }
            } else {
                if (result.value) {
                    if (result.value.length === 0) {
                        if (displayMsgIfInputIsEmpty) {
                            swal.fire(displayMsgIfInputIsEmpty);
                        }
                    } else if (successFunction && typeof (successFunction) == 'function') {
                        console.log('calling success function val : ' + result.value);
                        successFunction(result.value);
                    }
                } else {
                    if (displayMsgIfInputIsEmpty) {
                        swal.fire(displayMsgIfInputIsEmpty);
                    }
                }
            }
        });
    }

    function dialogMessageWithTextBoxWithoutAnimation(message, placeholder, confirmButtonText, cancelButtonText, displayMsgIfInputIsEmpty, successFunction, cancelFunction) {
        Swal.fire({
            confirmButtonText: confirmButtonText,
            cancelButtonText: cancelButtonText,
            showCancelButton: true,
            input: 'textarea',
            title: message,
            inputPlaceholder: placeholder,
            html: $('<div>')
                .addClass('some-class'),
            animation: false,
            customClass: 'animated fadeInDown faster'
        }).then((result) => {
            console.log(result);
            if (result.dismiss) {
                if (cancelFunction) {
                    cancelFunction();
                }
            } else {
                if (result.value) {
                    if (result.value.length === 0) {
                        if (displayMsgIfInputIsEmpty) {
                            swal.fire(displayMsgIfInputIsEmpty);
                        }
                    } else if (successFunction) {
                        console.log('calling success function val : ' + result.value);
                        successFunction(result.value);
                    }
                } else {
                    if (displayMsgIfInputIsEmpty) {
                        swal.fire(displayMsgIfInputIsEmpty);
                    }
                }
            }
        });
    }

    function getValueByLanguage(language, bnValue, enValue) {
        if (language === 'english' || language === 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }

    function getDigitByLanguage(language, number){
        if(!number || language === 'english' || language === 'English'){
            return number;
        }
        let numberBn = '';
        for(let digit of number.toString()){
            if(digit === '0') numberBn += '০';
            else if(digit === '1') numberBn += '১';
            else if(digit === '2') numberBn += '২';
            else if(digit === '3') numberBn += '৩';
            else if(digit === '4') numberBn += '৪';
            else if(digit === '5') numberBn += '৫';
            else if(digit === '6') numberBn += '৬';
            else if(digit === '7') numberBn += '৭';
            else if(digit === '8') numberBn += '৮';
            else if(digit === '9') numberBn += '৯';
            else numberBn += digit;
        }
        return numberBn;
    }

    const searchNav = {
        dosubmit: function (url, params, pushState = true) {
            document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    if (pushState) {
                        history.pushState(params, '', url + '&' + params);
                    }
                    setTimeout(() => {
                        document.getElementById('tableForm').innerHTML = this.responseText;
                        setPageNo();
                        searchChanged = 0;
                    }, 500);
                } else if (this.readyState === 4 && this.status !== 200) {
                    alert('failed ' + this.status);
                }
            };
            url += "&isPermanentTable=true&ajax=true"
            if (params) {
                url += "&" + params;
            }
            xhttp.open("GET", url, false);
            xhttp.send();
        }
    }
</script>