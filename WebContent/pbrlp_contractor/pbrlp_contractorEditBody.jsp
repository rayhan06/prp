
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="pbrlp_contractor.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>


<%
Pbrlp_contractorDTO pbrlp_contractorDTO;
pbrlp_contractorDTO = (Pbrlp_contractorDTO)request.getAttribute("pbrlp_contractorDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(pbrlp_contractorDTO == null)
{
	pbrlp_contractorDTO = new Pbrlp_contractorDTO();
	
}
System.out.println("pbrlp_contractorDTO = " + pbrlp_contractorDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.PBRLP_CONTRACTOR_EDIT_PBRLP_CONTRACTOR_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.PBRLP_CONTRACTOR_ADD_PBRLP_CONTRACTOR_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Pbrlp_contractorServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.PBRLP_CONTRACTOR_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pbrlp_contractorDTO.iD%>' tag='pb_html'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PBRLP_CONTRACTOR_EDIT_NAMEEN, loginDTO)):(LM.getText(LC.PBRLP_CONTRACTOR_ADD_NAMEEN, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'nameEn_div_<%=i%>'>	
		<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + pbrlp_contractorDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PBRLP_CONTRACTOR_EDIT_NAMEBN, loginDTO)):(LM.getText(LC.PBRLP_CONTRACTOR_ADD_NAMEBN, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'nameBn_div_<%=i%>'>	
		<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + pbrlp_contractorDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PBRLP_CONTRACTOR_EDIT_ADDRESS, loginDTO)):(LM.getText(LC.PBRLP_CONTRACTOR_ADD_ADDRESS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'address_div_<%=i%>'>	
		<div id ='address_geoDIV_<%=i%>' tag='pb_html'>
			<select class='form-control' name='address_active' id = 'address_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'address', this.getAttribute('row'))"  tag='pb_html' row = '<%=i%>'></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='address_text' id = 'address_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(pbrlp_contractorDTO.address)  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc' tag='pb_html'>
		<input type='hidden' class='form-control'  name='address' id = 'address_geolocation_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(pbrlp_contractorDTO.address)  + "'"):("'" + "1" + "'")%> tag='pb_html'>
		<%
		if(actionName.equals("edit"))
		{
		%>
		<label class="control-label"><%=GeoLocationDAO2.parseEnText(pbrlp_contractorDTO.address) + "," + GeoLocationDAO2.parseDetails(pbrlp_contractorDTO.address)%></label>
		<%
		}
		%>
			
						
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PBRLP_CONTRACTOR_EDIT_DESCRIPTION, loginDTO)):(LM.getText(LC.PBRLP_CONTRACTOR_ADD_DESCRIPTION, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'description_div_<%=i%>'>	
		<input type='text' class='form-control'  name='description' id = 'description_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + pbrlp_contractorDTO.description + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PBRLP_CONTRACTOR_EDIT_CONTRACTNO, loginDTO)):(LM.getText(LC.PBRLP_CONTRACTOR_ADD_CONTRACTNO, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'contractNo_div_<%=i%>'>	
		<input type='text' class='form-control'  name='contractNo' id = 'contractNo_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + pbrlp_contractorDTO.contractNo + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + pbrlp_contractorDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + pbrlp_contractorDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.PBRLP_CONTRACTOR_EDIT_PBRLP_CONTRACTOR_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.PBRLP_CONTRACTOR_ADD_PBRLP_CONTRACTOR_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.PBRLP_CONTRACTOR_EDIT_PBRLP_CONTRACTOR_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.PBRLP_CONTRACTOR_ADD_PBRLP_CONTRACTOR_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	return preprocessGeolocationBeforeSubmitting('address', row, false);

	// return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Pbrlp_contractorServlet");	
}

function init(row)
{

			initGeoLocation('address_geoSelectField_', row, "Pbrlp_contractorServlet");

	
}

var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






