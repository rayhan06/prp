<%@page pageEncoding="UTF-8" %>

<%@page import="pbrlp_contractor.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.PBRLP_CONTRACTOR_EDIT_LANGUAGE, loginDTO);

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_PBRLP_CONTRACTOR;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Pbrlp_contractorDTO pbrlp_contractorDTO = (Pbrlp_contractorDTO)request.getAttribute("pbrlp_contractorDTO");

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;
boolean canApprove = false, canValidate = false;
boolean isInPreviousOffice = false;
String Message = "Done";

if(!isPermanentTable)
{
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("pbrlp_contractor", pbrlp_contractorDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
}	

System.out.println("pbrlp_contractorDTO = " + pbrlp_contractorDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";



String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

Pbrlp_contractorDAO pbrlp_contractorDAO = (Pbrlp_contractorDAO)request.getAttribute("pbrlp_contractorDAO");


%>

											
		
											
											<td id = '<%=i%>_nameEn'>
											<%
											value = pbrlp_contractorDTO.nameEn + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_nameBn'>
											<%
											value = pbrlp_contractorDTO.nameBn + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_address'>
											<%
											value = pbrlp_contractorDTO.address + "";
											%>
											<%=GeoLocationDAO2.parseEnText(value)%>
											<%
											{
												String addressdetails = GeoLocationDAO2.parseDetails(value);
												if(!addressdetails.equals(""))
												{
												%>
													, <%=addressdetails%>
												<%
												}
											}
											%>

				
			
											</td>
		
											
											<td id = '<%=i%>_description'>
											<%
											value = pbrlp_contractorDTO.description + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_contractNo'>
											<%
											value = pbrlp_contractorDTO.contractNo + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
		
											
		
	

	
											<td id = '<%=i%>_Edit'>
											<%
											if(isPermanentTable || canValidate)
											{
											%>																						
												<a href="#"  onclick='fixedToEditable(<%=i%>,"", "<%=pbrlp_contractorDTO.iD%>", <%=isPermanentTable%>, "Pbrlp_contractorServlet")'><%=LM.getText(LC.PBRLP_CONTRACTOR_SEARCH_PBRLP_CONTRACTOR_EDIT_BUTTON, loginDTO)%></a>
									
											<%
											}
											else
											{
											%>
												You cannot Validate this.
											<%
											}
											%>
											</td>											
											<%
											if(!isPermanentTable)
											{
												%>
												<td><%=approval_execution_tableDTO !=null ? SessionConstants.operation_names[approval_execution_tableDTO.operation]: "Invalid Operation"%></td>
												<%
											}
											 %>
											 
											
											<%
											if(!isPermanentTable)
											{
											%>
											<td>
											<%
												if(approval_execution_tableDTO.operation == SessionConstants.UPDATE)
												{
													%>													
													<a href="#" id = '<%=i%>_getOriginal' onclick='getOriginal(<%=i%>, <%=pbrlp_contractorDTO.iD%>, <%=approval_execution_tableDTO.previousRowId%>, "Pbrlp_contractorServlet")'>View Original</a>
													<input type='hidden' id='<%=i%>_original_status' value='0'/>
													<%
												}
												%>
											</td>
											<%
											}
											%>
											<%
											if(!isPermanentTable)
											{
												
												
												%>
												<td id = 'tdapprove_<%=pbrlp_contractorDTO.iD%>'>
												<%
												if(canApprove)
												{
													%>													
													<a href="#"  id = 'approve_<%=pbrlp_contractorDTO.iD%>' onclick='approve("<%=approval_execution_tableDTO.updatedRowId%>", "<%=Message%>", <%=i%>, "Pbrlp_contractorServlet", true)'><%=LM.getText(LC.HM_APPROVE, loginDTO)%></a>
												<%
												}
												else 
												{
												%>
													Not in your Office.
												<%
												}
												%>
												</td>
												
											<%
											}
											%>											
											
											
											<td id='<%=i%>_checkbox'>
											<%
											if(isPermanentTable)
											{
											%>
												
												<div class='checker'>
												<span id='chkEdit' ><input type='checkbox' name='ID' value='<%=pbrlp_contractorDTO.iD%>'/></span>
												</div>
											<%
											}
											else
											{
											%>
												<a href="#"  id = 'reject_<%=pbrlp_contractorDTO.iD%>' onclick='approve("<%=pbrlp_contractorDTO.iD%>", "<%=Message%>", <%=i%>, "Pbrlp_contractorServlet", false)'><%=LM.getText(LC.HM_REJECT, loginDTO)%></a>
											<%
											}
											%>
											</td>



