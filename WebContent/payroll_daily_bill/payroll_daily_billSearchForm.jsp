<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="payroll_daily_bill.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoRepository" %>
<%@ page import="allowance_employee_info.AllowanceEmployeeInfoDTO" %>


<%
    String navigator2 = "navPAYROLL_DAILY_BILL";
    String servletName = "Payroll_daily_billServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th>
                <%=getDataByLanguage(
                        Language,
                        "মোবাইল নাম্বার",
                        "Mobile Number"
                )%>
            </th>
            <th>
                <%=getDataByLanguage(
                        Language,
                        "সঞ্চয়ী হিসাব নাম্বার",
                        "Savings Account Number"
                )%>
            </th>
            <th>
                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAILYRATE, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAY, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_REVENUESTAMPDEDUCTION, loginDTO)%>
            </th>
            <th>
                <%=getDataByLanguage(
                        Language,
                        "নীট দাবী",
                        "Net Amount"
                )%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Payroll_daily_billDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    for (int i = 0; i < size; i++) {
                        Payroll_daily_billDTO payroll_daily_billDTO = (Payroll_daily_billDTO) data.get(i);


        %>
        <tr>
            <td>
                <%=DateUtils.getMonthYear(payroll_daily_billDTO.monthYear, Language)%>
            </td>
            <td>
                <%
                    AllowanceEmployeeInfoDTO employeeInfoDTO =
                            AllowanceEmployeeInfoRepository.getInstance()
                                    .getByEmployeeRecordId(payroll_daily_billDTO.employeeRecordsId);

                %>
                <%=getDataByLanguage(Language, employeeInfoDTO.nameBn, employeeInfoDTO.nameEn)%>
                <br>
                <b>
                    <%=getDataByLanguage(Language, employeeInfoDTO.organogramNameBn, employeeInfoDTO.organogramNameEn)%>
                </b>
                <br>
                <%=getDataByLanguage(Language, employeeInfoDTO.officeNameBn, employeeInfoDTO.officeNameEn)%>
            </td>

            <td>
                <%=Utils.getDigits(employeeInfoDTO.mobileNumber, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(employeeInfoDTO.savingAccountNumber, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(payroll_daily_billDTO.dailyRate, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(payroll_daily_billDTO.day, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(payroll_daily_billDTO.revenueStampDeduction, Language)%>
            </td>

            <td>
                <%
                    int netAmount = payroll_daily_billDTO.dailyRate * payroll_daily_billDTO.day
                            - payroll_daily_billDTO.revenueStampDeduction;
                %>
                <%=Utils.getDigits(netAmount, Language)%>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			