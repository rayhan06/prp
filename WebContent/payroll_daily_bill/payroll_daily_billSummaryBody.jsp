<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.*" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="payroll_daily_bill.Payroll_daily_billDTO" %>
<%@ page import="payroll_daily_bill.Payroll_daily_billModel" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pb.Utils" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    List<Payroll_daily_billDTO> billDTOs = (List<Payroll_daily_billDTO>) request.getAttribute("billDTOs");
    List<Payroll_daily_billModel> billModels = billDTOs.stream()
                                                       .map(dto -> new Payroll_daily_billModel(dto, "BANGLA"))
                                                       .collect(Collectors.toList());

    long monthYear = (long) request.getAttribute("monthYear");
    long budgetOfficeId = (long) request.getAttribute("budgetOfficeId");

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);

    String pdfFileName = "Daily Basis Employees Payroll Summary "
            + DateUtils.getMonthYear(monthYear, "English", " ") + " "
            + Budget_officeRepository.getInstance().getText(budgetOfficeId, "English");

    String budgetOfficeNameBn = Budget_officeRepository.getInstance().getText(budgetOfficeId, "Bangla");
    String employeeCount = convertToBanNumber(String.valueOf(billDTOs.size()));
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 4px;
    }

    .align-top {
        vertical-align: top;
    }

    thead th,
    thead td {
        text-align: center;
    }
</style>
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg" id="bill-div">
                <div class="kt-subheader__main ml-4">
                    <label class="h2 kt-subheader__title" style="color: #00a1d4;">
                        <%=UtilCharacter.getDataByLanguage(
                                Language,
                                "দৈনিক ভিত্তিক সাং-বাৎসরিক কর্মচারীদের বেতন ভাতা ব্যাংক স্টেটমেন্ট",
                                "Daily Basis Employee Wages Bank Statement"
                        )%>
                    </label>
                </div>
                <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <%
                            long totalAmountRunningTotal = 0;
                            long deductionRunningTotal = 0;
                            long netAmountRunningTotal = 0;
                            boolean isLastPage = false;
                            final int rowsPerPage = 20;
                            int index = 0;
                            while (index < billModels.size()) {
                                boolean isFirstPage = (index == 0);
                        %>
                        <section class="page shadow" data-size="A4">
                            <%if (isFirstPage) {%>
                            <div class="text-center">
                                <h1 style="font-weight: normal">বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">অর্থ শাখা-১</h2>
                                <h2 style="font-weight: normal">
                                    বাংলাদেশ জাতীয় সংসদের
                                    <%=budgetOfficeNameBn%>
                                    কার্যালয়ে
                                    কর্মরত <%=employeeCount%> (<%=BangladeshiNumberInWord.convertToWord(employeeCount)%>
                                    )
                                    জন সাং-বাৎসরিক কর্মচারীর <%=DateUtils.getMonthYear(monthYear, Language, "/")%> মাসের
                                    মজুরী বাবদ
                                    বিলের বিবরণ নিম্নরূপ:
                                </h2>
                            </div>
                            <%}%>

                            <div>
                                <div class="mt-4">
                                    <table class="table-bordered mt-2 w-100">
                                        <thead>
                                        <tr style="height: 50px;background-color:lightgrey">
                                            <th style="width:5%;">ক্র. নং</th>
                                            <th style="width:25%;">নাম</th>
                                            <th style="width:15%;">পদবী</th>
                                            <th style="width:10%;">মোবাইল নম্বর</th>
                                            <th style="width:10%;">সঞ্চয়ী হিসাব নম্বর</th>
                                            <th style="width:10%;">প্রাপ্য মজুরী</th>
                                            <th style="width:10%;">রাজস্ব স্ট্যাম্প বাবদ কর্তন</th>
                                            <th style="width:15%;">নীট প্রাপ্য</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <%if (!isFirstPage) {%>
                                        <tr>
                                            <td colspan="3" class="text-right">
                                                পূর্ব পৃষ্ঠার জের=
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(deductionRunningTotal))
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(netAmountRunningTotal))
                                                )%>
                                            </td>
                                        </tr>
                                        <%}%>

                                        <%
                                            int rowsInThisPage = 0;
                                            while (index < billModels.size() && rowsInThisPage < rowsPerPage) {
                                                isLastPage = (index == (billModels.size() - 1));
                                                rowsInThisPage++;
                                                Payroll_daily_billModel model = billModels.get(index++);
                                                totalAmountRunningTotal += Long.parseLong(model.totalAmount);
                                                deductionRunningTotal += Long.parseLong(model.revenueStampDeduction);
                                                netAmountRunningTotal += Long.parseLong(model.netAmount);
                                        %>
                                        <tr style="height: 40px">
                                            <td class="text-center"><%=Utils.getDigits(index, "BANGLA")%>
                                            </td>
                                            <td class="align-top">
                                                <%=model.name%><br>
                                                পিতা-<%=Employee_recordsRepository.getInstance().getById(Long.parseLong(model.employeeRecordsId)).fatherNameBng%>
                                            </td>
                                            <td class="align-top text-center">
                                                <%=model.organogramName%>
                                            </td>
                                            <td style="text-align: center">
                                                <%=StringUtils.convertToBanNumber(model.mobileNumber)%>
                                            </td>
                                            <td style="text-align: center">
                                                <%=StringUtils.convertToBanNumber(model.savingAccountNumber)%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.totalAmount, "BANGLA"))%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.revenueStampDeduction, "BANGLA"))%>
                                            </td class="text-right">
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.netAmount, "BANGLA"))%>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                <%=isLastPage ? "সর্বমোট" : "উপমোট"%>=
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(deductionRunningTotal))
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(netAmountRunningTotal))
                                                )%>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <%if (isLastPage) {%>
                                    <div class="mt-2 offset-4 ml-auto text-center">
                                        কথায়:
                                        <strong><%=BangladeshiNumberInWord.convertToWord(Utils.getDigits(totalAmountRunningTotal, "Bangla"))%>
                                        </strong> টাকা মাত্র
                                    </div>
                                    <%}%>
                                </div>
                            </div>
                        </section>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.1,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>