<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="util.*" %>
<%@ page import="budget_mapping.Budget_mappingDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="payroll_daily_bill.Payroll_daily_billDTO" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="payroll_daily_bill.Payroll_daily_billServlet" %>
<%@ page import="java.util.stream.Stream" %>
<%@ page import="static java.util.stream.Collectors.joining" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget.BudgetInfo" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="payroll_daily_bill.Payroll_daily_billModel" %>
<%@ page import="pb.Utils" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="finance.FinanceUtil" %>
<%@ page import="budget_register.Budget_registerDTO" %>
<%@ page import="budget_register.Budget_registerDAO" %>
<%@ page pageEncoding="UTF-8" %>

<%
    List<Payroll_daily_billDTO> billDTOs = (List<Payroll_daily_billDTO>) request.getAttribute("billDTOs");
    List<Payroll_daily_billModel> billModels = billDTOs.stream()
                                                       .map(dto -> new Payroll_daily_billModel(dto, "BANGLA"))
                                                       .collect(Collectors.toList());
    Payroll_daily_billDTO aBillDTO = billDTOs.get(0);
    String voucherNumber = StringUtils.convertToBanNumber(String.format("%d", aBillDTO.budgetRegisterId));
    Budget_registerDTO budgetRegisterDTO = Budget_registerDAO.getInstance().getDTOFromID(aBillDTO.budgetRegisterId);
    String issueNumber = StringUtils.convertToBanNumber(budgetRegisterDTO.issueNumber);

    String languageConvertedBillAmount = convertToBanNumber(String.format("%d", budgetRegisterDTO.billAmount));
    String formattedBillAmount = BangladeshiNumberFormatter.getFormattedNumber(languageConvertedBillAmount);
    String sumTotalInWord = BangladeshiNumberInWord.convertToWord(languageConvertedBillAmount);


    long monthYear = (long) request.getAttribute("monthYear");
    long budgetOfficeId = (long) request.getAttribute("budgetOfficeId");
    Budget_mappingDTO budgetMappingDTO = Budget_mappingRepository.getInstance()
                                                                 .getById(budgetRegisterDTO.budgetMappingId);

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);

    String pdfFileName = "Daily Basis Employee Payroll Bill "
            + DateUtils.getMonthYear(monthYear, "English", " ") + " "
            + Budget_officeRepository.getInstance().getText(budgetOfficeId, "English");

    String budgetOfficeNameBn = Budget_officeRepository.getInstance().getText(budgetOfficeId, "Bangla");
    String employeeCount = StringUtils.convertToBanNumber(String.valueOf(billDTOs.size()));
    String billIssuedTo = FinanceUtil.getFinance1HeadIssuedToName("Bangla");
    String dots = Stream.generate(() -> ".").limit(1000).collect(joining());
    String formattedBillDate = StringUtils.getFormattedDate("bangla", System.currentTimeMillis());
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .full-border {
        border: 2px solid black;
        padding: 5px;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td {
        padding: 5px;
    }

    .no-top-border {
        border-top-color: white !important;
    }

    .no-bottom-border {
        border-bottom-color: white !important;
    }

    thead th,
    thead td {
        padding: 5px 8px;
    }

    span.tab {
        display: inline-block;
        width: 5ch;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg" id="bill-div">
                <div class="kt-subheader__main ml-4">
                    <label class="h2 kt-subheader__title" style="color: #00a1d4;">
                        <%=UtilCharacter.getDataByLanguage(Language, "দৈনিকভিত্তিক(সাং-বাৎসরিক) কর্মচারীর মজুরি বাবদ বিল", "Daily-basis Employee's Wage Bill")%>
                    </label>
                </div>
                <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div>
                                টি. আর ফর্ম নং ২১<br>
                                (এস.আর ১৮৩ দ্রষ্টব্য)
                            </div>

                            <div class="text-center">
                                <h1>বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2>ক্রয়, সরবরাহ ও সেবা বাবদ ব্যায়ের বিল</h2>

                                <div class="mt-3">
                                    <span class="full-border">কোড নং-</span> <%=budgetOfficeNameBn%>
                                    <span class="full-border" style="margin-left : 0;">১০২</span>
                                    &nbsp;-&nbsp;<span class="full-border">
                                        <%=Budget_institutional_groupRepository.getInstance().getCode(
                                                budgetMappingDTO.budgetInstitutionalGroupId,
                                                "Bangla"
                                        )%>
                                      </span>
                                    &nbsp;-&nbsp;<span class="full-border">
                                        <%=Budget_officeRepository.getInstance().getCode(
                                                budgetMappingDTO.budgetOfficeId,
                                                "Bangla"
                                        )%>
                                      </span>
                                    &nbsp;-&nbsp;<span class="full-border">
                                            <%=Budget_operationRepository.getInstance().getCode(
                                                    budgetMappingDTO.budgetOperationId,
                                                    "Bangla"
                                            )%>
                                      </span>
                                    &nbsp;-&nbsp;<span class="full-border">
                                            <%=Economic_sub_codeRepository.getInstance().getCode(
                                                    Payroll_daily_billServlet.DAILY_BILL_ECONOMIC_SUB_CODE_ID,
                                                    "Bangla"
                                            )%>
                                      </span>
                                </div>
                            </div>

                            <div>
                                <div class="mt-5">
                                    <div class="row">
                                        <div class="col-3 fix-fill">
                                            টোকেন নং
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            ভাউচার নং
                                            <div class="blank-to-fill">
                                                &nbsp;&nbsp;<%=voucherNumber%>
                                            </div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill">
                                                <%=formattedBillDate%>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table-bordered mt-3">
                                        <thead class="text-center">
                                        <tr>
                                            <td style="width: 20%;" rowspan="2">অর্থনৈতিক কোড</td>
                                            <td style="width: 60%;" rowspan="2">সরবরাহকৃত দ্রব্যের বিবরণ</td>
                                            <td style="width: 20%;" colspan="2">পরিমাণ</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70%;">টাকা</td>
                                            <td style="width: 30%;">পয়সা</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td style="width: 20%; vertical-align: top;" rowspan="100%">
                                                <%=Economic_sub_codeRepository.getInstance().getText(
                                                        "Bangla",
                                                        Payroll_daily_billServlet.DAILY_BILL_ECONOMIC_SUB_CODE_ID
                                                )%>
                                            </td>
                                            <td class="text-center no-top-border no-bottom-border"
                                                style="text-align: center;">
                                                <div>
                                                    <div class="row">
                                                        <div class="col-5 fix-fill">
                                                            নং
                                                            <div class="blank-to-fill">
                                                                <%=issueNumber%>
                                                            </div>
                                                        </div>
                                                        <div class="col-5 fix-fill">
                                                            তাং
                                                            <div class="blank-to-fill">
                                                                <%=formattedBillDate%>
                                                            </div>
                                                        </div>
                                                        <div class="col-2 text">
                                                            খ্রি.
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right no-top-border no-bottom-border"></td>
                                            <td class="text-right no-top-border no-bottom-border"></td>
                                        </tr>
                                        <tr>
                                            <td class="no-top-border no-bottom-border">
                                                <%=budgetOfficeNameBn%>-এ কর্মরত <%=employeeCount%>
                                                (<%=BangladeshiNumberInWord.convertToWord(employeeCount)%>)
                                                জন সাং-বাৎসরিক
                                                কর্মচারীর <%=DateUtils.getMonthYear(monthYear, "BANGLA", "/")%>
                                                মাসের মজুরী বাবদ, <strong>"<%=billIssuedTo%>" </strong>
                                                এর অনুকূলে চেক দেয়।
                                            </td>
                                            <td class="text-right no-top-border no-bottom-border">
                                                <%=formattedBillAmount%>/-
                                            </td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>

                                        <tr style="height: 80px;">
                                            <td class="no-top-border no-bottom-border"></td>
                                            <td class="no-top-border no-bottom-border"></td>
                                            <td class="no-top-border no-bottom-border"></td>
                                        </tr>

                                        <tr>
                                            <td class="no-top-border">প্রত্যয়ন করা যাইতেছে যে, মঞ্জুরীপত্র ও বিল ভাউচার
                                                সংযুক্ত।
                                            </td>
                                            <td class="no-top-border"></td>
                                            <td class="no-top-border"></td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td class="text-right">
                                                <%=formattedBillAmount%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="text-right mt-4 fix-fill">
                                কথায়=&nbsp;&nbsp;
                                <div class="blank-to-fill">
                                    <%=sumTotalInWord%> টাকা (মাত্র)
                                </div>
                            </div>

                            <div class="mt-4">
                                <p>
                                    ১। প্রত্যায়ন করা যাইতেছে যে, জনস্বার্থে এই ব্যয় অপরিহার্য ছিল। আমি আরও প্রত্যায়ন
                                    করিতেছি যে, আমার
                                    জ্ঞান
                                    ও বিশ্বাস মতে, এই বিলে উল্লিখিত প্রদত্ত অর্থ নিম্নবর্ণিত ক্ষেত্র ব্যতীত প্রকৃত
                                    প্রাপককে প্রদান করা
                                    হইয়াছে,
                                    তবে স্থায়ী অগ্রিমের টাকা অপেক্ষা দায় বেশী হওয়ায়, অবশিষ্ট পাওনা এই বিলে দাবিকৃত টাকা
                                    প্রাপ্তির পর
                                    প্রদান করা হইবে। আমি যথাসম্ভব সকল ভাউচার গ্রহণ করিয়াছি এবং সেগুলি এমনভাবে বাতিল করা
                                    হইয়াছে যেন
                                    পুনরায় ব্যবহার
                                    করা না যায়। ২৫ টাকার উর্ধ্বের সকল ভাউচারসমূহ এমনভাবে সংরক্ষণ করা হইয়াছে, যেন
                                    প্রয়োজনমত ৩ বৎসরের
                                    মধ্যে
                                    এইগুলি পেশ করা যায়। সকল পূর্তকর্মের বিল সঙ্গে সংযুক্ত করা হইল।
                                </p>


                                <p>
                                    ২। প্রত্যায়ন করা যাইতেছে যে, যে সকল দ্রব্যের জন্য স্টোর একাউন্টস সংরক্ষণ করা হয় সে
                                    সব দ্রব্যাদি স্টক
                                    রেজিস্টারে অর্ন্তভূক্ত করা হইয়াছে।
                                </p>

                                <p>
                                    ৩। প্রত্যায়ন করা যাইতেছে যে, যে সব দ্রব্যাদি ক্রয়ের বিল পেশ করা হইয়াছে, সে সব
                                    দ্রব্যের পরিমাণ সঠিক,
                                    গুণগতমান
                                    ভাল, যে দরে ক্রয় করা হইয়াছে, তাহা বাজার দরের অধিক নহে এবং কার্যাদেশ বা চালান
                                    (ইনভয়েস) এর যথাস্থানে
                                    লিপিবদ্ধ
                                    করা হইয়াছে। যাহাতে একই দ্রব্যের জন্য দ্বিতীয় (ডুপ্লিকেট) অর্থ প্রদান এড়ান যায়।
                                </p>

                                <p>
                                    ৪। প্রত্যায়ন করা যাইতেছে যে- <br>
                                    (ক) এই বিলে দাবিকৃত পরিবহন ভাড়া প্রকৃতপক্ষে দেওয়া হইয়াছে এবং ইহা অপরিহার্য ছিল এবং
                                    ভাড়ার হার প্রচলিত
                                    যানবাহন
                                    ভাড়ার হারের মধ্যেই; এবং <br>

                                    (খ) সংশ্লিষ্ট সরকারী কর্মচারী সাধারণ বিধিবলে এই ভ্রমণের জন্য ভ্রমণ ব্যয় প্রাপ্য হন
                                    না, এবং এর
                                    অতিরিক্ত
                                    কোন
                                    বিশেষ পারিশ্রমিক, এই দায়িত্ব পালনের জন্য প্রাপ্য হইবেন না।
                                </p>
                            </div>
                        </section>

                        <section class="page shadow" data-size="A4">
                            <div class="text-center">
                                <h2>বাংলাদেশ জাতীয় সংসদ সচিবালয়</h2>
                            </div>

                            <div class="mt-3">
                                <p>
                                    ৫। প্রত্যায়ন করা যাইতেছে যে, যে সকল অধঃস্তন কর্মচারীর বেতন বিলে দাবী করা হইয়াছে
                                    তাহারা ঐ সময়ে
                                    প্রকৃতই
                                    সরকারী
                                    কাজে নিয়োজিত ছিলেন (এস, আর, ১৭১)।
                                </p>

                                <p>
                                    ৬। প্রত্যায়ন করা যাইতেছে যে- <br>

                                    (ক) মনোহারী দ্রব্য বা স্ট্যাম্প বাবত ২০ টাকার অধিক কোন ক্রয় স্থানীয়ভাবে করা হয় নাই।
                                    <br>

                                    (খ) ব্যক্তিগত কাজে ব্যবহৃত তাঁবু বহনের কোন খরচ এই বিলে অন্তর্ভূক্ত করা হয় নাই। <br>

                                    (গ) আবাসিক ভবনে ব্যবহৃত কোন বিদ্যুৎ বাবদ খরচ এই বিলে অন্তর্ভূক্ত করা হয় নাই। <br>

                                    (ঘ) এই বৎসরে প্রসেস সার্ভারদের প্রদত্ত পারিতোষিক টাকা
                                    .................................... (যা গত ৩
                                    বৎসরের
                                    জরিমানা বাবদ প্রাপ্তির গড় টাকার সামান্য অধিক হইবে না)। <br>
                                </p>

                                <p>
                                    ৭। যাহার নামে চেক ইস্যু করা হইবে (প্রযোজ্য ক্ষেত্রে) <span
                                        style="font-weight: bold;">"<%=billIssuedTo%>"</span>,
                                    এর অনুকুলে চেক প্রদেয়।
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div style="margin: 0 20px;">
                                        <div>
                                            *নিয়ন্ত্রণকারী/প্রতিস্বাক্ষরকারী কর্মকর্তার স্বাক্ষর
                                        </div>
                                        <div class="fix-fill mt-4 w-100">
                                            &nbsp;
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            নাম
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            পদবী
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-5 w-100">
                                            সীল
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            স্থান
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>
                                    <table class="table-bordered mt-2">
                                        <thead>
                                        <tr>
                                            <th style="width: 50%;">বরাদ্দের হিসাব</th>
                                            <th style="width: 45%;">টাকা</th>
                                            <th style="width: 5%;">প.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>১। শেষ বিলের টাকার অংক</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="fix-fill">
                                                ২। এ যাবত অতিরিক্ত বরাদ্দ<br><br>
                                                (পত্র নং..........................................)
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        convertToBanNumber(String.format("%d", budgetRegisterDTO.allocatedBudget))
                                                )%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="fix-fill">
                                                ৩। এ যাবত যে অংকের বরাদ্দ কমানো হয়েছে<br><br>
                                                (পত্র নং..........................................)
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ৪। নীট মোট<br>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-6">
                                    <div style="margin: 0 20px;">
                                        <div class="fix-fill w-100">
                                            বুঝিয়া পাইয়াছি
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-4 w-100">
                                            আয়ন কর্মকর্তার স্বাক্ষর
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            নাম
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            পদবী
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-5 w-100">
                                            সীল
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            স্থান
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>

                                    <table class="table-bordered mt-2">
                                        <thead>
                                        <tr>
                                            <th style="width: 53%;">বরাদ্দের হিসাব</th>
                                            <th style="width: 37%;">টাকা</th>
                                            <th style="width: 8%;">প.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>গত বিলের মোট জের (+)</td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        convertToBanNumber(String.format("%d", budgetRegisterDTO.uptoLastBillTotal))
                                                )%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                এই বিলের মোট (+)<br><br><br>
                                            </td>
                                            <td class="text-right">
                                                <%=formattedBillAmount%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                সংযুক্ত ---- পূর্তকর্মের বিলের টাকা <br><br><br>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                মোট (পরবর্তী বিলে জের টেনে নেয়া হবে)
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        convertToBanNumber(String.format("%d", budgetRegisterDTO.totalCost))
                                                )%>/-
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="mt-3" style="border-top: 5px solid black;">
                                <div class="text-center mt-2">
                                    <h2>হিসাবরক্ষণ অফিসে ব্যবহারের জন্য</h2>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-4 fix-fill">
                                        প্রদানের জন্য পাস করা হল টাকা&nbsp;&nbsp;
                                        <div class="blank-to-fill">
                                            <%=formattedBillAmount%>/-
                                        </div>
                                    </div>
                                    <div class="col-8 fix-fill">
                                        কথায়&nbsp;&nbsp;
                                        <div class="blank-to-fill">
                                            <%=sumTotalInWord%> টাকা (মাত্র)
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-4">
                                        <div>
                                            <strong>অডিটর (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            নাম<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং<%=dots%>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>সুপার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            নাম<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-4">
                                            চেক নং<%=dots%>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>হিসাবরক্ষণ অফিসার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            নাম<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-4">
                                            তারিখ<%=dots%>
                                        </div>
                                        <div class="fix-fill mt-4">
                                            চেক প্রদানকারীর স্বাক্ষর
                                        </div>
                                        <div class="fix-fill mt-4">
                                            নাম<%=dots%>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-3 foot-note" style="border-top: 1px solid black;">
                                <div>
                                    * কেবল মাত্র প্রযোজ্য ক্ষেত্রে
                                </div>

                                <div class="mt-2">
                                    বি. দ্র. - ইহা স্পষ্ঠভাবে স্মরণ রাখিতে হইবে যে, বরাদ্দের অতিরিক্ত ব্যয়ের জন্য আয়ন
                                    কর্মকর্তা
                                    ব্যক্তিগতভাবে দায়ী থাকিবেন। বরাদ্দের অতিরিক্ত ব্যয়ের বিপরীতে যদি তিনি অতিরিক্ত
                                    বরাদ্দ মঞ্জুর করাইতে
                                    না পারেন, তবে অতিরিক্ত ব্যয়িত অর্থ তাহার ব্যক্তিগত ভাতাদি হইতে আদায় করা হইবে।
                                </div>

                                <div class="mt-1">
                                    বা.নি.মু. ডন-১০/২০১০-১১/৫,০০,০০০ কপি,মুঃ আঃ নং-৮১/০৯-১০ তাং ২৯/৬/২০১০ইং।
                                </div>
                            </div>
                        </section>

                        <section class="page shadow" data-size="A4">
                            <div class="text-center">
                                <h1 style="font-weight: normal">বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">অর্থ শাখা-১</h2>
                            </div>

                            <div class="row mt-4">
                                <div class="col-6">
                                    নম্বর: ১১.০০.০০০০.৬৫৯.৩৫.০০১.১১.<%=issueNumber%>
                                </div>
                                <div class="col-6 text-right">
                                    তারিখ: <%=formattedBillDate%>
                                </div>
                            </div>

                            <div class="mt-4">
                                <span class="tab">প্রাপক:</span> চিফ একাউন্টস এন্ড ফিন্যান্স অফিসার<br>
                                <span class="tab"></span> বাংলাদেশ জাতীয় সংসদ সচিবালয়<br>
                                <span class="tab"></span> হিসাব ভবন, সেগুনবাগিচা<br>
                                <span class="tab"></span> ঢাকা।<br>
                            </div>

                            <div class="mt-4">
                                <span class="tab">বিষয়:</span>
                                <strong style="border-bottom: 1px solid black;">
                                    <%=budgetOfficeNameBn%>-এ কর্মরত <%=employeeCount%>
                                    (<%=BangladeshiNumberInWord.convertToWord(employeeCount)%>)
                                    জন সাং-বাৎসরিক কর্মচারীর <%=DateUtils.getMonthYear(monthYear, "BANGLA", "/")%> মাসের
                                    মজুরী বাবদ
                                    <%=formattedBillAmount%>/- টাকা মঞ্জুরী।
                                </strong>
                            </div>

                            <div class="mt-5">
                                জনাব,<br>
                                <div>
                                    <span class="tab"></span>
                                    <%=budgetOfficeNameBn%>-এ কর্মরত <%=employeeCount%>
                                    (<%=BangladeshiNumberInWord.convertToWord(employeeCount)%>)
                                    জন সাং-বাৎসরিক কর্মচারীর <%=DateUtils.getMonthYear(monthYear, "BANGLA", "/")%> মাসের
                                    মজুরী বাবদ
                                    <%=formattedBillAmount%>/- (<%=sumTotalInWord%>) টাকা ব্যয়ের নিমিত্তে আমি মাননীয় স্পীকারের
                                    মঞ্জুরী করতে আদিষ্ট হয়েছি।
                                </div>


                                <div class="mt-4">
                                    <span class="tab">২।</span>
                                    উক্ত
                                    ব্যয় <%=convertToBanNumber(BudgetInfo.getEconomicYear(BudgetUtils.getEconomicYearBeginYear(monthYear)))%>
                                    অর্থ বছরের
                                    ১০২ - বাংলাদেশ জাতীয় সংসদ,
                                    <%=Budget_institutional_groupRepository.getInstance().getText(
                                            budgetMappingDTO.budgetInstitutionalGroupId,
                                            true,
                                            "Bangla"
                                    )%>, &nbsp;
                                    <%=Budget_officeRepository.getInstance().getText(
                                            budgetMappingDTO.budgetOfficeId,
                                            true,
                                            "Bangla"
                                    )%> এর পরিচালন কোড-
                                    <%=Budget_operationRepository.getInstance().getCode(
                                            budgetMappingDTO.budgetOperationId,
                                            "Bangla"
                                    )%> এর
                                    <%=Economic_sub_codeRepository.getInstance().getText(
                                            "Bangla",
                                            Payroll_daily_billServlet.DAILY_BILL_ECONOMIC_SUB_CODE_ID
                                    )%> খাত হতে বহনযোগ্য।
                                </div>

                                <div class="mt-4">
                                    <span class="tab">৩।</span> যথাযথ কর্তৃপক্ষের অনুমোদঙ্ক্রমে ব্যয়/আর্থিক মঞ্জুরী আদেশ
                                    জারী করা হল।
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-8"></div>
                                <div class="col-4 text-center">
                                    আপনার একান্ত
                                    <div style="margin-top: 4em;">
                                        <%=FinanceUtil.getFinance1HeadName("Bangla")%><br>
                                        <%=FinanceUtil.getFinance1HeadDesignation("Bangla")%><br>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <%
                            long runningTotal = 0;
                            boolean isLastPage = false;
                            final int rowsPerPage = 20;
                            int index = 0;
                            while (index < billModels.size()) {
                                boolean isFirstPage = (index == 0);
                        %>
                        <section class="page shadow" data-size="A4">
                            <%if (isFirstPage) {%>
                            <div class="text-center">
                                <h1 style="font-weight: normal">বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">অর্থ শাখা-১</h2>
                                <h2 style="font-weight: normal">
                                    বাংলাদেশ জাতীয় সংসদের
                                    <%=budgetOfficeNameBn%>
                                    কার্যালয়ে
                                    কর্মরত <%=convertToBanNumber(String.valueOf(billDTOs.size()))%> জন কর্মকর্তার
                                    <%=DateUtils.getMonthYear(monthYear, Language, "/")%> খ্রিঃ মাসের মজুরী বাবদ বিল
                                    বিল
                                </h2>
                            </div>
                            <%}%>

                            <div>
                                <div <%=isFirstPage ? "class=\"mt-4\"" : ""%>>
                                    <table class="table-bordered mt-2 w-100">
                                        <thead>
                                        <tr style="height: 50px;background-color:lightgrey">
                                            <th style="width: 6%;">ক্র. নং</th>
                                            <th style="width: 26%;">নাম</th>
                                            <th style="width: 15%;">পদবী</th>
                                            <th style="width: 8%;">দিন</th>
                                            <th style="width: 10%;">দৈনিক হার</th>
                                            <th style="width: 15%;">দেয় টাকা</th>
                                            <th style="width: 20%;">স্বাক্ষর</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <%if (!isFirstPage) {%>
                                        <tr>
                                            <td colspan="3" class="text-right">
                                                পূর্ব পৃষ্ঠার জের=
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(runningTotal))
                                                )%>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <%}%>

                                        <%
                                            int rowsInThisPage = 0;
                                            while (index < billModels.size() && rowsInThisPage < rowsPerPage) {
                                                isLastPage = (index == (billModels.size() - 1));
                                                rowsInThisPage++;
                                                Payroll_daily_billModel model = billModels.get(index++);
                                                runningTotal += Long.parseLong(model.totalAmount);
                                        %>
                                        <tr style="height: 40px">
                                            <td><%=Utils.getDigits(index, "BANGLA")%>
                                            </td>
                                            <td class="align-top">
                                                <%=model.name%><br>
                                                পিতা-<%=Employee_recordsRepository.getInstance().getById(Long.parseLong(model.employeeRecordsId)).fatherNameBng%>
                                            </td>
                                            <td class="align-top">
                                                <%=model.organogramName%>
                                            </td>
                                            <td class="text-center">
                                                <%=Utils.getDigits(model.day, "BANGLA")%>
                                            </td>
                                            <td class="text-center">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.dailyRate, "BANGLA"))%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.totalAmount, "BANGLA"))%>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                <%=isLastPage ? "সর্বমোট" : "উপমোট"%>=
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(runningTotal, "BANGLA"))%>
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <%if (isLastPage) {%>
                                    <div class="mt-2 offset-4 ml-auto text-center">
                                        কথায়:
                                        <strong><%=BangladeshiNumberInWord.convertToWord(Utils.getDigits(runningTotal, "Bangla"))%>
                                        </strong> টাকা মাত্র
                                    </div>
                                    <%}%>
                                </div>
                            </div>
                        </section>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>