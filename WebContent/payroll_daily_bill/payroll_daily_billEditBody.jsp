<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="payroll_daily_bill.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>

<%@include file="../pb/addInitializer2.jsp" %>

<%
	actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    String formTitle = LM.getText(LC.PAYROLL_DAILY_BILL_ADD_PAYROLL_DAILY_BILL_ADD_FORMNAME, loginDTO);
	String context = request.getContextPath() + "/";
%>

<style>
	.template-row {
		display: none;
	}
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					<%=formTitle%>
				</h3>
			</div>
		</div>

        <form class="form-horizontal" id="bill-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
					<div class="col-12 row mt-2">
						<div class="col-lg-4 col-md-6 form-group">
							<label class="h5" for="budgetInstitutionalGroup">
								<%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
							</label>
							<select id="budgetInstitutionalGroup"
									name='budgetInstitutionalGroupId'
									class='form-control rounded shadow-sm'
									onchange="institutionalGroupChanged(this);"
							>
								<%=Budget_institutional_groupRepository.getInstance().buildOptions(
										Language, 0L, false
								)%>
							</select>
						</div>

						<div class="col-lg-4 col-md-6 form-group">
							<label class="h5" for="budgetOffice">
								<%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
							</label>
							<select id="budgetOffice" name='budgetOfficeId'
									class='form-control rounded shadow-sm'
									onchange="budgetOfficeChanged(this);">
								<%--Dynamically Added with AJAX--%>
							</select>
						</div>

						<div class="col-lg-4 col-md-6 form-group">
							<label class="h5" for="budgetOffice">
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
							</label>
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="monthYear_js"/>
								<jsp:param name="LANGUAGE" value="<%=Language%>"/>
								<jsp:param name="HIDE_DAY" value="true"/>
							</jsp:include>
							<input type='hidden' name='monthYear' id='monthYear' value=''>
						</div>
					</div>
                </div>

				<div class="mt-4 table-responsive">
					<div class="row">
						<div class="col-sm-12 col-md-6 row">
							<label class="col-md-4 col-form-label text-md-right" for="issueNumber">
								<%=LM.getText(LC.BILL_MANAGEMENT_ADD_OFFICELETTERNUMBER, loginDTO)%>
								<span class="required"> * </span>
							</label>
							<div class="col-md-8">
								<input type='text' class='form-control' disabled name='issueNumber' id="issueNumber"/>
							</div>
						</div>
						<div class="col-sm-12 col-md-6 row">
							<label class="col-md-4 col-form-label text-md-right" for="issueNumber">
								<%=LM.getText(LC.BILL_MANAGEMENT_ADD_OFFICELETTERNUMBER, loginDTO)%>
								<span class="required"> * </span>
							</label>
							<div class="col-md-8">
								<jsp:include page="/date/date.jsp">
									<jsp:param name="DATE_ID" value="issueDate_js"/>
									<jsp:param name="IS_DISABLED" value="true"/>
									<jsp:param name="LANGUAGE" value="<%=Language%>"/>
								</jsp:include>
								<input type='hidden' name='issueDate' id='issueDate'>
							</div>
						</div>
					</div>

					<table id="allowance-table" class="table table-bordered table-striped text-nowrap mt-3">
						<thead>
						<tr>
							<th rowspan="2">
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
							</th>
							<th rowspan="2">
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_ORGANOGRAMID, loginDTO)%>
							</th>
							<th rowspan="2">
								<%=getDataByLanguage(
										Language,
										"মোবাইল নাম্বার",
										"Mobile Number"
								)%>
							</th>
							<th rowspan="2">
								<%=getDataByLanguage(
										Language,
										"সঞ্চয়ী হিসাব নাম্বার",
										"Savings Account Number"
								)%>
							</th>
							<th colspan="3" class="text-center">
								<%=getDataByLanguage(
										Language,
										"কর্তন ও আদায়",
										"Deduction and Collection"
								)%>
							</th>
							<th rowspan="2">
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_REVENUESTAMPDEDUCTION, loginDTO)%>
							</th>
							<th rowspan="2">
								<%=getDataByLanguage(
										Language,
										"নীট দাবী",
										"Net Amount"
								)%>
							</th>
						</tr>
						<tr>
							<th>
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAILYRATE, loginDTO)%>
							</th>
							<th>
								<%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_DAY, loginDTO)%>
							</th>
							<th>
								<%=getDataByLanguage(
										Language,
										"মোট টাকার পরিমাণ",
										"Total Amount"
								)%>
							</th>
						</tr>
						</thead>
						<tbody class="main-tbody">
						</tbody>

						<%--don't put these tr inside tbody--%>
						<tr class="loading-gif" style="display: none;">
							<td class="text-center" colspan="100%">
								<img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
								<span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
							</td>
						</tr>

						<tr class="template-row">
							<input type="hidden" name="employeeRecordsId" value="-1">
							<input type="hidden" name="payrollDailyBillId" value="-1">
							<td class="row-data-name"></td>
							<td class="row-data-organogramName"></td>
							<td class="row-data-mobileNumber"></td>
							<td class="row-data-savingAccountNumber"></td>
							<td class="row-data-dailyRate"></td>
							<td class="row-data-day"></td>
							<td class="row-data-totalAmount"></td>
							<td class="row-data-revenueStampDeduction"></td>
							<td class="row-data-netAmount"></td>
						</tr>
					</table>
				</div>

				<div class="row">
					<div class="col-12 mt-3 text-right">
						<button id="prepareSummary-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
								type="button" onclick="submitForm('prepareSummary')">
							<%=getDataByLanguage(Language, "ব্যাংক স্টেটমেন্ট প্রস্তুত করুন", "Prepare Bank Statement")%>
						</button>
						<button id="prepareBill-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
								type="button" onclick="submitForm('prepareBill')">
							<%=getDataByLanguage(Language, "বিল প্রস্তুত করুন", "Prepare Bill")%>
						</button>
					</div>
				</div>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp"%>
<script src="<%=context%>/assets/scripts/input_validation.js" type="text/javascript"></script>

<script type="text/javascript">
	let actionName = 'add';

	const $issueNumberElem = $('#issueNumber');
	const issueDateId = 'issueDate_js';
	const issueDateFieldId = 'issueDate';

	function showOrHideLoadingGif(tableId, toShow) {
		const loadingGif = $('#' + tableId + ' tr.loading-gif');
		if(toShow){
			document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
			document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
			loadingGif.show();
		}
		else loadingGif.hide();
	}

	function clearOffice() {
		document.getElementById('budgetOffice').innerHTML = '';
	}

	function clearMonthYear() {
		resetDateById('monthYear_js');
	}

	function clearTable() {
		const tableBody = document.querySelector('#allowance-table tbody.main-tbody');
		const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
		tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
				+ emptyTableText + '</td></tr>';

		document.querySelectorAll('#allowance-table tfoot')
				.forEach(tfoot => tfoot.remove());
	}

	function clearNextLevels(startIndex) {
		const toClearFunctions = [
			clearOffice, clearMonthYear, clearTable
		];
		for (let i = startIndex; i < toClearFunctions.length; i++) {
			toClearFunctions[i]();
		}
	}

	async function institutionalGroupChanged(selectElement) {
		const selectedInstitutionalGroupId = selectElement.value;
		clearNextLevels(0);
		if (selectedInstitutionalGroupId === '') return;

		const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=false'
				  + '&budget_instituitional_group_id=' + selectedInstitutionalGroupId;

		const response = await fetch(url);
		document.getElementById('budgetOffice').innerHTML = await response.text();
	}

	function budgetOfficeChanged() {
		clearNextLevels(1);
	}

	async function monthYearChanged(monthYear) {
		const budgetOfficeId = document.getElementById('budgetOffice').value;
		if (budgetOfficeId === '') return;

		showOrHideLoadingGif('allowance-table', true);

		const url = 'Payroll_daily_billServlet?actionType=ajax_getDailyPayrollData'
				  + '&monthYear=' + monthYear
				  + '&budgetOfficeId=' + budgetOfficeId;
		const response = await fetch(url);

		const resJson = await response.json();
		console.log({resJson});

		const {isAlreadyAdded, dailyBillModels, budgetRegisterModel} = resJson;
		console.log("isAlreadyAdded", isAlreadyAdded);
		console.log("dailyBillModels", dailyBillModels);
		actionName = isAlreadyAdded === true ? 'edit' : 'add';
		if(isAlreadyAdded === true) {
			if(budgetRegisterModel) {
				$issueNumberElem.val(budgetRegisterModel.issueNumber);
				setDateByTimestampAndId(issueDateId, budgetRegisterModel.issueDate);
			}
		} else {
			setDisableStatusById(issueDateId, false);
			$issueNumberElem.prop('disabled', false);
		}

		const tableBody = document.querySelector('#allowance-table tbody');
		const templateRow = document.querySelector('#allowance-table tr.template-row');

		showOrHideLoadingGif('allowance-table', false);
		if(dailyBillModels.length === 0){
			clearTable();
		}
		else{
			dailyBillModels.forEach(model => showModelInTable(tableBody, templateRow, model));
			calculateAllRowTotal();
		}
	}

	function setRowData(templateRow, payrollDailyBillModel) {
		const rowDataPrefix = 'row-data-';
		for (const key in payrollDailyBillModel) {
			const td = templateRow.querySelector('.' + rowDataPrefix + key);
			if (!td) continue;
			td.innerText = payrollDailyBillModel[key];
		}
	}

	function showModelInTable(tableBody, templateRow, payrollDailyBillModel) {
		const modelRow = templateRow.cloneNode(true);
		modelRow.classList.remove('template-row');
		modelRow.querySelector('input[name="employeeRecordsId"]').value = payrollDailyBillModel.employeeRecordsId;
		modelRow.querySelector('input[name="payrollDailyBillId"]').value = payrollDailyBillModel.payrollDailyBillId;
		setRowData(modelRow, payrollDailyBillModel);
		tableBody.append(modelRow);
	}

	function calculateAllRowTotal() {
		const colIndicesToSum = [8];
		const totalTitleColSpan = 1;
		const totalTitle = '<%=getDataByLanguage(Language, "সর্বমোট", "Total")%>';
		setupTotalRow('allowance-table', totalTitle, totalTitleColSpan, colIndicesToSum);
	}

	function setButtonDisableState(value){
		$('#prepareBill-btn').prop('disabled',value);
	}

	const form = $('#bill-form');
	function submitForm(source){
		setButtonDisableState(true);
		$.ajax({
			type: "POST",
			url: "Payroll_daily_billServlet?actionType=ajax_" + actionName + "&source=" + source,
			data: form.serialize(),
			dataType: 'JSON',
			success: function (response) {
				if (response.responseCode === 0) {
					$('#toast_message').css('background-color', '#ff6063');
					showToastSticky(response.msg, response.msg);
					setButtonDisableState(false);
				} else if (response.responseCode === 200) {
					window.location.assign(getContextPath() + response.msg);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
						+ ", Message: " + errorThrown);
				setButtonDisableState(false);
			}
		});
	}

    function init() {
		$('#monthYear_js').on('datepicker.change', (event, param) => {
			const isValidDate = dateValidator('monthYear_js', true);
			const monthYear = getDateStringById('monthYear_js');
			if (isValidDate) {
				$('#monthYear').val(monthYear);
				monthYearChanged(monthYear);
			}else {
				clearTable();
			}
		});

		$('#' + issueDateId).on('datepicker.change', (event, param) => {
			const isValidDate = dateValidator('monthYear_js', true);
			if (isValidDate) {
				$('#' + issueDateFieldId).val(getDateStringById(issueDateId));
			}
		});
    }

    $(document).ready(function () {
        init();
    });
</script>