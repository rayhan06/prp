<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="language.LM"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>


<%
	String url = request.getParameter("url");
	LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
	RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
	String context = "../../.." + request.getContextPath() + "/";
%>


<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
			<div class="row">
				<div class="col-md-6" >
					<div class="form-group row">
						<label class="col-md-2 col-form-label"><%=LM.getText(LC.TASK_TYPE_ADD_NAMEEN, loginDTO)%></label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="name_en" placeholder="" name="name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>

                <div class="col-md-6" >
					<div class="form-group row">
						<label class="col-md-2 col-form-label"><%=LM.getText(LC.TASK_TYPE_ADD_NAMEBN, loginDTO)%></label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="name_bn" placeholder="" name="name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>

				<div class="col-md-6" >
					<div class="form-group row">
						<label class="col-md-2 col-form-label"><%=LM.getText(LC.TASK_TYPE_ADD_OFFICEUNITID, loginDTO)%></label>
						<div class="col-md-10">
							<button type="button" class="btn btn-primary form-control" id="office_units_id_modal_button" onclick="officeModalButtonClicked();">
								<%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
							</button>
							<div class="input-group" id="office_units_id_div" style="display: none">
								<input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
								<button type="button" class="btn btn-secondary btn-block shadow btn-border-radius"
										id="office_units_id_text" onclick="officeModalEditButtonClicked()">
								</button>
								<span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger" onclick="crsBtnClicked('office_units_id');"
                                            id='office_units_id_crs_btn' tag='pb_html'>
                                        x
                                    </button>
                                </span>
							</div>
						</div>
					</div>
				</div>

                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label"><%=LM.getText(LC.TASK_TYPE_ADD_LEVEL, loginDTO)%></label>
                        <div class="col-md-10">
                            <input type="number" class="form-control" id="level" placeholder="" name="level" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<div class="ml-auto">
            <input type="hidden" name="search" value="yes"/>
            <button type="button" class="btn shadow " onclick="allfield_changed('',0)"
                    style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px; margin-right: 8px">
                <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
            </button>
        </div>
	</div>
</div>


<%@include file="../common/pagination_with_go2.jsp"%>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>


<script type="text/javascript">

	function dosubmit(params) {
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200)
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		  xhttp.open("Get", "Approval_path_assignServlet?actionType=search&" + params, true);
		  xhttp.send();
	}

	function allfield_changed(go, pagination_number)
	{
		var params = '';
		params +=  '&name_en='+ $('#name_en').val();
		params +=  '&name_bn='+ $('#name_bn').val();
		params +=  '&office_unit_ids='+ $('#office_units_id_input').val();
		params +=  '&level='+ $('#level').val();

		params +=  '&search=true&ajax=true';

		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged == 0)
		{
			console.log("go found");
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		dosubmit(params);

	}

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        console.log(selectedOffice);
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

	function officeModalEditButtonClicked() {
		officeSelectModalUsage = 'officeUnitId';
		officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
		$('#search_office_modal').modal();
	}
</script>

