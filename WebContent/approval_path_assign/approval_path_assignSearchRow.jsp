<%@page pageEncoding="UTF-8" %>
<%@page import="task_type.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="task_type_level.TaskTypeLevelUtil" %>

<%
    boolean hasApprovalNotPath = notAssigned.contains(taskTypeLevelDTO.iD);
    Task_typeDTO taskTypeDTO = Task_typeRepository.getInstance().getDTOById(taskTypeLevelDTO.taskTypeId);
%>

<td>
    <%=taskTypeDTO.nameEn%>
</td>

<td>
    <%=taskTypeDTO.nameBn%>
</td>

<td>
    <%=util.StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(taskTypeLevelDTO.level))%>
</td>

<td>
    <%=TaskTypeLevelUtil.showOfficesName(Language, taskTypeLevelDTO.officeUnitIds)%>
</td>

<td>
    <button type="button" class="btn btn-sm border-0 shadow"
            style="background-color: #cc22c1; color: white; border-radius: 8px"
            onclick="location.href='Task_type_approval_pathServlet?actionType=get<%=hasApprovalNotPath?"Add":"Edit"%>Page&taskTypeLevelId=<%=taskTypeLevelDTO.iD%>&data=approvalPathAssign'">
        <%=LM.getText(hasApprovalNotPath ? LC.USER_ADD_ADD : LC.USER_SEARCH_EDIT, loginDTO)%>
    </button>
</td>

<td>
    <button type="button" class="btn btn-sm border-0 shadow"
            style="background-color: #22ccc1; color: white; border-radius: 8px"
            onclick="location.href='Task_typeServlet?actionType=view&ID=<%=taskTypeDTO.iD%>'">
        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
    </button>
</td>