<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="user.*" %>
<%@ page import="task_type_assign.Task_type_assignDTO" %>
<%@ page import="task_type_assign.Task_type_assignRepository" %>
<%@ page import="task_type_level.TaskTypeLevelDTO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.Map" %>
<%@ page import="task_type_approval_path.TaskTypeApprovalPathRepository" %>
<%@ page import="java.util.Set" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.TASK_TYPE_ADD_NAMEEN, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.TASK_TYPE_ADD_NAMEBN, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.TASK_TYPE_ADD_LEVEL, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.TASK_TYPE_ADD_OFFICEUNITID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TASK_TYPE_APPROVAL_PATH_ADD_TASK_TYPE_APPROVAL_PATH_ADD_FORMNAME, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<TaskTypeLevelDTO> data = (List<TaskTypeLevelDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                List<Long> ids = data.stream()
                        .map(e->e.iD)
                        .collect(Collectors.toList());
                Set<Long> notAssigned = TaskTypeApprovalPathRepository.getInstance().isApprovalPathIsNotSetYet(ids);
                for (TaskTypeLevelDTO taskTypeLevelDTO : data) {
        %>
        <tr>
            <%@include file="approval_path_assignSearchRow.jsp" %>
        </tr>
        <% } } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>