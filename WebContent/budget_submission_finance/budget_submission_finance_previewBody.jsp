<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="budget_operation.BudgetOperationModel" %>
<%@ page import="economic_code.Economic_codeDTO" %>
<%@ page import="common.NameDTO" %>
<%@ page import="economic_group.EconomicGroupRepository" %>
<%@ page import="economic_code.Economic_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeDTO" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="java.util.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="static java.util.stream.Collectors.toList" %>
<%@ page import="budget_submission_info.Budget_submission_infoDTO" %>
<%@ page import="budget_submission_info.Budget_submission_infoDAO" %>
<%@ page import="budget.*" %>
<%@ page import="static java.util.stream.Collectors.toMap" %>
<%@ page import="budget_submission_info.Budget_submission_infoRepository" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);

    String context = request.getContextPath() + "/";

    String errorMessage = (String) request.getAttribute("errorMessage");

    BudgetInfo thisYearBudgetInfo = (BudgetInfo) request.getAttribute("thisYearBudgetInfo");
    if(thisYearBudgetInfo == null) thisYearBudgetInfo = new BudgetInfo();

    BudgetSelectionInfoDTO thisYearSelectionDTO = (BudgetSelectionInfoDTO) request.getAttribute("thisYearSelectionDTO");
    if(thisYearSelectionDTO == null) thisYearSelectionDTO = new BudgetSelectionInfoDTO();

    List<BudgetDTO> budgetDTOS = (List<BudgetDTO>) request.getAttribute("thisYearBudgetDTOs");
    Map<Long, List<BudgetDTO>> prevYearBudgetDTOsByMappingId = (Map<Long, List<BudgetDTO>>) request.getAttribute("prevYearBudgetDTOsByMappingId");


    Map<Long, List<BudgetDTO>> mapByBudgetMappingId = new HashMap<>();
    Budget_submission_infoDTO budgetSubmissionInfoDTO = null;
    BudgetTypeEnum budgetType = thisYearBudgetInfo.budgetTypeEnum;

    if (errorMessage == null) {
        budgetSubmissionInfoDTO = Budget_submission_infoDAO.getInstance().getByBudgetTypeAndBudgetOffice(
                thisYearSelectionDTO.iD, budgetType.getValue(), BudgetUtils.FINANCE_SECTION_OFFICE_ID
        );
        if(budgetDTOS == null || budgetDTOS.isEmpty()){
            mapByBudgetMappingId = new HashMap<>();
            errorMessage = UtilCharacter.getDataByLanguage(
                    Language, 
                    "চলমান অর্থবছরের জন্যে কোনো তথ্য দেওয়া হয় নি",
                    "No Data has been provided for this economic year."
            );
        }else{
            mapByBudgetMappingId = budgetDTOS.stream()
                    .collect(Collectors.groupingBy(dto -> dto.budgetMappingId));
        }
    }

    boolean isBudgetSubmitted = budgetSubmissionInfoDTO != null
            && budgetSubmissionInfoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue();

    String economicYear = StringUtils.convertBanglaIfLanguageIsBangla(
            Language,
            thisYearBudgetInfo.economicYear
    );
    String prevEconomicYear = StringUtils.convertBanglaIfLanguageIsBangla(
            Language,
            BudgetInfo.getEconomicYear(thisYearBudgetInfo.beginYear - 1)
    );
%>

<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-auto mr-3 mt-4">
    <button type="button" class="btn" id='printer'
            onclick="printDiv('kt_content')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet" style="background: #f9f9fb!important">
            <div class="kt-portlet__body m-4">
                <div class="row">
                    <div class="col-12 row">
                        <div class="offset-3 col-6 text-center">
                            <img width="20%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>

                <%if (errorMessage != null) {%>
                    <div class="container text-center text-danger m-4">
                        <h5>
                            <%=errorMessage%>
                        </h5>
                    </div>
                <%}%>
                <%--Initialized mapByBudgetMappingId with empty HashMap.No need to give else for if (errorMessage != null)--%>

                <%
                    for (Map.Entry<Long, List<BudgetDTO>> entry : mapByBudgetMappingId.entrySet()) {
                        Long budgetMappingId = entry.getKey();
                        List<BudgetDTO> budgetDTOsByMapping = entry.getValue();
                        BudgetOperationModel model = Budget_mappingRepository.getInstance()
                                .getBudgetOperationModelWithoutSubCode(budgetMappingId, thisYearBudgetInfo.economicYear, Language);

                        Map<Long, BudgetDTO> prevYearBudgetDTOsBySubCodeId =
                                prevYearBudgetDTOsByMappingId.getOrDefault(budgetMappingId, new ArrayList<>())
                                        .stream()
                                        .collect(toMap(dto -> dto.economicSubCodeId, dto -> dto));
                %>
                <div class="row mt-5 mx-0 py-3 rounded" style="border: 1px solid #00ACD8;">
                    <div class="col-md-9 text-left">
                        <h5 class="text-left mt-3 top-section-font" id="budgetCatHeading">
                            <%=model.budgetCatName + " - " + model.officeCode%>
                        </h5>
                        <h5 class="text-left mt-3 top-section-font" id="operationalCodeHeading">
                            <%=model.description + " - " + model.operationCode%>
                        </h5>
                    </div>
                    <div class="col-md-3">
                        <h5 class="text-md-right mt-3 top-section-font" id="economicYearHeading">
                            <%=model.economicYear%>
                        </h5>
                    </div>
                </div>

                        <%
                            long budgetOfficeId = Budget_mappingRepository.getInstance().getOfficeId(budgetMappingId);
                            boolean hasOfficeSubmitted = Budget_submission_infoRepository.getInstance().hasOfficeSubmitted(
                                    thisYearSelectionDTO.iD, budgetOfficeId, budgetType.getValue()
                            );
                            if(!hasOfficeSubmitted){
                        %>
                                <div class="text-danger text-center h4 m-5">
                                    <%=isLanguageEnglish? "Office has not yet submitted this Budget!"
                                                        : "অফিস এখনো এই বাজেট জমা দেয়নি"%>
                                </div>
                        <%
                                continue;
                            }
                        %>

                <div class="row mt-5 page-break">
                    <div class="w-100">
                        <h5 class="text-right">(<%=LM.getText(LC.BUDGET_MONEY_INPUT_UNIT_HEADER, loginDTO)%>)</h5>
                        <table class="table" id="budget-view-table">
                            <thead>
                            <tr>
                                <th>
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </th>
                                <th>
                                    <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                </th>

                                <%--Previous Year Data--%>
                                <th>
                                    <%=BudgetTypeEnum.BUDGET.getText(Language)%>
                                    <br>
                                    <%=prevEconomicYear%>
                                </th>

                                <th>
                                    <%=BudgetTypeEnum.REVISED_BUDGET.getText(Language)%>
                                    <br>
                                    <%=prevEconomicYear%>
                                </th>

                                <%--Conditional Heading--%>
                                <th>
                                    <%=BudgetTypeEnum.BUDGET.getText(Language)%>
                                    <br>
                                    <%=economicYear%>
                                </th>
                                <%if (budgetType == BudgetTypeEnum.REVISED_BUDGET) {%>
                                <th>
                                    <%=BudgetTypeEnum.REVISED_BUDGET.getText(Language)%>
                                    <br>
                                    <%=economicYear%>
                                </th>
                                <%}%>
                                <th>
                                    <%=LM.getText(LC.BUDGET_COMMENT, loginDTO)%>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <%--Economic Groups--%>
                            <%
                                TreeMap<Long, List<BudgetDTO>> sortedMapByEconomicGroup =
                                        budgetDTOsByMapping.stream()
                                                           .collect(Collectors.groupingBy(
                                                                   dto -> dto.economicGroupId,
                                                                   TreeMap::new,
                                                                   toList()
                                                           ));

                                for (Map.Entry<Long, List<BudgetDTO>> groupBudgedPair : sortedMapByEconomicGroup.entrySet()) {
                                    Long economicGroupId = groupBudgedPair.getKey();
                                    List<BudgetDTO> budgetDTOsByGroup = groupBudgedPair.getValue();
                                    NameDTO groupDTO = EconomicGroupRepository.getInstance().getDTOByID(economicGroupId);
                                    if (groupDTO == null) {
                                        groupDTO = new NameDTO();
                                    }
                            %>
                            <tr>
                                <td style=" font-weight: bold" colspan="100%">
                                    <%String groupID = StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(groupDTO.iD));%>
                                    <%=groupID + " - " + UtilCharacter.getDataByLanguage(Language, groupDTO.nameBn, groupDTO.nameEn)%>
                                </td>
                            </tr>

                            <%--Economic Code--%>
                            <%
                                Map<Long, List<BudgetDTO>> mapByEconomicCode = budgetDTOsByGroup.stream()
                                                                                                .collect(Collectors.groupingBy(dto -> dto.economicCodeId));

                                List<Long> economicCodeIdSortedByCode = mapByEconomicCode.keySet()
                                                                                         .stream().sorted(BudgetUtils::compareEconomicCodeId)
                                                                                         .collect(toList());

                                for (Long economicCodeId : economicCodeIdSortedByCode) {
                                    List<BudgetDTO> budgetDTOsByCode = mapByEconomicCode.get(economicCodeId);
                                    Economic_codeDTO codeDTO = Economic_codeRepository.getInstance().getById(economicCodeId);
                                    if (codeDTO == null) {
                                        codeDTO = new Economic_codeDTO();
                                    }
                            %>
                            <tr>
                                <td style="font-weight: bold" colspan="100%">
                                    <%
                                        String economicCode = StringUtils.convertBanglaIfLanguageIsBangla(Language, codeDTO.code);%>
                                    <%= economicCode + " - "
                                        + UtilCharacter.getDataByLanguage(Language, codeDTO.descriptionBn, codeDTO.descriptionEn)%>
                                </td>
                            </tr>

                            <%--Economic Sub Code--%>
                            <%
                                budgetDTOsByCode.sort(BudgetUtils::compareBudgetDTOonSubCode);

                                for (BudgetDTO budgetDTOofSubCode : budgetDTOsByCode) {
                                    BudgetDTO prevYearBudgetDTOofSubCode = prevYearBudgetDTOsBySubCodeId.getOrDefault(
                                            budgetDTOofSubCode.economicSubCodeId,
                                            new BudgetDTO()
                                    );

                                    Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance()
                                                                                                 .getDTOByID(budgetDTOofSubCode.economicSubCodeId);
                                    if (subCodeDTO == null) {
                                        subCodeDTO = new Economic_sub_codeDTO();
                                    }
                            %>
                            <tr>
                                <td>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, subCodeDTO.code)%>
                                </td>
                                <td>
                                    <%=UtilCharacter.getDataByLanguage(Language, subCodeDTO.descriptionBn,
                                            subCodeDTO.descriptionEn)%>
                                </td>

                                <%--Budget Info Goes here--%>

                                <%--Previous Year Data--%>
                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevYearBudgetDTOofSubCode.finalAmount
                                    )%>
                                </td>
                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevYearBudgetDTOofSubCode.revisedFinalAmount
                                    )%>
                                </td>

                                <td class="text-right" <%=BudgetUtils.markInvalidAmount(budgetDTOofSubCode.finalAmount)%>>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            budgetDTOofSubCode.finalAmount
                                    )%>
                                </td>
                                <%if (budgetType == BudgetTypeEnum.REVISED_BUDGET) {%>
                                <td class="text-right" <%=BudgetUtils.markInvalidAmount(budgetDTOofSubCode.revisedFinalAmount)%>>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            budgetDTOofSubCode.revisedFinalAmount
                                    )%>
                                </td>
                                <td>
                                    <%=budgetDTOofSubCode.revisedFinalComment%>
                                </td>
                                <%} else {%>
                                <td>
                                    <%=budgetDTOofSubCode.finalAmountComment%>
                                </td>
                                <%}%>
                            </tr>
                            <%}%> <%--end for economic sub group--%>
                            <%--Sub Total of Economic Codes--%>
                            <tr>
                                <td colspan="2" style="font-weight: bold" class="text-right">
                                    <%=LM.getText(LC.BUDGET_SUBTOTAL, loginDTO)%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevAmountSumByCode = budgetDTOsByCode.stream()
                                                                                     .map(dto -> dto.economicSubCodeId)
                                                                                     .map(prevYearBudgetDTOsBySubCodeId::get)
                                                                                     .mapToDouble(dto -> dto == null ? .0 : dto.finalAmount)
                                                                                     .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevAmountSumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevRevisedAmountSumByCode = budgetDTOsByCode.stream()
                                                                                            .map(dto -> dto.economicSubCodeId)
                                                                                            .map(prevYearBudgetDTOsBySubCodeId::get)
                                                                                            .mapToDouble(dto -> dto == null ? .0 : dto.revisedFinalAmount)
                                                                                            .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevRevisedAmountSumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double amountSumByCode = budgetDTOsByCode.stream()
                                                                                 .mapToDouble(dto -> dto.finalAmount)
                                                                                 .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            amountSumByCode
                                    )%>
                                </td>
                                <%if (budgetType == BudgetTypeEnum.REVISED_BUDGET) {%>
                                <td class="text-right">
                                    <%
                                        Double revisedAmountSumByCode = budgetDTOsByCode.stream()
                                                                                        .mapToDouble(dto -> dto.revisedFinalAmount)
                                                                                        .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            revisedAmountSumByCode
                                    )%>
                                </td>
                                <%}%>
                                <td>
                                    <%--Place horder on comment--%>
                                </td>
                            </tr>
                            <%}%> <%--end for economic code--%>
                            <%}%> <%--end for economic group--%>
                            <tr>
                                <td colspan="2" style="font-weight: bold" class="text-right">
                                    <%=LM.getText(LC.BUDGET_TOTAL, loginDTO)%> - <%=model.description%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevAmountSumByMapping = budgetDTOsByMapping.stream()
                                                                                           .map(dto -> dto.economicSubCodeId)
                                                                                           .map(prevYearBudgetDTOsBySubCodeId::get)
                                                                                           .mapToDouble(dto -> dto == null ? .0 : dto.finalAmount)
                                                                                           .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevAmountSumByMapping
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevRevisedAmountSumByMapping = budgetDTOsByMapping.stream()
                                                                                                  .map(dto -> dto.economicSubCodeId)
                                                                                                  .map(prevYearBudgetDTOsBySubCodeId::get)
                                                                                                  .mapToDouble(dto -> dto == null ? .0 : dto.revisedFinalAmount)
                                                                                                  .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevRevisedAmountSumByMapping
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double amountSumByMapping = budgetDTOsByMapping.stream()
                                                                                       .mapToDouble(dto -> dto.finalAmount)
                                                                                       .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            amountSumByMapping
                                    )%>
                                </td>
                                <%if (budgetType == BudgetTypeEnum.REVISED_BUDGET) {%>
                                <td class="text-right">
                                    <%
                                        Double revisedAmountSumByMapping = budgetDTOsByMapping.stream()
                                                                                              .mapToDouble(dto -> dto.revisedFinalAmount)
                                                                                              .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            revisedAmountSumByMapping
                                    )%>
                                </td>
                                <%}%>
                                <td>
                                    <%--Place horder on comment--%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%}%> <%--end for office--%>

                <%--Submit Button On Condition--%>
                <%
                    if (!isBudgetSubmitted) {
                %>

                <div id='button-div' class="row my-5">
                    <div class="col-12 text-right">
                        <button onclick="submitBudget();" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button">
                            <%=UtilCharacter.getDataByLanguage(Language, "দাখিল", "Submit")%>
                        </button>

                    </div>
                </div>

                <% } %>
            </div>
        </div>
    </div>
</div>

<script>
    function submitBudget() {
        const warning = '<%=BudgetUtils.getInvalidAmountWarning(
                budgetDTOS,
                budgetDTO -> budgetType == BudgetTypeEnum.BUDGET ? budgetDTO.finalAmount : budgetDTO.revisedFinalAmount,
                Language
        )%>'
            + '<br><br><%=isLanguageEnglish ? "Budget Submission will be closed after submission" : "জমা দেবার পর বাজেট জমাদান বন্ধ হয়ে যাবে।"%>';

        const confirmation = '<%=isLanguageEnglish ? "Are you sure?" : "আপনি কি নিশ্চিত?"%>';

        messageDialog(
            warning, confirmation, 'warning', true,
            '<%=StringUtils.getYesNo(Language,true)%>', '<%=StringUtils.getYesNo(Language,false)%>',
            confirmSubmit, () => {
            }
        );
    }

    async function confirmSubmit() {
        try {
            const url = 'BudgetSubmissionFinanceServlet?actionType=submitBudgetForFinance';
            const response = await fetch(url, {method: 'post'});
            const json = await response.json();

            if (!json.success) throw new Error(JSON.stringify(json));

            messageDialog(
                '<%=isLanguageEnglish ? "Successfully submitted." : "সফলভাবে দাখিল হয়েছে"%>', '',
                'success', false,
                '<%=LM.getText(LC.BUDGET_OKAY, loginDTO)%>', '',
                () => {
                }, () => {
                }
            );
            $("#button-div").hide();
        } catch (error) {
            console.error(error);
            messageDialog(
                '<%=isLanguageEnglish ? "Submission Failed!" : "দাখিল ব্যর্থ হয়েছে!"%>', '',
                'error', false,
                '<%=LM.getText(LC.BUDGET_OKAY, loginDTO)%>', '',
                () => {
                }, () => {
                }
            );
        }
    }

    function printDiv(divName) {
        let button = $("#button-div");
        if (button.length) {
            button.hide();
        }
        let printContents = document.getElementById(divName).innerHTML;
        let originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

        if (button.length) {
            $("#button-div").show();
        }
    }
</script>