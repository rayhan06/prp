<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="budget.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget_submission_info.Budget_submission_infoDTO" %>
<%@ page import="budget_submission_info.Budget_submission_infoDAO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDAO" %>

<%
    String context = request.getContextPath() + "/";

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.BUDGET_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    String formTitle = LM.getText(LC.BUDGET_SUBMISSION_FINANCE_FORM_NAME, loginDTO);

    BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);

    BudgetTypeEnum budgetTypeEnum = budgetInfo.budgetTypeEnum;
    Budget_submission_infoDTO budgetSubmissionInfoDTO = null;
    String errorMessage = null;

    boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
    if (isCodeSelectionDone) {
        budgetSubmissionInfoDTO = Budget_submission_infoDAO.getInstance().getByBudgetTypeAndBudgetOffice(
                budgetSelectionInfoDTO.iD, budgetTypeEnum.getValue(), BudgetUtils.FINANCE_SECTION_OFFICE_ID
        );
    } else {
        budgetSelectionInfoDTO = new BudgetSelectionInfoDTO();
        errorMessage = UtilCharacter.getDataByLanguage(
                Language,
                "এই অর্থ বছরের বাজেটের কোড বাছাইকরণ প্রক্রিয়া এখনো সম্পন্ন হয়নি!",
                "Budget code selection process has not yet been completed for this year!"
        );
    }

    boolean isBudgetSubmitted = budgetSubmissionInfoDTO != null
            && budgetSubmissionInfoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue();

    String economicYearText = StringUtils.convertBanglaIfLanguageIsBangla(Language, budgetInfo.economicYear);
    String previousEconomicYearText = StringUtils.convertBanglaIfLanguageIsBangla(
            Language,
            BudgetInfo.getEconomicYear(budgetInfo.beginYear - 1)
    );
%>

<style>
    /* dont remove this class */
    .template-row {
        display: none;
    }

    tfoot td {
        text-align: right;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal"
              action="BudgetSubmissionFinanceServlet?actionType=insertBudgetForFinance"
              id="budget-form" name="budgetform" method="POST" enctype="multipart/form-data">

            <%if (errorMessage != null) {%>
            <div class="my-4">
                <h5 class="text-center" style="color: red">
                    <%=errorMessage%>
                </h5>
            </div>
            <%} else {%>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row">
                        <div class="offset-3 col-md-6 text-center">
                            <img width="15%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>

                <%--be careful while removing hidden inputs--%>
                <input type="hidden" name="budgetType" id="budgetType" value="<%=budgetTypeEnum.getValue()%>">
                <input type='hidden' class='form-control' name='budgetSelectionInfoId' id='budgetSelectionInfoId'
                       value=<%=budgetSelectionInfoDTO.iD%>>
                <input type="hidden" name="budgetModels" id="budgetModels">

                <div class="row mt-5">
                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="budgetInstitutionalGroup">
                            <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                        </label>
                        <select id="budgetInstitutionalGroup" class='form-control rounded w-100 shadow-sm'
                                onchange="institutionalGroupChanged(this);">
                            <%=Budget_institutional_groupRepository.getInstance().buildOptions(Language, 0L, true)%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="budgetCat">
                            <%=LM.getText(LC.BUDGET_BUDGET_TYPE, loginDTO)%>
                        </label>
                        <select id="budgetCat" class='form-control rounded w-100 shadow-sm'
                                onchange="budgetCatChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="budgetOffice">
                            <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                        </label>
                        <select id="budgetOffice" class='form-control rounded w-100 shadow-sm'
                                onchange="budgetOfficeChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="budgetOperationId">
                            <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                        </label>
                        <select id="budgetOperationId" class='form-control rounded w-100 shadow-sm'
                                onchange="budgetOperationChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="economicGroup">
                            <%=LM.getText(LC.BUDGET_ECONOMIC_GROUP, loginDTO)%>
                        </label>
                        <select id="economicGroup" class='form-control rounded w-100 shadow-sm'
                                onchange="economicGroupChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="economicCodes">
                            <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                        </label>
                        <select id="economicCodes" class='form-control rounded w-100 shadow-sm'
                                onchange="economicCodesChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>
                </div>

                <div class="mt-5">
                    <h5 class="table-title text-right">
                        (<%=LM.getText(LC.BUDGET_MONEY_INPUT_UNIT_HEADER, loginDTO)%>)
                    </h5>
                   <div class="w-100">
                       <table class="table text-nowrap table-striped table-bordered" id="budget-view-table">
                           <thead>
                           <tr>
                               <th class="row-data-code">
                                   <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                               </th>
                               <th class="row-data-name">
                                   <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                               </th>
                               <th class="row-data-budget">
                                   <%=LM.getText(LC.BUDGET_BUDGET_AMOUNT, loginDTO)%>
                                   <br>
                                   <%=previousEconomicYearText%>
                               </th>
                               <th class="row-data-budget">
                                   <%=CatRepository.getInstance().getText(Language, "budget_type", BudgetTypeEnum.REVISED_BUDGET.getValue())%>
                                   <br>
                                   <%=previousEconomicYearText%>
                               </th>
                               <th class="row-data-budget">
                                   <%=BudgetTypeEnum.BUDGET.getText(Language)%>
                                   <br>
                                   <%=economicYearText%>
                               </th>
                               <%if (budgetTypeEnum == BudgetTypeEnum.REVISED_BUDGET) {%>
                               <th class="row-data-budget">
                                   <%=BudgetTypeEnum.REVISED_BUDGET.getText(Language)%>
                                   <br>
                                   <%=economicYearText%>
                               </th>
                               <%}%>
                               <th class="row-data-budget">
                                   <%=LM.getText(LC.BUDGET_COMMENT, loginDTO)%>
                               </th>
                           </tr>
                           </thead>
                           <tbody></tbody>

                           <%--dont put template row in tbody--%>
                           <tr class="template-row">
                               <td class="row-data-code"></td>
                               <td class="row-data-name"></td>
                               <td class="row-data-final-budget-prev text-right"></td>
                               <td class="row-data-final-revised-budget-prev text-right"></td>
                               <td class="row-data-final-budget text-right"></td>
                               <td class="row-data-final-revised-budget text-right"></td>
                               <td class="row-data-final-comment"></td>
                           </tr>
                       </table>
                   </div>
                </div>
                <div class="row">
                    <div class="col-12 form-actions mt-3 text-right">
                        <button class="btn btn-sm shadow text-white btn-border-radius border-0" type="button"
                                style="background: #4db2d1;"
                                onclick="location.href='BudgetSubmissionFinanceServlet?actionType=getPreviewPage'">
                            <%=LM.getText(LC.BUDGET_PREVIEW, loginDTO)%>
                        </button>

                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn mx-2"
                                type="button"
                                onclick="resetPage();">
                            <%=LM.getText(LC.BUDGET_ADD_BUDGET_CANCEL_BUTTON, loginDTO)%>
                        </button>

                        <%if (!isBudgetSubmitted) {%>
                        <button class="btn-sm shadow text-white border-0 submit-btn" type="submit">
                            <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                        <%}%>
                    </div>
                </div>
            </div>
            <%}%>
        </form>
    </div>
</div>

<%if (errorMessage == null) {%>
<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const budgetModelMap = new Map();
    const isBudgetSubmitted = <%=isBudgetSubmitted%>;
    const REVISED_BUDGET = '<%=BudgetTypeEnum.REVISED_BUDGET.getValue()%>';

    function resetPage() {
        document.getElementById('budgetInstitutionalGroup').value = '';
        clearSelects(0);
        clearTable();
    }

    $(() => {
        clearTable();

        $('#budget-form').submit(async (event) => {
            event.preventDefault();

            if (isBudgetSubmitted) return;

            const budgetModels = Array.from(budgetModelMap.values());
            const budgetType = document.getElementById('budgetType').value;

            for (let budgetModel of budgetModels) {
                if (budgetType === REVISED_BUDGET) {
                    budgetModel.revisedFinalBudgetAmount = document.getElementById('revised_budget_' + budgetModel.id).value;
                    budgetModel.revisedFinalBudgetComment = document.getElementById('revised_comment_' + budgetModel.id).value;
                } else {
                    budgetModel.finalBudgetAmount = document.getElementById('budget_' + budgetModel.id).value;
                    budgetModel.finalBudgetComment = document.getElementById('comment_' + budgetModel.id).value;
                }
                budgetModelMap.set(Number(budgetModel.id), budgetModel);
            }

            $('#budgetModels').val(JSON.stringify(Array.from(budgetModelMap.values())));

            const formElement = event.target;
            const formData = new FormData(formElement);
            const url = formElement.action;
            const searchParam = new URLSearchParams(formData);

            try {
                const response = await fetch(url, {
                    method: 'post',
                    body: searchParam
                });
                const json = await response.json();
                if (!json.success) throw new Error(json);

                $('#toast_message').css('background-color', '#04c73c');
                showToast(
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVED,"Bangla")%>',
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVED,"English")%>'
                );
            } catch (error) {
                console.error(error);
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVING_FAILED,"Bangla")%>',
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVING_FAILED,"English")%>'
                );
            }
        });
    });

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "BudgetServlet");
    }

    function clearSelects(startIndex) {
        const selectIds = ['budgetCat', 'budgetOffice', 'budgetOperationId', 'economicGroup', 'economicCodes'];
        for (let i = startIndex; i < selectIds.length; i++) {
            const selectElement = document.getElementById(selectIds[i]);
            selectElement.innerHTML = '';
        }
    }

    function getBudgetInputElement(id, value, onKeyUp) {
        const inputElement = document.createElement('input');
        inputElement.classList.add('form-control');
        inputElement.type = 'text';
        inputElement.id = id;
        inputElement.value = value;
        inputElement.onkeydown = keyDownEvent;
        if (onKeyUp) inputElement.onkeyup = onKeyUp;
        return inputElement;
    }

    function getBudgetCommentElement(id, value) {
        const textAreaElement = document.createElement('textarea');
        textAreaElement.classList.add('form-control');
        textAreaElement.id = id;
        textAreaElement.rows = 4;
        textAreaElement.cols = 30;
        textAreaElement.style.resize = 'none';
        textAreaElement.style.width = 'auto';
        textAreaElement.append(value);
        return textAreaElement;
    }

    function addDataInTable(tableId, jsonData) {
        const table = document.getElementById(tableId);
        const budgetType = document.getElementById('budgetType').value;

        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.id = jsonData.economicSubCode;
        templateRow.classList.remove('template-row');

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-code').innerText = jsonData.code;
        templateRow.querySelector('td.row-data-name').innerText = jsonData.name;
        const finalBudgetPrev = templateRow.querySelector('td.row-data-final-budget-prev');
        const finalRevisedBudgetPrev = templateRow.querySelector('td.row-data-final-revised-budget-prev');

        const finalBudget = templateRow.querySelector('td.row-data-final-budget');
        const finalRevisedBudget = templateRow.querySelector('td.row-data-final-revised-budget');
        const finalComment = templateRow.querySelector('td.row-data-final-comment');

        if (budgetType === REVISED_BUDGET) {
            if (!isBudgetSubmitted) {
                finalRevisedBudget.append(
                    getBudgetInputElement(
                        'revised_budget_' + jsonData.id,
                        jsonData.revisedFinalBudgetAmount,
                        function () {sumInputElementColumn(this, 2);}
                    )
                );
                finalComment.append(
                    getBudgetCommentElement('revised_comment_' + jsonData.id, jsonData.revisedFinalBudgetComment)
                );
            } else {
                finalRevisedBudget.innerText = jsonData.revisedFinalBudgetAmount;
                finalComment.innerText = jsonData.revisedFinalBudgetComment;
            }
            finalBudget.innerText = jsonData.finalBudgetAmount;
        } else {
            finalRevisedBudget.remove();
            if (!isBudgetSubmitted) {
                finalBudget.append(
                    getBudgetInputElement(
                        'budget_' + jsonData.id,
                        jsonData.finalBudgetAmount,
                        function () {sumInputElementColumn(this, 2);}
                    )
                );
                finalComment.append(
                    getBudgetCommentElement('comment_' + jsonData.id, jsonData.finalBudgetComment)
                );
            } else {
                finalBudget.innerText = jsonData.finalBudgetAmount;
                finalComment.innerText = jsonData.finalBudgetComment;
            }
        }
        if (jsonData.previousAmounts.length != 0) {
            finalBudgetPrev.innerText = jsonData.previousAmounts[0].budget;
            finalRevisedBudgetPrev.innerText = jsonData.previousAmounts[0].revisedBudget;
        } else {
            finalBudgetPrev.innerText = "0.0";
            finalRevisedBudgetPrev.innerText = "0.0";
        }
        const tableBody = table.querySelector('tbody');
        tableBody.append(templateRow);
    }

    function clearTable(optionalEmptyTableText) {
        budgetModelMap.clear();
        let noDataText = '<%=isLanguageEnglish? "No data found!" : "কোনো তথ্য পাওয়া যায় নি!"%>';
        if(optionalEmptyTableText) noDataText = optionalEmptyTableText;

        const table = document.getElementById("budget-view-table");
        table.querySelectorAll('tfoot').forEach(tfoot => tfoot.remove());
        table.querySelector('tbody').innerHTML = '<tr><td colspan="100%" class="text-center">' + noDataText + '</td></tr>';
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearSelects(0);
        clearTable();
        if (selectedInstitutionalGroupId === '') return;

        const url = 'Budget_mappingServlet?actionType=getBudgetCatList&budget_instituitional_group_id='
            + selectedInstitutionalGroupId;
        const response = await fetch(url);
        document.getElementById('budgetCat').innerHTML = await response.text();
    }

    async function budgetCatChanged(selectElement) {
        const selectedBudgetCat = selectElement.value;
        clearSelects(1);
        clearTable();
        if (selectedBudgetCat === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=true&budget_instituitional_group_id='
            + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat;
        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
    }

    async function budgetOfficeChanged(selectElement) {
        const selectedBudgetOffice = selectElement.value;
        clearSelects(2);
        clearTable();
        if (selectedBudgetOffice === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;
        const selectedBudgetCat = document.getElementById('budgetCat').value;

        const url = 'Budget_mappingServlet?actionType=getOperationCodeList&withCode=true&budget_instituitional_group_id='
            + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat
            + '&budget_office_id=' + selectedBudgetOffice;
        const response = await fetch(url);
        document.getElementById('budgetOperationId').innerHTML = await response.text();
    }

    async function fetchAndShowBudgetModel(url) {
        const response = await fetch(url);

        const responseJson = await response.json();

        if (responseJson.hasOfficeSubmittedBudget === false) {
            initBudgetTable(
                'budget-view-table',
                '<%=isLanguageEnglish? "This office has not submitted budget yet!" : "এই অফিস এখনো বাজেট জমা দেয়নি"%>'
            );
            return;
        }

        initBudgetTable('budget-view-table');
        const budgetModels = responseJson.subCodeModels;
        const hasBudgetData = Array.isArray(budgetModels) && (budgetModels.length > 0);
        if (hasBudgetData) {
            for (const budgetModel of budgetModels) {
                addDataInTable('budget-view-table', budgetModel);
                budgetModelMap.set(
                    Number(budgetModel.id),
                    budgetModel
                );
            }
            // columns [EconomicSubCode Description PrevBudget PrevRevisedBudget Budget RevisedBudget(on condition)]
            const colIndicesToSum = [2, 3, 4];
            const budgetType = document.getElementById('budgetType').value;
            if (budgetType === REVISED_BUDGET) colIndicesToSum.push(5);
            const totalTitleColSpan = 2;
            const totalTitle = '<%=LM.getText(LC.BUDGET_SUBTOTAL,loginDTO)%>';
            setupTotalRow('budget-view-table', totalTitle, totalTitleColSpan, colIndicesToSum, undefined, undefined, 2);
        }
    }

    async function budgetOperationChanged() {
        clearSelects(3);
        clearTable();

        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;

        const selectedOperationId = document.getElementById('budgetOperationId').value;
        if (selectedOperationId === '') return;

        const selectedBudgetOffice = document.getElementById('budgetOffice').value;

        const url = 'BudgetServlet?actionType=buildEconomicGroups&budgetSelectionInfoId='
            + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationId;

        const response = await fetch(url);
        document.getElementById('economicGroup').innerHTML = await response.text();

        const budgetModelUrl =
            'BudgetSubmissionFinanceServlet?actionType=getBudgetModels&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationId;
        await fetchAndShowBudgetModel(budgetModelUrl);
    }

    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroup = selectElement.value;
        clearSelects(4);
        clearTable();

        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationId = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOffice').value;

        if (selectedEconomicGroup !== '') {
            const url = 'BudgetServlet?actionType=buildEconomicCodes&budgetSelectionInfoId='
                + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationId
                + '&economicGroupId=' + selectedEconomicGroup;
            const response = await fetch(url);
            document.getElementById('economicCodes').innerHTML = await response.text();
        }

        const budgetModelUrl =
            'BudgetSubmissionFinanceServlet?actionType=getBudgetModels&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationId
            + '&economicGroupId=' + selectedEconomicGroup;
        await fetchAndShowBudgetModel(budgetModelUrl);
    }

    function initBudgetTable(tableId, initText) {
        const table = document.getElementById(tableId);
        initText = initText ? '<tr><td colspan="100%" class="text-center">' + initText + '</td></tr>'
                            : '';
        table.querySelector('tbody').innerHTML = initText;
        table.querySelectorAll('tfoot').forEach(tfoot => tfoot.remove());
    }

    async function economicCodesChanged(selectElement) {
        const selectedEconomicCode = selectElement.value;
        clearTable();
        const selectedEconomicGroup = document.getElementById('economicGroup').value;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOffice').value;
        const url =
            'BudgetSubmissionFinanceServlet?actionType=getBudgetModels&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
            + '&economicGroupId=' + selectedEconomicGroup + '&economicCodeId=' + selectedEconomicCode;

        await fetchAndShowBudgetModel(url);
    }

    function keyDownEvent(e) {
        console.log(e);
        let isvalid = inputValidationForFloatValue(e, $(this), 3, null);
        return true == isvalid;
    }
</script>
<%}%>






