<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="util.RecordNavigator" %>
<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="employee_assign.EmployeeAssignDTO" %>
<%@ page import="test_lib.util.Pair" %>
<%@ page import="java.util.*" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@page pageEncoding="utf-8" %>
<style>
    .btn:hover {
        color: #fff;
    }
</style>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.EMPLOYEE_HISTORY_EDIT_LANGUAGE, loginDTO);
    boolean isBanglaLanguage = "Bangla".equalsIgnoreCase(Language);
%>

<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<div>
    <table id="tableData" class="table table-bordered table-striped" onchange="filter_selected_employee(this)">
        <thead>
        <%--                    <tr>--%>
        <%--                        <th><b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%>--%>
        <%--                        </b></th>--%>
        <%--                        <th><b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%>--%>
        <%--                        </b></th>--%>
        <%--                        <th><b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%>--%>
        <%--                        </b></th>--%>
        <%--                        <th></th>--%>
        <%--                    </tr>--%>
        </thead>
        <tbody>
        <%
            List<OfficeUnitOrganograms> data = (List<OfficeUnitOrganograms>) session.getAttribute("data");
            if (data != null && data.size() > 0) {
                for (OfficeUnitOrganograms dto : data) {
        %>

        <%-- IMPORTANT: row id format-> <record id>_<officeunit id>_<organogram id>--%>
        <tr id="<%=dto.id%>_<%=dto.designation_eng%>">
            <td>
                <%=dto.id %>
            </td>
            <td>
                <%=dto.designation_bng %>
            </td>
            <td>
                <%=dto.designation_eng%>
            </td>
            <td style="width: 130px">
                <button class='btn btn-sm shadow btn-border-radius add-btn text-white d-flex justify-content-between align-items-center'
                        type="button" onclick="use_row_button(this);">
                    <i class="fa fa-plus"></i><%=LM.getText(LC.HM_ADD, loginDTO)%>
                </button>
            </td>
        </tr>

        <%
                }
            }
        %>
        </tbody>
    </table>
</div>