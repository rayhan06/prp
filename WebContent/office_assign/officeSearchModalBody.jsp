<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="office_units.Office_unitsRepository" %>

<div id="drop_down" class="row">
    <div class="col-md-12">
        <div id="ajax-content">
            <div class="search_by_div" id="by_organogram_div">
                <form class="kt-form">
                    <div class="kt-form" id="bigform" name="bigform" enctype="multipart/form-data">
                        <div id="office_layer_div_1" class="form-group col-12" style="margin-top: 10px">
                            <label>
                                <%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                                <span class="required"> * </span>
                            </label>
                            <div>
                                <select class='form-control formRequired office_leyer' required data-label="Office"
                                        required="required"
                                        name='office_layer_1' id='office_layer_1' tag='pb_html'
                                        onchange="layerChange(1)">
                                    <%=Office_unitsRepository.getInstance().buildOptionsByParentId(Language, 0L,null)%>
                                </select>
                            </div>
                        </div>


                        <div id="office_layer_div_2" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired office_leyer' name='office_layer_2'
                                        id='office_layer_2'
                                        tag='pb_html' onchange="layerChange(2)"></select>
                            </div>
                        </div>

                        <div id="office_layer_div_3" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired office_leyer' name='office_layer_3'
                                        id='office_layer_3'
                                        tag='pb_html' onchange="layerChange(3)"></select>
                            </div>
                        </div>

                        <div id="office_layer_div_4" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired' name='office_layer_4' id='office_layer_4'
                                        tag='pb_html' onchange="layerChange(4)"></select>
                            </div>
                        </div>

                        <div id="office_layer_div_5" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired' name='office_layer_5' id='office_layer_5'
                                        tag='pb_html' onchange="layerChange(5)"></select>
                            </div>
                        </div>

                        <div id="office_layer_div_6" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired' name='office_layer_6' id='office_layer_6'
                                        tag='pb_html' onchange="layerChange(6)"></select>
                            </div>
                        </div>

                        <div id="office_layer_div_7" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired' name='office_layer_7' id='office_layer_7'
                                        tag='pb_html' onchange="layerChange(7)"></select>
                            </div>
                        </div>

                        <div id="office_layer_div_8" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired' name='office_layer_8' id='office_layer_8'
                                        tag='pb_html' onchange="layerChange(8)"></select>
                            </div>
                        </div>

                        <div id="office_layer_div_9" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired' name='office_layer_9' id='office_layer_9'
                                        tag='pb_html' onchange="layerChange(9)"></select>
                            </div>
                        </div>

                        <div id="office_layer_div_10" class="form-group col-12" style="display: none">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control formRequired' name='office_layer_10' id='office_layer_10'
                                        tag='pb_html' onchange="layerChange(10)"></select>
                            </div>
                        </div>

                    </div>

                </form>
            </div>

            <hr/>

            <div class="" id="search_table">
            </div>
        </div>
    </div>
</div>