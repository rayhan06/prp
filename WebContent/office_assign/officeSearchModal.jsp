<!-- Modal -->
<%--
  Author: Latif Siddiq
  USAGE: see promotion_history/promotion_historyEditBody.jsp
--%>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_office_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h3 class="caption">
                    <%=LM.getText(LC.OFFICE_SEARCH_MODAL_FIND_DESIGNATION, userDTO)%>
                </h3>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body">
                <%@include file="officeSearchModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer">
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius" data-dismiss="modal">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    language = '<%=Language%>';

    function clearOfficeLayers() {
        // reset layer 1 to ""
        $('#office_layer_1').val("");
        // clear search table
        document.getElementById('search_table').innerHTML = "";
        // hide and clear layer 2 to 10
        for (let i = 2; i <= 10; i++) {
            document.getElementById("office_layer_div_" + i).style.display = 'none';
            document.getElementById("office_layer_" + i).innerHTML = '';
        }
    }

    function clearSearchByInfo() {
        // clear search table
        document.getElementById('search_table').innerHTML = "";
    }

    // modal on load event
    $('#search_office_modal').on('show.bs.modal', function () {
        clearOfficeLayers();
        clearSearchByInfo();
    });

    $('input:radio[name="search_by"]').change(function () {
        clearOfficeLayers();
        clearSearchByInfo();
        $(".search_by_div").hide();
        let selectedSearchByType = $(this).val();
        $('#' + selectedSearchByType + "_div").show();
    });

    // function hide_selected_employees(filter_from_table_name, selected_employees_map_or_set) {
    //     console.log("filter function");
    //     console.log("selected_employees_map_or_set");
    //     console.log(selected_employees_map_or_set);
    //     let table_obj = document.getElementById(filter_from_table_name);
    //     let n_row = table_obj.rows.length;
    //     for (let i = 0; i < n_row; i++) {
    //         let row = table_obj.rows[i];
    //         let employee_record_id = row.id.split('_')[0];
    //         if (selected_employees_map_or_set.has(employee_record_id)) {
    //             hide(row);
    //         } else {
    //             un_hide(row);
    //         }
    //     }
    // }

    function showInSearchTable(data) {
        document.getElementById('search_table').innerHTML = '';
        document.getElementById('search_table').innerHTML = data;

        // console.log('AJAX Fetched Data:\n' + data);

        // // TODO: filter result here
        // if (table_name_to_collcetion_map === undefined || table_name_to_collcetion_map === null) {
        //     console.log('table_name_to_collcetion_map not set!');
        // } else {
        //     hide_selected_employees(
        //         'tableData', table_name_to_collcetion_map.get(modal_button_dest_table).info_map
        //     );
        // }
    }

    function getPosts(officeId) {
        let url = "Promotion_historyServlet?actionType=getDesignationAddTable&office_unit_id=" + officeId;
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            success: function (data) {
                if (this.responseText !== '') {
                    showInSearchTable(data);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }


    function layerChange(layer) {
        officeName = $('#office_layer_' + layer + ' option:selected').text();
        let layerid = "#office_layer_" + layer;
        let nextLayer = layer + 1;
        for (let i = nextLayer; i < 10; i++) {
            document.getElementById("office_layer_div_" + i).style.display = 'none';
            document.getElementById("office_layer_" + i).innerHTML = '';
        }
        lastSelectedOfficeId = $(layerid).val();
        if (lastSelectedOfficeId === '') {
            clearSearchByInfo()
            return
        }
        let url = "Office_unitsServlet?actionType=ajax_office&parent_id=" + lastSelectedOfficeId + "&language=" + language;

        console.log("url : " + url);

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                if (fetchedData) {
                    document.getElementById("office_layer_div_" + nextLayer).style.display = 'block';
                    document.getElementById("office_layer_" + nextLayer).innerHTML = fetchedData;
                }
                if (!!isShowTable && isShowTable === 1)
                    getPosts(lastSelectedOfficeId);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function use_row_button(button) {

        add_containing_row_to_dest_table(button);

        $('#search_office_modal').modal('hide');


    }

    function add_containing_row_to_dest_table(button) {

        let this_row = button.parentNode.parentNode;
        officeId = lastSelectedOfficeId;
        officeUnitOrganogramId = this_row.id.split('_')[0];
        officeUnitOrganogramName = this_row.id.split('_')[1];

        triggerOfficeUnitOrganogramId();
        //console.log(this_row.id);

    }
</script>

<%-- Author: Latif Siddiq --%>