<%@page import="util.HttpRequestUtils"%>
<%@page import="language.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="row mx-0 col-md-12 px-0">

     <input type='hidden' class='form-control'
                       name='addAllChildren'
                       id='addAllChildren_hidden_<%=childPrefix%>'
                       value='false' tag='pb_html'/>
                       
     <input type='hidden' class='form-control'
                       name='assetAssignee.parentCatgories'
                       id='parentCatgories_hidden_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.parentCatgories%>' tag='pb_html'/>
                       
     <input type='hidden' class='form-control'
                       name='assetAssignee.modelName'
                       id='modelName_hidden_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.modelName%>' tag='pb_html'/>
                       
                       
                       
     <div class="row col-md-6 mx-0 form-group" type="thTdSoft">
    	 <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_KEY, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control' readonly
                       name='assetAssignee.softwareKey'
                       id='softwareKey_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.softwareKey%>' tag='pb_html'/>
            </div>
        </div>
    </div>
    
     <div class="row col-md-6 mx-0 form-group" type="thTdParent" id = "parentDiv_<%=childPrefix%>" tag='pb_html'>
    	 <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("English")?"Attched To":"সংযুক্ত"%>
            </label>
            <div class="col-md-8">
                <select class='form-control' name='assetAssignee.parentAssigneeId' id='parentAssigneeId_select_<%=childPrefix%>' tag='pb_html'>
                  <option value = '<%=assetAssigneeDTO.parentAssigneeId%>'></option>
             	</select>
            </div>
        </div>
    </div>
                       
	<div class="row col-md-6 mx-0 form-group" type="thTdLaptopCpuProcessor">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_PROCESSOR, loginDTO)%>
            </label>
            <div class="col-md-4">
                <select class='form-control' name='assetAssignee.processorCat' id='processorCat_select_<%=childPrefix%>' tag='pb_html'>
                  <%
                      Options = CatRepository.getOptions(Language, "processor", assetAssigneeDTO.processorCat);
                  %>
                  <%=Options%>
             	</select>
            </div>
            <div class="col-md-4">
                <select class='form-control' name='assetAssignee.processorGenCat' id='processorGenCat_select_<%=childPrefix%>' tag='pb_html'>
                  <%
                      Options = CatRepository.getOptions(Language, "processor_gen", assetAssigneeDTO.processorGenCat);
                  %>
                  <%=Options%>
             	</select>
            </div>
        </div>
    </div>
   
    <div class="row col-md-6 mx-0 form-group" type="thTdLaptopCpuRam">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_RAM, loginDTO)%>
            </label>
            <div class="col-md-4">
                <select class='form-control' name='assetAssignee.ramCat' id='ramCat_select_<%=childPrefix%>' tag='pb_html'>
                  <%
                      Options = CatRepository.getOptions(Language, "ram", assetAssigneeDTO.ramCat);
                  %>
                  <%=Options%>
             	</select>
            </div>
            <div class="col-md-4">
                <select class='form-control' name='assetAssignee.ramSizeCat' id='ramSizeCat_select_<%=childPrefix%>' tag='pb_html'>
                  <%
                      Options = CatRepository.getOptions(Language, "ram_size", assetAssigneeDTO.ramSizeCat);
                  %>
                  <%=Options%>
             	</select>
            </div>
        </div>
    </div>
    <div class="row col-md-6 mx-0 form-group" type="thTdLaptopCpuHd">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_HDNAME, loginDTO)%>
            </label>
            <div class="col-md-4">
            	<select class='form-control' name='assetAssignee.hdCat' id='hdCat_select_<%=childPrefix%>' tag='pb_html'>
                  <%
                      Options = CatRepository.getOptions(Language, "hd", assetAssigneeDTO.hdCat);
                  %>
                  <%=Options%>
             	</select>               
            </div>
            <div class="col-md-4">
            	<select class='form-control' name='assetAssignee.hdSizeCat' id='hdSizeCat_select_<%=childPrefix%>' tag='pb_html'>
                  <%
                      Options = CatRepository.getOptions(Language, "hd_size", assetAssigneeDTO.hdSizeCat);
                  %>
                  <%=Options%>
             	</select>                
            </div>
        </div>
    </div>

    <div class="row col-md-6 mx-0 form-group" type="thTdLaptopCpuAp">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_MAC, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.mac'
                       id='mac_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.mac%>' tag='pb_html'/>
            </div>
        </div>
    </div>
    
    <div class="row col-md-6 mx-0 form-group" type="thTdCpuNet">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_MK_BOX_NUMBER, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.mkBoxNumber'
                       id='mkBoxNumber_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.mkBoxNumber%>' tag='pb_html'/>
            </div>
        </div>
    </div>
    
    <div class="row col-md-6 mx-0 form-group" type="thTdCpuNet">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_ACTIVE_DIRECTORY_ID, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.activeDirectoryId'
                       id='mac_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.activeDirectoryId%>' tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="row col-md-6 mx-0 form-group" type="thTdLaptopCpuApPrinter">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_IP, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.ip'
                       id='ip_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.ip%>' tag='pb_html'/>
            </div>
        </div>
    </div>
   
    <div class="row col-md-6 mx-0 form-group" type="thTdCpuApNet">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_IDF_NUMBER, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.idfNumber'
                       id='idfNumber_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.idfNumber%>'
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="row col-md-6 mx-0 form-group" type="thTdAp">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_HOST_NAME, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.host'
                       id='host_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.host%>' tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="row col-md-6 mx-0 form-group" type="thTdAp">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_LOCATION, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.location'
                       id='location_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.location%>'
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="row col-md-6 mx-0 form-group" type="thTdAp">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_AP_LICENSE_FOR_USE, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.apLicenceForUse'
                       id='apLicenceForUse_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.apLicenceForUse%>'
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="row col-md-6 mx-0 form-group" type="thTdAp">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_PORT, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.port'
                       id='port_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.port%>' tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="row col-md-6 mx-0 form-group" type="thTdCpu">
        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
            <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("English")?"PC Name":"পিসির নাম" %>

            </label>
            <div class="col-md-8">
                <input type='text' class='form-control'
                       name='assetAssignee.pcName'
                       id='pcName_text_<%=childPrefix%>'
                       value='<%=assetAssigneeDTO.pcName%>' tag='pb_html'/>
            </div>
        </div>
    </div>
</div>