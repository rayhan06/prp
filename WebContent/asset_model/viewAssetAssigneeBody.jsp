<%@ page import="java.util.*"%>
<%@page import="asset_model.*"%>
<%@page import="asset_category.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="user.*"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="util.*"%>
<%@page import="pb.*"%>
<%@page import="workflow.*"%>
<%@page import="office_unit_organograms.*"%>
<%@page import="office_units.*"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%

AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();

LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
String Language = LM.getLanguage(loginDTO);
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
Asset_modelDAO asset_modelDAO = new Asset_modelDAO();
long assetAssigneeId = Long.parseLong(request.getParameter("id"));
AssetAssigneeDTO assetAssigneeDTO = (AssetAssigneeDTO)assetAssigneeDAO.getDTOByID(assetAssigneeId);
Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(assetAssigneeDTO.assetModelId, false);
boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
%>

<style>
    .rounded-border {
        border-radius: 8px!important;
    }

    .bg-primary-custom {
        background: #EDF8FC!important;
        color: #727476 !important;
    }

    .section-title {
        background: #EDF8FC!important;
        border: 4px solid #CDECF7!important;
        border-radius: 8px!important;
        color: #727476 !important;
        text-transform: capitalize!important;
    }

</style>

<div class="ml-auto mr-3">

	<button type="button" class="btn" id='printer2'
	           onclick="location.href='Asset_modelServlet?actionType=searchAssetAssignee'">
	       <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
	   </button>
    <button type="button" class="btn" id='printer2'
            onclick="printAnyDiv('assetDiv')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
<!--         <div class="kt-portlet__head"> -->
<!--             <div class="kt-portlet__head-label"> -->
<!--                 <h3 class="kt-portlet__head-title prp-page-title"> -->
<!--                     <i class="fa fa-gift"></i>&nbsp; -->
<%--                     <%=asset_modelDTO.nameEn%> --%>
<!--                 </h3> -->
<!--             </div> -->
<!--         </div> -->
        <div class="kt-portlet__body form-body">
        	<div class="mt-5" id = 'assetDiv'>
		        <div class="my-3 text-center">
					<h3 class="font-weight-bold"><%=WorkflowController.getNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, Language)%></h3>			
					<h4 class="font-weight-bold"><%=WorkflowController.getOfficeTypeNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, Language)%></h4>
					<h5 class="font-weight-bold"><%=WorkflowController.getWingNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, Language)%></h5>
				</div>
        		<input type = "hidden"  id = "cat_0" value = "<%=assetAssigneeDTO.assetCategoryType %>">
				<div class=" div_border attachement-div" id = "AssetAssignee_0">
				
					<%@include file="../asset_model/viewParticularAsset.jsp"%>
				</div>

				<div class="row w-100 mx-0">
					<div class="col-md-12 form-actions text-center mt-0 pr-0">
					 <%
		            if(userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE || userDTO.roleID == SessionConstants.ADMIN_ROLE)
		            {
		            %>
						<%@include file="../asset_model/assignmentFunctionalityButton.jsp"%>
					<%
           			 }
					%>
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
</div>
<input type = hidden id = "childTableStartingID" value = '1' />
<script type="text/javascript">
<%@include file="../asset_model/assetViewJs.jsp"%>
<%@include file="../asset_model/assignmentStatusFunctionality.jsp"%>
</script>

