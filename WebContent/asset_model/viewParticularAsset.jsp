<%@page import="asset_model.AssetAssigneeDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="common.QRCodeUtil" %>
      
<%
{
String qrContent = assetAssigneeDAO.getQrContents(assetAssigneeDTO);
String QR = QRCodeUtil.getQrCodeAsBase64(qrContent);
%>
<div>
	<div class="my-3">
		<img style="height: 200px;" src="<%= QR %>"/>
         <h4 class="section-title d-inline-block px-4 py-2"
             style="background:#e0f7fa"><%=CommonDAO.getName(Language, "asset_category", asset_modelDTO.assetCategoryType)%></h4>
         
	</div>
	<div class="row">
		<div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_MODEL, loginDTO)%></b>
                 </label>
                 <div class="col-md-8 form-control">
                    <%=asset_modelDTO.nameEn%>
                 </div>
             </div>
         </div>
         <div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_STATUS, loginDTO)%></b>
                 </label>
                 <div class="col-md-8 form-control">
                    <%=CatDAO.getName(Language, "assignment_status", assetAssigneeDTO.assignmentStatus)%>
                 </div>
             </div>
         </div>
		<%
		if(asset_modelDTO.brandType != -1)
		{
		%>
		<div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_BRANDTYPE, loginDTO)%></b>
                 </label>
                 <div class="col-md-8 form-control">
                     <%=CommonDAO.getName(Language, "brand", asset_modelDTO.brandType)%>
                 </div>
             </div>
         </div>
         <%
		}
         %>
         <%
		if(assetAssigneeDTO.pcName != null && !assetAssigneeDTO.pcName.equalsIgnoreCase(""))
		{
		%>
		<div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=Language.equalsIgnoreCase("English")?"PC Name":"পিসির নাম" %>
                 </b>
                 </label>
                 <div class="col-md-8 form-control">
                     <%=assetAssigneeDTO.pcName%>
                 </div>
             </div>
         </div>
         <%
		}
         %>
          <%
		if(!assetAssigneeDTO.softwareKey.equalsIgnoreCase(""))
		{
		%>
		<div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_KEY, loginDTO)%></b>
                 </label>
                 <div class="col-md-8 form-control">
                     <%=assetAssigneeDTO.softwareKey%>
                 </div>
             </div>
         </div>
         <%
		}
         %>
     
         <%
		if(!assetAssigneeDTO.parentModelName.equalsIgnoreCase(""))
		{
		%>
		<div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=Language.equalsIgnoreCase("English")?"Attched To":"সংযুক্ত"%></b>
                 </label>
                 <div class="col-md-8 form-control">
                     <%=assetAssigneeDTO.parentModelName%>
                 </div>
             </div>
         </div>
         <%
		}
         %>
         <%
		if(asset_modelDTO.assetCategoryType == Asset_categoryDTO.PRINTER)
		{
		%>
		<div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_PRINTER_TYPE, loginDTO)%></b>
                 </label>
                 <div class="col-md-8 form-control">
                    <%=CatDAO.getName(Language, "printer_network_type", asset_modelDTO.printerNetworkTypeCat)%> 
                    <%=CatDAO.getName(Language, "printer_type", asset_modelDTO.printerTypeCat)%>
                 </div>
             </div>
         </div>
         <%
		}
         %>
         <%
		if(asset_modelDTO.scannerTypeCat != -1 && asset_modelDTO.assetCategoryType == Asset_categoryDTO.SCANNER)
		{
		%>
		<div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_SCANNER_TYPE, loginDTO)%></b>
                 </label>
                 <div class="col-md-8 form-control">
                    <%=CatDAO.getName(Language, "scanner_type", asset_modelDTO.scannerTypeCat)%>
                 </div>
             </div>
         </div>
         <%
		}
         %>
		
         <%
		if(!assetAssigneeDTO.sl.equalsIgnoreCase(""))
		{
		%>
         <div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_SL, loginDTO)%></b>
                 </label>
                 <div class="col-md-8 form-control">
                    <%=assetAssigneeDTO.sl%>
                 </div>
             </div>
         </div>
         <%
		}
         %>
         <div class="row col-md-6 mx-0 form-group" type="thTdLaptopCpu">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_PROCESSOR, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=CatDAO.getName(Language, "processor", assetAssigneeDTO.processorCat) + " " + CatDAO.getName(Language, "processor_gen", assetAssigneeDTO.processorGenCat)%>
	            </div>	            
	        </div>
    	</div>
    	<div class="row col-md-6 mx-0 form-group" type="thTdLaptopCpu">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_RAM, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=CatDAO.getName(Language, "ram", assetAssigneeDTO.ramCat) + " " + CatDAO.getName(Language, "ram_size", assetAssigneeDTO.ramSizeCat)%>
	            </div>	            
	        </div>
    	</div>
    	<div class="row col-md-6 mx-0 form-group" type="thTdLaptopCpu">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_HDNAME, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=CatDAO.getName(Language, "hd", assetAssigneeDTO.hdCat) + " " + CatDAO.getName(Language, "hd_size", assetAssigneeDTO.hdSizeCat)%>
	            </div>	            
	        </div>
    	</div>
    	 <%
		if(asset_modelDTO.monitorTypeCat != -1 && asset_modelDTO.assetCategoryType == Asset_categoryDTO.MONITOR)
		{
		%>
		<div class="row col-md-6 mx-0 form-group">
             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                 <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_MONITOR, loginDTO)%> <%=LM.getText(LC.HM_TYPE, loginDTO)%></b>
                 </label>
                 <div class="col-md-8 form-control">
                    <%=CatDAO.getName(Language, "monitor_type", asset_modelDTO.monitorTypeCat)%>
                 </div>
             </div>
         </div>
         <%
		}
         %>
    	<div class="row col-md-6 mx-0 form-group" type="thTdLaptopMonitor">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_DISPLAYSIZE, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=CatDAO.getName(Language, "display_size", asset_modelDTO.displaySizeCat)%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	if(!assetAssigneeDTO.mac.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" >
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_MAC, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.mac%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	<%
    	if(!assetAssigneeDTO.ip.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_IP, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.ip%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	
    	
    	<%
    	if(!assetAssigneeDTO.idfNumber.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdCpuAp">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_IDF_NUMBER, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.idfNumber%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(!assetAssigneeDTO.mkBoxNumber.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdCpuNet">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_MK_BOX_NUMBER, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.mkBoxNumber%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(!assetAssigneeDTO.activeDirectoryId.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdCpuNet">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_ACTIVE_DIRECTORY_ID, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.activeDirectoryId%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	
    	
    	<%
    	if(!assetAssigneeDTO.host.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdAp">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_HOST_NAME, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.host%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(!assetAssigneeDTO.location.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdAp">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_LOCATION, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.location%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(!assetAssigneeDTO.apLicenceForUse.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdAp">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_AP_LICENSE_FOR_USE, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.apLicenceForUse%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(!assetAssigneeDTO.port.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdAp">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_PORT, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.port%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(asset_modelDTO.upsCapacityCat != -1)
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdUps">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_CAPACITY, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=CatDAO.getName(Language, "ups_capacity", asset_modelDTO.upsCapacityCat)%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(asset_modelDTO.upsTypeCat != -1)
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdUps">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.HM_UPS_TYPE, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=CatDAO.getName(Language, "ups_type", asset_modelDTO.upsTypeCat)%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(!assetAssigneeDTO.idfNumber.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdCpuApNet">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_IDFNUMBER, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.idfNumber%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
    	<%
    	if(!assetAssigneeDTO.remarks.equalsIgnoreCase(""))
    	{
    	%>
    	<div class="row col-md-6 mx-0 form-group" type="thTdAp">
	        <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
	            <label class="col-md-4 col-form-label"><b><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_REMARKS, loginDTO)%></b>
	            </label>
	            <div class="col-md-8 form-control">
	            	<%=assetAssigneeDTO.remarks%>
	            </div>	            
	        </div>
    	</div>
    	<%
    	}
    	%>
    	
	</div>
</div>
<%
}
%>