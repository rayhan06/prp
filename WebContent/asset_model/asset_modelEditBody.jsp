<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="asset_model.*" %>
<%@page import="asset_category.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Asset_modelDTO asset_modelDTO;
    asset_modelDTO = (Asset_modelDTO) request.getAttribute("asset_modelDTO");
    CommonDTO commonDTO = asset_modelDTO;
    if (asset_modelDTO == null) {
        asset_modelDTO = new Asset_modelDTO();

    }
    String tableName = "asset_model";
    boolean isAssignment = false;
    if (request.getAttribute("isAssignment") != null) {
        isAssignment = (Boolean) request.getAttribute("isAssignment");
    }
%>
<%@include file="../pb/addInitializer.jsp" %>
<%
    String formTitle = Language.equalsIgnoreCase("english")?"ASSET ENTRY": "সম্পদ যোগ করুন";
    String servletName = "Asset_modelServlet";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    if (isAssignment) {
        actionName = "assign";
    }

    List<Asset_modelDTO> assetModels = Asset_modelRepository.getInstance().getAsset_modelListByNames();

    int nextYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
    int nextDecade = Calendar.getInstance().get(Calendar.YEAR) + 10;
    int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();

%>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<style>
    .rounded-border {
        border-radius: 8px;
    }

    .bg-primary-custom {
        background: #EDF8FC;
        color: #727476 !important;
    }

    .section-title {
        background: #EDF8FC;
        border: 4px solid #CDECF7;
        border-radius: 8px;
        color: #727476 !important;
        text-transform: capitalize;
    }

</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_modelServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                        


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=asset_modelDTO.iD%>' tag='pb_html'/>

                                        <%
                                            if (!isAssignment) {
                                        %>
                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_MODEL, loginDTO)%>
                                                *</label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='nameEn' autocomplete="off"
                                                       id='nameEn' value='<%=asset_modelDTO.nameEn%>'
                                                       required="required" tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_PRODUCT_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='productName'
                                                       id='productName_text_<%=i%>'
                                                       value='<%=asset_modelDTO.productName%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        
                                       

                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
                                            <label class="col-md-4 col-form-label"></label>
                                            <div class="col-md-8">
                                                <h4>
                                                    <%=LM.getText(LC.HM_ASSET_DETAILS, loginDTO)%>
                                                </h4>
                                            </div>
                                        </div>

                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='assetCategoryType'
                                                        id='assetCategoryType' onchange="decoratePageByCat(this.value)" tag='pb_html'>
                                                    <%
                                                        Options = CommonDAO.getOptionsWithWhere(Language, "asset_category", asset_modelDTO.assetCategoryType,
                                                        		" id != " + Asset_categoryDTO.DATA_CENTRE + " and id != " + Asset_categoryDTO.SOFTWARE);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>
                                        


                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_BRANDTYPE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='brandType' id='brandType'
                                                        tag='pb_html'>
                                                    <%
                                                        Options = CommonDAO.getOptions(Language, "brand", asset_modelDTO.brandType);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>
                                        
                                         <div class="form-group row col-md-6 mx-0 form-group px-0">
                                            <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("english")?"Warehouse Address":"ওয়্যারহাউজের ঠিকানা"%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type = "text" class='form-control' name='warehouseAddress'
                                                       id='warehouseAddress_text_<%=i%>'
                                                        tag='pb_html' value='<%=asset_modelDTO.warehouseAddress%>'/>
                                            </div>
                                        </div>


                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_SL, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='sl' id='sl'
                                                       value='<%=asset_modelDTO.sl%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_QUANTITY, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">

                                                <input type='number' step="0.01"
                                                       class='form-control' <%=asset_modelDTO.isImmutable?"readonly":""%>
                                                       name='quantity' id='quantity'
                                                       value='<%=asset_modelDTO.quantity%>' tag='pb_html'>
                                            </div>
                                        </div>

                                        
                                            
                                            <div class="form-group row col-md-6 mx-0 form-group px-0" id="processorDiv">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_PROCESSOR, loginDTO)%>
                                                </label>
                                                <div class="col-md-4">
                                                    <select class='form-control' name='processorCat' id='processorCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "processor", asset_modelDTO.processorCat);
	                                                    %>
                                                    <%=Options%>
                                                	</select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class='form-control' name='processorGenCat' id='processorGenCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "processor_gen", asset_modelDTO.processorGenCat);
	                                                    %>
                                                    <%=Options%>
                                                	</select>
                                                </div>
                                            </div>
                                            <div class="form-group row col-md-6 mx-0 form-group px-0" id="ramDiv">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_RAM, loginDTO)%>
                                                </label>
                                                <div class="col-md-4">                                                   
                                                    <select class='form-control' name='ramCat' id='ramCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "ram", asset_modelDTO.ramCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>
                                                </div>
                                                <div class="col-md-4">                                                   
                                                    <select class='form-control' name='ramSizeCat' id='ramSizeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "ram_size", asset_modelDTO.ramSizeCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row col-md-6 mx-0 form-group px-0" id="hdDiv">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_HDNAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-4">
                                                    <select class='form-control' name='hdCat' id='hdCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "hd", asset_modelDTO.hdCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class='form-control' name='hdSizeCat' id='hdSizeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "hd_size", asset_modelDTO.hdSizeCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>
                                                </div>
                                            </div>
                                        
                                        
                                        <div id="monitorDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_MONITOR, loginDTO)%> <%=LM.getText(LC.HM_TYPE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                	<select class='form-control' name='monitorTypeCat' id='monitorTypeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "monitor_type", asset_modelDTO.monitorTypeCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>                                                   
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="dcDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("English")?"Data Centre Type":"ডেটা সেন্টারের ধরন" %>
                                                </label>
                                                <div class="col-md-8">
                                                	<select class='form-control' name='dataCenterTypeCat' id='monitorTypeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "data_center_type", asset_modelDTO.dataCenterTypeCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>                                                   
                                                </div>
                                            </div>
                                        </div>
                                         


                                        <div id="laptopMonitorDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_DISPLAYSIZE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                	<select class='form-control' name='displaySizeCat' id='displaySizeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "display_size", asset_modelDTO.displaySizeCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>                                                   
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="printerDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_PRINTER_TYPE, loginDTO)%>
                                                </label>
                                                <div class="col-md-4">
                                                	<select class='form-control' name='printerNetworkTypeCat' id='printerNetworkTypeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "printer_network_type", asset_modelDTO.printerNetworkTypeCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>                                                   
                                                </div>
                                                <div class="col-md-4">
                                                	<select class='form-control' name='printerTypeCat' id='printerTypeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "printer_type", asset_modelDTO.printerTypeCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>                                                   
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="scannerDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_SCANNER_TYPE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                	<select class='form-control' name='scannerTypeCat' id='scannerTypeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "scanner_type", asset_modelDTO.scannerTypeCat);
	                                                    %>
	                                                    <%=Options%>
                                                	</select>                                                   
                                                </div>
                                            </div>
                                        </div>


                                        <div id="upsDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"></label>
                                                <div class="col-md-8">
                                                    <h4>
                                                        <%=LM.getText(LC.HM_UPS_SPECS, loginDTO)%>
                                                    </h4>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_CAPACITY, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <select class='form-control' name='upsCapacityCat' id='upsCapacityCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "ups_capacity", asset_modelDTO.upsCapacityCat);
	                                                    %>
	                                                    <%=Options%>
	                                                </select>
	
	                                            </div>
	                                        </div>
                                            
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_UPS_TYPE, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <select class='form-control' name='upsTypeCat' id='upsTypeCat' tag='pb_html'>
	                                                    <%
	                                                        Options = CatRepository.getOptions(Language, "ups_type", asset_modelDTO.upsTypeCat);
	                                                    %>
	                                                    <%=Options%>
	                                                </select>
	
	                                            </div>
	                                        </div>
	                                        
                                           
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_IDFNUMBER, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='idfNumber'
                                                           id='idfNumber' value='<%=asset_modelDTO.idfNumber%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="dCSwitchDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_MAC, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='mac' id='mac'
                                                           value='<%=asset_modelDTO.mac%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label">IP</label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='ip' id='ip'
                                                           value='<%=asset_modelDTO.ip%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="dCSwitchNACamDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_LOCATION, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='location'
                                                           id='location' value='<%=asset_modelDTO.location%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_PART_NUMBER, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='partNumber'
                                                           id='partNumber' value='<%=asset_modelDTO.partNumber%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="switchDiv">
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_HOST_NAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='hostName'
                                                           id='hostName' value='<%=asset_modelDTO.hostName%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row col-md-6 mx-0 form-group px-0">
                                                <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_MANAGEMENT_VLAN, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='managementVlan'
                                                           id='managementVlan'
                                                           value='<%=asset_modelDTO.managementVlan%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_PURCHASE_DETAILS, loginDTO)%> <%=LM.getText(LC.HM_IS_AVAILABLE, loginDTO)%></label>
	                                            <div class="col-md-8">
	                                                <input type='checkbox' class='form-control-sm mt-1' name='purchaseInformationAvailable'
	                                                       id='purchaseInformationAvailable' 
	                                                       <%=asset_modelDTO.purchaseInformationAvailable?"checked":""%>
	                                                       onchange = "showPurchaseDiv()"
	                                                       tag='pb_html'/>
	                                            </div>
                                        </div>

                                        
                                        
                                        <div id = "purchaseDiv" style="display:none">
                                        	<div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"></label>
	                                            <div class="col-md-8">
	                                                <h4>
	                                                    <%=LM.getText(LC.HM_PURCHASE_DETAILS, loginDTO)%>
	                                                </h4>
	                                            </div>
                                        	</div>
                                        	
                                        	<div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_LOT, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <input type='text' class='form-control' name='lot' id='lot_text_<%=i%>'
	                                                       value='<%=asset_modelDTO.lot%>' tag='pb_html'/>
	                                            </div>
	                                        </div>

	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_VENDORCAT, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <select class='form-control' name='vendorCat' id='vendorCat_category'
	                                                        tag='pb_html'>
	                                                    <%
	                                                    	Options = CommonDAO.getOptions( Language, "asset_supplier", asset_modelDTO.assetSupplierType);
	                                                    %>
	                                                    <%=Options%>
	                                                </select>
	                                            </div>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_PONUMBER, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <input type='text' class='form-control' name='poNumber'
	                                                       id='poNumber_text_<%=i%>' value='<%=asset_modelDTO.poNumber%>'
	                                                       tag='pb_html'/>
	                                            </div>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_RECEIVINGDATE, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <%
	                                                	value = "receivingDate_js_" + i;
	                                                %>
	                                                <jsp:include page="/date/date.jsp">
	                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
	                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
	                                                    <jsp:param name="END_YEAR" value="<%=nextYear%>"></jsp:param>
	                                                </jsp:include>
	                                                <input type='hidden' name='receivingDate' id='receivingDate_date_<%=i%>'
	                                                       value='<%=dateFormat.format(new Date(asset_modelDTO.receivingDate))%>'
	                                                       tag='pb_html'>
	                                            </div>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_PROCUREMENTTYPECAT, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <select class='form-control' name='procurementTypeCat'
	                                                        id='procurementTypeCat_category_<%=i%>' tag='pb_html'>
	                                                    <%
	                                                    	Options = CatRepository.getInstance().buildOptions("procurement_type", Language, asset_modelDTO.procurementTypeCat);
	                                                    %>
	                                                    <%=Options%>
	                                                </select>
	
	                                            </div>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("English")?"Has TO&E":"টি ও ই আছে"%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <input type='checkbox' class='form-control-sm' name='hasTOE'
	                                                       id='hasTOE' 
	                                                       <%=asset_modelDTO.hasTOE?"checked":""%>
	                                                       tag='pb_html'/>
	                                            </div>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("English")?"Has Warranty":"ওয়্যারেন্টি আছে"%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <input type='checkbox' class='form-control-sm' name='hasWarranty'
	                                                       id='hasWarranty' onchange="showHideEDateDiv(this.id)"
	                                                       <%=asset_modelDTO.hasWarranty?"checked":""%>
	                                                       tag='pb_html'/>
	                                            </div>
	                                        </div>
	                                        
	                                         <div class="form-group row col-md-6 mx-0 form-group px-0" id = "eDateDiv" style="display:none">
	                                            <label class="col-md-4 col-form-label"><%=Language.equalsIgnoreCase("English")?"Warranty Expiry Date":"ওয়ার‍্যান্টি এক্সপায়ারি ডেট"%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <%
	                                                	value = "expiryDate_js_" + i;
	                                                %>
	                                                <jsp:include page="/date/date.jsp">
	                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
	                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
	                                                    <jsp:param name="END_YEAR" value="<%=nextDecade%>"></jsp:param>
	                                                    <jsp:param name="START_YEAR" value="<%=currentYear%>"></jsp:param>
	                                                </jsp:include>
	                                                <input type='hidden' name='expiryDate' id='expiryDate_date_<%=i%>'
	                                                       value='<%=dateFormat.format(new Date(asset_modelDTO.expiryDate))%>'
	                                                       tag='pb_html'>
	                                            </div>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_PROJECTNAME, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <input type='text' class='form-control' name='projectName'
	                                                       id='projectName_text_<%=i%>'
	                                                       value='<%=asset_modelDTO.projectName%>' tag='pb_html'/>
	                                            </div>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_COSTINGVALUE, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <input type='number' step="0.01" class='form-control'
	                                                       name='costingValue' id='costingValue'
	                                                       value='<%=asset_modelDTO.costingValue%>' tag='pb_html'/>
	                                            </div>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0" style="display:none">
	                                            <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSETMODELSTATUSCAT, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-8">
	                                                <select class='form-control' name='assetModelStatusCat'
	                                                        id='assetModelStatusCat_category_<%=i%>' tag='pb_html'>
	                                                    <%
	                                                    	Options = CatRepository.getInstance().buildOptions("asset_model_status", Language, asset_modelDTO.assetModelStatusCat);
	                                                    %>
	                                                    <%=Options%>
	                                                </select>
	                                            </div>
	                                        </div>
                                        </div>
                                        <%
                                        	} else {
                                        %>
                                        <%@include file="../asset_model/asset_modelViewFields.jsp" %>
                                        <%
                                            }
                                        %>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           
                <div class="row w-100 mx-0">
                    <div class="col-md-12 form-actions text-center mt-0 pr-0">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_MODEL_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                            <%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_MODEL_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=request.getContextPath() + "/"%>assets/scripts/autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {
        <%
        if(!actionName.equalsIgnoreCase("assign"))
        {
        %>
       
        preprocessDateBeforeSubmitting('receivingDate', row);
        preprocessDateBeforeSubmitting('expiryDate', row);
        <%
        }
        %>

     
        submitAddForm();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Asset_modelServlet");
    }

    <%@include file="../asset_model/showHideDivs.jsp"%>
    var models = [
        <%
        for(Asset_modelDTO assetModel: assetModels)
        {
        %>
        "<%=assetModel.nameEn%>",
        <%
        }
        %>
    ];

    function init(row) {

    	setDateByStringAndId('receivingDate_js_' + row, $('#receivingDate_date_' + row).val());
    	setDateByStringAndId('expiryDate_js_' + row, $('#expiryDate_date_' + row).val());
        showHideDivs();
        showPurchaseDiv();
        showHideEDateDiv();

    }

    function autocompleteListener(text) {
        console.log(text);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var asset_modelDTO = JSON.parse(this.responseText);
                Object.keys(asset_modelDTO).forEach(function (key) {
                    var value = asset_modelDTO[key];
                    //console.log("key = " + key + ", value = " + value);
                    if ($("#" + key).length) {
                        $("#" + key).val(value);
                    }
                });
                showHideDivs();

            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "Asset_modelServlet?actionType=getDTOByName&nameEn=" + text, true);
        xhttp.send();
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;
    var ctCount = <%=childTableStartingID%>;


   
    function removeDiv(binId)
    {
    	var rowId = binId.split("_")[1];
    	
    }


   

    var canUnassign = true;

  
	function showHideEDateDiv()
	{
		console.log("showHideEDateDiv called");
		if($("#hasWarranty").prop("checked"))
		{
			console.log("showHideEDateDiv called 1");
			$("#eDateDiv").removeAttr("style");
		}
		else
		{
			console.log("showHideEDateDiv called 2");
			$("#eDateDiv").css("display", "none");
		}
	}
    
    function decoratePageByCat(cat)
    {
    	showHideDivs();
    	var qs = "Asset_modelServlet?actionType=getBrandsByCategory&cat=" + cat;
    	fillSelect("brandType", qs);
    }
    
    function showPurchaseDiv()
    {
    	if($("#purchaseInformationAvailable").prop("checked"))
   		{
   			$("#purchaseDiv").css("display", "block");
   		}
    	else
   		{
    		$("#purchaseDiv").css("display", "none");
   		}
    }


</script>



