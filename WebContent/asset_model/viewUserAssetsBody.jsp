<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="asset_model.*" %>
<%@page import="asset_category.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%@page import="user.UserRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_model.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@ page import="office_units.*"%>


<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="user.*"%>


<%
    Asset_modelDTO asset_modelDTO;
    asset_modelDTO = (Asset_modelDTO) request.getAttribute("asset_modelDTO");
    CommonDTO commonDTO = asset_modelDTO;
    if (asset_modelDTO == null) {
        asset_modelDTO = new Asset_modelDTO();

    }
    String tableName = "asset_model";
    boolean isAssignment = false;
    if (request.getAttribute("isAssignment") != null) {
        isAssignment = (Boolean) request.getAttribute("isAssignment");
    }
%>
<%@include file="../pb/addInitializer.jsp" %>
<%
    String formTitle = LM.getText(LC.ASSET_MODEL_ADD_ASSET_MODEL_ADD_FORMNAME, loginDTO);
    String servletName = "Asset_modelServlet";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    if (isAssignment) {
        actionName = "assign";
    }

    List<Asset_modelDTO> assetModels = Asset_modelRepository.getInstance().getAsset_modelListByNames();

    int nextYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
    AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

%>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<style>
    .rounded-border {
        border-radius: 8px!important;
    }

    .bg-primary-custom {
        background: #EDF8FC!important;
        color: #727476 !important;
    }

    .section-title {
        background: #EDF8FC!important;
        border: 4px solid #CDECF7!important;
        border-radius: 8px!important;
        color: #727476 !important;
        text-transform: capitalize!important;
    }

</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
                <div class="ml-auto mr-3">
				    <button type="button" class="btn" id='printer2'
				            onclick="location.href='Asset_modelServlet?actionType=search'">
				        <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
				<div class="ml-auto mr-3">
				    <button type="button" class="btn" id='printer2'
				            onclick="location.href='Asset_modelServlet?actionType=getAddUserAssetsPage'">
				        <i class="fa fa-plus fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
            </div>
            
         </div>
          <form class="form-horizontal"
              action="Asset_modelServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
	            <div class="kt-portlet__body form-body">
	                <div class="row mb-4">
	                    <div class="col-md-12">
	                        <div class="onlyborder">
	                            <div class="row">
	                                <div class="col-md-10 offset-md-1">
	                                    <div class="sub_title_top">
	                                        <div class="sub_title">
	                                            <h4 style="background: white"><%=formTitle%>
	                                            </h4>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-3 col-form-label text-md-right">
	                                                <%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-9">
	                                                <button type="button"
	                                                        class="btn text-white shadow form-control btn-border-radius"
	                                                        style="background-color: #4a87e2"
	                                                        onclick="addEmployee()"
	                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
	                                                </button>
	                                                <table class="table table-bordered table-striped">
	                                                    <tbody id="employeeToSet"></tbody>
	                                                </table>
	                                                <input type='hidden' class='form-control' name='orgId'
                                                       id='orgId' value='-1' tag='pb_html'/>
	                                            </div>
	                                        </div>
	                                        
	                                           <div class="form-group row col-md-6 mx-0 form-group px-0" id="ownerDiv">
		                                        <label class="col-md-3 col-form-label text-md-right" for="ownerCat" >
		                                            <%=isLangEng ? "Owner Type" : "মালিকানার ধরণ"%>
		                                        </label>
		                                        <div class="col-md-9">
		                                            <select class='form-control' name='ownerCat' onchange = "showHideRoom()"
		                                                    id='ownerCat' tag='pb_html' >
		                                                <%=CatDAO.getOptions(Language, "owner", -1)%>
		                                            </select>
		
		                                        </div>
		                                    </div>
	                                        
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0" id="roomDiv" style = "display:none">
		                                        <label class="col-md-3 col-form-label text-md-right" for="roomNoCat">
		                                            <%=isLangEng ? "Room" : "কক্ষ"%>
		                                        </label>
		                                        <div class="col-md-9">
		                                            <select class='form-control' name='roomNoCat' onchange = 'clearInfo()'
		                                                    id='roomNoCat' tag='pb_html' >
		                                                <%=CatDAO.getOptions(Language, "room_no", -1)%>
		                                            </select>
		
		                                        </div>
		                                    </div>
		                                    
		                                    <div class="form-group row col-md-6 mx-0 form-group px-0">
			                                    	<label class="col-md-3 col-form-label text-md-right" >
			                                            
			                                        </label>
			                                        <div class="col-md-9">
			                                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" 
			                                            onclick = "fetchEmployeeAssets(<%=Asset_modelDTO.PAGE_ASSET%>)">
							                            	<%=isLangEng ? "Get Assets" : "সম্পদ দেখুন"%>
							                        	</button>
							                        	<button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" 
							                        	onclick = "fetchEmployeeAssets(<%=Asset_modelDTO.PAGE_QR%>)">
							                            	<%=isLangEng ? "Get QR Codes" : "কিউ আর কোড দেখুন"%>
							                        	</button>
			
			                                        </div>
			                                    	
			                                 </div>
	                                    </div>
	                                 </div>
	                             </div>
	                         </div>
	                     </div>
	                 </div>
	             </div>
	             <div class="kt-portlet__body form-body" id = "detailsDiv">
        	
				 </div>
           </form>
    </div>
</div>
    
    
    
    <script type="text/javascript">
    <%@include file="../asset_model/assetViewJs.jsp"%>
    function patient_inputted(userName, orgId) {
    	$("#orgId").val(orgId);
    	$("#roomNoCat").val('-1');
    	clearInfo();
    }
    
    function clearInfo()
    {
    	$("#detailsDiv").html('');
    	$("submitButton").hide();
    }
    
	function showHideRoom()
	{
		clearInfo();
		var owner = $("#ownerCat").val();
		if(owner == <%=AssetAssigneeDTO.OWNER_ROOM%>)
		{
			$("#roomDiv").show();
		}
		else
		{
			$("#roomDiv").hide();
		}
	}
    
    function fetchEmployeeAssets(type)
    {
    	var orgId = $("#orgId").val();
    	var ownerCat = $("#ownerCat").val();
    	var roomNoCat = $("#roomNoCat").val();
    	
    	if(orgId == '-1' || orgId == '' || orgId == null)
    	{
    		toastr.error("Choose Employee");
    		return;
    	}
    	
    	if(ownerCat == '-1' || ownerCat == '' || ownerCat == null)
    	{
    		toastr.error("Choose Owner Type");
    		return;
    	}
    	
    	if(ownerCat == '<%=AssetAssigneeDTO.OWNER_ROOM%>' && roomNoCat == '-1')
    	{
    		toastr.error("Choose Room");
    		return;
    	}
    	    
    	 var xhttp = new XMLHttpRequest();
         xhttp.onreadystatechange = function () {
             if (this.readyState == 4 && this.status == 200) 
             {
                 $("#detailsDiv").html(this.responseText);
                 console.log("calling showHideChildDivs");
                 showHideChildDivs();
             }
             else
             {
            	 
             }
             
         };

         xhttp.open("GET", "Asset_modelServlet?actionType=getUserAssetDetails&organogramId=" + orgId
        		 + "&ownerCat=" + ownerCat
        		 + "&type=" + type
         		+ "&roomNoCat=" + roomNoCat, true);
         xhttp.send();
    }
    
    
    </script>
