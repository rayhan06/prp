
<%@page import="asset_type.Asset_typeDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_model.*"%>
<%@page import="asset_category.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navASSET_MODEL";
String servletName = "Asset_modelServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_LOT, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_BRANDTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_MODEL, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_SL, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_QUANTITY, loginDTO)%></th>							
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_RECEIVINGDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_PROJECTNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_COSTINGVALUE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.ASSET_MODEL_SEARCH_ASSET_MODEL_EDIT_BUTTON, loginDTO)%></th>
								<th style="display:none"><%=LM.getText(LC.HM_ASSIGN, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute("viewASSET_MODEL");

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Asset_modelDTO asset_modelDTO = (Asset_modelDTO) data.get(i);
											Asset_categoryDTO asset_categoryDTO = Asset_categoryRepository.getInstance().getAsset_categoryDTOByID(asset_modelDTO.assetCategoryType);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = asset_modelDTO.assetCategoryType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "asset_category", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = asset_modelDTO.lot + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = asset_modelDTO.brandType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "brand", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = asset_modelDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
											<td>
											<%
											value = asset_modelDTO.sl + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td style= "text-align: right;">

				
											<%=Utils.getDigits((int)asset_modelDTO.quantity, Language)%>
				
			
											</td>
											
											<td>
											<%
											String formatted_receivingDate = simpleDateFormat.format(new Date(asset_modelDTO.receivingDate));
											%>
											<%=Utils.getDigits(formatted_receivingDate, Language)%>
				
			
											</td>
		
		
											<td>
											<%
											value = asset_modelDTO.projectName + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td style= "text-align: right;">
											<%
											value = asset_modelDTO.costingValue + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>				
	
											<%CommonDTO commonDTO = asset_modelDTO; %>
											<td>
												<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														<%
														if(asset_modelDTO.assetTypeId == Asset_typeDTO.SOFTWARE)
														{
														%>
														onclick="location.href='Software_typeServlet?actionType=view&ID=<%=asset_modelDTO.softwareTypeId%>'"
														<%
														}
														else
														{
														%>
														onclick="location.href='<%=servletName%>?actionType=view&ID=<%=asset_modelDTO.iD%>'"
														<%
														}
														%>
												>
													<i class="fa fa-eye"></i>
												</button>												
											</td>
											<td>
											
											<%
											//if(!asset_modelDTO.isImmutable)
											{
											%>
											<button
														type="button"
														class="btn-sm border-0 shadow btn-border-radius text-white"
														style="background-color: #ff6b6b;"
														onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
												>
													<i class="fa fa-edit"></i>
												</button>	
											
											<%
											}
											%>
											</td>	
											<td style="display:none">
											<%
												if(asset_categoryDTO.isAssignable)
																				{
											%>
												<button
														type="button"
														class="btn-sm border-0 shadow btn-border-radius text-white"
														style="background-color: #ff6b6b;"
														onclick="location.href='<%=servletName%>?actionType=getAssignmentPage&ID=<%=commonDTO.iD%>'"
												>
													<i class="fa fa-plus"></i>
												</button>
											<%
											}
											%>
											</td>							
																						
											<td class="text-right">
											<%
											//if(!asset_modelDTO.isImmutable)
											{
											%>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=asset_modelDTO.iD%>'/></span>
												</div>
												<%
											}
												%>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			