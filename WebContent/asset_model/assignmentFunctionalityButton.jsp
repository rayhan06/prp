<%@page pageEncoding="UTF-8"%>

<%
if (assetAssigneeDTO.assignmentStatus==AssetAssigneeDTO.ASSIGNED || assetAssigneeDTO.assignmentStatus==AssetAssigneeDTO.ACTIVE || assetAssigneeDTO.assignmentStatus==AssetAssigneeDTO.NOT_ASSIGNED ||
assetAssigneeDTO.assignmentStatus==AssetAssigneeDTO.ACTIVATE) {
%>
<button type="button" class="btn btn-primary" onclick="onMaintenanceButtonPress('<%=assetAssigneeDTO.iD%>')" id=""><%=isLanguageEnglish ? "SEND TO MAINTENANCE" : "মেইন্টেন্যান্সে পাঠান"%></button>
<button type="button" class="btn btn-danger" onclick="onCondemnButtonPress('<%=assetAssigneeDTO.iD%>', '0')" id=""><%=isLanguageEnglish ? "CONDEMN" : "কনডেম করুন"%></button>
<%
} else if (assetAssigneeDTO.assignmentStatus==AssetAssigneeDTO.CONDEMNED) {
%>
<button type="button" class="btn btn-success" onclick="onCondemnButtonPress('<%=assetAssigneeDTO.iD%>', '1')" id=""><%=isLanguageEnglish ? "ACTIVATE" : "সচল করুন"%></button>
<button type="button" class="btn btn-success" onclick="onReleaseButtonPress('<%=assetAssigneeDTO.iD%>', '1')" id=""><%=isLanguageEnglish ? "RELEASE " : "খারিজ করুন"%></button>
<%
} else if (assetAssigneeDTO.assignmentStatus==AssetAssigneeDTO.WARRANTY) {
%>
<button type="button" class="btn btn-primary" onclick="onReturnFromWarrantyButtonPress('<%=assetAssigneeDTO.iD%>')" id=""><%=isLanguageEnglish ? "RETURN FROM WARRANTY" : "ওয়্যারেন্টি থেকে ফেরত আনুন"%></button>
<%
} else if (assetAssigneeDTO.assignmentStatus==AssetAssigneeDTO.SEND_TO_REPAIR) {
%>
<button type="button" class="btn btn-primary" onclick="onReturnFromRepairButtonPress('<%=assetAssigneeDTO.iD%>')" id=""><%=isLanguageEnglish ? "RETURN FROM REPAIR" : "রিপেয়ার শপ থেকে ফেরত আনুন"%></button>
<%
    }
%>