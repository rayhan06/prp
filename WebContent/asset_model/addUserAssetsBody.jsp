<%@page import="asset_type.Asset_typeDTO"%>
<%@page import="software_type.Software_typeDTO"%>
<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="asset_model.*" %>
<%@page import="asset_category.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%@page import="user.UserRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_model.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@ page import="office_units.*"%>


<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="user.*"%>


<%
    Asset_modelDTO asset_modelDTO;
    asset_modelDTO = (Asset_modelDTO) request.getAttribute("asset_modelDTO");
    CommonDTO commonDTO = asset_modelDTO;
    if (asset_modelDTO == null) {
        asset_modelDTO = new Asset_modelDTO();

    }
    String tableName = "asset_model";
    boolean isAssignment = false;
    if (request.getAttribute("isAssignment") != null) {
        isAssignment = (Boolean) request.getAttribute("isAssignment");
    }
%>
<%@include file="../pb/addInitializer.jsp" %>
<%
    String formTitle = LM.getText(LC.ASSET_MODEL_ADD_ASSET_MODEL_ADD_FORMNAME, loginDTO);
    String servletName = "Asset_modelServlet";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    if (isAssignment) {
        actionName = "assign";
    }

    List<Asset_modelDTO> assetModels = Asset_modelRepository.getInstance().getAsset_modelListByNames();

    int nextYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
    AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
    actionName = "massAssign";
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<style>
    .rounded-border {
        border-radius: 8px;
    }

    .bg-primary-custom {
        background: #EDF8FC;
        color: #727476 !important;
    }

    .section-title {
        background: #EDF8FC;
        border: 4px solid #CDECF7;
        border-radius: 8px;
        color: #727476 !important;
        text-transform: capitalize;
    }

</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
         </div>
          <form class="form-horizontal"
              action="Asset_modelServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
	            <div class="kt-portlet__body form-body">
	                <div class="row mb-4">
	                    <div class="col-md-12">
	                        <div class="onlyborder">
	                            <div class="row">
	                                <div class="col-md-10 offset-md-1">
	                                    <div class="sub_title_top">
	                                        <div class="sub_title">
	                                            <h4 style="background: white"><%=formTitle%>
	                                            </h4>
	                                        </div>
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                            <label class="col-md-3 col-form-label text-md-right">
	                                                <%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
	                                            </label>
	                                            <div class="col-md-9">
	                                                <button type="button"
	                                                        class="btn text-white shadow form-control btn-border-radius"
	                                                        style="background-color: #4a87e2"
	                                                        onclick="addEmployee()"
	                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
	                                                </button>
	                                                <table class="table table-bordered table-striped">
	                                                    <tbody id="employeeToSet"></tbody>
	                                                </table>
	                                            </div>
	                                            <input type='hidden' class='form-control' name='orgId'
                                                       id='orgId' value='-1' tag='pb_html'/>
	                                        </div>
	                                        
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0" id="ownerDiv">
		                                        <label class="col-md-3 col-form-label text-md-right" for="ownerCat" >
		                                            <%=isLangEng ? "Owner Type" : "মালিকানার ধরণ"%>
		                                        </label>
		                                        <div class="col-md-9">
		                                            <select class='form-control' name='ownerCat' onchange = "showHideRoom()"
		                                                    id='ownerCat' tag='pb_html' >
		                                                <%=CatDAO.getOptions(Language, "owner", -1)%>
		                                            </select>
		
		                                        </div>
		                                    </div>
	                                        
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0" id="roomDiv" style = "display:none">
		                                        <label class="col-md-3 col-form-label text-md-right" for="roomNoCat">
		                                            <%=isLangEng ? "Room" : "কক্ষ"%>
		                                        </label>
		                                        <div class="col-md-9">
		                                            <select class='form-control' name='roomNoCat' onchange = 'clearInfo()'
		                                                    id='roomNoCat' tag='pb_html' >
		                                                <%=CatDAO.getOptions(Language, "room_no", -1)%>
		                                            </select>
		
		                                        </div>
		                                    </div>
		                                    
		                                    
		                                    
	                                        
	                                        <div id = "userDiv" style = "display:none">
		                                   
		                                        <div class="form-group row col-md-6 mx-0 form-group px-0" style="display:none">
		                                            <label class="col-md-3 col-form-label text-md-right">
		                                                <%=LM.getText(LC.HM_OFFICE_TYPE, loginDTO)%>
		                                            </label>
		                                            <div class="col-md-9">
		                                                
                                                          <select class='form-control'
                                                                  name='officeTypeCat'
                                                                  id='officeTypeCat'
                                                                  tag='pb_html'>
                                                              <%
                                                                  Options = CatDAO.getOptions(Language, "office_type", -1);
                                                              %>
                                                              <%=Options%>
                                                          </select>
                                                        
		                                            </div>
		                                        </div>
		                                        
		                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
	                                                <label class="col-md-3 col-form-label"><%=LM.getText(LC.HM_ASSIGNMENT_DATE, loginDTO)%>
	                                                </label>
	                                                <div class="col-md-9">
	                                                    <%value = "assignmentDate_js_" + i;%>
	                                                    <jsp:include page="/date/date.jsp">
	                                                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
	                                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
	                                                    </jsp:include>
	                                                    <input type='hidden' name='assignmentDate'
	                                                           id='assignmentDate_date_<%=i%>'
	                                                           value='<%=dateFormat.format(new Date())%>'
	                                                           tag='pb_html'>
	                                                </div>
                                            	</div>
                                            	
                                            	
	                                        </div>
	                                        
	                                        <div class="form-group row col-md-6 mx-0 form-group px-0">
			                                    	<label class="col-md-3 col-form-label text-md-right" >
			                                            
			                                        </label>
			                                        <div class="col-md-9">
			                                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick = "fetchEmployeeAssets()">
							                            	<%=isLangEng ? "Get Assets" : "সম্পদ দেখুন"%>
							                        	</button>
			
			                                        </div>
			                                    	
			                                 </div>
	                                       
	                                    </div>
	                                 </div>
	                             </div>
	                         </div>
	                     </div>
	                 </div>
	             </div>
	             <div class="kt-portlet__body form-body" id = "detailsDiv">
        	
				 </div>
				 <div class="row w-100 mx-0">
                    <div class="col-md-12 form-actions text-center mt-0 pr-0">
                        
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit" id = "submitButton" style = "display:none">
                            <%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_MODEL_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
             </form>
        </div>
    </div>
    
    <script type="text/javascript">
    
    <%@include file="../asset_model/showHideDivs.jsp"%>
    
    function clearInfo()
    {
    	$("#detailsDiv").html('');
    	$("submitButton").hide();
    }
    
	function showHideRoom()
	{
		clearInfo();
		var owner = $("#ownerCat").val();
		if(owner == <%=AssetAssigneeDTO.OWNER_ROOM%>)
		{
			$("#roomDiv").show();
		}
		else
		{
			$("#roomDiv").hide();
		}
	}
    
    function fetchParents(rowId)
    {
    	var parentCatStr = $("#parentCatgories_hidden_" + rowId).val();
    	console.log("parentCatStr = " + parentCatStr);
    	if(parentCatStr == "" ||  $("#type_" + rowId).val() == <%=Asset_typeDTO.HARDWARE%>)
   		{
    		console.log("hiding parent div " + rowId);
    		$("#parentDiv_" + rowId).css("display", "none");
   		}
    	else
   		{
    		console.log("showing parent div " + rowId);
    		var parents = parentCatStr.split(", ");
    		var options = "<option value = '-1'>Select</option>";
    		var defaultVal = $("#parentAssigneeId_select_" + rowId).val();
    		for (var i = 0; i < parents.length; i++) 
    		{
    			for(var j = 0; j < parseInt($("#childTableStartingID").val()); j ++)
    	   		{
    				if($("#cat_"+ j).length > 0 && $("#cat_"+ j).val() == parents[i])
    				{
    					var modelName = $("#modelName_hidden_" + j).val();
    					var sl = $("#sl_text_" + j).val();
    					var text = modelName;
    					if(sl != "")
    					{
    						text += ": " + sl;
    					}
    					var assigneeId = $("#assetAssignee_hidden_" + j).val();
    					options += "<option value = '" + assigneeId + "' ";
    					if(assigneeId == defaultVal)
   						{
    						options += " selected ";
   						}
    					options += ">" + text + "</option>";
    				}
    	   		}
    		}
    		$("#parentAssigneeId_select_" + rowId).html(options);
    		if($("#parentAssigneeId_select_" + rowId +" option").length <= 1)
   			{
    			console.log("hiding parent");
    			$("#parentDiv_" + rowId).css("display", "none");
   			}
    		else
   			{
    			console.log("showing parent length: " + $("#parentAssigneeId_select_" + rowId +" option").length);
    			$("#parentDiv_" + rowId).removeAttr("style");
   			}
   		}
    }
    
    function PreprocessBeforeSubmiting(row, validate) {
    	var orgId = $("#orgId").val();
    	var ownerCat = $("#ownerCat").val();
    	var roomNoCat = $("#roomNoCat").val();
    	
    	if(orgId == '-1' || orgId == '' || orgId == null)
    	{
    		toastr.error("Choose Employee");
    		return false;
    	}
    	
    	if(ownerCat == '-1' || ownerCat == '' || ownerCat == null)
    	{
    		toastr.error("Choose Owner Type");
    		return false;
    	}
    	
    	if(ownerCat == '<%=AssetAssigneeDTO.OWNER_ROOM%>' && roomNoCat == '-1')
    	{
    		toastr.error("Choose Room");
    		return false;
    	}
    	preprocessDateBeforeSubmitting('assignmentDate', row);
    }
    
    $(document).ready(function () {
    	var row = 0;
        setDateByStringAndId('assignmentDate_js_' + row, $('#assignmentDate_date_' + row).val());
    });
    
    function setHiddenUnassign(cbId)
    {
    	var rowId = cbId.split("_")[2];
    	var hiddenId = "unassign_hidden_" + rowId;
    	$("#" + hiddenId).val($("#" + cbId).prop("checked"));
    	if($("#" + cbId).prop("checked"))
   		{
   			$("#deactivate_text_" + rowId).css("display", "block");
   			$("#deactivate_div_" + rowId).css("display", "block");
   		}
    	else
   		{
    		$("#deactivate_text_" + rowId).css("display", "none");
   			$("#deactivate_div_" + rowId).css("display", "none");
   		}
    }
    
    function setHiddenDeactivate(cbId)
    {
    	var rowId = cbId.split("_")[2];
    	var hiddenId = "deactivate_hidden_" + rowId;
    	$("#" + hiddenId).val($("#" + cbId).prop("checked"));
    }
    
    function fetchEmployeeAssets()
    {
    	var orgId = $("#orgId").val();
    	var ownerCat = $("#ownerCat").val();
    	var roomNoCat = $("#roomNoCat").val();
    	
    	if(orgId == '-1' || orgId == '' || orgId == null)
    	{
    		toastr.error("Choose Employee");
    		return;
    	}
    	
    	if(ownerCat == '-1' || ownerCat == '' || ownerCat == null)
    	{
    		toastr.error("Choose Owner Type");
    		return;
    	}
    	
    	if(ownerCat == '<%=AssetAssigneeDTO.OWNER_ROOM%>' && roomNoCat == '-1')
    	{
    		toastr.error("Choose Room");
    		return;
    	}
    	    
    	
    	$("#submitButton").show();
    	var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) 
            {
                $("#detailsDiv").html(this.responseText);
                showHideChildDivs();
                $("#userDiv").css("display", "block");
                $("#officeTypeCat").val($("#remoteOfficeType").val());
            }
            else
            {
            }
            
        };

        xhttp.open("GET", "Asset_modelServlet?actionType=getUserAssetEdit&organogramId=" + orgId 
        		+ "&ownerCat=" + ownerCat
        		+ "&roomNoCat=" + roomNoCat
        		, true);
        xhttp.send();
    }
    
    function patient_inputted(userName, orgId) {
    	$("#orgId").val(orgId);
    	clearInfo();
    	$("#roomNoCat").val('-1');
    }
    
    function showHideChildDivs()
    {
    	for(var i = 0; i < parseInt($("#childTableStartingID").val()); i ++)
   		{
    		showHideDivsForAssignment($("#cat_"+ i).val(), "AssetAssignee_" + i);

    		fetchParents(i);
   		}
    }
    
    function addRemoveKeys(oldKey, newKey, type, rowId, keyText)
    {
    	console.log("oldKey = " + oldKey + " newKey = " + newKey + " type = " + type + " rowId = " + rowId + " keyText = " + keyText)
    	for(var i = 0; i < parseInt($("#childTableStartingID").val()); i ++)
   		{
    		var keySelect = "#" + type + "KeyId_select_" + i;
    		if(oldKey != -1)
   			{
    			if(i != rowId && $(keySelect + " option[value='" + oldKey + "']").length <= 0)
   				{
    				console.log("select found, appending for " + i );
    				$(keySelect).append("<option value='" + oldKey + "'>" + keyText +"</option>");
   				}
   			}
    		if(newKey != -1)
   			{
    			if(i != rowId && $(keySelect + " option[value='" + newKey + "']").length > 0)
   				{
    				console.log("select found, removing for " + i);
    				$(keySelect + " option[value='" + newKey + "']").remove();
   				}
   			}
    		
   		}
    }
    
    function syncKeys(id, type)
    {
    	var rowId = id.split("_")[2];
    	var newKey = $("#" + id).val();
    	var oldKey = $("#old_" + type + "_" + rowId).val();
    	var keyText = $("#old_" + type + "Text_" + rowId).val();
    	addRemoveKeys(oldKey, newKey, type, rowId, keyText);
    }
    
    function setOldValue(id, type)
    {
    	var rowId = id.split("_")[2];
    	var key = $("#" + id).val();
    	$("#old_" + type + "_" + rowId).val(key);
    	$("#old_" + type + "Text_" + rowId).val($( "#" + id + " option:selected" ).text());
    }
    
    
    function getSubCat(id)
    {
    	var cat = $("#" + id).val();
    	var rowId = id.split("_")[2];
    	var subCat = $("#softwareSubCat_select_" + rowId).val();
    	if(subCat == "" || subCat == null)
    	{
    		subCat = "-1";
    	}
    	console.log("subCat = " + subCat + " cat = " + cat);
    	var qs = "Software_typeServlet?actionType=getSubType&cat=" + cat + "&subCat=" + subCat;
    	fillSelect("softwareSubCat_select_" + rowId, qs);
    }
	
	function assigneeAdded(e)  
	{
    	console.log("add clicked");
        e.preventDefault();
        var t = $("#template-AssetAssignee");
        
        //console.log(t.html());

        $("#field-AssetAssignee").append(t.html());

        
        var child_table_extra_id = parseInt($("#childTableStartingID").val());
		console.log("children: " + $("#field-AssetAssignee").children().length);
		var lastChild = $("#field-AssetAssignee").children().length -1;
        var tr = $("#field-AssetAssignee").find("[a1='child']");
        tr.attr("id", "AssetAssignee_" + child_table_extra_id);
        var trId = tr.attr("id");
        tr.removeAttr("a1");
        //console.log(tr.html());
        console.log("found div id = " + trId);

        
        

        tr.attr("id", "AssetAssignee_" + child_table_extra_id);

        tr.find("[tag='pb_html']").each(function (index) {
            var prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_extra_id);
            //console.log( index + ": " + $( this ).attr('id')  + " prev_id = " + prev_id);
        });
        
        
        $("#innerDiv_" + child_table_extra_id).css("display", "block");
        
        child_table_extra_id ++;
        $("#childTableStartingID").val(child_table_extra_id);

    }
	
	function getCategories(typeId)
	{
		console.log("getting cats");
		var type = $("#" + typeId).val();
		var rowId = typeId.split("_")[1];
		$("#innererDiv_" + rowId).css("display", "none");
		$("#productDetails_" + rowId).css("display", "none");
		
		if(type == <%=Asset_typeDTO.SOFTWARE%>)
		{
			$("#catDiv_" + rowId).css("display", "none");
			$("#modelDiv_" + rowId).css("display", "none");
			$("#slDiv_" + rowId).css("display", "none");
			$("#softwareCatDiv_" + rowId).css("display", "block");
			$("#softwareSubCatDiv_" + rowId).css("display", "block");
			$("#innererDiv_" + rowId).css("display", "block");
			
			setValueToSelect("cat_" + rowId, <%=Asset_categoryDTO.SOFTWARE%>, "");
	
			console.log("software fields set");
		}
		else
		{
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if (this.responseText.includes('option')) {
						//console.log(this.responseText);
						$("#cat_" + rowId).html(this.responseText);	
						$("#catDiv_" + rowId).css("display", "block");
						$("#modelDiv_" + rowId).css("display", "block");
						$("#slDiv_" + rowId).css("display", "block");
						$("#softwareCatDiv_" + rowId).css("display", "none");
						$("#softwareSubCatDiv_" + rowId).css("display", "none");
					} else {
						console.log("got errror");
					}

				} else if (this.readyState == 4 && this.status != 200) {
					alert('failed ' + this.status);
				}
			};
			xhttp.open("GET", "Asset_modelServlet?actionType=getCategories&type=" + type, true);
			xhttp.send();
		}
	}
	
	function getModels(catId)
	{
		console.log("getting models");
		var cat = $("#" + catId).val();
		console.log("cat = " + cat);
		var rowId = catId.split("_")[1];
		for(var i = 0; i < $("#childTableStartingID").val(); i ++)
		{
			if(i != rowId)
			{
				if($("#cat_" + i).val() == cat && $("#type_" + i).val() == <%=Asset_typeDTO.HARDWARE%>)
				{
					toastr.error("User already has this type of asset.");
				}
			}
			
		}
		
		var modelId = "model_select_" + rowId;
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if (this.responseText.includes('option')) {
					//console.log(this.responseText);
					$("#" + modelId).html(this.responseText);
					$("#innererDiv_" + rowId).css("display", "block");
				} else {
					console.log("got errror");
				}

			} else if (this.readyState == 4 && this.status != 200) {
				alert('failed ' + this.status);
			}
		};
		xhttp.open("GET", "Asset_modelServlet?actionType=getModelsByCategory&cat=" + cat, true);
		xhttp.send();
		$("#productDetails_" + rowId).css("display", "none");
		$("#sl_text_" + rowId).val("");
	}
	
	function getKeys(licenseId)
	{
		var rowId = licenseId.split("_")[2];
		var licenseType = $("#" + licenseId).val();
		if(licenseType == 0)
		{
			$("#softwareKey_select_" + rowId).css("display", "none");
			getAssignee(4, licenseId);
		}
		else
		{
			var softwareCatId = "softwareCat_select_" + rowId;
			var softwareSubCatId = "softwareSubCat_select_" + rowId;
			
			var usedAssigneeIds = "";
			
			for(var i = 0; i < $("#childTableStartingID").val(); i ++)
			{
				if($("#assetAssignee_hidden_" + i).length && $("#assetAssignee_hidden_" + i).val() != "")
				{
					usedAssigneeIds += $("#assetAssignee_hidden_" + i).val() + ", ";
				}
			}
			
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
						$("#softwareKey_select_" + rowId).html(this.responseText);
						$("#softwareKey_select_" + rowId).select2({
							dropdownAutoWidth: true
						});
						$("#softwareKey_select_" + rowId).removeAttr("style");
						
					}
					
				else if (this.readyState == 4 && this.status != 200) {
					toastr.error('No Key Found');
				}
			};
			
			xhttp.open("GET", "Asset_modelServlet?actionType=getKeys&softwareCat=" + $("#" + softwareCatId).val() 
					+ "&softwareSubCat=" + $("#" + softwareSubCatId).val() 
					+ "&license=" + $("#" + licenseId).val() 
					+ "&usedAssigneeIds=" + usedAssigneeIds, true);
			
			
			xhttp.send();
		}
		
	}
	
	function getAssignee(idType, fieldId)
	{
		console.log("getAssignee called");
		var modelId = "";
		var slId = "";
		var softwareCatId = "softwareCat_select_";
		var softwareSubCatId = "softwareSubCat_select_";
		var licenseId = "license_select_";
		var softwareKeyId = "softwareKey_select_";
		var rowId = fieldId.split("_")[2];
		if(idType == 1)//modelId
		{
			modelId = fieldId;
			slId = "sl_text_" + rowId;
		}
		else if(idType == 3)
		{
			softwareSubCatId = fieldId;
			slId = "sl_text_" + rowId;
			modelId = "model_select_" + rowId;
			softwareCatId = softwareCatId + rowId;
			licenseId = licenseId + rowId;
		}
		else if(idType == 4)
		{
			licenseId = fieldId;
			slId = "sl_text_" + rowId;
			modelId = "model_select_" + rowId;
			softwareCatId = softwareCatId + rowId;
			softwareSubCatId = softwareSubCatId + rowId;
			softwareKeyId = softwareKeyId + rowId;
		}
		else if(idType == 5)
		{
			softwareKeyId = fieldId;
			slId = "sl_text_" + rowId;
			modelId = "model_select_" + rowId;
			licenseId = licenseId + rowId;
			softwareCatId = softwareCatId + rowId;
			softwareSubCatId = softwareSubCatId + rowId;
			
		}
		else
		{
			slId = fieldId;
			modelId = "model_select_" + rowId;
		}
		
		$("#" + slId).val("");//we clear sl 1st
		var model = $("#" + modelId).val();
		var sl = $("#" + slId).val();
		$("#assetAssignee_hidden_" + rowId).val("-1");
		console.log("modelId = " + modelId + " slId = " + slId);
		
		var usedAssigneeIds = "";
		
		for(var i = 0; i < $("#childTableStartingID").val(); i ++)
		{
			if($("#assetAssignee_hidden_" + i).length && $("#assetAssignee_hidden_" + i).val() != "")
			{
				usedAssigneeIds += $("#assetAssignee_hidden_" + i).val() + ", ";
			}
		}
		
		
		console.log("model = " + model + " sl = " + sl + " usedAssigneeIds = " + usedAssigneeIds);
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var assetAssigneeDTO = JSON.parse(this.responseText);
				if(assetAssigneeDTO == null)
				{
					toastr.error('No Asset Found');
					$("#productDetails_" + rowId).css("display", "none");
				}
				else
				{
					Object.keys(assetAssigneeDTO).forEach(function(key) 
					{
						if(key != "model")
						{
							if($("#" + key + "_text_" + rowId).length)
							{
								$("#" + key + "_text_" + rowId).val(assetAssigneeDTO[key]);
								console.log("adding " + key + " and value " +  assetAssigneeDTO[key]);
							}
							else if($("#" + key + "_select_" + rowId).length)
							{
								setValueToSelect(key + "_select_" + rowId, assetAssigneeDTO[key], "");
							}
							else if($("#" + key + "_hidden_" + rowId).length)
							{
								$("#" + key + "_hidden_" + rowId).val(assetAssigneeDTO[key]);
								console.log("adding " + key + " and value " +  assetAssigneeDTO[key]);
							}
							else
							{
								console.log("not adding " + key + " and value " +  assetAssigneeDTO[key]);
							}
						}
					});						
					
					
				
					$("#assetAssignee_hidden_" + rowId).val(assetAssigneeDTO.iD);
					console.log("id fetched = " + assetAssigneeDTO.iD);
					console.log("cat fetched = " + assetAssigneeDTO.assetCategoryType);
					showHideDivsForAssignment($("#cat_"+ rowId).val(), "AssetAssignee_" + rowId);
					fetchParents(rowId);
					$("#productDetails_" + rowId).css("display", "block");
					
				}
				
			} else if (this.readyState == 4 && this.status != 200) {
				toastr.error('No Asset Found');
			}
		};
		if(idType == 1 || idType == 2)
		{
			xhttp.open("GET", "Asset_modelServlet?actionType=getOneDTOByModel&model=" + model 
					+ "&sl=" + sl 
					+ "&usedAssigneeIds=" + usedAssigneeIds, true);
		}
		else
		{
			xhttp.open("GET", "Asset_modelServlet?actionType=getOneSoftware&softwareCat=" + $("#" + softwareCatId).val() 
					+ "&softwareSubCat=" + $("#" + softwareSubCatId).val() 
					+ "&license=" + $("#" + licenseId).val() 
					+ "&assetAssigneeIdWithKey=" + $("#" + softwareKeyId).val() 
					+ "&usedAssigneeIds=" + usedAssigneeIds, true);
		}
		
		xhttp.send();
	}
	 
	function assigneeRemoved(removeId)
	{
		var rowId = removeId.split("_")[1]; 
		
		
		if($("#assetAssignee_hidden_" + rowId).val() != -1)
		{
			for(var i = 0; i < $("#childTableStartingID").val(); i ++)
			{
				if($("#parentAssigneeId_select_" + i).val() == $("#assetAssignee_hidden_" + rowId).val())
				{
					$("#AssetAssignee_" + i).remove();
				}
			}
		}
		
		$("#AssetAssignee_" + rowId).remove();

	}
	
	function hideKeySelect(id, type)
	{
		var rowId = id.split("_")[2]; 
		var licenseSelect = type + "KeyId_select_" + rowId;
		$("#" + licenseSelect).css("display:none");
	}
	
	
    </script>
