<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page contentType="text/html;charset=utf-8" %>


<%
	System.out.println("Inside nav.jsp");
	String url = "Asset_modelServlet?actionType=search";	
%>
<%@include file="../pb/navInitializer.jsp"%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
				<input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text' class='form-control border-0'
					   onKeyUp='allfield_changed("",0)' id='anyfield'  name='anyfield'
					   value = '<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
				>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
<%--        <div class="kt-portlet__head-toolbar">--%>
<%--            <div class="kt-portlet__head-group">--%>
<%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
<%--                     aria-hidden="true" x-placement="top"--%>
<%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
<%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
<%--                    <div class="tooltip-inner">Collapse</div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
    </div>        
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

			<div class="row">
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, loginDTO)%></label>					
						<div class="col-md-8">
							<select class='form-control'  name='asset_category_type' id = 'asset_category_type' onSelect='setSearchChanged()'>
								<%
																	Options = CommonDAO.getOptions(Language, "select", "asset_category", "assetCategoryType_select_" + row, "form-control", "assetCategoryType", "any" );
																%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_LOT, loginDTO)%></label>					
						<div class="col-md-8">
							<input type="text" class="form-control" id="lot" placeholder="" name="lot" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_BRANDTYPE, loginDTO)%></label>					
						<div class="col-md-8">
							<select class='form-control'  name='brand_type' id = 'brand_type' onSelect='setSearchChanged()'>
								<%
																	Options = CommonDAO.getOptions(Language, "select", "brand", "brandType_select_" + row, "form-control", "brandType", "any" );
																%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_NAMEEN, loginDTO)%></label>					
						<div class="col-md-8">
							<input type="text" class="form-control" id="name_en" placeholder="" name="name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_SL, loginDTO)%></label>					
						<div class="col-md-8">
							<input type="text" class="form-control" id="sl" placeholder="" name="sl" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_VENDORCAT, loginDTO)%></label>					
						<div class="col-md-8">
							<select class='form-control'  name='vendor_cat' id = 'vendor_cat' onSelect='setSearchChanged()'>
								<%										
									Options = CatRepository.getInstance().buildOptions("vendor", Language, -1);
								%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_PONUMBER, loginDTO)%></label>					
						<div class="col-md-8">
							<input type="text" class="form-control" id="po_number" placeholder="" name="po_number" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.ASSET_MODEL_ADD_RECEIVINGDATE, loginDTO)%></label>					
						<div class="col-md-8">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="receiving_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="receiving_date_start" name="receiving_date_start">
						</div>
					</div>
				</div>			

				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.ASSET_MODEL_ADD_RECEIVINGDATE, loginDTO)%></label>
						<div class="col-md-8">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="receiving_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="receiving_date_end" name="receiving_date_end">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_PROCUREMENTTYPECAT, loginDTO)%></label>					
						<div class="col-md-8">
							<select class='form-control'  name='procurement_type_cat' id = 'procurement_type_cat' onSelect='setSearchChanged()'>
								<%										
									Options = CatRepository.getInstance().buildOptions("procurement_type", Language, -1);
								%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_PROJECTNAME, loginDTO)%></label>					
						<div class="col-md-8">
							<input type="text" class="form-control" id="project_name" placeholder="" name="project_name" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSETMODELSTATUSCAT, loginDTO)%></label>					
						<div class="col-md-8">
							<select class='form-control'  name='asset_model_status_cat' id = 'asset_model_status_cat' onSelect='setSearchChanged()'>
								<%										
									Options = CatRepository.getInstance().buildOptions("asset_model_status", Language, -1);
								%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>

		</div>
		<div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0, 0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                    
                </div>
            </div>
        </div>
        
    </div>
    
</div>

<div style="padding-right: 20px; padding-bottom: 20px">
	<button type="button"
	        class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
	        onclick="allfield_changed('',0, 1)"
	        style="background-color: #a8b194; float: right;">
	    <%=LM.getText(LC.HM_EXCEL, loginDTO)%>
	</button>
</div>

<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>


<script type="text/javascript">

	function dosubmit(params)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		//alert(params);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) 
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		  
		  
		  xhttp.open("GET", "Asset_modelServlet?actionType=search&isPermanentTable=<%=isPermanentTable%>&" + params, true);
		  
		  
		  xhttp.send();
		
	}

	function allfield_changed(go, pagination_number, type)
	{
		var params = 'AnyField=' + document.getElementById('anyfield').value;

		if($("#asset_category_type").val() != -1 && $("#asset_category_type").val() != "")
		{
			params +=  '&asset_category_type='+ $("#asset_category_type").val();
		}
		params +=  '&lot='+ $('#lot').val();
		if($("#brand_type").val() != -1 && $("#brand_type").val() != "")
		{
			params +=  '&brand_type='+ $("#brand_type").val();
		}
		params +=  '&name_en='+ $('#name_en').val();
	
		params +=  '&sl='+ $('#sl').val();
		
		if($("#vendor_cat").val() != -1 && $("#vendor_cat").val() != "")
		{
			params +=  '&vendor_cat='+ $("#vendor_cat").val();
		}
		params +=  '&po_number='+ $('#po_number').val();
		$("#receiving_date_start").val(getDateStringById('receiving_date_start_js', 'DD/MM/YYYY'));
		params +=  '&receiving_date_start='+ getBDFormattedDate('receiving_date_start');
		$("#receiving_date_end").val(getDateStringById('receiving_date_end_js', 'DD/MM/YYYY'));
		params +=  '&receiving_date_end='+ getBDFormattedDate('receiving_date_end');		
		if($("#procurement_type_cat").val() != -1 && $("#procurement_type_cat").val() != "")
		{
			params +=  '&procurement_type_cat='+ $("#procurement_type_cat").val();
		}
	
		params +=  '&project_name='+ $('#project_name').val();

		if($("#asset_model_status_cat").val() != -1 && $("#asset_model_status_cat").val() != "")
		{
			params +=  '&asset_model_status_cat='+ $("#asset_model_status_cat").val();
		}
		
		
		params +=  '&search=true&ajax=true';
		
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged == 0)
		{
			console.log("go found");
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		
		if(type == 0 || typeof(type) == "undefined")
		{
			dosubmit(params);
		}
		else
		{
			var url =  "Asset_modelServlet?actionType=xl&isPermanentTable=<%=isPermanentTable%>&" + params;
			window.location.href = url;
		}
		
	
	}

</script>

