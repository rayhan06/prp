

<%@page import="user.UserRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_model.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@ page import="office_units.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@page import="asset_category.*"%>
<%@page import="user.*"%>



<%
String servletName = "Asset_modelServlet";
Asset_modelDAO asset_modelDAO = new Asset_modelDAO("asset_model");
long organogramId = (long)request.getAttribute("organogramId");
UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(organogramId);
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String value = "";
String Language = LM.getLanguage(loginDTO);
String Value = "";
int i = 0;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
String name = WorkflowController.getNameFromOrganogramId(organogramId, Language);
%>

<style>
    .rounded-border {
        border-radius: 8px!important;
    }

    .bg-primary-custom {
        background: #EDF8FC!important;
        color: #727476 !important;
    }

    .section-title {
        background: #EDF8FC!important;
        border: 4px solid #CDECF7!important;
        border-radius: 8px!important;
        color: #727476 !important;
        text-transform: capitalize!important;
    }

</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=name%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
        	<jsp:include page="../asset_model/userAssetDetails.jsp" /> 
        	
        	
		</div>
	</div>
</div>

<script type="text/javascript">
<%@include file="../asset_model/assetViewJs.jsp"%>
</script>
