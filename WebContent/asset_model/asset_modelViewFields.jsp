								<%@page import="pb.*"%>
								<%@page import="language.LC" %>
<input type='hidden'  name='assetCategoryType' id = 'assetCategoryType' value='<%=asset_modelDTO.assetCategoryType%>'  tag='pb_html' />
																
								<div class="form-group row">
                                     <label class="col-md-4 col-form-label text-md-right"><b></b></label>
                                     <div class="col-md-8">
                                         <h4>
                                              <%=LM.getText(LC.HM_ASSET_DETAILS, loginDTO)%>
                                         </h4>
                                     </div>
                                 </div>
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.assetCategoryType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "asset_category", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.HM_ASSET_TYPE, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">

											<%
											value = CommonDAO.getName(Language, "asset_type", asset_modelDTO.assetTypeId);
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.HM_PRODUCT_NAME, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.productName + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=Language.equalsIgnoreCase("english")?"Warehouse Address":"ওয়্যারহাউজের ঠিকানা"%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.warehouseAddress + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_BRANDTYPE, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.brandType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "brand", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.HM_NAME, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.nameEn + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>

			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_SL, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.sl + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_QUANTITY, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											
				
											<%=Utils.getDigits(asset_modelDTO.quantity, Language)%>
				
			
                                    </div>
                                </div>
                                
                                
                                
				
									<div class="form-group row d-flex align-items-center" id="processorDiv">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.ASSET_MODEL_ADD_PROCESSOR, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "processor", asset_modelDTO.processorCat) 
												+ " " + CatRepository.getInstance().getText(Language, "processor_gen", asset_modelDTO.processorGenCat);
												%>
					
												<%=value%>
					
				
	                                    </div>
	                                </div>
				
									<div class="form-group row d-flex align-items-center" id="ramDiv">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.ASSET_MODEL_ADD_RAM, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "ram", asset_modelDTO.ramCat)
												+ " " + CatRepository.getInstance().getText(Language, "ram_size", asset_modelDTO.ramSizeCat);
												%>	
										
												<%=value%>
					
				
	                                    </div>
	                                </div>
				
									<div class="form-group row d-flex align-items-center" id="hdDiv">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.ASSET_MODEL_ADD_HDNAME, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "hd", asset_modelDTO.hdCat) +
												" " + CatRepository.getInstance().getText(Language, "hd_size", asset_modelDTO.hdSizeCat);
												%>	
										
												<%=value%>
					
				
	                                    </div>
	                                </div>
                                
                                
                                <div id = "monitorDiv">
                                	<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_MONITOR, loginDTO)%> <%=LM.getText(LC.HM_TYPE, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "monitor_type", asset_modelDTO.monitorTypeCat);
												%>	
										
												<%=value%>
					
				
	                                    </div>
	                                </div>
                                </div>
                                
                                 <div id = "dcDiv">
                                	<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=Language.equalsIgnoreCase("English")?"Data Centre Type":"ডেটা সেন্টারের ধরন" %>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "data_center_type", asset_modelDTO.dataCenterTypeCat);
												%>	
										
												<%=value%>
					
				
	                                    </div>
	                                </div>
                                </div>
                                
                                <div id = "printerDiv">
                                	<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_PRINTER_TYPE, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "printer_network_type", asset_modelDTO.printerNetworkTypeCat)
												+ " " + CatRepository.getInstance().getText(Language, "printer_type", asset_modelDTO.printerTypeCat);
												%>	
										
												<%=value%>
	                                    </div>
	                                </div>
                                </div>
                                
                                <div id = "scannerDiv">
                                	<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_SCANNER_TYPE, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "scanner_type", asset_modelDTO.scannerTypeCat);
												%>	
										
												<%=value%>
	                                    </div>
	                                </div>
                                </div>
                                
                                <div id = "laptopMonitorDiv">
                                	<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.ASSET_MODEL_ADD_DISPLAYSIZE, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "display_size", asset_modelDTO.displaySizeCat);
												%>	
										
												<%=value%>
					
				
	                                    </div>
	                                </div>
                                </div>
			
								<div id = "upsDiv">
									<div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><b></b></label>
                                        <div class="col-md-8">
                                            <h4>
                                                <%=LM.getText(LC.HM_UPS_SPECS, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
									<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.ASSET_MODEL_ADD_CAPACITY, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "ups_capacity", asset_modelDTO.upsCapacityCat);
												%>	
										
												<%=value%>
					
				
	                                    </div>
	                                </div>
	                                
	                                <div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_UPS_TYPE, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = CatRepository.getInstance().getText(Language, "ups_type", asset_modelDTO.upsTypeCat);
												%>	
										
												<%=value%>
					
				
	                                    </div>
	                                </div>
				
									<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.ASSET_MODEL_ADD_IDFNUMBER, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = asset_modelDTO.idfNumber + "";
												%>
					
												<%=value%>
					
				
	                                    </div>
	                                </div>
                                </div>
                                
                                <div id = "dCSwitchDiv">
									
									<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_MAC, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = asset_modelDTO.mac + "";
												%>
					
												<%=value%>
					
				
	                                    </div>
	                                </div>
	                                <div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        IP
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = asset_modelDTO.ip + "";
												%>
					
												<%=value%>
					
				
	                                    </div>
	                                </div>
                                </div>
                                
                                <div id = "dCSwitchNACamDiv">
									
									<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_LOCATION, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = asset_modelDTO.location + "";
												%>
					
												<%=value%>
					
				
	                                    </div>
	                                </div>
	                                
	                                <div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_PART_NUMBER, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = asset_modelDTO.partNumber + "";
												%>
					
												<%=value%>
					
				
	                                    </div>
	                                </div>
                                </div>
                                
                                <div id = "switchDiv">
									
									<div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_HOST_NAME, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = asset_modelDTO.hostName + "";
												%>
					
												<%=value%>
					
				
	                                    </div>
	                                </div>
	                                
	                                <div class="form-group row d-flex align-items-center">
	                                    <label class="col-md-4 col-form-label text-md-right"><b>
	                                        <%=LM.getText(LC.HM_MANAGEMENT_VLAN, loginDTO)%>
	                                    </b></label>
	                                    <div class="col-md-8">
												<%
												value = asset_modelDTO.managementVlan + "";
												%>
					
												<%=value%>
					
				
	                                    </div>
	                                </div>
                                </div>
                                
                                <%
                                if(asset_modelDTO.purchaseInformationAvailable)
                                {
                                %>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right"><b></b></label>
                                    <div class="col-md-8">
                                        <h4>
                                            <%=LM.getText(LC.HM_PURCHASE_DETAILS, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_LOT, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.lot + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
								
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_VENDORCAT, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											
											<%
											value = CommonDAO.getName(Language, "asset_supplier", asset_modelDTO.assetSupplierType);
											%>	
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_PONUMBER, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.poNumber + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_RECEIVINGDATE, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.receivingDate + "";
											%>
											<%
											String formatted_receivingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_receivingDate, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_PROCUREMENTTYPECAT, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.procurementTypeCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "procurement_type", asset_modelDTO.procurementTypeCat);
											%>	
				
											<%=value%>
											
											
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=Language.equalsIgnoreCase("English")?"Has TO&E":"টি ও ই আছে" %>
                                    </b></label>
                                    <div class="col-md-8">				
											<%=Utils.getYesNo(asset_modelDTO.hasTOE, Language)%>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=Language.equalsIgnoreCase("English")?"Has Warranty":"ওয়্যারেন্টি আছে" %>
                                    </b></label>
                                    <div class="col-md-8">				
											<%=Utils.getYesNo(asset_modelDTO.hasWarranty, Language)%>
                                    </div>
                                </div>
                                <%
                                if(asset_modelDTO.hasWarranty)
                                {
                                	%>
                               <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=Language.equalsIgnoreCase("English")?"Warranty Expiry Date":"ওয়ার‍্যান্টি এক্সপায়ারি ডেট" %>
                                    </b></label>
                                    <div class="col-md-8">				
											<%
											value = asset_modelDTO.expiryDate + "";
											%>
											<%
											String formatted_expiryDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_expiryDate, Language)%>
                                    </div>
                                </div>
                                	<%
                                }
                                %>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_PROJECTNAME, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.projectName + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_COSTINGVALUE, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.costingValue + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right"><b>
                                        <%=LM.getText(LC.ASSET_MODEL_ADD_ASSETMODELSTATUSCAT, loginDTO)%>
                                    </b></label>
                                    <div class="col-md-8">
											<%
											value = asset_modelDTO.assetModelStatusCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "asset_model_status", asset_modelDTO.assetModelStatusCat);
											%>	
				
											<%=value%>
				
			
                                    </div>
                                </div>
                                <%
                                }
                                %>
			
			