function showHideDivs()
{
	var assetCat = $("#assetCategoryType").val();
	if(assetCat == <%=Asset_categoryDTO.LAPTOP%>)
	{
		$("#processorDiv").removeAttr("style")
		$("#ramDiv").removeAttr("style")
		$("#hdDiv").removeAttr("style")
		
		$("#laptopMonitorDiv").css("display", "block");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').removeAttr("style");
		$('[type="thTdLaptopCpu"]').removeAttr("style");
		$('[type="thTdAp"]').css("display", "none");
		
				
	}
	else if(assetCat == <%=Asset_categoryDTO.MONITOR%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "block");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "block");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");
	
	}
	else if(assetCat == <%=Asset_categoryDTO.CPU%>)
	{
		$("#processorDiv").removeAttr("style")
		$("#ramDiv").removeAttr("style")
		$("#hdDiv").removeAttr("style")
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').removeAttr("style");
		$('[type="thTdLaptopCpu"]').removeAttr("style");
		$('[type="thTdAp"]').css("display", "none");
						
	}
	else if(assetCat == <%=Asset_categoryDTO.UPS%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "block");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");
	
	}
	else if(assetCat == <%=Asset_categoryDTO.ACCESS_POINT%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "block");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').removeAttr("style");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').removeAttr("style");
	
	}
	else if(assetCat == <%=Asset_categoryDTO.DATA_CENTRE%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "block");
		$("#dCSwitchNACamDiv").css("display", "block");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "block");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");
	
	}
	else if(assetCat == <%=Asset_categoryDTO.SWITCH%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "block");
		$("#dCSwitchNACamDiv").css("display", "block");
		$("#switchDiv").css("display", "block");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");
	
	}
	else if(assetCat == <%=Asset_categoryDTO.NETWORK_ACCESSORIES%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "block");
		$("#switchDiv").css("display", "none");

		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");
	
	}
	else if(assetCat == <%=Asset_categoryDTO.CAMERA_AND_MONITORING%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "block");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");
	
	}
	else if(assetCat == <%=Asset_categoryDTO.PRINTER%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "block");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");

	}
	else if(assetCat == <%=Asset_categoryDTO.SCANNER%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "block");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");

	}
	else if(assetCat == <%=Asset_categoryDTO.PROCESSOR%>)
	{
		$("#processorDiv").removeAttr("style")
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");

	}
	else if(assetCat == <%=Asset_categoryDTO.RAM%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").removeAttr("style")
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");

	}
	else if(assetCat == <%=Asset_categoryDTO.HARD_DISK%>)
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").removeAttr("style")
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");

	}
	else
	{
		$("#processorDiv").css("display", "none");
		$("#ramDiv").css("display", "none");
		$("#hdDiv").css("display", "none");
		
		$("#laptopMonitorDiv").css("display", "none");
		$("#upsDiv").css("display", "none");
		$("#dCSwitchDiv").css("display", "none");
		$("#dCSwitchNACamDiv").css("display", "none");
		$("#switchDiv").css("display", "none");
		
		$("#monitorDiv").css("display", "none");
		$("#printerDiv").css("display", "none");
		$("#scannerDiv").css("display", "none");
		$("#dcDiv").css("display", "none");
		
		$('[type="thTdLaptopCpuAp"]').css("display", "none");
		$('[type="thTdLaptopCpu"]').css("display", "none");
		$('[type="thTdAp"]').css("display", "none");

	}
}

function showHideDivsForAssignment(assetCat, parent)
    {
		console.log("assetCat = " + assetCat + " parent = " + parent);
    	if(assetCat == <%=Asset_categoryDTO.LAPTOP%>)
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').removeAttr("style");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').removeAttr("style");    
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");
				
    	}
    	else if(assetCat == <%=Asset_categoryDTO.MONITOR%>)
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");     
    	
    	}
    	else if(assetCat == <%=Asset_categoryDTO.CPU%>)
    	{
 
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').removeAttr("style");
    		
    		$("#" + parent).find('[type="thTdCpu"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdCpuAp"]').removeAttr("style");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').removeAttr("style");
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdCpuApNet"]').removeAttr("style"); 
    						
    	}
    	else if(assetCat == <%=Asset_categoryDTO.UPS%>)
    	{
    		
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none"); 
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none"); 
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  
    	
    	}
    	else if(assetCat == <%=Asset_categoryDTO.ACCESS_POINT%>)
    	{    		
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').removeAttr("style");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').removeAttr("style");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none"); 
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').removeAttr("style");  
    	}
    	else if(assetCat == <%=Asset_categoryDTO.DATA_CENTRE%>)
    	{
    		
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");   
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  	
    	}
    	else if(assetCat == <%=Asset_categoryDTO.SWITCH%>)
    	{    		
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none"); 
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");   
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  	
    	}
    	else if(assetCat == <%=Asset_categoryDTO.NETWORK_ACCESSORIES%>)
    	{
   		
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none")
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");   
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdCpuApNet"]').removeAttr("style");    	
    	}
    	else if(assetCat == <%=Asset_categoryDTO.CAMERA_AND_MONITORING%>)
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");  
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  
    	
    	}
    	else if(assetCat == <%=Asset_categoryDTO.PRINTER%>)
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').removeAttr("style");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none"); 
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  
    	}
    	else if(assetCat == <%=Asset_categoryDTO.RAM%>)
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");  
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  
    	}
    	else if(assetCat == <%=Asset_categoryDTO.HARD_DISK%>)
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').removeAttr("style");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");  
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  
    	}
    	else if(assetCat == <%=Asset_categoryDTO.PROCESSOR%>)
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdSoft"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').removeAttr("style");  
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  
    	}
    	else if(assetCat == <%=Asset_categoryDTO.SOFTWARE%>)
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		if($("#" + parent).find('[type="thTdSoft"]').val() != "")
    		{
    			$("#" + parent).find('[type="thTdSoft"]').removeAttr("style");
    		}
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");  
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  
    	}
    	else
    	{
    		$("#" + parent).find('[type="thTdLaptopCpuAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopMonitor"]').css("display", "none");
    		$("#" + parent).find('[type="thTdAp"]').css("display", "none");
    		$("#" + parent).find('[type="thTdUps"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuApPrinter"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdCpu"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuAp"]').css("display", "none");
    		
    		$("#" + parent).find('[type="thTdLaptopCpuRam"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuHd"]').css("display", "none");
    		$("#" + parent).find('[type="thTdLaptopCpuProcessor"]').css("display", "none");  
    		
    		$("#" + parent).find('[type="thTdCpuNet"]').css("display", "none");
    		$("#" + parent).find('[type="thTdCpuApNet"]').css("display", "none");  
    	}
	}