<%@ page import="java.util.*"%>
<%@page import="asset_model.*"%>
<%@page import="asset_category.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="user.*"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="util.*"%>
<%@page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@page pageEncoding="UTF-8" %>
<%
	AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
long organogramId = (long)request.getAttribute("organogramId");
int ownerCat = (int)request.getAttribute("ownerCat");
int roomNoCat = (int)request.getAttribute("roomNoCat");
UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(organogramId);
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
List<AssetAssigneeDTO> userAssets = assetAssigneeDAO.getByResponsibleIdRoomAndOwner(organogramId, roomNoCat, ownerCat);
String Language = LM.getLanguage(loginDTO);
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
Asset_modelDAO asset_modelDAO = new Asset_modelDAO();
AssetAssigneeDTO lastAssetAssigneeDTO = assetAssigneeDAO.getLastByOrganogramId(organogramId);
if(lastAssetAssigneeDTO == null)
{
	lastAssetAssigneeDTO = new AssetAssigneeDTO();
}
String Options = "";
%>
				<div class="row">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row mx-4">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.HM_ASSET, loginDTO).toUpperCase()%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div id="field-AssetAssignee">
                                    <%
                                    int index = -1;
                                    int childTableStartingID= 0;


                                    for (AssetAssigneeDTO assetAssigneeDTO : userAssets) {
                                        index++;

                                        System.out.println("index index = " + index);
                                        Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(assetAssigneeDTO.assetModelId, false);
                                    %>
                                    <div id="AssetAssignee_<%=childTableStartingID%>" tag='pb_html'>
                                    		<div class="my-3">
                                                <h4 class="section-title d-inline-block px-4 py-2"
                                                    style="background:#e0f7fa">
                                                    <%=CommonDAO.getName(Language, "asset_type", asset_modelDTO.assetTypeId)%>
                                                    <%
                                                    if(asset_modelDTO.assetCategoryType != Asset_categoryDTO.SOFTWARE)
                                                    {
                                                    %>
                                                    : <%=CommonDAO.getName(Language, "asset_category", asset_modelDTO.assetCategoryType)%>
                                                    <%
                                                    }
                                                    %>
                                                    </h4>
                                            </div>
                                            
                                            <div class="row">

                                                        <input type='hidden' class='form-control'
                                                               name='assetAssignee.iD'
                                                               id='assetAssignee_hidden_<%=childTableStartingID%>'
                                                               value='<%=assetAssigneeDTO.iD%>' tag='pb_html'/>

                                                         <input type='hidden' class='form-control'
                                                               name='cat'
                                                               id='cat_<%=childTableStartingID%>'
                                                               value='<%=asset_modelDTO.assetCategoryType%>' tag='pb_html'/>
                                                               
                                                         <input type='hidden' class='form-control'
                                                               name='type'
                                                               id='type_<%=childTableStartingID%>'
                                                               value='<%=asset_modelDTO.assetTypeId%>' tag='pb_html'/>

                                                         <input type='hidden' class='form-control'
                                                               name='model'
                                                               id='model_select_<%=childTableStartingID%>'
                                                               value='<%=asset_modelDTO.iD%>' tag='pb_html'/>
                                                               
                                                          <input type='hidden' class='form-control'
                                                               name='softwareCat'
                                                               id='softwareCat_select_<%=childTableStartingID%>'
                                                               value='<%=asset_modelDTO.softwareCat%>' tag='pb_html'/>
                                                               
                                                           <input type='hidden' class='form-control'
                                                               name='model'
                                                               id='softwareSubCat_select_<%=childTableStartingID%>'
                                                               value='<%=asset_modelDTO.softwareSubCat%>' tag='pb_html'/>


                                                <div class="row col-md-6 mx-0 form-group">
                                                    <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                                                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_MODEL, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type='text' class='form-control' readonly
                                                               name='assetModelText'
                                                               id='assetModel_text_<%=childTableStartingID%>'
                                                               value='<%=asset_modelDTO.nameEn%>' tag='pb_html'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row col-md-6 mx-0 form-group">
                                                    <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
                                                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_SL, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-8">
                                                            <input type='text' class='form-control'
                                                                   name='assetAssignee.sl'
                                                                   id='sl_text_<%=childTableStartingID%>'
                                                                   value='<%=assetAssigneeDTO.sl%>' tag='pb_html'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%
                                                String childPrefix = childTableStartingID + "";
                                                %>
                                                <%@include file="../asset_model/assetEditCommonChildren.jsp"%>
                                                 <div class="row col-md-12 mx-0 form-group">


                                                    <div class="row col-md-12 mx-0 form-group">
							                          <div class="col-12 row mx-0 py-2 rounded-border">
							                              <strong class=" ml-auto text-danger col-form-label">
							                                  <%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_REMOVE, loginDTO)%>
							                              </strong>
							                              <div class="mt-1 ml-2">
							
							                                    <button id="remove_<%=childPrefix%>" name="remove" onclick="assigneeRemoved(this.id)"
							                         				type="button" class="btn btn-sm remove-btn shadow ml-2 pl-4" tag='pb_html'>
							                     					<i class="fa fa-trash"></i>
							                 					</button>
							
							                              </div>
							                          </div>
							                      </div>
                                                </div>
                                                <div class="row col-md-12 mx-0 form-group">
                                                    <div class="col-12 row mx-0 py-2 rounded-border">

                                                    </div>
                                                </div>
                                             </div>
                                        </div>

                                            <%
                                                        childTableStartingID++;
                                                 }

                                            %>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 mb-5 w-100 mx-0">
                    <div class="col-md-12 text-right pr-0">
                        <button id="add-more-AssetAssignee" name="add-moreAssetAssignee" onclick="assigneeAdded(event)"
                                type="button" class="btn btn-sm text-white add-btn shadow">

                            <i class="fa fa-plus"></i>
                            <%=LM.getText(LC.HM_ADD, loginDTO)%>
                        </button>
                    </div>
                </div>
                <input type = hidden id = "childTableStartingID" value = <%=childTableStartingID%> />
                <%AssetAssigneeDTO assetAssigneeDTO = new AssetAssigneeDTO();%>
                <template id="template-AssetAssignee">
                	<div tag='pb_html' a1="child">
                		<div class="row mx-0" >
                		<div class="col-md-6 d-flex align-items-center mb-4">
                		<div class=" ">
		                        <h4 class=" d-inline-block px-4 py-2 section-title"
		                            style="background:#e0f7fa">
		                             <select class='form-control' name='type'
		                                   id='type_' onchange="getCategories(this.id)" tag='pb_html'>
		                                   <%
		                                       Options = CommonDAO.getOptionsWithWhere(Language, "asset_type", -1, "");
		                                   %>
		                                   <%=Options%>
		                               </select>
		                            </h4>
		                    </div>
		                    <div class=" ml-2" style = "display:none" tag='pb_html' id = 'catDiv_'>
		                        <h4 class=" d-inline-block px-4 py-2 section-title"
		                            style="background:#e0f7fa">
		                             <select class='form-control' name='cat'
		                                   id='cat_' onchange="getModels(this.id)" tag='pb_html'>
		                                   <%
		                                       Options = CommonDAO.getOptionsWithWhere(Language, "asset_category", -1, "is_assignable = 1");
		                                   %>
		                                   <%=Options%>
		                               </select>
		                            </h4>
		                    </div>
	                    
                		</div>
	                		</div>
	                    <div class="row" style = "display:none" tag='pb_html' id = 'innererDiv_'>
	                    	<div id = 'header_' tag='pb_html' class="row col-md-12 mx-0 form-group">
		                    	<div class="row col-md-6 mx-0" id = 'modelDiv_' style = "display:none" tag='pb_html'>
		                             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
		                                 <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_MODEL, loginDTO)%>
		                                 </label>
		                                 <div class="col-md-8">
		                                 	<select class='form-control' name='model' id='model_select_' tag='pb_html' onchange="getAssignee(1, this.id)">
		                                 	</select>

		                                 </div>
		                             </div>
		                         </div>
		                         
		                         <div class="row col-md-6 mx-0" id = 'softwareCatDiv_' style = "display:none" tag='pb_html'>
		                             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
		                                 <label class="col-md-4 col-form-label"><%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARECAT, loginDTO)%>
		                                 </label>
		                                 <div class="col-md-8">
		                                 	<select class='form-control'  name='softwareCat' id = 'softwareCat_select_' onchange="getSubCat(this.id)"  tag='pb_html'>		
											<%
												Options = CatDAO.getOptions( Language, "software", -1);
											%>
											<%=Options%>
											</select>

		                                 </div>
		                             </div>
		                         </div>
		                         
		                         <div class="row col-md-6 mx-0" id = 'softwareSubCatDiv_' style = "display:none" tag='pb_html'>
		                             <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
		                                 <label class="col-md-4 col-form-label"><%=LM.getText(LC.SOFTWARE_TYPE_ADD_SOFTWARESUBCAT, loginDTO)%>
		                                 </label>
		                                 <div class="row">
			                                 <div class="col-md-6">
			                                 	<select class='form-control' name='softwareSubCat' id='softwareSubCat_select_' tag='pb_html' >
			                                 	</select>
	
			                                 </div>
			                                 <div class="col-md-6">
			                                 	<select class='form-control' name='licenseType' id='license_select_' tag='pb_html' onchange="getKeys(this.id)">
			                                 		<option value ="-1"><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
			                                 		<option value ="0"><%=Language.equalsIgnoreCase("English")?"No License":"লাইসেন্স নেই" %></option>
			                                 		<option value ="1"><%=Language.equalsIgnoreCase("English")?"Has License":"লাইসেন্স আছে" %></option>
			                                 	</select>
	
			                                 </div>
			                                 <div class="col-md-12">
			                                 	<select class='form-control' name='softwareKey' id='softwareKey_select_'
			                                 	style = "display:none"
			                                 	 tag='pb_html' onchange="getAssignee(5, this.id)">
			                                 		<option value = '-1'></option>
			                                 	</select>
	
			                                 </div>
		                                 </div>
		                             </div>
		                         </div>

		                         <div class="row col-md-6 mx-0" style = "display:none" tag='pb_html' id = 'slDiv_'>
		                                  <div class="bg-primary-custom col-12 row mx-0 py-2 rounded-border">
		                                      <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_SL, loginDTO)%>
		                                      </label>
		                                      <div class="col-md-8">
		                                          <input type='text' class='form-control'
		                                                 name='assetAssignee.sl'
		                                                 id='sl_text_'
		                                                 
		                                                 value='' tag='pb_html'/>
		                                      </div>
		                                  </div>
		                          </div>
	                         </div>
	                         <div id = 'productDetails_' tag='pb_html' class="col-md-12 mx-0 form-group" style = "display:none">
	                         		
		                         	<input type='hidden' class='form-control'
                                                               name='assetAssignee.iD'
                                                               id='assetAssignee_hidden_'
                                                               value='-1' tag='pb_html'/>

	                              <%
                                   String childPrefix = "";
                                  %>
                                  <%@include file="../asset_model/assetEditCommonChildren.jsp"%>

                             </div>

	                    </div>
	                    <div class="row col-md-12 mx-0 form-group">
                          <div class="col-12 row mx-0 py-2 rounded-border">
                              <strong class=" ml-auto text-danger col-form-label">
                                  <%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_REMOVE, loginDTO)%>
                              </strong>
                              <div class="mt-1 ml-2">

                                    <button id="remove_" name="remove" onclick="assigneeRemoved(this.id)"
                         				type="button" class="btn btn-sm remove-btn shadow ml-2 pl-4" tag='pb_html'>
                     					<i class="fa fa-trash"></i>
                 					</button>

                              </div>
                          </div>
                      </div>
                    </div>
                </template>