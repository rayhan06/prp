<%@ page import="java.util.*"%>
<%@page import="asset_model.*"%>
<%@page import="asset_category.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="user.*"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="util.*"%>
<%@page import="pb.*"%>
<%@page import="workflow.*"%>
<%@page import="employee_offices.*"%>
<%@page import="office_unit_organograms.*"%>
<%@page import="office_units.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
	AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
long organogramId = (long)request.getAttribute("organogramId");
int ownerCat = AssetAssigneeDTO.OWNER_PERSON;
int roomNoCat = -1;
int type = Asset_modelDTO.PAGE_ASSET;
if(request.getAttribute("type") != null)
{
	type = (int)request.getAttribute("type");
}
boolean isAssetPage = type == Asset_modelDTO.PAGE_ASSET;

boolean useOldMethod = false;
System.out.println("owner = " + request.getAttribute("ownerCat") );
if(request.getAttribute("ownerCat") != null)
{	
	ownerCat = (int)request.getAttribute("ownerCat");
}
else
{
	useOldMethod= true;
}
if(request.getAttribute("roomNoCat") != null)
{
	roomNoCat = (int)request.getAttribute("roomNoCat");
}

UserDTO userDTO = UserRepository.getUserDTOByOrganogramID(organogramId);
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
List<AssetAssigneeDTO> userAssets;
String assetsFromRequest = (String)request.getAttribute("assets");
System.out.println("assetsFromRequest = " + assetsFromRequest);
if(assetsFromRequest!= null && !assetsFromRequest.equalsIgnoreCase("") && !assetsFromRequest.equalsIgnoreCase("-1"))
{
	if(assetsFromRequest.endsWith(","))
	{
		assetsFromRequest = assetsFromRequest.substring(0, assetsFromRequest.length() - 1);	
	}
	System.out.println("assetsFromRequest = " + assetsFromRequest);
	userAssets = assetAssigneeDAO.getByList(assetsFromRequest);
}
else
{
	if(useOldMethod)
	{
		userAssets = assetAssigneeDAO.getByOrganogramId(organogramId);
	}
	else
	{
		userAssets = assetAssigneeDAO.getByResponsibleIdRoomAndOwner(organogramId, roomNoCat, ownerCat);
	}
	
}

String Language = LM.getLanguage(loginDTO);
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
Asset_modelDAO asset_modelDAO = new Asset_modelDAO();
UserDTO empUserDTO = UserRepository.getUserDTOByOrganogramID(organogramId);
long employeeRecordId = -1;
if(empUserDTO != null)
{
	employeeRecordId = empUserDTO.employee_record_id;
}

boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<div class="ml-auto mr-3">

	<button type="button" class="btn" id='printer2'
            onclick="location.href='Asset_modelServlet?actionType=search'">
        <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>

    <button type="button" class="btn" id='printer2'
            onclick="location.href='Asset_modelServlet?actionType=getAddUserAssetsPage'">
        <i class="fa fa-plus fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    
    <button type="button" class="btn" id='printer2'
            onclick="printAnyDiv('assetDiv')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>
<div class="mt-5" id = 'assetDiv'>
		<div class="my-3 text-center">
			<h3 class="font-weight-bold"><%=WorkflowController.getNameFromOrganogramId(organogramId, Language)%></h3>			
			<h4 class="font-weight-bold"><%=WorkflowController.getOfficeTypeNameFromOrganogramId(organogramId, Language)%></h4>
			<h5 class="font-weight-bold"><%=WorkflowController.getWingNameFromOrganogramId(organogramId, Language)%></h5>
		</div>
        	<%
        	if(!isAssetPage)
    		{
        		%>
        		<div class = "row">
        		<%
    		}
        	int i = 0;
        	for (AssetAssigneeDTO assetAssigneeDTO : userAssets)
        	{
        		Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(assetAssigneeDTO.assetModelId, false);
        		if(isAssetPage)
        		{

        			
        		%>
        			<input type = "hidden"  id = "cat_<%=i%>" value = "<%=assetAssigneeDTO.assetCategoryType %>">
                	<div class=" div_border attachement-div" id = "AssetAssignee_<%=i%>">

						<%@include file="../asset_model/viewParticularAsset.jsp"%>
					</div>
				<%
        		}
        		else
        		{
        			String qrContent = assetAssigneeDAO.getQrContents(assetAssigneeDTO);
        			String QR = QRCodeUtil.getQrCodeAsBase64(qrContent);
        			%>
        			<div class = "col-3">
        				<table>
        					<tr> <td><img style="height: 200px;" src="<%= QR %>"/> </td> </tr>
        					<tr> <td style = "text-align: center"> 
        					<%=CommonDAO.getName("english", "asset_category", assetAssigneeDTO.assetCategoryType)%>:
        					<%=assetAssigneeDTO.model%></td> </tr>
        				</table>
        			</div>
        			<%
        		}
				i++;
        		
        	}
        	if(isAssetPage)
        	{
        		%>
        		</div>
        		<%
        	}
				%>
			<input type = hidden id = "childTableStartingID" value = <%=i%> />
</div>

<div class="mt-5" id = 'oldPostAssets'>
	<div class="my-3 text-center">
			<h3 class="font-weight-bold"><%=Language.equalsIgnoreCase("english")?"Current Assets in User's Other Posts":"ইউজারের অন্যান্য পদবীসমূহে বর্তমান সম্পদসমূহ"%></h3>		
			<%
			List<EmployeeOfficeDTO> employeeOfficeDTOList = EmployeeOfficesDAO.getInstance().getByEmployeeRecordIdIgnoringStatusAndIsDeleted(employeeRecordId);
			if(employeeOfficeDTOList != null)
			{
				%>
				<table class="table table-striped table-bordered" style="font-size: 14px">
			        <thead>
				        <tr>
				            <th >
				                <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENTOFFICEUNITID, loginDTO)%>
				                </b></th>
				            <th >
				                <b><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_DESIGNATION, loginDTO)%>
				                </b></th>
				            <th >
				                <b><%=Language.equalsIgnoreCase("english")?"Current Employee":"বর্তমান কর্মকর্তা"%>
				                </b></th>			           
				            <th >
				                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_SERVINGFROM, loginDTO)%>
				                </b></th>
				            <th >
				                <b><%=LM.getText(LC.EMPLOYEE_HISTORY_ADD_LASTOFFICEDATE, loginDTO)%>
				                </b></th>
				            <th >
				                <b><%=LM.getText(LC.HM_ASSET, loginDTO)%>
				                </b></th>
				            <th >
				                <b><%=Language.equalsIgnoreCase("english")?"Transfer":"ট্রান্সফার"%>
				                </b></th>	
				         </tr>
				     </thead>
				     <tbody>
				     <%
				     for(EmployeeOfficeDTO employeeOfficeDTO: employeeOfficeDTOList)
				     {
				    	 if(employeeOfficeDTO.officeUnitOrganogramId == organogramId)
				    	 {
				    		 continue;
				    	 }
				    	 int count = assetAssigneeDAO.getCountByOrganogramId(employeeOfficeDTO.officeUnitOrganogramId);
				    	 if(count <= 0)
				    	 {
				    		 continue;
				    	 }
				    	 %>
				    	 <tr>
				    	 	<td>
				    	 		<%=WorkflowController.getOfficeNameFromOrganogramId(employeeOfficeDTO.officeUnitOrganogramId, isLangEng) %>
				    	 	</td>
				    	 	<td>
				    	 		<%=WorkflowController.getOrganogramName(employeeOfficeDTO.officeUnitOrganogramId, isLangEng) %>
				    	 	</td>
				    	 	<td>
				    	 		<%=WorkflowController.getNameFromOrganogramId(employeeOfficeDTO.officeUnitOrganogramId, isLangEng) %>
				    	 		<br><%=WorkflowController.getUserNameFromOrganogramId(employeeOfficeDTO.officeUnitOrganogramId) %>
				    	 	</td>
				    	 	<td>
				    	 		<%=simpleDateFormat.format(new Date(employeeOfficeDTO.joiningDate))%>
				    	 	</td>
				    	 	<td>
				    	 		<%=employeeOfficeDTO.lastOfficeDate != Long.MIN_VALUE? simpleDateFormat.format(new Date(employeeOfficeDTO.lastOfficeDate)):""%>
				    	 	</td>
				    	 	<td>
				    	 		<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
							            onclick="location.href='Asset_modelServlet?actionType=getUserAsset&organogramId=<%=employeeOfficeDTO.officeUnitOrganogramId%>'">
							        <i class="fa fa-eye"></i>
							    </button>
				    	 	</td>
				    	 	<td>
				    	 	<%
				    	 		long userId = WorkflowController.getUserIDFromOrganogramId(employeeOfficeDTO.officeUnitOrganogramId);
				    	 		if(userId == -1 || userId == WorkflowController.getUserIDFromOrganogramId(organogramId))
				    	 		{
				    	 	%>
				    	 		<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
							            onclick="location.href='Asset_modelServlet?actionType=transfer&fromOrgId=<%=employeeOfficeDTO.officeUnitOrganogramId%>&toOrgId=<%=organogramId%>'">
							        <i class="fa fa-truck"></i></i>
							    </button>
							    <%
				    	 		}
							    %>
				    	 	</td>
				    	 </tr>
				    	 <%
				     }
				     %>
				     </tbody>
				  </table>
				<%
			}
			%>	
			
		</div>
</div>
