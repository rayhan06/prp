<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="asset_model.*"%>
<%@page import="workflow.WorkflowController"%>
<%@ page import="java.util.*"%>
<%@ page import="pb.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="util.*"%>
<%@page import="asset_category.*"%>



<style>
	td {
		text-align: center;
		vertical-align: middle;
	}
</style>

<%
String servletName = "Asset_modelServlet";
String ID = request.getParameter("ID");
Asset_modelDAO asset_modelDAO = new Asset_modelDAO("asset_model");
long id = Long.parseLong(ID);
Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(id, false);
CommonDTO commonDTO = asset_modelDTO;
Asset_categoryDTO asset_categoryDTO = Asset_categoryRepository.getInstance().getAsset_categoryDTOByID(asset_modelDTO.assetCategoryType);

%>
<%@include file="../pb/viewInitializer.jsp"%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_MODEL_ADD_FORMNAME, loginDTO)%>
                </h3>
                <div class="ml-auto mr-3">
				    <button type="button" class="btn" id='printer2'
				            onclick="location.href='Asset_modelServlet?actionType=search'">
				        <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_MODEL_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>		
								<%@include file="../asset_model/asset_modelViewFields.jsp"%>					
							</div>
                        </div>
                    </div>
                </div>
            </div>			

			<%
							if(asset_categoryDTO.isAssignable)
							{
						%>
			<form class="form-horizontal"
              action="Asset_modelServlet?actionType=activate&id=<%=asset_modelDTO.iD%>"
              id="bigform" name="bigform" method="POST">
             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5 class="table-title"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE, loginDTO)%></h5>
						<div class="table-responsive">
							<table class="table table-bordered table-striped text-nowrap">
							<tr>

								<th><%=LM.getText(LC.HM_SL, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_IS_AVAILABLE, loginDTO)%></th>
								
								<th type="thTdLaptopCpuAp"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_MAC, loginDTO)%></th>
								<th type="thTdLaptopCpuAp"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_IP, loginDTO)%></th>
						
								<th type="thTdAp"><%=LM.getText(LC.HM_IDF_NUMBER, loginDTO)%></th>
								<th type="thTdAp"><%=LM.getText(LC.HM_HOST_NAME, loginDTO)%></th>								
								<th type="thTdAp"><%=LM.getText(LC.HM_LOCATION, loginDTO)%></th>
								<th type="thTdAp"><%=LM.getText(LC.HM_AP_LICENSE_FOR_USE, loginDTO)%></th>
								<th type="thTdAp"><%=LM.getText(LC.HM_PART_NUMBER, loginDTO)%></th>
								<th type="thTdAp"><%=LM.getText(LC.HM_PORT, loginDTO)%></th>
								

								<th><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_WINGTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_NAME, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_OFFICE_TYPE, loginDTO)%></th>

								<th><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_DELIVERYDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_ASSIGNMENTUNASSIGNMENT_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_COSTINGVALUE, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_DELIVERYSTATUS, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_REMARKS, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								

							</tr>
							<%
                        	AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
                         	List<AssetAssigneeDTO> assetAssigneeDTOs = (List<AssetAssigneeDTO>)assetAssigneeDAO.getDTOsByParent("asset_model_id", asset_modelDTO.iD);
                         	
                         	for(AssetAssigneeDTO assetAssigneeDTO: assetAssigneeDTOs)
                         	{
                         		%>
                         			<tr>
										
										<td>
											<%
											value = assetAssigneeDTO.sl + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									
									<td>
										<%
										if(assetAssigneeDTO.assignmentStatus == AssetAssigneeDTO.NOT_ASSIGNED)
										{
											if (assetAssigneeDTO.isActive) {
										%>

										<button type="button" class="btn btn-primary" onclick="onMaintenanceButtonPress('<%=assetAssigneeDTO.iD%>')" id=""><%=isLanguageEnglish ? "SENT TO MAINTENANCE" : "মেন্টেনেন্সে পাঠান"%></button>
										<button type="button" class="btn btn-danger" onclick="onCondemnButtonPress('<%=assetAssigneeDTO.iD%>', '0')" id=""><%=isLanguageEnglish ? "CONDEMN" : "অব্যবহার্য বলে ঘোষনা করুন"%></button>
										<%
											} else {
										%>
										<button type="button" class="btn btn-success" onclick="onCondemnButtonPress('<%=assetAssigneeDTO.iD%>', '1')" id=""><%=isLanguageEnglish ? "ACTIVATE" : "সক্রিয় করুন"%></button>
										<%
											}
										}
										else
										{
										%>
										<i class="fa fa-check" aria-hidden="true"></i>
										<%
										}%>
									</td>

									<td type="thTdLaptopCpuAp">
										<%
											value = assetAssigneeDTO.mac + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									<td type="thTdLaptopCpuAp">
										<%
											value = assetAssigneeDTO.ip + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>



									


									<td type="thTdAp">
										<%
											value = assetAssigneeDTO.idfNumber + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									<td type="thTdAp">
										<%
											value = assetAssigneeDTO.host + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									<td type="thTdAp">
										<%
											value = assetAssigneeDTO.location + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									<td type="thTdAp">
										<%
											value = assetAssigneeDTO.apLicenceForUse + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									<td type="thTdAp">
										<%
											value = assetAssigneeDTO.partNumber + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									<td type="thTdAp">
										<%
											value = assetAssigneeDTO.port + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>







									<td>

										<%=WorkflowController.getWingNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, Language)%>


									</td>
									<td>

										<%=WorkflowController.getNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, Language)%>

									</td>
									
									<td>

										<%=WorkflowController.getOfficeTypeNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, Language)%>

									</td>

									<td>
										<%
										if(assetAssigneeDTO.deliveryDate > 0)
										{
										%>
										<%
											String formatted_Date = simpleDateFormat.format(new Date(assetAssigneeDTO.deliveryDate));
										%>
										<%=Utils.getDigits(formatted_Date, Language)%>
										<%
										}
										%>


									</td>
									<td>
									
										<%
										if(assetAssigneeDTO.assignmentDate > 0)
										{
										%>
										
										<%
											String formatted_Date = simpleDateFormat.format(new Date(assetAssigneeDTO.assignmentDate));
										%>
										<%=Utils.getDigits(formatted_Date, Language)%>
										<%
										}
										%>

									</td>
									<td>
										<%
											value = assetAssigneeDTO.costingValue + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									<td>


										<%=CatDAO.getName(Language, "delivery_status", assetAssigneeDTO.deliveryStatus)%>


									</td>
									<td>
										<%
											value = assetAssigneeDTO.remarks + "";
										%>

										<%=Utils.getDigits(value, Language)%>


									</td>
									
									<td>
										<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														onclick="location.href='<%=servletName%>?actionType=viewAssetAssignee&id=<%=assetAssigneeDTO.iD%>'"
												>
													<i class="fa fa-eye"></i>
												</button>	


									</td>
								</tr>
								<%

									}

								%>
							</table>
						</div>
                    </div>                    
                </div>
<%--        		<div class="row mt-3">--%>
<%--					<div class="col-12 form-actions text-right mt-1">--%>
<%--						<button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">--%>
<%--							<%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_MODEL_SUBMIT_BUTTON, loginDTO)%>--%>
<%--						</button>--%>
<%--					</div>--%>
<%--				</div>--%>
        	</form>
        	<%
			}
        	%>
        </div>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function(){
	showHideDivs();
});

function activate(cbId, hiddenId)
{
	if($("#" + cbId).prop("checked"))
	{
		$("#" + hiddenId).val("true");		
	}
	else
	{
		$("#" + hiddenId).val("false");
	}
	console.log(hiddenId + "=" + $("#" + hiddenId).val());

}


function onCondemnButtonPress(asset_assignee_id, status) {
	if (status==='0') {
		window.location = 'Asset_modelServlet?actionType=assetCondemnation&asset_assignee_id=' + asset_assignee_id;
	} else {
		window.location = 'Asset_modelServlet?actionType=assetActivation&asset_assignee_id=' + asset_assignee_id;
	}

}

function onMaintenanceButtonPress(asset_assignee_id) {
	window.location = 'Asset_modelServlet?actionType=assetMaintenance&asset_assignee_id=' + asset_assignee_id;
}

<%@include file="../asset_model/showHideDivs.jsp"%>
</script>