    function onCondemnButtonPress(asset_assignee_id, status) {
        if (status==='0') {
            window.location = 'Asset_modelServlet?actionType=assetCondemnation&asset_assignee_id=' + asset_assignee_id;
        } else {
            window.location = 'Asset_modelServlet?actionType=assetActivation&asset_assignee_id=' + asset_assignee_id;
        }

    }

    function onMaintenanceButtonPress(asset_assignee_id) {
        window.location = 'Asset_modelServlet?actionType=assetMaintenance&asset_assignee_id=' + asset_assignee_id;
    }

    function onReleaseButtonPress(asset_assignee_id) {
        window.location = 'Asset_modelServlet?actionType=assetRelease&asset_assignee_id=' + asset_assignee_id;
    }

    function onReturnFromWarrantyButtonPress(asset_assignee_id) {
        window.location = 'Asset_modelServlet?actionType=assetReturnFromWarranty&asset_assignee_id=' + asset_assignee_id;
    }

    function onReturnFromRepairButtonPress(asset_assignee_id) {
        window.location = 'Asset_modelServlet?actionType=assetReturnFromRepair&asset_assignee_id=' + asset_assignee_id;
    }

