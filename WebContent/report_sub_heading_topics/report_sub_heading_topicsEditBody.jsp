
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="report_sub_heading_topics.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>


<%
Report_sub_heading_topicsDTO report_sub_heading_topicsDTO;
report_sub_heading_topicsDTO = (Report_sub_heading_topicsDTO)request.getAttribute("report_sub_heading_topicsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(report_sub_heading_topicsDTO == null)
{
	report_sub_heading_topicsDTO = new Report_sub_heading_topicsDTO();
	
}
System.out.println("report_sub_heading_topicsDTO = " + report_sub_heading_topicsDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_REPORT_SUB_HEADING_TOPICS_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_REPORT_SUB_HEADING_TOPICS_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Report_sub_heading_topicsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=report_sub_heading_topicsDTO.iD%>' tag='pb_html'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_REPORTHEADINGTYPE, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_REPORTHEADINGTYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'reportHeadingType_div_<%=i%>'>	
		<select class='form-control' onchange = "getSubHeading(this)" name='reportHeadingType' id = 'reportHeadingType_select_<%=i%>'   tag='pb_html'>
<%
String Default = "<Option value = '-1'>Select a Heading</Option>";
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "report_heading", "reportHeadingType_select_" + i, "form-control", "reportHeadingType", report_sub_heading_topicsDTO.reportHeadingType + "");
}
else
{			
			Options = Default +  CommonDAO.getOptions(Language, "select", "report_heading", "reportHeadingType_select_" + i, "form-control", "reportHeadingType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_REPORTSUBHEADINGTYPE, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_REPORTSUBHEADINGTYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'reportSubHeadingType_div_<%=i%>'>	
		<select class='form-control' disabled required name='reportSubHeadingType' id = 'reportSubHeadingType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "report_sub_heading", "reportSubHeadingType_select_" + i, "form-control", "reportSubHeadingType", report_sub_heading_topicsDTO.reportSubHeadingType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "report_sub_heading", "reportSubHeadingType_select_" + i, "form-control", "reportSubHeadingType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + report_sub_heading_topicsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + report_sub_heading_topicsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	





				<div class="col-md-12" style="padding-top: 20px;">
					<legend class="text-left content_legend"><%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC, loginDTO)%></legend>
				</div>

				<div class="col-md-12">

					<div class="table-responsive">
						<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_TOPIC_NAMEEN, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_NAMEEN, loginDTO))%></th>
										<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_TOPIC_NAMEBN, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_NAMEBN, loginDTO))%></th>
										<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_TOPIC_UNIT, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_UNIT, loginDTO))%></th>
										<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_TOPIC_ESTIMATEDWORKVOLUME, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_ESTIMATEDWORKVOLUME, loginDTO))%></th>
										<th><%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
								<tbody id="field-Topic">
						
						
<%
	if(actionName.equals("edit")){
		int index = -1;
		
		
		for(TopicDTO topicDTO: report_sub_heading_topicsDTO.topicDTOList)
		{
			index++;
			
			System.out.println("index index = "+index);

%>	
							
									<tr>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='topic.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=topicDTO.iD%>' tag='pb_html'/>
	
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='topic.reportSubHeadingTopicsId' id = 'reportSubHeadingTopicsId_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + topicDTO.reportSubHeadingTopicsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td>										











		<input type='text' class='form-control'  name='topic.nameEn' id = 'nameEn_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + topicDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='topic.nameBn' id = 'nameBn_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + topicDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='topic.unit' id = 'unit_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + topicDTO.unit + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='topic.estimatedWorkVolume' id = 'estimatedWorkVolume_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + topicDTO.estimatedWorkVolume + "'"):("'" + "0.0" + "'")%>   tag='pb_html'/>					
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='topic.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= <%=actionName.equals("edit")?("'" + topicDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='topic.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + topicDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td><div class='checker'><span id='chkEdit' ><input type='checkbox' id='topic_cb_<%=index%>' name='checkbox' value=''/></span></div></td>
									</tr>								
<%	
			childTableStartingID ++;
		}
	}
%>						
						
								</tbody>
							</table>
						
						
						
					</div>
					<div class="form-group">
						<div class="col-xs-9 text-right">

							<button id="remove-Topic" name="removeTopic" type="button"
									class="btn btn-danger remove-me1"><%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_REMOVE, loginDTO)%></button>

							<button id="add-more-Topic" name="add-moreTopic" type="button"
									class="btn btn-primary"><%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_ADD_MORE, loginDTO)%></button>

						</div>
					</div>
					
					<%TopicDTO topicDTO = new TopicDTO();%>
					
					<template id="template-Topic" >						
								<tr>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='topic.iD' id = 'iD_hidden_' value='<%=topicDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='topic.reportSubHeadingTopicsId' id = 'reportSubHeadingTopicsId_hidden_' value=<%=actionName.equals("edit")?("'" + topicDTO.reportSubHeadingTopicsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td>











		<input type='text' class='form-control'  name='topic.nameEn' id = 'nameEn_text_' value=<%=actionName.equals("edit")?("'" + topicDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='topic.nameBn' id = 'nameBn_text_' value=<%=actionName.equals("edit")?("'" + topicDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='topic.unit' id = 'unit_text_' value=<%=actionName.equals("edit")?("'" + topicDTO.unit + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='topic.estimatedWorkVolume' id = 'estimatedWorkVolume_text_' value=<%=actionName.equals("edit")?("'" + topicDTO.estimatedWorkVolume + "'"):("'" + "0.0" + "'")%>   tag='pb_html'/>					
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='topic.isDeleted' id = 'isDeleted_hidden_' value= <%=actionName.equals("edit")?("'" + topicDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='topic.lastModificationTime' id = 'lastModificationTime_hidden_' value=<%=actionName.equals("edit")?("'" + topicDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td><div><span id='chkEdit' ><input type='checkbox' name='checkbox' value=''/></span></div></td>
								</tr>								
						
					</template>
				</div>		

				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_REPORT_SUB_HEADING_TOPICS_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_REPORT_SUB_HEADING_TOPICS_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_REPORT_SUB_HEADING_TOPICS_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_REPORT_SUB_HEADING_TOPICS_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

function getSubHeading(select) {
	var value = select.value;
    console.log("getSubHeading " + value);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.includes('option')) {
                console.log("got response");
                document.getElementById('reportSubHeadingType_select_0').innerHTML = this.responseText;
                document.getElementById('reportSubHeadingType_select_0').disabled = false;
            }
            else {
                console.log("got errror response");
            }

        }
        else if (this.readyState == 4 && this.status != 200) {
            alert('failed ' + this.status);
        }
    };
    xhttp.open("GET", "Report_sub_heading_topicsServlet?actionType=getSubHeading&headingId=" + value, true);
    xhttp.send();
}


function PreprocessBeforeSubmiting(row, validate)
{
	if(document.getElementById('reportSubHeadingType_select_0').disabled)
	{
		toastr.error("You must select a heading and a sub heading");
		return false;		
	}
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Report_sub_heading_topicsServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-Topic").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-Topic");

            $("#field-Topic").append(t.html());
			SetCheckBoxValues("field-Topic");
			
			var tr = $("#field-Topic").find("tr:last-child");
			
			tr.attr("id","Topic_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			
			child_table_extra_id ++;

        });

    
      $("#remove-Topic").click(function(e){    	
	    var tablename = 'field-Topic';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






