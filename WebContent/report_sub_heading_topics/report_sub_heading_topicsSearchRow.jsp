<%@page pageEncoding="UTF-8" %>

<%@page import="report_sub_heading_topics.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_REPORT_SUB_HEADING_TOPICS;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Report_sub_heading_topicsDTO report_sub_heading_topicsDTO = (Report_sub_heading_topicsDTO)request.getAttribute("report_sub_heading_topicsDTO");
CommonDTO commonDTO = report_sub_heading_topicsDTO;
String servletName = "Report_sub_heading_topicsServlet";


System.out.println("report_sub_heading_topicsDTO = " + report_sub_heading_topicsDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Report_sub_heading_topicsDAO report_sub_heading_topicsDAO = (Report_sub_heading_topicsDAO)request.getAttribute("report_sub_heading_topicsDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

											
		
											
											<td id = '<%=i%>_reportHeadingType'>
											<%
											value = report_sub_heading_topicsDTO.reportHeadingType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "report_heading", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_reportSubHeadingType'>
											<%
											value = report_sub_heading_topicsDTO.reportSubHeadingType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "report_sub_heading", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=value%>
				
			
											</td>
		
											
		
											
		
	

											<td>
												<a href='Report_sub_heading_topicsServlet?actionType=view&ID=<%=report_sub_heading_topicsDTO.iD%>'>View</a>
											
											
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Report_sub_heading_topicsServlet?actionType=getEditPage&ID=<%=report_sub_heading_topicsDTO.iD%>'><%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_SEARCH_REPORT_SUB_HEADING_TOPICS_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span id='chkEdit' ><input type='checkbox' name='ID' value='<%=report_sub_heading_topicsDTO.iD%>'/></span>
												</div>
											</td>
																						
											

