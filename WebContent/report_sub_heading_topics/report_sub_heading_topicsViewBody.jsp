

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="report_sub_heading_topics.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Report_sub_heading_topicsDAO report_sub_heading_topicsDAO = new Report_sub_heading_topicsDAO("report_sub_heading_topics");
Report_sub_heading_topicsDTO report_sub_heading_topicsDTO = (Report_sub_heading_topicsDTO)report_sub_heading_topicsDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title">Report Sub Heading Topics Details</h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5>Report Sub Heading Topics</h5>
                    </div>
			



			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_REPORTHEADINGTYPE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="reportHeadingType">
						
											<%
											value = report_sub_heading_topicsDTO.reportHeadingType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "report_heading", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_REPORTSUBHEADINGTYPE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="reportSubHeadingType">
						
											<%
											value = report_sub_heading_topicsDTO.reportSubHeadingType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "report_sub_heading", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			
			
			
		


			</div>	

                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5>Topic</h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_TOPIC_NAMEEN, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_NAMEEN, loginDTO))%></th>
								<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_TOPIC_NAMEBN, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_NAMEBN, loginDTO))%></th>
								<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_TOPIC_UNIT, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_UNIT, loginDTO))%></th>
								<th><%=(actionName.equals("edit"))?(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_TOPIC_ESTIMATEDWORKVOLUME, loginDTO)):(LM.getText(LC.REPORT_SUB_HEADING_TOPICS_ADD_TOPIC_ESTIMATEDWORKVOLUME, loginDTO))%></th>
							</tr>
							<%
                        	TopicDAO topicDAO = new TopicDAO();
                         	List<TopicDTO> topicDTOs = topicDAO.getTopicDTOListByReportSubHeadingTopicsID(report_sub_heading_topicsDTO.iD);
                         	
                         	for(TopicDTO topicDTO: topicDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = topicDTO.nameEn + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = topicDTO.nameBn + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = topicDTO.unit + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = topicDTO.estimatedWorkVolume + "";
											%>
														
											<%=value%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
               


        </div>