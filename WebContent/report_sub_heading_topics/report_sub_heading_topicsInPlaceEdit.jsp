<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="report_sub_heading_topics.Report_sub_heading_topicsDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
Report_sub_heading_topicsDTO report_sub_heading_topicsDTO = (Report_sub_heading_topicsDTO)request.getAttribute("report_sub_heading_topicsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(report_sub_heading_topicsDTO == null)
{
	report_sub_heading_topicsDTO = new Report_sub_heading_topicsDTO();
	
}
System.out.println("report_sub_heading_topicsDTO = " + report_sub_heading_topicsDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.REPORT_SUB_HEADING_TOPICS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=report_sub_heading_topicsDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_reportHeadingType'>")%>
			
	
	<div class="form-inline" id = 'reportHeadingType_div_<%=i%>'>
		<select class='form-control'  name='reportHeadingType' id = 'reportHeadingType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "report_heading", "reportHeadingType_select_" + i, "form-control", "reportHeadingType", report_sub_heading_topicsDTO.reportHeadingType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "report_heading", "reportHeadingType_select_" + i, "form-control", "reportHeadingType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_reportSubHeadingType'>")%>
			
	
	<div class="form-inline" id = 'reportSubHeadingType_div_<%=i%>'>
		<select class='form-control'  name='reportSubHeadingType' id = 'reportSubHeadingType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "report_sub_heading", "reportSubHeadingType_select_" + i, "form-control", "reportSubHeadingType", report_sub_heading_topicsDTO.reportSubHeadingType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "report_sub_heading", "reportSubHeadingType_select_" + i, "form-control", "reportSubHeadingType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + report_sub_heading_topicsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=report_sub_heading_topicsDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Report_sub_heading_topicsServlet?actionType=view&ID=<%=report_sub_heading_topicsDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Report_sub_heading_topicsServlet?actionType=view&modal=1&ID=<%=report_sub_heading_topicsDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	