<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PRESCRIBED_MEDICINE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control' name='dr_user_name' id='dr_user_name'
                        tag='pb_html'>
                    <%
                        Options = CommonDAO.getDoctorsByUserName();
                        out.print(Options);
                    %>
                </select>						
				</div>
			</div>
		</div>
		 <div class="search-criteria-div col-md-12">
	        <div class="form-group row">
	            <label class="col-md-3 col-form-label text-md-right">
	                <%=LM.getText(LC.HM_DRUGS, loginDTO)%>
	            </label>
	            <div class="col-md-9">
	                <div class="input-group">
				        <input
				          class="form-control py-2  border searchBox w-auto"
				          type="search"
				          placeholder="Search"
				          
	                                              id='suggested_drug_0'
	                                              tag='pb_html'
	                                              onkeyup="getDrugs(this.id)"
	                                              autocomplete="off"
				        />
				        
				      </div>
				      <div
				        id="searchSgtnSection_"
				        class="search-sgtn-section shadow-sm bg-white"
						tag='pb_html'
				      >
				        <div class="pt-3">
				          <ul class="px-0" style="list-style: none" id="drug_ul_0" tag='pb_html'>
				          </ul>
				        </div>
				      </div>
				      
				      <input name='formStr'
	                                           id='formStr_0' type="hidden"
	                                           tag='pb_html'
	                                           value=''></input>
	                                    
	                                           
	                   <input name='drugInformationType'
	                             id='drugInformationType_select_0' type="hidden"
	                             tag='pb_html'
	                             value=''></input>
	            </div>
	        </div>
	    </div>
		
	
    </div>
</div>
<script type="text/javascript">
function init()
{
	dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = false;
    $("#doctor_id").select2({
        dropdownAutoWidth: true,
        theme: "classic"
    });
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
	if($("#suggested_drug_0").val() == "")
	{
		$("#drugInformationType_select_0").val("");
	}
}
function drugClicked(rowId, drugText, drugId, form, stock)
{
	console.log("drugClicked with " + rowId + ", drugId = " + drugId + ", form = " + form);
	$("#suggested_drug_" + rowId).val(drugText);
	$("#suggested_drug_" + rowId).attr("style", "width: " + ($("#suggested_drug_" + rowId).val().length + 1)*8 + "px !important");
	
	 $("#drug_modal_textdiv_" + rowId).html(stock);
     $("#totalCurrentStock_text_" + rowId).val(stock);

	$("#drugInformationType_select_" + rowId).val(drugId);

	
	$("#drug_ul_" + rowId).html("");
	//$("#drug_ul_" + rowId).css("display", "none");
}
</script>