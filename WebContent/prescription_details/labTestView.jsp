<%@page import="pb.CommonDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<style>

th, td{
padding: .2rem .8rem
}

</style>

<table class="w-100 table-bordered border-bottom border-dark">
<%

LabTestListDAO labTestListDAO = new LabTestListDAO();
/////////////////////////////////////////////////////prescription view//////////////////////////////////////////////
if(addLabTest == null) //just prescription view
{
	int sl2 = 1;
	%>
	<thead class="text-nowrap">
	<tr class="border border-dark border-left-0 border-right-0">
		<th class="py-2"><%=LM.getText(LC.HM_SL, loginDTO)%></th>
		<th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTTYPE, loginDTO)%></th>
		<th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTDETAILSTYPES, loginDTO)%></th>
	</tr>
	</thead>
	<%
	for (PrescriptionLabTestDTO prescriptionLabTestDTO : prescriptionLabTestDTOs) 
	{
		%>
		<tr>
			<td valign="top" class="py-2"><%=Utils.getDigits(++sl, Language)%></td>
			<td valign="top"><%=prescriptionLabTestDAO.getTestName(prescriptionLabTestDTO)%></td>
			<td valign="top"><%=prescriptionLabTestDAO.getTestDetails(prescriptionLabTestDTO)%></td>
		</tr>
		<%
	}
	%>
	
	<%
}





/////////////////////////////////////////////////////just view for lab users//////////////////////////////////////////////
else if(addLabTest != null && justView != null)//just view for lab users
{
	int sl2 = 1;
	%>
	<thead class="text-nowrap">
	<tr class="border border-dark border-left-0 border-right-0">
		<th class="py-2"><%=LM.getText(LC.HM_SL, loginDTO)%></th>
		<th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTTYPE, loginDTO)%></th>
		<th><%=LM.getText(LC.HM_DETAILS, loginDTO)%></th>
		<th><%=LM.getText(LC.HM_TESTING_DATE, loginDTO)%></th>
		<th><%=Language.equalsIgnoreCase("english")?"Scheduled Date":"সম্ভাব্য  তারিখ"%>
		<th><%=Language.equalsIgnoreCase("english")?"Delivery Date":"ডেলিভারির  তারিখ"%>
		<th><%=LM.getText(LC.HM_STATUS, loginDTO)%>
		<th><%=LM.getText(LC.HM_REPORT, loginDTO)%> </th>
	</tr>
	</thead>
	<%
	for (PrescriptionLabTestDTO prescriptionLabTestDTO : prescriptionLabTestDTOs) 
	{
		%>
		<tr>
			<td valign="top" class="py-2"><%=Utils.getDigits(++sl, Language)%></td>
			<td valign="top"><%=prescriptionLabTestDAO.getTestName(prescriptionLabTestDTO)%></td>
			<td valign="top"><%=prescriptionLabTestDAO.getTestDetails(prescriptionLabTestDTO)%></td>
			<td valign="top">
               <%
                   if (prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_OTHER) {
               %>
               <%
                   if (prescriptionLabTestDTO.testingDate >= 0) {
                       value = ttDateFormat.format(new Date(prescriptionLabTestDTO.testingDate));
                   } else {
                       value = "";
                   }
               %>
               <%=value%>
               <%
                   }
               %>

           </td>
           <td valign="top">
               <%
                   if (prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_OTHER) {
               %>
               <%
                   if (prescriptionLabTestDTO.scheduledDeliveryDate >= 0) {
                       value = ttDateFormat.format(new Date(prescriptionLabTestDTO.scheduledDeliveryDate));
                   } else {
                       value = "";
                   }
               %>
               <%=value%>
               <%
                   }
               %>

           </td>
           <td valign="top">
                <%

                    if (prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_OTHER) {
                %>
                <%
                    long testTime = common_lab_reportDAO.getTimeByLabTestId(prescriptionLabTestDTO.iD);
                    if (testTime > 0) {
                        value = ttDateFormat.format(testTime);
                    } else {
                        value = "";
                    }
                %>
                <%=value%>
                <%
                    }
                %>
            </td>
            <td valign="top">
             <%
                   if (prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_OTHER) {
               %>
            	<%=prescriptionLabTestDAO.getProgress(prescriptionLabTestDTO, Language)%>
            	<%
                   }
            	%>
            </td>
            <td valign="top">
            <%
            long clrId = common_lab_reportDAO.getIdByLabTestId(prescriptionLabTestDTO.iD);
            %>
             <%
                  if (clrId != -1 && prescriptionLabTestDTO.labTestType != SessionConstants.LAB_TEST_OTHER ) {
                	  if(prescriptionLabTestDTO.approvalStatus == PrescriptionLabTestDTO.APPROVED
                		|| userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE 
                		|| userDTO.roleID == SessionConstants.PATHOLOGY_HEAD
                		|| userDTO.roleID == SessionConstants.ADMIN_ROLE
                		|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
                	  {
              %>
                 <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
			            onclick="location.href='Common_lab_reportServlet?actionType=view&ID=<%=clrId%>'">
			        <i class="fa fa-eye"></i>
			    </button>
              <%
                	  }
                  }
              %>
            </td>
		</tr>
		<%
	}
	%>
	
	<%
}







/////////////////////////////////////////////////////lab users work here//////////////////////////////////////////////
else if(addLabTest != null && justView == null)
{
	int sl2 = 1;
	%>
	<thead class="text-nowrap">
	<tr class="border border-dark border-left-0 border-right-0">
		<th class="py-2"><%=LM.getText(LC.HM_SL, loginDTO)%></th>
		<th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTTYPE, loginDTO)%></th>
		<th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTDETAILSTYPES, loginDTO)%></th>
		<th style="min-width: 32rem;"><%=LM.getText(LC.HM_TESTING_DATE, loginDTO)%></th>
		<th style="min-width: 32rem;"><%=Language.equalsIgnoreCase("english")?"Scheduled Delivery Date":"সম্ভাব্য ডেলিভারির তারিখ"%>
		<%
           if(userDTO.roleID == SessionConstants.XRAY_GUY)
           {
        	   %>
        	   <th><%=Language.equalsIgnoreCase("english")?"XRay Plate":"এক্স রে প্লেট"%></th>
        	   <th><%=LM.getText(LC.HM_IS_DONE, loginDTO)%>
        	   </th>
        	   <%
           }
           else
           {
        	   %>
        	   <th><%=LM.getText(LC.HM_REPORT, loginDTO)%></th>
        	   <%
        	   
           }
		%>

	</tr>
	</thead>
	<%
	for (PrescriptionLabTestDTO prescriptionLabTestDTO : prescriptionLabTestDTOs) 
	{
		if(userDTO.roleID == SessionConstants.RADIOLOGIST || userDTO.roleID == SessionConstants.XRAY_GUY)
		{
			if(!(prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_XRAY || prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_ULTRASONOGRAM) )
			{
				System.out.println("Continuing 1");
				continue;
			}
			if(userDTO.roleID == SessionConstants.XRAY_GUY && prescriptionLabTestDTO.isDone)
			{
				System.out.println("Continuing 2");
				continue;
			}
			if(userDTO.roleID == SessionConstants.RADIOLOGIST && !prescriptionLabTestDTO.isDone)
			{
				System.out.println("Continuing 3");
				continue;
			}
		}
		else if(userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD)
		{
			if(prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_XRAY 
					|| prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_ULTRASONOGRAM
					|| prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_OTHER )
			{
				continue;
			}
			if(userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE && prescriptionLabTestDTO.isDone)
			{
				System.out.println("Continuing 2");
				continue;
			}
			if(userDTO.roleID == SessionConstants.PATHOLOGY_HEAD && !prescriptionLabTestDTO.isDone)
			{
				System.out.println("Continuing 3");
				continue;
			}
		}
		long clrId = common_lab_reportDAO.getIdByLabTestId(prescriptionLabTestDTO.iD);
		if(userDTO.roleID == SessionConstants.PATHOLOGY_HEAD && clrId == -1)
		{
			continue;
		}
		if(userDTO.roleID != SessionConstants.PATHOLOGY_HEAD && clrId != -1)
		{
			continue;
		}

		%>
		<tr>
			
			<td valign="top" class="py-2"><%=Utils.getDigits(++sl, Language)%>
			<input type='hidden' class='form-control' name="testId" readonly="readonly"
                     value="<%=prescriptionLabTestDTO.iD%>"/>
			</td>
			<td valign="top"><%=prescriptionLabTestDAO.getTestName(prescriptionLabTestDTO)%></td>
			<td valign="top"><%=prescriptionLabTestDAO.getTestDetails(prescriptionLabTestDTO)%></td>
			<td valign="top">
                
              <%
              if(userDTO.roleID == SessionConstants.RADIOLOGIST)
              {
            	  %>
            	  <%=ttDateFormat.format(new Date(prescriptionLabTestDTO.testingDate))%>
            	  <%
              }
              else
              {
              %>
                     
              <%String dateId = "testingDate_js_" + prescriptionLabTestDTO.iD;%>
              <jsp:include page="/date/date.jsp">
                  <jsp:param name="DATE_ID" value="<%=dateId%>"></jsp:param>
                  <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                  <jsp:param name="END_YEAR" value="<%=nextYear%>"></jsp:param>
              </jsp:include>
              <input type='hidden' name='testingDate_<%=prescriptionLabTestDTO.iD%>'
                     id='testingDate_date_<%=prescriptionLabTestDTO.iD%>'
                     value='<%=ttDateFormat.format(new Date(prescriptionLabTestDTO.testingDate))%>'
                     tag='pb_html'>
                     <%
              }
                     %>

           </td>
           <td valign="top">
           <%
              if(userDTO.roleID == SessionConstants.RADIOLOGIST)
              {
            	  %>
            	  <%=ttDateFormat.format(new Date(prescriptionLabTestDTO.scheduledDeliveryDate))%>
            	  <%
              }
              else
              {
              %>
           		<%String dateId = "schDate_js_" + prescriptionLabTestDTO.iD;%>
              <jsp:include page="/date/date.jsp">
                  <jsp:param name="DATE_ID" value="<%=dateId%>"></jsp:param>
                  <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                  <jsp:param name="END_YEAR" value="<%=nextYear%>"></jsp:param>
              </jsp:include>
              <input type='hidden' name='schDate_<%=prescriptionLabTestDTO.iD%>'
                     id='schDate_date_<%=prescriptionLabTestDTO.iD%>'
                     value='<%=ttDateFormat.format(new Date(prescriptionLabTestDTO.scheduledDeliveryDate))%>'
                     tag='pb_html'>
                      <%
              }
                     %>

               
                
                           
               
            </td>
            <td valign="top">
            
             <%

            	   if(userDTO.roleID == SessionConstants.XRAY_GUY && prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_XRAY)
            	   {

            		   int j = 0;
            		   for(long lPlateType: prescriptionLabTestDTO.plateTypesArray)
            		   {
            			   
						
            			   %>
	            			<div class="d-flex p11">
								<div class="m-2">								
			   		           		<select class='form-control w-auto' name='xRayPlateType_<%=prescriptionLabTestDTO.iD%>'
			   		                            tag='pb_html'>
			   		                       <%
			   		                          String Options = CommonDAO.getOptionsWithWhere(Language, "medical_equipment_name", prescriptionLabTestDTO.plateTypesArray[j], " medical_equipment_cat = " + Medical_equipment_nameDTO.XRAY_CAT);
			   		                       %>
			   		                       <%=Options%>
			   		                 </select>
			   		                 
			   		             </div>
			   		             <div class="m-2">
	   		                 		<input type = 'number' class='form-control w-auto' name='plateQuantity_<%=prescriptionLabTestDTO.iD%>'  tag = tag='pb_html'
	   		                 		 value='<%=prescriptionLabTestDTO.plateQuantityArray[j]%>'  min = '0'/>
	   		                 	 </div>
	   		                 	 <div class="m-2">								
			   		           		<select class='form-control w-auto' name='plateWasted_<%=prescriptionLabTestDTO.iD%>'
			   		                            tag='pb_html'>
			   		                       <option value = "0" <%=prescriptionLabTestDTO.plateWastedArray[j] == 0?"selected":"" %> >
			   		                       		<%=Language.equalsIgnoreCase("english")?"Used":"ব্যবহৃত"%>
			   		                       </option>
			   		                       
			   		                       <option value = "1" <%=prescriptionLabTestDTO.plateWastedArray[j] == 1?"selected":"" %> >
			   		                       		<%=Language.equalsIgnoreCase("english")?"Wasted":"বিনষ্ট"%>
			   		                       </option>
			   		                 </select>
			   		                 
			   		             </div>
	   		                 	 <div class="m-2">
	   		                 		<button type="button" class="btn border-0  submit-btn text-white btn-border-radius pl-4 py-4" 
						            	onclick="plusClicked(this)">
								        <i class="fa fa-plus"></i>
								    </button>   		                 	 
								 </div>
	   		                 	 <div class="m-2">
	   		                 		<button type="button" class="btn border-0  cancel-btn text-white btn-border-radius ml-2 pl-4 py-4" 
						            	onclick="minusClicked(this)">
								        <i class="fa fa-minus"></i>
								    </button>   		                 	 
								 </div>
	   		                 </div>	           		
   		                  <%
   		              	  j++;
            		   
            		   }            		   
            	   }
            	   else if(userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE)
            	   {
            		   %>
            		   <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
			            onclick="location.href='Common_lab_reportServlet?actionType=getAddPage&isApproval=false&prescription_lab_test_id=<%=prescriptionLabTestDTO.iD%>&lab_test_id=<%=prescriptionLabTestDTO.labTestType%>&lab_test_details=<%=prescriptionLabTestDTO.labTestDetailsTypes.replace(" -4","").replace("-4, ","")%>'">
					        <i class="fa fa-plus"></i>
					    </button>
            		   <%
            	   }
            	   else if(userDTO.roleID == SessionConstants.PATHOLOGY_HEAD)
            	   {
            		   %>
            		   <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
			            onclick="location.href='Common_lab_reportServlet?actionType=getEditPage&ID=<%=clrId%>&isApproval=true&&prescription_lab_test_id=<%=prescriptionLabTestDTO.iD%>&lab_test_id=<%=prescriptionLabTestDTO.labTestType%>&lab_test_details=<%=prescriptionLabTestDTO.labTestDetailsTypes.replace(" -4","").replace("-4, ","")%>'">
					        <i class="fa fa-plus"></i>
					    </button>
            		   <%
            	   }
            	   else if(userDTO.roleID == SessionConstants.RADIOLOGIST)
            	   {
            		   %>
            		   <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
			            onclick="location.href='Usg_reportServlet?actionType=getAddPage&prescription_lab_test_id=<%=prescriptionLabTestDTO.iD%>'">
					        <i class="fa fa-plus"></i>
					    </button>
            		   <%
            	   }
            	   
           %>
            </td>
            <%
              if(userDTO.roleID == SessionConstants.XRAY_GUY)
              {
              %> 
              	<td valign="top">             	
              		<input type='checkbox' name='isDone_<%=prescriptionLabTestDTO.iD%>' id='isDone_<%=prescriptionLabTestDTO.iD%>' onchange='clickIsDone()'/>              	
              	</td>
              <%
              }
              %>
		</tr>
		<%
	}
	%>
	<%
}
%>
</table>
