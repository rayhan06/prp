   						<table class="w-100 border-bottom border-dark">
                            <tr class="border border-dark border-left-0 border-right-0">
                                <th class="py-2"><%=LM.getText(LC.HM_SL, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_DRUGINFORMATIONTYPE, loginDTO)%>
                                <th><%=LM.getText(LC.HM_QUANTITY, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_DOSAGETYPECAT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_FREQUENCY, loginDTO)%>
                                </th>
                                
                              
                                <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_FOODINSTRUCTIONSCAT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.HM_DOSE_SUPPLIED, loginDTO)%>
                                </th>
                                <%
                                    if (addMedicine != null && justView != null) {
                                %>


                                
                                
                                <th><%=LM.getText(LC.HM_DATE, loginDTO)%>
                                </th>
                                <%
                                    }
                                %>
                            </tr>
                            <%
                                Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
                                sl = 1;
                                for (PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO : patientPrescriptionMedicine2DTOs) {
                            %>
                            <tr>
                                <td class="py-3">
                                    <%=Utils.getDigits(sl++, Language)%>
                                </td>
                                <td>
                                    <%
                                        Drug_informationDTO drug_informationDTO = drug_informationDAO.getDrugInfoDetailsById(patientPrescriptionMedicine2DTO.drugInformationType);
                                        if ((patientPrescriptionMedicine2DTO.alternateDrugName == null
                                                || patientPrescriptionMedicine2DTO.alternateDrugName.equalsIgnoreCase(""))
                                                && drug_informationDTO != null
                                        ) 
                                        {
                                    %>
                                    <%

	                                        if (addMedicine != null &&  patientPrescriptionMedicine2DTO.quantity <= drug_informationDTO.availableStock
	                                        && drug_informationDTO.availableStock > 0 && drug_informationDAO.canReceiveThisDrug(drug_informationDTO, prescription_detailsDTO.employeeId)) 
	                                        {
                                    %>
                                    			<i class="fa fa-check" aria-hidden="true"></i>
                                    <%
                                    		} 
	                                        else if (addMedicine != null && drug_informationDTO.availableStock == 0) 
	                                        {
                                    %>
                                    			<i class="fa fa-times" aria-hidden="true"></i>
                                    <%
                                        	}
	                                        else if (addMedicine != null)
	                                        {
                                    	%>
                                    			<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    	<%
	                                        }

                                    %>
                                    		<%=drug_informationDTO.drugTextWithoutGNameBolded%>

                                    <%
                                        	if (addMedicine != null) 
                                        	{
                                    %>
                                    			<br> <h6><%=drug_informationDTO.availableStock %> pieces available in stock</h6>
                                    <%
                                        	}
                                    %>
                                    <%
                                    	}
                                        else 
                                        {
                                    %>
                                    	<%=patientPrescriptionMedicine2DTO.alternateDrugName%>
                                    <%
                                        }
                                    %>

                                </td>
                                 <td>
                                    <%
                                        value = patientPrescriptionMedicine2DTO.quantity + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td>

                                    <%
                                        if (patientPrescriptionMedicine2DTO.dosageTypeCat > 0) {
                                            value = CatDAO.getName(Language, "dosage_type", patientPrescriptionMedicine2DTO.dosageTypeCat);
                                            
                                    %>
                                    <%=value%>
                                    <%
                                        if (!patientPrescriptionMedicine2DTO.dose.equalsIgnoreCase("")) {
                                    %>
                                    <br>
                                    <%
                                            }
                                        }
                                        else
                                        {
                                        	%>
                                        	N/A
                                        	<%
                                        }
                                    
                                        if (!patientPrescriptionMedicine2DTO.dose.equalsIgnoreCase("")) {
                                            value = patientPrescriptionMedicine2DTO.dose;
                                    %>
                                    <%=value%>
                                    <%
                                        }
                                    %>


                                </td>
                                
                               


                                <td>
                                    <%
                                        if (patientPrescriptionMedicine2DTO.frequencyCat == -1) //continue
                                        {
                                            value = CatDAO.getName(Language, "frequency", patientPrescriptionMedicine2DTO.frequencyCat);
                                        } else {
                                        	if(patientPrescriptionMedicine2DTO.frequency.equalsIgnoreCase("0") ||
                                        			patientPrescriptionMedicine2DTO.frequency.equalsIgnoreCase(""))
    	                                    {
    	                                    	value = "N/A";
    	                                    }
                                        	else
                                        	{
                                            	value = patientPrescriptionMedicine2DTO.frequency + " "
                                                    + CatDAO.getName(Language, "frequency", patientPrescriptionMedicine2DTO.frequencyCat);
                                        	}
                                        }
	                                    
                                    %>


                                    <%=value%>


                                </td>

                                

                             

                                <td>
                                    <%
                                        value = patientPrescriptionMedicine2DTO.foodInstructionsCat + "";
                                    %>
                                    <%
                                        value = CatDAO.getName(Language, "food_instructions", patientPrescriptionMedicine2DTO.foodInstructionsCat);
                                    %>

                                    <%=value%>


                                </td>
                                
                                <td>
                                    <%
                                        value = patientPrescriptionMedicine2DTO.doseSold + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>
                                </td>
                                <%
                                    if (addMedicine != null && justView != null) {
                                %>


                                

                               


                                <td>
                                    <%
                                        if (patientPrescriptionMedicine2DTO.sellingDate != 0) {
                                            value = ttDateFormat.format(new Date(patientPrescriptionMedicine2DTO.sellingDate));
                                        } else {
                                            value = "";
                                        }

                                    %>

                                    <%=Utils.getDigits(value, Language)%>
                                </td>
                                <%

                                    }
                                %>
                            </tr>
                            <%

                                }

                            %>
                        </table>
