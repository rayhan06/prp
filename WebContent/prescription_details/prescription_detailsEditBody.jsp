<%@page import="lab_test.Lab_testRepository"%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="appointment.*" %>

<%@page import="prescription_details.*" %>
<%@page import="patient_measurement.*" %>
<%@page import="java.util.*" %>
<%@page import="drug_information.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="util.*" %>
<%@ page import="xray_report.*" %>

<%@ page import="pb.*" %>
<%@ page import="workflow.*" %>
<%@ page import="family.*" %>


<style>
    .customdropdown {
        min-width: 155px;
    }

    a#history {
        min-width: 155px !important;
        margin-top: 1px !important;
    }


    .dropdown-menu.show {
        background-color: #dddddd;
        width: 156px !important;
        left: 0px !important;

    }

    button#dropdownMenuButton {
        padding-right: 90px !important;
    }
    
   
      .searchBox.form-control:focus {
        outline: 0 !important;
        border-color: initial;
        box-shadow: none;
      }

    

      .searchBox {
        padding-left: 1rem;
        min-width: 6rem !important;
      }

      .search-sgtn-section {
        background: #c2c2c2;
        height: auto;
        max-height: 250px;
        border-bottom-left-radius: 4px;
        border-bottom-right-radius: 4px;
        margin-top: 0.2rem;
        overflow-y: auto;
      }

      .search-item-a,
      .search-item-a:hover,
      .search-item-a:focus,
      .search-item-a:visited {
        text-decoration: none;
        color: initial;
      }

      .search-list-item:hover {
        background-color: #007a6e;
        cursor: pointer;
        box-shadow: rgba(0, 0, 0, 0.35) 0px 15px 15px;
      }

      .search-list-item h5,
      .search-list-item small {
        color: #007a6e;
      }

      .search-list-item:hover.search-list-item h5,
      .search-list-item:hover.search-list-item small {
        color: #fff !important;
      }

</style>

<%
    Prescription_detailsDTO prescription_detailsDTO;
    prescription_detailsDTO = (Prescription_detailsDTO) request.getAttribute("prescription_detailsDTO");
    Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
    PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
    long appointmentId = -1;
    AppointmentDAO appointmentDAO = new AppointmentDAO();
    Patient_measurementDAO patient_measurementDAO = new Patient_measurementDAO();
    AppointmentDTO appointmentDTO = null;
    Patient_measurementDTO patient_measurementDTO = null;

    if (request.getParameter("appointmentId") != null) {
        appointmentId = Long.parseLong(request.getParameter("appointmentId"));
        
    }
    else
    {
    	appointmentId = prescription_detailsDTO.appointmentId;
    }
    appointmentDTO = appointmentDAO.getDTOByID(appointmentId);
    patient_measurementDTO = patient_measurementDAO.getLatestDTO(appointmentDTO.employeeUserName, appointmentDTO.whoIsThePatientCat);
    if(patient_measurementDTO == null)
    {
    	patient_measurementDTO = new Patient_measurementDTO();
    }
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (prescription_detailsDTO == null) {
        prescription_detailsDTO = new Prescription_detailsDTO(appointmentDTO);
    }
    System.out.println("prescription_detailsDTO = " + prescription_detailsDTO);


    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.PRESCRIPTION_DETAILS_EDIT_PRESCRIPTION_DETAILS_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_DETAILS_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;


    //Drug Nam params
    String url = "Drug_informationServlet?actionType=search";

    String[][] searchFieldInfo = SessionConstants.SEARCH_DRUG_INFORMATION;
    String context = request.getContextPath();


    String Language = LM.getText(LC.PRESCRIPTION_DETAILS_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    
    int maxTabId = 5;
    if(appointmentDTO.isDental)
    {
    	maxTabId = 6;
    }
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>


<script src="<%=context%>assets/tab/scripts/pb.js" type="text/javascript"></script>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form action="Prescription_detailsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="kt_form" class="kt-form" name="bigform" method="POST"
              enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3"
                     data-ktwizard-state="first">
                    <div class="kt-grid__item mx-5 px-5">
                        <!--begin: Form Wizard Nav -->
                        <div class="kt-wizard-v3__nav border-0 mt-3 mx-5 mt-5" style="margin-bottom: -5px">
                            <div class="kt-wizard-v3__nav-items shadow-sm mx-5"
                                 style="border-radius: 8px 8px 0px 0px;">
                                <div class="d-flex justify-content-between align-items-center"
                                     style="width: 100%; height: 50px">
                                    <div class="">
                                        <h3 class="kt-portlet__head-title prp-page-title ml-1"
                                            style="color: #0098bf">
                                            <i class="fas fa-file-prescription"></i>&nbsp;
                                            <%=formTitle%>
                                        </h3>
                                    </div>
                                    <div class="">
                                        <a type="button" class="btn-sm border-0 shadow bg-light btn-border-radius"
                                           style="color: #bb9b4b;"
                                           href='Prescription_detailsServlet?actionType=getFormattedSearchPage&userName=<%=appointmentDTO.employeeUserName%>&whoIsThePatientCat=<%=FamilyDTO.UNSELECTED%>'>
                                            <i class="fa fa-history"></i>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-wizard-v3__nav border-bottom-0 mt-3 mx-5" style="margin-top: -10px">
                            <div class="kt-wizard-v3__nav-items shadow-sm mx-5 mb-4 d-flex justify-content-around"
                                 style="border-radius: 0px 0px 8px 8px; padding-left: 10px; padding-right: 15px">
                                <a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step"
                                   onclick="tabclicked(1)"
                                   data-ktwizard-state="current">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <%=LM.getText(LC.HM_PATIENT_INFORMATION, loginDTO)%>
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step"
                                   onclick="tabclicked(2)"
                                   data-ktwizard-state="pending">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <%=LM.getText(LC.HM_DIAGNOSIS, loginDTO)%>
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step"
                                   onclick="tabclicked(3)"
                                   data-ktwizard-state="pending">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <%=LM.getText(LC.HM_MEDICINE, loginDTO)%>
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step"
                                   onclick="tabclicked(4)"
                                   data-ktwizard-state="pending">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <%=Language.equalsIgnoreCase("english")?"Lab Test":"ল্যাব টেস্ট"%>
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar"></div>
                                    </div>
                                </a>
                                <%
                                if(appointmentDTO.isDental)
                                {
                                	%>
                                	
                                <a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step"
                                   onclick="tabclicked(5)"
                                   data-ktwizard-state="pending">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <%=Language.equalsIgnoreCase("english")?"Dentistry":"ডেন্টিস্ট্রি"%>
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar"></div>
                                    </div>
                                </a>
                                	<%
                                }
                                %>
                                <a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step"
                                   onclick="tabclicked(<%=maxTabId%>)"
                                   data-ktwizard-state="pending">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <%=Language.equalsIgnoreCase("english")?"Preview":"প্রিভিউ"%>
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!--begin: Form Wizard Step 1-->
                    <div class="kt-wizard-v3__content" data-ktwizard-type="step-content"
                         data-ktwizard-state="current">
                        <div class="kt-form__section kt-form__section--first">
                            <div class="kt-wizard-v3__form">
                                <div class="kt-portlet__body form-body">
                                    <div class="">
                                        <div class="mx-5 px-4">
                                            <div class="onlyborder">
                                                <div class="row">
                                                    <div class="col-md-0"></div>
                                                    <div class="col-md-11">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="sub_title_top">
                                                                    <div class="sub_title">
                                                                        <h4 style="background: white"><%=LM.getText(LC.HM_PATIENT_INFORMATION, loginDTO)%>
                                                                        </h4>
                                                                    </div>
                                                                    <input type='hidden' class='form-control'
                                                                           name='iD'
                                                                           id='iD_hidden_<%=i%>'
                                                                           value='<%=prescription_detailsDTO.iD%>'
                                                                           tag='pb_html'/>
                                                                    <input type='hidden' class='form-control'
                                                                           name='appointmentId'
                                                                           id='appointmentId_hidden_<%=i%>'
                                                                           value=<%=actionName.equals("edit")?("'" + prescription_detailsDTO.appointmentId + "'"):("'" + appointmentId + "'")%> tag='pb_html'/>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.HM_NAME, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                            <input
                                                                                    type='text'
                                                                                    class='form-control'
                                                                                    name='name'
                                                                                    readonly
                                                                                    id='name_text_<%=i%>'
                                                                                    value=<%=actionName.equals("edit")?("'" + prescription_detailsDTO.name + "'"):("'" + appointmentDTO.patientName + "'")%>
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_DATEOFBIRTH, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                            <input
                                                                                    type='text'
                                                                                    class="form-control"
                                                                                    readonly
                                                                                    data-label="Document Date"
                                                                                    name='dateOfBirth'
                                                                                    id='dateOfBirth_text_<%=i%>'
                                                                                    value=<%String formatted_Date;if(actionName.equals("edit")){formatted_Date = dateFormat.format(new Date(prescription_detailsDTO.dateOfBirth));%>
                                                                                            '<%=formatted_Date%>'
                                                                            <%
                                                                            } else {
                                                                                formatted_Date = dateFormat.format(new Date(appointmentDTO.dateOfBirth));
                                                                            %>
                                                                            '<%=Utils.getDigits(formatted_Date, Language)%>'
                                                                            <%
                                                                                }
                                                                                value = "";
                                                                            %>   tag='pb_html'/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_HEIGHT, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                            
                                                                            <input type='number'
                                                                                   class='form-control'
                                                                                   name='height2'
                                                                                   id='height_text_<%=i%>'
                                                                                   value='<%=patient_measurementDTO.height > 0 ? patient_measurementDTO.height: ""%>'   tag='pb_html'/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_WEIGHT, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                           
                                                                            <input type='number'
                                                                                   class='form-control'
                                                                                   name='weight2'
                                                                                   id='weight_text_<%=i%>'
                                                                                   value='<%=patient_measurementDTO.weight > 0 ? patient_measurementDTO.weight: ""%>'   tag='pb_html'/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.HM_BLOOD_PRESSURE, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9"
                                                                             style="font-family: Roboto,SolaimanLipi!important; font-weight: 400;">
                                                                            <div class="row d-flex justify-content-between">
                                                                                <div class="col-6 row d-flex align-items-center">
                                                                                    <div class="col-3">
                                                                                        <%=LM.getText(LC.HM_SYSTOLE, loginDTO)%>
                                                                                    </div>
                                                                                    <div class="col-9">
                                                                                        
                                                                                        <input type='number'
                                                                                               class='form-control ml-3'
                                                                                               name='bloodPressureSystole'
                                                                                               id='bloodPressureSystole_text_<%=i%>'
                                                                                               value='<%=patient_measurementDTO.bloodPressureSystole > 0 ? patient_measurementDTO.bloodPressureSystole: ""%>'   tag='pb_html'/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-6 row d-flex align-items-center">
                                                                                    <div class="col-4">
                                                                                        <%=LM.getText(LC.HM_DIASTOLE, loginDTO)%>
                                                                                    </div>
                                                                                    <div class="col-8">
                                                                                       
                                                                                        <input type='number'
                                                                                               class='form-control'
                                                                                               name='bloodPressureDiastole'
                                                                                               id='bloodPressureDiastole_text_<%=i%>'
                                                                                               value='<%=patient_measurementDTO.bloodPressureDiastole > 0 ? patient_measurementDTO.bloodPressureDiastole: ""%>'   tag='pb_html'/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=Language.equalsIgnoreCase("english")?"Blood Sugar":"রক্তের শর্করা"%>
                                        
                                                                        </label>
                                                                        <div class="col-9">
                                                                            
                                                                            <input
                                                                                    type='number'
                                                                                    class='form-control'
                                                                                    name='sugar'
                                                                                    id='sugar'
                                                                                    value='<%=(patient_measurementDTO.sugar != null && !patient_measurementDTO.sugar.equalsIgnoreCase("")) ? patient_measurementDTO.sugar: ""%>'   tag='pb_html'
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PULSE, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                            
                                                                            <input
                                                                                    type='number'
                                                                                    class='form-control'
                                                                                    name='pulse'
                                                                                    id='pulse_text_<%=i%>'
                                                                                    value=''<%=patient_measurementDTO.pulse > 0 ? patient_measurementDTO.pulse: ""%>'   tag='pb_html'
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_TEMPERATURE, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                            
                                                                            <input type='number'
                                                                                   class='form-control'
                                                                                   name='temperature'
                                                                                   id='temperature_text_<%=i%>'
                                                                                   value='<%=patient_measurementDTO.temperature > 0 ? patient_measurementDTO.temperature: ""%>'   tag='pb_html'/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_OXYGENSATURATION, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                            
                                                                            <input
                                                                                    type='number'
                                                                                    class='form-control'
                                                                                    name='oxygenSaturation'
                                                                                    id='oxygenSaturation_text_<%=i%>'
                                                                                    value='<%=patient_measurementDTO.oxygenSaturation > 0 ? patient_measurementDTO.oxygenSaturation: ""%>'   tag='pb_html'/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-0"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: Form Wizard Step 1-->

                    <!--begin: Form Wizard Step 2-->
                    <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                        <div class="kt-form__section kt-form__section--first">
                            <div class="kt-wizard-v3__form">
                                <div class="kt-portlet__body form-body">
                                    <div class="">
                                        <div class="mx-5 px-4">
                                            <div class="onlyborder">
                                                <div class="row">
                                                    <div class="col-md-0"></div>
                                                    <div class="col-md-11">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="sub_title_top">
                                                                    <div class="sub_title">
                                                                        <h4 style="background: white">
                                                                            <%=LM.getText(LC.HM_DIAGNOSIS, loginDTO)%>
                                                                        </h4>
                                                                    </div>
                                                                   
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_CHIEFCOMPLAINTS, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                                      <textarea
                                                                                              class='form-control'
                                                                                              name='chiefComplaints'
                                                                                              id='chiefComplaints_text_<%=i%>'
                                                                                              tag='pb_html'><%=actionName.equals("edit") ? (prescription_detailsDTO.chiefComplaints) : ("")%></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_CLINICALFEATURES, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                                      <textarea class='form-control'
                                                                                                name='clinicalFeatures'
                                                                                                id='clinicalFeatures_text_<%=i%>'
                                                                                                tag='pb_html'><%=actionName.equals("edit") ? (prescription_detailsDTO.clinicalFeatures) : ("")%></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_DIAGNOSIS, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                                     <textarea
                                                                                             class='form-control'
                                                                                             name='diagnosis'
                                                                                             id='diagnosis_text_<%=i%>'
                                                                                             tag='pb_html'><%=actionName.equals("edit") ? (prescription_detailsDTO.diagnosis) : ("")%></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_ADVICE, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                                       <textarea
                                                                                               class='form-control'
                                                                                               name='advice'
                                                                                               id='advice_text_<%=i%>'
                                                                                               tag='pb_html'
                                                                                       ><%=actionName.equals("edit") ? (prescription_detailsDTO.advice) : ("")%></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_REMARKS, loginDTO)%>
                                                                        </label>
                                                                        <div class="col-9">
                                                                                       <textarea
                                                                                               class='form-control'
                                                                                               name='remarks'
                                                                                               id='remarks_text_<%=i%>'
                                                                                               tag='pb_html'
                                                                                       ><%=actionName.equals("edit") ? (prescription_detailsDTO.remarks) : ("")%></textarea>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <%
                                                                    if(!actionName.equals("edit"))
                                                                    {
                                                                    %>
                                                                    <div class="form-group row">
                                                                        <label class="col-3 col-form-label text-right">
                                                                        </label>
                                                                        <div class="col-9 text-right">
                                                                            <button
                                                                                    type="button"
                                                                                    class="btn border-0 shadow text-white"
                                                                                    style="background-color: #aa7fe4; border-radius: 8px"
                                                                                    onclick="showReferredToBlock(1)">
                                                                                <%=LM.getText(LC.HM_FORWARD, loginDTO)%>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                            <input type='hidden'
                                                                                   class='form-control-sm'
                                                                                   name='therapyNeeded'
                                                                                   id='therapyNeeded_cb_<%=i%>'
                                                                                    
                                                                                   
                                                                                   tag='pb_html'
                                                                            />
                                                                       
                                                                    <div id="referralDiv" style='display:none'>
                                                                        <div class="form-group row">
                                                                            <label class="col-3 col-form-label text-right">
                                                                                <%=LM.getText(LC.HM_SPECIALITY, loginDTO)%>
                                                                            </label>
                                                                            <div class="col-9">
                                                                                <select class='form-control'
                                                                                        name='referrence_dept'
                                                                                        id='referrence_dept'
                                                                                        onchange="dept_selected(this, 'referredTo')"
                                                                                        tag='pb_html'>


                                                                                    
                                                                                    <%
                                                                                        Options = CatDAO.getOptions(Language, "department", -1);
                                                                                    %>
                                                                                    <%=Options%>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-3 col-form-label text-right">
                                                                                <%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
                                                                            </label>
                                                                            <div class="col-9">
                                                                                <select class='form-control'
                                                                                        name='referredTo'
                                                                                        id='referredTo'
                                                                                        tag='pb_html'>
                                                                                    <%
                                                                                        Options = CommonDAO.getDoctorsByOrganogramID(prescription_detailsDTO.referredTo, "", CommonDAO.DR_AND_THERAPIST, true);
                                                                                    %>
                                                                                    <%=Options%>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-3 col-form-label text-right">
                                                                                <%=LM.getText(LC.HM_REFERRAL_MESSAGE, loginDTO)%>
                                                                            </label>
                                                                            <div class="col-9">
                                                                                        <textarea
                                                                                                class='form-control'
                                                                                                name='referralMessage'
                                                                                                id='referralMessage'
                                                                                                tag='pb_html'></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-3 col-form-label text-right">
                                                                                <%=LM.getText(LC.HM_CREATE_APPOINTMENT, loginDTO)%>
                                                                            </label>
                                                                            <div class="col-9">
                                                                                <input
                                                                                        type="checkbox"
                                                                                        class='form-control-sm'
                                                                                        name='createAppointment'
                                                                                        id='createAppointment'
                                                                                        tag='pb_html'
                                                                                />
                                                                            </div>
                                                                        </div>
                                                                        <input type='hidden'
                                                                               class='form-control'
                                                                               name='isDeleted'
                                                                               id='isDeleted_hidden_<%=i%>'
                                                                               value=<%=actionName.equals("edit")?("'" + prescription_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'
                                                                        />
                                                                        <input type='hidden'
                                                                               class='form-control'
                                                                               name='lastModificationTime'
                                                                               id='lastModificationTime_hidden_<%=i%>'
                                                                               value=<%=actionName.equals("edit")?("'" + prescription_detailsDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                                                       tag='pb_html'
                                                                        />
                                                                    </div>
                                                                
                                                                <%
                                                                    }
                                                                    else
                                                                    {
                                                                    	%>
                                                                    	 <%
                    
													                    if (prescription_detailsDTO.referredTo != -1) 
													                    {
													                %>
													                
														                	<div class="form-group row">
	                                                                            <label class="col-3 col-form-label text-right">
	                                                                                <%=LM.getText(LC.HM_REFERRED_TO, loginDTO)%>
	                                                                            </label>
	                                                                            <div class="col-9">
	                                                                                        <input type = "text" readonly
	                                                                                                class='form-control'
	                                                                                                name='referredTo' id='referredTo'
	                                                                                                value="<%=WorkflowController.getNameFromOrganogramId(prescription_detailsDTO.referredTo, Language)%>"
	                                                                                                tag='pb_html'>
	                                                                            </div>
	                                                                        </div>
	                                                                        <%
	                                                                        if (!prescription_detailsDTO.referralMessage.equalsIgnoreCase("")) 
	                                                                        {
	                                                                        	%>	                                                                        	
	                                                                        	<div class="form-group row">
		                                                                            <label class="col-3 col-form-label text-right">
		                                                                                <%=LM.getText(LC.HM_REFERRAL_MESSAGE, loginDTO)%>
		                                                                            </label>
		                                                                            <div class="col-9">
		                                                                                        <input type = "text" readonly
		                                                                                                class='form-control'
		                                                                                                name='referralMessage' id='referralMessage'
		                                                                                                value="<%=prescription_detailsDTO.referralMessage%>"
		                                                                                                tag='pb_html'>
		                                                                            </div>
	                                                                        	</div>
	                                                                        <%													                	
																            }
	                                                                        else
	                                                                        {
	                                                                        	%>
	                                                                        	<div class="col-9">
		                                                                                        <input type = "hidden" readonly
		                                                                                                class='form-control'
		                                                                                                value="<%=prescription_detailsDTO.referralMessage%>"
		                                                                                                tag='pb_html'>
		                                                                            </div>
	                                                                        	<%
	                                                                        }
                                                                    	}
                                                                    }
                                                                	%>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-0"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: Form Wizard Step 2-->

                    <!--begin: Form Wizard Step 3-->
                    <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                        <div class="kt-form__section kt-form__section--first">
                            <div class="kt-wizard-v3__nav-items px-5 mx-5">
                                <div class="row mt-3">
                                    <div class="col-12 text-center">
                                        <legend class="text-left content_legend" style="color: #0098bf">
                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2, loginDTO)%>
                                        </legend>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered text-nowrap">
                                        <thead>
                                        <tr>
                                            <th ><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_DRUGINFORMATIONTYPE, loginDTO)%>
                                            </th>
                                             <th ><%=LM.getText(LC.HM_QUANTITY, loginDTO)%>
                                            </th>
                                            <th ><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_DOSAGETYPECAT, loginDTO)%>
                                            </th>
                                            <th ><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_DOSE, loginDTO)%>
                                            </th>
                                            <th ><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_FREQUENCY, loginDTO)%>
                                            </th>
                                           
                                            <th ><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_FOODINSTRUCTIONSCAT, loginDTO)%>
                                            </th>
                                            <th style=""><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_REMOVE, loginDTO)%>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="field-PatientPrescriptionMedicine2">
                                        <%
                                            if (actionName.equals("edit")) {
                                                int index = -1;
                                                PatientPrescriptionMedicine2DAO patientPrescriptionMedicine2DAO = new PatientPrescriptionMedicine2DAO();
                                                prescription_detailsDTO.patientPrescriptionMedicine2DTOList = patientPrescriptionMedicine2DAO.getPatientPrescriptionMedicine2DTOListByPrescriptionDetailsID(prescription_detailsDTO.iD);
                                                for (PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO : prescription_detailsDTO.patientPrescriptionMedicine2DTOList) {
                                                    index++;

                                                    System.out.println("index index = " + index);

                                        %>
                                        <tr id="PatientPrescriptionMedicine2_<%=index + 1%>">
                                            <td style="display: none;">

                                                <input type='hidden' class='form-control'
                                                       name='patientPrescriptionMedicine2.iD'
                                                       id='iD_hidden_<%=childTableStartingID%>'
                                                       value='<%=patientPrescriptionMedicine2DTO.iD%>'
                                                       tag='pb_html'/>

                                            </td>
                                            <td style="display: none;">

                                                <input type='hidden' class='form-control'
                                                       name='patientPrescriptionMedicine2.prescriptionDetailsId'
                                                       id='prescriptionDetailsId_hidden_<%=childTableStartingID%>'
                                                       value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.prescriptionDetailsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                            </td>
                                            <td>
                                            	
                                            	<%
                                            	
                                            	if(actionName.equals("edit"))
                                            	{
                                            		
                                            		if(patientPrescriptionMedicine2DTO.drugInformationType != -1)
                                            		{
                                            			Drug_informationDTO drug_informationDTO = drug_informationDAO.getDrugInfoDetailsById(patientPrescriptionMedicine2DTO.drugInformationType);
	                                            		%>
	                                            		<input class='form-control'
	                                                       name='suggested_drug'
	                                                       id='suggested_drug_<%=childTableStartingID%>'
	                                                       type="text" tag='pb_html' readonly
	                                                       value = "<%=drug_informationDTO.stockText%><%=drug_informationDTO.drugText%>"
	                                                       style = "width:<%=drug_informationDTO.drugTextLen%>"
	                                                       ></input>
	                                                       
	                                                       <input name='formStr'
		                                                   id='formStr_<%=childTableStartingID%>' type="hidden"
		                                                   tag='pb_html'
		                                                   value='<%=drug_informationDTO.formName%>'></input>
		                                                   
		                                                   <input class='form-control'
	                                                       name='patientPrescriptionMedicine2.alternateDrugName'
	                                                       id='alternateDrugName_text_<%=childTableStartingID%>'
	                                                       type="hidden" tag='pb_html'
	                                                       value='<%=patientPrescriptionMedicine2DTO.alternateDrugName%>'>
	                                                       </input>
	                                            		<%
                                            		}
                                            		else
                                            		{
                                            			%>
                                            			<input class='form-control'
	                                                       name='suggested_drug'
	                                                       id='suggested_drug_<%=childTableStartingID%>'
	                                                       type="hidden" tag='pb_html' readonly
	                                                       value = ""
	                                                       
	                                                       ></input>
                                            			<input class='form-control'
                                                                name='patientPrescriptionMedicine2.alternateDrugName'
                                                                id='alternateDrugName_text_<%=childTableStartingID%>'' type="text"
                                                                tag='pb_html' readonly
                                                                value='<%=patientPrescriptionMedicine2DTO.alternateDrugName%>'></input>
                                                         <%
                                            			
                                            		}
													
                                            	}
                                            	%>
                                            
                                            	
                                               


                                                <div name="drug_modal_textdiv"
                                                     id="drug_modal_textdiv_<%=childTableStartingID%>"
                                                     tag='pb_html'></div>
                                                <input name='patientPrescriptionMedicine2.drugInformationType'
                                                       id='drugInformationType_select_<%=childTableStartingID%>'
                                                       type="hidden" tag='pb_html'
                                                       value='<%=patientPrescriptionMedicine2DTO.drugInformationType%>'></input>
                                                


                                            </td>
                                            <td>
                                            <input type='number' required style="width:80px"
                                                   class='form-control'
                                                   name='patientPrescriptionMedicine2.quantity'
                                                   id='quantity_text_<%=childTableStartingID%>' min='1'
                                                   value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.quantity + "'"):("'" + "1" + "'")%>   tag='pb_html'/>
                                        </td>

                                        <td>

                                            <select class='form-control' style="width:100px"
                                                    name='patientPrescriptionMedicine2.dosageTypeCat'
                                                    id='dosageTypeCat_category_<%=childTableStartingID%>'
                                                    onchange="setQuantity(this.id)" tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                    	%>
                                                    	<option value='-1'>Select</option>
                                                    	<%
                                                        Options = CatDAO.getOptions(Language, "dosage_type", patientPrescriptionMedicine2DTO.dosageTypeCat);
                                                    } else {
                                                        Options = CatDAO.getOptions(Language, "dosage_type", -1);
                                                    }
                                                %>
                                                <%=Options%>
                                            </select>

                                        </td>
                                        <td>

                                                            <textarea class='form-control' style="width:100px"
                                                                      name='patientPrescriptionMedicine2.dose'
                                                                      id='dose_text_<%=childTableStartingID%>'
                                                                      tag='pb_html'><%=actionName.equals("edit") ? (patientPrescriptionMedicine2DTO.dose) : ("")%></textarea>
                                        </td>

                                        <td>
                                            <div class="row" style="width:190px">
                                                <div class="col-lg-5">
                                                    <input type='number' min='0' required
                                                           class='form-control'
                                                           oninput="setQuantity(this.id)"
                                                           name='patientPrescriptionMedicine2.frequency'
                                                           id='frequency_text_<%=childTableStartingID%>' 
                                                           value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.frequency + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                                </div>
                                                <div class="col-lg-7">
                                                    <select class='form-control' required
                                                            onchange="setQuantity(this.id)"
                                                            name='patientPrescriptionMedicine2.frequencyCat'
                                                            id='frequencyCat_category_<%=childTableStartingID%>' 
                                                            tag='pb_html'>
                                                        <%
                                                            if (actionName.equals("edit")) {
                                                            	%>
                                                            	
                                                            	<%
                                                                Options = CatDAO.getOptions(Language, "frequency", patientPrescriptionMedicine2DTO.frequencyCat);
                                                            } else {

                                                                Options = CatDAO.getOptions(Language, "frequency", 1);
                                                            }
                                                        %>
                                                        <option value=''>Select</option>
                                                        <%=Options%>
                                                    </select>
                                                </div>
                                            </div>

                                        </td>
                                       

                                        <td>
                                            <select class='form-control' style="width:150px" required
                                                    name='patientPrescriptionMedicine2.foodInstructionsCat'
                                                    id='foodInstructionsCat_category_<%=childTableStartingID%>' tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                    	%>
                                                    	
                                                    	<%
                                                        Options = CatDAO.getOptions(Language, "food_instructions", patientPrescriptionMedicine2DTO.foodInstructionsCat);
                                                    } else {

                                                        Options = CatDAO.getOptions(Language, "food_instructions", -1);
                                                    }
                                                %>
                                               
                                                <%=Options%>
                                            </select>
                                        </td>
                                            <td style="display: none;">


                                                <input type='hidden' class='form-control'
                                                       name='patientPrescriptionMedicine2.isDeleted'
                                                       id='isDeleted_hidden_<%=childTableStartingID%>'
                                                       value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                            </td>
                                            <td style="display: none;">

                                                <input type='hidden' class='form-control'
                                                       name='patientPrescriptionMedicine2.lastModificationTime'
                                                       id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                                       value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                            </td>
                                            <td class="">
                                                <div class='checker'>
                                                        <span >
                                                            <input
                                                                    type='checkbox'
                                                                    id='patientPrescriptionMedicine2_cb_<%=index%>'
                                                                    name='checkbox' value=''
                                                            />
                                                        </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <%
                                                    childTableStartingID++;
                                                }
                                            }
                                        %>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 text-right my-5 pb-5">
                                        <button
                                                id="add-more-PatientPrescriptionMedicine2"
                                                name="add-morePatientPrescriptionMedicine2"
                                                type="button"
                                                class="btn btn-sm text-white add-btn shadow">
                                            <i class="fa fa-plus"></i>
                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_ADD_MORE, loginDTO)%>
                                        </button>
                                        <button
                                                id="remove-PatientPrescriptionMedicine2"
                                                name="removePatientPrescriptionMedicine2"
                                                type="button"
                                                class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                                <%PatientPrescriptionMedicine2DTO patientPrescriptionMedicine2DTO = new PatientPrescriptionMedicine2DTO();%>
                                <template id="template-PatientPrescriptionMedicine2">
                                    <tr>
                                        <td style="display: none;">

                                            <input type='hidden' class='form-control'
                                                   name='patientPrescriptionMedicine2.iD'
                                                   id='iD_hidden_2'
                                                   value='<%=patientPrescriptionMedicine2DTO.iD%>'
                                                   tag='pb_html'/>

                                        </td>
                                        <td style="display: none;">

                                            <input type='hidden' class='form-control'
                                                   name='patientPrescriptionMedicine2.prescriptionDetailsId'
                                                   id='prescriptionDetailsId_hidden_'
                                                   value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.prescriptionDetailsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                        </td>
                                        <td>
                                        
                                        		<div class="input-group">
 
											        <input
											          class="form-control py-2  border searchBox w-auto"
											          type="search"
											          placeholder="Search"
											          name='suggested_drug'
                                                      id='suggested_drug_'
                                                      tag='pb_html'
                                                      onkeyup="getDrugs(this.id, <%=prescription_detailsDTO.employeeId%>)"
                                                      onSearch = "clearIfNeeded(this.id)"
                                                      style="width:300px"
                                                      autocomplete="off"
											        />
											        <button
											            type="button"
											            class="btn-sm border-0 "
											            style="color: white; background-color: "#773455";"
											            onclick="toggleAlt(this.id)" id='toggle_button_'
											            tag='pb_html'
											    >
											        <i class="fa fa-plus" id='toggle_icon_' tag='pb_html'></i>
											    </button>
											        
											      </div>
											      <div
											        id="searchSgtnSection_"
											        class="search-sgtn-section shadow-sm bg-white"
													tag='pb_html'
													style = "display:none"
											      >
											        <div class="pt-3">
											          <ul class="px-0" style="list-style: none" id="drug_ul_" tag='pb_html'>
											          </ul>
											        </div>
											      </div>
											      
											      <div
											        id="lastVisitDiv_"
													tag='pb_html'
													style = "display:none"
											      >
											       
											      </div>
											      
											      
                                        
                                        	<input name='formStr'
                                                   id='formStr_' type="hidden"
                                                   tag='pb_html'
                                                   value=''></input>
                                                
                                            <div name="drug_modal_textdiv" id="drug_modal_textdiv_"
                                                 tag='pb_html'></div>
                                            <input name='patientPrescriptionMedicine2.drugInformationType'
                                                   id='drugInformationType_select_' type="hidden"
                                                   tag='pb_html'
                                                   value='<%=patientPrescriptionMedicine2DTO.drugInformationType%>'></input>
                                                   
                                          
                                            <input class='form-control'
                                                   name='patientPrescriptionMedicine2.alternateDrugName'
                                                   id='alternateDrugName_text_' type="hidden"
                                                   tag='pb_html'
                                                   value='<%=patientPrescriptionMedicine2DTO.alternateDrugName%>'></input>

                                        </td>
                                        
                                         <td>
                                            <input type='number' required style="width:80px"
                                                   class='form-control'
                                                   name='patientPrescriptionMedicine2.quantity'
                                                   id='quantity_text_' min='1'
                                                   value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.quantity + "'"):("'" + "1" + "'")%>   tag='pb_html'/>
                                        </td>

                                        <td>

                                            <select class='form-control' style="width:100px"
                                                    name='patientPrescriptionMedicine2.dosageTypeCat'
                                                    id='dosageTypeCat_category_'
                                                    onchange="setQuantity(this.id)" tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                    	%>
                                                    	<option value='-1'>Select</option>
                                                    	<%
                                                        Options = CatDAO.getOptions(Language, "dosage_type", patientPrescriptionMedicine2DTO.dosageTypeCat);
                                                    } else {
                                                        Options = CatDAO.getOptions(Language, "dosage_type", -1);
                                                    }
                                                %>
                                                <%=Options%>
                                            </select>

                                        </td>
                                        <td>

                                                            <textarea class='form-control' style="width:100px"
                                                                      name='patientPrescriptionMedicine2.dose'
                                                                      id='dose_text_'
                                                                      tag='pb_html'><%=actionName.equals("edit") ? (patientPrescriptionMedicine2DTO.dose) : ("")%></textarea>
                                        </td>

                                        <td>
                                            <div class="row" style="width:190px">
                                                <div class="col-lg-5">
                                                    <input type='number' min='0'
                                                           class='form-control' required
                                                           oninput="setQuantity(this.id)"
                                                           name='patientPrescriptionMedicine2.frequency'
                                                           id='frequency_text_' 
                                                           value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.frequency + "'"):("''")%>   tag='pb_html'/>
                                                </div>
                                                <div class="col-lg-7">
                                                    <select class='form-control' required
                                                            onchange="setQuantity(this.id)"
                                                            name='patientPrescriptionMedicine2.frequencyCat'
                                                            id='frequencyCat_category_' 
                                                            tag='pb_html'>
                                                        <%
                                                            if (actionName.equals("edit")) {
                                                            	%>
                                                            	
                                                            	<%
                                                                Options = CatDAO.getOptions(Language, "frequency", patientPrescriptionMedicine2DTO.frequencyCat);
                                                            } else {

                                                                Options = CatDAO.getOptions(Language, "frequency", 1);
                                                            }
                                                        %>
                                                        <option value=''>Select</option>
                                                        <%=Options%>
                                                    </select>
                                                </div>
                                            </div>

                                        </td>
                                       

                                        <td>
                                            <select class='form-control' style="width:150px"
                                                    name='patientPrescriptionMedicine2.foodInstructionsCat' 
                                                    id='foodInstructionsCat_category_' tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                    	%>
                                                    	
                                                    	<%
                                                        Options = CatDAO.getOptions(Language, "food_instructions", patientPrescriptionMedicine2DTO.foodInstructionsCat);
                                                    } else {

                                                        Options = CatDAO.getOptions(Language, "food_instructions", -1);
                                                    }
                                                %>
                                                
                                                <%=Options%>
                                            </select>
                                        </td>
                                        <td style="display: none;">
                                            <input type='hidden' class='form-control'
                                                   name='patientPrescriptionMedicine2.isDeleted'
                                                   id='isDeleted_hidden_'
                                                   value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
                                        </td>
                                        <td style="display: none;">
                                            <input type='hidden' class='form-control'
                                                   name='patientPrescriptionMedicine2.lastModificationTime'
                                                   id='lastModificationTime_hidden_'
                                                   value=<%=actionName.equals("edit")?("'" + patientPrescriptionMedicine2DTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                        </td>
                                        <td class="d-flex justify-content-center align-items-center">
                                            <div class="">
                                                        <span >
                                                            <input type='checkbox'
                                                                   name='checkbox'
                                                                   value=''
                                                            />
                                                        </span>
                                            </div>
                                        </td>
                                    </tr>

                                </template>
                            </div>
                        </div>
                    </div>
                    <!--end: Form Wizard Step 3-->

                    <!--begin: Form Wizard Step 4-->
                    <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                        <div class="kt-form__section kt-form__section--first">
                            <div class="kt-wizard-v3__form px-5 mx-5">
                                
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <legend class="text-left content_legend" style="color: #0098bf">
                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST, loginDTO)%>
                                        </legend>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th ><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTTYPE, loginDTO)%>
                                            </th>
                                            <th ><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTDETAILSTYPES, loginDTO)%>
                                            </th>

                                            <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_REMOVE, loginDTO)%>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="field-PrescriptionLabTest" style="border: 1px solid #D3D3D3">
                                        <%
                                            if (actionName.equals("edit")) {
                                                int index = -1;
                                               
                                                prescription_detailsDTO.prescriptionLabTestDTOList = prescriptionLabTestDAO.getPrescriptionLabTestDTOListByPrescriptionDetailsID(prescription_detailsDTO.iD);

                                                for (PrescriptionLabTestDTO prescriptionLabTestDTO : prescription_detailsDTO.prescriptionLabTestDTOList) {
                                                    index++;

                                                    System.out.println("index index = " + index);

                                        %>
                                        <tr id="PrescriptionLabTest_<%=index + 1%>">
                                            <td style="display: none;">
                                                <input type='hidden' class='form-control'
                                                       name='prescriptionLabTest.iD'
                                                       id='iD_hidden_<%=childTableStartingID%>'
                                                       value='<%=prescriptionLabTestDTO.iD%>'
                                                       tag='pb_html'/>

                                            </td>
                                            <td style="display: none;">
                                                <input type='hidden' class='form-control'
                                                       name='prescriptionLabTest.prescriptionDetailsId'
                                                       id='prescriptionDetailsId_hidden_<%=childTableStartingID%>'
                                                       value=<%=actionName.equals("edit")?("'" + prescriptionLabTestDTO.prescriptionDetailsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                            </td>
                                            <td>
                                                
                                                <div id = "lt_<%=childTableStartingID%>"  type = "text" name = "lt" >
                                                <%=prescriptionLabTestDAO.getTestName(prescriptionLabTestDTO)%>
                                                </div>
                                                
                                                <input type="hidden" class='form-control'
                                                       name='prescriptionLabTest.labTestType'
                                                       id='labTestType_select_<%=childTableStartingID%>'
                                                       value = "<%=prescriptionLabTestDTO.labTestType%>"
                                                       tag='pb_html'
                                                       value="<%=prescriptionLabTestDTO.description%>"/>
                                                <input type="hidden" class='form-control'
                                                       name='prescriptionLabTest.description'
                                                       id='description_text_<%=childTableStartingID%>'
                                                       tag='pb_html'
                                                       value="<%=prescriptionLabTestDTO.description%>"/>

                                                <input class='form-control' style="width:200px"
                                                       name='prescriptionLabTest.alternateLabTestName'
                                                       id='alternateLabTestName_text_<%=childTableStartingID%>'
                                                       type="hidden" tag='pb_html'
                                                       value='<%=prescriptionLabTestDTO.alternateLabTestName%>'></input>

                                            </td>
                                            <td>
                                            
                                            	<div id = "ltd_<%=childTableStartingID%>"  type = "text" name = "ltd" >
                                                <%=prescriptionLabTestDAO.getTestDetails(prescriptionLabTestDTO)%>
                                                </div>
                                                
                                                <input name = 'prescriptionLabTest.xRayReportId' 
                                                id='xRayReportId_select_<%=childTableStartingID%>'
                                            	value = '<%=prescriptionLabTestDTO.xRayReportId%>'
                                            	 type="hidden" tag='pb_html'></input>
                                                
                                                <input name='prescriptionLabTest.labTestDetailsTypesHidden'
                                                       id='hidden_labTestDetailsTypes_select_<%=childTableStartingID%>'
                                                       value = '<%=prescriptionLabTestDTO.labTestDetailsTypes%>'
                                                       type="hidden" tag='pb_html'></input>

                                                <input class='form-control' style="width:200px"
                                                       name='prescriptionLabTest.alternateLabTestDetails'
                                                       id='alternateLabTestDetails_text_<%=childTableStartingID%>'
                                                       type="hidden" tag='pb_html'
                                                       value='<%=prescriptionLabTestDTO.alternateLabTestDetails%>'></input>

                                                

                                            </td>
                                            <td style="display: none;">
                                                <input type='hidden' class='form-control'
                                                       name='prescriptionLabTest.isDeleted'
                                                       id='isDeleted_hidden_<%=childTableStartingID%>'
                                                       value=<%=actionName.equals("edit")?("'" + prescriptionLabTestDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                            </td>
                                            <td style="display: none;">
                                                <input type='hidden' class='form-control'
                                                       name='prescriptionLabTest.lastModificationTime'
                                                       id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                                       value=<%=actionName.equals("edit")?("'" + prescriptionLabTestDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                            </td>
                                            <td>
                                                <div class='checker'>
                                                            <span>
                                                                <input
                                                                        type='checkbox'
                                                                        id='prescriptionLabTest_cb_<%=index%>'
                                                                        name='checkbox' value=''/>
                                                            </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <%
                                                    childTableStartingID++;
                                                }
                                            }
                                        %>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 text-right my-5 pb-5">
                                        <button
                                                id="add-more-PrescriptionLabTest"
                                                name="add-morePatientPrescriptionMedicine2"
                                                type="button"
                                                class="btn btn-sm text-white add-btn shadow">
                                            <i class="fa fa-plus"></i>
                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_ADD_MORE, loginDTO)%>
                                        </button>
                                        <button
                                                id="remove-PrescriptionLabTest"
                                                name="removePatientPrescriptionMedicine2"
                                                type="button"
                                                class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                                <%PrescriptionLabTestDTO prescriptionLabTestDTO = new PrescriptionLabTestDTO();%>
                                <template id="template-PrescriptionLabTest">
                                    <tr>
                                        <td style="display: none;">

                                            <input type='hidden' class='form-control'
                                                   name='prescriptionLabTest.iD' id='iD_hidden_'
                                                   value='<%=prescriptionLabTestDTO.iD%>'
                                                   tag='pb_html'/>

                                        </td>
                                        <td style="display: none;">
                                            <input type='hidden' class='form-control'
                                                   name='prescriptionLabTest.prescriptionDetailsId'
                                                   id='prescriptionDetailsId_hidden_3'
                                                   value=<%=actionName.equals("edit")?("'" + prescriptionLabTestDTO.prescriptionDetailsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                        </td>
                                        <td>

                                            <select class='form-control' style="width:400px"
                                                    name='prescriptionLabTest.labTestType'
                                                    id='labTestType_select_'
                                                    onchange="labtestSelected(this.id)" tag='pb_html'>
                                                <%
                                                   
                                                    Options = Lab_testRepository.getInstance().getOptions(isLangEng, prescriptionLabTestDTO.labTestType);
                                                   
                                                %>
                                                
                                                <%=Options%>
                                            </select>
                                            
                                            
                                            
                                            
                                            <input type="hidden" class='form-control'
                                                   name='prescriptionLabTest.description'
                                                   id='description_text_' tag='pb_html'
                                                   value="<%=prescriptionLabTestDTO.description%>"/>

                                            <input class='form-control' style="width:200px"
                                                   name='prescriptionLabTest.alternateLabTestName'
                                                   id='alternateLabTestName_text_' type="hidden"
                                                   tag='pb_html'
                                                   value='<%=prescriptionLabTestDTO.alternateLabTestName%>'></input>

                                        </td>
                                        <td>
                                        	<select name='prescriptionLabTest.xRayReportId' style = "display:none"
                                                    id='xRayReportId_select_' tag='pb_html' onchange = "setDesc(this.id)" style="width:400px">
                                                    <option value = "-1"><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
		                                              <%
		                                              List<Xray_reportDTO> xray_reportDTOs = Xray_reportRepository.getInstance().getXray_reportList();
		                                              for(Xray_reportDTO xray_reportDTO: xray_reportDTOs)
		                                              {
		                                            	  %>
		                                            	  <option value = "<%=xray_reportDTO.iD%>" ><%=xray_reportDTO.comment%></option>
		                                            	  <%
		                                              }
		                                              %>
		                                              
                                            </select>
                                            <div id="ltdiv_" tag='pb_html' style="display:none">
                                                <select class='form-control' style="width:400px"
                                                        name='prescriptionLabTest.labTestDetailsTypes'
                                                        id='labTestDetailsTypes_select_' multiple="multiple"
                                                        onchange="showMoreTest(this.id)" tag='pb_html'>
                                                </select>
                                            </div>
                                            <input name='prescriptionLabTest.labTestDetailsTypesHidden'
                                                   style="width:200px"
                                                   id='hidden_labTestDetailsTypes_select_' type="hidden"
                                                   tag='pb_html'></input>
                                            <input class='form-control'
                                                   name='prescriptionLabTest.alternateLabTestDetails'
                                                   id='alternateLabTestDetails_text_' type="hidden"
                                                   tag='pb_html'
                                                   value='<%=prescriptionLabTestDTO.alternateLabTestDetails%>'></input>

                                        </td>


                                        <td style="display: none;">
                                            <input type='hidden' class='form-control'
                                                   name='prescriptionLabTest.isDeleted'
                                                   id='isDeleted_hidden_3'
                                                   value=<%=actionName.equals("edit")?("'" + prescriptionLabTestDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                        </td>
                                        <td style="display: none;">
                                            <input type='hidden' class='form-control'
                                                   name='prescriptionLabTest.lastModificationTime'
                                                   id='lastModificationTime_hidden_3'
                                                   value=<%=actionName.equals("edit")?("'" + prescriptionLabTestDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                        </td>
                                        <td>
                                            <div class="">
                                                        <span >
                                                            <input
                                                                    type='checkbox'
                                                                    name='checkbox'
                                                                    value=''/>
                                                        </span>
                                            </div>
                                        </td>
                                    </tr>

                                </template>
                            </div>
                        </div>
                    </div>
                    <!--end: Form Wizard Step 4-->
                    
                    
                    <%
                    if(appointmentDTO.isDental)
                    {
                    	%>
                    	
                    	
                    <!--begin: Form Wizard Step 5-->
                    <div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
                        <div class="kt-form__section kt-form__section--first">
                            <div class="kt-wizard-v3__form px-5 mx-5">
                                
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <legend class="text-left content_legend" style="color: #0098bf">
                                            <%=Language.equalsIgnoreCase("english")?"Dentistry":"ডেন্টিস্ট্রি"%>
                                        </legend>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-3 col-form-label text-right">
                                        <%=Language.equalsIgnoreCase("english")?"Print Dental Instructions":"দন্ত নির্দেশনা প্রিন্ট করুন"%>
                                    </label>
                                    <div class="col-9">
                                        <input
                                                type="checkbox"
                                                class='form-control-sm'
                                                name='dentalInstructionNeeded'
                                                id='dentalInstructionNeeded'
                                                tag='pb_html'
                                        />
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                         	<th ><%=LM.getText(LC.HM_ACTION, loginDTO)%>
                                            </th>
                                            <th ><%=Language.equalsIgnoreCase("english")?"Doctor/Dental Technologist":"ডাক্তার/ডেন্টাল টেকনোলজিস্ট"%>
                                            </th>
                                            <th ><%=LM.getText(LC.HM_DETAILS, loginDTO)%>
                                            </th>
											<th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_REMOVE, loginDTO)%>
                                            </th>
                                        </tr>
                                     </thead>
                                     <tbody id="field-dental" style="border: 1px solid #D3D3D3">
                                      <%
                                            if (actionName.equals("edit")) {
                                                int index = -1;
                                               
                                                prescription_detailsDTO.dental_activitiesDTOList = Dental_activitiesDAO.getInstance().getByPrescriptionId(prescription_detailsDTO.iD);
												
                                                if(prescription_detailsDTO.dental_activitiesDTOList != null)
                                                {
                                                	for (Dental_activitiesDTO dental_activitiesDTO : prescription_detailsDTO.dental_activitiesDTOList) {
                                                        index++;

                                                        System.out.println("index index = " + index);
                                                        
                                                        %>
                                                        <tr id="Dental_<%=index + 1%>">
                                                        	<td>
                                                        		<select class='form-control'
					                                                    name='dentalActionCat'
					                                                    id='dentalActionCat_select_<%=childTableStartingID%>'
					                                                    tag='pb_html'>
					                                                <%
					                                                   
					                                                    Options = CatDAO.getOptions("english", "dental_action", dental_activitiesDTO.dentalActionCat);
					                                                   
					                                                %>
					                                                
					                                                <%=Options%>
					                                            </select>
                                                        	</td>
                                                        	
                                                        	<td>
                                                        		<select class='form-control'
					                                                    name='dtOrganogramId'
					                                                    id='dtOrganogramId_select_<%=childTableStartingID%>'
					                                                    tag='pb_html'>
					                                                <%
					                                                   
					                                                    Options = CommonDAO.getDentalTechnologists(dental_activitiesDTO.dtOrganogramId, appointmentDTO.doctorId);
					                                                   
					                                                %>
					                                                
					                                                <%=Options%>
					                                            </select>
                                                        	</td>
                                                        	
                                                        	<td>
                                                        		<textarea class='form-control' 
                                                                      name='doctorRemarks'
                                                                      id='doctorRemarks_text_<%=childTableStartingID%>'
                                                                      tag='pb_html'><%=dental_activitiesDTO.doctorRemarks == null?"":dental_activitiesDTO.doctorRemarks
                                                                      %></textarea>
                                                        	</td>
															
															<td>
																<div class='checker'>
																		<span>
																			<input
																					type='checkbox'
																					id='dental_cb_<%=index%>'
																					name='checkbox' value=''/>
																		</span>
																</div>
															</td>
                                                        </tr>
                                                        <%
                                                        childTableStartingID++;
                                                    }
                                                }
                                                
                                            }

                                        %>
                                      </tbody>
                                 	</table>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-xs-9 text-right my-5 pb-5">
                                        <button
                                                id="add-more-Dental"
                                                name="add-more-Dental"
                                                type="button"
                                                class="btn btn-sm text-white add-btn shadow">
                                            <i class="fa fa-plus"></i>
                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_ADD_MORE, loginDTO)%>
                                        </button>
                                        <button
                                                id="remove-Dental"
                                                name="remove-Dental"
                                                type="button"
                                                class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                                <%Dental_activitiesDTO dental_activitiesDTO = new Dental_activitiesDTO();%>
                                <template id="template-Dental">
                                    <tr>
                                    	<td>
                                                        		<select class='form-control'
					                                                    name='dentalActionCat'
					                                                    id='dentalActionCat_select_'
					                                                    tag='pb_html'>
					                                                <%
					                                                   
					                                                    Options = CatDAO.getOptions("english", "dental_action", dental_activitiesDTO.dentalActionCat);
					                                                   
					                                                %>
					                                                
					                                                <%=Options%>
					                                            </select>
                                                        	</td>
                                                        	
                                                        	<td>
                                                        		<select class='form-control'
					                                                    name='dtOrganogramId'
					                                                    id='dtOrganogramId_select_'
					                                                    tag='pb_html'>
					                                                <%
					                                                   
					                                                    Options = CommonDAO.getDentalTechnologists(dental_activitiesDTO.dtOrganogramId, appointmentDTO.doctorId);
					                                                   
					                                                %>
					                                                
					                                                <%=Options%>
					                                            </select>
                                                        	</td>
                                                        	
                                                        	<td>
                                                        		<textarea class='form-control' 
                                                                      name='doctorRemarks'
                                                                      id='doctorRemarks_text_'
                                                                      tag='pb_html'><%=dental_activitiesDTO.doctorRemarks == null?"":dental_activitiesDTO.doctorRemarks
                                                                      %></textarea>
                                                        	</td>
															
															<td>
																<div class="">
																			<span >
																				<input
																						type='checkbox'
																						name='checkbox'
																						value=''/>
																			</span>
																</div>
															</td>
                                    </tr>
                                </template>
                             </div>
                          </div>
                      </div>
                      <!--end: Form Wizard Step 5-->  
                             	
                    	<%
                    }
                    %>

                    <div class="kt-wizard-v3__content" data-ktwizard-type="step-content"
                         data-ktwizard-state="current">
                        <div class="kt-form__section kt-form__section--first">
	                        <div class="kt-wizard-v3__form px-5 mx-5">
	                                
	                                <div class="row">
	                                    <div class="col-12 text-center">
	                                        <legend class="text-left content_legend" style="color: #0098bf">
	                                            <%=LM.getText(LC.HM_PRESCRIPTION, loginDTO)%>
	                                        </legend>
	                                    </div>
	                                </div>
	                                <%
	                                String dt = dateFormat.format(new Date(appointmentDTO.visitDate));
	                                %>
	                                <div class="kt-content prescription-details-report-page-for-browser" id="kt_content">
									    <div class="kt-portlet" id="modalbody" style="background-color: #f2f2f2!important;">
									        <div class="kt-portlet__body m-3" style="background-color: #f6f9fb!important;">
				                                <div class="mb-5">
					                                 <table class="w-100 mt-3">
									                    <tr>
									                        <td>
									                            <p class="mb-1"><%=LM.getText(LC.HM_ID, loginDTO)%>
									                                : <%if (appointmentDTO.sl != null) {%><%=Utils.getDigits(appointmentDTO.sl, Language)%>
									                                <br><%}%></p>
									                            <p class="mb-1"><%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
									                                : <%=appointmentDTO.patientName%>
									                                / <%=TimeFormat.getAge(appointmentDTO.dateOfBirth, Language)%>
									                            </p>
									                            <p class="mb-1"><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
									                                : <%=Utils.getDigits(appointmentDTO.employeeUserName, Language)%>
									                            </p>
									                            <small class="mb-0" id = "prevMeasurement">
									                               
									                            </small>
									                        </td>
									                        <td valign="top" align="right">
									                            <p class="mb-0"><%=LM.getText(LC.APPOINTMENT_ADD_VISITDATE, loginDTO)%>
									                                : <%=Utils.getDigits(dt, Language)%>
									                            </p>
									                        </td>
									                    </tr>
									                </table>
								                </div>
								                
								                <div  id = "diagPreview" >								                
								            	</div>
								            	
								            	<div class="mt-1 mx-3">
                									<table class="w-100 mt-4">
									                    <tr>
									                        <td>
									                            <h4 class="mb-0">R<sub>x</sub></h4>
									                        </td>
									                    </tr>
									                </table>
									            </div>
									            
									            <div class="mt-1 attachement-div table-responsive mx-3">
								                    <table class="w-100 border-bottom border-dark text-nowrap">
								                    	<thead>
									                        <tr class="border border-dark border-left-0 border-right-0">
									                            <th class="py-2"><%=LM.getText(LC.HM_SL, loginDTO)%>
									                            </th>
									                            
									                            <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_DRUGINFORMATIONTYPE, loginDTO)%>
									                            </th>
									                            <th><%=LM.getText(LC.HM_QUANTITY, loginDTO)%>
									                            </th>
									                            
									                            <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_DOSAGETYPECAT, loginDTO)%>
									                            </th>
									                            <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_FREQUENCY, loginDTO)%>
									                            </th>
									                            <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PATIENT_PRESCRIPTION_MEDICINE2_FOODINSTRUCTIONSCAT, loginDTO)%>
                            									</th>
									                            
									                         </tr>
								                         </thead>
								                         <tbody id = "drugBody">
								                         </tbody>
								                     </table>
								                </div>
								                
								                <div class="attachement-div mt-5 table-responsive mx-3">
								                    <table class="w-100 border-bottom border-dark text-nowrap">
								                    	<thead>
								                        	<tr class="border border-dark border-left-0 border-right-0">
									                            <th class="py-2"><%=LM.getText(LC.HM_SL, loginDTO)%>
									                            </th>
									                            <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTTYPE, loginDTO)%>
									                            </th>
									                            <th><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_PRESCRIPTION_LAB_TEST_LABTESTDETAILSTYPES, loginDTO)%>
									                            </th>
									                         </tr>
									                      </thead>
									                      <tbody id = "labBody">
								                         </tbody>
								                     </table>
								                </div>
								                
								                <%
									                if(appointmentDTO.isDental)
									                {
									                
									                	%>
									                	<br>
									                	<div class="attachement-div mt-5 table-responsive mx-3">
									                		<h4 class="mb-0"><%=Language.equalsIgnoreCase("english")?"Dental Actions":"দন্ত বিষয়ক কার্যকলাপ"%></h4>
									                		
										                	<table class="w-100 table-bordered border-bottom border-dark">
										                		<thead class="text-nowrap">
											                		<tr class="border border-dark border-left-0 border-right-0">
																		<th class="py-2"><%=LM.getText(LC.HM_SL, loginDTO)%></th>
																		<th><%=LM.getText(LC.HM_DOCTOR, loginDTO)%>/<%=LM.getText(LC.HM_DENTAL_TECHNOLIST, loginDTO)%>
                        												</th>
																		<th ><%=LM.getText(LC.HM_ACTION, loginDTO)%></th>            
											                            <th ><%=LM.getText(LC.HM_DETAILS, loginDTO)%></th>
																	</tr>
																</thead>
																<tbody id = "dentalBody">
																</tbody>
															</table>
														</div>
														
														<div id = "insBody" style = "display:none">
															<br><br>
									                		<div style = "text-align: right;">
										                		<img width="25%" src="<%=context%>/assets/images/dat.png"
									                                             alt=""/>
								                             </div>
														</div>
														<%
									                }
														%>
								                
								                <table class="w-100 mt-5">
									                <tr>
									                    <td align="right">
									                        <p class="mb-0">
									                            <%
									                                value = WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, Language);
									                            %>
									                            <%=value%>
									                        </p>
									                    </td>
									                </tr>
									                <tr>
									                    <td align="right">
									                        <p><%=appointmentDTO.qualifications%>
									                        </p>
									                    </td>
									                </tr>
									            </table>
									            
									            
								            </div>
								            
								            

								        </div>
								    </div>
	                                
	                        </div>
	                    </div>
                    </div>
                    
                    <%--start: bottom button section--%>
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">

                        <div class="kt-form__actions mx-5 px-5"
                             style="width: 100vw">
                            <div class="d-flex justify-content-between mb-5">
                                <button class="btn shadow cancel-btn text-white btn-border-radius"
                                        data-ktwizard-type="action-prev" onclick="tabDec()">
                                    <%=LM.getText(LC.HM_PREVIOUS, loginDTO)%>
                                </button>
                                <button class="btn shadow submit-btn text-white btn-border-radius"
                                        id="submitButton"
                                        style="display:none"
                                        type="submit">
                                    <%
                                        if (actionName.equals("edit")) {
                                    %>
                                    <%=LM.getText(LC.LAB_TEST_EDIT_LAB_TEST_SUBMIT_BUTTON, loginDTO)%>
                                    <%
                                    } else {
                                    %>
                                    <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_SUBMIT_BUTTON, loginDTO)%>
                                    <%
                                        }
                                    %>
                                </button>
                                <button class="btn shadow btn-brand ml-auto btn-border-radius"
                                        data-ktwizard-type="action-next" onclick="tabInc()"
                                        style="background-color: #4a87e2;">
                                    <%=LM.getText(LC.HM_NEXT_STEP, loginDTO)%>
                                </button>
                            </div>
                        </div>

                    </div>
                    <%--end: bottom button section--%>
                </div>
            </div>
        </form>
    </div>
</div>


<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    var drugModalRowId = -1;
    var drugForm = 0;
    var formStr = "";

    var tabId = 1;
    
    function generatePreview()
    {
    	var measurementText = "";
    	if($("#weight_text_0").val() != "" && $("#weight_text_0").val() != 0)
   		{
    		measurementText += "<%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_WEIGHT, loginDTO)%>: " + $("#weight_text_0").val() + " ";
   		}
    	if($("#height_text_0").val() != "" && $("#height_text_0").val() != 0)
   		{
    		measurementText += "<%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_HEIGHT, loginDTO)%>: " + $("#height_text_0").val() + " ";
   		}
    	if($("#height_text_0").val() != "" && $("#height_text_0").val() != 0)
   		{
    		measurementText += "<%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_HEIGHT, loginDTO)%>: " + $("#height_text_0").val() + " ";
   		}
    	if($("#sugar").val() != "" && $("#sugar").val() != 0)
   		{
    		measurementText += "<%=Language.equalsIgnoreCase("english")?"Blood Sugar":"রক্তের শর্করা"%>: " + $("#sugar").val() + " ";
   		}
    	if($("#bloodPressureSystole_text_0").val() != "" && $("#bloodPressureSystole_text_0").val() != 0
    			&& $("#bloodPressureDiastole_text_0").val() != "" && $("#bloodPressureDiastole_text_0").val() != 0)
   		{
    		measurementText += "<%=LM.getText(LC.HM_BLOOD_PRESSURE, loginDTO)%>: " + $("#bloodPressureSystole_text_0").val() + "/" 
    		+ $("#bloodPressureDiastole_text_0").val() + " ";
   		}
    	$("#prevMeasurement").html(measurementText);
    	
    	var diagText = "";
    	if($("#chiefComplaints_text_0").val() != "")
   		{
    		diagText += "<p class='mb-0'><strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_CHIEFCOMPLAINTS, loginDTO)%>:</strong> " 
    		+ $("#chiefComplaints_text_0").val() + "</p>";
   		}
    	if($("#clinicalFeatures_text_0").val() != "")
   		{
    		diagText += "<p class='mb-0'><strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_CLINICALFEATURES, loginDTO)%>:</strong> " 
    		+ $("#clinicalFeatures_text_0").val() + "</p>";
   		}
    	if($("#diagnosis_text_0").val() != "")
   		{
    		diagText += "<p class='mb-0'><strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_DIAGNOSIS, loginDTO)%>:</strong> " 
    		+ $("#diagnosis_text_0").val() + "</p>";
   		}
    	if($("#advice_text_0").val() != "")
   		{
    		diagText += "<p class='mb-0'><strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_ADVICE, loginDTO)%>:</strong> " 
    		+ $("#advice_text_0").val() + "</p>";
   		}
    	if($("#remarks_text_0").val() != "")
   		{
    		diagText += "<p class='mb-0'><strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_REMARKS, loginDTO)%>:</strong> " 
    		+ $("#remarks_text_0").val() + "</p>";
   		}
    	if($("#referredTo").length > 0)
   		{
    		console.log("rto = " + $("#referredTo").get(0).tagName);
    		if($("#referredTo").get(0).tagName.toLowerCase() == "select" && $("#referredTo").val() != "-1" && $("#referredTo").val() !="")
       		{
        		diagText += "<p class='mb-0'><strong><%=Language.equalsIgnoreCase("english")?"Forwarded To":"ফরওয়ার্ডকৃত চিকিৎসক"%>:</strong> " 
        		+ $( "#referredTo option:selected" ).text() + "</p>";
       		}
    		else if ($("#referredTo").get(0).tagName.toLowerCase() != "select")
   			{
    			diagText += "<p class='mb-0'><strong><%=Language.equalsIgnoreCase("english")?"Forwarded To":"ফরওয়ার্ডকৃত চিকিৎসক"%>:</strong> " 
        		+ $( "#referredTo" ).val() + "</p>";
   			}
   		}
    	
    	if($("#referralMessage").length > 0 && $("#referralMessage").val() != "")
   		{
    		diagText += "<p class='mb-0'><strong><%=LM.getText(LC.HM_REFERRAL_MESSAGE, loginDTO)%>:</strong> " 
    			+ $("#referralMessage").val() + "</p>";
   		}
    	
    	$("#diagPreview").html(diagText);
    	
    	var drugText = "";
    	var i = 1;
    	$("#field-PatientPrescriptionMedicine2 tr").each(function () {
    		drugText += "<tr>";
    		drugText += "<td class='py-2'>" + i + ".</td>";
			var altVal = $(this).find($('[name="patientPrescriptionMedicine2.alternateDrugName"]')).val();
			if(altVal == "")
			{
				var suggestedDrug = $(this).find($('[name="suggested_drug"]')).val();
				var splitted = suggestedDrug.split("|");
				suggestedDrug = splitted[0] + "|" + splitted[1] + "|" + splitted[2] + "|" + splitted[3];
				drugText += "<td>" + suggestedDrug + "</td>";
			}
			else
			{
				drugText += "<td>" + altVal + "</td>";
			}
    		
    		drugText += "<td>" + $(this).find($('[name="patientPrescriptionMedicine2.quantity"]')).val() + "</td>";
    		drugText += "<td>";
    		if($(this).find($('[name="patientPrescriptionMedicine2.dosageTypeCat"]')).val() != '-1'
    				&& $(this).find($('[name="patientPrescriptionMedicine2.dosageTypeCat"]')).val() != '')
   			{
    			var id = $(this).find($('[name="patientPrescriptionMedicine2.dosageTypeCat"]')).attr("id");
    			drugText += $("#" + id + " option:selected" ).text();
   			}
    		else
   			{
    			if($(this).find($('[name="patientPrescriptionMedicine2.dose"]')).val() != "")
   				{
    				drugText += $(this).find($('[name="patientPrescriptionMedicine2.dose"]')[0]).val();
   				}
    			else
   				{
    				drugText += "N/A";
   				}
    			
   			}
    		drugText += "</td>";
    		drugText += "<td>";
    		var id = $(this).find($('[name="patientPrescriptionMedicine2.frequencyCat"]')).attr("id");
    		if($(this).find($('[name="patientPrescriptionMedicine2.frequency"]')).val() == '0' || $(this).find($('[name="patientPrescriptionMedicine2.frequency"]')).val() == '')
   			{
    			drugText += "N/A";
   			}
    		else if($("#" + id).val() != '-3' && $("#" + id).val() != '')
   			{
    			
    			drugText += $(this).find($('[name="patientPrescriptionMedicine2.frequency"]')).val() + " " 
    			+ $( "#" + id + " option:selected" ).text();
   			}
    		else
   			{
    			drugText += $( "#" + id + " option:selected" ).text();
    			
   			}
    		drugText += "</td>";
    		id = $(this).find($('[name="patientPrescriptionMedicine2.foodInstructionsCat"]')).attr("id");
    		drugText += "<td>" + $( "#" + id + " option:selected" ).text() + "</td>";
    		drugText += "</tr>";
    		i++;
        });
    	
    	$("#drugBody").html(drugText);
    	
    	
    	var labText = "";
    	var labI = 1;
    	
    	$("#field-PrescriptionLabTest tr").each(function () {
    		
    		labText += "<tr>";
    		labText += "<td class='py-2'>" + labI + ".</td>";
    		labText += "<td>";
    		
    		if($(this).find($('[name="lt"]')).length > 0)
   			{
    			labText += $(this).find($('[name="lt"]')).html();
   			}
    		else
   			{
    			var id = $(this).find($('[name="prescriptionLabTest.labTestType"]')).attr("id");
    			if($("#" + id).val() == <%=SessionConstants.LAB_TEST_OTHER%>)
   				{
    				labText += $(this).find($('[name="prescriptionLabTest.description"]')).val();
   				}
    			else
   				{
    				if($("#" + id).select2('data') != null)
           			{
            			labText += $("#" + id).select2('data')[0]['text'];
           			}
            		if($(this).find($('[name="prescriptionLabTest.description"]')).val() != "")
           			{
            			labText += ": " + $(this).find($('[name="prescriptionLabTest.description"]')).val();
           			}
   				}
        		
   			}
    		labText += "</td>";
    		
    		
    		labText += "<td>";
    		if($(this).find($('[name="ltd"]')).length > 0)
   			{
    			labText += $(this).find($('[name="ltd"]')).html();
   			}
    		else
   			{
    			var id = $(this).find($('[name="prescriptionLabTest.labTestDetailsTypes"]')).attr("id");
        		var details = $("#" + id).select2('data');
        		var first = true;
        		var labTextSub = "";
        		if(details != null)
       			{
        			for (let j = 0; j < details.length; j++) 
            		{
            			console.log("i = " + i  + " id = " + details[j].id + " text = " + details[j].text);
            			if(!first)
           				{
            				labTextSub += ", ";
           				}
            			if(details[j].id >= 0)
           				{
            				labTextSub += details[j].text;
                			first = false;
                			
           				}
            			else if(details[j].id == -3)
           				{
            				labTextSub = "All";
            				break;
           				}
            			else if(details[j].id == -4)
           				{
            				labTextSub = $(this).find($('[name="prescriptionLabTest.alternateLabTestDetails"]')).val();
            				break;
           				}
            			
            		}
       			}
        		
        		labText += labTextSub;   			
   			}
    		
    		labText += "</td>";
    		labText += "</tr>";
    		i++;
    		labI ++;
    	});
    	
    	$("#labBody").html(labText);
    	
    	<%
    	if(appointmentDTO.isDental)
    	{
    		%>
    	var dentalText = 0;
    	var dentalI = 1;
    	$("#field-dental tr").each(function () {
    		
    		dentalText += "<tr>";
    		dentalText += "<td class='py-2'>" + dentalI + ".</td>";
    		
			var dtOrganogramId = $(this).find($('[name="dtOrganogramId"]')).attr("id");
    		
    		dentalText += "<td>";
    		dentalText += $( "#" + dtOrganogramId + " option:selected" ).text();
    		dentalText += "</td>";

    		var id = $(this).find($('[name="dentalActionCat"]')).attr("id");
    		
    		dentalText += "<td>";
    		dentalText += $( "#" + id + " option:selected" ).text();
    		dentalText += "</td>";
    		
    		dentalText += "<td>";
    		dentalText += $(this).find($('[name="doctorRemarks"]')).val();
    		dentalText += "</td>";
    		
    		dentalText += "</tr>";
    	});
    	
    	$("#dentalBody").html(dentalText);
    	
    	if($("#dentalInstructionNeeded").prop("checked"))
   		{
    		$("#insBody").show();
   		}
    	else
    	{
    		$("#insBody").hide();
    	}
    	
    		<%
    	}
    	%>
    }

    function toggleSubmitButton() {
        if (tabId == <%=maxTabId%>) {
            $("#submitButton").css("display", "block");
            generatePreview();
        } else {
            $("#submitButton").css("display", "none");
        }
    }

    function tabclicked(val) {
        tabId = val;
        console.log("tab id = " + tabId);
        toggleSubmitButton();
    }

    function tabDec() {
        tabId--;
        console.log("tab id = " + tabId);
        toggleSubmitButton();
    }

    function tabInc() {
        tabId++;
        console.log("tab id = " + tabId);
        toggleSubmitButton();
    }


    function setDrugLabel(value, stock) {

    }

    function setQuantity(id) {
    	return;
        console.log("setQuantity called with id = " + id);
        var quantityToSet = 1;
        var rowId = id.split("_")[2];

       
        formStr = $("#formStr_" + rowId).val().toLowerCase();
        console.log("formStr = " + formStr);
        if (formStr.includes("capsule") || formStr.includes("tablet") || formStr.includes("sachet"))//capsule
        {

            var dosaseSelect = $("#dosageTypeCat_category_" + rowId);
            if ($('option:selected', dosaseSelect).val() != -1) {
                var dosageText = $('option:selected', dosaseSelect).html();
                var dose = 0;
                var dosageTextSplitted = dosageText.split("+");
                $.each(dosageTextSplitted, function (index, value) {
                    dose += parseInt(value);
                });
                console.log("dose = " + dose);
                var frequency = $("#frequency_text_" + rowId).val();
                var frequencyCat = $("#frequencyCat_category_" + rowId).val();

                if (frequencyCat > 0) {
                    var quantity = parseInt(frequency) * parseInt(frequencyCat) * parseInt(dose);
                    quantityToSet = quantity;

                    console.log("1 quantity = " + quantityToSet);
                } else {
                    quantityToSet = 1;
                    console.log("2 quantity = " + quantityToSet);
                }

            } else {
                quantityToSet = 1;
                console.log("3 quantity = " + quantityToSet);
            }

        } else {
            quantityToSet = 1;

            console.log("4 quantity = " + quantityToSet);
        }

		if(quantityToSet != '' && quantityToSet != null && !isNaN(quantityToSet))
		{
			$("#quantity_text_" + rowId).val(quantityToSet);
		}
        
        
        if (formStr.includes("syrup") || formStr.includes("drops"))//liquid
        {
        	console.log("setting max = 3");
        	$("#quantity_text_" + rowId).attr("max", "3");
        }
        else
       	{
        	$("#quantity_text_" + rowId).removeAttr("max");
       	}
        if ($("#frequencyCat_category_" + rowId).val() < 0) {
            $("#frequency_text_" + rowId).attr("type", "hidden");
        } else {
            $("#frequency_text_" + rowId).attr("type", "number");
        }
    }



	
    function PreprocessBeforeSubmiting(row, validate) {
        console.log("preprocessing");
        processMultipleSelectBoxBeforeSubmit("prescriptionLabTest.labTestDetailsTypes");
        //return true;
        var drugs = [];
        var duplicateFound = false;
         $( "[name= 'patientPrescriptionMedicine2.drugInformationType']" ).each(function( index )
         {
        	console.log("alt val = " + $( "[name= 'patientPrescriptionMedicine2.alternateDrugName']" ).eq(index).val());
	        if ($(this).val() != -1 && drugs.includes($(this).val())) 
	        {
	            toastr.error("Duplicate/Invalid Medicine is not allowed");
	            duplicateFound = true;
	            return;
	        }       
	        else if ($(this).val() == <%=Drug_informationDTO.ACCU_CHECK_ID%>) 
	        {
	            toastr.error("You cannot prescribe Accu Check");
	            duplicateFound = true;
	            return;
	        }
	        else if ($(this).val() == -1) 
	        {
	            toastr.error("Invalid medicine");
	            duplicateFound = true;
	            return;
	        }
	        
	        if (parseInt($( "[name= 'patientPrescriptionMedicine2.quantity']" ).eq(index).val()) < 1) 
	        {
	            toastr.error("Quantity must be at least 1");
	            duplicateFound = true;

	        }
	        else 
	        {
	            drugs.push($(this).val());
	        }
	      });
         
         var invalidDoseIndex = -1;
         $( "[name= 'patientPrescriptionMedicine2.dosageTypeCat']" ).each(function( index )
         {
        	 if ($(this).val() == -1)
        	 {
        		 invalidDoseIndex = index;

        	 }
        	 
         });
         if(invalidDoseIndex != -1)
       	 {
       	 	if($( "[name= 'patientPrescriptionMedicine2.dose']" ).eq(invalidDoseIndex).val() == '')
   	 		{
	       	 	toastr.error("Dosage need to be chosen");
	            duplicateFound = true;
	            return;
   	 		}
       	 }
         
         $( "[name= 'patientPrescriptionMedicine2.foodInstructionsCat']" ).each(function( index )
         {
        	 if ($(this).val() == '' || $(this).val() == '-1')
        	 {
        		 toastr.error("Instructions need to be chosen");
        		 duplicateFound = true;

        	 }
        	 
         });
         
         $( "[name= 'patientPrescriptionMedicine2.frequency']" ).each(function( index )
         {
        	 if ($(this).val() == '')
        	 {
        		 toastr.error("Durations need to be chosen");
        		 duplicateFound = true;

        	 }
        	 
         });
         
         $( "[name= 'patientPrescriptionMedicine2DTO.frequencyCat']" ).each(function( index )
         {
        	 if ($(this).val() == '')
        	 {
        		 toastr.error("Durations need to be chosen");
        		 duplicateFound = true;

        	 }
        	 
         });
         
         $( "[name= 'dentalActionCat']" ).each(function( index )
          {
         	 if ($(this).val() == '-1')
         	 {
         		 toastr.error("Dental Action cannot be empty");
         		 duplicateFound = true;

         	 }
         	 
          });
                 
         
         
	        
	        console.log("Duplicates found = " + duplicateFound);
	       
	        
	        if(!duplicateFound)
	        {
	        	 if(confirm('Are you sure?')) {
	        		 $("#submitButton").prop("disabled",true);
	        		 history.pushState(null, document.title, location.href);
	        		 window.addEventListener('popstate', function (event)
	        		 {
	        		   history.pushState(null, document.title, location.href);
	        		 });
	        		 document.getElementById('kt_form').submit();
	             }
	        	
	        }
        
        
    }

    function labtestSelected(labTestId) {
        var rowId = labTestId.split("_")[2];
        var labtestDetailsId = "labTestDetailsTypes_select_" + rowId;
        var value = $("#" + labTestId).val();
        console.log("labtestSelected = " + value);
        var text = $("#" + labTestId + " option:selected").text();
		$("#description_text_" + rowId).val("");
		$("#xRayReportId_select_" + rowId).val("-1");
		if(value == <%=SessionConstants.LAB_TEST_OTHER%>)
		{
			$("#description_text_" + rowId).attr("type", "text");
            $("#description_text_" + rowId).css("width", "400px");
            if ($("#xRayReportId_select_" + rowId).data('select2')) 
			{
			   $("#xRayReportId_select_" + rowId).select2('destroy');
			}
			$("#xRayReportId_select_" + rowId).css("display", "none");
			return;
		}
		else if (value == <%=SessionConstants.LAB_TEST_XRAY%>) 
		{
            $("#description_text_" + rowId).attr("type", "hidden");
			$("#xRayReportId_select_" + rowId).removeAttr("style");
			$("#xRayReportId_select_" + rowId).select2({
				dropdownAutoWidth: true,
				theme: "classic"
			});		
			return;
        }
		else if (value == <%=SessionConstants.LAB_TEST_ULTRASONOGRAM%>) 
		{
            $("#description_text_" + rowId).attr("type", "text");
            $("#description_text_" + rowId).css("width", "400px");
			if ($("#xRayReportId_select_" + rowId).data('select2')) 
			{
			   $("#xRayReportId_select_" + rowId).select2('destroy');
			}
			$("#xRayReportId_select_" + rowId).css("display", "none");
			return;
        }
		else 
		{
            $("#description_text_" + rowId).attr("type", "hidden");
			if ($("#xRayReportId_select_" + rowId).data('select2')) 
			{
			   $("#xRayReportId_select_" + rowId).select2('destroy');
			}
			$("#xRayReportId_select_" + rowId).css("display", "none");
        }

        if (value >= 0) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    console.log(this.responseText);
                    var options = this.responseText.split('<option');

                    if (options.length >= 1) {
                        $("#" + labtestDetailsId).html(this.responseText);
                        $("#ltdiv_" + rowId).css("display", "block");
                    } else {
                        console.log("making inv");
                        $("#ltdiv_" + rowId).css("display", "none");
                    }

                } else {
                    console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
                }
            };

            xhttp.open("POST", "Prescription_detailsServlet?actionType=getLabtestDetails&id=" + value, true);
            xhttp.send();
        } else {
            $("#" + labtestDetailsId).html("");
        }


    }
	
	function setDesc(id)
	{
		var rowId = id.split("_")[2];
		var value = $("#" + id).val();
		var descriptionElem = "description_text_" + rowId;
		if(value == "-1")
		{
			$("#" + descriptionElem).val("");
		}
		else
		{
			var text = $("#" + id + " option:selected").text();
			$("#" + descriptionElem).val(text);
		}
	}


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Prescription_detailsServlet");
    }

    function init(row) {
        
        /*$("[name='prescriptionLabTest.labTestDetailsTypes']").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });*/
        
        console.log("setting freq");
        for (i = 1; i < child_table_extra_id; i++) 
        {
        	
        	if ($("#frequencyCat_category_" + i).val() < 0) {
                $("#frequency_text_" + i).attr("type", "hidden");
            } else {
                $("#frequency_text_" + i).attr("type", "number");
            }
        }
    }

    var row = 0;

    $(document).ready(function () {
        init(row);
        //CKEDITOR.replaceAll();
    });

    var openSelect2;

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-PatientPrescriptionMedicine2").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-PatientPrescriptionMedicine2");

            $("#field-PatientPrescriptionMedicine2").append(t.html());
            SetCheckBoxValues("field-PatientPrescriptionMedicine2");

            var tr = $("#field-PatientPrescriptionMedicine2").find("tr:last-child");

            tr.attr("id", "PatientPrescriptionMedicine2_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                //console.log(index + ": " + $(this).attr('id'));
            });


            child_table_extra_id++;

        });


    $("#remove-PatientPrescriptionMedicine2").click(function (e) {
        var tablename = 'field-PatientPrescriptionMedicine2';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });
    $("#add-more-PrescriptionLabTest").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-PrescriptionLabTest");

            $("#field-PrescriptionLabTest").append(t.html());
            SetCheckBoxValues("field-PrescriptionLabTest");

            var tr = $("#field-PrescriptionLabTest").find("tr:last-child");

            tr.attr("id", "PrescriptionLabTest_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                //console.log(index + ": " + $(this).attr('id'));
            });
            
            $( "[name = 'prescriptionLabTest.labTestType']" ).each(function( index ) {
            	  if($( this ).val() != -1 && $( this ).val() != <%=SessionConstants.LAB_TEST_XRAY%> && $( this ).val() != <%=SessionConstants.LAB_TEST_ULTRASONOGRAM%>) // 400 = xray, 401 = ultrasono
            	  {
            		  $("#labTestType_select_" + child_table_extra_id + " option[value='" + $( this ).val() + "']").remove();
            	  }
            	});

            $("#labTestType_select_" + child_table_extra_id).select2({
                dropdownAutoWidth: true,
                theme: "classic"
            });
            
            /*
            $("#labTestType_select_" + child_table_extra_id).select2({
                dropdownAutoWidth: true,
                theme: "classic",
                language: {
                    noResults: function (term) {
                        console.log(event.target.value);
                        typed = event.target.value;
                        if (typed.length >= 4) {
                            //console.log("adding new option");
                            var newOption = new Option(typed, "-4", true, true);
                            var id = openSelect2;
                            //console.log("current id = " + id + " presumed id = " + "#labTestType_select_" + child_table_extra_id);
                            // Append it to the select
                            $("#" + openSelect2).append(newOption).trigger('change');
                            var rowId = openSelect2.split("_")[2];
                            var hiddenId = "alternateLabTestName_text_" + rowId;
                            $("#" + hiddenId).val(typed);
                        }
                    }
                }
            })
                .on("select2:open", function (e) {
                    openSelect2 = e.target.id;
                });
            */

            $("[name='prescriptionLabTest.labTestDetailsTypes']").select2({
                dropdownAutoWidth: true,
                theme: "classic"
            });
            
            child_table_extra_id++;

        });


    $("#remove-PrescriptionLabTest").click(function (e) {
        var tablename = 'field-PrescriptionLabTest';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });

    $(document).ready(function () {

        $('.datepicker').datepicker({

            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            todayHighlight: true,
            showButtonPanel: true
        });
    });

    

    function showMoreTest(id) {
        console.log("id = " + id);
        var value = $("#" + id).val();
        console.log("value = " + value);

        var othersChosen = false;
        if (value != null && value.includes("-4")) {
            othersChosen = true;
        }
        var rowId = id.split("_")[2];
        var alt = $("#alternateLabTestDetails_text_" + rowId);
        if (othersChosen) {
            alt.attr("type", "text");
            alt.css("width", "400px");
        } else {
            alt.attr("type", "hidden");
        }
    }

    function showReferredToBlock(val) {
        console.log("showReferredToBlock called");
        if (val != -1) {
            $('#referralDiv').css('display', 'block');
            $("#createAppointment").prop( "checked", true );
        } else {
            $('#referralDiv').css('display', 'none');
            $("#createAppointment").prop( "checked", false );
        }
    }
    
    function drugClicked(rowId, drugText, drugId, form, stock, len, showTick)
    {
    	console.log("drugClicked with " + rowId + ", drugId = " + drugId + ", form = " + form);
    	
    	if(showTick)
		{
    		drugText = "✔ " + drugText;
		}
		else
		{
			drugText = "✖ " + drugText;
		}
    	
    	$("#suggested_drug_" + rowId).val(drugText);
    	$("#suggested_drug_" + rowId).attr("style", "width: " +len + "px !important");
    	$("#drug_ul_" + rowId).html("");
		$("#drugInformationType_select_" + rowId).val(drugId);
		$("#formStr_" + rowId).val(form);
		$("#alternateDrugName_text_" + rowId).val("");
		$("#searchSgtnSection_"+ rowId).css("display", "none");
		console.log("stock = " + stock);
		
		
    	
    	var drugModalRowId = rowId;
        var drugForm = 0;
        var formStr = form;
        
        getLast(drugId, rowId);
        
        setQuantity("drugInformationType_select_" + rowId);
    }
	
	function toggleAlt(id)
	{
		var rowId = id.split("_")[2];
		var altId = "alternateDrugName_text_" + rowId;
		console.log("altId = " + altId);
		if($("#" + altId).attr("type") == "hidden")
		{
			$("#" + altId).attr("type", "text");
			$("#toggle_icon_" + rowId).attr("class", "fa fa-minus");
			$("#suggested_drug_" + rowId).val("");
			$("#suggested_drug_" + rowId).prop("readonly", "readonly");
			$("#drugInformationType_select_" + rowId).val(-1);
			$("#lastVisitDiv_" + rowId).html("");
			$("#lastVisitDiv_" + rowId).hide();
		}
		else
		{
			$("#" + altId).attr("type", "hidden");
			$("#toggle_icon_" + rowId).attr("class", "fa fa-plus");
			$("#suggested_drug_" + rowId).removeAttr("readonly");
		}
	
	}
	
	
	function getLast(drugInformationId, rowId)
	{
		$("#lastVisitDiv_" + rowId).html("");
		$("#lastVisitDiv_" + rowId).hide();
		var xhttp = new XMLHttpRequest();
	    xhttp.onreadystatechange = function () {
	        if (this.readyState == 4 && this.status == 200) {
	        	var patientPrescriptionMedicine2DTO = JSON.parse(this.responseText);
	        	if(patientPrescriptionMedicine2DTO.visitTime != -1)
        		{
        			var text = "<b>" + patientPrescriptionMedicine2DTO.quantity + "</b>" 
        			+ " piece(s) prescribed on " 
        			+ "<b>" + patientPrescriptionMedicine2DTO.formattedVisitTime + "</b>" ;
        			$("#lastVisitDiv_" + rowId).html(text);
        			$("#lastVisitDiv_" + rowId).show();
        		}
	        	else
        		{
	        		$("#lastVisitDiv_" + rowId).html("");
        			$("#lastVisitDiv_" + rowId).hide();
        		}
	        	
	        } else if (this.readyState == 4 && this.status != 200) {
	            alert('failed ' + this.status);
	        }
	    };

	    xhttp.open("GET",  "Prescription_detailsServlet?actionType=getLastDrugReceived&drugInformationId=" + drugInformationId
	    		+ "&erId=<%=appointmentDTO.erId%>&whoIsThePatientCat=<%=appointmentDTO.whoIsThePatientCat%>"
	    		, true);
	    xhttp.send();

	}
	
	function clearIfNeeded(id)
	{
		var rowId = id.split("_")[2];
		var text = $("#suggested_drug_" + rowId).val();
		if(text == "")
		{
			$("#lastVisitDiv_" + rowId).html("");
			$("#lastVisitDiv_" + rowId).hide();
		}
	}
	
	
    $("#add-more-Dental").click(
            function (e) {
                e.preventDefault();
                var t = $("#template-Dental");

                $("#field-dental").append(t.html());
                SetCheckBoxValues("field-dental");

                var tr = $("#field-dental").find("tr:last-child");

                tr.attr("id", "Dental_" + child_table_extra_id);

                tr.find("[tag='pb_html']").each(function (index) {
                    var prev_id = $(this).attr('id');
                    $(this).attr('id', prev_id + child_table_extra_id);
                    //console.log(index + ": " + $(this).attr('id'));
                });
        
               
                
                child_table_extra_id++;

            });


        $("#remove-Dental").click(function (e) {
            var tablename = 'field-dental';
            var i = 0;
            console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
            var element = document.getElementById(tablename);

            var j = 0;
            for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
                var tr = document.getElementById(tablename).childNodes[i];
                if (tr.nodeType === Node.ELEMENT_NODE) {
                    console.log("tr.childNodes.length= " + tr.childNodes.length);
                    var checkbox = tr.querySelector('input[type="checkbox"]');
                    if (checkbox.checked == true) {
                        tr.remove();
                    }
                    j++;
                }

            }
        });
    
    


</script>






