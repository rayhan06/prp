<%@page import="lab_test.LabTestListDAO" %>
<%@page import="lab_test.LabTestListDTO" %>
<%@page import="common_lab_report.Common_lab_reportDAO" %>
<%@page import="common_lab_report.Common_lab_reportDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="prescription_details.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="java.awt.image.BufferedImage" %>
<%@ page import="javax.imageio.ImageIO" %>
<%@ page import="java.io.ByteArrayOutputStream" %>
<%@ page import="java.io.File" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="appointment.*" %>
<%@page import="drug_information.*" %>
<%@page import="workflow.WorkflowController" %>
<%@ page import="util.*" %>
<%@ page import="user.*" %>
<%@ page import="medical_equipment_name.*" %>
<%@ page import="common.QRCodeUtil" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    int nextYear = Calendar.getInstance().get(Calendar.YEAR) + 1;
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.PRESCRIPTION_DETAILS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO("prescription_details");
    Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();


    AppointmentDAO appointmentDAO = new AppointmentDAO();
    AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);

    String addMedicine = request.getParameter("addMedicine");
    if (userDTO.roleID == SessionConstants.PHARMACY_PERSON && !appointmentDTO.employeeUserName.equalsIgnoreCase(userDTO.userName)) {
        addMedicine = "true";
    }

    String addLabTest = request.getParameter("addLabTest");
    if (userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE && !appointmentDTO.employeeUserName.equalsIgnoreCase(userDTO.userName)) {
        addLabTest = "true";
    }
    String justView = request.getParameter("justView");

    SimpleDateFormat ttDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
    Date date = new Date();
    String datestr = ttDateFormat.format(date);

//Drug Nam params
    boolean isPermanentTable = true;
    String url = "Drug_informationServlet?actionType=search";

    String[][] searchFieldInfo = SessionConstants.SEARCH_DRUG_INFORMATION;
    Common_lab_reportDAO common_lab_reportDAO = new Common_lab_reportDAO();

    String context = request.getContextPath() + "/";

    String errorMsg = request.getParameter("errorMsg");
    String barCode = QRCodeUtil.getBarCodeAsBase64(prescription_detailsDTO.sl);
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<%
    String dt = ttDateFormat.format(new Date(prescription_detailsDTO.visitTime));
    String visitTime = dateTimeFormat.format(new Date(prescription_detailsDTO.visitTime));
    int sl = 0;
    PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
    List<PrescriptionLabTestDTO> prescriptionLabTestDTOs;
    PatientPrescriptionMedicine2DAO patientPrescriptionMedicine2DAO = new PatientPrescriptionMedicine2DAO();
    List<PatientPrescriptionMedicine2DTO> patientPrescriptionMedicine2DTOs;
%>

<style>
    .website-link,
    .website-link:visited,
    .website-link:focus {
        color: #000 !important;
        text-decoration: none;
    }

    .website-link:hover {
        text-decoration: none;
    }

    .prescription-details-report-page-for-app {
        visibility: hidden;
        height: 0;
    }

    .prescription-details-report-page-for-app table th, .prescription-details-report-page-for-app table td {
        padding-left: .5rem;
    }

    @media print {
        td, th {
            font-size: 1.9rem !important;
        }

        h4 {
            font-size: 150% !important;
        }

        .prp-logo {
            width: 70% !important;
        }

        .parliament-logo {
            width: 15% !important;
        }

        .header-section {
            margin-top: 0px !important;
        }

        .barcode-image {
            width: 380px !important;
            margin-right: -30px;
        }
    }


      .signature-container {
        max-width: 250px;
        text-align: center;
      }

      .signature-container .signature-image {
        width: 200px;
        object-fit: contain;
      }
    </style>



<div class="ml-auto mr-2 mt-4">
    <%
        if ((addLabTest == null && addMedicine == null) || justView != null) {
    %>
    <button type="button" class="btn" id='pdf'
            onclick="downloadPdf('prescription.pdf','modalbody')">
        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='printer'>
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <%
        }
    %>
    <%
        if (addLabTest == null && addMedicine == null) {
    %>
    <button type="button" class="btn" id='cross'
            onclick="location.href='Prescription_detailsServlet?actionType=search'">
        <i class="fa fa-times fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <%
    } else if (addLabTest != null) {
    %>
    <button type="button" class="btn" id='cross'
            onclick="location.href='Prescription_detailsServlet?actionType=search&addLabTest=1'">
        <i class="fa fa-times fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <%
    } else if (addMedicine != null) {
    %>
    <button type="button" class="btn" id='cross'
            onclick="location.href='Prescription_detailsServlet?actionType=search&addMedicine=1'">
        <i class="fa fa-times fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <%
        }
    %>
    <%
        if ((prescription_detailsDTO.doctorId == userDTO.organogramID 
        || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
        || userDTO.roleID == SessionConstants.ADMIN_ROLE
        )
        && prescription_detailsDTO.canBeEdited) {
    %>

    <button type="button" class="btn" id='cross'
            onclick="location.href='Prescription_detailsServlet?actionType=getEditPage&ID=<%=prescription_detailsDTO.iD%>'">
        <i class="fa fa-edit fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <%
        }
    %>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content prescription-details-report-page-for-browser" id="kt_content">
    <div class="kt-portlet" id="modalbody" style="background-color: #f2f2f2!important;">
        <div class="kt-portlet__body m-3" style="background-color: #f6f9fb!important;">
            <table class="w-100 mt-5 header-section">
                <tr>
                    <td align="center">
                        <img
                                class="parliament-logo"
                                width="8%"
                                src="<%=context%>assets/images/perliament_logo_final2.png"
                                alt=""
                        />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <p class="mb-0"><%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <a class="website-link" href="www.parliament.gov.bd"
                        >www.parliament.gov.bd</a
                        >
                    </td>
                </tr>
            </table>
            <div class="mb-5">
                <table class="w-100 mt-5">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="pt-1">
                                        <img class="prp-logo" width="65%" src="<%=context%>assets/images/prp_logo.png"
                                             alt="parliament logo"/>
                                    </td>
                                    <td>
                                        <table style="margin-left: -50px">
                                            <tr>
                                                <td>
                                                    <p class="mb-0">

                                                        <%=WorkflowController.getNameFromEmployeeRecordID(appointmentDTO.drEmployeeRecordId, Language)%>
                                                    </p>
                                                    <p class="mb-0"><%=WorkflowController.getOrganogramName(appointmentDTO.doctorId, Language)%>
                                                    </p>
                                                    <p class="mb-0"><%=Utils.capitalizeFirstLetter(LM.getText(LC.APPOINTMENT_ADD_APPOINTMENT_ADD_FORMNAME, loginDTO))%>
                                                        : prp.parliament.gov.bd</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <table>
                                <tr>
                                    <td>
                                        <p class="mb-0"><%=Utils.capitalizeFirstLettersOfWords(LM.getText(LC.HM_PARLIAMENT_MEDICAL_CENTRE, loginDTO))%>
                                        </p>
                                        <p class="mb-0"><%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                                        </p>
                                        <p class="mb-0"><%=LM.getText(LC.HM_PARLIAMENT_PHONE, loginDTO)%>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="border-top border-dark mt-3"></div>
                <%
                    /*if (addMedicine == null && addLabTest == null) */
                    {
                %>
                <%@include file="./patientHeightWeight.jsp" %>
                <%
                    }
                %>
                <%
                    if (addMedicine == null && addLabTest == null) {
                %>
                <%
                    if (!prescription_detailsDTO.chiefComplaints.equalsIgnoreCase("")) {
                %>
                <table class="w-100 mt-4">
                    <tr>
                        <td>
                            <p class="mb-0">
                                <strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_CHIEFCOMPLAINTS, loginDTO)%>
                                    :</strong> <%=prescription_detailsDTO.chiefComplaints%>
                            </p>
                        </td>
                    </tr>
                </table>
                <%
                    }
                %>
                <%
                    if (!prescription_detailsDTO.clinicalFeatures.equalsIgnoreCase("")) {
                %>
                <table class="w-100 mt-4">
                    <tr>
                        <td>
                            <p class="mb-0">
                                <strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_CLINICALFEATURES, loginDTO)%>
                                    :</strong> <%=prescription_detailsDTO.clinicalFeatures%>
                            </p>
                        </td>
                    </tr>
                </table>
                <%
                    }
                %>
                <%
                    if (!prescription_detailsDTO.diagnosis.equalsIgnoreCase("")) {
                %>
                <table class="w-100 mt-4">
                    <tr>
                        <td>
                            <p class="mb-0"><strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_DIAGNOSIS, loginDTO)%>
                                :</strong> <%=prescription_detailsDTO.diagnosis%>
                            </p>
                        </td>
                    </tr>
                </table>
                <%
                    }
                %>
                <%
                    if (!prescription_detailsDTO.advice.equalsIgnoreCase("")) {
                %>
                <table class="w-100 mt-4">
                    <tr>
                        <td>
                            <p class="mb-0"><strong><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_ADVICE, loginDTO)%>
                                :</strong> <%=prescription_detailsDTO.advice%>
                            </p>
                        </td>
                    </tr>
                </table>
                <%
                    }
                %>
                <%
                    }
                %>

                <%
                    patientPrescriptionMedicine2DTOs = patientPrescriptionMedicine2DAO.getPatientPrescriptionMedicine2DTOListByPrescriptionDetailsID(prescription_detailsDTO.iD);

                    if (addLabTest == null && !patientPrescriptionMedicine2DTOs.isEmpty()) {

                %>
                <table class="w-100 mt-4">
                    <tr>
                        <td>
                            <h4 class="mb-0">R<sub>x</sub></h4>
                        </td>
                    </tr>
                </table>
                <%
                    if (addMedicine != null) {
                %>
                <form class="form-horizontal"
                      action="Prescription_detailsServlet?actionType=addMedicine&ID=<%=prescription_detailsDTO.iD%>"
                      id="bigform" name="bigform" method="POST">
                    <%
                        }
                    %>
                    <div class="mt-1 attachement-div">
                        <%@include file="./medicineView.jsp" %>
                    </div>
                    <%
                        if (addMedicine != null && justView == null) {
                            if (!prescription_detailsDTO.medicineAdded) {
                    %>
                    <div class="form-actions text-center">
                        <button class="btn btn-success" type="submit"
                                onClick="this.form.submit(); this.disabled=true; this.value='Sending…';"><%=LM.getText(LC.HM_ADD_MEDICINE, loginDTO)%>
                        </button>
                    </div>
                </form>
                <%
                            }
                        }

                    }
                %>
                <%
                    sl = 0;
                    prescriptionLabTestDTOs = prescriptionLabTestDAO.getPrescriptionLabTestDTOListByPrescriptionDetailsID(prescription_detailsDTO.iD);
                    if (addMedicine == null && !prescriptionLabTestDTOs.isEmpty()) {

                %>
                <%
                    if (addLabTest != null) {
                %>


                <form class="form-horizontal"
                      action="Prescription_detailsServlet?actionType=updateLabTest&ID=<%=prescription_detailsDTO.iD%>"
                      id="bigform" name="bigform" method="POST" onsubmit="return PreprocessBeforeSubmiting()">

                    <%
                        }
                    %>
                    <div class="attachement-div mt-5c table-responsive">
                        <br>
                        <h4 class="mb-0"><%=Language.equalsIgnoreCase("english") ? "Lab Tests" : "পরীক্ষাগার পরীক্ষাসমূহ"%>
                        </h4>
                        <%@include file="./labTestView.jsp" %>
                    </div>
                    <%
                        if (addLabTest != null && justView == null) {
                            if (userDTO.roleID != SessionConstants.RADIOLOGIST) {
                    %>
                    <div class="form-actions text-center">
                        <button class="btn btn-success" id="labSubmitButton"
                                
                                type="submit" <%=userDTO.roleID == SessionConstants.XRAY_GUY ? "style='display:none'" : ""%>>
                            Submit
                        </button>
                    </div>
                </form>
                <%
                            }
                        }
                    }
                %>

                <%
                    if (prescription_detailsDTO.isDental) {

                %>
                <br>
                <h4 class="mb-0"><%=Language.equalsIgnoreCase("english") ? "Dental Actions" : "দন্ত বিষয়ক কার্যকলাপ"%>
                </h4>

                <table class="table w-100 table-bordered border-bottom border-dark">
                    <thead class="text-nowrap">
                    <tr class="border border-dark border-left-0 border-right-0">
                        <th class="py-2"><%=LM.getText(LC.HM_SL, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.HM_DOCTOR, loginDTO)%>/<%=LM.getText(LC.HM_DENTAL_TECHNOLIST, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.HM_ACTION, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.HM_DETAILS, loginDTO)%>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        prescription_detailsDTO.dental_activitiesDTOList = Dental_activitiesDAO.getInstance().getByPrescriptionId(prescription_detailsDTO.iD);
                        if (prescription_detailsDTO.dental_activitiesDTOList != null) {
                            for (Dental_activitiesDTO dental_activitiesDTO : prescription_detailsDTO.dental_activitiesDTOList) {
                    %>
                    <tr>
                        <td valign="top" class="py-2"><%=Utils.getDigits(++sl, Language)%>
                        </td>
                        <td valign="top"><%=WorkflowController.getNameFromEmployeeRecordID(dental_activitiesDTO.dtErId, isLangEng) %>
                        </td>
                        <td valign="top"><%=CatDAO.getName("english", "dental_action", dental_activitiesDTO.dentalActionCat)%>
                        </td>
                        <td valign="top"><%=dental_activitiesDTO.doctorRemarks%>
                        </td>
                    </tr>
                    <%
                            }

                        }
                    %>
                    </tbody>
                </table>
                <%
                    if (prescription_detailsDTO.dentalInstructionNeeded) {
                %>
                <br><br>
                <div style="text-align: right;">
                    <img width="25%" src="<%=context%>assets/images/dat.png"
                         alt=""/>
                </div>
                <%
                    }
                %>

                <%
                    }
                %>
                <table class="w-100 mt-5">

                    <%
                        if (addMedicine == null && addLabTest == null) {

                            if (!prescription_detailsDTO.remarks.equalsIgnoreCase("")) {
                    %>
                    <tr>
                        <td><p><strong><%=LM.getText(LC.HM_REMARKS, loginDTO)%>
                            :</strong> <%=prescription_detailsDTO.remarks%>
                        </p>
                        </td>
                    </tr>
                    <%
                        }
                        if (prescription_detailsDTO.referredTo != -1) {
                    %>
                    <tr>
                        <td><p><strong><%=LM.getText(LC.HM_REFERRED_TO, loginDTO)%>
                            :</strong> <%=WorkflowController.getNameFromOrganogramId(prescription_detailsDTO.referredTo, Language)%>
                        </p>
                        </td>
                    </tr>
                    <%
                        }

                        if (prescription_detailsDTO.referredTo != -1) {
                    %>
                    <tr>
                        <td><p><strong><%=LM.getText(LC.HM_REFERRAL_MESSAGE, loginDTO)%>
                            :</strong> <%=prescription_detailsDTO.referralMessage%>
                        </p>
                        </td>
                    </tr>
                    <%
                            }
                        }
                    %>
                </table>
                <div class="d-flex justify-content-end mt-5">
			      <div class="signature-container">
			        		<%
                                byte[] encodeBase64Photo = WorkflowController.getBase64SignatureFromUserName(prescription_detailsDTO.drUserName);
                            %>
                            <img src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>'
                                 style="width:150px"
                                 id="signature-img">
			        <p class="mb-0"><b><%=WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, Language)%></b></p>
			        <p class="mb-0">
			          <%=appointmentDTO.qualifications%>
			        </p>
			      </div>
			    </div>
    
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->


<jsp:include page="drugModal.jsp"></jsp:include>
<script type="text/javascript">

    function checkbox_toggeled(cb, hiddenId) {
        console.log("hidenid = " + hiddenId);
        if (cb.checked) {
            $("#" + hiddenId).val("1");
        } else {
            $("#" + hiddenId).val("0");
        }
    }

    function PreprocessBeforeSubmiting() 
    {

        <%
        if(addLabTest != null && justView == null)
        {
            for (PrescriptionLabTestDTO prescriptionLabTestDTO : prescriptionLabTestDTOs)
            {
                %>
		        if ($("#testingDate_date_<%=prescriptionLabTestDTO.iD%>").length) {
		            var testingDate = getDateStringById('testingDate_js_<%=prescriptionLabTestDTO.iD%>', 'DD/MM/YYYY');
		            $("#testingDate_date_<%=prescriptionLabTestDTO.iD%>").val(testingDate);
		        }
		        if ($("#schDate_date_<%=prescriptionLabTestDTO.iD%>").length) {
		            var schDate = getDateStringById('schDate_js_<%=prescriptionLabTestDTO.iD%>', 'DD/MM/YYYY');
		            $("#schDate_date_<%=prescriptionLabTestDTO.iD%>").val(schDate);
		        }
		        if($("#isDone_<%=prescriptionLabTestDTO.iD%>").prop("checked"))
		        {		        
		        	var validType = true;
		        	$( "[name= 'xRayPlateType_<%=prescriptionLabTestDTO.iD%>']" ).each(function( index )
		        	{
			        	if($(this).val() == '-1')
				        {
				        	toastr.error("Plate type must be chosen");
				        	validType= false;
				        	return false;
				        }
		        	});
		        	if(!validType)
	        		{
	        			return false;
	        		}
		        	
		        	var validQuantity = true;
		        	$( "[name= 'plateQuantity_<%=prescriptionLabTestDTO.iD%>']" ).each(function( index )
		        	{
			        	if($(this).val() == '0')
				        {
				        	toastr.error("Plate quantity must be greater then 0");
				        	validQuantity= false;
				        	return false;
				        }
		        	});
		        	if(!validQuantity)
	        		{
	        			return false;
	        		}
		        }
		        
		        
        		<%
    		}

		}
		%>
		$("#labSubmitButton").prop("disabled",true);
		return true;
    }

    $(document).ready(function () {

        <%
        if(addLabTest != null && justView == null)
        {
        	for (PrescriptionLabTestDTO prescriptionLabTestDTO : prescriptionLabTestDTOs) 
        	{
        		%>
        if ($("#testingDate_date_<%=prescriptionLabTestDTO.iD%>").length) {
            setDateByTimestampAndId('testingDate_js_<%=prescriptionLabTestDTO.iD%>', '<%=prescriptionLabTestDTO.testingDate%>');
        }
        if ($("#schDate_date_<%=prescriptionLabTestDTO.iD%>").length) {
            setDateByTimestampAndId('schDate_js_<%=prescriptionLabTestDTO.iD%>', '<%=prescriptionLabTestDTO.scheduledDeliveryDate%>');
        }


        <%
    }

}
%>

        <%
        if(errorMsg!= null && !errorMsg.equalsIgnoreCase(""))
        {
        	%>
        toastr.error("<%=errorMsg%>");
        <%
    }
    %>

    });
    var drugModalRowId = -1;

    function setDrugLabel(value, stock) {

        $("#drug_modal_textdiv_" + drugModalRowId).html(stock + " pieces in stock");
        $('#drugInformationType_select_' + drugModalRowId).val(value);
        $('#doseSold_' + drugModalRowId).attr('max', stock);
        console.log("max now is" + $('#doseSold_' + drugModalRowId).attr('max'));

    }

    function setHiddenEdate(id) {
        drugModalRowId = id.split("_")[2];
        var select = $('#' + id);
        var stockInId = $('option:selected', select).val();
        var edate = $('option:selected', select).attr('edate');
        var stock = $('option:selected', select).attr('stock');

        $('#doseSold_' + drugModalRowId).attr('max', stock);
        $('#hidden_edate_' + drugModalRowId).val(edate);
        $('#hidden_inventoryIn_' + drugModalRowId).val(stockInId);
    }

    function drugSelected(id) {
        drugModalRowId = id.split("_")[2];
        console.log("drugModalRowId " + drugModalRowId);
        var select = $('#' + id);
        setDrugLabel($('option:selected', select).val(),
            $('option:selected', select).attr('stock'));
        getEdate($('option:selected', select).val(), drugModalRowId);
    }

    function getEdate(drugInfoId, rowId) {

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("edate_select_" + rowId).innerHTML = this.responseText;

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("POST", "Medical_inventory_lotServlet?actionType=getEdate&drugInfoId=" + drugInfoId, true);
        xhttp.send();

    }


    function init(row) {
        $("[name='drug_select']").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });
    }

    var row = 0;

    window.onload = function () {
        init(row);
        //CKEDITOR.replaceAll();
    }


    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

    function clickIsDone() {
        $("#labSubmitButton").removeAttr("style");
    }

    function plusClicked(button) {
        var div = $(button).closest('.p11');
        var td = $(button).closest('td');
        div.clone().appendTo(td);
    }

    function minusClicked(button) {
        var td = $(button).closest('td');
        var count = td.children().length;
        if (count <= 1) {
            toastr.error("At least one plate type is required");
        } else {
            var div = $(button).closest('.p11');
            div.remove();
        }

    }
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"
></script>
<!-- Jquery print cdn -->
<script
        src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.2/jQuery.print.min.js"
        integrity="sha512-t3XNbzH2GEXeT9juLjifw/5ejswnjWWMMDxsdCg4+MmvrM+MwqGhxlWeFJ53xN/SBHPDnW0gXYvBx/afZZfGMQ=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
></script>
<script>
    $("#printer").click(function () {
        $.print("#modalbody");
    });
</script>







































