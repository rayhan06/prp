<jsp:include page="../common/layout.jsp" flush="true">
	<jsp:param name="title" value="Prescription" />
	<jsp:param name="body" value="../prescription_details/prescription_detailsEditBody.jsp"></jsp:param>
	<jsp:param name="css" value="assets/css/metronic/pages/general/wizard/wizard-3.css"></jsp:param>
	<jsp:param name="js" value="assets/js/metronic/pages/wizard/wizard-3.js"></jsp:param>
</jsp:include> 