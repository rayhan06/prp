<%@page import="restriction_config.RestrictionConfigDetailsDTO"%>
<%@page import="restriction_config.Restriction_configDTO"%>
<%@page import="restriction_config.Restriction_configRepository"%>
<%@page import="employee_records.Employee_recordsRepository"%>
<%@page import="workflow.WorkflowController" %>
<%@page pageEncoding="UTF-8" %>

<%@page import="prescription_details.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@page import="config.*" %>
<%@page import="appointment.*" %>
<%@page import="nurse_action.*" %>
<%@page import="config.*" %>
<%@page import="pharmacy_shift.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PRESCRIPTION_DETAILS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PRESCRIPTION_DETAILS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Prescription_detailsDTO prescription_detailsDTO = (Prescription_detailsDTO) request.getAttribute("prescription_detailsDTO");
    CommonDTO commonDTO = prescription_detailsDTO;
    String servletName = "Prescription_detailsServlet";


    System.out.println("prescription_detailsDTO = " + prescription_detailsDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Prescription_detailsDAO prescription_detailsDAO = (Prescription_detailsDAO) request.getAttribute("prescription_detailsDAO");
	AppointmentDAO appointmentDAO = new AppointmentDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat complexDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");

    String addMedicine = request.getParameter("addMedicine");
    String addLabTest = request.getParameter("addLabTest");


    AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);

    PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
    int doneTestCount = prescriptionLabTestDAO.getDoneCount(prescription_detailsDTO.iD);
    int notDoneTestCount = prescriptionLabTestDAO.getNotDoneCount(prescription_detailsDTO.iD, userDTO.roleID);
    System.out.println("doneTestCount = " + doneTestCount);
   
    boolean isExpired = prescription_detailsDAO.isExpired(prescription_detailsDTO);
    
    boolean isOpen = PharmacyShiftDetailsRepository.getInstance().isPharmacyOpenNow();
    boolean isLangEng = Language.equalsIgnoreCase("english");

%>


<td class="" id='<%=i%>_appointmentId'>
    <%
        value = complexDateFormat.format(new Date(prescription_detailsDTO.visitTime));
    %>
    <b><%=Utils.getDigits(value, Language)%></b><br>
    
    <%=Utils.getDigits(prescription_detailsDTO.sl, Language)%>
    
    <%
	long lastVisitDate = prescription_detailsDAO.getLastVisitDate(appointmentDTO.visitDate, appointmentDTO.erId);
	if(lastVisitDate != -1)
	{
		%>
		 <br><%=Language.equalsIgnoreCase("english")?"Last Visit Date":"শেষ ভিজিটের তারিখ"%>
		:<%=Utils.getDigits(simpleDateFormat.format(new Date(lastVisitDate)), Language)%><br> 
		<%
	}
	%>
</td>

<td >

	<%
        value = appointmentDTO.patientName + "";
    %>
	

    <b><%=value%></b><br> 
    <%
    value = "Relation: ";
	if(!isLangEng)
	{
		value =  "সম্পর্ক: ";
	}
    %>
    <b><%=value%></b>
    
    <%=appointmentDAO.getRelation(appointmentDTO, isLangEng)%><br>
    
	
    <%=Utils.getDigits(appointmentDTO.phoneNumber, Language)%>
    <%
    if(appointmentDTO.alternatePhone != null && !appointmentDTO.alternatePhone.equalsIgnoreCase(""))
    {
    	%>
    	<br><%=Utils.getDigits(appointmentDTO.alternatePhone, Language)%>
    	<%
    }
    %>
    
	<%
	 if(appointmentDTO.bearerName != null && !appointmentDTO.bearerName.equalsIgnoreCase(""))
	 {
		 %>
		 <br><%=Language.equalsIgnoreCase("english")?"Bearer's Name":"বাহকের নাম"%>
		 : <%=appointmentDTO.bearerName%>
		 <br><%=Language.equalsIgnoreCase("english")?"Bearer's Phone Number":"বাহকের ফোন নাম্বার"%>
		 : <%=Utils.getDigits(appointmentDTO.bearerPhone, Language)%>
		 
		 <%
	 }
	 %>
                            


</td>

<td >

	

    <%=LM.getText(LC.HM_ID, loginDTO)%>: <%=WorkflowController.getUserNameFromErId(appointmentDTO.erId, isLangEng)%><br>
    
    <%=WorkflowController.getNameFromUserName(appointmentDTO.employeeUserName, Language) %><br>
    <%=WorkflowController.getOrganogramName(appointmentDTO.employeeUserName, Language) %>
    <%=appointmentDTO.othersDesignationAndId%>
    <br>
    <%=WorkflowController.getOfficeNameFromOrganogramId(appointmentDTO.patientId, Language) %>
    <%=CommonDAO.getName(Language, "other_office", appointmentDTO.othersOfficeId)%>
    <br>
    
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='AppointmentServlet?actionType=view&ID=<%=appointmentDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td class="" id='<%=i%>_name'>
    

     <b> <%=WorkflowController.getNameFromEmployeeRecordID(appointmentDTO.drEmployeeRecordId, Language)%></b>
 <%
 if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
 {
 %>     
    <br><%=LM.getText(LC.HM_ID, loginDTO)%>:<%=Utils.getDigits(appointmentDTO.drUserName, Language)%>
<%
 }
%>    



</td>



<%
    if (addMedicine == null && addLabTest == null)
    {
%>



<td class="" >
    <%
			if(prescription_detailsDTO.referredTo != -1)
			{
				value = WorkflowController.getNameFromOrganogramId(prescription_detailsDTO.referredTo, Language);
				%>
				<b><%=value%></b><br>
				<%
			}
			
			%>
						
			
			
			<%
			if(prescription_detailsDTO.referredTo != -1 && prescription_detailsDTO.referredAppointmentId == -1)
			{
				
				%>
				<button type="button" type="button" class="btn btn-sm border-0 shadow btn-border-radius "
			            style="background-color: #334477; color: white; "
			            onclick="location.href='AppointmentServlet?actionType=getFormattedAddPagePreselected&referredTo=<%=prescription_detailsDTO.referredTo%>&&userName=<%=prescription_detailsDTO.employeeUserName%>&&whoIsThePatientCat=<%=prescription_detailsDTO.whoIsThePatientCat%>&&fromPrescription=<%=prescription_detailsDTO.iD%>'">
			        <i class="fa fa-plus"></i>&nbsp;<%=LM.getText(LC.HM_CREATE_APPOINTMENT, loginDTO)%>
			
			    </button>
				 
				<%
				
			}
			else if(prescription_detailsDTO.referredTo != -1 && prescription_detailsDTO.referredAppointmentId != -1)
			{
				AppointmentDTO createdAppointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.referredAppointmentId);
				if(createdAppointmentDTO != null)
				{
					SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm aa");
					value = LM.getText(LC.HM_APPOINTMENT_ON, loginDTO) +
							" " + simpleDateFormat.format(new Date(createdAppointmentDTO.visitDate));
					if(appointmentDAO.isDoctor(createdAppointmentDTO.doctorId))
					{
						value += ", " + hourFormat.format(new Date(createdAppointmentDTO.availableTimeSlot)) ;
					}
					
					%>
					<%=Utils.getDigits(value, Language)%>
					<%
				}
			}
			
			%>



</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Prescription_detailsServlet?justView=1&actionType=view&ID=<%=prescription_detailsDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>
<td>
    <%
        if (doneTestCount > 0) 
        {
    %>
    <button type="button" type="button" class="btn btn-sm btn-success border-0 shadow btn-border-radius pl-4"
            onclick="location.href='Prescription_detailsServlet?justView=1&actionType=view&addLabTest=1&ID=<%=prescription_detailsDTO.iD%>'">
        <i class="fa fa-file"></i>
    </button>
    <%
        }
    %>
</td>

<td>
	<%
	if(userDTO.roleID == SessionConstants.NURSE_ROLE)
	{
	%>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Nurse_actionServlet?actionType=getAddPage&appointmentId=<%=prescription_detailsDTO.appointmentId%>'">
        <i class="fa fa-plus"></i>
    </button>
    <%
	}
    %>
    <%
    if(Nurse_actionDAO.getInstance().getIDByAppointmentId(prescription_detailsDTO.appointmentId) != -1)
    {
    %>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Nurse_actionServlet?actionType=viewByAppointmentId&appointmentId=<%=prescription_detailsDTO.appointmentId%>'">
        <i class="fa fa-eye"></i>
    </button>
    <%
    }
    %>
</td>

<td class="text-nowrap">
<%
if(prescription_detailsDTO.modifiedByErId >= 0)
{
	%>

	<%=isLangEng?"Last Editor":"সর্বশেষ সম্পাদক"%>
	:
	<br>
	<b><%=WorkflowController.getNameFromEmployeeRecordID(prescription_detailsDTO.modifiedByErId) %></b>
	
	<br>
	<%=simpleDateFormat.format(new Date(prescription_detailsDTO.lastModificationTime))%>
	<br>
<%
}
%>
<%
if((
		prescription_detailsDTO.doctorId == userDTO.organogramID 
		|| userDTO.roleID == SessionConstants.ADMIN_ROLE 
		|| userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
)
&& prescription_detailsDTO.canBeEdited)
{
%>
 <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=prescription_detailsDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
    <%
}
    %>
</td>


<%
} //drs view ends here
else if (addMedicine != null) //pharmacist sees them
{
%>
<td>
    <button type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Prescription_detailsServlet?justView=1&actionType=view&addMedicine=1&ID=<%=prescription_detailsDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
    <%
    boolean hasRestriction = Restriction_configRepository.getInstance().restrictionInList(appointmentDTO.erId, RestrictionConfigDetailsDTO.MEDICAL);
    if (!prescription_detailsDTO.medicineAdded && !isExpired && isOpen && !hasRestriction) {
    %>

    <button type="button" type="button" class="btn btn-sm cancel-btn text-white border-0 shadow btn-border-radius pl-4 ml-2"
            onclick="location.href='Prescription_detailsServlet?actionType=view&addMedicine=1&ID=<%=prescription_detailsDTO.iD%>'">
        <i class="fa fa-plus"></i>
    </button>


    <%
    } 
    %>
    
    <%
    if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
    {
    %>
    
    <button type="button" type="button" class="btn btn-sm cancel-btn text-white border-0 shadow btn-border-radius pl-4 ml-2"
            onclick="location.href='Deliver_or_return_medicineServlet?actionType=getAddPage&userName=<%=WorkflowController.getUserNameFromErId(appointmentDTO.erId, true)%>&medicalItemType=<%=SessionConstants.MEDICAL_ITEM_DRUG%>'">
        <i class="fa fa-minus"></i>
    </button>
    <%
    }
    %>
    <%
    if(isExpired)
    {
    	%>
    	<br><b><%=Language.equalsIgnoreCase("english")?"Validity Expired":"বৈধতার মেয়াদউত্তীর্ণ" %>
    	</b>
    	<%
    }
    	
    %>
    
       <%
    if(hasRestriction)
    {
    	%>
    	<br><b><%=Language.equalsIgnoreCase("english")?"User is restricted.":"ইউজারকে রেস্টড়িক্ট করা হয়েছে" %>
    	</b>
    	<%
    }
    	
    %>
    
    <%
    if(!isOpen)
    {
    	%>
    	<br><b><%=Language.equalsIgnoreCase("english")?"Pharmacy Closed":"ফার্মেসি বন্ধ" %>
    	</b>
    	<%
    }
    	
    %>
</td>

<%
} else if (addLabTest != null)// lab emploees see them
{
%>
<%
if(userDTO.roleID == SessionConstants.XRAY_GUY || userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE || userDTO.roleID == SessionConstants.RADIOLOGIST || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD)
{
%>
<td>
<%
        if (notDoneTestCount > 0 ) 
        {
    %>
    <button type="button" type="button" class="btn btn-sm border-0 shadow btn-border-radius "
            style="background-color: #334477; color: white; "
            onclick="location.href='Prescription_detailsServlet?actionType=view&addLabTest=1&ID=<%=prescription_detailsDTO.iD%>'">
        <i class="fa fa-plus"></i>&nbsp;<%=LM.getText(LC.HM_ADD_LAB_REPORT, loginDTO)%>

    </button>
<%
        }
%>

</td>
<%
}
%>

<td>

    <button type="button" type="button" class="btn btn-sm border-0 shadow btn-border-radius "
            style="background-color: #774433; color: white; "
            onclick="location.href='Prescription_detailsServlet?justView=1&actionType=view&addLabTest=1&ID=<%=prescription_detailsDTO.iD%>'">
        <i class="fa fa-eye"></i>&nbsp;<%=LM.getText(LC.HM_VIEW_LAB_REPORTS, loginDTO)%>

    </button>


</td>
<%
    }
%>
<td>
<%=prescription_detailsDAO.getMedicineStatus(prescription_detailsDTO, Language)%>
</td>		
	

											