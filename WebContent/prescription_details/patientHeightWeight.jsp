        <table class="w-100 mt-3">
                    <tr>
                        <td>
                            <p class="mb-1"><%=LM.getText(LC.HM_ID, loginDTO)%>
                                : <%if (appointmentDTO.sl != null) {%><%=Utils.getDigits(appointmentDTO.sl, Language)%>
                                <br><%}%></p>
                            <p class="mb-1"><%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
                                : <%=prescription_detailsDTO.name%>
                                / <%=TimeFormat.getAge(appointmentDTO.dateOfBirth, Language)%>
                            </p>
                            <p class="mb-1"><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
                                : <%=Utils.getDigits(prescription_detailsDTO.employeeUserName, Language)%>
                            </p>
                            <small class="mb-0">
                                <%
                                    if (prescription_detailsDTO.weight > 0) {
                                %>
                                <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_WEIGHT, loginDTO)%>
                                : <%=prescription_detailsDTO.weight > 0 ? Utils.getDigits(prescription_detailsDTO.weight, Language) : ""%>
                                <%
                                    }
                                %>
                                <%
                                    if (prescription_detailsDTO.height > 0) {
                                %>
                                <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_HEIGHT, loginDTO)%>
                                : <%=prescription_detailsDTO.height > 0 ? Utils.getDigits(prescription_detailsDTO.height, Language) : ""%>
                                <%
                                    }
                                %>
                                <%
                                    if (prescription_detailsDTO.bloodPressureSystole > 0 && prescription_detailsDTO.bloodPressureSystole > 0) {
                                %>
                                <%=LM.getText(LC.HM_BLOOD_PRESSURE, loginDTO)%>:
                                <%=Utils.getDigits(prescription_detailsDTO.bloodPressureSystole, Language) + " /"%>
                                <%=Utils.getDigits(prescription_detailsDTO.bloodPressureDiastole, Language)%>
                                <%
                                    }
                                %>
                                <%
                                    if (prescription_detailsDTO.pulse > 0) {
                                %>
                                <br>
                                <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PULSE, loginDTO)%>:
                                <%=Utils.getDigits(prescription_detailsDTO.pulse, Language)%>
                                <%
                                    }
                                %>
                                 <%
                                    if (prescription_detailsDTO.sugar != null && !prescription_detailsDTO.sugar.equalsIgnoreCase("")) {
                                %>
                                <br>
                                <%=Language.equalsIgnoreCase("english")?"Blood Sugar":"রক্তের শর্করা"%>:
                                <%=Utils.getDigits(prescription_detailsDTO.sugar, Language)%>
                                <%
                                    }
                                %>
                                <%
                                    if (prescription_detailsDTO.oxygenSaturation > 0) {
                                %>
                                <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_OXYGENSATURATION, loginDTO)%>:
                                <%=Utils.getDigits(prescription_detailsDTO.oxygenSaturation, Language)%>
                                <%
                                    }
                                %>
                            </small>
                        </td>
                        <td valign="top" align="right">
                            <p class="mb-0"><%=LM.getText(LC.APPOINTMENT_ADD_VISITDATE, loginDTO)%>
                                : <%=Utils.getDigits(visitTime, Language)%>
                            </p>
                            
                            <p class="mb-0 text-right mt-3">
                                <img class="barcode-image"  width="250" src="<%=barCode%>"/>
                            </p>
                        </td>
                    </tr>
                </table>