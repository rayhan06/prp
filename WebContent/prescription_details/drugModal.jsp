<%@ page import="offices.OfficesDTO" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="pb.CatDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="OfficeUnitsEdms.OfficeUnitsEdmsDTO" %>

<%
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);


    String Language3 = LM.getText(LC.EDMS_DOCUMENTS_EDIT_LANGUAGE, loginDTO2 );
%>
<!-- Doc Reference Modal -->
<div id="drugModal" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Select Drug </h4>
            </div>

            <div class="modal-body" id="drugModalBody">
                

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>
</div>
