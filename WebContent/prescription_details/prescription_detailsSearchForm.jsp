<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="prescription_details.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.PRESCRIPTION_DETAILS_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Prescription_detailsDAO prescription_detailsDAO = (Prescription_detailsDAO) request.getAttribute("prescription_detailsDAO");


    String navigator2 = SessionConstants.NAV_PRESCRIPTION_DETAILS;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    String addMedicine = request.getParameter("addMedicine");
    String addLabTest = request.getParameter("addLabTest");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<%
if(addMedicine != null)
{
%>
<input type='hidden' id='addMedicine' value='1'/>
<%
}
%>
<%
if(addLabTest != null)
{
%>
<input type='hidden' id='addLabTest' value='1'/>
<%
}
%>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped ">
        <thead class="">
        <tr>
        	<th><%=LM.getText(LC.HM_SL, loginDTO)%>
            </th>
            <th ><%=LM.getText(LC.HM_DATE, loginDTO)%>
            </th>
             <th><%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
            </th>
            
            <th ><%=LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)%>
            </th>
            <th ><%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
            </th>



            <%
                if (addMedicine == null && addLabTest == null)
                {
            %>
            <th ><%=LM.getText(LC.HM_REFERRED_TO, loginDTO)%>
            </th>
            <th ><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th ><%=LM.getText(LC.HM_VIEW_LAB_REPORTS, loginDTO)%>
            </th>
            <th ><%=Utils.capitalizeFirstLetter(LM.getText(LC.NURSE_ACTION_ADD_NURSE_ACTION_ADD_FORMNAME, loginDTO).toLowerCase())%>
            </th>
            <th ><%=LM.getText(LC.PI_RETURN_SEARCH_PI_RETURN_EDIT_BUTTON, loginDTO)%></th>
            <%
            } else if (addMedicine != null) //pharmacist sees them
            {
            %>
            <th ><%=LM.getText(LC.HM_UPDATE, loginDTO)%>/<%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <%
            } else if (addLabTest != null)// lab emploees see them
            {
            %>
            <%
			if(userDTO.roleID == SessionConstants.XRAY_GUY || userDTO.roleID == SessionConstants.LAB_TECHNITIAN_ROLE || userDTO.roleID == SessionConstants.RADIOLOGIST || userDTO.roleID == SessionConstants.PATHOLOGY_HEAD)
			{
			%>
            <th><%=LM.getText(LC.HM_UPDATE, loginDTO)%>
            </th>
            <%
			}
            %>
            <th><%=LM.getText(LC.HM_VIEW_LAB_REPORTS, loginDTO)%>
            </th>
            <%
                }
            %>
			<th><%=Language.equalsIgnoreCase("english")?"Medicine Status":"মেডিসিনের অবস্থা"%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_PRESCRIPTION_DETAILS);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Prescription_detailsDTO prescription_detailsDTO = (Prescription_detailsDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>
			<td>
				<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>
			</td>

            <%
                request.setAttribute("prescription_detailsDTO", prescription_detailsDTO);
            %>

            <jsp:include page="./prescription_detailsSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			