
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="family.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>


<%
FilesDAO filesDAO = new FilesDAO();
FamilyDTO familyDTO;
familyDTO = (FamilyDTO)request.getAttribute("familyDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(familyDTO == null)
{
	familyDTO = new FamilyDTO();
	
}
System.out.println("familyDTO = " + familyDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.FAMILY_EDIT_FAMILY_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.FAMILY_ADD_FAMILY_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="FamilyServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				


<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.FAMILY_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=familyDTO.iD%>' tag='pb_html'/>
	
		<label class="col-lg-3 control-label">
			<%=LM.getText(LC.HM_SELECT_EMPLOYEE, loginDTO)%>
		</label>
		<div class="form-group ">					
			<div class="col-lg-6 " id = 'doctorId_div_<%=i%>'>	
				<select class='form-control' required  name='organogramId' id = 'organogramId_hidden_<%=i%>'   tag='pb_html'>
		<%
		if(actionName.equals("edit"))
		{
					Options = CommonDAO.getOrganogramsByOrganogramID(familyDTO.organogramId);
		}
		else
		{			
					Options = CommonDAO.getOrganogramsByOrganogramID();			
		}
		%>
		<%=Options%>
				</select>
				
			</div>
		</div>			
														
		

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + familyDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + familyDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	





				<div class="col-md-12" style="padding-top: 20px;">
					<legend class="text-left content_legend"><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER, loginDTO)%></legend>
				</div>

				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_NAMEEN, loginDTO)%></th>
										<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_NAMEBN, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_RELATIONSHIP, loginDTO)%></th>
										<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_DATEOFBIRTH, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_BLOOD_GROUP, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_GENDER, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_RELIGION, loginDTO)%></th>
										<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_ADRESS, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_PHOTO, loginDTO)%></th>
										<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_NID, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_PHONE, loginDTO)%></th>
										<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_BIRTHREGISTRATIONNUMBER, loginDTO)%></th>
										
										<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
								<tbody id="field-FamilyMember">
						
						
<%
	if(actionName.equals("edit")){
		int index = -1;
		
		
		for(FamilyMemberDTO familyMemberDTO: familyDTO.familyMemberDTOList)
		{
			index++;
			
			System.out.println("index index = "+index);

%>	
							
									<tr id = "FamilyMember_<%=index + 1%>">
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='familyMember.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=familyMemberDTO.iD%>' tag='pb_html'/>
	
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='familyMember.familyId' id = 'familyId_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.familyId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td>										











		<input type='text' class='form-control'  name='familyMember.nameEn' id = 'nameEn_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='familyMember.nameBn' id = 'nameBn_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<select class='form-control'  name='familyMember.relationshipCat' id = 'relationshipCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "relationship", familyMemberDTO.relationshipCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "relationship", -1);			
}
%>
<%=Options%>
		</select>
		
										</td>
										<td>										











		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" name="familyMember.dateOfBirth" id = 'dateOfBirth_date_<%=childTableStartingID%>' value=<%
if(actionName.equals("edit"))
{
	String formatted_dateOfBirth = dateFormat.format(new Date(familyMemberDTO.dateOfBirth));
	%>
	'<%=formatted_dateOfBirth%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		
										</td>
										<td>										











		<select class='form-control'  name='familyMember.bloodGroupCat' id = 'bloodGroupCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "blood_group", familyMemberDTO.bloodGroupCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "blood_group", -1);			
}
%>
<%=Options%>
		</select>
		
										</td>
										<td>										











		<select class='form-control'  name='familyMember.genderCat' id = 'genderCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "gender", familyMemberDTO.genderCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "gender", -1);			
}
%>
<%=Options%>
		</select>
		
										</td>
										<td>										











		<select class='form-control'  name='familyMember.religionCat' id = 'religionCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "religion", familyMemberDTO.religionCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "religion", -1);			
}
%>
<%=Options%>
		</select>
		
										</td>
										<td>										











		<input type='text' class='form-control'  name='familyMember.adress' id = 'adress_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.adress + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<%
			if(familyMemberDTO.drawingFileId != -1)
			{
				FilesDTO filesDTO = filesDAO.getMiniDTOById(familyMemberDTO.drawingFileId);
				
				if(filesDTO.thumbnailBlob != null)
				{
				byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
				%>
				<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' >
				<%
				}
				%>
				<a onclick="window.open(this.href,'_blank');return false;"
			   href='Edms_documentsServlet?actionType=previewDropzoneFile&id=<%=filesDTO.iD%>'
			   download><%=filesDTO.fileTitle%>
				</a>
				<a href='Edms_documentsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download>
					Download
				</a>
				<%
			}
			%>				
		<input type='file' class='form-control' style="width:100px"  name='familyMember.drawingBlob' id = 'drawingBlob_blob_jpg_<%=childTableStartingID%>'   tag='pb_html'/>
			
						
										</td>
										<td>										

		<input type='text' class='form-control'  name='familyMember.nid' id = 'nid_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.nid + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										

		<input type='text' class='form-control'  name='familyMember.mobile' id = 'mobile_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.mobile + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='familyMember.birthRegistrationNumber' id = 'birthRegistrationNumber_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.birthRegistrationNumber + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='familyMember.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= <%=actionName.equals("edit")?("'" + familyMemberDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='familyMember.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
										</td>
										<td><div class='checker'><span id='chkEdit' ><input type='checkbox' id='familyMember_cb_<%=index%>' name='checkbox' value=''/></span></div></td>
									</tr>								
<%	
			childTableStartingID ++;
		}
	}
%>						
						
								</tbody>
							</table>
						
						
						
					</div>
					<div class="form-group">
						<div class="col-xs-9 text-right">

							<button id="remove-FamilyMember" name="removeFamilyMember" type="button"
									class="btn btn-danger remove-me1"><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_REMOVE, loginDTO)%></button>

							<button id="add-more-FamilyMember" name="add-moreFamilyMember" type="button"
									class="btn btn-primary"><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_ADD_MORE, loginDTO)%></button>

						</div>
					</div>
					
					<%FamilyMemberDTO familyMemberDTO = new FamilyMemberDTO();%>
					
					<template id="template-FamilyMember" >						
								<tr>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='familyMember.iD' id = 'iD_hidden_' value='<%=familyMemberDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='familyMember.familyId' id = 'familyId_hidden_' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.familyId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td>











		<input type='text' class='form-control'  name='familyMember.nameEn' id = 'nameEn_text_' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='familyMember.nameBn' id = 'nameBn_text_' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<select class='form-control'  name='familyMember.relationshipCat' id = 'relationshipCat_category_'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "relationship", familyMemberDTO.relationshipCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "relationship", -1);			
}
%>
<%=Options%>
		</select>
		
									</td>
									<td>











		<input type='text' class='form-control formRequired datepicker' name="familyMember.dateOfBirth" readonly="readonly" data-label="Document Date" id = 'dateOfBirth_date_' value=<%
if(actionName.equals("edit"))
{
	String formatted_dateOfBirth = dateFormat.format(new Date(familyMemberDTO.dateOfBirth));
	%>
	'<%=formatted_dateOfBirth%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		
									</td>
									<td>











		<select class='form-control'  name='familyMember.bloodGroupCat' id = 'bloodGroupCat_category_'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "blood_group", familyMemberDTO.bloodGroupCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "blood_group", -1);			
}
%>
<%=Options%>
		</select>
		
									</td>
									<td>











		<select class='form-control'  name='familyMember.genderCat' id = 'genderCat_category_'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "gender", familyMemberDTO.genderCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "gender", -1);			
}
%>
<%=Options%>
		</select>
		
									</td>
									<td>











		<select class='form-control'  name='familyMember.religionCat' id = 'religionCat_category_'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "religion", familyMemberDTO.religionCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "religion", -1);			
}
%>
<%=Options%>
		</select>
		
									</td>
									<td>











		<input type='text' class='form-control'  name='familyMember.adress' id = 'adress_text_' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.adress + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>











		<%
			if(familyMemberDTO.drawingFileId != -1)
			{
				FilesDTO filesDTO = filesDAO.getMiniDTOById(familyMemberDTO.drawingFileId);
				
				if(filesDTO.thumbnailBlob != null)
				{
				byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
				%>
				<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' >
				<%
				}
				%>
				<a onclick="window.open(this.href,'_blank');return false;"
			   href='Edms_documentsServlet?actionType=previewDropzoneFile&id=<%=filesDTO.iD%>'
			   download><%=filesDTO.fileTitle%>
				</a>
				<a href='Edms_documentsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download>
					Download
				</a>
				<%
			}
			%>				
		<input type='file' class='form-control' style="width:100px"  name='familyMember.drawingBlob' id = 'drawingBlob_blob_jpg_'    tag='pb_html'/>
			
						
									</td>
									<td>

		<input type='text' class='form-control'  name='familyMember.nid' id = 'nid_text_' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.nid + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td>										

		<input type='text' class='form-control'  name='familyMember.mobile' id = 'mobile_text_' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.mobile + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
										</td>
									<td>











		<input type='text' class='form-control'  name='familyMember.birthRegistrationNumber' id = 'birthRegistrationNumber_text_' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.birthRegistrationNumber + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='familyMember.isDeleted' id = 'isDeleted_hidden_' value= <%=actionName.equals("edit")?("'" + familyMemberDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='familyMember.lastModificationTime' id = 'lastModificationTime_hidden_' value=<%=actionName.equals("edit")?("'" + familyMemberDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
									</td>
									<td><div><span id='chkEdit' ><input type='checkbox' name='checkbox' value=''/></span></div></td>
								</tr>								
						
					</template>
				</div>		

				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.FAMILY_EDIT_FAMILY_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.FAMILY_ADD_FAMILY_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.FAMILY_EDIT_FAMILY_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.FAMILY_ADD_FAMILY_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    $('.datepicker').datepicker({

        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        todayHighlight:	true,
        showButtonPanel: true
    });
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	for(i = 1; i < child_table_extra_id; i ++)
	{
		if(document.getElementById("dateOfBirth_date_" + i))
		{
			if(document.getElementById("dateOfBirth_date_" + i).getAttribute("processed") == null)
			{
				preprocessDateBeforeSubmitting('dateOfBirth', i);	
				document.getElementById("dateOfBirth_date_" + i).setAttribute("processed","1");
			}
		}
	}
	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "FamilyServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-FamilyMember").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-FamilyMember");

            $("#field-FamilyMember").append(t.html());
			SetCheckBoxValues("field-FamilyMember");
			
			var tr = $("#field-FamilyMember").find("tr:last-child");
			
			tr.attr("id","FamilyMember_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			
			child_table_extra_id ++;
			
			$('.datepicker').datepicker({

	              changeMonth: true,
	              changeYear: true,
	              dateFormat: 'dd/mm/yy',
	              todayHighlight:	true,
	              showButtonPanel: true
	           });

        });

    
      $("#remove-FamilyMember").click(function(e){    	
	    var tablename = 'field-FamilyMember';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






