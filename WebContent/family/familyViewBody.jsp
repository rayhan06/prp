

<%@page import="workflow.WorkflowController"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="family.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.FAMILY_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
FamilyDAO familyDAO = new FamilyDAO("family");
FamilyDTO familyDTO = (FamilyDTO)familyDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title">Family Details</h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5>Family</h5>
                    </div>
			



			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.FAMILY_ADD_ORGANOGRAMID, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="organogramId">
						
											<%
											value = WorkflowController.getNameFromOrganogramId(familyDTO.organogramId, Language)  + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			
			
			
		


			</div>	

                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5>Family Member</h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_NAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_RELATIONSHIP, loginDTO)%></th>
								<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_DATEOFBIRTH, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_BLOOD_GROUP, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_GENDER, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_RELIGION, loginDTO)%></th>
								<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_ADRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_PHOTO, loginDTO)%></th>
								<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_NID, loginDTO)%></th>
								<th><%=LM.getText(LC.FAMILY_ADD_FAMILY_MEMBER_BIRTHREGISTRATIONNUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_PHONE, loginDTO)%></th>
							</tr>
							<%
                        	FamilyMemberDAO familyMemberDAO = new FamilyMemberDAO();
                         	List<FamilyMemberDTO> familyMemberDTOs = familyMemberDAO.getFamilyMemberDTOListByFamilyID(familyDTO.iD);
                         	
                         	for(FamilyMemberDTO familyMemberDTO: familyMemberDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = familyMemberDTO.nameEn + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = familyMemberDTO.nameBn + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = familyMemberDTO.relationshipCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "relationship", familyMemberDTO.relationshipCat);
											%>											
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = familyMemberDTO.dateOfBirth + "";
											%>
											<%
											String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_dateOfBirth%>
				
			
										</td>
										<td>
											<%
											value = familyMemberDTO.bloodGroupCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "blood_group", familyMemberDTO.bloodGroupCat);
											%>											
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = familyMemberDTO.genderCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "gender", familyMemberDTO.genderCat);
											%>											
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = familyMemberDTO.religionCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "religion", familyMemberDTO.religionCat);
											%>											
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = familyMemberDTO.adress + "";
											%>
														
											<%=value%>
				
			
										</td>
											<td>
													<%
														if(familyMemberDTO.drawingFileId != -1)
														{
															FilesDTO filesDTO = filesDAO.getMiniDTOById(familyMemberDTO.drawingFileId);
															
															if(filesDTO.thumbnailBlob != null)
															{
															byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
															%>
															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' >
															<%
															}
															%>
															<a onclick="window.open(this.href,'_blank');return false;"
														   href='Edms_documentsServlet?actionType=previewDropzoneFile&id=<%=filesDTO.iD%>'
														   download><%=filesDTO.fileTitle%>
															</a>
															<a href='Edms_documentsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download>
																Download
															</a>
															<%
														}
														%>												
						
					
												</td>
										<td>
											<%
											value = familyMemberDTO.nid + "";
											%>
														
											<%=value%>
				
			
										</td>
										<td>
											<%
											value = familyMemberDTO.birthRegistrationNumber + "";
											%>
														
											<%=value%>
				
			
										</td>
										
										<td>
											<%
											value = familyMemberDTO.mobile + "";
											%>
														
											<%=value%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
               


        </div>