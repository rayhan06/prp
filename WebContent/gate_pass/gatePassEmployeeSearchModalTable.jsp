<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="util.RecordNavigator" %>
<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="employee_assign.EmployeeAssignDTO" %>
<%@ page import="test_lib.util.Pair" %>
<%@ page import="java.util.*" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="util.StringUtils" %>
<%@page pageEncoding="utf-8" %>
<style>
    .btn:hover {
        color: #fff;
    }
</style>
<%
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String Language = "Bangla";

    boolean isBanglaLanguage = "Bangla".equalsIgnoreCase(Language);
%>

<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<div class="table-responsive">
    <table id="gatePassEmployeeSearchModal_tableData" class="table table-bordered table-striped">
        <thead class="text-nowrap">
        <tr>
            <th class="table-title"><b><%=LM.getText(LC.USER_ADD_USER_NAME, Language)%></b></th>
            <th class="table-title"><b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, Language)%></b></th>
            <th class="table-title"><b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, Language)%></b></th>
            <th></th>
        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<EmployeeAssignDTO> data = (List<EmployeeAssignDTO>) session.getAttribute("data");
            if (data != null && data.size() > 0) {
                for (EmployeeAssignDTO dto : data) {
        %>

        <%-- IMPORTANT: row id format-> <record id>_<officeunit id>_<organogram id>--%>
        <tr id="<%=dto.employee_id%>_<%=dto.office_unit_id%>_<%=dto.organogram_id%>">
            <input type='hidden' id='<%=dto.employee_id%>_<%=dto.office_unit_id%>_<%=dto.organogram_id%>_data'
                   value='<%=EmployeeSearchModalUtil.getEmployeeSearchModelJson(dto.employee_id,dto.office_unit_id,dto.organogram_id)%>'>
            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, dto.username)%>
            </td>
            <td>
                <input type="hidden" name ="e_employeeId" id = "employeeId_<%=dto.employee_id%>_<%=dto.office_unit_id%>_<%=dto.organogram_id%>" value = "<%=dto.organogram_id%>" />
                <input type="hidden" name ="e_userName" id = "userName_<%=dto.employee_id%>_<%=dto.office_unit_id%>_<%=dto.organogram_id%>" value = "<%=dto.username%>" />
                <%=isBanglaLanguage ? dto.employee_name_bng : dto.employee_name_eng%>
            </td>
            <td id="<%=dto.organogram_id%>">
                <%=isBanglaLanguage ? dto.organogram_name_bng.concat("<br>").concat(dto.unit_name_bng)
                        : dto.organogram_name_eng.concat("<br>").concat(dto.unit_name_eng)%>
            </td>
            <td>
                <button class='btn btn-sm shadow btn-border-radius add-btn text-white d-flex justify-content-between align-items-center'
                        type="button"
                        onclick="use_row_button(this, '<%=dto.employee_id%>_<%=dto.office_unit_id%>_<%=dto.organogram_id%>');">
                    <i class="fa fa-plus"></i>
                    <%=LM.getText(LC.HM_ADD, Language)%>
                </button>
            </td>
        </tr>

        <%
                }
            }
        %>
        </tbody>
    </table>
</div>