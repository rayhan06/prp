<%@page import="gate_pass_item.Gate_pass_itemDAO"%>
<%@page import="gate_pass_affiliated_person.Gate_pass_affiliated_personDAO"%>
<%@page pageEncoding="UTF-8" %>

<%@page import="gate_pass.*" %>
<%@page import="gate_pass_type.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = Language.equalsIgnoreCase("English");
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_GATE_PASS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Gate_pass_searchModel gate_passDTO = (Gate_pass_searchModel) request.getAttribute("gate_passDTO");
    CommonDTO commonDTO = gate_passDTO;
    String servletName = "Gate_passServlet";

   
    String Message = "Done";
    
    System.out.println("gate_passDTO = " + gate_passDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Gate_passDAO gate_passDAO = (Gate_passDAO) request.getAttribute("gate_passDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    
    SimpleDateFormat hFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");

    boolean notificaiton = userDTO.employee_record_id == gate_passDTO.referrerId;

    long today = Calendar.getInstance().getTimeInMillis();
    
    GatePassModel gatePassModel = gate_passDAO.getGatePassModelById(gate_passDTO.gate_pass_id);
    Gate_pass_itemDAO gate_pass_itemDAO = new Gate_pass_itemDAO();
    List<GatePassCardIssueModalItem> gatePassCardIssueModalItems = gate_pass_itemDAO.getAdditionalItems(gate_passDTO.gate_pass_id);
    boolean isGalleryPass = gate_passDTO.gatePassTypeId == Gate_pass_typeDTO.GALLERY_PASS;
%>

<td id='gatePassType'>
    <%="<b>" + (isLangEng ? (gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ?
            gate_passDTO.gatePassTypeEng + " (" + gate_passDTO.gatePassSubTypeEng + ")" : gate_passDTO.gatePassTypeEng) : (gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ?
            gate_passDTO.gatePassTypeBng + " (" + gate_passDTO.gatePassSubTypeBng + ")" : gate_passDTO.gatePassTypeBng)) + "</b>"%>
</td>

<td>
<span id='<%=i%>_visitingDate'>
    <%
        String formatted_visitingDate, formatted_visitingStartDate, formatted_visitingEndDate;
        if (gate_passDTO.gatePassSubTypeEng.equalsIgnoreCase("official") && gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
            formatted_visitingStartDate = simpleDateFormat.format(new Date(gate_passDTO.validityFrom));
            formatted_visitingEndDate = simpleDateFormat.format(new Date(gate_passDTO.validityTo));
    %>
    <%="<b>" + Utils.getDigits(formatted_visitingStartDate, Language)%> <%=isLangEng ? "to" : "থেকে"%> <%=Utils.getDigits(formatted_visitingEndDate, Language) + "</b>"%>
    <%
    } else {
        formatted_visitingDate = simpleDateFormat.format(new Date(gate_passDTO.visitingDate));
    %>
    <%="<b>" + Utils.getDigits(formatted_visitingDate, Language) + "</b>"%>
    <%
        }
    %>
</span>

</td>

<td id='gatePassVisitorName'>
    <%
        String address = gate_passDTO.currentAddress.addressText;
        if (isLangEng) {
            address += gate_passDTO.currentAddress.thanaEng.equals("") ? "" : ", " + gate_passDTO.currentAddress.thanaEng;
            address += gate_passDTO.currentAddress.districtEng.equals("") ? "" : ", " + gate_passDTO.currentAddress.districtEng;
            address += gate_passDTO.currentAddress.divisionEng.equals("") ? "" : ", " + gate_passDTO.currentAddress.divisionEng;
        } else {
            address += gate_passDTO.currentAddress.thanaBng.equals("") ? "" : ", " + gate_passDTO.currentAddress.thanaBng;
            address += gate_passDTO.currentAddress.districtBng.equals("") ? "" : ", " + gate_passDTO.currentAddress.districtBng;
            address += gate_passDTO.currentAddress.divisionBng.equals("") ? "" : ", " + gate_passDTO.currentAddress.divisionBng;
        }
        value = "<em><b>" + gate_passDTO.name + "</b></em><br/>" + address;
    %>

    <%=value%>


</td>

<td id='gatePassVisitorMobileNo'>
    <%
        value = "<b>" + Utils.getDigits(gate_passDTO.mobileNumber, Language) + "</b><br/>" +
                (isLangEng ? gate_passDTO.credentialTypeEng + " - " : gate_passDTO.credentialTypeBng + " - ") +
                Utils.getDigits(gate_passDTO.credentialNo, Language);
    %>

    <%=value%>

</td>

<td>
 <%
 if(gatePassModel != null && gatePassModel.persons != null)
 {
        for (GatePassPersonModel model : gatePassModel.persons) {
    %>
    
    
            <b><%=model.name%>: </b>  <%=Language.equalsIgnoreCase("English") ? model.mobileNoEng : model.mobileNoBng%>
            <br>
        
    <%
        }
 }
    %>
</td>

<td>
    <%
        if (HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng) {
            if (gate_passDTO.referrerOfficeUnitEng.equals("")) {
                value = "<em><b>" + gate_passDTO.referrerNameEng + "</b></em>";
            } else {
                value = "<em><b>" + gate_passDTO.referrerNameEng + "</b></em><br/>" + gate_passDTO.referrerOfficeUnitEng;
            }
        } else {
            if (gate_passDTO.referrerOfficeUnitBng.equals("")) {
                value = "<em><b>" + gate_passDTO.referrerNameBng + "</b></em>";
            } else {
                value = "<em><b>" + gate_passDTO.referrerNameBng + "</b></em><br/>" + gate_passDTO.referrerOfficeUnitBng;
            }
        }
    %>
    <%=value%>
    <br>
    <%=WorkflowController.getUserNameFromErId(gate_passDTO.referrerId, Language)%>

</td>

<td>
	<b><%=gate_passDTO.secondLayerApprovalStatus == 1? WorkflowController.getNameFromUserName(gate_passDTO.secondLayerApprovedBy, Language): ""%></b>
</td>

<td>

   <%
     if (gate_passDTO.partiallyIssued) {
     %>
         <span class="text-primary font-italic" id="<%=i%>_approvalStatus">
             <%=isLangEng ? "Issued For" : "যাদের জন্য ইস্যুকৃত"%>
         </span>
     <%
	     if(gatePassCardIssueModalItems != null)
	     {
	    	 System.out.println("LLLanguage = " + Language);
	    	 %>
	    	 <table>
	    	 	<tr>
		    	 	<td><%=isLangEng ? "Name" : "নাম"%>
		    	 	</td>
		    	 	<td><%=isLangEng ? "Card Number" : "কার্ড নাম্বার"%>
		    	 	</td>
		    	 	<%
		    	 	if(isGalleryPass)
		    	 	{
		    	 		%>
		    	 	<td><%=isLangEng ? "Gallery" : "গ্যালারি"%>
		    	 	</td>
		    	 		<%
		    	 	}
		    	 	else
		    	 	{
		    	 	%>
		    	 	<td><%=isLangEng ? "Items" : "আইটেমসমূহ"%>
		    	 	</td>
		    	 	<%
		    	 	}
		    	 	%>
		    	 	<td><%=isLangEng ? "Return" : "ফেরত দিন"%>
		    	 	</td>
		    	 	
	    	 	
	    	 	</tr>
	    	 <%
	    	int iCount = 0;
	     	for(GatePassCardIssueModalItem gatePassCardIssueModalItem: gatePassCardIssueModalItems)
	     	{
	     		%>
	     		<tr>
	     		<td style = "text-align: left;">
	     			<b><%=Utils.getDigits(++iCount, Language)%>. <%=gatePassCardIssueModalItem.visitorName%></b>
	     		</td>
	     		<td style = "text-align: left;" class = "cardNumber"><%=Utils.getDigits(gatePassCardIssueModalItem.cardNumber, Language)%></td>
	     		<%
		    	 	if(isGalleryPass)
		    	 	{
		    	 		%>
		    	 	<td style = "text-align: left;" ><%=CatDAO.getName(Language, "gallery", gatePassCardIssueModalItem.galleryCat)%></td>
		    	 		<%
		    	 	}
		    	 	else
		    	 	{
		    	 	%>
			    	<td>
			    	<%
			    	if(gatePassCardIssueModalItem.itemIdList != null && !gatePassCardIssueModalItem.itemIdList.equalsIgnoreCase(""))
			    	{
			    		String []items = gatePassCardIssueModalItem.itemIdList.split(", ");
			    		if(items != null)
			    		{
			    			int j = 0;
			    			for(String item: items)
			    			{		    				
				    		%>
				    		<%=j>0?", ":""%><%=CommonDAO.getName(Language, "parliament_item", Integer.parseInt(item)) %>
				    		<%
				    		j++;
			    			}
			    		}
				    		
			    	}
			    	%>
			    	</td>
			    	<%
		    	 	}
			    	%>
	     			<td>
	     			<%
	     			if(gatePassCardIssueModalItem.returnTime < 0)
	     			{
	     				if(userDTO.roleID == SessionConstants.PBS_ROLE)
	     				{
	     			%>
		     			<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
					            onclick="location.href='Gate_pass_itemServlet?&actionType=return&id=<%=gatePassCardIssueModalItem.id%>'">
					        <i class="fas fa-undo"></i>
					    </button>
				    <%
	     				}
	     			}
	     			else
	     			{
	     				%>
	     				<%=isLangEng ? "Returned On: " : "ফেরতের সময়: "%>
	     				<%=Utils.getDigits(hFormat.format(new Date(gatePassCardIssueModalItem.returnTime)), Language)%>
	     				<%
	     			}
				    %>
	     		</td>
	     		
	     		</tr>
	     		<%
	     	}
	     	%>
	     	</table>
	     	<%
	     }
     }
    else if (gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") && gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
    %>
    <span class="text-primary font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Pending" : "অনিষ্পন্ন"%>
        </span>
    <%
    } else if (gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") && gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
    %>
    <span class="text-success  font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Approved" : "অনুমোদিত"%>
        </span>
    <%
    } else if (gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") && gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusCancelled) {
    %>
    <span class="text-danger font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Dismissed" : "অননুমোদিত"%>
        </span>
    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved && gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
    %>
    <span class="text-info font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Recommended" : "সুপারিশকৃত"%>
        </span>
    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved && gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
    %>
    <span class="text-success font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Approved" : "অনুমোদিত"%>
        </span>
    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved && gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusCancelled) {
    %>
    <span class="text-danger font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Dismissed" : "অননুমোদিত"%>
        </span>

    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved && gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusProcessing) {
    %>
    <span class="text-info font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Processing" : "প্রক্রিয়াধীন"%>
        </span>
    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
    %>
    <span class="text-primary font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Pending" : "অনিষ্পন্ন"%>
        </span>

    <%
    } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusCancelled) {
    %>
    <span class="text-danger font-italic" id="<%=i%>_approvalStatus">
            <%=isLangEng ? "Dismissed" : "অননুমোদিত"%>
        </span>
    <%
        }
    %>

</td>

<%
if(userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE)
{
%>

<td  >

    <div class="button-group">
    	<table>
    		<tr>
        <%
            if (!gate_passDTO.partiallyIssued) 
            {
        		if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending) 
        		{
        %>
        <td>
	        <button type="button" id='<%=i%>_button_dismiss'
	                class="btn btn-sm btn-danger border-0 shadow  btn-border-radius"
	                onclick="sea_dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
	            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
	        </button>
        </td>
        <td>
	        <button type="button" id='<%=i%>_button_approve'
	                class="btn btn-sm btn-success border-0 shadow  btn-border-radius"
	                onclick="sea_approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.visitingDate%>')">
	            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
	        </button>
        </td>
        <td>
	        <button type="button" id='<%=i%>_button_process'
	                class="btn btn-sm btn-primary border-0 shadow  btn-border-radius"
	                onclick="sea_processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
	            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
	        </button>
	     </td>
        <%
        	}
        	else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) 
        	{
        %>
        <td><button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm btn-danger border-0 shadow  btn-border-radius"
                onclick="sea_dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button></td>
        <td><button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm btn-success border-0 shadow  btn-border-radius"
                onclick="sea_approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.visitingDate%>')"
                disabled>
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
        </button></td>
        <td><button type="button" id='<%=i%>_button_process'
                class="btn btn-sm btn-primary border-0 shadow  btn-border-radius"
                onclick="sea_processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
        </button></td>
        <%
        	} 
        	else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusProcessing) 
        	{
        %>

        <td><button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm btn-danger border-0 shadow  show-approval-notification btn-border-radius"
                onclick="sea_dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button></td>
        <td><button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm btn-success border-0 shadow  btn-border-radius"
                onclick="sea_approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.visitingDate%>')">
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
        </button></td>
        <td><button type="button" id='<%=i%>_button_process'
                class="btn btn-sm btn-primary border-0 shadow  btn-border-radius"
                onclick="sea_processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')"
                disabled>
            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
        </button></td>

        <%
        	}
        	else 
        	{
        %>
        <td><button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm border-0 btn-danger shadow show-approval-notification btn-border-radius"

                onclick="sea_dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')"
                disabled>
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button></td>
        <td><button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm border-0 btn-success shadow btn-border-radius"
                onclick="sea_approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.visitingDate%>')">
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
        </button></td>
        <td><button type="button" id='<%=i%>_button_process'
                class="btn btn-sm border-0 btn-primary shadow btn-border-radius"
                onclick="sea_processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
        </button></td>
        <%
            }
         }
         else
         {
         	%>
         	<td><button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm border-0 btn-danger shadow show-approval-notification btn-border-radius"
                disabled>
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        	</button></td>
	        <td><button type="button" id='<%=i%>_button_approve'
	                class="btn btn-sm border-0 btn-success shadow btn-border-radius"
	                disabled>
	            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
	        </button></td>
	        <td><button type="button" id='<%=i%>_button_process'
	                class="btn btn-sm border-0 btn-primary shadow btn-border-radius"
	                disabled>
	            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
	        </button></td>
         	<%
         }
        %>
        	</tr>
        </table>

    </div>

</td>
	
	<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Gate_passServlet?actionType=armsOfficeView&ID=<%=gate_passDTO.gate_pass_id%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>



	
	<%
}
%>

<%
  if(userDTO.roleID != SessionConstants.SERJEANT_AT_ARMS_ROLE && userDTO.roleID != SessionConstants.PBS_ROLE)
  {
%>
<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Gate_passServlet?notification=<%=notificaiton%>&actionType=view&ID=<%=gate_passDTO.gate_pass_id%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>


<td id='<%=i%>_approval' class="text-nowrap" >

    <%--    not a referrer but a creator, will only can check the status--%>
    <%
        if ((gate_passDTO.creatorId == userDTO.employee_record_id && gate_passDTO.referrerId != userDTO.employee_record_id) ||
                (gate_passDTO.visitingDate < today) || gate_passDTO.partiallyIssued) {
    %>

    <%
    } else {
    %>
    <div class="button-group">
        <%
            formatted_visitingDate = simpleDateFormat.format(new Date(gate_passDTO.visitingDate));
            if (gate_passDTO.secondLayerApprovalStatus != Gate_passDAO.approvalStatusPending) {
        %>
        <button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm border-0 shadow text-white submit-btn btn-border-radius"
                style=""
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.gatePassSubTypeEng%>', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.visitingDate%>')"
                disabled>
            <%=gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (isLangEng ? "APPROVE" :
                    "অনুমোদিত") : (isLangEng ? "RECOMMEND" : "সুপারিশকৃত")%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm border-0 shadow text-white cancel-btn btn-border-radius" style=""
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.gate_pass_id%>', '<%=Utils.getDigits(formatted_visitingDate, Language)%>', '<%=gate_passDTO.gatePassSubTypeEng%>')"
                disabled>
            <%=gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (isLangEng ? "DISMISS" :
                    "অননুমোদিত") : (isLangEng ? "DISMISS" : "অসুপারিশকৃত")%>
        </button>
        <%
        } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
        %>
        <button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm border-0 shadow text-white submit-btn btn-border-radius" style=""
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.gatePassSubTypeEng%>', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.visitingDate%>')">
            <%=gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (isLangEng ? "APPROVE" :
                    "অনুমোদিত") : (isLangEng ? "RECOMMEND" : "সুপারিশকৃত")%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm border-0 shadow text-white cancel-btn btn-border-radius" style=""
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.gate_pass_id%>', '<%=Utils.getDigits(formatted_visitingDate, Language)%>', '<%=gate_passDTO.gatePassSubTypeEng%>')">
            <%=gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (isLangEng ? "DISMISS" :
                    "অননুমোদিত") : (isLangEng ? "DISMISS" : "অসুপারিশকৃত")%>
        </button>
        <%
        } else if (gate_passDTO.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
        %>
        <button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm border-0 shadow text-white submit-btn btn-border-radius"
                style=""
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.gatePassSubTypeEng%>', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.visitingDate%>')"
                disabled>
            <%=gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (isLangEng ? "APPROVED" :
                    "অনুমোদিত") : (isLangEng ? "RECOMMEND" : "সুপারিশকৃত")%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm border-0 shadow text-white cancel-btn btn-border-radius" style=""
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.gate_pass_id%>', '<%=Utils.getDigits(formatted_visitingDate, Language)%>', '<%=gate_passDTO.gatePassSubTypeEng%>')">
            <%=gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (isLangEng ? "DISMISS" :
                    "অননুমোদিত") : (isLangEng ? "DISMISS" : "অসুপারিশকৃত")%>
        </button>
        <%
        } else {
        %>
        <button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm border-0 shadow text-white submit-btn btn-border-radius" style=""
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.gatePassSubTypeEng%>', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.visitingDate%>')">
            <%=gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (isLangEng ? "APPROVE" :
                    "অনুমোদিত") : (isLangEng ? "RECOMMEND" : "সুপারিশকৃত")%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm border-0 shadow text-white cancel-btn btn-border-radius" style=""
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.gate_pass_id%>', '<%=Utils.getDigits(formatted_visitingDate, Language)%>', '<%=gate_passDTO.gatePassSubTypeEng%>')"
                disabled>
            <%=gate_passDTO.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (isLangEng ? "DISMISS" :
                    "অননুমোদিত") : (isLangEng ? "DISMISS" : "অসুপারিশকৃত")%>
        </button>
        <%
                }
            }
        %>

    </div>

</td>

<%
  }
%>

<%
if(userDTO.roleID == SessionConstants.PBS_ROLE)
  {
  	%>

  <td>
  	<button
            type="button" class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Gate_passServlet?actionType=parliamentBuildingSecurityView&ID=<%=gate_passDTO.gate_pass_id%>'">
        <i class="fa fa-eye"></i>
    </button>
  </td>
 	<%
  }
  %>

<div class="modal fade" id="gate-pass-validity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"
                    style="font-size: large; font-weight: bolder;"><%=isLangEng ?
                        "Pass Validity" : "প্রবেশ পাসের বৈধতার সময়সীমা"%>
                </h5>
            </div>
            <div class="modal-body">
                <form id="gate-validity-form" method="post">

                    <div class="form-body">
                        <label class="form-group control-label"><%=LM.getText(LC.GATE_PASS_ADD_VALIDITYFROM, loginDTO)%>
                        </label>
                        <div class="form-group ">
                            <div class="" id='validityFrom_div_<%=i%>'>
                                <div class="container">
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID" value="validity-from-date-js"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                </div>
                                <input type='hidden' class='form-control' id='validity-from-date' name='validityFrom'
                                       value=''
                                       tag='pb_html'/>
                            </div>
                        </div>


                        <label class="form-group control-label"><%=LM.getText(LC.GATE_PASS_ADD_VALIDITYTO, loginDTO)%>
                        </label>
                        <div class="form-group ">
                            <div class="" id='validityTo_div_<%=i%>'>
                                <div class="container">
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID" value="validity-to-date-js"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                </div>
                                <input type='hidden' class='form-control' id='validity-to-date' name='validityTo'
                                       value=''
                                       tag='pb_html'/>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="modal_submit"
                        onclick="modalDataSubmit()"><%=isLangEng ? "Submit" : "সম্পাদন করুন"%>
                </button>
            </div>
        </div>
    </div>
</div>



<style>
    td {
        text-align: center;
    }
</style>

