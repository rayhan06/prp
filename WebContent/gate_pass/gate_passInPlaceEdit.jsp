<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="gate_pass.Gate_passDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Gate_passDTO gate_passDTO = (Gate_passDTO)request.getAttribute("gate_passDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(gate_passDTO == null)
{
	gate_passDTO = new Gate_passDTO();
	
}
System.out.println("gate_passDTO = " + gate_passDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=gate_passDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gatePassVisitorId'>")%>
			

		<input type='hidden' class='form-control'  name='gatePassVisitorId' id = 'gatePassVisitorId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + gate_passDTO.gatePassVisitorId + "'"):("'" + "0" + "'")%>
 tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_visitingDate'>")%>
			
	
	<div class="form-inline" id = 'visitingDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'visitingDate_date_<%=i%>' name='visitingDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_visitingDate = dateFormat.format(new Date(gate_passDTO.visitingDate));
	%>
	'<%=formatted_visitingDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gatePassTypeId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='gatePassTypeId' id = 'gatePassTypeId_select2_<%=i%>' value='<%=gate_passDTO.gatePassTypeId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gatePassSubTypeId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='gatePassSubTypeId' id = 'gatePassSubTypeId_select2_<%=i%>' value='<%=gate_passDTO.gatePassSubTypeId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_referrerId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='referrerId' id = 'referrerId_select2_<%=i%>' value='<%=gate_passDTO.referrerId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_parliamentGateId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='parliamentGateId' id = 'parliamentGateId_select2_<%=i%>' value='<%=gate_passDTO.parliamentGateId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_validityFrom" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='validityFrom' id = 'validityFrom_hidden_<%=i%>' value='<%=gate_passDTO.validityFrom%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_validityTo" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='validityTo' id = 'validityTo_hidden_<%=i%>' value='<%=gate_passDTO.validityTo%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobCat" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value='<%=gate_passDTO.jobCat%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=gate_passDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=gate_passDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=gate_passDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + gate_passDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=gate_passDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Gate_passServlet?actionType=view&ID=<%=gate_passDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Gate_passServlet?actionType=view&modal=1&ID=<%=gate_passDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	