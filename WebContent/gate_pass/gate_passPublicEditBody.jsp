<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="parliament_gate.Parliament_gateRepository" %>
<%@ page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@ page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="gate_pass_sub_type.Gate_pass_sub_typeDTO" %>
<%@ page import="gate_pass_sub_type.Gate_pass_sub_typeDAO" %>
<%@ page import="com.google.gson.JsonObject" %>
<%@ page import="com.google.gson.JsonParser" %>
<%@ page import="com.google.gson.JsonArray" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="workflow.WorkflowController" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="com.google.gson.JsonArray" %>
<%@page import="com.google.gson.JsonParser" %>
<%@page import="dbm.DBMW" %>
<%@page import="files.FilesDAO" %>
<%@page import="gate_pass.Gate_passDTO" %>

<%@page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%@page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="parliament_gate.Parliament_gateRepository" %>

<%@page import="pb.CatDAO" %>
<%@page import="pb.CommonDAO" %>

<%
    Gate_passDTO gate_passDTO;
    gate_passDTO = (Gate_passDTO) request.getAttribute("gate_passDTO");
    if (gate_passDTO == null) {
        gate_passDTO = new Gate_passDTO();
    }


    String actionName = "add";
    String formTitle = language.equalsIgnoreCase("English") ? "Gate Pass" : "প্রবেশ পাস";
    boolean isPermanentTable = true;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }

    int i = 0;
    String value = "";
    int childTableStartingID = 1;
    long ColumnID;

    String tableName = "gate_pass";

    String isToast = (String) session.getAttribute("isToast");
    if (isToast == null) {
        isToast = "no";
    }

//    Now it's time to remove attribute
    session.setAttribute("isToast", "no");

    String Language = "Bangla";
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String jsonDTO = Gate_pass_sub_typeRepository.getInstance().buildJSON(Language);
    JsonArray jsonArray = new JsonParser().parse(jsonDTO).getAsJsonArray();

    String loginURL = request.getRequestURL().toString();
    String context_folder = request.getContextPath();

    LM.getInstance();

    String errorMessage = (Language == "English" ? "Select Referrer!" : "সুপারিশকারী নির্বাচন করুন!");

    String galleryPassDates = (String) session.getAttribute("galleryPassDates");

    if (galleryPassDates == null) {
        galleryPassDates = Language.equalsIgnoreCase("English") ? "<option>Select</option>" :
                "<option>বাছাই করুন</option>";
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <form class="form-horizontal"
                      action="Gate_passPublicServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-8 offset-1">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white">
                                                                <%=formTitle%>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <input type='hidden' class='form-control' name='id'
                                                           id='id_hidden_<%=i%>' value='<%=gate_passDTO.id%>'
                                                           tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='gatePassVisitorId'
                                                           id='gatePassVisitorId_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.gatePassVisitorId + "'"):("'" + "0" + "'")%>
                                                                   tag='pb_html'/>
                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Pass Type": "পাসের ধরণ"%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div class="" id='gatePassTypeId_div_<%=i%>'>
                                                                <select class='form-control' name='gatePassTypeId'
                                                                        id='gatePassTypeId'
                                                                        tag='pb_html'>
                                                                    <%=actionName.equals("edit") ? Gate_pass_typeRepository.getInstance().buildOptions(Language, gate_passDTO.gatePassTypeId) : Gate_pass_typeRepository.getInstance().buildOptions(Language, 0)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row" id='gatePassSubTypeId_div' style="display: none">
                                                        <label class="col-4 col-form-label text-right"
                                                               id="gatePassSubTypeIdLabel">
                                                            <%=Language.equalsIgnoreCase("English") ? "Pass SubType": "পাসের উপধরণ"%>
                                                        </label>
                                                        <div class="col-8">
                                                            <select class='form-control ignore-gate-pass-subtype-validation'
                                                                    name='gatePassSubTypeId'
                                                                    id='gatePassSubTypeId'
                                                                    tag='pb_html'>
                                                                <%--<%=actionName.equals("edit") ? Gate_pass_sub_typeRepository.getInstance().buildOptions(Language,gate_passDTO.gatePassSubTypeId) : Gate_pass_sub_typeRepository.getInstance().buildOptions(Language,0)%>--%>
                                                            </select>
                                                            <span class="error" id="sub-pass-type-error-msg" style="display:none; color: #fd397a; font-size: x-small">
                                                                <%=Language.equalsIgnoreCase("English") ? "Select sub pass type!" : "পাসের উপধরণ বাছাই করুন!"%>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row" id="gallery-date" style="display: none">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Visiting Date": "সাক্ষাতের তারিখ"%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div class="" id='gallery-pass-date-div'>
                                                                <select class='form-control' name='gallery-date' id='gallery-pass-dates' tag='pb_html'>
                                                                    <%=galleryPassDates%>
                                                                </select>
                                                                <span class="error" id="gallery-date-error-msg" style="display:none; color: #fd397a; font-size: x-small">
                                                                <%=Language.equalsIgnoreCase("English") ? "Select visiting date!" : "সাক্ষাতের তারিখ দিন!"%>
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row" id="default-date">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Visiting Date": "সাক্ষাতের তারিখ"%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div class="" id='visitingDate_div_<%=i%>'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="visiting-date-js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='visiting-date' name='visitingDate' value=''
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Referrer": "সুপারিশকারী"%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div class="" id='select-referrer-div'>
                                                                <input type='hidden' class='form-control'
                                                                       id="referrer-id" name="referrerId"
                                                                       tag='pb_html'/>
                                                                <button type="button"
                                                                        class="btn btn-primary form-control rounded"
                                                                        id="tagEmp_modal_button" disabled>
                                                                    <%=Language.equalsIgnoreCase("English") ? "Select Referrer": "সুপারিশকারী নির্বাচন করুন"%>
                                                                </button>
                                                                <div class="error-alert" id="referrer_error"
                                                                     style="display: none">
                                                                    <%=errorMessage%>
                                                                </div>
                                                                <table class="table table-bordered table-striped">
                                                                    <thead></thead>
                                                                    <tbody id="tagged_emp_table" class="rounded">
                                                                    <tr style="display: none;" class="selected_employee_row">
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td>
                                                                            <button type="button"
                                                                                    class="btn btn-danger btn-block rounded"
                                                                                    onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                                <%=Language.equalsIgnoreCase("English") ? "Remove": "মুছুন"%>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Gate No": "গেইট নং"%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div class="" id='parliamentGateId_div_<%=i%>'>
                                                                <select class='form-control' name='parliamentGateId'
                                                                        id='parliamentGateId_select2_<%=i%>'
                                                                        tag='pb_html'>
                                                                    <%=actionName.equals("edit") ? Parliament_gateRepository.getInstance().buildOptions(Language, gate_passDTO.parliamentGateId) : Parliament_gateRepository.getInstance().buildOptions(Language, 0)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type='hidden' class='form-control' name='validityFrom'
                                                           id='validityFrom_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.validityFrom + "'"):("'" + "0" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='validityTo'
                                                           id='validityTo_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.validityTo + "'"):("'" + "0" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='jobCat'
                                                           id='jobCat_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.jobCat + "'"):("'" + "-1" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='insertionDate'
                                                           id='insertionDate_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='insertedBy'
                                                           id='insertedBy_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.insertedBy + "'"):("'" + "" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='modifiedBy'
                                                           id='modifiedBy_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='isDeleted'
                                                           id='isDeleted_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control'
                                                           name='lastModificationTime'
                                                           id='lastModificationTime_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                                   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-8 offset-2 mt-5">
                                <div class="form-actions text-right">
                                    <button class="btn border-0 btn-primary shadow" style="border-radius: 8px" type="submit">
                                        <%=Language.equalsIgnoreCase("English") ? "GO NEXT": "পরবর্তী"%>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<jsp:include page="../gate_pass/gatePassEmployeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    const obj = <%=jsonArray%>;
    language = '<%=Language%>';
    $('#gatePassTypeId').on('change', (event) => {
        const typeId = event.target.value;
        let count = 0;
        $('#gatePassSubTypeId').empty();
        $('#gatePassSubTypeId').append($('<option>', {
            text: (language == 'English' ? 'Select' : 'বাছাই করুন'),
            value: ''
        }));
        obj.forEach(elem => {
            if (elem.gatePassTypeId == typeId) {
                $('#gatePassSubTypeId').append($('<option>', {
                    text: (language == 'English' ? elem.nameEng : elem.nameBng),
                    value: elem.id
                }));  //"<option value = '"+i.toString()+"'>" + (language == 'English' ? i.toString() : convDateStrToBn(i)) + "</option>";
                count++;
            }
        });

        // if(count > 0) $('#gatePassSubTypeId').prop('disabled', false);
        // else $('#gatePassSubTypeId').prop('disabled', true);

        if (count > 0) {
            $('#gatePassSubTypeId_div').show();
            $('#gatePassSubTypeIdLabel').show();
            $("#gatePassSubTypeId").select2({
                dropdownAutoWidth: true
            });
        } else {
            $('#gatePassSubTypeId_div').hide();
            $('#gatePassSubTypeIdLabel').hide();
        }

        if ($('#gatePassTypeId').val()==11) {
            $('#default-date').hide();
            $('#gallery-date').show();
        } else {
            $('#default-date').show();
            $('#gallery-date').hide();
            $('#gallery-date-error-msg').hide();
        }

        if ($('#gatePassTypeId').val()=='') {
            $('#tagEmp_modal_button').prop("disabled", true);
            $('.selected_employee_row').hide();
            $('#referrer-id').val('');
        } else {
            $('#tagEmp_modal_button').prop("disabled", false);
            $('.selected_employee_row').hide();
            $('#referrer-id').val('');
        }
    });

    $('#gatePassSubTypeId').on('change', (event) => {
        if ($("#gatePassSubTypeIdLabel").is(":visible")) {
            if ($('#gatePassSubTypeId').val()) {
                $('#sub-pass-type-error-msg').hide();
            }
        }
    });

    $(document).ready(function () {
        // $('#gatePassSubTypeId').prop('disabled', true);
        $('#gatePassSubTypeId_div').hide();
        $('#gatePassSubTypeIdLabel').hide();

        setDateByStringAndId('visiting-date-js', '<%=dateFormat.format(new Date(System.currentTimeMillis()))%>');

        $.validator.addMethod('passSelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('referrerSelection', function (value, element) {

            let referrer_id_value = document.getElementById('referrer-id').value;
            if (referrer_id_value == "" || referrer_id_value == undefined) {
                console.log("No referrer selected!");
                return false;
            } else {
                console.log("Referrer selected!");
                return true;
            }
        });
        $.validator.addMethod('gateSelection', function (value, element) {
            return value != 0;
        });
        $("#bigform").validate({
            ignore: [],
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                gatePassTypeId: {
                    required: true,
                    passSelection: true
                },
                referrerId: {
                    required: true,
                    referrerSelection: true
                },
                parliamentGateId: {
                    required: true,
                    gateSelection: true
                }
            },

            messages: {
                gatePassTypeId: '<%=Language.equalsIgnoreCase("English") ? "Select gate pass type!" : "পাসের ধরণ বাছাই করুন!"%>',
                <%--gatePassSubTypeId: '<%=Language.equalsIgnoreCase("English") ? "Select gate pass sub-type!" : "পাসের উপ-ধরন বাছাই করুন!"%>',--%>
                referrerId: '<%=Language.equalsIgnoreCase("English") ? "Select your referrer!" : "সুপারিশকারি নির্বাচন করুন!"%>',
                parliamentGateId: '<%=Language.equalsIgnoreCase("English") ? "Select a gate!" : "কোন গেইট দিয়ে আপনি প্রবেশ করতে চান?"%>'
            }
        });


        // fire a swal
        let isToast = '<%=isToast%>';
        if (isToast == 'yes') {
            $(".swal-overlay").css("background-color", "#0080ff");
            $(".swal-title").css("font-size", "32px");

            Swal.fire({
                position: 'center',
                type: 'success',
                text: '<%=Language.equalsIgnoreCase("English") ? "Your Gate Pass request is submitted for approval!" : "আপনার প্রবেশ পাসটি অনুমোদনের জন্য কতৃপক্ষের নিকট পাঠানো হয়েছে!"%>',
                showConfirmButton: false,
                timer: 3000
            });
        }

    });


    $(function () {



        <%
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.YEAR, 1);
            String maxVisitingDate = dateFormat.format(c.getTime());
        %>
        setMinDateById('visiting-date-js', '<%=datestr%>');
        setMaxDateById('visiting-date-js', '<%=maxVisitingDate%>');

        <%if (actionName.equals("edit")) {%>
        setDateByTimestampAndId('visiting-date-js', <%=gate_passDTO.visitingDate%>);
        <%}%>

    });

    function PreprocessBeforeSubmiting(row, validate) {
        let validReferrer = true;
        let validGalleryDate = false;
        $('#gallery-date-error-msg').hide();
        // validate gate pass sub type
        if ($("#gatePassSubTypeIdLabel").is(":visible")) {
            if (!$('#gatePassSubTypeId').val()) {
                $('#sub-pass-type-error-msg').show();
                return false;
            } else {
                $('#sub-pass-type-error-msg').hide();
            }
        }

        // dateValidator('visiting-date-js', true);

        if (added_employee_info_map.size !== 0) {
            $('#organogramId_error').hide();
            validReferrer = true;
        } else {
            validReferrer = false;
            $('#organogramId_error').show();
        }


        if ($('#gatePassTypeId').val()==11) {
            if ($('#gallery-pass-dates').val() !== "") {
                validGalleryDate = true;
                $('#visiting-date').val($('#gallery-pass-dates').val());
            } else {
                $('#gallery-date-error-msg').show();
            }

            return validGalleryDate && validReferrer;
        } else {
            $('#visiting-date').val(getDateStringById('visiting-date-js'));

            return dateValidator('visiting-date-js', true, {
                'errorEn': 'Enter valid visiting date!',
                'errorBn': 'সাক্ষাতের তারিখ প্রবেশ করুন!'
            }) && validReferrer;
        }

    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Gate_passServlet");
    }

    function init(row) {

        $("#gatePassTypeId").select2({
            dropdownAutoWidth: true
        });

        $("#gatePassSubTypeId").select2({
            dropdownAutoWidth: true
        });

        $("#referrerId_select2_" + row).select2({
            dropdownAutoWidth: true
        });

        $("#parliamentGateId_select2_" + row).select2({
            dropdownAutoWidth: true
        });


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true,
                callBackFunction: function (empInfo) {

                    document.getElementById('referrer-id').value = empInfo.employeeRecordId;
                    console.log('callBackFunction called and referrer Id is being populated!!');
                    $('#referrer-id-error').hide();
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#gate_pass_search_emp_modal').modal();
    });


    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);

        $('#referrer-id').val('');
    }
</script>






