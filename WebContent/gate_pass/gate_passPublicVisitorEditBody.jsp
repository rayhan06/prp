<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="gate_pass_visitor.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="gate_pass.Gate_passDTO" %>
<%@ page import="gate_pass.Gate_passDAO" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="gate_pass_item.Gate_pass_itemDTO" %>
<%@ page import="parliament_item.Parliament_itemRepository" %>
<%@ page import="nl.captcha.Captcha" %>

<%
    Gate_pass_visitorDTO gate_pass_visitorDTO;
    Gate_pass_itemDTO gate_pass_itemDTO;
    gate_pass_visitorDTO = (Gate_pass_visitorDTO) request.getAttribute("gate_pass_visitorDTO");
    if (gate_pass_visitorDTO == null) {
        gate_pass_visitorDTO = new Gate_pass_visitorDTO();

    }


    String actionName = "add";


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;


    Gate_passDTO gate_passDTO = (Gate_passDTO) session.getAttribute("gate_passDTO");
    System.out.println("Men in Gate: " + gate_passDTO);
    String Language = "Bangla";
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <form class="form-horizontal"
                      action="Gate_passPublicServlet?actionType=insertVisitor&isPermanentTable=<%=isPermanentTable%>"
                      id="visitor-form" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-10 offset-1">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white">
                                                                <%=Language.equalsIgnoreCase("English") ? "Gate Pass" : "প্রবেশ পাস"%>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <input type='hidden' class='form-control' name='id'
                                                           id='id_hidden_<%=i%>'
                                                           value='<%=gate_pass_visitorDTO.id%>' tag='pb_html'/>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Mobile Number" : "মোবাইল নাম্বার"%>
                                                        </label>
                                                        <div class="col-9">
                                                            <div class="input-group" id='mobileNumber_div_<%=i%>'>
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon3"><%=Language.equalsIgnoreCase("English") ? "+88" : "+৮৮"%></span>
                                                                </div>
                                                                <input type='text'
                                                                       class='form-control englishDigitOrCharOnly'
                                                                       name='mobileNumber'
                                                                       id='mobileNumber'
                                                                       value='<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.mobileNumber + "'"):("'" + "" + "'")%>'
                                                                       placeholder='<%=Language.equalsIgnoreCase("English") ? "enter your 11 digit mobile number, ex: 01710101010" : "১১ ডিজিটের মোবাইল নাম্বারটি দিন, উদাহরণঃ 01710101010"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "NID/Passport/Birth-Certificate" : "এন আই ডি/পাসপোর্ট/জন্ম-নিবন্ধন"%>
                                                        </label>
                                                        <div class="col-9">
                                                            <div id='credentialType_div_<%=i%>'>
                                                                <select class='form-control' name='credentialType'
                                                                        id='credentialType'
                                                                        tag='pb_html'>
                                                                    <%=CatRepository.getInstance().buildOptions("credential", Language, null)%>
                                                                </select>
                                                            </div>
                                                            <div id='credentialNo_div_<%=i%>'>
                                                                <input type='text' class='form-control englishDigitOrCharOnly'
                                                                       name='credentialNo' id='credentialNo'
                                                                       value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.credentialNo + "'"):("'" + "" + "'")%>
                                                                               placeholder='<%=Language.equalsIgnoreCase("English") ? "enter credential number" : "পরিচয়পত্রের নাম্বার প্রবেশ করান"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Email" : "ইমেইল"%>
                                                        </label>
                                                        <div class="col-9">
                                                            <div id='email_div_<%=i%>'>
                                                                <input type='text' class='form-control' name='email'
                                                                       id='email'
                                                                       value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.email + "'"):("'" + "" + "'")%>
                                                                               required="required"
                                                                       placeholder='<%=Language.equalsIgnoreCase("English") ? "enter valid email" : "ইমেইল এড্রেস প্রবেশ করান"%>'
                                                                       pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                                       title='<%=Language.equalsIgnoreCase("English") ? "email must be a of valid format" : "বৈধ ইমেইল প্রবেশ করান!"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Visitor's Name" : "সাক্ষাৎ প্রার্থীর নাম"%>
                                                        </label>
                                                        <div class="col-9">
                                                            <div id='name_div_<%=i%>'>
                                                                <input type='text' class='form-control' name='name'
                                                                       id='name'
                                                                       value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.name + "'"):("'" + "" + "'")%>
                                                                               placeholder='<%=Language.equalsIgnoreCase("English") ? "enter visitor name" : "সাক্ষাৎ প্রার্থীর নাম প্রবেশ করান"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Father's Name" : "পিতার নাম"%>
                                                        </label>
                                                        <div class="col-9">
                                                            <div id='fatherName_div_<%=i%>'>
                                                                <input type='text' class='form-control'
                                                                       name='fatherName' id='fatherName'
                                                                       value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.fatherName + "'"):("'" + "" + "'")%>
                                                                               placeholder='<%=Language.equalsIgnoreCase("English") ? "enter visitor`s father`s name" : "সাক্ষাৎ প্রার্থীর পিতার নাম প্রবেশ করান"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Mother's Name" : "মাতার নাম"%>
                                                        </label>
                                                        <div class="col-9">
                                                            <div id='motherName_div_<%=i%>'>
                                                                <input type='text' class='form-control'
                                                                       name='motherName' id='motherName'
                                                                       value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.motherName + "'"):("'" + "" + "'")%>
                                                                               placeholder='<%=Language.equalsIgnoreCase("English") ? "enter visitor`s mother`s name" : "সাক্ষাৎ প্রার্থীর মাতার নাম প্রবেশ করান"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row" style="display: none" id="addressSelector">
                                                        <label class="col-3 col-form-label text-right">
                                                        </label>
                                                        <div class="col-9 d-flex align-items-center">
                                                            <input class="form-control-sm form-check-input ml-1"
                                                                   type="checkbox" value=""
                                                                   id="checkNewAddress">
                                                            <label class="form-check-label ml-4 mt-1"
                                                                   for="checkNewAddress"
                                                                   style="font-size:small;font-weight: bolder; padding-left: 5px">
                                                                <%=Language.equalsIgnoreCase("English") ? "check, if you want to update address" : "ঠিকানা পরিবর্তন করতে চাইলে টিক দিন"%>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div id="address_without_data">
                                                        <div class="form-group row">
                                                            <label class="col-3 col-form-label text-right">
                                                                <%=Language.equalsIgnoreCase("English") ? "Permanent Address" : "স্থায়ী ঠিকানা"%>
                                                            </label>
                                                            <div class="col-9">
                                                                <div id='permanentAddress_div_<%=i%>'>
                                                                    <div id='permanentAddress_geoDIV_<%=i%>'
                                                                         tag='pb_html'>
                                                                        <select class='form-control'
                                                                                name='permanentAddress_active'
                                                                                id='permanentAddress_geoSelectField_<%=i%>'
                                                                                onchange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'permanentAddress', this.getAttribute('row'))"
                                                                                tag='pb_html' row='<%=i%>'></select>
                                                                    </div>
                                                                    <%if (Language.equalsIgnoreCase("English")) {%>
                                                                    <input type='text'
                                                                           class='englishAddressValidation form-control rounded'
                                                                           name='permanentAddress_text'
                                                                           id='permanentAddress_geoTextField_<%=i%>'
                                                                           value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.permanentAddress)  + "'"):("'" + "" + "'")%>
                                                                                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Village/House Number" : "গ্রাম/বাড়ি নাম্বার"%>'
                                                                           tag='pb_html'>
                                                                    <%} else {%>
                                                                    <input type='text'
                                                                           class='noEnglish form-control rounded'
                                                                           name='permanentAddress_text'
                                                                           id='permanentAddress_geoTextField_<%=i%>'
                                                                           value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.permanentAddress)  + "'"):("'" + "" + "'")%>
                                                                                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Village/House Number" : "গ্রাম/বাড়ি নাম্বার"%>'
                                                                           tag='pb_html'>
                                                                    <%
                                                                        }
                                                                    %>

                                                                    <input type='hidden' class='form-control'
                                                                           name='permanentAddress'
                                                                           id='permanentAddress_geolocation_<%=i%>'
                                                                           value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(gate_pass_visitorDTO.permanentAddress)  + "'"):("'" + "1" + "'")%>
                                                                                   tag='pb_html'>
                                                                    <%
                                                                        if (actionName.equals("edit")) {
                                                                    %>
                                                                    <label class="control-label"><%=GeoLocationDAO2.parseText(gate_pass_visitorDTO.permanentAddress, Language) + "," + GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.permanentAddress)%>
                                                                    </label>
                                                                    <%
                                                                        }
                                                                    %>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row d-flex align-items-center">
                                                            <label class="col-3 col-form-label text-right">

                                                            </label>
                                                            <div class="col-9 d-flex align-items-center">
                                                                <input class="form-control-sm form-check-input ml-1"
                                                                       type="checkbox" value=""
                                                                       id="checkCurrentAddress">
                                                                <label class="form-check-label ml-4 mt-1"
                                                                       for="checkCurrentAddress"
                                                                       style="font-size:small;font-weight: bolder; padding-left: 5px">
                                                                    <%=Language.equalsIgnoreCase("English") ? "check, if permanent address and current/official address is same" : "বর্তমান/দাপ্তরিক ঠিকানা এবং স্থায়ী ঠিকানা একই হলে টিক দিন"%>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id='currentAddress_div'>
                                                            <label class="col-3 col-form-label text-right">
                                                                <%=Language.equalsIgnoreCase("English") ? "Current/Official Address" : "বর্তমান/দাপ্তরিক ঠিকানা"%>
                                                            </label>
                                                            <div class="col-9">
                                                                <div id='currentAddress_geoDIV_<%=i%>'
                                                                     tag='pb_html'>
                                                                    <select class='form-control'
                                                                            name='currentAddress_active'
                                                                            id='currentAddress_geoSelectField_<%=i%>'
                                                                            onchange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'currentAddress', this.getAttribute('row'))"
                                                                            tag='pb_html' row='<%=i%>'></select>
                                                                </div>
                                                                <%if (Language.equalsIgnoreCase("English")) {%>
                                                                <input type='text'
                                                                       class='englishAddressValidation form-control rounded'
                                                                       name='currentAddress_text'
                                                                       id='currentAddress_geoTextField_<%=i%>'
                                                                       value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.currentAddress)  + "'"):("'" + "" + "'")%>
                                                                               placeholder='<%=Language.equalsIgnoreCase("English") ? "Village/House Number" : "গ্রাম/বাড়ি নাম্বার"%>'
                                                                       tag='pb_html'>
                                                                <%} else {%>
                                                                <input type='text'
                                                                       class='noEnglish form-control rounded'
                                                                       name='currentAddress_text'
                                                                       id='currentAddress_geoTextField_<%=i%>'
                                                                       value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.currentAddress)  + "'"):("'" + "" + "'")%>
                                                                               placeholder='<%=Language.equalsIgnoreCase("English") ? "Village/House Number" : "গ্রাম/বাড়ি নাম্বার"%>'
                                                                       tag='pb_html'>
                                                                <%
                                                                    }
                                                                %>
                                                                <input type='hidden' class='form-control'
                                                                       name='currentAddress'
                                                                       id='currentAddress_geolocation_<%=i%>'
                                                                       value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(gate_pass_visitorDTO.currentAddress)  + "'"):("'" + "1" + "'")%>
                                                                               tag='pb_html'>
                                                                <%
                                                                    if (actionName.equals("edit")) {
                                                                %>

                                                                <label class="control-label"><%=GeoLocationDAO2.parseText(gate_pass_visitorDTO.currentAddress, Language) + "," + GeoLocationDAO2.parseDetails(gate_pass_visitorDTO.currentAddress)%>
                                                                </label>
                                                                <%
                                                                    }
                                                                %>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="address_with_data" style="display:none">
                                                        <div class="form-group row">
                                                            <label class="col-3 col-form-label">
                                                                <%=Language.equalsIgnoreCase("English") ? "Permanent Address" : "স্থায়ী ঠিকানা"%>
                                                            </label>
                                                            <div class="col-9">
                                                                    <span id='stored_permanent_address'
                                                                          style="font-weight:bold"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-3 col-form-label">
                                                                <%=Language.equalsIgnoreCase("English") ? "Current/Official Address" : "বর্তমান/দাপ্তরিক ঠিকানা"%>
                                                            </label>
                                                            <div class="col-9">
                                                                    <span id='stored_current_address'
                                                                          style="font-weight:bold"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Gender" : "লিঙ্গ"%>
                                                        </label>
                                                        <div class="col-9 " id='genderType_div_<%=i%>'>
                                                            <select class='form-control' name='genderType'
                                                                    id='genderType_select_<%=i%>' tag='pb_html'>
                                                                <%=CatRepository.getInstance().buildOptions("gender", Language, null)%>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label text-right">
                                                            <%=Language.equalsIgnoreCase("English") ? "Officer Type" : "কর্মকর্তার ধরণ"%>
                                                        </label>
                                                        <div class="col-9" id='officerType_div_<%=i%>'>
                                                            <select class='form-control' name='officerType'
                                                                    id='officerType_select_<%=i%>' tag='pb_html'>
                                                                <%=CatRepository.getInstance().buildOptions("officer", Language, null)%>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-0" id="affiliated-persons-add-lebel">
                                                        <div class="col-12 col-form-label">
                                                            <span class="h5">
                                                                <%=Language.equalsIgnoreCase("English") ? "Address the affiliated persons:" : "আপনার সাথে প্রবেশকারী ব্যক্তিবর্গের পরিচয় দিনঃ"%>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div id="affiliated-persons-add">

                                                        <div class="form-group row" id="add_person_div">
                                                            <label class="col-3 col-form-label text-right"
                                                                   id="gatePassSubTypeIdLabel">
                                                                <%=Language.equalsIgnoreCase("English") ? "Number of persons" : "কতজন প্রবেশ করবেন"%>
                                                            </label>
                                                            <div class="col-9" style="margin-bottom: 2%">
                                                                <div class="row">
                                                                    <div class="col-lg-7">
                                                                        <input type='number'
                                                                               class='form-control'
                                                                               name='additional_person'
                                                                               id='additional_person'
                                                                               min="0" value="0" tag='pb_html'/>
                                                                    </div>
                                                                    <div class="col-lg-5 text-right">
                                                                        <button class="btn btn-info rounded form-control"
                                                                                type="button"
                                                                                id="additional_person_button">

                                                                            <%=Language.equalsIgnoreCase("English") ? "GIVE THEIR INFO" : "তাদের তথ্য দিন"%>

                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <input type='hidden' class='form-control'
                                                               name='additionalPersonCount'
                                                               id='additionalPersonCount'
                                                               value="0" tag='pb_html'/>

                                                    </div>


                                                    <div class="form-group row mb-0" id="add-item-div-lebel">
                                                        <div class="col-12 col-form-label">
                                                            <span class="h5">
                                                                <%=Language.equalsIgnoreCase("English") ? "Select items you want to carry with" : "আপনি কি কি আইটেম সাথে নিয়ে সংসদ ভবনে প্রবেশ করতে চানঃ"%>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div id="add-item-div">
                                                        <div class="form-group row mb-0">
                                                            <div class="col-12 ">
                                                                <div id="parliament-items-add">
                                                                    <table id="parliament-items-tableData"
                                                                           class="table table-bordered table-striped table-full-width">
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="width: 30%; color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "Item" : "বস্তু"%>
                                                                            </th>
                                                                            <th style="width: 20%; color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "Amount" : "পরিমাণ"%>
                                                                            </th>
                                                                            <th style="width: 50%; color:#0a6aa1"><%=Language.equalsIgnoreCase("English") ? "Description" : "বিবরণ"%>
                                                                            </th>
                                                                        </tr>
                                                                        </thead>

                                                                        <tbody>
                                                                        <tr id="gate_pass_item_1">
                                                                            <td>
                                                                                <select class='form-control'
                                                                                        name='parliamentItemId_1'
                                                                                        id='parliamentItemId_1'
                                                                                        tag='pb_html'>
                                                                                    <%=Parliament_itemRepository.getInstance().buildOptions(Language, 0)%>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input type='number'
                                                                                       class='form-control'
                                                                                       name='amount_1'
                                                                                       id='amount_1' value="1"
                                                                                       min="1"
                                                                                       tag='pb_html'>
                                                                            </td>
                                                                            <td>
                                                                                <input type='text'
                                                                                       class='form-control'
                                                                                       name='description_1'
                                                                                       id='description_1'
                                                                                       tag='pb_html'/>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <input type='hidden' class='form-control'
                                                                           name='totalItemCount' id='totalItemCount'
                                                                           value="1" tag='pb_html'/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-12 form-actions text-right mt-0">
                                                                <button class="btn btn-info rounded"
                                                                        type="button" id="add_more_item_button">
                                                                    <%=Language.equalsIgnoreCase("English") ? "Add More Item" : "আরো বস্তু যোগ করুন"%>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <input type='hidden' class='form-control' name='addNewAddress'
                                                           id='addNewAddress'
                                                           value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='insertionDate'
                                                           id='insertionDate_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='insertedBy'
                                                           id='insertedBy_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.insertedBy + "'"):("'" + "" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='modifiedBy'
                                                           id='modifiedBy_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='isDeleted'
                                                           id='isDeleted_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                                                   tag='pb_html'/>
                                                    <input type='hidden' class='form-control'
                                                           name='lastModificationTime'
                                                           id='lastModificationTime_hidden_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + gate_pass_visitorDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                                   tag='pb_html'/>
                                                    <div class="form-group row">

                                                        <div class="col-6 text-left">
                                                            <div class="">
                                                                <div class="d-flex justify-content-start align-items-center">
                                                                    <div class="">
                                                                        <img
                                                                                class="img-thumbnail"
                                                                                id="captcha" src="<%=request.getContextPath()%>/simpleCaptchaServlet"
                                                                                alt="loading captcha..."
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex justify-content-start align-items-center mt-2">
                                                                    <div id='captcha_input_div_0'>
                                                                        <input
                                                                                type='text'
                                                                                id='answer_id'
                                                                                class='form-control inputBackground preventEnter'
                                                                                name='captchaInput'
                                                                                id='captcha_input_text_0'
                                                                                placeholder='<%=Language.equalsIgnoreCase("English") ? "Write Captcha" : "ক্যাপচা লিখুন"%>'
                                                                                tag='pb_html'
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-2 text-center">
                                                            <img
                                                                    src="<%=request.getContextPath()%>/assets/images/refresh.png"
                                                                    width="40"
                                                                    height="40"
                                                                    title="Refresh Captcha"
                                                                    alt="refresh captcha"
                                                                    id="reloadCaptcha"
                                                                    onclick="getcapcha()"
                                                                    class="img-thumbnail"
                                                            />
                                                        </div>

                                                        <div class="col-4 form-actions text-right">
                                                            <button class="btn btn-success rounded" type="submit">
                                                                <%=Language.equalsIgnoreCase("English") ? "SUBMIT" : "জমা দিন"%>
                                                            </button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    let dataLoaded = false;
    const language = '<%=Language%>';
    let error1, error2, error3;
    if (language == 'English') {
        error1 = 'select credential type!';
        error2 = 'enter credential number!';
        error3 = 'enter 11 digit phone number!';
        error4 = 'enter your full name!';
    } else {
        error1 = 'পরিচয়পত্রের ধরণ বাছাই করুন!';
        error2 = 'পরিচয়পত্রের নম্বর প্রদান করুন!';
        error3 = '১১ সংখ্যার ফোন নম্বর প্রদান করুন!';
        error4 = 'আপনার পুরো নাম প্রবেশ করুন!';
    }
    $('#visitor-form').on('submit', () => {
        let valid = true;
        $('.credential-type').each((index, element) => {
            if (!$(element).val().match('[0-9]+')) {
                $(element).closest('.affliatedPerson').find('.affiliated-type-error').show();
                $(element).closest('.affliatedPerson').find('.affiliated-type-error').text(error1);
                valid = false;
            }
        });
        $('.credential-no').each((index, element) => {
            if ($(element).val() == "") {
                $(element).closest('.affliatedPerson').find('.affiliated-id-error').show();
                $(element).closest('.affliatedPerson').find('.affiliated-id-error').text(error2);
                valid = false;
            }
        });
        $('.name').each((index, element) => {
            if ($(element).val() == "") {
                $(element).closest('.affliatedPerson').find('.name-error').show();
                $(element).closest('.affliatedPerson').find('.name-error').text(error4);
                valid = false;
            }
        });
        $('.mobile-number').each((index, element) => {
            if (!$(element).val().match('^01[0-9]{9}$') && !$(element).val().match('^০১[০-৯]{9}$')) {
                $(element).closest('.affliatedPerson').find('.affiliated-mobile-error').show();
                $(element).closest('.affliatedPerson').find('.affiliated-mobile-error').text(error3);
                valid = false;
            }
        });

        return valid;
    });

    $(document).ready(function () {

        $('#checkNewAddress').prop('checked', true);

        initGeoLocation('permanentAddress_geoSelectField_', 0, "Gate_passPublicServlet");
        initGeoLocation('currentAddress_geoSelectField_', 0, "Gate_passPublicServlet");

        const currentAddressHTML = $('#currentAddress_div').html();
        let lastPersonCount = 0;
        let currentItemCount = 1;

        $('#additional_person_button').click(() => {
            const currentPersonCount = $('#additional_person').val();
            if (!isNaN(parseInt(currentPersonCount))) {
                for (let i = lastPersonCount - 1; i >= currentPersonCount; i--) {
                    $('#affliatedPerson_div_' + i).remove();
                }
                if (lastPersonCount < currentPersonCount) {
                    let url = "Gate_passPublicServlet?actionType=getAffiliatedPersonAddPage" + "&language=<%=Language%>&personCount="
                        + currentPersonCount + "&startVal=" + lastPersonCount;
                    // console.log("office_unit ajax url : " + url);
                    $.ajax({
                        url: url,
                        type: "GET",
                        async: true,
                        success: function (data) {

                            $('#add_person_div').append(data);
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                lastPersonCount = currentPersonCount;
                $('#additionalPersonCount').val(lastPersonCount);
            }
        });

        $('#add_more_item_button').click(() => {
            currentItemCount++;
            let url = "Gate_passPublicServlet?actionType=getItemAddPage" + "&language=<%=Language%>&rowNumber="
                + currentItemCount;
            // console.log("office_unit ajax url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                success: function (data) {

                    $('#parliament-items-tableData > tbody').append(data);
                },
                error: function (error) {
                    console.log(error);
                }
            });
            $('#totalItemCount').val(currentItemCount);
        });

        // $('#add_person').click(() => {
        //     $('#add_person_div').show();
        //     $('#add_person').hide();
        //     // alert( "Handler for .click() called." );
        // });


        // validation on input
        $.validator.addMethod('isValidMobileNumber', function (value, element) {
            let english = /^[A-Za-z0-9]*$/;
            let mobileNo = $('#mobileNumber').val();
            if (!english.test(mobileNo)) {
                return value.match('[\u09E6-\u09EF]');
            } else {
                return value.match('^01[3-9]{1}[0-9]{8}$');
            }
        });

        $.validator.addMethod('genderSelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('officerSelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('credentialSelection', function (value, element) {
            return value != 0;
        });

        $("#visitor-form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                name: "required",
                fatherName: "required",
                motherName: "required",
                credentialNo: "required",
                genderType: {
                    required: true,
                    genderSelection: true
                },
                officerType: {
                    required: true,
                    officerSelection: true
                },
                credentialType: {
                    required: true,
                    credentialSelection: true
                },
                mobileNumber: {
                    required: true,
                    isValidMobileNumber: true
                }
                // permanentAddress_active: {
                //     required: true,
                //     permanentAddress_activeSelection: true
                // },
                // currentAddress_active: {
                //     required: true,
                //     currentAddress_activeSelection: true
                // }
            },
            messages: {
                name: '<%=Language.equalsIgnoreCase("English") ? "enter visitor`s name!" : "সাক্ষাৎ প্রার্থীর নাম প্রবেশ করান!"%>',
                fatherName: '<%=Language.equalsIgnoreCase("English") ? "enter visitor`s father`s name!" : "সাক্ষাৎ প্রার্থীর পিতার নাম প্রবেশ করান!"%>',
                motherName: '<%=Language.equalsIgnoreCase("English") ? "enter visitor`s mother`s name!" : "সাক্ষাৎ প্রার্থীর মাতার নাম প্রবেশ করান!"%>',
                credentialNo: '<%=Language.equalsIgnoreCase("English") ? "enter credential number!" : "পরিচয়পত্রের নাম্বার প্রবেশ করান!"%>',
                genderType: '<%=Language.equalsIgnoreCase("English") ? "select gender!" : "লিঙ্গ বাছাই করুন!"%>',
                officerType: '<%=Language.equalsIgnoreCase("English") ? "select officer type!" : "কর্মকর্তার ধরন বাছাই করুন!"%>',
                credentialType: '<%=Language.equalsIgnoreCase("English") ? "select credential type!" : "পরিচয়পত্রের ধরন বাছাই করুন!"%>',
                mobileNumber: '<%=Language.equalsIgnoreCase("English") ? "mobile number must be 11 digit, ex: 01710101010" : "মোবাইল নাম্বারটি অবশ্যই ১১ ডিজিটের হতে হবে, উদাহরণঃ 01710101010"%>'
                <%--permanentAddress_active: '<%=Language.equalsIgnoreCase("English") ? "select permanent address!" : "স্থায়ী ঠিকানা বাছাই করুন!"%>',--%>
                <%--currentAddress_active: '<%=Language.equalsIgnoreCase("English") ? "select current/official address!" : "বর্তমান/দাপ্তরিক ঠিকানা বাছাই করুন!"%>'--%>
            }
        });


        // checkbox for permanent and current/official address

        $('#checkCurrentAddress').click(function () {

            if ($('#checkCurrentAddress').prop('checked')) {
                $('#currentAddress_div').hide();
            } else {
                $('#currentAddress_div').html(currentAddressHTML);
                initGeoLocation('currentAddress_geoSelectField_', 0, "Gate_passPublicServlet");
                $('#currentAddress_div').show();
            }
        });

        $('#checkNewAddress').click(function () {

            if ($('#checkNewAddress').prop('checked')) {
                $('#address_without_data').show();
                $('#address_with_data').hide();
            } else {
                $('#address_without_data').hide();
                $('#address_with_data').show();
            }
        });


        const gate_pass_type_id = '<%=gate_passDTO.gatePassTypeId%>';
        if (gate_pass_type_id === '1') {
            $('#affiliated-persons-add').show();
            $('#add-item-div').hide();
            $('#affiliated-persons-add-lebel').show();
            $('#add-item-div-lebel').hide();
        } else if (gate_pass_type_id === '10') {
            $('#affiliated-persons-add').hide();
            $('#add-item-div').show();
            $('#affiliated-persons-add-lebel').hide();
            $('#add-item-div-lebel').show();
        } else if (gate_pass_type_id === '11') {
            $('#affiliated-persons-add').hide();
            $('#add-item-div').hide();
            $('#affiliated-persons-add-lebel').hide();
            $('#add-item-div-lebel').hide();
        } else {
            $('#affiliated-persons-add').show();
            $('#add-item-div').show();
            $('#affiliated-persons-add-lebel').show();
            $('#add-item-div-lebel').show();
        }


        function populateInputFields() {
            let mobileNo = $('#mobileNumber').val();
            let credentialNo = $('#credentialNo').val();
            let credentialType = $('#credentialType').val();

            if (mobileNo.length === 11 && credentialType &&
                ((credentialType == '1' && (credentialNo.length == 10 || credentialNo.length == 13)) ||
                    (credentialType == '2' && credentialNo.length == 9) ||
                    (credentialType == '3' && credentialNo.length == 17))) {

                mobileNo = '88' + mobileNo;
                console.log(mobileNo);
                // ajax call for stored data
                let url = "Gate_passPublicServlet?actionType=getStoredDataByMobileNumber" + "&mobileNumber=" + mobileNo
                    + "&credentialNo=" + credentialNo;

                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    success: function (data) {

                        if (data.localeCompare('noString') === -1) {

                            let dto = JSON.parse(data);
                            // $('#isPreviousData').val('true');
                            // Now populate data with stored data
                            PopulateData(dto);
                            dataLoaded = true;
                        } else if (data.localeCompare('noString') === 1) {
                            // $('#isPreviousData').val('false');
                            // Now unpopulate data with stored data
                            if (dataLoaded) {
                                UnPopulateData();
                                dataLoaded = false;
                            }
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        }


        $("#mobileNumber").on("input", () => {
            let mobileNo = $('#mobileNumber').val();
            if (mobileNo.length > 11) {
                mobileNo = mobileNo.substr(0, 11);
                $('#mobileNumber').val(mobileNo);
            }
            const english = /^[A-Za-z0-9]*$/;
            if (!english.test(mobileNo)) {
                mobileNo = convertToEnglishNumber(mobileNo);
                $('#mobileNumber').val(mobileNo);
            }
            populateInputFields();
        });

        $("#credentialNo").on("input", () => {
            populateInputFields();
        });

        $("#credentialType").on("change", () => {
            populateInputFields();
        });

    });


    function PopulateData(dto) {
        $('#addressSelector').show();
        $('#checkNewAddress').prop('checked', false);

        $('#name').val(dto.name);
        // $('#name').prop("readonly", true);

        $('#fatherName').val(dto.fatherName);
        // $('#fatherName').prop("readonly", true);

        $('#motherName').val(dto.motherName);
        // $('#motherName').prop("readonly", true);

        $('#email').val(dto.email);
        // $('#email').prop("readonly", true);

        // $('#credentialNo').val(dto.credentialNo);
        // $('#credentialNo').prop("readonly", true);

        // $('#credentialType').val(dto.credentialType);
        // $('#credentialType').attr("disabled", true);

        $('#genderType_select_0').val(dto.genderType);
        // $('#genderType_select_0').attr("disabled", true);

        $('#officerType_select_0').val(dto.officerType);
        // $('#officerType_select_0').attr("disabled", true);

        $('#address_without_data').hide();

        <%
            if(Language.equalsIgnoreCase("English")) {
        %>

        document.getElementById("stored_permanent_address").textContent = parseEnglishAddress(dto.permanentAddress);
        document.getElementById("stored_current_address").textContent = parseEnglishAddress(dto.currentAddress);
        $('#address_with_data').show();
        <%
            } else {
        %>
        document.getElementById("stored_permanent_address").textContent = parseBanglaAddress(dto.permanentAddress);
        document.getElementById("stored_current_address").textContent = parseBanglaAddress(dto.currentAddress);
        $('#address_with_data').show();
        <%
            }
        %>
        let currentAddr = dto.currentAddress.split(':')[0]+'$'+dto.currentAddress.split(':')[2];
        let permanentAddr = dto.permanentAddress.split(':')[0]+'$'+dto.permanentAddress.split(':')[2];
        $('#currentAddress_geolocation_0').val(currentAddr);
        $('#permanentAddress_geolocation_0').val(permanentAddr);
    }

    function UnPopulateData() {
        // Now unpopulate data with stored data
        $('#addressSelector').hide();
        $('#checkNewAddress').prop('checked', true);

        $('#name').val('');
        // $('#name').prop("readonly", false);

        $('#fatherName').val('');
        // $('#fatherName').prop("readonly", false);

        $('#motherName').val('');
        // $('#motherName').prop("readonly", false);

        $('#email').val('');
        // $('#email').prop("readonly", false);

        // $('#credentialNo').val('');
        // $('#credentialNo').prop("readonly", false);

        // $('#credentialType').prop('selectedIndex', 0);
        // $('#credentialType').attr("disabled", false);

        $('#genderType_select_0').prop('selectedIndex', 0);
        // $('#genderType_select_0').attr("disabled", false);

        $('#officerType_select_0').prop('selectedIndex', 0);
        // $('#officerType_select_0').attr("disabled", false);

        $('#address_without_data').show();

        $('#address_with_data').hide();
    }

    function PreprocessBeforeSubmiting(row) {
        if($('#checkNewAddress').prop('checked')) {
            preprocessGeolocationBeforeSubmitting('permanentAddress', row, false);

            if ($('#checkCurrentAddress').prop('checked')) {
                $('#currentAddress_geolocation_0').val($('#permanentAddress_geolocation_0').val());
            } else {
                preprocessGeolocationBeforeSubmitting('currentAddress', row, false);
            }
        }
        console.log($('#currentAddress_geolocation_0').val());
        let noSamePass = true;
        const mobileNo = '88' + $('#mobileNumber').val();
        const credentialNo = $('#credentialNo').val();

        let url = "Gate_passPublicServlet?actionType=samePassCheck" + "&mobileNumber=" + mobileNo
            + "&credentialNo=" + credentialNo + "&visitingDate=<%=gate_passDTO.visitingDate%>"
            + "&gatePassType=<%=gate_passDTO.gatePassTypeId%>&gatePassSubType=<%=gate_passDTO.gatePassSubTypeId%>";

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                console.log(data);
                if (data.localeCompare('foundSamePass') === 0) {
                    noSamePass = false;
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: '<%=Language.equalsIgnoreCase("English") ? "A gate pass application has already been listed for you in this date!" : "পূর্ব হতেই আপনার আজকের দিনের প্রবেশ পাসের আবেদন করা আছে!"%>',
                        showCancelButton: false,
                        confirmButtonText: '<%=Language.equalsIgnoreCase("English") ? "OK" : "ঠিক আছে"%>',
                        allowOutsideClick: false
                    });
                }
            },
            error: function (error) {
                console.log(error);
            }
        });


        let isCorrectCaptcha = false;
        if ($("#visitor-form").valid()) {
            url = "Gate_passPublicServlet?actionType=checkCaptcha&answerString=" + $('#answer_id').val();

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (data) {
                    if (data.localeCompare('true') === 0) {
                        isCorrectCaptcha = true;
                    } else {
                        isCorrectCaptcha = false;
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: '<%=Language.equalsIgnoreCase("English") ? "Please, enter the correct CAPTCHA!" : "অনুগ্রহ করে সঠিক ক্যাপচা দিন!"%>',
                            showCancelButton: false,
                            confirmButtonText: '<%=Language.equalsIgnoreCase("English") ? "OK" : "ঠিক আছে"%>',
                            allowOutsideClick: false
                        });
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        return noSamePass && isCorrectCaptcha;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Gate_passPublicServlet");
    }

    var child_table_extra_id = <%=childTableStartingID%>;


    function parseEnglishAddress(address) {
        let addresses = address.split(":");

        return addresses[2] + ", " + addresses[1];
    }

    function parseBanglaAddress(address) {
        let addresses = address.split(":");

        return addresses[2] + ", " + addresses[3];
    }


    $(".englishDigitOrCharOnly").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        var charStr = String.fromCharCode(ew);

        if (charStr == "0") {
            return true;
        } else if (charStr == "1") {
            return true;
        } else if (charStr == "2") {
            return true;
        } else if (charStr == "3") {
            return true;
        } else if (charStr == "4") {
            return true;
        } else if (charStr == "5") {
            return true;
        } else if (charStr == "6") {
            return true;
        } else if (charStr == "7") {
            return true;
        } else if (charStr == "8") {
            return true;
        } else if (charStr == "9") {
            return true;
        } else if (charStr >= 'A' && charStr <= 'Z'){
            return true;
        } else if (charStr >= 'a' && charStr <= 'z'){
            return true;
        }
        return false;
    });


    function convertToEnglishNumber(input) {
        let numbers = {
            '০': 0,
            '১': 1,
            '২': 2,
            '৩': 3,
            '৪': 4,
            '৫': 5,
            '৬': 6,
            '৭': 7,
            '৮': 8,
            '৯': 9
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    function getcapcha()
    {
        var date2 = Math.floor(Math.random() * 100) + 1;

        var img = document.getElementById("captcha");

        img.src= "<%=request.getContextPath()%>/simpleCaptchaServlet?test="+ date2;
    }

</script>