<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="gate_pass.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@page import="util.StringUtils" %>
<%@ page import="pb.Utils" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="java.util.Date" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Gate_passDAO gate_passDAO = new Gate_passDAO("gate_pass");

    GatePassModel gatePassModel = gate_passDAO.getGatePassModelById(id);

    String viewPageTitle = Language.equalsIgnoreCase("English") ? gatePassModel.gatePassTypeEng : gatePassModel.gatePassTypeBng;
    if (Language.equalsIgnoreCase("English")) {
        viewPageTitle = viewPageTitle.substring(0, 1).toUpperCase() + viewPageTitle.substring(1);
    }
    int i = 0, count = 1;

    String context = request.getContextPath() + "/";
%>

<style>
    .font-color {
        color: #00a1d4 !important;
    }

    .signature-image {
        border-bottom: 2px solid #a406dc !important;
        width: 200px !important;
        height: 53px !important;
    }

    .signature-div {
        font-size: .95rem;
        color: #a406dc;
    }

    .table th, .table td, .table thead th {
        border: 1px solid black;
    }

    table, tr, th, td {
        border: 1px solid black;
    }

    @media print {
        th, td, .kt-content, .kt-portlet__body, .kt-portlet__head {
            font-size: 1.5rem !important;
        }

        .button_process, .button_approve, .button_dismiss {
            display: none !important;
        }

        .fa-plus-circle, .fa-minus-circle {
            display: none;
        }
    }

</style>

<!-- begin:: Subheader -->
<div class="ml-auto mr-3 mt-4">

    <button type="button" class="btn" id='print-btn'>
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="" id="modalbody">
        <div class="kt-portlet" style="background-color: #f2f2f2!important;">
            <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
                <div class="row">
                    <div class="col-12 text-center">
                        <img
                                width="110"
                                src="<%=context%>assets/static/parliament_logo.png"
                                alt="logo"
                                class="logo-default"
                        />
                        <h2 class=" font-color mt-3">
                            বাংলাদেশ জাতীয় সংসদ সচিবালয়
                        </h2>
                        <h4 class="font-color mt-2">
                            শেরেবাংলা নগর, ঢাকা-১২০৭
                        </h4>
                        <h5 class="font-color mt-2">
                            <%=gatePassModel.gatePassTypeBng%> এর জন্য অধিযাচন ফরম
                        </h5>
                    </div>
                </div>
                <div class="mt-5">
                    <lable>
                        নিম্নোক্ত ব্যক্তি/ব্যক্তিবর্গকে অদ্য <%=gatePassModel.visitingDateBng%> ইং
                        তারিখ <%=gatePassModel.gatePassTypeBng%>
                        ইস্যুর জন্য
                        অনুরােধ করছি :
                    </lable>
                    <table class="table table-striped mt-2">
                        <tr>
                            <th class="font-color">
                                <b>
                                    <%=Language == "English" ? "Serial No." : "ক্রমিক নং"%>
                                </b>
                            </th>
                            <th class="font-color">
                                <b><%=Language == "English" ? "Person/Persons Pass Issued For" : "যাদের/যাহাদের জন্য পাস ইস্যু করা হবে"%>
                                </b>
                            </th>
                            <th class="font-color">
                                <b><%=Language.equals("English") ? "Full Address" : "সম্পূর্ণ ঠিকানা"%>
                                </b>
                            </th>
                        </tr>
                        <tr>
                            <td><%=StringUtils.convertToBanNumber(Integer.toString(count++))%>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_NAME, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassModel.visitorName%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">
                                        <b><%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_FATHER_NAME, loginDTO)%>
                                        :</b></div>
                                    <div class="col-7"><%=gatePassModel.visitorFatherName%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_CREDENTIAL, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.visitorCredentialTypeEng + "-" + Utils.getDigits(gatePassModel.visitorCredentialNo, "English") :
                                            gatePassModel.visitorCredentialTypeBng + " - " + Utils.getDigits(gatePassModel.visitorCredentialNo, "Bangla")%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_MOBILE, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.visitorMobileNumberEng : gatePassModel.visitorMobileNumberBng%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassModel.designation%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassModel.officeName%>
                                    </div>
                                </div>
                                <%
                                if(gatePassModel.hasVehicle)
                                {
                                	%>
                                <div class="row">
                                    <div class="col-5"><b><%=Language.equalsIgnoreCase("English") ?"Vehicle Details":"যানবাহনের বিবরণ"%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassModel.vehicleDescription%>
                                    </div>
                                </div>
                                
                                 <div class="row">
                                    <div class="col-5"><b><%=Language.equalsIgnoreCase("English") ?"Vehicle Number":"যানবাহনের নম্বর"%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassModel.vehicleNumber%>
                                    </div>
                                </div>
                                	<%
                                }
                                %>
                                

                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_VILLAGE, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassModel.currentAddress.addressText%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UNION, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.unionEng : gatePassModel.currentAddress.unionBng%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">
                                        <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UPOZILLA, loginDTO)%>
                                        :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.thanaEng : gatePassModel.currentAddress.thanaBng%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">
                                        <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DISTRICT, loginDTO)%>
                                        :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.districtEng : gatePassModel.currentAddress.districtBng%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <%
                            for (GatePassPersonModel gatePassPersonModel : gatePassModel.persons) {
                        %>
                        <tr>
                            <td><%=StringUtils.convertToBanNumber(Integer.toString(count++))%>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-5">
                                        <b><%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_NAME, loginDTO)%>
                                        :</b></div>
                                    <div class="col-7"><%=gatePassPersonModel.name%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_MOBILE, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassPersonModel.mobileNoEng : gatePassPersonModel.mobileNoBng%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VIEW_CREDENTIAL, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassPersonModel.credentialTypeEng + "-" + Utils.getDigits(gatePassPersonModel.credentialNo, "English") :
                                            gatePassPersonModel.credentialTypeBng + " - " + Utils.getDigits(gatePassPersonModel.credentialNo, "Bangla")%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassPersonModel.designation%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassPersonModel.officeName%>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_VILLAGE, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=gatePassModel.currentAddress.addressText%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5"><b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UNION, loginDTO)%>
                                    :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.unionEng : gatePassModel.currentAddress.unionBng%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">
                                        <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UPOZILLA, loginDTO)%>
                                        :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.thanaEng : gatePassModel.currentAddress.thanaBng%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5">
                                        <b><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DISTRICT, loginDTO)%>
                                        :</b></div>
                                    <div class="col-7"><%=Language.equalsIgnoreCase("English") ? gatePassModel.currentAddress.districtEng : gatePassModel.currentAddress.districtBng%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                    </table>
                </div>
                <%
                    if (gatePassModel.items.size() > 0) {
                %>
                <div class="mt-5">
                    <lable>
                        <%=Language.equalsIgnoreCase("English") ? "What visitor will bring with him:" : "সাথে যা যা নিতে পারবেনঃ"%>
                    </lable>
                    <table class="table text-center mt-2">
                        <thead>
                        <tr>
                            <th class="font-color"><%=Language.equalsIgnoreCase("English") ? "ITEM" : "আইটেম"%>
                            </th>
                            <th class="font-color"><%=Language.equalsIgnoreCase("English") ? "AMOUNT" : "পরিমান"%>
                            </th>
                            <th class="font-color"><%=Language.equalsIgnoreCase("English") ? "DESCRIPTION" : "বিবরণ"%>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            for (GatePassItemModel gatePassItemModel : gatePassModel.items) {
                        %>
                        <tr>
                            <td style="width: 25%"><%=Language.equalsIgnoreCase("English") ? gatePassItemModel.nameEng : gatePassItemModel.nameBng%>
                            </td>


                            <td style="width: 50%">
								
                                <div class="inline-block">
                                <%
								if(gatePassModel.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending)
								{
								%>
                                    <span onclick="counting('plus', '<%=gatePassItemModel.id%>_counter')"
                                          id="<%=gatePassItemModel.id%>_plus"><i class="fa fa-plus-circle"
                                                                                 style="color:green"></i></span>
                                <%
								}
                                %>
                                    <span id="<%=gatePassItemModel.id%>_counter"><%=Language.equalsIgnoreCase("English") ? gatePassItemModel.approvedAmount : StringUtils.convertToBanNumber(String.valueOf(gatePassItemModel.approvedAmount))%></span>
                                <%
								if(gatePassModel.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending)
								{
								%>    
                                    <span onclick="counting('minus', '<%=gatePassItemModel.id%>_counter')"
                                          id="<%=gatePassItemModel.id%>_minus"><i class="fa fa-minus-circle"
                                                                                  style="color:red"></i></span>
                                <%
								}
                                %>                                                
                                </div>
                               

                            </td>


                            <td style="width: 25%"><%=gatePassItemModel.description%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                    </table>
                </div>
                <%
                    }
                %>

                <%
                    if (gatePassModel.gatePassType == 10) {
                %>
                <div class="row mt-4">
                    <div class="col-lg-6">
                        <b><%=LM.getText(LC.GATE_PASS_PARLIAMENT_BUILDING_REASON_FOR_VISIT, loginDTO)%>
                            : </b><%=gatePassModel.reasonForVisit%>
                    </div>
                </div>
                <%
                    }
                %>

                <%--Signature Div--%>
                <div class="my-5">
                    <h6 style="color: #0098bf !important; font-weight: bold">বর্ণিত ব্যক্তি/ব্যক্তিবর্গ আমার
                        পরিচিত।</h6>
                </div>
                <div class="row mt-2">
                    <div class="col-4 text-center">
                        <div class="signature-div d-flex justify-content-start">
                            <div>
                                <div>
                                    <img class="signature-image"
                                         src='<%=StringUtils.getBase64EncodedImageStr(gatePassModel.referrerSignature)%>'/>
                                    <br>
                                    <%=StringUtils.getFormattedTime(false, gatePassModel.firstLayerApprovalTime)%>
                                </div>
                                <div>
                                    <%=gatePassModel.referrerBng%><br>
                                    <%=gatePassModel.referrerOfficeOrgBng%><br>
                                    <%=gatePassModel.referrerOfficeBng%><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 text-center if-approved-by-sergeant" style="display: none">
                        <div class="signature-div">
                            <img
                                    width="200"
                                    style="object-fit: contain; transform: rotate(10.2deg)"
                                    src="<%=context%>assets/static/approval_seal.png"
                                    alt="logo"
                                    class="logo-default"
                            />
                        </div>
                    </div>
                    <div class="col-4 text-center if-approved-by-sergeant" style="display: none">
                        <div class="signature-div">
                            <div>
                                <div>
                                    <img class="signature-image"
                                         src='<%=StringUtils.getBase64EncodedImageStr(gatePassModel.sergeantSignature)%>'/>
                                    <br>
                                    <%=StringUtils.getFormattedTime(false, gatePassModel.secondLayerApprovalTime)%>
                                </div>
                                <div>
                                    <%=gatePassModel.sergeantNameBng%><br>
                                    <%=gatePassModel.sergeantOfficeOrgBng%><br>
                                    <%=gatePassModel.sergeantOfficeBng%><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-5 row btn-section no-print">
                    <div class="col-12 text-right">
                        <%
                            if (gatePassModel.partiallyIssued) {
                        %>
                        <%
                        } else if (gatePassModel.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
                        %>
                        <button type="button" id='<%=i%>_button_dismiss'
                                class="btn btn-danger shadow btn-border-radius btn-sm button_dismiss"
                                style="border-radius: 8px;"
                                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=id%>', '<%=gatePassModel.secondLayerApprovalStatus%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
                        </button>
                        <button type="button" id='<%=i%>_button_approve'
                                class="btn btn-success shadow btn-border-radius btn-sm mx-2 button_approve"
                                style="border-radius: 8px;"
                                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=id%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
                        </button>
                        <button type="button" id='<%=i%>_button_process'
                                class="btn btn-primary shadow btn-border-radius btn-sm button_process"
                                style="border-radius: 8px;"
                                onclick="processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=id%>', '<%=gatePassModel.secondLayerApprovalStatus%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
                        </button>
                        <%
                        } else if (gatePassModel.secondLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
                        %>
                        <button type="button" id='<%=i%>_button_dismiss'
                                class="btn btn-danger shadow btn-border-radius btn-sm button_dismiss"
                                style="border-radius: 8px;"
                                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=id%>', '<%=gatePassModel.secondLayerApprovalStatus%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
                        </button>
                        <button type="button" id='<%=i%>_button_approve'
                                class="btn btn-success shadow btn-border-radius btn-sm mx-2 button_approve"
                                style="border-radius: 8px;"
                                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=id%>', '<%=gatePassModel.gatePassTypeEng%>')"
                                disabled>
                            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
                        </button>
                        <button type="button" id='<%=i%>_button_process'
                                class="btn btn-primary shadow btn-border-radius btn-sm button_process"
                                style="border-radius: 8px;"
                                onclick="processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=id%>', '<%=gatePassModel.secondLayerApprovalStatus%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
                        </button>
                        <%
                        } else if (gatePassModel.secondLayerApprovalStatus == Gate_passDAO.approvalStatusProcessing) {
                        %>
                        <button type="button" id='<%=i%>_button_dismiss'
                                class="btn btn-danger shadow show-approval-notification btn-border-radius btn-sm button_dismiss"
                                style="border-radius: 8px;"
                                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=id%>', '<%=gatePassModel.secondLayerApprovalStatus%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
                        </button>
                        <button type="button" id='<%=i%>_button_approve'
                                class="btn btn-success shadow mx-2 btn-border-radius btn-sm button_approve"
                                style="border-radius: 8px;"
                                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=id%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
                        </button>
                        <button type="button" id='<%=i%>_button_process'
                                class="btn btn-primary shadow btn-border-radius btn-sm button_process"
                                style="border-radius: 8px;"
                                onclick="processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=id%>', '<%=gatePassModel.secondLayerApprovalStatus%>', '<%=gatePassModel.gatePassTypeEng%>')"
                                disabled>
                            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
                        </button>
                        <%
                        } else {
                        %>
                        <button type="button" id='<%=i%>_button_dismiss'
                                class="btn btn-danger shadow show-approval-notification btn-border-radius btn-sm button_dismiss"
                                style="border-radius: 8px;"
                                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=id%>', '<%=gatePassModel.secondLayerApprovalStatus%>', '<%=gatePassModel.gatePassTypeEng%>')"
                                disabled>
                            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
                        </button>
                        <button type="button" id='<%=i%>_button_approve'
                                class="btn btn-success shadow mx-2 btn-border-radius btn-sm button_approve"
                                style="border-radius: 8px;"
                                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=id%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
                        </button>
                        <button type="button" id='<%=i%>_button_process'
                                class="btn btn-primary shadow btn-border-radius btn-sm button_process"
                                style="border-radius: 8px;"
                                onclick="processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=id%>', '<%=gatePassModel.secondLayerApprovalStatus%>', '<%=gatePassModel.gatePassTypeEng%>')">
                            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
                        </button>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="select-gallery-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"
                    style="font-size: large; font-weight: bold"><%=Language.equalsIgnoreCase("English") ?
                        "SELECT GALLERY" : "গ্যালারি নির্বাচন করুন"%>
                </h5>
            </div>
            <div class="modal-body">
                <form id="select-gallery-form" method="post">

                    <div class="form-body">
                        <div class="form-group row">
                            <label class="control-label col-md-2"
                                   style="font-weight: bold"><%=Language.equalsIgnoreCase("English") ? "Gallery" : "গ্যালারি"%>
                            </label>
                            <div class="col-md-10" id="gallery-list">
                                <select class='form-control' name='parliament-gallery' onchange="populateGalleryModal()"
                                        id='parliament-gallery' tag='pb_html'>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class=" form-group row">
                                    <label class="control-label col-md-6"
                                           style="font-weight: bold"><%=Language.equalsIgnoreCase("English") ? "Total Seat" : "মোট সিট"%>
                                    </label>
                                    <div class="col-md-6" id='total-seat-div'>
                                        <span id="total-seat">

                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-6 control-label"
                                           style="font-weight: bold"><%=Language.equalsIgnoreCase("English") ? "Available Seat" : "ফাকা সিট"%>
                                    </label>
                                    <div class="col-md-6" id='available-seat-div'>
                                        <span id="available-seat">

                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--                        <label class="form-group control-label"><%=Language.equalsIgnoreCase("English") ? "Gallery" : "গ্যালারি"%>--%>
                        <%--                        </label>--%>
                        <%--                        <div class="form-group ">--%>
                        <%--                            <div class="" id="gallery-list">--%>
                        <%--                                <select class='form-control' name='parliament-gallery' onchange="populateGalleryModal()" id='parliament-gallery' tag='pb_html'>--%>
                        <%--                                </select>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>


                        <%--                        <label class="form-group control-label"><%=Language.equalsIgnoreCase("English") ? "Total Seat" : "মোট সিট"%>--%>
                        <%--                        </label>--%>
                        <%--                        <div class="form-group ">--%>
                        <%--                            <div class="" id='total-seat-div'>--%>
                        <%--                                <span id="total-seat">--%>

                        <%--                                </span>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>


                        <%--                        <label class="form-group control-label"><%=Language.equalsIgnoreCase("English") ? "Available Seat" : "ফাকা সিট"%>--%>
                        <%--                        </label>--%>
                        <%--                        <div class="form-group ">--%>
                        <%--                            <div class="" id='available-seat-div'>--%>
                        <%--                                <span id="available-seat">--%>

                        <%--                                </span>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="modal_dismiss"
                        onclick="modalDismiss()"><%=Language.equalsIgnoreCase("English") ? "CANCEL" : "বাতিল করুন"%>
                </button>
                <button type="button" class="btn btn-primary" id="modal_submit"
                        onclick="modalDataSubmit()"><%=Language.equalsIgnoreCase("English") ? "ISSUE GALLERY" : "গ্যালারি ইস্যু করুন"%>
                </button>
            </div>
        </div>
    </div>
</div>

<script>

    let global_gallery_dto, global_gate_pass_gallery_id = 0;
    let itemIdAndAmount = '';
    let itemAmount;

    $(document).ready(function () {

        <%
            if (gatePassModel.secondLayerApprovalStatus==Gate_passDAO.approvalStatusApproved) {
        %>
            document.getElementsByClassName('if-approved-by-sergeant')[0].style.display = 'block';
            document.getElementsByClassName('if-approved-by-sergeant')[1].style.display = 'block';
        <%
            }
        %>
    });

    function populateGalleryModal() {
        let galleryId = $('#parliament-gallery').val();
        let galleryIds = global_gallery_dto.galleryIds, totalSeats = global_gallery_dto.totalSeatAllGallery,
            availableSeats = global_gallery_dto.availableSeatAllGallery;
        for (let key in galleryIds) {
            if (galleryIds[key] == galleryId) {

                <%
                    if(!Language.equalsIgnoreCase("English")) {
                %>
                $('#total-seat').text(convertToBanglaNumber(totalSeats[key]));
                $('#available-seat').text(convertToBanglaNumber(availableSeats[key]));
                <%
                    } else {
                %>
                $('#total-seat').text(totalSeats[key]);
                $('#available-seat').text(availableSeats[key]);
                <%
                    }
                %>

                global_gate_pass_gallery_id = galleryId;
                if (availableSeats[key] == 0) {
                    $('#modal_submit').prop("disabled", true);
                }
                break;
            }
        }
    }

    function approval(approve_button_id, dismiss_button_id, process_button_id, gate_pass_id, gate_pass_type) {
        <%
            for (GatePassItemModel gatePassItemModel: gatePassModel.items) {
        %>
        itemAmount = document.getElementById('<%=gatePassItemModel.id%>_counter').innerHTML;
        <%
            if (!Language.equalsIgnoreCase("English")) {
        %>
        itemAmount = convertToEnglishNumber(itemAmount);
        <%
            }
        %>

        itemIdAndAmount += '<%=gatePassItemModel.id%>' + ',' + itemAmount + ';';

        <%
            }
        %>

        if (gate_pass_type == "parliament gallery pass") {
            let galleryURL = "Parliament_galleryServlet?actionType=getGatePassGalleryModel" + "&visitingDate=" + '<%=gatePassModel.visitingDateLongFormat%>' + "&language=" + '<%=Language%>';
            $.ajax({
                url: galleryURL,
                type: "GET",
                async: true,
                success: function (data) {
                    let dto = JSON.parse(data);
                    global_gallery_dto = dto;
                    $('#parliament-gallery').html(dto.options);
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                    $('#select-gallery-modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            });
        } else {
            let url = "Gate_passServlet?actionType=gatePassSecondLayerApprovalFromView" + "&gatePassId=" + gate_pass_id + "&approvalstatus=approved&items=" + itemIdAndAmount;
            document.location.href = '<%=request.getContextPath()%>' + '/' + url;
        }
    }

    function dismissal(dismiss_button_id, approve_button_id, process_button_id, gate_pass_id, approval_status, gate_pass_type) {
        let remarks = '';
        // taking the reason of the dismiss
        Swal.fire({
            title: '<%=Language.equalsIgnoreCase("English") ? "Remarks" : "মন্তব্য"%>',
            text: '<%=Language.equalsIgnoreCase("English") ? "Why do you dismiss the Gate Pass request?" : "আপনি কেন গেইট পাসটির অনুমোদন বাতিল করেছেন?"%>',
            input: 'text',
            showCancelButton: false,
            confirmButtonText: '<%=Language.equalsIgnoreCase("English") ? "OK" : "ঠিক আছে"%>',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                remarks = result.value;
            }

            let url;
            if (approval_status == 1 && gate_pass_type == "parliament gallery pass") {
                url = "Gate_passServlet?actionType=gatePassSecondLayerApprovalFromView" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss&gallery=gallery&remarks=" + remarks;
            } else {
                url = "Gate_passServlet?actionType=gatePassSecondLayerApprovalFromView" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss&remarks=" + remarks;
            }
            document.location.href = '<%=request.getContextPath()%>' + '/' + url;
        });
    }

    function processing(dismiss_button_id, approve_button_id, process_button_id, gate_pass_id, approval_status, gate_pass_type) {
        let url;
        if (approval_status == 1 && gate_pass_type == "parliament gallery pass") {
            url = "Gate_passServlet?actionType=gatePassSecondLayerApprovalFromView" + "&gatePassId=" + gate_pass_id + "&approvalstatus=process&gallery=gallery";
        } else {
            url = "Gate_passServlet?actionType=gatePassSecondLayerApprovalFromView" + "&gatePassId=" + gate_pass_id + "&approvalstatus=process";
        }
        document.location.href = '<%=request.getContextPath()%>' + '/' + url;
    }


    function modalDataSubmit() {
        // Hide the model
        $('#select-gallery-modal').modal('hide');
        let url = "Gate_passServlet?actionType=gatePassSecondLayerApprovalFromView" + "&gatePassId=" + '<%=gatePassModel.gatePassId%>' + "&approvalstatus=approved&gallery=gallery&gallery_id=" + global_gate_pass_gallery_id + "&items=" + itemIdAndAmount;
        document.location.href = '<%=request.getContextPath()%>' + '/' + url;
    }

    function counting(action, counter_id) {
        let counter = document.getElementById(counter_id).innerHTML;
        let parsed;
        let output;
        <%
            if (Language.equalsIgnoreCase("English")) {
        %>
        parsed = parseInt(counter);
        output = action === "minus" ? (parseInt(parsed) > 0 ? parsed - 1 : parsed) : parsed + 1;
        document.getElementById(counter_id).innerHTML = output;
        <%
            } else {
        %>
        parsed = parseInt(convertToEnglishNumber(counter));
        output = action === "minus" ? (parseInt(parsed) > 0 ? parsed - 1 : parsed) : parsed + 1;
        document.getElementById(counter_id).innerHTML = convertToBanglaNumber(output);
        <%
            }
        %>
    }

    function convertToEnglishNumber(input) {
        let numbers = {
            '০': 0,
            '১': 1,
            '২': 2,
            '৩': 3,
            '৪': 4,
            '৫': 5,
            '৬': 6,
            '৭': 7,
            '৮': 8,
            '৯': 9
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    function convertToBanglaNumber(english_number) {
        let input = english_number + '';
        let numbers = {
            '0': '০',
            '1': '১',
            '2': '২',
            '3': '৩',
            '4': '৪',
            '5': '৫',
            '6': '৬',
            '7': '৭',
            '8': '৮',
            '9': '৯'
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                // console.log("here man!" + input[i] + " " + numbers);
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    function modalDismiss() {
        // Hide the model
        $('#select-gallery-modal').modal('hide');
    }

    $("#print-btn").click(function () {
        $(".no-print").css("display", "none");
        $.print("#modalbody");
        $(".no-print").css("display", "block");
    });

</script>


