<%@page import="java.util.*" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@ page import="geolocation.*" %>
<%@page pageEncoding="UTF-8" %>
<%
    int iParentID = Integer.parseInt(request.getParameter("myID"));
    String lang = request.getParameter("language");

    GeoLocationDTO geoLocationDTO = GeoLocationRepository.getInstance().getById(iParentID);
    int iLevel = 0;
    if (geoLocationDTO != null)
        iLevel = geoLocationDTO.geoLevelID + 1;

    String Language = "Bangla";
    if (lang != null) {
        Language = lang;
    }
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    List<GeoLocationDTO> geoLocationDTOList = GeoLocationRepository.getInstance().getByParentGeoIdAndGeoLevelId(iParentID, iLevel);
//List<GeoLocationDTO> GeoLocationDTOList2 = geoLocDao.getLocation(iParentID, iLevel);
    GeoLocationTypeDTO geoLocationTypeDTO = GeoLocationTypeRepository.getInstance().getById(iLevel);
//String sLevelName = geoLocDao.getLevelName(iLevel, Language);
    String sLevelName;
    if (geoLocationTypeDTO != null) {
        if (isLanguageEnglish) {
            sLevelName = geoLocationTypeDTO.nameEn;
        } else {
            sLevelName = geoLocationTypeDTO.nameBn;
        }
    } else {
        sLevelName = "";
    }

    StringBuilder optionBuilder = new StringBuilder();
    if (geoLocationDTOList.size() > 0) {
        if (isLanguageEnglish) {
            optionBuilder.append("<option class='form-control'><strong>Select ").append(sLevelName).append("</strong><br>");
        } else {
            optionBuilder.append("<option class='form-control'><strong>বাছাই করুন  ").append(sLevelName).append("</strong><br>");
        }
        for (GeoLocationDTO locationDTO : geoLocationDTOList) {
            //System.out.println("Language = " + Language + " geoLocationDTO.name_bn = " + geoLocationDTO.name_bn);
            optionBuilder.append("<option class='form-control");
            optionBuilder.append("' name = '");
            if (isLanguageEnglish) {
                optionBuilder.append(locationDTO.name_en);
            } else if (Language.equals("Bangla")) {
                optionBuilder.append(locationDTO.name_bn);
            }

            optionBuilder.append("' id = '");
            optionBuilder.append("loc_").append(locationDTO.id);
            optionBuilder.append("' value='");
            optionBuilder.append(locationDTO.id);
            optionBuilder.append("' level='");
            optionBuilder.append(locationDTO.geoLevelID);
            optionBuilder.append("'>");
            if (isLanguageEnglish) {
                optionBuilder.append(locationDTO.name_en);
            } else if (Language.equals("Bangla")) {
                optionBuilder.append(locationDTO.name_bn);
            }
            optionBuilder.append("<br>");
        }
    }

    out.println(optionBuilder.toString());
%>