<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="gate_pass.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.HttpRequestUtils" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = Language.equalsIgnoreCase("English");
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Gate_passDAO gate_passDAO = (Gate_passDAO) request.getAttribute("gate_passDAO");


    String navigator2 = SessionConstants.NAV_GATE_PASS;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");

    String nameAddressVisitor = LM.getText(LC.GATE_PASS_VISITOR_ADD_NAME, loginDTO) + " ( " +
            LM.getText(LC.GATE_PASS_VISITOR_ADD_CURRENTADDRESS, loginDTO) + ")";
    String mobileCredentialVisitor = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng ?
            "Mobile Number ( Credential )" : "মোবাইল নাম্বার ( পরিচয় পরিপত্র)";
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th style="text-align:center"><%=LM.getText(LC.GATE_PASS_VIEW_PASS_TYPE, loginDTO)%>
            </th>
            <th style="text-align:center"><%=LM.getText(LC.GATE_PASS_ADD_VISITINGDATE, loginDTO)%>
            </th>
            <th style="text-align:center"><%=nameAddressVisitor%>
            </th>
            <th style="text-align:center"><%=mobileCredentialVisitor%>
            </th>
             <th style="text-align:center"><%=isLangEng ? "Affiliated Persons" : "সহচর "%>
            </th>
            <th style="text-align:center"><%=LM.getText(LC.GATE_PASS_VIEW_REFERRER, loginDTO)%>
            </th>
            <th style="text-align:center"><%=isLangEng ? "Approver" : "অনুমোদনকারী"%>
            </th>
            <th style="text-align:center"><%=isLangEng ? "STATUS" : "পাসের অবস্থা"%>
            </th>
             <%
              if(userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE)
              {
            %>
            
            <th ><%=Language.equalsIgnoreCase("English") ? "Serjeant-at-arms Approval" : "সার্জেন্ট-অ্যাট-আর্মসের  সম্পাদনা"%>
            </th>
            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "View For Approval" : "অনুমোদনের জন্য দেখুন"%>
            </th>
            	<%
            }
            %>
            <%
              if(userDTO.roleID != SessionConstants.SERJEANT_AT_ARMS_ROLE && userDTO.roleID != SessionConstants.PBS_ROLE)
              {
            %>
            <th style="text-align:center"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th style="text-align:center"><%=isLangEng ? "Referer Approval" : "রেফারকারীর সম্পাদনা"%>
            </th>
            <%
              }
            %>
           
            <%
            if(userDTO.roleID == SessionConstants.PBS_ROLE)
            {
            	%>
          
            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Issue Card" : "কার্ড ইস্যু করুন"%>
            </th>
            	<%
            }
            %>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_GATE_PASS);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Gate_pass_searchModel gate_passDTO = (Gate_pass_searchModel) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("gate_passDTO", gate_passDTO);
            %>

            <jsp:include page="./gate_passSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

