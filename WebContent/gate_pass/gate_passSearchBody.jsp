<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "Gate_passServlet?actionType=search";
    String navigator = SessionConstants.NAV_GATE_PASS;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
    boolean isLangEng = Language.equalsIgnoreCase("English");

    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";

    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;

    String approvalSts = (String) session.getAttribute("notificationAfterApproval");
    if (approvalSts == null) {
        approvalSts = "";
    }
    session.setAttribute("notificationAfterApproval", "");
%>

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            <%=isLangEng ? "SEARCH GATE PASS" : "গেইট পাস খুঁজুন"%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="./gate_passNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.GATE_PASS_SEARCH_GATE_PASS_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <form action="Gate_passServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                          method="POST"
                          id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="gate_passSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.GATE_PASS_SEARCH_GATE_PASS_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initDeleteCheckBoxes();
        dateTimeInit("<%=Language%>");

        <%
               if (approvalSts.equalsIgnoreCase("approved")) {
        %>
        Swal.fire({
            position: 'center',
            type: 'success',
            text: '<%=isLangEng ? "Requested gate pass has been approved successfully" : "প্রবেশ পাসটির অনুমোদন সম্পন্ন হয়েছে"%>',
            showConfirmButton: false,
            timer: 3000
        });

        <%} else if (approvalSts.equalsIgnoreCase("dismiss")) {
        %>
        Swal.fire({
            position: 'center',
            type: 'warning',
            text: '<%=isLangEng ? "Requested gate pass has been dismissed successfully" : "প্রবেশ পাসটির অনুমোদন বাতিল করা হয়েছে"%>',
            showConfirmButton: false,
            timer: 3000
        });
        <%
            }
        %>
    });
    
    
    let global_gate_pass_id, official_visiting_date_id;

    let global_gallery_dto, global_approval_button, global_dismissal_button, global_processing_button,
     global_gate_pass_gallery_id = 0;
    
    

    function approval(approve_button_id, dismiss_button_id, gate_pass_type, gate_pass_sub_type, gate_pass_id, gate_pass_date) {

        if (gate_pass_type == "parliament area pass" && gate_pass_sub_type == "official") {
            global_gate_pass_id = gate_pass_id;
            official_visiting_date_id = approve_button_id.replace('_button_approve', '') + '_visitingDate';
            $('#gate-pass-validity').modal({
                backdrop: 'static',
                keyboard: false
            });

            setDateByTimestampAndId('validity-from-date-js', gate_pass_date);
            setDateByTimestampAndId('validity-to-date-js', gate_pass_date);
            setMinDateByTimestampAndId('validity-to-date-js', gate_pass_date);

            <%
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            String datestr = dateFormat.format(date);
            %>

            setMinDateById('validity-from-date-js', '<%=datestr%>');

            // $('#validity-from-date-js').on('datepicker.change', () => {
            //     setMinDateById('validity-to-date-js', getDateStringById('validity-from-date-js'));
            // });
        }


        let approval_status_id = approve_button_id.replace('_button_approve', '') + '_approvalStatus';

        let url = "Gate_passServlet?actionType=gatePassFirstLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=approved";
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {

                $('#' + approve_button_id).prop("disabled", true);
                $('#' + approve_button_id).html(gate_pass_type === "parliament area pass" ? '<%=(isLangEng ? "APPROVE" : "অনুমোদিত")%>' :
                    '<%=isLangEng ? "RECOMMEND" : "সুপারিশকৃত"%>');
                $('#' + dismiss_button_id).prop("disabled", false);
                $('#' + dismiss_button_id).html(gate_pass_type === "parliament area pass" ? '<%=(isLangEng ? "DISMISS" : "অননুমোদিত")%>' :
                    '<%=isLangEng ? "DISMISS" : "অসুপারিশকৃত"%>');
                $('#' + approval_status_id).html(gate_pass_type === "parliament area pass" ? '<%=(isLangEng ? "Approved" : "অনুমোদিত")%>' :
                    '<%=isLangEng ? "Recommended" : "সুপারিশকৃত"%>');
                $('#' + approval_status_id).removeClass("text-danger text-primary text-info").addClass(gate_pass_type === "parliament area pass" ? "text-info" : "text-success");
            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                if (gate_pass_type == "parliament area pass" && gate_pass_sub_type == "official") {
                    // do nothing
                } else {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        text: '<%=isLangEng ? "Requested gate pass has been approved successfully" : "প্রবেশ পাসটির অনুমোদন সম্পন্ন হয়েছে"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            }
        });

    }

    function dismissal(dismiss_button_id, approve_button_id, gate_pass_type, gate_pass_id, visiting_date, gate_pass_sub_type) {

        let approval_status_id = approve_button_id.replace('_button_approve', '') + '_approvalStatus';

        let url = "Gate_passServlet?actionType=gatePassFirstLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss";
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {

                $('#' + approve_button_id).prop("disabled", false);
                $('#' + dismiss_button_id).prop("disabled", true);
                $('#' + dismiss_button_id).html(gate_pass_type === "parliament area pass" ? '<%=(isLangEng ? "DISMISS" : "অননুমোদিত")%>' :
                    '<%=isLangEng ? "DISMISS" : "অসুপারিশকৃত"%>');
                $('#' + approve_button_id).html(gate_pass_type === "parliament area pass" ? '<%=(isLangEng ? "APPROVE" : "অনুমোদিত")%>' :
                    '<%=isLangEng ? "RECOMMEND" : "সুপারিশকৃত"%>');
                $('#' + approval_status_id).html(gate_pass_type === "parliament area pass" ? '<%=(isLangEng ? "Dismissed" : "অননুমোদিত")%>' :
                    '<%=isLangEng ? "DISMISS" : "অসুপারিশকৃত"%>');
                $('#' + approval_status_id).removeClass("text-success text-primary text-info").addClass("text-danger");
            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                Swal.fire({
                    position: 'center',
                    type: 'warning',
                    text: '<%=isLangEng ? "Requested gate pass has been dismissed successfully" : "প্রবেশ পাসটির অনুমোদন বাতিল করা হয়েছে"%>',
                    showConfirmButton: false,
                    timer: 3000
                });

                if (gate_pass_sub_type == "official") {
                    let visiting_date_id = approve_button_id.replace('_button_approve', '') + '_visitingDate';
                    $('#' + visiting_date_id).text(visiting_date);
                }
            }
        });
    }

    function modalDataSubmit() {
        // Insert validity to gate_pass table
        let validityFromDateFormat, validityToDateFormat;
        let validityFrom = getDateTimestampById('validity-from-date-js');
        let validityTo = getDateTimestampById('validity-to-date-js');
        validityFromDateFormat = getDateStringById('validity-from-date-js');
        validityToDateFormat = getDateStringById('validity-to-date-js')
        let url = "Gate_passServlet?actionType=insertGatePassValidity" + "&gatePassId=" + global_gate_pass_id + "&validityFrom=" + validityFrom + "&validityTo=" + validityTo;
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {
                console.log(data);
            },
            error: function (error) {
                console.log(error);
            }
        });


        // Hide the model
        $('#gate-pass-validity').modal('hide');

        // fire a sweet alert
        Swal.fire({
            position: 'center',
            type: 'success',
            text: '<%=isLangEng ? "Requested gate pass has been approved successfully" : "প্রবেশ পাসটির অনুমোদন সম্পন্ন হয়েছে"%>',
            showConfirmButton: false,
            timer: 3000
        });

        // set visitingDate
        <%
            if (isLangEng) {
        %>
        $('#' + official_visiting_date_id).text(validityFromDateFormat + "\nto\n" + validityToDateFormat);
        <%
            } else {
        %>
        $('#' + official_visiting_date_id).text(convertToBanglaNumber(validityFromDateFormat) + "\nথেকে\n" + convertToBanglaNumber(validityToDateFormat));
        <%
            }
        %>
    }


    $(function () {

        $('#validity-from-date-js').on('datepicker.change', () => {
            if (!dateValidator('validity-from-date-js', true))
                $('#modal_submit').prop('disabled', true);
            else {
                $('#modal_submit').prop('disabled', false);
                setMinDateById('validity-to-date-js', getDateStringById('validity-from-date-js'));
                if (!dateValidator('validity-to-date-js', true))
                    $('#modal_submit').prop('disabled', true);
            }
        });

        $('#validity-to-date-js').on('datepicker.change', () => {
            if (!dateValidator('validity-to-date-js', true))
                $('#modal_submit').prop('disabled', true);
            else
                $('#modal_submit').prop('disabled', false);
        });
    });

    function convertToBanglaNumber(english_number) {
        let input = english_number + '';
        let numbers = {
            '0': '০',
            '1': '১',
            '2': '২',
            '3': '৩',
            '4': '৪',
            '5': '৫',
            '6': '৬',
            '7': '৭',
            '8': '৮',
            '9': '৯'
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                // console.log("here man!" + input[i] + " " + numbers);
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }
    
    
    
    function sea_approval(approve_button_id, dismiss_button_id, process_button_id, gate_pass_id, gate_pass_type, visiting_date) {

        if (gate_pass_type == "parliament gallery pass") {
            global_approval_button = approve_button_id;
            global_dismissal_button = dismiss_button_id;
            global_processing_button = process_button_id;
            global_gate_pass_id = gate_pass_id;

            let galleryURL = "Parliament_galleryServlet?actionType=getGatePassGalleryModel" + "&visitingDate=" + visiting_date + "&language=" + '<%=Language%>';
            $.ajax({
                url: galleryURL,
                type: "GET",
                async: true,
                success: function (data) {
                    let dto = JSON.parse(data);
                    global_gallery_dto = dto;
                    $('#parliament-gallery').html(dto.options);
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                    $('#select-gallery-modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            });
        } else {

            let approval_status_id = approve_button_id.replace('_button_approve', '') + '_approvalStatus';

            let url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=approved";
            $.ajax({
                url: url,
                type: "POST",
                async: true,
                success: function (data) {

                    $('#' + approve_button_id).prop("disabled", true);
                    $('#' + dismiss_button_id).prop("disabled", false);
                    $('#' + process_button_id).prop("disabled", false);
                    $('#' + approval_status_id).html('<%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>');
                    $('#' + approval_status_id).removeClass("text-danger text-primary text-info").addClass("text-success");
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        text: '<%=Language.equalsIgnoreCase("English") ? "Requested gate pass has been approved successfully" : "প্রবেশ পাসটি অনুমোদিত হয়েছে"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            });
        }

    }


    function sea_dismissal(dismiss_button_id, approve_button_id, process_button_id, gate_pass_id, approval_status, gate_pass_type) {
        let approval_status_id = approve_button_id.replace('_button_approve', '') + '_approvalStatus';
        let remarks = '';
        // taking the reason of the dismiss
        Swal.fire({
            title: '<%=Language.equalsIgnoreCase("English") ? "Remarks" : "মন্তব্য"%>',
            text: '<%=Language.equalsIgnoreCase("English") ? "Why do you dismiss the Gate Pass request?" : "আপনি কেন গেইট পাসটির অনুমোদন বাতিল করেছেন?"%>',
            input: 'text',
            type: 'info',
            showCancelButton: false,
            confirmButtonText: '<%=Language.equalsIgnoreCase("English") ? "OK" : "ঠিক আছে"%>',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                remarks = result.value;
            }

            let url;
            if (approval_status == 1 && gate_pass_type == "parliament gallery pass") {
                console.log("here!");
                url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss&gallery=gallery&remarks=" + remarks;
            } else {
                url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss&remarks=" + remarks;
            }

            $.ajax({
                url: url,
                type: "POST",
                async: true,
                success: function (data) {
                    $('#' + dismiss_button_id).prop("disabled", true);
                    $('#' + approve_button_id).prop("disabled", false);
                    $('#' + process_button_id).prop("disabled", false);
                    $('#' + approval_status_id).html('<%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>');
                    $('#' + approval_status_id).removeClass("text-success text-primary text-info").addClass("text-danger");
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                    Swal.fire({
                        position: 'center',
                        type: 'warning',
                        text: '<%=Language.equalsIgnoreCase("English") ? "Requested gate pass has been dismissed successfully" : "প্রবেশ পাসটির অনুমোদন বাতিল করা হয়েছে"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            });

        });
    }


    function sea_processing(process_button_id, approve_button_id, dismiss_button_id, gate_pass_id, approval_status, gate_pass_type) {
        let approval_status_id = approve_button_id.replace('_button_approve', '') + '_approvalStatus';
        let url;
        if (approval_status == 1 && gate_pass_type == "parliament gallery pass") {
            console.log('HERE BOY!');
            url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=process&gallery=gallery";
        } else {
            url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=process";
        }
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {

                $('#' + approve_button_id).prop("disabled", false);
                $('#' + dismiss_button_id).prop("disabled", false);
                $('#' + process_button_id).prop("disabled", true);
                $('#' + approval_status_id).html('<%=Language.equalsIgnoreCase("English") ? "Processing" : "প্রক্রিয়াধীন"%>');
                $('#' + approval_status_id).removeClass("text-success text-primary text-danger").addClass("text-info");
            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                Swal.fire({
                    position: 'center',
                    type: 'warning',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Requested gate pass has been hold for processing! " : "প্রবেশ পাসটি প্রক্রিয়াধীন অবস্থায় আছে!"%>',
                    showConfirmButton: false,
                    timer: 3000
                });
            }
        });
    }

</script>


