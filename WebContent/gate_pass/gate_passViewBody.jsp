<%@page import="util.TimeConverter"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="gate_pass.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.StringUtils" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Gate_passDAO gate_passDAO = new Gate_passDAO("gate_pass");
    Gate_passDTO gate_passDTO = gate_passDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    GatePassModel gatePassModel = gate_passDAO.getGatePassModelById(id);

    String viewPageTitle = Language.equalsIgnoreCase("English") ? (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? gatePassModel.gatePassTypeEng + "(" + gatePassModel.gatePassSubTypeEng + ")" : gatePassModel.gatePassTypeEng) :
            (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? gatePassModel.gatePassTypeBng + "(" + gatePassModel.gatePassSubTypeBng + ")" : gatePassModel.gatePassTypeBng);

    if (Language.equalsIgnoreCase("English")) {
        viewPageTitle = viewPageTitle.substring(0, 1).toUpperCase() + viewPageTitle.substring(1);
    }
    String context = request.getContextPath() + "/";

    int personCount = 0;

    long todayInMilSec = Calendar.getInstance().getTimeInMillis();
%>

<style>
    body {
        font-family: 'Roboto', 'SolaimanLipi', sans-serif;
    }

    .title {
        color: #0098bf !important;
        font-weight: bold;
    }
</style>



<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;">
        <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
            <div class="row">
                <div class="col-md-3 mb-4 mb-md-0">
                            <span class="lead font-weight-bold"
                                  style="color: #0098bf !important"><%=LM.getText(LC.GATE_PASS_VIEW_GATE_NO, loginDTO)%>
                                :
                                <%=Language.equalsIgnoreCase("English") ? gatePassModel.parliamentGateEng : gatePassModel.parliamentGateBng%>
                            </span>
                </div>
                <div class="col-md-6 d-flex justify-content-center">
                    <div class="text-center">
                        <img
                                width="35%"
                                src="<%=context%>assets/static/parliament_logo.png"
                                alt="logo"
                                class="logo-default"
                        />
                        <h1 class="mt-3 table-title">
                            <%=Language.equalsIgnoreCase("English") ? "BANGLADESH NATIONAL PARLIAMENT" : "বাংলাদেশ জাতীয় সংসদ"%>
                        </h1>
                        <h4 class="mt-2 table-title">
                            <%=Language.equalsIgnoreCase("english") ? (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ?
                                    gatePassModel.gatePassTypeEng + " (" + gatePassModel.gatePassSubTypeEng + ")" : gatePassModel.gatePassTypeEng) : (gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ?
                                    gatePassModel.gatePassTypeBng + " (" + gatePassModel.gatePassSubTypeBng + ")" : gatePassModel.gatePassTypeBng) %>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VIEW_VISITING_DATE, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-3">
                                <span>
                                    <%
                                        String today;
                                        if (gatePassModel.gatePassSubTypeEng.equalsIgnoreCase("official") && gatePassModel.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
                                            today = simpleDateFormat.format(new Date());
                                    %>
                                        <%=Utils.getDigits(today, Language)%>
                                    <%
                                    } else {
                                    %>
                                        <%=Language.equalsIgnoreCase("English") ? gatePassModel.visitingDateEng : gatePassModel.visitingDateBng%>
                                    <%
                                        }
                                    %>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VIEW_MOBILE, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                    <%=Language.equalsIgnoreCase("English") ? gatePassModel.visitorMobileNumberEng : gatePassModel.visitorMobileNumberBng%>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VIEW_CREDENTIAL, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                    <%=Language.equalsIgnoreCase("English") ? (gatePassModel.visitorCredentialTypeEng + " - " + gatePassModel.visitorCredentialNo) :
                                            (gatePassModel.visitorCredentialTypeBng + " - " + StringUtils.convertToBanNumber(gatePassModel.visitorCredentialNo))%>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                   <%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_NAME, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                    <%=gatePassModel.visitorName%>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                   <%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_FATHER_NAME, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                   <%=gatePassModel.visitorFatherName%>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_MOTHER_NAME, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                  <%=gatePassModel.visitorMotherName%>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <div class="row mb-5 mb-md-0">
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_PERMANENTADDRESS, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DIVISION, loginDTO)%>
                                </span>
                            <br>
                            <span>
                                    <%=Language == "English" ? gatePassModel.permanentAddress.divisionEng : gatePassModel.permanentAddress.divisionBng%>
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DISTRICT, loginDTO)%>
                                </span>
                            <br>
                            <span>
                                    <%=Language == "English" ? gatePassModel.permanentAddress.districtEng : gatePassModel.permanentAddress.districtBng%>
                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UPOZILLA, loginDTO)%>
                                </span>
                            <br>
                            <span>
                                    <%=Language == "English" ? gatePassModel.permanentAddress.thanaEng : gatePassModel.permanentAddress.thanaBng%>
                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UNION, loginDTO)%>
                                </span>
                            <br>
                            <span>
                                    <%=Language == "English" ? gatePassModel.permanentAddress.unionEng : gatePassModel.permanentAddress.unionBng%>
                                </span>
                        </div>
                        <div class="col-md-4 offset-md-2 mt-3">
                            <span class="title"><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_VILLAGE, loginDTO)%></span>
                            <br>
                            <span><%=gatePassModel.permanentAddress.addressText%></span>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-2">
                                <span class="title">
                                   <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_CURRENTADDRESS, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DIVISION, loginDTO)%>
                                </span>
                            <br>
                            <span>
                                    <%=Language == "English" ? gatePassModel.currentAddress.divisionEng : gatePassModel.currentAddress.divisionBng%>
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_DISTRICT, loginDTO)%>
                                </span>
                            <br>
                            <span>
                                    <%=Language == "English" ? gatePassModel.currentAddress.districtEng : gatePassModel.currentAddress.districtBng%>
                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UPOZILLA, loginDTO)%>
                                </span>
                            <br>
                            <span>
                                    <%=Language == "English" ? gatePassModel.currentAddress.thanaEng : gatePassModel.currentAddress.thanaBng%>
                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_UNION, loginDTO)%>
                                </span>
                            <br>
                            <span>
                                    <%=Language == "English" ? gatePassModel.currentAddress.unionEng : gatePassModel.currentAddress.unionBng%>
                                </span>
                        </div>
                        <div class="col-md-4 offset-md-2 mt-3">
                            <span class="title"><%=LM.getText(LC.GATE_PASS_VISITOR_ADD_VILLAGE, loginDTO)%></span>
                            <br>
                            <span><%=gatePassModel.currentAddress.addressText%></span>
                        </div>
                    </div>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.GATE_PASS_VIEW_OFFICER_TYPE, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                  <%=Language.equalsIgnoreCase("English") ? gatePassModel.officerTypeEng : gatePassModel.officerTypeBng %>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
									 <%=LM.getText(LC.HM_OFFICE, loginDTO)%> :
                                </span>
                            <br>
                            <span>
								<%=gatePassModel.officeName%>
                             </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">
									<%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                </span>
                            <br>
                            <span>
								<%=gatePassModel.designation%>
                            </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>

					<%
					if(gatePassModel.hasVehicle)
					{
					%>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                  <%=Language.equalsIgnoreCase("English")?"Vehicle Information":"যানবাহনের তথ্য"%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                  <%=gatePassModel.vehicleDescription%>
                                </span>
                           
                        </div>
                        
                        <div class="col-md-2">
                                <span class="title">
                                  <%=Language.equalsIgnoreCase("English")?"Vehicle Number":"যানবাহনের নম্বর"%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                  <%=gatePassModel.vehicleNumber%>
                                </span>
                           
                        </div>
                        
                    </div>
                    <%
					}
                    %>
                    
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.GATE_PASS_VIEW_GENDER, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                  <%=Language.equalsIgnoreCase("English") ? gatePassModel.genderTypeEng : gatePassModel.genderTypeBng %>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <%
                        if (gatePassModel.secondLayerApprovalStatus == Gate_passDAO.approvalStatusCancelled) {
                    %>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                  <%=Language.equalsIgnoreCase("English") ? "Remarks" : "মন্তব্য"%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                  <%=gatePassModel.remarks%>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <%
                        }
                    %>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                  <%=Language.equalsIgnoreCase("English") ? "Verified NID" : "যাচাইকৃত এনআইডি"%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                  <%=Language.equalsIgnoreCase("English") ? "No" : "না"%>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <%
                        if (gatePassModel.gatePassTypeEng.equalsIgnoreCase("visitor pass(parliament building entry)")) {
                    %>
                    <div class="row my-3">
                        <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.GATE_PASS_PARLIAMENT_BUILDING_REASON_FOR_VISIT, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span>
                                  <%=gatePassModel.reasonForVisit%>
                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <%
                        }
                    %>
                    <%if ((gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") || gatePassModel.gatePassTypeEng.equalsIgnoreCase("meeting/briefing pass") || gatePassModel.gatePassTypeEng.equalsIgnoreCase("visitor pass(parliament building entry)")) && (gatePassModel.persons.size() > 0)) {%>
                    <div class="row">
                        <div class="col-md-2">
                                <span class="title">
                                   <%=LM.getText(LC.GATE_PASS_VIEW_AFFILIATED_PERSONS, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.GATE_PASS_VIEW_VISITOR_NAME, loginDTO)%>
                                </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <br>
                            <span>
                                    <%=model.name%>
                                </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.GATE_PASS_VIEW_MOBILE, loginDTO)%>
                                </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <br>
                            <span>
                                    <%=Language.equalsIgnoreCase("English") ? model.mobileNoEng : model.mobileNoBng%>
                                </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VIEW_CREDENTIAL, loginDTO)%>
                                </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <br>
                            <span>
                                    <%=Language.equalsIgnoreCase("English") ? model.credentialTypeEng + "-" + model.credentialNo : model.credentialTypeBng + "-" + model.credentialNo%>
                                </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <br>
                            	<span>
                                    <%= model.designation%>
                                </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                                </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <br>
                            	<span>
                                    <%= model.officeName%>
                                </span>
                            <%
                                }
                            %>
                        </div>
                        <div  style = "display:none">
                                <span class="title">
                                    <%=Language.equalsIgnoreCase("English") ? "Switch" : "বিকল্প" %>
                                </span>
                            <%
                                for (GatePassPersonModel model : gatePassModel.persons) {
                            %>
                            <%--                                <br>--%>
                            <span>
<%--                                    <%=Language.equalsIgnoreCase("English") ? model.credentialTypeEng + "-" + model.credentialNo : model.credentialTypeBng + "-" + model.credentialNo%>--%>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" checked
                                               id="customSwitch_<%=personCount%>"
                                               onclick="processSwitches(this.id,'<%=model.id%>')">
<%--                                        <span class="slider round"></span>--%>
                                        <label class="custom-control-label"
                                               for="customSwitch_<%=personCount++%>"></label>
                                    </div>
                                </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-4 offset-md-2 mt-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <%}%>
                    <%if ((gatePassModel.gatePassTypeEng.equalsIgnoreCase("visitor pass(parliament building entry)") || gatePassModel.gatePassTypeEng.equalsIgnoreCase("meeting/briefing pass")) && (gatePassModel.items.size() > 0)) {%>
                    <div class="row">
                        <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.GATE_PASS_VIEW_PERMITTED_ITEMS, loginDTO)%> :
                                </span>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.GATE_PASS_VIEW_ITEM_NAME, loginDTO)%>
                                </span>
                            <%
                                for (GatePassItemModel model : gatePassModel.items) {
                            %>
                            <br>
                            <span>
                                    <%=Language.equalsIgnoreCase("English") ? model.nameEng : model.nameBng%>
                                </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-2">
                                <span class="title">
                                    <%=Language.equalsIgnoreCase("English") ? "Amount (Issued)" : "পরিমাণ (ইস্যুকৃত)"%>
                                </span>
                            <%
                                for (GatePassItemModel model : gatePassModel.items) {
                            %>
                            <br>
                            <span>
                                    <%=Language.equalsIgnoreCase("English") ? model.amount : StringUtils.convertToBanNumber(String.valueOf(model.amount))%>
                                </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-3">
                            <%
                                if ((gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") && gatePassModel.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) ||
                                        ((!gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass")) && gatePassModel.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved && gatePassModel.secondLayerApprovalStatus == Gate_passDAO.approvalStatusApproved)) {
                            %>
                            <span class="title">
                                        <%=Language.equalsIgnoreCase("English") ? "Amount (Approved)" : "পরিমাণ (অনুমোদিত)"%>
                                    </span>
                            <%
                                for (GatePassItemModel model : gatePassModel.items) {
                            %>
                            <br>
                            <span>
                                        <%=Language.equalsIgnoreCase("English") ? model.approvedAmount : StringUtils.convertToBanNumber(String.valueOf(model.approvedAmount))%>
                                    </span>

                            <%
                                }
                            } else {
                            %>
                            <span class="title">

                                    </span>
                            <br>
                            <span>

                                    </span>
                            <%
                                }
                            %>
                        </div>
                        <div class="col-md-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                        <div class="col-md-4 offset-md-2 mt-3">
                                <span class="title">

                                </span>
                            <br>
                            <span>

                                </span>
                        </div>
                    </div>
                    <%}%>
                    <div>
                        <div class="row my-5" id="notice">
                            <div class="col-12">
                                    <span class="title">
                                        <%=Language.equalsIgnoreCase("English") ? "N.B: This pass have to return to the gate office!" : "বিঃ দ্রঃ -পাসটি ইস্যুকৃত গেইটে ফেরত প্রদান করতে হবে"%>
                                    </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right my-4">
                                <div class="inline-block text-right">

                                    <%
                                    	System.out.println("gatePassModel.firstLayerApprovalStatus == Gate_passDAO.approvalStatusPending " 
                                    + (gatePassModel.firstLayerApprovalStatus == Gate_passDAO.approvalStatusPending));
                                        
                                    if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending && userDTO.employee_record_id == gate_passDTO.referrerId)
                                    {
                                  
	                                    if (gatePassModel.firstLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
	                                    %>
	
	                                    <button type="button" id='<%=i%>_button_approve'
	                                            class="btn  btn-sm shadow text-white submit-btn shadow btn-border-radius" style=""
	                                            onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=gatePassModel.gatePassTypeEng%>', '<%=gatePassModel.gatePassSubTypeEng%>', '<%=id%>', '<%=gatePassModel.visitingDateEng%>')">
	                                        <%=gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (Language.equalsIgnoreCase("English") ? "APPROVE" :
	                                                "অনুমোদন দিন") : (Language.equalsIgnoreCase("English") ? "RECOMMEND" : "সুপারিশ করুন")%>
	                                    </button>
	                                    <button type="button" id='<%=i%>_button_dismiss'
	                                            class="btn  btn-sm shadow text-white cancel-btn shadow btn-border-radius ml-2" style=""
	                                            onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=gatePassModel.gatePassTypeEng%>', '<%=id%>')">
	                                        <%=gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (Language.equalsIgnoreCase("English") ? "DISMISS" :
	                                                "খারিজ করুন") : (Language.equalsIgnoreCase("English") ? "REJECT" : "খারিজ করুন")%>
	                                    </button>
	                                    <%
	                                    } else if (gatePassModel.firstLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
	                                    %>
	                                    <button type="button" id='<%=i%>_button_approve'
	                                            class="btn  btn-sm shadow text-white submit-btn shadow btn-border-radius" style=""
	                                            disabled>
	                                        <%=gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (Language.equalsIgnoreCase("English") ? "APPROVED" :
	                                                "অনুমোদিত") : (Language.equalsIgnoreCase("English") ? "RECOMMENDED" : "সুপারিশকৃত")%>
	                                    </button>
	                                    <button type="button" id='<%=i%>_button_dismiss'
	                                            class="btn  btn-sm shadow text-white cancel-btn shadow btn-border-radius ml-2" style=""
	                                            onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=gatePassModel.gatePassTypeEng%>', '<%=id%>')">
	                                        <%=gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (Language.equalsIgnoreCase("English") ? "DISMISS" :
	                                                "খারিজ করুন") : (Language.equalsIgnoreCase("English") ? "REJECT" : "খারিজ করুন")%>
	                                    </button>
	                                    <%
	                                    } else {
	                                    %>
	                                    <button type="button" id='<%=i%>_button_approve'
	                                            class="btn  btn-sm shadow text-white submit-btn shadow btn-border-radius" style=""
	                                            onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=gatePassModel.gatePassTypeEng%>', '<%=gatePassModel.gatePassSubTypeEng%>', '<%=id%>', '<%=gatePassModel.visitingDateEng%>')">
	                                        <%=gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (Language.equalsIgnoreCase("English") ? "APPROVE" :
	                                                "অনুমোদন দিন") : (Language.equalsIgnoreCase("English") ? "RECOMMEND" : "সুপারিশ করুন")%>
	                                    </button>
	                                    <button type="button" id='<%=i%>_button_dismiss'
	                                            class="btn  btn-sm shadow text-white cancel-btn shadow btn-border-radius ml-2" style=""
	                                            disabled>
	                                        <%=gatePassModel.gatePassTypeEng.equalsIgnoreCase("parliament area pass") ? (Language.equalsIgnoreCase("English") ? "DISMISSED" :
	                                                "অননুমোদিত") : (Language.equalsIgnoreCase("English") ? "REJECTED" : "খারিজকৃত")%>
	                                    </button>
	                                    <%
	                                        }
                                    }
	                                    %>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="modal fade" id="gate-pass-validity" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle"
                                                    style="font-size: large; font-weight: bolder;"><%=Language.equalsIgnoreCase("English") ?
                                                        "Pass Validity" : "প্রবেশ পাসের বৈধতার সময়সীমা"%>
                                                </h5>
                                            </div>
                                            <div class="modal-body">
                                                <form id="gate-validity-form" method="post">
                                                    <div class="form-body">
                                                        <label class="form-group control-label">
                                                            <%=LM.getText(LC.GATE_PASS_ADD_VALIDITYFROM, loginDTO)%>
                                                        </label>
                                                        <div class="form-group ">
                                                            <div class="" id='validityFrom_div_<%=i%>'>
                                                                <div class="container">
                                                                    <jsp:include page="/date/date.jsp">
                                                                        <jsp:param name="DATE_ID"
                                                                                   value="validity-from-date-js"></jsp:param>
                                                                        <jsp:param name="LANGUAGE"
                                                                                   value="<%=Language%>"></jsp:param>
                                                                    </jsp:include>
                                                                </div>
                                                                <input type='hidden' class='form-control'
                                                                       id='validity-from-date'
                                                                       name='validityFrom' value=''
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                        <label class="form-group control-label">
                                                            <%=LM.getText(LC.GATE_PASS_ADD_VALIDITYTO, loginDTO)%>
                                                        </label>
                                                        <div class="form-group ">
                                                            <div class="" id='validityTo_div_<%=i%>'>
                                                                <div class="container">
                                                                    <jsp:include page="/date/date.jsp">
                                                                        <jsp:param name="DATE_ID"
                                                                                   value="validity-to-date-js"></jsp:param>
                                                                        <jsp:param name="LANGUAGE"
                                                                                   value="<%=Language%>"></jsp:param>
                                                                    </jsp:include>
                                                                </div>
                                                                <input type='hidden' class='form-control'
                                                                       id='validity-to-date'
                                                                       name='validityTo' value=''
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" id="modal_submit"
                                                        class="btn-sm shadow border-0 text-white submit-btn"
                                                        style=";"
                                                        onclick="modalDataSubmit()">
                                                    <%=Language.equalsIgnoreCase("English") ? "Submit" : "সম্পাদন করুন"%>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>


    // $('.custom-control-input').on('click', () => {
    //     alert(this.id);
    // });

    let global_url, global_gate_pass_id;

    const global_person_ids = new Set();

    <%
        String notification = request.getParameter("notification");
    %>

    function approval(approve_button_id, dismiss_button_id, gate_pass_type, gate_pass_sub_type, gate_pass_id, gate_pass_date) {
        let personIdsInString = "";
        for (let item of global_person_ids) personIdsInString += (item + ";");
        let url = '<%=request.getContextPath()%>' + '/' + "Gate_passServlet?actionType=gatePassFirstLayerApprovalFromView" + "&gatePassId=" + gate_pass_id + "&approvalstatus=approved&notification=<%=notification%>&personIds=" + personIdsInString;

        if (gate_pass_type == "parliament area pass" && gate_pass_sub_type == "official") {
            global_url = url;
            global_gate_pass_id = gate_pass_id;
            $('#gate-pass-validity').modal({
                backdrop: 'static',
                keyboard: false
            });
        } else {
            document.location.href = url;
        }

        setDateByStringAndId('validity-from-date-js', gate_pass_date, '-');
        setDateByStringAndId('validity-to-date-js', gate_pass_date, '-');
    }


    function dismissal(dismiss_button_id, approve_button_id, gate_pass_type, gate_pass_id) {
        let personIdsInString = "";
        for (let item of global_person_ids) personIdsInString += (item + ";");

        let url = "Gate_passServlet?actionType=gatePassFirstLayerApprovalFromView" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss&notification=<%=notification%>&personIds=" + personIdsInString;
        document.location.href = '<%=request.getContextPath()%>' + '/' + url;
    }


    function modalDataSubmit() {
        // Insert validity to gate_pass table
        let validityFrom = getDateTimestampById('validity-from-date-js');
        let validityTo = getDateTimestampById('validity-to-date-js');
        let url = "Gate_passServlet?actionType=insertGatePassValidity" + "&gatePassId=" + global_gate_pass_id + "&validityFrom=" + validityFrom + "&validityTo=" + validityTo;
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {
                document.location.href = global_url;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    $(function () {

        <%
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            String datestr = dateFormat.format(date);
        %>
        setMinDateById('validity-from-date-js', '<%=datestr%>');

        $('#validity-from-date-js').on('datepicker.change', () => {
            if (!dateValidator('validity-from-date-js', true))
                $('#modal_submit').prop('disabled', true);
            else {
                $('#modal_submit').prop('disabled', false);
                setMinDateById('validity-to-date-js', getDateStringById('validity-from-date-js'));
                if (!dateValidator('validity-to-date-js', true))
                    $('#modal_submit').prop('disabled', true);
            }
        });

        $('#validity-to-date-js').on('datepicker.change', () => {
            if (!dateValidator('validity-to-date-js', true))
                $('#modal_submit').prop('disabled', true);
            else
                $('#modal_submit').prop('disabled', false);
        });
    });


    function processSwitches(id, affiliatedPersonId) {

        if ($('#' + id).prop("checked") == false) {
            global_person_ids.add(affiliatedPersonId);
        } else {
            global_person_ids.delete(affiliatedPersonId);
        }
    }


</script>