<%@page pageEncoding="UTF-8" %>

<%@page import="gate_pass.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_GATE_PASS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Gate_pass_searchModel gate_passDTO = (Gate_pass_searchModel) request.getAttribute("gate_passDTO");
    CommonDTO commonDTO = gate_passDTO;
    String servletName = "Gate_passServlet";

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    String Message = "Done";
    approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("gate_pass", gate_passDTO.iD);

    System.out.println("gate_passDTO = " + gate_passDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Gate_passDAO gate_passDAO = (Gate_passDAO) request.getAttribute("gate_passDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='gatePassType'>
    <%="<b>" + (Language.equalsIgnoreCase("english") ? gate_passDTO.gatePassTypeEng : gate_passDTO.gatePassTypeBng) + "</b>"%>
</td>

<td id='visitingDate'>
    <%
        value = gate_passDTO.visitingDate + "";
    %>
    <%
        String formatted_visitingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%="<b>" + Utils.getDigits(formatted_visitingDate, Language) + "</b>"%>


</td>

<td id='gatePassVisitorName'>
    <%
        String address = gate_passDTO.currentAddress.addressText;
        if (Language.equalsIgnoreCase("English")) {
            address += gate_passDTO.currentAddress.thanaEng.equals("") ? "" : ", " + gate_passDTO.currentAddress.thanaEng;
            address += gate_passDTO.currentAddress.districtEng.equals("") ? "" : ", " + gate_passDTO.currentAddress.districtEng;
        } else {
            address += gate_passDTO.currentAddress.thanaBng.equals("") ? "" : ", " + gate_passDTO.currentAddress.thanaBng;
            address += gate_passDTO.currentAddress.districtBng.equals("") ? "" : ", " + gate_passDTO.currentAddress.districtBng;
        }

        value = "<em><b>" + gate_passDTO.name + "</b></em><br/>" + address;
    %>

    <%=value%>


</td>

<td id='gatePassReferrerName'>
    <%
        if (HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng) {
            if (gate_passDTO.referrerOfficeUnitEng.equals("")) {
                value = "<em><b>" + gate_passDTO.referrerNameEng + "</b></em>";
            } else {
                value = "<em><b>" + gate_passDTO.referrerNameEng + "</b></em><br/>" + gate_passDTO.referrerOfficeUnitEng;
            }
        } else {
            if (gate_passDTO.referrerOfficeUnitBng.equals("")) {
                value = "<em><b>" + gate_passDTO.referrerNameBng + "</b></em>";
            } else {
                value = "<em><b>" + gate_passDTO.referrerNameBng + "</b></em><br/>" + gate_passDTO.referrerOfficeUnitBng;
            }
        }
    %>
    <%=value%>
</td>


<%--<td id='gatePassVisitorAddress'>--%>
<%--    --%>
<%--</td>--%>

<td>
    <%
        if (gate_passDTO.isIssued == 1) {
    %>
    <span class="text-primary font-italic font-weight-bold">
            <%=Language.equalsIgnoreCase("English") ? "Issued" : "ইস্যুকৃত"%>
        </span>
    <%
    } else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending) {
    %>
    <span class="text-primary font-italic" id="<%=i%>_approvalStatus">
            <%=Language.equalsIgnoreCase("English") ? "Recommended" : "সুপারিশকৃত"%>
        </span>
    <%
    } else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusProcessing) {
    %>
    <span class="text-info font-italic" id="<%=i%>_approvalStatus">
            <%=Language.equalsIgnoreCase("English") ? "Processing" : "প্রক্রিয়াধীন"%>
        </span>
    <%
    } else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) {
    %>
    <span class="text-success font-italic" id="<%=i%>_approvalStatus">
            <%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>
        </span>

    <%
    } else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusCancelled) {
    %>
    <span class="text-danger font-italic" id="<%=i%>_approvalStatus">
            <%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>
        </span>
    <%
        }
    %>

</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Gate_passServlet?actionType=armsOfficeView&ID=<%=gate_passDTO.gate_pass_id%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>


<td id='<%=i%>_approval' style='white-space: nowrap' colspan="6">

    <div class="button-group">
        <%
            if (gate_passDTO.isIssued == 0) 
            {
        		if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusPending) 
        		{
        %>
        <button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm btn-danger border-0 shadow  btn-border-radius"
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm btn-success border-0 shadow  btn-border-radius"
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.visitingDate%>')">
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
        </button>
        <button type="button" id='<%=i%>_button_process'
                class="btn btn-sm btn-primary border-0 shadow  btn-border-radius"
                onclick="processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
        </button>
        <%
        	}
        	else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusApproved) 
        	{
        %>
        <button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm btn-danger border-0 shadow  btn-border-radius"
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm btn-success border-0 shadow  btn-border-radius"
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.visitingDate%>')"
                disabled>
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
        </button>
        <button type="button" id='<%=i%>_button_process'
                class="btn btn-sm btn-primary border-0 shadow  btn-border-radius"
                onclick="processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
        </button>
        <%
        	} 
        	else if (gate_passDTO.secondLayerApprovalStatus == Gate_passDAO.approvalStatusProcessing) 
        	{
        %>

        <button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm btn-danger border-0 shadow  show-approval-notification btn-border-radius"
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm btn-success border-0 shadow  btn-border-radius"
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.visitingDate%>')">
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
        </button>
        <button type="button" id='<%=i%>_button_process'
                class="btn btn-sm btn-primary border-0 shadow  btn-border-radius"
                onclick="processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')"
                disabled>
            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
        </button>

        <%
        	}
        	else 
        	{
        %>
        <button type="button" id='<%=i%>_button_dismiss'
                class="btn btn-sm border-0 btn-danger shadow show-approval-notification btn-border-radius"

                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')"
                disabled>
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <button type="button" id='<%=i%>_button_approve'
                class="btn btn-sm border-0 btn-success shadow btn-border-radius"
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=i%>_button_process', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.gatePassTypeEng%>', '<%=gate_passDTO.visitingDate%>')">
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদিত"%>
        </button>
        <button type="button" id='<%=i%>_button_process'
                class="btn btn-sm border-0 btn-primary shadow btn-border-radius"
                onclick="processing(this.id, '<%=i%>_button_approve', '<%=i%>_button_dismiss', '<%=gate_passDTO.gate_pass_id%>', '<%=gate_passDTO.secondLayerApprovalStatus%>', '<%=gate_passDTO.gatePassTypeEng%>')">
            <%=Language.equalsIgnoreCase("English") ? "PROCESS" : "প্রক্রিয়াধীন"%>
        </button>
        <%
            }
         }
        %>

    </div>

</td>


<div class="modal fade" id="select-gallery-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"
                    style="font-size: large; font-weight: bold"><%=Language.equalsIgnoreCase("English") ?
                        "SELECT GALLERY" : "গ্যালারি নির্বাচন করুন"%>
                </h5>
            </div>
            <div class="modal-body">
                <form id="select-gallery-form" method="post">

                    <div class="form-body">
                        <div class="form-group row">
                            <label class="control-label col-md-2"
                                   style="font-weight: bold"><%=Language.equalsIgnoreCase("English") ? "Gallery" : "গ্যালারি"%>
                            </label>
                            <div class="col-md-10" id="gallery-list">
                                <select class='form-control' name='parliament-gallery' onchange="populateGalleryModal()"
                                        id='parliament-gallery' tag='pb_html'>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class=" form-group row">
                                    <label class="control-label col-md-6"
                                           style="font-weight: bold"><%=Language.equalsIgnoreCase("English") ? "Total Seat" : "মোট সিট"%>
                                    </label>
                                    <div class="col-md-6" id='total-seat-div'>
                                        <span id="total-seat">

                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-6 control-label"
                                           style="font-weight: bold"><%=Language.equalsIgnoreCase("English") ? "Available Seat" : "ফাকা সিট"%>
                                    </label>
                                    <div class="col-md-6" id='available-seat-div'>
                                        <span id="available-seat">

                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--                        <label class="form-group control-label"><%=Language.equalsIgnoreCase("English") ? "Gallery" : "গ্যালারি"%>--%>
                        <%--                        </label>--%>
                        <%--                        <div class="form-group ">--%>
                        <%--                            <div class="" id="gallery-list">--%>
                        <%--                                <select class='form-control' name='parliament-gallery' onchange="populateGalleryModal()" id='parliament-gallery' tag='pb_html'>--%>
                        <%--                                </select>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>


                        <%--                        <label class="form-group control-label"><%=Language.equalsIgnoreCase("English") ? "Total Seat" : "মোট সিট"%>--%>
                        <%--                        </label>--%>
                        <%--                        <div class="form-group ">--%>
                        <%--                            <div class="" id='total-seat-div'>--%>
                        <%--                                <span id="total-seat">--%>

                        <%--                                </span>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>


                        <%--                        <label class="form-group control-label"><%=Language.equalsIgnoreCase("English") ? "Available Seat" : "ফাকা সিট"%>--%>
                        <%--                        </label>--%>
                        <%--                        <div class="form-group ">--%>
                        <%--                            <div class="" id='available-seat-div'>--%>
                        <%--                                <span id="available-seat">--%>

                        <%--                                </span>--%>
                        <%--                            </div>--%>
                        <%--                        </div>--%>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="modal_dismiss"
                        onclick="modalDismiss()"><%=Language.equalsIgnoreCase("English") ? "CANCEL" : "বাতিল করুন"%>
                </button>
                <button type="button" class="btn btn-primary" id="modal_submit"
                        onclick="modalDataSubmit()"><%=Language.equalsIgnoreCase("English") ? "ISSUE GALLERY" : "গ্যালারি ইস্যু করুন"%>
                </button>
            </div>
        </div>
    </div>
</div>


<script>

    let global_gallery_dto, global_approval_button, global_dismissal_button, global_processing_button,
        global_gate_pass_id, global_gate_pass_gallery_id = 0;

    function populateGalleryModal() {
        let galleryId = $('#parliament-gallery').val();
        let galleryIds = global_gallery_dto.galleryIds, totalSeats = global_gallery_dto.totalSeatAllGallery,
            availableSeats = global_gallery_dto.availableSeatAllGallery;
        for (let key in galleryIds) {
            console.log(key, galleryIds[key], galleryId);
            if (galleryIds[key] == galleryId) {

                <%
                    if(!Language.equalsIgnoreCase("English")) {
                %>
                $('#total-seat').text(convertToBanglaNumber(totalSeats[key]));
                $('#available-seat').text(convertToBanglaNumber(availableSeats[key]));
                <%
                    } else {
                %>
                $('#total-seat').text(totalSeats[key]);
                $('#available-seat').text(availableSeats[key]);
                <%
                    }
                %>

                global_gate_pass_gallery_id = galleryId;
                if (availableSeats[key] == 0) {
                    $('#modal_submit').prop("disabled", true);
                }
                break;
            }
        }
    }

    function approval(approve_button_id, dismiss_button_id, process_button_id, gate_pass_id, gate_pass_type, visiting_date) {

        if (gate_pass_type == "parliament gallery pass") {
            global_approval_button = approve_button_id;
            global_dismissal_button = dismiss_button_id;
            global_processing_button = process_button_id;
            global_gate_pass_id = gate_pass_id;

            let galleryURL = "Parliament_galleryServlet?actionType=getGatePassGalleryModel" + "&visitingDate=" + visiting_date + "&language=" + '<%=Language%>';
            $.ajax({
                url: galleryURL,
                type: "GET",
                async: true,
                success: function (data) {
                    let dto = JSON.parse(data);
                    global_gallery_dto = dto;
                    $('#parliament-gallery').html(dto.options);
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                    $('#select-gallery-modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            });
        } else {

            let approval_status_id = approve_button_id.replace('_button_approve', '') + '_approvalStatus';

            let url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=approved";
            $.ajax({
                url: url,
                type: "POST",
                async: true,
                success: function (data) {

                    $('#' + approve_button_id).prop("disabled", true);
                    $('#' + dismiss_button_id).prop("disabled", false);
                    $('#' + process_button_id).prop("disabled", false);
                    $('#' + approval_status_id).html('<%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>');
                    $('#' + approval_status_id).removeClass("text-danger text-primary text-info").addClass("text-success");
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        text: '<%=Language.equalsIgnoreCase("English") ? "Requested gate pass has been approved successfully" : "প্রবেশ পাসটি অনুমোদিত হয়েছে"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            });
        }

    }


    function dismissal(dismiss_button_id, approve_button_id, process_button_id, gate_pass_id, approval_status, gate_pass_type) {
        let approval_status_id = approve_button_id.replace('_button_approve', '') + '_approvalStatus';
        let remarks = '';
        // taking the reason of the dismiss
        Swal.fire({
            title: '<%=Language.equalsIgnoreCase("English") ? "Remarks" : "মন্তব্য"%>',
            text: '<%=Language.equalsIgnoreCase("English") ? "Why do you dismiss the Gate Pass request?" : "আপনি কেন গেইট পাসটির অনুমোদন বাতিল করেছেন?"%>',
            input: 'text',
            type: 'info',
            showCancelButton: false,
            confirmButtonText: '<%=Language.equalsIgnoreCase("English") ? "OK" : "ঠিক আছে"%>',
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                remarks = result.value;
            }

            let url;
            if (approval_status == 1 && gate_pass_type == "parliament gallery pass") {
                console.log("here!");
                url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss&gallery=gallery&remarks=" + remarks;
            } else {
                url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss&remarks=" + remarks;
            }

            $.ajax({
                url: url,
                type: "POST",
                async: true,
                success: function (data) {
                    $('#' + dismiss_button_id).prop("disabled", true);
                    $('#' + approve_button_id).prop("disabled", false);
                    $('#' + process_button_id).prop("disabled", false);
                    $('#' + approval_status_id).html('<%=Language.equalsIgnoreCase("English") ? "Dismissed" : "অননুমোদিত"%>');
                    $('#' + approval_status_id).removeClass("text-success text-primary text-info").addClass("text-danger");
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                    Swal.fire({
                        position: 'center',
                        type: 'warning',
                        text: '<%=Language.equalsIgnoreCase("English") ? "Requested gate pass has been dismissed successfully" : "প্রবেশ পাসটির অনুমোদন বাতিল করা হয়েছে"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            });

        });
    }


    function processing(process_button_id, approve_button_id, dismiss_button_id, gate_pass_id, approval_status, gate_pass_type) {
        let approval_status_id = approve_button_id.replace('_button_approve', '') + '_approvalStatus';
        let url;
        if (approval_status == 1 && gate_pass_type == "parliament gallery pass") {
            console.log('HERE BOY!');
            url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=process&gallery=gallery";
        } else {
            url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=process";
        }
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {

                $('#' + approve_button_id).prop("disabled", false);
                $('#' + dismiss_button_id).prop("disabled", false);
                $('#' + process_button_id).prop("disabled", true);
                $('#' + approval_status_id).html('<%=Language.equalsIgnoreCase("English") ? "Processing" : "প্রক্রিয়াধীন"%>');
                $('#' + approval_status_id).removeClass("text-success text-primary text-danger").addClass("text-info");
            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                Swal.fire({
                    position: 'center',
                    type: 'warning',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Requested gate pass has been hold for processing! " : "প্রবেশ পাসটি প্রক্রিয়াধীন অবস্থায় আছে!"%>',
                    showConfirmButton: false,
                    timer: 3000
                });
            }
        });
    }


    function modalDataSubmit() {
        // Hide the model
        $('#select-gallery-modal').modal('hide');
        // reset data
        $('#total-seat').text(' ');
        $('#available-seat').text(' ');


        let approval_status_id = global_approval_button.replace('_button_approve', '') + '_approvalStatus';

        let url = "Gate_passServlet?actionType=gatePassSecondLayerApproval" + "&gatePassId=" + global_gate_pass_id + "&approvalstatus=approved" + "&galleryId=" + global_gate_pass_gallery_id + "&method=subtract";
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {

                $('#' + global_approval_button).prop("disabled", true);
                $('#' + global_dismissal_button).prop("disabled", false);
                $('#' + global_processing_button).prop("disabled", false);
                $('#' + approval_status_id).html('<%=Language.equalsIgnoreCase("English") ? "Approved" : "অনুমোদিত"%>');
                $('#' + approval_status_id).removeClass("text-info text-primary text-danger").addClass("text-success");
            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                Swal.fire({
                    position: 'center',
                    type: 'success',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Requested gate pass has been approved successfully" : "প্রবেশ পাসটি অনুমোদিত হয়েছে"%>',
                    showConfirmButton: false,
                    timer: 3000
                });
            }
        });
    }

    function modalDismiss() {
        // Hide the model
        $('#select-gallery-modal').modal('hide');
        // reset data
        $('#total-seat').text(' ');
        $('#available-seat').text(' ');
    }

    function convertToBanglaNumber(bangla_number) {
        let input = bangla_number + '';
        console.log(typeof input);
        let numbers = {
            '0': '০',
            '1': '১',
            '2': '২',
            '3': '৩',
            '4': '৪',
            '5': '৫',
            '6': '৬',
            '7': '৭',
            '8': '৮',
            '9': '৯'
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                // console.log("here man!" + input[i] + " " + numbers);
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }


</script>

<style>
    td {
        text-align: center;
    }
</style>