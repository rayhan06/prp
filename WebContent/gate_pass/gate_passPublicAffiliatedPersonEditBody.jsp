<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="gate_pass_affiliated_person.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Gate_pass_affiliated_personDTO gate_pass_affiliated_personDTO;
    gate_pass_affiliated_personDTO = (Gate_pass_affiliated_personDTO) request.getAttribute("gate_pass_affiliated_personDTO");
    if (gate_pass_affiliated_personDTO == null) {
        gate_pass_affiliated_personDTO = new Gate_pass_affiliated_personDTO();

    }
    System.out.println("gate_pass_affiliated_personDTO = " + gate_pass_affiliated_personDTO);

    String Language = request.getParameter("language");
    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.GATE_PASS_AFFILIATED_PERSON_ADD_GATE_PASS_AFFILIATED_PERSON_ADD_FORMNAME, Language);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;

    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    int personCount = 0;
    if (request.getParameter("personCount") != "")
        personCount = Integer.parseInt(request.getParameter("personCount"));
    int startVal = Integer.parseInt(request.getParameter("startVal"));
    for (int x = startVal; x < personCount; x++) {
%>

<div class="affliatedPerson mt-4 col-md-12" id="affliatedPerson_div_<%=x%>">
    <div class=" row" id="person_row_div">
        <div class="col-lg-3 .englishDigitOrCharOnly<<%=x%>" id='mobileNumber_div_<%=x%>'>
            <input type='text' class='form-control mobile-number' name='mobileNumber_<%=x%>'
                   id='mobileNumber_<%=x%>'
                   placeholder='<%=Language.equalsIgnoreCase("English") ? "phone number" : "ফোন/মোবাইল নাম্বার"%>'
                   tag='pb_html'/>
            <span class="affiliated-mobile-error"
                  style="color:#ff0000; text-align:left; font-size:x-small; margin-top: auto; display: none"></span>
        </div>
        <div class="col-lg-3 " id='credentialType_div_<%=x%>'>
            <select class='form-control credential-type' name='credentialType_<%=x%>' id='credentialType_<%=x%>'
                    tag='pb_html'>
                <%=CatRepository.getInstance().buildOptions("credential", Language, null)%>
            </select>
            <span class="affiliated-type-error"
                  style="color:#ff0000; text-align:left; font-size:x-small; margin-top: auto; display: none"></span>
        </div>
        <div class="col-lg-3 .englishDigitOrCharOnly<%=x%>" id='credentialNo_div_<%=x%>'>
            <input type='text' class='form-control credential-no' name='credentialNo_<%=x%>'
                   id='credentialNo_<%=x%>'
                   placeholder='<%=Language.equalsIgnoreCase("English") ? "credential number" : "পরিচয়পত্র নাম্বার"%>'
                   tag='pb_html'/>
            <span class="affiliated-id-error"
                  style="color:#ff0000; text-align:left; font-size:x-small; margin-top: auto; display: none"></span>
        </div>
        <div class="col-lg-3 .englishDigitOrCharOnly<%=x%>" id='credentialNo_div_<%=x%>'>
            <input type='text' class='form-control name' name='name_<%=x%>'
                   id='name_<%=x%>'
                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Enter Name" : "ব্যক্তির নাম লিখুন"%>'
                   tag='pb_html'/>
            <span class="name-error"
                  style="color:#ff0000; text-align:left; font-size:x-small; margin-top: auto; display: none"></span>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $("#credentialType_<%=x%>").on("change", function () {
            const element = $(this).closest('.affliatedPerson').find('.affiliated-type-error');
            if ($(this).val().match('[0-9]+')) element.hide();
        });

        $("#credentialNo_<%=x%>").on("input", function () {
            const element = $(this).closest('.affliatedPerson').find('.affiliated-id-error');
            if ($(this).val() != "") element.hide();
        });

        $("#name_<%=x%>").on("input", function () {
            const element = $(this).closest('.affliatedPerson').find('.name-error');
            if ($(this).val() != "") element.hide();
        });

        $("#mobileNumber_<%=x%>").on("input", function () {
            <%--populateInputFields<%=x%>();--%>
            let mobileNo = $('#mobileNumber_<%=x%>').val();
            if(mobileNo.length > 11){
                mobileNo = mobileNo.substr(0,11);
            }
            $('#mobileNumber_<%=x%>').val(mobileNo);
            const element = $(this).closest('.affliatedPerson').find('.affiliated-mobile-error');
            if ($(this).val().match('^01[0-9]{9}$') || $(this).val().match('^০১[০-৯]{9}$')) element.hide();
        });

        <%--let credentialTypeOk = parseInt($('#credentialType_<%=x%>').val()) != NaN;--%>
        <%--let credentialNoOk = $('#credentialNo_<%=x%>').val().localeCompare("") != 0;--%>
        <%--let mobileNumberOk = $('#mobileNumber_<%=x%>').val().match('8801[3-9]{1}[0-9]{8}');--%>

    });

    function populateInputFields<%=x%>(event) {
        let english = /^[A-Za-z0-9]*$/;
        let mobileNo = $('#mobileNumber_<%=x%>').val();

        if (mobileNo.length === 11) {
            if (!english.test(mobileNo)) {
                mobileNo = convertToEnglishNumber(mobileNo);
                $('#mobileNumber_<%=x%>').val(mobileNo);
            }
            mobileNo = '88' + mobileNo;
            console.log(mobileNo);
            // ajax call for stored data
            let url = "Gate_passPublicServlet?actionType=getStoredDataByMobileNumberForAffiliatedPerson" + "&mobileNumber=" + mobileNo;

            $.ajax({
                url: url,
                type: "GET",
                async: true,
                success: function (data) {

                    if (data.localeCompare('noString') === -1) {

                        let dto = JSON.parse(data);

                        // Now populate data with stored data
                        $('#credentialType_<%=x%>').val(dto.credentialType);
                        $('#credentialType_<%=x%>').prop("disabled", true);

                        $('#credentialNo_<%=x%>').val(dto.credentialNo);
                        $('#credentialNo_<%=x%>').prop("readonly", true);

                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    $(".englishDigitOrCharOnly<%=x%>").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        var charStr = String.fromCharCode(ew);

        if (charStr == "0") {
            return true;
        } else if (charStr == "1") {
            return true;
        } else if (charStr == "2") {
            return true;
        } else if (charStr == "3") {
            return true;
        } else if (charStr == "4") {
            return true;
        } else if (charStr == "5") {
            return true;
        } else if (charStr == "6") {
            return true;
        } else if (charStr == "7") {
            return true;
        } else if (charStr == "8") {
            return true;
        } else if (charStr == "9") {
            return true;
        } else if (charStr >= 'A' && charStr <= 'Z'){
            return true;
        } else if (charStr >= 'a' && charStr <= 'z'){
            return true;
        }
        return false;
    });

</script>

<%
    }
%>