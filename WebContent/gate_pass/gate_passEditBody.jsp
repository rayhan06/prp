<%@page import="gate_pass_type.Gate_pass_typeDTO"%>
<%@page import="approval_execution_table.Approval_execution_tableDAO" %>
<%@page import="approval_execution_table.Approval_execution_tableDTO" %>

<%@page import="approval_path.ApprovalPathDetailsDAO" %>
<%@page import="approval_path.ApprovalPathDetailsDTO" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="com.google.gson.JsonArray" %>
<%@page import="com.google.gson.JsonParser" %>
<%@page import="dbm.DBMW" %>
<%@page import="files.FilesDAO" %>
<%@page import="gate_pass.Gate_passDTO" %>

<%@page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%@page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="parliament_gate.Parliament_gateRepository" %>

<%@page import="pb.CatDAO" %>
<%@page import="pb.CommonDAO" %>

<%
    Gate_passDTO gate_passDTO;
    gate_passDTO = (Gate_passDTO) request.getAttribute("gate_passDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (gate_passDTO == null) {
        gate_passDTO = new Gate_passDTO();

    }
    System.out.println("gate_passDTO = " + gate_passDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.GATE_PASS_ADD_GATE_PASS_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    if (request.getParameter("isPermanentTable") != null) {
        isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
    }

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
    ApprovalPathDetailsDTO approvalPathDetailsDTO = null;

    String tableName = "gate_pass";

    boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

    if (!isPermanentTable) {
        approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("gate_pass", gate_passDTO.iD);
        System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
        approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
        approval_execution_table_initiationDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("gate_pass", gate_passDTO.iD);
        if (approvalPathDetailsDTO != null && approvalPathDetailsDTO.organogramId == userDTO.organogramID) {
            canApprove = true;
            if (approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR) {
                canValidate = true;
            }
        }

        isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

        canTerminate = isInitiator && gate_passDTO.isDeleted == 2;
    }
    
   

    String fromGate = request.getParameter("fromGate");
    boolean isFromGate = "true".equals(fromGate);

//    Now it's time to remove attribute
    session.setAttribute("isToast", "no");
%>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="parliament_gate.Parliament_gateRepository" %>
<%@ page import="gate_pass_type.Gate_pass_typeRepository" %>
<%@ page import="gate_pass_sub_type.Gate_pass_sub_typeRepository" %>
<%@ page import="com.google.gson.JsonParser" %>
<%@ page import="com.google.gson.JsonArray" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="workflow.WorkflowController" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="employee_offices.*" %>

<%
    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String jsonDTO = Gate_pass_sub_typeRepository.getInstance().buildJSON(Language);
    JsonArray jsonArray = new JsonParser().parse(jsonDTO).getAsJsonArray();

    String loginURL = request.getRequestURL().toString();
    String context_folder = request.getContextPath();

    LM.getInstance();
    String context = "../../.." + request.getContextPath() + "/";

    String errorMessage = (Language == "English" ? "Select Referrer!" : "সুপারিশকারী নির্বাচন করুন!");

    String galleryPassDates = (String) session.getAttribute("galleryPassDates");

    boolean isLangEng = Language.equalsIgnoreCase("English");
    
    if (galleryPassDates == null) {
        galleryPassDates = isLangEng ? "<option>Select</option>" :
                "<option>বাছাই করুন</option>";
    }

    String message = null;
    if(request.getParameter("message") != null)
    {
    	int messageIndex = Integer.parseInt(request.getParameter("message"));
    	if(isLangEng)
    	{
    		message = Gate_passDTO.errorsEn[messageIndex];
    	}
    	else
    	{
    		message = Gate_passDTO.errorsBn[messageIndex];
    	}
    }
    String error = request.getParameter("error");
    boolean hasError = error != null && error.equalsIgnoreCase("1");

    String isToast = (String) session.getAttribute("isToast");
    if (isToast == null) {
        isToast = "no";
    }
    if(message != null)
    {
    	isToast = "yes";
    }

    Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
    
   
    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
    boolean isEligible = userDTO.userName.startsWith("0") || (employeeOfficeDTO.jobGradeTypeCat >= 0 && employeeOfficeDTO.jobGradeTypeCat <= 9);

%>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <img src="<%=context_folder%>/assets/images/menu_icons/1244000@2x.png" alt="prp">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Gate_passServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='id'
                                           id='id_hidden_<%=i%>' value='<%=gate_passDTO.id%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='gatePassVisitorId'
                                           id='gatePassVisitorId_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.gatePassVisitorId + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.GATE_PASS_ADD_GATEPASSTYPEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="" id='gatePassTypeId_div_<%=i%>'>
                                                <select class='form-control' name='gatePassTypeId'
                                                        id='gatePassTypeId'
                                                        tag='pb_html'>
                                                    <%=actionName.equals("edit") ? Gate_pass_typeRepository.getInstance().buildOptions(Language, gate_passDTO.gatePassTypeId) : Gate_pass_typeRepository.getInstance().buildOptions(Language, 0)%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row" id='gatePassSubTypeId_div'
                                         style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               id="gatePassSubTypeIdLabel">
                                            <%=LM.getText(LC.GATE_PASS_ADD_GATEPASSSUBTYPEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control ignore-gate-pass-subtype-validation'
                                                    name='gatePassSubTypeId'
                                                    id='gatePassSubTypeId'
                                                    tag='pb_html'>
                                                <%--<%=actionName.equals("edit") ? Gate_pass_sub_typeRepository.getInstance().buildOptions(Language,gate_passDTO.gatePassSubTypeId) : Gate_pass_sub_typeRepository.getInstance().buildOptions(Language,0)%>--%>
                                            </select>
                                            <span class="error" id="sub-pass-type-error-msg"
                                                  style="display:none; color: #fd397a; font-size: x-small">
                                                                <%=isLangEng ? "Select sub pass type!" : "পাসের উপধরণ বাছাই করুন!"%>
                                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="gallery-date" style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.GATE_PASS_ADD_VISITINGDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="" id='gallery-pass-date-div'>
                                                <select class='form-control' name='gallery-date'
                                                        id='gallery-pass-dates' tag='pb_html'>
                                                    <%=galleryPassDates%>
                                                </select>
                                                <span class="error" id="gallery-date-error-msg"
                                                      style="display:none; color: #fd397a; font-size: x-small">
                                                                <%=isLangEng ? "Select visiting date!" : "সাক্ষাতের তারিখ দিন!"%>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="default-date">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.GATE_PASS_ADD_VISITINGDATE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="" id='visitingDate_div_<%=i%>'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID"
                                                               value="visiting-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' class='form-control'
                                                       id='visiting-date' name='visitingDate' value=''
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.GATE_PASS_ADD_REFERRERID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="" id='select-referrer-div'>
                                                <input type='hidden' class='form-control'
                                                       id="referrer-id" name="referrerId" value = '<%=isEligible?userDTO.employee_record_id:""%>'
                                                       tag='pb_html'/>
                                                       
                                                 <button type="button"
                                                        class="btn text-white shadow form-control btn-border-radius"
                                                        style="background-color: #4a87e2"
                                                        onclick="addEmployee()"
                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.GATE_PASS_ADD_SELECT_REFERRER, loginDTO)%>
                                                </button>
                                                <table class="table table-bordered table-striped"
                                                       >
                                                    <tbody id="employeeToSet">
                                                    <%
                                                    if(isEligible)
                                                    {
                                                    	%>
                                                    	<tr>
                                                    		<td style = 'text-align: center !important'>
                                                    			<%=isLangEng?"Self":"নিজে" %>
                                                    		</td>
                                                    	</tr>
                                                    	<%
                                                    }
                                                    %>
                                                    </tbody>
                                                </table>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.GATE_PASS_ADD_PARLIAMENTGATEID, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="" id='parliamentGateId_div_<%=i%>'>
                                                <select class='form-control' name='parliamentGateId'
                                                        id='parliamentGateId_select2_<%=i%>' required
                                                        tag='pb_html'>
                                                    <%=Parliament_gateRepository.getInstance().buildOptions(Language, 0)%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row" style="display: none" id="reason_for_visit_div">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.GATE_PASS_PARLIAMENT_BUILDING_REASON_FOR_VISIT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='reasonForVisit'
                                                   id='reason_for_visit'
                                                   value='<%=gate_passDTO.reasonForVisit%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='validityFrom'
                                           id='validityFrom_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.validityFrom + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='validityTo'
                                           id='validityTo_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.validityTo + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='jobCat'
                                           id='jobCat_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.jobCat + "'"):("'" + "-1" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.insertedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                                   tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + gate_passDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                                   tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          
                <div class="row">
                    <div class="col-md-10 mt-3">
                        <div class="form-actions text-right">
                            <button class="btn border-0 btn-primary shadow" style="border-radius: 8px" type="submit">
                                <%=LM.getText(LC.GATE_PASS_ADD_NEXT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">
    const obj = <%=jsonArray%>;
    language = '<%=Language%>';
    var isLangEng = <%=isLangEng%>;
    
    var grade = <%=userDTO.userName.startsWith("0")?1:employeeOfficeDTO.jobGradeTypeCat%>;
    var maxGrade = <%=Gate_passDTO.MAX_NORMAL_LEVEL%>;
    $('#gatePassTypeId').on('change', (event) => {
        const typeId = event.target.value;
        let count = 0;
        $('#gatePassSubTypeId').empty();
        $('#gatePassSubTypeId').append($('<option>', {
            text: (language == 'English' ? 'Select' : 'বাছাই করুন'),
            value: ''
        }));
        obj.forEach(elem => {
            if (elem.gatePassTypeId == typeId) {
                $('#gatePassSubTypeId').append($('<option>', {
                    text: (language == 'English' ? elem.nameEng : elem.nameBng),
                    value: elem.id
                }));  //"<option value = '"+i.toString()+"'>" + (language == 'English' ? i.toString() : convDateStrToBn(i)) + "</option>";
                count++;
            }
        });

        // if(count > 0) $('#gatePassSubTypeId').prop('disabled', false);
        // else $('#gatePassSubTypeId').prop('disabled', true);

        if (count > 0) {
            $('#gatePassSubTypeId_div').show();
            $('#gatePassSubTypeIdLabel').show();
            $("#gatePassSubTypeId").select2({
                dropdownAutoWidth: true
            });
        } else {
            $('#gatePassSubTypeId_div').hide();
            $('#gatePassSubTypeIdLabel').hide();
        }

        if ($('#gatePassTypeId').val() == <%=Gate_pass_typeDTO.GALLERY_PASS%>) {
            $('#default-date').hide();
            $('#gallery-date').show();
            maxGrade = <%=Gate_passDTO.MAX_GALLERY_LEVEL%>;
        } else {
            $('#default-date').show();
            $('#gallery-date').hide();
            $('#gallery-date-error-msg').hide();
            maxGrade = <%=Gate_passDTO.MAX_NORMAL_LEVEL%>;
        }


        if ($('#gatePassTypeId').val() == '') {
            $('#tagEmp_modal_button').prop("disabled", true);
            $('.selected_employee_row').hide();
            $('#tagged_emp_table tr:visible').remove();
            $('#referrer-id').val('');
        } else {
            $('#tagEmp_modal_button').prop("disabled", false);
            $('.selected_employee_row').hide();
        }

        if ($('#gatePassTypeId').val() == 10) {
            $('#reason_for_visit_div').show();
        } else {
            $('#reason_for_visit_div').hide();
        }

    });

    $('#gatePassSubTypeId').on('change', (event) => {
        if ($("#gatePassSubTypeIdLabel").is(":visible")) {
            if ($('#gatePassSubTypeId').val()) {
                $('#sub-pass-type-error-msg').hide();
            }
        }
    });

    $(document).ready(function () {
        init();
        // $('#gatePassSubTypeId').prop('disabled', true);
        $('#gatePassSubTypeId_div').hide();
        $('#gatePassSubTypeIdLabel').hide();

        setDateByStringAndId('visiting-date-js', '<%=dateFormat.format(new Date(System.currentTimeMillis()))%>');

        $.validator.addMethod('passSelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('referrerSelection', function (value, element) {

            let referrer_id_value = document.getElementById('referrer-id').value;
            if (referrer_id_value == "" || referrer_id_value == undefined) {
                console.log("No referrer selected!");
                return false;
            } else {
                console.log("Referrer selected!");
                return true;
            }
        });

        // $.validator.addMethod('gateSelection', function (value, element) {
        //     return value != 0;
        // });

        $("#bigform").validate({
            ignore: [],
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                gatePassTypeId: {
                    required: true,
                    passSelection: true
                },
                referrerId: {
                    required: true,
                    referrerSelection: true
                }
                // ,
                // parliamentGateId: {
                //     required: true,
                //     gateSelection: true
                // }
            },

            messages: {
                gatePassTypeId: '<%=isLangEng ? "Select gate pass type!" : "পাসের ধরণ বাছাই করুন!"%>',
                <%--gatePassSubTypeId: '<%=isLangEng ? "Select gate pass sub-type!" : "পাসের উপ-ধরন বাছাই করুন!"%>',--%>
                referrerId: '<%=isLangEng ? "Select your referrer!" : "সুপারিশকারি নির্বাচন করুন!"%>'
                <%--,--%>
                <%--parliamentGateId: '<%=isLangEng ? "Select a gate!" : "কোন গেইট দিয়ে আপনি প্রবেশ করতে চান?"%>'--%>
            }
        });


        // fire a swal
        let isToast = '<%=isToast%>';
        if (isToast == 'yes') {
            $(".swal-overlay").css("background-color", "#0080ff");
            $(".swal-title").css("font-size", "32px");

            Swal.fire({
                position: 'center',
                type: '<%=hasError?"error":"success"%>',
                text: '<%=message%>',
                showConfirmButton: false,
                timer: 3000
            });
        }

        <%
            if (!isFromGate) {
        %>
        //setInitialReferrer();
        <%
            }
        %>

    });


    $(function () {



        <%
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.YEAR, 1);
            String maxVisitingDate = dateFormat.format(c.getTime());
        %>
        setMinDateById('visiting-date-js', '<%=datestr%>');
        setMaxDateById('visiting-date-js', '<%=maxVisitingDate%>');

        <%if (actionName.equals("edit")) {%>
        setDateByTimestampAndId('visiting-date-js', <%=gate_passDTO.visitingDate%>);
        <%}%>

    });

    function PreprocessBeforeSubmiting(row, validate) {
    	console.log("PreprocessBeforeSubmiting");
    	//return false;
        let validReferrer = true;
        let validGalleryDate = false;
        $('#gallery-date-error-msg').hide();
        // validate gate pass sub type
        if ($("#gatePassSubTypeIdLabel").is(":visible")) {
            if (!$('#gatePassSubTypeId').val()) {
                $('#sub-pass-type-error-msg').show();
                return false;
            } else {
                $('#sub-pass-type-error-msg').hide();
            }
        }

        // dateValidator('visiting-date-js', true);

//         if (added_employee_info_map.size !== 0) {
//             $('#organogramId_error').hide();
//             validReferrer = true;
//         } else {
//              if ($('#referrer-id').val() == '<%=userDTO.employee_record_id%>') { 
//                 $('#organogramId_error').hide();
//                 validReferrer = true;
//             } else {
//                 validReferrer = false;
//                 $('#organogramId_error').show();
//             }
//         }


		if ($('#referrer-id').val() == '' || grade > maxGrade) 
		{                  
            toastr.error("Invalid Referrer");
            console.log("Invalid Referrer");
            validReferrer = false;
            return false;
        }
		else 
		{
            validReferrer = true;
                
        }


        if ($('#gatePassTypeId').val() == <%=Gate_pass_typeDTO.GALLERY_PASS%>) {
            if ($('#gallery-pass-dates').val() !== "") {
                validGalleryDate = true;
                $('#visiting-date').val($('#gallery-pass-dates').val());
            } else {
                $('#gallery-date-error-msg').show();
            }
	
            return validGalleryDate && validReferrer;
        } else {
            $('#visiting-date').val(getDateStringById('visiting-date-js'));

            return dateValidator('visiting-date-js', true, {
                'errorEn': 'Enter valid visiting date!',
                'errorBn': 'সাক্ষাতের তারিখ প্রবেশ করুন!'
            }) && validReferrer;
        }

    }


    function setInitialReferrer() {
        let index = 1;
        $("#tagged_emp_table").append('<tr id="initial-referrer">' + $(".selected_employee_row:first").html() + '</tr>');

        $('#tagged_emp_table tr:last').each(function () {
            $(this).find('td').each(function () {
                if (index == 1) {
                    this.append('<%=isLangEng ? userDTO.userName: StringUtils.convertToBanNumber(userDTO.userName)%>');
                } else if (index == 2) {
                    this.append('<%=isLangEng ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng%>');
                } else if (index == 3) {
                    this.append('<%=isLangEng ? "Own" : "স্বয়ং/নিজেই"%>');
                }
                index++;
            })
        });

        $('#referrer-id').val('<%=userDTO.employee_record_id%>');
    }

    function init() {

        $("#gatePassTypeId").select2({
            dropdownAutoWidth: true
        });

        $("#gatePassSubTypeId").select2({
            dropdownAutoWidth: true
        });

        $("#referrerId_select2_0").select2({
            dropdownAutoWidth: true
        });

       /* $("#parliamentGateId_select2_0").select2({
            dropdownAutoWidth: true
        });*/
    }

    var child_table_extra_id = <%=childTableStartingID%>;


   
    

    
    function patient_inputted(userName, orgId, erId) {
        console.log("patient_inputted " + userName + " erId = " + erId);
        $("#referrer-id").val(erId);
        
        if(!userName.startsWith('0'))
        {
        	console.log("getGrade for " + userName + " erId = " + erId);
        	 var xhttp = new XMLHttpRequest();
             xhttp.onreadystatechange = function () {
                 if (this.readyState == 4 && this.status == 200) {
                	 grade = parseInt(this.responseText);
                 	 if(grade < 0 || grade > maxGrade )
                 	 {
                 		 if(isLangEng)
               			 {
               			 	toastr.error("Referer Grade is " + grade + ". But it must be between 1 and " + maxGrade + ".");
               			 }
                 		 else
                 		 {
                 			toastr.error("সুপারিশকারীর গ্রেড  " + convertToBanglaNumber(grade) + "। কিন্তু গ্রেড ১ থেকে " 
							+ convertToBanglaNumber(maxGrade) + " এর ভেতর হতে হবে");
                 		 }
                 		$("#referrer-id").val('');
                 		$("#employeeToSet").html('');
                 	 }
                 } else {
                     //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
                 }
             };

             xhttp.open("GET", "Gate_passServlet?actionType=getGrade&referrerId=" + erId, true);
             xhttp.send();
        }
        else
        {
        	grade = 1;
        }
       
    }
    
    

</script>






