<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="gate_pass_item.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="pb.*" %>
<%@ page import="parliament_item.Parliament_itemRepository" %>

<%
    Gate_pass_itemDTO gate_pass_itemDTO;
    gate_pass_itemDTO = (Gate_pass_itemDTO) request.getAttribute("gate_pass_itemDTO");

    if (gate_pass_itemDTO == null) {
        gate_pass_itemDTO = new Gate_pass_itemDTO();
    }
    System.out.println("gate_pass_itemDTO = " + gate_pass_itemDTO);

    String Language = request.getParameter("language");
    CommonDAO.language = Language;
    CatDAO.language = Language;

    int rowNumber = Integer.parseInt(request.getParameter("rowNumber"));
%>

<tr id="gate_pass_item_<%=rowNumber%>">
    <td>
        <select class='form-control' name='parliamentItemId_<%=rowNumber%>' id='parliamentItemId_<%=rowNumber%>'
                tag='pb_html'>
            <%=Parliament_itemRepository.getInstance().buildOptions(Language,0)%>
        </select>
    </td>
    <td>
        <input type='number' class='form-control' name='amount_<%=rowNumber%>'
               id='amount_<%=rowNumber%>' value="1" tag='pb_html'>
    </td>
    <td>
        <input type='text' class='form-control' name='description_<%=rowNumber%>'
               id='description_<%=rowNumber%>' tag='pb_html'/>
    </td>
</tr>

<script src="nicEdit.js" type="text/javascript"></script>






