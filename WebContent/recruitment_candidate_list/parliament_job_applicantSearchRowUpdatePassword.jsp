<%@page pageEncoding="UTF-8" %>

<%@page import="parliament_job_applicant.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="recruitment_job_specific_candidate_list.CandidateHelperService" %>
<%@ page import="recruitment_job_specific_candidate_list.CandidateJobWithDecisionDTO" %>
<%@ page import="recruitment_job_specific_candidate_list.CandidateCountDTO" %>
<%@ page import="config.GlobalConfigurationRepository" %>
<%@ page import="config.GlobalConfigConstants" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PARLIAMENT_JOB_APPLICANT;
    RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Parliament_job_applicantDTO parliament_job_applicantDTO = (Parliament_job_applicantDTO)request.getAttribute("parliament_job_applicantDTO");
    CommonDTO commonDTO = parliament_job_applicantDTO;
    String servletName = "Parliament_job_applicantServlet";


    System.out.println("parliament_job_applicantDTO = " + parliament_job_applicantDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Parliament_job_applicantDAO parliament_job_applicantDAO = (Parliament_job_applicantDAO)request.getAttribute("parliament_job_applicantDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    CandidateHelperService candidateHelperService = new CandidateHelperService();
    List<CandidateJobWithDecisionDTO> candidateJobWithDecisionDTOS =
            (candidateHelperService.getJobWithLevelInfo(parliament_job_applicantDTO.iD));

%>


<td id = '<%=i%>_userId'>
    <%
        value = parliament_job_applicantDTO.userId + "";
    %>

    <%=value%>


</td>


<td id = '<%=i%>_nameBn'>
    <%
        value = parliament_job_applicantDTO.nameBn + "";
    %>

    <%=value%>


</td>

<td id = '<%=i%>_nameEn'>
    <%
        value = parliament_job_applicantDTO.nameEn + "";
    %>

    <%=value%>


</td>


<td id = '<%=i%>_fatherName'>
    <%
        value = parliament_job_applicantDTO.fatherName + "";
    %>

    <%=value%>
</td>


<td id = '<%=i%>_motherName'>
    <%
        value = parliament_job_applicantDTO.motherName + "";
    %>

    <%=value%>


</td>

<td id = '<%=i%>_contactNumber'>
    <%
        value = parliament_job_applicantDTO.contactNumber + "";
    %>

    <%=value%>


</td>


<td id = '<%=i%>_email'>
    <%
        value = parliament_job_applicantDTO.email + "";
    %>

    <%=value%>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=viewMultiForm&tab=1&ID=<%=parliament_job_applicantDTO.iD%>&userId=<%=parliament_job_applicantDTO.userId%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow "
            style="background-color: #1E90FF;color: white;border-radius: 8px;cursor: pointer;"
            onclick="updatePassword(<%=parliament_job_applicantDTO.iD%>)"
    >
        <%=UtilCharacter.getDataByLanguage(Language, "আপডেট", "Update")%>
    </button>
</td>


