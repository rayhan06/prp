﻿<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="util.RecordNavigator" %>


<%


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);


    RecordNavigator rn = new RecordNavigator();
    request.getSession().setAttribute(SessionConstants.NAV_PARLIAMENT_JOB_APPLICANT, rn);
    String context = "../../.." + request.getContextPath() + "/";
    int pagination_number = 0;

%>

<!--new layout start-->

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.CANDIDATE_LIST_TITLE, loginDTO)%>
                </h3>
            </div>
        </div>
        <form id="candidate-form" name="candidate-form" method="POST">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.CANDIDATE_LIST_TITLE, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_NAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text" class='form-control' name='name' id='name'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_PHONE_NUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="number" class='form-control' name='phoneNumber'
                                                   id='phoneNumber' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 control-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_NID, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="number" class='form-control' name='nid' id='nid'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10 form-actions text-right">
                        <a style="color: white;background-color: #00a1d4;cursor: pointer;"
                           class="btn btn-sm text-white shadow btn-border-radius" onclick="onSubmit('')">
                            <i class="fas fa-spinner"></i>
                            <%=LM.getText(LC.CANDIDATE_LIST_SEARCH, loginDTO)%>
                        </a>
                    </div>
                </div>
                <div class="mt-5" id='data-div-parent' style="display: none">
                    <div id='data-div' class="text-nowrap">
                        <% pagination_number = 1;%>
                    </div>
                    <%@include file="../common/pagination_with_go2.jsp" %>
                </div>
            </div>
        </form>
    </div>
</div>

<!--new layout end-->

<%--<div class="box box-primary">--%>
<%--    <div class="box-header with-border">--%>
<%--        <h3 class="box-title"><i class="fa fa-gift"></i>--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_TITLE, loginDTO)%>--%>
<%--        </h3>--%>

<%--    </div>--%>

<%--    <div class="box-body">--%>
<%--        <form id = "candidate-form" name = "candidate-form" method="POST" >--%>
<%--            <div class="form-body">--%>

<%--                <div class="form-row">--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>&ndash;%&gt;--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_NAME, loginDTO)%>--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <input type="text" class='form-control'  name='name' id = 'name'   tag='pb_html'>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                            &lt;%&ndash;                            <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>&ndash;%&gt;--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_PHONE_NUMBER, loginDTO)%>--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <input type="number" class='form-control'  name='phoneNumber' id = 'phoneNumber'   tag='pb_html'>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                            &lt;%&ndash;                            <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>&ndash;%&gt;--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_NID, loginDTO)%>--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <input type="number" class='form-control'  name='nid' id = 'nid'   tag='pb_html'>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>


<%--                </div>--%>


<%--                <div style="" id = "submit-button-div" class="form-actions text-center">--%>
<%--                    <a style="color: white"  class="btn btn-success"  onclick="onSubmit('')">--%>

<%--                        <%=LM.getText(LC.CANDIDATE_LIST_SEARCH, loginDTO)%>--%>
<%--&lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_SUBMIT_BUTTON, loginDTO)%>&ndash;%&gt;--%>

<%--                    </a>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </form>--%>


<%--<br>--%>

<%--        <div id = 'data-div-parent' style="display: none">--%>
<%--            <div id = 'data-div' >--%>
<%--                <% pagination_number = 1;%>--%>
<%--            </div>--%>
<%--            <%@include file="../common/pagination_with_go2.jsp"%>--%>

<%--        </div>--%>


<%--    </div>--%>
<%--</div>--%>


<script type="text/javascript">

    let language = '<%=Language%>';

    function convertToBn(value) {
        if (language == 'English') {
            return value;
        }

        return toBn(value);
    }

    let numberBangla = {
        '0': '০',
        '1': '১',
        '2': '২',
        '3': '৩',
        '4': '৪',
        '5': '৫',
        '6': '৬',
        '7': '৭',
        '8': '৮',
        '9': '৯'
    };

    function toBn(retStr) {
        retStr = retStr + "";
        for (let x in numberBangla) {
            retStr = retStr.replace(new RegExp(x, 'g'),
                numberBangla[x]);
        }
        return retStr;
    }

    function showDataDiv() {
        document.getElementById('data-div-parent').style.display = 'block';
    }

    function hideDataDiv() {
        document.getElementById('data-div-parent').style.display = 'none';
    }

    function onSubmit(params) {
        let name = document.getElementById('name').value;
        let phoneNo = document.getElementById('phoneNumber').value;
        let nid = document.getElementById('nid').value;

        // console.log({name, phoneNo, nid});

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('data-div').innerHTML = this.responseText;
                setPageNo();
                showDataDiv();
                // getAllDataCount();

            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        params = params + "&name=" + name.toString().trim()
            + "&phoneNo=" + phoneNo.toString().trim()
            + "&nid=" + nid.toString().trim();
        //     + "&level=" + levelValue;
        let prefixParam = "RecruitmentJobSpecificCandidateListServlet?actionType=getDataForCandidateSearch" + params;
        // console.log(prefixParam)

        xhttp.open("Get", prefixParam, true);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number) {
        // console.log('came here')


        let params = '&search=true&ajax=true';

        // var extraParams = document.getElementsByName('extraParam');
        // extraParams.forEach((param) => {
        //     params += "&" + param.getAttribute("tag") + "=" + param.value;
        // })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            // console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[0].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        // console.log(params)
        onSubmit(params);

    }

</script>
