<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_bulk_mark_entry.Recruitment_bulk_mark_entryDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.CANDIDATE_LIST_DECISION, loginDTO);


    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ROLLNUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.CANDIDATE_LIST_DECISION_REMARKS, loginDTO)%>
            </th>

        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute("job_applicant_applicationDTOS");

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        System.out.println("In jsp, parsed dto = " + data.get(i));
                        Job_applicant_applicationDTO job_applicant_applicationDTO = (Job_applicant_applicationDTO) data.get(i);
                        if(job_applicant_applicationDTO.rollNumber != null && !job_applicant_applicationDTO.rollNumber.equals("")){
                        long ID = job_applicant_applicationDTO.iD;
                        out.println("<tr id = 'tr_" + i + "'>");
        %>


        <%=("<td id = '" + i + "_id" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>'
               value='<%=job_applicant_applicationDTO.iD%>' tag='pb_html'/>


        <%=("</td>")%>

        <%=("<td id = '" + i + "_jobId" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='jobId' id='jobId_hidden_<%=i%>'
               value='<%=request.getParameter("job_id")%>' tag='pb_html'/>

        <%=("</td>")%>

        <%=("<td id = '" + i + "_levelId" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='levelId' id='levelId_hidden_<%=i%>'
               value='<%=request.getParameter("level_id")%>' tag='pb_html'/>


        <%=("</td>")%>

        <%=("<td id = '" + i + "_approve" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='approve' id='approve_hidden_<%=i%>'
               value='<%=request.getParameter("approve")%>' tag='pb_html'/>
        <%System.out.println("IN DECISION REVIEW APPROVE: " + request.getParameter("approve"));%>


        <%=("</td>")%>

        <%=("<td id = '" + i + "_roll'>")%>


        <div class="form-inline" id='roll_div_<%=i%>'>
            <input type='text' style="width:100%;" class='form-control' name='rollNumber'
                   id='roll_text_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.rollNumber + "'"):("'" + "0" + "'")%>
                           tag='pb_html'/>
        </div>

        <%=("</td>")%>

        <%=("<td id = '" + i + "_marks'>")%>


        <div class="form-inline" id='marks_div_<%=i%>'>
            <input type='text' style="width:100%;" class='form-control' name='decision_remarks'
                   id='marks_text_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.decision_remarks + "'"):("'" + "0" + "'")%>
                           tag='pb_html'/>
        </div>

        <%=("</td>")%>


        <%=("<td id = '" + i + "_insertionDate" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='insertionDate' id='insertionDate_hidden_<%=i%>'
               value='<%=job_applicant_applicationDTO.insertionDate%>' tag='pb_html'/>


        <%=("</td>")%>

        <%=("<td id = '" + i + "_insertedBy" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='insertedBy' id='insertedBy_hidden_<%=i%>'
               value='<%=job_applicant_applicationDTO.insertedByUserId%>' tag='pb_html'/>


        <%=("</td>")%>

        <%=("<td id = '" + i + "_modifiedBy" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='modifiedBy' id='modifiedBy_hidden_<%=i%>'
               value='<%=job_applicant_applicationDTO.modifiedBy%>' tag='pb_html'/>


        <%=("</td>")%>

        <%=("<td id = '" + i + "_isDeleted" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
               value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.isDeleted + "'"):("'" + "false" + "'")%>
                       tag='pb_html'/>


        <%=("</td>")%>

        <%=("<td id = '" + i + "_lastModificationTime" + "' style='display:none;'>")%>


        <input type='hidden' class='form-control' name='lastModificationTime'
               id='lastModificationTime_hidden_<%=i%>'
               value='<%=job_applicant_applicationDTO.lastModificationTime%>' tag='pb_html'/>


        <%=("</td>")%>

        <%
                        out.println("</tr>");
                    }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        %>
        </tbody>
    </table>
</div>