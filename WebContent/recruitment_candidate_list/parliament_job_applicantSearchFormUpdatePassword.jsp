<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="parliament_job_applicant.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.UtilCharacter" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Parliament_job_applicantDAO parliament_job_applicantDAO = (Parliament_job_applicantDAO) request.getAttribute("parliament_job_applicantDAO");


    String navigator2 = SessionConstants.NAV_PARLIAMENT_JOB_APPLICANT;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>

<div class="row">
    <div class="col-lg-12">
        <div class="kt-portlet shadow-none" style="margin-top: -20px">

            <div class="kt-portlet__body form-body">


                <div class="table-responsive">
                    <table id="tableData" class="table table-bordered table-striped">
                        <thead class="thead-light">
                        <tr>
                            <th style="text-align:left"><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_USERID, loginDTO)%>
                            </th>
                            <th style="text-align:left"><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NAMEBN, loginDTO)%>
                            </th>
                            <th style="text-align:left"><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NAMEEN, loginDTO)%>
                            </th>
                            <th style="text-align:left"><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_FATHERNAME, loginDTO)%>
                            </th>
                            <th style="text-align:left"><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_MOTHERNAME, loginDTO)%>
                            </th>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_MARITALSTATUSCAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_SPOUSENAME, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_DATEOFBIRTH, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_GENDERCAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_ETHNICMINORITYCAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_FREEDOMFIGHTERCAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NATIONALITYCAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_DISABILITYCAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_SPECIALQUOTACAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_HEIGHT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_WEIGHT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_CHEST, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_IDENTIFICATIONTYPECAT, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_IDENTIFICATIONNO, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_PERMANENTADDRESS, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_PRESENTADDRESS, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_HOMEDISTRICTNAME, loginDTO)%></th>--%>
                            <th style="text-align:left"><%=UtilCharacter.getDataByLanguage(Language, "ফোন/ইউজার নেম", "Phone/Username")%>
                            </th>
                            <th style="text-align:left"><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_EMAIL, loginDTO)%>
                            </th>
                            <th style="text-align:left"><%=UtilCharacter.getDataByLanguage(Language, "বিস্তারিত", "View Details")%>
                            </th>
                            <th style="text-align:left"><%=UtilCharacter.getDataByLanguage(Language, "আপডেট পাসওয়ার্ড", "Update Password")%>
                            </th>
                            <%--								<th style="text-align:left"> <%=LM.getText(LC.CANDIDATE_LIST_NID, loginDTO)%> </th>--%>
                            <%--								<th style="text-align:left"> <%=LM.getText(LC.CANDIDATE_LIST_APPLIED_JOBS, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_EMAIL, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_USERID, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>--%>
                            <%--								<th><%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_SEARCH_PARLIAMENT_JOB_APPLICANT_EDIT_BUTTON, loginDTO)%></th>--%>
                            <%--								<th><input type="submit" class="btn btn-xs btn-danger" value="<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_SEARCH_PARLIAMENT_JOB_APPLICANT_DELETE_BUTTON, loginDTO)%>" /></th>--%>


                        </tr>
                        </thead>
                        <tbody>
                        <%
                            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_PARLIAMENT_JOB_APPLICANT);

                            try {

                                if (data != null) {
                                    int size = data.size();
                                    System.out.println("data not null and size = " + size + " data = " + data);
                                    for (int i = 0; i < size; i++) {
                                        Parliament_job_applicantDTO parliament_job_applicantDTO = (Parliament_job_applicantDTO) data.get(i);


                        %>
                        <tr id='tr_<%=i%>'>
                            <%

                            %>


                            <%
                                request.setAttribute("parliament_job_applicantDTO", parliament_job_applicantDTO);
                            %>

                            <jsp:include page="parliament_job_applicantSearchRowUpdatePassword.jsp">
                                <jsp:param name="pageName" value="searchrow"/>
                                <jsp:param name="rownum" value="<%=i%>"/>
                            </jsp:include>


                            <%

                            %>
                        </tr>
                        <%
                                    }

                                    System.out.println("printing done");
                                } else {
                                    System.out.println("data  null");
                                }
                            } catch (Exception e) {
                                System.out.println("JSP exception " + e);
                            }
                        %>


                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<style>
    .password-label{
        color:black;
    }
    .auto-generate-button {
        background-color: white;
        color: black;
        border: 2px solid #3085D6;
        padding: 10px 16px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 12px;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
    }

    .auto-generate-button:hover {
        background-color: #3085D6;
        color: white;
    }
</style>



<script>
    function createRandomPassword(passwordSize) {
        let password = "";
        let char = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'
            , 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '1', '2', '3', '4', '5', '6', '7', '8', '9']

        let charSize = char.length;

        for (let i = 0; i < passwordSize; i++) {
            password += char[Math.floor(Math.random() * charSize)];
        }

        document.getElementById("password").value = password;
        document.getElementById("confirm-password").value = password;

        console.log("auto genereated password: " + password);
        return password;
    }

    $(document).on('click', "#generate-password", function () {
        let password = createRandomPassword(8);
    });


    async function updatePassword(parliamentJobApplicantId) {
        const {value: formValues} = await Swal.fire({
            title: '<%=UtilCharacter.getDataByLanguage(Language, "পাসওয়ার্ড আপডেট করুন", "Update Password:")%>',
            confirmButtonText: '<%=UtilCharacter.getDataByLanguage(Language, "সেভ করুন", "Save")%>',
            html:
                `<div>
                    <div>
                        <label class="password-label" for="password">
                        <%=UtilCharacter.getDataByLanguage(Language, "পাসওয়ার্ড ", "Password:")%></label>
                        <input id="password" class="swal2-input">
                        <label class="password-label" for="confirm-password">
                            <%=UtilCharacter.getDataByLanguage(Language, "কনফার্ম পাসওয়ার্ড ", "Confirm Password:")%></label>
                        <input id="confirm-password" class="swal2-input">
                    </div>
                    <button class="auto-generate-button" id="generate-password" type="button">
                        <%=UtilCharacter.getDataByLanguage(Language, "জেনারেট  পাসওয়ার্ড ", "Generate Paaword")%></button>
                </div>`,
            focusConfirm: false,
            preConfirm: () => {
                let password = document.getElementById('password').value;
                let confirmPassword = document.getElementById('confirm-password').value;
                if (!password || password === "") {
                    showToastSticky("পাসওয়ার্ড লিখুন", "Please enter password");
                    return false;
                } else if (password !== confirmPassword) {
                    showToastSticky("পাসওয়ার্ড মিলেনি", "Password Mismatch");
                    return false;
                } else {
                    return [
                        password,
                        confirmPassword
                    ]
                }
            }
        })

        submitChangePassword({
            "password": formValues[0],
            "confirmPassword": formValues[1],
            "parliamentJobApplicantId": parliamentJobApplicantId
        });

    }

    function submitChangePassword(jsonData) {
        console.log(jsonData);
        $.ajax({
            type: "POST",
            url: "Parliament_job_applicantServlet?actionType=updatePassword",
            data: jsonData,
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 200) {
                    Swal.fire({
                        type: 'success',
                        title: '<%=UtilCharacter.getDataByLanguage(Language, "সফল", "Successful")%>',
                        text: '<%=UtilCharacter.getDataByLanguage(Language, "পাসওয়ার্ড আপডেট হয়েছে", "Password Updated")%>',
                        confirmButtonText: '<%=UtilCharacter.getDataByLanguage(Language, "ক্যানসেল করুন", "Cancel")%>',
                    })
                } else {
                    Swal.fire({
                        type: 'error',
                        title: '<%=UtilCharacter.getDataByLanguage(Language, "দুঃখিত...", "Sorry...")%>',
                        text: '<%=UtilCharacter.getDataByLanguage(Language, "অনাকাঙ্খিত কিছু ভুল হয়েছে!", "Something went wrong!")%>',
                        confirmButtonText: '<%=UtilCharacter.getDataByLanguage(Language, "ক্যানসেল করুন", "Cancel")%>',
                    })
                }
            },
            error: function () {
                Swal.fire({
                    icon: 'error',
                    title: '<%=UtilCharacter.getDataByLanguage(Language, "দুঃখিত...", "Sorry...")%>',
                    text: '<%=UtilCharacter.getDataByLanguage(Language, "অনাকাঙ্খিত কিছু ভুল হয়েছে!", "Something went wrong!")%>',
                    confirmButtonText: '<%=UtilCharacter.getDataByLanguage(Language, "ক্যানসেল করুন", "Cancel")%>',
                })
            }
        });

    }

</script>


			