<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "Parliament_job_applicantServlet?actionType=search";
    String navigator = SessionConstants.NAV_PARLIAMENT_JOB_APPLICANT;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>
<jsp:include page="./parliament_job_applicantNav.jsp" flush="true">
    <jsp:param name="url" value="<%=url%>"/>
    <jsp:param name="navigator" value="<%=navigator%>"/>
    <jsp:param name="pageName"
               value="<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_SEARCH_PARLIAMENT_JOB_APPLICANT_SEARCH_FORMNAME, loginDTO)%>"/>
</jsp:include>

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
            <i class="fa fa-gift"></i>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid pt-0" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <form action="Parliament_job_applicantServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                          method="POST" id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="parliament_job_applicantSearchFormUpdatePassword.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_SEARCH_PARLIAMENT_JOB_APPLICANT_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<%--<div class="portlet box">--%>
<%--    <div class="portlet-body">--%>
<%--        <form action="Parliament_job_applicantServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"--%>
<%--              method="POST" id="tableForm" enctype="multipart/form-data">--%>
<%--            <jsp:include page="parliament_job_applicantSearchForm.jsp" flush="true">--%>
<%--                <jsp:param name="pageName"--%>
<%--                           value="<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_SEARCH_PARLIAMENT_JOB_APPLICANT_SEARCH_FORMNAME, loginDTO)%>"/>--%>
<%--            </jsp:include>--%>
<%--        </form>--%>
<%--    </div>--%>
<%--</div>--%>

<% pagination_number = 1;%>
<%@include file="../common/pagination_with_go2.jsp" %>
<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#tableForm').submit(function (e) {
            var currentForm = this;
            var selected = false;
            e.preventDefault();
            var set = $('#tableData').find('tbody > tr > td:last-child input[type="checkbox"]');
            $(set).each(function () {
                if ($(this).prop('checked')) {
                    selected = true;
                }
            });
            if (!selected) {
                bootbox.confirm("Select rows to delete!", function (result) {
                });
            } else {
                bootbox.confirm("Are you sure you want to delete the record(s)?", function (result) {
                    if (result) {
                        currentForm.submit();
                    }
                });
            }
        });


        $(document).on("click", '.chkEdit', function () {

            $(this).toggleClass("checked");
        });

        $('.datepicker').datepicker({

            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            todayHighlight: true,
            showButtonPanel: true
        });
    });

</script>


