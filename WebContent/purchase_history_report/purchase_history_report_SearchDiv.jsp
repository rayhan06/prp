<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PURCHASE_HISTORY_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.PURCHASE_HISTORY_REPORT_WHERE_PURCHASEORDERNUMBER, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='purchaseOrderNumber' id = 'purchaseOrderNumber' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.PURCHASE_HISTORY_REPORT_WHERE_FISCALYEARTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='fiscalYearType' id = 'fiscalYearType' >		
						<%		
						Options = CommonDAO.getOptions(Language, "fiscal_year", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.PURCHASE_HISTORY_REPORT_WHERE_PIVENDORAUCTIONEERDETAILSTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='piVendorAuctioneerDetailsType' id = 'piVendorAuctioneerDetailsType' >		
						<%		
						Options = CommonDAO.getOptions(Language, "pi_vendor_auctioneer_details", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
    </div>
</div>
<script type="text/javascript">
function init()
{
	addables = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0];
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
</script>