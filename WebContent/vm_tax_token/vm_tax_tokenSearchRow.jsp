<%@page pageEncoding="UTF-8" %>

<%@page import="vm_tax_token.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDTO" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_TAX_TOKEN_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_TAX_TOKEN;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_tax_tokenDTO vm_tax_tokenDTO = (Vm_tax_tokenDTO)request.getAttribute("vm_tax_tokenDTO");
CommonDTO commonDTO = vm_tax_tokenDTO;
String servletName = "Vm_tax_tokenServlet";


System.out.println("vm_tax_tokenDTO = " + vm_tax_tokenDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_tax_tokenDAO vm_tax_tokenDAO = (Vm_tax_tokenDAO)request.getAttribute("vm_tax_tokenDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

	Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
	List<Fiscal_yearDTO> fiscal_yearDTOS = fiscal_yearDAO.getAllFiscal_year(true);
	Fiscal_yearDTO fiscal_yearDTO ;

%>
		
											<td id = '<%=i%>_fiscalYearId'>
											<%
												fiscal_yearDTO = fiscal_yearDAO.getFiscalYearFromList(fiscal_yearDTOS,vm_tax_tokenDTO.fiscalYearId);
												if(fiscal_yearDTO!=null){
													value = Language.equalsIgnoreCase("english")?fiscal_yearDTO.nameEn:fiscal_yearDTO.nameBn ;
												}
												else{
													value="";
												}

											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_vehicleTypeCat'>
											
				
			
											</td>
											<td id = '<%=i%>_vehicleTypeCat'>



											</td>
											<td id = '<%=i%>_vehicleTypeCat'>



											</td>
		
		
		
		
		
		
		
		
		
		
		
	

											<td>
												<button
														type="button"
														class="btn-sm border-0 shadow bg-light btn-border-radius"
														style="color: #ff6b6b;"
														onclick="location.href='Vm_tax_tokenServlet?actionType=view&ID=<%=vm_tax_tokenDTO.iD%>'"
												>
													<i class="fa fa-eye"></i>
												</button>												
											</td>
	
											<td id = '<%=i%>_Edit'>
												<button
														type="button"
														class="btn-sm border-0 shadow btn-border-radius text-white"
														style="background-color: #ff6b6b;"
														onclick="location.href='Vm_tax_tokenServlet?actionType=AllVehicleSearch&fiscalYearID=<%=vm_tax_tokenDTO.fiscalYearId%>'"
												>
													<i class="fa fa-edit"></i>
												</button>																			
											</td>											
											
											
											<td id='<%=i%>_checkbox' class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_tax_tokenDTO.iD%>'/></span>
												</div>
											</td>
																						
											

