<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_tax_token.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="java.util.stream.Collectors" %>

<%
    Vm_tax_tokenDTO vm_tax_tokenDTO;
    vm_tax_tokenDTO = (Vm_tax_tokenDTO) request.getAttribute("vm_tax_tokenDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_tax_tokenDTO == null) {
        vm_tax_tokenDTO = new Vm_tax_tokenDTO();

    }
    System.out.println("vm_tax_tokenDTO = " + vm_tax_tokenDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_tax_tokenServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_TAX_TOKEN_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

//	Fiscal_yearRepository fiscal_yearRepository =  Fiscal_yearRepository.getInstance();
//	fiscal_yearRepository.reload(true);
//	List <Fiscal_yearDTO> fiscal_years = fiscal_yearRepository.getFiscal_yearList();
//
//	for(Fiscal_yearDTO fiscal_yearDTO : fiscal_years){
//		System.out.println("fiscal: "+fiscal_yearDTO);
//	}

    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    //List<Fiscal_yearDTO> fiscal_yearDTOS = fiscal_yearDAO.getAllFiscal_year(true);
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-12 ">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_SELECT_FISCAL_YEAR, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>

                                            <div class="col-md-10">
                                                <select class='form-control' name='fiscalYearId'
                                                        id='fiscalYearId_hidden_<%=i%>' tag='pb_html'>
                                                    <%
                                                        String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                        StringBuilder option = new StringBuilder();
                                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {
                                                            //System.out.println("fiscal dto id: "+fiscal_yearDTO.id);
                                                            option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                        }
                                                        fiscalYearOptions += option.toString();
                                                    %>
                                                    <%=fiscalYearOptions%>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="mt-4 mb-4">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VEHICLETYPECAT, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_TAXTOKENFEES, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_FITNESSFEES, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_DIGITALNUMBERFEES, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VAT, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_SAME_AS, loginDTO)%>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <%
                                                        CatRepository catRepository = CatRepository.getInstance();
                                                        List<CategoryLanguageModel> modelList = catRepository.getCategoryLanguageModelList("vehicle_type");
                                                        int count = -1;
                                                        for (CategoryLanguageModel model : modelList) {
                                                            vm_tax_tokenDTO = new Vm_tax_tokenDTO();
                                                            ++count;

                                                    %>

                                                    <tr>
                                                        <input type='hidden' class='form-control' name='iD'
                                                               id='iD_hidden_<%=count%>' value='<%=vm_tax_tokenDTO.iD%>'
                                                               tag='pb_html'/>

                                                        <td>
                                                            <input type='hidden' class='form-contro'
                                                                   name='vehicleTypeCat'
                                                                   id='vehicleTypeCat_category_<%=count%>'
                                                                   value='<%=model.categoryValue%>' tag='pb_html'>
                                                            <%
                                                                value = isLanguageEnglish ? model.englishText : model.banglaText;
                                                            %>
                                                            <%=value%>
                                                        </td>
                                                        <%value = "0";%>
                                                        <td>
                                                            <input type='number' class='form-control w-auto' step="0.001"
                                                                   name='taxTokenFees'
                                                                   id='taxTokenFees_number_<%=count%>'
                                                                   value='<%=value%>' tag='pb_html'>
                                                        </td>
                                                        <td>
                                                            <input type='number' class='form-control w-auto' step="0.001"
                                                                   name='fitnessFees' id='fitnessFees_number_<%=count%>'
                                                                   value='<%=value%>' tag='pb_html'>
                                                        </td>
                                                        <td>
                                                            <input type='number' class='form-control w-auto' step="0.001"
                                                                   name='digitalNumberFees'
                                                                   id='digitalNumberFees_number_<%=count%>'
                                                                   value='<%=value%>' tag='pb_html'>
                                                        </td>
                                                        <td>
                                                            <input type='number' class='form-control w-auto' step="0.001"
                                                                   name='vat' id='vat_number_<%=count%>'
                                                                   value='<%=value%>' tag='pb_html'>
                                                        </td>

                                                        <td>

                                                            <%
                                                                if (true) {

                                                            %>

                                                            <select class='form-control w-auto'
                                                                    onchange="sameAsVehicleCat(this)"
                                                                    id='vehicleTypeCat_same_category_<%=count%>'
                                                                    tag='pb_html'>
                                                                <%
                                                                    Options = CatRepository.getInstance().buildOptions("vehicle_type", Language, vm_tax_tokenDTO.vehicleTypeCat);
                                                                %>
                                                                <%=Options%>
                                                            </select>
                                                            <%}%>


                                                        </td>

                                                    </tr>


                                                    <%}%>

                                                </table>
                                            </div>
                                        </div>
                                        <%--													<div class="form-group row">--%>
                                        <%--                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VEHICLETYPECAT, loginDTO)%></label>--%>
                                        <%--                                                            <div class="col-8">--%>
                                        <%--																<select class='form-control'  name='vehicleTypeCat' id = 'vehicleTypeCat_category_<%=i%>'   tag='pb_html'>		--%>
                                        <%--																<%--%>
                                        <%--																	Options = CatRepository.getInstance().buildOptions("vehicle_type", Language, vm_tax_tokenDTO.vehicleTypeCat);--%>
                                        <%--																%>--%>
                                        <%--																<%=Options%>--%>
                                        <%--																</select>--%>
                                        <%--	--%>
                                        <%--															</div>--%>
                                        <%--                                                      </div>									--%>
                                        <%--													<div class="form-group row">--%>
                                        <%--                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_ADD_TAXTOKENFEES, loginDTO)%></label>--%>
                                        <%--                                                            <div class="col-8">--%>
                                        <%--																<%--%>
                                        <%--																	value = "";--%>
                                        <%--																	if(vm_tax_tokenDTO.taxTokenFees != -1)--%>
                                        <%--																	{--%>
                                        <%--																	value = vm_tax_tokenDTO.taxTokenFees + "";--%>
                                        <%--																	}--%>
                                        <%--																%>		--%>
                                        <%--																<input type='number' class='form-control'  name='taxTokenFees' id = 'taxTokenFees_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		--%>
                                        <%--															</div>--%>
                                        <%--                                                      </div>									--%>
                                        <%--													<div class="form-group row">--%>
                                        <%--                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_ADD_FITNESSFEES, loginDTO)%></label>--%>
                                        <%--                                                            <div class="col-8">--%>
                                        <%--																<%--%>
                                        <%--																	value = "";--%>
                                        <%--																	if(vm_tax_tokenDTO.fitnessFees != -1)--%>
                                        <%--																	{--%>
                                        <%--																	value = vm_tax_tokenDTO.fitnessFees + "";--%>
                                        <%--																	}--%>
                                        <%--																%>		--%>
                                        <%--																<input type='number' class='form-control'  name='fitnessFees' id = 'fitnessFees_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		--%>
                                        <%--															</div>--%>
                                        <%--                                                      </div>									--%>
                                        <%--													<div class="form-group row">--%>
                                        <%--                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_ADD_DIGITALNUMBERFEES, loginDTO)%></label>--%>
                                        <%--                                                            <div class="col-8">--%>
                                        <%--																<%--%>
                                        <%--																	value = "";--%>
                                        <%--																	if(vm_tax_tokenDTO.digitalNumberFees != -1)--%>
                                        <%--																	{--%>
                                        <%--																	value = vm_tax_tokenDTO.digitalNumberFees + "";--%>
                                        <%--																	}--%>
                                        <%--																%>		--%>
                                        <%--																<input type='number' class='form-control'  name='digitalNumberFees' id = 'digitalNumberFees_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		--%>
                                        <%--															</div>--%>
                                        <%--                                                      </div>									--%>
                                        <%--													<div class="form-group row">--%>
                                        <%--                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VAT, loginDTO)%></label>--%>
                                        <%--                                                            <div class="col-8">--%>
                                        <%--																<%--%>
                                        <%--																	value = "";--%>
                                        <%--																	if(vm_tax_tokenDTO.vat != -1)--%>
                                        <%--																	{--%>
                                        <%--																	value = vm_tax_tokenDTO.vat + "";--%>
                                        <%--																	}--%>
                                        <%--																%>		--%>
                                        <%--																<input type='number' class='form-control'  name='vat' id = 'vat_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		--%>
                                        <%--															</div>--%>
                                        <%--                                                      </div>									--%>


                                        <%--                                                    <input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=vm_tax_tokenDTO.searchColumn%>' tag='pb_html'/>--%>
                                        <%--													<div class="form-group row" style="display: none;">--%>
                                        <%--                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_ADD_INSERTEDBY, loginDTO)%></label>--%>
                                        <%--                                                            <div class="col-8">--%>
                                        <%--																<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=vm_tax_tokenDTO.insertedBy%>' 																  tag='pb_html'/>					--%>
                                        <%--															</div>--%>
                                        <%--                                                      </div>									--%>
                                        <%--													<div class="form-group row" style="display: none;">--%>
                                        <%--                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_TAX_TOKEN_ADD_MODIFIEDBY, loginDTO)%></label>--%>
                                        <%--                                                            <div class="col-8">--%>
                                        <%--																<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=vm_tax_tokenDTO.modifiedBy%>' 																  tag='pb_html'/>					--%>
                                        <%--															</div>--%>
                                        <%--                                                      </div>									--%>
                                        <%--														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=vm_tax_tokenDTO.insertionDate%>' tag='pb_html'/>--%>
                                        <%--														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=vm_tax_tokenDTO.isDeleted%>' tag='pb_html'/>--%>
                                        <%--											--%>
                                        <%--														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=vm_tax_tokenDTO.lastModificationTime%>' tag='pb_html'/>--%>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--               <div class="mt-4">--%>
                <%--                    <div class="form-body">--%>
                <%--                        <h5 class="table-title">--%>
                <%--                            <%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM, loginDTO)%>--%>
                <%--                        </h5>--%>
                <%--                        <div class="table-responsive">--%>
                <%--                            <table class="table table-bordered table-striped">--%>
                <%--                                <thead>--%>
                <%--									<tr>--%>
                <%--										<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_ISDIGITAL, loginDTO)%></th>--%>
                <%--										<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_TOTALVAT, loginDTO)%></th>--%>
                <%--										<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_TOTAL, loginDTO)%></th>--%>
                <%--										<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_INSERTEDBY, loginDTO)%></th>--%>
                <%--										<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_MODIFIEDBY, loginDTO)%></th>--%>
                <%--										<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_REMOVE, loginDTO)%></th>--%>
                <%--									</tr>--%>
                <%--								</thead>--%>
                <%--							<tbody id="field-VmTaxTokenItem">--%>
                <%--						--%>
                <%--						--%>
                <%--								<%--%>
                <%--									if(actionName.equals("edit")){--%>
                <%--										int index = -1;--%>
                <%--										--%>
                <%--										--%>
                <%--										for(VmTaxTokenItemDTO vmTaxTokenItemDTO: vm_tax_tokenDTO.vmTaxTokenItemDTOList)--%>
                <%--										{--%>
                <%--											index++;--%>
                <%--											--%>
                <%--											System.out.println("index index = "+index);--%>

                <%--								%>	--%>
                <%--							--%>
                <%--								<tr id = "VmTaxTokenItem_<%=index + 1%>">--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=vmTaxTokenItemDTO.iD%>' tag='pb_html'/>--%>
                <%--	--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.vehicleId' id = 'vehicleId_hidden_<%=childTableStartingID%>' value='<%=vmTaxTokenItemDTO.vehicleId%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.vmTaxTokenId' id = 'vmTaxTokenId_hidden_<%=childTableStartingID%>' value='<%=vmTaxTokenItemDTO.vmTaxTokenId%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td>										--%>


                <%--																<input type='checkbox' class='form-control-sm' name='vmTaxTokenItem.isDigital' id = 'isDigital_checkbox_<%=childTableStartingID%>' value='true' 																<%=(String.valueOf(vmTaxTokenItemDTO.isDigital).equals("true"))?("checked"):""%>--%>
                <%-- 																  tag='pb_html'>--%>
                <%--									</td>--%>
                <%--									<td>										--%>


                <%--																<%--%>
                <%--																	value = "";--%>
                <%--																	if(vmTaxTokenItemDTO.totalVat != -1)--%>
                <%--																	{--%>
                <%--																	value = vmTaxTokenItemDTO.totalVat + "";--%>
                <%--																	}--%>
                <%--																%>		--%>
                <%--																<input type='number' class='form-control'  name='vmTaxTokenItem.totalVat' id = 'totalVat_number_<%=childTableStartingID%>' value='<%=value%>'  tag='pb_html'>		--%>
                <%--									</td>--%>
                <%--									<td>										--%>


                <%--																<%--%>
                <%--																	value = "";--%>
                <%--																	if(vmTaxTokenItemDTO.total != -1)--%>
                <%--																	{--%>
                <%--																	value = vmTaxTokenItemDTO.total + "";--%>
                <%--																	}--%>
                <%--																%>		--%>
                <%--																<input type='number' class='form-control'  name='vmTaxTokenItem.total' id = 'total_number_<%=childTableStartingID%>' value='<%=value%>'  tag='pb_html'>		--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.searchColumn' id = 'searchColumn_hidden_<%=childTableStartingID%>' value='<%=vmTaxTokenItemDTO.searchColumn%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td>										--%>


                <%--																<input type='text' class='form-control'  name='vmTaxTokenItem.insertedBy' id = 'insertedBy_text_<%=childTableStartingID%>' value='<%=vmTaxTokenItemDTO.insertedBy%>' 																  tag='pb_html'/>					--%>
                <%--									</td>--%>
                <%--									<td>										--%>


                <%--																<input type='text' class='form-control'  name='vmTaxTokenItem.modifiedBy' id = 'modifiedBy_text_<%=childTableStartingID%>' value='<%=vmTaxTokenItemDTO.modifiedBy%>' 																  tag='pb_html'/>					--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.insertionDate' id = 'insertionDate_hidden_<%=childTableStartingID%>' value='<%=vmTaxTokenItemDTO.insertionDate%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=vmTaxTokenItemDTO.isDeleted%>' tag='pb_html'/>--%>
                <%--											--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=vmTaxTokenItemDTO.lastModificationTime%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td>--%>
                <%--										<span id='chkEdit'>--%>
                <%--											<input type='checkbox' name='checkbox' value='' deletecb='true'--%>
                <%--												   class="form-control-sm"/>--%>
                <%--										</span>--%>
                <%--									</td>--%>
                <%--								</tr>								--%>
                <%--								<%	--%>
                <%--											childTableStartingID ++;--%>
                <%--										}--%>
                <%--									}--%>
                <%--								%>						--%>
                <%--						--%>
                <%--								</tbody>--%>
                <%--							</table>--%>
                <%--						</div>--%>
                <%--						<div class="form-group">--%>
                <%--								<div class="col-xs-9 text-right">--%>
                <%--									<button--%>
                <%--											id="add-more-VmTaxTokenItem"--%>
                <%--											name="add-moreVmTaxTokenItem"--%>
                <%--											type="button"--%>
                <%--											class="btn btn-sm text-white add-btn shadow">--%>
                <%--										<i class="fa fa-plus"></i>--%>
                <%--										<%=LM.getText(LC.HM_ADD, loginDTO)%>--%>
                <%--									</button>--%>
                <%--									<button--%>
                <%--											id="remove-VmTaxTokenItem"--%>
                <%--											name="removeVmTaxTokenItem"--%>
                <%--											type="button"--%>
                <%--											class="btn btn-sm remove-btn shadow ml-2 pl-4">--%>
                <%--										<i class="fa fa-trash"></i>--%>
                <%--									</button>--%>
                <%--								</div>--%>
                <%--							</div>--%>
                <%--					--%>
                <%--							<%VmTaxTokenItemDTO vmTaxTokenItemDTO = new VmTaxTokenItemDTO();%>--%>
                <%--					--%>
                <%--							<template id="template-VmTaxTokenItem" >						--%>
                <%--								<tr>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.iD' id = 'iD_hidden_' value='<%=vmTaxTokenItemDTO.iD%>' tag='pb_html'/>--%>
                <%--	--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.vehicleId' id = 'vehicleId_hidden_' value='<%=vmTaxTokenItemDTO.vehicleId%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.vmTaxTokenId' id = 'vmTaxTokenId_hidden_' value='<%=vmTaxTokenItemDTO.vmTaxTokenId%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td>--%>


                <%--																<input type='checkbox' class='form-control-sm' name='vmTaxTokenItem.isDigital' id = 'isDigital_checkbox_' value='true' 																<%=(String.valueOf(vmTaxTokenItemDTO.isDigital).equals("true"))?("checked"):""%>--%>
                <%-- 																  tag='pb_html'>--%>
                <%--									</td>--%>
                <%--									<td>--%>


                <%--																<%--%>
                <%--																	value = "";--%>
                <%--																	if(vmTaxTokenItemDTO.totalVat != -1)--%>
                <%--																	{--%>
                <%--																	value = vmTaxTokenItemDTO.totalVat + "";--%>
                <%--																	}--%>
                <%--																%>		--%>
                <%--																<input type='number' class='form-control'  name='vmTaxTokenItem.totalVat' id = 'totalVat_number_' value='<%=value%>'  tag='pb_html'>		--%>
                <%--									</td>--%>
                <%--									<td>--%>


                <%--																<%--%>
                <%--																	value = "";--%>
                <%--																	if(vmTaxTokenItemDTO.total != -1)--%>
                <%--																	{--%>
                <%--																	value = vmTaxTokenItemDTO.total + "";--%>
                <%--																	}--%>
                <%--																%>		--%>
                <%--																<input type='number' class='form-control'  name='vmTaxTokenItem.total' id = 'total_number_' value='<%=value%>'  tag='pb_html'>		--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.searchColumn' id = 'searchColumn_hidden_' value='<%=vmTaxTokenItemDTO.searchColumn%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td>--%>


                <%--																<input type='text' class='form-control'  name='vmTaxTokenItem.insertedBy' id = 'insertedBy_text_' value='<%=vmTaxTokenItemDTO.insertedBy%>' 																  tag='pb_html'/>					--%>
                <%--									</td>--%>
                <%--									<td>--%>


                <%--																<input type='text' class='form-control'  name='vmTaxTokenItem.modifiedBy' id = 'modifiedBy_text_' value='<%=vmTaxTokenItemDTO.modifiedBy%>' 																  tag='pb_html'/>					--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.insertionDate' id = 'insertionDate_hidden_' value='<%=vmTaxTokenItemDTO.insertionDate%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.isDeleted' id = 'isDeleted_hidden_' value= '<%=vmTaxTokenItemDTO.isDeleted%>' tag='pb_html'/>--%>
                <%--											--%>
                <%--									</td>--%>
                <%--									<td style="display: none;">--%>


                <%--														<input type='hidden' class='form-control'  name='vmTaxTokenItem.lastModificationTime' id = 'lastModificationTime_hidden_' value='<%=vmTaxTokenItemDTO.lastModificationTime%>' tag='pb_html'/>--%>
                <%--									</td>--%>
                <%--									<td>--%>
                <%--											<span id='chkEdit'>--%>
                <%--												<input type='checkbox' name='checkbox' value='' deletecb='true'--%>
                <%--													   class="form-control-sm"/>--%>
                <%--											</span>--%>
                <%--									</td>--%>
                <%--								</tr>								--%>
                <%--						--%>
                <%--							</template>--%>
                <%--                        </div>--%>
                <%--                    </div>               --%>
                <div class="form-actions text-right">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="formSubmit()">
                        <%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    function formSubmit() {
        event.preventDefault();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid();
        let language = '<%=Language%>';

        if (valid) {
            let fiscalYearId = $("#fiscalYearId_hidden_0").val();
            let url = "Vm_tax_tokenServlet?actionType=checkFiscalYearValidation&fiscalYearId="
                + fiscalYearId + "&language=" + language;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    if (fetchedData && fetchedData.valid == true) {
                        formSubmitAfterValidation();
                    } else if (fetchedData && fetchedData.valid == false) {
                        toastr.error(fetchedData.errMsg);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    }


    function formSubmitAfterValidation() {
        //document.forms.bigform.submit();
        submitForm();
    }


    const vehicleReceiveForm = $('#bigform');

    function submitForm() {


        if ($("#bigform").valid()) {
            $.ajax({
                type: "POST",
                url: "Vm_tax_tokenServlet?actionType=<%=actionName%>",
                data: vehicleReceiveForm.serialize(),
                dataType: 'JSON',
                success: function (response) {

                    if (response.responseCode === 0) {
                        showToastSticky(response.msg, response.msg);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                    toastr.error('Can not update tax token & fitness fee');
                }
            });
        } else {

        }
    }

    function sameAsVehicleCat(selectedObject) {

        let idSplit = selectedObject.id.split("_");
        let curFieldIdIndex = (idSplit[idSplit.length - 1]);
        //let prevFieldIdIndex = curFieldIdIndex-1;
        let prevFieldIdIndex = selectedObject.value - 1;
        //console.log(selectedObject.value);
        let updatedFields = ["taxTokenFees", "fitnessFees", "digitalNumberFees", "vat"];
        let middleString = "number";
        updatedFields.forEach(value => {
            let curField = value + "_" + middleString + "_" + curFieldIdIndex;
            let prevField = value + "_" + middleString + "_" + prevFieldIdIndex;
            if (document.getElementById(prevField)) {
                document.getElementById(curField).value = document.getElementById(prevField).value;
            }

        });

    }

    function PreprocessBeforeSubmiting(row, validate) {


        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("isDigital_checkbox_" + i)) {
                if (document.getElementById("isDigital_checkbox_" + i).getAttribute("processed") == null) {
                    preprocessCheckBoxBeforeSubmitting('isDigital', i);
                    document.getElementById("isDigital_checkbox_" + i).setAttribute("processed", "1");
                }
            }
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_tax_tokenServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;
    $(document).ready(function () {
        documentValidationReady();
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-VmTaxTokenItem").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-VmTaxTokenItem");

            $("#field-VmTaxTokenItem").append(t.html());
            SetCheckBoxValues("field-VmTaxTokenItem");

            var tr = $("#field-VmTaxTokenItem").find("tr:last-child");

            tr.attr("id", "VmTaxTokenItem_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });


            child_table_extra_id++;

        });


    $("#remove-VmTaxTokenItem").click(function (e) {
        var tablename = 'field-VmTaxTokenItem';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });

    function documentValidationReady() {

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {

                fiscalYearId: {
                    required: true,
                },

            },

            messages: {
                fiscalYearId: "<%=LM.getText(LC.VM_TAX_TOKEN__PLEASE_SELECT_A_FISCAL_YEAR, loginDTO)%>",
            }
        });

    }


</script>






