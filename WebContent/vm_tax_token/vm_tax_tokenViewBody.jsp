

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="vm_tax_token.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
String servletName = "Vm_tax_tokenServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.VM_TAX_TOKEN_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Vm_tax_tokenDAO vm_tax_tokenDAO = new Vm_tax_tokenDAO("vm_tax_token");
//Vm_tax_tokenDTO vm_tax_tokenDTO = (Vm_tax_tokenDTO)vm_tax_tokenDAO.getDTOByID(id);
Vm_tax_tokenDTO vm_tax_tokenDTO = Vm_tax_tokenRepository.getInstance().getVm_tax_tokenDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="form-body">
                <h5 class="table-title">
                    <%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_TAX_TOKEN_ADD_FISCALYEARID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_tax_tokenDTO.fiscalYearId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VEHICLETYPECAT, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_tax_tokenDTO.vehicleTypeCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "vehicle_type", vm_tax_tokenDTO.vehicleTypeCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			
			
			
			
			
			
			
			
			
			
		
						</table>
                  </div>
			</div>	

				<div class="div_border attachement-div mt-5">
						<h5 class="table-title">
							<%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM, loginDTO)%>
						</h5>                        
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_ISDIGITAL, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_TOTALVAT, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_TOTAL, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_INSERTEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ITEM_MODIFIEDBY, loginDTO)%></th>
							</tr>
							<%
                        	VmTaxTokenItemDAO vmTaxTokenItemDAO = new VmTaxTokenItemDAO();
                         	List<VmTaxTokenItemDTO> vmTaxTokenItemDTOs = vmTaxTokenItemDAO.getVmTaxTokenItemDTOListByVmTaxTokenID(vm_tax_tokenDTO.iD);
                         	
                         	for(VmTaxTokenItemDTO vmTaxTokenItemDTO: vmTaxTokenItemDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = vmTaxTokenItemDTO.isDigital + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = vmTaxTokenItemDTO.totalVat + "";
											%>
											<%
											value = String.format("%.1f", vmTaxTokenItemDTO.totalVat);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = vmTaxTokenItemDTO.total + "";
											%>
											<%
											value = String.format("%.1f", vmTaxTokenItemDTO.total);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = vmTaxTokenItemDTO.insertedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = vmTaxTokenItemDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>                                       
                </div>
        </div>
	</div>
</div>