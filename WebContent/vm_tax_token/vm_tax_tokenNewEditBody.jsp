<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_tax_token.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="vm_fuel_vendor.VmFuelVendorItemDAO" %>

<%
    Vm_tax_tokenDTO vm_tax_tokenDTO;
    vm_tax_tokenDTO = (Vm_tax_tokenDTO) request.getAttribute("vm_tax_tokenDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_tax_tokenDTO == null) {
        vm_tax_tokenDTO = new Vm_tax_tokenDTO();

    }
    System.out.println("vm_tax_tokenDTO = " + vm_tax_tokenDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }

    actionName = "allExistingVehicleDeleteAndAdd";
    String formTitle = LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_tax_tokenServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_TAX_TOKEN_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;


    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    Fiscal_yearDTO fiscal_yearDTO = fiscal_yearDAO.getDTOByID(Long.valueOf(request.getParameter("fiscalYearID")));
    List<Vm_tax_tokenDTO> vm_tax_tokenDTOS = (List<Vm_tax_tokenDTO>) request.getAttribute("vm_tax_tokenDTOS");

    VmFuelVendorItemDAO vmFuelVendorItemDAO = new VmFuelVendorItemDAO();

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <%--        <form class="form-horizontal"--%>
        <%--              action="Vm_tax_tokenServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
        <%--              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"--%>
        <%--              >--%>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 ">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FISCALYEARID, loginDTO)%> <%=Utils.getDigits(fiscal_yearDTO.nameEn, Language)%>
                                            </h4>
                                        </div>

                                        <div class="my-4">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VEHICLETYPECAT, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_TAXTOKENFEES, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_FITNESSFEES, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_DIGITALNUMBERFEES, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_ADD_VAT, loginDTO)%>
                                                        </th>
                                                        <th><%=LM.getText(LC.VM_TAX_TOKEN_SAME_AS, loginDTO)%>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <input type='hidden' class='form-control' name='fiscalYearId'
                                                           id='fiscalYearId_hidden_0' value='<%=fiscal_yearDTO.id%>'
                                                           tag='pb_html'/>
                                                    <%
                                                        CatRepository catRepository = CatRepository.getInstance();
                                                        List<CategoryLanguageModel> modelList = catRepository.getCategoryLanguageModelList("vehicle_type");
                                                        int count = -1;
                                                        for (Vm_tax_tokenDTO vm_tax_tokenDTO1 : vm_tax_tokenDTOS) {
                                                            vm_tax_tokenDTO = new Vm_tax_tokenDTO();
                                                            CategoryLanguageModel model = vmFuelVendorItemDAO.getDTOFromModelCatID(modelList, Long.valueOf(vm_tax_tokenDTO1.vehicleTypeCat));
                                                            ++count;

                                                    %>

                                                    <tr>
                                                        <input type='hidden' class='form-control' name='iD'
                                                               id='iD_hidden_<%=count%>' value='<%=vm_tax_tokenDTO.iD%>'
                                                               tag='pb_html'/>

                                                        <td>
                                                            <input type='hidden' class='form-control'
                                                                   name='vehicleTypeCat'
                                                                   id='vehicleTypeCat_category_<%=count%>'
                                                                   value='<%=vm_tax_tokenDTO1.vehicleTypeCat%>'
                                                                   tag='pb_html'>
                                                            <%
                                                                value = isLanguageEnglish ? model.englishText : model.banglaText;
                                                            %>
                                                            <%=value%>
                                                        </td>
                                                        <%value = "0";%>
                                                        <td>
                                                            <input type='number' class='form-control' step="0.001"
                                                                   name='taxTokenFees'
                                                                   id='taxTokenFees_number_<%=count%>'
                                                                   value='<%=vm_tax_tokenDTO1.taxTokenFees%>'
                                                                   tag='pb_html'>
                                                        </td>
                                                        <td>
                                                            <input type='number' class='form-control' step="0.001"
                                                                   name='fitnessFees' id='fitnessFees_number_<%=count%>'
                                                                   value='<%=vm_tax_tokenDTO1.fitnessFees%>'
                                                                   tag='pb_html'>
                                                        </td>
                                                        <td>
                                                            <input type='number' class='form-control' step="0.001"
                                                                   name='digitalNumberFees'
                                                                   id='digitalNumberFees_number_<%=count%>'
                                                                   value='<%=vm_tax_tokenDTO1.digitalNumberFees%>'
                                                                   tag='pb_html'>
                                                        </td>
                                                        <td>
                                                            <input type='number' class='form-control' step="0.001"
                                                                   name='vat' id='vat_number_<%=count%>'
                                                                   value='<%=vm_tax_tokenDTO1.vat%>' tag='pb_html'>
                                                        </td>

                                                        <td>

                                                            <%
                                                                if (true) {

                                                            %>

                                                            <select class='form-control'
                                                                    onchange="sameAsVehicleCat(this)"
                                                                    id='vehicleTypeCat_same_category_<%=count%>'
                                                                    tag='pb_html'>
                                                                <%
                                                                    Options = CatRepository.getInstance().buildOptions("vehicle_type", Language, vm_tax_tokenDTO.vehicleTypeCat);
                                                                %>
                                                                <%=Options%>
                                                            </select>
                                                            <%}%>


                                                        </td>

                                                    </tr>


                                                    <%}%>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-right mt-3">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="submitForm()">
                        <%=LM.getText(LC.VM_TAX_TOKEN_ADD_VM_TAX_TOKEN_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const vehicleReceiveForm = $('#bigform');

    function submitForm() {


        if ($("#bigform").valid()) {
            $.ajax({
                type: "POST",
                url: "Vm_tax_tokenServlet?actionType=<%=actionName%>",
                data: vehicleReceiveForm.serialize(),
                dataType: 'JSON',
                success: function (response) {

                    if (response.responseCode === 0) {
                        showToastSticky(response.msg, response.msg);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                    toastr.error('Can not update tax token & fitness fee');
                }
            });
        } else {

        }
    }

    function formSubmit() {
        event.preventDefault();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid();
        let language = '<%=Language%>';

        if (valid) {
            let fiscalYearId = $("#fiscalYearId_hidden_0").val();
            let url = "Vm_tax_tokenServlet?actionType=checkFiscalYearValidation&fiscalYearId="
                + fiscalYearId + "&language=" + language;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    if (fetchedData && fetchedData.valid == true) {
                        formSubmitAfterValidation();
                    } else if (fetchedData && fetchedData.valid == false) {
                        toastr.error(fetchedData.errMsg);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });

        }
    }


    function formSubmitAfterValidation() {
        document.forms.bigform.submit();
    }

    function sameAsVehicleCat(selectedObject) {

        let idSplit = selectedObject.id.split("_");
        let curFieldIdIndex = (idSplit[idSplit.length - 1]);
        //let prevFieldIdIndex = curFieldIdIndex-1;
        let prevFieldIdIndex = selectedObject.value - 1;
        //console.log(selectedObject.value);
        let updatedFields = ["taxTokenFees", "fitnessFees", "digitalNumberFees", "vat"];
        let middleString = "number";
        updatedFields.forEach(value => {
            let curField = value + "_" + middleString + "_" + curFieldIdIndex;
            let prevField = value + "_" + middleString + "_" + prevFieldIdIndex;
            if (document.getElementById(prevField)) {
                document.getElementById(curField).value = document.getElementById(prevField).value;
            }

        });

    }

    function PreprocessBeforeSubmiting(row, validate) {


        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("isDigital_checkbox_" + i)) {
                if (document.getElementById("isDigital_checkbox_" + i).getAttribute("processed") == null) {
                    preprocessCheckBoxBeforeSubmitting('isDigital', i);
                    document.getElementById("isDigital_checkbox_" + i).setAttribute("processed", "1");
                }
            }
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_tax_tokenServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-VmTaxTokenItem").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-VmTaxTokenItem");

            $("#field-VmTaxTokenItem").append(t.html());
            SetCheckBoxValues("field-VmTaxTokenItem");

            var tr = $("#field-VmTaxTokenItem").find("tr:last-child");

            tr.attr("id", "VmTaxTokenItem_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });


            child_table_extra_id++;

        });


    $("#remove-VmTaxTokenItem").click(function (e) {
        var tablename = 'field-VmTaxTokenItem';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


</script>






