<%@page pageEncoding="UTF-8" %>

<%@page import="job_applicant_application.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="util.*" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="javafx.util.Pair" %>
<%@ page import="config.GlobalConfigurationRepository" %>
<%@ page import="config.GlobalConfigConstants" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeDao" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.JOB_APPLICANT_APPLICATION_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_JOB_APPLICANT_APPLICATION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Job_applicant_applicationDTO job_applicant_applicationDTO = (Job_applicant_applicationDTO) request.getAttribute("job_applicant_applicationDTO");
    CommonDTO commonDTO = job_applicant_applicationDTO;
    String servletName = "Job_applicant_applicationServlet";

    Double passMarks = (Double) request.getAttribute("passMark");
    if (passMarks == null) {
        passMarks = 0.0;
    }

//	System.out.println(passMarks);

    Map<Integer, String> levelMap = (Map<Integer, String>) request.getAttribute("levelMap");
    if (levelMap == null) {
        levelMap = new HashMap<>();
    }


    System.out.println("job_applicant_applicationDTO = " + job_applicant_applicationDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    FilesDAO filesDAO = new FilesDAO();

//	Recruitment_job_descriptionDTO jobDescriptionDTO =
//			(Recruitment_job_descriptionDTO) request.getAttribute("jobDescriptionDTO");

//	SummaryDTO convertedObject = new Gson().fromJson(job_applicant_applicationDTO.summary,
//			SummaryDTO.class);

    String name = UtilCharacter.getDataByLanguage(Language, job_applicant_applicationDTO.applicant_name_bn,
            job_applicant_applicationDTO.applicant_name_en);

    String fatherName = Language.equalsIgnoreCase("english") ? "Father Name: " : "পিতার নাম: ";
    fatherName += job_applicant_applicationDTO.father_name;

    String permanentAddress = (Language.equalsIgnoreCase("english") ? "Permanent Address: " : "স্থায়ী ঠিকানা: ") +
            UtilCharacter.getDataByLanguage(Language, job_applicant_applicationDTO.
                    permanent_address_name_bn, job_applicant_applicationDTO.permanent_address_name_en);
    String dateOfBirth = UtilCharacter.getDataByLanguage(Language, job_applicant_applicationDTO.
            applicant_dob_bn, job_applicant_applicationDTO.applicant_dob_en);
//	String gender = "";
    String currentAge = "";
    long internShipId = Long.parseLong(GlobalConfigurationRepository.
            getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);
    if (job_applicant_applicationDTO.jobId != internShipId) {
        currentAge = "(".concat(UtilCharacter.getDataByLanguage(Language, job_applicant_applicationDTO.
                applicant_age_bn, job_applicant_applicationDTO.applicant_age_en)).concat(")");
    }
    String quota = UtilCharacter.getDataByLanguage(Language, job_applicant_applicationDTO.
            quota_bn, job_applicant_applicationDTO.quota_en);


//	String phoneNo = "";
//	 jobApplicationDate = "";
//	String ethnicMin = "";
//	String freedomQuota = "";
//	String disQuota = "";
//	String specialQuota = "";
//	String lastEduName = "";
//	String lastDegreeName = "";
//	String lastEduInstituteName = "";
    String totalExperience = UtilCharacter.getDataByLanguage(Language, job_applicant_applicationDTO.district_name_bn,
            job_applicant_applicationDTO.district_name_en);
//			UtilCharacter.getDataByLanguage(Language, job_applicant_applicationDTO.
//			job_experience_bn, job_applicant_applicationDTO.job_experience_en);
    String lastEduInfo = (Language.equalsIgnoreCase("english") ? "Highest Education: " : "সর্বোচ্চ শিক্ষাগত যোগ্যতা: ") +
            UtilCharacter.getDataByLanguage(Language, job_applicant_applicationDTO.
                    edu_info_bn, job_applicant_applicationDTO.edu_info_en);

    String jobApplicationDate = simpleDateFormat.format
            (new Date(Long.parseLong(job_applicant_applicationDTO.insertionDate + "")));
    Pair<String, String> attendancePair = Job_applicant_applicationDAO.getPresent(job_applicant_applicationDTO.isPresent);
//	String attendance = UtilCharacter.getDataByLanguage(Language, attendancePair.
//			getValue(), attendancePair.getKey());

    Pair<String, String> marksPair = Job_applicant_applicationDAO.getMarks(job_applicant_applicationDTO.marks, passMarks);
    String marks = UtilCharacter.getDataByLanguage(Language, marksPair.
            getValue(), marksPair.getKey());

    String rollNumber = job_applicant_applicationDTO.rollNumber;

//	System.out.println("###");
//	System.out.println(job_applicant_applicationDTO.prev_level_data);


    String prevLevelData = "";
    PrevLevelDataSummaryDTO convertedObject = new Gson().fromJson(job_applicant_applicationDTO.prev_level_data,
            PrevLevelDataSummaryDTO.class);
    if (convertedObject != null && convertedObject.data != null && convertedObject.data.size() > 0) {
        System.out.println(convertedObject.data);
        for (PrevLevelDataDTO prevLevelDataDTO : convertedObject.data) {
            if (prevLevelDataDTO.level != 0 && prevLevelDataDTO.marks > -1.0) {
                String pLevelData = "";
                if (levelMap.get(prevLevelDataDTO.level) != null) {
                    pLevelData = levelMap.get(prevLevelDataDTO.level);
                }
                prevLevelData += " " + pLevelData + " : " + Utils.getDigits(prevLevelDataDTO.marks, Language) + " .";
            }
        }
    }

//	System.out.println(prevLevelData);
//
//	System.out.println("###");

//	long internShipId = Long.parseLong(GlobalConfigurationRepository.getInstance().
//			getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);
//	if(job_applicant_applicationDTO.jobId ==internShipId){
//		rollNumber = UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
//	}
//	List<Job_applicant_photo_signatureSummaryDTO> job_applicant_photo_signatureDTOs = new ArrayList<>();
//	List<Job_applicant_education_infoSummaryDTO> jobApplicantEducationInfoSummaryDTOS;


//	if(convertedObject != null && convertedObject.parliament_job_applicantDTO != null){
//		Parliament_job_applicantDTO jobApplicantDTO = convertedObject.parliament_job_applicantDTO;
    // for name
//		name = UtilCharacter.getDataByLanguage(Language,jobApplicantDTO.nameBn, jobApplicantDTO.nameEn);
//
    // for permanent address
//		value = GeoLocationDAO2.parseText(jobApplicantDTO.permanentAddress, Language);
//		String[] list = value.split(",");
//		int size = list.length;
//		permanentAddress = GeoLocationDAO2.parseDetails(jobApplicantDTO.permanentAddress);
//		for (int k = size -1; k >= 0; k--){
//			permanentAddress += ", " + list[k];
//		}

    // for gender

//		gender = CatDAO.getName(Language, "gender", jobApplicantDTO.genderCat);

    // for dob and age

//		value = jobApplicantDTO.dateOfBirth + "";
//		dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
//
//		currentAge = TimeFormat.getAgeFromSpecificDate
//				(jobApplicantDTO.dateOfBirth, jobDescriptionDTO.lastAgeCalculationDate, Language);


//		phoneNo = jobApplicantDTO.contactNumber;


    // Quota

//		ethnicMin = CatDAO.getName(Language, "ethnic_minority", jobApplicantDTO.ethnicMinorityCat);
//		freedomQuota = CatDAO.getName(Language, "freedom_fighter", jobApplicantDTO.freedomFighterCat);
//		disQuota = CatDAO.getName(Language, "job_applicant_disability", jobApplicantDTO.disabilityCat);
//		specialQuota = CatDAO.getName(Language, "job_applicant_special_quota", jobApplicantDTO.specialQuotaCat);


    // Photo

//		job_applicant_photo_signatureDTOs = convertedObject.job_applicant_photo_signatureSummaryDTOList
//				.stream().filter(ii -> ii.photoOrSignature == 1).collect(Collectors.toList());


    // last edu

//		jobApplicantEducationInfoSummaryDTOS = convertedObject.job_applicant_education_infoSummaryDTOList;
//		if(jobApplicantEducationInfoSummaryDTOS.size() > 0){
//			Job_applicant_education_infoSummaryDTO reqDTO = jobApplicantEducationInfoSummaryDTOS
//					.stream().max(Comparator.comparing(ii -> ii.educationLevelType)).get();
//
//			if(reqDTO != null){
//				lastEduInstituteName = reqDTO.institutionName;
//				lastEduName = CommonDAO.getName(reqDTO.educationLevelType, "education_level", Language.equals("English")?"name_en":"name_bn", "id");
//				lastDegreeName = CommonDAO.getName(reqDTO.degreeExamType, "degree_exam", Language.equals("English")?"name_en":"name_bn", "id");
//			}
//		}
//	}

    // experience

//	if(convertedObject != null && convertedObject.totalExperience != 0){
//		totalExperience = TimeFormat.getAgeFromSpecificDays(convertedObject.totalExperience, Language);
//	}


%>


<td style="vertical-align: middle;" id='<%=i%>_jobApplicantId'>
    <%

        //												job_applicant_photo_signatureDTOs.get(0).filesDropzone

//												if (!job_applicant_photo_signatureDTOs.isEmpty()) {

        List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(job_applicant_applicationDTO.photo_file_id);

        if (filesDropzoneDTOList != null && !filesDropzoneDTOList.isEmpty()) {
            for (int j = 0; j < 1; j++) {
                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
    %>
    <div id='filesDropzone_td_<%=filesDTO.iD%>'>
        <%
            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
        %>
        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style=' border-radius: 50%'/>
        <%
            }
        %>
        <%--															<a href = 'Job_applicant_photo_signatureServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>--%>
    </div>
    <%
            }
        }

//													}

    %>


</td>


<td id='<%=i%>_basicDetails'>

    <p>
        <%=name%> <br>
        <%=fatherName%> <br>
        <%=permanentAddress%><br>
        <%=lastEduInfo%>
        <%--													<%=Utils.getDigits(dateOfBirth, Language)%><br>--%>
        <%--													<%=gender%> <br>--%>
        <%--													<%=currentAge%><br>--%>
        <%--													<%=phoneNo%><br>--%>
        <%--													<%=lastEduName%>, <%=lastDegreeName%>, <%=lastEduInstituteName%>--%>

    </p>


</td>

<td>
    <%=Utils.getDigits(dateOfBirth, Language)%> <br>
    <%=currentAge%>
</td>


<td>
    <%=totalExperience%>
</td>

<td>
    <%=quota%>
    <%--	<p>--%>
    <%--		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_ETHNICMINORITYCAT, loginDTO)%>: <%=ethnicMin%><br>--%>
    <%--		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_FREEDOMFIGHTERCAT, loginDTO)%>: <%=freedomQuota%><br>--%>
    <%--		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_DISABILITYCAT, loginDTO)%>: <%=disQuota%> <br>--%>
    <%--		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_SPECIALQUOTACAT, loginDTO)%>: <%=specialQuota%> <br>--%>
    <%--	</p>--%>
</td>

<td>
    <%=rollNumber%>
    <%--	", " + attendance +--%>
</td>
<td>
    <%=marks%>
    <%--	", " + attendance +--%>
</td>

<% if(internShipId != job_applicant_applicationDTO.jobId){ %>
    <td>
        <%=prevLevelData%>
    </td>

<% } %>


<td>
    <%=Utils.getDigits(jobApplicationDate, Language)%>
</td>

<td>
    <%
        value = "";
        Pair<String, String> decisionValue =
                Job_applicant_applicationDAO.getDecision(job_applicant_applicationDTO.isSelected, job_applicant_applicationDTO.isRejected);

        if (decisionValue != null) {
            value = UtilCharacter.getDataByLanguage(Language, decisionValue.getValue(), decisionValue.getKey());
        }
    %>
    <%=value%>
</td>

<td class="text-center">

    <button style="" class="btn btn-sm btn-info text-white shadow btn-border-radius"
            onclick="makeViewed(<%=job_applicant_applicationDTO.iD%>)"><%=LM.getText(LC.CANDIDATE_LIST_VIEW, loginDTO)%>
    </button>

    <%
        boolean showShorlistButton = job_applicant_applicationDTO.isSelected != 1 && job_applicant_applicationDTO.isSelected != 2;

        if (showShorlistButton) {
    %>

    <button style="" class="btn btn-sm btn-success text-white shadow btn-border-radius"
            onclick="shortList(<%=job_applicant_applicationDTO.iD%>)"><%=LM.getText(LC.CANDIDATE_LIST_SHORTLIST, loginDTO)%>
    </button>

    <% }


        if (!job_applicant_applicationDTO.isRejected) {

    %>
    <button style="" class="btn btn-sm btn-danger text-white shadow btn-border-radius"
            onclick="reject(<%=job_applicant_applicationDTO.iD%>)"><%=LM.getText(LC.CANDIDATE_LIST_REJECT, loginDTO)%>
    </button>

    <% } %>
</td>


<%--											<td id = '<%=i%>_acceptanceStatus'>--%>
<%--											<%--%>
<%--											value = job_applicant_applicationDTO.acceptanceStatus + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=value%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_insertionDate'>--%>
<%--											<%--%>
<%--											value = job_applicant_applicationDTO.insertionDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=formatted_insertionDate%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_insertedByUserId'>--%>
<%--											<%--%>
<%--											value = job_applicant_applicationDTO.insertedByUserId + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = WorkflowController.getNameFromUserId(job_applicant_applicationDTO.insertedByUserId, Language);--%>
<%--											if(value.equalsIgnoreCase(""))--%>
<%--											{--%>
<%--												value = "superman";--%>
<%--											}--%>
<%--											%>--%>
<%--														--%>
<%--											<%=value%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_modifiedBy'>--%>
<%--											<%--%>
<%--											value = job_applicant_applicationDTO.modifiedBy + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=value%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%--											<td id = '<%=i%>_rollNumber'>--%>
<%--											<%--%>
<%--											value = job_applicant_applicationDTO.rollNumber + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=value%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--	--%>

<%--											<td>--%>
<%--												<a href='Job_applicant_applicationServlet?actionType=view&ID=<%=job_applicant_applicationDTO.iD%>'>View</a>--%>
<%--										--%>
<%--											</td>--%>
<%--	--%>
<%--											<td id = '<%=i%>_Edit'>																																	--%>
<%--	--%>
<%--												<a href='Job_applicant_applicationServlet?actionType=getEditPage&ID=<%=job_applicant_applicationDTO.iD%>'><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_JOB_APPLICANT_APPLICATION_EDIT_BUTTON, loginDTO)%></a>--%>
<%--																				--%>
<%--											</td>											--%>
<%--											--%>
<%--											--%>
<%--											<td id='<%=i%>_checkbox'>--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=job_applicant_applicationDTO.iD%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>
																						
											

