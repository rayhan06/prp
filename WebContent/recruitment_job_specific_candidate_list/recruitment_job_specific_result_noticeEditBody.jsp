<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_maintenance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle_parts.Vm_vehicle_partsRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationRepository" %>
<%@ page import="admit_card.Admit_cardDTO" %>
<%@ page import="admit_card.Admit_cardRepository" %>
<%@ page import="admit_card.Admit_cardDAO" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="bangla_date_converter.BanglaDateConverter" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>


<%

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);

    String job = request.getParameter("job");
    String level = request.getParameter("level");
    String approve = request.getParameter("approve");

    long jobId = Long.parseLong(job);
    long levelId = Long.parseLong(level);
    //long approveId = Long.parseLong(approve);

    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId);
    job_applicant_applicationDTOS = job_applicant_applicationDTOS.
                                    stream().
                                    filter(e -> e.level == levelId).collect(Collectors.toList());

    String approvalStatus = ".....";
    if (approve != null && !approve.equalsIgnoreCase("ALL")) {
        if (approve.equalsIgnoreCase("REJECTED")) {
            approvalStatus = UtilCharacter.getDataByLanguage(Language,"প্রত্যাখ্যাত হয়েছেনঃ","Rejected");
            job_applicant_applicationDTOS = job_applicant_applicationDTOS.
                    stream().
                    filter(e -> e.isRejected == true).collect(Collectors.toList());
        } else if (approve.equalsIgnoreCase("RUNNING")) {
            approvalStatus = UtilCharacter.getDataByLanguage(Language,"চলমান রয়েছেনঃ","Running");
            job_applicant_applicationDTOS = job_applicant_applicationDTOS.
                    stream().
                    filter(e -> e.isRejected == false && e.isSelected == 0).collect(Collectors.toList());
        } else {
            approvalStatus = UtilCharacter.getDataByLanguage(Language,"উত্তীর্ণ হয়েছেনঃ","passed");
            job_applicant_applicationDTOS = job_applicant_applicationDTOS.
                    stream()
                    .filter(e -> e.isSelected == 1 || e.isSelected == 2).collect(Collectors.toList());
        }
    }
    if(job_applicant_applicationDTOS != null){
        job_applicant_applicationDTOS.sort(Comparator.comparing(dto -> dto.rollNumber));
    }

    

    Admit_cardDAO admit_cardDAO = new Admit_cardDAO();
    List<Admit_cardDTO> admit_cardDTOs = admit_cardDAO.getAllAdmit_cardByJobID(jobId);
    Admit_cardDTO admit_cardDTO = null;
    if(admit_cardDTOs!= null && admit_cardDTOs.size()>0){
        admit_cardDTO = admit_cardDAO.getAllAdmit_cardByJobID(jobId).get(0);
    }
    Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
            getById(admit_cardDTO != null? admit_cardDTO.employeeRecordsId : -1);

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionDTOByID(jobId);
    List<RecruitmentJobSpecificExamTypeDTO> jobSpecificExamTypeDTOs = JobSpecificExamTypeRepository.getInstance().
            getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);

    String examType = "";
    if(jobSpecificExamTypeDTOs.size() > 0){
        jobSpecificExamTypeDTOs = jobSpecificExamTypeDTOs.stream().filter(e -> e.order == levelId).collect(Collectors.toList());
        if(jobSpecificExamTypeDTOs.size()>0){
            int jobExamTypeCat = jobSpecificExamTypeDTOs.get(0).jobExamTypeCat;
            examType =  CatRepository.getInstance().getText(Language,"job_exam_type",jobExamTypeCat);
        }
    }

    String grade = CatRepository.getInstance().getText(Language,"job_grade",recruitment_job_descriptionDTO.jobGradeCat);
    String postName = UtilCharacter.getDataByLanguage(Language,recruitment_job_descriptionDTO.jobTitleBn,recruitment_job_descriptionDTO.jobTitleEn);
    String examDate = Utils.getDigits(simpleDateFormat.format(new Date(Long.parseLong(String.valueOf(admit_cardDTO!=null?admit_cardDTO.dateOfExam:-1)))), Language);

    String noticeHeaderBN = "বাংলাদেশ জাতীয় সংসদ সচিবালয়ের "+grade+" "+postName+" পদে জনবল নিয়োগের লক্ষ্যে "+examDate+" তারিখে অনুষ্ঠিত "+examType+ " পরীক্ষায় নিম্নবর্ণিত রোল নম্বরধারী প্রার্থীগণ "+approvalStatus;
    String noticeHeaderEN = "Candidates with the following roll number have "+approvalStatus+" the examination held on "+examDate+"  for the recruitment of manpower for the post of "+grade+" "+postName+" of the Secretariat of Bangladesh Jatiya Sangsad.";

%>

<style>

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

</style>


<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg p-0" id="bill-div">
                <div class="ml-auto my-5">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', 'Result notice');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="mx-1">
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div class="row">
                                <div class="col-12 text-center">

                                    <h5 class="mt-2 font-weight-bold">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                    </h5>
                                    <h5 class="text-dark">
                                        <%=UtilCharacter.getDataByLanguage(Language, "মানব সম্পদ উইং", "Human Resources Wing")%>
                                    </h5>
                                    <a class="text-dark" href="https://www.Parliament.gov.bd" target="_blank">
                                        <u><%=UtilCharacter.getDataByLanguage(Language, "www.Parliament.gov.bd", "www.Parliament.gov.bd")%></u>
                                    </a>

                                </div>
                            </div>

                            <div class="row mt-2 ">
                                <div class="col-12 " style="display: flex;justify-content: space-between;">

                                    <span>
                                        <%=UtilCharacter.getDataByLanguage(Language, "নংঃ- ", "Subject:- ")%>&nbsp;
                                    </span>

                                    <span >
                                        <% String formattedBillBanglaDateInWord = BanglaDateConverter.getFormattedBanglaDate(admit_cardDTO!=null?admit_cardDTO.dateOfExam:-1, true);
                                            String formattedBillDateInWord = DateUtils.getDateInWord("bangla", admit_cardDTO!=null?admit_cardDTO.dateOfExam:-1);

                                        %>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td rowspan="2">তারিখ:</td>
                                                <td style="text-align: center;"><%=formattedBillBanglaDateInWord%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid black; text-align: center;">
                                                    <%=formattedBillDateInWord%>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </span>
                                </div>
                            </div>

                            <div class=" mt-10 mb-3 text-center" >

                                <u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "বিজ্ঞপ্তি", "Notice")%></u>

                            </div>

                            <div class=" mt-10 mx-2" style="text-indent: 3rem;">

                                <%=UtilCharacter.getDataByLanguage(Language, noticeHeaderBN, noticeHeaderEN)%>

                            </div>


                            <div class="row mx-2 mt-1">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >
                                    <thead>
                                    <tr>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "পদের নাম", "Post Name")%>
                                        </th>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "রোল নম্বর", "Roll Number")%>
                                        </th>


                                    </tr>
                                    </thead>
                                    <tbody>


                                    <tr>

                                        <td style="padding: 5px 10px;min-width: 12rem;text-align: center;">


                                            <%=postName%>

                                        </td>
                                        <td style="padding: 10px 20px;">

                                            <%
                                                int dtoSize = job_applicant_applicationDTOS.size();
                                                int curSize = 0;
                                                for (Job_applicant_applicationDTO job_applicant_applicationDTO:job_applicant_applicationDTOS){

                                            %>

                                            <%=Utils.getDigits(job_applicant_applicationDTO.rollNumber, Language)%><%if(dtoSize != ++curSize)%>,&nbsp;
                                            <%
                                                }
                                            %>

                                        </td>


                                    </tr>

                                    

                                    </tbody>
                                </table>
                            </div>



                            <div class="mt-5">
                                <div class="row">
                                    <div class="col-3 offset-9 text-center">
                                        <div>
                                            <%
                                                byte[] encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.signature);
                                            %>
                                            <img
                                                    src="data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>"
                                                    width="50%"
                                                    id="applicantSignature"
                                            >
                                        </div>
                                        <div>
                    <span
                            class=""
                            id="applicantName"
                            placeholder="নাম"
                    >
                    (<%out.println(employee_recordsDTO.nameBng);%>)
                </span>
                                        </div>
                                        <div>
                    <span
                            class="text-nowrap"
                            id="applicantOrganogramName"
                            placeholder="পদের নাম"
                    >
                    <%out.println(admit_cardDTO!=null?admit_cardDTO.signatory_post_name_bn:"");%>
                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>