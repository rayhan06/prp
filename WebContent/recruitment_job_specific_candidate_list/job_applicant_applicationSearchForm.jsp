<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="job_applicant_application.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="config.GlobalConfigurationRepository" %>
<%@ page import="config.GlobalConfigConstants" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    long jobId = Long.parseLong(request.getParameter("jobId"));
//    System.out.println("###");
//    System.out.println(jobId);
//    System.out.println("###");
    long internShipId = Long.parseLong(GlobalConfigurationRepository.
            getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);

    String value = "";
    String Language = LM.getText(LC.JOB_APPLICANT_APPLICATION_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Job_applicant_applicationDAO job_applicant_applicationDAO = (Job_applicant_applicationDAO) request.getAttribute("job_applicant_applicationDAO");


    String navigator2 = SessionConstants.NAV_JOB_APPLICANT_APPLICATION;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="thead-light" style="">
        <tr>
            <th style=" width: 10%;text-align: center;">
                <%--									<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOBAPPLICANTID, loginDTO)%>--%>
            </th>
            <th style=" width: 20%;text-align: center;">
                <%--									<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOBID, loginDTO)%>--%>
                <%=LM.getText(LC.CANDIDATE_LIST_BASIC_INFO, loginDTO)%>
            </th>
            <th style="width: 10%;text-align: center;">
                <%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_DATEOFBIRTH, loginDTO)%>
            </th>
            <th style=" width: 5%;text-align: center;">
                <%=LM.getText(LC.CANDIDATE_LIST_TOTAL_JOB_EXPERIENCE, loginDTO)%>
            </th>
            <th style=" width: 10%;text-align: center;">
                <%=LM.getText(LC.CANDIDATE_LIST_QUOTA, loginDTO)%>
            </th>
            <th style=" width: 10%;text-align: center;">
                <%=UtilCharacter.getDataByLanguage(Language,"রোল নম্বর","Roll Number")%>
            </th>

            <th style=" width: 10%;text-align: center;">
                <%=UtilCharacter.getDataByLanguage(Language,"নম্বর","Number")%>
            </th>

            <% if(jobId != internShipId){ %>
            <th style=" width: 5%;text-align: center;">
                <%=UtilCharacter.getDataByLanguage(Language,
                        "পূর্বের নম্বরসমূহ ", "Previous Marks")%>
            </th>

            <% } %>

            <th style=" width: 5%;text-align: center;">
                <%=LM.getText(LC.CANDIDATE_LIST_APPLY_DATE, loginDTO)%>
            </th>
            <th style=" width: 5%;">

                <%=LM.getText(LC.CANDIDATE_LIST_CANDIDATE_DECISION, loginDTO)%>
            </th>
            <th style=" width: 20%;text-align: center;">
                <%=LM.getText(LC.CANDIDATE_LIST_ACTIONS, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ACCEPTANCESTATUS, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_INSERTIONDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_INSERTEDBYUSERID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_MODIFIEDBY, loginDTO)%></th>--%>
            <%--								<th>--%>
            <%--									<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ROLLNUMBER, loginDTO)%>--%>
            <%--								</th>--%>
            <%--								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_JOB_APPLICANT_APPLICATION_EDIT_BUTTON, loginDTO)%></th>--%>
            <%--								<th><input type="submit" class="btn btn-xs btn-danger" value="<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_JOB_APPLICANT_APPLICATION_DELETE_BUTTON, loginDTO)%>" /></th>--%>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_JOB_APPLICANT_APPLICATION);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Job_applicant_applicationDTO job_applicant_applicationDTO = (Job_applicant_applicationDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("job_applicant_applicationDTO", job_applicant_applicationDTO);
            %>

            <jsp:include page="./job_applicant_applicationSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>



			