<%@page import="util.*" %>
<%@ page import="user.UserServlet" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_maintenance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle_parts.Vm_vehicle_partsRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationRepository" %>
<%@ page import="admit_card.Admit_cardDTO" %>
<%@ page import="admit_card.Admit_cardRepository" %>
<%@ page import="admit_card.Admit_cardDAO" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="bangla_date_converter.BanglaDateConverter" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>

<%
    String context_folder = request.getContextPath();
    String Language = "Bangla";
    int my_language = 1;
%>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.maateen.me/solaiman-lipi/font.css">
    <title>View Result</title>
    <style>

        .custom-toast {
            visibility: hidden;
            width: 300px;
            min-height: 60px;
            color: #fff;
            text-align: center;
            padding: 20px 30px;
            position: fixed;
            z-index: 1;
            right: 23px;
            bottom: 60px;
            font-size: 13pt;
        }

        #toast_message {
            background-color: #008aa6;
        }

        #error_message {
            background-color: #ca5e59;
        }

        #toast_message.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        #error_message.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        @-webkit-keyframes fadein {
            from {
                right: -38%;
                opacity: 0;
            }
            to {
                right: 23px;
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                right: -38%;
                opacity: 0;
            }
            to {
                right: 23px;
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeout {
            from {
                right: 23px;
                opacity: 1;
            }
            to {
                right: -38%;
                opacity: 0;
            }
        }

        @keyframes fadeout {
            from {
                right: 23px;
                opacity: 1;
            }
            to {
                right: -38%;
                opacity: 0;
            }
        }


        /* ==================== General Style =========== */

        * {
            margin: 0;
            padding: 0;
            outline: none;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            text-decoration: none;
            list-style: none;
        }

        /* ================== Body =================== */
        body {
        <%--background: url(<%=context_folder%>/images/pin_retrieve/BG.png) no-repeat;--%> background: rgb(0, 159, 201);
            background: radial-gradient(circle, rgba(0, 159, 201, 1) 6%, rgba(0, 54, 115, 1) 60%, rgba(0, 27, 93, 1) 91%);
            max-width: 100vw;
            height: 100vh;
            background-position: center center;
            font-family: 'SolaimanLipi', sans-serif;
            -webkit-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
            background-size: cover;
            background-attachment: fixed;
        }

        /* ======================== Top Logo area ================== */
        .leftlogo img {
            width: 159px;
            height: 113px;
            margin-top: 30px;
            margin-left: 30px;
        }

        .rightlogo img {
            width: 175px;
            height: 83px;
            margin-top: 60px;
            float: right;
            margin-right: 60px;
        }

        .leftnews {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-pack: end;
            -ms-flex-pack: end;
            justify-content: flex-end;
            justify-items: flex-end;
            float: right;
            padding-top: 35%;
        }


        .rightnews {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-pack: start;
            -ms-flex-pack: start;
            justify-content: flex-start;
            justify-items: flex-start;
            float: left;
            padding-top: 35%;
        }

        .formrelative {
            position: relative;
        }

        .bottomrightcontent {
            position: relative;
        }

        .formarea {
            display: block;
            float: left;
            width: 90%;
            margin: 0 5%;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            height: auto;
            background-color: rgba(255, 255, 255, 0.15);
            border-radius: 20px;
            color: #fff;
            /*padding: 26px 47px 37px 47px;*/
        }

        .formarea h2 {
            margin-bottom: 24px;
        }

        .rightallbutton {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: end;
            -ms-flex-pack: end;
            justify-content: flex-end;
            color: #fff;
        }

        .rightallbutton button {
            margin: 0px 0px 0px 10px;
        }

        .rightallbutton .btnleft {
            color: #000;
            background-color: #FFE600;
            border: none;
            border-radius: 8px;
        }

        .rightallbutton .btnright {
            color: #fff;
            background-color: #00D4FF;
            border: none;
            border-radius: 8px;
        }

        .maintitle h2 {
            font-size: 2.25rem;
            margin-bottom: 25px;
        }


        input[type='text']::-webkit-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::-moz-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']:-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-webkit-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-moz-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']:-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::placeholder {
            color: #B1CADB;
            font-size: 14px;
        }


        .leftnews img {
            width: 256px;
            height: 154px;
            padding-bottom: 10px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 15px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 15px;
        }

        .rightnews img {
            width: 256px;
            height: 154px;
            padding-bottom: 10px;
        }

        .maintitle h2 {
            font-size: 1.5rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        .leftlogo img {
            width: 130px;
            height: 90px;
            margin-top: 20px;
            margin-left: 30px;
        }

        .rightlogo img {
            width: 120px;
            height: 57px;
            margin-top: 50px;
            float: right;
            margin-right: 60px;
        }

        .mainlogo.text-center img {
            width: 90px;
            height: 90px;
            line-height: 90px;
        }

        .leftnews {
            padding-top: 140px;
        }

        .rightnews {
            padding-top: 140px;
        }

        h2.title {
            font-size: 25px;
        }

        .formarea h2 {
            margin-bottom: 10px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 25px 47px;*/
        /*}*/

        .leftnews img {
            width: 220px;
            height: 132px;
        }

        .rightnews img {
            width: 220px;
            height: 132px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 20px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 20px;
        }

        .songshodimage img {
            width: 100%;
            height: 100%;
            margin-top: 30px;
        }

        .btn-border-radius {
            border-radius: 6px !important;
        }

        /* ======================== Responsive Area ================== */

        @media screen and (max-width: 1537px) {
            .songshodimage img {
                width: 78%;
                height: 100%;
                margin-top: 15px;
            }

            .leftnews {
                padding-top: 30%;
            }

            .rightnews {
                padding-top: 30%;
            }

        }

        @media screen and (max-width: 1480px) {

            .songshodimage img {
                width: 78%;
                height: 100%;
                margin-top: 15px;
            }

            .leftnews {
                padding-top: 140px;
            }

            .rightnews {
                padding-top: 140px;
            }


            .maintitle h2 {
                font-size: 1.25rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
        }


        @media screen and (max-width: 1366px) {

            .songshodimage img {
                width: 70%;
                height: 100%;
                margin-top: 0px;
            }

            .leftnews {
                padding-top: 140px;
            }

            .rightnews {
                padding-top: 140px;
            }


            .maintitle h2 {
                font-size: 1.25rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
            .leftlogo img {

                margin-top: 10px;

            }

            .rightlogo img {
                margin-top: 40px;

            }

        }


        @media screen and (max-width: 1100px) {
            .maintitle h2 {
                font-size: 1.15rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
            .rightallbutton .btnleft {
                font-size: 12px;
            }

            .rightallbutton .btnright {
                font-size: 12px;
            }

            /*.formarea {*/
            /*    padding: 24px 20px 26px 20px;*/
            /*}*/
            .formarea {
                display: block;
                float: left;
                width: 100%;
                margin: 0;
            }

            img.img-fluid.circularimage {
                margin-bottom: 10px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 10px;
            }

            .songshodimage img {
                width: 100%;
            }

        }

        @media screen and (max-width: 980px) {
            .maintitle h2 {
                font-size: 1rem;

            }

            .leftnews {
                padding-top: 135px;
            }

            .rightnews {
                padding-top: 135px;
            }

            img.img-fluid.circularimage {
                margin-bottom: 11px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 11px;
            }

        }

        @media screen and (max-width: 790px) {
            .maintitle h2 {
                font-size: 0.90rem;

            }

            .leftnews {
                padding-top: 135px;
            }

            .rightnews {
                padding-top: 135px;
            }

            img.img-fluid.circularimage {
                margin-bottom: 11px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 11px;
            }

        }

        @media screen and (max-width: 767px) {

            .leftnews {
                padding-top: 30px;
                padding-bottom: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftlogo img {
                margin-left: 6px;
            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

        }

        @media screen and (max-width: 464px) {

            .leftnews {
                padding-top: 30px;
                padding-bottom: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 195px;
                height: 132px;
            }

            .rightnews img {
                width: 195px;
                height: 132px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

        }


        @media screen and (max-width: 420px) {

            .leftnews {
                padding-top: 10px;
                padding-bottom: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 170px;
                height: 132px;
            }

            .rightnews img {
                width: 170px;
                height: 132px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 0px;
            }

        }

        @media screen and (max-width: 370px) {

            .leftnews {
                padding-top: 10px;
                padding-bottom: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 105px;
                height: 70px;
            }

            .rightnews img {
                width: 105px;
                height: 70px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 0px;
            }

        }

    </style>
</head>
<body>


<%


    String job = request.getParameter("job");
    String level = request.getParameter("level");
    String approve = request.getParameter("approve");

    long jobId = Long.parseLong(job);
    long levelId = Long.parseLong(level);
    //long approveId = Long.parseLong(approve);

    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = Job_applicant_applicationRepository.getInstance().getJob_applicant_applicationDTOByjob_id(jobId);
    job_applicant_applicationDTOS = job_applicant_applicationDTOS.
            stream().
            filter(e -> e.level == levelId).collect(Collectors.toList());

    String approvalStatus = ".....";
    if (approve != null && !approve.equalsIgnoreCase("ALL")) {
        if (approve.equalsIgnoreCase("REJECTED")) {
            approvalStatus = UtilCharacter.getDataByLanguage(Language,"প্রত্যাখ্যাত হয়েছেনঃ","Rejected");
            job_applicant_applicationDTOS = job_applicant_applicationDTOS.
                    stream().
                    filter(e -> e.isRejected == true).collect(Collectors.toList());
        } else if (approve.equalsIgnoreCase("RUNNING")) {
            approvalStatus = UtilCharacter.getDataByLanguage(Language,"চলমান রয়েছেনঃ","Running");
            job_applicant_applicationDTOS = job_applicant_applicationDTOS.
                    stream().
                    filter(e -> e.isRejected == false && e.isSelected == 0).collect(Collectors.toList());
        } else {
            approvalStatus = UtilCharacter.getDataByLanguage(Language,"উত্তীর্ণ হয়েছেনঃ","passed");
            job_applicant_applicationDTOS = job_applicant_applicationDTOS.
                    stream()
                    .filter(e -> e.isSelected == 1 || e.isSelected == 2).collect(Collectors.toList());
        }
    }
    if(job_applicant_applicationDTOS != null){
        job_applicant_applicationDTOS.sort(Comparator.comparing(dto -> dto.rollNumber));
    }



    Admit_cardDAO admit_cardDAO = new Admit_cardDAO();
    List<Admit_cardDTO> admit_cardDTOS =  admit_cardDAO.getAllAdmit_cardByJobID(jobId);
    Admit_cardDTO admit_cardDTO = null;
    long admitDateOfExam = -1L;
    if(admit_cardDTOS != null && admit_cardDTOS.size() > 0){
        admit_cardDTO = admit_cardDAO.getAllAdmit_cardByJobID(jobId).get(0);
        admitDateOfExam = admit_cardDTO.dateOfExam;
    }

    Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().
            getById(admit_cardDTO != null ? admit_cardDTO.employeeRecordsId : -1L);

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance().getRecruitment_job_descriptionDTOByID(jobId);
    List<RecruitmentJobSpecificExamTypeDTO> jobSpecificExamTypeDTOs = JobSpecificExamTypeRepository.getInstance().
            getRecruitmentJobSpecificExamTypeDTOByjob_id(jobId);

    String examType = "";
    if(jobSpecificExamTypeDTOs.size() > 0){
        jobSpecificExamTypeDTOs = jobSpecificExamTypeDTOs.stream().filter(e -> e.order == levelId && e.isSelected).collect(Collectors.toList());
        if(jobSpecificExamTypeDTOs.size()>0){
            int jobExamTypeCat = jobSpecificExamTypeDTOs.get(0).jobExamTypeCat;
            examType =  CatRepository.getInstance().getText(Language,"job_exam_type",jobExamTypeCat);
        }
    }

    String grade = CatRepository.getInstance().getText(Language,"job_grade",recruitment_job_descriptionDTO.jobGradeCat);
    String postName = UtilCharacter.getDataByLanguage(Language,recruitment_job_descriptionDTO.jobTitleBn,recruitment_job_descriptionDTO.jobTitleEn);
    String examDate = Utils.getDigits(simpleDateFormat.format(new Date(Long.parseLong(String.valueOf(admitDateOfExam)))), Language);

    String noticeHeaderBN = "বাংলাদেশ জাতীয় সংসদ সচিবালয়ের "+grade+" "+postName+" পদে জনবল নিয়োগের লক্ষ্যে "+examDate+" তারিখে অনুষ্ঠিত "+examType+ " পরীক্ষায় নিম্নবর্ণিত রোল নম্বরধারী প্রার্থীগণ "+approvalStatus;
    String noticeHeaderEN = "Candidates with the following roll number have "+approvalStatus+" the examination held on "+examDate+"  for the recruitment of manpower for the post of "+grade+" "+postName+" of the Secretariat of Bangladesh Jatiya Sangsad.";

%>

<style>

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

</style>

<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg p-0" id="bill-div">
                <div class="ml-auto my-5">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', 'Result notice');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="mx-1">
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div class="row">
                                <div class="col-12 text-center">

                                    <h5 class="mt-2 font-weight-bold">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                    </h5>
                                    <h5 class="text-dark">
                                        <%=UtilCharacter.getDataByLanguage(Language, "মানব সম্পদ উইং", "Human Resources Wing")%>
                                    </h5>
                                    <a class="text-dark" href="https://www.Parliament.gov.bd" target="_blank">
                                        <u><%=UtilCharacter.getDataByLanguage(Language, "www.Parliament.gov.bd", "www.Parliament.gov.bd")%></u>
                                    </a>

                                </div>
                            </div>

                            <div class="row mt-2 ">
                                <div class="col-12 " style="display: flex;justify-content: space-between;">

                                    <span>
                                        <%=UtilCharacter.getDataByLanguage(Language, "নংঃ- ", "Subject:- ")%>&nbsp;
                                    </span>

                                    <span >
                                        <% String formattedBillBanglaDateInWord = BanglaDateConverter.getFormattedBanglaDate(admitDateOfExam, true);
                                            String formattedBillDateInWord = DateUtils.getDateInWord("bangla", admitDateOfExam);

                                        %>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td rowspan="2">তারিখ:</td>
                                                <td style="text-align: center;"><%=formattedBillBanglaDateInWord%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid black; text-align: center;">
                                                    <%=formattedBillDateInWord%>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </span>
                                </div>
                            </div>

                            <div class=" mt-10 mb-3 text-center" >

                                <u style="text-underline-position: under;"><%=UtilCharacter.getDataByLanguage(Language, "বিজ্ঞপ্তি", "Notice")%></u>

                            </div>

                            <div class=" mt-10 mx-2" style="text-indent: 3rem;">

                                <%=UtilCharacter.getDataByLanguage(Language, noticeHeaderBN, noticeHeaderEN)%>

                            </div>


                            <div class="row mx-2 mt-1">
                                <table
                                        class="mt-3 w-100 rounded"
                                        border="1px solid #323233;"
                                        style="border-collapse: collapse"
                                >
                                    <thead>
                                    <tr>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "পদের নাম", "Post Name")%>
                                        </th>
                                        <th class="text-center"
                                            style="padding: 5px 10px"><%=UtilCharacter.getDataByLanguage(Language, "রোল নম্বর", "Roll Number")%>
                                        </th>


                                    </tr>
                                    </thead>
                                    <tbody>


                                    <tr>

                                        <td style="padding: 5px 10px;min-width: 12rem;text-align: center;">


                                            <%=postName%>

                                        </td>
                                        <td style="padding: 10px 20px;">

                                            <%
                                                int dtoSize = job_applicant_applicationDTOS.size();
                                                int curSize = 0;
                                                for (Job_applicant_applicationDTO job_applicant_applicationDTO:job_applicant_applicationDTOS){

                                            %>

                                            <%=Utils.getDigits(job_applicant_applicationDTO.rollNumber, Language)%><%if(dtoSize != ++curSize)%>,&nbsp;
                                            <%
                                                }
                                            %>

                                        </td>


                                    </tr>



                                    </tbody>
                                </table>
                            </div>



                            <div class="mt-5">
                                <div class="row">
                                    <div class="col-3 offset-9 text-center">
                                        <div>
                                            <%
                                                byte[] encodeBase64Photo = null ;
                                                if(employee_recordsDTO != null && employee_recordsDTO.signature != null){
                                                    encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.signature);
                                                }

                                            %>
                                            <img
                                                    src="data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>"
                                                    width="50%"
                                                    id="applicantSignature"
                                            >
                                        </div>
                                        <div>
                    <span
                            class=""
                            id="applicantName"
                            placeholder="নাম"
                    >
                    (<%out.println(employee_recordsDTO != null ? employee_recordsDTO.nameBng:"");%>)
                </span>
                                        </div>
                                        <div>
                    <span
                            class="text-nowrap"
                            id="applicantOrganogramName"
                            placeholder="পদের নাম"
                    >
                    <%out.println(admit_cardDTO != null ? admit_cardDTO.signatory_post_name_bn : "");%>
                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="<%=context_folder%>/assets/backup/js/jquery-3.5.1.slim.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/bootstrap.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/popper.min.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/login/main.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/util1.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/pb.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery.min.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-validation/js/jquery.validate.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-ui/jquery-ui.min.js"></script>

<script>
    let downloadTimer;

    $(document).ready(function () {
        $("#password_submit").prop("disabled", true);
        $("#resend_otp").prop("disabled", true);

    });

    const phoneNoInput = document.getElementById("phoneNo");
    const emailInput = document.getElementById("email");
    const validChars = new Set([
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯',
    ]);

    const validOTPChars = new Set([
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    ]);

    phoneNoInput.addEventListener("keypress", function (event) {
        if (!validChars.has(event.key)) {
            event.preventDefault();
        }
    });

    document.getElementById('otp_input').addEventListener("keypress", function (event) {
        if (!validOTPChars.has(event.key)) {
            event.preventDefault();
        }
    });

    phoneNoInput.addEventListener("keyup", function (event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            resetCitizenPinCode();
        }
    });

    function sendOTP(isResend) {
        const pinCodeRetrieveForm = $("#pinCodeRetrieveForm");
        const url = "UserServlet?actionType=sendOTP";
        $.ajax({
            type: "POST",
            url: url,
            data: pinCodeRetrieveForm.serialize(),
            dataType: 'text',
            success: function (data) {
                const response = JSON.parse(data);
                if (response.success) {
                    showToast(response.messageBn, response.messageEn);
                    if (!isResend) {
                        $("#mobile_submit").prop("disabled", true);
                        $("#mobile_submit_div").removeClass('d-flex');
                        $("#mobile_submit_div").hide();
                        document.getElementById("phoneNo").readOnly = true;
                        document.getElementById("email").readOnly = true;
                        $("#methodDiv").removeClass('d-flex');
                        $("#methodDiv").hide();
                        $("#otp_password_div").show();
                    } else {
                        $("#resend_otp").prop("disabled", true);
                        $("#resend_otp_div").hide();
                        clearOTPPasswords();
                        clearInterval(downloadTimer);
                    }
                    $("#password_submit").prop("disabled", false);
                    $("#password_submit_div").show();
                    timer(response.TimeRemaining);
                } else {
                    showError(response.messageBn, response.messageEn);
                }
            },
            error: function (req, err) {
                showError("ওটিপি প্রদান করা সম্ভব হয়নি", "Could not send OTP");
            }
        });
    }

    function clearOTPPasswords() {
        $("#otp_input").val("");
        $("#password").val("");
        $("#confirmPassword").val("");
    }

    function submitPassword() {
        const pinCodeRetrieveForm = $("#pinCodeRetrieveForm");
        if ($("#otp_input").val() === "") {
            showToast("ওটিপি ইনপুট দিন", "Please Insert OTP");
            return;
        }

        const passwordVal = $("#password").val();
        if (passwordVal === "") {
            showToast("পাসওয়ার্ড ইনপুট দিন", "Please Insert Password");
            return;
        }

        const confirmPasswordVal = $("#confirmPassword").val();
        if (confirmPasswordVal === "") {
            showToast("পাসওয়ার্ড কনফার্ম করুন", "Please Confirm Password");
            return;
        }

        if (passwordVal.length < <%=UserServlet.MIN_PASSWORD_LENGTH%>) {
            showToast("পাসওয়ার্ড ন্যূনতম দৈর্ঘ্যের চেয়ে ছোট হয়েছে", "Password is too short");
            return;
        }

        if (confirmPasswordVal !== passwordVal) {
            showToast("একই পাসওয়ার্ড দিয়ে নিশ্চিত করুন", "Confirm with same password");
            return;
        }

        const url = "UserServlet?actionType=validatePasswordOTP";
        $.ajax({
            type: "POST",
            url: url,
            data: pinCodeRetrieveForm.serialize(),
            dataType: 'text',
            success: function (data) {
                console.log("data", data);
                const response = JSON.parse(data);
                if (response.success) {
                    showToast(response.messageBn, response.messageEn);
                    setTimeout(
                        () => window.location.replace("<%=request.getContextPath()%>" + response.redirectPath),
                        3000
                    );
                } else {
                    showError(response.messageBn, response.messageEn);
                }
            },
            error: function (req, err) {
                showError("পাসওয়ার্ড পরিবর্তন ব্যর্থ হয়েছে!", "Password change failed!");
            }
        });
    }

    function timer(timeLeft) {
        downloadTimer = setInterval(function () {
            if (timeLeft <= 0) {
                clearInterval(downloadTimer);
                document.getElementById("p_message").innerHTML = "ওটিপি মেয়াদোত্তীর্ণ হয়েছে";
                timerOut();
            } else {
                let minutes = Math.floor(timeLeft / 60);
                let seconds = Math.floor(timeLeft % 60);
                document.getElementById("p_message").innerHTML = "ওটিপি মেয়াদোত্তীর্ণ হবে "
                    + convertToBangla(minutes) + " মিনিট "
                    + convertToBangla(seconds) + " সেকেন্ড";
            }
            timeLeft -= 1;
        }, 1000);
    }

    function timerOut() {
        $("#password_submit").prop("disabled", true);
        $("#password_submit_div").hide();
        $("#resend_otp").prop("disabled", false);
        $("#resend_otp_div").show();
        clearOTPPasswords();
    }

    function showToast(bn, en) {
        const toastMessageDiv = document.getElementById("toast_message_div");
        toastMessageDiv.innerHTML = '';
        const my_language = document.getElementById('my_language').value;
        toastMessageDiv.innerHTML = parseInt(my_language) === 1 ? bn : en;

        document.getElementById('toast_message').classList.add("show");

        setTimeout(() => document.getElementById('toast_message').classList.remove("show"), 3000);
    }

    function showError(bn, en) {
        const toastMessageDiv = document.getElementById("error_message_div");
        toastMessageDiv.innerHTML = '';
        const my_language = document.getElementById('my_language').value;
        toastMessageDiv.innerHTML = parseInt(my_language) === 1 ? bn : en;

        document.getElementById('error_message').classList.add("show");

        setTimeout(() => document.getElementById('error_message').classList.remove("show"), 3000);
    }

    function convertToBangla(str) {
        str = String(str);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    let currentValue = 'mobile';

    function handleMethodRadioClick(methodRadio) {
        currentValue = methodRadio.value;
        if (currentValue == 'mobile') {
            document.getElementById('p_message').innerText = 'পাসওয়ার্ড রিসেটের জন্য ওটিপি পেতে আপনার মোবাইল নম্বর (বাংলায়/ইংরেজিতে)টি দিন';
            $("#mobileNoDiv").show();
            $("#emailDiv").hide();
        } else {
            document.getElementById('p_message').innerText = 'পাসওয়ার্ড রিসেটের জন্য ওটিপি পেতে আপনার ইমেইল আইডিটি দিন';
            $("#mobileNoDiv").hide();
            $("#emailDiv").show();
        }

    }
</script>
</body>
</html>