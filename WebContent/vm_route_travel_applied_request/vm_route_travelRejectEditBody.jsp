<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vm_route_travel.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="vm_requisition.CommonApprovalStatus" %>

<%
Vm_route_travelDTO vm_route_travelDTO;
vm_route_travelDTO = (Vm_route_travelDTO)request.getAttribute("vm_route_travelDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(vm_route_travelDTO == null)
{
	vm_route_travelDTO = new Vm_route_travelDTO();
	
}
System.out.println("vm_route_travelDTO = " + vm_route_travelDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_ADD_FORMNAME, loginDTO);
String servletName = "Vm_route_travel_applied_requestServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID = -1;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Vm_route_travel_applied_requestServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                     </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=vm_route_travelDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=vm_route_travelDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=vm_route_travelDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=vm_route_travelDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=vm_route_travelDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=vm_route_travelDTO.lastModificationTime%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=vm_route_travelDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requestedStoppageId' id = 'requestedStoppageId_hidden_<%=i%>' value='<%=vm_route_travelDTO.requestedStoppageId%>' tag='pb_html'/>
													<input type='hidden' class='form-control' name='status' id = 'status_hidden_<%=i%>' value='<%=CommonApprovalStatus.DISSATISFIED.getValue()%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_DECISIONREMARKS, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='decisionRemarks' id = 'decisionRemarks_text_<%=i%>' value='<%=vm_route_travelDTO.decisionRemarks%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FILESDROPZONE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																fileColumnName = "filesDropzone";
																if(actionName.equals("edit"))
																{
																	List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_route_travelDTO.filesDropzone);
																%>			
																	<%@include file="../pb/dropzoneEditor.jsp"%>
																<%
																}
																else
																{
																	ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
																	vm_route_travelDTO.filesDropzone = ColumnID;
																}
																%>
				
																<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_route_travelDTO.filesDropzone%>">
																	<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>			
																</div>								
																<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
																<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=vm_route_travelDTO.filesDropzone%>'/>		
		


															</div>
                                                      </div>
					
														</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>                               
                          </div>
                       </div>
                       <div class="form-actions text-center mb-5">
	                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_CANCEL_BUTTON, loginDTO)%></a>										
							<button class="btn btn-success" type="submit"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_SUBMIT_BUTTON, loginDTO)%></button>
					   </div>
                   </form>
               </div>                      
          </div>
      </div>
 </div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vm_route_travel_applied_requestServlet");
}

function init(row)
{

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






