<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="physiotherapy_plan.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="appointment.*" %>
<%@page import="patient_measurement.*" %>
<%@page import="util.*" %>


<%
    String servletName = "Physiotherapy_planServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.PHYSIOTHERAPY_PLAN_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Physiotherapy_planDAO physiotherapy_planDAO = new Physiotherapy_planDAO("physiotherapy_plan");
    Physiotherapy_planDTO physiotherapy_planDTO = physiotherapy_planDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

    AppointmentDAO appointmentDAO = new AppointmentDAO();
    AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(physiotherapy_planDTO.appointmentId);
    String context = request.getContextPath() + "/";
    String justView = request.getParameter("justView");
    boolean bJustView = (justView != null);

%>
<!-- begin:: Subheader -->
<div class="ml-auto mr-3 mt-4">

    <button type="button" class="btn" id='printer1'
            onclick="downloadPdf('Report.pdf','modalbody')">
        <%--        <%=LM.getText(LC.HM_PDF, loginDTO)%>--%>
        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='printer2'
            onclick="printAnyDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
        <%--        <%=LM.getText(LC.HM_PRINT, loginDTO)%>--%>
    </button>
</div>
<!-- end:: Subheader -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row" id="modalbody">
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-8 col-xl-9">
                        <div class="d-flex align-items-center">
                            <div>
                                <img
                                        width="30%"
                                        src="<%=context%>assets/static/parliament_logo.png"
                                        alt="logo"
                                        class="logo-default"
                                />
                            </div>
                            <div class="prescription-parliament-info">
                                <h3 class="text-left">
                                    <%=LM.getText(LC.HM_PARLIAMENT_MEDICAL_CENTRE, loginDTO)%>
                                </h3>
                                <h5 class="text-left">
                                    <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                                </h5>
                                <h5 class="text-left">
                                    <%=LM.getText(LC.HM_PARLIAMENT_PHONE, loginDTO)%>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xl-3">
                        <div class="table-content patient_info_div">
                            <table class="table table-borderless rounded">
                                <thead></thead>
                                <tbody>
                                <tr>
                                    <td class="row row-margin">
                                        <div class="col text-right">
                                            <%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>:
                                        </div>
                                        <div class="col">
                                            <%=physiotherapy_planDTO.name%>
                                        </div>
                                    </td>
                                </tr>
                                <%
                                    if (!WorkflowController.getNameFromOrganogramId(appointmentDTO.patientId, Language).equalsIgnoreCase(physiotherapy_planDTO.name)) {
                                %>
                                <tr>
                                    <td class="row row-margin">
                                        <div class="col text-right">
                                            <%=LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)%>:
                                        </div>
                                        <div class="col">
                                            <%=WorkflowController.getNameFromOrganogramId(appointmentDTO.patientId, Language)%>
                                        </div>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                                <tr>
                                    <td class="row row-margin">
                                        <div class="col text-right">
                                            <%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_AGE, loginDTO)%>:
                                        </div>
                                        <div class="col">
                                            <%=TimeFormat.getAge(appointmentDTO.dateOfBirth, Language)%>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row row-margin">
                                        <div class="col text-right">
                                            <%=LM.getText(LC.HM_GENDER, loginDTO)%>:
                                        </div>
                                        <div class="col">
                                            <%=CatDAO.getName(Language, "gender", appointmentDTO.genderCat)%>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row row-margin">
                                        <div class="col text-right">
                                            <%=Language.equalsIgnoreCase("english")?"Physiotherapist":"থেরাপিদাতা"%>:
                                        </div>
                                        <div class="col">
                                            <%=WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, Language)%>
                                        </div>
                                    </td>
                                </tr>
                                <%
                                    String dt = simpleDateFormat.format(new Date(appointmentDTO.visitDate));
                                %>
                                <tr>
                                    <td class="row row-margin">
                                        <div class="col text-right">
                                            <%=LM.getText(LC.APPOINTMENT_ADD_VISITDATE, loginDTO)%>:
                                        </div>
                                        <div class="col">
                                            <%=Utils.getDigits(dt, Language)%>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="table-section">
                    <div class="mt-5">
                        <h5>
                            <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PATIENT_MEASUREMENT_ADD_FORMNAME, loginDTO)%>
                        </h5>
                        <table class="table table-bordered table-striped">
                            <thead></thead>
                            <tbody>
                            <tr class="table-success">
                                <th><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_WEIGHT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_HEIGHT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_BMI, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_TEMPERATURE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.HM_BLOOD_PRESSURE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_OXYGENSATURATION, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PULSE, loginDTO)%>
                                </th>
                            </tr>
                            <tr>
                                <td><%=physiotherapy_planDTO.weight != -1 ? Utils.getDigits(physiotherapy_planDTO.weight, Language) : ""%>
                                </td>
                                <td><%=physiotherapy_planDTO.height != -1 ? Utils.getDigits(physiotherapy_planDTO.height, Language) : ""%>
                                </td>
                                <td><%=physiotherapy_planDTO.bmi != -1 ? Utils.getDigits(String.format("%.2f", physiotherapy_planDTO.bmi), Language) : ""%>
                                </td>
                                <td><%=physiotherapy_planDTO.temperature != -1 ? Utils.getDigits(physiotherapy_planDTO.temperature, Language) : ""%>
                                </td>
                                <td><%=physiotherapy_planDTO.bloodPressureSystole != -1 ? Utils.getDigits(physiotherapy_planDTO.bloodPressureSystole, Language) + " /" : ""%>
                                    <%=physiotherapy_planDTO.bloodPressureDiastole != -1 ? Utils.getDigits(physiotherapy_planDTO.bloodPressureDiastole, Language) : ""%>
                                </td>
                                <td><%=physiotherapy_planDTO.oxygenSaturation != -1 ? Utils.getDigits(physiotherapy_planDTO.oxygenSaturation, Language) : ""%>
                                </td>
                                <td><%=physiotherapy_planDTO.pulse != -1 ? Utils.getDigits(physiotherapy_planDTO.pulse, Language) : ""%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-5">
                        <table class="table table-bordered table-striped">


                            <%
                                if (true) {
                            %>

                            <%
                                if (!physiotherapy_planDTO.remarks.equalsIgnoreCase("")) {
                            %>
                            <tr>
                                <td><strong><%=LM.getText(LC.HM_REMARKS, loginDTO)%>
                                </strong></td>
                            </tr>
                            <tr>
                                <td><%=physiotherapy_planDTO.remarks%>
                                </td>
                            </tr>

                            <%
                                    }
                                }
                            %>
                        </table>
                    </div>
                    <div class="mt-5">
                        <h5>
                            <%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE, loginDTO)%>
                        </h5>
                        <%
                            if (!bJustView) {
                        %>
                        <form class="form-horizontal"
                              action="Physiotherapy_planServlet?actionType=addTherapy&ID=<%=physiotherapy_planDTO.iD%>"
                              id="bigform" name="bigform" method="POST"
                              onsubmit="return PreprocessBeforeSubmiting()">
                            <%
                                }
                            %>
                            <table class="table table-bordered table-striped">
                                <thead>
                                </thead>
                                <tbody>
                                <tr class="table-danger">
                                    <th><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_THERAPYDATE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.HM_ACTUAL_THERAPY_TIME, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_THERAPYCAT, loginDTO)%>
                                    </th>
                                    <th>Disease</th>
                                	<th>Treatment</th>
                                    <th><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_THERAPISTUSERID, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_REMARKS, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_ISDONE, loginDTO)%>
                                    </th>
                                </tr>
                                    <%
	                        	PhysiotherapyScheduleDAO physiotherapyScheduleDAO = new PhysiotherapyScheduleDAO();
	                         	List<PhysiotherapyScheduleDTO> physiotherapyScheduleDTOs = physiotherapyScheduleDAO.getPhysiotherapyScheduleDTOListByPhysiotherapyPlanID(physiotherapy_planDTO.iD);
	                         	
	                         	for(PhysiotherapyScheduleDTO physiotherapyScheduleDTO: physiotherapyScheduleDTOs)
	                         	{
	                         		%>
                                <tr>
                                    <td>
                                        <%
                                            value = physiotherapyScheduleDTO.therapyDate + "";
                                        %>
                                        <%
                                            String formatted_therapyDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_therapyDate, Language)%>,

                                        <%
                                            value = physiotherapyScheduleDTO.therapyTime + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </td>
                                    <td>
                                        <%
                                            if (physiotherapyScheduleDTO.isDone) {
                                                formatted_therapyDate = dateTimeFormat.format(new Date(physiotherapyScheduleDTO.actualTherapyTime));
                                            } else {
                                                formatted_therapyDate = "";
                                            }
                                        %>
                                        <%=Utils.getDigits(formatted_therapyDate, Language)%>
                                    </td>
                                    <td>
                                       
                                        <%
                                        	value = CommonDAO.getName(Language, "therapy_type", physiotherapyScheduleDTO.therapyTypeType);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </td>
                                    <td><%=physiotherapyScheduleDTO.therapyDiseaseName%>
                                    </td>
                                    <td><%=CatDAO.getName(Language, "therapy_treatment", physiotherapyScheduleDTO.therapyTreatmentCat)%>
                                    </td>
                                    <td>
                                        <%
                                            if (physiotherapyScheduleDTO.therapistOrganogramId != -1) {
                                                value = WorkflowController.getNameFromOrganogramId(physiotherapyScheduleDTO.therapistOrganogramId, Language);
                                            } else {
                                                value = "";
                                            }

                                        %>

                                        <%=Utils.getDigits(value, Language)%>
                                    </td>

                                    <td>
                                        <%
                                            if (bJustView || physiotherapyScheduleDTO.isDone) {
                                        %>
                                        <%
                                            value = physiotherapyScheduleDTO.remarks + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>
                                        <input type="hidden" name="remarks"
                                               value="<%=physiotherapyScheduleDTO.remarks%>"/>
                                        <%
                                        } else {
                                        %>
                                        <textarea class='form-control' name='remarks'
                                                  tag='pb_html'><%=physiotherapyScheduleDTO.remarks%></textarea>
                                        <%
                                            }
                                        %>
                                    </td>
                                    <td>
                                        <%
                                            if (bJustView || physiotherapyScheduleDTO.isDone) {
                                        %>
                                        <input type="hidden" name='isDone'
                                               value="<%=physiotherapyScheduleDTO.isDone%>"/>
                                        <%

                                            if (physiotherapyScheduleDTO.isDone) {
                                        %>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <%
                                        } else {
                                        %>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                        <%
                                            }

                                        %>
                                        <%
                                        } else {
                                        %>
                                        <input type='checkbox' class='form-control-sm'
                                               onchange="setIsdone(this.id, 'isDone_<%=physiotherapyScheduleDTO.iD%>')"
                                               name='isDoneCb'
                                               id='isDoneCb_<%=physiotherapyScheduleDTO.iD%>'  <%=physiotherapyScheduleDTO.isDone ? "checked" : ""%>
                                               tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='scId'
                                               value="<%=physiotherapyScheduleDTO.iD%>" tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='isDone'
                                               id='isDone_<%=physiotherapyScheduleDTO.iD%>'
                                               value="<%=physiotherapyScheduleDTO.isDone%>" tag='pb_html'/>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                    <%
	                         		
	                         	}
	                         	
	                        %>
                            </table>
                            <%
                                if (!bJustView) {
                            %>
                            <div class="form-actions text-right">
                                <button class="btn btn-sm btn-success shadow btn-border-radius" id = "submitButton"
                                        type="submit"><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_PLAN_SUBMIT_BUTTON, loginDTO)%>
                                </button>
                            </div>
                        </form>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function setIsdone(cbId, hiddenId) {
        console.log("setIsdone called " + hiddenId);
        if ($("#" + cbId).prop("checked")) {
            $("#" + hiddenId).val("true");
        } else {
            $("#" + hiddenId).val("flase");
        }

    }
    
    function PreprocessBeforeSubmiting()
    {
    	$("#submitButton").prop("disabled",true);
    }
</script>