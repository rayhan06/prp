<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="physiotherapy_plan.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@page import="appointment.*" %>
<%@page import="patient_measurement.*" %>
<%@page import="therapy_type.*" %>

<%
    Physiotherapy_planDTO physiotherapy_planDTO;
    physiotherapy_planDTO = (Physiotherapy_planDTO) request.getAttribute("physiotherapy_planDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (physiotherapy_planDTO == null) {
        physiotherapy_planDTO = new Physiotherapy_planDTO();

    }
    System.out.println("physiotherapy_planDTO = " + physiotherapy_planDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_PLAN_ADD_FORMNAME, loginDTO);
    String servletName = "Physiotherapy_planServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;

    long appointmentId = -1;
    AppointmentDAO appointmentDAO = new AppointmentDAO();
    Patient_measurementDAO patient_measurementDAO = new Patient_measurementDAO();
    AppointmentDTO appointmentDTO = null;
    Patient_measurementDTO patient_measurementDTO = null;

    if (request.getParameter("appointmentId") != null && actionName.equals("add")) {
        appointmentId = Long.parseLong(request.getParameter("appointmentId"));
        appointmentDTO = appointmentDAO.getDTOByID(appointmentId);
        patient_measurementDTO = patient_measurementDAO.getLatestDTO(appointmentDTO.employeeUserName, appointmentDTO.whoIsThePatientCat);

        if (patient_measurementDTO != null) {
            physiotherapy_planDTO.bloodPressureDiastole = patient_measurementDTO.bloodPressureDiastole;
            physiotherapy_planDTO.bloodPressureSystole = patient_measurementDTO.bloodPressureSystole;

            physiotherapy_planDTO.bmi = patient_measurementDTO.bmi;
            physiotherapy_planDTO.height = patient_measurementDTO.height;
            physiotherapy_planDTO.weight = patient_measurementDTO.weight;
            physiotherapy_planDTO.pulse = patient_measurementDTO.pulse;
            physiotherapy_planDTO.oxygenSaturation = patient_measurementDTO.oxygenSaturation;
        }

        physiotherapy_planDTO.appointmentId = appointmentDTO.iD;
        physiotherapy_planDTO.age = appointmentDTO.age;
        physiotherapy_planDTO.name = appointmentDTO.patientName;
        physiotherapy_planDTO.dateOfBirth = appointmentDTO.dateOfBirth;


    } else {
        appointmentDTO = appointmentDAO.getDTOByID(physiotherapy_planDTO.appointmentId);
    }


    String Language = LM.getText(LC.PHYSIOTHERAPY_PLAN_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Physiotherapy_planServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-10 text-right">
<%--                         <%@include file="../pb/patientHistory.jsp" %> --%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white">
                                                        <%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.iD%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='appointmentId'
                                                   id='appointmentId_hidden_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.appointmentId%>' tag='pb_html'/>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_NAME, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <input type='text' class='form-control' name='name'
                                                           id='name_text_<%=i%>' value='<%=physiotherapy_planDTO.name%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_DATEOFBIRTH, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <input type='text' class='form-control' readonly="readonly"
                                                           data-label="Document Date" id='dateOfBirth_date_<%=i%>'
                                                           name='dateOfBirth' value=<%
																String formatted_dateOfBirth = dateFormat.format(new Date(physiotherapy_planDTO.dateOfBirth));
																%>
                                                                   '<%=formatted_dateOfBirth%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='age' id='age_text_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.age%>' tag='pb_html'/>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_HEIGHT, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <%
                                                        value = "";
                                                        if (physiotherapy_planDTO.height != -1) {
                                                            value = physiotherapy_planDTO.height + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control' name='height'
                                                           id='height_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_WEIGHT, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <%
                                                        value = "";
                                                        if (physiotherapy_planDTO.weight != -1) {
                                                            value = physiotherapy_planDTO.weight + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control' name='weight'
                                                           id='weight_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.HM_BLOOD_PRESSURE, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <div class="row d-flex justify-content-between"
                                                         style="font-family: 'Roboto', 'SolaimanLipi', sans-serif; font-weight: 400;">
                                                        <div class="col-6 row d-flex align-items-center pr-0">
                                                            <div class="col-3">
                                                                <%=LM.getText(LC.HM_SYSTOLE, loginDTO)%>
                                                            </div>
                                                            <div class="col-9">
                                                                <%
                                                                    value = "";
                                                                    if (physiotherapy_planDTO.bloodPressureSystole != -1) {
                                                                        value = physiotherapy_planDTO.bloodPressureSystole + "";
                                                                    }
                                                                %>
                                                                <input type='number' class='form-control'
                                                                       name='bloodPressureSystole'
                                                                       id='bloodPressureSystole_text_<%=i%>'
                                                                       value='<%=value%>'   tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                        <div class="col-6 row d-flex align-items-center pl-0">
                                                            <div class="col-4 pl-0">
                                                                <%=LM.getText(LC.HM_DIASTOLE, loginDTO)%>
                                                            </div>
                                                            <div class="col-8 pl-0">
                                                                <%
                                                                    value = "";
                                                                    if (physiotherapy_planDTO.bloodPressureDiastole != -1) {
                                                                        value = physiotherapy_planDTO.bloodPressureDiastole + "";
                                                                    }
                                                                %>
                                                                <input type='number' class='form-control'
                                                                       name='bloodPressureDiastole'
                                                                       id='bloodPressureDiastole_text_<%=i%>'
                                                                       value='<%=value%>'   tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PULSE, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <%
                                                        value = "";
                                                        if (physiotherapy_planDTO.pulse != -1) {
                                                            value = physiotherapy_planDTO.pulse + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control' name='pulse'
                                                           id='pulse_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_TEMPERATURE, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <%
                                                        value = "";
                                                        if (physiotherapy_planDTO.temperature != -1) {
                                                            value = physiotherapy_planDTO.temperature + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control' name='temperature'
                                                           id='temperature_number_<%=i%>' value='<%=value%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_OXYGENSATURATION, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <%
                                                        value = "";
                                                        if (physiotherapy_planDTO.oxygenSaturation != -1) {
                                                            value = physiotherapy_planDTO.oxygenSaturation + "";
                                                        }
                                                    %>
                                                    <input type='number' class='form-control' name='oxygenSaturation'
                                                           id='oxygenSaturation_number_<%=i%>' value='<%=value%>'
                                                           tag='pb_html'>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='bmi' id='bmi_number_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.bmi%>' tag='pb_html'>
                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right"><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_REMARKS, loginDTO)%>
                                                </label>
                                                <div class="col-9">
                                                    <input type='text' class='form-control' name='remarks'
                                                           id='remarks_text_<%=i%>'
                                                           value='<%=physiotherapy_planDTO.remarks%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.insertedByUserId%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.insertionDate%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='lastModifierUser'
                                                   id='lastModifierUser_hidden_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.lastModifierUser%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.isDeleted%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=physiotherapy_planDTO.lastModificationTime%>'
                                                   tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_THERAPYDATE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_THERAPYTIME, loginDTO)%>
                                </th>
                                <th><%=Language.equalsIgnoreCase("english")?"Therapy Type":"থেরাপির ধরণ"%></th>
                                <th><%=Language.equalsIgnoreCase("english")?"Disease":"রোগ"%></th>
                                <th><%=Language.equalsIgnoreCase("english")?"Treatment":"চিকিৎসা"%></th>

                                <th><%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-PhysiotherapySchedule">


                            <%
                                if (actionName.equals("edit")) {
                                    int index = -1;


                                    for (PhysiotherapyScheduleDTO physiotherapyScheduleDTO : physiotherapy_planDTO.physiotherapyScheduleDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>

                            <tr id="PhysiotherapySchedule_<%=index + 1%>">
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='physiotherapySchedule.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.physiotherapyPlanId'
                                           id='physiotherapyPlanId_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.physiotherapyPlanId%>'
                                           tag='pb_html'/>
                                </td>
                                <td>


                                    <%value = "therapyDate_js_" + childTableStartingID;%>
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' name='physiotherapySchedule.therapyDate'
                                           id='therapyDate_date_<%=childTableStartingID%>'
                                           value='<%=dateFormat.format(new Date(physiotherapyScheduleDTO.therapyDate))%>'
                                           tag='pb_html'>
                                </td>
                                <td>


                                    <%value = "therapyTime_js_" + childTableStartingID;%>
                                    <jsp:include page="/time/time.jsp">
                                        <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' value="<%=physiotherapyScheduleDTO.therapyTime%>"
                                           name='physiotherapySchedule.therapyTime'
                                           id='therapyTime_time_<%=childTableStartingID%>' tag='pb_html'/>
                                </td>
                                
                                <td>
                                    <select class='form-control' name='physiotherapySchedule.therapyCat'
                                            id='therapyCat_category_<%=childTableStartingID%>' tag='pb_html' onchange="therapyTypeSelected(this.id)">
                                        <%=Therapy_typeRepository.getInstance().getOptions(Language, physiotherapyScheduleDTO.therapyTypeType)%>
                                    </select>

                                </td>
								
								<td>
                                    <select class='form-control' name='physiotherapySchedule.therapyDiseaseType'
                                            id='therapyDiseaseType_type_<%=childTableStartingID%>' tag='pb_html' required 
                                            onchange="therapyDiseaseSelected(this.id)">
                                        <option value = "<%=physiotherapyScheduleDTO.therapyDiseaseType%>"><%=physiotherapyScheduleDTO.therapyDiseaseName%></option>
                                    </select>
                                    <input type = 'text' class='form-control' name='physiotherapySchedule.therapyDiseaseName'
                                    value = '<%=physiotherapyScheduleDTO.therapyDiseaseName%>'
                                    id='therapyDiseaseName_type_<%=childTableStartingID%>' tag='pb_html' />
                                </td>
								
								<td>
									<select class='form-control' name='physiotherapySchedule.therapyTreatmentCat'
	                                        id='therapyTreatmentCat_category_<%=childTableStartingID%>' tag='pb_html'>
	                                    <%
	                                    	Options = CatDAO.getOptions("English", "therapy_treatment", physiotherapyScheduleDTO.therapyTreatmentCat);
	                                    %>
	                                    <%=Options%>
	                                </select>
								</td>

                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.isDone'
                                           id='isDone_text_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.isDone%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.remarks'
                                           id='remarks_text_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.remarks%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.insertionDate%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.lastModifierUser'
                                           id='lastModifierUser_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.lastModifierUser%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.therapistUserId'
                                           id='therapistUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.therapistUserId%>' tag='pb_html'/>
                                </td>

                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.isDeleted%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='physiotherapySchedule.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=physiotherapyScheduleDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <div class='checker'><span id='chkEdit'><input type='checkbox'
                                                                                   id='physiotherapySchedule_cb_<%=index%>'
                                                                                   name='checkbox'
                                                                                   value=''/></span></div>
                                </td>
                            </tr>
                            <%
                            	childTableStartingID++;
                                                                }
                                                            }
                            %>

                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-9 text-right">
                            <button
                                    id="add-more-PhysiotherapySchedule"
                                    name="add-morePhysiotherapySchedule"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                            </button>
                            <button
                                    id="remove-PhysiotherapySchedule"
                                    name="removePhysiotherapySchedule"
                                    type="button"
                                    class="btn btn-sm remove-btn shadow ml-2">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    <%
                    	PhysiotherapyScheduleDTO physiotherapyScheduleDTO = new PhysiotherapyScheduleDTO();
                    %>
                    <template id="template-PhysiotherapySchedule">
                        <tr>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='physiotherapySchedule.iD'
                                       id='iD_hidden_' value='<%=physiotherapyScheduleDTO.iD%>' tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='physiotherapySchedule.physiotherapyPlanId'
                                       id='physiotherapyPlanId_hidden_'
                                       value='<%=physiotherapyScheduleDTO.physiotherapyPlanId%>' tag='pb_html'/>
                            </td>
                            <td>


                                <div id="therapyDate_div_" tag='pb_html'></div>


                                <input type='hidden' name='physiotherapySchedule.therapyDate'
                                       id='therapyDate_date_'
                                       value='<%=dateFormat.format(new Date(physiotherapyScheduleDTO.therapyDate))%>'
                                       tag='pb_html'>
                            </td>
                            <td>


                                <div id="therapyTime_div_" tag='pb_html'></div>

                                <input type='hidden' value="<%=physiotherapyScheduleDTO.therapyTime%>"
                                       name='physiotherapySchedule.therapyTime' id='therapyTime_time_'
                                       tag='pb_html'/>
                            </td>
                                                    
                                  <td>
                                    <select class='form-control' name='physiotherapySchedule.therapyCat'
                                            id='therapyCat_category_' tag='pb_html' onchange="therapyTypeSelected(this.id)">
                                        <%=Therapy_typeRepository.getInstance().getOptions(Language, physiotherapyScheduleDTO.therapyTypeType)%>
                                    </select>

                                </td>
								
								<td>
                                    <select class='form-control' name='physiotherapySchedule.therapyDiseaseType'
                                            id='therapyDiseaseType_type_' tag='pb_html' required 
                                            onchange="therapyDiseaseSelected(this.id)">
                                        <option value = "<%=physiotherapyScheduleDTO.therapyDiseaseType%>"><%=physiotherapyScheduleDTO.therapyDiseaseName%></option>
                                    </select>
                                    <input type = 'hidden' class='form-control' name='physiotherapySchedule.therapyDiseaseName'
                                    id='therapyDiseaseName_type_' value='<%=physiotherapyScheduleDTO.therapyDiseaseName%>' tag='pb_html' />
                                </td>
								
								<td>
									<select class='form-control' name='physiotherapySchedule.therapyTreatmentCat'
	                                        id='therapyTreatmentCat_category_' tag='pb_html'>
	                                    <%
	                                    	Options = CatDAO.getOptions("English", "therapy_treatment", physiotherapyScheduleDTO.therapyTreatmentCat);
	                                    %>
	                                    <%=Options%>
	                                </select>
								</td>

                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='physiotherapySchedule.isDone'
                                       id='isDone_text_' value='<%=physiotherapyScheduleDTO.isDone%>'
                                       tag='pb_html'/>
                                <input type='hidden' class='form-control' name='physiotherapySchedule.remarks'
                                       id='remarks_text_' value='<%=physiotherapyScheduleDTO.remarks%>'
                                       tag='pb_html'/>


                                <input type='hidden' class='form-control'
                                       name='physiotherapySchedule.insertedByUserId'
                                       id='insertedByUserId_hidden_'
                                       value='<%=physiotherapyScheduleDTO.insertedByUserId%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='physiotherapySchedule.insertedByOrganogramId'
                                       id='insertedByOrganogramId_hidden_'
                                       value='<%=physiotherapyScheduleDTO.insertedByOrganogramId%>'
                                       tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='physiotherapySchedule.insertionDate' id='insertionDate_hidden_'
                                       value='<%=physiotherapyScheduleDTO.insertionDate%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='physiotherapySchedule.lastModifierUser'
                                       id='lastModifierUser_hidden_'
                                       value='<%=physiotherapyScheduleDTO.lastModifierUser%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='physiotherapySchedule.therapistUserId' id='therapistUserId_hidden_'
                                       value='<%=physiotherapyScheduleDTO.therapistUserId%>' tag='pb_html'/>
                            </td>

                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='physiotherapySchedule.isDeleted'
                                       id='isDeleted_hidden_' value='<%=physiotherapyScheduleDTO.isDeleted%>'
                                       tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='physiotherapySchedule.lastModificationTime'
                                       id='lastModificationTime_hidden_'
                                       value='<%=physiotherapyScheduleDTO.lastModificationTime%>'
                                       tag='pb_html'/>
                            </td>
                            <td>
                                <div><span id='chkEdit'><input type='checkbox' name='checkbox' value=''/></span>
                                </div>
                            </td>
                        </tr>

                    </template>
                </div>
                <div class="form-actions text-center">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_PLAN_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit" id = "submitButton">
                        <%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_PLAN_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {

    	 $("select").removeAttr("disabled");
        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("therapyDate_date_" + i)) {
                if (document.getElementById("therapyDate_date_" + i).getAttribute("processed") == null) {
                    console.log("processing therapy date");
                    preprocessDateBeforeSubmitting('therapyDate', i);
                    document.getElementById("therapyDate_date_" + i).setAttribute("processed", "1");
                }
            }

            if (document.getElementById("therapyTime_time_" + i)) {
                if (document.getElementById("therapyTime_time_" + i).getAttribute("processed") == null) {
                    console.log("processing therapy time");
                    preprocessTimeBeforeSubmitting('therapyTime', i);
                    document.getElementById("therapyTime_time_" + i).setAttribute("processed", "1");
                }
            }

        }
        $("#submitButton").prop("disabled",true);
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Physiotherapy_planServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
            setDateByStringAndId('therapyDate_js_' + i, $('#therapyDate_date_' + i).val());
            console.log("setting time the new way");
            setTimeById('therapyTime_js_' + i, $('#therapyTime_time_' + i).val());

        }
        
        <%
        for(int j = 1; j < childTableStartingID; j ++)
        {
        	if(physiotherapy_planDTO.physiotherapyScheduleDTOList.get(j - 1).isDone)
        	{
        		System.out.println("Done found");
        %>
        		var tr = $("#PhysiotherapySchedule_<%=j%>");
        		tr.find( "select" ).attr('disabled', true);
				tr.find( "input" ).attr('readonly', "readonly");
        		$("#physiotherapySchedule_cb_<%=j - 1%>").css("display", "none");
        <%
        	}
        }
        %>

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-PhysiotherapySchedule").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-PhysiotherapySchedule");

            $("#field-PhysiotherapySchedule").append(t.html());
            SetCheckBoxValues("field-PhysiotherapySchedule");

            var tr = $("#field-PhysiotherapySchedule").find("tr:last-child");

            tr.attr("id", "PhysiotherapySchedule_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
               // console.log(index + ": " + $(this).attr('id'));
            });


            //setTimeById('therapyTime_js_' + child_table_extra_id, $('#therapyTime_time_' + child_table_extra_id).val());
            //setTimeById('actualTherapyTime_js_' + child_table_extra_id, $('#actualTherapyTime_time_' + child_table_extra_id).val());

            createDatePicker("therapyDate_div_" + child_table_extra_id, {
                'DATE_ID': 'therapyDate_js_' + child_table_extra_id,
                'LANGUAGE': '<%=Language%>',
                'END_YEAR': new Date().getFullYear() + 1
            }, timeCreator);


        });

    function timeCreator() {
    	setDateByStringAndId('therapyDate_js_' + child_table_extra_id, $('#therapyDate_date_' + child_table_extra_id).val());
        createTimePicker("therapyTime_div_" + child_table_extra_id, {
            'TIME_ID': 'therapyTime_js_' + child_table_extra_id,
            'LANGUAGE': '<%=Language%>',
            'IS_AMPM': 'TRUE'
        }, childIncrementer);
    }

    function childIncrementer() {
        console.log("incrementing child");
        child_table_extra_id++;
    }


    $("#remove-PhysiotherapySchedule").click(function (e) {
        var tablename = 'field-PhysiotherapySchedule';
        var i = 0;
        //console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });
    
    function therapyTypeSelected(typeId)
    {
    	var rowId = typeId.split("_")[2];
    	var diseaseDropdownId = "therapyDiseaseType_type_" + rowId;
    	var defaultVal = $("#" + diseaseDropdownId).val();
    	var type = $("#" + typeId).val();
    	
    	var url = "Therapy_typeServlet?actionType=getDisease&type=" + type + "&defaultVal=" + defaultVal;
    	fillSelect(diseaseDropdownId, url);
    }
	
	function therapyDiseaseSelected(diseaseId)
    {
    	var rowId = diseaseId.split("_")[2];
		var diseaseName = $( "#" + diseaseId + " option:selected" ).text();
		var diseaseNameText = $( "#therapyDiseaseName_type_" + rowId);
		var diseaseVal = $( "#" + diseaseId).val();
		diseaseNameText.val(diseaseName);
		if(diseaseVal == <%=TherapyDiseaseDTO.OTHER%>)
		{
			diseaseNameText.attr("type", "text");
			diseaseNameText.val("");
		}
		else
		{
			diseaseNameText.attr("type", "hidden");
		}
    	
    }


</script>






