<%@page pageEncoding="UTF-8" %>

<%@page import="physiotherapy_plan.*" %>
<%@page import="appointment.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="family.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PHYSIOTHERAPY_PLAN_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PHYSIOTHERAPY_PLAN;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Physiotherapy_planDTO physiotherapy_planDTO = (Physiotherapy_planDTO) request.getAttribute("physiotherapy_planDTO");
    CommonDTO commonDTO = physiotherapy_planDTO;
    String servletName = "Physiotherapy_planServlet";


    System.out.println("physiotherapy_planDTO = " + physiotherapy_planDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Physiotherapy_planDAO physiotherapy_planDAO = (Physiotherapy_planDAO) request.getAttribute("physiotherapy_planDAO");
    AppointmentDAO appointmentDAO = new AppointmentDAO();
    AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(physiotherapy_planDTO.appointmentId);


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    
    PhysiotherapyScheduleDAO physiotherapyScheduleDAO = new PhysiotherapyScheduleDAO();
    int notDoneCount = physiotherapyScheduleDAO.getNotDoneCount(physiotherapy_planDTO.iD);
    int doneCount = physiotherapyScheduleDAO.getDoneCount(physiotherapy_planDTO.iD);

%>
<td>
	<%=Utils.getDigits(simpleDateFormat.format(new Date(appointmentDTO.visitDate)), Language) %>
</td>
<td >
    

    <%=WorkflowController.getNameFromOrganogramId(physiotherapy_planDTO.insertedByOrganogramId, Language)%>


</td>



<td id='<%=i%>_name'>
    <%

        value = appointmentDTO.patientName + "";


    %>
	

    <b><%=value%></b><br> 
    
	<%
        value = appointmentDTO.employeeUserName + "";
        if (value.equalsIgnoreCase("null") || value.equalsIgnoreCase(FamilyDTO.defaultUser)) {
            value = "";
        }
    %>

    <%=LM.getText(LC.HM_ID, loginDTO)%>: <%=Utils.getDigits(value, Language)%><br>
    <%=Utils.getDigits(appointmentDTO.phoneNumber, Language)%>
    <%
    if(appointmentDTO.alternatePhone != null && !appointmentDTO.alternatePhone.equalsIgnoreCase(""))
    {
    	%>
    	<br><%=Utils.getDigits(appointmentDTO.alternatePhone, Language)%>
    	<%
    }
    %>
    
	<%
	 if(appointmentDTO.bearerName != null && !appointmentDTO.bearerName.equalsIgnoreCase(""))
	 {
		 %>
		 <br><%=Language.equalsIgnoreCase("english")?"Bearer's Name":"বাহকের নাম"%>
		 : <%=appointmentDTO.bearerName%>
		 <br><%=Language.equalsIgnoreCase("english")?"Bearer's Phone Number":"বাহকের ফোন নাম্বার"%>
		 : <%=Utils.getDigits(appointmentDTO.bearerPhone, Language)%>
		 
		 <%
	 }
	 %>


</td>

<td id='<%=i%>_dateOfBirth'>
    <%
        value = physiotherapy_planDTO.dateOfBirth + "";
    %>
    <%
        String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_dateOfBirth, Language)%>

	<br>
    <%
        value = physiotherapy_planDTO.age + "";
    %>

    <%=Utils.getDigits(value, Language)%>
    
    <%=LM.getText(LC.HM_YEAR, loginDTO)%>


</td>


<td>
<%
	List<PhysiotherapyScheduleDTO> physiotherapyScheduleDTOs = physiotherapyScheduleDAO.getPhysiotherapyScheduleDTOListByPhysiotherapyPlanID(physiotherapy_planDTO.iD);
	
	for(PhysiotherapyScheduleDTO physiotherapyScheduleDTO: physiotherapyScheduleDTOs)
	{
		if(physiotherapyScheduleDTO.therapyDate == TimeConverter.getToday() && physiotherapyScheduleDTO.isDone == false)
		{
			%>
			<%
			value = physiotherapyScheduleDTO.therapyTime + "";
			%>
						
			<%=Utils.getDigits(value, Language)%>:
			
			
			<%
				value = CatDAO.getName(Language, "therapy_treatment", physiotherapyScheduleDTO.therapyTreatmentCat);
			%>	
						
			<%=Utils.getDigits(value, Language)%><br>
			<%
		}
		
	}
%>
   


</td>

<%
  if(userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE)
  {
  %>
<td>
<%
	if(notDoneCount > 0)
	{
%>
    <button type="button" type="button" class="btn btn-sm border-0 shadow"
            style="background-color: #22ccc1; color: white; border-radius: 8px"
            onclick="location.href='Physiotherapy_planServlet?actionType=view&ID=<%=physiotherapy_planDTO.iD%>'">
        <i class="fa fa-plus"></i>
        <%=LM.getText(LC.HM_ADD_THERAPY, loginDTO)%>
    </button>
    <%
	}
    %>
</td>
<%
  }
%>

<td>

    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Physiotherapy_planServlet?actionType=view&justView=1&ID=<%=physiotherapy_planDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>

</td>

<td id='<%=i%>_Edit'>
    <button class="btn-sm border-0 shadow btn-border-radius text-white" type="button"
            style="background-color: #ff6b6b"
            onclick="location.href='Physiotherapy_planServlet?actionType=getEditPage&ID=<%=physiotherapy_planDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
<%
if(doneCount == 0)
{
%>
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=physiotherapy_planDTO.iD%>'/>
        </span>
    </div>
<%
}
%>
</td>
																						
											

