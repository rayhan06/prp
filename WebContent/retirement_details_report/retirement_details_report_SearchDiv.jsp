<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="geolocation.GeoLocationRepository" %>
<%@page pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.RETIREMENT_DETAILS_REPORT_EDIT_LANGUAGE, loginDTO);
    String context = "../../.." + request.getContextPath() + "/assets/";
    int year = Calendar.getInstance().get(Calendar.YEAR);
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>
<script src="<%=context%>scripts/util1.js"></script>
<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>
<div class="row mx-2">
    <div id="criteriaSelectionId" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <button type="button"
                        class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                        data-toggle="modal" data-target="#select_criteria_div" id="select_criteria_btn"
                        onclick="beforeOpenCriteriaModal()">
                    <%=isLangEng ? "Criteria Select" : "ক্রাইটেরিয়া বাছাই"%>
                </button>
            </div>
        </div>
    </div>
    <div id="criteriaPaddingId" class="search-criteria-div col-md-6">
    </div>
    <div id="officeUnitId_div" class="search-criteria-div col-md-6 officeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="office_units_id_modal_button"
                        onclick="officeModalButtonClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                </button>
                <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 officeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস"%>
            </label>
            <div class="col-1" id='onlySelectedOffice'>
                <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                       id='onlySelectedOffice_checkbox'
                       onchange="this.value = this.checked;" value='false'>
            </div>
            <div class="col-8"></div>
        </div>
    </div>
    <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
        <div class="form-group row">
            <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                <div class="tag template-tag">
                    <span class="tag-name"></span>
                    <i class="fas fa-times-circle tag-remove-btn"></i>
                </div>
            </div>
        </div>
    </div>
    <div id="officeUnitOrganogramId_div" class="search-criteria-div col-md-6 employeeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="organogram_id_modal_button"
                        onclick="organogramIdModalBtnClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                </button>
                <div class="input-group" id="organogram_id_div" style="display: none">
                    <input type="hidden" name='officeUnitOrganogramId' id='organogram_id_input' value="">
                    <button type="button" class="btn btn-secondary form-control" disabled
                            id="organogram_id_text"></button>
                    <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger modalCross"
                                    onclick="crsBtnClicked('organogram_id');"
                                    id='organogram_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                </div>
            </div>
        </div>
    </div>
    <div id="employmentCat_div" class="search-criteria-div col-md-6 employmentCatClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.WORKPLACE_AGE_REPORT_WHERE_EMPLOYMENTCAT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='employmentCat' id='employmentCat'>
                    <%=CatRepository.getInstance().buildOptions("employment", Language, null)%>
                </select>
            </div>
        </div>
    </div>
    <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
            </label>
            <div class="col-md-9">
                <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng" style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in English" : "কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>

    <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
            </label>
            <div class="col-md-9">
                <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng" style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in Bangla" : "কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>
    <div id="age_div" class="search-criteria-div col-md-6 ageClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>
            </label>
            <div class="col-4">
                <input class='form-control' type="text" name='age_start' id='age_start'
                       placeholder='<%=LM.getText(LC.HR_REPORT_FROM, loginDTO)%>' style="width: 100%">
            </div>
            <div class="col-1"></div>
            <div class="col-4">
                <input class='form-control' type="text" name='age_end' id='age_end'
                       placeholder='<%=LM.getText(LC.HR_REPORT_TO, loginDTO)%>' style="width: 100%">
            </div>
        </div>
    </div>
    <div id="permanent_district_div" class="search-criteria-div col-md-6 districtClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='home_district' id='home_district' style="width: 100%">
                    <%=GeoLocationRepository.getInstance().buildOptionByLevel(Language, 2, null)%>
                </select>
            </div>
        </div>
    </div>
    <div id="startDate_div" class="search-criteria-div col-md-6 retirementDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.RETIREMENT_DETAILS_REPORT_WHERE_RETIREMENTDATE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID"
                               value="start-date-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 50%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control'
                       id='startDate'
                       name='startDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="endDate_div" class="search-criteria-div col-md-6 retirementDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.RETIREMENT_DETAILS_REPORT_WHERE_RETIREMENTDATE_4, loginDTO)%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID"
                               value="end-date-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 60%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control'
                       id='endDate'
                       name='endDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="lprDateStart_div" class="search-criteria-div col-md-6 lprDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_LPRDATE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="lpr-date-start-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 50%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='lprDateStart'
                       name='lprDateStart' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="lprDateEnd_div" class="search-criteria-div col-md-6 lprDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_LPRDATE_12, loginDTO)%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="lpr-date-end-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 60%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='lprDateEnd'
                       name='lprDateEnd' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 jobGradeClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="jobGradeTypeCat">
                <%=LM.getText(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT, loginDTO)%>
            </label>
            <div class="col-md-5">
                <select class='form-control' name='jobGradeTypeCat' id='jobGradeTypeCat'>
                    <%=CatRepository.getOptions(Language, "job_grade_type", CatDTO.CATDEFAULT)%>
                </select>
            </div>
            <div class="col-md-4 form-group row">
                <div class="col-md-4">
                    <button
                            type="button" id="lessButton"
                            class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                            style="color: white;background-color: grey"
                            onclick="lessButtonClicked()"
                    >
                        <
                    </button>
                </div>
                <div class="col-md-4">
                    <button
                            type="button" id="equalButton"
                            class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                            style="color: white;background-color: lightgreen"
                            onclick="equalButtonClicked()"
                    >
                        =
                    </button>
                </div>
                <div class="col-md-4">
                    <button
                            type="button" id="moreButton"
                            class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                            style="color: white;background-color: grey"
                            onclick="moreButtonClicked()"
                    >
                        >
                    </button>
                </div>
            </div>
            <input type="hidden" id="jobGradeLess" name="jobGradeLess" value="0">
            <input type="hidden" id="jobGradeEqual" name="jobGradeEqual" value="1">
            <input type="hidden" id="jobGradeMore" name="jobGradeMore" value="0">
        </div>
    </div>
</div>

<%@include file="../common/hr_report_modal_util.jsp" %>
<script src="<%=context%>scripts/input_validation.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });

    function init() {
        dateTimeInit($("#Language").val());
        select2SingleSelector('#employmentCat', '<%=Language%>');
        select2SingleSelector('#home_district', '<%=Language%>');
        select2SingleSelector('#jobGradeTypeCat', '<%=Language%>');
        document.getElementById('age_start').onkeydown = keyDownEvent;
        document.getElementById('age_end').onkeydown = keyDownEvent;
        criteriaArray = [{
            class: 'officeModalClass',
            title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>',
            show: false,
            specialCategory: 'officeModal'
        },
            {
                class: 'employeeModalClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>',
                show: false,
                specialCategory: 'employeeModal'
            },
            {
                class: 'employmentCatClass',
                title: '<%=LM.getText(LC.WORKPLACE_AGE_REPORT_WHERE_EMPLOYMENTCAT, loginDTO)%>',
                show: false
            },
            {
                class: 'employeeNameClass',
                title: '<%=isLangEng ? "Employee Name" : "কর্মকর্তা/কর্মচারীর নাম"%>',
                show: false
            },
            {class: 'ageClass', title: '<%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>', show: false},
            {
                class: 'districtClass',
                title: '<%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>',
                show: false
            },
            {
                class: 'retirementDateClass',
                title: '<%=isLangEng ? "Retirement Date" : "অবসরের তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['start-date-js', 'end-date-js']
            },
            {
                class: 'lprDateClass',
                title: '<%=isLangEng ? "LPR Date" : "এলপিআর তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['lpr-date-start-js', 'lpr-date-end-js']
            },
            {
                class: 'jobGradeClass',
                title: '<%=LM.getText(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT, loginDTO)%>',
                show: false
            }]
        
        
        var startDate = getParamValue("startDate");
    	console.log("startDate from param = " + startDate);
    	if(startDate != null)
    	{
    		$("#lprDateStart_div").show();
    		//$("#c12").prop( "checked", true );
    		setDateByTimestampAndId('lpr-date-start-js', startDate);
    		processNewCalendarDateAndSubmit();
    	}
    	
    	var endDate = getParamValue("endDate");
    	console.log("endDate from param = " + endDate);
    	if(endDate != null)
    	{
    		$("#lprDateEnd_div").show();
    		//$("#c12").prop( "checked", true );
    		setDateByTimestampAndId('lpr-date-end-js', endDate);
    		processNewCalendarDateAndSubmit();
    	}
    }

    function PreprocessBeforeSubmiting() {
        $('#startDate').val(getDateTimestampById('start-date-js'));
        $('#endDate').val(getDateTimestampById('end-date-js'));
        $('#lprDateStart').val(getDateTimestampById('lpr-date-start-js'));
        $('#lprDateEnd').val(getDateTimestampById('lpr-date-end-js'));
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 200);
        return true == isvalid;
    }

    function lessButtonClicked() {
        if (document.getElementById("jobGradeLess").value === "0")
            document.getElementById("lessButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("lessButton").style.backgroundColor = "grey";
        switchValue("jobGradeLess");
        if (document.getElementById("jobGradeMore").value == "1") {
            document.getElementById("jobGradeMore").value = "0";
            document.getElementById("moreButton").style.backgroundColor = "grey";
        }
    }

    function moreButtonClicked() {
        if (document.getElementById("jobGradeMore").value === "0")
            document.getElementById("moreButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("moreButton").style.backgroundColor = "grey";
        switchValue("jobGradeMore");
        if (document.getElementById("jobGradeLess").value == "1") {
            document.getElementById("jobGradeLess").value = "0";
            document.getElementById("lessButton").style.backgroundColor = "grey";
        }
    }

    function equalButtonClicked() {
        if (document.getElementById("jobGradeEqual").value === "0")
            document.getElementById("equalButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("equalButton").style.backgroundColor = "grey";
        switchValue("jobGradeEqual");
    }

    function switchValue(hiddenId) {
        if (document.getElementById(hiddenId).value === "0")
            document.getElementById(hiddenId).value = "1";
        else
            document.getElementById(hiddenId).value = "0";
    }
</script>