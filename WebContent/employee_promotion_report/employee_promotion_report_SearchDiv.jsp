<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="geolocation.GeoLocationRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_EDIT_LANGUAGE, loginDTO);
    int year = Calendar.getInstance().get(Calendar.YEAR);
    String context = request.getContextPath() + "/";
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>

<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row px-4">
    <div id="criteriaSelectionId" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <button type="button"
                        class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                        data-toggle="modal" data-target="#select_criteria_div" id="select_criteria_btn"
                        onclick="beforeOpenCriteriaModal()">
                    <%=isLangEng ? "Criteria Select" : "ক্রাইটেরিয়া বাছাই"%>
                </button>
            </div>
        </div>
    </div>
    <div id="criteriaPaddingId" class="search-criteria-div col-md-6">
    </div>
    <div id="officeUnitId" class="search-criteria-div col-md-6 officeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
            </label>
            <div class="col-md-8">
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="office_units_id_modal_button"
                        onclick="officeModalButtonClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                </button>
                <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 officeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস"%>
            </label>
            <div class="col-1" id='onlySelectedOffice'>
                <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                       id='onlySelectedOffice_checkbox'
                       onchange="this.value = this.checked;" value='false'>
            </div>
            <div class="col-7"></div>
        </div>
    </div>
    <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
        <div class="form-group row">
            <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                <div class="tag template-tag">
                    <span class="tag-name"></span>
                    <i class="fas fa-times-circle tag-remove-btn"></i>
                </div>
            </div>
        </div>
    </div>
    <div id="officeUnitOrganogramId" class="search-criteria-div col-md-6 employeeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>
            </label>
            <div class="col-md-8">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="organogram_id_modal_button"
                        onclick="organogramIdModalBtnClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                </button>
                <div class="input-group" id="organogram_id_div" style="display: none">
                    <input type="hidden" name='officeUnitOrganogramId' id='organogram_id_input' value="">
                    <button type="button" class="btn btn-secondary form-control" disabled
                            id="organogram_id_text"></button>
                    <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                    <button type="button" class="btn btn-outline-danger modalCross"
                            onclick="crsBtnClicked('organogram_id');"
                            id='organogram_id_crs_btn' tag='pb_html'>
                        x
                    </button>
                </span>
                </div>

            </div>
        </div>
    </div>
    <div id="promotionNatureCat_div" class="search-criteria-div col-md-6 promotionNatureClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right" for="promotionNatureCat">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_PROMOTIONNATURECAT, loginDTO)%>
            </label>
            <div class="col-md-8">
                <select class='form-control' name='promotionNatureCat' id='promotionNatureCat'
                        style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("promotion_nature", Language, CatDTO.CATDEFAULT)%>
                </select>
            </div>
        </div>
    </div>
    <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right" for="nameEng">
                <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
            </label>
            <div class="col-md-8">
                <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng" style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in English" : "কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>

    <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right" for="nameBng">
                <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
            </label>
            <div class="col-md-8">
                <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng" style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in Bangla" : "কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>
    <div id="dobStartDate_div" class="search-criteria-div col-md-6 dobClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_DATEOFBIRTH, loginDTO)%>
            </label>
            <div class="col-md-8">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="dobStartDate-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='dobStartDate'
                       name='dobStartDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="dobEndDate_div" class="search-criteria-div col-md-6 dobClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_DATEOFBIRTH_10, loginDTO)%>
            </label>
            <div class="col-md-8">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="dobEndDate-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 50%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='dobEndDate'
                       name='dobEndDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="startDate_div" class="search-criteria-div col-md-6 joiningDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_JOININGDATE, loginDTO)%>
            </label>
            <div class="col-md-8">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="startDate-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='startDate'
                       name='startDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="endDate_div" class="search-criteria-div col-md-6 joiningDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_JOININGDATE_3, loginDTO)%>
            </label>
            <div class="col-md-8">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="endDate-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 50%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='endDate'
                       name='endDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="lprDateStart_div" class="search-criteria-div col-md-6 lprDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_LPRDATE, loginDTO)%>
            </label>
            <div class="col-md-8">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="lprDateStart-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='lprDateStart'
                       name='lprDateStart' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="lprDateEnd_div" class="search-criteria-div col-md-6 lprDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_LPRDATE_6, loginDTO)%>
            </label>
            <div class="col-md-8">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="lprDateEnd-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 50%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='lprDateEnd'
                       name='lprDateEnd' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="joinStartDate_div" class="search-criteria-div col-md-6 promotionJoiningDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_JOININGDATE_13, loginDTO)%>
            </label>
            <div class="col-md-8">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="joinStartDate-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='joinStartDate'
                       name='joinStartDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="joinEndDate_div" class="search-criteria-div col-md-6 promotionJoiningDateClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_JOININGDATE_14, loginDTO)%>
            </label>
            <div class="col-md-8">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="joinEndDate-js"></jsp:param>
                    <jsp:param name="LANGUAGE"
                               value="<%=Language%>"></jsp:param>
                    <jsp:param name="END_YEAR"
                               value="<%=year + 100%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control' id='joinEndDate'
                       name='joinEndDate' value=''
                       tag='pb_html'/>
            </div>
        </div>
    </div>
    <div id="permanent_district_div" class="search-criteria-div col-md-6 districtClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>
            </label>
            <div class="col-md-8">
                <select class='form-control' name='home_district' id='home_district' style="width: 100%">
                    <%=GeoLocationRepository.getInstance().buildOptionByLevel(Language, 2, null)%>
                </select>
            </div>
        </div>
    </div>
    <div id="age_div" class="search-criteria-div col-md-6 ageClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right">
                <%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>
            </label>
            <div class="col-md-4">
                <input class='form-control' type="text" name='age_start' id='age_start'
                       placeholder='<%=LM.getText(LC.HR_REPORT_FROM, loginDTO)%>' style="width: 100%">
            </div>

            <div class="col-md-4">
                <input class='form-control' type="text" name='age_end' id='age_end'
                       placeholder='<%=LM.getText(LC.HR_REPORT_TO, loginDTO)%>' style="width: 100%">
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6 jobGradeClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right" for="jobGradeTypeCat">
                <%=LM.getText(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT, loginDTO)%>
            </label>
            <div class="col-md-5">
                <select class='form-control' name='jobGradeTypeCat' id='jobGradeTypeCat'>
                    <%=CatRepository.getOptions(Language, "job_grade_type", CatDTO.CATDEFAULT)%>
                </select>
            </div>
            <div class="col-md-3 form-group row">
                <div class="col-md-4">
                    <button
                            type="button" id="lessButton"
                            class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                            style="color: white;background-color: grey"
                            onclick="lessButtonClicked()"
                    >
                        <
                    </button>
                </div>
                <div class="col-md-4">
                    <button
                            type="button" id="equalButton"
                            class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                            style="color: white;background-color: lightgreen"
                            onclick="equalButtonClicked()"
                    >
                        =
                    </button>
                </div>
                <div class="col-md-4">
                    <button
                            type="button" id="moreButton"
                            class="btn shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                            style="color: white;background-color: grey"
                            onclick="moreButtonClicked()"
                    >
                        >
                    </button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="jobGradeLess" name="jobGradeLess" value="0">
    <input type="hidden" id="jobGradeEqual" name="jobGradeEqual" value="1">
    <input type="hidden" id="jobGradeMore" name="jobGradeMore" value="0">
</div>

<%@include file="../common/hr_report_modal_util.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script src="<%=context%>assets/scripts/input_validation.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });

    function init() {
        dateTimeInit($("#Language").val());
        select2SingleSelector('#educationLevelType', '<%=Language%>');
        select2SingleSelector('#promotionNatureCat', '<%=Language%>');
        select2SingleSelector('#home_district', '<%=Language%>');
        select2SingleSelector('#jobGradeTypeCat', '<%=Language%>');
        document.getElementById('age_start').onkeydown = keyDownEvent;
        document.getElementById('age_end').onkeydown = keyDownEvent;
        criteriaArray = [{
            class: 'officeModalClass',
            title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>',
            show: false,
            specialCategory: 'officeModal'
        },
            {
                class: 'employeeModalClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>',
                show: false,
                specialCategory: 'employeeModal'
            },
            {
                class: 'promotionNatureClass',
                title: '<%=LM.getText(LC.EMPLOYEE_PROMOTION_REPORT_WHERE_PROMOTIONNATURECAT, loginDTO)%>',
                show: false
            },
            {
                class: 'employeeNameClass',
                title: '<%=isLangEng ? "Employee Name" : "কর্মকর্তা/কর্মচারীর নাম"%>',
                show: false
            },
            {
                class: 'dobClass',
                title: '<%=isLangEng ? "Date of Birth" : "জন্ম তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['dobStartDate-js', 'dobEndDate-js']
            },
            {
                class: 'joiningDateClass',
                title: '<%=isLangEng ? "Joining Date" : "বর্তমান অফিসে যোগদান তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['startDate-js', 'endDate-js']
            },
            {
                class: 'lprDateClass',
                title: '<%=isLangEng ? "LPR Date" : "এলপিআর তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['lprDateStart-js', 'lprDateEnd-js']
            },
            {
                class: 'promotionJoiningDateClass',
                title: '<%=isLangEng ?"Promotion Office Joining Date" : "পদোন্নতি অফিসে যোগদান তারিখ"%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['joinStartDate-js', 'joinEndDate-js']
            },
            {class: 'ageClass', title: '<%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>', show: false},
            {
                class: 'districtClass',
                title: '<%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>',
                show: false
            },
            {
                class: 'jobGradeClass',
                title: '<%=LM.getText(LC.DESIGNATION_STATUS_REPORT_WHERE_JOBGRADETYPECAT, loginDTO)%>',
                show: false
            }]
    }

    function PreprocessBeforeSubmiting() {
        $('#dobStartDate').val(getDateTimestampById('dobStartDate-js'));
        $('#dobEndDate').val(getDateTimestampById('dobEndDate-js'));
        $('#startDate').val(getDateTimestampById('startDate-js'));
        $('#endDate').val(getDateTimestampById('endDate-js'));
        $('#lprDateStart').val(getDateTimestampById('lprDateStart-js'));
        $('#lprDateEnd').val(getDateTimestampById('lprDateEnd-js'));
        $('#joinStartDate').val(getDateTimestampById('joinStartDate-js'));
        $('#joinEndDate').val(getDateTimestampById('joinEndDate-js'));
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 200);
        return true == isvalid;
    }

    function lessButtonClicked() {
        if (document.getElementById("jobGradeLess").value === "0")
            document.getElementById("lessButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("lessButton").style.backgroundColor = "grey";
        switchValue("jobGradeLess");
        if (document.getElementById("jobGradeMore").value == "1") {
            document.getElementById("jobGradeMore").value = "0";
            document.getElementById("moreButton").style.backgroundColor = "grey";
        }
    }

    function moreButtonClicked() {
        if (document.getElementById("jobGradeMore").value === "0")
            document.getElementById("moreButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("moreButton").style.backgroundColor = "grey";
        switchValue("jobGradeMore");
        if (document.getElementById("jobGradeLess").value == "1") {
            document.getElementById("jobGradeLess").value = "0";
            document.getElementById("lessButton").style.backgroundColor = "grey";
        }
    }

    function equalButtonClicked() {
        if (document.getElementById("jobGradeEqual").value === "0")
            document.getElementById("equalButton").style.backgroundColor = "lightgreen";
        else
            document.getElementById("equalButton").style.backgroundColor = "grey";
        switchValue("jobGradeEqual");
    }

    function switchValue(hiddenId) {
        if (document.getElementById(hiddenId).value === "0")
            document.getElementById(hiddenId).value = "1";
        else
            document.getElementById(hiddenId).value = "0";
    }
</script>