<%@page import="sessionmanager.SessionConstants" %>
<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <%
        if (commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {
    %>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
    <%
        }
    %>
</td>
