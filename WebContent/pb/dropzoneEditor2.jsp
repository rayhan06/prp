			<table>
				<tr>
			<%
			if(fileList != null)
			{
				for(int j = 0; j < fileList.size(); j ++)
				{
					FilesDTO filesDTO = fileList.get(j);
					byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
					%>
					<td id = '<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
					<%
					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
					{
						%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64) 
						%>' style='width:100px' />
						<%
					}
					%>
					<a href = '<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					</td>
					<%
				}
			}
			%>
			</tr>
			</table>