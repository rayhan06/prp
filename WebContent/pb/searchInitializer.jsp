<%@page import="workflow.*"%>
<%@page import="login.LoginDTO" %>
<%@page import="user.UserDTO" %>
<%@page import="util.*" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@ page import="common.BaseServlet" %>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String value = "";
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    if (rn2 == null) {
        rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    }
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2 == null || rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;
    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    String ViewAll;
    if (request.getParameter("ViewAll") != null) {
        ViewAll = request.getParameter("ViewAll");
    } else {
        ViewAll = "0";
    }
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>