<style>
    .file-preview-link-text {
        display: block;
        width: 100px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    .file-preview-image {
        width:60px;
        height: 60px;
        object-fit: contain;
    }
</style>

<div class="row m-2">
    <%
        if (fileList != null) {
            for (int j = 0; j < fileList.size(); j++) {
                FilesDTO filesDTO = fileList.get(j);
                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
    %>

    <div class="col" id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
        <div class="col">
            <%if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {%>
            <img class="file-preview-image" src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'/>
            <%}%>

            <a class='btn btn-sm cancel-btn text-light shadow btn-border-radius p-2'
               onclick='deletefile(<%=filesDTO.iD%>, "<%=fileColumnName%>_td_<%=filesDTO.iD%>", "<%=fileColumnName%>FilesToDelete_<%=i%>")'>
                <i class="fa fa-trash"></i>
            </a>

            <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
               class="file-preview-link-text"
               title="<%=filesDTO.fileTitle%>"
               download
            >
                <%=filesDTO.fileTitle%>
            </a>
        </div>
    </div>
    <%
            }
        }
    %>
</div>