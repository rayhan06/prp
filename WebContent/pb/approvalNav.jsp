<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="approval_path.*" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = request.getParameter("action");
    System.out.println("URLaction = " + action);
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = request.getParameter("Language");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);

    Approval_pathDAO approval_pathDAO2 = new Approval_pathDAO();
    List<Approval_pathDTO> approval_pathDTOs = approval_pathDAO2.getallByOfficeAndModule(-1, "");
%>


<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0 !important;">
    <div class="kt-portlet shadow-none">
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label">
                            <%=LM.getText(LC.HM_APPROVAL_PATH, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <select class='form-control' name='job_cat' id='job_cat'>
                                <option value="-1"><%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                </option>
                                <%
                                    for (Approval_pathDTO approval_pathDTO : approval_pathDTOs) {
                                %>
                                <option value="<%=approval_pathDTO.jobCat%>">
                                    <%=Language.equalsIgnoreCase("english") ? approval_pathDTO.nameEn : approval_pathDTO.nameBn%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label">
                            <%=LM.getText(LC.HM_STATUS, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <select class='form-control' name='approval_status_cat'
                                    id='approval_status_cat'>
                                <%=CatRepository.getInstance().buildOptions("approval_status",Language,null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label">
                            <%=LM.getText(LC.HM_INITIATED_BY, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <select class='form-control' name='initiator' id='initiator'>
                                <%=EmployeeOfficeRepository.getInstance().buildOptions(Language)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label">
                            <%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <select class='form-control' name='assigned_to' id='assigned_to'>
                                <%=EmployeeOfficeRepository.getInstance().buildOptionsForOrganogram(Language)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label">
                            <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.HM_DATE_OF_INITIATION, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <input type="text" class="form-control formRequired datepicker"
                                   name='starting_date' id='starting_date'
                                   style="width: 100%"
                            />
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label">
                            <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.HM_DATE_OF_INITIATION, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <input type="text" class="form-control formRequired datepicker"
                                   name='ending_date' id='ending_date'
                            style="width: 100%"
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-actions text-right">
                        <input type="hidden" name="search" value="yes"/>
                        <button type="submit" class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                                onclick="allfield_changed('',0)"
                                style="background-color: #00a1d4;">
                            <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- search control -->
<div class="portlet box portlet-btcl">
    <div
            class="portlet-body form expand"
    >
        <!-- BEGIN FORM-->
        <div class="container-fluid">
            <div class="row col-lg-offset-1">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
                    <div class="col-xs-2 col-sm-4 col-md-4">
                        <label 
                               class="control-label pull-right">

                        </label>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8">

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
                    <div class="col-xs-2 col-sm-4 col-md-4">
                        <label 
                               class="control-label pull-right">

                        </label>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8">

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
                    <div class="col-xs-2 col-sm-4 col-md-4">
                        <label 
                               class="control-label pull-right">

                        </label>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8">

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
                    <div class="col-xs-2 col-sm-4 col-md-4">
                        <label 
                               class="control-label pull-right">
                        </label>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8">

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
                    <div class="col-xs-2 col-sm-4 col-md-4">
                        <label 
                               class="control-label pull-right">

                        </label>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8">

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">
                    <div class="col-xs-2 col-sm-4 col-md-4">
                        <label 
                               class="control-label pull-right">
                        </label>
                    </div>
                    <div class="col-xs-10 col-sm-8 col-md-8">

                    </div>
                </div>

            </div>


            <div class=clearfix></div>

            <div class="form-actions fluid" style="margin-top:10px">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-offset-3 col-xs-12 col-md-12  col-md-12 col-lg-9">
                            <div class="col-xs-4  col-sm-4  col-md-6">

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END FORM-->
    </div>
</div>


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading"
             src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = "";
        if (document.getElementById("job_cat").value >= 0) {
            params += '&job_cat=' + document.getElementById("job_cat").value;
        }
        if (document.getElementById("approval_status_cat").value != '' && document.getElementById("approval_status_cat").value >= 0) {
            params += '&approval_status_cat=' + document.getElementById("approval_status_cat").value;
        }
        if (document.getElementById("initiator").value != '' && document.getElementById("initiator").value >= 0) {
            params += '&initiator=' + document.getElementById("initiator").value;
        }
        if (document.getElementById("assigned_to").value != '' && document.getElementById("assigned_to").value >= 0) {
            params += '&assigned_to=' + document.getElementById("assigned_to").value;
        }

        var formattedStartingDate = getBDFormattedDate('starting_date');
        if(formattedStartingDate != '')
        {
        	params += '&starting_date=' + formattedStartingDate;
        }
        
        var formattedEndingDate = getBDFormattedDate('ending_date');
        if(formattedEndingDate != '')
        {
        	params += '&ending_date=' + formattedEndingDate;
        }


        params += '&search=true&ajax=true&ViewAll=' + document.getElementById("ViewAll").value;

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        } else {
            console.log("go not found")
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

