<%@page import="pb.*" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.UserDTO" %>
<%@page import="files.FilesDAO" %>
<%@page import="util.HttpRequestUtils" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date"%>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String actionName;
    boolean isEditActionType;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "ajax_add";
        isEditActionType = false;
    } else {
        actionName = "ajax_edit";
        isEditActionType = true;
    }

    String fileColumnName = "";

    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    if (request.getParameter("isPermanentTable") != null) {
        isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
    }


    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String Options = "";
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String contextOfPath = request.getContextPath() + "/";
%>