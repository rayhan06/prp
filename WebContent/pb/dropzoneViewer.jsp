			<table class="table table-bordered table-striped">
					<%
					if (FilesDTOList != null && FilesDTOList.size() > 0) 
	                {
					%>
				    <thead>
				    	<tr>
				    		<th>File Name</th>
				    		
				    		<th>Download</th>

				    	</tr>
				    </thead>
				    <%
	                }
				    %>
				    <tbody>
			        
			            <%
			                if (FilesDTOList != null) 
			                {
			                    for (int j = 0; j < FilesDTOList.size(); j++) 
			                    {
			                        FilesDTO filesDTO = FilesDTOList.get(j);
			                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
			            %>
			            <tr>
			            <td>
			            <%
                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) 
                                {
                            %>
                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/><br>
                            <%
                                }
                            %>
			            <%=filesDTO.fileTitle%>
			            </td>
			            
			             <td>
			                <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
			                   download><i class="fa fa-download" aria-hidden="true"></i>
			                </a>
			            </td>

			            </tr>
			            <%
			                    }
			                }
			            %>
			        </tbody>
			    </table>