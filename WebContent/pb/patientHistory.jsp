<style>
    .customdropdown {
        min-width: 155px;
    }

    a#history {
        min-width: 155px !important;
        margin-top: 1px !important;
    }


    .dropdown-menu.show {
        background-color: #dddddd;
        width: 156px !important;
        left: 0px !important;

    }

    button#dropdownMenuButton {
        padding-right: 90px !important;
    }


    .open > .dropdown-menu, .show > .dropdown-menu {
        display: none;
    }

</style>

<div class="form-group">
    <div class="">
        <div class="dropdown">
            <button onclick="viewHistory()" class="btn btn-info shadow btn-border-radius dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <%=LM.getText(LC.HM_HISTORY, loginDTO)%>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a id="history" type="button"
                   href="Prescription_detailsServlet?actionType=getFormattedSearchPage&userName=<%=appointmentDTO.employeeUserName%>&whoIsThePatientCat=<%=appointmentDTO.whoIsThePatientCat%>&phoneNumber=<%=appointmentDTO.phoneNumber%>&patientName=<%=appointmentDTO.patientName%>"
                   class="btn btn-primary dropdown-item"><%=LM.getText(LC.HM_PRESCRIPTION_HISTORY, loginDTO)%>
                </a>

                <a id="history" type="button"
                   href="Common_lab_reportServlet?actionType=getFormattedSearchPage&userName=<%=appointmentDTO.employeeUserName%>&whoIsThePatientCat=<%=appointmentDTO.whoIsThePatientCat%>&phoneNumber=<%=appointmentDTO.phoneNumber%>&patientName=<%=appointmentDTO.patientName%>"
                   class="btn btn-primary dropdown-item"><%=LM.getText(LC.HM_LAB_TEST_HISTORY, loginDTO)%>
                </a>

                <a id="history" type="button"
                   href="Physiotherapy_planServlet?actionType=getFormattedSearchPage&userName=<%=appointmentDTO.employeeUserName%>&whoIsThePatientCat=<%=appointmentDTO.whoIsThePatientCat%>&phoneNumber=<%=appointmentDTO.phoneNumber%>&patientName=<%=appointmentDTO.patientName%>"
                   class="btn btn-primary dropdown-item"><%=LM.getText(LC.HM_PHYSIOTHERAPY_HISTORY, loginDTO)%>
                </a>
            </div>
        </div>
    </div>
</div>