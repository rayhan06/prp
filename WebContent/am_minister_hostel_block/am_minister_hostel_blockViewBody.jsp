

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_minister_hostel_block.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
    String servletName = "Am_minister_hostel_blockServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Am_minister_hostel_blockDTO am_minister_hostel_blockDTO = Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockDTOByID(id);
    CommonDTO commonDTO = am_minister_hostel_blockDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_BLOCK_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_BLOCK_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_BLOCKNO, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_minister_hostel_blockDTO.blockNo + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_SIDE, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
<%--								<th><%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_SIDE_AMMINISTERHOSTELBLOCKTYPE, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_SIDE_MINISTERHOSTELSIDECAT, loginDTO)%></th>
							</tr>
							<%
                         	List<AmMinisterHostelSideDTO> amMinisterHostelSideDTOs = AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByBlockID(am_minister_hostel_blockDTO.iD);
                         	
                         	for(AmMinisterHostelSideDTO amMinisterHostelSideDTO: amMinisterHostelSideDTOs)
                         	{
                         		%>
                         			<tr>
<%--										<td>--%>
<%--											<%--%>
<%--											value = amMinisterHostelSideDTO.amMinisterHostelBlockId + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CommonDAO.getName(Integer.parseInt(value), "am_minister_hostel_block", Language.equals("English")?"name_en":"name_bn", "id");--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--										</td>--%>
										<td>
											<%
											value = amMinisterHostelSideDTO.ministerHostelSideCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "am_minister_hostel_side", amMinisterHostelSideDTO.ministerHostelSideCat);
											%>	
				
											<%=value%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>