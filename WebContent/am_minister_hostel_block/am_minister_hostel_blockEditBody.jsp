<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="am_minister_hostel_block.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Am_minister_hostel_blockDTO am_minister_hostel_blockDTO = new Am_minister_hostel_blockDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        am_minister_hostel_blockDTO = Am_minister_hostel_blockDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = am_minister_hostel_blockDTO;
    String tableName = "am_minister_hostel_block";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_BLOCK_ADD_FORMNAME, loginDTO);
    String servletName = "Am_minister_hostel_blockServlet";
    String otherText = Language.equalsIgnoreCase("english") ? "Others" : "অন্যান্য";

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Am_minister_hostel_blockServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=am_minister_hostel_blockDTO.iD%>' tag='pb_html'/>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_BLOCKNO, loginDTO)%><span>*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='blockNo'
                                                       id='blockNo_text_<%=i%>'
                                                       value='<%=am_minister_hostel_blockDTO.blockNo%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <input type='hidden' class='form-control' name='searchColumn'
                                               id='searchColumn_hidden_<%=i%>'
                                               value='<%=am_minister_hostel_blockDTO.searchColumn%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='insertedByUserId'
                                               id='insertedByUserId_hidden_<%=i%>'
                                               value='<%=am_minister_hostel_blockDTO.insertedByUserId%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                               id='insertedByOrganogramId_hidden_<%=i%>'
                                               value='<%=am_minister_hostel_blockDTO.insertedByOrganogramId%>'
                                               tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='insertionDate'
                                               id='insertionDate_hidden_<%=i%>'
                                               value='<%=am_minister_hostel_blockDTO.insertionDate%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='lastModifierUser'
                                               id='lastModifierUser_hidden_<%=i%>'
                                               value='<%=am_minister_hostel_blockDTO.lastModifierUser%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='isDeleted'
                                               id='isDeleted_hidden_<%=i%>'
                                               value='<%=am_minister_hostel_blockDTO.isDeleted%>' tag='pb_html'/>

                                        <input type='hidden' class='form-control' name='lastModificationTime'
                                               id='lastModificationTime_hidden_<%=i%>'
                                               value='<%=am_minister_hostel_blockDTO.lastModificationTime%>'
                                               tag='pb_html'/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_SIDE, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_SIDE_MINISTERHOSTELSIDECAT, loginDTO)%>
                                    </th>

                                    <th><%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_SIDE_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-AmMinisterHostelSide">


                                <%
                                    if (actionName.equals("ajax_edit")) {
                                        int index = -1;


                                        for (AmMinisterHostelSideDTO amMinisterHostelSideDTO : am_minister_hostel_blockDTO.amMinisterHostelSideDTOList) {
                                            index++;

                                %>

                                <tr id="AmMinisterHostelSide_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='amMinisterHostelSide.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=amMinisterHostelSideDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td>


                                        <label class="error-msg" style="color: red;display: none"></label>

                                        <select class='form-control' name='amMinisterHostelSide.ministerHostelSideCat'
                                                id='ministerHostelSideCat_category_<%=childTableStartingID%>'
                                                tag='pb_html'
                                                onchange="isOtherSelected(this)">
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("am_minister_hostel_side",
                                                        Language, amMinisterHostelSideDTO.ministerHostelSideCat);
                                                Options += "<option value='-2'>" + otherText + "</option>";
                                            %>
                                            <%=Options%>
                                        </select>

                                        <div class="hiddenOtherField otherElement">
                                            <input type="text" name="amMinisterHostelSide.otherMinisterHostelSideEn" placeholder="<%=UtilCharacter.getDataByLanguage(Language,
                                            "অন্যান্য মন্ত্রী হোস্টেল পাশ ইংরেজি","Other Minister Hostel Side English")%>" class="form-control mt-2">
                                            <input type="text" name="amMinisterHostelSide.otherMinisterHostelSideBn" placeholder="<%=UtilCharacter.getDataByLanguage(Language,
                                            "অন্যান্য মন্ত্রী হোস্টেল পাশ বাংলা","Other Minister Hostel Side Bangla")%>" class="form-control mt-2">
                                        </div>

                                    </td>
                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-9 text-right">
                                <button
                                        id="add-more-AmMinisterHostelSide"
                                        name="add-moreAmMinisterHostelSide"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-AmMinisterHostelSide"
                                        name="removeAmMinisterHostelSide"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%AmMinisterHostelSideDTO amMinisterHostelSideDTO = new AmMinisterHostelSideDTO();%>

                        <template id="template-AmMinisterHostelSide">
                            <tr>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='amMinisterHostelSide.iD'
                                           id='iD_hidden_' value='<%=amMinisterHostelSideDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td>


                                    <label class="error-msg" style="color: red;display: none"></label>

                                    <select class='form-control' name='amMinisterHostelSide.ministerHostelSideCat'
                                            id='ministerHostelSideCat_category_' tag='pb_html' onchange="isOtherSelected(this)">
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("am_minister_hostel_side",
                                                    Language, amMinisterHostelSideDTO.ministerHostelSideCat);
                                            Options += "<option value='-2'>" + otherText + "</option>";
                                        %>
                                        <%=Options%>
                                    </select>
                                    <div class="hiddenOtherField otherElement">
                                        <input type="text" name="amMinisterHostelSide.otherMinisterHostelSideEn" placeholder="<%=UtilCharacter.getDataByLanguage(Language,
                                            "অন্যান্য মন্ত্রী হোস্টেল পাশ ইংরেজি","Other Minister Hostel Side English")%>" class="form-control mt-2">
                                        <input type="text" name="amMinisterHostelSide.otherMinisterHostelSideBn" placeholder="<%=UtilCharacter.getDataByLanguage(Language,
                                            "অন্যান্য মন্ত্রী হোস্টেল পাশ বাংলা","Other Minister Hostel Side Bangla")%>" class="form-control mt-2">
                                    </div>

                                </td>

                                <td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center mb-2 mt-5">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_BLOCK_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.AM_MINISTER_HOSTEL_BLOCK_ADD_AM_MINISTER_HOSTEL_BLOCK_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>





    </div>
</div>

<script type="text/javascript">


    let duplicatePackageErr;
    let requiredMsg, duplicateMsg;

    function resetErrorMsg() {
        var inp = document.getElementsByClassName("error-msg");
        for (var i = 0; i < inp.length; ++i) {
            inp[i].style.display = 'none';
        }
    }

    function noDuplicate() {

        var nameBns = document.getElementsByName("amMinisterHostelSide.ministerHostelSideCat");

        var noDuplicate = true;
        const concatedDataSet = new Set();

        for (var i = 0; i < nameBns.length; ++i) {
            var concatedData = nameBns[i].value;
            if (concatedDataSet.has(concatedData)) {
                nameBns[i].parentNode.childNodes.item(1).style.display = 'block';
                nameBns[i].parentNode.childNodes.item(1).innerText = duplicateMsg;

                noDuplicate = false;
            }
            if(+concatedData !== -2) concatedDataSet.add(concatedData); // skip other option
        }

        return noDuplicate;

    }

    function validateString() {
        var foundErr = false;
        var fields = ['amMinisterHostelSide.ministerHostelSideCat'];

        for (var field = 0; field < fields.length; ++field) {
            var inp = document.getElementsByName(fields[field]);
            for (var i = 0; i < inp.length; ++i) {
                if (!(inp[i].value && inp[i].value !== undefined && inp[i].value.toString().trim().length > 0
                    && (parseInt(inp[i].value.toString()) > 0 || parseInt(inp[i].value.toString()) === -2))) {
                    inp[i].parentNode.childNodes.item(1).style.display = 'block';
                    inp[i].parentNode.childNodes.item(1).innerText = requiredMsg;
                    foundErr = true;
                }
            }
        }

        return !foundErr;
    }

    function PreprocessBeforeSubmiting(row, validate) {

        resetErrorMsg();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid();
        valid = valid && validateString() && noDuplicate();
        if (valid) {
            submitMinisterHostelBlock();
        }
        return false;
    }

    function submitMinisterHostelBlock() {
        buttonStateChange(true);
        var form = $("#bigform");
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    toastr.error(response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    showToastSticky("মিনিস্টার হোস্টেল ব্লক সেভ করা হয়েছে", "Minister hostel block saved");
                    setTimeout(() => {
                        location.href = response.msg;
                    }, 1000);


                    buttonStateChange(true);
                }
            }
        });
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Am_minister_hostel_blockServlet");
    }

    function init(row) {

        let lang = '<%=Language%>';

        if (lang == 'English') {
            requiredMsg = 'This is required';
            duplicateMsg = 'This is duplicated';

        } else {
            requiredMsg = '<%="এই তথ্যটি আবশ্যক"%>';
            duplicateMsg = '<%="পূর্বে ব্যবহৃত হয়েছে"%>';

        }


        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                blockNo: {
                    required: true,
                },

            },
            messages: {
                blockNo: requiredMsg,

            }
        });
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-AmMinisterHostelSide").click(
        function (e) {
            e.preventDefault();
            let t = $("#template-AmMinisterHostelSide");

            $("#field-AmMinisterHostelSide").append(t.html());
            SetCheckBoxValues("field-AmMinisterHostelSide");

            let tr = $("#field-AmMinisterHostelSide").find("tr:last-child");

            tr.attr("id", "AmMinisterHostelSide_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                let prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
            });


            child_table_extra_id++;

        });


    $("#remove-AmMinisterHostelSide").click(function (e) {
        var tablename = 'field-AmMinisterHostelSide';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });

    function isOtherSelected(selectedSelector){
        let otherVal = selectedSelector.value;
        let otherElements = selectedSelector.parentNode.querySelectorAll('.otherElement');
        if(otherVal === '-2' ){
            otherElements.forEach(item => {
                item.classList.remove('hiddenOtherField');
            });
        }
        else{
            otherElements.forEach(item => {
                item.classList.add('hiddenOtherField');
            });
        }
    }


</script>






