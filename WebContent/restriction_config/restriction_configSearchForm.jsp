
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="restriction_config.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.*"%>
<%@page contentType="text/html;charset=utf-8" %>

<%
String navigator2 = "navRESTRICTION_CONFIG";
String servletName = "Restriction_configServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTIONREASON, loginDTO)%></th>
								<th><%=isLanguageEnglish?"Action taken By":"পদক্ষেপগ্রহীতা"%></th>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_HISTORY, loginDTO)%></th>								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Restriction_configDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Restriction_configDTO restriction_configDTO = (Restriction_configDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%=WorkflowController.getNameFromEmployeeRecordID(restriction_configDTO.employeeRecordId, Language)%><br>
											<%=WorkflowController.getUserNameFromErId(restriction_configDTO.employeeRecordId, Language)%>
											</td>
		
											<td>
											<%=restriction_configDTO.restrictionReason%>
											</td>
		
											<td>
											<%=WorkflowController.getNameFromEmployeeRecordID(restriction_configDTO.insertedBy, Language)%><br>
											<%=WorkflowController.getUserNameFromErId(restriction_configDTO.insertedBy, Language)%>
											</td>
		
											<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, restriction_configDTO.insertionDate), Language)%>
											</td>
		
		
											<td>
											<%
											List<RestrictionConfigDetailsDTO> restrictionConfigDetailsDTOs = RestrictionConfigDetailsRepository.getInstance().getRestrictionConfigDetailsDTOByrestrictionConfigId(restriction_configDTO.iD);
											for(RestrictionConfigDetailsDTO restrictionConfigDetailsDTO: restrictionConfigDetailsDTOs)
											{
												%>
												<%=CatRepository.getInstance().getText(Language, "restriction", restrictionConfigDetailsDTO.restrictionCat)%>
												<br>
												<%
											}
											%>
											</td>
											
											<td >
												<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
											            onclick="location.href='Restriction_configServlet?actionType=history&erId=<%=restriction_configDTO.employeeRecordId%>'">
											        <i class="fa fa-history"></i>
											    </button>
											</td>
													
	
											
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			