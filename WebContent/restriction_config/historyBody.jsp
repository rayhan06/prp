

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="restriction_config.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Restriction_configServlet";
long erId = (Long)request.getAttribute("erId");
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.HM_HISTORY, loginDTO)%>
                </h3>
            </div>
            <div class="ml-auto mr-5 mt-4">
			    <button type="button" class="btn" id='printer2'
			            onclick="printAnyDiv('modalbody')">
			        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
			    </button>
			
			</div>
		            
        </div>
         
        <div class="kt-portlet__body form-body" id = 'modalbody'>
         		

             <div class="mt-5">
                <div class=" div_border attachement-div">
                		<div style = 'text-align: center;'>
	                        <h4><%=WorkflowController.getNameFromEmployeeRecordID(erId, isLanguageEnglish)%>
	                        , <%=WorkflowController.getUserNameFromErId(erId, Language)%>
	                        </h4>
	                        <h5><%=isLanguageEnglish?"Restriction History":"রেস্ট্রিকশনের ইতিহাস"%></h5>
                        </div>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=isLanguageEnglish?"Action taken By":"পদক্ষেপগ্রহীতা"%></th>
								<th><%=LM.getText(LC.HM_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTIONREASON, loginDTO)%></th>
							</tr>
							<%
                         	List<Restriction_configDTO> restriction_configDTOs = (List<Restriction_configDTO>)Restriction_configDAO.getInstance().getDTOsByParentTimeDesc("employee_record_id", erId);
                         	
                         	for(Restriction_configDTO restriction_configDTO: restriction_configDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, restriction_configDTO.insertionDate), Language)%>
										</td>
										<td>
											<%=WorkflowController.getNameFromEmployeeRecordID(restriction_configDTO.insertedBy, Language)%><br>
											<%=WorkflowController.getUserNameFromErId(restriction_configDTO.insertedBy, Language)%>
										</td>
										<td>
											<%
											List<RestrictionConfigDetailsDTO> restrictionConfigDetailsDTOs = RestrictionConfigDetailsRepository.getInstance().getRestrictionConfigDetailsDTOByrestrictionConfigId(restriction_configDTO.iD);
											for(RestrictionConfigDetailsDTO restrictionConfigDetailsDTO: restrictionConfigDetailsDTOs)
											{
												%>
												<%=CatRepository.getInstance().getText(Language, "restriction", restrictionConfigDetailsDTO.restrictionCat)%>
												<br>
												<%
											}
											%>
										</td>
										<td>
											<%=restriction_configDTO.restrictionReason%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>