

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="restriction_config.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Restriction_configServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Restriction_configDTO restriction_configDTO = Restriction_configDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = restriction_configDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_EMPLOYEERECORDID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(restriction_configDTO.employeeRecordId, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTIONREASON, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=restriction_configDTO.restrictionReason%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_INSERTEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(restriction_configDTO.insertedBy, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_INSERTTIONDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, restriction_configDTO.insertionDate), Language)%>
                                    </div>
                                </div>
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_DETAILS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_DETAILS_RESTRICTIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_DETAILS_INSERTEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_DETAILS_INSERTTIONDATE, loginDTO)%></th>
							</tr>
							<%
                        	RestrictionConfigDetailsDAO restrictionConfigDetailsDAO = RestrictionConfigDetailsDAO.getInstance();
                         	List<RestrictionConfigDetailsDTO> restrictionConfigDetailsDTOs = (List<RestrictionConfigDetailsDTO>)restrictionConfigDetailsDAO.getDTOsByParent("restriction_config_id", restriction_configDTO.iD);
                         	
                         	for(RestrictionConfigDetailsDTO restrictionConfigDetailsDTO: restrictionConfigDetailsDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=CatRepository.getInstance().getText(Language, "restriction", restrictionConfigDetailsDTO.restrictionCat)%>
										</td>
										<td>
											<%=Utils.getDigits(restrictionConfigDetailsDTO.insertedBy, Language)%>
										</td>
										<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, restrictionConfigDetailsDTO.inserttionDate), Language)%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>