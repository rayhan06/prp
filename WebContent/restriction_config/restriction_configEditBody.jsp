<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="restriction_config.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="category.*"%>

<%
Restriction_configDTO restriction_configDTO = new Restriction_configDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	restriction_configDTO = Restriction_configDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = restriction_configDTO;
String tableName = "restriction_config";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_ADD_FORMNAME, loginDTO);
String servletName = "Restriction_configServlet";
List<CategoryDTO> catList = CatRepository.getInstance().getCategoryDTOList("restriction");
System.out.println("catList size = " + catList.size());
%>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Restriction_configServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=restriction_configDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='employeeRecordId' id = 'employeeRecordId' value='<%=restriction_configDTO.employeeRecordId%>' tag='pb_html'/>
														
														
													<div class="form-group row">
			                                            <label class="col-4 col-form-label text-right">
			                                                <%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
			                                            </label>
			                                            <div class="col-md-8">
			                                                <button type="button"
			                                                        class="btn text-white shadow form-control btn-border-radius"
			                                                        style="background-color: #4a87e2"
			                                                        onclick="addEmployee()"
			                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
			                                                </button>
			                                                <table class="table table-bordered table-striped"
			                                                       >
			                                                    <tbody id="employeeToSet"></tbody>
			                                                </table>
			                                            </div>
			                                        </div>	
			                                        
			                                        
			                                        <div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
															</label>
                                                            <div class="col-6">
																<input type='text' class='form-control'  name='userName' id = 'userName' value='' required  tag='pb_html'/>					
															</div>
															<div class="col-2">
																 <button type="button"
			                                                        class="btn text-white shadow form-control btn-border-radius"
			                                                        style="background-color: #4a87e2"
			                                                        onclick="setBoxes(convertToEnglishNumber($('#userName').val()))"
			                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.GLOBAL_GO, loginDTO)%>
			                                                	</button>				
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTIONREASON, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='restrictionReason' id = 'restrictionReason_text_<%=i%>' value='<%=restriction_configDTO.restrictionReason%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													
																			
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_DETAILS, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_DETAILS_RESTRICTIONCAT, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_SELECT, loginDTO)%></th>
										
									</tr>
								</thead>
							<tbody id="field-RestrictionConfigDetails">
						
						
								<%
									{
										int index = -1;
										
										
										for(CategoryDTO categoryDTO: catList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "RestrictionConfigDetails_<%=index + 1%>">
									
									
									
									<td>										

										<%=isLanguageEnglish? categoryDTO.nameEn: categoryDTO.nameBn%>
	
									</td>
						
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='restrictionConfigDetails.restrictionCat' data = '<%=categoryDTO.value%>' fetched = '0' 
													id = "restrictionCat_<%=index%>" onChange = "check(this.id)"
												   class="form-control-sm"/>
												   
											<input type='hidden' name='sCat'  id = 'sCat_<%=index%>' value = "<%=categoryDTO.value%>"
												   class="form-control-sm"/>
												   
												   
											 <input type='hidden' name='sCatVal'  id = 'sCatVal_<%=index%>' value = "0"
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
					
					
							<%RestrictionConfigDetailsDTO restrictionConfigDetailsDTO = new RestrictionConfigDetailsDTO();%>
					
                        </div>
                    </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.RESTRICTION_CONFIG_ADD_RESTRICTION_CONFIG_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">

function check(cbId)
{
	var rowIndex = cbId.split("_")[1];
	if($("#" + cbId).prop('checked'))
	{
		$("#sCatVal_" + rowIndex).val(1);
	}
	else
	{
		$("#sCatVal_" + rowIndex).val(0);
	}
}

function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	
	var changeCount = 0;
	
	$( "[name = 'restrictionConfigDetails.restrictionCat']" ).each(function( index ) {
		  if(($( this ).prop("checked") && $( this ).attr("fetched") == 0)
				  || (! ($( this ).prop("checked")) && $( this ).attr("fetched") == 1))
		  {
			  changeCount ++;
		  }
	});
	
	if(changeCount == 0)
	{
		toastr.error("No change detected");
		return false;
	}
	

	
	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Restriction_configServlet");	
}

function init(row)
{

	
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function processRowsWhileAdding(childName)
{
	
}

function setBoxes(userName)
{
	 var xhttp = new XMLHttpRequest();
	    xhttp.onreadystatechange = function () {
	        if (this.readyState == 4 && this.status == 200) {
	        	var rcDTO = JSON.parse(this.responseText);
	        	console.log(rcDTO);
	        	if(rcDTO != null)
	        	{
	        		var restrictionConfigDetailsDTOList = rcDTO.restrictionConfigDetailsDTOList;
	        		
	        		console.log("restrictionConfigDetailsDTOList.length = " + restrictionConfigDetailsDTOList.length);
	            	
	            	$( "[name = 'restrictionConfigDetails.restrictionCat']" ).each(function( index ) {
	            		 $( this ).prop("checked", false);
	            		 $( this ).attr("fetched", '0');
	            		 $( $("#sCatVal_" + index)).val(0);
	      			});
	            	
	            	for (let i = 0; i < restrictionConfigDetailsDTOList.length; i++) {
	            		console.log("restrictionConfigDetailsDTOList[i].restrictionCat = " + restrictionConfigDetailsDTOList[i].restrictionCat);
	            		$( "[name = 'restrictionConfigDetails.restrictionCat']" ).each(function( index ) {
	            			  if($( this ).attr('data') == restrictionConfigDetailsDTOList[i].restrictionCat)
	           				  {
	            				  $( this ).prop("checked", true);
	            				  $( this ).attr("fetched", '1');
	            				  
	            				  $( $("#sCatVal_" + index)).val(1);
	           				  }
	            		});
	            	}
	        	}
	        	
	        }
	        else 
	        {
	            //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
	        }
	    };

	    xhttp.open("GET", "Restriction_configServlet?actionType=getLastByErId&userName=" + userName, true);
	    xhttp.send();
}

function patient_inputted(userName, orgId, erId) {
    console.log("patient_inputted " + userName + " erId = " + erId);
    $("#erId").val(erId);
    $("#userName").val(userName);
    setBoxes(userName);
   
}

</script>






