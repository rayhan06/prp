<link rel="stylesheet" href="https://fonts.maateen.me/solaiman-lipi/font.css" />

<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<!--begin:: Global Mandatory Vendors -->
<link href="<%=context%>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<link href="<%=context%>assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />

<%--<script src="<%=context%>assets/vendors/general/dropzone/dist/dropzone.css" type="text/javascript"></script>--%>
<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="<%=context%>assets/css/metronic/style.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="<%=context%>assets/css/metronic/skins/header/base/light.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/css/metronic/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/css/metronic/skins/brand/light.css" rel="stylesheet" type="text/css" />
<link href="<%=context%>assets/css/metronic/skins/aside/light.css" rel="stylesheet" type="text/css" />
<!--end::Layout Skins -->


<%--<link href="<%=context%>assets/css/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />--%>
<%--<link href="<%=context%>assets/css/new_styles.css" rel="stylesheet" type="text/css" />--%>

<script src="<%=context%>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js'></script>
<%--<script src="<%=context%>assets/scripts/client-autocomplete-tag.js" type="text/javascript"></script>--%>
<script src="<%=context%>assets/scripts/filePreview.js" type="text/javascript"></script>

<script src="<%=context%>assets/scripts/dropzone.js" ></script>
<link href="<%=context%>assets/scripts/dropzone.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.11/themes/default/style.min.css" />
<link rel="stylesheet" type="text/css" href="<%=context%>assets/css/custom.css" />
<link rel="stylesheet" type="text/css" href="<%=context%>assets/css/custom_styles.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>