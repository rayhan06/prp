<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#0076a7",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="<%=context%>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>

<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<script src="<%=context%>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/js/vendors/bootstrap-switch.init.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
<%--<script src="<%=context%>assets/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>--%>
<script src="<%=context%>assets/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/js/vendors/bootstrap-markdown.init.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/js/vendors/bootstrap-notify.init.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/raphael/raphael.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/morris.js/morris.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/custom/js/vendors/sweetalert2.init.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>
<script src="<%=context%>assets/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/scripts/jQuery.print.min.js" type="text/javascript"></script>


<!--end:: Global Optional Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<%=context%>assets/js/metronic/scripts.bundle.js" type="text/javascript"></script>

<script src="<%=context%>assets/js/metronic/pages/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<script src="<%=context%>assets/js/bootbox/bootbox.all.js"></script>

<script src="<%=context%>assets/scripts/menu.js" type="text/javascript"></script>
<script src="<%=context%>assets/scripts/pb.js" type="text/javascript"></script>
<%--<script src="<%=context%>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>--%>

<%--<script src="<%=context%>assets/global/plugins/jquery-validation-1.19.2/jquery-validate.min.js" type="text/javascript"></script>
<script src="<%=context%>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>--%>

<script src="<%=context%>assets/scripts/utility.js" type="text/javascript"></script>
<script src="<%=context%>assets/scripts/common.js" type="text/javascript"></script>
<%--<script src="<%=context%>assets/scripts/btcl-config.js" type="text/javascript"></script> --%>
<%--<script src="<%=context%>assets/scripts/adjust_side_bar.js" type="text/javascript"></script>--%>
<script src="<%=context%>assets/scripts/jquery.dataTables.js"></script>
<script src="<%=context%>assets/scripts/dataTables.bootstrap.min.js"></script>
<%--<script src="<%=context%>assets/filePreview.js"></script>--%>

<%--<script src="<%=context%>assets/ckeditor/ckeditor.js"></script>--%>

<%--<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>--%>
<%--<script src="https://ckeditor.com/apps/ckfinder/3.5.0/ckfinder.js"></script>--%>
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.11/jstree.min.js" ></script>

<script src="<%=context%>assets/scripts/form_submit.js"></script>
<%@include file="/tree/treeUtilJs.jsp"%>
<%@include file="/date/dateUtils.jsp"%>
<%@include file="/geolocation/geoLocationUtils.jsp"%>
<%@include file="/time/timeUtils.jsp"%>
<%@include file="/utility/util.jsp"%>

<script type="text/javascript">
   let contextPathHeader = '<%=context.replace("../","")%>';
   if(!contextPathHeader.startsWith("/")){
       contextPathHeader ="/"+contextPathHeader;
   }
   if(!contextPathHeader.endsWith("/")){
       contextPathHeader +="/";
   }
    $(document).ready( function(){
        <%if( headerLanguage.equalsIgnoreCase( "Bangla" ) ){%>
        $( "#lang-switcher" ).bootstrapSwitch( 'state', true );
        <%}else{%>
        $( "#lang-switcher" ).bootstrapSwitch( 'state', false );
        <%}%>

        $( "#lang-switcher" ).on( 'switchChange.bootstrapSwitch', function( e, data ){
            setTimeout( function(){
                window.location.replace(window.location.origin+contextPathHeader+'languageChangeServlet');
            }, 500 );
        });
    })

</script>