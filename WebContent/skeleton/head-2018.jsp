<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
	type="text/css" />
<link href="${context}assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${context}assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
	type="text/css" />
<link href="${context}assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${context}assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="${context}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
	type="text/css" />
<link href="${context}assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES --> 

<link href="${context}assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${context}assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${context}assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${context}assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${context}assets/layouts/layout4/css/custom.css" rel="stylesheet" type="text/css" />
<link href="${context}assets/layouts/layout_btcl.css" rel="stylesheet" type="text/css" />
<link href="${context}assets/styles/styles.css" rel="stylesheet" type="text/css" />

<link href="${context}assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />
<link href="${context}assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />


<!-- END THEME LAYOUT STYLES -->
<script src="${context}assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- 2018 -->
<!--  -->
<!--%@ include file="../assets/scripts/utility-2018.jsp"%-->
<!--  -->
<!-- VUE Begins-->
<!-- vue -->
<script src="${context}assets/global/plugins/vue2.5.16/vue.js" type="text/javascript"></script>
<!-- vue multiselect-->
<script src="${context}assets/global/plugins/vue-multiselect/vue-multiselect.min.js" type="text/javascript"></script>
<link href="${context}assets/global/plugins/vue-multiselect/vue-multiselect.css" rel="stylesheet">
<!-- vue datepicker -->
<script src="${context}assets/global/plugins/vue-datepicker/vue-datepicker.js" type="text/javascript"></script>
<!-- vue modal -->
<script src="${context}assets/global/plugins/vue-modal/index.js" type="text/javascript"></script>
<script src="${context}assets/global/plugins/vue-modal/ssr.index.js" type="text/javascript"></script>
<script src="${context}assets/global/plugins/vue-modal/ssr.nocss.js" type="text/javascript"></script>
<link href="${context}assets/global/plugins/vue-modal/styles.css" rel="stylesheet">
<!-- vue btcl -->
<script src="${context}assets/global/scripts/btcl-vue.js" type="text/javascript"></script> 
<!-- VUE Ends -->

<!-- Axios Begins -->
<script src="${context}assets/global/plugins/axios/axios.min.js" type="text/javascript"></script>
<!-- Axios Ends -->
