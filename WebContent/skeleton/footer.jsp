<%@page import="language.LM"%>
<%@page import="language.LC"%>
<%@page import="java.util.Calendar"%>
<%--<div class="page-footer">
<div style="display:inline-block;float:left"><%=Calendar.getInstance().get(Calendar.YEAR)%> &copy; <%=LM.getText(LC.FOOTER_BOTTOM_LEFT_TEXT)%></div>
<div style="display:inline-block;float:right">Powered By  
<a target="_blank" href="http://www.revesoft.com">
<img src="<%=request.getContextPath()%>/images/reve.png" alt="REVE Systems" width="100px">
</a>
</div>
</div>--%>

<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-footer__copyright">
        <%=Calendar.getInstance().get(Calendar.YEAR)%> &copy; Parliament Resource Planning&nbsp;&copy;&nbsp;
        <a href="revesoft.com" target="_blank" class="kt-link">REVE Systems Ltd</a>
    </div>
</div>

<!-- end:: Footer -->
