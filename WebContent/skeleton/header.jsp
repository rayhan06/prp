<%@page import="support_ticket.*"%>
<%@page contentType="text/html;charset=utf-8" %>
<%@page import="user.*" %>
<%@page import="pb.*" %>
<%@page import="java.util.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="pb_notifications.*" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@ page import="util.StringUtils" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="role.RoleDTO" %>
<%@ page import="role.PermissionRepository" %>
<%@ page import="common.RoleEnum" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%
    List<Pb_notificationsDTO> pb_notificationsDTOList = null;
    Long unseenCount = 0L;
    Object dtoList = request.getSession(true).getAttribute(SessionConstants.USER_NOTIFICATION);
    if (dtoList != null) {
        pb_notificationsDTOList = (List<Pb_notificationsDTO>) dtoList;
    }
    Object countObj = request.getSession(true).getAttribute(SessionConstants.USER_NOTIFICATION_COUNT);
    if (countObj != null) {
        unseenCount = (long) countObj;
    }
    LoginDTO localLoginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO localUserDTO = UserRepository.getUserDTOByUserID(localLoginDTO);
    List<UserDTO> otherUserDTOs = null;
    if (localUserDTO.ID != -1) {
        otherUserDTOs = new UserDAO().getOtherUserDTOsByUserIdAndRoleId(localUserDTO.ID, localUserDTO.roleID, localUserDTO.organogramID);
    }

    if (pb_notificationsDTOList == null || pb_notificationsDTOList.size() == 0) {
        Pb_notificationsDAO pb_notificationsDAO = Pb_notificationsDAO.getInstance();
        pb_notificationsDTOList = pb_notificationsDAO.initialFetchNotification(localLoginDTO.userID, localUserDTO.roleID, localUserDTO.organogramID);
        if (pb_notificationsDTOList.size() > 0) {
            request.getSession(true).setAttribute(SessionConstants.USER_NOTIFICATION, pb_notificationsDTOList);
            unseenCount = pb_notificationsDAO.getTotalUnseenCount(localLoginDTO.userID, localUserDTO.roleID, localUserDTO.organogramID);
        } else {
            unseenCount = 0L;
        }
        request.getSession(true).removeAttribute(SessionConstants.USER_NOTIFICATION_COUNT);
        request.getSession(true).setAttribute(SessionConstants.USER_NOTIFICATION_COUNT, unseenCount);
    } else {
        List<Pb_notificationsDTO> topList = Pb_notificationsDAO.getInstance().topUnFetchNotificationSQL(pb_notificationsDTOList.get(0).iD, localLoginDTO.userID, localUserDTO.roleID, localUserDTO.organogramID);
        if (topList.size() > 0) {
            unseenCount += topList.stream()
                    .filter(dto -> !dto.isSeen)
                    .count();
            pb_notificationsDTOList.addAll(0, topList);
            request.getSession(true).removeAttribute(SessionConstants.USER_NOTIFICATION_COUNT);
            request.getSession(true).setAttribute(SessionConstants.USER_NOTIFICATION_COUNT, unseenCount);
        }
    }

    String headerLanguage = "English";
    if (localUserDTO.languageID != 1) {
        headerLanguage = "Bangla";
    }
    int totalNotificationCount = pb_notificationsDTOList.size();
    String unseenCountStr = unseenCount > 99 ? "99+" : String.valueOf(unseenCount);
    Support_ticketDAO support_ticketDAO = new Support_ticketDAO();
%>

<style>
@media (max-width: 1024px){

.demo-class.show{
	  top: 45px!important
	}
}
</style>

<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i>
    </button>
    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">

        </div>
    </div>
    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">

        <!--begin: Notifications -->
        <div class="kt-header__topbar-item dropdown mr-3">

            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="true">
				<span class="kt-header__topbar-icon">
					<i class="flaticon2-bell-alarm-symbol"></i>
					<span class="kt-badge kt-badge--danger"
                          style="position: absolute; top: 10px; left: 25px;">
						<%=StringUtils.convertBanglaIfLanguageIsBangla(headerLanguage, unseenCountStr)%>
					</span>
				</span>
                <span class="kt-hidden kt-badge kt-badge--dot kt-badge--notify kt-badge--sm"></span>
            </div>

            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
                <form>
                    <input type="hidden" id="header_language_id" value="<%=headerLanguage%>">
                    <div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b"
                         style="background-image: url(<%=context%>assets/media/misc/bg-1.jpg)">
                        <h3 class="kt-head__title">
                            <%=headerLanguage.equalsIgnoreCase("english") ? "User Notifications" : "ব্যাবহারকারী নোটিফিকেশন"%>
                            &nbsp;
                            <span class="btn btn-success btn-sm btn-bold btn-font-md"><%=headerLanguage.equalsIgnoreCase("english") ? "New " : "নতুন "%>
                            <span id = "nc"><%=StringUtils.convertBanglaIfLanguageIsBangla(headerLanguage, unseenCountStr)%></span>
                             </span>
                        </h3>
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x"
                            role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" data-toggle="tab"
                                   href="#topbar_notifications_notifications" role="tab" aria-selected="true">
                                    <%=headerLanguage.equalsIgnoreCase("english") ? "All Notifications" : "সমস্ত নোটিফিকেশন"%>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <!--end: Head -->
                    <div class="tab-content">
                        <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
                            <div id="notification_div" class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll"
                                 data-scroll="true" data-height="300" data-mobile-height="200">
                                <%
                                    int countToShow = pb_notificationsDTOList.size();
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

                                    int nc = 0;
                                    for (int i = 0; i < countToShow; i++) {

                                        String notiDate = simpleDateFormat.format(new Date(pb_notificationsDTOList.get(i).showingDate));
                                        String text = "";
                                        Pb_notificationsDTO pbNotificationsDTO = pb_notificationsDTOList.get(i);
                                        String[] tokens = pbNotificationsDTO.text.split("\\$");
                                        if (headerLanguage.equalsIgnoreCase("english") && tokens.length >= 1) {
                                            text = tokens[0];
                                        } else if (tokens.length >= 2) {
                                            text = tokens[1];
                                        }
                                        boolean show = true;
                                        if(tokens[0].startsWith(Support_ticketDAO.DEADLINE_TICKET_START))
                                        {
                                        	try
                                        	{
                                        		long ticketId = Long.parseLong(tokens[0].split("#")[1]);
                                        		Support_ticketDTO support_ticketDTO = support_ticketDAO.getDTOByID(ticketId);
                                                show = support_ticketDTO != null && support_ticketDTO.ticketStatusCat == Support_ticketDTO.OPEN
                                                        && System.currentTimeMillis() > support_ticketDAO.getCrossTime(support_ticketDTO);
                                        	}
                                        	catch(Exception ex)
                                        	{
                                        		ex.printStackTrace();
                                        	}
                                        	
                                        }
                                        if(show)
                                        {
                                        	if(!pbNotificationsDTO.isSeen)
                                        	{
                                        		nc ++;
                                        	}
                                %>
                                <a href="<%=pb_notificationsDTOList.get(i).url%>"
                                   onclick='return seeNotification( <%=pb_notificationsDTOList.get(i).iD%>,<%=i%> );'
                                   class="kt-notification__item">
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title"
                                             style="color: <%=pbNotificationsDTO.isSeen?"#6C7293":"forestgreen"%>">
                                            <%=text%>
                                        </div>
                                        <div class="kt-notification__item-time">
                                            <%= Utils.getDigits(notiDate, headerLanguage)%>
                                        </div>
                                    </div>
                                </a>

                                <%
                                        }
                                    }

                                    if (countToShow == 0) {
                                %>

                                <div style="width: 50%; margin: auto; padding-top: 30px;">
										<span class="kt-font-success">
											No new Notification
										</span>
                                </div>
                                <%}%>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                <div class="kt-header__topbar-user">
                    <input
                            data-switch="true"
                            type="checkbox"
                            checked="checked"
                            id="lang-switcher"
                            data-off-text="বাংলা"
                            data-on-text="English"
                            data-off-color="success"
                            data-on-color="info"
                            data-label-text="<i class='fa fa-globe'></i>"
                    />
                </div>
            </div>
        </div>

        <!--begin: User Bar -->
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                <div class="kt-header__topbar-user">
					<span class="kt-header__topbar-username kt-hidden-mobile text-right">
                        <%
                            OfficeUnitOrganograms officeUnitOrganograms = null;
                            if (localUserDTO.organogramID > 0) {
                                officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(localUserDTO.organogramID);
                            }
                            Office_unitsDTO officeUnitsDTO = null;
                            if (officeUnitOrganograms != null) {
                                officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(officeUnitOrganograms.office_unit_id);
                            }
                            Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(localUserDTO.employee_record_id);
                            boolean isLangEng = "English".equalsIgnoreCase(headerLanguage);
                            /*RoleDTO roleDTO = PermissionRepository.getRoleDTOByRoleID(localUserDTO.roleID);*/
                        %>
                        <%if (employeeRecordsDTO != null) {%>
                        <%=isLangEng ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng%>
                        <%}%>

                        <%if (officeUnitOrganograms != null) {%>
                        - <%=(isLangEng ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng)%> <br>
                        <%}%>

                        <%if (officeUnitsDTO != null) {%>
                            <%=(isLangEng ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng)%>
                        <%}%>
                        <%--<%=roleDTO == null ? "" : ""+roleDTO.roleName+""%>--%>
                          <br><%=isLangEng ? "Role" : "রোল:"%>
                            : <%=isLangEng ? CommonDAO.getName(localUserDTO.roleID, "role", "roleName", "id"):  CommonDAO.getName(localUserDTO.roleID, "role", "role_bn", "id")%>
					</span>

                    <%
                        byte[] pp = (byte[]) request.getSession().getAttribute(SessionConstants.USER_PP);
                        if (pp != null) {
                    %>
                    <img src='data:image/jpg;base64,<%=new String(pp)%>'/>
                    <%} else {%>

                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                    <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><%=localUserDTO.userName.substring(0, 1)%></span>
                    <%}%>
                </div>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl demo-class">

                <!--begin: Navigation -->
                <div class="kt-notification" style="max-height: 90vh; overflow-y: auto">
                    <%
                        if (localUserDTO.roleID != RoleEnum.ADMIN.getRoleId()) {
                    %>
                    <a href="<%=context%>Employee_recordsServlet?actionType=viewMyProfile"
                       class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon-profile-1 kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                <%=LM.getText(LC.GLOBAL_MY_PROFILE, localLoginDTO)%>
                            </div>
                            <div class="kt-notification__item-time">
                                <%=LM.getText(LC.GLOBAL_ACCOUNT_SETTING_AND_MORE, localLoginDTO)%>
                            </div>
                        </div>
                    </a>
                    <%
                        }
                    %>

                    <%
                        if (otherUserDTOs != null) {
                            for (UserDTO otherUserDTO : otherUserDTOs) {
                    %>

                    <a target=""
                       href="<%=context%>UserServlet?actionType=changeUser&userId=<%=otherUserDTO.ID%>&roleId=<%=otherUserDTO.roleID%>&organogramId=<%=otherUserDTO.organogramID%>"
                       class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon-profile-1 kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details font-weight-bold">
                            <%--<%=WorkflowController.getNameFromOrganogramId(otherUserDTO.organogramID, headerLanguage)%><br>--%>
                            <%
                                officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(otherUserDTO.unitID);
                                if (officeUnitsDTO != null) {
                            %>
                            <%=isLangEng ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng%><br>
                            <%
                                }
                            %>
                            <%--<%=WorkflowController.getOfficeUnitName(otherUserDTO.unitID, headerLanguage) %><br>--%>
                            <%
                                officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(otherUserDTO.organogramID);
                                if (officeUnitOrganograms != null) {
                            %>
                            <%=isLangEng ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng%>
                            <%
                                }
                            %>
                            <%--<%=WorkflowController.getOrganogramName(otherUserDTO.organogramID, headerLanguage) %><br>--%>
                            <br>
                            <%=isLangEng ? "Role" : "রোল:"%>
                            : <%=CommonDAO.getName(otherUserDTO.roleID, "role", "roleName", "id") %>
                        </div>
                    </a>

                    <%
                            }
                        }
                    %>

                    <%
                        if (localUserDTO.roleID != RoleEnum.ADMIN.getRoleId()) {
                    %>
                    <a href="<%=context%>UserServlet?actionType=getChangePassword" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon-lock kt-font-info"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                <%=LM.getText(LC.GLOBAL_CHANGE_PASSWORD, localLoginDTO)%>
                            </div>
                            <div class="kt-notification__item-time">
                                <%=LM.getText(LC.GLOBAL_CHANGE_EXISTING_PASSWORD, localLoginDTO)%>
                            </div>
                        </div>
                    </a>
                    <%
                        }
                    %>

                    <a href="<%=context%>LogoutServlet" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon-logout kt-font-danger"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                <%=LM.getText(LC.GLOBAL_LOGOUT, localLoginDTO)%>
                            </div>
                            <div class="kt-notification__item-time">
                                <%=LM.getText(LC.GLOBAL_END_YOUR_SESSION, localLoginDTO)%>
                            </div>
                        </div>
                    </a>

                </div>

                <!--end: Navigation -->
            </div>
        </div>

        <!--end: User Bar -->
    </div>

    <!-- end:: Header Topbar -->
</div>

<script>
    let hasMoreNotification = true <%--<%=pb_notificationsDTOList.size()>=30%>--%>;
    let totalNotificationCount = <%=totalNotificationCount%>;
    console.log('totalNotificationCount : ' + totalNotificationCount);
    const headerLanguage = '<%=headerLanguage%>'.toLowerCase();
    const nextFetchNotificationCount = <%=SessionConstants.NEXT_FETCH_NOTIFICATION_COUNT%>

        function seeNotification(id, i) {
            console.log('seeNotification called');
            var formData = new FormData();
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (this.responseText != '') {
                        console.log("Response");
                    } else {
                        console.log("No Response");
                    }
                } else if (this.readyState == 4 && this.status != 200) {
                    alert('failed ' + this.status);
                }
            };
            xhttp.open("POST", 'Pb_notificationsServlet?actionType=makeseen&id=' + id + '&index=' + i, false);
            xhttp.send(formData);
            return true;
        }

    function fetchNextNotifications() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    let list = JSON.parse(this.responseText);
                    console.log('fetched notification length : ' + list.length);
                    for (let i = 0; i < list.length; i++) {
                        let dto = list[i];
                        let tokens = dto.text.split('$');
                        let text = '';
                        if (headerLanguage === 'english' && tokens.length >= 1) {
                            text = tokens[0];
                        } else if (tokens.length >= 2) {
                            text = tokens[1];
                        }
                        let color = dto.isSeen ? "#6C7293" : "forestgreen";
                        let newRow = '<a href="' + dto.url + '" onclick="return seeNotification(' + dto.iD + ',' + totalNotificationCount + ');" class="kt-notification__item">'
                            + '<div class="kt-notification__item-details">'
                            + '<div class="kt-notification__item-title" style="color: ' + color + '">' + text + '</div>'
                            + '<div class="kt-notification__item-time">' + dto.dateStr + '</div>'
                            + '</div></a>';
                        $('#notification_div').append(newRow);
                        totalNotificationCount = totalNotificationCount + 1;
                    }
                    if (list.length <= nextFetchNotificationCount) {
                        hasMoreNotification = true;
                    }
                } else {
                    console.log("No Response");
                }
            } else if (this.readyState == 4 && this.status != 200) {
                //alert('failed ' + this.status);
                hasMoreNotification = true;
                console.log('Failed to get notification');
            }
        };
        xhttp.open("GET", 'Pb_notificationsServlet?actionType=nextFetchNotification&language=' + headerLanguage, true);
        xhttp.send();
    }

    $(document).ready(() => {
        $('#notification_div').scroll(() => {
            if (hasMoreNotification) {
                let notificationSelector = $('#notification_div')[0];
                let remainHeight = notificationSelector.scrollHeight - notificationSelector.offsetHeight - Math.ceil(notificationSelector.scrollTop);
                if (remainHeight < 1000) {
                    hasMoreNotification = false;
                    fetchNextNotifications();
                }
            }
        });
        $("#nc").html('<%=StringUtils.convertBanglaIfLanguageIsBangla(headerLanguage, nc + "")%>');
    })
</script>

