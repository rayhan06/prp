<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="budget_operation.BudgetOperationModel" %>
<%@ page import="economic_code.Economic_codeDTO" %>
<%@ page import="common.NameDTO" %>
<%@ page import="economic_group.EconomicGroupRepository" %>
<%@ page import="economic_code.Economic_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeDTO" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="java.util.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="static java.util.stream.Collectors.toList" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_submission_info.Budget_submission_infoDAO" %>
<%@ page import="budget.*" %>

<%

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);

    String context = request.getContextPath() + "/";

    long selectionId = Long.parseLong(request.getParameter("selectionInfoId"));
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoRepository.getInstance().getDTOByID(selectionId);

    List<BudgetDTO> budgetDTOS = null;
    Map<Long, List<BudgetDTO>> mapByBudgetMappingId;
    String errorMessage = null;
    String economicYear = "";
    String economicYearWithDescription = "";

    int budgetType = Integer.parseInt(request.getParameter("budgetType"));
    String budgetTypeDescription = "";
    if(budgetType == BudgetTypeEnum.BUDGET.getValue()){
        budgetTypeDescription = UtilCharacter.getDataByLanguage(Language, "প্রকৃত ব্যায় ", "Expenditure");
    } else {
        budgetTypeDescription = UtilCharacter.getDataByLanguage(Language, "সংশোধিত প্রকৃত ব্যায় ", "Revised Expenditure");
    }

    boolean isAbleToSubmitExpenditure = false;

    if(budgetSelectionInfoDTO != null){
        budgetDTOS = BudgetDAO.getInstance().getDTOsBySelectionInfo(budgetSelectionInfoDTO.iD);

        isAbleToSubmitExpenditure = Budget_submission_infoDAO.getInstance().isAbleToSubmitExpenditure(
                budgetSelectionInfoDTO.iD, BudgetUtils.FINANCE_SECTION_OFFICE_ID, budgetType
        );

        economicYear = StringUtils.convertBanglaIfLanguageIsBangla(
                Language,
                budgetSelectionInfoDTO.economicYear
        );

        economicYearWithDescription = (isLanguageEnglish ? "Economic Year - " : "আর্থিক বছর - ")
                + economicYear;
    }

    if(budgetDTOS != null){
        mapByBudgetMappingId = budgetDTOS.stream()
                .collect(Collectors.groupingBy(dto -> dto.budgetMappingId));
    }else{
        mapByBudgetMappingId = new HashMap<>();
        errorMessage = UtilCharacter.getDataByLanguage(
                Language, "বিগত  ইকোনোমিক বছরের জন্যে কোনো ডাটা দেওয়া হয় নি ",
                "No Data entried for last economic year."
        );
    }
%>

<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-auto mr-3 mt-4">
    <button type="button" class="btn" id='printer'
            onclick="printDiv('kt_content')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content" >
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body m-4 page-bg">
                <div class="row">
                    <div class="col-12 row">
                        <div class="offset-3 col-6 text-center">
                            <img width="20%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>

                <%if(errorMessage != null){%>
                    <div class="container text-center m-4">
                        <h5>
                            <%=errorMessage%>
                        </h5>
                    </div>
                <%}%>

                <%
                    for (Map.Entry<Long, List<BudgetDTO>> entry : mapByBudgetMappingId.entrySet()) {
                        Long budgetMappingId = entry.getKey();
                        List<BudgetDTO> budgetDTOsByMapping = entry.getValue();
                        BudgetOperationModel model = Budget_mappingRepository.getInstance()
                                .getBudgetOperationModelWithoutSubCode(budgetMappingId, budgetSelectionInfoDTO.economicYear, Language);
                %>
                        <div class="row mt-5 mx-0 py-3 rounded" style="border: 1px solid #00ACD8;">
                            <div class="col-9 text-left">
                                <h5 class="text-left mt-3 top-section-font" id="budgetCatHeading">
                                    <%=model.budgetCatName + " - " + model.officeCode%>
                                </h5>
                                <h5 class="text-left mt-3 top-section-font" id="operationalCodeHeading">
                                    <%=model.description + " - " + model.operationCode%>
                                </h5>
                            </div>
                            <div class="col-3">
                                <h5 class="text-right mt-3 top-section-font" id="economicYearHeading">
                                    <%=economicYearWithDescription%>
                                </h5>
                            </div>
                        </div>

                        <div class="row mt-5 page-break">
                            <div class="table-responsive">
                                <table class="table" id="budget-view-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                        </th>
                                        <th>
                                            <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                        </th>

                                        <th>
                                            <%=budgetTypeDescription%>
                                            <br>
                                            <%=economicYear%>
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                        <%--Economic Groups--%>
                                        <%
                                            TreeMap<Long, List<BudgetDTO>> sortedMapByEconomicGroup =
                                                    budgetDTOsByMapping.stream()
                                                    .collect(Collectors.groupingBy(
                                                            dto -> dto.economicGroupId,
                                                            TreeMap::new,
                                                            toList()
                                                    ));

                                            for (Map.Entry<Long, List<BudgetDTO>> groupBudgedPair : sortedMapByEconomicGroup.entrySet()){
                                                Long economicGroupId = groupBudgedPair.getKey();
                                                List<BudgetDTO> budgetDTOsByGroup = groupBudgedPair.getValue();
                                                NameDTO groupDTO = EconomicGroupRepository.getInstance().getDTOByID(economicGroupId);
                                                if(groupDTO == null){
                                                    groupDTO = new NameDTO();
                                                }
                                        %>
                                                <tr >
                                                    <td style=" font-weight: bold" colspan="100%">
                                                        <%String groupID = StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(groupDTO.iD));%>
                                                        <%=groupID + " - " + UtilCharacter.getDataByLanguage(Language, groupDTO.nameBn, groupDTO.nameEn)%>
                                                    </td>
                                                </tr>

                                                <%--Economic Code--%>
                                                <%
                                                    Map<Long, List<BudgetDTO>> mapByEconomicCode = budgetDTOsByGroup.stream()
                                                                    .collect(Collectors.groupingBy(dto -> dto.economicCodeId));

                                                    List<Long> economicCodeIdSortedByCode = mapByEconomicCode.keySet()
                                                            .stream().sorted(BudgetUtils::compareEconomicCodeId)
                                                            .collect(toList());

                                                    for (Long economicCodeId : economicCodeIdSortedByCode){
                                                        List<BudgetDTO> budgetDTOsByCode = mapByEconomicCode.get(economicCodeId);
                                                        Economic_codeDTO codeDTO = Economic_codeRepository.getInstance().getById(economicCodeId);
                                                        if(codeDTO == null){
                                                            codeDTO = new Economic_codeDTO();
                                                        }
                                                %>
                                                        <tr >
                                                            <td style=" font-weight: bold" colspan="100%">
                                                                <%String economicCode = StringUtils.convertBanglaIfLanguageIsBangla(Language, codeDTO.code);%>
                                                                <%= economicCode + " - "
                                                                        + UtilCharacter.getDataByLanguage(Language, codeDTO.descriptionBn, codeDTO.descriptionEn)%>
                                                            </td>
                                                        </tr>

                                                        <%--Economic Sub Code--%>
                                                        <%
                                                            budgetDTOsByCode.sort(BudgetUtils::compareBudgetDTOonSubCode);

                                                            for (BudgetDTO budgetDTOofSubCode : budgetDTOsByCode){
                                                                Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance()
                                                                        .getDTOByID(budgetDTOofSubCode.economicSubCodeId);
                                                                if(subCodeDTO == null){
                                                                    subCodeDTO = new Economic_sub_codeDTO();
                                                                }
                                                        %>
                                                                <tr>
                                                                    <td>
                                                                        <%=StringUtils.convertBanglaIfLanguageIsBangla(Language,subCodeDTO.code)%>
                                                                    </td>
                                                                    <td>
                                                                        <%=UtilCharacter.getDataByLanguage(Language, subCodeDTO.descriptionBn, subCodeDTO.descriptionEn)%>
                                                                    </td>

                                                                    <%--Budget Info Goes here--%>
                                                                    <%--Conditional Heading--%>
                                                                    <%if(budgetType == BudgetTypeEnum.BUDGET.getValue()){%>
                                                                        <td>
                                                                            <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                                                                    Language,
                                                                                    String.valueOf(budgetDTOofSubCode.expenditureAmount)
                                                                            )%>
                                                                        </td>
                                                                    <%}else{%>
                                                                        <td>
                                                                            <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                                                                    Language,
                                                                                    String.valueOf(budgetDTOofSubCode.revisedExpenditureAmount)
                                                                            )%>
                                                                        </td>
                                                                    <%}%>
                                                                </tr>
                                                        <%}%> <%--end for economic sub group--%>
                                                        <%--Sub Total of Economic Codes--%>
                                                        <tr>
                                                            <td colspan="2" style="font-weight: bold" class="text-right">
                                                                <%=LM.getText(LC.BUDGET_SUBTOTAL, loginDTO)%>
                                                            </td>

                                                            <td>
                                                                <%
                                                                    Double sumByCode = budgetDTOsByCode.stream()
                                                                            .mapToDouble(
                                                                                    dto -> budgetType == BudgetTypeEnum.BUDGET.getValue()
                                                                                            ? dto.expenditureAmount : dto.revisedExpenditureAmount
                                                                            )
                                                                            .sum();
                                                                %>
                                                                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                                                        Language,
                                                                        String.format("%.3f", sumByCode)
                                                                )%>
                                                            </td>
                                                        </tr>
                                                <%}%> <%--end for economic code--%>
                                        <%}%> <%--end for economic group--%>
                                        <tr>
                                            <td colspan="2" style="font-weight: bold" class="text-right">
                                                <%=LM.getText(LC.BUDGET_TOTAL, loginDTO)%> - <%=model.description%>
                                            </td>
                                            <td>
                                                <%
                                                    Double sumByMapping = budgetDTOsByMapping.stream()
                                                            .mapToDouble(
                                                                    dto -> budgetType == BudgetTypeEnum.BUDGET.getValue()
                                                                            ? dto.expenditureAmount : dto.revisedExpenditureAmount
                                                            )
                                                            .sum();
                                                %>
                                                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                                        Language,
                                                        String.format("%.3f", sumByMapping)
                                                )%>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                <%}%>

                <%--Submit Button On Condition--%>
                <%
                    if(isAbleToSubmitExpenditure){
                %>

                <div id = 'button-div' class="row my-5">
                    <div class="col-12 text-right">
                        <button onclick="submitBudget();" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button">
                            <%=UtilCharacter.getDataByLanguage(Language, "দাখিল", "Submit")%>
                        </button>

                    </div>
                </div>

                <% } %>
            </div>
        </div>
    </div>
</div>

<script>
    function submitBudget(){
        const warning = '<%=isLanguageEnglish ? "After submission, new submission will be closed." : "দাখিলের পর জমাদান বন্ধ হয়ে যাবে।"%>';
        const confirmation = '<%=isLanguageEnglish ? "Are you sure?" : "আপনি কি নিশ্চিত?"%>';

        messageDialog(
            warning, confirmation, 'warning', true,
            '<%=StringUtils.getYesNo(Language,true)%>','<%=StringUtils.getYesNo(Language,false)%>',
            confirmSubmit, () => {}
        );
    }

    async function confirmSubmit(){
        try {
            const url = 'Expenditure_submissionServlet?actionType=submitAllExpenditure';
            const searchParamData = {
                budgetSelectionInfoId: <%=selectionId%>,
                budgetTypeCat: <%=budgetType%>
            }

            const response = await fetch(url,{
                method: 'post',
                body: new URLSearchParams(searchParamData)
            });

            const json = await response.json();

            if (!json.success) throw new Error(JSON.stringify(json));

            messageDialog(
                '<%=isLanguageEnglish ? "Successfully submitted." : "সফলভাবে দাখিল হয়েছে"%>', '',
                'success', false,
                '<%=LM.getText(LC.BUDGET_OKAY, loginDTO)%>','',
                () => {}, () => {}
            );
            $("#button-div").hide();
        }catch (error){
            console.error(error);
            messageDialog(
                '<%=isLanguageEnglish ? "Submission Failed!" : "দাখিল ব্যর্থ হয়েছে!"%>', '',
                'error', false,
                '<%=LM.getText(LC.BUDGET_OKAY, loginDTO)%>','',
                () => {}, () => {}
            );
        }
    }

    function printDiv(divName) {
        let button = $("#button-div");
        if(button.length){
            button.hide();
        }
        let printContents = document.getElementById(divName).innerHTML;
        let originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

        if(button.length){
            $("#button-div").show();
        }
    }
</script>