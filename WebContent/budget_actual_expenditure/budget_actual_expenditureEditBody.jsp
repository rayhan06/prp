<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="budget.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.BUDGET_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String formTitle = LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE, loginDTO);

    String context = request.getContextPath() + "/";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
%>

<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .template-row {
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Expenditure_submissionServlet?actionType=insertExpenditureAmount"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row">
                        <div class="offset-3 col-6 text-center">
                            <img width="15%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>
                
                <div class="mt-5">

                    <input type="hidden" name="budgetModels" id="budgetModels">

                    <div class="row new-contents">
                        <div class="col-md-6 col-lg-3 my-3">
                            <label class="h5" for="budgetInstitutionalGroup">
                                <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                            </label>
                            <select id="budgetInstitutionalGroup" class='form-control rounded w-100 shadow-sm'
                                    onchange="institutionalGroupChanged(this);">
                                <%=Budget_institutional_groupRepository.getInstance().buildOptions(Language, 0L,false)%>
                            </select>
                        </div>

                        <div class="col-md-6 col-lg-3 my-3">
                            <label class="h5" for="budgetCat">
                                <%=LM.getText(LC.BUDGET_BUDGET_TYPE, loginDTO)%>
                            </label>
                            <select id="budgetCat" class='form-control rounded w-100 shadow-sm' onchange="budgetCatChanged(this);">
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>

                        <div class="col-md-6 col-lg-3 my-3">
                            <label class="h5" for="budgetOffice">
                                <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                            </label>
                            <select id="budgetOffice" class='form-control rounded shadow-sm w-100' onchange="budgetOfficeChanged(this);">
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>

                        <div class="col-md-6 col-lg-3 my-3">
                            <label class="h5" for="budgetOperation">
                                <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                            </label>
                            <select id="budgetOperation" class='form-control rounded w-100 shadow-sm' onchange="budgetOperationIdChanged(this);">
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>

                        <div class="col-md-6 col-lg-3 my-3">
                            <label class="h5" for="budgetSelectionInfoId">
                                <%=LM.getText(LC.BUDGET_ECONOMIC_YEAR, loginDTO)%>
                            </label>
                            <select id="budgetSelectionInfoId" name="budgetSelectionInfoId" class='form-control rounded w-100 shadow-sm'
                                    onchange="budgetSelectionChanged(this);">
                            </select>
                        </div>

                        <div class="col-md-6 col-lg-3 my-3">
                            <label class="h5" for="budgetTypeCat">
                                <%=LM.getText(LC.BUDGET_PASSWORD_ADD_SELECT_A_BUDGET_TYPE, loginDTO)%>
                            </label>
                            <select id="budgetTypeCat" name="budgetTypeCat" class='form-control rounded w-100 shadow-sm' onchange="budgetTypeCatChanged(this)">
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>

                        <div class="col-md-6 col-lg-3 my-3">
                            <label class="h5" for="economicGroup">
                                <%=LM.getText(LC.BUDGET_ECONOMIC_GROUP, loginDTO)%>
                            </label>
                            <select id="economicGroup" class='form-control rounded w-100 shadow-sm'
                                    onchange="economicGroupChanged(this);">
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>

                        <div class="col-md-6 col-lg-3 my-3">
                            <label class="h5" for="economicCodes">
                                <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                            </label>
                            <select id="economicCodes" class='form-control rounded w-100 shadow-sm'
                                    onchange="economicCodesChanged(this);">
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="mt-5">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap" id="budget-view-table">
                            <thead>
                            <tr>
                                <th class="row-data-code">
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </th>
                                <th class="row-data-name">
                                    <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                </th>
                                <th class="row-data-budget" id="th-actual-expenditure">
                                    <%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>

                            <tbody></tbody>

                            <tr class="template-row">
                                <td class="row-data-code"></td>
                                <td class="row-data-name"></td>
                                <td class="row-data-expenditure"></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 mt-3 text-right">
                        <button class="btn btn-sm shadow text-white btn-border-radius border-0"
                                type="button"
                                style="background: #4db2d1;"
                                id="previewBtn">
                            <%=LM.getText(LC.BUDGET_EXPENDITURE_PREVIEW, loginDTO)%>
                        </button>

                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn mx-2 btn-border-radius" type="button">
                            <%=LM.getText(LC.BUDGET_ADD_BUDGET_CANCEL_BUTTON, loginDTO)%>
                        </button>

                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius" type="submit">
                            <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp"%>

<script type="text/javascript">
    const budgetModelMap = new Map();
    let isAbleToSubmit = false;
    const REVISED_BUDGET = '<%=BudgetTypeEnum.REVISED_BUDGET.getValue()%>';
    const BUDGET = '<%=BudgetTypeEnum.BUDGET.getValue()%>';
    const submitBtn = $('#submit-btn');

    function PreprocessBeforeSubmit() {
        if(!isAbleToSubmit) return;

        const budgetModels = Array.from(budgetModelMap.values());
        const budgetTypeCat = document.getElementById('budgetTypeCat').value;

        for (let budgetModel of budgetModels) {
            if(budgetTypeCat === BUDGET){
                budgetModel.expenditureAmount = document.getElementById('budget_' + budgetModel.id).value;
            }
            else if(budgetTypeCat === REVISED_BUDGET){
                budgetModel.revisedExpenditureAmount = document.getElementById('revised_budget_' + budgetModel.id).value;
            }
            budgetModelMap.set(Number(budgetModel.id), budgetModel);
        }
        $('#budgetModels').val(JSON.stringify(Array.from(budgetModelMap.values())));
    }

    function clearSelects(startIndex) {
        const selectIds = [
            "budgetCat", "budgetOffice", "budgetOperation",
            "budgetSelectionInfoId", "economicGroup", "economicCodes"
        ];

        for (let i = startIndex; i < selectIds.length; i++) {
            const selectElement = document.getElementById(selectIds[i]);
            selectElement.innerHTML = '';
        }
    }

    function addDataInTableForExpenditure(tableId, jsonData, budgetType, isAbleToSubmit) {
        const table = document.getElementById(tableId);

        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.id = jsonData.economicSubCode;
        templateRow.classList.remove('template-row');

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-code').innerText = jsonData.code;
        templateRow.querySelector('td.row-data-name').innerText = jsonData.name;

        const expenditureTd = templateRow.querySelector('td.row-data-expenditure');

        let inputIdPrefix, value;
        if(budgetType === REVISED_BUDGET){
            inputIdPrefix = 'revised_budget_';
            value = jsonData.revisedExpenditureAmount;
        }else{
            inputIdPrefix = 'budget_';
            value = jsonData.expenditureAmount;
        }

        if (isAbleToSubmit) {
            expenditureTd.append(
                getBudgetInputElement(
                    inputIdPrefix + jsonData.id,
                    value,
                    sumThisColumn
                )
            );
        } else {
            expenditureTd.innerText = value;
        }

        const tableBody = table.querySelector('tbody');
        tableBody.append(templateRow);
    }

    function clearTable() {
        budgetModelMap.clear();
        const noDataText= '<%=isLanguageEnglish? "No data found!" : "কোনো তথ্য পাওয়া যায় নি!"%>';

        const table = document.getElementById('budget-view-table');
        table.querySelector('tbody').innerHTML = '<tr><td colspan="100%" class="text-center">' + noDataText + '</td></tr>';
        table.querySelectorAll('tfoot').forEach(tfoot => tfoot.remove());
    }

    function budgetOperationIdChanged(selectElement){
        const selectedOperationId = selectElement.value;
        if(selectedOperationId === '') return;
        document.getElementById('budgetSelectionInfoId').innerHTML =  "<%=BudgetSelectionInfoRepository.getInstance().buildEconomicYears(Language, 0L)%>";
    }

    async function budgetTypeCatChanged(selectElement){
        const selectedBudgetCat = selectElement.value;
        if(selectedBudgetCat == REVISED_BUDGET){
            document.getElementById('th-actual-expenditure').innerHTML = '<%=LM.getText(LC.BUDGET_ADD_REVISEDEXPENDITUREAMOUNT, loginDTO)%>'
        }
        else{
            document.getElementById('th-actual-expenditure').innerHTML = '<%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE, loginDTO)%>'
        }
        clearSelects(4);
        if (selectedBudgetCat === '') return;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        if (selectedSelectionCat === '') return;
        const selectedOperationGroup = document.getElementById('budgetOperation').value;
        const selectedBudgetOffice = document.getElementById('budgetOffice').value;

        const url = 'BudgetServlet?actionType=buildEconomicGroups&budgetSelectionInfoId='
            + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup;

        const response = await fetch(url);
        document.getElementById('economicGroup').innerHTML = await response.text();
    }

    async function budgetSelectionChanged(selectElement) {
        clearSelects(4);
        const selectedSelectionCat = selectElement.value;
        if (selectedSelectionCat === '') return;
        const url = 'BudgetServlet?actionType=buildBudgetType&budgetSelectionInfoId='
            + selectedSelectionCat;
        const response = await fetch(url);
        document.getElementById('budgetTypeCat').innerHTML = await response.text();
    }

    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroup = selectElement.value;
        clearSelects(5);
        if (selectedEconomicGroup === '') return;

        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperation').value;
        const selectedBudgetOffice = document.getElementById('budgetOffice').value;

        const url = 'BudgetServlet?actionType=buildEconomicCodes&budgetSelectionInfoId='
            + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
            + '&economicGroupId=' + selectedEconomicGroup;

        const response = await fetch(url);
        document.getElementById('economicCodes').innerHTML = await response.text();
    }

    async function checkBudgetExist(selectElement){
        const selectedEconomicCode = selectElement.value;
        clearTable();
        if (selectedEconomicCode === '') return;
        const budgetTypeCat = document.getElementById('budgetTypeCat').value;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;

        const url = 'BudgetServlet?actionType=checkBudgetExist&budgetSelectionInfoId='
            + selectedSelectionCat + '&budgetTypeCat=' + budgetTypeCat ;
        const response = await fetch(url);

        return await response.text();
    }

    async function economicCodesChanged(selectElement) {
        const selectedEconomicCode = selectElement.value;
        clearTable();
        if (selectedEconomicCode === '') return;

        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const budgetTypeCat = document.getElementById('budgetTypeCat').value;
        const selectedOperationId = document.getElementById('budgetOperation').value;
        const budgetOfficeId = document.getElementById('budgetOffice').value;
        const economicCodeId = document.getElementById('economicCodes').value;

        const url = 'BudgetServlet?actionType=getEconomicCodesForExpenditure&budgetSelectionInfoId='
            + selectedSelectionCat  + '&budgetOperationId=' + selectedOperationId
            + '&budgetTypeCat=' + budgetTypeCat + '&budgetOfficeId=' + budgetOfficeId
            + '&economicCodeId=' + economicCodeId;

        const response = await fetch(url);
        const jsonData = await response.json();
        const budgetModels = jsonData.budgetModels;
        isAbleToSubmit = jsonData.isAbleToSubmit;

        if(isAbleToSubmit) submitBtn.show();
        else submitBtn.hide();

        document.getElementById('budget-view-table').querySelector('tbody').innerHTML = '';
        if (Array.isArray(budgetModels)) {
            for(const budgetModel of budgetModels){
                addDataInTableForExpenditure('budget-view-table', budgetModel, budgetTypeCat, isAbleToSubmit);
                budgetModelMap.set(
                    Number(budgetModel.id),
                    budgetModel
                );
            }
            // columns [EconomicSubCode Description Expenditure]
            const colIndicesToSum = [2];
            const totalTitleColSpan = 2;
            const totalTitle = '<%=LM.getText(LC.BUDGET_SUBTOTAL,loginDTO)%>';
            setupTotalRow('budget-view-table', totalTitle, totalTitleColSpan, colIndicesToSum);
        }
    }

    function showErrorToastMessage(){
        $('#toast_message').css('background-color', '#ff6063');
        showToastSticky(
            '<%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE_EXPENDITURE_SAVING_FAILED,"Bangla")%>',
            '<%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE_EXPENDITURE_SAVING_FAILED,"English")%>'
        );
    }

    $(document).ready(function () {
        clearTable();
        submitBtn.hide();

        let previewBtn = document.getElementById('previewBtn');
        previewBtn.addEventListener('click', function(event) {
            let selectionInfoId = document.getElementById('budgetSelectionInfoId');
            let budgetTypeCat = document.getElementById('budgetTypeCat');
            if(selectionInfoId.value && budgetTypeCat.value){
                window.location.href='Expenditure_submissionServlet?actionType=budget_actual_expenditure_preview&selectionInfoId='+selectionInfoId.value+'&budgetType='+budgetTypeCat.value;
            }
            else{
                toastr.error('<%=LM.getText(LC.BUDGET_PLEASE_SELECT_BUDGET_TYPE_AND_ECONOMIC_YEAR, loginDTO)%>');
            }
        });

        $('#cancel-btn').click(() => {
            $('#budgetInstitutionalGroup').val('');
            $('#budgetTypeCat').val('');
            clearSelects(0);
            clearTable();
        });

        $("#bigform").submit(function(event) {
            event.preventDefault();
            PreprocessBeforeSubmit();
            let formData = $("#bigform").serialize();
            $.ajax({
                type: "POST",
                url: "Expenditure_submissionServlet?actionType=insertExpenditureAmount",
                data: formData,
                success: function(data){
                    const obj = JSON.parse(data);
                    if(obj.success){
                        $('#toast_message').css('background-color', '#04c73c');
                        showToast(
                            '<%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE_EXPENDITURE_SAVED,"Bangla")%>',
                            '<%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE_EXPENDITURE_SAVED,"English")%>'
                        );
                    }
                    else showErrorToastMessage();
                },
                error: showErrorToastMessage
            });
        });
    });

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearSelects(0);
        if (selectedInstitutionalGroupId === '') return;

        const url = 'Budget_mappingServlet?actionType=getBudgetCatList&budget_instituitional_group_id='
            + selectedInstitutionalGroupId;

        const response = await fetch(url);
        document.getElementById('budgetCat').innerHTML = await response.text();
    }

    async function budgetCatChanged(selectElement) {
        const selectedBudgetCat = selectElement.value;
        clearSelects(1);
        if (selectedBudgetCat === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&budget_instituitional_group_id='
            + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat;
        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
    }

    async function budgetOfficeChanged(selectElement) {
        const selectedBudgetOffice = selectElement.value;
        clearSelects(2);
        if (selectedBudgetOffice === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;
        const selectedBudgetCat = document.getElementById('budgetCat').value;

        const url = 'Budget_mappingServlet?actionType=getOperationCodeList&budget_instituitional_group_id='
            + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat
            + '&budget_office_id=' + selectedBudgetOffice;
        const response = await fetch(url);
        document.getElementById('budgetOperation').innerHTML = await response.text();
    }

    function getBudgetInputElement(id, value, onKeyUp) {
        const inputElement = document.createElement('input');
        inputElement.classList.add('form-control');
        inputElement.type = 'text';
        inputElement.id = id;
        inputElement.value = value;
        inputElement.onkeydown = keyDownEvent;
        if (onKeyUp) inputElement.onkeyup = onKeyUp;
        return inputElement;
    }

    function keyDownEvent(e) {
        return inputValidationForFloatValue(e, $(this), 3, null) === true;
    }
</script>






