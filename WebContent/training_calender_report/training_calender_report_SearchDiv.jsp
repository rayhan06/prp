<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="pb.*" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String context = "../../.." + request.getContextPath() + "/assets/";
%>

<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>

<div class="row">
    <div class="col-12 px-0">
        <div class="row mx-4">
            <div id="criteriaSelectionId" class="search-criteria-div col-md-12">
                <div class="form-group row">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button type="button"
                                class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                                data-toggle="modal" data-target="#select_criteria_div" id="select_criteria_btn"
                                onclick="beforeOpenCriteriaModal()">
                            <%=isLangEng ? "Criteria Select" : "ক্রাইটেরিয়া বাছাই"%>
                        </button>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-6 col-form-label text-md-right" for="onlyMpOffice_checkbox">
                        <%=isLangEng ? "MPs only" : "কেবলমাত্র সংসদ সদস্যগণ"%>
                    </label>
                    <div class="col-md-6 col-2">
                        <input type='checkbox' class='form-control-sm mt-1' name='onlyMpOffice'
                               id='onlyMpOffice_checkbox'
                               onchange="onlyMpOfficeChanged(this)" value='false'>
                    </div>
                </div>
            </div>

            <div id="start_date_from_div" class="search-criteria-div col-md-6 dateRange" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_STARTDATE_1, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="start_date_from-js"></jsp:param>
                            <jsp:param name="LANGUAGE"
                                       value="<%=Language%>"></jsp:param>
                            <jsp:param name="END_YEAR"
                                       value="<%=year%>"></jsp:param>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='start_date_from'
                               name='start_date_from' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>
            <div id="start_date_to_div" class="search-criteria-div col-md-6 dateRange" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_STARTDATE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="start_date_to-js"></jsp:param>
                            <jsp:param name="LANGUAGE"
                                       value="<%=Language%>"></jsp:param>
                            <jsp:param name="END_YEAR"
                                       value="<%=year + 50%>"></jsp:param>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='start_date_to'
                               name='start_date_to' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>
            <div id="end_date_from_div" class="search-criteria-div col-md-6 dateRange" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_ENDDATE_3, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="end_date_from-js"></jsp:param>
                            <jsp:param name="LANGUAGE"
                                       value="<%=Language%>"></jsp:param>
                            <jsp:param name="END_YEAR"
                                       value="<%=year%>"></jsp:param>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='end_date_from'
                               name='end_date_from' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>
            <div id="end_date_to_div" class="search-criteria-div col-md-6 dateRange" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_ENDDATE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="end_date_to-js"></jsp:param>
                            <jsp:param name="LANGUAGE"
                                       value="<%=Language%>"></jsp:param>
                            <jsp:param name="END_YEAR"
                                       value="<%=year + 50%>"></jsp:param>
                        </jsp:include>
                        <input type='hidden' class='form-control' id='end_date_to'
                               name='end_date_to' value=''
                               tag='pb_html'/>
                    </div>
                </div>
            </div>
            <div class="search-criteria-div col-md-6 nameClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_NAMEEN, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng"
                               style="width: 100%"
                               placeholder="<%=isLangEng ? "Enter English Name" : "ইংরেজিতে নাম লিখুন"%>" value="">
                    </div>
                </div>
            </div>

            <div id="name_bng_div" class="search-criteria-div col-md-6 mpNameClass nameClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_NAMEBN, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng"
                               style="width: 100%"
                               placeholder="<%=isLangEng ? "Enter Bangla Name" : "বাংলাতে নাম লিখুন"%>" value="">
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 typeClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_TRAININGTYPECAT, loginDTO)%>
                    </label>
                    <div class="col-sm-9">
                        <select class='form-control' name='trainingTypeCat' id='trainingTypeCat'>
                            <%=CatRepository.getInstance().buildOptions("training_type", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>
            <div class="search-criteria-div col-md-6 catClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_TRAININGMODECAT, loginDTO)%>
                    </label>
                    <div class="col-sm-9">
                        <select class='form-control' name='trainingModeCat' id='trainingModeCat'>
                            <%=CatRepository.getInstance().buildOptions("training_mode", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 officeClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                id="office_units_id_modal_button"
                                onclick="officeModalButtonClicked();">
                            <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                        </button>
                        <div class="input-group" id="office_units_id_div" style="display: none">
                            <button type="button"
                                    class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                    id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                            </button>
                            <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 officeClass" style="display:none">
                <div class="form-group row">
                    <label class="col-sm-6 col-xl-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                        <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস"%>
                    </label>
                    <div class="col-2" id='onlySelectedOffice'>
                        <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                               id='onlySelectedOffice_checkbox'
                               onchange="this.value = this.checked;" value='false'>
                    </div>
                    <div class="col-4"></div>
                    <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
                        <div class="form-group row">
                            <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                                <div class="tag template-tag">
                                    <span class="tag-name"></span>
                                    <i class="fas fa-times-circle tag-remove-btn"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="search-criteria-div col-md-6 usernameClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="username">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <input class="englishOnly form-control" type="text" name="username" id="username"
                               style="width: 100%"
                               placeholder="<%=isLangEng? "Type in English":"ইংরেজিতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 employeeNameClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="employeeNameEn">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_NAME_EMP_MP_ENG, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <input class="englishOnly form-control" type="text" name="employeeNameEn" id="employeeNameEn"
                               style="width: 100%"
                               placeholder="<%=isLangEng? "Enter Employee/MP Name in English":"কর্মকর্তা/কর্মচারী/এমপির নাম ইংরেজিতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 employeeNameClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="employeeNameBn">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_NAME_EMP_MP_BNG, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <input class="noEnglish form-control" type="text" name="employeeNameBn" id="employeeNameBn"
                               style="width: 100%"
                               placeholder="<%=isLangEng? "Enter Employee/MP Name in Bangla":"কর্মকর্তা/কর্মচারী/এমপির নাম বাংলাতে লিখুন"%>"
                               value="">
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 trainerCatClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right" for="trainerCat">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_TRAINERCAT, loginDTO)%>
                    </label>
                    <div class="col-sm-9">
                        <select class='form-control' name='trainerCat' id='trainerCat'>
                            <%=CatRepository.getInstance().buildOptions("trainer", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 privateClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right" for="isPrivate">
                        <%=LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_PRIVATE_TRAINING, loginDTO)%>
                    </label>
                    <div class="col-sm-9">
                        <select class='form-control' name='isPrivate' id='isPrivate'>
                            <%=CatRepository.getInstance().buildOptions("yes_no", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 arrangedClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right" for="arrangedBy">
                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_ARRANGEDBY, loginDTO)%>
                    </label>
                    <div class="col-sm-9">
                        <select class='form-control' name='arrangedBy' id='arrangedBy'>
                            <%=CatRepository.getInstance().buildOptions("training_arrange_by", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6 countryClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right" for="country">
                        <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO)%>
                    </label>
                    <div class="col-sm-9">
                        <select class='form-control' name='country' id='country'>
                            <%=GeoCountryRepository.getInstance().buildOptions(Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="gender_div" class="search-criteria-div col-md-6 genderClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="genderCat">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='genderCat' id='genderCat'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("gender", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="employeeClass_div" class="search-criteria-div col-md-6 employeeClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="employeeClassCat">
                        <%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='employeeClassCat' id='employeeClassCat'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("employee_class", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="employmentCat_div" class="search-criteria-div col-md-6 employmentCatClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="employmentCat">
                        <%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='employmentCat' id='employmentCat'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("employment", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div id="emp_officer_div" class="search-criteria-div col-md-6 empOfficerClass" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="empOfficerCat">
                        <%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='empOfficerCat' id='empOfficerCat'
                                style="width: 100%">
                            <%=CatRepository.getInstance().buildOptions("emp_officer", Language, null)%>
                        </select>
                    </div>
                </div>
            </div>


            <div id="age_div" class="search-criteria-div col-md-6 ageClass" style="display:none">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>
                    </label>
                    <div class="col-6 col-md-5">
                        <input class='form-control' type="text" name='age_start' id='age_start'
                               placeholder='<%=LM.getText(LC.HR_REPORT_FROM, loginDTO)%>' style="width: 100%">
                    </div>
                    <div class="col-6 col-md-4">
                        <input class='form-control' type="text" name='age_end' id='age_end'
                               placeholder='<%=LM.getText(LC.HR_REPORT_TO, loginDTO)%>' style="width: 100%">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="<%=context%>scripts/input_validation.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });

    function init() {
        dateTimeInit($("#Language").val());
        select2SingleSelector('#trainingTypeCat', '<%=Language%>');
        select2SingleSelector('#trainerCat', '<%=Language%>');
        select2SingleSelector('#trainingModeCat', '<%=Language%>');
        select2SingleSelector('#isPrivate', '<%=Language%>');
        select2SingleSelector('#country', '<%=Language%>');
        select2SingleSelector('#arrangedBy', '<%=Language%>');
        document.getElementById('age_start').onkeydown = keyDownEvent;
        document.getElementById('age_end').onkeydown = keyDownEvent;
        select2SingleSelector('#genderCat', '<%=Language%>');
        select2SingleSelector('#employeeClassCat', '<%=Language%>');
        select2SingleSelector('#employmentCat', '<%=Language%>');
        select2SingleSelector('#empOfficerCat', '<%=Language%>');

        criteriaArray = [
            {
                class: 'dateRange',
                title: '<%=LM.getText(LC.HM_DATE, loginDTO)%>',
                show: false,
                specialCategory: 'date',
                datejsIds: ['start_date_from-js', 'start_date_to-js', 'end_date_from-js', 'end_date_to-js']
            },
            {
                class: 'nameClass',
                title: '<%=LM.getText(LC.HM_NAME, loginDTO)%>',
                show: false
            },
            {
                class: 'typeClass',
                title: '<%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_TRAININGTYPECAT, loginDTO)%>',
                show: false
            },
            {
                class: 'catClass',
                title: '<%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_TRAININGMODECAT, loginDTO)%>',
                show: false
            },
            {
                class: 'officeClass',
                title: '<%=LM.getText(LC.HM_OFFICE, loginDTO)%>',
                show: false
            },
            {
                class: 'usernameClass',
                title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_SELECT_USER_ID, loginDTO)%>',
                show: false
            },
            {
                class: 'employeeNameClass',
                title: '<%=Language.equalsIgnoreCase("english")?"Employee Name":"কর্মকর্তার নাম"%>',
                show: false
            },
            {
                class: 'trainerCatClass',
                title: '<%=LM.getText(LC.TRAINING_CALENDER_REPORT_WHERE_TRAINERCAT, loginDTO)%>',
                show: false
            },

            {
                class: 'privateClass',
                title: '<%=LM.getText(LC.TRAINING_CALENDER_REPORT_SELECT_PRIVATE_TRAINING, loginDTO)%>',
                show: false
            },

            {
                class: 'arrangedClass',
                title: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ARRANGEDBY, loginDTO)%>',
                show: false
            },

            {
                class: 'countryClass',
                title: '<%=LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO)%>',
                show: false
            },
            {
                class: 'genderClass',
                title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT, loginDTO)%>',
                show: false
            },
            {class: 'employeeClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>',
                show: false
            },
            {
                class: 'employmentCatClass',
                title: '<%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>',
                show: false
            },
            {class: 'empOfficerClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>',
                show: false
            },
            {
                class: 'ageClass',
                title: '<%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>',
                show: false
            }
        ]
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInTags,
            isMultiSelect: true,
            keepLastSelectState: true
        }]
    ]);

    function keyDownEvent(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 200);
        return true == isvalid;
    }

    function officeModalButtonClicked() {
        console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function onlyMpOfficeChanged(elem) {
        const $officeUnitIds = $('#officeUnitIds_input');
        if(elem.checked) {
            $officeUnitIds.val('["<%=Office_unitsRepository.MP_OFFICE_UNIT_ID%>"]');
        } else {
            $officeUnitIds.val('');
        }
    }

    function PreprocessBeforeSubmiting() {
        $('#start_date_from').val(getDateTimestampById('start_date_from-js'));
        $('#start_date_to').val(getDateTimestampById('start_date_to-js'));
        $('#end_date_from').val(getDateTimestampById('end_date_from-js'));
        $('end_date_to').val(getDateTimestampById('end_date_to-js'));
        const $username = $('#username');
        $username.val(convertToEnglishNumber($username.val()));
    }
</script>