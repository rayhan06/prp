﻿<div class="kt-portlet__body" style="background-color: #FFFFFF">
    <div id="absent_status" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

    let absentReportFetchedData;
    let absentReceivedData = [];

    function showAbsentStatFromSummery( absentReportFetchedData ){

        absentReceivedData = [];
        absentReceivedData.push(['<%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>',
            '<%=LM.getText(LC.ATTENDANCE_DASHBOARD_ABSENT_PERCENT, loginDTO)%>',
            { role: "style" }]);
        for(let i = 0; i < absentReportFetchedData.data.length; i++){
            let rowData = absentReportFetchedData.data[i];
            if(rowData){
                <%if(language.equalsIgnoreCase("english")) { %>
                absentReceivedData.push([rowData.departmentNameEn, rowData.absentPercent, "red"]);
                <%} else { %>
                absentReceivedData.push([rowData.departmentNameBn, rowData.absentPercent, "red"]);
                <%} %>

            }
        }
        google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(drawChartAbsentStat);
    }


    function drawChartAbsentStat() {

        var data = google.visualization.arrayToDataTable(absentReceivedData);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            title: "<%=LM.getText(LC.ATTENDANCE_DASHBOARD_ABSENT_STATISTICS, loginDTO)%>",
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("absent_status"));
        chart.draw(view, options);
    }
</script>