﻿<div class="kt-portlet__body" style="background-color: #FFFFFF">
    <div id="late_in_status" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

    let lateReportFetchedData;
    let lateReceivedData = [];

    function showLateStatFromSummery( lateReportFetchedData ){

        lateReceivedData = [];
        lateReceivedData.push(['<%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>',
            '<%=LM.getText(LC.ATTENDANCE_DASHBOARD_LATE_IN_PERCENT, loginDTO)%>',
            { role: "style" }]);
        for(let i = 0; i < lateReportFetchedData.data.length; i++){
            let rowData = lateReportFetchedData.data[i];
            if(rowData){
                <%if(language.equalsIgnoreCase("english")) { %>
                lateReceivedData.push([rowData.departmentNameEn, rowData.latePercent, "gold"]);
                <%} else { %>
                lateReceivedData.push([rowData.departmentNameBn, rowData.latePercent, "gold"]);
                <%} %>

            }
        }

        google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(drawChartLateStat);
    }


    function drawChartLateStat() {
        var data = google.visualization.arrayToDataTable(lateReceivedData);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            title: "<%=LM.getText(LC.ATTENDANCE_DASHBOARD_LATE_IN_STATISTICS, loginDTO)%>",
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("late_in_status"));
        chart.draw(view, options);
    }
</script>