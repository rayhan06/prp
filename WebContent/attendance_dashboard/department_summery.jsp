﻿<%@ page import="mps_attendance.Mps_attendanceDAO" %>
<%@ page import="attendance_dashboard.AttendanceDashboardConstant" %>
<div class="kt-portlet__body" style="background-color: #FFFFFF">
    <div id="summery_status" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

    let summeryFetchedData;
    let summeryReceivedData = [];

    $(document).ready(()=>{
        showSummeryStat(null,null, null);
    })

    function showSummeryStat( month, year, officeUnitId ){

        let url = "AttendanceDashboardServlet?actionType=departmentWiseReport&month=" + month + "&year=" + year+"&reportType=<%=AttendanceDashboardConstant.ALL_DEPARTMENT_SUMMERY%>&officeUnitId="+officeUnitId;

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    summeryFetchedData = fetchedData;
                    summeryReceivedData = [];
                    summeryReceivedData.push(['<%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>',
                        '<%=LM.getText(LC.ATTENDANCE_DASHBOARD_LATE_IN_PERCENT, loginDTO)%>',
                        '<%=LM.getText(LC.ATTENDANCE_DASHBOARD_EARLY_OUT_PERCENT, loginDTO)%>',
                        '<%=LM.getText(LC.ATTENDANCE_DASHBOARD_ABSENT_PERCENT, loginDTO)%>'
                        ]);
                    for(let i = 0; i < summeryFetchedData.data.length; i++){
                        let rowData = summeryFetchedData.data[i];
                        if(rowData){
                            <%if(language.equalsIgnoreCase("english")) { %>
                           // summeryReceivedData.push([rowData.departmentNameEn, 10,15,20]);
                            summeryReceivedData.push([rowData.departmentNameEn, rowData.latePercent,rowData.earlyOutPercent,rowData.absentPercent]);
                            <%} else { %>
                          //  summeryReceivedData.push([rowData.departmentNameEn, 10,15,20]);
                            summeryReceivedData.push([rowData.departmentNameBn, rowData.latePercent,rowData.earlyOutPercent,rowData.absentPercent]);
                            <%} %>

                        }
                    }

                    google.charts.load("current", {packages:['corechart']});
                    google.charts.setOnLoadCallback(drawChartSummeryStat);

                    showAbsentStatFromSummery( summeryFetchedData );
                    showEarlyOutStatFromSummery (summeryFetchedData );
                    showLateStatFromSummery( summeryFetchedData );
                },
                error: function (error) {
                    console.log(error);
                }
            });

    }


    function drawChartSummeryStat() {
        var data = google.visualization.arrayToDataTable(summeryReceivedData);
        var view = new google.visualization.DataView(data);

        var options = {
            title: "<%=LM.getText(LC.ATTENDANCE_DASHBOARD_ORGANIZATION_WISE_ATTENDANCE_SUMMERY, loginDTO)%>",
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("summery_status"));
        chart.draw(view, options);
    }
</script>