﻿<%@ page import="mps_attendance.Mps_attendanceDAO" %>
<%@ page import="attendance_dashboard.AttendanceDashboardConstant" %>
<%@ page import="attendance_dashboard.AttendanceDashboardServlet" %>
<div class="kt-portlet__body" style="background-color: #FFFFFF">
<%--    <div class="row">
        <div class="col-lg-6">
            <select onchange="onSelectMonthYear()" class='form-control' name='officeUnit' id='officeUnitId' tag='pb_html'>
                <%=new AttendanceDashboardServlet().buildOfficeUnits(language)%>
            </select>
        </div>
        <div class="col-lg-3">
            <select onchange="onSelectMonthYear()" class='form-control' name='month' id='monthId' tag='pb_html'>
                <%= new Mps_attendanceDAO().buildOptionMonth(language)%>
            </select>
        </div>
        <div class="col-lg-3">
            <select onchange="onSelectMonthYear()" class='form-control' name='year' id='yearId' tag='pb_html'>
                <%= new Mps_attendanceDAO().buildOptionYear(language)%>
            </select>
        </div>
    </div>--%>


    <div id="all_status" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

    let allReportFetchedData;
    let allReceivedData = [];

    $(document).ready(()=>{
        let officeUnitId = $("#officeUnitId").val();
        showAllStat(null,null,officeUnitId);
    })

 /*   function onSelectMonthYear() {
        let month = $("#monthId").val();
        let year = $("#yearId").val();
        let officeUnitId = $("#officeUnitId").val();
        if(month && year) {
            showAllStat(month,year,officeUnitId);
        }
    }*/
    function showAllStat( month, year, officeUnitId ){

        let url = "AttendanceDashboardServlet?actionType=departmentWiseReport&month=" + month + "&year=" + year+"&reportType=<%=AttendanceDashboardConstant.DEPARTMENT_ALL_REPORT%>&officeUnitId="+officeUnitId;

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                allReportFetchedData = fetchedData;
                allReceivedData = [];
                allReceivedData.push(['<%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>',
                    '',
                    { role: "style" }]);
                for(let i = 0; i < allReportFetchedData.data.length; i++){
                    let rowData = allReportFetchedData.data[i];
                    if(rowData){
                        allReceivedData.push(['<%=LM.getText(LC.ATTENDANCE_DASHBOARD_LATE_IN, loginDTO)%>', rowData.latePercent, "gold"]);
                        allReceivedData.push(['<%=LM.getText(LC.ATTENDANCE_DASHBOARD_EARLY_OUT, loginDTO)%>', rowData.earlyOutPercent, "gold"]);
                        allReceivedData.push(['<%=LM.getText(LC.ATTENDANCE_DASHBOARD_ABSENT, loginDTO)%>', rowData.absentPercent, "red"]);
                    }
                }

                // console.log(receivedData)

                google.charts.load("current", {packages:['corechart']});
                google.charts.setOnLoadCallback(drawChartAllStat);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }


    function drawChartAllStat() {
        var data = google.visualization.arrayToDataTable(allReceivedData);
        /*var data = google.visualization.arrayToDataTable([
            ["Department", "Late rate", { role: "style" } ],
            ["Finance and public relation", 8.94, "gold"],
            ["Administrative support", 10.49, "gold"],
            ["Affairs and security", 19.30, "gold"],
            ["Human resource", 21.45, "gold"],
            ["Broadcasting and IT", 12.14, "gold"],
            ["Legislative support", 9.25, "gold"],
            ["Committee support", 2.35, "gold"]
        ]);*/

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            title: "<%=LM.getText(LC.ATTENDANCE_DASHBOARD_ORGANIZATION_ATTENDANCE_SUMMERY, loginDTO)%>",
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("all_status"));
        chart.draw(view, options);
    }
</script>