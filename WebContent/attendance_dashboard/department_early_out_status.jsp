﻿<div class="kt-portlet__body" style="background-color: #FFFFFF">
    <div id="early_out_status" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

    let earlyOutReportFetchedData;
    let earlyOutReceivedData = [];

    function showEarlyOutStatFromSummery( earlyOutReportFetchedData ){

        earlyOutReceivedData = [];
        earlyOutReceivedData.push(['<%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>',
            '<%=LM.getText(LC.ATTENDANCE_DASHBOARD_EARLY_OUT_PERCENT, loginDTO)%>',
            { role: "style" }]);
        for(let i = 0; i < earlyOutReportFetchedData.data.length; i++){
            let rowData = earlyOutReportFetchedData.data[i];
            if(rowData){
                <%if(language.equalsIgnoreCase("english")) { %>
                earlyOutReceivedData.push([rowData.departmentNameEn, rowData.earlyOutPercent, "gold"]);
                <%} else { %>
                earlyOutReceivedData.push([rowData.departmentNameBn, rowData.earlyOutPercent, "gold"]);
                <%} %>

            }
        }
        google.charts.load("current", {packages:['corechart']});
        google.charts.setOnLoadCallback(drawChartEarlyOutStat);
    }

    function drawChartEarlyOutStat() {
        var data = google.visualization.arrayToDataTable(earlyOutReceivedData);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            title: "<%=LM.getText(LC.ATTENDANCE_DASHBOARD_EARLY_OUT_STATISTICS, loginDTO)%>",
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("early_out_status"));
        chart.draw(view, options);
    }
</script>