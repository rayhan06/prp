﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.COMMON_ATTENDANCE_DASHBOARD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body bg-light">
            <%--Office Search modal--%>
            <div class="">
                <div class="d-flex flex-column flex-md-row justify-content-md-between align-items-md-center mb-4">
                    <div class="d-flex">
                        <select
                                onchange="onSelectMonthYear()"
                                class='form-control shadow'
                                name='month'
                                id='monthId' tag='pb_html'
                        >
                            <%= new Mps_attendanceDAO().buildOptionMonth(language)%>
                        </select>
                        <select onchange="onSelectMonthYear()"
                                class='form-control shadow ml-3'
                                name='year'
                                id='yearId'
                                tag='pb_html'>
                            <%= new Mps_attendanceDAO().buildOptionYear(language)%>
                        </select>
                    </div>
                    <div class="mt-3 mt-md-0">
                        <input type="hidden" name='officeUnitId' id='office_units_id_input' value="1">
                        <button type="button" class="btn btn-secondary shadow btn-border-radius" disabled
                                id="office_units_id_text"></button>
                        <button type="button" class="btn btn-primary shadow btn-border-radius ml-2"
                                id="office_units_id_modal_button"
                                onclick="officeModalButtonClicked();">
                            <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="shadow mb-3 btn-border-radius"
                            >
                                <%@include file="employee_monthly_attendance_comparison.jsp" %>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="shadow mb-3 btn-border-radius"
                            >
                                <%@include file="department_all_status.jsp" %>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="shadow mb-3 btn-border-radius"
                            >
                                <%@include file="department_early_out_status.jsp" %>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="shadow mb-3 btn-border-radius"
                            >
                                <%@include file="department_absent_status.jsp" %>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="shadow mb-3 btn-border-radius"
                            >
                                <%@include file="department_late_in_status.jsp" %>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="shadow mb-3 btn-border-radius"
                            >
                                <%@include file="department_summery.jsp" %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script>
    $(() => {
        viewOfficeIdInInput({
            name: '<%=language.equalsIgnoreCase("English")? "SPEAKER" : "মাননীয় স্পীকারের কার্যালয়"%>',
            id: 1
        });
    });

    function onSelectMonthYear() {
        let month = $("#monthId").val();
        let year = $("#yearId").val();
        if (month && year) {
            showAllChart();
        } else if(!month && !year) {
            showAllChart();
        }
    }

    function showAllChart() {

        let officeUnitId = $("#office_units_id_input").val();
        let month = $("#monthId").val();
        let year = $("#yearId").val();

        showAllStat(month, year, officeUnitId);
        showSummeryStat(month, year, officeUnitId);
    }

    /*   function onSelectOffice() {
           let month = $("#monthId").val();
           let year = $("#yearId").val();
           if (month && year) {
               showAllStat(month, year);
           }
       }*/
    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        console.log(selectedOffice);
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
        showAllChart();
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }
</script>


