﻿
<div class="kt-portlet__body" style="background-color: #FFFFFF">
    <div id="barchart_material" style="height: 300px;"></div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    let employeeAttendanceData;
    let receivedData = [];

    $(document).ready(()=>{
        getMonthlyAttendanceData();
    })
    function getMonthlyAttendanceData(){
            let url = "AttendanceDashboardServlet?actionType=employeeMonthlyAttendance&language=<%=language%>";
        let wd = 'Working days';
        if('<%=language%>'=='Bangla') {
            wd = 'কর্মদিবস';
        }
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    employeeAttendanceData = fetchedData;
                    receivedData = [];

                    receivedData.push(['<%=LM.getText(LC.HM_MONTH, loginDTO)%>',
                        wd,
                        '<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT, loginDTO)%>',
                        ]);
                    for(let i = 0; i < employeeAttendanceData.data.length; i++){
                        let rowData = employeeAttendanceData.data[i];
                        if(rowData){
                            receivedData.push([rowData.month, rowData.workingDays, rowData.presentDays]);
                        }
                    }

                    google.charts.load("current", {packages:['corechart']});
                    google.charts.setOnLoadCallback(drawChartEmployeeMonthlyAttendance);
                },
                error: function (error) {
                    console.log(error);
                }
            });

    }
    function drawChartEmployeeMonthlyAttendance() {
        var data = google.visualization.arrayToDataTable(receivedData);

   /*         google.visualization.arrayToDataTable([
            ['Month', 'Working days', 'Present days'],
            [ 'April',  22, 17],
            [ 'March',  20, 19],
            [ 'February', 21, 21],
            [ 'January',  20, 18],
        ]);*/

/*
        var options = {
            chart: {
                title: 'Attendance report',

                // subtitle: 'Sales, Expenses, and Profit: 2014-2017',
            },
            bars: 'vertical' // Required for Material Bar Charts.
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));*/


        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" },
            2]);

        var options = {
            title: "<%=LM.getText(LC.ATTENDANCE_DASHBOARD_ATTENDANCE_STATISTICS, loginDTO)%>",
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.ColumnChart(document.getElementById("barchart_material"));
        chart.draw(view, options);
    }
</script>