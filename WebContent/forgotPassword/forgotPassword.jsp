<%@page import="java.util.Calendar"%><%@page import="language.LC"%><%@page import="language.LM"%><%@ page language="java" %>
<%
String loginURL = request.getRequestURL().toString();
//String context = request.getContextPath() + "/";
String context_folder = request.getContextPath();
String context = request.getContextPath() + "/";
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->    
<head>
<html:base />
<meta charset="utf-8" />
<title><%=LM.getText(LC.GLOBAL_PASSWORD_RECOVERY_TITLE) %></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />        
<link href="<%=context_folder%>/assets/css/google_font.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/uniform.default.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/styles.css" rel="stylesheet" type="text/css" />       
<link href="<%=context_folder%>/assets/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<%=context_folder%>/assets/css/plugins.min.css" rel="stylesheet" type="text/css" />        
<link href="<%=context_folder%>/assets/css/forgot_password.css" rel="stylesheet" type="text/css" />        

<style type="text/css">
.login{
background-color: #fff !important;
}
.login .copyright {
color: black !important;
}
.login .content h3{
color: black !important;
}
</style>        
</head>		
<body class=" login">    
 <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        
        <!-- BEGIN LOGO -->
         <div class="logo">
            <a href="<%=request.getContextPath()%>">
                <img  class="img-responsive login-page-logo" src="<%=request.getContextPath()%>/images/company-name.png" alt="" /> </a>
        </div>
        <!-- END LOGO -->
       
        <!-- BEGIN LOGIN -->
        <div class="content">
            
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<%=request.getContextPath()%>/VerificationServlet?actionType=check-user" method="post">
                
                <input type="hidden" name="method" value="sendOTP" />
                
                <h3 class="form-title font-purple">Recover Password</h3>
                
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter your username </span>
                </div>
                               
                
                <div class="from-group">
                	<html:errors property="loginFailure" />
					<input type="hidden" name="loginURL" value="<%=loginURL%>"/>
                </div>	
                
                <div class="form-group">
                <label>
                    <input type="radio" class="rdo1" name="verificationType" value="2" checked="checked"/> Email                    
                 </label>
                 <label>    
                    <input type="radio" class="rdo1" name="verificationType" value="1" /> Username
                    </label>
                </div>
                
                <div class="form-group">
                    <!-- <label class="control-label visible-ie8 visible-ie9">Username or Email</label> -->
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="" name="username" /> 
                </div>
                
                <div class="form-actions">
                    <button type="submit" class="btn green-jungle  btn-block uppercase">Submit</button>
                </div>
                
                 <div class="form-actions">
	                 <a href="<%=request.getContextPath() %>/" > Sign In </a> &nbsp; &nbsp; &nbsp;
                </div>
	             
            </form>
            <!-- END LOGIN FORM -->
        </div>
        
         <div class="copyright"> <%=Calendar.getInstance().get(Calendar.YEAR) %> @ BTCL Bangladesh Limited </div>
<!--[if lt IE 9]>
<script src="<%=context_folder%>/assetsglobal/plugins/respond.min.js"></script>
<script src="<%=context_folder%>/assetsglobal/plugins/excanvas.min.js"></script> 
<![endif]-->             
<script src="<%=context_folder%>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/scripts/app.min.js" type="text/javascript"></script>             
<script src="<%=context_folder%>/assets/pages/scripts/login.min.js" type="text/javascript"></script>   
</body>
</html>