<%@ page language="java"%><%@page import="language.LC"%><%@page import="language.LM"%>
<%
String loginURL = request.getRequestURL().toString();
String context = "../../.."  + request.getContextPath() + "/";

String context_folder = request.getContextPath();

if(request.getAttribute("emailVerified") == null)
{		
	response.sendRedirect(context);
}
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title><%=LM.getText(LC.GLOBAL_RESET_PASSWORD_TITLE) %> </title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="<%=context_folder%>/assets/css/google_font.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/uniform.default.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<%=context_folder%>/assets/css/forgot_password.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.login {
background-color: #fff !important;
}

.login .copyright {
color: black !important;
}

.login .content h3 {
color: black !important;
}
</style>
</head>
<body class=" login">
<div class="menu-toggler sidebar-toggler"></div>
<div class="logo">
<a href="<%=context_folder%>"> <img
src="<%=context_folder%>/images/company-name.png" alt="" />
</a>
</div>	
<div class="content">
<form action="VerificationServlet?actionType=reset-password" method="post">
<h3 class="form-title font-purple"><%=LM.getText(LC.GLOBAL_RESET_PASSWORD) %></h3>
<div class="note note-warning custom-alert">
<p><%=LM.getText(LC.RESET_PASSWORD_WARNING) %><%=LM.getText(LC.RESET_PASSWORD_WARNING2) %></p>
</div>
<input type="hidden" name="id" value="<%=request.getAttribute("id")%>" />
<input type="hidden" name="token" value="<%=request.getAttribute("token")%>" />
<%-- <jsp:include page='../common/flushActionStatus.jsp' /> --%>
<div class="form-group">
<label class="control-label"><%=LM.getText(LC.GLOBAL_NEW_PASSWORD) %> <span class="required"> * </span> </label>				
<input name="password" type="password" id="password" class="form-control" />
<div class="pwstrength_viewport_progress"></div>
</div>			
<div class="form-group">
<label class="control-label"><%=LM.getText(LC.GLOBAL_CONFIRM_NEW_PASSWORD) %> <span class="required"> * </span></label>				
<input name="rePassword" type="password" class="form-control" />
</div>
<div class="margin-top-10">
<input name="type" value="password" type="hidden">
<input type="reset" class="btn btn-danger" value="Reset"> 
<input type="submit" class="btn green-jungle uppercase" value=" Change Password" />
</div>			
</form>
</div>
<div class="copyright">2019 @ Reve Systems Limited</div>
<!--[if lt IE 9]>
<script src="<%=context_folder%>/assetsglobal/plugins/respond.min.js"></script>
<script src="<%=context_folder%>/assetsglobal/plugins/excanvas.min.js"></script> 
<![endif]-->	
<script src="<%=context_folder%>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<%=context_folder%>/assets/global/scripts/app.min.js" type="text/javascript"></script>	
<script src="<%=context_folder%>/assets/pages/scripts/login.min.js" type="text/javascript"></script>	
<script src="<%=context_folder %>/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"/>
<script src="<%=context_folder %>/assets/pages/scripts/profile.min.js" type="text/javascript"/>
<script src="<%=context_folder %>/assets/global/plugins/pwstrength.js" type="text/javascript"/>	
<script type="text/javascript">
$(document).ready(function() {
	 "use strict";
var options = {};

options.ui = {
   	container: "#pwd-container",
   	showVerdictsInsideProgressBar: true,
   	showStatus: true,
   	viewports: {
   	    progress: ".pwstrength_viewport_progress"
   	},
   	progressBarExtraCssClasses: "progress-bar-striped active",
   	showPopover: true,
	showErrors: true,
	showProgressBar: true
};

options.rules = {
	activated: {
	   wordTwoCharacterClasses: true,
	   wordRepetitions: true
	}
};

$('#password').pwstrength(options);
})
</script>
</body>
</html>