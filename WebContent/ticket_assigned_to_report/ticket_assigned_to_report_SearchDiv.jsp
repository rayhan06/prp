<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.TICKET_ASSIGNED_TO_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="">
    <div class="row mx-2">
		<div  class="search-criteria-div col-md-6">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
                    <%=LM.getText(LC.APPOINTMENT_ADD_PATIENTID, loginDTO)%>
				</label>
				<div class="col-md-9">
                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3" onclick="addEmployee()" id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%></button>
					<table class="table table-bordered table-striped" >
						<tbody id="employeeToSet"></tbody>
					</table>
					
					<input class='form-control' type = 'hidden' 	name='officeUnitType' id = 'officeUnitType' value=''/>
					<input class='form-control' type = 'hidden' 	name='organogramType' id = 'organogramType' value=''/>
				</div>
			</div>
		</div>
        <div id="doctorType" class="search-criteria-div col-md-6">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
                    <%=LM.getText(LC.TICKET_STATUS_REPORT_WHERE_TICKETSTATUSCAT, loginDTO)%>
				</label>
				<div class="col-md-9">
                   <select class='form-control'  name='ticketStatusCat' id = 'ticketStatusCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "ticket_status", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>

				</div>
			</div>
		</div>
		<div class="col-md-6">
       		<%@include file="../pbreport/yearmonth.jsp"%>
		</div>
		<div class="col-md-6">
        	<%@include file="../pbreport/calendar.jsp"%>
		</div>
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}




function patient_inputted(userName, orgId)
{
	console.log("patient_inputted " + userName);
	$("#organogramType").val(orgId);

}
</script>