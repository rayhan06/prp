<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="pb.Utils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="asset_list.Asset_listDTO" %>
<%@ page import="employee_records.Employee_recordsDAO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ASSET_LICENSE_EDIT_LANGUAGE, loginDTO);
    Asset_listDTO assetListDTO = (Asset_listDTO) request.getAttribute("assetListDTO");
    String assetListId = request.getParameter("ID");
    int assignOrRevoke = (int) request.getAttribute("assignOrRevoke");
    String action = "";
    if(assignOrRevoke == 1) action = "assignAsset";
    else action = "revokeAsset";
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    boolean isLanguageEnglish = Language.equals("English");
%>


<div class="box box-primary">
    <div class="box-body">
        <form class="form-horizontal" action="Asset_listServlet?actionType=<%=action%>"
              id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data" onsubmit="return PreprocessBeforeSubmiting()">
            <div class="form-body">

                <input type='hidden' class='form-control'  name='assetListId' id = 'assetListId' value='<%=assetListId%>' tag='pb_html'/>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.HM_NAME, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id = 'softwareName_div' style="padding-top: 7px; padding-left: 20px;">
                        <%=(isLanguageEnglish? assetListDTO.nameEn : assetListDTO.nameBn)%>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LIST_ADD_ASSETMODELTYPE, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " style="padding-top: 7px; padding-left: 20px;">
                        <%
                           String value = assetListDTO.assetModelType + "";
                        %>
                        <%
                            value = CommonDAO.getName(Integer.parseInt(value), "asset_model", Language.equals("English")?"name_en":"name_bn", "id");
                        %>

                        <%=Utils.getDigits(value, Language)%>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LIST_ADD_ASSETCATEGORYTYPE, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " style="padding-top: 7px; padding-left: 20px;">
                        <%
                            value = assetListDTO.assetCategoryType + "";
                        %>
                        <%
                            value = CommonDAO.getName(Integer.parseInt(value), "asset_category", Language.equals("English")?"name_en":"name_bn", "id");
                        %>

                        <%=Utils.getDigits(value, Language)%>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_EMPLOYEE_NAME, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 ">

                            <% if(action.equals("assignAsset")) { %>
                        <input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId' tag='pb_html'/>
                            <button type="button" class="btn btn-primary form-control" id="tagEmp_modal_button">
                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                            </button>
                            <table class="table table-bordered table-striped">
                                <thead></thead>

                                <tbody id="tagged_emp_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-block"
                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                            Remove
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <% } else
                            { %>
                        <input type='hidden' class='form-control'  name='revokeEmployeeRecordsId' value='<%=assetListDTO.employeeRecordsId%>' id = 'revokeEmployeeRecordsId' tag='pb_html'/>
                            <%=Employee_recordsDAO.getEmployeeName(assetListDTO.employeeRecordsId, Language)%>
                            <% }
                            %>



                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.ASSET_LICENSE_ADD_DESCRIPTION, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 ">
                        <input type='text' class='form-control'  name='remarks' id = 'remarks' tag='pb_html'/>
                    </div>
                </div>




                <div class="form-actions text-center">
                    <button class="btn btn-success" type="submit">

                        <%=LM.getText(LC.ASSET_LICENSE_ADD_ASSET_LICENSE_SUBMIT_BUTTON, loginDTO)%>

                    </button>
                </div>

            </div>

        </form>

    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
    <jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<script type="text/javascript">


    $(document).ready( function(){

    });

    function PreprocessBeforeSubmiting() {
        document.getElementById('employeeRecordsId').value = +added_employee_info_map.keys().next().value;
        return true;
    }


    function init(row)
    {



    }

    var row = 0;

    window.onload =function ()
    {
        init(row);
        CKEDITOR.replaceAll();
    }


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });
</script>






