<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="asset_list.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Asset_listServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.ASSET_LIST_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Asset_listDAO asset_listDAO = new Asset_listDAO("asset_list");
    Asset_listDTO asset_listDTO = asset_listDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ASSET_LIST_ADD_ASSET_LIST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.HM_HARDWARE_INFO, loginDTO)%>
            </h5>
            <table class="table table-bordered table-striped">
                <tr>
                    <td>
                        <b>
                            <%=LM.getText(LC.ASSET_LIST_ADD_ASSETCATEGORYTYPE, loginDTO)%>
                        </b>
                    </td>
                    <td>

                        <%
                            value = asset_listDTO.assetCategoryType + "";
                        %>
                        <%
                            value = CommonDAO.getName(Integer.parseInt(value), "asset_category", Language.equals("English") ? "name_en" : "name_bn", "id");
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_ASSETMODELTYPE, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.assetModelType + "";
                        %>
                        <%
                            value = (asset_listDTO.iD + 100000) + ": " + CommonDAO.getName(Integer.parseInt(value), "asset_model", Language.equals("English") ? "name_en" : "name_bn", "id");
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_DESCRIPTION, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.description + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_IMAGEDROPZONE, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.imageDropzone + "";
                        %>
                        <%
                            {
                                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(asset_listDTO.imageDropzone);
                        %>
                        <%@include file="../pb/dropzoneViewer.jsp" %>
                        <%
                            }
                        %>


                    </td>

                </tr>


                <%
                    if (asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_DESKTOP
                            || asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_LAPTOP
                            || asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_TABLET) {
                %>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_RAM, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.ram + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_CPU, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.cpu + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_HDD, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.hdd + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_MAC, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.mac + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

                <tr>
                    <td><b><%=LM.getText(LC.HM_OS, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.os + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <%
                    if (asset_listDTO.hasPreactivatedOs) {
                %>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_HASPREACTIVATEDOS, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.hasPreactivatedOs + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_OSLICENCENUMBER, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.osLicenceNumber + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <%
                        }
                    }
                %>
            </table>
            <h5 class="table-title mt-5">
                <%=LM.getText(LC.HM_VENDOR_DETAILS, loginDTO)%>
            </h5>
            <table class="table table-bordered table-striped">


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_ASSETSUPPLIERTYPE, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.assetSupplierType + "";
                        %>
                        <%
                            value = CommonDAO.getName(Integer.parseInt(value), "asset_supplier", Language.equals("English") ? "name_en" : "name_bn", "id");
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_VENDORNAME, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.vendorName + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_VENDORPHONE, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.vendorPhone + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_VENDOREMAIL, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.vendorEmail + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_TAG, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.tag + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_ORDERNO, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.orderNo + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.HM_PURCHASE_DATE, loginDTO)%>
                    </b></td>
                    <td>


                        <%
                            value = asset_listDTO.purchaseDate + "";
                        %>
                        <%
                            String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                        %>
                        <%=Utils.getDigits(formatted_purchaseDate, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_PURCHASECOST, loginDTO)%>
                    </b></td>
                    <td>


                        <%
                            if (asset_listDTO.purchaseCost > 0) {
                                value = String.format("%.1f", asset_listDTO.purchaseCost);
                            } else {
                                value = "";
                            }
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_WARRANTY, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.warranty + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.ASSET_LIST_ADD_SUPPORTDURATION, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.supportDuration + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.HM_QUANTITY, loginDTO)%>
                    </b></td>
                    <td>

                        <%
                            value = asset_listDTO.quantity + "";
                        %>

                        <%=Utils.getDigits(value, Language)%>


                    </td>

                </tr>

            </table>
            <h5 class="table-title mt-5">
                <%=LM.getText(LC.HM_STATUS, loginDTO)%>
            </h5>
            <table class="table table-bordered table-striped">
                <tr>
                    <td><b><%=LM.getText(LC.HM_STATUS, loginDTO)%>
                    </b></td>
                    <td>


                        <%=CatDAO.getName(Language, "asset_status", asset_listDTO.status)%>


                    </td>

                </tr>


                <tr>
                    <td><b><%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%>
                    </b></td>
                    <td>


                        <%=WorkflowController.getNameFromOrganogramId(asset_listDTO.employeeRecordsId, Language)%>


                    </td>

                </tr>


            </table>
        </div>
    </div>
</div>