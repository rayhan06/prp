<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="asset_list.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="asset_category.*" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Asset_listDTO asset_listDTO;
    asset_listDTO = (Asset_listDTO) request.getAttribute("asset_listDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (asset_listDTO == null) {
        asset_listDTO = new Asset_listDTO();

    }
    System.out.println("asset_listDTO = " + asset_listDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ASSET_LIST_ADD_ASSET_LIST_ADD_FORMNAME, loginDTO);
    String servletName = "Asset_listServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.ASSET_LIST_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    Asset_categoryDAO asset_categoryDAO = new Asset_categoryDAO();
    List<Asset_categoryDTO> asset_categoryDTOs = asset_categoryDAO.getNonLicenceAssets();
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_listServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=asset_listDTO.iD%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_ASSETCATEGORYTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-7">
                                            <select class='form-control' onclick="getCats(this.id)"
                                                    onchange="getModelsOfCategory('assetModelType')"
                                                    name='assetCategoryType'
                                                    id='assetCategoryType_select_<%=i%>' tag='pb_html'>

                                                <%
                                                    if (actionName.equalsIgnoreCase("add")) {
                                                %>
                                                <option value="-1"><%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                                </option>
                                                <%
                                                    }
                                                    for (Asset_categoryDTO asset_categoryDTO : asset_categoryDTOs) {
                                                %>
                                                <option value="<%=asset_categoryDTO.iD%>"
                                                        isWholesale="<%=asset_categoryDTO.isAssignable%>"
                                                        <%=asset_listDTO.assetCategoryType == asset_categoryDTO.iD ? "selected" : ""%>>
                                                    <%=asset_categoryDTO.nameEn%>
                                                </option>
                                                <%
                                                    }
                                                %>

                                            </select>


                                        </div>
                                        <div class="col-1">
                                            <button type="button" class="btn btn-warning text-white shadow btn-border-radius pl-4"
                                                    onclick="window.open('Asset_categoryServlet?actionType=getAddPage', '_blank')">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_ASSETMODELTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-7">
                                            <select class='form-control'
                                                    onclick="onlyGetModels(this.id)" required
                                                    name='assetModelType' id='assetModelType'
                                                    tag='pb_html'>
                                                <%
                                                    Options = CommonDAO.getOptions(Language, "asset_model", asset_listDTO.assetModelType);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                        <div class="col-1">
                                            <button type="button" class="btn btn-warning text-white shadow btn-border-radius pl-4"
                                                    onclick="window.open('Asset_modelServlet?actionType=getAddPage', '_blank')">

                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="status"
                                           value="<%=asset_listDTO.status%>"/>
                                    <div id="quantityDiv" style="display:none">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_QUANTITY, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='number' class='form-control'
                                                       name='quantity' id='quantity'
                                                       value='<%=asset_listDTO.quantity%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="pcdiv" style="display:none">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_RAM, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='ram'
                                                       id='ram_text_<%=i%>'
                                                       value='<%=asset_listDTO.ram%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_CPU, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='cpu'
                                                       id='cpu_text_<%=i%>'
                                                       value='<%=asset_listDTO.cpu%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_HDD, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='hdd'
                                                       id='hdd_text_<%=i%>'
                                                       value='<%=asset_listDTO.hdd%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_MAC, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='mac'
                                                       id='mac_text_<%=i%>'
                                                       value='<%=asset_listDTO.mac%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_OS, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='os'
                                                       id='os_text_<%=i%>' value='<%=asset_listDTO.os%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <input type='hidden' class='form-control-sm'
                                               name='hasPreactivatedOs'
                                               id='hasPreactivatedOs_checkbox_<%=i%>' value='false'
                                               tag='pb_html'>

                                        <div id="osDiv" style="display:none">
                                            <div class="form-group row">
                                                <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_OSLICENCENUMBER, loginDTO)%>
                                                </label>
                                                <div class="col-8">
                                                    <input type='text' class='form-control'
                                                           name='osLicenceNumber'
                                                           id='osLicenceNumber_text_<%=i%>'
                                                           value='<%=asset_listDTO.osLicenceNumber%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.HM_PURCHASE_DATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "purchaseDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="MIN_DATE"
                                                           value="<%=datestr%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='purchaseDate'
                                                   id='purchaseDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(asset_listDTO.purchaseDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_ASSETSUPPLIERTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='assetSupplierType'
                                                    id='assetSupplierType_select_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CommonDAO.getOptions(Language, "asset_supplier", asset_listDTO.assetSupplierType);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_TAG, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='tag'
                                                   id='tag_text_<%=i%>' value='<%=asset_listDTO.tag%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_ORDERNO, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='orderNo'
                                                   id='orderNo_text_<%=i%>'
                                                   value='<%=asset_listDTO.orderNo%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_PURCHASECOST, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                value = "";
                                                if (asset_listDTO.purchaseCost != -1) {
                                                    value = asset_listDTO.purchaseCost + "";
                                                }
                                            %>
                                            <input type='number' class='form-control'
                                                   name='purchaseCost' id='purchaseCost_number_<%=i%>'
                                                   value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_WARRANTY, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='warranty'
                                                   id='warranty_text_<%=i%>'
                                                   value='<%=asset_listDTO.warranty%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_DESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='description'
                                                   id='description_text_<%=i%>'
                                                   value='<%=asset_listDTO.description%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_VENDORNAME, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='vendorName'
                                                   id='vendorName_text_<%=i%>'
                                                   value='<%=asset_listDTO.vendorName%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_VENDORPHONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='vendorPhone'
                                                   id='vendorPhone_text_<%=i%>'
                                                   value='<%=asset_listDTO.vendorPhone%>'                                                                <%
                                                if (!actionName.equals("edit")) {
                                            %>
                                                   pattern="880[0-9]{10}"
                                                   title="vendorPhone must start with 880, then contain 10 digits"
                                                    <%
                                                        }
                                                    %>
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_VENDOREMAIL, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='vendorEmail'
                                                   id='vendorEmail_text_<%=i%>'
                                                   value='<%=asset_listDTO.vendorEmail%>'                                                                <%
                                                if (!actionName.equals("edit")) {
                                            %>
                                                   pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                   title="vendorEmail must be a of valid email address format"
                                                    <%
                                                        }
                                                    %>
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_SUPPORTDURATION, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   name='supportDuration'
                                                   id='supportDuration_text_<%=i%>'
                                                   value='<%=asset_listDTO.supportDuration%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.ASSET_LIST_ADD_IMAGEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "imageDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(asset_listDTO.imageDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    asset_listDTO.imageDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=asset_listDTO.imageDropzone%>">
                                                <input type='file' style="display:none"
                                                       name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>'
                                                   tag='pb_html'
                                                   value='<%=asset_listDTO.imageDropzone%>'/>


                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='employeeRecordsId'
                                           id='employeeRecordsId_hidden_<%=i%>'
                                           value='<%=asset_listDTO.employeeRecordsId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isAssigned'
                                           id='isAssigned' value='<%=asset_listDTO.isAssigned%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=asset_listDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=asset_listDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=asset_listDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=asset_listDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=asset_listDTO.lastModifierUser%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=asset_listDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=asset_listDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-10 text-right">
                        <a type="button" class="btn btn-sm cancel-btn shadow text-white btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.ASSET_LIST_ADD_ASSET_LIST_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn shadow text-white btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.ASSET_LIST_ADD_ASSET_LIST_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {

        preprocessDateBeforeSubmitting('purchaseDate', row);
        preprocessCheckBoxBeforeSubmitting('hasPreactivatedOs', row);
        preprocessCheckBoxBeforeSubmitting('isAssigned', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Asset_listServlet");
    }

    function init(row) {
        setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        getModelsOfCategory('assetModelType');
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    function getModelsOfCategory(dropdown) {
        var value = $("#assetCategoryType_select_<%=i%>").val();
        var isWholesale = $("#assetCategoryType_select_<%=i%>").find('option:selected').attr('isWholesale');
        if (value == <%=SessionConstants.ASSET_CAT_DESKTOP%>
            || value == <%=SessionConstants.ASSET_CAT_LAPTOP%>
            || value == <%=SessionConstants.ASSET_CAT_TABLET%>) {
            $("#pcdiv").css("display", "block");
        } else {
            $("#pcdiv").css("display", "none");
        }
        if (isWholesale == "true") {
            $("#quantityDiv").css("display", "block");
        } else {
            $("#quantityDiv").css("display", "none");
        }
        var origModel = $("#" + dropdown).val();
        fillDropDown(value, dropdown, "Asset_modelServlet?actionType=getModelsOfCategory&cat=" + value
            + "&language=<%=Language%>&id=" + origModel);
    }

    function showOsDiv(cbId) {
        if ($("#" + cbId).prop("checked") == true) {
            $("#osDiv").css("display", "block");
        } else {
            $("#osDiv").css("display", "none");
        }
    }

    function onlyGetModels(dropdown) {
        var value = $("#assetCategoryType_select_<%=i%>").val();
        console.log("value = " + value + " dropdown = " + dropdown);
        var origModel = $("#" + dropdown).val();
        fillDropDown(value, dropdown, "Asset_modelServlet?actionType=getModelsOfCategory&cat=" + value
            + "&language=<%=Language%>&id=" + origModel);
    }

    function getCats(id) {
        console.log("id = " + id);
        var value = $('#' + id).val();
        fillDropDown(value, id, "Asset_categoryServlet?actionType=getCats&id=" + value + "&language=<%=Language%>");
    }

</script>






