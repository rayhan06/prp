<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.ASSET_LIST_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <%
                    out.println("<input placeholder='অনুসন্ধান করুন' autocomplete='off' type='text' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ANYFIELD, loginDTO) + "' ");
                    String value = (String) session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);

                    if (value != null) {
                        out.println("value = '" + value + "'");
                    } else {
                        out.println("value=''");
                    }

                    out.println("/><br />");
                %>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.ASSET_LIST_ADD_ASSETCATEGORYTYPE, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <select class='form-control' name='asset_category_type' id='asset_category_type'
                                    onChange="fillDropDown(value, 'asset_model_type', 'Asset_modelServlet?actionType=getModelsOfCategory&cat=' + this.value + '&language=<%=Language%>');"
                            >
                                <%
                                    Options = CommonDAO.getOptions(Language, "select", "asset_category", "assetCategoryType_select_" + row, "form-control", "assetCategoryType", "any");
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.ASSET_LIST_ADD_ASSETMODELTYPE, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <select class='form-control' name='asset_model_type' id='asset_model_type'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CommonDAO.getOptions(Language, "select", "asset_model", "assetModelType_select_" + row, "form-control", "assetModelType", "any");
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3" onclick="addEmployee()"
                                    id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                            </button>
                            <table class="table table-bordered table-striped">
                                <tbody id="employeeToSet"></tbody>
                            </table>

                            <input class='form-control' type='hidden' name='employeeUnit' id='officeUnitType' value=''/>
                            <input class='form-control' type='hidden' name='employeeId' id='organogramType' value=''/>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.ASSET_LIST_ADD_TAG, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <input type="text" class="form-control" id="tag" placeholder="" name="tag"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.ASSET_LIST_ADD_ORDERNO, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <input type="text" class="form-control" id="order_no" placeholder="" name="order_no"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.ASSET_LIST_ADD_VENDORNAME, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <input type="text" class="form-control" id="vendor_name" placeholder="" name="vendor_name"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.ASSET_LIST_ADD_VENDORPHONE, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <input type="text" class="form-control" id="vendor_phone" placeholder="" name="vendor_phone"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.HM_STATUS, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <select class='form-control' name='status' id='status' onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("asset_status", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.ASSET_LIST_ADD_ASSETSUPPLIERTYPE, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <select class='form-control' name='asset_supplier_type' id='asset_supplier_type'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CommonDAO.getOptions(Language, "asset_supplier", -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.HM_PURCHASE_DATE, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="purchase_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="purchase_date_start" name="purchase_date_start">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group row">
                        <label class="col-4 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.HM_PURCHASE_DATE, loginDTO)%>
                        </label>
                        <div class="col-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="purchase_date_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="purchase_date_end" name="purchase_date_end">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    function patient_inputted(userName, orgId) {
        console.log("patient_inputted " + userName);
        $("#organogramType").val(orgId);

    }

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        console.log(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#asset_category_type").val() != -1) {
            params += '&asset_category_type=' + $("#asset_category_type").val();
        }
        if ($("#asset_model_type").val() != -1 && $("#asset_model_type").val() != "") {
            params += '&asset_model_type=' + $("#asset_model_type").val();
        }

        if ($("#organogramType").val() != "") {
            params += "&employee_records_id=" + $("#organogramType").val();
        }


        params += '&tag=' + $('#tag').val();
        params += '&order_no=' + $('#order_no').val();


        params += '&vendor_name=' + $('#vendor_name').val();
        params += '&vendor_phone=' + $('#vendor_phone').val();
        if ($("#status").val() != -1) {
            params += '&status=' + $("#status").val();
        }
        if ($("#asset_supplier_type").val() != -1) {
            params += '&asset_supplier_type=' + $("#asset_supplier_type").val();
        }

        $("#purchase_date_start").val(getDateStringById('purchase_date_start_js', 'DD/MM/YYYY'));
        params += '&purchase_date_start=' + getBDFormattedDate('purchase_date_start');
        $("#purchase_date_end").val(getDateStringById('purchase_date_end_js', 'DD/MM/YYYY'));
        params += '&purchase_date_end=' + getBDFormattedDate('purchase_date_end');

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

