<%@page import="asset_category.Asset_categoryDAO" %>
<%@page pageEncoding="UTF-8" %>

<%@page import="asset_list.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@page import="asset_category.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ASSET_LIST_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ASSET_LIST;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Asset_listDTO asset_listDTO = (Asset_listDTO) request.getAttribute("asset_listDTO");
    CommonDTO commonDTO = asset_listDTO;
    String servletName = "Asset_listServlet";


    System.out.println("asset_listDTO = " + asset_listDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Asset_listDAO asset_listDAO = (Asset_listDAO) request.getAttribute("asset_listDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td>
    <%
        value = asset_listDTO.purchaseDate + "";
    %>
    <%
        String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_purchaseDate, Language)%>


</td>
<td id='<%=i%>_assetCategoryType'>
    <%
        value = asset_listDTO.assetCategoryType + "";


    %>
    <%
        value = CommonDAO.getName(Integer.parseInt(value), "asset_category", Language.equals("English") ? "name_en" : "name_bn", "id");
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_assetModelType'>
    <%
        value = asset_listDTO.assetModelType + "";
    %>
    <%
        value = (asset_listDTO.iD + 100000) + ": " + CommonDAO.getName(Integer.parseInt(value), "asset_model", Language.equals("English") ? "name_en" : "name_bn", "id");
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td>
    <%=CommonDAO.getName(Language, "asset_supplier", asset_listDTO.assetSupplierType)%>
</td>


<td>
    <%=CatRepository.getInstance().getText(Language, "asset_status", asset_listDTO.status)%>


</td>

<td>
    <%=WorkflowController.getNameFromOrganogramId(asset_listDTO.employeeRecordsId, Language)%>


</td>

<td>
    <%
        value = asset_listDTO.quantity + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td>
    <%
        value = asset_listDTO.remainingQuantity + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Asset_listServlet?actionType=view&ID=<%=asset_listDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Asset_listServlet?actionType=getEditPage&ID=<%=asset_listDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td>

    <%
        Asset_categoryDAO asset_categoryDAO = new Asset_categoryDAO();
        Asset_categoryDTO asset_categoryDTO = asset_categoryDAO.getDTOByID(asset_listDTO.assetCategoryType);
        if (asset_listDTO.employeeRecordsId != -1) {
    %>
    <button type="button" class="btn btn-sm border-0 shadow"
            style="background-color: #773351; color: white; border-radius: 8px"
            onclick="location.href='Assign_assetServlet?actionType=getRecentPage&organogramId=<%=asset_listDTO.employeeRecordsId%>'">
        <%=LM.getText(LC.HM_REASSIGN, loginDTO)%>
    </button>
    <%
    	} else if (asset_listDTO.quantity > 0
                && (asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_LAPTOP
                || asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_DESKTOP
                || asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_TABLET
                || asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_MONITOR
                || asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_PRINTER
                || asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_SCANNER
                || asset_listDTO.assetCategoryType == SessionConstants.ASSET_CAT_ROUTER
                || (asset_categoryDTO != null && asset_categoryDTO.isAssignable)
        )) {
    %>
    <button type="button" class="btn btn-sm border-0 shadow"
            style="background-color: #cc3341; color: white; border-radius: 8px"
            onclick="location.href='Assign_assetServlet?actionType=getAddPage&asset_category_type=<%=asset_listDTO.assetCategoryType%>&id=<%=asset_listDTO.iD%>'">
        <%=LM.getText(LC.HM_ASSIGN, loginDTO)%>
    </button>
    <%
        }
    %>

</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=asset_listDTO.iD%>'/></span>
    </div>
</td>
																						
											

