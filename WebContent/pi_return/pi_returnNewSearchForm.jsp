<%@page import="pi_return.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>

<%
    Pi_returnDTO pi_returnDTO = new Pi_returnDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_returnDTO = Pi_returnDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_returnDTO;
    String tableName = "pi_return";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_returnServlet";

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">
                                                <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                            </label>
                                            <div class="col-md-7">
                                                <!-- Button trigger modal -->
                                                <button type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius"
                                                        id="employeeRecordId_modal_button"
                                                        onclick="employeeRecordIdModalBtnClicked();">
                                                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                                </button>
                                                <div class="input-group" id="employeeRecordId_div"
                                                     style="display: none">
                                                    <input type="hidden" name='employeeRecordId'
                                                           id='employeeRecordId_input'
                                                           value="<%=userDTO.organogramID%>">
                                                    <button type="button" class="btn btn-secondary form-control"
                                                            disabled
                                                            id="employeeRecordId_text"></button>
                                                    <span class="input-group-btn" style="width: 5%" tag='pb_html'>
														<button type="button" class="btn btn-outline-danger"
                                                                onclick="crsBtnClicked('employeeRecordId');"
                                                                id='employeeRecordId_crs_btn' tag='pb_html'>
															x
														</button>
													</span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn shadow text-white border-0 btn-success btn-border-radius "
                                                        id="emp-wise-search-btn" type="button" tag='pb_html'>
                                                    <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%>
                                                </button>
                                            </div>
                                        </div>
                                        <%--										<div class="form-group row">--%>
                                        <%--											<label class="col-md-3 col-form-label "><%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PIPACKAGEVENDORID, loginDTO)%></label>--%>
                                        <%--											<div class="col-md-7">--%>
                                        <%--												<%value = "returnDate_js_" + i;%>--%>
                                        <%--												<jsp:include page="/date/date.jsp">--%>
                                        <%--													<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
                                        <%--													<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
                                        <%--												</jsp:include>--%>
                                        <%--												<input type='hidden' name='returnDate' id = 'returnDate_date_<%=i%>' value= '' tag='pb_html'>--%>
                                        <%--											</div>--%>
                                        <%--										</div>--%>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="" id="accordionParentDiv">

                </div>


                <%--				<div class="form-actions text-right mb-2 mt-4 tableDiv" style="display: none;">--%>
                <%--					<button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">--%>
                <%--						<%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_CANCEL_BUTTON, loginDTO)%>--%>
                <%--					</button>--%>
                <%--					<button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn" type="button"--%>
                <%--							onclick="submitForm()">--%>
                <%--						<%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_SUBMIT_BUTTON, loginDTO)%>--%>
                <%--					</button>--%>
                <%--				</div>--%>
            </div>
        </form>
        <%@ include file="pi_returnAccordion.jsp" %>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<style>
    .template-row {
        display: none;
    }

    .accordion-contents {
        display: none;
    }
</style>

<style>
    #aspect-content {
        margin: 50px 0 0;
        font-family: Roboto, SolaimanLipi;
    }

    #aspect-content * {
        box-sizing: border-box;
    }

    .aspect-tab {
        position: relative;
        width: 100%;
        margin: 0 auto 10px;
        border-radius: 8px;
        background-color: #fff;
        box-shadow: 0 0 0 1px #ececec;
        opacity: 1;
        transition: box-shadow .2s, opacity .4s;
    }

    .aspect-tab:hover {
        box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.11);
    }

    .aspect-input {
        display: none;
    }

    .aspect-input:checked ~ .aspect-content + .aspect-tab-content {
        max-height: 5000px;
    }

    .aspect-input:checked ~ .aspect-content:after {
        transform: rotate(0);
    }

    .aspect-label {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        max-height: 80px;
        width: 100%;
        margin: 0;
        padding: 0;
        font-size: 0;
        z-index: 1;
        cursor: pointer;
    }

    .aspect-label:hover ~ .aspect-content:after {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTI0IDI0SDBWMGgyNHoiIG9wYWNpdHk9Ii44NyIvPgogICAgICAgIDxwYXRoIGZpbGw9IiM1NTZBRUEiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE1Ljg4IDE1LjI5TDEyIDExLjQxbC0zLjg4IDMuODhhLjk5Ni45OTYgMCAxIDEtMS40MS0xLjQxbDQuNTktNC41OWEuOTk2Ljk5NiAwIDAgMSAxLjQxIDBsNC41OSA0LjU5Yy4zOS4zOS4zOSAxLjAyIDAgMS40MS0uMzkuMzgtMS4wMy4zOS0xLjQyIDB6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=");
    }

    .aspect-content {
        position: relative;
        display: block;
        height: 80px;
        margin: 0;
        padding: 0 87px 0 30px;
        font-size: 0;
        white-space: nowrap;
        cursor: pointer;
        user-select: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
    }

    .aspect-content:before, .aspect-content:after {
        content: '';
        display: inline-block;
        vertical-align: middle;
    }

    .aspect-content:before {
        height: 100%;
    }

    .aspect-content:after {
        position: absolute;
        width: 24px;
        height: 100%;
        right: 30px;
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTI0IDI0SDBWMGgyNHoiIG9wYWNpdHk9Ii44NyIvPgogICAgICAgIDxwYXRoIGZpbGw9IiNBOUFDQUYiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE1Ljg4IDE1LjI5TDEyIDExLjQxbC0zLjg4IDMuODhhLjk5Ni45OTYgMCAxIDEtMS40MS0xLjQxbDQuNTktNC41OWEuOTk2Ljk5NiAwIDAgMSAxLjQxIDBsNC41OSA0LjU5Yy4zOS4zOS4zOSAxLjAyIDAgMS40MS0uMzkuMzgtMS4wMy4zOS0xLjQyIDB6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=");
        background-repeat: no-repeat;
        background-position: center;
        transform: rotate(180deg);
    }

    .aspect-name {
        display: inline-block;
        width: 75%;
        margin-left: 10px;
        font-weight: 400;
        white-space: normal;
        text-align: left;
        vertical-align: middle;
    }

    .aspect-stat {
        width: 40%;
        text-align: right;
    }

    .all-opinions,
    .aspect-name {
        font-size: 1.5rem;
        line-height: 22px;
        font-family: Roboto, SolaimanLipi;
    }

    .all-opinions {
        color: #5d5d5d;
        text-align: left;
    }

    .aspect-content + .aspect-tab-content {
        max-height: 0;
        overflow: hidden;
        /*transition: max-height 1s ease-in-out;*/
    }

    .aspect-content > div,
    .aspect-stat > div {
        display: inline-block;
    }

    .aspect-content > div {
        vertical-align: middle;
    }

    .positive-count,
    .negative-count,
    .neutral-count {
        display: inline-block;
        margin: 0 0 0 20px;
        padding-left: 26px;
        background-repeat: no-repeat;
        font-size: 13px;
        line-height: 20px;
        color: #363636;
    }

    .positive-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiM3RUQzMjEiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNSAxMS40MjdhNSA1IDAgMCAwIDEwIDAgLjcxNC43MTQgMCAxIDAtMS40MjkgMCAzLjU3MSAzLjU3MSAwIDAgMS03LjE0MiAwIC43MTQuNzE0IDAgMSAwLTEuNDI5IDB6bTEuMDcxLTVhMS4wNzEgMS4wNzEgMCAxIDAgMCAyLjE0MyAxLjA3MSAxLjA3MSAwIDAgMCAwLTIuMTQzem03Ljg1OCAwYTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDMgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3oiLz4KPC9zdmc+Cg==");
    }

    .negative-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiNGRjZFMDAiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNSAxNC45OThhLjcxNC43MTQgMCAwIDAgMS40MjkgMCAzLjU3MSAzLjU3MSAwIDAgMSA3LjE0MiAwIC43MTQuNzE0IDAgMSAwIDEuNDI5IDAgNSA1IDAgMSAwLTEwIDB6bTEuMDcxLTguNTdhMS4wNzEgMS4wNzEgMCAxIDAgMCAyLjE0MiAxLjA3MSAxLjA3MSAwIDAgMCAwLTIuMTQzem03Ljg1OCAwYTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDIgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3oiLz4KPC9zdmc+Cg==");
    }

    .neutral-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiNCQUMyRDYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNS43MTQgMTEuNDI3YS43MTQuNzE0IDAgMSAwIDAgMS40MjloOC41NzJhLjcxNC43MTQgMCAxIDAgMC0xLjQyOUg1LjcxNHptLjM1Ny01YTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDMgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3ptNy44NTggMGExLjA3MSAxLjA3MSAwIDEgMCAwIDIuMTQzIDEuMDcxIDEuMDcxIDAgMCAwIDAtMi4xNDN6Ii8+Cjwvc3ZnPgo=");
    }

    .aspect-info {
        width: 60%;
        white-space: nowrap;
        font-size: 0;
    }

    .aspect-info:before {
        content: '';
        display: inline-block;
        height: 44px;
        vertical-align: middle;
    }

    .chart-pie {
        position: relative;
        display: inline-block;
        height: 44px;
        width: 44px;
        border-radius: 50%;
        background-color: #e4e4e4;
        vertical-align: middle;
    }

    .chart-pie:after {
        content: '';
        display: block;
        position: absolute;
        height: 40px;
        width: 40px;
        top: 2px;
        left: 2px;
        border-radius: 50%;
        background-color: #fff;
    }

    .chart-pie-count {
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        font-size: 14px;
        font-weight: 500;
        line-height: 44px;
        color: #242a32;
        text-align: center;
        z-index: 1;
    }

    .chart-pie > div {
        clip: rect(0, 44px, 44px, 22px);
    }

    .chart-pie > div,
    .chart-pie.over50 .first-fill {
        position: absolute;
        height: 44px;
        width: 44px;
        border-radius: 50%;
    }

    .chart-pie.over50 > div {
        clip: rect(auto, auto, auto, auto);
    }

    .chart-pie.over50 .first-fill {
        clip: rect(0, 44px, 44px, 22px);
    }

    .chart-pie:not(.over50) .first-fill {
        display: none;
    }

    .second-fill {
        position: absolute;
        clip: rect(0, 22px, 44px, 0);
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border-width: 3px;
        border-style: solid;
        box-sizing: border-box;
    }

    .chart-pie.positive .first-fill {
        background-color: #82d428;
    }

    .chart-pie.positive .second-fill {
        border-color: #82d428;
    }

    .chart-pie.negative .first-fill {
        background-color: #ff6e00;
    }

    .chart-pie.negative .second-fill {
        border-color: #ff6e00;
    }

    .aspect-tab-content {
        background-color: #f9f9f9;
        /*font-size: 0;*/
        text-align: justify;
    }

    .sentiment-wrapper {
        /*padding: 24px 30px 30px;*/
    }

    .sentiment-wrapper > div {
        display: inline-block;
        width: 100%;
    }

    .sentiment-wrapper > div > div {
        width: 100%;
        padding: 0px 16px;
    }

    .opinion-header {
        position: relative;
        width: 100%;
        margin: 0 0 24px;
        font-size: 13px;
        font-weight: 500;
        line-height: 20px;
        color: #242a32;
        text-transform: capitalize;
    }

    .opinion-header > span:nth-child(2) {
        position: absolute;
        right: 0;
    }

    .opinion-header + div > span {
        font-size: 13px;
        font-weight: 400;
        line-height: 22px;
        color: #363636;
    }

    @media screen and (max-width: 800px) {
        .aspect-label {
            max-height: 102px;
        }

        .aspect-content {
            height: auto;
            padding: 10px 87px 10px 30px;
        }

        .aspect-content:before {
            display: none;
        }

        .aspect-content:after {
            top: 0;
        }

        .aspect-content > div {
            display: block;
            width: 100%;
        }

        .aspect-stat {
            margin-top: 10px;
            text-align: left;
        }
    }

    @media screen and (max-width: 750px) {
        .sentiment-wrapper > div {
            display: block;
            width: 100%;
            max-width: 100%;
        }

        .sentiment-wrapper > div:not(:first-child) {
            margin-top: 10px;
        }
    }

    @media screen and (max-width: 500px) {
        .aspect-label {
            max-height: 140px;
        }

        .aspect-stat > div {
            display: block;
            width: 100%;
        }

        .all-opinions {
            margin-bottom: 10px;
        }

        .all-opinions + div > span:first-child {
            margin: 0;
        }
    }

    .form-control-plaintext {
        font-size: 1.2rem !important;
        font-family: Roboto, SolaimanLipi !important;
    }

    th, td {
        text-align: left;
    }
</style>

<script type="text/javascript">

    const givenProductMap = new Map();
    const idPrefix = 'product-details-';
    const checkboxClassPrefix = 'product-details-checkbox-';
    const piReturnForm = $("#bigform");

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmitting()) {

            $.ajax({
                type: "POST",
                url: "<%=servletName%>?actionType=<%=actionName%>",
                data: piReturnForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    $('#emp-wise-search-btn').on('click', function () {
        buildTableData($('#employeeRecordId_input').val(), $('#returnDate_date_0').val());
    });
    const buildTableData = async (employeeOrgId, returnDate) => {

        if (document.querySelectorAll('.given-products:not(.accordion-contents)') !== null) {
            let tables = document.querySelectorAll('.given-products:not(.accordion-contents)');
            tables.forEach(item => {
                item.innerHTML = '';
            });
        }

        if (employeeOrgId === '') return;
        const url = 'Pi_unique_itemServlet?actionType=buildReturnedItemTableData&employeeOrgId=' + employeeOrgId + '&returnDate=' + returnDate;
        const response = await fetch(url);
        const productDetailsJson = await response.json();
        showGivenProductsInTable(productDetailsJson, givenProductMap);
    }

    function showNotFound(){
        setTimeout(function () {
            swal.fire({
                title: language==="English"?"Can not find any product that was returned by this employee":"প্রদত্ত কর্মকর্তা/কর্মচারী পণ্য ফেরত দিয়েছে এমন তথ্য পাওয়া যায়নি",
                type: "error",
                allowOutsideClick: false,
                confirmButtonColor: "#00B4B4"
            }, function () {
                window.location.href = "";
            });
        }, 0);

    }

    const showGivenProductsInTable = (productDetailsJson, givenProductMap) => {
        if (!Array.isArray(productDetailsJson)) {
            showNotFound();
            return;
        }
        if (productDetailsJson !== '') {
            displayClass('block');
        } else {
            displayClass('none');
            showNotFound();
            return;
        }

        let givenProductArr = [];
        let givenProductNameMap = new Map();
        let givenHiddenProductDateArr = [];
        let givenProductDateArr = [];

        let givenTableIdArr = [];
        let givenPurchaseIdArr = [];
        let givenUniqueItemIdArr = [];

        for (let productDetail of productDetailsJson) {
            givenProductArr = [...givenProductArr, +productDetail.itemId];
            givenProductDateArr = [...givenProductDateArr, productDetail.returnDate];
            givenHiddenProductDateArr = [...givenHiddenProductDateArr, productDetail.hiddenGivenDate];

            givenTableIdArr = [...givenTableIdArr, productDetail.iD];
            givenPurchaseIdArr = [...givenPurchaseIdArr, productDetail.purchaseId];
            givenUniqueItemIdArr = [...givenUniqueItemIdArr, productDetail.uniqueItemId];

            givenProductNameMap.set(
                +productDetail.itemId,
                productDetail.productName
            );
        }
        addDataInTable('product-details-table', givenProductNameMap, givenProductArr, givenProductDateArr, givenTableIdArr, givenPurchaseIdArr, givenHiddenProductDateArr, givenUniqueItemIdArr);
    }

    function addDataInTable(tableId, givenProductNameMap, givenProductArr, givenProductDateArr, givenTableIdArr, givenPurchaseIdArr, givenHiddenProductDateArr, givenUniqueItemIdArr) {

        let lan = '<%=Language%>';

        givenProductNameMap.forEach((item, itemInd) => {

            let accordionContent = document.querySelector('.accordion-contents').cloneNode(true);
            accordionContent.classList.replace('accordion-contents', 'accordion-populated');

            accordionContent.querySelector('.given-product-summary').innerHTML = item;

            let aspect_input = accordionContent.querySelector('.aspect-input');
            aspect_input.id = idPrefix + itemInd;

            let aspect_input_label = accordionContent.querySelector('.aspect-label');
            aspect_input_label.setAttribute("for", aspect_input.id);

            let aspect_checkbox = accordionContent.querySelector('.checkbox-class');
            let newCheckboxClass = checkboxClassPrefix + itemInd;
            aspect_checkbox.classList.replace("checkbox-class", newCheckboxClass);
            aspect_checkbox.id = newCheckboxClass;

            accordionContent.querySelector('th.row-data-given-product-count').style.display = 'none';

            accordionContent.querySelector('th.row-data-given-date').innerText = '<%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_SEARCH_ANYFIELD, loginDTO)%>';

            for (let i = 0; i < givenProductArr.length; i++) {
                if (givenProductArr[i] === +itemInd) {
                    let newRow = document.querySelector('.template-row').cloneNode(true);
                    newRow.classList.remove('template-row');

                    newRow.querySelector('td.row-data-product-name').innerText = item;
                    newRow.querySelector('td.row-data-given-date').innerText = givenProductDateArr[i];
                    newRow.querySelector('td.row-data-checkbox').style.display = 'none';

                    newRow.classList.remove('template-input');

                    accordionContent.querySelector('.product-details-table tbody').append(newRow);
                }
            }

            $("#accordionParentDiv").append(accordionContent);
        });
    }

    let allChecked = (selected) => {
        let allChecked = document.getElementById(selected.id).checked;
        let checkboxes = document.querySelectorAll("." + selected.id);
        [].slice.call(checkboxes).forEach((chk) => {
            chk.checked = allChecked;
        });
    }

    let addAccordionContent = () => {

        let accordionContent = document.querySelector('.accordion-contents').cloneNode(true);
        accordionContent.classList.remove('accordion-contents');
        return accordionContent;
    }

    let displayClass = (displayType) => {
        let displayArr = document.querySelectorAll('.tableDiv');
        displayArr.forEach(item => {
            item.style.display = displayType;
        });
    }
    let numbers = {
        0: "\u09E6",
        1: "\u09E7",
        2: "\u09E8",
        3: "\u09E9",
        4: "\u09EA",
        5: "\u09EB",
        6: "\u09EC",
        7: "\u09ED",
        8: "\u09EE",
        9: "\u09EF"
    };

    function convertENToBN(input, language) {

        if (language === "English") {
            return input;
        }
        let output = [];
        input = input.toString(10);
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    function employeeRecordIdInInput(empInfo) {

        $('#employeeRecordId_modal_button').hide();
        $('#employeeRecordId_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.employeeNameEn + ' (' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn + ')';
        } else {
            designation = empInfo.employeeNameBn + ' (' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn + ')';
        }
        document.getElementById('employeeRecordId_text').innerHTML = designation;
        //$('#employeeRecordId_input').val(empInfo.employeeRecordId);
        $('#employeeRecordId_input').val(empInfo.organogramId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: employeeRecordIdInInput
        }]
    ]);

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        displayClass('none');
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        let accordions = document.querySelectorAll('.accordion-populated');
        [...accordions].forEach(accordion => {
            accordion.innerHTML = '';
        });
    }


    function PreprocessBeforeSubmitting() {
        preprocessDateBeforeSubmitting('returnDate', 0);
        let template_objs = document.querySelectorAll(".template-row");
        template_objs.forEach(template_obj => {
            template_obj.remove();
        });

        let checkboxes = document.getElementsByName('selected_items');
        checkboxes.forEach((item, index) => {

            if (item.checked == true) {
                document.getElementsByClassName("hidden_checkbox")[index].value = "1";
            } else {
                document.getElementsByClassName("hidden_checkbox")[index].value = "-1";
            }

        });
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_returnServlet");
    }

    function init(row) {
        //setDateByStringAndId('returnDate_js_' + row, $('#returnDate_date_' + row).val());
    }

    var row = 0;
    $(document).ready(function () {
        displayClass('none');
        init(row);
        <%if(userDTO != null) {%>
        const employeeModel = JSON.parse('<%=EmployeeSearchModalUtil.getEmployeeDefaultSearchModelJson(userDTO.employee_record_id)%>')
        employeeRecordIdInInput(employeeModel);
        <%}%>
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-PiReturnItem").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-PiReturnItem");

            $("#field-PiReturnItem").append(t.html());
            SetCheckBoxValues("field-PiReturnItem");

            var tr = $("#field-PiReturnItem").find("tr:last-child");

            tr.attr("id", "PiReturnItem_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);

            });

            setDateByStringAndId('purchaseDate_js_' + child_table_extra_id, $('#purchaseDate_date_' + child_table_extra_id).val());

            child_table_extra_id++;

        });


    $("#remove-PiReturnItem").click(function (e) {
        var tablename = 'field-PiReturnItem';
        var i = 0;
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


</script>






