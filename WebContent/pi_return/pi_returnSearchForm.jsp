
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_return.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navPI_RETURN";
String servletName = "Pi_returnServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTERORGID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEREMPID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTERPHONENUM, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTERNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTERNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PURCHASEDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.PI_RETURN_SEARCH_PI_RETURN_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Pi_returnDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Pi_returnDTO pi_returnDTO = (Pi_returnDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = pi_returnDTO.requesterOrgId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterOfficeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterEmpId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.requesterOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_returnDTO.purchaseDate + "";
											%>
											<%
											String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_purchaseDate, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = pi_returnDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=pi_returnDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			