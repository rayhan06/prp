

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_return.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Pi_returnServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_returnDTO pi_returnDTO = Pi_returnDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pi_returnDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTERORGID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOrgId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOfficeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEREMPID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterEmpId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTERPHONENUM, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTERNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTERNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICENAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICENAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.requesterOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_RETURN_ADD_PURCHASEDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_returnDTO.purchaseDate + "";
											%>
											<%
											String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_purchaseDate, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTERPHONENUM, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTERNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTERNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTEROFFICENAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTEROFFICENAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_PURCHASEDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_RETURNAMOUNT, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REQUISITIONAMOUNT, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_RETURN_ADD_PI_RETURN_ITEM_REMAININGAMOUNT, loginDTO)%></th>
							</tr>
							<%
                        	PiReturnItemDAO piReturnItemDAO = PiReturnItemDAO.getInstance();
                         	List<PiReturnItemDTO> piReturnItemDTOs = (List<PiReturnItemDTO>)piReturnItemDAO.getDTOsByParent("pi_return_id", pi_returnDTO.iD);
                         	
                         	for(PiReturnItemDTO piReturnItemDTO: piReturnItemDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = piReturnItemDTO.requesterPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requesterNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requesterNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requesterOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requesterOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requesterOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requesterOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requesterOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requesterOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.purchaseDate + "";
											%>
											<%
											String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_purchaseDate, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.returnAmount + "";
											%>
											<%
											value = String.format("%.1f", piReturnItemDTO.returnAmount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.requisitionAmount + "";
											%>
											<%
											value = String.format("%.1f", piReturnItemDTO.requisitionAmount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = piReturnItemDTO.remainingAmount + "";
											%>
											<%
											value = String.format("%.1f", piReturnItemDTO.remainingAmount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>