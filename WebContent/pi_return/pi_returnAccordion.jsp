<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%--
  Created by IntelliJ IDEA.
  User: Nafees
  Date: 9/16/2021
  Time: 10:16 AM
  To change this template use File | Settings | File Templates.
--%>
<div id="aspect-content" style="margin: 2rem;" class="accordion-contents given-products">
    <div class="aspect-tab">
        <input  type="checkbox" class="aspect-input"
               name="aspect">
        <label  class="aspect-label"></label>
        <div class="aspect-content">
            <div class="aspect-info">
                <span class="aspect-name text-dark given-product-summary font-weight-bold">&nbsp;

                </span>
            </div>
            <div class="aspect-stat">

            </div>
        </div>
        <div class="aspect-tab-content">
            <div class="sentiment-wrapper">
                <div>
                    <div>
                        <div class="neutral-count opinion-header">
                        </div>
                        <div>
                            <%-- tab content start--%>

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered text-nowrap product-details-table" id="product-details-table">
                                    <thead>
                                    <tr>
                                        <th class="row-data-product-name">
                                            <%=LM.getText(LC.PI_AUCTION_ADD_PRODUCT_NAME, loginDTO)%>
                                        </th>
                                        <th class="row-data-given-date">
                                            <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_EDIT_PI_VENDOR_AUCTIONEER_DETAILS_EDIT_FORMNAME, loginDTO)%>
                                        </th>
                                        <th class="row-data-given-product-count">
                                            <div class="">
                                                <span><%=LM.getText(LC.HM_SELECT, loginDTO)%></span>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-between mt-3">
                                                <input type="checkbox" class="checkbox-class" id="" onclick="allChecked(this)"/>
                                            </div>

                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody></tbody>

                                    <tr class="template-row">

                                        <td class="row-data-hidden-tableId" style="display: none;"><input class="template-input" type="hidden" name="tableId" value="" /></td>
                                        <td class="row-data-hidden-uniqueItemId" style="display: none;"><input class="template-input" type="hidden" name="uniqueItemId" value="" /></td>
                                        <td class="row-data-hidden-itemId" style="display: none;"><input class="template-input" type="hidden" name="itemId" value=""/></td>
                                        <td class="row-data-hidden-purchaseId" style="display: none;"><input class="template-input" type="hidden" name="purchaseId" value="" /></td>
                                        <td class="row-data-hidden-purchaseDate" style="display: none;"><input class="template-input" type="hidden" name="purchaseDate" value="" /></td>

                                        <td class="row-data-product-name"></td>
                                        <td class="row-data-given-date"></td>
                                        <td class="row-data-checkbox"><input type='checkbox' name='selected_items' value=''
                                                                             class="form-control-sm child-checkboxes" onclick="singleChecked(this)" /></td>
                                        <td style="display: none;" class="hidden_vendor_price_checkbox_td">
                                            <input type="hidden"
                                                   class="form-control-sm hidden_checkbox"
                                                   name="hiddenCheckbox"
                                                   value="-1"
                                                   tag="pb_html">
                                        </td>
                                    </tr>
                                </table>
                            </div>


                            <%--tab content end--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
