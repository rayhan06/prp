
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="nothi_configuration.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
String navigator2 = "navNOTHI_CONFIGURATION";
String servletName = "Nothi_configurationServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>		
<%
boolean isLangEng = Language.equalsIgnoreCase("english");
%>		
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_SL, loginDTO)%></th>
								<th><%=LM.getText(LC.NOTHI_CONFIGURATION_ADD_NOTHINUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.NOTHI_CONFIGURATION_ADD_DESCRIPTION, loginDTO)%></th>		
								<th><%=isLangEng?"Edit":"এডিট"%></th>					

								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Nothi_configurationDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Nothi_configurationDTO nothi_configurationDTO = (Nothi_configurationDTO) data.get(i);
																																
											
											%>
											<tr>
								
											<td>
												<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language)%>.
											</td>
											<td>
											<%=Utils.getDigits(nothi_configurationDTO.nothiNumber, Language)%>
											</td>
		
											<td>
											<%=nothi_configurationDTO.description%>
											</td>
											
											<td>
												<button
											            type="button"
											            class="btn-sm border-0 shadow btn-border-radius text-white"
											            style="background-color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=nothi_configurationDTO.iD%>'"
											    >
											        <i class="fa fa-edit"></i>
											    </button>
										    </td>
										
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=nothi_configurationDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			