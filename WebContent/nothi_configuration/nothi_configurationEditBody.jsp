<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="nothi_configuration.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Nothi_configurationDTO nothi_configurationDTO = new Nothi_configurationDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	nothi_configurationDTO = Nothi_configurationDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = nothi_configurationDTO;
String tableName = "nothi_configuration";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.NOTHI_CONFIGURATION_ADD_NOTHI_CONFIGURATION_ADD_FORMNAME, loginDTO);
String servletName = "Nothi_configurationServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Nothi_configurationServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=nothi_configurationDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.NOTHI_CONFIGURATION_ADD_NOTHINUMBER, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='nothiNumber' 
																required 
																 id = 'nothiNumber_text_<%=i%>' value='<%=nothi_configurationDTO.nothiNumber%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.NOTHI_CONFIGURATION_ADD_DESCRIPTION, loginDTO)%>															</label>
                                                            <div class="col-8">
																<textarea  class='form-control'  
																name='description' 
																id = 'description_text_<%=i%>' 
																 tag='pb_html'><%=nothi_configurationDTO.description%></textarea>				
															</div>
                                                      </div>									
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.NOTHI_CONFIGURATION_ADD_NOTHI_CONFIGURATION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.NOTHI_CONFIGURATION_ADD_NOTHI_CONFIGURATION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Nothi_configurationServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);

	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	


</script>






