<%@page pageEncoding="UTF-8" %>

<%@page import="gate_pass.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="visitor_pass.Visitor_passDTO" %>
<%@ page import="visitor_pass.Visitor_passDAO" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VISITOR_PASS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VISITOR_PASS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Visitor_passDTO visitor_passDTO = (Visitor_passDTO) request.getAttribute("visitor_passDTO");
    String servletName = "Visitor_passServlet";
    System.out.println("visitor_passDTO = " + visitor_passDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Visitor_passDAO visitor_passDAO = (Visitor_passDAO) request.getAttribute("visitor_passDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td >
    <%=
        visitor_passDTO.name + ""
    %>
</td>

<td >
    <%
        value = visitor_passDTO.visitingDate + "";
    %>
    <%
        String formatted_visitingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_visitingDate, Language)%>

</td>

<td >
    <%
        int typeOfVisitor = visitor_passDTO.typeOfVisitor;
        if(typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_FOREIGNER) {
            value = Language.equals("English") ? "Foreigner" : "বিদেশী";
        }
        else {
            value = Language.equals("English") ? "Local" : "স্থানীয়";
        }
    %>
    <%=Utils.getDigits(value, Language)%>

</td>

<td id='<%=i%>_address'>
    <%
        value = visitor_passDTO.address + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Visitor_passServlet?actionType=armsOfficeView&ID=<%=visitor_passDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_approval' style='white-space: nowrap' colspan="3">

    <div class="button-group">
        <%
            if (visitor_passDTO.secondLayerApprovalStatus == Visitor_passDAO.approvalStatusPending) {
        %>
        <button type="button" id='<%=i%>_button_approve' class="btn-sm btn-success border-0 shadow " style="border-radius: 8px;"
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>')">
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদন"%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss' class="btn-sm btn-danger border-0 shadow " style="border-radius: 8px;"
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>')">
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <%
        } else if (visitor_passDTO.secondLayerApprovalStatus == Visitor_passDAO.approvalStatusCancelled) {
        %>
        <button type="button" id='<%=i%>_button_approve' class="btn-sm btn-success border-0 shadow " style="border-radius: 8px;"
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>')">
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদন"%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss' class="btn-sm btn-danger border-0 shadow " style="border-radius: 8px;"
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>')" disabled>
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <%
        } else {
        %>
        <button type="button" id='<%=i%>_button_approve' class="btn-sm btn-success border-0 shadow " style="border-radius: 8px;background-color: #555555;"
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>')" disabled>
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদন"%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss' class="btn-sm btn-danger border-0 shadow " style="border-radius: 8px;background-color: #555555;"
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>')" disabled>
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <%
            }
        %>

    </div>

</td>


<script>

    function approval(approve_button_id, dismiss_button_id, gate_pass_id) {

        let url = "Visitor_passServlet?actionType=visitorPassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=approved";
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {
                $('#' + approve_button_id).prop("disabled", true);
                $('#' + dismiss_button_id).prop("disabled", true);
                $('#' + approve_button_id).css('backgroundColor', '#555555');
                $('#' + dismiss_button_id).css('backgroundColor', '#555555');
            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                Swal.fire({
                    position: 'center',
                    type: 'success',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Requested visitor pass has been approved successfully" : "সংসদ ভিজিটর পাসটি অনুমোদিত হয়েছে"%>',
                    showConfirmButton: false,
                    timer: 3000
                });
            }
        });
    }


    function dismissal(dismiss_button_id, approve_button_id, gate_pass_id) {
        let url = "Visitor_passServlet?actionType=visitorPassSecondLayerApproval" + "&gatePassId=" + gate_pass_id + "&approvalstatus=dismiss";

        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {
                $('#' + dismiss_button_id).prop("disabled", true);
                $('#' + approve_button_id).prop("disabled", false);
            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                Swal.fire({
                    position: 'center',
                    type: 'warning',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Requested visitor pass has been dismissed successfully" : "সংসদ ভিজিটর পাসটির অনুমোদন বাতিল করা হয়েছে"%>',
                    showConfirmButton: false,
                    timer: 3000
                });
            }
        });
    }

</script>


