<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="visitor_pass.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="nationality.NationalityDAO" %>
<%@ page import="nationality.NationalityRepository" %>
<%@ page import="profession.ProfessionRepository" %>
<%@ page import="visitor_pass_affiliated_person.Visitor_pass_affiliated_personDTO" %>
<%@ page import="visitor_pass_affiliated_person.Visitor_pass_affiliated_personDAO" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>


<%
    String servletName = "Visitor_passServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VISITOR_PASS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Visitor_passDAO visitor_passDAO = new Visitor_passDAO("visitor_pass");
    Visitor_passDTO visitor_passDTO = visitor_passDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    String context = request.getContextPath() + "/";

    Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO = new Visitor_pass_affiliated_personDAO();
    List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = visitor_pass_affiliated_personDAO.getPersonsByVisitorPassId(id);
%>

<style>
    body {
        font-family: 'Roboto', 'SolaimanLipi', sans-serif;
    }

    .title {
        color: #0098bf !important;
        font-weight: bold;
    }
</style>

<!-- begin:: Subheader -->
<%--<div class="ml-auto mr-3 mt-4">--%>
<%--    <button--%>
<%--            type="button"--%>
<%--            class="btn" id='printer'--%>
<%--            onclick="downloadPdf('<%=gatePassModel.visitorName%>('+ '<%=gatePassModel.visitingDateEng%>' +').pdf','modalbody')"--%>
<%--    >--%>
<%--        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--    </button>--%>
<%--    <button--%>
<%--            type="button"--%>
<%--            class="btn"--%>
<%--            id='printer2'--%>
<%--            onclick="printAnyDiv('modalbody')"--%>
<%--    >--%>
<%--        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--    </button>--%>
<%--</div>--%>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="" id="modalbody">
        <div class="kt-portlet" style="background-color: #f2f2f2!important;">
            <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
                <div class="row">
                    <div class="col-md-3 mb-4 mb-md-0">
                            <span class="lead font-weight-bold"
                                  style="color: #0098bf !important"><%=LM.getText(LC.GATE_PASS_VIEW_GATE_NO, loginDTO)%>
                                :
                                ......................................
                            </span>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center">
                        <div class="text-center">
                            <img
                                    width="30%"
                                    src="<%=context%>assets/static/parliament_logo.png"
                                    alt="logo"
                                    class="logo-default"
                            />
                            <h2 class="mt-3" style="color: #0098bf !important">
                                বাংলাদেশ জাতীয় সংসদ
                            </h2>
                            </h1>
                            <h4 class="mt-2 table-title">
                                সংসদ ভিজিট পাস
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="row">
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGDATE, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%
                                    value = visitor_passDTO.visitingDate + "";
                                %>
                                <%
                                    String formatted_visitingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_visitingDate, Language)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGTIME, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%
                                    value = visitor_passDTO.visitingTime + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=Language.equalsIgnoreCase("English") ? "Type of Visitor" : "দর্শনার্থীর ধরন"%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? (Language.equalsIgnoreCase("English") ? "Local" : "স্থানীয়") :
                                        (Language.equalsIgnoreCase("English") ? "Foreigner" : "বিদেশী")%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? (Language.equalsIgnoreCase("English") ? "Organization Name" : "প্রতিষ্ঠানের নাম") :
                                            (Language.equalsIgnoreCase("English") ? "Name" : "নাম")%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.name%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? (Language.equalsIgnoreCase("English") ? "Organization Address" : "প্রতিষ্ঠানের ঠিকানা") :
                                            (Language.equalsIgnoreCase("English") ? "Address" : "ঠিকানা")%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.address%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_MOBILENUMBER, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=Utils.getDigits(visitor_passDTO.mobileNumber.substring(2), Language)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_EMAIL, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.email%>
                            </div>
                        </div>
                        <%
                            if (visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_FOREIGNER) {
                        %>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_NATIONALITYTYPE, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=NationalityRepository.getInstance().getText(Language, visitor_passDTO.nationalityType)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_PROFESSIONTYPE, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=ProfessionRepository.getInstance().getText(Language, visitor_passDTO.professionType)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_PASSPORTNO, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=Utils.getDigits(visitor_passDTO.passportNo, Language)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2 form-group">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_VISAEXPIRY, loginDTO)%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%
                                    value = visitor_passDTO.visaExpiry + "";
                                %>
                                <%
                                    formatted_visitingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_visitingDate, Language)%>
                            </div>
                        </div>
                        <%
                            }

                            if (visitor_pass_affiliated_personDTOList.size() > 0) {
                        %>
                        <div class="col-12 row">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=Language.equalsIgnoreCase("English") ? "Companions" : "সহচরবৃন্দ"%>
                                </label>
                            </div>
                            <div class="col-3">
                                <span class="title">
                                  <%=LM.getText(LC.VISITOR_PASS_ADD_NAME, loginDTO)%>
                                </span>
                            </div>
                            <div class="col-3">
                                <span class="title">
                                  <%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_CREDENTIALNO, loginDTO)%>
                                </span>
                            </div>
                            <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.VISITOR_PASS_ADD_NATIONALITYTYPE, loginDTO)%>
                                </span>
                            </div>
                            <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_MOBILENUMBER, loginDTO)%>
                                </span>
                            </div>
                        </div>
                        <%
                            }
                            for (Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO : visitor_pass_affiliated_personDTOList) {
                        %>
                        <div class="col-12 row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-3">
                                <%=visitor_pass_affiliated_personDTO.name%>
                            </div>
                            <div class="col-3">
                                <%=Utils.getDigits(visitor_pass_affiliated_personDTO.credentialNo, Language)%>
                            </div>
                            <div class="col-md-2">
                                <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? (Language.equalsIgnoreCase("English") ? "Bangladeshi" : "বাংলাদেশী") :
                                        (NationalityRepository.getInstance().getText(Language, visitor_pass_affiliated_personDTO.nationalityType))%>
                            </div>
                            <div class="col-md-2">
                                <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? Utils.getDigits(visitor_pass_affiliated_personDTO.mobileNumber, Language) : ""%>
                            </div>
                        </div>
                        <%
                            }
                        %>
                        <div class="col-12 row mt-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=Language.equalsIgnoreCase("English") ? "Watch Assembly" : "অধিবেশন দেখতে চান"%>
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.enjoyAssembly == 1 ? (Language.equalsIgnoreCase("English") ? "Yes" : "হ্যা") :
                                        (Language.equalsIgnoreCase("English") ? "No" : "না")%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12">
                        <div class="button-group text-right">
                            <%
                                if (visitor_passDTO.secondLayerApprovalStatus == Visitor_passDAO.approvalStatusPending) {
                            %>
                            <button type="button" id='<%=i%>_button_approve' class="btn-sm btn-success border-0 shadow "
                                    style="border-radius: 8px;"
                                    onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>')">
                                <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদন"%>
                            </button>
                            <button type="button" id='<%=i%>_button_dismiss' class="btn-sm btn-danger border-0 shadow "
                                    style="border-radius: 8px;"
                                    onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>')">
                                <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
                            </button>
                            <%
                            } else if (visitor_passDTO.secondLayerApprovalStatus == Visitor_passDAO.approvalStatusCancelled) {
                            %>
                            <button type="button" id='<%=i%>_button_approve' class="btn-sm btn-success border-0 shadow "
                                    style="border-radius: 8px;"
                                    onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>')">
                                <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদন"%>
                            </button>
                            <button type="button" id='<%=i%>_button_dismiss' class="btn-sm btn-danger border-0 shadow "
                                    style="border-radius: 8px;"
                                    onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>')"
                                    disabled>
                                <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
                            </button>
                            <%
                            } else {
                            %>
                            <button type="button" id='<%=i%>_button_approve' class="btn-sm btn-success border-0 shadow "
                                    style="border-radius: 8px;background-color: #555555;"
                                    onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>')"
                                    disabled>
                                <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদন"%>
                            </button>
                            <button type="button" id='<%=i%>_button_dismiss' class="btn-sm btn-danger border-0 shadow "
                                    style="border-radius: 8px;background-color: #555555;"
                                    onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>')"
                                    disabled>
                                <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
                            </button>
                            <%
                                }
                            %>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    let url;

    function approval(approve_button_id, dismiss_button_id, visitor_pass_id) {
        url = "Visitor_passServlet?actionType=visitorPassSecondLayerApprovalFromView" + "&visitorPassId=" + visitor_pass_id + "&approvalstatus=approved";
        document.location.href = '<%=request.getContextPath()%>' + '/' + url;
    }


    function dismissal(dismiss_button_id, approve_button_id, visitor_pass_id) {
        url = "Visitor_passServlet?actionType=visitorPassSecondLayerApprovalFromView" + "&visitorPassId=" + visitor_pass_id + "&approvalstatus=dismiss";
        document.location.href = '<%=request.getContextPath()%>' + '/' + url;
    }

</script>