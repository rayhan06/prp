<%@page import="dbm.DBMW" %>

<%@page import="visitor_pass.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@page import="files.*" %>

<%
    Visitor_passDTO visitor_passDTO;
    visitor_passDTO = (Visitor_passDTO) request.getAttribute("visitor_passDTO");
    if (visitor_passDTO == null) {
        visitor_passDTO = new Visitor_passDTO();

    }

    String actionName = "add";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = "Bangla";
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    FilesDAO filesDAO = new FilesDAO();
    long ColumnID;


    String fireSwal = (String) session.getAttribute("fireSwal");
    if (fireSwal == null) {
        fireSwal = "no";
    }

//    Now it's time to remove attribute
    session.setAttribute("fireSwal", "no");
%>

<style>
    hr {
        border: solid 1px black;
        width: 100%;
        color: #050505;
        height: 1px;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="row" id="modalbody">
        <div class="kt-portlet">
            <div class="kt-portlet__body" style="background-color: #f6f9fb">
                <div class="py-4 shadow-sm rounded" style="background-color: #FFFFFF">
                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                           value='<%=visitor_passDTO.iD%>' tag='pb_html'/>
                    <div class="d-flex justify-content-center">
                        <h4 class="table-title">
                            <%=Language.equalsIgnoreCase("English") ? "Type of Visitor" : "ভিজিটের ধরণ"%>
                        </h4>
                    </div>
                    <div class="d-flex justify-content-center">
                        <select class='form-control w-25 mt-1' name='typeOfVisitor'
                                id='typeOfVisitor_select2_<%=i%>'
                                onchange="typesOfVisitorChanged()"
                                tag='pb_html'>
                            <%=CatRepository.getInstance().buildOptions("visitorPassTypes", Language, null)%>
                        </select>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Gate_passPublicServlet?actionType=addVisitorPass&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="row mt-5">
                        <div class="col-6 my-3" id="visiting_date_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Visiting Date" : "সাক্ষাতের তারিখ"%>
                            </h5>
                            <div>
                                <%value = "visitingDate_js_" + i;%>
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                </jsp:include>
                                <input type='hidden' name='visitingDate' id='visitingDate_date_<%=i%>'
                                       value='<%=dateFormat.format(new Date(visitor_passDTO.visitingDate))%>'
                                       tag='pb_html'>
                            </div>
                        </div>
                        <div class="col-6 my-3" id="visiting_time_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Visiting TIme" : "সাক্ষাতের সময়"%>
                            </h5>
                            <div>
                                <%value = "visitingTime_js_" + i;%>
                                <jsp:include page="/time/time.jsp">
                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                </jsp:include>
                                <input
                                        type='hidden'
                                        name='visitingTime'
                                        id='visitingTime_time_<%=i%>'
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-6 my-3" id="name_div" style="display: none">
                            <h5 id="name_label">

                            </h5>
                            <div>
                                <input type='text' class='form-control' name='name'
                                       id='name_text_<%=i%>'
                                       tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-6 my-3" id="address_div" style="display: none">
                            <h5 id="address_label">

                            </h5>
                            <div>
                                <input
                                        type='text'
                                        class='form-control'
                                        name='address'
                                        id='address_text_<%=i%>'
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-6 my-3" id="nationality_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Nationality" : "জাতীয়তা"%>
                            </h5>
                            <div>
                                <select
                                        class='form-control'
                                        name='nationalityType'
                                        id='nationalityType_select2_<%=i%>'
                                        tag='pb_html'
                                        style="width: 100%!important;"
                                >
                                    <%
                                        Options = CommonDAO.getOptions(Language, "nationality", -2);
                                    %>
                                    <%=Options%>
                                </select>
                            </div>
                        </div>
                        <div class="col-6 my-3" id="profession_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Profession" : "পেশা"%>
                            </h5>
                            <div>
                                <select
                                        class='form-control'
                                        name='professionType'
                                        id='professionType_select2_<%=i%>'
                                        tag='pb_html'
                                        style="width: 100%!important;"
                                >
                                    <%
                                        Options = CommonDAO.getOptions(Language, "profession", -2);
                                    %>
                                    <%=Options%>
                                </select>
                            </div>
                        </div>
                        <div class="col-6 my-3" id="passport_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Passport No" : "পাসপোর্ট নং"%>
                            </h5>
                            <div>
                                <input
                                        type='text'
                                        class='form-control'
                                        name='passportNo'
                                        id='passportNo_text_<%=i%>'
                                        placeholder="<%=Language.equalsIgnoreCase("English") ? "Enter passport number" : "পাসপোর্ট নাম্বার দিন"%>"
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-6 my-3" id="visa_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Visa Expiry" : "ভিসার মেয়াদ"%>
                            </h5>
                            <div>
                                <%value = "visaExpiry_js_" + i;%>
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                </jsp:include>
                                <input
                                        type='hidden'
                                        name='visaExpiry'
                                        id='visaExpiry_date_<%=i%>'
                                        value='<%=dateFormat.format(new Date(visitor_passDTO.visitingDate))%>'
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-6 my-3" id="mobile_number_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Mobile Number" : "মোবাইল নাম্বার"%>
                            </h5>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3"><%=Language.equalsIgnoreCase("English") ? "+88" : "+৮৮"%></span>
                                </div>
                                <input
                                        type='text'
                                        class='form-control'
                                        name='mobileNumber'
                                        placeholder="<%=Language.equalsIgnoreCase("English") ? "Enter mobile number" : "মোবাইল নাম্বার দিন"%>"
                                        id='mobileNumber_text_<%=i%>'
                                        onkeypress="return (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-6 my-3" id="email_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Email" : "ইমেইল"%>
                            </h5>
                            <div>
                                <input
                                        type='text'
                                        class='form-control'
                                        name='email'
                                        id='email_text_<%=i%>'
                                        placeholder="<%=Language.equalsIgnoreCase("English") ? "Enter email address" : "ইমেইল এড্রেস দিন"%>"
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-6 my-3" id="institutional_letter_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Uploaded Letter (must be written on institutional letter pad)" :
                                        "আপলোডেড চিঠি (অবশ্যই প্রতিষ্ঠানের লেটার প্যাড এ হতে হবে)"%>
                            </h5>
                            <div class="col-12  " id='filesDropzone_div_<%=i%>' style="padding: unset; color: #b7bacd">
                                <%
                                    if (actionName.equals("edit")) {
                                        List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(visitor_passDTO.fileID);
                                %>
                                <table>
                                    <tr>
                                        <%
                                            if (filesDropzoneDTOList != null) {
                                                for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                    FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                        %>
                                        <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                            <%
                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                            %> <img
                                                src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                style='width: 100px'/> <%
                                            }
                                        %> <a
                                                href='Visitor_passServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                download><%=filesDTO.fileTitle%>
                                        </a> <a class='btn btn-danger'
                                                onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                        </td>
                                        <%
                                                }
                                            }
                                        %>
                                    </tr>
                                </table>
                                <%
                                    }
                                %>

                                <%
                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                %>
                                <div class="dropzone" style="min-height: auto; border: dotted 1px" id="file_dropzone"
                                     action="Visitor_passServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit") ? visitor_passDTO.fileID : ColumnID%>">
                                    <input type='file' style="display: none"
                                           name='filesDropzoneFile'
                                           id='filesDropzone_dropzone_File_<%=i%>'
                                           tag='pb_html'/>
                                </div>
                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                       id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                       tag='pb_html'/>
                                <input type='hidden' name='filesDropzone'
                                       id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                       value='<%=actionName.equals("edit") ? visitor_passDTO.fileID : ColumnID%>'/>


                            </div>
                            <%--                            <div>--%>
                            <%--                                <div class='modal fade' id='institutionalLetterModal_<%=i%>'>--%>
                            <%--                                    <div class='modal-dialog modal-lg' role='document'>--%>
                            <%--                                        <div class='modal-content'>--%>
                            <%--                                            <div class='modal-body'>--%>
                            <%--                                                <button type='button' class='close' data-dismiss='modal'--%>
                            <%--                                                        aria-label='Close'>--%>
                            <%--                                                    <span aria-hidden='true'>&times;</span>--%>
                            <%--                                                </button>--%>
                            <%--                                            </div>--%>
                            <%--                                        </div>--%>
                            <%--                                    </div>--%>
                            <%--                                </div>--%>
                            <%--                                <input type='file' class='form-control' name='institutionalLetter'--%>
                            <%--                                       id='institutionalLetter_file_<%=i%>'--%>
                            <%--                                       value='<%=visitor_passDTO.institutionalLetter%>' tag='pb_html'/>--%>
                            <%--                            </div>--%>
                        </div>
                        <div class="col-12 my-3 py-3" id="affiliated-persons-add-lebel" style="display: none">
                            <h4>
                                <%=Language.equalsIgnoreCase("English") ? "Address the affiliated persons:" : "আপনার সাথে প্রবেশকারী ব্যক্তিবর্গের পরিচয় দিনঃ"%>
                            </h4>
                            <div class="d-flex justify-content-between align-items-center">
                                <label
                                        class="text-nowrap h5"
                                        id="gatePassSubTypeIdLabel">
                                    <%=Language.equalsIgnoreCase("English") ? "Number of persons" : "কতজন প্রবেশ করবেন"%>
                                </label>
                                <input type='number'
                                       class='form-control mx-3'
                                       name='additional_person'
                                       id='additional_person'
                                       min="0" value="0" tag='pb_html'/>
                                <button class="btn submit-btn text-white shadow btn-border-radius text-nowrap"
                                        type="button"
                                        id="additional_person_button">

                                    <%=Language.equalsIgnoreCase("English") ? "GIVE THEIR INFO" : "তাদের তথ্য দিন"%>

                                </button>
                                <input type='hidden' class='form-control'
                                       name='additionalPersonCount'
                                       id='additionalPersonCount'
                                       value="0" tag='pb_html'/>
                            </div>

                            <div id="add_person_div" class="mt-2">
                            </div>

                        </div>
                        <div class="col-6 my-3" id="enjoy_assembly_div" style="display: none">
                            <h5>
                                <%=Language.equalsIgnoreCase("English") ? "Enjoy Assembly?" : "অধিবেশন দেখতে চান?"%>
                            </h5>
                            <div class="d-flex align-items-center">
                                <div>
                                    <input
                                            type="radio"
                                            class="form-group"
                                            id="yes_radio"
                                            name="enjoyAssemblyRadio"
                                            value="1"
                                    />
                                    <label for="yes_radio">
                                        <%=Language.equalsIgnoreCase("English") ? "Yes" : "হ্যা"%>
                                    </label>
                                </div>
                                <div class="ml-3">
                                    <input
                                            type="radio"
                                            class="form-group"
                                            id="no_radio"
                                            name="enjoyAssemblyRadio"
                                            value="0"
                                            checked="checked"
                                    />
                                    <label for="no_radio">
                                        <%=Language.equalsIgnoreCase("English") ? "No" : "না"%>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <input type='hidden' class='form-control' name='enjoyAssembly'
                               id='enjoyAssembly'
                               tag='pb_html'>
                        <input type='hidden' class='form-control' name='paymentStatus'
                               id='paymentStatus_hidden_<%=i%>'
                               value='<%=visitor_passDTO.paymentStatus%>'
                               tag='pb_html'/>
                        <input type='hidden' class='form-control' name='firstLayerApprovalStatus'
                               id='firstLayerApprovalStatus_hidden_<%=i%>'
                               value='<%=visitor_passDTO.firstLayerApprovalStatus%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='firstLayerApprovedBy'
                               id='firstLayerApprovedBy_hidden_<%=i%>'
                               value='<%=visitor_passDTO.firstLayerApprovedBy%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='firstLayerApprovalTime'
                               id='firstLayerApprovalTime_hidden_<%=i%>'
                               value='<%=visitor_passDTO.firstLayerApprovalTime%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='secondLayerApprovalStatus'
                               id='secondLayerApprovalStatus_hidden_<%=i%>'
                               value='<%=visitor_passDTO.secondLayerApprovalStatus%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='secondLayerApprovedBy'
                               id='secondLayerApprovedBy_hidden_<%=i%>'
                               value='<%=visitor_passDTO.secondLayerApprovedBy%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='secondLayerApprovalTime'
                               id='secondLayerApprovalTime_hidden_<%=i%>'
                               value='<%=visitor_passDTO.secondLayerApprovalTime%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='insertedByUserId'
                               id='insertedByUserId_hidden_<%=i%>'
                               value='<%=visitor_passDTO.insertedByUserId%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='insertedByOrganogramId'
                               id='insertedByOrganogramId_hidden_<%=i%>'
                               value='<%=visitor_passDTO.insertedByOrganogramId%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='insertionDate'
                               id='insertionDate_hidden_<%=i%>'
                               value='<%=visitor_passDTO.insertionDate%>'
                               tag='pb_html'/>
                        <input type='hidden' class='form-control' name='lastModifierUser'
                               id='lastModifierUser_hidden_<%=i%>'
                               value='<%=visitor_passDTO.lastModifierUser%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='isDeleted'
                               id='isDeleted_hidden_<%=i%>' value='<%=visitor_passDTO.isDeleted%>'
                               tag='pb_html'/>
                        <input type='hidden' class='form-control' name='lastModificationTime'
                               id='lastModificationTime_hidden_<%=i%>'
                               value='<%=visitor_passDTO.lastModificationTime%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='localOrForeign'
                               id='localOrForeign_hidden'
                               tag='pb_html'/>
                        <div class="col-12" id="submit_btn_captcha" style="display: none">
                            <div class="row">
                                <div class="col-8 text-left">
                                    <div class="">
                                        <div class="d-flex justify-content-start align-items-center">
                                            <div class="">
                                                <img
                                                        class="img-thumbnail"
                                                        id="captcha" src="<%=request.getContextPath()%>/simpleCaptchaServlet"
                                                        alt="loading captcha..."
                                                />
                                            </div>

                                            <div class="ml-5">
                                                <img
                                                        src="<%=request.getContextPath()%>/assets/images/refresh.png"
                                                        width="40"
                                                        height="40"
                                                        title="Refresh Captcha"
                                                        alt="refresh captcha"
                                                        id="reloadCaptcha"
                                                        onclick="getcapcha()"
                                                        class="img-thumbnail"
                                                />
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-start align-items-center mt-2">
                                            <div id='captcha_input_div_0'>
                                                <input
                                                        type='text'
                                                        id='answer_id'
                                                        class='form-control inputBackground preventEnter'
                                                        name='captchaInput'
                                                        id='captcha_input_text_0'
                                                        placeholder='<%=Language.equalsIgnoreCase("English") ? "Write Captcha" : "ক্যাপচা লিখুন"%>'
                                                        tag='pb_html'
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div >

                                <div class="col-4 form-actions text-right">
                                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                        <%=Language.equalsIgnoreCase("English") ? "SUBMIT" : "জমা দিন"%>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" id="submit_btn" style="display: none">
                            <div class="form-actions text-right">
                                <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                    <%=Language.equalsIgnoreCase("English") ? "SUBMIT" : "জমা দিন"%>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    let dataLoaded = false;
    const language = '<%=Language%>';
    let error1, error2, error3, error4, error5;
    if (language == 'English') {
        error1 = 'select credential type!';
        error2 = 'enter credential number!';
        error3 = 'enter 11 digit phone number!';
        error4 = 'enter your full name!';
        error5 = 'enter your nationality!';
    } else {
        error1 = 'পরিচয়পত্রের ধরণ বাছাই করুন!';
        error2 = 'পরিচয়পত্রের নম্বর প্রদান করুন!';
        error3 = '১১ সংখ্যার ফোন নম্বর প্রদান করুন!';
        error4 = 'আপনার পুরো নাম প্রবেশ করুন!';
        error5 = 'আপনার জাতীয়তা বাছাই করুন!';
    }

    $('#bigform').on('submit', () => {
        let valid = true;
        if ($('#typeOfVisitor_select2_0').val() == '1') {
            $('.credential-type').each((index, element) => {
                if (!$(element).val().match('[0-9]+')) {
                    $(element).closest('.affliatedPerson').find('.affiliated-type-error').show();
                    $(element).closest('.affliatedPerson').find('.affiliated-type-error').text(error1);
                    valid = false;
                }
            });
            $('.mobile-number').each((index, element) => {
                if (!$(element).val().match('^01[0-9]{9}$') && !$(element).val().match('^০১[০-৯]{9}$')) {
                    $(element).closest('.affliatedPerson').find('.affiliated-mobile-error').show();
                    $(element).closest('.affliatedPerson').find('.affiliated-mobile-error').text(error3);
                    valid = false;
                }
            });
        } else if ($('#typeOfVisitor_select2_0').val() == '2') {
            $('.nationality').each((index, element) => {
                if (!$(element).val().match('[0-9]+')) {
                    $(element).closest('.affliatedPerson').find('.nationality-error').show();
                    $(element).closest('.affliatedPerson').find('.nationality-error').text(error5);
                    valid = false;
                }
            });
        }

        $('.credential-no').each((index, element) => {
            if ($(element).val() == "") {
                $(element).closest('.affliatedPerson').find('.affiliated-id-error').show();
                $(element).closest('.affliatedPerson').find('.affiliated-id-error').text(error2);
                valid = false;
            }
        });
        $('.name').each((index, element) => {
            if ($(element).val() == "") {
                $(element).closest('.affliatedPerson').find('.name-error').show();
                $(element).closest('.affliatedPerson').find('.name-error').text(error4);
                valid = false;
            }
        });

        return valid;
    });

    function PreprocessBeforeSubmiting(row, validate) {

        if (dateValidator('visitingDate_js_0', true))
            preprocessDateBeforeSubmitting('visitingDate', row);
        if (timeNotEmptyValidator('visitingTime_js_' + row))
            preprocessTimeBeforeSubmitting('visitingTime', row);
        let validVisa = true;
        if ($('#typeOfVisitor_select2_0').val() == '2') {
            validVisa = dateValidator('visaExpiry_js_0', true);
            if (validVisa)
                preprocessDateBeforeSubmitting('visaExpiry', row);
        }

        $('#enjoyAssembly').val($('input[name="enjoyAssemblyRadio"]:checked').val());


        $('#localOrForeign_hidden').val($('#typeOfVisitor_select2_0').val());

        return dateValidator('visitingDate_js_0', true) && timeNotEmptyValidator('visitingTime_js_' + row) && validVisa;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Visitor_passServlet");
    }

    function init(row) {
        let today = new Date();
        setDateByStringAndId('visitingDate_js_' + row, $('#visitingDate_date_' + row).val());
        setMinDateById('visitingDate_js_' + row, $('#visitingDate_date_' + row).val());
        resetMaxMinYearById('visitingDate_js_' + row, today.getFullYear() + 1, today.getFullYear());
        setTimeById('visitingTime_js_' + row, '10:15 AM', true);
        setDateByStringAndId('visaExpiry_js_' + row, $('#visaExpiry_date_' + row).val());
        setMinDateById('visaExpiry_js_' + row, $('#visaExpiry_date_' + row).val());
        resetMaxMinYearById('visaExpiry_js_' + row, today.getFullYear() + 1, today.getFullYear());
        $("#typeOfVisitor_select2_" + row).select2({
            dropdownAutoWidth: true
        });
        $("#nationalityType_select2_" + row).select2({
            dropdownAutoWidth: true
        });
        $("#professionType_select2_" + row).select2({
            dropdownAutoWidth: true
        });

        typesOfVisitorChanged();

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        // validation on input
        $.validator.addMethod('isValidMobileNumber', function (value, element) {
            let english = /^[A-Za-z0-9]*$/;
            let mobileNo = $('#mobileNumber').val();
            if (!english.test(mobileNo)) {
                return value.match('[\u09E6-\u09EF]');
            } else {
                return value.match('^01[3-9]{1}[0-9]{8}$');
            }
        });

        $.validator.addMethod('visitorSelection', function (value, element) {
            return value.match('^[0-9]+$');
        });

        $.validator.addMethod('nationalitySelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('officerSelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('credentialSelection', function (value, element) {
            return value != 0;
        });

        $.validator.addMethod('emailValidator', function (value, element) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(value);
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            ignore: ":hidden",
            rules: {
                name: "required",
                passportNo: "required",
                address: "required",
                institutionalLetter: "required",
                typeOfVisitor: {
                    required: true,
                    visitorSelection: true
                },
                nationalityType: {
                    required: true,
                    nationalitySelection: true
                },
                professionType: {
                    required: true,
                    officerSelection: true
                },
                mobileNumber: {
                    required: true,
                    isValidMobileNumber: true
                },
                email: {
                    required: true,
                    emailValidator: true
                }
                // permanentAddress_active: {
                //     required: true,
                //     permanentAddress_activeSelection: true
                // },
                // currentAddress_active: {
                //     required: true,
                //     currentAddress_activeSelection: true
                // }
            },
            messages: {
                name: '<%=Language.equalsIgnoreCase("English") ? "enter your/your companies name!" : "আপনার/প্রতিষ্ঠানের নাম প্রবেশ করুন!"%>',
                address: '<%=Language.equalsIgnoreCase("English") ? "enter your/your companies address!" : "আপনার/প্রতিষ্ঠানের ঠিকানা প্রবেশ করুন!"%>',
                passportNo: '<%=Language.equalsIgnoreCase("English") ? "enter passport number!" : "পাসপোর্ট নাম্বার প্রবেশ করুন!"%>',
                nationalityType: '<%=Language.equalsIgnoreCase("English") ? "select nationality!" : "জাতীয়তা বাছাই করুন!"%>',
                typeOfVisitor: '<%=Language.equalsIgnoreCase("English") ? "select visitor type!" : "ভিজিটের ধরণ বাছাই করুন!"%>',
                professionType: '<%=Language.equalsIgnoreCase("English") ? "select occupation!" : "পেশা বাছাই করুন!"%>',
                mobileNumber: '<%=Language.equalsIgnoreCase("English") ? "mobile number must be 11 digit, ex: 01710101010" : "মোবাইল নাম্বারটি অবশ্যই ১১ ডিজিটের হতে হবে, উদাহরণঃ 01710101010"%>',
                email: '<%=Language.equalsIgnoreCase("English") ? "enter valid email address!" : "যথাযথ ইমেইল এড্রেস প্রবেশ করুন!"%>',
                institutionalLetter: '<%=Language.equalsIgnoreCase("English") ? "upload required file!" : "চিঠি আপলোড করুন!"%>'
                <%--permanentAddress_active: '<%=Language.equalsIgnoreCase("English") ? "select permanent address!" : "স্থায়ী ঠিকানা বাছাই করুন!"%>',--%>
                <%--currentAddress_active: '<%=Language.equalsIgnoreCase("English") ? "select current/official address!" : "বর্তমান/দাপ্তরিক ঠিকানা বাছাই করুন!"%>'--%>
            }
        });


        // fire a swal
        let fireSwal = '<%=fireSwal%>';
        if (fireSwal == 'yes') {
            $(".swal-overlay").css("background-color", "#0080ff");
            $(".swal-title").css("font-size", "32px");

            Swal.fire({
                position: 'center',
                type: 'success',
                text: '<%=Language.equalsIgnoreCase("English") ? "Your Visitor Pass request has been submitted for approval!" : "আপনার সংসদ ভিজিট পাসটি অনুমোদনের জন্য কতৃপক্ষের নিকট পাঠানো হয়েছে!"%>',
                showConfirmButton: false,
                timer: 3000
            });
        }
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    function typesOfVisitorChanged() {
        let types_of_visitor = $('#typeOfVisitor_select2_0').val();

        $('#visiting_date_div').hide();
        $('#visiting_time_div').hide();
        $('#name_div').hide();
        $('#address_div').hide();
        $('#institutional_letter_div').hide();
        $('#nationality_div').hide();
        $('#profession_div').hide();
        $('#passport_div').hide();
        $('#visa_div').hide();
        $('#mobile_number_div').hide();
        $('#email_div').hide();
        $('#hr_line_div').hide();
        $('#enjoy_assembly_div').hide();
        $('#affiliated-persons-add').hide();
        $('#affiliated-persons-add-lebel').hide();
        $('#submit_btn_captcha').hide();
        $('#submit_btn').hide();
        const currentPersonCount = $('#additional_person').val();
        for (let i = 0; i < currentPersonCount; i++) {
            $('#affliatedPerson_div_' + i).remove();
        }
        lastPersonCount = 0;

        if (types_of_visitor == 1) {
            $('#name_label').text('<%=Language.equalsIgnoreCase("English") ? "Organization Name":"প্রতিষ্ঠানের নাম"%>');
            $('#address_label').text('<%=Language.equalsIgnoreCase("English") ? "Organization Address":"প্রতিষ্ঠানের ঠিকানা"%>');

            $("input[name='name']").attr('placeholder','<%=Language.equalsIgnoreCase("English") ? "Enter your organization name" : "প্রতিষ্ঠানের নাম দিন"%>');
            $("input[name='address']").attr('placeholder','<%=Language.equalsIgnoreCase("English") ? "Enter organization address" : "প্রতিষ্ঠানের ঠিকানা দিন"%>');

            $('#visiting_date_div').show();
            $('#visiting_time_div').show();
            $('#name_div').show();
            $('#address_div').show();
            $('#mobile_number_div').show();
            $('#email_div').show();
            $('#institutional_letter_div').show();
            $('#hr_line_div').show();
            $('#enjoy_assembly_div').show();
            $('#affiliated-persons-add').show();
            $('#affiliated-persons-add-lebel').show();
            $('#submit_btn_captcha').show();


        } else if (types_of_visitor == 2) {
            $('#name_label').text('<%=Language.equalsIgnoreCase("English") ? "Name":"নাম"%>');
            $('#address_label').text('<%=Language.equalsIgnoreCase("English") ? "Address":"ঠিকানা"%>');

            $("input[name='name']").attr('placeholder','<%=Language.equalsIgnoreCase("English") ? "Enter name" : "আপনার নাম লিখুন"%>');
            $("input[name='address']").attr('placeholder','<%=Language.equalsIgnoreCase("English") ? "Enter address" : "আপনার ঠিকানা দিন"%>');

            $('#visiting_date_div').show();
            $('#visiting_time_div').show();
            $('#name_div').show();
            $('#address_div').show();
            $('#nationality_div').show();
            $('#profession_div').show();
            $('#passport_div').show();
            $('#visa_div').show();
            $('#mobile_number_div').show();
            $('#email_div').show();
            $('#hr_line_div').show();
            $('#enjoy_assembly_div').show();
            $('#affiliated-persons-add').show();
            $('#affiliated-persons-add-lebel').show();
            $('#submit_btn').show();
        }
    }

    let lastPersonCount = 0;

    $('#additional_person_button').click(() => {
        const currentPersonCount = $('#additional_person').val();
        if (!isNaN(parseInt(currentPersonCount))) {
            for (let i = lastPersonCount - 1; i >= currentPersonCount; i--) {
                $('#affliatedPerson_div_' + i).remove();
            }
            if (lastPersonCount < currentPersonCount) {
                let types_of_visitor = $('#typeOfVisitor_select2_0').val();
                let url;
                if (types_of_visitor == 1) {
                    url = "Visitor_pass_affiliated_personServlet?actionType=getAffiliatedPersonModal" + "&language=<%=Language%>&personCount="
                        + currentPersonCount + "&startVal=" + lastPersonCount;
                } else {
                    url = "Visitor_pass_affiliated_personServlet?actionType=getAffiliatedPersonModalForeign" + "&language=<%=Language%>&personCount="
                        + currentPersonCount + "&startVal=" + lastPersonCount;
                }
                // console.log("office_unit ajax url : " + url);
                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    success: function (data) {

                        $('#add_person_div').append(data);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
            lastPersonCount = currentPersonCount;
            $('#additionalPersonCount').val(lastPersonCount);
        }
    });

    function getcapcha()
    {
        var date2 = Math.floor(Math.random() * 100) + 1;

        var img = document.getElementById("captcha");

        img.src= "<%=request.getContextPath()%>/simpleCaptchaServlet?test="+ date2;
    }

</script>






