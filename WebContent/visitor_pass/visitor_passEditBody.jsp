<%@page import="util.TimeConverter"%>
<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="dbm.DBMW" %>

<%@page import="visitor_pass.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@page import="files.*" %>

<%

    Visitor_passDTO visitor_passDTO;
    visitor_passDTO = (Visitor_passDTO) request.getAttribute("visitor_passDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (visitor_passDTO == null) {
        visitor_passDTO = new Visitor_passDTO();

    }
    System.out.println("visitor_passDTO = " + visitor_passDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VISITOR_PASS_ADD_VISITOR_PASS_ADD_FORMNAME, loginDTO);
    String servletName = "Visitor_passServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VISITOR_PASS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    FilesDAO filesDAO = new FilesDAO();
    long ColumnID;


    String fireSwal = (String) session.getAttribute("fireSwal");
    if (fireSwal == null) {
        fireSwal = "no";
    }
    

//    Now it's time to remove attribute
    session.setAttribute("fireSwal", "no");
    boolean isLangEng = Language.equalsIgnoreCase("english");
    
    long tomorrow = TimeConverter.getTomorrow();
    long nextWeek = TimeConverter.getNthDay(7);
%>

<style>
    hr {
        border: solid 1px black;
        width: 100%;
        color: #050505;
        height: 1px;
    }
    
    .testing_excelDatabase_preview{
    	display: none;
    }
</style>


<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
	
    <div class="" id="modalbody">
        <div class="kt-portlet" style="background-color: #F2F2F2!important;">
	        <div class="kt-portlet__head" >
				<div class="kt-portlet__head-label" >
					<h3 class="kt-portlet__head-title prp-page-title" style="text-align: center !important">
						<%=Language.equalsIgnoreCase("english")?"Parliament Visit Pass":"সংসদ ভিজিট পাস"%>
					</h3>
				</div>
			</div>
            <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
                <div class="py-4 shadow-sm rounded" style="background-color: #FFFFFF">
                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                           value='<%=visitor_passDTO.iD%>' tag='pb_html'/>
                    <div class="d-flex justify-content-center">
                        <h4 class="table-title">
                            <%=LM.getText(LC.VISITOR_PASS_ADD_TYPEOFVISITOR, loginDTO)%>
                        </h4>
                    </div>
                    <div class="d-flex justify-content-center">
                        <select class='form-control w-50 mt-2' name='typeOfVisitor'
                                id='typeOfVisitor_select2_<%=i%>'
                                onchange="typesOfVisitorChanged()"
                                tag='pb_html'>
                            <%=CatRepository.getInstance().buildOptions("visitorPassTypes", Language, null)%>
                        </select>
                    </div>
                </div>
                <div class="py-4 shadow-sm rounded" style="background-color: #FFFFFF" id = "noticeDiv" style = "display:none">
                    
                    <div class="d-flex justify-content-center">
                        <h4 class="table-title">
                            <%=isLangEng?"Notice Period":"নোটিসের ধরন"%>
                        </h4>
                    </div>
                    <div class="d-flex justify-content-center">
                        <select class='form-control w-50 mt-2' name=visitNoticeCat
                                id='visitNoticeCat'                                
                                tag='pb_html'>
                            <%=CatRepository.getInstance().buildOptions("visit_notice", Language, -1)%>
                        </select>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Visitor_passServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="row mt-5">
                        <div class="col-md-6 my-3" id="visiting_date_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGDATE, loginDTO)%>
                            </h5>
                            <div>
                                <%value = "visitingDate_js_" + i;%>
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                </jsp:include>
                                <input type='hidden' name='visitingDate' id='visitingDate_date_<%=i%>'
                                       value='<%=dateFormat.format(new Date(visitor_passDTO.visitingDate))%>'
                                       tag='pb_html'>
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="visiting_time_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGTIME, loginDTO)%>
                            </h5>
                            <div>
                                <%value = "visitingTime_js_" + i;%>
                                <jsp:include page="/time/time.jsp">
                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                </jsp:include>
                                <input
                                        type='hidden'
                                        name='visitingTime'
                                        id='visitingTime_time_<%=i%>'
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="name_div" style="display: none">
                            <h5 id="name_label">

                            </h5>
                            <div>
                                <input type='text' class='form-control' name='name'
                                       id='name_text_<%=i%>'
                                       tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="address_div" style="display: none">
                            <h5 id="address_label">

                            </h5>
                            <div>
                                <input
                                        type='text'
                                        class='form-control'
                                        name='address'
                                        id='address_text_<%=i%>'
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        
                        <div class="col-md-6 my-3" id="office_div" style="display: none">
                            <h5 id="office_level">
								<%=isLangEng?"Office":"অফিস" %>
                            </h5>
                            <div>
                                <input type='text' class='form-control' name='officeName' 
                                       id='officeName_text_<%=i%>' placeholder = '<%=isLangEng?"Enter Office Name":"অফিসের নাম দিন" %>'
                                       tag='pb_html'/>
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="designation_div" style="display: none">
                            <h5 id="designation_level">
								<%=isLangEng?"Designation":"পদবী" %>
                            </h5>
                            <div>
                                <input type='text' class='form-control' name='designation' 
                                       id='designation_text_<%=i%>' placeholder = '<%=isLangEng?"Enter designation":"পদবী দিন" %>'
                                       tag='pb_html'/>
                            </div>
                        </div>
                        
                        <div class="col-md-6 my-3" id="nationality_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_NATIONALITYTYPE, loginDTO)%>
                            </h5>
                            <div>
                                <select
                                        class='form-control'
                                        name='nationalityType'
                                        id='nationalityType_select2_<%=i%>'
                                        tag='pb_html'
                                        style="width: 100%!important;"
                                >
                                    <%
                                        Options = CommonDAO.getOptions(Language, "nationality", -2);
                                    %>
                                    <%=Options%>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="profession_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_PROFESSIONTYPE, loginDTO)%>
                            </h5>
                            <div>
                                <select
                                        class='form-control'
                                        name='professionType'
                                        id='professionType_select2_<%=i%>'
                                        tag='pb_html'
                                        style="width: 100%!important;"
                                >
                                    <%
                                        Options = CommonDAO.getOptions(Language, "profession", -2);
                                    %>
                                    <%=Options%>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="passport_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_PASSPORTNO, loginDTO)%>
                            </h5>
                            <div>
                                <input
                                        type='text'
                                        class='form-control'
                                        name='passportNo'
                                        id='passportNo_text_<%=i%>'
                                        placeholder="<%=Language.equalsIgnoreCase("English") ? "Enter passport number" : "পাসপোর্ট নাম্বার দিন"%>"
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="visa_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_VISAEXPIRY, loginDTO)%>
                            </h5>
                            <div>
                                <%value = "visaExpiry_js_" + i;%>
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                </jsp:include>
                                <input
                                        type='hidden'
                                        name='visaExpiry'
                                        id='visaExpiry_date_<%=i%>'
                                        value='<%=dateFormat.format(new Date(visitor_passDTO.visitingDate))%>'
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="mobile_number_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_MOBILENUMBER, loginDTO)%>
                            </h5>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"
                                          id="basic-addon3"><%=Language.equalsIgnoreCase("English") ? "+88" : "+৮৮"%></span>
                                </div>
                                <input
                                        type='text'
                                        class='form-control'
                                        name='mobileNumber'
                                        placeholder="<%=Language.equalsIgnoreCase("English") ? "Enter mobile number" : "মোবাইল নাম্বার দিন"%>"
                                        id='mobileNumber_text_<%=i%>'
                                        onkeypress="return (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="email_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_EMAIL, loginDTO)%>
                            </h5>
                            <div>
                                <input
                                        type='text'
                                        class='form-control'
                                        name='email'
                                        id='email_text_<%=i%>'
                                        placeholder="<%=Language.equalsIgnoreCase("English") ? "Enter email address" : "ইমেইল এড্রেস দিন"%>"
                                        tag='pb_html'
                                />
                            </div>
                        </div>
                        <div class="col-md-6 my-3" id="institutional_letter_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_INSTITUTIONALLETTER, loginDTO)%>
                            </h5>
                            <div class="col-12  " id='filesDropzone_div_<%=i%>' style="padding: unset; color: #b7bacd">
                                <%
                                    if (actionName.equals("edit")) {
                                        List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(visitor_passDTO.fileID);
                                %>
                                <table>
                                    <tr>
                                        <%
                                            if (filesDropzoneDTOList != null) {
                                                for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                    FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                        %>
                                        <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                            <%
                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                            %> <img
                                                src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                style='width: 100px'/> <%
                                            }
                                        %> <a
                                                href='Visitor_passServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                download><%=filesDTO.fileTitle%>
                                        </a> <a class='btn btn-danger'
                                                onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                        </td>
                                        <%
                                                }
                                            }
                                        %>
                                    </tr>
                                </table>
                                <%
                                    }
                                %>

                                <%
                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                %>
                                <div class="dropzone" style="min-height: auto; border: dotted 1px" id="file_dropzone"
                                     action="Visitor_passServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit") ? visitor_passDTO.fileID : ColumnID%>">
                                    <input type='file' style="display: none"
                                           name='filesDropzoneFile'
                                           id='filesDropzone_dropzone_File_<%=i%>'
                                           tag='pb_html'/>
                                </div>
                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                       id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                       tag='pb_html'/>
                                <input type='hidden' name='filesDropzone'
                                       id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                       value='<%=actionName.equals("edit") ? visitor_passDTO.fileID : ColumnID%>'/>


                            </div>
                          
                        </div>
                        <div class="col-12 my-3 py-3" id="affiliated-persons-add-lebel" style="display: none">
                            <h4>
                                <%=Language.equalsIgnoreCase("English") ? "Address the affiliated persons:" : "আপনার সাথে প্রবেশকারী ব্যক্তিবর্গের পরিচয় দিনঃ"%>
                            </h4>
                            <div>
                            <div class="d-flex justify-content-between align-items-center">
                                <label
                                        class="text-nowrap h5"
                                        id="gatePassSubTypeIdLabel">
                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_ADDITIONAL_NUMBER_OF_PERSON, loginDTO)%>
                                </label>
                                <input type='number'
                                       class='form-control mx-3'
                                       name='additional_person'
                                       id='additional_person'
                                       min="0" value="0" tag='pb_html'/>
                                
                                
                                <button class="btn submit-btn text-white shadow btn-border-radius text-nowrap"
                                        type="button"
                                        id="additional_person_button">

                                    <%=LM.getText(LC.GATE_PASS_VISITOR_ADD_GIVE_THEIR_INFO, loginDTO)%>

                                </button>
                                </div>
                               
                                <div class="d-flex justify-content-end align-items-center mt-3">
                                <h4><%=LM.getText(LC.HM_OR, loginDTO)%></h4>
                                
                                
                                <a class="btn submit-btn text-white shadow btn-border-radius text-nowrap mx-3" id = "sampleXl"
                                        href='Support_ticketServlet?actionType=downloadDropzoneFile&id=<%=Visitor_passDTO.SAMPLE_XL_ID%>'
                                        download>
                                        

                                    <%=Language.equalsIgnoreCase("English") ? "Sample XL" : "স্যাম্পল এক্সেল"%>


                                </a>
                                <br>
                                
                                <input
		                                        type='file'
		                                        style="display:none;"
		                                        class='form-control'
		                                        name='testing_excelDatabase'
		                                        id='testing_excelDatabase'
		                                />
		                                
                                <button type="button" class="btn submit-btn text-white shadow btn-border-radius text-nowrap"
                                        style="color: white" id="loadFileXml"
                                        onclick="uploadClicked()"><i
                                        class="fas fa-cloud-upload-alt"></i><%=LM.getText(LC.CANDIDATE_LIST_UPLOAD_EXCEL, loginDTO) %>
                                </button>
                                
                                <input type='hidden' class='form-control'
                                       name='additionalPersonCount'
                                       id='additionalPersonCount'
                                       value="0" tag='pb_html'/>
                            </div>
                            </div>

                            <div id="add_person_div" class="mt-2">
                            </div>

                        </div>
                        <div class="col-md-6 my-3" id="enjoy_assembly_div" style="display: none">
                            <h5>
                                <%=LM.getText(LC.VISITOR_PASS_ADD_ENJOYASSEMBLY, loginDTO)%>
                            </h5>
                            <div class="d-flex align-items-center">
                                <div>
                                    <input
                                            type="radio"
                                            class="form-group"
                                            id="yes_radio"
                                            name="enjoyAssemblyRadio"
                                            value="1"
                                    />
                                    <label for="yes_radio">
                                        <%=Language.equalsIgnoreCase("English") ? "Yes" : "হ্যা"%>
                                    </label>
                                </div>
                                <div class="ml-3">
                                    <input
                                            type="radio"
                                            class="form-group"
                                            id="no_radio"
                                            name="enjoyAssemblyRadio"
                                            value="0"
                                            checked="checked"
                                    />
                                    <label for="no_radio">
                                        <%=Language.equalsIgnoreCase("English") ? "No" : "না"%>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <input type='hidden' class='form-control' name='enjoyAssembly'
                               id='enjoyAssembly'
                               tag='pb_html'>
                        <input type='hidden' class='form-control' name='paymentStatus'
                               id='paymentStatus_hidden_<%=i%>'
                               value='<%=visitor_passDTO.paymentStatus%>'
                               tag='pb_html'/>
                        <input type='hidden' class='form-control' name='firstLayerApprovalStatus'
                               id='firstLayerApprovalStatus_hidden_<%=i%>'
                               value='<%=visitor_passDTO.firstLayerApprovalStatus%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='firstLayerApprovedBy'
                               id='firstLayerApprovedBy_hidden_<%=i%>'
                               value='<%=visitor_passDTO.firstLayerApprovedBy%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='firstLayerApprovalTime'
                               id='firstLayerApprovalTime_hidden_<%=i%>'
                               value='<%=visitor_passDTO.firstLayerApprovalTime%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='secondLayerApprovalStatus'
                               id='secondLayerApprovalStatus_hidden_<%=i%>'
                               value='<%=visitor_passDTO.secondLayerApprovalStatus%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='secondLayerApprovedBy'
                               id='secondLayerApprovedBy_hidden_<%=i%>'
                               value='<%=visitor_passDTO.secondLayerApprovedBy%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='secondLayerApprovalTime'
                               id='secondLayerApprovalTime_hidden_<%=i%>'
                               value='<%=visitor_passDTO.secondLayerApprovalTime%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='insertedByUserId'
                               id='insertedByUserId_hidden_<%=i%>'
                               value='<%=visitor_passDTO.insertedByUserId%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='insertedByOrganogramId'
                               id='insertedByOrganogramId_hidden_<%=i%>'
                               value='<%=visitor_passDTO.insertedByOrganogramId%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='insertionDate'
                               id='insertionDate_hidden_<%=i%>'
                               value='<%=visitor_passDTO.insertionDate%>'
                               tag='pb_html'/>
                        <input type='hidden' class='form-control' name='lastModifierUser'
                               id='lastModifierUser_hidden_<%=i%>'
                               value='<%=visitor_passDTO.lastModifierUser%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='isDeleted'
                               id='isDeleted_hidden_<%=i%>' value='<%=visitor_passDTO.isDeleted%>'
                               tag='pb_html'/>
                        <input type='hidden' class='form-control' name='lastModificationTime'
                               id='lastModificationTime_hidden_<%=i%>'
                               value='<%=visitor_passDTO.lastModificationTime%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='localOrForeign'
                               id='localOrForeign_hidden'
                               tag='pb_html'/>
                        <div class="col-12" id="submit_btn" style="display: none">
                            <div class="form-actions text-right">
                                <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_VISITOR_PASS_CANCEL_BUTTON, loginDTO)%>
                                </button>
                                <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_VISITOR_PASS_SUBMIT_BUTTON, loginDTO)%>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    let dataLoaded = false;
    const language = '<%=Language%>';
    let error1, error2, error3, error4, error5;
    if (language == 'English') {
        error1 = 'select credential type!';
        error2 = 'enter credential number!';
        error3 = 'enter 11 digit phone number!';
        error4 = 'enter your full name!';
        error5 = 'enter your nationality!';
    } else {
        error1 = 'পরিচয়পত্রের ধরণ বাছাই করুন!';
        error2 = 'পরিচয়পত্রের নম্বর প্রদান করুন!';
        error3 = '১১ সংখ্যার ফোন নম্বর প্রদান করুন!';
        error4 = 'আপনার পুরো নাম প্রবেশ করুন!';
        error5 = 'আপনার জাতীয়তা বাছাই করুন!';
    }

    $('#bigform').on('submit', () => {
        let valid = true;
        if ($('#typeOfVisitor_select2_0').val() == '1') {
        	
            $('.credential-type').each((index, element) => {
                if (!$(element).val().match('[0-9]+')) {
                    $(element).closest('.affliatedPerson').find('.affiliated-type-error').show();
                    $(element).closest('.affliatedPerson').find('.affiliated-type-error').text(error1);
                    valid = false;
                }
            });
            $('.mobile-number').each((index, element) => {
                if (!$(element).val().match('^01[0-9]{9}$') && !$(element).val().match('^০১[০-৯]{9}$')) {
                    $(element).closest('.affliatedPerson').find('.affiliated-mobile-error').show();
                    $(element).closest('.affliatedPerson').find('.affiliated-mobile-error').text(error3);
                    valid = false;
                }
            });
        } else if ($('#typeOfVisitor_select2_0').val() == '2') {
        	
            $('.nationality').each((index, element) => {
                if ($(element).val() == '') {
                    $(element).closest('.affliatedPerson').find('.nationality-error').show();
                    $(element).closest('.affliatedPerson').find('.nationality-error').text(error5);
                    valid = false;
                }
            });
        }

        $('.credential-no').each((index, element) => {
            if ($(element).val() == "") {
                $(element).closest('.affliatedPerson').find('.affiliated-id-error').show();
                $(element).closest('.affliatedPerson').find('.affiliated-id-error').text(error2);
                valid = false;
            }
        });
        $('.name').each((index, element) => {
            if ($(element).val() == "") {
                $(element).closest('.affliatedPerson').find('.name-error').show();
                $(element).closest('.affliatedPerson').find('.name-error').text(error4);
                valid = false;
            }
        });

        return valid;
    });

    function PreprocessBeforeSubmiting(row, validate) {

        if (dateValidator('visitingDate_js_0', true))
            preprocessDateBeforeSubmitting('visitingDate', row);
        
        var visitDateLong = getBDFormattedDate("visitingDate_date_<%=i%>");
        console.log("visitDateLong = " + visitDateLong);
        if(
        		($("#visitNoticeCat").val() == '<%=Visitor_passDTO.SHORT_TIME%>' && visitDateLong < <%=tomorrow%>)
        		||
        		($("#visitNoticeCat").val() != '<%=Visitor_passDTO.SHORT_TIME%>' && visitDateLong < <%=nextWeek%>)
        )
       	{
       		toastr.error("<%=isLangEng?"Visit date is too early":"দর্শনের তারিখ আরও পেছাতে হবে"%>");
       		return false;
       	}
        	
        if (timeNotEmptyValidator('visitingTime_js_' + row))
            preprocessTimeBeforeSubmitting('visitingTime', row);
        let validVisa = true;
        if ($('#typeOfVisitor_select2_0').val() == '2') {
            validVisa = dateValidator('visaExpiry_js_0', true);
            if (validVisa)
                preprocessDateBeforeSubmitting('visaExpiry', row);
        }

        $('#enjoyAssembly').val($('input[name="enjoyAssemblyRadio"]:checked').val());


        $('#localOrForeign_hidden').val($('#typeOfVisitor_select2_0').val());

        return dateValidator('visitingDate_js_0', true) && timeNotEmptyValidator('visitingTime_js_' + row) && validVisa;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Visitor_passServlet");
    }

    function init(row) {
        let today = new Date();
        setDateByStringAndId('visitingDate_js_' + row, $('#visitingDate_date_' + row).val());
        setMinDateById('visitingDate_js_' + row, $('#visitingDate_date_' + row).val());
        resetMaxMinYearById('visitingDate_js_' + row, today.getFullYear() + 1, today.getFullYear());
        setTimeById('visitingTime_js_' + row, '10:15 AM', true);
        setDateByStringAndId('visaExpiry_js_' + row, $('#visaExpiry_date_' + row).val());
        setMinDateById('visaExpiry_js_' + row, $('#visaExpiry_date_' + row).val());
        resetMaxMinYearById('visaExpiry_js_' + row, today.getFullYear() + 1, today.getFullYear());
       
      
        $("#professionType_select2_" + row).select2({
            dropdownAutoWidth: true
        });

        typesOfVisitorChanged();

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        
        $('#testing_excelDatabase').on("change", function () {
            uploadFile();
        });
		$("#visitNoticeCat").val('<%=Visitor_passDTO.LONG_TIME%>');
        // validation on input
        $.validator.addMethod('isValidMobileNumber', function (value, element) {
            let english = /^[A-Za-z0-9]*$/;
            let mobileNo = $('#mobileNumber').val();
            if (!english.test(mobileNo)) {
                return value.match('[\u09E6-\u09EF]');
            } else {
                return value.match('^01[3-9]{1}[0-9]{8}$');
            }
        });

        $.validator.addMethod('visitorSelection', function (value, element) {
            return value.match('^[0-9]+$');
        });

        $.validator.addMethod('nationalitySelection', function (value, element) {
            return value != '';
        });


        $.validator.addMethod('officerSelection', function (value, element) {
            return value != 0;
        });


        $.validator.addMethod('credentialSelection', function (value, element) {
            return value != 0;
        });

        $.validator.addMethod('emailValidator', function (value, element) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(value);
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            ignore: ":hidden",
            rules: {
                name: "required",
                passportNo: "required",
                address: "required",
                officeName: "required",
                designation: "required",
                institutionalLetter: "required",
                typeOfVisitor: {
                    required: true,
                    visitorSelection: true
                },
                nationalityType: {
                    required: true,
                    nationalitySelection: true
                },
                professionType: {
                    required: true,
                    officerSelection: true
                },
                mobileNumber: {
                    required: true,
                    isValidMobileNumber: true
                },
                email: {
                    required: true,
                    emailValidator: true
                }
                // permanentAddress_active: {
                //     required: true,
                //     permanentAddress_activeSelection: true
                // },
                // currentAddress_active: {
                //     required: true,
                //     currentAddress_activeSelection: true
                // }
            },
            messages: {
                name: '<%=Language.equalsIgnoreCase("English") ? "enter your/your companies name!" : "আপনার/প্রতিষ্ঠানের নাম প্রবেশ করুন!"%>',
                address: '<%=Language.equalsIgnoreCase("English") ? "enter your/your companies address!" : "আপনার/প্রতিষ্ঠানের ঠিকানা প্রবেশ করুন!"%>',
                passportNo: '<%=Language.equalsIgnoreCase("English") ? "enter passport number!" : "পাসপোর্ট নাম্বার প্রবেশ করুন!"%>',
                officeName: '<%=Language.equalsIgnoreCase("English") ? "enter office!" : "অফিস যোগ করুন!"%>',
                designation: '<%=Language.equalsIgnoreCase("English") ? "enter designation!" : "পদবী যোগ করুন!"%>',
                nationalityType: '<%=Language.equalsIgnoreCase("English") ? "select nationality!" : "জাতীয়তা বাছাই করুন!"%>',
                typeOfVisitor: '<%=Language.equalsIgnoreCase("English") ? "select visitor type!" : "ভিজিটের ধরণ বাছাই করুন!"%>',
                professionType: '<%=Language.equalsIgnoreCase("English") ? "select occupation!" : "পেশা বাছাই করুন!"%>',
                mobileNumber: '<%=Language.equalsIgnoreCase("English") ? "mobile number must be 11 digit, ex: 01710101010" : "মোবাইল নাম্বারটি অবশ্যই ১১ ডিজিটের হতে হবে, উদাহরণঃ 01710101010"%>',
                email: '<%=Language.equalsIgnoreCase("English") ? "enter valid email address!" : "যথাযথ ইমেইল এড্রেস প্রবেশ করুন!"%>',
                institutionalLetter: '<%=Language.equalsIgnoreCase("English") ? "upload required file!" : "চিঠি আপলোড করুন!"%>'
                <%--permanentAddress_active: '<%=Language.equalsIgnoreCase("English") ? "select permanent address!" : "স্থায়ী ঠিকানা বাছাই করুন!"%>',--%>
                <%--currentAddress_active: '<%=Language.equalsIgnoreCase("English") ? "select current/official address!" : "বর্তমান/দাপ্তরিক ঠিকানা বাছাই করুন!"%>'--%>
            }
        });


        // fire a swal
        let fireSwal = '<%=fireSwal%>';
        if (fireSwal == 'yes') {
            $(".swal-overlay").css("background-color", "#0080ff");
            $(".swal-title").css("font-size", "32px");

            Swal.fire({
                position: 'center',
                type: 'success',
                text: '<%=Language.equalsIgnoreCase("English") ? "Your Visitor Pass request has been submitted for approval!" : "আপনার সংসদ ভিজিট পাসটি অনুমোদনের জন্য কতৃপক্ষের নিকট পাঠানো হয়েছে!"%>',
                showConfirmButton: false,
                timer: 3000
            });
        }
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    function typesOfVisitorChanged() {
        let types_of_visitor = $('#typeOfVisitor_select2_0').val();
        

        $('#visiting_date_div').hide();
        $('#visiting_time_div').hide();
        $('#name_div').hide();
        $('#address_div').hide();
        $('#institutional_letter_div').hide();
        $('#nationality_div').hide();
        $('#profession_div').hide();
        $('#passport_div').hide();
        $('#visa_div').hide();
        $('#mobile_number_div').hide();
        $('#email_div').hide();
        $('#hr_line_div').hide();
        $('#enjoy_assembly_div').hide();
        $('#affiliated-persons-add').hide();
        $('#affiliated-persons-add-lebel').hide();
        $('#submit_btn').hide();
        $("#noticeDiv").hide();
        const currentPersonCount = $('#additional_person').val();
        for (let i = 0; i < currentPersonCount; i++) {
            $('#affliatedPerson_div_' + i).remove();
        }
        lastPersonCount = 0;

        if (types_of_visitor == 1) {
        	console.log("1 selected");
        	$("#sampleXl").attr("href", "Support_ticketServlet?actionType=downloadDropzoneFile&id=<%=Visitor_passDTO.SAMPLE_XL_ID%>");
        	
            $('#name_label').text('<%=Language.equalsIgnoreCase("English") ? "Name":"নাম"%>');
            $('#address_label').text('<%=Language.equalsIgnoreCase("English") ? "Address":"ঠিকানা"%>');

            $("input[name='name']").attr('placeholder', '<%=Language.equalsIgnoreCase("English") ? "Enter name" : "নাম দিন"%>');
            $("input[name='address']").attr('placeholder', '<%=Language.equalsIgnoreCase("English") ? "Enter address" : "ঠিকানা দিন"%>');

            $('#visiting_date_div').show();
            $('#visiting_time_div').show();
            $('#name_div').show();
            $('#address_div').show();
			$('#office_div').show();
            $('#designation_div').show();
            $('#mobile_number_div').show();
            $('#email_div').show();
            $('#institutional_letter_div').show();
            $('#hr_line_div').show();
            $('#enjoy_assembly_div').show();
            $('#affiliated-persons-add').show();
            $('#affiliated-persons-add-lebel').show();
            $('#submit_btn').show();
            $("#noticeDiv").hide();

        } else if (types_of_visitor == 2) {
			console.log("2 selected");
        	$("#sampleXl").attr("href", "Support_ticketServlet?actionType=downloadDropzoneFile&id=<%=Visitor_passDTO.FOREIGN_XL_ID%>");
        	
            $('#name_label').text('<%=Language.equalsIgnoreCase("English") ? "Name":"নাম"%>');
            $('#address_label').text('<%=Language.equalsIgnoreCase("English") ? "Address":"ঠিকানা"%>');

            $("input[name='name']").attr('placeholder', '<%=Language.equalsIgnoreCase("English") ? "Enter name" : "আপনার নাম লিখুন"%>');
            $("input[name='address']").attr('placeholder', '<%=Language.equalsIgnoreCase("English") ? "Enter address" : "আপনার ঠিকানা দিন"%>');

            $('#visiting_date_div').show();
            $('#visiting_time_div').show();
            $('#name_div').show();
            $('#address_div').show();
			$('#office_div').show();
            $('#designation_div').show();
            $('#nationality_div').show();
            $('#profession_div').show();
            $('#passport_div').show();
            $('#visa_div').show();
            $('#mobile_number_div').show();
            $('#email_div').show();
            $('#hr_line_div').show();
            $('#enjoy_assembly_div').show();
            $('#affiliated-persons-add').show();
            $('#affiliated-persons-add-lebel').show();
            $('#submit_btn').show();
            $("#noticeDiv").show();
        }
    }

    let lastPersonCount = 0;
	let setData = false;
    $('#additional_person_button').click(() => {
        const currentPersonCount = $('#additional_person').val();
        if (!isNaN(parseInt(currentPersonCount))) {
        	$("#add_person_div").html("");
            if (true) {
                let types_of_visitor = $('#typeOfVisitor_select2_0').val();
                let url;
                if (types_of_visitor == 1) {
                    url = "Visitor_pass_affiliated_personServlet?actionType=getAffiliatedPersonModal" + "&language=<%=Language%>&personCount="
                        + currentPersonCount + "&startVal=" + lastPersonCount;
                } else {
                    url = "Visitor_pass_affiliated_personServlet?actionType=getAffiliatedPersonModalForeign" + "&language=<%=Language%>&personCount="
                        + currentPersonCount + "&startVal=" + lastPersonCount;
                }
                // console.log("office_unit ajax url : " + url);
                $.ajax({
                    url: url,
                    type: "GET",
                    async: true,
                    success: function (data) {

                        $('#add_person_div').append(data);
                        if(setData)
                        {
                        	for (let i = 0; i < visitor_pass_affiliated_personDTOList.length; i++) {
                        		console.log("name = " + visitor_pass_affiliated_personDTOList[i].name);
                        		if($('#typeOfVisitor_select2_0').val() == '1')
                        		{
                        			$("#mobileNumber_" + i).val(visitor_pass_affiliated_personDTOList[i].mobileNumber);
                          		    $("#credentialType_" + i).val(visitor_pass_affiliated_personDTOList[i].credentialCat);
                        		}
                        		else
                        		{
                        			$("#nationality_" + i).val(visitor_pass_affiliated_personDTOList[i].nationality);
                        		
                        		}
                        		  
                        		$("#credentialNo_" + i).val(visitor_pass_affiliated_personDTOList[i].credentialNo);
                        		$("#name_" + i).val(visitor_pass_affiliated_personDTOList[i].name);
                        	}
                        	setData = false;
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
            
            $('#additionalPersonCount').val(currentPersonCount);
        }
    });
    var visitor_pass_affiliated_personDTOList = null;
    function uploadFile() {
        console.log('submitAjax called');

        var formData = new FormData();
        var value;


        console.log('uploadFile called');

        formData.append('testing_excelDatabase', document.getElementById('testing_excelDatabase').files[0]);


        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	
                if (this.responseText != '') {
                	$("#add_person_div").html("");
                	visitor_pass_affiliated_personDTOList = JSON.parse(this.responseText);
                	console.log("visitor_pass_affiliated_personDTOList length " + visitor_pass_affiliated_personDTOList.length);
                	$("#additional_person").val(visitor_pass_affiliated_personDTOList.length);
                	setData = true;
                	$("#additional_person_button").click();
                	$("#testing_excelDatabase").val(null);
                	
                	
                } else {
                    console.log("No Response");
                    //document.getElementById('reviewDiv').innerHTML = this.responseText;
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", 'Visitor_passServlet?actionType=upload&visitorType=' + $('#typeOfVisitor_select2_0').val(), true);
        xhttp.send(formData);
    }
    
    function uploadClicked()
    {
    	console.log("uploadClicked()");
    	document.getElementById('testing_excelDatabase').click();
    }


</script>






