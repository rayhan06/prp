<%@page pageEncoding="UTF-8" %>
<%@ page import="org.apache.commons.lang3.ArrayUtils" %>

<%
    String language = "Bangla";
    String headerLanguage = "Bangla";

    String pageTitle = "Add Visitor Pass";

    String context = request.getContextPath() + "/";
    String pluginsContext = context + "assets/global/plugins/";
    request.setAttribute("context", context);
    request.setAttribute("pluginsContext", pluginsContext);

    int my_language = 1; // 1 for Bangla, taking constant in initial development process
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>
        <%=pageTitle%>
    </title>
    <%@ include file="../skeleton/head.jsp" %>
    <%
        String[] cssStr = request.getParameterValues("css");
        for (int i = 0; ArrayUtils.isNotEmpty(cssStr) && i < cssStr.length; i++) {
    %>
    <link href="${context}<%=cssStr[i]%>" rel="stylesheet" type="text/css"/>
    <%}%>
    <script type="text/javascript">
        var context = '${context}';
        var pluginsContext = '${pluginsContext}';
    </script>
</head>

<body class="">
<input type="hidden" id="my_language" value="<%=my_language%>"/>

<div class="container w-100 mt-2">
    <%--header div--%>
    <div class="row mt-3">
        <div class="col-12">
            <h1 class="text-center">
                <%=language.equalsIgnoreCase("English") ? "Bangladesh National Parliament" : "বাংলাদেশ জাতীয় সংসদ"%>
            </h1>
            <div class="text-center">
                <img src="<%=context%>assets/static/parliament_logo.png" alt="logo" class="img-fluid" width="15%"/>
            </div>
        </div>
    </div>

    <%@include file="../visitor_pass/visitor_passPublicEditBody.jsp"%>


    <%@ include file="../skeleton/footer.jsp"%>
</div>


<%@ include file="../skeleton/includes.jsp" %>
<%
    String[] helpers = request.getParameterValues("helpers");
    for (int j = 0; ArrayUtils.isNotEmpty(helpers) && j < helpers.length; j++) {
%>
<jsp:include page="<%=helpers[j] %>" flush="true">
    <jsp:param name="helper" value="<%=j %>"/>
</jsp:include>
<%}%>
<%
    String[] jsStr = request.getParameterValues("js");
    for (int j = 0; ArrayUtils.isNotEmpty(jsStr) && j < jsStr.length; j++) {
%>
<script src="${context}<%=jsStr[j]%>" type="text/javascript"></script>
<%}%>

<div id="toast_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;"><i class="fa fa-times" onclick="onClickHideToast()"></i></div>
    <div id="toast_message_div"></div>
</div>
<div id="error_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;"><i class="fa fa-times" onclick="onClickHideToast()"></i></div>
    <div id="error_message_div"></div>
</div>

<script>
    var success_message_bng = 'সম্পন্ন হয়েছে';
    var success_message_eng = 'Success';

    var failed_message_bng = 'ব্যর্থ হয়েছে';
    var failed_message_eng = 'Failed';

    var fill_all_input_bng = "অনুগ্রহ করে সব তথ্য যথাযথ ভাবে দিন";
    var fill_all_input_eng = "Please fill all input correctly";

    var no_data_found_eng = "No data found";
    var no_data_found_bng = "কোন তথ্য পাওয়া যায় নি";

    var inplaceSubmitButton = '<%=language.equalsIgnoreCase("English") ? "Submit" : "সাবমিট"%>';

    function showToast(bn, en) {
        const x = document.getElementById("toast_message_div");
        x.innerHTML = '';
        const my_language = document.getElementById('my_language').value;

        x.innerHTML = parseInt(my_language) === 1 ? bn : en;
        document.getElementById('toast_message').classList.add("show");

        setTimeout(function () {
            document.getElementById('toast_message').classList.remove("show");
        }, 3000);
    }

    function showError(bn, en) {
        document.getElementById("error_message_div").innerHTML = ('');
        const my_language = document.getElementById('my_language').value;

        document.getElementById("error_message_div").innerHTML = parseInt(my_language) === 1 ? bn : en;
        document.getElementById('error_message').classList.add("show");

        setTimeout(function () {
            document.getElementById('error_message').classList.remove("show");
        }, 3000);
    }

    function getLanguageBaseText(en, bn) {
        const my_language = document.getElementById('my_language').value;
        return parseInt(my_language) === 1 ? (bn == undefined || bn == null ? "পাওয়া যায় নি" : bn) : (en == undefined || en == null ? "Not found" : en);
    }

    function onClickHideToast() {
        document.getElementById('toast_message').classList.remove("show");
        document.getElementById('error_message').classList.remove("show");
    }
</script>

<script>
    var global_my_language = '1';

    function isNullOrNegativeValue(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === undefined || arr[i] === null || arr[i] == '' || arr[i].length == 0 || arr[i] == '-1') return true;
        }
        return false;
    }

    function optional_select(op) {
        const my_language = document.getElementById('my_language').value;
        op = new Option(my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --", '-1');
        select_2_call();
    }

    $(document).ready(function () {
        global_my_language = document.getElementById('my_language').value;
        /*$("select").select2({
            dropdownAutoWidth: true,
            placeholder: my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --"
        });*/

        activateMenu();
    });

    function getDataTable() {
        $('#tableData').DataTable({
            dom: 'B',
            "ordering": false,
            buttons: [
                {
                    extend: 'excel',
                    charset: 'UTF-8'
                }, {
                    extend: 'print',
                    charset: 'UTF-8'
                }
            ]
        });
        document.getElementById('tableData_wrapper').classList.add("pull-right");
        document.getElementById('tableData').classList.remove("no-footer");
    }

</script>

<script src="<%=context%>ek_sheba_common/ek_sheba_commom_js.js" type="text/javascript"></script>
<script src="<%=context%>ek_sheba_common/ek_sheba_geo.js" type="text/javascript"></script>

<style>
    .select2-container--bootstrap .select2-selection--single .select2-selection__rendered {
        padding-top: 7px;
    }

    .custom-toast {
        visibility: hidden;
        width: 300px;
        min-height: 60px;
        color: #fff;
        text-align: center;
        padding: 20px 30px;
        position: fixed;
        z-index: 1;
        right: 23px;
        bottom: 60px;
        font-size: 13pt;
    }

    #toast_message {
        background-color: #008aa6;
    }

    #error_message {
        background-color: #ca5e59;
    }

    #toast_message.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    #error_message.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    @-webkit-keyframes fadein {
        from {
            right: -38%;
            opacity: 0;
        }
        to {
            right: 23px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            right: -38%;
            opacity: 0;
        }
        to {
            right: 23px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            right: 23px;
            opacity: 1;
        }
        to {
            right: -38%;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            right: 23px;
            opacity: 1;
        }
        to {
            right: -38%;
            opacity: 0;
        }
    }

    .modal {
    / / position: absolute;
        float: left;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        width: 100%;
        height: 100%;
    }

</style>
</body>
</html>


<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
</script>






