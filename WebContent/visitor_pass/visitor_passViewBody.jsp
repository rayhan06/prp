<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="visitor_pass.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="dbm.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="nationality.NationalityDAO" %>
<%@ page import="nationality.NationalityRepository" %>
<%@ page import="profession.ProfessionRepository" %>
<%@ page import="visitor_pass_affiliated_person.Visitor_pass_affiliated_personDTO" %>
<%@ page import="visitor_pass_affiliated_person.Visitor_pass_affiliated_personDAO" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@page import="user.*" %>

<%
    String servletName = "Visitor_passServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String actionName = "edit";
    long ColumnID;
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VISITOR_PASS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Visitor_passDAO visitor_passDAO = new Visitor_passDAO("visitor_pass");
    Visitor_passDTO visitor_passDTO = visitor_passDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    String context = request.getContextPath() + "/";

    Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO = new Visitor_pass_affiliated_personDAO();
    List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = visitor_pass_affiliated_personDAO.getPersonsByVisitorPassId(id);
%>

<style>
    body {
        font-family: Roboto, SolaimanLipi !important;
    }

    .title {
        color: #0098bf !important;
        font-weight: bold;
    }
</style>

<!-- begin:: Subheader -->
<%--<div class="ml-auto mr-3 mt-4">--%>
<%--    <button--%>
<%--            type="button"--%>
<%--            class="btn" id='printer'--%>
<%--            onclick="downloadPdf('<%=gatePassModel.visitorName%>('+ '<%=gatePassModel.visitingDateEng%>' +').pdf','modalbody')"--%>
<%--    >--%>
<%--        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--    </button>--%>
<%--    <button--%>
<%--            type="button"--%>
<%--            class="btn"--%>
<%--            id='printer2'--%>
<%--            onclick="printAnyDiv('modalbody')"--%>
<%--    >--%>
<%--        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--    </button>--%>
<%--</div>--%>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="" id="modalbody">
        <div class="kt-portlet" style="background-color: #F2F2F2!important;">
            <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
                <div class="row">
                    <div class="col-md-3 mb-4 mb-md-0">
                        <span class="lead font-weight-bold"
                              style="color: #0098bf !important"><%=LM.getText(LC.GATE_PASS_VIEW_GATE_NO, loginDTO)%>
                            :
                            ......................................
                        </span>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center">
                        <div class="text-center">
                            <img
                                    width="30%"
                                    src="<%=context%>assets/static/parliament_logo.png"
                                    alt="logo"
                                    class="logo-default"
                            />
                            <h2 class="mt-3" style="color: #0098bf !important">
                                বাংলাদেশ জাতীয় সংসদ
                            </h2>
                            </h1>
                            <h5 class="mt-3">
                                সংসদ ভিজিট পাস
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="row">
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGDATE, loginDTO)%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%
                                    value = visitor_passDTO.visitingDate + "";
                                %>
                                <%
                                    String formatted_visitingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_visitingDate, Language)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGTIME, loginDTO)%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%
                                    value = visitor_passDTO.visitingTime + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=Language.equalsIgnoreCase("English") ? "Type of Visitor" : "দর্শনার্থীর ধরণ"%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? (Language.equalsIgnoreCase("English") ? "Local" : "স্থানীয়") :
                                        (Language.equalsIgnoreCase("English") ? "Foreigner" : "বিদেশী")%>
                            	
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? (Language.equalsIgnoreCase("English") ? "Organization Name" : "প্রতিষ্ঠানের নাম") :
                                            (Language.equalsIgnoreCase("English") ? "Name" : "নাম")%>
                                		:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.name%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? (Language.equalsIgnoreCase("English") ? "Organization Address" : "প্রতিষ্ঠানের ঠিকানা") :
                                            (Language.equalsIgnoreCase("English") ? "Address" : "ঠিকানা")%>
                                		:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.address%>
                            </div>
                        </div>
                        
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=Language.equalsIgnoreCase("English") ? "Office" : "অফিস"%>
                                		:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.officeName%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=Language.equalsIgnoreCase("English") ? "Designation" : "পদবী"%>
                                		:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.designation%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_MOBILENUMBER, loginDTO)%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=Utils.getDigits(visitor_passDTO.mobileNumber.substring(2), Language)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_EMAIL, loginDTO)%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.email%>
                            </div>
                        </div>
                        <%
                            if (visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_FOREIGNER) {
                        %>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_NATIONALITYTYPE, loginDTO)%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=NationalityRepository.getInstance().getText(Language, visitor_passDTO.nationalityType)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_PROFESSIONTYPE, loginDTO)%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=ProfessionRepository.getInstance().getText(Language, visitor_passDTO.professionType)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_PASSPORTNO, loginDTO)%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=Utils.getDigits(visitor_passDTO.passportNo, Language)%>
                            </div>
                        </div>
                        <div class="col-12 row my-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_VISAEXPIRY, loginDTO)%>:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%
                                    value = visitor_passDTO.visaExpiry + "";
                                %>
                                <%
                                    formatted_visitingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_visitingDate, Language)%>
                            </div>
                        </div>
                        <%
                            }

                            if (visitor_pass_affiliated_personDTOList.size() > 0) {
                        %>
                        <div class="col-12 row">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=Language.equalsIgnoreCase("English") ? "Companions" : "সহচরবৃন্দ"%>
                                	:
                                </label>
                            </div>
                            <div class="col-3">
                                <span class="title">
                                  <%=LM.getText(LC.VISITOR_PASS_ADD_NAME, loginDTO)%>:
                                </span>
                            </div>
                            <div class="col-3">
                                <span class="title">
                                  <%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_CREDENTIALNO, loginDTO)%>:
                                </span>
                            </div>
                            <div class="col-md-2">
                                <span class="title">
                                  <%=LM.getText(LC.VISITOR_PASS_ADD_NATIONALITYTYPE, loginDTO)%>:
                                </span>
                            </div>
                            <div class="col-md-2">
                                <span class="title">
                                    <%=LM.getText(LC.VISITOR_PASS_ADD_MOBILENUMBER, loginDTO)%>:
                                </span>
                            </div>
                        </div>
                        <%
                            }
                            for (Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO : visitor_pass_affiliated_personDTOList) {
                        %>
                        <div class="col-12 row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-3">
                                <%=visitor_pass_affiliated_personDTO.name%>
                            </div>
                            <div class="col-3">
                                <%=Utils.getDigits(visitor_pass_affiliated_personDTO.credentialNo, Language)%>
                            </div>
                            <div class="col-md-2">
                                <%=visitor_pass_affiliated_personDTO.nationality%>
                            </div>
                            <div class="col-md-2">
                                <%=visitor_passDTO.typeOfVisitor == Visitor_passDAO.VISITOR_TYPE_LOCAL ? Utils.getDigits(visitor_pass_affiliated_personDTO.mobileNumber, Language) : ""%>
                            </div>
                        </div>
                        <%
                            }
                        %>
                        <div class="col-12 row mt-2">
                            <div class="col-md-2">
                                <label class="title" for="">
                                    <%=Language.equalsIgnoreCase("English") ? "Watch Assembly" : "অধিবেশন দেখতে চান"%>
                                	:
                                </label>
                            </div>
                            <div class="col-md-9">
                                <%=visitor_passDTO.enjoyAssembly == 1 ? (Language.equalsIgnoreCase("English") ? "Yes" : "হ্যাঁ") :
                                        (Language.equalsIgnoreCase("English") ? "No" : "না")%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12">
                        <div class="button-group text-right">
                           <%
					            if (visitor_passDTO.firstLayerApprovalStatus == Visitor_passDAO.approvalStatusPending && userDTO.roleID == SessionConstants.VISITOR_APROVER) {
					        %>
					        <button type="button" id='<%=i%>_button_approve' class="btn-sm border-0 shadow text-white submit-btn"
					                style="border-radius: 8px;" data-toggle="tooltip" data-placement="top" title=""
					                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>', '<%=visitor_passDTO.firstLayerApprovalStatus%>', '<%=visitor_passDTO.secondLayerApprovalStatus%>', '<%=visitor_passDTO.visitingDate%>', '<%=visitor_passDTO.visitingTime%>')">
					            <%=Language.equalsIgnoreCase("English") ? "VERIFY" : "যাচাই করুন"%>
					        </button>
					        <button type="button" id='<%=i%>_button_dismiss' class="btn-sm border-0 shadow text-white cancel-btn"
					                style="border-radius: 8px;" data-toggle="tooltip" data-placement="top" title=""
					                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>', '<%=visitor_passDTO.firstLayerApprovalStatus%>')">
					            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
					        </button>
					        <%
					        } 
					            else if (visitor_passDTO.firstLayerApprovalStatus == Visitor_passDAO.approvalStatusApproved 
					            		&& visitor_passDTO.secondLayerApprovalStatus != Visitor_passDAO.approvalStatusApproved
					            		&& userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE) {
					        %>
					        <button type="button" id='<%=i%>_button_approve' class="btn-sm border-0 shadow text-white submit-btn"
					                style="border-radius: 8px;background-color: #555555;" data-toggle="tooltip" data-placement="top"
					                title=""
					                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>', '<%=visitor_passDTO.firstLayerApprovalStatus%>', '<%=visitor_passDTO.secondLayerApprovalStatus%>', '<%=visitor_passDTO.visitingDate%>', '<%=visitor_passDTO.visitingTime%>')"
					                disabled>
					            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদন"%>
					        </button>
					        <button type="button" id='<%=i%>_button_dismiss' class="btn-sm border-0 shadow text-white cancel-btn"
					                style="border-radius: 8px;background-color: #555555;" data-toggle="tooltip" data-placement="top"
					                title=""
					                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>', '<%=visitor_passDTO.firstLayerApprovalStatus%>')"
					                disabled>
					            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
					        </button>
					        <%
					        } 
					        %>
        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="visitor-pass-approval-letter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"
                    style="font-size: large; font-weight: bolder;"><%=Language.equalsIgnoreCase("English") ?
                        "UPLOAD APPROVAL FILE" : "অনুমোদন ফাইল আপলোড করুন"%>
                </h5>
            </div>
            <div class="modal-body">
                <form id="visitor-pass-approval-letter-form" method="post">
                    <div class="form-body">
                        <label class="form-group control-label">
                            <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGDATE, loginDTO)%>
                        </label>
                        <div class="form-group ml-3">
                            <div class="" id='visitingDate_div'>
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID"
                                               value="visitingDate_js"></jsp:param>
                                    <jsp:param name="LANGUAGE"
                                               value="<%=Language%>"></jsp:param>
                                </jsp:include>
                                <input type='hidden' class='form-control'
                                       id='visitingDate' name='visitingDate' value=''
                                       tag='pb_html'/>
                            </div>
                        </div>
                    </div>

                    <div class="form-body">
                        <label class="form-group control-label">
                            <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGTIME, loginDTO)%>
                        </label>
                        <div class="form-group ml-3">
                            <div class="" id='visitingTime_div'>
                                <jsp:include page="/time/time.jsp">
                                    <jsp:param name="TIME_ID" value="visitingTime_js"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                </jsp:include>
                                <input type='hidden' name='visitingTime'
                                       id='visitingTime'
                                       tag='pb_html'/>
                            </div>
                        </div>
                    </div>

                    <div class="form-body">
                        <label class="form-group control-label">
                            <%=Language.equalsIgnoreCase("English") ? "Approval File" : "অনুমোদন ফাইল"%>
                        </label>
                        <div class="col-12  " id='filesDropzone_div_<%=i%>' style="padding: unset; color: #b7bacd">
                            <%
                                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                            %>
                            <div class="dropzone" style="min-height: auto; border: dotted 1px" id="file_dropzone"
                                 action="Visitor_passServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=ColumnID%>">
                                <input type='file' style="display: none"
                                       name='filesDropzoneFile'
                                       id='filesDropzone_dropzone_File_<%=i%>'
                                       tag='pb_html'/>
                            </div>
                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                   id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                   tag='pb_html'/>
                            <input type='hidden' name='filesDropzone'
                                   id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                   value='<%=ColumnID%>'/>
                        </div>
                      
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn submit-btn text-white shadow btn-border-radius" id="modal_submit"
                        onclick="modalDataSubmit()"><%=Language.equalsIgnoreCase("English") ? "Upload and Send" : "আপলোড এবং প্রেরণ"%>
                </button>
            </div>
        </div>
    </div>
</div>


<script>

    let url, global_approve_button_id, global_dismiss_button_id, global_visitor_pass_id;

    function approval(approve_button_id, dismiss_button_id, visitor_pass_id, approval_status, second_layer_status) {

        if (approval_status == '2' || approval_status == '0') {
            url = '<%=request.getContextPath()%>' + '/' + "Visitor_passServlet?actionType=visitorPassFirstLayerApprovalFromView" + "&visitorPassId=" + visitor_pass_id + "&approvalstatus=approved";
            document.location.href = url;
        } else {
            global_visitor_pass_id = visitor_pass_id;

            setDateByTimestampAndId('visitingDate_js', '<%=visitor_passDTO.visitingDate%>');
            setTimeById('visitingTime_js', '<%=visitor_passDTO.visitingTime%>', true);

            let today = new Date();
            setMinDateByTimestampAndId('visitingDate_js', today.getTime());

            $('#visitor-pass-approval-letter').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    }

    function dismissal(dismiss_button_id, approve_button_id, visitor_pass_id, approval_status) {
        url = "Visitor_passServlet?actionType=visitorPassFirstLayerApprovalFromView" + "&visitorPassId=" + visitor_pass_id + "&approvalstatus=dismiss";
        document.location.href = '<%=request.getContextPath()%>' + '/' + url;
    }

    function modalDataSubmit() {
        if (dateValidator('visitingDate_js', true) == false || timeNotEmptyValidator('visitingTime_js') == false)
            return;
        $('#visitor-pass-approval-letter').modal('hide');
        url = "Visitor_passServlet?actionType=visitorPassFirstLayerApprovalFromView" + "&visitorPassId=" + global_visitor_pass_id + "&approvalstatus=approved&approvalFile=" + $('#filesDropzone_dropzone_0').val();
        url += "&visitingDate=" + getDateTimestampById('visitingDate_js') + "&visitingTime=" + getTimeById('visitingTime_js', true);
        document.location.href = '<%=request.getContextPath()%>' + '/' + url;
    }
</script>