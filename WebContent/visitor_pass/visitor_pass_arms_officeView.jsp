<%
    String context = "../../.."  + request.getContextPath() + "/";
%>
<link href="<%=context%>/assets/css/pagecustom.css" rel="stylesheet" type="text/css"/>
<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="View Visitor Pass (Serjeant-at-Arms)" />
    <jsp:param name="body" value="../visitor_pass/visitor_pass_arms_officeViewBody.jsp" />
</jsp:include>