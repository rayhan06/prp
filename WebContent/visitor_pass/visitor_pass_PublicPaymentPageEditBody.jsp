<%@page pageEncoding="UTF-8" %>
<%@ page import="visitor_pass.Visitor_passDTO" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="visitor_pass_affiliated_person.Visitor_pass_affiliated_personDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.Utils" %>


<%
    Visitor_passDTO visitor_passDTO;
    visitor_passDTO = (Visitor_passDTO) request.getAttribute("visitor_passDTO");
    if (visitor_passDTO == null) {
        visitor_passDTO = new Visitor_passDTO();

    }

    List<Visitor_pass_affiliated_personDTO> visitor_pass_affiliated_personDTOList = (List<Visitor_pass_affiliated_personDTO>) session.getAttribute("visitor_pass_affiliated_personDTOList");

    int numberOfVisitors = 1;

    if (visitor_pass_affiliated_personDTOList.size()!=0) {
        numberOfVisitors += visitor_pass_affiliated_personDTOList.size();
    }

    String actionName = "add";

    String Language = "Bangla";
    String formTitle = Language.equalsIgnoreCase("English") ? "CHECKOUT" : "চেক আউট";
    String servletName = "Visitor_passServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center mb-3">
                                <h2 class="table-title">
                                    <%=formTitle%>
                                </h2>
                            </div>
                            <table class="table table-bordered">
                                <thead class="">
                                <tr class="text-center text-nowrap">
                                    <th scope="col">
                                        <span class="table-title font-weight-bold">
                                        <%=Language.equalsIgnoreCase("English") ? "Total Person" : "মোট ব্যক্তি"%>
                                    </span>
                                    </th>
                                    <th scope="col">
                                        <span class="table-title font-weight-bold">
                                        <%=Language.equalsIgnoreCase("English") ? "Amount Per Person" : "জনপ্রতি"%>
                                    </span>
                                    </th>
                                    <th scope="col">
                                        <span class="table-title font-weight-bold">
                                        <%=Language.equalsIgnoreCase("English") ? "Total Amount" : "মোট টাকা"%>
                                    </span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="text-center text-nowrap">
                                    <th scope="row"><%=Language.equalsIgnoreCase("English") ? numberOfVisitors : Utils.getDigits(numberOfVisitors, Language)%>
                                    </th>
                                    <td><%=Language.equalsIgnoreCase("English") ? "500" : Utils.getDigits("500", Language)%>
                                    </td>
                                    <td><%=Language.equalsIgnoreCase("English") ? numberOfVisitors*500 : Utils.getDigits(numberOfVisitors*500, Language)%>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row mt-4">

                        <div class="col-8 text-left">
                            <div class="">
                                <div class="d-flex justify-content-start align-items-center">
                                    <div class="">
                                        <img
                                                class="img-thumbnail"
                                                id="captcha" src="<%=request.getContextPath()%>/simpleCaptchaServlet"
                                                alt="loading captcha..."
                                        />
                                    </div>
                                    <div class="ml-5">
                                        <img
                                                src="<%=request.getContextPath()%>/assets/images/refresh.png"
                                                width="40"
                                                height="40"
                                                title="Refresh Captcha"
                                                alt="refresh captcha"
                                                id="reloadCaptcha"
                                                onclick="getcaptcha()"
                                                class="img-thumbnail"
                                        />
                                    </div>
                                </div>
                                <div class="d-flex justify-content-start align-items-center mt-2">
                                    <div id='captcha_input_div_0'>
                                        <input
                                                type='text'
                                                id='answer_id'
                                                class='form-control inputBackground preventEnter'
                                                name='captchaInput'
                                                id='captcha_input_text_0'
                                                placeholder='<%=Language.equalsIgnoreCase("English") ? "Write Captcha" : "ক্যাপচা লিখুন"%>'
                                                tag='pb_html'
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4 text-right">
                            <a class="btn border-0 shadow btn-border-radius btn-primary text-white"
                               style="border-radius: 8px; background-color: #19b8ad;"
                               href="<%=request.getContextPath() + "/Gate_passPublicServlet?actionType=makePayment"%>"><%=Language.equalsIgnoreCase("English") ? "PAYMENT" : "পেমেন্ট করুন"%>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<script>
    function getcaptcha()
    {
        var date2 = Math.floor(Math.random() * 100) + 1;

        var img = document.getElementById("captcha");

        img.src= "<%=request.getContextPath()%>/simpleCaptchaServlet?test="+ date2;
    }
</script>
