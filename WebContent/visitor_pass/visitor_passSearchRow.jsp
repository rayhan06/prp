<%@page pageEncoding="UTF-8" %>

<%@page import="visitor_pass.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@page import="files.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VISITOR_PASS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VISITOR_PASS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Visitor_passDTO visitor_passDTO = (Visitor_passDTO) request.getAttribute("visitor_passDTO");
    CommonDTO commonDTO = visitor_passDTO;
    String servletName = "Visitor_passServlet";

    FilesDAO filesDAO = new FilesDAO();
    long ColumnID;


    System.out.println("visitor_passDTO = " + visitor_passDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Visitor_passDAO visitor_passDAO = (Visitor_passDAO) request.getAttribute("visitor_passDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<td id='<%=i%>_name'>
    <%
        value = visitor_passDTO.name + "";
    %>

    <%=Utils.getDigits(value, Language)%>
</td>


<td id='<%=i%>_visitingDate'>
    <%
        value = visitor_passDTO.visitingDate + "";
    %>
    <%
        String formatted_visitingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_visitingDate, Language)%>
</td>


<td id='<%=i%>_typeOfVisitor'>
    <%
        value = String.valueOf(visitor_passDTO.typeOfVisitor);
    %>

    <%=value.equalsIgnoreCase("1") ? (Language.equalsIgnoreCase("English") ? "Local" : "স্থানীয়") : (Language.equalsIgnoreCase("English") ? "Foreigner" : "বিদেশী")%>
</td>


<td id='<%=i%>_address'>
    <%
        value = visitor_passDTO.address + "";
    %>

    <%=Utils.getDigits(value, Language)%>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Visitor_passServlet?actionType=view&ID=<%=visitor_passDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_approval' style='white-space: nowrap' colspan="3">

	 <span class="text-primary font-italic" id="<%=i%>_approvalStatus">
            <%=visitor_passDAO.getStatus(visitor_passDTO, isLangEng)%>
     </span>
<%-- 	<br><%=visitor_passDTO.firstLayerApprovalStatus%>, <%=visitor_passDTO.secondLayerApprovalStatus%> --%>

    <div class="button-group">
        <%
            if (visitor_passDTO.firstLayerApprovalStatus == Visitor_passDAO.approvalStatusPending && userDTO.roleID == SessionConstants.VISITOR_APROVER) {
        %>
        <button type="button" id='<%=i%>_button_approve' class="btn-sm border-0 shadow text-white submit-btn"
                style="border-radius: 8px;" data-toggle="tooltip" data-placement="top" title=""
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>', '<%=visitor_passDTO.firstLayerApprovalStatus%>', '<%=visitor_passDTO.secondLayerApprovalStatus%>', '<%=visitor_passDTO.visitingDate%>', '<%=visitor_passDTO.visitingTime%>')">
            <%=Language.equalsIgnoreCase("English") ? "VERIFY" : "যাচাই করুন"%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss' class="btn-sm border-0 shadow text-white cancel-btn"
                style="border-radius: 8px;" data-toggle="tooltip" data-placement="top" title=""
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>', '<%=visitor_passDTO.firstLayerApprovalStatus%>')">
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <%
        } 
            else if (visitor_passDTO.firstLayerApprovalStatus == Visitor_passDAO.approvalStatusReceived 
            		&& visitor_passDTO.secondLayerApprovalStatus != Visitor_passDAO.approvalStatusApproved
            		&& userDTO.roleID == SessionConstants.SERJEANT_AT_ARMS_ROLE) {
        %>
        <button type="button" id='<%=i%>_button_approve' class="btn-sm border-0 shadow text-white submit-btn"
                style="border-radius: 8px;background-color: #555555;" data-toggle="tooltip" data-placement="top"
                title=""
                onclick="approval(this.id, '<%=i%>_button_dismiss', '<%=visitor_passDTO.iD%>', '<%=visitor_passDTO.firstLayerApprovalStatus%>', '<%=visitor_passDTO.secondLayerApprovalStatus%>', '<%=visitor_passDTO.visitingDate%>', '<%=visitor_passDTO.visitingTime%>')"
                >
            <%=Language.equalsIgnoreCase("English") ? "APPROVE" : "অনুমোদন"%>
        </button>
        <button type="button" id='<%=i%>_button_dismiss' class="btn-sm border-0 shadow text-white cancel-btn"
                style="border-radius: 8px;background-color: #555555;" data-toggle="tooltip" data-placement="top"
                title=""
                onclick="dismissal(this.id, '<%=i%>_button_approve', '<%=visitor_passDTO.iD%>', '<%=visitor_passDTO.firstLayerApprovalStatus%>')"
                disabled>
            <%=Language.equalsIgnoreCase("English") ? "DISMISS" : "অনুমোদিত নহে"%>
        </button>
        <%
        } 
        %>
        

    </div>
</td>


<div class="modal fade" id="visitor-pass-approval-letter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"
                    style="font-size: large; font-weight: bolder;"><%=Language.equalsIgnoreCase("English") ?
                        "UPLOAD APPROVAL FILE" : "অনুমোদন ফাইল আপলোড করুন"%>
                </h5>
            </div>
            <div class="modal-body">
                <form id="visitor-pass-approval-letter-form" method="post">
                    <div class="form-body">
                        <label class="form-group control-label">
                            <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGDATE, loginDTO)%>
                        </label>
                        <div class="form-group ml-3">
                            <div class="" id='visitingDate_div'>
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID"
                                               value="visitingDate_js"></jsp:param>
                                    <jsp:param name="LANGUAGE"
                                               value="<%=Language%>"></jsp:param>
                                </jsp:include>
                            </div>
                        </div>
                    </div>

                    <div class="form-body">
                        <label class="form-group control-label">
                            <%=LM.getText(LC.VISITOR_PASS_ADD_VISITINGTIME, loginDTO)%>
                        </label>
                        <div class="form-group ml-3">
                            <div class="" id='visitingTime_div'>
                                <jsp:include page="/time/time.jsp">
                                    <jsp:param name="TIME_ID" value="visitingTime_js"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                </jsp:include>
                            </div>
                        </div>
                    </div>

                    <div class="form-body">
                        <label class="form-group control-label">
                            <%=Language.equalsIgnoreCase("English") ? "Approval File" : "অনুমোদন ফাইল"%>
                        </label>
                        <div class="col-12  " id='filesDropzone_div_<%=i%>' style="padding: unset; color: #b7bacd">

                            <%
                                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                            %>
                            <div class="dropzone" style="min-height: auto; border: dotted 1px" id="file_dropzone"
                                 action="Visitor_passServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=ColumnID%>">
                                <input type='file' style="display: none"
                                       name='filesDropzoneFile'
                                       id='filesDropzone_dropzone_File_<%=i%>'
                                       tag='pb_html'/>
                            </div>
                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                   id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                   tag='pb_html'/>
                            <input type='hidden' name='filesDropzone'
                                   id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                   value='<%=ColumnID%>'/>
                        </div>
                   

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="modal_submit"
                        onclick="modalDataSubmit()"><%=Language.equalsIgnoreCase("English") ? "Upload and Send" : "আপলোড এবং প্রেরণ"%>
                </button>
            </div>
        </div>
    </div>
</div>

<script>

    let global_approve_button_id, global_dismiss_button_id, global_visitor_pass_id;
    
    function redirect()
    {
    	location.href='Visitor_passServlet?actionType=search';
    }

    function approval(approve_button_id, dismiss_button_id, visitor_pass_id, approval_status, second_layer_status, date, time) {

        if (approval_status == '2' || approval_status == '0') {
            let url = "Visitor_passServlet?actionType=visitorPassFirstLayerApproval" + "&visitorPassId=" + visitor_pass_id + "&approvalstatus=approved";
            $.ajax({
                url: url,
                type: "POST",
                async: true,
                success: function (data) {
                	
                	setTimeout(redirect, 2000);
                	

//                     $('#' + approve_button_id).prop("disabled", true);

//                     $('#' + approve_button_id).css('backgroundColor', '#32a0a8');
//                     $('#' + dismiss_button_id).prop("disabled", false);

//                     if (second_layer_status == '1') {
//                         $('#' + approve_button_id).prop("title", '');
//                     } else {
<%--                         $('#' + approve_button_id).prop("title", '<%=Language.equalsIgnoreCase("English") ? "Serjeant-at-Arms is processing this request!" : "নিরাপত্তা অধিশাখায় প্রক্রিয়াধীন অবস্থায় আছে!"%>'); --%>
//                     }

                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        text: '<%=Language.equalsIgnoreCase("English") ? "Requested visitor pass has been sent to Serjeant-at-Arms for further inspection!" : "ভিজিটর পাসটির অনুমোদনের জন্য সার্জেন্ট-অ্যাট-আর্মস অফিসে পাঠানো হয়েছে!"%>',
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            });
        } else {
            global_approve_button_id = approve_button_id;
            global_dismiss_button_id = dismiss_button_id;
            global_visitor_pass_id = visitor_pass_id;

            setDateByTimestampAndId('visitingDate_js', date);
            setTimeById('visitingTime_js', time, true);
            let today = new Date();
            setMinDateByTimestampAndId('visitingDate_js', today.getTime());

            $('#visitor-pass-approval-letter').modal({
                backdrop: 'static',
                keyboard: false
            });
        }

    }

    function dismissal(dismiss_button_id, approve_button_id, visitor_pass_id, approval_status) {

        let url = "Visitor_passServlet?actionType=visitorPassFirstLayerApproval" + "&visitorPassId=" + visitor_pass_id + "&approvalstatus=dismiss";
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {

                $('#' + approve_button_id).prop("disabled", false);
                $('#' + dismiss_button_id).prop("disabled", true);
                $('#' + approve_button_id).html('<%=Language.equalsIgnoreCase("English") ? "RECEIVE" : "গ্রহণ করুন"%>');
                $('#' + approve_button_id).css('backgroundColor', '#4a87e2');
                $('#' + approve_button_id).prop("title", '');
            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                Swal.fire({
                    position: 'center',
                    type: 'warning',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Requested visitor pass has been dismissed successfully" : "ভিজিটর পাসটির অনুমোদন বাতিল করা হয়েছে"%>',
                    showConfirmButton: false,
                    timer: 3000
                });
            }
        });
    }

    function modalDataSubmit() {
        if (dateValidator('visitingDate_js', true) == false || timeNotEmptyValidator('visitingTime_js') == false)
            return;
        let url = "Visitor_passServlet?actionType=visitorPassFirstLayerApproval" + "&visitorPassId=" + global_visitor_pass_id + "&approvalstatus=approved&approvalFile=" + $('#filesDropzone_dropzone_0').val();
        url += "&visitingDate=" + getDateTimestampById('visitingDate_js') + "&visitingTime=" + getTimeById('visitingTime_js', true);
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {

                $('#' + global_approve_button_id).css('backgroundColor', '#555555');
                $('#' + global_dismiss_button_id).css('backgroundColor', '#555555');
                $('#' + global_dismiss_button_id).prop("disabled", true);
                $('#' + global_approve_button_id).prop("disabled", true);

                $('#visitor-pass-approval-letter').modal('hide');

            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                Swal.fire({
                    position: 'center',
                    type: 'success',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Requested visitor pass has been approved successfully" : "ভিজিটর পাসটির অনুমোদন সম্পন্ন হয়েছে"%>',
                    showConfirmButton: false,
                    timer: 3000
                });
            }
        });
    }
</script>

