<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*"%>
<%@ page import="employee_offices.*"%>
<%@ page import="pb.*" %>
<%@page import="workflow.WorkflowController"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.TICKET_HISTORY_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select name='forwardToOrganogramId' id = 'forwardToOrganogramId'	class='form-control'>
						<option value = ""></option>
						 <%
						     Set<Long> engineersAndAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE);
						
							Set<Long> ticketAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.SUPPORT_ENGINEER_ROLE);
							engineersAndAdmins.addAll(ticketAdmins);
						
						 
                            for(Long employee: engineersAndAdmins)
                            {
                            %>
                            <option value = "<%=employee%>" >
                             <%=WorkflowController.getNameFromOrganogramId(employee, Language)%>
                            </option>
                            <%
                            }
                            %>
						</select>					
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_ISSUE_RAISER, loginDTO)%>
				</label>
				<div class="col-sm-9">
					
					<button type="button" class="btn btn-block submit-btn btn-border-radius text-white"
	                        onclick="addEmployee()"
	                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
	                </button>
	                <table class="table table-bordered table-striped">
	                    <tbody id="employeeToSet"></tbody>
	                </table>
                	<input type='hidden'  name='issueRaiserOrganogramId' id = 'issueRaiserOrganogramId' value=""/>						
				</div>
			</div>
		</div>
		
		
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=isLangEng ? "Room" : "কক্ষ"%>

				</label>
				<div class="col-sm-9">
					<select class='form-control' name='roomNoCat' onchange = "setOrgFromRoom(this.value)"
	                                                   id='roomNoCat' tag='pb_html' onchange = "setOrgFromRoom(this.value)">
	                       <%=CatRepository.getInstance().buildOptions("room_no", Language, -1)%>
	                </select>	
				</div>
			</div>
		</div>
		
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=isLangEng ? "Priority" : "গুরুত্ব"%>

				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='priorityCat' id = 'priorityCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "priority", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=isLangEng ? "Status" : "অবস্থা"%>

				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='ticketStatusCat' id = 'ticketStatusCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "ticket_status", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
function init()
{
	dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = true;
    processNewCalendarDateAndSubmit();
    addables = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
}
function PreprocessBeforeSubmiting()
{
}
function getLink(list)
{
	return "Support_ticketServlet?actionType=view&ID=" + list[1];
}
function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#issueRaiserOrganogramId").val(orgId);
}

function setOrgFromRoom(room)
{
	if(room != -1)
	{
		$("#issueRaiserOrganogramId").val(parseInt(room) + <%=SessionConstants.ROOM_OFFSET%>);
	}
	console.log("org now = " + $("#issueRaiserOrganogramId").val());
}
</script>