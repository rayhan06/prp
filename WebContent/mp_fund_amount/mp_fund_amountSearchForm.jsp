<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="mp_fund_amount.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget.BudgetInfo" %>
<%@ page import="util.StringUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>


<%
    String navigator2 = "navMP_FUND_AMOUNT";
    String servletName = "Mp_fund_amountServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
			<th>
				<%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ECONOMICYEARSTART, loginDTO)%>
			</th>
            <th>
				<%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONDETAILSID, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONCONSTITUENCYID, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.MP_FUND_AMOUNT_ADD_TOTALAMOUNT, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.MP_FUND_AMOUNT_ADD_AMOUNTLEFT, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
			List<Mp_fund_amountDTO> data = (List<Mp_fund_amountDTO>) rn2.list;
            try {
                if (data != null) {
                    for (Mp_fund_amountDTO mp_fund_amountDTO :  data) {
                    	if(mp_fund_amountDTO.iD == Mp_fund_amountServlet.fixedTotalMpFundAmountId)
                    		continue;
        %>
        <tr>
			<td>
				<%=StringUtils.convertBanglaIfLanguageIsBangla(
						Language,
						BudgetInfo.getEconomicYear(mp_fund_amountDTO.economicYearStart)
				)%>
			</td>
            <td>
                <%=Election_detailsRepository.getInstance().getText(mp_fund_amountDTO.electionDetailsId, Language)%>
            </td>

            <td>
                <%=Election_constituencyRepository.getInstance().getConstituencyText(
						mp_fund_amountDTO.electionConstituencyId, Language
				)%>
            </td>

            <td>
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                		StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(mp_fund_amountDTO.totalAmount))
				)%>/-
            </td>

            <td>
				<%=BangladeshiNumberFormatter.getFormattedNumber(
						StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(mp_fund_amountDTO.amountLeft))
				)%>/-
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>