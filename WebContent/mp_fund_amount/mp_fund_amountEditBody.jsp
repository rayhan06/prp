<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="mp_fund_amount.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>

<%
    Mp_fund_amountDTO mpFundAmountDTO = Mp_fund_amountDAO.getInstance().getDTOFromID(Mp_fund_amountServlet.fixedTotalMpFundAmountId);
    if (mpFundAmountDTO == null) {
        mpFundAmountDTO = new Mp_fund_amountDTO();
    }
    String context = request.getContextPath() + "/";
%>

<%@include file="../pb/addInitializer2.jsp" %>

<%
    String formTitle = LM.getText(LC.MP_FUND_AMOUNT_ADD_MP_FUND_AMOUNT_ADD_FORMNAME, loginDTO);
    String servletName = "Mp_fund_amountServlet";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal"
              action="Mp_fund_amountServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="totalAmount">
                                            <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_TOTALAMOUNT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='totalAmount' id='totalAmount'
                                                   data-only-integer="true"
                                                   value='<%=mpFundAmountDTO.totalAmount%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_MP_FUND_AMOUNT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_MP_FUND_AMOUNT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const form = $('#bigform');

    function PreprocessBeforeSubmiting() {
        if(form.valid()) submitAddForm2();
        return false;
    }

    function typeOnlyInteger(e) {
        return true === inputValidationForIntValue(e, $(this));
    }

    function init() {
        $('body').delegate('[data-only-integer="true"]', 'keydown', typeOnlyInteger);

        form.validate({
            rules: {
                totalAmount: {
                    required: true,
                    min : 1
                }
            },
            messages: {
                totalAmount: '<%=getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>'
            }
        });
    }

    $(document).ready(function () {
        init();
    });
</script>