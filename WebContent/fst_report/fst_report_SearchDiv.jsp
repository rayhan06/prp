<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@ page import="pb.*"%>
<%
String Language = LM.getText(LC.FST_REPORT_EDIT_LANGUAGE, loginDTO);
String Options;
int i = 0;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div id = "religionCat" class="search-criteria-div">
        <div class="form-group">
        	<label class="col-sm-3 control-label">
        		<%=LM.getText(LC.FST_REPORT_WHERE_RELIGIONCAT, loginDTO)%>
        	</label>
        	<div class="col-sm-6">
				<select class='form-control'  name='religionCat' id = 'religionCat' onChange="ajaxSubmit();">		
					<%		
					Options = CatDAO.getOptions(Language, "religion", CatDTO.CATDEFAULT);								
					%>
					<%=Options%>
				</select>
        	</div>
        </div>
</div>
<div id = "medicalEquipmentNameType" class="search-criteria-div">
        <div class="form-group">
        	<label class="col-sm-3 control-label">
        		<%=LM.getText(LC.FST_REPORT_WHERE_MEDICALEQUIPMENTNAMETYPE, loginDTO)%>
        	</label>
        	<div class="col-sm-6">
				<select class='form-control'  name='medicalEquipmentNameType' id = 'medicalEquipmentNameType' onChange="ajaxSubmit();">		
					<%		
					Options = CommonDAO.getOptions(Language, "medical_equipment_name", CatDTO.CATDEFAULT);								
					%>
					<%=Options%>
				</select>
        	</div>
        </div>
</div>
<%@include file="../pbreport/yearmonth.jsp"%>
<%@include file="../pbreport/calendar.jsp"%>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
</script>