<%@page pageEncoding="UTF-8" %>
<%@ page import="util.StringUtils" %>
<%@ page import="incident.IncidentRepository" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>

<td>
    <%=CatRepository.getInstance().getText(Language, "complaint_status", disciplinary_logDTO.complaintStatusCat)%>
</td>

<td>
    <%=IncidentRepository.getInstance().getText(Language, disciplinary_logDTO.incidentType)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "complaint_source", disciplinary_logDTO.complaintSourceCat)%>
</td>


<td>
    <%=StringUtils.getFormattedDate(Language, disciplinary_logDTO.reportedDate)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, disciplinary_logDTO.complaintResolvedDate)%>
</td>

<%
    if (hasAjax) {
        if (request.getParameter("complain_by") != null) {
            String complainByName=Employee_recordsRepository.getInstance().getEmployeeName(Long.parseLong(request.getParameter("complain_by")),Language);
%>
<td>
    <%=complainByName%>
</td>
<%
    }
    if (request.getParameter("complain_for") != null) {
        String complainForName=Employee_recordsRepository.getInstance().getEmployeeName(Long.parseLong(request.getParameter("complain_for")),Language);
%>
<td>
    <%=complainForName%>
</td>
<%
        }
    }
%>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Disciplinary_logServlet?actionType=view&ID=<%=disciplinary_logDTO.iD%>&incidentNumber=<%=disciplinary_logDTO.incidentNumber%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<%
    if (!isUserParliamentMember) {
%>
<%-- Take Disciplinary Action   --%>
<td>
    <button type="button" class="btn btn-sm btn-info shadow btn-border-radius"
            onclick="location.href='Disciplinary_actionServlet?actionType=<%=CatRepository.getInstance().getText("English", "complaint_status", disciplinary_logDTO.complaintStatusCat).equalsIgnoreCase("open")?"getAddPage" : "getEditPage"%>&incidentNumber=<%=disciplinary_logDTO.incidentNumber%>'">
        <%=LM.getText(LC.DISCIPLINARY_LOG_EDIT__TAKE_ACTION, userDTO)%>
    </button>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Disciplinary_logServlet?actionType=getEditPage&ID=<%=disciplinary_logDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=disciplinary_logDTO.iD%>'/></span>
    </div>
</td>
<%
    }
%>