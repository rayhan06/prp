<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="incident.IncidentRepository" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="common.BaseServlet" %>
<%@page pageEncoding="UTF-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.DISCIPLINARY_LOG_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0' onKeyUp='allfield_changed("",0)' id='anyfield'
                       value = '<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
<%--        <div class="kt-portlet__head-toolbar">--%>
<%--            <div class="kt-portlet__head-group">--%>
<%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
<%--                     aria-hidden="true" x-placement="top"--%>
<%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
<%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
<%--                    <div class="tooltip-inner">Collapse</div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="complaint_source_cat"
                               class="col-md-3 col-form-label"><%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_COMPLAINTSOURCECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='complaint_source_cat' id='complaint_source_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("complaint_source", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="incident_type"
                               class="col-md-3 col-form-label"><%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_INCIDENTTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='incident_type' id='incident_type'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=IncidentRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="complaint_status_cat"
                               class="col-md-3 col-form-label"><%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_COMPLAINTSTATUSCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='complaint_status_cat' id='complaint_status_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("complaint_status", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_START_DATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="reported_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden'
                               class='form-control'
                               id='reported_date_start'
                               name='reported_date'
                               value='' tag='pb_html'/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_END_DATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="complaint_resolved_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden'
                               class='form-control'
                               id='reported_date_end'
                               name='complaint_resolved_date'
                               value='' tag='pb_html'/>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label pr-0">
                            <%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINT_AGAINST, loginDTO)%>
                        </label>
                        <div class="col-md-9" id='complaint_for_div_'>
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="tagEmp_modal_button">
                                <%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINT_AGAINST, userDTO)%>
                            </button>

                        </div>
                        <div class="container">
                            <table class="table table-bordered table-striped">
                                <thead></thead>
                                <tbody id="tagged_emp_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-sm delete-trash-btn"
                                                onclick="remove_containing_row(this,'tagged_emp_table')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, loginDTO)%>
                        </label>
                        <div class="col-md-9" id='complaint_by_div_'>
                            <button id="complainedBy_modal_button" type="button"
                                    class="btn btn-primary btn-block shadow btn-border-radius">
                                <%=isLanguageEnglish ? "Select " + LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, userDTO)
                                        : LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, userDTO) + " বাছাই করুন"%>
                            </button>

                        </div>
                        <div class="container table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead></thead>
                                <tbody id="complainedBy_internal_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-sm delete-trash-btn"
                                                onclick="remove_containing_row(this,'complainedBy_internal_table')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script type="text/javascript">

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    $(document).ready(() => {
        select2SingleSelector("#complaint_source_cat", '<%=Language%>');
        select2SingleSelector("#incident_type", '<%=Language%>');
        select2SingleSelector("#complaint_status_cat", '<%=Language%>');
    });

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;
        $("#reported_date_start").val(getDateStringById("reported_date_js"));
        $("#reported_date_end").val(getDateStringById("complaint_resolved_date_js"));
        if ($("#complaint_source_cat").val()) {
            params += '&complaint_source_cat=' + $("#complaint_source_cat").val();
        }
        if ($("#incident_type").val()) {
            params += '&incident_type=' + $("#incident_type").val();
        }
        if ($("#complaint_status_cat").val()) {
            params += '&complaint_status_cat=' + $("#complaint_status_cat").val();
        }
        if ($("#reported_date_start").val()) {
            params += '&reported_date_start=' + getBDFormattedDate("reported_date_start");
        }
        if ($("#reported_date_end").val()) {
            params += '&reported_date_end=' + getBDFormattedDate("reported_date_end");
        }
        if (complained_by_info_map.size > 0) {
            params += '&complain_by=' + complained_by_info_map.keys().next().value;
        }
        if (added_employee_info_map.size > 0) {
            params += '&complain_for=' + added_employee_info_map.keys().next().value;
        }
        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    $(function () {

        $("#reported_date_start").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>",
            onSelect: function (dateText) {
                $("#reported_date_end").datepicker('option', 'minDate', dateText);
            }
        });
        $("#reported_date_end").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>"
        });

        $('#reported_date-crs-but').click(() => {
            $("#reported_date_end").datepicker('option', 'minDate', null);
            $("#reported_date_start").val('');
        });
        $('#resolve_date-crs-but').click(() => {
            $("#reported_date_end").val('');
        });


        $("#reported_date_start").datepicker('option', 'maxDate', new Date());
        $("#reported_date_end").datepicker('option', 'minDate', new Date());

        $("#reported_date_start").val('');
        $("#reported_date_end").val();

    });

    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();
    // map to store and send added employee data
    complained_by_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true
            }],
            ['complainedBy_internal_table', {
                info_map: complained_by_info_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });

    $('#complainedBy_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'complainedBy_internal_table';
        $('#search_emp_modal').modal();
    });


    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);
    }
</script>

<style>

    .input-width89 {
        width: 87%;
        float: left;
    }

    .button-cross-picker {
        height: 34px;
        width: 40px;
        border: 1px solid #c2cad8;
    }
</style>
