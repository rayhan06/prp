<%@page import="common.NameDTO" %>
<%@page import="complain_action.Complain_actionRepository" %>
<%@page import="disciplinary_action.Disciplinary_actionDTO" %>
<%@ page import="disciplinary_details.Disciplinary_detailsDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="disciplinary_details.Disciplinary_detailsDTO" %>
<%@page import="disciplinary_log.Disciplinary_logComplainByDTO" %>
<%@page import="disciplinary_log.Disciplinary_logDAO" %>
<%@page import="disciplinary_log.Disciplinary_logDTO" %>
<%@page import="employee_records.Employee_recordsRepository" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="incident.IncidentRepository" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="pb.Utils" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="disciplinary_log.Disciplinary_logComplainByDAO" %>
<%@ page import="disciplinary_action.Disciplinary_actionDAO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);

    // TODO: here disciplinary_logDTO is taken from DAO not from repo
    Disciplinary_logDAO disciplinary_logDAO = Disciplinary_logDAO.getInstance();
    Disciplinary_logDTO disciplinary_logDTO = (Disciplinary_logDTO) disciplinary_logDAO.getDTOByID(id);
    FilesDAO filesDAO = new FilesDAO();

    List<Disciplinary_detailsDTO> disciplinary_detailsDTOList = new Disciplinary_detailsDAO().getByForeignKey(disciplinary_logDTO.incidentNumber);
    List<Long> empList = disciplinary_detailsDTOList.stream().map(dto -> dto.employeeRecordsId).collect(Collectors.toList());
    //Map<Long, EmployeeModel> employeeModelMap = EmployeeOfficesDAO.getInstance().getOfficeAndDesignation(empList, Language);

    List<Disciplinary_logComplainByDTO> complainByDTOList
            = new Disciplinary_logComplainByDAO().getByForeignKey(disciplinary_logDTO.incidentNumber);

    Disciplinary_actionDTO disciplinary_actionDTO = Disciplinary_actionDAO.getInstance().getDTObyIncidentNumber(disciplinary_logDTO.incidentNumber);
    if (disciplinary_actionDTO == null) {
        disciplinary_actionDTO = new Disciplinary_actionDTO();
    }
%>


<%--
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<h3 class="kt-subheader__title"> Asset Management </h3>
	</div>
</div>

<!-- end:: Subheader -->--%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_ANYFIELD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_ANYFIELD, userDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_INCIDENTTYPE, userDTO)%>
                                </b>
                            </td>
                            <td class="data-view-data">
                                <%=IncidentRepository.getInstance().getText(Language, disciplinary_logDTO.incidentType)%>
                                <%if (!"".equals(disciplinary_logDTO.incidentTypeOther.trim())) {%>
                                (<%=disciplinary_logDTO.incidentTypeOther%>)
                                <%}%>
                            </td>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_INCIDENTSUMMARY, userDTO)%>
                                </b>
                            </td>
                            <td class="data-view-data">
                                <%=disciplinary_logDTO.incidentSummary%>
                            </td>
                        </tr>

                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_REPORTEDDATE, userDTO)%>
                                </b>
                            </td>
                            <td class="data-view-data">
                                <%=StringUtils.getFormattedDate(Language, disciplinary_logDTO.reportedDate)%>
                            </td>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTRESOLVEDDATE, userDTO)%>
                                </b></td>
                            <td class="data-view-data">
                                <%=StringUtils.getFormattedDate(Language, disciplinary_logDTO.complaintResolvedDate)%>
                            </td>
                        </tr>

                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTSTATUSCAT, userDTO)%>
                                </b>
                            </td>
                            <td class="data-view-data">
                                <%=CatRepository.getInstance().getText(Language, "complaint_status", disciplinary_logDTO.complaintStatusCat)%>
                            </td>

                            <td>
                                <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_HANDLINGDURATION, userDTO)%>
                                </b>
                            </td>
                            <td class="data-view-data">
                                <%=StringUtils.convertIntToString(Language, disciplinary_logDTO.handlingDuration)%>
                            </td>
                        </tr>

                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_FILESDROPZONE, userDTO)%>
                                </b>
                            </td>
                            <td colspan="3">

                                <%
                                    {
                                        List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_logDTO.filesDropzone);
                                %>
                                <table>
                                    <tr>
                                        <%
                                            if (FilesDTOList != null) {
                                                for (int j = 0; j < FilesDTOList.size(); j++) {
                                                    FilesDTO filesDTO = FilesDTOList.get(j);
                                                    byte[] encodeBase64 = Base64.encodeBase64(filesDTO.thumbnailBlob);
                                        %>
                                        <td>
                                            <%
                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                            %>
                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                 style='width:100px'/>
                                            <%
                                                }
                                            %>
                                            <a href='Disciplinary_logServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                               download><%=filesDTO.fileTitle%>
                                            </a>
                                        </td>
                                        <%
                                                }
                                            }
                                        %>
                                    </tr>
                                </table>
                                <%
                                    }
                                %>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="mt-5">
                <h5 class="table-title">
                    <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, userDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-nowrap">
                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTSOURCECAT, userDTO)%>
                                </b></td>
                            <td colspan="2">
                                <%=CatRepository.getInstance().getText(Language, "complaint_source", disciplinary_logDTO.complaintSourceCat)%>
                            </td>
                        </tr>

                        <%if (!"".equals(disciplinary_logDTO.complainedByExternal)) {%>
                        <tr>
                            <td class="data-view-heading"></td>
                            <td colspan="2">
                                <%=disciplinary_logDTO.complainedByExternal%>
                            </td>
                        </tr>
                        <%}%>

                        <%
                            if ("internal".equalsIgnoreCase(CatRepository.getInstance().getText("English", "complaint_source", disciplinary_logDTO.complaintSourceCat))) {
                        %>
                        <tr>
                            <td><b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%>
                            </b></td>
                            <td><b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%>
                            </b></td>
                            <td><b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%>
                            </b></td>
                        </tr>
                        <%}%>

                        <%if (complainByDTOList != null && complainByDTOList.size() > 0) {%>
                        <%for (Disciplinary_logComplainByDTO complainByDTO : complainByDTOList) {%>
                        <tr>
                            <td>
                                <%=Utils.getDigits(complainByDTO.employeeUserName,Language)%>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? complainByDTO.employeeNameEn : complainByDTO.employeeNameBn%>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? (complainByDTO.organogramNameEn + ", " + complainByDTO.officeNameEn)
                                        : (complainByDTO.organogramNameBn + ", " + complainByDTO.officeNameBn)%>
                            </td>
                        </tr>
                        <%}%>
                        <%}%>

                    </table>
                </div>
            </div>
            <div class="mt-5">
                <h5 class="table-title">
                    <%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARY_ACTION_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_WORKSDONE, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%=Utils.getDigits(disciplinary_actionDTO.worksDone, Language)%>
                            </td>

                        </tr>

                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_FINDINGS, loginDTO)%>
                                </b>
                            </td>

                            <td>
                                <%=Utils.getDigits(disciplinary_actionDTO.findings, Language)%>
                            </td>
                        </tr>

                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_PREVENTIONADVICE, loginDTO)%>
                                </b>
                            </td>
                            <td>
                                <%=Utils.getDigits(disciplinary_actionDTO.preventionAdvice, Language)%>
                            </td>

                        </tr>

                        <tr>
                            <td class="data-view-heading">
                                <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_FILESDROPZONE, loginDTO)%>
                                </b>
                            </td>
                            <td>

                                <%
                                    if (disciplinary_actionDTO.filesDropzone != 0L) {
                                        List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_actionDTO.filesDropzone);
                                %>
                                <table>
                                    <tr>
                                        <%
                                            if (FilesDTOList != null) {
                                                for (int j = 0; j < FilesDTOList.size(); j++) {
                                                    FilesDTO filesDTO = FilesDTOList.get(j);
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                        %>
                                        <td>
                                            <%
                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                            %>
                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                 style='width:100px'/>
                                            <%
                                                }
                                            %>
                                            <a href='Disciplinary_actionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                               download><%=filesDTO.fileTitle%>
                                            </a>
                                        </td>
                                        <%
                                                }
                                            }
                                        %>
                                    </tr>
                                </table>
                                <%
                                    }
                                %>
                            </td>

                        </tr>

                    </table>
                </div>
            </div>
            <div class="mt-5">
                <h5 class="table-title">
                    <%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINT_AGAINST, userDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th style="width: 8%">
                                <b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%>
                                </b>

                            </th>

                            <th style="width: 18%">
                                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%>
                                </b>
                            </th>

                            <th style="width: 20%">
                                <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%>
                                </b>
                            </th>

                            <th>
                                <b><%=LM.getText(LC.PORTAL_FEEDBACK_SEARCH_ACTIONTAKEN, userDTO)%>
                                </b>
                            </th>
                            <th><b><%=LM.getText(LC.DISCIPLINARY_DETAILS_ADD_REMARKS, userDTO)%>
                            </b></th>
                        </tr>

                        <%
                            if (disciplinary_detailsDTOList != null) {
                                for (Disciplinary_detailsDTO disciplinary_detailsDTO : disciplinary_detailsDTOList) {%>
                        <tr>
                            <td style="width: 8%">
                                <%=Utils.getDigits(Employee_recordsRepository.getInstance()
                                        .getById(disciplinary_detailsDTO.employeeRecordsId).employeeNumber,Language)%>
                            </td>

                            <td style="width: 18%">
                                <%=isLanguageEnglish ? disciplinary_detailsDTO.employeeNameEn : disciplinary_detailsDTO.employeeNameBn%>
                            </td>

                            <td style="width: 20%">
                                <%=isLanguageEnglish ? (disciplinary_detailsDTO.organogramNameEn + ", " + disciplinary_detailsDTO.officeNameEn)
                                        : (disciplinary_detailsDTO.organogramNameBn + ", " + disciplinary_detailsDTO.officeNameBn)%>
                            </td>

                            <td>
                                <%=Complain_actionRepository.getInstance().getText(Language, disciplinary_detailsDTO.complainActionType)%>
                                <%
                                    NameDTO actionDTO = Complain_actionRepository.getInstance().getDTOByID(disciplinary_detailsDTO.complainActionType);
                                    if (actionDTO != null) {
                                        String actionTaken = "";
                                        if (actionDTO.nameEn.equalsIgnoreCase("Other")) {
                                            actionTaken = disciplinary_detailsDTO.complianOther;
                                        } else {
                                            actionTaken = StringUtils.getFormattedDate(Language, disciplinary_detailsDTO.fromDate)
                                                    + (isLanguageEnglish ? " To " : " থেকে ")
                                                    + StringUtils.getFormattedDate(Language, disciplinary_detailsDTO.toDate);
                                        }

                                %>
                                (<%=actionTaken%>)
                                <%
                                    }
                                %>
                            </td>
                            <td><%=disciplinary_detailsDTO.remarks%>
                            </td>
                        </tr>
                        <%
                                }
                            }
                        %>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<%--<div class="modal-content viewmodal">--%>
<%--    <div class="modal-header">--%>
<%--        <div class="col-md-12">--%>
<%--            <div class="row">--%>
<%--                <div class="col-md-9 col-sm-12">--%>
<%--                    <h5 class="modal-title">Disciplinary Log Details</h5>--%>
<%--                </div>--%>
<%--                <div class="col-md-3 col-sm-12">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-md-6">--%>
<%--                            <a href="javascript:" style="display: none" class="btn btn-success app_register"--%>
<%--                               data-id="419637"> Register </a>--%>
<%--                        </div>--%>
<%--                        <div class="col-md-6">--%>
<%--                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"--%>
<%--                               data-id="419637"> Reject </a>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>


<%--    </div>--%>

<%--    <div class="modal-body container">--%>
<%--        <div class="row div_border office-div">--%>
<%--            <div class="col-md-12">--%>
<%--                <h3>--%>
<%--                    <%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_ANYFIELD, userDTO)%>--%>
<%--                </h3>--%>
<%--                <table class="table table-bordered table-striped">--%>

<%--                    <tr>--%>
<%--                        <td class="data-view-heading">--%>
<%--                            <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_INCIDENTTYPE, userDTO)%>--%>
<%--                            </b>--%>
<%--                        </td>--%>
<%--                        <td class="data-view-data">--%>
<%--                            <%=IncidentRepository.getInstance().getText(Language, disciplinary_logDTO.incidentType)%>--%>
<%--                            <%if (!"".equals(disciplinary_logDTO.incidentTypeOther.trim())) {%>--%>
<%--                            (<%=disciplinary_logDTO.incidentTypeOther%>)--%>
<%--                            <%}%>--%>
<%--                        </td>--%>
<%--                        <td class="data-view-heading">--%>
<%--                            <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_INCIDENTSUMMARY, userDTO)%>--%>
<%--                            </b>--%>
<%--                        </td>--%>
<%--                        <td class="data-view-data">--%>
<%--                            <%=disciplinary_logDTO.incidentSummary%>--%>
<%--                        </td>--%>
<%--                    </tr>--%>

<%--                    <tr>--%>
<%--                        <td class="data-view-heading">--%>
<%--                            <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_REPORTEDDATE, userDTO)%>--%>
<%--                            </b>--%>
<%--                        </td>--%>
<%--                        <td class="data-view-data">--%>
<%--                            <%=StringUtils.getFormattedDate(Language, disciplinary_logDTO.reportedDate)%>--%>
<%--                        </td>--%>
<%--                        <td class="data-view-heading">--%>
<%--                            <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTRESOLVEDDATE, userDTO)%>--%>
<%--                            </b></td>--%>
<%--                        <td class="data-view-data">--%>
<%--                            <%=StringUtils.getFormattedDate(Language, disciplinary_logDTO.complaintResolvedDate)%>--%>
<%--                        </td>--%>
<%--                    </tr>--%>

<%--                    <tr>--%>
<%--                        <td class="data-view-heading">--%>
<%--                            <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTSTATUSCAT, userDTO)%>--%>
<%--                            </b>--%>
<%--                        </td>--%>
<%--                        <td class="data-view-data">--%>
<%--                            <%=CatRepository.getInstance().getText(Language, "complaint_status", disciplinary_logDTO.complaintStatusCat)%>--%>
<%--                        </td>--%>

<%--                        <td>--%>
<%--                            <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_HANDLINGDURATION, userDTO)%>--%>
<%--                            </b>--%>
<%--                        </td>--%>
<%--                        <td class="data-view-data">--%>
<%--                            <%=StringUtils.convertIntToString(Language, disciplinary_logDTO.handlingDuration)%>--%>
<%--                        </td>--%>
<%--                    </tr>--%>

<%--                    <tr>--%>
<%--                        <td class="data-view-heading">--%>
<%--                            <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_FILESDROPZONE, userDTO)%>--%>
<%--                            </b>--%>
<%--                        </td>--%>
<%--                        <td colspan="3">--%>

<%--                            <%--%>
<%--                                {--%>
<%--                                    List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_logDTO.filesDropzone);--%>
<%--                            %>--%>
<%--                            <table>--%>
<%--                                <tr>--%>
<%--                                    <%--%>
<%--                                        if (FilesDTOList != null) {--%>
<%--                                            for (int j = 0; j < FilesDTOList.size(); j++) {--%>
<%--                                                FilesDTO filesDTO = FilesDTOList.get(j);--%>
<%--                                                byte[] encodeBase64 = Base64.encodeBase64(filesDTO.thumbnailBlob);--%>
<%--                                    %>--%>
<%--                                    <td>--%>
<%--                                        <%--%>
<%--                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {--%>
<%--                                        %>--%>
<%--                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'--%>
<%--                                             style='width:100px'/>--%>
<%--                                        <%--%>
<%--                                            }--%>
<%--                                        %>--%>
<%--                                        <a href='Disciplinary_logServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'--%>
<%--                                           download><%=filesDTO.fileTitle%>--%>
<%--                                        </a>--%>
<%--                                    </td>--%>
<%--                                    <%--%>
<%--                                            }--%>
<%--                                        }--%>
<%--                                    %>--%>
<%--                                </tr>--%>
<%--                            </table>--%>
<%--                            <%--%>
<%--                                }--%>
<%--                            %>--%>
<%--                        </td>--%>
<%--                    </tr>--%>
<%--                </table>--%>
<%--            </div>--%>
<%--        </div>--%>

<%--        <div>--%>
<%--            <h3><%=LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, userDTO)%>--%>
<%--            </h3>--%>

<%--            <table class="table table-striped table-bordered">--%>
<%--                <tr>--%>
<%--                    <td class="data-view-heading">--%>
<%--                        <b><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTSOURCECAT, userDTO)%>--%>
<%--                        </b></td>--%>
<%--                    <td colspan="2">--%>
<%--                        <%=CatRepository.getInstance().getText(Language, "complaint_source", disciplinary_logDTO.complaintSourceCat)%>--%>
<%--                    </td>--%>
<%--                </tr>--%>

<%--                <%if (!"".equals(disciplinary_logDTO.complainedByExternal)) {%>--%>
<%--                <tr>--%>
<%--                    <td class="data-view-heading"></td>--%>
<%--                    <td colspan="2">--%>
<%--                        <%=disciplinary_logDTO.complainedByExternal%>--%>
<%--                    </td>--%>
<%--                </tr>--%>
<%--                <%}%>--%>

<%--                <%--%>
<%--                    if ("internal".equalsIgnoreCase(CatRepository.getInstance().getText("English", "complaint_source", disciplinary_logDTO.complaintSourceCat))) {--%>
<%--                %>--%>
<%--                <tr>--%>
<%--                    <td><b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%>--%>
<%--                    </b></td>--%>
<%--                    <td><b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%>--%>
<%--                    </b></td>--%>
<%--                    <td><b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%>--%>
<%--                    </b></td>--%>
<%--                </tr>--%>
<%--                <%}%>--%>

<%--                <%if (complainByDTOList != null && complainByDTOList.size() > 0) {%>--%>
<%--                <%for (Disciplinary_logComplainByDTO complainByDTO : complainByDTOList) {%>--%>
<%--                <tr>--%>
<%--                    <td>--%>
<%--                        <%=complainByDTO.employeeUserName%>--%>
<%--                    </td>--%>
<%--                    <td>--%>
<%--                        <%=isLanguageEnglish ? complainByDTO.employeeNameEn : complainByDTO.employeeNameBn%>--%>
<%--                    </td>--%>
<%--                    <td>--%>
<%--                        <%=isLanguageEnglish ? (complainByDTO.organogramNameEn + ", " + complainByDTO.officeNameEn)--%>
<%--                                             : (complainByDTO.organogramNameBn + ", " + complainByDTO.officeNameBn)%>--%>
<%--                    </td>--%>
<%--                </tr>--%>
<%--                <%}%>--%>
<%--                <%}%>--%>

<%--            </table>--%>
<%--        </div>--%>

<%--        <div>--%>
<%--            <h3>--%>
<%--                <%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARY_ACTION_ADD_FORMNAME, loginDTO)%>--%>
<%--            </h3>--%>
<%--            <table class="table table-bordered table-striped">--%>
<%--                <tr>--%>
<%--                    <td class="data-view-heading">--%>
<%--                        <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_WORKSDONE, loginDTO)%>--%>
<%--                        </b>--%>
<%--                    </td>--%>
<%--                    <td>--%>
<%--                        <%=Utils.getDigits(disciplinary_actionDTO.worksDone, Language)%>--%>
<%--                    </td>--%>

<%--                </tr>--%>

<%--                <tr>--%>
<%--                    <td class="data-view-heading">--%>
<%--                        <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_FINDINGS, loginDTO)%>--%>
<%--                        </b>--%>
<%--                    </td>--%>

<%--                    <td>--%>
<%--                        <%=Utils.getDigits(disciplinary_actionDTO.findings, Language)%>--%>
<%--                    </td>--%>
<%--                </tr>--%>

<%--                <tr>--%>
<%--                    <td class="data-view-heading">--%>
<%--                        <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_PREVENTIONADVICE, loginDTO)%>--%>
<%--                        </b>--%>
<%--                    </td>--%>
<%--                    <td>--%>
<%--                        <%=Utils.getDigits(disciplinary_actionDTO.preventionAdvice, Language)%>--%>
<%--                    </td>--%>

<%--                </tr>--%>

<%--                <tr>--%>
<%--                    <td class="data-view-heading">--%>
<%--                        <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_FILESDROPZONE, loginDTO)%>--%>
<%--                        </b>--%>
<%--                    </td>--%>
<%--                    <td>--%>

<%--                        <%--%>
<%--                            if (disciplinary_actionDTO.filesDropzone != 0L) {--%>
<%--                                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_actionDTO.filesDropzone);--%>
<%--                        %>--%>
<%--                        <table>--%>
<%--                            <tr>--%>
<%--                                <%--%>
<%--                                    if (FilesDTOList != null) {--%>
<%--                                        for (int j = 0; j < FilesDTOList.size(); j++) {--%>
<%--                                            FilesDTO filesDTO = FilesDTOList.get(j);--%>
<%--                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);--%>
<%--                                %>--%>
<%--                                <td>--%>
<%--                                    <%--%>
<%--                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {--%>
<%--                                    %>--%>
<%--                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'--%>
<%--                                         style='width:100px'/>--%>
<%--                                    <%--%>
<%--                                        }--%>
<%--                                    %>--%>
<%--                                    <a href='Disciplinary_actionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'--%>
<%--                                       download><%=filesDTO.fileTitle%>--%>
<%--                                    </a>--%>
<%--                                </td>--%>
<%--                                <%--%>
<%--                                        }--%>
<%--                                    }--%>
<%--                                %>--%>
<%--                            </tr>--%>
<%--                        </table>--%>
<%--                        <%--%>
<%--                            }--%>
<%--                        %>--%>
<%--                    </td>--%>

<%--                </tr>--%>

<%--            </table>--%>
<%--        </div>--%>

<%--        <div>--%>
<%--            <h3>--%>
<%--                <%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINT_AGAINST, userDTO)%>--%>
<%--            </h3>--%>
<%--            <table class="table table-striped table-bordered">--%>
<%--                <tr>--%>
<%--                    <th style="width: 8%">--%>
<%--                        <b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%></b>--%>

<%--                    </th>--%>

<%--                    <th style="width: 18%">--%>
<%--                        <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%></b>--%>
<%--                    </th>--%>

<%--                    <th style="width: 20%">--%>
<%--                        <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%></b>--%>
<%--                    </th>--%>

<%--                    <th>--%>
<%--                        <b><%=LM.getText(LC.PORTAL_FEEDBACK_SEARCH_ACTIONTAKEN, userDTO)%></b>--%>
<%--                    </th>--%>
<%--                    <th><b><%=LM.getText(LC.DISCIPLINARY_DETAILS_ADD_REMARKS, userDTO)%>--%>
<%--                    </b></th>--%>
<%--                </tr>--%>

<%--                <%--%>
<%--                    if (disciplinary_detailsDTOList != null) {--%>
<%--                        for (Disciplinary_detailsDTO disciplinary_detailsDTO : disciplinary_detailsDTOList) {%>--%>
<%--                <tr>--%>
<%--                    <td style="width: 8%">--%>
<%--                        <%=Employee_recordsRepository.getInstance()--%>
<%--                                .getById(disciplinary_detailsDTO.employeeRecordsId).employeeNumber%>--%>
<%--                    </td>--%>

<%--                    <td style="width: 18%">--%>
<%--                        <%=isLanguageEnglish ? disciplinary_detailsDTO.employeeNameEn : disciplinary_detailsDTO.employeeNameBn%>--%>
<%--                    </td>--%>

<%--                    <td style="width: 20%">--%>
<%--                        <%=isLanguageEnglish ? (disciplinary_detailsDTO.organogramNameEn + ", " + disciplinary_detailsDTO.officeNameEn)--%>
<%--                                             : (disciplinary_detailsDTO.organogramNameBn + ", " + disciplinary_detailsDTO.officeNameBn)%>--%>
<%--                    </td>--%>

<%--                    <td>--%>
<%--                        <%=Complain_actionRepository.getInstance().getText(Language, disciplinary_detailsDTO.complainActionType)%>--%>
<%--                        <%--%>
<%--                            NameDTO actionDTO = Complain_actionRepository.getInstance().getDTOByID(disciplinary_detailsDTO.complainActionType);--%>
<%--                            if (actionDTO != null) {--%>
<%--                                String actionTaken = "";--%>
<%--                                if (actionDTO.nameEn.equalsIgnoreCase("Other")) {--%>
<%--                                    actionTaken = disciplinary_detailsDTO.complianOther;--%>
<%--                                } else {--%>
<%--                                    actionTaken = StringUtils.getFormattedDate(Language, disciplinary_detailsDTO.fromDate)--%>
<%--                                            + (isLanguageEnglish ? " To " : " থেকে ")--%>
<%--                                            + StringUtils.getFormattedDate(Language, disciplinary_detailsDTO.toDate);--%>
<%--                                }--%>

<%--                        %>--%>
<%--                        (<%=actionTaken%>)--%>
<%--                        <%--%>
<%--                            }--%>
<%--                        %>--%>
<%--                    </td>--%>
<%--                    <td><%=disciplinary_detailsDTO.remarks%>--%>
<%--                    </td>--%>
<%--                </tr>--%>
<%--                <%--%>
<%--                        }--%>
<%--                    }--%>
<%--                %>--%>
<%--            </table>--%>
<%--        </div>--%>


<%--    </div>--%>
<%--</div>--%>