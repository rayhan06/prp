<%@page import="dbm.DBMW" %>
<%@page import="disciplinary_details.Disciplinary_detailsDTO" %>
<%@page import="disciplinary_log.Disciplinary_logComplainByDTO" %>
<%@page import="disciplinary_log.Disciplinary_logDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="employee_assign.EmployeeSearchIds" %>
<%@page import="employee_assign.EmployeeSearchModalUtil" %>
<%@page import="employee_records.Employee_recordsRepository" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="incident.IncidentRepository" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="disciplinary_details.Disciplinary_detailsDAO" %>
<%@ page import="disciplinary_log.Disciplinary_logComplainByDAO" %>

<%
    Disciplinary_logDTO disciplinary_logDTO = (Disciplinary_logDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    if (disciplinary_logDTO == null) {
        disciplinary_logDTO = new Disciplinary_logDTO();

    }

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    List<Disciplinary_detailsDTO> detailsDTOList=null;
    List<Disciplinary_logComplainByDTO> complainByDTOList=null;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.DISCIPLINARY_LOG_EDIT_DISCIPLINARY_LOG_EDIT_FORMNAME, userDTO);
        detailsDTOList= new Disciplinary_detailsDAO().getByForeignKey(disciplinary_logDTO.incidentNumber);
        complainByDTOList=new Disciplinary_logComplainByDAO().getByForeignKey(disciplinary_logDTO.incidentNumber);
    } else {
        formTitle = LM.getText(LC.DISCIPLINARY_LOG_ADD_DISCIPLINARY_LOG_ADD_FORMNAME, userDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String url="Disciplinary_logServlet?actionType="+"ajax_"+actionName+"&isPermanentTable=true";
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" action="<%=url%>"
              id="disciplinary_form" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.DISCIPLINARY_LOG_ADD_REPORTEDDATE, userDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='reportedDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="reported-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' id='reported-date' name='reportedDate'
                                                   value='' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINTSOURCECAT, userDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='complaintSourceCat_div_<%=i%>'>
                                            <select class='form-control' name='complaintSourceCat'
                                                    id='complaintSourceCat_category' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("complaint_source", Language, disciplinary_logDTO.complaintSourceCat)%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row" id="complain_by_div"
                                         style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, userDTO)%>
                                            <span class="required"> * </span>
                                        </label>

                                        <div class="col-md-9" id="complainedBy_external_div">

                                            <input type='text' id='complainedBy_external'
                                                   class='form-control'
                                                   name='complainedByExternal' tag='pb_html'
                                                   value='<%=disciplinary_logDTO.complainedByExternal%>'>
                                        </div>

                                        <div class="col-md-9" id="complainedBy_internal_div">
                                            <div>
                                                <input type='hidden' id='complainedBy_internal'
                                                       class='form-control'
                                                       name='complainedByInternal'
                                                       tag='pb_html' value=''>

                                                <button id="complainedBy_modal_button" type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius mb-3">
                                                    <%=isLanguageEnglish ? "Select " + LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, userDTO)
                                                            : LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, userDTO) + " বাছাই করুন"%>
                                                </button>

                                               <div class="table-responsive">
                                                   <table class="table table-bordered table-striped text-nowrap">
                                                       <thead>
                                                       <tr>
                                                           <th style="width: 12%">
                                                               <b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%>
                                                               </b></th>
                                                           <th style="width: 30%">
                                                               <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%>
                                                               </b></th>
                                                           <th style="width: 53%">
                                                               <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%>
                                                               </b></th>
                                                           <th style="width: 5%"></th>
                                                       </tr>
                                                       </thead>

                                                       <tbody id="complainedBy_internal_table">
                                                       <tr style="display: none;">
                                                           <td></td>
                                                           <td></td>
                                                           <td></td>
                                                           <td>
                                                               <button type="button"
                                                                       class="btn btn-sm delete-trash-btn"
                                                                       onclick="remove_containing_row(this,'complainedBy_internal_table');">
                                                                   <i class="fa fa-trash"></i>
                                                               </button>
                                                           </td>
                                                       </tr>
                                                       <%if (complainByDTOList != null) {%>
                                                       <%for (Disciplinary_logComplainByDTO complainByDTO : complainByDTOList) {%>
                                                       <tr>
                                                           <td>
                                                               <%=complainByDTO.employeeUserName%>
                                                           </td>
                                                           <td>
                                                               <%=isLanguageEnglish ? complainByDTO.employeeNameEn : complainByDTO.employeeNameBn%>
                                                           </td>
                                                           <td>
                                                               <%=isLanguageEnglish ? (complainByDTO.organogramNameEn + ", " + complainByDTO.officeNameEn)
                                                                       : (complainByDTO.organogramNameBn + ", " + complainByDTO.officeNameBn)%>
                                                           </td>

                                                           <td id='<%=complainByDTO.employeeRecordsId%>_td_button'>
                                                               <button type="button"
                                                                       class="btn btn-sm delete-trash-btn"
                                                                       onclick="remove_containing_row(this,'complainedBy_internal_table');">
                                                                   <i class="fa fa-trash"></i>
                                                               </button>
                                                           </td>
                                                       </tr>
                                                       <%}%>
                                                       <%}%>
                                                       </tbody>
                                                   </table>
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- Tag Employee --%>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINT_AGAINST, userDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <div>
                                                <!-- Button trigger modal -->
                                                <button type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                        id="tagEmp_modal_button">
                                                    <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_TAG_EMPLOYEE, userDTO)%>
                                                </button>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped text-nowrap">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 12%">
                                                                <b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%>
                                                                </b></th>
                                                            <th style="width: 30%">
                                                                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%>
                                                                </b></th>
                                                            <th style="width: 53%">
                                                                <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%>
                                                                </b></th>
                                                            <th style="width: 5%"></th>
                                                        </tr>
                                                        </thead>

                                                        <tbody id="tagged_emp_table">
                                                        <tr style="display: none;">
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <button type="button"
                                                                        class="btn btn-sm delete-trash-btn"
                                                                        onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <%if (detailsDTOList != null) {%>
                                                        <%for (Disciplinary_detailsDTO detailsDTO : detailsDTOList) {%>
                                                        <tr>
                                                            <td>
                                                                <%--employeeNumber and user name is same--%>
                                                                <%=Employee_recordsRepository.getInstance()
                                                                        .getById(detailsDTO.employeeRecordsId).employeeNumber%>
                                                            </td>
                                                            <td>
                                                                <%=isLanguageEnglish ? detailsDTO.employeeNameEn : detailsDTO.employeeNameBn%>
                                                            </td>
                                                            <td>
                                                                <%=isLanguageEnglish ? (detailsDTO.organogramNameEn + ", " + detailsDTO.officeNameEn)
                                                                        : (detailsDTO.organogramNameBn + ", " + detailsDTO.officeNameBn)%>
                                                            </td>
                                                            <td id='<%=detailsDTO.employeeRecordsId%>_td_button'>
                                                                <button type="button"
                                                                        class="btn btn-sm delete-trash-btn"
                                                                        onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <%}%>
                                                        <%}%>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <input type='hidden' class='form-control'
                                                       name='addedEmployees'
                                                       id='addedEmployees' value='' tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.DISCIPLINARY_LOG_ADD_INCIDENTTYPE, userDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9"
                                             id='incidentType_div_<%=i%>'>
                                            <select class='form-control' name='incidentType'
                                                    id='incidentType_select' tag='pb_html'>
                                                <%=IncidentRepository.getInstance().buildOptions(Language, disciplinary_logDTO.incidentType)%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row" id='incidentTypeOther_div'
                                         style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.DISCIPLINARY_LOG_ADD_INCIDENTTYPEOTHER, userDTO)%><span
                                                class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="col-12">
                                                <input type='text' class='form-control'
                                                       name='incidentTypeOther'
                                                       id='incidentTypeOther_text'
                                                       value='<%=disciplinary_logDTO.incidentTypeOther%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.DISCIPLINARY_LOG_ADD_INCIDENTSUMMARY, userDTO)%>
                                        </label>
                                        <div class="col-md-9"
                                             id='incidentSummary_div_<%=i%>'>
						                                    <textarea class='form-control' name='incidentSummary'
                                                                      id='incidentSummary_text_<%=i%>'
                                                                      style="resize: none" maxlength="2048" rows="4"
                                                                      tag='pb_html'><%=disciplinary_logDTO.incidentSummary%></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.DISCIPLINARY_LOG_ADD_FILESDROPZONE, userDTO)%>
                                        </label>
                                        <div class="col-md-9 " id='filesDropzone_div_<%=i%>'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_logDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %> <img
                                                            src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                            style='width: 100px'/> <%
                                                        }
                                                    %> <a
                                                            href='Disciplinary_logServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                            download><%=filesDTO.fileTitle%>
                                                    </a> <a class='btn btn-danger'
                                                            onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%
                                                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                            %>
                                            <div class="dropzone"
                                                 action="Disciplinary_logServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit") ? disciplinary_logDTO.filesDropzone : ColumnID%>">
                                                <input type='file' style="display: none"
                                                       name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='filesDropzone'
                                                   id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=actionName.equals("edit") ? disciplinary_logDTO.filesDropzone : ColumnID%>'/>
                                            <input type='hidden' class='form-control' name='iD'
                                                   id='iD_hidden_<%=i%>'
                                                   value='<%=disciplinary_logDTO.iD%>' tag='pb_html'/>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                            <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_DISCIPLINARY_LOG_CANCEL_BUTTON, userDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn" type="button" onclick="submitForm()">
                            <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_DISCIPLINARY_LOG_SUBMIT_BUTTON, userDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<%--Include with Param --%>
<%--isHierarchyNeeded--%>
<%--true : restrict employee to view other employee's data based on hierarchy--%>
<%--false: starting from top office show all employee--%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">
    language = "<%=Language%>";
    const other = language == 'English' ? 'Other' : 'অন্যান্য';
    const closed = language == 'English' ? 'CLOSED' : 'বন্ধ';
    const internal = language === 'English' ? 'internal' : 'অভ্যন্তরীন';
    const external = language === 'English' ? 'external' : 'বহিরাগত';
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const disciplinaryForm = $('#disciplinary_form');

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        showOtherIncident();
        showResolvedDate();
        showOrHideComplaintBy(false);
        select2SingleSelector('#incidentType_select', '<%=Language%>');
        select2SingleSelector('#complaintSourceCat_category', '<%=Language%>');

        $('#incidentType_select').change(function () {
            showOtherIncident();
        });

        $('#complaintSourceCat_category').change(function () {
            showOrHideComplaintBy(true);
        });

        $('#complaintStatusCat_category').change(function () {
            showResolvedDate();
        });
        dateTimeInit("<%=Language%>");
        initValidation();
    });

    function initValidation() {
        $.validator.addMethod('complaintSourceSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('incidentTypeSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('complaintStatusCatSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('complaintResolvedDateSelection', function (value, element) {
            if (!$('#complaintResolvedDate_div').is(":visible")) {
                return true;
            }
            let rd = convertToDate($('#reportedDate_date').val());
            let closeDate = convertToDate(value);
            let today = new Date();
            today.setHours(0, 0, 0, 0);
            return closeDate >= rd && closeDate <= today;
        });
        $.validator.addMethod('reportedDateSelection', function (value, element) {
            let rd = convertToDate(value);
            let today = new Date();
            today.setHours(0, 0, 0, 0);
            return rd <= today;
        });
        $("#disciplinary_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                complaintSourceCat: {
                    required: true,
                    complaintSourceSelection: true
                },
                complainedBy: {
                    required: true
                },
                incidentType: {
                    required: true,
                    incidentTypeSelection: true
                },
                incidentTypeOther: {
                    required: function () {
                        return $('#incidentTypeOther_text').is(":visible");
                    }
                },
                complaintStatusCat: {
                    required: true,
                    complaintStatusCatSelection: true
                },
                complaintResolvedDate: {
                    required: true,
                    complaintResolvedDateSelection: true
                }
            },

            messages: {
                complaintSourceCat: language=="English"?"Please select a complaint source":"অভিযোগের উৎস",
                complainedBy: "Please select a complaint by",
                incidentType: language=="English"?"Please select a incident type":"ঘটনার ধরন নির্বাচন করুন",
                incidentTypeOther: "Please enter other incident type",
                complaintResolvedDate: "Resolved date should be after or equal date of reported date and before or equal today",
                complaintStatusCat: "Please select a complaint status"
            }
        });
    }

    function convertToDate(dateString) {
        const dateParts = dateString.split("/");
        return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }

    function showOtherIncident() {
        console.log("Type Changed");
        if ($('#incidentType_select option:selected').text() === other) {
            $('#incidentTypeOther_div').show();
        } else {
            $('#incidentTypeOther_div').hide();
        }
    }


    function showOrHideComplaintBy(bool_doClearVal) {

        switch ($('#complaintSourceCat_category option:selected').text().toLowerCase()) {
            case internal:
                $('#complain_by_div').show();
                $('#complainedBy_internal_div').show();
                $('#complainedBy_external_div').hide();
                break;
            case external:
                $('#complain_by_div').show();
                $('#complainedBy_internal_div').hide();
                $('#complainedBy_external_div').show();
                break;
            default:
                $('#complain_by_div').hide();
        }
        if (bool_doClearVal) {
            $('#complainedBy_external').val('');
        }
    }

    function showResolvedDate() {
        if ($('#complaintStatusCat_category option:selected').text() === closed) {
            $('#complaintResolvedDate_div').show();
        } else {
            $('#complaintResolvedDate_div').hide();
        }
    }

    function PreprocessBeforeSubmiting(row, validate) {
        $("#disciplinary_form").validate();
        document.getElementById("addedEmployees").value = JSON.stringify(
            Array.from(added_employee_info_map.values())
        );

        document.getElementById("complainedBy_internal").value = JSON.stringify(
            Array.from(complained_by_info_map.values())
        );


        $('#reported-date').val(getDateStringById('reported-date-js'));
        const jQueryValid = disciplinaryForm.valid();
        const dateValid=dateValidator('reported-date-js', true, {
            'errorEn': 'Enter valid report date!',
            'errorBn': 'নালিশের তারিখ প্রবেশ করান!'
        });
        return jQueryValid && dateValid;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Disciplinary_logServlet");
    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;

    $(function () {


        <%if(actionName.equals("edit")) {%>
        setDateByTimestampAndId('reported-date-js', <%=disciplinary_logDTO.reportedDate%>);
        <%}%>
    });
    function submitForm() {
        buttonStateChange(true);
        if(PreprocessBeforeSubmiting(0,'<%=actionName%>')){
            submitAjaxForm('disciplinary_form');
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button
    // map to store and send added employee data
    <%
        // add a list of employees
        // make a list of EmployeeSearchIds
        List<EmployeeSearchIds> addedEmpIdsList = null;
        if(detailsDTOList != null){
            addedEmpIdsList = detailsDTOList.stream()
                                            .map(dto -> new EmployeeSearchIds(dto.employeeRecordsId,dto.officeUnitId,dto.organogramId))
                                            .collect(Collectors.toList());
        }
        // to initialize map for a Single Employee use
        // EmployeeSearchModalUtil.initJsMapSingleEmployee(employeeRecordId,officeUnitId,organogramId)
    %>
    added_employee_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);


    // map to store and send added employee data
    <%
        List<EmployeeSearchIds> complainedByIdsList = null;
        if(complainByDTOList != null){
            complainedByIdsList = complainByDTOList.stream()
                                                   .map(dto -> new EmployeeSearchIds(dto.employeeRecordsId,dto.officeUnitId,dto.organogramId))
                                                   .collect(Collectors.toList());
        }
    %>
    complained_by_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(complainedByIdsList)%>);

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: false,
                callBackFunction: function (empInfo) {
                    console.log('callBackFunction called with latest added emp info JSON');
                    console.log(empInfo);
                }
            }],
            ['complainedBy_internal_table', {
                info_map: complained_by_info_map,
                isSingleEntry: false,
                callBackFunction: function (empInfo) {
                    console.log('callBackFunction called with latest added emp info JSON');
                    console.log(empInfo);
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });

    $('#complainedBy_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'complainedBy_internal_table';
        $('#search_emp_modal').modal();
    });
</script>