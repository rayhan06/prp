<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="disciplinary_log.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="user.*" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="common.RoleEnum" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="common.BaseServlet" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String navigator2 = SessionConstants.NAV_DISCIPLINARY_LOG;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    boolean isUserParliamentMember = userDTO.roleID == RoleEnum.EMPLOYEE_OF_PARLIAMENT.getRoleId();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTSTATUSCAT, userDTO)%>
            </th>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_INCIDENTTYPE, userDTO)%>
            </th>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTSOURCECAT, userDTO)%>
            </th>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_REPORTEDDATE, userDTO)%>
            </th>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINTRESOLVEDDATE, userDTO)%>
            </th>
            <%
                if (hasAjax) {
                    if (request.getParameter("complain_by") != null) {
            %>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINT_BY, userDTO)%>
            </th>
            <%
                }
                if (request.getParameter("complain_for") != null) {
            %>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT_COMPLAINT_AGAINST, userDTO)%>
            </th>
            <%
                    }
                }
            %>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, userDTO)%>
            </th>
            <%
                if (!isUserParliamentMember) {
            %>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_EDIT__TAKE_ACTION, userDTO)%>
            </th>
            <th><%=LM.getText(LC.DISCIPLINARY_LOG_SEARCH_DISCIPLINARY_LOG_EDIT_BUTTON, userDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
            <%
                }
            %>
        </tr>
        </thead>
        <tbody>
        <%
            List<Disciplinary_logDTO> data = (List<Disciplinary_logDTO>) rn2.list;
                if (data != null && data.size() > 0) {
                    for (Disciplinary_logDTO disciplinary_logDTO : data) {
        %>
                        <tr><%@include file="disciplinary_logSearchRow.jsp" %></tr>
        <% } }%>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>