<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="pi_office_unit_store_keeper_mapping.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="offices.OfficesRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>

<%
Pi_office_unit_store_keeper_mappingDTO pi_office_unit_store_keeper_mappingDTO = new Pi_office_unit_store_keeper_mappingDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	pi_office_unit_store_keeper_mappingDTO = Pi_office_unit_store_keeper_mappingDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = pi_office_unit_store_keeper_mappingDTO;
String tableName = "pi_office_unit_store_keeper_mapping";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_FORMNAME, loginDTO);
String servletName = "Pi_office_unit_store_keeper_mappingServlet";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_office_unit_store_keeper_mappingServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pi_office_unit_store_keeper_mappingDTO.iD%>' tag='pb_html'/>
	

													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_ORGANOGRAMID, loginDTO)%></label>
                                                            <div class="col-8">
																<button type="button" class="btn btn-primary form-control" onclick="addEmployeeWithRow(this.id)" id="organogramId_button_<%=i%>" tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%></button>
																<table class="table table-bordered table-striped">
																	<tbody id="organogramId_table_<%=i%>" tag='pb_html'>
																	<%
																	if(pi_office_unit_store_keeper_mappingDTO.organogramId != -1)
																	{
																		%>
																		<tr>
																			<td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(pi_office_unit_store_keeper_mappingDTO.organogramId)%></td>
																			<td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(pi_office_unit_store_keeper_mappingDTO.organogramId, Language)%></td>
																			<td><%=WorkflowController.getOrganogramName(pi_office_unit_store_keeper_mappingDTO.organogramId, Language)%>, <%=WorkflowController.getUnitNameFromOrganogramId(pi_office_unit_store_keeper_mappingDTO.organogramId, Language)%></td>
																		</tr>
																		<%
																	}
																	%>
																	</tbody>
																</table>
																<input type='hidden' class='form-control'  name = 'organogramId' id = 'organogramId_hidden_<%=i%>' value='<%=pi_office_unit_store_keeper_mappingDTO.organogramId%>' tag='pb_html'/>
			
															</div>
                                                      </div>


                                                    <div class="form-group row ">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_OFFICEID, loginDTO)%>
<%--                                                            <span class="required" style="color: red;"> * </span>--%>
                                                        </label>

                                                        <div class="col-6">
                                                            <input type="hidden" name='officeUnitId'
                                                                   id='office_units_id_input' value="<%=pi_office_unit_store_keeper_mappingDTO.officeUnitId%>">

                                                            <%
                                                                Office_unitsDTO office_unitsDTO = null;
                                                                String office = "";
                                                                if(actionName.equalsIgnoreCase("ajax_edit")){
                                                                    office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(pi_office_unit_store_keeper_mappingDTO.officeUnitId);
                                                                    if(office_unitsDTO != null){

                                                                        office = Language.equalsIgnoreCase("English")?office_unitsDTO.unitNameEng:office_unitsDTO.unitNameBng;
                                                                    }
                                                                }

                                                            %>

                                                            <button type="button" class="btn btn-secondary form-control shadow btn-border-radius"
                                                                    disabled id="office_units_id_text" ><%=office%></button>
                                                        </div>
                                                        <div class="col-2">
                                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                                                    id="office_units_id_modal_button"
                                                                    onclick="officeModalButtonClicked();">
                                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                                            </button>
                                                        </div>


                                                    </div>


														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=pi_office_unit_store_keeper_mappingDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=pi_office_unit_store_keeper_mappingDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=pi_office_unit_store_keeper_mappingDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='lastModifierUser' id = 'lastModifierUser_hidden_<%=i%>' value='<%=pi_office_unit_store_keeper_mappingDTO.lastModifierUser%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=pi_office_unit_store_keeper_mappingDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=pi_office_unit_store_keeper_mappingDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

function viewOfficeIdInInput(selectedOffice) {
    if (selectedOffice.id === '') {
        return;
    }
    // console.log(selectedOffice);
    document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
    $('#office_units_id_input').val(selectedOffice.id);
}

officeSelectModalUsage = 'none';
officeSelectModalOptionsMap = new Map([
    ['officeUnitId', {
        officeSelectedCallback: viewOfficeIdInInput
    }]
]);

function officeModalButtonClicked() {
    // console.log('Button Clicked!');
    officeSelectModalUsage = 'officeUnitId';
    $('#search_office_modal').modal();
}



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Pi_office_unit_store_keeper_mappingServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






