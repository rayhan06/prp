

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_office_unit_store_keeper_mapping.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>


<%
String servletName = "Pi_office_unit_store_keeper_mappingServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_office_unit_store_keeper_mappingDTO pi_office_unit_store_keeper_mappingDTO = Pi_office_unit_store_keeper_mappingDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pi_office_unit_store_keeper_mappingDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_OFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">

                                            <%
                                                Office_unitsDTO office_unitsDTO = null;
                                                value = "";

                                                office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(pi_office_unit_store_keeper_mappingDTO.officeUnitId);
                                                if(office_unitsDTO != null){

                                                    value = Language.equalsIgnoreCase("English")?office_unitsDTO.unitNameEng:office_unitsDTO.unitNameBng;
                                                }


                                            %>

											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_ORGANOGRAMID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_office_unit_store_keeper_mappingDTO.organogramId + "";
											%>
											<%
											value = WorkflowController.getNameFromOrganogramId(pi_office_unit_store_keeper_mappingDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(pi_office_unit_store_keeper_mappingDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(pi_office_unit_store_keeper_mappingDTO.organogramId, Language);
											%>											
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>