
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_office_unit_store_keeper_mapping.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="office_units.Office_unitsRepository" %>


<%
String navigator2 = "navPI_OFFICE_UNIT_STORE_KEEPER_MAPPING";
String servletName = "Pi_office_unit_store_keeper_mappingServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_OFFICEUNITID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_ADD_ORGANOGRAMID, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_SEARCH_PI_OFFICE_UNIT_STORE_KEEPER_MAPPING_EDIT_BUTTON, loginDTO)%></th>
<%--								<th class="">--%>
<%--									<div class="text-center">--%>
<%--										<span>All</span>--%>
<%--									</div>--%>
<%--									<div class="d-flex align-items-center justify-content-between mt-3">--%>
<%--										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">--%>
<%--											<i class="fa fa-trash"></i>--%>
<%--										</button>--%>
<%--										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>--%>
<%--									</div>--%>
<%--								</th>--%>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Pi_office_unit_store_keeper_mappingDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Pi_office_unit_store_keeper_mappingDTO pi_office_unit_store_keeper_mappingDTO = (Pi_office_unit_store_keeper_mappingDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = pi_office_unit_store_keeper_mappingDTO.officeUnitId + "";
											value = Office_unitsRepository.getInstance().geText(Language, pi_office_unit_store_keeper_mappingDTO.officeUnitId);
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_office_unit_store_keeper_mappingDTO.organogramId + "";
											%>
											<%
											value = WorkflowController.getNameFromOrganogramId(pi_office_unit_store_keeper_mappingDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(pi_office_unit_store_keeper_mappingDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(pi_office_unit_store_keeper_mappingDTO.organogramId, Language);
											%>											
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = pi_office_unit_store_keeper_mappingDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>
																						
<%--											<td class="text-right">--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=pi_office_unit_store_keeper_mappingDTO.iD%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			