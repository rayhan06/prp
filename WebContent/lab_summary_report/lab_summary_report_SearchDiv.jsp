<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="workflow.WorkflowController"%>
<%@ page import="pb.*" %>
<%@ page import="java.util.*" %>
<%@page import="employee_offices.*" %>
<%@page pageEncoding="UTF-8"%>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.NURSE_SUMMARY_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
			
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=isLangEng?"Lab Technologist":"ল্যাব টেকনোলজিস্ট"%>
				</label>
				<div class="col-sm-9">
					<select  class='form-control' name='userName'
                          id='userName' 
                          tag='pb_html' >
                          <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
                          <%
                          Set<Long> emps = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.LAB_TECHNITIAN_ROLE);

                          for(Long em: emps)
                          {
                          %>
                          <option value = "<%=WorkflowController.getUserNameFromOrganogramId(em)%>">
                          <%=WorkflowController.getNameFromOrganogramId(em, Language)%>
                          </option>
                          <%
                          }
                          %>
                    </select>							
				</div>
			</div>
		</div>
		
    </div>
</div>
<script type="text/javascript">
function init()
{
	$("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = false;
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}
</script>