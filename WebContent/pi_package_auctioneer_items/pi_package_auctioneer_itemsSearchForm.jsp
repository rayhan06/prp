
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_package_auctioneer_items.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navPI_PACKAGE_AUCTIONEER_ITEMS";
String servletName = "Pi_package_auctioneer_itemsServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PIPACKAGEAUCTIONEERID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PIPACKAGEAUCTIONEERCHILDRENID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PIAUCTIONID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_FISCALYEARID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PRICE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_SEARCH_PI_PACKAGE_AUCTIONEER_ITEMS_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Pi_package_auctioneer_itemsDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO = (Pi_package_auctioneer_itemsDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = pi_package_auctioneer_itemsDTO.piPackageAuctioneerId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_package_auctioneer_itemsDTO.piPackageAuctioneerChildrenId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_package_auctioneer_itemsDTO.piAuctionId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_package_auctioneer_itemsDTO.fiscalYearId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
											<td>
											<%
											value = pi_package_auctioneer_itemsDTO.price + "";
											%>
											<%
											value = String.format("%.1f", pi_package_auctioneer_itemsDTO.price);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = pi_package_auctioneer_itemsDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=pi_package_auctioneer_itemsDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			