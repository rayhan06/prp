

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_package_auctioneer_items.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Pi_package_auctioneer_itemsServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO = Pi_package_auctioneer_itemsDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pi_package_auctioneer_itemsDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PI_PACKAGE_AUCTIONEER_ITEMS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PI_PACKAGE_AUCTIONEER_ITEMS_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PIPACKAGEAUCTIONEERID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_package_auctioneer_itemsDTO.piPackageAuctioneerId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PIPACKAGEAUCTIONEERCHILDRENID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_package_auctioneer_itemsDTO.piPackageAuctioneerChildrenId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PIAUCTIONID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_package_auctioneer_itemsDTO.piAuctionId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_FISCALYEARID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_package_auctioneer_itemsDTO.fiscalYearId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ITEMS_ADD_PRICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_package_auctioneer_itemsDTO.price + "";
											%>
											<%
											value = String.format("%.1f", pi_package_auctioneer_itemsDTO.price);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>