

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="pi_package_auctioneer.*"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="pi_auction.Pi_auctionRepository" %>
<%@ page import="pi_package_auctioneer_items.Pi_package_auctioneer_itemsDTO" %>
<%@ page import="pi_package_auctioneer_items.Pi_package_auctioneer_itemsDAO" %>
<%@ page import="pi_auction.Pi_auctionDTO" %>
<%@ page import="pi_auction.Pi_auctionDAO" %>


<%
String servletName = "Pi_package_auctioneerServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_package_auctioneerDTO pi_package_auctioneerDTO = Pi_package_auctioneerDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pi_package_auctioneerDTO;
Pi_auctionDTO pi_auctionDTO = null;
if(pi_package_auctioneerDTO != null){
	pi_auctionDTO = Pi_auctionDAO.getInstance().getDTOByID(pi_package_auctioneerDTO.piAuctionId);
}

%>
<%@include file="../pb/viewInitializer.jsp"%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>


								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PIAUCTIONID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = Pi_auctionRepository.getInstance().getPi_auctionDTOByiD(pi_package_auctioneerDTO.piAuctionId).auctionPackageName+ "";
											%>

											<%=value%>


                                    </div>
                                </div>


								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDERADVERTISECAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">


										<%
											value = "";
											value = pi_package_auctioneerDTO.tenderAdvertiseCat+"";
											StringTokenizer tokenizer = new StringTokenizer(value, ",");
											String separator = "";
											value = "";
											while (tokenizer.hasMoreTokens()) {
												String val = tokenizer.nextToken().trim();
												if(!val.equalsIgnoreCase("")){
													Long distNum = Long.valueOf(val);
													String advertiseMedium = CatRepository.getInstance().getText(Language, "tender_advertise", distNum);
													value = value + separator + advertiseMedium;
													separator = ", ";
												}
											}
										%>

										<%=value%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDERADVERTISEDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.tenderAdvertiseDate + "";
											%>
											<%
											String formatted_tenderAdvertiseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_tenderAdvertiseDate, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDERADVERTISETIME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.tenderAdvertiseTime + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDEROPENINGDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.tenderOpeningDate + "";
											%>
											<%
											String formatted_tenderOpeningDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_tenderOpeningDate, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDEROPENINGTIME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.tenderOpeningTime + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_NOADATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.noaDate + "";
											%>
											<%
											String formatted_noaDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_noaDate, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_NOATIME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.noaTime + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_AGREEMENTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.agreementDate + "";
											%>
											<%
											String formatted_agreementDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_agreementDate, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_AGREEMENTTIME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.agreementTime + "";
											%>

											<%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_AGREEMENTENDINGDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.agreementEndingDate + "";
											%>
											<%
											String formatted_agreementEndingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_agreementEndingDate, Language)%>


                                    </div>
                                </div>

								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_AGREEMENTENDINGTIME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = pi_package_auctioneerDTO.agreementEndingTime + "";
											%>

											<%=Utils.getDigits(value, Language)%>
                                    </div>
                                </div>
							</div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5 class="table-title"><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_PIPACKAGEAUCTIONEERID, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_NAME, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_ADDRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_MOBILE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_ISCHOSEN, loginDTO)%></th>

							</tr>
							<%
                        	PiPackageAuctioneerChildrenDAO piPackageAuctioneerChildrenDAO = PiPackageAuctioneerChildrenDAO.getInstance();
                         	List<PiPackageAuctioneerChildrenDTO> piPackageAuctioneerChildrenDTOs = (List<PiPackageAuctioneerChildrenDTO>)piPackageAuctioneerChildrenDAO.getDTOsByParent("pi_package_auctioneer_id", pi_package_auctioneerDTO.iD);

                         	for(PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO: piPackageAuctioneerChildrenDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = piPackageAuctioneerChildrenDTO.name + "";
											%>

											<%=Utils.getDigits(value, Language)%>


										</td>
										<td>
											<%
											value = piPackageAuctioneerChildrenDTO.address + "";
											%>
											<%=value%>


										</td>
										<td>
											<%
											value = piPackageAuctioneerChildrenDTO.mobile + "";
											%>

											<%=Utils.getDigits(value, Language)%>


										</td>
										<td>
											<%
											value = piPackageAuctioneerChildrenDTO.isChosen + "";
											%>

											<%=Utils.getYesNo(value, Language)%>


										</td>

                         			</tr>
                         		<%

                         	}

                        %>
						</table>
                    </div>
                </div>

			<div class="mt-4">
				<div class="form-body">
					<h5 class="table-title"><%=LM.getText(LC.PI_AUCTION_ADD_PRODUCT_PACKAGE, loginDTO)%></h5>
					<div class="table-responsive">
						<table class="table table-bordered table-striped text-nowrap">
							<thead>
							<tr id="dynamicTrHeader">
								<th><%=LM.getText(LC.PI_AUCTION_ADD_AUCTIONPACKAGENAME, loginDTO)%></th>
								<%int headerIndex=1;%>

								<%for(PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO1: pi_package_auctioneerDTO.piPackageAuctioneerChildrenDTOList)
								{
								%>
								<th id="auctioneer_header_<%=headerIndex++%>"><%=piPackageAuctioneerChildrenDTO1.name%></th>
								<%}%>

							</tr>
							</thead>
							<tbody id="NAuctioneerProducts">


							<%
								int pvpIndex=-1;
								pvpIndex++;
							%>

							<tr class="productsAndAuctioneerPriceTr" id="productsAndAuctioneerPriceTr_<%=pvpIndex%>">
								<td class="productsAndVendorPriceTd">
									<%
										if(pi_auctionDTO != null){
									%>
										<%=pi_auctionDTO.auctionPackageName%>
									<%}%>
								</td>

								<%
									int forDelIndex = 1;
									List<Pi_package_auctioneer_itemsDTO> pi_package_auctioneer_itemsDTOS = Pi_package_auctioneer_itemsDAO.getInstance()
											.getAllDTOByAuctioneerAndAuctionPackageId
													(pi_package_auctioneerDTO.iD,pi_package_auctioneerDTO.piAuctionId);
									for(PiPackageAuctioneerChildrenDTO piPackageChildDTO: pi_package_auctioneerDTO.piPackageAuctioneerChildrenDTOList)
									{
										Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO = Pi_package_auctioneer_itemsDAO.getInstance()
												.getPiPackageItemsDtoByChildrenId(pi_package_auctioneer_itemsDTOS,piPackageChildDTO.iD);
										if(pi_package_auctioneer_itemsDTO!=null){
								%>

								<td class="auctioneer_data_<%=(forDelIndex)%>"><%=Utils.getDigits(pi_package_auctioneer_itemsDTO.price,Language)%></td>

								<%}else{%>
								<td class="auctioneer_data_<%=(forDelIndex)%>"></td>

								<%}%>
								<%}%>


							</tr>
							<%

							%>

							</tbody>
						</table>
					</div>


				</div>
			</div>
        </div>
    </div>
</div>