<%@page import="pi_package_auctioneer.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="pi_auction.Pi_auctionDTO" %>
<%@ page import="pi_auction.Pi_auctionDAO" %>
<%@ page import="pi_package_auctioneer_items.Pi_package_auctioneer_itemsDTO" %>
<%@ page import="pi_package_auctioneer_items.Pi_package_auctioneer_itemsDAO" %>
<%@ page import="pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository" %>

<%
    Pi_package_auctioneerDTO pi_package_auctioneerDTO = new Pi_package_auctioneerDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_package_auctioneerDTO = Pi_package_auctioneerDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_package_auctioneerDTO;
    String tableName = "pi_package_auctioneer";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_package_auctioneerServlet";

    long piItemAuctionId = -1;
    long officeUnitId = -1;
    Pi_auctionDTO pi_auctionDTO = null;
    if (request.getParameter("piItemAuctionId") != null) {
        piItemAuctionId = Long.parseLong(request.getParameter("piItemAuctionId"));
        pi_auctionDTO = Pi_auctionDAO.getInstance().getDTOByID(piItemAuctionId);
        if (pi_auctionDTO != null) {
            officeUnitId = pi_auctionDTO.officeUnitId;
        }
    }
    if (actionName.equalsIgnoreCase("ajax_edit") && pi_package_auctioneerDTO != null) {
        piItemAuctionId = pi_package_auctioneerDTO.piAuctionId;
        pi_auctionDTO = Pi_auctionDAO.getInstance().getDTOByID(piItemAuctionId);
        if (pi_auctionDTO != null) {
            officeUnitId = pi_auctionDTO.officeUnitId;
        }
    }

    List<Pi_package_auctioneer_itemsDTO> pi_package_auctioneer_itemsDTOS = Pi_package_auctioneer_itemsDAO.getInstance()
            .getAllDTOByAuctioneerAndAuctionPackageId
                    (pi_package_auctioneerDTO.iD, pi_auctionDTO.iD);

    String levelOptions = Pi_vendor_auctioneer_detailsRepository.getInstance().getOptions(Language, -1, "2");

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row mx-md-2">
                                <div class="col-md-12 ">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>

                                        <!--Child table started-->
                                        <div class="mt-4">
                                            <div class="form-body">
                                                <h5 class="table-title">
                                                    <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN, loginDTO)%>
                                                </h5>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped text-nowrap">
                                                        <thead>
                                                        <tr>
                                                            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_NAME, loginDTO)%>
                                                            </th>
                                                            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_ADDRESS, loginDTO)%>
                                                            </th>
                                                            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_MOBILE, loginDTO)%>
                                                            </th>
                                                            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_ISCHOSEN, loginDTO)%>
                                                            </th>
                                                            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_REMOVE, loginDTO)%>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="field-PiPackageAuctioneerChildren">


                                                        <%
                                                            if (actionName.equals("ajax_edit")) {
                                                                int index = -1;

                                                                for (PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO : pi_package_auctioneerDTO.piPackageAuctioneerChildrenDTOList) {
                                                                    index++;

                                                        %>

                                                        <tr id="PiPackageAuctioneerChildren_<%=index + 1%>">
                                                            <td style="display: none;">
                                                                <input type='hidden' class='form-control'
                                                                       name='piPackageAuctioneerChildren.iD'
                                                                       id='iD_hidden_<%=childTableStartingID%>'
                                                                       value='<%=piPackageAuctioneerChildrenDTO.iD%>'
                                                                       tag='pb_html'/>

                                                            </td>
                                                            <td style="display: none;">
                                                                <input type='hidden' class='form-control'
                                                                       name='piPackageAuctioneerChildren.piPackageAuctioneerId'
                                                                       id='piPackageAuctioneerId_hidden_<%=childTableStartingID%>'
                                                                       value='<%=piPackageAuctioneerChildrenDTO.piPackageAuctioneerId%>'
                                                                       tag='pb_html'/>
                                                            </td>
                                                            <td>

                                                                <select class='form-control'
                                                                        onchange="vendorChanged(this)"
                                                                        data-selectVal="<%=piPackageAuctioneerChildrenDTO.actualAuctioneerId%>"
                                                                        name='piPackageAuctioneerChildren.name'
                                                                        id='name_text_<%=childTableStartingID%>'
                                                                        tag='pb_html'>
                                                                </select>
                                                            </td>
                                                            <td>

                                                                <input type='text'
                                                                       class='form-control'
                                                                       name='address_text'
                                                                       id='address_geoTextField_<%=childTableStartingID%>'
                                                                       value='<%=piPackageAuctioneerChildrenDTO.address%>'
                                                                       tag='pb_html' disabled="disabled">

                                                            </td>
                                                            <td>

                                                                <input type='text' class='form-control'
                                                                       name='piPackageAuctioneerChildren.mobile'
                                                                       id='mobile_text_<%=childTableStartingID%>'
                                                                       required="required"
                                                                       pattern="^(01[3-9]{1}[0-9]{8})"
                                                                       value='<%=piPackageAuctioneerChildrenDTO.mobile ==null || piPackageAuctioneerChildrenDTO.mobile.length() == 0?"":
                                                                   (piPackageAuctioneerChildrenDTO.mobile.startsWith("88") ? piPackageAuctioneerChildrenDTO.mobile.substring(2) : piPackageAuctioneerChildrenDTO.mobile)%>'
                                                                       placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                                       title='<%=isLanguageEnglish?"personal mobile number must start with 01, then contain 9 digits"
                                                               :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                                                       tag='pb_html' disabled="disabled"/>
                                                            </td>
                                                            <td>
                                                                <input type='checkbox'
                                                                       class='form-control-sm'
                                                                       name='piPackageAuctioneerChildren.isChosen'
                                                                       id='isChosen_checkbox_<%=childTableStartingID%>'
                                                                       value='true' <%=(String.valueOf(piPackageAuctioneerChildrenDTO.isChosen).equals("true"))?("checked"):""%>
                                                                       tag='pb_html'
                                                                       onclick="onlyOne(this)">
                                                            </td>
                                                            <td style="display: none;">
                                                                <input type='hidden'
                                                                       class='form-control isChosenVal_hidden_input'
                                                                       name='piPackageAuctioneerChildren.isChosenVal'
                                                                       id='isChosenVal_hidden_<%=childTableStartingID%>'
                                                                       value='-1' tag='pb_html'/>

                                                            </td>
                                                            <td>
																<span id='chkEdit'>
																	<input type='checkbox' name='checkbox' value=''
                                                                           deletecb='true'
                                                                           class="form-control-sm"/>
																</span>
                                                            </td>
                                                        </tr>
                                                        <%
                                                                    childTableStartingID++;
                                                                }
                                                            }
                                                        %>

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="form-group">
                                                    <div class="text-right mt-3 mt-md-0">
                                                        <button
                                                                id="add-more-PiPackageAuctioneerChildren"
                                                                name="add-morePiPackageAuctioneerChildren"
                                                                type="button"
                                                                class="btn btn-sm text-white add-btn shadow">
                                                            <i class="fa fa-plus"></i>
                                                            <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                                        </button>
                                                        <button
                                                                id="remove-PiPackageAuctioneerChildren"
                                                                name="removePiPackageAuctioneerChildren"
                                                                type="button"
                                                                class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                                <%PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO = new PiPackageAuctioneerChildrenDTO();%>

                                                <template id="template-PiPackageAuctioneerChildren">
                                                    <tr>
                                                        <td style="display: none;">
                                                            <input type='hidden' class='form-control'
                                                                   name='piPackageAuctioneerChildren.iD' id='iD_hidden_'
                                                                   value='<%=piPackageAuctioneerChildrenDTO.iD%>'
                                                                   tag='pb_html'/>

                                                        </td>
                                                        <td style="display: none;">

                                                            <input type='hidden' class='form-control'
                                                                   name='piPackageAuctioneerChildren.piPackageAuctioneerId'
                                                                   id='piPackageAuctioneerId_hidden_'
                                                                   value='<%=piPackageAuctioneerChildrenDTO.piPackageAuctioneerId%>'
                                                                   tag='pb_html'/>
                                                        </td>
                                                        <td>

                                                            <select class='form-control' onchange="vendorChanged(this)"
                                                                    name='piPackageAuctioneerChildren.name'
                                                                    id='name_text_' tag='pb_html'>
                                                            </select>
                                                        </td>
                                                        <td>

                                                            <input type='text'
                                                                   class='form-control'
                                                                   value='<%=piPackageAuctioneerChildrenDTO.address%>'
                                                                   name='address_text'
                                                                   id='address_geoTextField_'
                                                                   tag='pb_html' disabled="disabled">

                                                        </td>
                                                        <td>

                                                            <input type='text' class='form-control'
                                                                   name='piPackageAuctioneerChildren.mobile'
                                                                   id='mobile_text_'
                                                                   required="required"
                                                                   pattern="^(01[3-9]{1}[0-9]{8})"
                                                                   value='<%=piPackageAuctioneerChildrenDTO.mobile == null || piPackageAuctioneerChildrenDTO.mobile.length() == 0?"":
                                                                   (piPackageAuctioneerChildrenDTO.mobile.startsWith("88") ? piPackageAuctioneerChildrenDTO.mobile.substring(2) : piPackageAuctioneerChildrenDTO.mobile)%>'
                                                                   placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                                   title='<%=isLanguageEnglish?"personal mobile number must start with 01, then contain 9 digits"
                                                               :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                                                   tag='pb_html' disabled="disabled"/>

                                                        </td>
                                                        <td>
                                                            <input type='checkbox'
                                                                   class='form-control-sm'
                                                                   name='piPackageAuctioneerChildren.isChosen'
                                                                   id='isChosen_checkbox_'
                                                                   value='true' <%=(String.valueOf(piPackageAuctioneerChildrenDTO.isChosen).equals("true"))?("checked"):"" %>
                                                                   tag='pb_html' onclick="onlyOne(this)">
                                                        </td>
                                                        <td style="display: none;">
                                                            <input type='hidden'
                                                                   class='form-control isChosenVal_hidden_input'
                                                                   name='piPackageAuctioneerChildren.isChosenVal'
                                                                   id='isChosenVal_hidden_' value='-1' tag='pb_html'/>
                                                        </td>
                                                        <td>
															<span id='chkEdit'>
																<input type='checkbox' name='checkbox' value=''
                                                                       deletecb='true'
                                                                       class="form-control-sm"/>
															</span>
                                                        </td>
                                                    </tr>

                                                </template>
                                            </div>
                                        </div>
                                        <!--Child table end-->


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=pi_package_auctioneerDTO.iD%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='officeUnitId'
                                               id='officeUnitId_hidden_<%=i%>' value='<%=officeUnitId%>' tag='pb_html'/>


                                        <div class="mt-4">
                                            <div class="form-body">

                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped text-nowrap">
                                                        <thead>
                                                        <tr id="dynamicTrHeader">
                                                            <th><%=LM.getText(LC.PI_AUCTION_ADD_AUCTIONPACKAGENAME, loginDTO)%>
                                                            </th>
                                                            <%int headerIndex = 1;%>

                                                            <%
                                                                for (PiPackageAuctioneerChildrenDTO piPackageAuctioneerChildrenDTO1 : pi_package_auctioneerDTO.piPackageAuctioneerChildrenDTOList) {
                                                            %>
                                                            <th id="auctioneer_header_<%=headerIndex++%>"><%=piPackageAuctioneerChildrenDTO1.name%> <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_PIPACKAGEAUCTIONEERID, loginDTO)%>
                                                            </th>
                                                            <%}%>

                                                        </tr>
                                                        </thead>
                                                        <tbody id="NAuctioneerProducts">


                                                        <%
                                                            int pvpIndex = -1;
                                                            pvpIndex++;
                                                        %>

                                                        <tr class="productsAndAuctioneerPriceTr"
                                                            id="productsAndAuctioneerPriceTr_<%=pvpIndex%>">
                                                            <td class="productsAndVendorPriceTd">
                                                                <%=pi_auctionDTO.auctionPackageName%>
                                                                <input type='hidden' class='form-control'
                                                                       name='piAuctionId' id='piAuctionId_hidden_<%=i%>'
                                                                       value='<%=pi_auctionDTO.iD%>' tag='pb_html'/>
                                                            </td>

                                                            <%
                                                                int forDelIndex = 1;
                                                                for (PiPackageAuctioneerChildrenDTO piPackageChildDTO : pi_package_auctioneerDTO.piPackageAuctioneerChildrenDTOList) {
                                                                    Pi_package_auctioneer_itemsDTO pi_package_auctioneer_itemsDTO = Pi_package_auctioneer_itemsDAO.getInstance()
                                                                            .getPiPackageItemsDtoByChildrenId(pi_package_auctioneer_itemsDTOS, piPackageChildDTO.iD);
                                                                    if (pi_package_auctioneer_itemsDTO != null) {
                                                            %>

                                                            <td class="auctioneer_data_<%=(forDelIndex)%>"><input
                                                                    type="number" step="0.01"
                                                                    id="auctioneer_data_<%=(forDelIndex++)%>"
                                                                    name="submittedAuctioneerPrice" class="form-control"
                                                                    value="<%=pi_package_auctioneer_itemsDTO.price%>"
                                                                    tag="pb_html"/></td>

                                                            <%} else {%>
                                                            <td class="auctioneer_data_<%=(forDelIndex)%>"><input
                                                                    type="number" step="0.01"
                                                                    id="auctioneer_data_<%=(forDelIndex++)%>"
                                                                    name="submittedAuctioneerPrice" class="form-control"
                                                                    tag="pb_html"/></td>

                                                            <%}%>
                                                            <%}%>


                                                        </tr>
                                                        <%

                                                        %>

                                                        </tbody>
                                                    </table>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDERADVERTISECAT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">


                                                <select multiple="multiple" class='form-control'
                                                        name='tenderAdvertiseCat'
                                                        id='tenderAdvertiseCat' tag='pb_html'>

                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDERADVERTISEDATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-5">
                                                <%value = "tenderAdvertiseDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='tenderAdvertiseDate'
                                                       id='tenderAdvertiseDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_auctioneerDTO.tenderAdvertiseDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "tenderAdvertiseTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden'
                                                       value="<%=pi_package_auctioneerDTO.tenderAdvertiseTime%>"
                                                       name='tenderAdvertiseTime' id='tenderAdvertiseTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDEROPENINGDATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-5">
                                                <%value = "tenderOpeningDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='tenderOpeningDate'
                                                       id='tenderOpeningDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_auctioneerDTO.tenderOpeningDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "tenderOpeningTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden'
                                                       value="<%=pi_package_auctioneerDTO.tenderOpeningTime%>"
                                                       name='tenderOpeningTime' id='tenderOpeningTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_NOADATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-5">
                                                <%value = "noaDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='noaDate' id='noaDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_auctioneerDTO.noaDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "noaTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' value="<%=pi_package_auctioneerDTO.noaTime%>"
                                                       name='noaTime' id='noaTime_time_<%=i%>' tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_AGREEMENTDATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-5">
                                                <%value = "agreementDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='agreementDate' id='agreementDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_auctioneerDTO.agreementDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "agreementTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' value="<%=pi_package_auctioneerDTO.agreementTime%>"
                                                       name='agreementTime' id='agreementTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_AGREEMENTENDINGDATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-5">
                                                <%value = "agreementEndingDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='agreementEndingDate'
                                                       id='agreementEndingDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_auctioneerDTO.agreementEndingDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "agreementEndingTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden'
                                                       value="<%=pi_package_auctioneerDTO.agreementEndingTime%>"
                                                       name='agreementEndingTime' id='agreementEndingTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_FILESDROPZONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    fileColumnName = "filesDropzone";
                                                    if (actionName.equals("ajax_edit")) {
                                                        List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(pi_package_auctioneerDTO.filesDropzone);
                                                %>
                                                <%@include file="../pb/dropzoneEditor.jsp" %>
                                                <%
                                                    } else {
                                                        ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                        pi_package_auctioneerDTO.filesDropzone = ColumnID;
                                                    }
                                                %>

                                                <div class="dropzone"
                                                     action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=pi_package_auctioneerDTO.filesDropzone%>">
                                                    <input type='file' style="display:none"
                                                           name='<%=fileColumnName%>File'
                                                           id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                       id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                       tag='pb_html'/>
                                                <input type='hidden' name='<%=fileColumnName%>'
                                                       id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=pi_package_auctioneerDTO.filesDropzone%>'/>


                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                    type="button" onclick="submitForm()">
                                <%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const piPackageAuctioneerForm = $("#bigform");

    function processMultipleSelectBoxBeforeSubmit2(name) {

        $("[name='" + name + "']").each(function (i) {
            var selectedInputs = $(this).val();
            var temp = "";
            if (selectedInputs != null) {
                selectedInputs.forEach(function (value, index, array) {
                    if (index > 0) {
                        temp += ", ";
                    }
                    temp += value;
                });
            }
            if (temp.includes(',')) {
                $(this).append('<option value="' + temp + '"></option>');
            }
            $(this).val(temp);

        });
    }


    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting(0)) {
            processMultipleSelectBoxBeforeSubmit2("tenderAdvertiseCat");
            $.ajax({
                type: "POST",
                url: "<%=servletName%>?actionType=<%=actionName%>",
                data: piPackageAuctioneerForm.serialize(),
                dataType: 'JSON',
                success: function (response) {

                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }


    function onlyOne(checkbox) {
        let checkboxes = document.getElementsByName('piPackageAuctioneerChildren.isChosen');

        checkboxes.forEach((item, index) => {
            let hiddenCheckBoxInd = index + 1;
            let hiddenCheckBoxId = 'isChosenVal_hidden_' + hiddenCheckBoxInd;
            if (item !== checkbox) {
                item.checked = false;
                document.getElementById(hiddenCheckBoxId).value = "-1";
            } else {
                document.getElementById(hiddenCheckBoxId).value = "1";
            }
        });
    }

    var numbers = {
        0: "\u09E6",
        1: "\u09E7",
        2: "\u09E8",
        3: "\u09E9",
        4: "\u09EA",
        5: "\u09EB",
        6: "\u09EC",
        7: "\u09ED",
        8: "\u09EE",
        9: "\u09EF"
    };

    function convertENToBN(input, language) {

        if (language === "English") {
            return input;
        }
        let output = [];
        input = input.toString(10).split('').map(Number);
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    function AuctioneerNameChange(selectedInput) {
        let name = document.getElementById(selectedInput.id).value;
        let selectedIdIndex = selectedInput.id.split('_')[2];
        document.getElementById('auctioneer_header_' + selectedIdIndex).innerText = name + ' ' + '<%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%>';

    }

    var addAuctioneerPriceTd = (child_table_extra_id) => {

        let dynamicTrHeaderContent = '<th id="auctioneer_header_' + child_table_extra_id + '"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VENDORID, loginDTO)%> ' + convertENToBN(child_table_extra_id, '<%=Language%>') + ' <%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%></th>';
        $("#dynamicTrHeader").append(dynamicTrHeaderContent);

        let products = document.getElementsByClassName("productsAndAuctioneerPriceTr");

        let onlyProductsVal = Array.prototype.slice
            .call(products)
            .map((product) => (product.id));

        let content = '<td class="auctioneer_data_' + child_table_extra_id + '"><input type="number" step="0.01" id="auctioneer_data_' + child_table_extra_id + '" name="submittedAuctioneerPrice" class="form-control" tag="pb_html"/></td>';

        onlyProductsVal.forEach((item, index) => {
            $('#' + item).append(content);
        });
    }


    function PreprocessBeforeSubmiting(row, action) {

        preprocessTimeBeforeSubmitting('momentCodeTime', row);
        preprocessDateBeforeSubmitting('tenderAdvertiseDate', row);
        preprocessTimeBeforeSubmitting('tenderAdvertiseTime', row);
        preprocessDateBeforeSubmitting('tenderOpeningDate', row);
        preprocessTimeBeforeSubmitting('tenderOpeningTime', row);
        preprocessDateBeforeSubmitting('noaDate', row);
        preprocessTimeBeforeSubmitting('noaTime', row);
        preprocessDateBeforeSubmitting('agreementDate', row);
        preprocessTimeBeforeSubmitting('agreementTime', row);
        preprocessDateBeforeSubmitting('agreementEndingDate', row);
        preprocessTimeBeforeSubmitting('agreementEndingTime', row);


        for (i = 1; i < child_table_extra_id; i++) {

            if (document.getElementById("isChosen_checkbox_" + i)) {
                if (document.getElementById("isChosen_checkbox_" + i).getAttribute("processed") == null) {
                    preprocessCheckBoxBeforeSubmitting('isChosen', i);
                    document.getElementById("isChosen_checkbox_" + i).setAttribute("processed", "1");
                }
            }
        }
        let checkboxes = document.getElementsByName('piPackageAuctioneerChildren.isChosen');

        checkboxes.forEach((item, index) => {

            if (item.checked == true) {
                document.getElementsByClassName("isChosenVal_hidden_input")[index].value = "1";
            } else {
                document.getElementsByClassName("isChosenVal_hidden_input")[index].value = "-1";
            }
        });

        let restrictDuplicateVendorArr = [];
        let selectedVendors = document.querySelectorAll('select[name="piPackageAuctioneerChildren.name"]');
        selectedVendors.forEach(selectedVendor => {
            restrictDuplicateVendorArr = [...restrictDuplicateVendorArr, +selectedVendor.value];
        });
        if (restrictDuplicateVendorArr.length === 0) {
            showToastSticky("অনুগ্রহপূর্বক নিলামে অংশগ্রহণকারী যোগ করুন", "Please add auctioneer");
            return false;
        }
        let hasDuplicate = restrictDuplicateVendorArr.some((value, index) => {
            return restrictDuplicateVendorArr.indexOf(value) !== index;
        });
        if (hasDuplicate) {
            showToastSticky("ডুপ্লিকেট নিলামে অংশগ্রহণকারী", "Duplicate auctioneer");
            return false;
        }

        let winnerVendor = document.querySelectorAll("input[type=checkbox][name='piPackageAuctioneerChildren.isChosen']:checked");
        if (winnerVendor.length !== 1) {
            showToastSticky("অনুগ্রহপূর্বক একজন বিজয়ী নির্বাচন করুন", "Please select only one winner");
            return false;
        }

        piPackageAuctioneerForm.validate();
        return piPackageAuctioneerForm.valid();
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_package_auctioneerServlet");
    }

    function init(row) {

        // setTimeById('momentCodeTime_js_' + row, $('#momentCodeTime_time_' + row).val(), true);
        setDateByStringAndId('tenderAdvertiseDate_js_' + row, $('#tenderAdvertiseDate_date_' + row).val());
        setTimeById('tenderAdvertiseTime_js_' + row, $('#tenderAdvertiseTime_time_' + row).val(), true);
        setDateByStringAndId('tenderOpeningDate_js_' + row, $('#tenderOpeningDate_date_' + row).val());
        setTimeById('tenderOpeningTime_js_' + row, $('#tenderOpeningTime_time_' + row).val(), true);
        setDateByStringAndId('noaDate_js_' + row, $('#noaDate_date_' + row).val());
        setTimeById('noaTime_js_' + row, $('#noaTime_time_' + row).val(), true);
        setDateByStringAndId('agreementDate_js_' + row, $('#agreementDate_date_' + row).val());
        setTimeById('agreementTime_js_' + row, $('#agreementTime_time_' + row).val(), true);
        setDateByStringAndId('agreementEndingDate_js_' + row, $('#agreementEndingDate_date_' + row).val());
        setTimeById('agreementEndingTime_js_' + row, $('#agreementEndingTime_time_' + row).val(), true);


    }

    var row = 0;
    let selectedTenderAdvertiseCat;
    let selectedTenderAdvertiseCattArr;
    var language = '<%=Language%>';
    $(document).ready(function () {

        let prevAddedVendors = document.querySelectorAll(('select[name="piPackageAuctioneerChildren.name"]'));
        prevAddedVendors.forEach((prevAddedVendor) => {

            let index = prevAddedVendor.id.split("_")[2];
            let selectVal = prevAddedVendor.getAttribute("data-selectVal");
            select2SingleSelector("#" + prevAddedVendor.id, '<%=Language%>');
            fetchVendors(selectVal, index);
        });

        select2MultiSelector("#tenderAdvertiseCat", '<%=Language%>');
        <% if(actionName.equalsIgnoreCase("ajax_edit")){ %>

        selectedTenderAdvertiseCat = '<%=pi_package_auctioneerDTO.tenderAdvertiseCat%>';
        selectedTenderAdvertiseCattArr = selectedTenderAdvertiseCat.split(',').map(function (item) {
            return item.trim();
        });
        <% }%>
        fetchAdvertiseMedium(selectedTenderAdvertiseCattArr);
        init(row);

        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function fetchAdvertiseMedium(selectedTenderAdvertiseCattArr) {

        let url = "Pi_package_auctioneerServlet?actionType=getAllAdvertiseMedium";
        //console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#tenderAdvertiseCat').html("");
                let o;
                let str;
                $('#tenderAdvertiseCat').append(o);
                const response = JSON.parse(fetchedData);
                if (response && response.length > 0) {
                    for (let x in response) {
                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (selectedTenderAdvertiseCattArr && selectedTenderAdvertiseCattArr.length > 0) {
                            if (selectedTenderAdvertiseCattArr.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#tenderAdvertiseCat').append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-PiPackageAuctioneerChildren").click(
        function (e) {
            e.preventDefault();


            var t = $("#template-PiPackageAuctioneerChildren");

            $("#field-PiPackageAuctioneerChildren").append(t.html());
            SetCheckBoxValues("field-PiPackageAuctioneerChildren");

            var tr = $("#field-PiPackageAuctioneerChildren").find("tr:last-child");

            tr.attr("id", "PiPackageAuctioneerChildren_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });

            initializeSingleSelector(child_table_extra_id);
            addAuctioneerPriceTd(child_table_extra_id);

            child_table_extra_id++;

        });


    $("#remove-PiPackageAuctioneerChildren").click(function (e) {
        var tablename = 'field-PiPackageAuctioneerChildren';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                    let deleteHeaderAndData = tr.querySelector('select[name="piPackageAuctioneerChildren.name"]').id.split("_")[2];
                    $("#auctioneer_header_" + deleteHeaderAndData).remove();
                    $(".auctioneer_data_" + deleteHeaderAndData).remove();
                }
                j++;
            }

        }
    });

    const vendorChanged = async (selectedVal) => {
        const vendorAuctioneerId = selectedVal.value;

        let index = selectedVal.id.split("_")[2];
        let address_id = 'address_geoTextField_' + index;
        let mobile_id = 'mobile_text_' + index;

        document.getElementById(address_id).value = '';
        document.getElementById(mobile_id).value = '';
        document.getElementById('auctioneer_header_' + index).innerText = '<%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VENDORID, loginDTO)%> ' + convertENToBN(index, '<%=Language%>') + ' <%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%>';

        if (vendorAuctioneerId === '') return;

        document.getElementById('auctioneer_header_' + index).innerText = selectedVal.options[selectedVal.selectedIndex].text + ' ' + '<%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%>';

        const url = 'Pi_vendor_auctioneer_detailsServlet?actionType=getAddressAndMobile&vendorAuctioneerId='
            + vendorAuctioneerId;

        const response = await fetch(url);
        const vendorDetailsJson = await response.json();

        document.getElementById(address_id).value = vendorDetailsJson.address;
        document.getElementById(mobile_id).value = vendorDetailsJson.mobile;
    }

    let prevSelectedItem;
    let initializeSingleSelector = (child_table_extra_id) => {
        select2SingleSelector("#name_text_" + child_table_extra_id, '<%=Language%>');
        fetchVendors(prevSelectedItem, child_table_extra_id);
    }

    function fetchVendors(prevSelectedItem, child_table_extra_id) {

        let url = "Pi_vendor_auctioneer_detailsServlet?actionType=getVendorOrAuctioneer&venAucType=2";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#name_text_' + child_table_extra_id).html("");
                let o;
                let str;
                if (language === 'English') {
                    str = 'Select';
                    o = new Option('Select', '-1');
                } else {
                    o = new Option('বাছাই করুন', '-1');
                    str = 'বাছাই করুন';
                }
                $(o).html(str);
                $('#name_text_' + child_table_extra_id).append(o);

                const response = JSON.parse(fetchedData);

                if (response && response.length > 0) {
                    for (let x in response) {

                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (prevSelectedItem && prevSelectedItem.length > 0) {
                            if (prevSelectedItem.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#name_text_' + child_table_extra_id).append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }


</script>






