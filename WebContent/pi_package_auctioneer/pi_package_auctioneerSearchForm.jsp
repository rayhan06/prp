<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_package_auctioneer.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="pi_auction.Pi_auctionRepository" %>


<%
    String navigator2 = "navPI_PACKAGE_AUCTIONEER";
    String servletName = "Pi_package_auctioneerServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PIAUCTIONID, loginDTO)%>
            </th>

            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_PI_PACKAGE_AUCTIONEER_CHILDREN_PIPACKAGEAUCTIONEERID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_ADDRESS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_MOBILE, loginDTO)%>
            </th>

            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_TENDERADVERTISECAT, loginDTO)%>
            </th>

            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_AGREEMENTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_ADD_AGREEMENTENDINGDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_AUCTIONEER_SEARCH_PI_PACKAGE_AUCTIONEER_EDIT_BUTTON, loginDTO)%>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Pi_package_auctioneerDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Pi_package_auctioneerDTO pi_package_auctioneerDTO = (Pi_package_auctioneerDTO) data.get(i);


        %>
        <tr>


            <td>
                <%
                    value = Pi_auctionRepository.getInstance().getPi_auctionDTOByiD(pi_package_auctioneerDTO.piAuctionId).auctionPackageName + "";
                %>

                <%=value%>


            </td>

            <td>
                <%=pi_package_auctioneerDTO.winnerAuctioneerName%>
            </td>
            <td>
                <%=pi_package_auctioneerDTO.winnerAuctioneerAddress%>
            </td>
            <td>
                <%=Utils.getDigits(pi_package_auctioneerDTO.winnerAuctioneerMobile, Language)%>
            </td>


            <td>

                <%
                    value = "";
                    value = pi_package_auctioneerDTO.tenderAdvertiseCat;
                    StringTokenizer tokenizer = new StringTokenizer(value, ",");
                    String separator = "";
                    value = "";
                    while (tokenizer.hasMoreTokens()) {
                        String val = tokenizer.nextToken().trim();
                        if (!val.equalsIgnoreCase("")) {
                            Long distNum = Long.valueOf(val);
                            String advertiseMedium = CatRepository.getInstance().getText(Language, "tender_advertise", distNum);
                            value = value + separator + advertiseMedium;
                            separator = ", ";
                        }
                    }
                %>

                <%=value%>


            </td>


            <td>
                <%
                    value = pi_package_auctioneerDTO.agreementDate + "";
                %>
                <%
                    String formatted_agreementDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_agreementDate, Language) + " " + Utils.getDigits(pi_package_auctioneerDTO.agreementTime, Language)%>


            </td>


            <td>
                <%
                    value = pi_package_auctioneerDTO.agreementEndingDate + "";
                %>
                <%
                    String formatted_agreementEndingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_agreementEndingDate, Language) + " " + Utils.getDigits(pi_package_auctioneerDTO.agreementEndingTime, Language)%>


            </td>

            <%CommonDTO commonDTO = pi_package_auctioneerDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

        </tr>
        <%
                    }
                } else {
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			