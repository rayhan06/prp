<%@ page import="static permission.MenuConstants.INVENTORY_AND_PURCHASE_MANAGEMENT" %>
<%@ page import="static permission.MenuConstants.PI_AUCTION" %>
<%@ page import="static permission.MenuConstants.*" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%
	request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(INVENTORY_AND_PURCHASE_MANAGEMENT,
			PI_AUCTION_MANAGEMENT,
			PI_AUCTION)));

%>
<jsp:include page="../common/layout.jsp" flush="true">

<jsp:param name="title" value="Edit User" /> 
	<jsp:param name="body" value="../pi_package_auctioneer/pi_package_auctioneerEditBody.jsp" />
</jsp:include> 