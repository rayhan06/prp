<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.LOGIN_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2 mx-md-0">
    <div class="col-12">
        <div id="employeeRecordId_div" class="search-criteria-div col-md-6 employeeModalClass">
            <div class="form-group row">
                <label class="col-md-3 col-form-label text-md-right">
                    <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)%>
                </label>
                <div class="col-md-9">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                            id="employee_record_id_modal_button"
                            onclick="employeeRecordIdModalBtnClicked();">
                        <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                    </button>
                    <div class="input-group" id="employee_record_id_div" style="display: none">
                        <input type="hidden" name='employeeRecordId' id='employee_record_id_input' value="">
                        <button type="button" class="btn btn-secondary form-control" disabled
                                id="employee_record_id_text"></button>
                        <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger modalCross"
                                    onclick="crsBtnClicked('employee_record_id');"
                                    id='employee_record_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="search-criteria-div col-md-6 ">
        <div class="form-group row">
            <label class="col-md-3 control-label text-md-right">
                <%=LM.getText(LC.HM_START_DATE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="startDate-js"/>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                </jsp:include>
                <input type="hidden" name='startDate' id='startDate' value=""/>
            </div>
        </div>
    </div>

    <div class="search-criteria-div col-md-6 ">
        <div class="form-group row">
            <label class="col-md-3 control-label text-md-right">
                <%=LM.getText(LC.HM_END_DATE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="endDate-js"/>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                </jsp:include>
                <input type="hidden" name='endDate' id='endDate' value=""/>
            </div>
        </div>
    </div>

</div>
<%
    String modalTitle = Language.equalsIgnoreCase("English") ? "Find Designation" : "পদবী খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>
<script type="text/javascript">
    function init() {
        showFooter = false;
        dateTimeInit($("#Language").val());
    }

    function PreprocessBeforeSubmiting() {
        $('#startDate').val(getDateTimestampById('startDate-js'));
        $('#endDate').val(getDateTimestampById('endDate-js'));
    }

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.employeeNameEn + ', ' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + ', ' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
        $('#employee_record_id_input').val(empInfo.employeeRecordId);
    }

    modal_button_dest_table = 'none';

    table_name_to_collcetion_map = new Map([

        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: viewEmployeeRecordIdInInput
        }]
    ]);

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

</script>