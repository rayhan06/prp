<!DOCTYPE html>
<%@page import="user.*" %>
<%@page import="org.apache.commons.lang3.ArrayUtils" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="permission.MenuDTO" %>
<%@ page import="permission.MenuRepository" %>
<%@page contentType="text/html;charset=utf-8" %>
<%
    String context = "../../.." + request.getContextPath() + "/";
    String pluginsContext = context + "assets/global/plugins/";
    request.setAttribute("context", context);
    request.setAttribute("pluginsContext", pluginsContext);


    List<Integer> menuIDPath = (List<Integer>) request.getAttribute("menuIDPath");
    if (menuIDPath == null) {
        menuIDPath = new ArrayList<Integer>();
    }

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    UserDTO userDTO = null;

    if (loginDTO.isOisf == 1) {
        userDTO = UserRepository.getUserDTOByOrganogramID(loginDTO.userID);
    } else {
        userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    }


    String pageTitle = "";

    if (menuIDPath.isEmpty()) {
        pageTitle = request.getRequestURL().toString();
        System.out.println("menuIDPath is empty, pagetitle = " + pageTitle);

        if (userDTO != null) {
            pageTitle = (LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? "Dashboard" : "ড্যাশবোর্ড");
        } else {
            pageTitle = "Dashboard";
        }
    } else {

        int currentMenuID = menuIDPath.get(menuIDPath.size() - 1);
        MenuDTO currentMenu = MenuRepository.getInstance().getMenuDTOByMenuID(currentMenuID);


        System.out.println("layout.jsp, menu = " + currentMenu.menuName);

        if (userDTO != null) {
            pageTitle = (LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? currentMenu.menuName : currentMenu.menuNameBangla);
        } else {
            pageTitle = "Dashboard";
        }

    }

    int my_language = LM.getLanguageIDByUserDTO(userDTO) == CommonConstant.Language_ID_English ? 2 : 1;

%>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1">
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            font: 16px Arial;
        }

        /*the container must be positioned relative:*/
        .autocomplete {
            position: relative;
            display: inline-block;
        }

        input {
            border: 1px solid transparent;
            background-color: #f1f1f1;
            padding: 10px;
            font-size: 16px;
        }

        input[type=submit] {
            background-color: DodgerBlue;
            color: #fff;
            cursor: pointer;
        }

        .autocomplete-items {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            /*position the autocomplete items to be the same width as the container:*/
            top: 100%;
            left: 0;
            right: 0;
        }

        .autocomplete-items div {
            padding: 10px;
            cursor: pointer;
            background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }

        /*when hovering an item:*/
        .autocomplete-items div:hover {
            background-color: #e9e9e9;
        }

        /*when navigating through the items using the arrow keys:*/
        .autocomplete-active {
            background-color: DodgerBlue !important;
            color: #ffffff;
        }
    </style>
    <title><%=pageTitle %>
    </title>
    <%@ include file="../skeleton/head.jsp" %>
    <%
        String[] cssStr = request.getParameterValues("css");
        for (int i = 0; ArrayUtils.isNotEmpty(cssStr) && i < cssStr.length; i++) {
    %>
    <link href="${context}<%=cssStr[i]%>" rel="stylesheet" type="text/css"/>
    <%
        }
    %>
    <script type="text/javascript">
        var context = '${context}';
        var pluginsContext = '${pluginsContext}';
    </script>

    <style>
        .tab-group {

            width: 100%;
        }

        .tab-group .btn.active {
            background-color: #595959;
            color: lime !important;
            position: relative;
        }

        .tab-group .col-md-2 {

            padding-left: 5px !important;
            padding-right: 5px !important;
        }

        .tab-group .btn {

            border: none;
            padding: 1px 0px;
            width: 100%;
            color: #8e5c00 !important;
        }

        .tab-group .btn.active:hover {
            color: white !important;
        }

        .tab-group .btn:hover {
            color: #000000 !important;
        }

        .tab-group button {

            padding: 15px !important;
            font-size: 14px;
        }

        .card-header h5 {

            margin: 0px;
            font-size: 14px;
        }

        .arrow-down {
            width: 0;
            height: 0;
            border-left: 10px solid transparent;
            border-right: 10px solid transparent;
            border-top: 10px solid #595959;
            margin: auto;
        }

    </style>

</head>
<%
    String fullMenu = "'";

    for (int i = 0; i < menuIDPath.size(); i++) {

        int menuID = menuIDPath.get(i);
        if (i != 0) {
            fullMenu += "','";
        }
        fullMenu += menuID;
    }

    fullMenu += "'";
    int j;
    System.out.println("fullMenu=" + fullMenu);
%>
<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
<input type="hidden" id="my_language" value="<%=my_language%>"/>
<div id="fakeLoader"></div>
<div class="page-header navbar navbar-fixed-top">
    <%@ include file="../skeleton/header.jsp" %>
</div>
<div class="clearfix"></div>
<div class="page-container">
    <%@ include file="../skeleton/menu.jsp" %>
    <div class="page-content-wrapper">
        <div class="page-content">
            <%-- <jsp:include page='flushActionStatus.jsp' />--%>
            <%--	<jsp:include page="error_msg.jsp" />--%>
            <%--	<%@ include file="../common/error_page.jsp"%>--%>
            <%
                System.out.println("#############");
                System.out.println(request.getParameter("body"));
                System.out.println("#############");
            %>
            <jsp:include page='<%=request.getParameter("body")%>'/>
        </div>
    </div>
</div>
<%@ include file="../skeleton/footer.jsp" %>
<%@ include file="../skeleton/includes.jsp" %>
<%
    String[] helpers = request.getParameterValues("helpers");
    for (int i = 0; ArrayUtils.isNotEmpty(helpers) && i < helpers.length; i++) {
%>
<jsp:include page="<%=helpers[i] %>" flush="true">
    <jsp:param name="helper" value="<%=i %>"/>
</jsp:include>
<% } %>
<%
    String[] jsStr = request.getParameterValues("js");
    for (int i = 0; ArrayUtils.isNotEmpty(jsStr) && i < jsStr.length; i++) {
%>
<script src="${context}<%=jsStr[i]%>" type="text/javascript"></script>
<%
    }
%>
</body>
</html>


<div id="toast_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;"><i class="fa fa-times" onclick="onClickHideToast()"></i></div>
    <div id="toast_message_div"></div>
</div>
<div id="error_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;"><i class="fa fa-times" onclick="onClickHideToast()"></i></div>
    <div id="error_message_div"></div>
</div>

<script>
    var success_message_bng = 'সম্পন্ন হয়েছে';
    var success_message_eng = 'Success';

    var failed_message_bng = 'ব্যর্থ হয়েছে';
    var failed_message_eng = 'Failed';

    var fill_all_input_bng = "অনুগ্রহ করে সব তথ্য যথাযথ ভাবে দিন";
    var fill_all_input_eng = "Please fill all input correctly";

    var no_data_found_eng = "No data found";
    var no_data_found_bng = "কোন তথ্য পাওয়া যায় নি";

    var inplaceSubmitButton = '<%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>';

    function showToast(bn, en) {
        const x = document.getElementById("toast_message_div");
        x.innerHTML = '';
        const my_language = document.getElementById('my_language').value;

        x.innerHTML = parseInt(my_language) === 1 ? bn : en;
        document.getElementById('toast_message').classList.add("show");

        setTimeout(function () {
            document.getElementById('toast_message').classList.remove("show");
        }, 3000);
    }

    function showError(bn, en) {
        document.getElementById("error_message_div").innerHTML = ('');
        const my_language = document.getElementById('my_language').value;

        document.getElementById("error_message_div").innerHTML = parseInt(my_language) === 1 ? bn : en;
        document.getElementById('error_message').classList.add("show");

        setTimeout(function () {
            document.getElementById('error_message').classList.remove("show");
        }, 3000);
    }

    function getLanguageBaseText(en, bn) {
        const my_language = document.getElementById('my_language').value;
        return parseInt(my_language) === 1 ? (bn == undefined || bn == null ? "পাওয়া যায় নি" : bn) : (en == undefined || en == null ? "Not found" : en);
    }

    function onClickHideToast() {
        document.getElementById('toast_message').classList.remove("show");
        document.getElementById('error_message').classList.remove("show");
    }
</script>

<script>
    var global_my_language = '1';

    function isNullOrNegativeValue(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === undefined || arr[i] === null || arr[i] == '' || arr[i].length == 0 || arr[i] == '-1') return true;
        }
        return false;
    }

    function optional_select(op) {
        const my_language = document.getElementById('my_language').value;
        op = new Option(my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --", '-1');
        select_2_call();
    }

    $(document).ready(function () {
        global_my_language = document.getElementById('my_language').value;
        /*$("select").select2({
            dropdownAutoWidth: true,
            placeholder: my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --"
        });*/

        activateMenu();
    });

    function getDataTable() {
        $('#tableData').DataTable({
            dom: 'B',
            "ordering": false,
            buttons: [
                {
                    extend: 'excel',
                    charset: 'UTF-8'
                }, {
                    extend: 'print',
                    charset: 'UTF-8'
                }
            ]
        });
        document.getElementById('tableData_wrapper').classList.add("pull-right");
        document.getElementById('tableData').classList.remove("no-footer");
    }

    var fullMenu = [<%=fullMenu%>];

</script>

<script src="<%=context%>ek_sheba_common/ek_sheba_commom_js.js" type="text/javascript"></script>
<script src="<%=context%>ek_sheba_common/ek_sheba_geo.js" type="text/javascript"></script>


<style>
    .select2-container--bootstrap .select2-selection--single .select2-selection__rendered {
        padding-top: 7px;
    }

    .custom-toast {
        visibility: hidden;
        width: 300px;
        min-height: 60px;
        color: #fff;
        text-align: center;
        padding: 20px 30px;
        position: fixed;
        z-index: 1;
        right: 23px;
        bottom: 60px;
        font-size: 13pt;
    }

    #toast_message {
        background-color: #008aa6;
    }

    #error_message {
        background-color: #ca5e59;
    }

    #toast_message.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    #error_message.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    @-webkit-keyframes fadein {
        from {
            right: -38%;
            opacity: 0;
        }
        to {
            right: 23px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            right: -38%;
            opacity: 0;
        }
        to {
            right: 23px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            right: 23px;
            opacity: 1;
        }
        to {
            right: -38%;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            right: 23px;
            opacity: 1;
        }
        to {
            right: -38%;
            opacity: 0;
        }
    }

    .modal {
    / / position: absolute;
        float: left;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        width: 100%;
        height: 100%;
    }

</style>

