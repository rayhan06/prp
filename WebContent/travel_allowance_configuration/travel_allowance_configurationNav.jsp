<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Travel_allowance_configurationServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
    </div>
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_ALLOWANCECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='allowance_cat' id='allowance_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("travel_allowance", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_ALLOWANCEATTENDANCETYPE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='allowance_attendance_type' id='allowance_attendance_type'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("allowance_attendance", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>

<script type="text/javascript">
    const allowance_catInput = $('#allowance_cat');
    const allowance_attendance_typeInput = $('#allowance_attendance_type');

    $(document).ready(() => {
        select2SingleSelector('#allowance_cat', '<%=Language%>');
        select2SingleSelector('#allowance_attendance_type', '<%=Language%>');
    });

    function resetInputs() {
        allowance_catInput.select2("val", '-1');
        allowance_attendance_typeInput.select2("val", '-1');
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state;
            dosubmit(params, false);
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {
                        case 'allowance_cat':
                            allowance_catInput.val(item[1]).trigger('change');
                            break;
                        case 'allowance_attendance_type':
                            allowance_attendance_typeInput.val(item[1]).trigger('change');
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (pushState) {
                    history.pushState(params, '', 'Travel_allowance_configurationServlet?actionType=search&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText ;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "Travel_allowance_configurationServlet?actionType=search&ajax=true&isPermanentTable=<%=isPermanentTable%>&" + params, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        let params = 'search=true';

        if ($("#allowance_cat").val() != -1 && $("#allowance_cat").val() != "") {
            params += '&allowance_cat=' + document.getElementById('allowance_cat').value;
        }
        if ($("#allowance_attendance_type").val() != -1 && $("#allowance_attendance_type").val() != "") {
            params += '&allowance_attendance_type=' + document.getElementById('allowance_attendance_type').value;
        }


        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params, true);

    }

</script>

