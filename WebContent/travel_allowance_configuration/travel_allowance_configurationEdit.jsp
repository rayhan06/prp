<%@ page import="util.HttpRequestUtils" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String formTitle = LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_TRAVEL_ALLOWANCE_CONFIGURATION_ADD_FORMNAME, loginDTO);
%>

<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="<%=formTitle%>"/>
    <jsp:param name="body" value="../travel_allowance_configuration/travel_allowance_configurationEditBody.jsp"/>
</jsp:include> 