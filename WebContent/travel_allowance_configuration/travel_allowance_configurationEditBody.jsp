<%@page import="login.LoginDTO" %>
<%@page import="travel_allowance_configuration.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%
    Travel_allowance_configurationDTO travel_allowance_configurationDTO = new Travel_allowance_configurationDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        travel_allowance_configurationDTO = Travel_allowance_configurationDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    String context = request.getContextPath() + "/";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_TRAVEL_ALLOWANCE_CONFIGURATION_ADD_FORMNAME, loginDTO);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Travel_allowance_configurationServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=travel_allowance_configurationDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_ALLOWANCECAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='allowanceCat' onchange='onChanged()'
                                                    id='allowance_cat' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("travel_allowance", Language, travel_allowance_configurationDTO.allowanceCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_ALLOWANCEATTENDANCETYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='allowanceAttendanceType'
                                                    onchange='onChanged()'
                                                    id='allowance_attendance_type' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("allowance_attendance", Language, travel_allowance_configurationDTO.allowanceAttendanceType);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_AMOUNT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                value = "";
                                                if (travel_allowance_configurationDTO.amount != -1) {
                                                    value = travel_allowance_configurationDTO.amount + "";
                                                }
                                            %>
                                            <input type='text' class='form-control' name='amount'
                                                   data-only-number="true"
                                                   id='amount_number' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_TRAVEL_ALLOWANCE_CONFIGURATION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_TRAVEL_ALLOWANCE_CONFIGURATION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Travel_allowance_configurationServlet");
    }

    function init() {
        document.querySelectorAll('[data-only-number="true"]')
            .forEach(inputField => inputField.onkeydown = keyDownEvent);

    }

    async function onChanged() {
        let allowance_cat = $("#allowance_cat").val();
        let allowance_attendance_type = $("#allowance_attendance_type").val();
        if (allowance_cat !== -1 && allowance_cat !== "" && allowance_attendance_type !== -1 && allowance_attendance_type !== "") {
            const url = 'Travel_allowance_configurationServlet?actionType=getAmount&allowance_cat=' + allowance_cat
                + '&allowance_attendance_type=' + allowance_attendance_type;

            const response = await fetch(url);
            const {value} = await response.json();
            console.log(value);
            document.getElementById('amount_number').value = value;
        }

    }

    function keyDownEvent(e) {
        $(e.target).data('previousValue', $(e.target).val());
        let isvalid = inputValidationForIntValue(e, $(this), null);
        return true === isvalid;
    }

    var row = 0;
    $(document).ready(function () {
        init();
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

</script>






