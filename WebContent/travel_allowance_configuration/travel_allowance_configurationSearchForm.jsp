<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="travel_allowance_configuration.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    String navigator2 = "navTRAVEL_ALLOWANCE_CONFIGURATION";
    String servletName = "Travel_allowance_configurationServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_ALLOWANCECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_ALLOWANCEATTENDANCETYPE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_AMOUNT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_SEARCH_TRAVEL_ALLOWANCE_CONFIGURATION_EDIT_BUTTON, loginDTO)%>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Travel_allowance_configurationDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Travel_allowance_configurationDTO travel_allowance_configurationDTO = (Travel_allowance_configurationDTO) data.get(i);


        %>
        <tr>


            <td>
                <%
                    value = travel_allowance_configurationDTO.allowanceCat + "";
                %>
                <%
                    value = CatRepository.getInstance().getText(Language, "travel_allowance", travel_allowance_configurationDTO.allowanceCat);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = travel_allowance_configurationDTO.allowanceAttendanceType + "";
                %>
                <%
                    value = CatRepository.getInstance().getText(Language, "allowance_attendance", travel_allowance_configurationDTO.allowanceAttendanceType);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = travel_allowance_configurationDTO.amount + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <%CommonDTO commonDTO = travel_allowance_configurationDTO; %>
            <td>
                <%
                    if (commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {
                %>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
                <%
                    }
                %>
            </td>


        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			