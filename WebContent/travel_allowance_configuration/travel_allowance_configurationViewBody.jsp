

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="travel_allowance_configuration.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Travel_allowance_configurationServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Travel_allowance_configurationDTO travel_allowance_configurationDTO = Travel_allowance_configurationDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = travel_allowance_configurationDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_TRAVEL_ALLOWANCE_CONFIGURATION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_TRAVEL_ALLOWANCE_CONFIGURATION_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_ALLOWANCECAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = travel_allowance_configurationDTO.allowanceCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "allowance", travel_allowance_configurationDTO.allowanceCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_ALLOWANCEATTENDANCETYPE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = travel_allowance_configurationDTO.allowanceAttendanceType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "allowance_attendance", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_AMOUNT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = travel_allowance_configurationDTO.amount + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_INSERTEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = travel_allowance_configurationDTO.insertedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_INSERTIONTIME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = travel_allowance_configurationDTO.insertionTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_CONFIGURATION_ADD_MODIFIEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = travel_allowance_configurationDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>