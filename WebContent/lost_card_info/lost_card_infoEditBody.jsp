<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="lost_card_info.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    Lost_card_infoDTO lost_card_infoDTO = (Lost_card_infoDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    String formTitle = LM.getText(LC.LOST_CARD_INFO_ADD_LOST_CARD_INFO_ADD_FORMNAME, loginDTO);
    String servletName = "Lost_card_infoServlet";
    String fileColumnName;
    FilesDAO filesDAO = new FilesDAO();
    int i = 0;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Lost_card_infoServlet?actionType=edit&isPermanentTable=true"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=lost_card_infoDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='cardInfoId'
                                           id='cardInfoId_hidden_<%=i%>' value='<%=lost_card_infoDTO.cardInfoId%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.LOST_CARD_INFO_ADD_GDCOPYFILEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "gdCopyFileDropzone";
                                                if (lost_card_infoDTO.gdCopyFileDropzone > 0) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(lost_card_infoDTO.gdCopyFileDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                }
                                                if (lost_card_infoDTO.gdCopyFileDropzone == 0) {
                                                    lost_card_infoDTO.gdCopyFileDropzone = DBMW.getInstance().getNextSequenceId("fileid");
                                                }
                                            %>
                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=edit&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=lost_card_infoDTO.gdCopyFileDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=lost_card_infoDTO.gdCopyFileDropzone%>'/>


                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.LOST_CARD_INFO_ADD_MONEYRECEIPTFILEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "moneyReceiptFileDropzone";
                                                if (lost_card_infoDTO.moneyReceiptFileDropzone > 0) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(lost_card_infoDTO.moneyReceiptFileDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                }
                                                if (lost_card_infoDTO.moneyReceiptFileDropzone == 0) {
                                                    lost_card_infoDTO.moneyReceiptFileDropzone = DBMW.getInstance().getNextSequenceId("fileid");
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=edit&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=lost_card_infoDTO.moneyReceiptFileDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=lost_card_infoDTO.moneyReceiptFileDropzone%>'/>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.LOST_CARD_INFO_ADD_LOST_CARD_INFO_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.LOST_CARD_INFO_ADD_LOST_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });
</script>






