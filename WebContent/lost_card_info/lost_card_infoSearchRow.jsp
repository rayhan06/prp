<%@page pageEncoding="UTF-8" %>

<%@page import="lost_card_info.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="card_info.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.LOST_CARD_INFO_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_LOST_CARD_INFO;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Lost_card_infoDTO lost_card_infoDTO = (Lost_card_infoDTO) request.getAttribute("lost_card_infoDTO");
    CommonDTO commonDTO = lost_card_infoDTO;
    String servletName = "Lost_card_infoServlet";


    System.out.println("lost_card_infoDTO = " + lost_card_infoDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));

    String value = "";


    Lost_card_infoDAO lost_card_infoDAO = (Lost_card_infoDAO) request.getAttribute("lost_card_infoDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    Card_infoDTO cardInfoDTO = CardInfoDAO.getInstance().getDTOFromID(lost_card_infoDTO.cardInfoId);
    CardEmployeeInfoDTO employeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeInfoId);
    CardEmployeeOfficeInfoDTO officeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(cardInfoDTO.cardEmployeeOfficeInfoId);

    CardEmployeeImagesDTO cardEmployeeImagesDTO = CardEmployeeImagesDAO.getInstance().getDTOFromID(cardInfoDTO.cardEmployeeImagesId);
    byte[] encodeBase64Photo = Base64.encodeBase64(cardEmployeeImagesDTO.photo);
%>

<td style='display:none;' class="text-nowrap">
    <input type='hidden' id='failureMessage_<%=i%>' value=''>
</td>

<td id='<%=i%>_cardInfoId' class="text-nowrap">
    <%=lost_card_infoDTO.cardInfoId%>
</td>

<td id='<%=i%>_cardCat' class="text-nowrap">
    <span class="btn btn-sm border-0 shadow" style="background-color: <%=CardCategoryEnum.getColor(cardInfoDTO.cardCat)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language, "card", cardInfoDTO.cardCat)%>
    </span>
</td>

<td id='<%=i%>_cardStatusCat' class="text-nowrap">
    <span class="btn btn-sm border-0 shadow" style="background-color:<%=CardStatusEnum.getColor(cardInfoDTO.cardStatusCat)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language,"card_status", cardInfoDTO.cardStatusCat)%>
    </span>
</td>

<td id='<%=i%>_cardEmployeeInfoId' class="text-nowrap">
    <img width="75px" height="75px" src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>'>
    <br>
    <b><%=isLanguageEnglish?employeeInfoDTO.nameEn :employeeInfoDTO.nameBn%></b>
    <br>
    <%=isLanguageEnglish?officeInfoDTO.organogramEng + "<br>" + officeInfoDTO.officeUnitEng
                        : officeInfoDTO.organogramBng+ "<br>" + officeInfoDTO.officeUnitBng%>
</td>


<%--<td>
    <button type="button" class="btn btn-sm border-0 shadow"
            style="background-color: #22ccc1; color: white; border-radius: 8px"
            onclick="location.href='Lost_card_infoServlet?actionType=view&ID=<%=lost_card_infoDTO.iD%>'">
        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
    </button>
</td>--%>

<td id='<%=i%>_Edit'>
    <button type="button" class="btn btn-sm border-0 btn-success shadow btn-border-radius text-nowrap"
            style="color: white; border-radius: 8px"
            onclick="location.href='Lost_card_infoServlet?actionType=getEditPage&ID=<%=lost_card_infoDTO.iD%>'">
        <%=LM.getText(LC.LOST_CARD_INFO_ADD_REISSUE_CARD, loginDTO)%>
    </button>
</td>


<%--<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=lost_card_infoDTO.iD%>'/>
        </span>
    </div>
</td>--%>
																						
											

