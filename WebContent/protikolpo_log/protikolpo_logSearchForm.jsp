<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="protikolpo_log.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.PROTIKOLPO_LOG_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    Protikolpo_logDAO protikolpo_logDAO = (Protikolpo_logDAO) request.getAttribute("protikolpo_logDAO");
    String navigator2 = SessionConstants.NAV_PROTIKOLPO_LOG;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;
    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="thead-light text-center">
        <tr>
            <th><%=LM.getText(LC.PROTIKOLPO_LOG_EDIT_EMPLOYEEOFFICEIDFROMNAME, loginDTO)%></th>
            <th><%=LM.getText(LC.PROTIKOLPO_LOG_EDIT_EMPLOYEEOFFICEIDTONAME, loginDTO)%></th>
            <th><%=LM.getText(LC.PROTIKOLPO_LOG_EDIT_PROTIKOLPOSTARTDATE, loginDTO)%></th>
            <th><%=LM.getText(LC.PROTIKOLPO_LOG_EDIT_PROTIKOLPOENDDATE, loginDTO)%></th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_PROTIKOLPO_LOG);
            try {
                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Protikolpo_logDTO row = (Protikolpo_logDTO) data.get(i);
                        TempTableDTO tempTableDTO = null;
                        if (!isPermanentTable) {
                            tempTableDTO = protikolpo_logDAO.getTempTableDTOFromTableById(tableName, row.iD);
                        }
                        out.println("<tr id = 'tr_" + i + "'>");
        %>
        <%
            request.setAttribute("protikolpo_logDTO", row);
        %>
        <jsp:include page="./protikolpo_logSearchRow.jsp">
            <jsp:param name="pageName" value="searchrow"/>
            <jsp:param name="rownum" value="<%=i%>"/>
        </jsp:include>
        <%
                        out.println("</tr>");
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    function getOriginal(i, tempID, parentID, ServletName) {
        console.log("getOriginal called");
        var idToSubmit;
        var isPermanentTable;
        var state = document.getElementById(i + "_original_status").value;
        if (state == 0) {
            idToSubmit = parentID;
            isPermanentTable = true;
        } else {
            idToSubmit = tempID;
            isPermanentTable = false;
        }
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var response = JSON.parse(this.responseText);
                document.getElementById(i + "_protikolpoStartDate").innerHTML = response.protikolpoStartDate;
                document.getElementById(i + "_protikolpoEndDate").innerHTML = response.protikolpoEndDate;
                document.getElementById(i + "_protikolpoEndedBy").innerHTML = response.protikolpoEndedBy;
                document.getElementById(i + "_employeeOfficeIdFromName").innerHTML = responseemployeeOfficeIdFromName;
                document.getElementById(i + "_employeeOfficeIdToName").innerHTML = response.employeeOfficeIdToName;
                if (state == 0) {
                    document.getElementById(i + "_getOriginal").innerHTML = "View Edited";
                    state = 1;
                } else {
                    document.getElementById(i + "_getOriginal").innerHTML = "View Original";
                    state = 0;
                }
                document.getElementById(i + "_original_status").value = state;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", ServletName + "?actionType=getDTO&ID=" + idToSubmit + "&isPermanentTable=" + isPermanentTable, true);
        xhttp.send();
    }
    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;
            if (!document.getElementById('protikolpoStartDate_date_' + row).checkValidity()) {
                empty_fields += "'protikolpoStartDate'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('protikolpoEndDate_date_' + row).checkValidity()) {
                empty_fields += "'protikolpoEndDate'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('protikolpoEndedBy_text_' + row).checkValidity()) {
                empty_fields += "'protikolpoEndedBy'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('employeeOfficeIdFromName_text_' + row).checkValidity()) {
                empty_fields += "'employeeOfficeIdFromName'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('employeeOfficeIdToName_text_' + row).checkValidity()) {
                empty_fields += "'employeeOfficeIdToName'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }
        }
        console.log("found date = " + document.getElementById('protikolpoStartDate_date_Date_' + row).value);
        document.getElementById('protikolpoStartDate_date_' + row).value = new Date(document.getElementById('protikolpoStartDate_date_Date_' + row).value).getTime();
        console.log("found date = " + document.getElementById('protikolpoEndDate_date_Date_' + row).value);
        document.getElementById('protikolpoEndDate_date_' + row).value = new Date(document.getElementById('protikolpoEndDate_date_Date_' + row).value).getTime();
        return true;
    }
    function PostprocessAfterSubmiting(row) {
    }
    function init(row) {
    }
    function submitAjax(i, deletedStyle) {
        console.log('submitAjax called');
        var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
        if (isSubmittable == false) {
            return;
        }
        var formData = new FormData();
        var value;
        value = document.getElementById('id_hidden_' + i).value;
        console.log('submitAjax i = ' + i + ' id = ' + value);
        formData.append('id', value);
        formData.append("identity", value);
        formData.append("ID", value);
        formData.append('protikolpoId', document.getElementById('protikolpoId_hidden_' + i).value);
        formData.append('protikolpoStartDate', document.getElementById('protikolpoStartDate_date_' + i).value);
        formData.append('protikolpoEndDate', document.getElementById('protikolpoEndDate_date_' + i).value);
        formData.append('protikolpoEndedBy', document.getElementById('protikolpoEndedBy_text_' + i).value);
        formData.append('employeeOfficeIdFromName', document.getElementById('employeeOfficeIdFromName_text_' + i).value);
        formData.append('employeeOfficeIdToName', document.getElementById('employeeOfficeIdToName_text_' + i).value);
        formData.append('protikolpoStatus', document.getElementById('protikolpoStatus_hidden_' + i).value);
        formData.append('created', document.getElementById('created_hidden_' + i).value);
        formData.append('modified', document.getElementById('modified_hidden_' + i).value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('tr_' + i).innerHTML = this.responseText;
                    ShowExcelParsingResult(i);
                } else {
                    console.log("No Response");
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", 'Protikolpo_logServlet?actionType=edit&inplacesubmit=true&isPermanentTable=<%=isPermanentTable%>&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
        xhttp.send(formData);
    }
    window.onload = function () {
        ShowExcelParsingResult('general');
    }
</script>