
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="protikolpo_log.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
Protikolpo_logDTO protikolpo_logDTO;
protikolpo_logDTO = (Protikolpo_logDTO)request.getAttribute("protikolpo_logDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(protikolpo_logDTO == null)
{
	protikolpo_logDTO = new Protikolpo_logDTO();
	
}
System.out.println("protikolpo_logDTO = " + protikolpo_logDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.PROTIKOLPO_LOG_EDIT_PROTIKOLPO_LOG_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.PROTIKOLPO_LOG_ADD_PROTIKOLPO_LOG_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
Protikolpo_logDTO row = protikolpo_logDTO;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Protikolpo_logServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.PROTIKOLPO_LOG_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=ID%>'/>
	
												

		<input type='hidden' class='form-control'  name='protikolpoId' id = 'protikolpoId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.protikolpoId + "'"):("'" + "0" + "'")%>/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PROTIKOLPO_LOG_EDIT_PROTIKOLPOSTARTDATE, loginDTO)):(LM.getText(LC.PROTIKOLPO_LOG_ADD_PROTIKOLPOSTARTDATE, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'protikolpoStartDate_div_<%=i%>'>	
		<input type='date' class='form-control'  name='protikolpoStartDate_Date_<%=i%>' id = 'protikolpoStartDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_protikolpoStartDate = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_protikolpoStartDate = format_protikolpoStartDate.format(new Date(protikolpo_logDTO.protikolpoStartDate));
	out.println("'" + formatted_protikolpoStartDate + "'");
}
else
{
	out.println("''");
}
%>
  >
		<input type='hidden' class='form-control'  name='protikolpoStartDate' id = 'protikolpoStartDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.protikolpoStartDate + "'"):("''")%> required="required"
>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PROTIKOLPO_LOG_EDIT_PROTIKOLPOENDDATE, loginDTO)):(LM.getText(LC.PROTIKOLPO_LOG_ADD_PROTIKOLPOENDDATE, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'protikolpoEndDate_div_<%=i%>'>	
		<input type='date' class='form-control'  name='protikolpoEndDate_Date_<%=i%>' id = 'protikolpoEndDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_protikolpoEndDate = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_protikolpoEndDate = format_protikolpoEndDate.format(new Date(protikolpo_logDTO.protikolpoEndDate));
	out.println("'" + formatted_protikolpoEndDate + "'");
}
else
{
	out.println("''");
}
%>
  >
		<input type='hidden' class='form-control'  name='protikolpoEndDate' id = 'protikolpoEndDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.protikolpoEndDate + "'"):("''")%> required="required"
>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PROTIKOLPO_LOG_EDIT_PROTIKOLPOENDEDBY, loginDTO)):(LM.getText(LC.PROTIKOLPO_LOG_ADD_PROTIKOLPOENDEDBY, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'protikolpoEndedBy_div_<%=i%>'>	
		<input type='text' class='form-control'  name='protikolpoEndedBy' id = 'protikolpoEndedBy_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.protikolpoEndedBy + "'"):("''")%> required="required"
  />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PROTIKOLPO_LOG_EDIT_EMPLOYEEOFFICEIDFROMNAME, loginDTO)):(LM.getText(LC.PROTIKOLPO_LOG_ADD_EMPLOYEEOFFICEIDFROMNAME, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'employeeOfficeIdFromName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='employeeOfficeIdFromName' id = 'employeeOfficeIdFromName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.employeeOfficeIdFromName + "'"):("''")%> required="required"
  />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.PROTIKOLPO_LOG_EDIT_EMPLOYEEOFFICEIDTONAME, loginDTO)):(LM.getText(LC.PROTIKOLPO_LOG_ADD_EMPLOYEEOFFICEIDTONAME, loginDTO))%>
	<span class="required"> * </span>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'employeeOfficeIdToName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='employeeOfficeIdToName' id = 'employeeOfficeIdToName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.employeeOfficeIdToName + "'"):("''")%> required="required"
  />					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='protikolpoStatus' id = 'protikolpoStatus_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.protikolpoStatus + "'"):("'" + "false" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='created' id = 'created_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.created + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='modified' id = 'modified_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.modified + "'"):("'" + "0" + "'")%>/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.PROTIKOLPO_LOG_EDIT_PROTIKOLPO_LOG_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.PROTIKOLPO_LOG_ADD_PROTIKOLPO_LOG_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.PROTIKOLPO_LOG_EDIT_PROTIKOLPO_LOG_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.PROTIKOLPO_LOG_ADD_PROTIKOLPO_LOG_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;
		if(!document.getElementById('protikolpoStartDate_date_' + row).checkValidity())
		{
			empty_fields += "'protikolpoStartDate'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('protikolpoEndDate_date_' + row).checkValidity())
		{
			empty_fields += "'protikolpoEndDate'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('protikolpoEndedBy_text_' + row).checkValidity())
		{
			empty_fields += "'protikolpoEndedBy'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('employeeOfficeIdFromName_text_' + row).checkValidity())
		{
			empty_fields += "'employeeOfficeIdFromName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}
		if(!document.getElementById('employeeOfficeIdToName_text_' + row).checkValidity())
		{
			empty_fields += "'employeeOfficeIdToName'";
			if(i > 0)
			{
				empty_fields += ", ";
			}
			i ++;
		}


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	console.log("found date = " + document.getElementById('protikolpoStartDate_date_Date_' + row).value);
	document.getElementById('protikolpoStartDate_date_' + row).value = new Date(document.getElementById('protikolpoStartDate_date_Date_' + row).value).getTime();
	console.log("found date = " + document.getElementById('protikolpoEndDate_date_Date_' + row).value);
	document.getElementById('protikolpoEndDate_date_' + row).value = new Date(document.getElementById('protikolpoEndDate_date_Date_' + row).value).getTime();
	return true;
}

function PostprocessAfterSubmiting(row)
{
}


function init(row)
{
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}





</script>






