<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="protikolpo_log.Protikolpo_logDTO" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%
    Protikolpo_logDTO protikolpo_logDTO = (Protikolpo_logDTO) request.getAttribute("protikolpo_logDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (protikolpo_logDTO == null) {
        protikolpo_logDTO = new Protikolpo_logDTO();
    }
    System.out.println("protikolpo_logDTO = " + protikolpo_logDTO);
    String actionName = "edit";
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = Integer.parseInt(request.getParameter("rownum"));
    String deletedStyle = request.getParameter("deletedstyle");
    String value = "";
    Protikolpo_logDTO row = protikolpo_logDTO;
%>


<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    String Language = LM.getText(LC.PROTIKOLPO_LOG_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    String datestr = dateFormat.format(date);
%>


<%=("<td id = '" + i + "_id" + "' style='display:none;'>")%>


<input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>' value='<%=ID%>'/>


<%=("</td>")%>

<%=("<td id = '" + i + "_protikolpoId" + "' style='display:none;'>")%>


<input type='hidden' class='form-control' name='protikolpoId' id='protikolpoId_hidden_<%=i%>'
       value='<%=protikolpo_logDTO.protikolpoId%>'/>


<%=("</td>")%>

<%=("<td id = '" + i + "_protikolpoStartDate'>")%>


<div class="form-inline" id='protikolpoStartDate_div_<%=i%>'>
    <input type='date' class='form-control' name='protikolpoStartDate_Date_<%=i%>'
           id='protikolpoStartDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_protikolpoStartDate = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_protikolpoStartDate = format_protikolpoStartDate.format(new Date(protikolpo_logDTO.protikolpoStartDate));
	out.println("'" + formatted_protikolpoStartDate + "'");
}
else
{
	out.println("''");
}
%>
    >
    <input type='hidden' class='form-control' name='protikolpoStartDate' id='protikolpoStartDate_date_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.protikolpoStartDate + "'"):("''")%> required="required"
    >

</div>

<%=("</td>")%>

<%=("<td id = '" + i + "_protikolpoEndDate'>")%>


<div class="form-inline" id='protikolpoEndDate_div_<%=i%>'>
    <input type='date' class='form-control' name='protikolpoEndDate_Date_<%=i%>' id='protikolpoEndDate_date_Date_<%=i%>'
           value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_protikolpoEndDate = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_protikolpoEndDate = format_protikolpoEndDate.format(new Date(protikolpo_logDTO.protikolpoEndDate));
	out.println("'" + formatted_protikolpoEndDate + "'");
}
else
{
	out.println("''");
}
%>
    >
    <input type='hidden' class='form-control' name='protikolpoEndDate' id='protikolpoEndDate_date_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.protikolpoEndDate + "'"):("''")%> required="required"
    >

</div>

<%=("</td>")%>

<%=("<td id = '" + i + "_protikolpoEndedBy'>")%>


<div class="form-inline" id='protikolpoEndedBy_div_<%=i%>'>
    <input type='text' class='form-control' name='protikolpoEndedBy' id='protikolpoEndedBy_text_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.protikolpoEndedBy + "'"):("''")%> required="required"
    />
</div>

<%=("</td>")%>

<%=("<td id = '" + i + "_employeeOfficeIdFromName'>")%>


<div class="form-inline" id='employeeOfficeIdFromName_div_<%=i%>'>
    <input type='text' class='form-control' name='employeeOfficeIdFromName' id='employeeOfficeIdFromName_text_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.employeeOfficeIdFromName + "'"):("''")%> required="required"
    />
</div>

<%=("</td>")%>

<%=("<td id = '" + i + "_employeeOfficeIdToName'>")%>


<div class="form-inline" id='employeeOfficeIdToName_div_<%=i%>'>
    <input type='text' class='form-control' name='employeeOfficeIdToName' id='employeeOfficeIdToName_text_<%=i%>'
           value=<%=actionName.equals("edit")?("'" + protikolpo_logDTO.employeeOfficeIdToName + "'"):("''")%> required="required"
    />
</div>

<%=("</td>")%>

<%=("<td id = '" + i + "_protikolpoStatus" + "' style='display:none;'>")%>


<input type='hidden' class='form-control' name='protikolpoStatus' id='protikolpoStatus_hidden_<%=i%>'
       value='<%=protikolpo_logDTO.protikolpoStatus%>'/>


<%=("</td>")%>

<%=("<td id = '" + i + "_created" + "' style='display:none;'>")%>


<input type='hidden' class='form-control' name='created' id='created_hidden_<%=i%>'
       value='<%=protikolpo_logDTO.created%>'/>


<%=("</td>")%>

<%=("<td id = '" + i + "_modified" + "' style='display:none;'>")%>


<input type='hidden' class='form-control' name='modified' id='modified_hidden_<%=i%>'
       value='<%=protikolpo_logDTO.modified%>'/>


<%=("</td>")%>
					
		