<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="economic_code.Economic_codeRepository" %>

<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    if (rn == null) {
        rn = (RecordNavigator) request.getAttribute("recordNavigator");
    }
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.ECONOMIC_SUB_CODE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>

<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label"><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMICCODEID, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <select class='form-control' style="width: 100%" name='economic_code_id'
                                    id="economicCodeId">
                                <%=Economic_codeRepository.getInstance().buildEconomicCodeOption(Language, 0L)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label"><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_CODE, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <input type="text" class="form-control" id="code" placeholder="" name="code"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label"><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_DESCRIPTIONEN, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <input type="text" class="form-control" id="description_en" placeholder=""
                                   name="description_en" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label"><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_DESCRIPTIONBN, loginDTO)%>
                        </label>
                        <div class="col-md-8 col-xl-9">
                            <input type="text" class="form-control" id="description_bn" placeholder=""
                                   name="description_bn" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ml-auto">
            <input type="hidden" name="search" value="yes"/>
            <button
                    type="button"
                    class="btn shadow btn-border-radius"
                    onclick="allfield_changed('',0)"
                    style="background-color: #00a1d4; color: white;"
            >
                <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
            </button>
        </div>
    </div>
</div>


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    $(() => {
        select2SingleSelector('#economicCodeId', '<%=Language%>');
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        let params = '';
        params += '&economic_code_id=' + $('#economicCodeId').val();
        params += '&code=' + $('#code').val();
        params += '&description_en=' + $('#description_en').val();
        params += '&description_bn=' + $('#description_bn').val();

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

