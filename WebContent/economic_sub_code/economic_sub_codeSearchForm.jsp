<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="economic_sub_code.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.List" %>
<%@ page import="economic_code.Economic_codeRepository" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.ECONOMIC_SUB_CODE_EDIT_LANGUAGE, loginDTO);
    String navigator2 = SessionConstants.NAV_ECONOMIC_SUB_CODE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMICCODEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_CODE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_DESCRIPTIONEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_DESCRIPTIONBN, loginDTO)%>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
            List<Economic_sub_codeDTO> data = (List<Economic_sub_codeDTO>) recordNavigator.list;
            if (data != null && data.size() > 0) {
                for (Economic_sub_codeDTO economic_sub_codeDTO : data) {
        %>
        <tr>
            <td id=economicCodeId'>
                <%=Economic_codeRepository.getInstance().getById(economic_sub_codeDTO.economicCodeId).getFormattedCodeAndName(Language)%>
            </td>

            <td id=code'>
                <%=Utils.getDigits(economic_sub_codeDTO.code + "", Language)%>
            </td>

            <td id=descriptionEn'>
                <%=economic_sub_codeDTO.descriptionEn%>
            </td>

            <td id=descriptionBn'>
                <%=economic_sub_codeDTO.descriptionBn%>
            </td>
        </tr>
        <% }
        } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			