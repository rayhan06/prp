<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="economic_sub_code.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    String servletName = "Economic_sub_codeServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.ECONOMIC_SUB_CODE_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Economic_sub_codeDAO economic_sub_codeDAO = Economic_sub_codeDAO.getInstance();
    Economic_sub_codeDTO economic_sub_codeDTO = economic_sub_codeDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title"><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMIC_SUB_CODE_ADD_FORMNAME, loginDTO)%>
                    </h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <h3><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMIC_SUB_CODE_ADD_FORMNAME, loginDTO)%>
                </h3>
                <table class="table table-bordered table-striped">


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMICCODEID, loginDTO)%>
                        </b></td>
                        <td>
                            <%=economic_sub_codeDTO.economicCodeId%>
                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_CODE, loginDTO)%>
                        </b></td>
                        <td>
                            <%=Utils.getDigits(economic_sub_codeDTO.code, Language)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_DESCRIPTIONEN, loginDTO)%>
                        </b></td>
                        <td>
                            <%=economic_sub_codeDTO.descriptionEn%>
                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_DESCRIPTIONBN, loginDTO)%>
                        </b></td>
                        <td>
                            <%=economic_sub_codeDTO.descriptionBn%>
                        </td>

                    </tr>


                </table>
            </div>


        </div>


    </div>
</div>