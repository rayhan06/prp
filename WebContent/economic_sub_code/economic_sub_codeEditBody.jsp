<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="economic_sub_code.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="economic_group.EconomicGroupRepository" %>

<%
    Economic_sub_codeDTO economic_sub_codeDTO;
    economic_sub_codeDTO = (Economic_sub_codeDTO) request.getAttribute("economic_sub_codeDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (economic_sub_codeDTO == null) {
        economic_sub_codeDTO = new Economic_sub_codeDTO();
    }
    String formTitle = LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMIC_SUB_CODE_ADD_FORMNAME, loginDTO);
    String Language = LM.getText(LC.ECONOMIC_SUB_CODE_EDIT_LANGUAGE, loginDTO);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform" action="Economic_sub_codeServlet?actionType=add">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4  col-form-label text-md-right">
                                            <label>
                                                <%=LM.getText(LC.BUDGET_ECONOMIC_GROUP, loginDTO)%><span
                                                    style="color: red">*</span>
                                            </label>
                                        </label>
                                        <div class="col-md-8 ">
                                            <select class='form-control' name="economicGroup" id='economic-group-select'
                                                    onchange="economicGroupChanged(this);" required>
                                                <%=EconomicGroupRepository.getInstance().buildOptionWithCode(Language, null)%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMICCODEID, loginDTO)%><span
                                                style="color: red">*</span>
                                        </label>
                                        <div class="col-md-8 ">
                                            <select class='form-control' name='economicCodeId' id="economicCodeId"
                                                    required>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_CODE, loginDTO)%><span
                                                style="color: red">*</span>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control' name='code' id='code_text'
                                                   value='<%=economic_sub_codeDTO.code%>' required tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_DESCRIPTIONEN, loginDTO)%><span
                                                style="color: red">*</span>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control' name='descriptionEn'
                                                   id='descriptionEn_text' required
                                                   value='<%=economic_sub_codeDTO.descriptionEn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4  col-form-label text-md-right">
                                            <%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_DESCRIPTIONBN, loginDTO)%><span
                                                style="color: red">*</span>
                                        </label>
                                        <div class="col-md-8 ">
                                            <input type='text' class='form-control' name='descriptionBn'
                                                   id='descriptionBn_text' required
                                                   value='<%=economic_sub_codeDTO.descriptionBn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMIC_SUB_CODE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit"
                                    id="submit-btn" onclick="submitForm()">
                                <%=LM.getText(LC.ECONOMIC_SUB_CODE_ADD_ECONOMIC_SUB_CODE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const economicSubCodeForm = $('#bigform');
    $(document).ready(() => {
        $.validator.addMethod('economicCodeIdSelection', function (value) {
            return value != '';
        });
        $.validator.addMethod('economicGroupSelection', function (value) {
            return value != '';
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                descriptionBn: "required",
                descriptionEn: "required",
                code: "required",
                economicCodeId: {
                    required: true,
                    economicCodeIdSelection: true
                },
                economicGroup: {
                    required: true,
                    economicGroupSelection: true
                }
            },
            messages: {
                descriptionBn: isLangEng ? "Please write bangla description" : "অনুগ্রহ করে বাংলায় বিবরণ লিখুন",
                descriptionEn: isLangEng ? "Please write english description" : "অনুগ্রহ করে ইংরেজীতে বিবরণ লিখুন",
                code: isLangEng ? "Please write code" : "অনুগ্রহ করে কোড লিখুন",
                economicCodeId: isLangEng ? "Please select economic code" : "অর্থনৈতিক কোড বাছাই করুন",
                economicGroup: isLangEng ? "Please select economic group" : "অর্থনৈতিক গ্রুপ বাছাই করুন",
            }
        });
    });

    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroup = selectElement.value;
        document.getElementById('economicCodeId').innerHTML = '';
        if (selectedEconomicGroup === '') return;
        const url = 'Budget_mappingServlet?actionType=buildEconomicCode&economic_group_type=' + selectedEconomicGroup;
        const data = await fetch(url);
        document.getElementById('economicCodeId').innerHTML = await data.text();
    }

    function submitForm() {
        if ($("#bigform").valid()) {
            submitAjaxForm();
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>
