<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.UNIVERSITY_INFO_EDIT_LANGUAGE, loginDTO);
%>

<%--
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title"> Asset Management </h3>
    </div>
</div>

<!-- end:: Subheader -->--%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet kt-portlet">

        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    Tree Demo
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">

            <div class="row">

                <div class="col-lg-3"></div>

                <div class="col-lg-6">

                    <jsp:include page="/tree/tree.jsp">
                        <jsp:param name="TREE_ID" value="jsTree"></jsp:param>
                        <jsp:param name="AJAX_SEARCH" value="true"></jsp:param>
                        <jsp:param name="SHOW_CHECKBOX_AT_LEAF_ONLY" value="true"></jsp:param>
                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                    </jsp:include>

                </div>

            </div>

        </div>

    </div>
</div>