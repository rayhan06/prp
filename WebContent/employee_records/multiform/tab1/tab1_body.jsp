<%@page import="employee_records.Employee_recordsServlet"%>
<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserDTO" %>

<%
    String context = request.getContextPath();
    Employee_recordsDTO employee_recordsDTO;
    employee_recordsDTO = (Employee_recordsDTO) request.getAttribute("employee_recordsDTO");
    if (employee_recordsDTO == null) {
        employee_recordsDTO = new Employee_recordsDTO();
    }
    int i = 0;
%>

<style>
    #aspect-content {
        margin: 50px 0 0;
        font-family: Roboto, SolaimanLipi;
    }

    #aspect-content * {
        box-sizing: border-box;
    }

    .aspect-tab {
        position: relative;
        width: 100%;
        margin: 0 auto 10px;
        border-radius: 8px;
        background-color: #fff;
        box-shadow: 0 0 0 1px #ececec;
        opacity: 1;
        transition: box-shadow .2s, opacity .4s;
    }

    .aspect-tab:hover {
        box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.11);
    }

    .aspect-input {
        display: none;
    }

    .aspect-input:checked ~ .aspect-content + .aspect-tab-content {
        max-height: 5000px;
    }

    .aspect-input:checked ~ .aspect-content:after {
        transform: rotate(0);
    }

    .aspect-label {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        max-height: 80px;
        width: 100%;
        margin: 0;
        padding: 0;
        font-size: 0;
        z-index: 1;
        cursor: pointer;
    }

    .aspect-label:hover ~ .aspect-content:after {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTI0IDI0SDBWMGgyNHoiIG9wYWNpdHk9Ii44NyIvPgogICAgICAgIDxwYXRoIGZpbGw9IiM1NTZBRUEiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE1Ljg4IDE1LjI5TDEyIDExLjQxbC0zLjg4IDMuODhhLjk5Ni45OTYgMCAxIDEtMS40MS0xLjQxbDQuNTktNC41OWEuOTk2Ljk5NiAwIDAgMSAxLjQxIDBsNC41OSA0LjU5Yy4zOS4zOS4zOSAxLjAyIDAgMS40MS0uMzkuMzgtMS4wMy4zOS0xLjQyIDB6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=");
    }

    .aspect-content {
        position: relative;
        display: block;
        height: 80px;
        margin: 0;
        padding: 0 87px 0 30px;
        font-size: 0;
        white-space: nowrap;
        cursor: pointer;
        user-select: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
    }

    .aspect-content:before, .aspect-content:after {
        content: '';
        display: inline-block;
        vertical-align: middle;
    }

    .aspect-content:before {
        height: 100%;
    }

    .aspect-content:after {
        position: absolute;
        width: 24px;
        height: 100%;
        right: 30px;
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTI0IDI0SDBWMGgyNHoiIG9wYWNpdHk9Ii44NyIvPgogICAgICAgIDxwYXRoIGZpbGw9IiNBOUFDQUYiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE1Ljg4IDE1LjI5TDEyIDExLjQxbC0zLjg4IDMuODhhLjk5Ni45OTYgMCAxIDEtMS40MS0xLjQxbDQuNTktNC41OWEuOTk2Ljk5NiAwIDAgMSAxLjQxIDBsNC41OSA0LjU5Yy4zOS4zOS4zOSAxLjAyIDAgMS40MS0uMzkuMzgtMS4wMy4zOS0xLjQyIDB6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=");
        background-repeat: no-repeat;
        background-position: center;
        transform: rotate(180deg);
    }

    .aspect-name {
        display: inline-block;
        width: 75%;
        margin-left: 10px;
        font-weight: 400;
        white-space: normal;
        text-align: left;
        vertical-align: middle;
    }

    .aspect-stat {
        width: 40%;
        text-align: right;
    }

    .all-opinions,
    .aspect-name {
        font-size: 1.5rem;
        line-height: 22px;
        font-family: Roboto, SolaimanLipi;
    }

    .all-opinions {
        color: #5d5d5d;
        text-align: left;
    }

    .aspect-content + .aspect-tab-content {
        max-height: 0;
        overflow: hidden;
        transition: max-height 1s ease-in-out;
    }

    .aspect-content > div,
    .aspect-stat > div {
        display: inline-block;
    }

    .aspect-content > div {
        vertical-align: middle;
    }

    .positive-count,
    .negative-count,
    .neutral-count {
        display: inline-block;
        margin: 0 0 0 20px;
        padding-left: 26px;
        background-repeat: no-repeat;
        font-size: 13px;
        line-height: 20px;
        color: #363636;
    }

    .positive-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiM3RUQzMjEiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNSAxMS40MjdhNSA1IDAgMCAwIDEwIDAgLjcxNC43MTQgMCAxIDAtMS40MjkgMCAzLjU3MSAzLjU3MSAwIDAgMS03LjE0MiAwIC43MTQuNzE0IDAgMSAwLTEuNDI5IDB6bTEuMDcxLTVhMS4wNzEgMS4wNzEgMCAxIDAgMCAyLjE0MyAxLjA3MSAxLjA3MSAwIDAgMCAwLTIuMTQzem03Ljg1OCAwYTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDMgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3oiLz4KPC9zdmc+Cg==");
    }

    .negative-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiNGRjZFMDAiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNSAxNC45OThhLjcxNC43MTQgMCAwIDAgMS40MjkgMCAzLjU3MSAzLjU3MSAwIDAgMSA3LjE0MiAwIC43MTQuNzE0IDAgMSAwIDEuNDI5IDAgNSA1IDAgMSAwLTEwIDB6bTEuMDcxLTguNTdhMS4wNzEgMS4wNzEgMCAxIDAgMCAyLjE0MiAxLjA3MSAxLjA3MSAwIDAgMCAwLTIuMTQzem03Ljg1OCAwYTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDIgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3oiLz4KPC9zdmc+Cg==");
    }

    .neutral-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiNCQUMyRDYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNS43MTQgMTEuNDI3YS43MTQuNzE0IDAgMSAwIDAgMS40MjloOC41NzJhLjcxNC43MTQgMCAxIDAgMC0xLjQyOUg1LjcxNHptLjM1Ny01YTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDMgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3ptNy44NTggMGExLjA3MSAxLjA3MSAwIDEgMCAwIDIuMTQzIDEuMDcxIDEuMDcxIDAgMCAwIDAtMi4xNDN6Ii8+Cjwvc3ZnPgo=");
    }

    .aspect-info {
        width: 60%;
        white-space: nowrap;
        font-size: 0;
    }

    .aspect-info:before {
        content: '';
        display: inline-block;
        height: 44px;
        vertical-align: middle;
    }

    .chart-pie {
        position: relative;
        display: inline-block;
        height: 44px;
        width: 44px;
        border-radius: 50%;
        background-color: #e4e4e4;
        vertical-align: middle;
    }

    .chart-pie:after {
        content: '';
        display: block;
        position: absolute;
        height: 40px;
        width: 40px;
        top: 2px;
        left: 2px;
        border-radius: 50%;
        background-color: #fff;
    }

    .chart-pie-count {
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        font-size: 14px;
        font-weight: 500;
        line-height: 44px;
        color: #242a32;
        text-align: center;
        z-index: 1;
    }

    .chart-pie > div {
        clip: rect(0, 44px, 44px, 22px);
    }

    .chart-pie > div,
    .chart-pie.over50 .first-fill {
        position: absolute;
        height: 44px;
        width: 44px;
        border-radius: 50%;
    }

    .chart-pie.over50 > div {
        clip: rect(auto, auto, auto, auto);
    }

    .chart-pie.over50 .first-fill {
        clip: rect(0, 44px, 44px, 22px);
    }

    .chart-pie:not(.over50) .first-fill {
        display: none;
    }

    .second-fill {
        position: absolute;
        clip: rect(0, 22px, 44px, 0);
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border-width: 3px;
        border-style: solid;
        box-sizing: border-box;
    }

    .chart-pie.positive .first-fill {
        background-color: #82d428;
    }

    .chart-pie.positive .second-fill {
        border-color: #82d428;
    }

    .chart-pie.negative .first-fill {
        background-color: #ff6e00;
    }

    .chart-pie.negative .second-fill {
        border-color: #ff6e00;
    }

    .aspect-tab-content {
        background-color: #f9f9f9;
        /*font-size: 0;*/
        text-align: justify;
    }

    .sentiment-wrapper {
        /*padding: 24px 30px 30px;*/
    }

    .sentiment-wrapper > div {
        display: inline-block;
        width: 100%;
    }

    .sentiment-wrapper > div > div {
        width: 100%;
        padding: 0px 16px;
    }

    .opinion-header {
        position: relative;
        width: 100%;
        margin: 0 0 24px;
        font-size: 13px;
        font-weight: 500;
        line-height: 20px;
        color: #242a32;
        text-transform: capitalize;
    }

    .opinion-header > span:nth-child(2) {
        position: absolute;
        right: 0;
    }

    .opinion-header + div > span {
        font-size: 13px;
        font-weight: 400;
        line-height: 22px;
        color: #363636;
    }

    @media screen and (max-width: 800px) {
        .aspect-label {
            max-height: 102px;
        }

        .aspect-content {
            height: auto;
            padding: 10px 87px 10px 30px;
        }

        .aspect-content:before {
            display: none;
        }

        .aspect-content:after {
            top: 0;
        }

        .aspect-content > div {
            display: block;
            width: 100%;
        }

        .aspect-stat {
            margin-top: 10px;
            text-align: left;
        }
    }

    @media screen and (max-width: 750px) {
        .sentiment-wrapper > div {
            display: block;
            width: 100%;
            max-width: 100%;
        }

        .sentiment-wrapper > div:not(:first-child) {
            margin-top: 10px;
        }
    }

    @media screen and (max-width: 500px) {
        .aspect-label {
            max-height: 140px;
        }

        .aspect-stat > div {
            display: block;
            width: 100%;
        }

        .all-opinions {
            margin-bottom: 10px;
        }

        .all-opinions + div > span:first-child {
            margin: 0;
        }
    }

    .form-control-plaintext {
        font-size: 1.2rem !important;
        font-family: Roboto, SolaimanLipi !important;
    }

    th, td {
        text-align: left;
    }
</style>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String actionName = "";
    String actionType = request.getParameter("actionType");
    long empId;
    if (actionType.equals("viewMyProfile")) {
        UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
        empId = userDTO.employee_record_id;
    } else {
        empId = Long.parseLong(request.getParameter("ID"));
    }

    switch (actionType) {
        case "addMultiForm":
            actionName = "add";
            break;
        case "editMultiForm":
            actionName = "edit";
            break;
        case "viewMyProfile":
        case "viewMultiForm":
            actionName = "view";
            break;
    }
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title" style="color: #4a95dc;">
                    <i class="fa fa-user-cog"></i>
                    <%
                        switch (actionName) {
                            case "view":
                    %>
                    <%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERSONAL_AND_FAMILY_INFORMATION, loginDTO) %>
                    <%
                            break;
                        case "add":
                    %>
                    <%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_CREATE_NEW_EMPLOYEE, loginDTO) %>
                    <%
                            break;
                        case "edit":
                    %>
                    <%= LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERSONAL_INFO, loginDTO) %>
                    <%
                                break;
                        }
                    %>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="">
                <%if (actionName.equals("view")) {%>
                <div class="" style="margin-bottom: 15px;">
                    <jsp:include page="/employee_records/multiform/tabs.jsp"></jsp:include>
                </div>
                <br/><br/>
                <%}%>
                <%if (actionName.equals("view")) {%>
                <div class="onlyborder">
                    <div class="row">
                        <div class="col-12">
                            <div class="sub_title_top">
                                <div class="sub_title">
                                    <h4 style="background: white">
                                        <i class="fa fa-user-circle"></i>
                                        <%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTO) %>
                                    </h4>
                                </div>
                            </div>

                            <%--new accordian start--%>
                            <div id="aspect-content" style="margin: 2rem;">
                                <div class="aspect-tab">
                                    <input id="person_info_portlet" type="checkbox" class="aspect-input"
                                           name="aspect">
                                    <label for="person_info_portlet" class="aspect-label"></label>
                                    <div class="aspect-content">
                                        <div class="aspect-info">
                                            <span class="aspect-name text-dark">&nbsp;
                                                <%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTO) %>
                                            </span>
                                        </div>
                                        <div class="aspect-stat">

                                        </div>
                                    </div>
                                    <div class="aspect-tab-content">
                                        <div class="sentiment-wrapper">
                                            <div>
                                                <div>
                                                    <div class="neutral-count opinion-header">
                                                    </div>
                                                    <div>
                                                        <%-- tab content start--%>

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="edit-tools">
                                                                    <button class="btn view-btn btn-primary shadow btn-border-radius mr-2"
                                                                            id="view_details_link"
                                                                            onclick="location.href='Employee_recordsServlet?actionType=viewSummary&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                                                                        <i class="fa fa-eye"></i>&nbsp;<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>
                                                                    </button>

                                                                    <button class="btn view-btn btn-primary shadow btn-border-radius mr-2"
                                                                            id="view_short_pds_link"
                                                                            onclick="location.href='Employee_recordsServlet?actionType=pds&pdsType=shortPDS&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                                                                        <i class="fa fa-eye"></i>&nbsp;<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_SHORT_PDS, loginDTO) %>
                                                                    </button>

                                                                    <%if (employee_recordsDTO.isMP == 1) {%>
                                                                    <button type="button" class="btn download-btn"
                                                                            id="file-download-btn"
                                                                            style="border-radius: 15px"><i
                                                                            class="fa fa-download"></i><%= LM.getText(LC.EDMS_DOCUMENTS_DOWNLOAD, loginDTO) %>
                                                                    </button>
                                                                    <%}%>

                                                                    <button class="btn edit-btn btn-info shadow btn-border-radius"
                                                                            id="btn_personal_info_edit"
                                                                            onclick="location.href='<%=context%>/Employee_recordsServlet?actionType=editMultiForm&tab=1&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                                                                        <i class="fa fa-edit"></i> <%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO) %>&nbsp;
                                                                    </button>
                                                                </div>
                                                                <span style="clear: both"></span>
                                                            </div>
                                                        </div>

                                                        <%if (actionName.equals("view")) {%>
                                                        <div class="" id="personal_info_view">
                                                            <%@include file="section1_personal_info_view.jsp" %>
                                                        </div>

                                                        <%}%>


                                                        <%--tab content end--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--new accordian end--%>

                            <%--new accordian start--%>
                            <%if (actionName.equals("view")) {%>
                            <div id="aspect-content" style="margin: 2rem;">
                                <div class="aspect-tab">
                                    <input id="family_member_portlet" type="checkbox" class="aspect-input"
                                           name="aspect">
                                    <label for="family_member_portlet" class="aspect-label"></label>
                                    <div class="aspect-content">
                                        <div class="aspect-info">
                                            <span class="aspect-name text-dark">
                                                <%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_FAMILY_MEMBER, loginDTO) %>
                                            </span>
                                        </div>
                                        <div class="aspect-stat">

                                        </div>
                                    </div>
                                    <div class="aspect-tab-content">
                                        <div class="sentiment-wrapper">
                                            <div>
                                                <div>
                                                    <div class="neutral-count opinion-header">
                                                    </div>
                                                    <div>
                                                        <%-- tab content start--%>
                                                        <div class="kt-portlet__content" id="family_member_card_body">
                                                        </div>
                                                        <%--tab content end--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%}%>
                            <%--new accordian end--%>
                        </div>
                    </div>
                </div>
                <%}%>
                <%if (actionName.equals("add") || actionName.equals("edit")) {%>
                <div class="" id="personal_info_edit">
                    <%@include file="section1_personal_info.jsp" %>
                </div>
                <%}%>
            </div>
        </div>
    </div>
</div>


<script>
    var employee_record;
    var has_family_member_html = false;

    $(document).ready(() => {
        $('#person_info_portlet').prop('checked', false);
        $('#family_member_portlet').prop('checked', false);
        <%if (actionName.equals("view")) {%>
        $("#person_info_portlet").change(function () {
            if (this.checked) {
                fetchJSONPersonalInfo(1, <%=empId%>);
            }
        });
        $("#family_member_portlet").change(function () {
            if (this.checked) {
                fetchFamilyMenberInfoHTML(<%=empId%>);
            }
        });
        <%}%>
        getEmploymentStatus(<%=employee_recordsDTO.officerTypeCat%>);  
        switch ('<%=actionName%>') {
            case 'add':
            case 'edit':
                $('#person_info_portlet').removeClass("kt-portlet--collapsed");
                break;
        }

        let selectorId = null;
        console.log("<%=request.getParameter("data")%>");
        switch ("<%=request.getParameter("data")%>") {
            case 'family':
                fetchFamilyMenberInfoHTML(<%=empId%>);
                selectorId = $('#family_member_portlet');
                break;
            case 'personal-info':
                fetchJSONPersonalInfo(1, <%=empId%>);
                selectorId = $('#person_info_portlet');
                break;
        }
        if (selectorId) {
            // selectorId.removeClass("kt-portlet--collapsed");
            selectorId.prop('checked', true);
            let selectorIdParent = $(selectorId).parent();
            $('html, body').animate({scrollTop: selectorIdParent.offset().top - 200}, 'slow');
        } else {
            $('#person_info_portlet').prop('checked', false);
            $('#family_member_portlet').prop('checked', false);
        }

        let showPasswordDiv = $("#changePasswordCheckBox").val();
        if (showPasswordDiv != null) {
            $("#updatePassword").hide();
        }

        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });

    });


    function validate() {

        event.preventDefault();

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                $('#file-validation-message').text(this.responseText);

                let resp = this.responseText + "";
                if (resp.includes("Invalid")) {
                    $("#file-validation-message").addClass("errmsg");
                    $("#file-validation-message").removeClass("validmsg");
                    console.log('invalid.......');
                } else {
                    $("#file-validation-message").addClass("validmsg");
                    $("#file-validation-message").removeClass("errmsg");
                    console.log('valid...........');
                }

            }
        };
        xhttp.open("Post", "Employee_recordsServlet?actionType=validate&ID=<%=empId%>", true);
        xhttp.send(new FormData(document.getElementById('employee-personal-info')));

    }

    function addressSelectedByLanguage(value, htmlID, selectedIndex, tagname, fieldName, row, language) {
        if (language == 'English') {
            addrselectedFuncByLang(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_recordsServlet", language);
            addrselectedFuncByLang(value, 'Bn_' + htmlID, selectedIndex, 'Bn_' + tagname, 'Bn_' + fieldName, row, false, "Employee_recordsServlet", 'Bangla');
        } else {
            addrselectedFuncByLang(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_recordsServlet", language);
            htmlID += '';
            tagname += '';
            fieldName += '';
            addrselectedFuncByLang(value, htmlID.substring(3), selectedIndex, tagname.substring(3), fieldName.substring(3), row, false, "Employee_recordsServlet", 'English');
        }

        if (htmlID == 'permanentAddress_geoSelectField_0_1') {
            var dist = $('#permanentAddress_geoSelectField_0_1 option:selected').text();
            $('#home_district_text_0').val(dist);
            $('#home-district').val(value);
        }
    }

    function init(row) {

        initGeoLocationByLang('presentAddress_geoSelectField_', row, "Employee_recordsServlet", "English");
        initGeoLocationByLang('permanentAddress_geoSelectField_', row, "Employee_recordsServlet", "English");
        initGeoLocationByLang('Bn_presentAddress_geoSelectField_', row, "Employee_recordsServlet", "Bangla");
        initGeoLocationByLang('Bn_permanentAddress_geoSelectField_', row, "Employee_recordsServlet", "Bangla");
        select2SingleSelector("#geoThanaType_select", '<%=Language%>');
        select2SingleSelector("#gender_select_0", '<%=Language%>');
        select2SingleSelector("#nationalityType_select_0", '<%=Language%>');
        select2SingleSelector("#birth_district_text_0", '<%=Language%>');
        select2SingleSelector("#bloodGroup_select_0", '<%=Language%>');
        select2MultiSelector("#freedomFighter_select_0", '<%=Language%>');
    }

    var row = 0;

    $(document).ready(function () {
        init(row);
    });


    function openAddFamilyMember() {
        $(".add-new-family-member").show();
    }

    function closeAddFamilyMember() {
        $(".add-new-family-member").hide();
    }

    function fetchJSONPersonalInfo(tabId, recordId) {
        if (typeof employee_record !== 'undefined') {
            return;
        }
        $.ajax({
            url: "Employee_recordsServlet?actionType=ajax_getEmployeeRecord&language=<%=Language%>&tab=" + tabId + "&ID=" + recordId,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                employee_record = fetchedData;
                showEmployeeRecord();
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function showEmployeeRecord() {
        if (typeof employee_record === 'undefined') {
            return;
        }

        document.getElementById("nid-front-img").src = employee_record.nidFrontPhoto;
        document.getElementById("nid-back-img").src = employee_record.nidBackPhoto;
        document.getElementById("photo-img").src = employee_record.photo;
        document.getElementById('signature-img').src = employee_record.signature;
        if (employee_record.nameEng) {
            document.getElementById('nameEng').innerText = employee_record.nameEng;
        }
        if (employee_record.nameBng) {
            document.getElementById('nameBng').innerText = employee_record.nameBng;
        }
        if (employee_record.fatherNameEng) {
            document.getElementById('fatherNameEng').innerText = employee_record.fatherNameEng;
        }
        if (employee_record.fatherNameBng) {
            document.getElementById('fatherNameBng').innerText = employee_record.fatherNameBng;
        }
        if (employee_record.motherNameEng) {
            document.getElementById('motherNameEng').innerText = employee_record.motherNameEng;
        }
        if (employee_record.motherNameBng) {
            document.getElementById('motherNameBng').innerText = employee_record.motherNameBng;
        }
        if (employee_record.nid) {
            document.getElementById('nid').innerText = employee_record.nid;
        }
        if (employee_record.homeDistrict) {
            document.getElementById('homeDistrict').innerText = employee_record.homeDistrict;
        }
        if (employee_record.dateOfBirth) {
            document.getElementById('dateOfBirth').innerText = employee_record.dateOfBirth;
        }
        if (employee_record.birthDistrict) {
            document.getElementById('birthDistrict').innerText = employee_record.birthDistrict;
        }
        if (employee_record.identificationMark) {
            document.getElementById('identificationMark').innerText = employee_record.identificationMark;
        }
        if (employee_record.maritalStatus) {
            document.getElementById('maritalStatus').innerText = employee_record.maritalStatus;
        }
        if (employee_record.passportNo) {
            document.getElementById('passportNo').innerText = employee_record.passportNo;
        }
        if (employee_record.nationality) {
            document.getElementById('nationality').innerText = employee_record.nationality;
        }
        if (employee_record.bcn) {
            document.getElementById('bcn').innerText = employee_record.bcn;
        }
        if (employee_record.gender) {
            document.getElementById('gender').innerText = employee_record.gender;
        }
        if (employee_record.presentAddressEng) {
            document.getElementById('presentAddressEng').innerText = employee_record.presentAddressEng;
        }
        if (employee_record.presentAddressBng) {
            document.getElementById('presentAddressBng').innerText = employee_record.presentAddressBng;
        }
        if (employee_record.permanentAddressEng) {
            document.getElementById('permanentAddressEng').innerText = employee_record.permanentAddressEng;
        }
        if (employee_record.permanentAddressBng) {
            document.getElementById('permanentAddressBng').innerText = employee_record.permanentAddressBng;
        }
        if (employee_record.personalMobile) {
            document.getElementById('personalMobile').innerText = employee_record.personalMobile;
        }
        if (employee_record.personalEml) {
            document.getElementById('personalEml').innerText = employee_record.personalEml;
        }
        if (employee_record.officialEml) {
            document.getElementById('officialEml').innerText = employee_record.officialEml;
        }
        if (employee_record.personalMobile) {
            document.getElementById('personalMobile').innerText = employee_record.personalMobile;
        }
        if (employee_record.officePhoneNumber) {
            document.getElementById('officePhoneNumber').innerText = employee_record.officePhoneNumber;
        }
        if (employee_record.faxNumber) {
            document.getElementById('faxNumber').innerText = employee_record.faxNumber;
        }
        if (employee_record.height) {
            document.getElementById('height').innerText = employee_record.height;
        }
        if (employee_record.passportIssueDate) {
            document.getElementById('passportIssueDate').innerText = employee_record.passportIssueDate;
        }
        if (employee_record.passportExpiryDate) {
            document.getElementById('passportExpiryDate').innerText = employee_record.passportExpiryDate;
        }
        if (employee_record.marriageDate) {
            document.getElementById('marriageDate').innerText = employee_record.marriageDate;
        }
        if (employee_record.bloodGroup) {
            document.getElementById('bloodGroup').innerText = employee_record.bloodGroup;
        }
        if (employee_record.religion) {
            document.getElementById('religion').innerText = employee_record.religion;
        }
        if (employee_record.alternativeMobile) {
            document.getElementById('alternativeMobile').innerText = employee_record.alternativeMobile;
        }
        if (employee_record.officePhoneExtension) {
            document.getElementById('officePhoneExtension').innerText = employee_record.officePhoneExtension;
        }
        if (employee_record.freedomFighterInfo) {
            document.getElementById('freedomFighterInfo').innerText = employee_record.freedomFighterInfo;
        }
        if (employee_record.joiningDate) {
            document.getElementById('dateOfJoiningDate').innerText = employee_record.joiningDate;
        }
        if (employee_record.govtJobJoiningDate) {
            document.getElementById('dateOfGovtJobJoiningDate').innerText = employee_record.govtJobJoiningDate;
        }
        document.getElementById('payScale').innerText = employee_record.payScale;

        if (employee_record.lprDate) {
            document.getElementById('dateOfLPR').innerText = employee_record.lprDate;
        }

        if (employee_record.retirementDate) {
            document.getElementById('dateOfRetirement').innerText = employee_record.retirementDate;
        }

        if (employee_record.isMp == 1) {
            $('.mp_div').show();
            if (employee_record.parliamentNumber) {
                $('#parliamentNumber').text(employee_record.parliamentNumber);
            }
            if (employee_record.electionConstituency) {
                $('#electionConstituency').text(employee_record.electionConstituency);
            }
            if (employee_record.politicalParty) {
                $('#politicalParty').text(employee_record.politicalParty);
            }
            if (employee_record.professionOfMP) {
                $('#mpProfession').text(employee_record.professionOfMP);
            }
            if (employee_record.addressOfMP) {
                $('#mpAddress').text(employee_record.addressOfMP);
            }
            document.getElementById("mpAltSign").src = employee_record.alternateImageOfMP;
        } else {
            $('.emp_div').show();
            if (employee_record.employeeCatagory) {
                $('#employeeCatagory').text(employee_record.employeeCatagory);
            }
            if (employee_record.employeeClass) {
                $('#employeeClass').text(employee_record.employeeClass);
            }
            if (employee_record.officerType) {
                $('#officerType').text(employee_record.officerType);               
            }
            if (employee_record.officerType) {
                $('#officerType').text(employee_record.officerType);               
            }
            if (employee_record.currentOffice) {
            	$('.other_div').show();
                $('#currentOffice').text(employee_record.currentOffice);
                $('#deptAndDesignation').text(employee_record.deptAndDesignation);
            }
            else
           	{
            	$('.other_div').hide();
           	}
            if (employee_record.provisionEndDate) {
                $('#provisionEndPeriod').text(employee_record.provisionEndDate);
            }
        }
        console.log("END: showEmployeeRecord");
    }

    // make idDiv div visible and updates id input fields value
    function makevisible_and_update_value(id, value) {
        document.getElementById(id + "Div").style.display = "block";
        document.getElementById(id).innerText = value;
    }

    function hasAjaxMarker(ajaxHtml) {
        return ajaxHtml.includes('data-ajax-marker="true"');
    }

    function fetchFamilyMenberInfoHTML(id) {
        if (has_family_member_html) {
            return;
        }
        $.ajax({
            url: "Employee_recordsServlet?actionType=ajax_getFamilyMembers&language=<%=Language%>&ID=" + id + "&userId=" +<%=request.getParameter("userId")%>,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if (hasAjaxMarker(fetchedData)) {
                    has_family_member_html = true;
                    document.getElementById('family_member_card_body').innerHTML = fetchedData;
                } else {
                    window.location.href = "";
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    
    function getEmploymentStatus(type)
    {
    	fillSelect("employmentStatus", 
    			"Employee_recordsServlet?actionType=getStatusOptions&type=" + type + "&status=<%=employee_recordsDTO.employmentStatus%>");
    	if(type == <%=Employee_recordsServlet.OTHER_OFFICER%>)
   		{
   			$("#otherOfficeOptionsDiv").removeAttr("style");
   			$("#otherOfficeDesignationDiv").removeAttr("style");
   			getDept($("#currentOffice_category").val());
   		}
    	else
   		{
    		$("#otherOfficeOptionsDiv").css("display", "none");
    		$("#otherOfficeDesignationDiv").css("display", "none");
    		$("#otherOfficeDeptDiv").css("display", "none");
   		}
    }
    
    function getDept(value)
    {
    	console.log("getDept = " + value);
    	var dept = $("#otherOfficeDeptType").val();
    	if(value != -1)
   		{
   			$("#otherOfficeDeptDiv").removeAttr("style");
   			fillSelect("otherOfficeDeptType", "Other_office_departments_listServlet?actionType=getDept&otherOfficeId=" + value + "&dept=" + dept);
   		}
    	else
   		{
    		$("#otherOfficeDeptDiv").css("display", "none");
   		}
    }

</script>
