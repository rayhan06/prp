<%@page contentType="text/html;charset=utf-8" %>
<%@page import="election_constituency.Election_constituencyRepository" %>
<%@page import="election_details.Election_detailsRepository" %>
<%@ page import="religion.ReligionRepository" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="nationality.NationalityRepository" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="geolocation.*" %>
<%@ page import="election_wise_mp.Election_wise_mpDTO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.RoleEnum" %>
<%@ page import="election_wise_mp.Election_wise_mpDAO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="other_office.*" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="grade_wise_pay_scale.Grade_wise_pay_scaleRepository" %>
<%
    int maxLen = 50;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    Election_wise_mpDTO election_wise_mpDTO = null;
    if (employee_recordsDTO.isMP == 1) {
        String userId = request.getParameter("userId");
        if (userId != null) {
            election_wise_mpDTO = Election_wise_mpDAO.getInstance().getDTOByID(employee_recordsDTO.electionWiseMpId);
        }
    }
    if (election_wise_mpDTO == null) {
        election_wise_mpDTO = new Election_wise_mpDTO();
    }
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    boolean loginUserIsAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId() || userDTO.roleID == RoleEnum.DATA_ENTRY.getRoleId();
    boolean isLangEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String fileColumnName = "";
    FilesDAO filesDAO = new FilesDAO();
    long ColumnID = -1;
    String servletName = "Employee_recordsServlet";
%>
<style>
    .double_blank_row {
        height: 20px !important;
        background-color: transparent;
    }

    @media (max-width: 575.98px) {
        .eye-icon-1 {
            position: relative;
            top: -27px;
            left: 93%;
            cursor: pointer;
            z-index: 1;
        }

        .eye-icon-2 {
            position: relative;
            top: -27px;
            left: 93%;
            cursor: pointer;
            z-index: 1;
        }

    }

    @media (min-width: 576px) and (max-width: 767.98px) {
        .eye-icon-1 {
            position: relative;
            top: -27px;
            left: 95%;
            cursor: pointer;
            z-index: 1;
        }

        .eye-icon-2 {
            position: relative;
            top: -27px;
            left: 95%;
            cursor: pointer;
            z-index: 1;
        }

    }

    @media (min-width: 768px) and (max-width: 991.98px) {
        .eye-icon-1 {
            position: relative;
            top: -27px;
            left: 91%;
            cursor: pointer;
            z-index: 1;
        }

        .eye-icon-2 {
            position: relative;
            top: -27px;
            left: 91%;
            cursor: pointer;
            z-index: 1;
        }

    }

    @media (min-width: 992px) and (max-width: 1199.98px) {
        .eye-icon-1 {
            position: relative;
            top: -27px;
            left: 90%;
            cursor: pointer;
            z-index: 1;
        }

        .eye-icon-2 {
            position: relative;
            top: -27px;
            left: 90%;
            cursor: pointer;
            z-index: 1;
        }

    }

    @media (min-width: 1200px) and (max-width: 1399.98px) {
        .eye-icon-1 {
            position: relative;
            top: -29px;
            left: 90%;
            cursor: pointer;
            z-index: 1;
        }

        .eye-icon-2 {
            position: relative;
            top: -29px;
            left: 90%;
            cursor: pointer;
            z-index: 1;
        }

    }

    @media (min-width: 1400px) and (max-width: 1599.98px) {
        .eye-icon-1 {
            position: relative;
            top: -29px;
            left: 93%;
            cursor: pointer;
            z-index: 1;
        }

        .eye-icon-2 {
            position: relative;
            top: -29px;
            left: 93%;
            cursor: pointer;
            z-index: 1;
        }

    }

    @media (min-width: 1600px){
        .eye-icon-1 {
            position: relative;
            top: -29px;
            left: 95%;
            cursor: pointer;
            z-index: 1;
        }

        .eye-icon-2 {
            position: relative;
            top: -29px;
            left: 95%;
            cursor: pointer;
            z-index: 1;
        }

    }

</style>

<form class="kt-form" id="employee-personal-info">
    <input type='hidden' class='form-control' name='iD'
           id='iD_hidden_<%=i%>' value='<%=empId%>'/>
    <div class="form-body my-5">
        <div class="row">
            <div class="col-12">
                <div class="onlyborder">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_IDENTITY, loginDTO)%>
                                            </h4>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='nameEng_div_<%=i%>'>
                                                                <input maxlength="<%=maxLen%>" type='text'
                                                                       class='englishOnly form-control rounded'
                                                                       name='nameEng' required="required"
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_NAME_IN_ENGLISH, loginDTO)%>'
                                                                       id='nameEng_text'
                                                                       value='<%=employee_recordsDTO.nameEng!=null?employee_recordsDTO.nameEng:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='nameBng_div_<%=i%>'>
                                                                <input maxlength="<%=maxLen%>" type='text'
                                                                       class='noEnglish form-control rounded'
                                                                       name='nameBng' required="required"
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_NAME_IN_BANGLA, loginDTO)%>'
                                                                       id='nameBng_text'
                                                                       value='<%=employee_recordsDTO.nameBng!=null?employee_recordsDTO.nameBng:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEENG, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='fatherNameEng_div_<%=i%>'>
                                                                <input maxlength="255" type='text'
                                                                       class='englishOnly form-control rounded'
                                                                       name='fatherNameEng'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_FATHERS_NAME_IN_ENGLISH, loginDTO)%>'
                                                                       id='fatherNameEng_text'
                                                                       value='<%=employee_recordsDTO.fatherNameEng!=null?employee_recordsDTO.fatherNameEng:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEBNG, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='fatherNameBng_div_<%=i%>'>
                                                                <input maxlength="255" type='text'
                                                                       class='noEnglish form-control rounded'
                                                                       name='fatherNameBng'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_FATHERS_NAME_IN_BANGLA, loginDTO)%>'
                                                                       id='fatherNameBng_text'
                                                                       value='<%=employee_recordsDTO.fatherNameBng!=null?employee_recordsDTO.fatherNameBng:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEENG, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='motherNameEng_div_<%=i%>'>
                                                                <input maxlength="255" type='text'
                                                                       class='englishOnly form-control rounded'
                                                                       name='motherNameEng'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_MOTHERS_NAME_IN_ENGLISH, loginDTO)%>'
                                                                       id='motherNameEng_text'
                                                                       value='<%=employee_recordsDTO.motherNameEng!=null?employee_recordsDTO.motherNameEng:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEBNG, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='motherNameBng_div_<%=i%>'>
                                                                <input maxlength="255" type='text'
                                                                       class='noEnglish form-control rounded'
                                                                       name='motherNameBng'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_MOTHERS_NAME_IN_BANGLA, loginDTO)%>'
                                                                       id='motherNameBng_text'
                                                                       value='<%=employee_recordsDTO.motherNameBng!=null?employee_recordsDTO.motherNameBng:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='dateOfBirth_div_<%=i%>'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID" value="date-of-birth-js"/>
                                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='date-of-birth'
                                                                       name='dateOfBirth' value=''
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_RELIGION, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='religion_div_<%=i%>'>
                                                                <select class='form-control rounded' name='religion'
                                                                        id='religion_select'>
                                                                    <%= ReligionRepository.getInstance().buildOptions(Language, employee_recordsDTO.religion)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_GENDER, loginDTO)%>
                                                            <span
                                                                    class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='gender_div_<%=i%>'>
                                                                <select class='form-control' name='gender' required
                                                                        id='gender_select'>
                                                                    <%=CatRepository.getInstance().buildOptions("gender", Language, employee_recordsDTO.gender)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NATIONALITYTYPE, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='nationalityType_div_<%=i%>'>
                                                                <select class='form-control rounded'
                                                                        name='nationalityType'
                                                                        required
                                                                        id='nationalityType_select' tag='pb_html'>
                                                                    <%=NationalityRepository.getInstance().buildOptions(Language, employee_recordsDTO.nationalityType)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="personal-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeePersonalForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_ADDRESS, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_ENG, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <jsp:include page="/geolocation/geoLocation.jsp">
                                                                <jsp:param name="GEOLOCATION_ID"
                                                                           value="permanentAddressEng_js"></jsp:param>
                                                                <jsp:param name="LANGUAGE"
                                                                           value="English"></jsp:param>
                                                            </jsp:include>
                                                            <input type="hidden" id="permanentAddressEng"
                                                                   name="permanentAddressEng">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_BNG, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <jsp:include page="/geolocation/geoLocation.jsp">
                                                                <jsp:param name="GEOLOCATION_ID"
                                                                           value="permanentAddressBng_js"></jsp:param>
                                                                <jsp:param name="LANGUAGE"
                                                                           value="Bnagla"></jsp:param>
                                                            </jsp:include>
                                                            <input type="hidden" id="permanentAddressBng"
                                                                   name="permanentAddressBng">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <input <%=employee_recordsDTO.sameAsPermanent?"checked":"unchecked"%>
                                                            style="margin: 0 5px 20px 0" type='checkbox' class="rounded"
                                                           name='sameAsPermanent' id='same-as-permanent'>
                                                    <b><%=isLangEnglish?"Present address same as permanent address":"বর্তমান ঠিকানা স্থায়ী ঠিকানার মতোই"%></b>
                                                </div>
                                                <div class="col-md-6 present-address" style="display: <%=employee_recordsDTO.sameAsPermanent?"none":"block"%>">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_ENG, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <jsp:include page="/geolocation/geoLocation.jsp">
                                                                <jsp:param name="GEOLOCATION_ID"
                                                                           value="presentAddressEng_js"></jsp:param>
                                                                <jsp:param name="LANGUAGE"
                                                                           value="English"></jsp:param>
                                                            </jsp:include>
                                                            <input type="hidden" id="presentAddressEng"
                                                                   name="presentAddressEng">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 present-address" style="display: <%=employee_recordsDTO.sameAsPermanent?"none":"block"%>">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_BNG, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <jsp:include page="/geolocation/geoLocation.jsp">
                                                                <jsp:param name="GEOLOCATION_ID"
                                                                           value="presentAddressBng_js"></jsp:param>
                                                                <jsp:param name="LANGUAGE"
                                                                           value="Bnagla"></jsp:param>
                                                            </jsp:include>
                                                            <input type="hidden" id="presentAddressBng"
                                                                   name="presentAddressBng">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_BIRTH_DISTRICT, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='birth_district_div_<%=i%>'>
                                                                <select class='form-control rounded'
                                                                        name='birthDistrict'
                                                                        id='birth_district_text_<%=i%>'
                                                                        tag='pb_html'>
                                                                    <%=GeoDistrictRepository.getInstance().getDistrictOptionUpdated(Language, employee_recordsDTO.birthDistrict)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_HOME_DISTRICT, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='home_district_div_<%=i%>'>
                                                                <input type='text' class='form-control rounded'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_HOME_DISTRICT_IS_SAME_AS_DISTRICT_IN_PERMANENT_ADDRESS, loginDTO)%>'
                                                                       name='homeDistrictValue'
                                                                       id='home_district_text_<%=i%>'
                                                                       value='<%=actionName.equals("edit") ? GeoLocationRepository.getInstance().getText(Language,employee_recordsDTO.homeDistrict) : ""%>'
                                                                       readonly/>
                                                            </div>
                                                            <input type='hidden' class='form-control'
                                                                   name='homeDistrict'
                                                                   id='homeDistrict'
                                                                   value="<%=actionName.equals("edit") ? employee_recordsDTO.homeDistrict : ""%>"
                                                                   tag='pb_html'>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="address-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeeAddressForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CONFIDENTIAL_INFORMATION, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PASSPORTNO, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div class=" " id='passportNo_div_<%=i%>'>
                                                                <input maxlength="20" type='text'
                                                                       class='form-control rounded' name='passportNo'
                                                                       id='passportNo_text'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_PASSPORT_NUMBER, loginDTO)%>'
                                                                       value='<%=employee_recordsDTO.passportNo!=null?employee_recordsDTO.passportNo:""%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_BCN, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='bcn_div_<%=i%>'>
                                                                <input maxlength="20" type='text'
                                                                       class='form-control rounded' name='bcn'
                                                                       id='bcn_text'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_BIRTH_CERTIFICATE_NO, loginDTO)%>'
                                                                       value='<%=employee_recordsDTO.bcn!=null?employee_recordsDTO.bcn:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PASSPORTISSUEDATE, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='passportIssueDate_div_<%=i%>'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="passport-issue-date-js"/>
                                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='passport-issue-date' name='passportIssueDate'
                                                                       value='' tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PASSPORTEXPIRYDATE, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='passportExpiryDate_div_<%=i%>'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="passport-expiry-date-js"/>
                                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                                                    <jsp:param name="END_YEAR" value="<%=year + 11%>"/>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='passport-expiry-date'
                                                                       name='passportExpiryDate' value=''
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="confidential-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeeConfidentialForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_EMPLOYMENT_INFORMATION, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_JOININGDATE, loginDTO)%>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id="joiningDate_div">
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="joining-date-js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='joining-date'
                                                                       name='joiningDate' value=''
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%
                                                    if (employee_recordsDTO.isMP == 2) {
                                                        if (loginUserIsAdmin) {
                                                %>
                                                <div class="col-md-6 employee_class">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO) %>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='employeeClass_type_div_<%=i%>'>
                                                                <select class='form-control rounded' required
                                                                        name='employeeClassType'
                                                                        id='employeeClassType_select_<%=i%>'>
                                                                    <%=CatRepository.getInstance().buildOptions("employee_class", Language, employee_recordsDTO.employeeClass)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-6 employment_category">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= (LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)) %>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='employment_type_div_<%=i%>'>
                                                                <select class='form-control rounded' required
                                                                        name='employmentType'
                                                                        id='employmentType_select_<%=i%>'>
                                                                    <%=CatRepository.getInstance().buildOptions("employment", Language, employee_recordsDTO.employmentType)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 employee_office_type">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO) %>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='officer_type_div_<%=i%>'>
                                                                <select class='form-control rounded' name='officerType'
                                                                        required
                                                                        id='officerType_select_<%=i%>'
                                                                        <%--onchange = 'getEmploymentStatus(this.value)' --%>
                                                                        >
                                                                    <%=CatRepository.getInstance().buildOptions("emp_officer", Language, employee_recordsDTO.officerTypeCat)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-6 employee_office_type" id = "otherOfficeOptionsDiv" style="display: none">
		                                            <div class="form-group row">
		                                                <label class="col-md-3 col-form-label text-md-right">
		                                                    <%=isLanguageEnglish ? "Current Office" : "বর্তমান অফিস"%>
		                                                    <span>*</span>
		                                                </label>
		                                                <div class="col-md-9">
		                                                    <select class='form-control rounded' name='currentOffice' <%--onChange = 'getDept(this.value)'--%>
		                                                            id='currentOffice_category' tag='pb_html'>
		                                                       <%=Other_officeRepository.getInstance().buildOptions(Language, employee_recordsDTO.currentOffice)%>
		                                                    </select>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        
		                                        <div class="col-md-6 employee_class" id ="otherOfficeDeptDiv" style="display: none">
		                                            <div class="form-group row">
		                                                <label class="col-md-3 col-form-label text-md-right">
		                                                    <%=isLanguageEnglish ? "Department" : "বিভাগ"%>
		                                                    <span>*</span>
		                                                </label>
		                                                <div class="col-md-9">
		                                                    <select class='form-control rounded' name='otherOfficeDeptType'
		                                                            id='otherOfficeDeptType' tag='pb_html'>
		                                                        <option value = '<%=employee_recordsDTO.otherOfficeDeptType%>'></option>
		                                                    </select>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        
		                                        <div class="col-md-6 employee_office_type" id ="otherOfficeDesignationDiv" style="display: none">
		                                            <div class="form-group row">
		                                                <label class="col-md-3 col-form-label text-md-right">
		                                                    <%=isLanguageEnglish ? "Current Designation" : "বর্তমান পদবী"%>
		
		                                                </label>
		                                                <div class="col-md-9">
		                                                    <input type='text' class='form-control rounded'
		                                                           name='currentDesignation' value = '<%=employee_recordsDTO.currentDesignation%>'
		                                                           placeholder="<%=isLanguageEnglish?"Enter current designation":"বর্তমান পদবী লিখুন"%>"
		                                                           id='currentDesignation' tag='pb_html'/>
		                                                </div>
		                                            </div>
		                                        </div>
		                                        
		                                         <div class="col-md-6 employee_office_type" id ="payScaleDiv" >
		                                            <div class="form-group row">
		                                                <label class="col-md-3 col-form-label text-md-right">
		                                                    <%=isLanguageEnglish ? "Pay-Scale" : "পে স্কেল"%>
		
		                                                </label>
		                                                <div class="col-md-9">
		                                                    
		                                                           
		                                                           
                                                           <select class="form-control" class='form-control rounded' id="payScale" name="payScale" tag='pb_html'>
											                    <%=Grade_wise_pay_scaleRepository.getInstance().buildOptions(Language,  employee_recordsDTO.payScale)%>
											                </select>
		                                                </div>
		                                            </div>
		                                        </div>

                                                <div class="col-md-6 provision_period">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_PROVISION_PERIOD, loginDTO) %>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <input type='text'
                                                                   class='form-control rounded digitOnly'
                                                                   name='provision_period'
                                                                   id='provision_period_<%=i%>' maxlength="3"
                                                                   placeholder='<%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PROVISION_IN_MONTHS, loginDTO)%>'
                                                                   value='<%=employee_recordsDTO.provisionPeriod!=0?employee_recordsDTO.provisionPeriod:""%>'
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 employment_status">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Employement Status" : "চাকুরির অবস্থা"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='employment_status_div_<%=i%>'>
                                                                <select class='form-control rounded' required
                                                                        name='employmentStatus'
                                                                        id='employmentStatus'>
                                                                    <%=CatRepository.getInstance().buildOptions("employees_employment_status", Language, employee_recordsDTO.employmentStatus)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6" id="employment_status_change_date_div"
                                                     style="display: none">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Employment Status Change Date" : "চাকুরির অবস্থা পরিবর্তনের তারিখ"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='employmentStatusChangeDate_div'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="employment-status-change-date-js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='employment-status-change-date'
                                                                       name='employmentStatusChangeDate'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <%
                                                    }
                                                } else {
                                                    if (loginUserIsAdmin) {
                                                %>

                                                <div class="col-md-6 parliament_number">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.EMPLOYEE_PARLIAMENT_NUMBER, loginDTO) %>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='parliament_number_div_<%=i%>'>
                                                                <select class='form-control rounded'
                                                                        name='parliamentNumber' required
                                                                        id='parliament_number_text'
                                                                        tag='pb_html' onchange="changeElection()">
                                                                    <%=Election_detailsRepository.getInstance().buildOptions(Language, election_wise_mpDTO.electionDetailsId)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 election_constituency">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.EMPLOYEE_ELECTION_CONSTITUENCY, loginDTO) %>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='election_constituency_of_mp_div_<%=i%>'>
                                                                <select class='form-control rounded' required
                                                                        name='electionConstituencyOfMp'
                                                                        id='election_constituency_of_mp_text'
                                                                        tag='pb_html'>
                                                                    <%
                                                                        if (actionName.equals("edit") && employee_recordsDTO.isMP == 1) {
                                                                    %>
                                                                    <%=Election_constituencyRepository.getInstance().buildOptions(Language, election_wise_mpDTO.electionConstituencyId, election_wise_mpDTO.electionDetailsId)%>
                                                                    <%}%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 political_party">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.ELECTION_WISE_MP_POLITICAL_PARTY, loginDTO) %>
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='politicalPartyId_div_<%=i%>'>
                                                                <select class='form-control rounded'
                                                                        name='politicalPartyId' required
                                                                        id='politicalPartyId'
                                                                        tag='pb_html'>
                                                                    <%=Political_partyRepository.getInstance().buildOptions(Language, election_wise_mpDTO.politicalPartyId)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 elected_times">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right"
                                                               for="electedCount">
                                                            <%=LM.getText(LC.PALCEHOLDER_HOW_MANY_TIMES_ELECTED, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id="electedTimes_div">
                                                                <input type="text" class="form-control rounded"
                                                                       id="electedCount"
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_HOW_MANY_TIMES_ELECTED, loginDTO)%>'
                                                                       name="electedCount"
                                                                       value='<%=employee_recordsDTO.mpElectedCount>0 ? employee_recordsDTO.mpElectedCount : ""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 mp_status">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.HM_MP_STATUS, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='politicalPartyId_div_<%=i%>'>
                                                                <select class='form-control rounded'
                                                                        name='mpStatus' required
                                                                        id='mpStatus'
                                                                        tag='pb_html'>
                                                                    <%=CatRepository.getInstance().buildOptionsWithoutSelectOption("mp_status", Language, election_wise_mpDTO.mpStatus)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6" id="mp_status_change_date_div"
                                                     style="display: none">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "MP Status Change Date" : "এমপি অবস্থা পরিবর্তনের তারিখ"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='mpStatusChangeDate_div'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="mp-status-change-date-js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='mp-status-change-date'
                                                                       name='mpStatusChangeDate'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 mp_remarks">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right"
                                                               for="mp_remark">
                                                            <%=LM.getText(LC.HM_MP_REMARKS, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                                    <textarea type="text" class="form-control rounded"
                                                                              id="mp_remark" maxlength="1024"
                                                                              placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_REMARKS, loginDTO)%>'
                                                                              rows="3" style="resize: none;"
                                                                              name="mp_remark"><%=election_wise_mpDTO.mpRemarks != null ? election_wise_mpDTO.mpRemarks : ""%></textarea>
                                                            <p id="mp_remarks_len"
                                                               style="width: 100%;text-align: right;font-size: small">
                                                                0/1024</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mp_organization">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right"
                                                               for="mp_organization">
                                                            <%=isLangEnglish ? "Organization List" : "সংঠনসমূহের তালিকা"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                                    <textarea type="text" class="form-control rounded"
                                                                              id="mp_organization" maxlength="1024"
                                                                              placeholder='<%=isLangEnglish?"Enter organization list":"সংঠনসমূহের তালিকা লিখুন"%>'
                                                                              rows="3" style="resize: none;"
                                                                              name="mp_organization"><%=election_wise_mpDTO.mpOrganization != null ? election_wise_mpDTO.mpOrganization : ""%></textarea>
                                                            <p id="mp_organization_len"
                                                               style="width: 100%;text-align: right;font-size: small">
                                                                0/1024</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%
                                                    }
                                                %>
                                                <div class="col-md-6 mp_profession">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_MP_PROFESSION, loginDTO) %>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id="ProfessionOfMP_div_<%=i%>">
                                                                <input type="text" class="form-control rounded"
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_CURRENT_PROFESSION_AS_MP, loginDTO)%>'
                                                                       name="ProfessionOfMP"
                                                                       value='<%=election_wise_mpDTO.professionOfMP!=null?election_wise_mpDTO.professionOfMP:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 mp_address">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_MP_ADDRESS, loginDTO) %>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id="ParliamentAddress_div_<%=i%>">
                                                                <input type="text" class="form-control rounded"
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_CURRENT_ADDRESS_AS_MP, loginDTO)%>'
                                                                       name="ParliamentAddress"
                                                                       value='<%=election_wise_mpDTO.addressOfMP!=null?election_wise_mpDTO.addressOfMP:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 mp_alt_sign">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_MP_ALTERNATE_SIGN, loginDTO) %>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <%
                                                                if (election_wise_mpDTO.alternateImageOfMP != null) {
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(election_wise_mpDTO.alternateImageOfMP);
                                                            %>
                                                            <img src='data:image/jpg;base64,<%=new String(encodeBase64)%>'
                                                                 class="rounded"
                                                                 style='width:100px' id="mp-alt-sign">
                                                            <%
                                                                }
                                                            %>
                                                            <input type='file' class='form-control rounded'
                                                                   name='alternateImage'
                                                                   id='alternateImage_blob' tag='pb_html'/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <%--Employee hobby, gazette date and gazette document--%>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "MP Hobbies" : "সাংসদের শখ"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='mp_hobbies_div_<%=i%>'>
                                                                <input maxlength="200" type='text'
                                                                       class='form-control rounded'
                                                                       placeholder='<%=isLangEnglish ? "Describe Hobbies of Hononary MP" : "সাংসদের শখগুলো উল্লেখ করুন"%>'
                                                                       name='mp_hobbies'
                                                                       id='mp_hobbies'
                                                                       value='<%=election_wise_mpDTO.mpHobbies!=null?election_wise_mpDTO.mpHobbies:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Parliamentary Designation" : "সংসদীয় পদবি"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='parliamentary_designation_div_<%=i%>'>
                                                                <input maxlength="200" type='text'
                                                                       class='form-control rounded'
                                                                       placeholder='<%=isLangEnglish ? "Describe parliamentary designation" : "সংসদীয় পদবি দিন"%>'
                                                                       name='parliamentary_designation'
                                                                       id='parliamentary_designation'
                                                                       value='<%=election_wise_mpDTO.parliamentaryDesignation!=null?election_wise_mpDTO.parliamentaryDesignation:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6" id="election_date_div">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Election Date" : "নির্বাচনের তারিখ"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='electionDate_div_<%=i%>'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="election-date-js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='election-date'
                                                                       name='electionDate'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6" id="mp_gazette_date_div">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Gazette Date" : "গেজেটের তারিখ"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='mpGazetteDate_div_<%=i%>'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="mp-gazette-date-js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='mp-gazette-date'
                                                                       name='mpGazetteDate'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6" id="oath_date_div">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Oath Date" : "শপথের তারিখ"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='oathDate_div_<%=i%>'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="oath-date-js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='oath-date'
                                                                       name='oathDate'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6" id="mpGazetteDocument">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Gazette Document of MP" : "সাংসদের গেজেট ডকুমেন্ট"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='mpGazetteDocument_div_<%=i%>'>
                                                                <%
                                                                    if (election_wise_mpDTO.mpGazetteDocument != null) {
                                                                        byte[] encodeBase64Photo = Base64.encodeBase64(election_wise_mpDTO.mpGazetteDocument);
                                                                %>
                                                                <img src='data:image/jpg;base64,<%=new String(encodeBase64Photo)%>'
                                                                     class="rounded"
                                                                     style='width:120px;height:75px;'
                                                                     id="mp-gazette-doc">
                                                                <%
                                                                    }
                                                                %>
                                                                <input type='file'
                                                                       accept="application/pdf, image/png, image/jpeg"
                                                                       class='form-control rounded'
                                                                       name='mp_gazette_doc' id='mpGazetteDocument_blob'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%
                                                    }
                                                %>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="employment-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeeEmploymentForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%-- PRL Date Begin --%>
            <%if (employee_recordsDTO.isMP == 2 && loginUserIsAdmin) {%>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_EMPLOYMENT_EXTENSION, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input onchange="showLprDate()" style="margin: 0 5px 20px 0"
                                                           type='checkbox' name='extendPRL'
                                                           id='extendPRL_checkbox' value='0' tag='pb_html'>
                                                    <b><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_EXTEND_PRL_DATE, loginDTO)%></b>
                                                </div>
                                                <div class="col-md-6" id="lprDateDiv" style="display: none">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_LPR_DATE, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <jsp:include page="/date/date.jsp">
                                                                <jsp:param name="DATE_ID"
                                                                           value="lprDate-js"></jsp:param>
                                                                <jsp:param name="LANGUAGE"
                                                                           value="<%=Language%>"></jsp:param>
                                                                <jsp:param name="END_YEAR"
                                                                           value="<%=year + 45%>"></jsp:param>
                                                            </jsp:include>
                                                            <input type='hidden' class='form-control' id='lprDate'
                                                                   name='lprDate' value=''
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="lpr-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeeLPRForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <%}%>
        <%-- PRL Date End --%>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CONTACT, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_PERSONAL_MOBILE_NUMBER, loginDTO)%>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div class="input-group mb-2">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text">+88</div>
                                                                </div>
                                                                <input type='text'
                                                                       class='digitOnly form-control rounded'
                                                                       name='personalMobile' maxlength="11"
                                                                       id='personalMobile_text' required
                                                                       pattern="^(01[3-9]{1}[0-9]{8})"
                                                                       value='<%=employee_recordsDTO.personalMobile ==null || employee_recordsDTO.personalMobile.length() == 0?"":
                                                                   (employee_recordsDTO.personalMobile.startsWith("88") ? employee_recordsDTO.personalMobile.substring(2) : employee_recordsDTO.personalMobile)%>'
                                                                       placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                                       title='<%=isLanguageEnglish?"personal mobile number must start with 01, then contain 9 digits"
                                                               :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_ALTERNATIVEMOBILE, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div class="input-group mb-2">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text">+88</div>
                                                                </div>
                                                                <input type='text'
                                                                       class='digitOnly form-control rounded'
                                                                       name='alternativeMobile' maxlength="11"
                                                                       id='alternativeMobile_text'
                                                                       pattern="^(01[3-9]{1}[0-9]{8})"
                                                                       value='<%=employee_recordsDTO.alternativeMobile ==null || employee_recordsDTO.alternativeMobile.length() == 0?"":
                                                                   (employee_recordsDTO.alternativeMobile.startsWith("88") ? employee_recordsDTO.alternativeMobile.substring(2) : employee_recordsDTO.alternativeMobile)%>'
                                                                       placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                                       title='<%=isLanguageEnglish?"Alternative mobile number must start with 01, then contain 9 digits"
                                                               :"বিকল্প মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_NUMBER, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='officePhone_div_<%=i%>'>
                                                                <input type='text'
                                                                       class='form-control rounded digitOnly'
                                                                       name='officePhoneNumber' maxlength="15"
                                                                       id='officePhoneNumber_text'
                                                                       value='<%=employee_recordsDTO.officePhoneNumber!=null?employee_recordsDTO.officePhoneNumber:""%>'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_OFFICE_PHONE_NUMBER, loginDTO)%>'
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_EXTENSION, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='officePhoneExtension_div_<%=i%>'>
                                                                <input type='text'
                                                                       class='form-control rounded digitOnly'
                                                                       name='officePhoneExtension' maxlength="15"
                                                                       id='officePhoneExtension_text'
                                                                       value='<%=employee_recordsDTO.officePhoneExtension!=null?employee_recordsDTO.officePhoneExtension:""%>'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_OFFICE_PHONE_EXTENSION, loginDTO)%>'
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_FAX_NUMBER, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='faxNumber_div_<%=i%>'>
                                                                <input type='text' class='form-control rounded'
                                                                       name='faxNumber'
                                                                       id='faxNumber_text'
                                                                       value='<%=employee_recordsDTO.faxNumber!=null?employee_recordsDTO.faxNumber:""%>'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_FAX_NUMBER, loginDTO)%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERSONALEMAIL, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='personalEmail_div_<%=i%>'>
                                                                <input type='text' class='form-control rounded'
                                                                       name='personalEml'
                                                                       id='personal_text'
                                                                       value='<%=employee_recordsDTO.personalEml!=null?employee_recordsDTO.personalEml:""%>'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_YOUR_PERSONAL_EMAIL, loginDTO)%>'
                                                                       pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                                       title="personal email must be a of valid format"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Official Email" : "অফিশিয়াল ইমেইল"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='officialEmail_div_<%=i%>'>
                                                                <input type='text' class='form-control rounded'
                                                                       <%=loginUserIsAdmin?"":"disabled"%>
                                                                       name='officialEml'
                                                                       id='official_text'
                                                                       value='<%=employee_recordsDTO.officialEml!=null?employee_recordsDTO.officialEml:""%>'
                                                                       placeholder='<%=isLangEnglish?"Enter Your Official Email":"আপনার অফিশিয়াল ইমেইল দিন"%>'
                                                                       pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                                       title="official email must be a of valid format"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="contact-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeeContactForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_MISCELLANEOUS, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_MARITALSTATUS, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='maritalStatus_div_<%=i%>'>
                                                                <select class='form-control rounded'
                                                                        name='maritalStatus'
                                                                        id='maritalStatus_select_<%=i%>'>
                                                                    <%=CatRepository.getInstance().buildOptions("marital_status", Language, employee_recordsDTO.maritalStatus)%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" id="marriage_date_div">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MARRIAGEEDATE, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='marriageDate_div_<%=i%>'>
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="marriage-date-js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                                <input type='hidden' class='form-control'
                                                                       id='marriage-date'
                                                                       name='marriageDate' value=''
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" id="dummy_div" style="display:none;">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                        </label>
                                                        <div class="col-md-9">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_IDENTIFICATION_MARK, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='identification_mark_div_<%=i%>'>
                                                                <input maxlength="200" type='text'
                                                                       class='form-control rounded'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_IDENTIFICATION_MARK, loginDTO)%>'
                                                                       name='identificationMark'
                                                                       id='identification_mark_text'
                                                                       value='<%=employee_recordsDTO.identificationMark!=null?employee_recordsDTO.identificationMark:""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HEIGHT, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='height_div_<%=i%>'>
                                                                <input class="form-control rounded" type="text"
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_HEIGHT_IN_CM, loginDTO)%>'
                                                                       name="height" id="height"
                                                                       value='<%=employee_recordsDTO.height>0 ? String.format("%.2f", employee_recordsDTO.height):""%>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_BLOODGROUP, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='bloodGroup_div_<%=i%>'>
                                                                <select class='form-control rounded' name='bloodGroup'
                                                                        id='bloodGroup_select'>
                                                                    <%=CatRepository.getInstance().buildOptions("blood_group", Language, employee_recordsDTO.bloodGroup)%>
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_FREEDOM_FIGHTER_INFO, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='freedomFighter_div_<%=i%>'>
                                                                <select multiple="multiple" class='form-control rounded'
                                                                        name='freedomFighter'
                                                                        id='freedomFighter_select_<%=i%>'>
                                                                    <%=CatRepository.getInstance().buildOptionsForMultipleSelection("freedom_fighter_info", Language, employee_recordsDTO.freedomFighterInfo)%>
                                                                </select>
                                                                <input hidden class="form-control rounded" type="text"
                                                                       name="hidden_freedomFighter_select_<%=i%>"
                                                                       id="hidden_freedomFighter_select_<%=i%>"
                                                                       value=''/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=isLangEnglish ? "Quota" : "কোটা"%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='bloodGroup_div_<%=i%>'>
                                                                <select class='form-control rounded' name='jobQuota'
                                                                        id='jobQuota_select'>
                                                                    <%=CatRepository.getInstance().buildOptions("job_quota", Language, employee_recordsDTO.jobQuota)%>
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.NATIONAL_PAY_SCALE_ADD_FILEDROPZONE, loginDTO)%>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <%
                                                                fileColumnName = "quota_file";
                                                                if (actionName.equals("edit")) {
                                                                    if(employee_recordsDTO.quotaFile<=0){
                                                                        employee_recordsDTO.quotaFile = DBMW.getInstance().getNextSequenceId("fileid");
                                                                    }
                                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(employee_recordsDTO.quotaFile);
                                                            %>
                                                            <%@include file="../../../pb/dropzoneEditor.jsp" %>
                                                            <%
                                                                }
                                                            %>

                                                            <div class="dropzone"
                                                                 action="Employee_recordsServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=employee_recordsDTO.quotaFile%>">
                                                                <input type='file' style="display:none"
                                                                       name='<%=fileColumnName%>File'
                                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                                   tag='pb_html'/>
                                                            <input type='hidden' name='<%=fileColumnName%>'
                                                                   id='<%=fileColumnName%>_dropzone_<%=i%>'
                                                                   tag='pb_html'
                                                                   value='<%=employee_recordsDTO.quotaFile%>'/>


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="miscellaneous-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeeMiscellaneousForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NATIONAL_ID_CARD, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NID, loginDTO)%>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='nid_div_<%=i%>'>
                                                                <input maxlength="20" type='text'
                                                                       class='form-control rounded'
                                                                       name='nid'
                                                                       id='nid_text'
                                                                       required
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_NATIONAL_IDENTITY_NUMBER, loginDTO)%>'
                                                                       value='<%=employee_recordsDTO.nid!=null?employee_recordsDTO.nid:"" %>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                </div>
                                                <div class="col-md-6" id="nidFrontphoto">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_FRONT_SIDE, loginDTO) %>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='photo_div_<%=i%>'>
                                                                <%
                                                                    if (employee_recordsDTO.nidFrontPhoto != null) {
                                                                        byte[] encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.nidFrontPhoto);
                                                                %>
                                                                <img src='data:image/jpg;base64,<%=new String(encodeBase64Photo)%>'
                                                                     class="rounded"
                                                                     style='width:120px;height:75px;'
                                                                     id="nid-front-img">
                                                                <%
                                                                    }
                                                                %>
                                                                <input type='file' accept="image/png, image/jpeg, image/bmp"
                                                                       class='form-control rounded'
                                                                       name='nid_front_photo' id='nidFrontPhoto_blob'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" id="nidBackPhoto">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_BACK_SIDE, loginDTO) %>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <%
                                                                if (employee_recordsDTO.nidBackPhoto != null) {
                                                                    byte[] encodeBase64Signature = Base64.encodeBase64(employee_recordsDTO.nidBackPhoto);
                                                            %>
                                                            <img src='data:image/jpg;base64,<%=new String(encodeBase64Signature)%>'
                                                                 class="rounded"
                                                                 style='width:120px;height:75px;' id="nid-back-img">
                                                            <%
                                                                }
                                                            %>
                                                            <input type='file' accept="image/png, image/jpeg, image/bmp"
                                                                   class='form-control rounded'
                                                                   name='nid_back_photo' id='nidBackPhoto_blob'
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="nid-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeeNIDForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_SUBMIT_AND_GO, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6" id="photoImage">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_PHOTO, loginDTO) %>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='photo_div_<%=i%>'>
                                                                <%
                                                                    if (employee_recordsDTO.photo != null) {
                                                                        byte[] encodeBase64Photo = Base64.encodeBase64(employee_recordsDTO.photo);
                                                                %>
                                                                <img src='data:image/jpg;base64,<%=new String(encodeBase64Photo)%>'
                                                                     class="rounded"
                                                                     style='width:120px;height:75px;' id="photo-img">
                                                                <%
                                                                    }
                                                                %>
                                                                <input type='file' accept="image/png, image/jpeg, image/bmp"
                                                                       class='form-control rounded'
                                                                       name='photo' id='photoImage_blob' tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" id="signatureImage">
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_SIGNATURE, loginDTO) %>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <%
                                                                if (employee_recordsDTO.signature != null) {
                                                                    byte[] encodeBase64Signature = Base64.encodeBase64(employee_recordsDTO.signature);
                                                            %>
                                                            <img src='data:image/jpg;base64,<%=new String(encodeBase64Signature)%>'
                                                                 class="rounded"
                                                                 style='width:120px;height:75px;' id="signature-img">
                                                            <%
                                                                }
                                                            %>
                                                            <input type='file' accept="image/png, image/jpeg, image/bmp"
                                                                   class='form-control rounded'
                                                                   name='signature' id='signature_blob' tag='pb_html'/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="image-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeeImageForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body my-5">
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.EMPLOYEE_RECORD_PASSWORD, loginDTO)%>
                                                </h4>
                                            </div>
                                            <div class="row">
                                                <%
                                                    if (actionName.equals("edit")) {
                                                %>
                                                <div class="col-md-6">
                                                    <input type='checkbox' class="rounded" name='changePassword'
                                                           id='changePassword_checkbox_0' style="margin: 0 5px 20px 0">
                                                    <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_UPDATE_PASSWORD, loginDTO)%></b>
                                                </div>
                                                <div class="col-md-6">
                                                </div>
                                                <%
                                                    }
                                                %>
                                                <div class="col-md-6">
                                                    <div class="form-group row" id="password_div">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORD_PASSWORD, loginDTO)%>
                                                            <span
                                                                    class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='passwordDiv'>
                                                                <input autocomplete="new-password" type='password'
                                                                       class='form-control rounded'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_PASSWORD, loginDTO)%>'
                                                                       name='password'
                                                                       id='password'
                                                                       value=<%=actionName.equals("edit") ? ("'" + employee_recordsDTO.password != null ? employee_recordsDTO.password : "" + "'") : ("''")%>/>
                                                                <span class="fa fa-eye-slash eye-icon-1"
                                                                      id="togglePassword1"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row" id="confirm_password_div">
                                                        <label class="col-md-3 col-form-label text-md-right">
                                                            <%=LM.getText(LC.EMPLOYEE_RECORD_CONFIRM_PASSWORD, loginDTO)%>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-9">
                                                            <div id='confirmPasswordDiv'>
                                                                <input type='password' class='form-control rounded'
                                                                       name='confirmPassword'
                                                                       id='confirmPassword'
                                                                       placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_PASSWORD_AGAIN, loginDTO)%>'
                                                                       value=<%=actionName.equals("edit") ? ("'" + employee_recordsDTO.confirmPassword != null ? employee_recordsDTO.confirmPassword : "" + "'") : ("''")%>/>
                                                                <span class="fa fa-eye-slash eye-icon-2"
                                                                      id="togglePassword2"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-12 text-md-right">
                                                            <button id="password-submit-btn"
                                                                    class="btn-sm shadow submit-btn text-white border-0"
                                                                    type="button"
                                                                    onclick="submitEmployeePasswordForm()">
                                                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-md-right kt-form__actions mt-5">
                        <button class="btn-sm shadow cancel-btn text-white border-0" id="cancel-btn" type="button"
                                onclick="location.href='<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_EMPLOYEE_RECORDS_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow submit-btn text-white ml-2 border-0" type="button"
                                onclick="submitEmployeeForm()">
                            <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_EMPLOYEE_POSTING_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <input type='hidden' class='form-control' name='joiningDate'
               id='joiningDate_hidden_<%=i%>'
               value=<%=actionName.equals("edit")
                ? ("'" + employee_recordsDTO.joiningDate + "'")
                : ("'" + System.currentTimeMillis() + "'")%>/>
</form>
<style>
    .custom-error-feedback {
        width: 100%;
        margin-top: 0.25em;
        font-size: 80%;
        color: #fd397a;
    }
</style>
<script src="<%=context%>/assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script>
    const form = $("#employee-personal-info");
    let language = '<%=Language%>';
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    let passwordErrorSelector = '<div id="password-error_1" class="custom-error-feedback">' +
        '<%=isLangEnglish ?"Please enter password":"পাসওয়ার্ড প্রবেশ করুন"%>' + '</div>';
    let confirmPasswordErrorSelector = '<div id="confirmPassword-error_1" class="custom-error-feedback">' +
        '<%=isLangEnglish ?"Please enter confirm password":"পাসওয়ার্ড নিশ্চিত করুন"%>' + '</div>';
    let confirmPasswordMatchErrorSelector = '<div id="confirmPassword-error_2" class="custom-error-feedback">' +
        '<%=isLangEnglish ?"Confirm password does not match with password":"নিশ্চিত পাসওয়ার্ড পাসওয়ার্ডের সাথে মিলে নাই"%>' + '</div>';
    const presentAddressEngErrorId = $('#present_address_eng_error_id');
    const presentAddressBngErrorId = $('#present_address_bng_error_id');
    const permanentAddressEngErrorId = $('#permanent_address_eng_error_id');
    const permanentAddressBngErrorId = $('#permanent_address_bng_error_id');

    $('#same-as-permanent').on('change', function () {
        const isChecked = document.getElementById("same-as-permanent").checked;
        if (isChecked) {
            $("#same-as-permanent").val('0');
            $(".present-address").hide();
        } else {
            $(".present-address").show();
            $("#same-as-permanent").val('1');
        }
    });

    $(document).ready(() => {
    	$("#payScale").select2();
        if (document.getElementById('mp_remarks_len') != null && document.getElementById('mp_remark') != null) {
            $('#mp_remarks_len').text($('#mp_remark').val().length + "/1024");
            $('#mp_remark').keyup(() => {
                $('#mp_remarks_len').text($('#mp_remark').val().length + "/1024");
            });
        }
        if (document.getElementById('mp_organization_len') != null && document.getElementById('mp_organization') != null) {
            $('#mp_organization_len').text($('#mp_organization').val().length + "/1024");
            $('#mp_organization').keyup(() => {
                $('#mp_organization_len').text($('#mp_organization').val().length + "/1024");
            });
        }
        $('#password_div').hide();
        $('#confirm_password_div').hide();
        setGeoLocation("<%=employee_recordsDTO.presentAddress%>", 'presentAddressEng_js');
        setGeoLocation("<%=employee_recordsDTO.presentAddressBng%>", 'presentAddressBng_js');
        setGeoLocation("<%=employee_recordsDTO.permanentAddress%>", 'permanentAddressEng_js');
        setGeoLocation("<%=employee_recordsDTO.permanentAddressBng%>", 'permanentAddressBng_js');

        $('#signature_blob').change(() => {
            $('#signature-img').css("display", "none");
        });

        $('#photoImage_blob').change(() => {
            $('#photo-img').css("display", "none");
        });

        $('#alternateImage_blob').change(() => {
            $('#mp-alt-sign').css("display", "none");
        });
        $('#nidFrontPhoto_blob').change(() => {
            $('#nid-front-img').css("display", "none");
        });
        $('#nidBackPhoto_blob').change(() => {
            $('#nid-back-img').css("display", "none");
        });
        $('#mpGazetteDocument_blob').change(() => {
            $('#mp-gazette-doc').css("display", "none");
        });
        $('#electedCount').keydown(function (e) {
            return inputValidationForIntValue(e, $('#electedCount'), 100);
        });

        $('#presentAddressEng_js').on('geolocation.change', () => {
            let str = getGeoLocation('presentAddressEng_js');
            setGeoLocation(str.substr(0, str.lastIndexOf('@@') + 2), 'presentAddressBng_js');
        });
        $('#presentAddressBng_js').on('geolocation.change', () => {
            let str = getGeoLocation('presentAddressBng_js');
            setGeoLocation(str.substr(0, str.lastIndexOf('@@') + 2), 'presentAddressEng_js');
        });
        $('#permanentAddressEng_js').on('geolocation.change', (event, param) => {
            let str = getGeoLocation('permanentAddressEng_js');
            setGeoLocation(str.substr(0, str.lastIndexOf('@@') + 2), 'permanentAddressBng_js');
            <%if(isLangEnglish) {%>
            $('#home_district_text_<%=i%>').val(getDistrictText('permanentAddressEng_js'));
            <%} else {%>
            $('#home_district_text_<%=i%>').val(getDistrictText('permanentAddressBng_js'));
            <%}%>
            $('#homeDistrict').val(param.data.district);
        });
        $('#permanentAddressBng_js').on('geolocation.change', (event, param) => {
            let str = getGeoLocation('permanentAddressBng_js');
            setGeoLocation(str.substr(0, str.lastIndexOf('@@') + 2), 'permanentAddressEng_js');
            <%if(isLangEnglish) {%>
            $('#home_district_text_<%=i%>').val(getDistrictText('permanentAddressEng_js'));
            <%} else {%>
            $('#home_district_text_<%=i%>').val(getDistrictText('permanentAddressBng_js'));
            <%}%>
            $('#homeDistrict').val(param.data.district);
        });

        $('#mpStatus').on('change', function () {
            if ($(this).val() == 1) {
                setDateByTimestampAndId('mp-status-change-date-js', <%=election_wise_mpDTO.mpStatusChangeDate%>);
                $('#mp_status_change_date_div').hide();
            } else {
                setDateByTimestampAndId('mp-status-change-date-js', <%=election_wise_mpDTO.mpStatusChangeDate%>);
                $('#mp_status_change_date_div').show();
            }
        });

        if ('<%=election_wise_mpDTO.mpStatus%>' != '1') {
            $('#mpStatus').prop("disabled", true);
            setDateByTimestampAndId('mp-status-change-date-js', <%=election_wise_mpDTO.mpStatusChangeDate%>);
            $('#mp_status_change_date_div').show();
            //$('#mpStatusChangeDate_div').addClass("disableAllChildInput");
        }

        $('#employmentStatus').on('change', function () {
            if ($(this).val() == 1) {
                setDateByTimestampAndId('employment-status-change-date-js', <%=employee_recordsDTO.employmentStatusChangeDate%>);
                $('#employment_status_change_date_div').hide();
            } else {
                setDateByTimestampAndId('employment-status-change-date-js', <%=employee_recordsDTO.employmentStatusChangeDate%>);
                $('#employment_status_change_date_div').show();
            }

        });

        if ('<%=employee_recordsDTO.employmentStatus%>' != '1' && '<%=employee_recordsDTO.mpStatus%>' != '1') {
            $('#employmentStatus').prop("disabled", true);
            setDateByTimestampAndId('employment-status-change-date-js', <%=employee_recordsDTO.employmentStatusChangeDate%>);
            $('#employment_status_change_date_div').show();
            //$('#employmentStatusChangeDate_div').addClass("disableAllChildInput");
        }


        basicInit();
        initUI();
    });

    function maritalStatusChange() {
        if ($("#maritalStatus_select_0").val() == '1') {
            $('#marriage_date_div').hide();
            $('#dummy_div').show();
            resetDateById('marriage-date-js');
        } else {
            $('#marriage_date_div').show();
            $('#dummy_div').hide();
        }
    }

    function initUI() {
        $('#height').keydown(function (e) {
            return inputValidationForFloatValue(e, $(this), 2, 300);
        });
        maritalStatusChange();
        $('#maritalStatus_select_0').change(function () {
            maritalStatusChange();
        });

        $('#changePassword_checkbox_0').change(function () {
            $('#password_div').toggle();
            $('#confirm_password_div').toggle();
            $('#password').val('');
            $('#confirmPassword').val('');
            if (document.getElementById('confirmPassword-error_1') != null) {
                $('#confirmPassword-error_1').remove();
            }
            if (document.getElementById('confirmPassword-error_2') != null) {
                $('#confirmPassword-error_2').remove();
            }
            if (document.getElementById('password-error_1') != null) {
                $('#password-error_1').remove();
            }
        });

        $.validator.addMethod('nidFrontSelection', function (value, element) {
            if (document.getElementById("nid-front-img") != null)
                return true;
            return value != 0;
        });
        $.validator.addMethod('nidBackSelection', function (value, element) {
            if (document.getElementById("nid-back-img") != null)
                return true;
            return value != 0;
        });
        $.validator.addMethod('passwordDivCheck', function (value, element) {
            if (!$('#changePasswordCheckBox').is(":checked")) {
                return true;
            }
            return value != '';
        });
        $.validator.addMethod('confirmPasswordDivCheck', function (value, element) {
            if (!$('#changePasswordCheckBox').is(":checked")) {
                return true;
            }
            return value != '';
        });

        $("#employee-personal-info").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                nameEng: "required",
                nameBng: "required",
                personalMobile: "required",
                password: {
                    passwordDivCheck: true
                },
                confirmPassword: {
                    confirmPasswordDivCheck: true
                },
            },

            messages: {
                nameEng: '<%=isLangEnglish ? "Please enter name in english":"ইংরেজিতে নাম লিখুন"%>',
                nameBng: '<%=isLangEnglish ? "Please enter name in bangla":"বাংলাতে নাম লিখুন"%>',
                fatherNameEng: '<%=isLangEnglish ?"Please enter father name in english":"ইংরেজিতে পিতার নাম লিখুন"%>',
                fatherNameBng: '<%=isLangEnglish ?"Please enter father name in bangla":"বাংলাতে পিতার নাম লিখুন"%>',
                motherNameEng: '<%=isLangEnglish ?"Please enter mother name in english":"ইংরেজিতে মাতার নাম লিখুন"%>',
                motherNameBng: '<%=isLangEnglish ?"Please enter mother name in bangla":"বাংলাতে মাতার নাম লিখুন"%>',
                nid: '<%=isLangEnglish ?"Please enter national id number":"জাতীয় পরিচ্যপত্র নাম্বার লিখুন"%>',
                personalMobile: '<%=isLangEnglish ?"Please enter mobile number":"মোবাইল নাম্বার লিখুন"%>',
                password: '<%=isLangEnglish ?"Please enter password":"পাসওয়ার্ড প্রবেশ করুন"%>',
                confirmPassword: '<%=isLangEnglish ?"Please enter confirm password":"পাসওয়ার্ড নিশ্চিত করুন"%>',
                religion: '<%=isLangEnglish ?"Please select religion":"ধর্ম বাছাই করুন"%>',
                gender: '<%=isLangEnglish ?"Please select gender":"লিংগ বাছাই করুন"%>',
                nationalityType: '<%=isLangEnglish ?"Please select nationality":"জাতীয়তা বাছাই করুন"%>',
                maritalStatus: '<%=isLangEnglish ?"Please select marital status":"বৈবাহিক অবস্থা বাছাই করুন"%>',
                employmentType: '<%=isLangEnglish ?"Please select employment type":"কর্মকর্তার ধরন বাছাই করুন"%>',
                parliamentNumber: '<%=isLangEnglish ?"Please select parliament number":"সংসদ নম্বর বাছাই করুন"%>',
                electionConstituencyOfMp: '<%=isLangEnglish ?"Please select constituency":"নির্বাচনী এলাকা বাছাই করুন"%>',
                officerType: '<%=isLangEnglish ?"Please Select Officer Type":"অফিসারের ধরন বাছাই করুন"%>',
                employeeClassType: '<%=isLangEnglish ?"Please Select employee Class Type":"কর্মচারীর শ্রেনী বাছাই করুন"%>',
                nid_front_photo: '<%=isLangEnglish ?"Please upload NID front side picture":"জাতীয় পরিচয়পত্রের সামনের অংশ আপলোড করুন"%>',
                nid_back_photo: '<%=isLangEnglish ?"Please upload NID back side picture":"জাতীয় পরিচয়পত্রের পিছনের অংশ আপলোড করুন"%>',
                politicalPartyId: '<%=isLangEnglish ?"Please select political party":"রাজনৈতিক দল বাছাও করুন"%>'
            }
        });

        $('#passport-issue-date-js').on('datepicker.change', () => {
            setMinDateById('passport-expiry-date-js', getDateStringById('passport-issue-date-js'));
        });

        setDateByTimestampAndId('date-of-birth-js', <%=employee_recordsDTO.dateOfBirth%>);
        setDateByTimestampAndId('passport-issue-date-js', <%=employee_recordsDTO.passportIssueDate%>);
        setDateByTimestampAndId('passport-expiry-date-js', <%=employee_recordsDTO.passportExpiryDate%>);
        setDateByTimestampAndId('joining-date-js', <%=employee_recordsDTO.joiningDate%>);
        <%if(employee_recordsDTO.isMP == 2 && loginUserIsAdmin){%>
        setDateByTimestampAndId('lprDate-js', <%=employee_recordsDTO.lprDate%>);
        <%}%>
        setDateByTimestampAndId('marriage-date-js', <%=employee_recordsDTO.marriageDate%>);
        setDateByTimestampAndId('mp-gazette-date-js', <%=election_wise_mpDTO.mpGazetteDate%>);
        setDateByTimestampAndId('oath-date-js', <%=election_wise_mpDTO.oathDate%>);
        setDateByTimestampAndId('election-date-js', <%=election_wise_mpDTO.electionDate%>);
        setDateByTimestampAndId('mp-status-change-date-js', <%=election_wise_mpDTO.mpStatusChangeDate%>);
        setDateByTimestampAndId('employment-status-change-date-js', <%=employee_recordsDTO.employmentStatusChangeDate%>);
    }

    function validateAndSubmit() {
        event.preventDefault();
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let resp = this.responseText + "";
                if (resp.includes("Invalid")) {
                    $('#file-validation-message').text(this.responseText);
                    $("#file-validation-message").addClass("errmsg");
                    $("#file-validation-message").removeClass("validmsg");
                    console.log('invalid.......');
                } else {
                    $('#employee-personal-info').submit();
                }
            }
        };
        xhttp.open("Post", "Employee_recordsServlet?actionType=Employee_recordsServlet&ID=<%=empId%>", true);
        xhttp.send(new FormData(document.getElementById('employee-personal-info')));
    }

    function PreprocessBeforeSubmiting() {
        console.log('PreprocessBeforeSubmiting...');
        let formValidation = form.valid();
        // preprocessCheckBoxBeforeSubmitting("changePassword", 0);
        $('#date-of-birth').val(getDateStringById('date-of-birth-js'));
        $('#passport-issue-date').val(getDateStringById('passport-issue-date-js'));
        $('#passport-expiry-date').val(getDateStringById('passport-expiry-date-js'));
        $('#joining-date').val(getDateStringById('joining-date-js'));
        $('#marriage-date').val(getDateStringById('marriage-date-js'));
        $('#mp-gazette-date').val(getDateStringById('mp-gazette-date-js'));
        $('#oath-date').val(getDateStringById('oath-date-js'));
        $('#election-date').val(getDateStringById('election-date-js'));
        $('#mp-status-change-date').val(getDateStringById('mp-status-change-date-js'));
        $('#employment-status-change-date').val(getDateStringById('employment-status-change-date-js'));

        $('#presentAddressEng').val(getGeoLocation('presentAddressEng_js'));
        $('#presentAddressBng').val(getGeoLocation('presentAddressBng_js'));
        $('#permanentAddressEng').val(getGeoLocation('permanentAddressEng_js'));
        $('#permanentAddressBng').val(getGeoLocation('permanentAddressBng_js'));

        let validationResult;
        validationResult = dateValidator('date-of-birth-js', true, {
            'errorEn': 'Enter a valid date of birth!',
            'errorBn': 'জন্মতারিখ প্রবেশ করান!'
        });
        let temp = dateValidator('joining-date-js', true, {
            'errorEn': 'Enter a valid joining date!',
            'errorBn': 'চাকরিতে যোগদানের তারিখ প্রবেশ করান!'
        });
        if (validationResult) {
            validationResult = temp;
        }
        <%if(employee_recordsDTO.isMP == 2 && loginUserIsAdmin){%>
        if (document.getElementById("extendPRL_checkbox").checked) {
            $('#lprDate').val(getDateStringById('lprDate-js'));
            validationResult = dateValidator('lprDate-js', true, {
                'errorEn': 'Enter a valid LPR date!',
                'errorBn': 'এলপিআর তারিখ প্রবেশ করান!'
            }) && validationResult;
        }
        <%}%>
        if ($('#changePassword_checkbox_0').is(":checked")) {
            if (!$('#password').val()) {
                validationResult = false;
                if (document.getElementById('password-error_1') == null) {
                    $('#passwordDiv').append(passwordErrorSelector);
                }
                if (!$('#confirmPassword').val()) {
                    if (!document.getElementById('confirmPassword-error_1')) {
                        $('#confirmPasswordDiv').append(confirmPasswordErrorSelector);
                    }
                } else if ($('#confirmPassword').val() !== $('#password').val()) {
                    validationResult = false;
                    if (document.getElementById('confirmPassword-error_1') != null) {
                        $('#confirmPassword-error_1').remove();
                    }
                    if (!document.getElementById('confirmPassword-error_2')) {
                        $('#confirmPasswordDiv').append(confirmPasswordMatchErrorSelector);
                    }
                }
            } else {
                if ($('#confirmPassword').val() !== $('#password').val()) {
                    validationResult = false;
                    if (document.getElementById('confirmPassword-error_1') != null) {
                        $('#confirmPassword-error_1').remove();
                    }
                    if (!document.getElementById('confirmPassword-error_2')) {
                        $('#confirmPasswordDiv').append(confirmPasswordMatchErrorSelector);
                    }
                }
            }
        }

        validationResult = addressValidation() && validationResult;
        if (validationResult) {
            processMultipleSelectBoxBeforeSubmit("freedomFighter");
        }
        let allInputs = $("#employee-personal-info :input");
        for (let input of allInputs) {
            // console.log(input.id, input.validity)
            if (input.validity.valid == false) {
                document.getElementById(input.id).focus();
                return false;
            }
        }
        return validationResult && formValidation;
    }

    function addressValidation() {
        let permanentAddressValidationEng = geoLocationValidator('permanentAddressEng_js');
        let permanentAddressValidationBng = geoLocationValidator('permanentAddressBng_js');
        if(document.getElementById("same-as-permanent").checked){
            return permanentAddressValidationEng && permanentAddressValidationBng;
        }
        let presentAddressValidationEng = geoLocationValidator('presentAddressEng_js');
        let presentAddressValidationBng = geoLocationValidator('presentAddressBng_js');
        return presentAddressValidationEng && presentAddressValidationBng && permanentAddressValidationEng && permanentAddressValidationBng;
    }

    function checkValid(id) {
        console.log(id)
        return id.val() === null || id.val() === "undefined" || id.val().length <= 0;
    }

    function submitEmployeePersonalForm() {
        let msg = null;

        if (checkValid($('#nameEng_text'))) {
            msg = isLangEng ? "Please enter name in english" : "ইংরেজিতে নাম লিখুন";
        }
        if (checkValid($('#nameBng_text'))) {
            msg = isLangEng ? "Please enter name in bangla" : "বাংলাতে নাম লিখুন";
        }
        if (checkValid($('#gender_select'))) {
            msg = isLangEng ? "Please select gender" : "লিংগ বাছাই করুন";
        }
        if (checkValid($('#nationalityType_select'))) {
            msg = isLangEng ? "Please select nationality" : "জাতীয়তা বাছাই করুন";
        }
        $('#date-of-birth').val(getDateStringById('date-of-birth-js'));
        let dateOfBirthValid = dateValidator('date-of-birth-js', true, {
            'errorEn': 'Enter a valid date of birth!',
            'errorBn': 'জন্মতারিখ প্রবেশ করান!'
        });
        if (!dateOfBirthValid) {
            msg = isLangEng ? "Enter a valid date of birth!" : "জন্মতারিখ প্রবেশ করান!"
        }

        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            return;
        }


        let url = 'Employee_recordsServlet?actionType=personalInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'


        let data = {
            nameEng: $('#nameEng_text').val(),
            nameBng: $('#nameBng_text').val(),
            fatherNameEng: $('#fatherNameEng_text').val(),
            fatherNameBng: $('#fatherNameBng_text').val(),
            motherNameEng: $('#motherNameEng_text').val(),
            motherNameBng: $('#motherNameBng_text').val(),
            dateOfBirth: $('#date-of-birth').val(),
            gender: $('#gender_select').val(),
            religion: $('#religion_select').val(),
            nationalityType: $('#nationalityType_select').val()
        };
        submitAjaxByData(data, url, 'POST', successResponse);

    }


    function submitEmployeeAddressForm() {
        let msg = null;
        $('#presentAddressEng').val(getGeoLocation('presentAddressEng_js'));
        $('#presentAddressBng').val(getGeoLocation('presentAddressBng_js'));
        $('#permanentAddressEng').val(getGeoLocation('permanentAddressEng_js'));
        $('#permanentAddressBng').val(getGeoLocation('permanentAddressBng_js'));
        let isAddressValid = addressValidation();
        if (!isAddressValid) {
            msg = isLangEng ? "Enter a valid address!" : "সঠিক ঠিকানা প্রবেশ করান!"
        }

        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            return;
        }


        let url = 'Employee_recordsServlet?actionType=addressInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'


        let data = {
            permanentAddressEng: $('#permanentAddressEng').val(),
            permanentAddressBng: $('#permanentAddressBng').val(),
            birthDistrict: $('#birth_district_text_0').val(),
            homeDistrict: $('#homeDistrict').val()
        };
        if(document.getElementById("same-as-permanent").checked){
            data.presentAddressSameAsPermanent = true;
        }else{
            data.presentAddressSameAsPermanent = false;
            data.presentAddressEng= $('#presentAddressEng').val();
            data.presentAddressBng= $('#presentAddressBng').val();
        }


        submitAjaxByData(data, url, 'POST', successResponse);

    }


    function submitEmployeeConfidentialForm() {

        let url = 'Employee_recordsServlet?actionType=confidentialInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'
        $('#passport-issue-date').val(getDateStringById('passport-issue-date-js'));
        $('#passport-expiry-date').val(getDateStringById('passport-expiry-date-js'));

        let data = {
            passportIssueDate: $('#passport-issue-date').val(),
            passportExpiryDate: $('#passport-expiry-date').val(),
            passportNo: $('#passportNo_text').val(),
            bcn: $('#bcn_text').val()
        };
        submitAjaxByData(data, url, 'POST', successResponse);
    }


    function submitEmployeeEmploymentForm() {
        buttonStateChangeFunction(true);
        let msg = null;
        let item = form.serializeArray();
        let form_data = new FormData();

        for (let i = 0; i < item.length; i++) {
            let addData = false;
            console.log(item[i].name, item[i].value);
            switch (item[i].name) {
                case 'employeeClassType':
                    if (!item[i].value) {
                        msg = isLangEng ? "Please Select employee Class Type" : "কর্মচারীর শ্রেনী বাছাই করুন";
                    } else {
                        addData = true;
                    }
                    break;
                case 'officerType':
                    if (!item[i].value) {
                        msg = isLangEng ? "Please Select Officer Type" : "অফিসারের ধরন বাছাই করুন";
                    } else {
                        addData = true;
                    }
                    break;
                case 'employmentType':
                    if (!item[i].value) {
                        msg = isLangEng ? "Please select employment type" : "কর্মকর্তার ধরন বাছাই করুন";
                    } else {
                        addData = true;
                    }
                    break;
                case 'provision_period':
                    addData = true;
                    break;

                case 'employmentStatus':
                    addData = true;
                    break;
                case 'payScale':
                    addData = true;
                    break;

                case 'parliamentNumber':
                    if (!item[i].value) {
                        msg = isLangEng ? "Please select parliament number" : "সংসদ নম্বর বাছাই করুন";
                    } else {
                        addData = true;
                    }
                    break;
                case 'electionConstituencyOfMp':
                    if (!item[i].value) {
                        msg = isLangEng ? "Please select constituency" : "নির্বাচনী এলাকা বাছাই করুন";
                    } else {
                        addData = true;
                    }
                    break;
                case 'politicalPartyId':
                    if (!item[i].value) {
                        msg = isLangEng ? "Please select political party" : "রাজনৈতিক দল বাছাই করুন";
                    } else {
                        addData = true;
                    }
                    break;
                case 'electedCount':
                    addData = true;
                    break;
                case 'mpStatus':
                    addData = true;
                    break;
                case 'mp_remark':
                    addData = true;
                    break;
                case 'mp_organization':
                    addData = true;
                    break;
                case 'ProfessionOfMP':
                    addData = true;
                    break;
                case 'ParliamentAddress':
                    addData = true;
                    break;
                case 'mp_hobbies':
                    addData = true;
                    break;
                case 'parliamentary_designation':
                    addData = true;
                    break;
                case 'otherOfficeDeptType':
                    addData = true;
                    break;
                case 'currentOffice':
                    addData = true;
                    break;
                case 'currentDesignation':
                    addData = true;
                    break;
            }
            if (addData) {
                form_data.append(item[i].name, item[i].value);
            }
        }

        for (let i = 0; i < $('input[type=file]').length; i++) {
            let file = $('input[type=file]')[i];
            switch (file.name) {
                case 'alternateImage':
                    if (file.files.length > 0) {
                        form_data.append(file.name, file.files[0]);
                    }
                    break;
                case 'mp_gazette_doc':
                    if (file.files.length > 0) {
                        form_data.append(file.name, file.files[0]);
                    }
                    break;
            }
        }

        $('#employment-status-change-date').val(getDateStringById('employment-status-change-date-js'));
        form_data.append('employmentStatusChangeDate', $('#employment-status-change-date').val());


        $('#mp-status-change-date').val(getDateStringById('mp-status-change-date-js'));
        form_data.append('mpStatusChangeDate', $('#mp-status-change-date').val());

        $('#election-date').val(getDateStringById('election-date-js'));
        form_data.append('electionDate', $('#election-date').val());

        $('#mp-gazette-date').val(getDateStringById('mp-gazette-date-js'));
        form_data.append('mpGazetteDate', $('#mp-gazette-date').val());

        $('#oath-date').val(getDateStringById('oath-date-js'));
        form_data.append('oathDate', $('#oath-date').val());

        $('#joining-date').val(getDateStringById('joining-date-js'));
        let joiningDateValid = dateValidator('joining-date-js', true, {
            'errorEn': 'Enter a valid joining date!',
            'errorBn': 'চাকরিতে যোগদানের তারিখ প্রবেশ করান!'
        });
        if (!joiningDateValid) {
            msg = isLangEng ? "Enter a valid joining date!" : "চাকরিতে যোগদানের তারিখ প্রবেশ করান!"
        } else {
            form_data.append('joiningDate', $('#joining-date').val());
        }
        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            buttonStateChangeFunction(false);
            return;
        }
        console.log("form_data = " + form_data);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "Employee_recordsServlet?actionType=employmentInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>",
            data: form_data,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
                let data = JSON.parse(response);
                console.log(data);
                if (data.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToast(data.msg, data.msg);
                    buttonStateChangeFunction(false);
                } else if (data.responseCode === 200) {
                    $('#toast_message').css('background-color', '#2e9133');
                    showToast("চাকরি সংক্রান্ত তথ্য সঠিকভাবে সাবমিট হয়েছে", "Employment information submitted successfully");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChangeFunction(false);
                removeCross('select2-selection__choice');
            }
        });

        buttonStateChangeFunction(false);

    }

    function submitEmployeeLPRForm() {

        $('#lprDate').val(getDateStringById('lprDate-js'));
        let url = 'Employee_recordsServlet?actionType=LPRInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'

        let data = {
            lprDate: $('#lprDate').val(),
            extendPRL: $('#extendPRL_checkbox').val(),

        };
        console.log(data);
        submitAjaxByData(data, url, 'POST', successResponse);

    }

    function submitEmployeeContactForm() {
        let msg = null;
        if (checkValid($('#personalMobile_text'))) {
            msg = isLangEng ? "Please enter mobile number" : "মোবাইল নাম্বার লিখুন";
        }
        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            return;
        }
        let url = 'Employee_recordsServlet?actionType=contactInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'
        let data = {
            personalMobile: $('#personalMobile_text').val(),
            alternativeMobile: $('#alternativeMobile_text').val(),
            officePhoneNumber: $('#officePhoneNumber_text').val(),
            officePhoneExtension: $('#officePhoneExtension_text').val(),
            faxNumber: $('#faxNumber_text').val(),
            personalEml: $('#personal_text').val(),
            officialEml: $('#official_text').val(),

        };
        submitAjaxByData(data, url, 'POST', successResponse);

    }

    function submitEmployeeMiscellaneousForm() {
        let url = 'Employee_recordsServlet?actionType=miscellaneousInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'
        let msg = null;
        processMultipleSelectBoxBeforeSubmit("freedomFighter");
        if ($('#jobQuota_select').val() && $('#jobQuota_select').val() == 2) {
            if ($('#hidden_freedomFighter_select_0').val() == '') {
                msg = isLangEng ? "Please enter freedom fighter informtion" : "মুক্তিযোদ্ধা তথ্য লিখুন";
            }
        }

        $('#marriage-date').val(getDateStringById('marriage-date-js'));
        let data = {
            freedomFighter: $('#hidden_freedomFighter_select_0').val(),
            marriageDate: $('#marriage-date').val(),
            maritalStatus: $('#maritalStatus_select_0').val(),
            identificationMark: $('#identification_mark_text').val(),
            height: $('#height').val(),
            bloodGroup: $('#bloodGroup_select').val(),
            jobQuota: $('#jobQuota_select').val(),
            quota_file: $('#quota_file_dropzone_0').val(),
            quota_fileFilesToDelete: $('#quota_fileFilesToDelete_0').val()
        };
        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            return;
        }
        submitAjaxByData(data, url, 'POST', successResponse);
    }

    function submitEmployeeNIDForm() {
        if (checkValid($('#nid_text'))) {
            let msg = isLangEng ? "Please enter nid number":"অনুগ্রহ করে জাতীয় পরিচয়পত্র নাম্বার লিখুন";
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            return;
        }
        buttonStateChangeFunction(true);
        let form_data = new FormData();
        for (let i = 0; i < $('input[type=file]').length; i++) {
            let file = $('input[type=file]')[i];
            console.log(file.name, file.files[0])
            switch (file.name) {
                case 'nid_front_photo':
                    if (file.files.length > 0) {
                        form_data.append(file.name, file.files[0]);
                    }
                    break;
                case 'nid_back_photo':
                    if (file.files.length > 0) {
                        form_data.append(file.name, file.files[0]);
                    }
                    break;
            }
        }
        form_data.append('nid', $('#nid_text').val());

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "Employee_recordsServlet?actionType=nidInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>",
            data: form_data,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
                let data = JSON.parse(response);
                console.log(data);
                if (data.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToast(data.msg, data.msg);
                    buttonStateChangeFunction(false);
                } else if (data.responseCode === 200) {
                    $('#toast_message').css('background-color', '#2e9133');
                    showToast("ছবি ও স্বাক্ষরের তথ্য সঠিকভাবে সাবমিট হয়েছে", "Photo & Signature information submitted successfully");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChangeFunction(false);
                removeCross('select2-selection__choice');
            }
        });

        buttonStateChangeFunction(false);
    }

    function submitEmployeeImageForm() {
        buttonStateChangeFunction(true);
        let form_data = new FormData();

        for (let i = 0; i < $('input[type=file]').length; i++) {
            let file = $('input[type=file]')[i];
            console.log(file.name, file.files[0])
            switch (file.name) {
                case 'photo':
                    if (file.files.length > 0) {
                        form_data.append(file.name, file.files[0]);
                    }
                    break;
                case 'signature':
                    if (file.files.length > 0) {
                        form_data.append(file.name, file.files[0]);
                    }
                    break;
            }
        }
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "Employee_recordsServlet?actionType=imageInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>",
            data: form_data,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
                let data = JSON.parse(response);
                console.log(data);
                if (data.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToast(data.msg, data.msg);
                    buttonStateChangeFunction(false);
                } else if (data.responseCode === 200) {
                    $('#toast_message').css('background-color', '#2e9133');
                    showToast("ছবি ও স্বাক্ষরের তথ্য সঠিকভাবে সাবমিট হয়েছে", "Photo & Signature information submitted successfully");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChangeFunction(false);
                removeCross('select2-selection__choice');
            }
        });

        buttonStateChangeFunction(false);

    }

    function submitEmployeePasswordForm() {

        let msg = null;

        if ($('#changePassword_checkbox_0').is(":checked")) {
            if (!$('#password').val()) {
                if (!$('#confirmPassword').val()) {
                    if (!document.getElementById('confirmPassword-error_1')) {
                        $('#confirmPasswordDiv').append(confirmPasswordErrorSelector);
                        msg = isLangEng ? "Please enter confirm password" : "পাসওয়ার্ড নিশ্চিত করুন";
                    }
                } else if ($('#confirmPassword').val() !== $('#password').val()) {
                    if (document.getElementById('confirmPassword-error_1') != null) {
                        $('#confirmPassword-error_1').remove();
                    }
                    if (!document.getElementById('confirmPassword-error_2')) {
                        $('#confirmPasswordDiv').append(confirmPasswordMatchErrorSelector);
                        msg = isLangEng ? "Confirm password does not match with password" : "নিশ্চিত পাসওয়ার্ড পাসওয়ার্ডের সাথে মিলে নাই";
                    }
                }
                if (document.getElementById('password-error_1') == null) {
                    $('#passwordDiv').append(passwordErrorSelector);
                    msg = isLangEng ? "Please enter password" : "পাসওয়ার্ড প্রবেশ করুন";
                }

            } else {
                if ($('#confirmPassword').val() !== $('#password').val()) {
                    if (document.getElementById('confirmPassword-error_1') != null) {
                        $('#confirmPassword-error_1').remove();
                    }
                    if (!document.getElementById('confirmPassword-error_2')) {
                        $('#confirmPasswordDiv').append(confirmPasswordMatchErrorSelector);
                        msg = isLangEng ? "Confirm password does not match with password" : "নিশ্চিত পাসওয়ার্ড পাসওয়ার্ডের সাথে মিলে নাই";
                    }
                }
            }
        }


        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast(msg, msg);
            return;
        }

        let url = 'Employee_recordsServlet?actionType=passwordInfoEdit&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>'

        let data = {
            changePassword: $('#changePassword_checkbox_0').is(":checked") ? "on" : "off",
            password: $('#password').val(),
            confirmPassword: $('#confirmPassword').val(),

        };
        submitAjaxByData(data, url, 'POST', successResponse);
    }

    function submitEmployeeForm() {
        buttonStateChangeFunction(true);
        if (PreprocessBeforeSubmiting()) {
            let item = form.serializeArray();
            let form_data = new FormData();
            for (let i = 0; i < item.length; i++) {
                form_data.append(item[i].name, item[i].value);
            }

            for (let i = 0; i < $('input[type=file]').length; i++) {
                let file = $('input[type=file]')[i];
                if (file.files.length > 0) {
                    form_data.append(file.name, file.files[0]);
                }
            }
            form_data.append("presentAddressSameAsPermanent",document.getElementById("same-as-permanent").checked);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "Employee_recordsServlet?actionType=<%=actionName%>&identity=<%=empId%>&ID=<%=empId%>&userId=<%=request.getParameter("userId")%>",
                data: form_data,
                processData: false,
                contentType: false,
                success: function (response) {
                    console.log(response);
                    let data = JSON.parse(response);
                    console.log(data);
                    if (data.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToast(data.msg, data.msg);
                        buttonStateChangeFunction(false);
                        removeCross('select2-selection__choice', 1, form_data.get('freedomFighter'));
                    } else if (data.responseCode === 200) {
                        window.location.href = (getContextPath() + data.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChangeFunction(false);
                    removeCross('select2-selection__choice');
                }
            });
        } else {
            buttonStateChangeFunction(false);
        }
    }

    function successResponse(response) {
        if (response.responseCode === 0) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast(response.msg, response.msg);
        } else if (response.responseCode === 200) {
            $('#toast_message').css('background-color', '#2e9133');
            showToast(response.msg, response.msg);
        }
    }

    function removeCross(className, rep, ffi) {
        if (ffi != null) {
            $("#freedomFighter_select_0").select2("destroy");
            $("#freedomFighter_select_0").select2();
        }
        let list = document.getElementsByClassName(className);
        for (let i = 0; i < list.length; i++) {
            console.log(list);
            let element = list[i];
            console.log(element);
            console.log(element.innerText);
            console.log(element.innerText == "×")
            if (element.innerText == "×") {
                element.remove();
            }
        }
        if (rep != null) {
            removeCross(className, null, null);
        }
    }

    function buttonStateChangeFunction(value) {
        // $('#submit-btn').prop('disabled', value);
        // $('#cancel-btn').prop('disabled', value);
        $("button").prop('disabled', value);
        $("input").prop('disabled', value);
    }

    <% if(employee_recordsDTO.isMP == 1){%>

    function changeElection() {
        document.getElementById("election_constituency_of_mp_text").innerHTML = '';
        electionDetailsId = $('#parliament_number_text').val();
        if (!electionDetailsId) {
            return;
        }
        let url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId + "&language=" + language;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                document.getElementById("election_constituency_of_mp_text").innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    <%}else{%>

    function showLprDate() {
        const isChecked = document.getElementById("extendPRL_checkbox").checked;
        if (isChecked) {
            $("#lprDateDiv").show();
            $("#extendPRL_checkbox").val('1');
        } else {
            $("#extendPRL_checkbox").val('0');
            $("#lprDateDiv").hide();
        }
    }

    <%}%>

</script>
<style>
    .disableAllChildInput {
        pointer-events: none;
        opacity: 0.6;
    }

    .input-width93 {
        width: 93%;
        float: left;
    }

    .button-cross-picker {
        height: 34px;
        width: 35px;
        border: 1px solid #c2cad8;
    }

    .red12bold {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 10px;
        color: #F00;
        text-decoration: none;
        font-weight: bold;
    }

    .black12bold {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000000;
        text-decoration: none;
        font-weight: bold;
    }

    .red12 {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #F00;
        text-decoration: none;
    }

    .errmsg {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: #FFF;
        text-decoration: none;
        font-weight: bold;
        padding: 5px;
        background-color: #D20000;
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-right-style: none;
        border-bottom-style: solid;
        border-left-style: none;
        border-top-color: #FFF;
        border-right-color: #FFF;
        border-bottom-color: #FFF;
        border-left-color: #FFF;
    }

    .validmsg {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: #FFF;
        text-decoration: none;
        font-weight: bold;
        padding: 5px;
        background-color: #1aaf4f;
        border-top-width: 1px;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-left-width: 1px;
        border-top-style: solid;
        border-right-style: none;
        border-bottom-style: solid;
        border-left-style: none;
        border-top-color: #FFF;
        border-right-color: #FFF;
        border-bottom-color: #FFF;
        border-left-color: #FFF;
    }

</style>
<script>
    const togglePassword1 = document.querySelector("#togglePassword1");
    const togglePassword2 = document.querySelector("#togglePassword2");
    const password = document.querySelector("#password");
    const confirmPassword = document.querySelector("#confirmPassword");

    togglePassword1.addEventListener("click", function (e) {
        // toggle the type attribute
        const type =
            password.getAttribute("type") === "password" ? "text" : "password";
        password.setAttribute("type", type);
        // toggle the eye / eye slash icon

        if (password.getAttribute("type") === "password") {
            $("#togglePassword1").removeClass("fa fa-eye");
            $("#togglePassword1").addClass("fa fa-eye-slash");
        } else {
            $("#togglePassword1").removeClass("fa fa-eye-slash");
            $("#togglePassword1").addClass("fa fa-eye");
        }
    });

    togglePassword2.addEventListener("click", function (e) {
        // toggle the type attribute
        const type =
            confirmPassword.getAttribute("type") === "password" ? "text" : "password";
        confirmPassword.setAttribute("type", type);
        // toggle the eye / eye slash icon

        if (confirmPassword.getAttribute("type") === "password") {
            $("#togglePassword2").removeClass("fa fa-eye");
            $("#togglePassword2").addClass("fa fa-eye-slash");
        } else {
            $("#togglePassword2").removeClass("fa fa-eye-slash");
            $("#togglePassword2").addClass("fa fa-eye");
        }
    });
</script>