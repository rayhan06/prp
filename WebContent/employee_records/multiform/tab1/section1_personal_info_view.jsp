<%@page import="util.StringUtils" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="election_wise_mp.Election_wise_mpRepository" %>
<%@ page import="election_wise_mp.ElectionWiseMpModel" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="common.NameDTO" %>
<%
    ElectionWiseMpModel electionWiseMpModel = null;
    if (employee_recordsDTO.isMP == 1) {
        electionWiseMpModel = Election_wise_mpRepository.getInstance().getLatestElection_wise_mpDTO(empId);
    }
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>
<div style="text-align: right;margin-top: 10px"><img id="photo-img"></div>
<div class="table-responsive">
    <div class="table table-bordered table-striped">
        <table style="width: 100%">
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO) %>
                </td>
                <td style="width: 35%" id="nameEng"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO) %>
                </td>
                <td style="width: 35%" id="nameBng"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEENG, loginDTO) %>
                </td>
                <td style="width: 35%" id="fatherNameEng"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEBNG, loginDTO) %>
                </td>
                <td style="width: 35%" id="fatherNameBng"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEENG, loginDTO) %>
                </td>
                <td style="width: 35%" id="motherNameEng"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEBNG, loginDTO) %>
                </td>
                <td style="width: 35%" id="motherNameBng"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO) %>
                </td>
                <td style="width: 35%" id="dateOfBirth"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_BIRTH_DISTRICT, loginDTO) %>
                </td>
                <td style="width: 35%" id="birthDistrict"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_ENG, loginDTO) %>
                </td>
                <td style="width: 35%" id="presentAddressEng"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_BNG, loginDTO) %>
                </td>
                <td style="width: 35%" id="presentAddressBng"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_ENG, loginDTO) %>
                </td>
                <td style="width: 35%" id="permanentAddressEng"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_BNG, loginDTO) %>
                </td>
                <td style="width: 35%" id="permanentAddressBng"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_GENDER, loginDTO) %>
                </td>
                <td style="width: 35%" id="gender"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_RELIGION, loginDTO) %>
                </td>
                <td style="width: 35%" id="religion"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_NATIONALITYTYPE, loginDTO) %>
                </td>
                <td style="width: 35%" id="nationality"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_IDENTIFICATION_MARK, loginDTO) %>
                </td>
                <td style="width: 35%" id="identificationMark"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_MARITALSTATUS, loginDTO) %>
                </td>
                <td style="width: 35%" id="maritalStatus"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_MARRIAGEEDATE, loginDTO) %>
                </td>
                <td style="width: 35%" id="marriageDate"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_PASSPORTNO, loginDTO) %>
                </td>
                <td style="width: 35%" id="passportNo"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO) %>
                </td>
                <td style="width: 35%" id="homeDistrict"></td>
            </tr>

            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_PASSPORTISSUEDATE, loginDTO) %>
                </td>
                <td style="width: 35%" id="passportIssueDate"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_PASSPORTEXPIRYDATE, loginDTO) %>
                </td>
                <td style="width: 35%" id="passportExpiryDate"></td>
            </tr>

            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_BCN, loginDTO) %>
                </td>
                <td style="width: 35%" id="bcn"></td>
                <td style="width: 15%"></td>
                <td style="width: 35%"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERSONALMOBILE, loginDTO) %>
                </td>
                <td style="width: 35%" id="personalMobile"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_ALTERNATIVEMOBILE, loginDTO) %>
                </td>
                <td style="width: 35%" id="alternativeMobile"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_NUMBER, loginDTO)%>
                </td>
                <td style="width: 35%" id="officePhoneNumber"></td>
                <td style="width: 15%"><%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_EXTENSION, loginDTO)%>
                </td>
                <td style="width: 35%" id="officePhoneExtension"></td>
            </tr>
            <tr>
                <td style="width: 15%">
                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERSONALEMAIL, loginDTO)%>
                </td>
                <td style="width: 35%" id="personalEml">
                </td>
                <td style="width: 15%">
                    <%=Language.equals("English") ? "Official Email" : "অফিশিয়াল ইমেইল"%>
                </td>
                <td style="width: 35%" id="officialEml">
                </td>
            </tr>
            <tr>
                <td style="width: 15%"><%=LM.getText(LC.HR_MANAGEMENT_OTHERS_FAX_NUMBER, loginDTO)%>
                </td>
                <td style="width: 35%" id="faxNumber"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_HEIGHT, loginDTO) %>
                </td>
                <td style="width: 35%" id="height"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_BLOODGROUP, loginDTO) %>
                </td>
                <td style="width: 35%" id="bloodGroup"></td>
                <td style="width: 15%"><%= LM.getText(LC.HR_MANAGEMENT_OTHERS_FREEDOM_FIGHTER_INFO, loginDTO) %>
                </td>
                <td style="width: 35%" id="freedomFighterInfo"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_JOININGDATE, loginDTO) %>
                </td>
                <td style="width: 35%" id="dateOfJoiningDate"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_LPR_DATE, loginDTO) %>
                </td>
                <td style="width: 35%" id="dateOfLPR"></td>
            </tr>
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_RETIREMENT_DATE, loginDTO) %>
                </td>
                <td style="width: 35%" id="dateOfRetirement"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO) %>
                </td>
                <td style="width: 35%" id="nid"></td>
            </tr>
            <tr>
                <td style="width: 15%;text-align: left"><%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_FRONT_SIDE, loginDTO) %>
                </td>
                <td style="width: 35%"><img id="nid-front-img"></td>
                <td style="width: 15%;text-align: left"><%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_BACK_SIDE, loginDTO) %>
                </td>
                <td style="width: 35%"><img id="nid-back-img"></td>
            </tr>

            <tr class="mp_div" style="display: none">
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_PARLIAMENT_NUMBER, loginDTO) %>
                </td>
                <td style="width: 35%" id="parliamentNumber"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_ELECTION_CONSTITUENCY, loginDTO) %>
                </td>
                <td style="width: 35%" id="electionConstituency"></td>
            </tr>

            <tr class="mp_div" style="display: none">
                <td style="width: 15%"><%= LM.getText(LC.ELECTION_WISE_MP_POLITICAL_PARTY, loginDTO) %>
                </td>
                <td style="width: 35%" id="politicalParty"></td>
                <td style="width: 15%"><%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_MP_PROFESSION, loginDTO) %>
                </td>
                <td style="width: 35%" id="mpProfession"></td>
            </tr>

            <tr class="mp_div" style="display: none">
                <td style="width: 15%"><%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_MP_ADDRESS, loginDTO) %>
                </td>
                <td style="width: 35%" id="mpAddress"></td>
                <td style="width: 15%"><%= LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_MP_ALTERNATE_SIGN, loginDTO) %>
                </td>
                <td style="width: 35%"><img id="mpAltSign"></td>
            </tr>

            <tr class="emp_div">
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO) %>
                </td>
                <td style="width: 35%" id="employeeClass"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO) %>
                </td>
                <td style="width: 35%" id="officerType"></td>
            </tr>

            <tr class="emp_div">
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO) %>
                </td>
                <td style="width: 35%" id="employeeCatagory"></td>
                <td style="width: 15%"></td>
                <td style="width: 35%"></td>
            </tr>
            
            <tr class="other_div" style = "display:none">
                <td style="width: 15%"><%=isLangEng?"Current Office":"বর্তমান অফিস"%>
                </td>
                <td style="width: 35%" id="currentOffice"></td>
                <td style="width: 15%"><%=isLangEng?"Department and Designation":"বিভাগ ও পদবী"%>                
                </td>
                <td style="width: 35%" id="deptAndDesignation"></td>
            </tr>
            
             <tr class="payScale_div" >
                <td style="width: 15%"><%=isLangEng?"Pay Scale":"পে স্কেল"%>
                </td>
                <td id="payScale" colspan = "3"></td>
                
            </tr>
            
            <tr>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_PROVISION_IN_MONTHS, loginDTO) %>
                </td>
                <td style="width: 35%" id="provisionPeriod"></td>
                <td style="width: 15%"><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_PROVISION_END_DATE, loginDTO) %>
                </td>
                <td style="width: 35%" id="provisionEndPeriod"></td>
            </tr>

        </table>
    </div>
</div>
<div class="text-right my-5 mr-2"><img height="100" width="200" style="object-fit: contain" id="signature-img"></div>

<%--Information of Minister of the Parliament--%>
<style>
    tr.spaceUnder > td {
        padding-bottom: 1em;
    }

    #detail-view {
        position: relative;
    }

    #image_MP {
        color: blue;
        width: 100px;
        height: 100px;
        border: 1px solid black;
        position: absolute;
        top: 0px;
        right: 15px;
    }

    #sign_MP {
        width: 120px;
        height: 40px;
        border: 1px solid black;
        position: absolute;;
        bottom: 92px;
        right: 170px;
    }

    #photo-img {
        width: 100px;
        /*position: absolute;*/
        /*right: 0;*/
        /*top: 0;*/
        /*transform: translate(50%,-50%);*/
        /*z-index: 100;*/
    }

    #signature-img {
        width: 100px;
    }

    #nid-front-img {
        width: 100px;
    }

    #nid-back-img {
        width: 100px;
    }

    .spacer {
        display: block;
        height: 80%;
        width: 100%;
        margin: 0 auto;
        content: "";

    }


</style>


<%
    if (employee_recordsDTO.isMP == 1) {
%>

<div class="modal-content viewmodal" style="display: none;">
    <div class="modal-body container">
        <div id="detail-view" class="row div_border office-div">
            <div class="col-md-12">
                <img src='data:image/jpg;base64,<%= (employee_recordsDTO.photo!=null && employee_recordsDTO.photo.length>0)? new String(Base64.encodeBase64(employee_recordsDTO.photo)) : "''"%>'
                     id="image_MP">
                <table class="my-table" style="width: 100%; margin-top: 1%;margin-bottom: 20px;">
                    <tbody>
                    <tr>
                        <td align="center" style="font-size: x-large;"><u>মাননীয় সংসদ-সদস্যগণের ব্যক্তিগত তথ্য</u></td>
                    </tr>
                    </tbody>
                </table>
                <table class="my-table" align="center">
                    <tbody>
                    <tr>
                        <td style="width: 10px; margin-right: 20px;">১ ।</td>
                        <td style="width: 40%;">নাম <span style="padding-left: 20px;"></span> (ক)<span
                                style="padding-left: 5px;">বাংলায় (গেজেট অনুসারে)</span></td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: top;">:</td>
                        <td style="vertical-align: bottom;"><%=employee_recordsDTO.nameBng%>
                        </td>
                    </tr>


                    <tr class="spaceUnder">
                        <td style="width: 50px; margin-right: 30px;">&nbsp;</td>
                        <td style="width: 40%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                                style="padding-left: 20px;"></span>(খ)<span style="padding-left: 5px;"> ইংরেজিতে (বড় অক্ষরে)</span>
                        </td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: top;">:</td>
                        <td style="vertical-align: bottom;"><%=employee_recordsDTO.nameEng.toUpperCase()%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>


                    <tr>
                        <td style="width: 50px; margin-right: 30px;">২।</td>
                        <td style="width: 40%;">(ক) নির্বাচনী এলাকা(নাম ও নম্বরসহ)</td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: top;">:</td>
                        <td style="vertical-align: bottom;"><%=electionWiseMpModel != null && electionWiseMpModel.electionConstituencyDTO != null ?
                                electionWiseMpModel.electionConstituencyDTO.constituencyNameBn : ""%>
                        </td>
                    </tr>


                    <tr class="spaceUnder">
                        <td style="width: 50px; margin-right: 30px;">&nbsp;</td>
                        <td style="width: 40%;">(খ)<span style="padding-left: 20px;">দলগত পরিচয়</span></td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: top;">:</td>
                        <td style="vertical-align: bottom;"><%=electionWiseMpModel != null && electionWiseMpModel.politicalPartyDTO != null ?
                                electionWiseMpModel.politicalPartyDTO.nameBn : ""%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width: 50px; margin-right: 30px;">৩ ।</td>
                        <td style="width: 40%;">বর্তমান ঠিকানা(ঢাকাস্থ ঠিকানা)</td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: bottom;"></td>
                        <td></td>
                    </tr>


                    <tr>
                        <td style="width: 50px; margin-right: 30px;">&nbsp;</td>
                        <td style="width: 40%;"><span style="padding-left: 30px;">&nbsp;&nbsp;</span>(ক)<span
                                style="padding-left: 20px;">বাংলায়</span></td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: top; ">:</td>
                        <td style="vertical-align: bottom;"><%=GeoLocationDAO2.parseAddress(employee_recordsDTO.presentAddressBng)%>
                        </td>
                    </tr>


                    <tr class="spaceUnder">
                        <td style="width: 50px; margin-right: 30px;">&nbsp;</td>
                        <td style="width: 40%;"><span style="padding-left: 30px;">&nbsp;&nbsp;</span>(খ) <span
                                style="padding-left: 20px;">ইংরেজিতে(বড় অক্ষরে)</span></td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: top;">:</td>
                        <td style="vertical-align: bottom;"><%=(GeoLocationDAO2.parseAddress(employee_recordsDTO.presentAddress)).toUpperCase()%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>


                    <tr>
                        <td style="width: 50px; margin-right: 30px;">৪ ।</td>
                        <td style="width: 40%;">স্থায়ী ঠিকানা(টেলিফোন নাম্বারসহ)</td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: bottom;">

                        </td>
                        <td></td>
                    </tr>


                    <tr>
                        <td style="width: 50px; margin-right: 30px;">&nbsp;</td>
                        <td style="width: 40%;"><span style="padding-left: 30px;">&nbsp;&nbsp;</span>(ক)<span
                                style="padding-left: 20px;">বাংলায়</span></td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: top;">:</td>
                        <td style="vertical-align: bottom;"><%
                            StringBuilder builder = new StringBuilder(GeoLocationDAO2.parseAddress(employee_recordsDTO.permanentAddressBng));
                            builder.append("<br>মোবাইল : ");
                            builder.append(employee_recordsDTO.personalMobile != null && employee_recordsDTO.personalMobile.length() > 0 ?
                                    (StringUtils.convertToBanNumber(employee_recordsDTO.personalMobile)).substring(2) : "");
                        %>
                            <%=builder.toString()%>
                        </td>
                    </tr>


                    <tr class="spaceUnder">
                        <td style="width: 50px; margin-right: 30px;">&nbsp;</td>
                        <td style="width: 40%;"><span style="padding-left: 30px;">&nbsp;&nbsp</span>(খ) <span
                                style="padding-left: 20px;">ইংরেজিতে(বড় অক্ষরে)</span></td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: top;">:</td>
                        <td style="vertical-align: bottom;">
                            <%
                                builder = new StringBuilder((GeoLocationDAO2.parseAddress(employee_recordsDTO.permanentAddress)).toUpperCase());
                                builder.append("<br>Mobile No : ");
                                builder.append(employee_recordsDTO.personalMobile != null && employee_recordsDTO.personalMobile.length() > 0 ?
                                        (employee_recordsDTO.personalMobile).substring(2) : "");
                            %>
                            <%=builder.toString()%>
                        </td>
                        <td>&nbsp;</td>
                    </tr>


                    <tr class="spaceUnder">
                        <td style="width: 50px; margin-right: 30px;">৫ ।</td>
                        <td style="width: 40%;">ধর্ম</td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;">
                            <%
                                NameDTO religionDTO = ReligionRepository.getInstance().getDTOByID(employee_recordsDTO.religion); %>
                            <%=religionDTO == null ? "" : (Language.equals("English") ? religionDTO.nameEn : religionDTO.nameBn)%>
                        </td>
                    </tr>

                    <tr class="spaceUnder">
                        <td style="width: 50px; margin-right: 30px;">৬ ।</td>
                        <td style="width: 40%;">পেশা(পূর্বের ও বর্তমান)</td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"><%=employee_recordsDTO.professionOfMP%>
                        </td>
                    </tr>

                    <tr class="spaceUnder">
                        <td style="width: 50px; margin-right: 30px; vertical-align: top;">৭ ।</td>
                        <td style="width: 40%;">টেলিফোন ও মোবাইল নম্বর</td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"><%=employee_recordsDTO.personalMobile != null && employee_recordsDTO.personalMobile.length() > 0 ?
                                util.StringUtils.convertToBanNumber(employee_recordsDTO.personalMobile) : ""%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50px; margin-right: 30px;">৮ ।</td>
                        <td style="width: 40%;">ইমেইল এড্রেস(যদি থাকে)</td>
                        <td style="width: 40px; margin-right: 30px; vertical-align: bottom;">:</td>
                        <td style="vertical-align: bottom;"><%=employee_recordsDTO.personalEml%>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <br/>
                <br/>

                <table class="my-table" align="right" style="margin-right: 25%; margin-top: 2%;">
                    <tbody>
                    <tr>
                        <td style="width: 60%;">&nbsp;</td>
                        <td style="width: 20%; text-align: right;"></td>
                    </tr>
                    <tr>
                        <td style="width: 60%;"><img
                                src='image/jpg;base64,<%= (employee_recordsDTO.signature!=null && employee_recordsDTO.signature.length>0) ? new String(Base64.encodeBase64(employee_recordsDTO.signature)):""%>'
                                id="sign_MP"></td>
                        <td style="width: 20%; text-align: right;">স্বাক্ষর ও তারিখ</td>
                    </tr>
                    </tbody>
                </table>
                <br/>
                <br/>


                <table class="my-table" align="left" style="margin-left: 10%; margin-top: 2%; margin-bottom: 2%;">
                    <tbody>
                    <tr>
                        <td style="width: 100%;">বিঃদ্রঃ <span style="padding-left: 10px;"></span>(ক) উপর্যুক্ত তথ্যাদি
                            টেলিফোন নির্দেশিকায় ব্যবহৃত হবে।
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;"><span style="padding-left: 30px;">&nbsp;&nbsp;&nbsp;</span>(খ) এ ফরমটি
                            পূরণপূর্বক শপথ গ্রহনের অব্যবহিত পরই সেবা শাখায় জমা দিতে অনুরোধ জানান হলো।
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<%
    }
%>

<script>
    $('#file-download-btn').click(function () {
        downloadPdf('MP_detail.pdf', "detail-view");
    });

</script>