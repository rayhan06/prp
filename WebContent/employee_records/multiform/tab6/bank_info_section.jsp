<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equals("English");

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long empId = Long.parseLong( ID );
%>

<div class="form-body">
    <%@include file="/employee_bank_information/employee_bank_info_view_page.jsp"%>
</div>