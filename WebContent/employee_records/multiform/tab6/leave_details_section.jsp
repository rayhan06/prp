<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equals("English");
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong( ID );
    long empId = id;
%>

<div class="form-body">
    <%@include file="/employee_leave_details/employee_leave_details_view_page.jsp"%>
</div>