<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<script>

    var language ="<%=Language%>";
    var curYear="<%=currentYear%>";
    let degreeExam = <%=degreeExamType%>;
    const psc_jsc_pattern =language=='English'? /psc|jsc|Please Select/i : /পিএসসি|জেএসসি|অনুগ্রহ করে নির্বাচন করুন/
    const grade = language=='English'?'Grade':'গ্রেড'
    const other = language=='English'?'Other':'অন্যান্য'
    const please_select = language=='English'?'Please Select':'অনুগ্রহ করে নির্বাচন করুন'

    $(document).ready( function(){

        dateTimeInit("<%=Language%>");
        initUi();

        $.validator.addMethod('passingYearValidDuration',function(value,element){
            value = parseInt(value);
            return value>=1900 && value<=parseInt(curYear);
        });
        $.validator.addMethod('educationLevelSelection',function(value,element){
            return value!=0;
        });
        $.validator.addMethod('degreeTypeSelection',function(value,element){
            return value!=0;
        });
        $.validator.addMethod('resultTypeSelection',function(value,element){
            return value!=0;
        });
        $.validator.addMethod('gpaSelection',function(value,element){
            return $('#resultExamType_select option:selected').text() == grade && value!=0;
        });
        $.validator.addMethod('boardSelection',function(value,element){
            return value!=0;
        });

        $( "#bigform" ).validate({

            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                nameEng: "required",
                personalMobile: "required",
                personalEmail: "required"
            },
            <% if(  Language.equalsIgnoreCase( "bangla" ) ) { %>

            messages: {
                nameEng: "অনুগ্রহ করে ব্যাক্তির নাম প্রদান করুন",
                personalMobile:"অনুগ্রহ করে ব্যাক্তির মোবাইল নম্বর প্রদান করুন, ৮৮০ দিয়ে শুরু হতে হবে।",
                personalEmail: "অনুগ্রহ করে একটি ভ্যালিড ইমেইল এড্রেস প্রদান করুন"
            }
            <%}else{%>

            messages: {
                nameEng: "Please give person name",
                personalMobile:"Please give person's phone number, Must start with 880",
                personalEmail: "Please give a valid email address"
            }
            <%}%>
        });

        $("#emp_edu_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                institutionName: "required",
                boardType: {
                    required :true,
                    boardSelection : true
                },
                major : {
                    required : function(){
                        if($('#major_div').is(":visible")){
                            return true;
                        }
                        return false;
                    }
                },
                marksPercentage : {
                    required : {
                        function(){
                            if($('#resultExamType_select option:selected').text() != grade){
                                let marks = $('#marksPercentage_text').val();
                                console.log("marks : "+marks);
                                if(marks){
                                    return false;
                                }
                                return true;
                            }
                            return false;
                        }
                    }
                },
                cgpaNumber : {
                    required : {
                        function(){
                            if($('#resultExamType_select option:selected').text() != grade){
                                let cgpa = $('#cgpaNumber_text').val();
                                if(cgpa){
                                    return false;
                                }
                                return true;
                            }
                            return false;
                        }
                    }
                },
                gradePointCat : {
                    required : function(){
                        return $('#resultExamType_select option:selected').text() == grade;
                    },
                    gpaSelection : true
                },
                yearOfPassingNumber : {
                    required :true,
                    passingYearValidDuration : true
                },
                durationYears : {
                    required : {
                        function(){
                            if($('#durationYears_number').val()){
                                return false;
                            }
                            return true;
                        }
                    }
                },
                educationLevelType : {
                    required : true,
                    educationLevelSelection : true
                },
                degreeExamType : {
                    required : true,
                    degreeTypeSelection : true
                },
                resultExamType : {
                    required : true,
                    resultTypeSelection : true
                },
            },

            <% if(  Language.equalsIgnoreCase( "bangla" ) ) { %>

            messages: {
                institutionName: "অনুগ্রহ করে শিক্ষাপ্রতিষ্ঠানের নাম লিখুন",
                major:"অনুগ্রহ করে মেজর / গ্রুপ লিখুন",
                cgpaNumber : "অনুগ্রহ করে জিপিএ/সিজিপিএ লিখুন",
                marksPercentage : "অনুগ্রহ করে মার্ক(%) লিখুন",
                yearOfPassingNumber : "অনুগ্রহ করে পাশের সন লিখুন",
                durationYears : "অনুগ্রহ করে সময়কাল লিখুন",
                educationLevelType : "অনুগ্রহ করে শিক্ষা পর্যায় নির্বাচন করুন",
                degreeExamType : "অনুগ্রহ করে পরীক্ষা/ডিগ্রীর নাম নির্বাচন করুন",
                resultExamType : "অনুগ্রহ করে পরীক্ষার ফলাফল নির্বাচন করুন",
                gradePointCat : "অনুগ্রহ করে গ্রেড স্কেল নির্বাচন করুন",
                boardType : "অনুগ্রহ করে বোর্ড নির্বাচন করুন"
            }
            <%}else{%>

            messages: {
                institutionName: "Please enter institute name",
                major:"Please enter major subject",
                cgpaNumber : "Please enter GPA/CGPA",
                marksPercentage : "Please enter marks percentage",
                yearOfPassingNumber : "Please enter valid passing year between 1900 to "+curYear,
                durationYears : "Please enter duration in years",
                educationLevelType : "Please select a education level",
                degreeExamType : "Please select a degree type of exam",
                resultExamType : "Please select a result type of exam",
                gradePointCat : "Please select a grade point scale",
                boardType : "Please select a board"
            }
            <%}%>
        });

        $("#employee-family-info").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                nationalIdNo: {
                    digits: true
                },
                lastNameEn: "required",
                lastNameBn: "required",
                relationType: "required",
                genderCat: "required",
                mobileNumber1: "required",
                mobileNumber2: {
                    digits: true
                }
            },
            <% if( Language.equalsIgnoreCase( "bangla" ) ) { %>
            messages: {
                nationalIdNo: {
                    digits: "অনুগ্রহ করে নাম্বার প্রদান করুন"
                },
                lastNameEn: "অনুগ্রহ করে নাম প্রদান করুন ",
                lastNameBn: "অনুগ্রহ করে নাম প্রদান করুন",
                relationType: "অনুগ্রহ করে সম্পর্কের ধরন প্রদান করুন",
                genderCat: "অনুগ্রহ করে লিঙ্গের ধরন প্রদান করুন",
                mobileNumber1: "অনুগ্রহ করে মোবাইল নাম্বার প্রদান করুন",
                mobileNumber2: {
                    digits: "অনুগ্রহ করে নাম্বার প্রদান করুন"
                }
            }
            <%} else { %>
            messages: {
                nationalIdNo: {
                    digits: "Please enter digits only"
                },
                lastNameEn: "Please enter lastname",
                lastNameBn: "Please enter lastname",
                relationType: "Please enter relation type",
                genderCat: "Please enter gender",
                mobileNumber1: "Please enter mobile number",
                mobileNumber2: {
                    digits: "Please enter digits only"
                }
            }
            <%}%>
        });

    });

    function initUi(){

        degreeExamOther('degreeExamType_select','Other','otherDegreeExam_div');
        degreeExamOther('resultExamType_select','Grade','grade_div','marksPercentage_div');
        if($('#educationLevelType_select').val()){
            fetchDegree($('#educationLevelType_select').val(),degreeExam);
        }
        $('#educationLevelType_select').change(function(){
            fetchDegree($(this).val(),null);
            showOrHideMajor();
        });
        $('#degreeExamType_select').change(function(){
            degreeExamOther('degreeExamType_select','Other','otherDegreeExam_div');
        });

        $('#resultExamType_select').change(function(){
            degreeExamOther('resultExamType_select','Grade','grade_div','marksPercentage_div');
            $('#marksPercentage_text').val("");
            $('#cgpaNumber_text').val("");
            $("#gradePointCat_category").val($("#gradePointCat_category option:first").val());
        });

        $('#marksPercentage_text').keydown(function(e){
            return inputValidationForFloatValue(e,$(this),2,100);
        });

        $('#yearOfPassingNumber_number').keydown(function(e){
            return inputValidationForIntValue(e,$(this),curYear);
        });

        $('#durationYears_number').keydown(function(e){
            return inputValidationForIntValue(e,$(this),9);
        });

        $('#gradePointCat_category').change(function(){
            $('#cgpaNumber_text').val("");
        });

        $('#cgpaNumber_text').keydown(function(e){
            return inputValidationForFloatValue(e,$(this),2,$("#gradePointCat_category option:selected").text());
        });
        showOrHideMajor();
    }

    function showOrHideMajor(){
        let text = $('#educationLevelType_select option:selected').text();
        if(psc_jsc_pattern.test(text)){
            $('#major_div').hide();
            $('#major_text').val("");
        }else{
            $('#major_div').show();
        }
    }

    function degreeExamOther(selecterId,selecterVal,showId,hideId){
        let x = '#'+selecterId+' option:selected';
        if($(x).text() == selecterVal){
            $('#'+showId).show();
            if(hideId){
                $('#'+hideId).hide();
            }
        }else{
            $('#'+showId).hide();
            if(hideId){
                $('#'+hideId).show();
            }
        }
    }

    function fetchDegree(educationLevelId,selectedDegreeValue) {
        let url = "employee_education_Servlet?a=getDegrees&education_level_id=" + educationLevelId;
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                $('#degreeExamType_select').html("");
                const response = JSON.parse(fetchedData).payload;
                if (response && response.length > 0) {
                    $('#degreeExamType_div').show();
                    $('#otherDegreeExam_div').hide();
                    let label = "<%=(actionName.equals("edit"))
					? (LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_OTHERDEGREEEXAM, loginDTO))
					: (LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_OTHERDEGREEEXAM, loginDTO))%>";
                    $('#otherDegreeExam_label').text(label);
                    $('#otherDegreeExam_label').append('<span class="required"> *</span>');
                    for (let x in response) {
                        let str;
                        if (language == 'English') {
                            str = response[x].nameEn;
                        } else {
                            str = response[x].nameBn;
                        }
                        let o;
                        if (selectedDegreeValue != null) {
                            if (selectedDegreeValue == response[x].iD) {
                                o = new Option(str, response[x].iD, false, true);
                            } else {
                                o = new Option(str, response[x].iD);
                            }
                        } else {
                            o = new Option(str, response[x].iD);
                        }
                        $(o).html(str);
                        $('#degreeExamType_select').append(o);
                    }
                } else {
                    $('#degreeExamType_div').hide();
                    $('#otherDegreeExam_div').show();
                    let label = "<%=(actionName.equals("edit"))
				? (LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO))
						: (LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_DEGREEEXAMTYPE, loginDTO))%>";
                    $('#otherDegreeExam_label').text(label);
                    $('#otherDegreeExam_label').append('<span class="required"> *</span>');
                }
            },
            error: function (error) {
                console.log(error);
            }
        });

    }

    function educationInfoOnSubmit( row ){

        $("#emp_edu_form").validate();
        preprocessCheckBoxBeforeSubmitting('isForeign', row);
    }

    function familyMemberOnSubmit(row) {

        let employeeRecordAdded = <%=employee_recordsDTO.iD > 0%>;

        if( !employeeRecordAdded ){

            toastr.error( "Please, add Personal details first in Personal tab" );
            return false;
        }

        $("#employee-family-info").validate();

        return preprocessGeolocationBeforeSubmitting('presentAddress', row, false) &&
        preprocessGeolocationBeforeSubmitting('permanentAddress', row, false);

        // return true;
    }

    function personalDetailOnSubmit( row ) {

        $("#bigform").validate();

        try {

            var empty_fields = "";
            var i = 0;
            if (!document.getElementById('nameEng_text_' + row).checkValidity()) {
                empty_fields += "'nameEng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('nameBng_text_' + row).checkValidity()) {
                empty_fields += "'nameBng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('fatherNameEng_text_' + row).checkValidity()) {
                empty_fields += "'fatherNameEng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('fatherNameBng_text_' + row).checkValidity()) {
                empty_fields += "'fatherNameBng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('motherNameEng_text_' + row).checkValidity()) {
                empty_fields += "'motherNameEng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('motherNameBng_text_' + row).checkValidity()) {
                empty_fields += "'motherNameBng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('dateOfBirth_date_' + row).checkValidity()) {
                empty_fields += "'dateOfBirth'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('nid_text_' + row).checkValidity()) {
                empty_fields += "'nid'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('bcn_text_' + row).checkValidity()) {
                empty_fields += "'bcn'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('ppn_text_' + row).checkValidity()) {
                empty_fields += "'ppn'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('gender_select_' + row).checkValidity()) {
                empty_fields += "'gender'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('religion_select_' + row).checkValidity()) {
                empty_fields += "'religion'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('bloodGroup_select_' + row).checkValidity()) {
                empty_fields += "'bloodGroup'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('maritalStatus_select_' + row).checkValidity()) {
                empty_fields += "'maritalStatus'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }

            const p1 = document.getElementById('password').value;
            const p2 = document.getElementById('confirmPassword').value;
            const id = document.getElementById('iD_hidden_' + row).value;

            var actionName = '<%=actionName%>';

            if( actionName == 'edit' ){

                if($("#updatePassword").prop("checked") == true){

                    if (!checkPassword(p1, p2)) {
                        return false;
                    }
                }
            }
            else{

                if (!checkPassword(p1, p2)) {
                    return false;
                }
            }

            console.log("found date = " + document.getElementById('dateOfBirth_date_Date_' + row).value);
            document.getElementById('dateOfBirth_date_' + row).value = new Date(document.getElementById('dateOfBirth_date_Date_' + row).value).getTime();
            if (document.getElementById('status_checkbox_' + row).checked) {
                document.getElementById('status_checkbox_' + row).value = "true";
            }
            else {
                document.getElementById('status_checkbox_' + row).value = "false";
            }

            return preprocessGeolocationBeforeSubmitting('presentAddress', row, false) &&
            preprocessGeolocationBeforeSubmitting('permanentAddress', row, false);

        } catch (e) {
            showError(fill_all_input_bng, fill_all_input_eng);
            return false;
        }
    }

</script>