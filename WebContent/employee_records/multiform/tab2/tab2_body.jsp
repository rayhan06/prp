<%
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>

<style>
    #aspect-content {
        margin: 50px 0 0;
        font-family: Roboto, SolaimanLipi;
    }

    #aspect-content * {
        box-sizing: border-box;
    }

    .aspect-tab {
        position: relative;
        width: 100%;
        margin: 0 auto 10px;
        border-radius: 8px;
        background-color: #fff;
        box-shadow: 0 0 0 1px #ececec;
        opacity: 1;
        transition: box-shadow .2s, opacity .4s;
    }

    .aspect-tab:hover {
        box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.11);
    }

    .aspect-input {
        display: none;
    }

    .aspect-input:checked ~ .aspect-content + .aspect-tab-content {
        max-height: 5000px;
    }

    .aspect-input:checked ~ .aspect-content:after {
        transform: rotate(0);
    }

    .aspect-label {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        max-height: 80px;
        width: 100%;
        margin: 0;
        padding: 0;
        font-size: 0;
        z-index: 1;
        cursor: pointer;
    }

    .aspect-label:hover ~ .aspect-content:after {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTI0IDI0SDBWMGgyNHoiIG9wYWNpdHk9Ii44NyIvPgogICAgICAgIDxwYXRoIGZpbGw9IiM1NTZBRUEiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE1Ljg4IDE1LjI5TDEyIDExLjQxbC0zLjg4IDMuODhhLjk5Ni45OTYgMCAxIDEtMS40MS0xLjQxbDQuNTktNC41OWEuOTk2Ljk5NiAwIDAgMSAxLjQxIDBsNC41OSA0LjU5Yy4zOS4zOS4zOSAxLjAyIDAgMS40MS0uMzkuMzgtMS4wMy4zOS0xLjQyIDB6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=");
    }

    .aspect-content {
        position: relative;
        display: block;
        height: 80px;
        margin: 0;
        padding: 0 87px 0 30px;
        font-size: 0;
        white-space: nowrap;
        cursor: pointer;
        user-select: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
    }

    .aspect-content:before, .aspect-content:after {
        content: '';
        display: inline-block;
        vertical-align: middle;
    }

    .aspect-content:before {
        height: 100%;
    }

    .aspect-content:after {
        position: absolute;
        width: 24px;
        height: 100%;
        right: 30px;
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTI0IDI0SDBWMGgyNHoiIG9wYWNpdHk9Ii44NyIvPgogICAgICAgIDxwYXRoIGZpbGw9IiNBOUFDQUYiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE1Ljg4IDE1LjI5TDEyIDExLjQxbC0zLjg4IDMuODhhLjk5Ni45OTYgMCAxIDEtMS40MS0xLjQxbDQuNTktNC41OWEuOTk2Ljk5NiAwIDAgMSAxLjQxIDBsNC41OSA0LjU5Yy4zOS4zOS4zOSAxLjAyIDAgMS40MS0uMzkuMzgtMS4wMy4zOS0xLjQyIDB6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=");
        background-repeat: no-repeat;
        background-position: center;
        transform: rotate(180deg);
    }

    .aspect-name {
        display: inline-block;
        width: 75%;
        margin-left: 10px;
        font-weight: 400;
        white-space: normal;
        text-align: left;
        vertical-align: middle;
    }

    .aspect-stat {
        width: 40%;
        text-align: right;
    }

    .all-opinions,
    .aspect-name {
        font-size: 1.5rem;
        line-height: 22px;
        font-family: Roboto, SolaimanLipi;
    }

    .all-opinions {
        color: #5d5d5d;
        text-align: left;
    }

    .aspect-content + .aspect-tab-content {
        max-height: 0;
        overflow: hidden;
        transition: max-height 1s ease-in-out;
    }

    .aspect-content > div,
    .aspect-stat > div {
        display: inline-block;
    }

    .aspect-content > div {
        vertical-align: middle;
    }

    .positive-count,
    .negative-count,
    .neutral-count {
        display: inline-block;
        margin: 0 0 0 20px;
        padding-left: 26px;
        background-repeat: no-repeat;
        font-size: 13px;
        line-height: 20px;
        color: #363636;
    }

    .positive-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiM3RUQzMjEiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNSAxMS40MjdhNSA1IDAgMCAwIDEwIDAgLjcxNC43MTQgMCAxIDAtMS40MjkgMCAzLjU3MSAzLjU3MSAwIDAgMS03LjE0MiAwIC43MTQuNzE0IDAgMSAwLTEuNDI5IDB6bTEuMDcxLTVhMS4wNzEgMS4wNzEgMCAxIDAgMCAyLjE0MyAxLjA3MSAxLjA3MSAwIDAgMCAwLTIuMTQzem03Ljg1OCAwYTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDMgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3oiLz4KPC9zdmc+Cg==");
    }

    .negative-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiNGRjZFMDAiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNSAxNC45OThhLjcxNC43MTQgMCAwIDAgMS40MjkgMCAzLjU3MSAzLjU3MSAwIDAgMSA3LjE0MiAwIC43MTQuNzE0IDAgMSAwIDEuNDI5IDAgNSA1IDAgMSAwLTEwIDB6bTEuMDcxLTguNTdhMS4wNzEgMS4wNzEgMCAxIDAgMCAyLjE0MiAxLjA3MSAxLjA3MSAwIDAgMCAwLTIuMTQzem03Ljg1OCAwYTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDIgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3oiLz4KPC9zdmc+Cg==");
    }

    .neutral-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiNCQUMyRDYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNS43MTQgMTEuNDI3YS43MTQuNzE0IDAgMSAwIDAgMS40MjloOC41NzJhLjcxNC43MTQgMCAxIDAgMC0xLjQyOUg1LjcxNHptLjM1Ny01YTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDMgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3ptNy44NTggMGExLjA3MSAxLjA3MSAwIDEgMCAwIDIuMTQzIDEuMDcxIDEuMDcxIDAgMCAwIDAtMi4xNDN6Ii8+Cjwvc3ZnPgo=");
    }

    .aspect-info {
        width: 60%;
        white-space: nowrap;
        font-size: 0;
    }

    .aspect-info:before {
        content: '';
        display: inline-block;
        height: 44px;
        vertical-align: middle;
    }

    .chart-pie {
        position: relative;
        display: inline-block;
        height: 44px;
        width: 44px;
        border-radius: 50%;
        background-color: #e4e4e4;
        vertical-align: middle;
    }

    .chart-pie:after {
        content: '';
        display: block;
        position: absolute;
        height: 40px;
        width: 40px;
        top: 2px;
        left: 2px;
        border-radius: 50%;
        background-color: #fff;
    }

    .chart-pie-count {
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        font-size: 14px;
        font-weight: 500;
        line-height: 44px;
        color: #242a32;
        text-align: center;
        z-index: 1;
    }

    .chart-pie > div {
        clip: rect(0, 44px, 44px, 22px);
    }

    .chart-pie > div,
    .chart-pie.over50 .first-fill {
        position: absolute;
        height: 44px;
        width: 44px;
        border-radius: 50%;
    }

    .chart-pie.over50 > div {
        clip: rect(auto, auto, auto, auto);
    }

    .chart-pie.over50 .first-fill {
        clip: rect(0, 44px, 44px, 22px);
    }

    .chart-pie:not(.over50) .first-fill {
        display: none;
    }

    .second-fill {
        position: absolute;
        clip: rect(0, 22px, 44px, 0);
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border-width: 3px;
        border-style: solid;
        box-sizing: border-box;
    }

    .chart-pie.positive .first-fill {
        background-color: #82d428;
    }

    .chart-pie.positive .second-fill {
        border-color: #82d428;
    }

    .chart-pie.negative .first-fill {
        background-color: #ff6e00;
    }

    .chart-pie.negative .second-fill {
        border-color: #ff6e00;
    }

    .aspect-tab-content {
        background-color: #f9f9f9;
        /*font-size: 0;*/
        text-align: justify;
    }

    .sentiment-wrapper {
        /*padding: 24px 30px 30px;*/
    }

    .sentiment-wrapper > div {
        display: inline-block;
        width: 100%;
    }

    .sentiment-wrapper > div > div {
        width: 100%;
        padding: 0px 16px;
    }

    .opinion-header {
        position: relative;
        width: 100%;
        margin: 0 0 24px;
        font-size: 13px;
        font-weight: 500;
        line-height: 20px;
        color: #242a32;
        text-transform: capitalize;
    }

    .opinion-header > span:nth-child(2) {
        position: absolute;
        right: 0;
    }

    .opinion-header + div > span {
        font-size: 13px;
        font-weight: 400;
        line-height: 22px;
        color: #363636;
    }

    @media screen and (max-width: 800px) {
        .aspect-label {
            max-height: 102px;
        }

        .aspect-content {
            height: auto;
            padding: 10px 87px 10px 30px;
        }

        .aspect-content:before {
            display: none;
        }

        .aspect-content:after {
            top: 0;
        }

        .aspect-content > div {
            display: block;
            width: 100%;
        }

        .aspect-stat {
            margin-top: 10px;
            text-align: left;
        }
    }

    @media screen and (max-width: 750px) {
        .sentiment-wrapper > div {
            display: block;
            width: 100%;
            max-width: 100%;
        }

        .sentiment-wrapper > div:not(:first-child) {
            margin-top: 10px;
        }
    }

    @media screen and (max-width: 500px) {
        .aspect-label {
            max-height: 140px;
        }

        .aspect-stat > div {
            display: block;
            width: 100%;
        }

        .all-opinions {
            margin-bottom: 10px;
        }

        .all-opinions + div > span:first-child {
            margin: 0;
        }
    }

    .form-control-plaintext {
        font-size: 1.2rem !important;
        font-family: Roboto, SolaimanLipi !important;
    }

    th, td {
        text-align: left;
    }
</style>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title" style="color: #4a95dc;">
                    <i class="fa fa-language"></i>
                    <%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_EDUCATION_LANGUAGE, loginDTO2) %>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="my-3">
                <%@include file="/employee_records/multiform/tabs.jsp" %>
            </div>
            <div class="my-3">
                <div class="onlyborder">
                    <div class="row">
                        <div class="col-12">
                            <div class="sub_title_top">
                                <div class="sub_title">
                                    <h4 style="background: white">
                                        <i class="fa fa-graduation-cap"></i>
                                        <%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_EDUCATION_LANGUAGE_INFORMATION, loginDTO) %>
                                    </h4>
                                </div>
                            </div>

                            <%--new accordian start--%>
                            <div id="aspect-content" style="margin: 2rem;">
                                <div class="aspect-tab">
                                    <input id="education_portlet" type="checkbox" class="aspect-input"
                                           name="aspect">
                                    <label for="education_portlet" class="aspect-label"></label>
                                    <div class="aspect-content">
                                        <div class="aspect-info">
                                            <span class="aspect-name text-dark">
                                                <%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_EDUCATIONAL_INFORMATION, loginDTO) %>
                                            </span>
                                        </div>
                                        <div class="aspect-stat">
                                        </div>
                                    </div>
                                    <div class="aspect-tab-content">
                                        <div class="sentiment-wrapper">
                                            <div>
                                                <div>
                                                    <div class="neutral-count opinion-header">
                                                    </div>
                                                    <div>
                                                        <%-- tab content start--%>
                                                            <div class="kt-portlet__content" id="education_item_div">
                                                            </div>
                                                        <%--tab content end--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--new accordian end--%>

                            <%--new accordian start--%>
                            <div id="aspect-content" style="margin: 2rem;">
                                <div class="aspect-tab">
                                    <input id="language_portlet" type="checkbox" class="aspect-input"
                                           name="aspect">
                                    <label for="language_portlet" class="aspect-label"></label>
                                    <div class="aspect-content">
                                        <div class="aspect-info">
                                            <span class="aspect-name text-dark">
                                                <%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_LINGUISTIC_INFORMATION, loginDTO) %>
                                            </span>
                                        </div>
                                        <div class="aspect-stat">
                                        </div>
                                    </div>
                                    <div class="aspect-tab-content">
                                        <div class="sentiment-wrapper">
                                            <div>
                                                <div>
                                                    <div class="neutral-count opinion-header">
                                                    </div>
                                                    <div>
                                                        <%-- tab content start--%>
                                                            <div class="kt-portlet__content" id="language_item_div">
                                                            </div>
                                                        <%--tab content end--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--new accordian end--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    const educationSelector = $('#education_portlet');
    const languageSelector = $('#language_portlet');
    noLanguageData = true;
    noEducationData = true;
    $(document).ready(() => {
        educationSelector.prop('checked', false);
        languageSelector.prop('checked', false);
        <%if (actionName.equals("view")) {%>
        educationSelector.change(function () {
            if (this.checked) {
                setEducationData();
            }
        });
        languageSelector.change(function () {
            if (this.checked) {
                setLanguageData();
            }
        });
        <%}%>

        let selectorId = null;
        switch ("<%=request.getParameter("data")%>") {
            case 'education':
                setEducationData();
                selectorId = educationSelector;
                break;
            case 'language':
                setLanguageData();
                selectorId = languageSelector;
                break;
        }
        if (selectorId) {
            selectorId.prop('checked', true);
            let selectorIdParent = $(selectorId).parent();
            $('html, body').animate({scrollTop: selectorIdParent.offset().top - 200}, 'slow');
        }
    });

    function hasAjaxMarker(ajaxHtml) {
        return ajaxHtml.includes('data-ajax-marker="true"');
    }

    function setLanguageData() {
        if (noLanguageData === false)
            return;
        $.ajax({
            url: "Employee_recordsServlet?actionType=ajax_getLanguageRecord&ID=" + <%=ID%>+"&userId="+<%=request.getParameter("userId")%>,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if(hasAjaxMarker(fetchedData)) {
                    noLanguageData = false;
                    document.getElementById("language_item_div").innerHTML = fetchedData;
                }
                else {
                    window.location.href = "";
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function setEducationData() {
        if (noEducationData === false)
            return;
        $.ajax({
            url: "Employee_recordsServlet?actionType=ajax_getEducationRecord&ID=" + <%=ID%>+"&userId="+<%=request.getParameter("userId")%>,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if(hasAjaxMarker(fetchedData)) {
                    document.getElementById("education_item_div").innerHTML = fetchedData;
                    noEducationData = false;
                }
                else {
                    window.location.href = "";
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>