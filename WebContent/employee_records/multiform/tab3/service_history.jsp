<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="util.HttpRequestUtils" %>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong( ID );
    long empId = id;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String actionName="";
    String actionType = request.getParameter( "actionType" );
%>
<div class="form-body">
    <%@include file="/employee_service_history/employee_service_history_view_page.jsp"%>
</div>