<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    boolean isLanguageEnglish = Language.equals("English");
    String actionType = request.getParameter("actionType");
%>
<div class="form-body">
    <%@include file="/promotion_history/promotion_history_details_info_view.jsp" %>
</div>
