<%@page import="employee_offices.EmployeeOfficeRepository"%>
<%@page import="employee_offices.Employee_officesDTO"%>
<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="employee_office_report.InChargeLevelEnum" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.time.ZoneId" %>
<%@ page import="java.time.Period" %>
<%@ page import="employee_attachment.Employee_attachmentDTO" %>
<%@ page import="common.RoleEnum" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="promotion_history.*" %>

<%!
    public String calculatePeriod(long startDate, long endDate, String language) {
        LocalDate serviceFrom = new Date(startDate).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        LocalDate servingTo = new Date(endDate).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        Period p = Period.between(serviceFrom, servingTo);
        String period = "";
        if ("English".equalsIgnoreCase(language)) {
            period = p.getYears() + "y " + p.getMonths() + "m " + p.getDays() + "d";
        } else {
            period = StringUtils.convertToBanNumber(p.getYears() + "ব " + p.getMonths() + "মা " + p.getDays() + "দি");
        }
        return period;
    }
%>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equals("English");

    List<EmployeeOfficeDTO> employeeOfficeDTOList = (List<EmployeeOfficeDTO>) request.getAttribute("employeeOfficeDTOList");
    List<Employee_attachmentDTO> employeeAttachmentDTOList = (List<Employee_attachmentDTO>) request.getAttribute("employeeAttachmentDTOList");
    boolean deletePermission = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId();

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    long empId = id;
    Promotion_historyDAO promotion_historyDAO = Promotion_historyDAO.getInstance();
%>
<input type="hidden" data-ajax-marker="true">
<div class="form-body">
    <table class="table table-striped table-bordered" style="font-size: 14px">
        <thead>
        <tr>
            <th style="vertical-align: middle;text-align: left">
                <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENTOFFICEUNITID, loginDTO)%>
                </b></th>
            <th style="vertical-align: middle;text-align: left">
                <b><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_DESIGNATION, loginDTO)%>
                </b></th>
            <th style="vertical-align: middle;text-align: left">
                <b><%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_INCHARGELABEL, loginDTO)%>
                </b></th>
            <th style="vertical-align: middle;text-align: left">
                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_SERVINGFROM, loginDTO)%>
                </b></th>
            <th style="vertical-align: middle;text-align: left">
                <b><%=LM.getText(LC.EMPLOYEE_HISTORY_ADD_LASTOFFICEDATE, loginDTO)%>
                </b></th>
            
            <th style="vertical-align: middle;text-align: left"><b><%=isLanguageEnglish ? "Period" : "সময়কাল"%>
            </b></th>
            <th style="vertical-align: middle;text-align: left"><b><%=isLanguageEnglish ? "Previous Post" : "পূর্বের পদ"%>
            </b></th>
            <%
                if (deletePermission) {
            %>
            <th></th>
            <%
                }
            %>
        </tr>
        </thead>
        <tbody>
        <%
            int index = 0;
            for (EmployeeOfficeDTO employeeOfficeDTO : employeeOfficeDTOList) {
        %>
        <tr>
            <td style="vertical-align: middle;text-align: left">
                <%=isLanguageEnglish ? employeeOfficeDTO.unitNameEn : employeeOfficeDTO.unitNameBn%>
            </td>

            <td style="vertical-align: middle;text-align: left">
                <%=isLanguageEnglish ? employeeOfficeDTO.designationEn : employeeOfficeDTO.designationBn%>
            </td>

            <td style="vertical-align: middle;text-align: left">
                <%=InChargeLevelEnum.getTextByValue(Language, employeeOfficeDTO.inchargeLabel)%>
            </td>

            <td style="vertical-align: middle;text-align: left">
                <%=StringUtils.getFormattedDate(Language, employeeOfficeDTO.joiningDate)%>
            </td>

            <td style="vertical-align: middle;text-align: left">
                <%
                    String lastOfficeSate = StringUtils.getFormattedDate(Language, employeeOfficeDTO.lastOfficeDate);
                    if ("".equals(lastOfficeSate)) {
                        lastOfficeSate = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISCURRENTLYWORKING, loginDTO);
                    }
                %>
                <%=lastOfficeSate%>
            </td>
            <td style="vertical-align: middle;text-align: left">
                <%
                    if (employeeOfficeDTO.lastOfficeDate != SessionConstants.MIN_DATE) {
                %>
                <%=calculatePeriod(employeeOfficeDTO.joiningDate, employeeOfficeDTO.lastOfficeDate, Language)%>
                <%} else {%>
                -
                <%}%>
            </td>
            	
            <td>
            	<%if(employeeOfficeDTO.promotionHistoryId != -1)
            		{
            			Promotion_historyDTO promotion_historyDTO = promotion_historyDAO.getDTOByID(employeeOfficeDTO.promotionHistoryId);
            			if(promotion_historyDTO != null)
            			{
            				EmployeeOfficeDTO dto = EmployeeOfficeRepository.getInstance().getById(promotion_historyDTO.previousEmployeeOfficeId);
            				if(dto != null)
            				{
            					
            					if(isLanguageEnglish)
            					{
            						%>
            						<%=dto.unitNameEn%>, <%=dto.designationEn%>
            						<%
            					}
            					else
            					{
            						%>
            						<%=dto.unitNameBn%>, <%=dto.designationBn%>
            						<%
            					}
            			
            				}
            			}
            		}
            		%>
            </td>
            <%
                if (deletePermission) {
            %>
            <td style="text-align: center; vertical-align: middle;">
                <form action="EmployeeOfficesServlet?isPermanentTable=true&actionType=ajax_delete&tab=3&ID=<%=employeeOfficeDTO.iD%>&empId=<%=employeeOfficeDTO.employeeRecordId%>&userId=<%=request.getParameter("userId")%>"
                      method="POST" id="employeeOfficeTableForm<%=index%>" enctype="multipart/form-data">
                    <div class="btn-group" role="group" aria-label="Basic example">&nbsp;
                        <button type="button" class="btn-danger"
                                title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                                onclick="deleteItem('employeeOfficeTableForm',<%=index%>)"><i
                                class="fa fa-trash"></i>&nbsp;
                        </button>
                    </div>
                </form>
            </td>
            <%
                }
                index++;
            %>
        </tr>
        <%}%>

        <%
            index = 0;
            for (Employee_attachmentDTO employee_attachmentDTO : employeeAttachmentDTOList) {
        %>
        <tr>
            <td style="vertical-align: middle;text-align: left">
                <%=Office_unitsRepository.getInstance().geText(Language, employee_attachmentDTO.officeUnitId)%>
            </td>
            <td></td>
            <td><%=isLanguageEnglish ? "Attachment Responsibility" : "সংযুক্তি দায়িত্ব"%>
            </td>
            <td><%=StringUtils.getFormattedDate(Language, employee_attachmentDTO.startDate)%>
            </td>
            <td style="vertical-align: middle;text-align: left">
                <%
                    String lastOfficeSate = StringUtils.getFormattedDate(Language, employee_attachmentDTO.endDate);
                    if ("".equals(lastOfficeSate)) {
                        lastOfficeSate = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISCURRENTLYWORKING, loginDTO);
                    }
                %>
                <%=lastOfficeSate%>
            </td>
            <td style="vertical-align: middle;text-align: left">
                <%
                    if (employee_attachmentDTO.endDate != SessionConstants.MIN_DATE) {
                %>
                <%=calculatePeriod(employee_attachmentDTO.startDate, employee_attachmentDTO.endDate, Language)%>
                <%} else {%>
                -
                <%}%>
            </td>
            <%
                if (deletePermission) {
            %>
            <td style="text-align: center; vertical-align: middle;">
                <form action="Employee_attachmentServlet?isPermanentTable=true&actionType=delete&tab=3&ID=<%=employee_attachmentDTO.iD%>&empId=<%=employee_attachmentDTO.employeeRecordsId%>&userId=<%=request.getParameter("userId")%>"
                      method="POST" id="attachmentTableForm<%=index%>" enctype="multipart/form-data">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn-danger"
                                title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                                onclick="deleteItem('attachmentTableForm',<%=index%>)"><i
                                class="fa fa-trash"></i>&nbsp;
                        </button>
                    </div>
                </form>
            </td>
            <%
                }
                index++;
            %>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 text-right mt-3">
            <button class="btn btn-gray m-t-10 rounded-pill"
                    onclick="location.href='<%=request.getContextPath()%>/Employee_postingServlet?actionType=getAddPage&tab=3&data=localPosting&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-plus"></i><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ADD_LOCAL_POSTING, loginDTO)%>
            </button>
        </div>
    </div>
</div>