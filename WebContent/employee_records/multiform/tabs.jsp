<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<style>
    .double_blank_row {
        height: 40px !important;
        background-color: transparent;
    }

    .form-control-plaintext {
        padding: 0;
        font-size: 1.2em;
    }

    .form-group label {
        font-size: 1em;
        margin: 0;
    }

    .edit-tools {
        display: inline-block;
        float: right;
    }

    .edit-tools .btn {
        background-color: #ffffff;
        font-size: 12px;
        font-weight: 600;
        color: #808080;
        border: none;
        padding: 0 5px;
        font-family: 'Montserrat', sans-serif;
    }

    .edit-tools .btn {
        color: #0060DB;
        padding: 6px 10px;
        box-shadow: none;
        text-transform: uppercase;
        font-weight: bold;
    }

    .edit-tools .delete-btn {
        color: #E71313;
    }

    .edit-tools .delete-btn:hover {
        color: #CF1111;
    }

    .edit-tools .edit-btn:hover {
        color: #0055C2;
    }

    .edit-tools .btn:hover {
        background-color: #eceff1;
        box-shadow: none;
    }

    .edit-tools .view-btn {
        color: #44cf50;
    }


    .edit-tools .download-btn {
        color: greenyellow;
    }

    .btn-gray,
    .btn-gray:focus {
        background-color: #ffffff;
        color: #008020;
        margin-bottom: 10px;
        border: 2px solid #e0e0e0;
        padding: 10px 15px;
        font-weight: 600;
    }

    .btn-gray:hover {
        background-color: #ffffff;
        color: #008020;
        border: 2px solid #008020;
        box-shadow: none !important;
    }

</style>
<%
    Integer tab = (Integer) request.getAttribute("tab");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equals("English");
    String actionName = "";
    String actionType = request.getParameter("actionType");
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    if (actionType.equals("viewMyProfile")) {
        UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
        ID = new Long(userDTO.employee_record_id).toString();
    }
    long id = Long.parseLong(ID);
    long empId = id;
    String additionalParam = (id > 0 ? "&ID=" + id : "")+"&userId="+request.getParameter("userId");


    switch (actionType) {
        case "addMultiForm":
            actionName = "add";
            break;
        case "editMultiForm":
            actionName = "edit";
            break;
        case "viewMyProfile":
            additionalParam = "";
        case "viewMultiForm":
            actionName = "view";
            break;


    }
%>

<div class="btn-group tab-group row" role="group" style="margin: 0px 0px 5px 0px !important;">
    <div class="col-md-2">
        <%if (request.getParameter("actionType").equals("addMultiForm")) {%>
        <button type="button"
                id="btn_personal_info"
                onclick="location.href='<%=request.getContextPath()%>/Employee_recordsServlet?actionType=addMultiForm&tab=1<%=additionalParam%>'"
                class="btn tab-btn <%=tab == 1?"active":""%> btn-tab-personal">
            <i class="fa fa-user-circle"></i>&nbsp;
            <small>
                <%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTO) %>
            </small>
        </button>
        <%
        } else if (request.getParameter("actionType").equals("editMultiForm")) {
        %>
        <button type="button" id="btn_personal_info"
                onclick="location.href='<%=request.getContextPath()%>/Employee_recordsServlet?actionType=editMultiForm&tab=1<%=additionalParam%>'"
                class="btn tab-btn <%=tab == 1?"active":""%> btn-tab-personal">
            <i class="fa fa-user-circle"></i>&nbsp;
            <small><%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTO) %>
            </small>
        </button>
        <%} else {%>
        <button type="button" id="btn_personal_info"
                onclick="location.href='<%=request.getContextPath()%>/Employee_recordsServlet?actionType=<%=actionType%>&tab=1<%=additionalParam%>'"
                class="btn tab-btn <%=tab == 1?"active":""%> btn-tab-personal">
            <i class="fa fa-user-circle"></i>&nbsp;
            <small><%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTO) %>
            </small>
        </button>
        <%}%>

        <%if (tab == 1) { %>
        <div class="arrow-down"></div>
        <%}%>
    </div>

    <div class="col-md-2">
        <button type="button" id="btn_education_info"
                onclick="location.href='<%=request.getContextPath()%>/Employee_recordsServlet?actionType=<%=actionType%>&tab=2<%=additionalParam%>'"
                class="btn tab-btn <%=tab == 2?"active":""%> btn-tab-education">
            <i class="fa fa-graduation-cap"></i>&nbsp;
            <small><%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO, loginDTO) %>
            </small>
        </button>

        <%if (tab == 2) { %>
        <div class="arrow-down"></div>
        <%}%>
    </div>

    <div class="col-md-2">
        <button type="button" id="btn_occupation_info"
                onclick="location.href='<%=request.getContextPath()%>/Employee_recordsServlet?actionType=<%=actionType%>&tab=3<%=additionalParam%>'"
                class="btn tab-btn <%=tab == 3?"active":""%> btn-tab-employment">
            <i class="fa fa-briefcase "></i>&nbsp;<small><%= LM.getText(LC.HR_MANAGEMENT_OCCUPATIONAL_INFORMATION, loginDTO) %>
        </small>
        </button>

        <%if (tab == 3) { %>
        <div class="arrow-down"></div>
        <%}%>
    </div>

    <div class="col-md-2">
        <button type="button" id="btn_training_info"
                onclick="location.href='<%=request.getContextPath()%>/Employee_recordsServlet?actionType=<%=actionType%>&tab=4<%=additionalParam%>'"
                class="btn tab-btn <%=tab == 4?"active":""%> btn-tab-employment">
            <i class="fa fa-university"></i>&nbsp;<small><%= LM.getText(LC.HR_MANAGEMENT_JOB_POSTING_AND_TRAINING, loginDTO) %>
        </small>
        </button>

        <%if (tab == 4) { %>
        <div class="arrow-down"></div>
        <%}%>
    </div>

    <div class="col-md-2">
        <button type="button" id="btn_travel_info"
                onclick="location.href='<%=request.getContextPath()%>/Employee_recordsServlet?actionType=<%=actionType%>&tab=5<%=additionalParam%>'"
                class="btn tab-btn <%=tab == 5?"active":""%> btn-tab-employment">
            <i class="fa fa-plane"></i>&nbsp;<small><%= LM.getText(LC.HR_MANAGEMENT_TRAVEL_AND_CERTIFICATION, loginDTO) %>
        </small>
        </button>

        <%if (tab == 5) { %>
        <div class="arrow-down"></div>
        <%}%>
    </div>

    <div class="col-md-2">
        <button type="button" id="btn_other_info"
                onclick="location.href='<%=request.getContextPath()%>/Employee_recordsServlet?actionType=<%=actionType%>&tab=6<%=additionalParam%>'"
                class="btn tab-btn <%=tab == 6?"active":""%> btn-tab-employment">
            <i class="fa fa-recycle"></i>&nbsp;<small><%= LM.getText(LC.HR_MANAGEMENT_OTHERS, loginDTO) %>
        </small>
        </button>

        <%if (tab == 6) { %>
        <div class="arrow-down"></div>
        <%}%>
    </div>

</div>

<br/>
<script src="<%=request.getContextPath()%>/assets/scripts/util1.js"></script>
<script>
    function deleteItem(id, index) {
        deleteDialog(null, () => {
            let formName = '#' + id + index;
            console.log($(formName));
            $(formName).submit();
        });
    }

    let formEditOption = '<%= request.getParameter("actionType") %>';
    if (formEditOption === "addMultiForm") {
        $('#btn_education_info').prop('disabled', true).css('opacity', '0.5');
        $('#btn_occupation_info').prop('disabled', true).css('opacity', '0.5');
        $('#btn_training_info').prop('disabled', true).css('opacity', '0.5');
        $('#btn_travel_info').prop('disabled', true).css('opacity', '0.5');
        $('#btn_other_info').prop('disabled', true).css('opacity', '0.5');

    } else {
        $('#btn_education_info').prop('disabled', false).css('opacity', '1');
        $('#btn_occupation_info').prop('disabled', false).css('opacity', '1');
        $('#btn_training_info').prop('disabled', false).css('opacity', '1');
        $('#btn_travel_info').prop('disabled', false).css('opacity', '1');
        $('#btn_other_info').prop('disabled', false).css('opacity', '1');
    }

</script>