<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equals("English");
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long empId = Long.parseLong(ID);
    String actionName="";
    String actionType = request.getParameter( "actionType" );
%>
<div class="form-body">
    <%@include file="/emp_travel_details/emp_travel_details_info_view.jsp"%>
</div>
