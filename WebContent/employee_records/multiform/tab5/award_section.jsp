<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long empId = Long.parseLong(ID);
    boolean isLanguageEnglish = Language.equals("English");
%>
<div class="form-body">
    <%@include file="/employee_honors_awards/employee_honor_award_view_page.jsp" %>
</div>