<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.List" %>
<%@ page import="training_calendar_details.TrainingCalendarDetailsShortInfo" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDAO" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
    Boolean isLangEng = Language.equalsIgnoreCase("English");
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long empId = Long.parseLong(ID);
    String actionType = request.getParameter("actionType");
    System.out.println("actionType = " + request.getParameter("actionType"));
    List<TrainingCalendarDetailsShortInfo> savedTrainingList = new Training_calendar_detailsDAO().getTrainingCalendarDetailsShortInfoList(empId, Language);
%>

<input type="hidden" data-ajax-marker="true">

<div class="form-body">
    <table class="table table-striped table-bordered" style="font-size: 14px">
        <thead>
        <tr>
            <th><b><%=LM.getText(LC.EMP_TRAINING_DETAILS_EDIT_TRAININGCALENDARTYPE, loginDTO)%>
            </b></th>
            <th><b><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGTYPECAT, loginDTO)%>
            </b></th>
            <th><b><%=LM.getText(LC.START_DATE_TIME, loginDTO)%>
            </b></th>
            <th><b><%=LM.getText(LC.END_DATE_TIME, loginDTO)%>
            </b></th>
            <th><b><%=Language.equalsIgnoreCase("English") ? "Feedback" : "ফিডব্যাক"%>
            </b></th>
            <%
                if (!actionType.equals("viewSummary")) {
            %>
            <th><b></b></th>
            <%
                }
            %>
        </tr>
        </thead>
        <tbody>
        <%
            if (savedTrainingList != null && savedTrainingList.size() > 0) {
                for (TrainingCalendarDetailsShortInfo trainingItem : savedTrainingList) {
        %>
        <tr>
            <%@include file="/training_calendar_details/training_details_view_item.jsp" %>
        </tr>
        <%
                }
            }
        %>
        </tbody>
    </table>

    <div class="row">
        <div class="col-12 text-right mt-3">
            <button class="btn btn-gray m-t-10 rounded-pill"
                    onclick="location.href='<%=request.getContextPath()%>/Training_calenderServlet?actionType=getAddPage&tab=4&data=trainingCalender&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-plus"></i> <%=isLangEng ? "Add Training" : "প্রশিক্ষণ যোগ করুন"%>
            </button>
        </div>
    </div>
</div>