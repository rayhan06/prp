<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%
    boolean isLangEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String title = isLangEnglish ? "Employee Information" : "কর্মকর্তার তথ্য";
%>

<jsp:include page="/common/layout.jsp" flush="true">
    <jsp:param name="title" value='<%=title%>'/>
    <jsp:param name="body" value="/employee_records/multiform/tab4/tab4_body.jsp"/>
</jsp:include>