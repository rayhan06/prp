<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.EMPLOYEE_RECORDS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
%>


<!-- search control -->
<div class="portlet box portlet-btcl">
    <div>
        <!-- BEGIN FORM-->
        <div class="form-horizontal">
            <div class="form-body">
                <div class="col-10">

                    <div class="col-lg-3">
                        <div class="row">
                            <label for=""
                                   class="control-label col-4"><%=LM.getText(LC.GLOBAL_RECORD_PER_PAGE, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <select class="form-control" id="pagenumber"
                                        name="<%=LM.getText(LC.GLOBAL_RECORD_PER_PAGE, loginDTO)%>"
                                        onchange='allfield_changed()'>
                                    <option value='10'>10</option>
                                    <option value='20'>20</option>
                                    <option value='30'>30</option>
                                    <option value='50'>50</option>
                                    <option value='100'>30</option>
                                    <option value='100000'>All</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label for=""
                                   class="control-label col-4"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_ANYFIELD, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <input type="text" class="form-control" id="anyfield"
                                       placeholder="" name="anyfield" onfocusout='anyfield_changed()'>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label for=""
                                   class="control-label col-4"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_NAMEENG, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <input type="text" class="form-control" id="name_eng" placeholder="" name="name_eng"
                                       onfocusout="allfield_changed()">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label for=""
                                   class="control-label col-4"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_NAMEBNG, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <input type="text" class="form-control" id="name_bng" placeholder="" name="name_bng"
                                       onfocusout="allfield_changed()">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label for=""
                                   class="control-label col-4"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_NID, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <input type="text" class="form-control" id="nid" placeholder="" name="nid"
                                       onfocusout="allfield_changed()">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label for=""
                                   class="control-label col-4"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_PERSONALEMAIL, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <input type="text" class="form-control" id="personal_email" placeholder=""
                                       name="personal_email" onfocusout="allfield_changed()">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label for=""
                                   class="control-label col-4"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_PERSONALMOBILE, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <input type="text" class="form-control" id="personal_mobile" placeholder=""
                                       name="personal_mobile" onfocusout="allfield_changed()">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="row">
                            <label for=""
                                   class="control-label col-4"><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_ALTERNATIVEMOBILE, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <input type="text" class="form-control" id="alternative_mobile" placeholder=""
                                       name="alternative_mobile" onfocusout="allfield_changed()">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    var searchChanged = 0;

    function setValues(x, value) {
        for (i = 0; i < x.length; i++) {
            x[i].value = value;
        }
    }

    function setPageNoInAllFields(value) {
        var x = document.getElementsByName("pageno");
        setValues(x, value);
    }

    function setRPPInAllFields(value) {
        var x = document.getElementsByName("RECORDS_PER_PAGE");
        setValues(x, value);
    }

    function setTotalPageInAllFields(value) {
        var x = document.getElementsByName("totalpage");
        setValues(x, value);
    }

    function setPageNo() {

        setPageNoInAllFields(document.getElementById('hidden_pageno').value);
        setTotalPageInAllFields(document.getElementById('hidden_totalpage').value);
        console.log("totalpage now is " + document.getElementById('hidden_totalpage').value);
        //document.getElementById('totalpage').innerHTML= document.getElementById('hidden_totalpage').value;
    }

    function setSearchChanged() {
        searchChanged = 1;
    }

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            }
            else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&" + params, true);
        xhttp.send();

    }

    function anyfield_changed(go) {
        var lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        var totalRecords = document.getElementById('hidden_totalrecords').value;
        var params = 'AnyField=' + document.getElementById('anyfield').value
            + '&search=true&ajax=true&RECORDS_PER_PAGE=' + document.getElementsByName('RECORDS_PER_PAGE')[0].value
            + '&pageno=' + document.getElementsByName('pageno')[0].value
        ;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);
    }

    function setPageNoAndSubmit(link) {
        var value = -1;
        if (link == 1) //next
        {
            value = parseInt(document.getElementsByName('pageno')[0].value, 10) + 1;
        }
        else if (link == 2) //previous
        {
            value = parseInt(document.getElementsByName('pageno')[0].value, 10) - 1;
        }
        else if (link == 3) //last
        {
            value = document.getElementById('hidden_totalpage').value;
        }
        else // 1st
        {
            value = 1
        }
        setPageNoInAllFields(value);
        console.log('pageno = ' + document.getElementsByName('pageno')[0].value);
        allfield_changed('go', 0);
    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;
        <%
        for(int i = 0; i < searchFieldInfo.length - 1; i ++)
        {
            out.println("params += '&" +  searchFieldInfo[i][1] + "='+document.getElementById('"
                + searchFieldInfo[i][1] + "').value");
        }
        %>
        params += '&search=true&ajax=true';

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
        var totalRecords = document.getElementById('hidden_totalrecords').value;
        var lastSearchTime = document.getElementById('hidden_lastSearchTime').value;

        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

