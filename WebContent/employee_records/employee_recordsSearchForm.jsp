<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_records.Employee_recordsDTO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.StringUtils" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value = "";
    String Language = LM.getText(LC.EMPLOYEE_RECORDS_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="thead-light text-center">
        <tr>
            <%
                if (isLanguageEnglish) {
            %>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NAMEENG, loginDTO)%>
            </th>
            <%
            } else {
            %>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NAMEBNG, loginDTO)%>
            </th>
            <%
                }
            %>
            <th><%=LM.getText(LC.USER_SEARCH_USER_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERSONALEMAIL, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERSONALMOBILE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_STATUS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_RECORDS_SEARCH_EMPLOYEE_RECORDS_EDIT_BUTTON, loginDTO)%>
            </th>
            <th>
                <div class="d-flex flex-row">
                    <button type="submit" class="btn btn-xs btn-danger" id="delete-btn" onclick="onDeleteEmployee()">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_EMPLOYEE_RECORDS);
            try {
                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Employee_recordsDTO row = (Employee_recordsDTO) data.get(i);
                        String deletedStyle = "color:red";
                        if (!row.isDeleted) deletedStyle = "";
                        out.println("<tr id = 'tr_" + i + "'>");
                        out.println("<td id = '" + i + "_nameEng'>");
                        if (isLanguageEnglish) {
                            value = row.nameEng;
                        } else {
                            value = row.nameBng;
                        }
                        if (value == null) {
                            value = "";
                        }
                        out.println(value);
                        out.println("</td>");
                        out.println("<td id = '" + i + "_employeeNumber'>");
                        if (isLanguageEnglish) {
                            value = row.employeeNumber;
                        } else {
                            value = StringUtils.convertToBanNumber(row.employeeNumber);
                        }
                        if (value == null) {
                            value = "";
                        }
                        out.println(value);
                        out.println("</td>");
                        out.println("<td id = '" + i + "_personalEml'>");
                        value = row.personalEml;
                        if (value == null) {
                            value = "";
                        }
                        out.println(value);
                        out.println("</td>");
                        out.println("<td id = '" + i + "_personalMobile'>");
                        if (row.personalMobile != null && row.personalMobile.length() > 2) {
                            value = row.personalMobile.substring(2);
                        } else {
                            value = row.personalMobile;
                        }
                        if (!isLanguageEnglish) {
                            value = StringUtils.convertToBanNumber(value);
                        }
                        if (value == null) {
                            value = "";
                        }
                        out.println(value);
                        out.println("</td>");
                        out.println("<td id = '" + i + "_personalNID'>");
                        value = row.nid;
                        if (!isLanguageEnglish) {
                            value = StringUtils.convertToBanNumber(value);
                        }
                        if (value == null) {
                            value = "";
                        }
                        out.println(value);
                        out.println("</td>");
                        out.println("<td id = '" + i + "_status'>");
                        value = row.status ? LM.getText(LC.GLOBAL_STATUS_ACTIVE, loginDTO) : LM.getText(LC.GLOBAL_STATUS_INACTIVE, loginDTO);
                        out.println(value);
                        out.println("</td>");
                        String onclickFunc = "\"fixedToEditable(" + i + ",'" + deletedStyle + "', '" + row.iD + "' )\"";
                        out.println("<td>");
                        out.println("<a class='btn btn-success btn-elevate btn-icon btn-square btn-sm' id = '" + i + "_Edit' href='" + request.getContextPath() + "/Employee_recordsServlet?actionType=viewMultiForm&tab=1&ID=" + row.iD + "'><i class='fa fa-edit'></i></a>");
                        out.println("</td>");
                        out.println("<td>");
                        out.println("<label id='chkEdit' class='kt-checkbox kt-checkbox'><input type='checkbox' class='checkbox' name='ID' value='" + row.iD + "'><span></span></label>");
                        out.println("</td>");
                        out.println("</tr>");
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>
<%
    String navigator2 = SessionConstants.NAV_EMPLOYEE_RECORDS;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).on("change", "th .checkbox", function () {
        if ($(this).is(":checked")) {
            $("td .checkbox").prop('checked', true);
            $("delete-btn").classList.add('d-block');
        } else {
            $("td .checkbox").prop('checked', false);
        }
    });

    function getOfficer(officer_id, officer_select) {
        console.log("getting officer");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response for officer");
                    document.getElementById(officer_select).innerHTML = this.responseText;
                    if (document.getElementById(officer_select).length > 1) {
                        document.getElementById(officer_select).removeAttribute("disabled");
                    }
                } else {
                    console.log("got errror response for officer");
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "Employee_recordsServlet?actionType=getGRSOffice&officer_id=" + officer_id, true);
        xhttp.send();
    }

    function getLayer(layernum, layerID, childLayerID, selectedValue) {
        console.log("getting layer");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response");
                    document.getElementById(childLayerID).innerHTML = this.responseText;
                } else {
                    console.log("got errror response");
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", "Employee_recordsServlet?actionType=getGRSLayer&layernum=" + layernum + "&layerID="
            + layerID + "&childLayerID=" + childLayerID + "&selectedValue=" + selectedValue, true);
        xhttp.send();
    }

    function layerselected(layernum, layerID, childLayerID, hiddenInput, hiddenInputForTopLayer, officerElement) {
        var layervalue = document.getElementById(layerID).value;
        console.log("layervalue = " + layervalue);
        document.getElementById(hiddenInput).value = layervalue;
        if (layernum == 0) {
            document.getElementById(hiddenInputForTopLayer).value = layervalue;
        }
        if (layernum == 0 || (layernum == 1 && document.getElementById(hiddenInputForTopLayer).value == 3)) {
            document.getElementById(childLayerID).setAttribute("style", "display: inline;");
            getLayer(layernum, layerID, childLayerID, layervalue);
        }
        if (officerElement !== null) {
            getOfficer(layervalue, officerElement);
        }
    }

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;
            if (!document.getElementById('nameEng_text_' + row).checkValidity()) {
                empty_fields += "'nameEng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('nameBng_text_' + row).checkValidity()) {
                empty_fields += "'nameBng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('fatherNameEng_text_' + row).checkValidity()) {
                empty_fields += "'fatherNameEng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('fatherNameBng_text_' + row).checkValidity()) {
                empty_fields += "'fatherNameBng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('motherNameEng_text_' + row).checkValidity()) {
                empty_fields += "'motherNameEng'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('dateOfBirth_date_' + row).checkValidity()) {
                empty_fields += "'dateOfBirth'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('nid_text_' + row).checkValidity()) {
                empty_fields += "'nid'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('bcn_text_' + row).checkValidity()) {
                empty_fields += "'bcn'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('ppn_text_' + row).checkValidity()) {
                empty_fields += "'ppn'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('gender_select_' + row).checkValidity()) {
                empty_fields += "'gender'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('religion_select_' + row).checkValidity()) {
                empty_fields += "'religion'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('bloodGroup_select_' + row).checkValidity()) {
                empty_fields += "'bloodGroup'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('maritalStatus_select_' + row).checkValidity()) {
                empty_fields += "'maritalStatus'";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }
        }
        console.log("found date = " + document.getElementById('dateOfBirth_date_Date_' + row).value);
        document.getElementById('dateOfBirth_date_' + row).value = new Date(document.getElementById('dateOfBirth_date_Date_' + row).value).getTime();
        if (document.getElementById('status_checkbox_' + row).checked) {
            document.getElementById('status_checkbox_' + row).value = "true";
        } else {
            document.getElementById('status_checkbox_' + row).value = "false";
        }
        return true;
    }

    function PostprocessAfterSubmiting(row) {
    }

    function addHTML(id, HTML) {
        document.getElementById(id).innerHTML += HTML;
    }

    function getRequests() {
        var s1 = location.search.substring(1, location.search.length).split('&'),
            r = {}, s2, i;
        for (i = 0; i < s1.length; i += 1) {
            s2 = s1[i].split('=');
            r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
        }
        return r;
    }

    function Request(name) {
        return getRequests()[name.toLowerCase()];
    }

    function ShowExcelParsingResult(suffix) {
        var failureMessage = document.getElementById("failureMessage_" + suffix);
        if (failureMessage == null) {
            console.log("failureMessage_" + suffix + " not found");
        }
        console.log("value = " + failureMessage.value);
        if (failureMessage != null && failureMessage.value != "") {
            alert("Excel uploading result:" + failureMessage.value);
        }
    }

    function init(row) {
    }

    function doEdit(params, i, id, deletedStyle) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    var onclickFunc = "submitAjax(" + i + ",'" + deletedStyle + "')";
                    document.getElementById('tr_' + i).innerHTML = this.responseText;
                    document.getElementById('tr_' + i).innerHTML += "<td id = '" + i + "_Submit'></td>";
                    document.getElementById(i + '_Submit').innerHTML += "<button class=\"btn btn-primary btn-hover-brand btn-square\" onclick=\"" + onclickFunc + "\">Submit</button>";
                    document.getElementById('tr_' + i).innerHTML += "<td>"
                        + "<div >"
                        + "<label class='kt-checkbox kt-checkbox--brand' id='chkEdit'><input type='checkbox' class='checkbox' name='ID' value='" + id + "'/><span></span></label>"
                        + "</td>";
                    init(i);
                    select_2_call();
                } else {
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("Get", "Employee_recordsServlet?actionType=getEditPage" + params, true);
        xhttp.send();
    }

    function submitAjax(i, deletedStyle) {
        console.log('submitAjax called');
        var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
        if (isSubmittable == false) {
            return;
        }
        var formData = new FormData();
        var value;
        value = document.getElementById('iD_hidden_' + i).value;
        console.log('submitAjax i = ' + i + ' id = ' + value);
        formData.append('iD', value);
        formData.append("identity", value);
        formData.append("ID", value);
        formData.append('nameEng', document.getElementById('nameEng_text_' + i).value);
        formData.append('nameBng', document.getElementById('nameBng_text_' + i).value);
        formData.append('fatherNameEng', document.getElementById('fatherNameEng_text_' + i).value);
        formData.append('fatherNameBng', document.getElementById('fatherNameBng_text_' + i).value);
        formData.append('motherNameEng', document.getElementById('motherNameEng_text_' + i).value);
        formData.append('motherNameBng', document.getElementById('motherNameBng_text_' + i).value);
        formData.append('dateOfBirth', document.getElementById('dateOfBirth_date_' + i).value);
        formData.append('placeOfBirth', document.getElementById('placeOfBirth_hidden_' + i).value);
        formData.append('nationality', document.getElementById('nationality_hidden_' + i).value);
        formData.append('presentAddress', document.getElementById('presentAddress_hidden_' + i).value);
        formData.append('permanentAddress', document.getElementById('permanentAddress_hidden_' + i).value);
        formData.append('occupation', document.getElementById('occupation_hidden_' + i).value);
        formData.append('nid', document.getElementById('nid_text_' + i).value);
        formData.append('nidValid', document.getElementById('nidValid_hidden_' + i).value);
        formData.append('bcn', document.getElementById('bcn_text_' + i).value);
        formData.append('ppn', document.getElementById('ppn_text_' + i).value);
        formData.append('gender', document.getElementById('gender_select_' + i).value);
        formData.append('religion', document.getElementById('religion_select_' + i).value);
        formData.append('bloodGroup', document.getElementById('bloodGroup_select_' + i).value);
        formData.append('maritalStatus', document.getElementById('maritalStatus_select_' + i).value);
        formData.append('spouseNameEng', document.getElementById('spouseNameEng_hidden_' + i).value);
        formData.append('spouseNameBng', document.getElementById('spouseNameBng_hidden_' + i).value);
        formData.append('personalEml', document.getElementById('personalEml_text_' + i).value);
        formData.append('personalMobile', document.getElementById('personalMobile_text_' + i).value);
        formData.append('alternativeMobile', document.getElementById('alternativeMobile_text_' + i).value);
        formData.append('isCadre', document.getElementById('isCadre_hidden_' + i).value);
        formData.append('employeeCadreId', document.getElementById('employeeCadreId_hidden_' + i).value);
        formData.append('employeeBatchId', document.getElementById('employeeBatchId_hidden_' + i).value);
        formData.append('identityNo', document.getElementById('identityNo_hidden_' + i).value);
        formData.append('appointmentMemoNo', document.getElementById('appointmentMemoNo_hidden_' + i).value);
        formData.append('joiningDate', document.getElementById('joiningDate_hidden_' + i).value);
        formData.append('serviceRankId', document.getElementById('serviceRankId_hidden_' + i).value);
        formData.append('serviceGradeId', document.getElementById('serviceGradeId_hidden_' + i).value);
        formData.append('serviceMinistryId', document.getElementById('serviceMinistryId_hidden_' + i).value);
        formData.append('serviceOfficeId', document.getElementById('serviceOfficeId_hidden_' + i).value);
        formData.append('currentOfficeMinistryId', document.getElementById('currentOfficeMinistryId_hidden_' + i).value);
        formData.append('currentOfficeLayerId', document.getElementById('currentOfficeLayerId_hidden_' + i).value);
        formData.append('currentOfficeId', document.getElementById('currentOfficeId_hidden_' + i).value);
        formData.append('currentOfficeUnitId', document.getElementById('currentOfficeUnitId_hidden_' + i).value);
        formData.append('currentOfficeJoiningDate', document.getElementById('currentOfficeJoiningDate_hidden_' + i).value);
        formData.append('currentOfficeDesignationId', document.getElementById('currentOfficeDesignationId_hidden_' + i).value);
        formData.append('currentOfficeAddress', document.getElementById('currentOfficeAddress_hidden_' + i).value);
        formData.append('eSign', document.getElementById('eSign_hidden_' + i).value);
        formData.append('dSign', document.getElementById('dSign_hidden_' + i).value);
        formData.append('imageFileName', document.getElementById('imageFileName_hidden_' + i).value);
        formData.append('status', document.getElementById('status_checkbox_' + i).value);
        formData.append('createdBy', document.getElementById('createdBy_hidden_' + i).value);
        formData.append('modifiedBy', document.getElementById('modifiedBy_hidden_' + i).value);
        formData.append('created', document.getElementById('created_hidden_' + i).value);
        formData.append('modified', document.getElementById('modified_hidden_' + i).value);
        formData.append('isDeleted', document.getElementById('isDeleted_hidden_' + i).value);
        formData.append('lastModificationTime', document.getElementById('lastModificationTime_hidden_' + i).value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('tr_' + i).innerHTML = this.responseText;
                    ShowExcelParsingResult(i);
                } else {
                    console.log("No Response");
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", 'Employee_recordsServlet?actionType=edit&inplacesubmit=true&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
        xhttp.send(formData);
    }

    window.onload = function () {
        ShowExcelParsingResult('general');
    }
</script>