﻿<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="View Employee Record Summary"/>
    <jsp:param name="body" value="../employee_records/employee_record_tabular_view_summary.jsp"/>
</jsp:include>