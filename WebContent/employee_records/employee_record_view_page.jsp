<%@page import="java.util.stream.Collectors"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_education_info.*"%>
<%@page import="java.util.*"%>
<%@page import="pb.*"%>
<%@page import="files.*"%>
<%@page import="result_exam.Result_examRepository" %>
<%@page import="employee_honors_awards.*" %>
<%@page import="employee_publication.*" %>
<%@page import="disciplinary_log.*" %>
<%@page import="disciplinary_details.*" %>
<%@page import="employee_language_proficiency.*" %>
<%@page import="employee_service_history.*" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsDAO" %>
<%@ page import="geolocation.GeoLocationDAO2" %>
<%@ page import="religion.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="employee_family_info.Employee_family_infoDtoWithValue" %>
<%@ page import="emp_training_details.EmployeeTrainingDetailsModel" %>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="emp_travel_details.Emp_travel_detailsDtoWithValue" %>
<%@ page import="employee_certification.Employee_certificationDTO" %>
<%@ page import="employee_nominee.EmployeeNomineeModel" %>
<%@ page import="employee_bank_information.EmployeeBankInfoModel" %>
<%@ page import="employee_leave_details.EmpLeaveDetails" %>

<style>
	.double_blank_row {
		height: 40px !important;
		background-color: transparent;
	}
	.form-control-plaintext{
		padding:0;
		font-size:1.5em;
	}
	.form-group label{
		font-size:1.5em;
		font-weight: bold;
		margin : 0;
	}
	.edit-tools {
		display: inline-block;
		float: right;
	}

	.edit-tools .btn {
		background-color: #ffffff;
		font-size: 12px;
		font-weight: 600;
		color: #808080;
		border: none;
		padding: 0px 5px;
		font-family: 'Roboto', 'SolaimanLipi', sans-serif;
	}

	.edit-tools .btn {
		color: #0060DB;
		padding: 6px 10px;
		box-shadow: none;
		text-transform: uppercase;
		font-weight: bold;
	}

	.edit-tools .delete-btn {
		color: #E71313;
	}

	.edit-tools .delete-btn:hover {
		color: #CF1111;
	}

	.edit-tools .edit-btn:hover {
		color: #0055C2;
	}
	.edit-tools .btn:hover {
		background-color: #eceff1;
		box-shadow: none;
	}

	.edit-tools .view-btn {
		color: #44cf50;
	}


	.edit-tools .download-btn {
		color: greenyellow;
	}

	.btn-gray,
	.btn-gray:focus {
		background-color: #ffffff;
		color: #008020;
		margin-bottom: 10px;
		border: 2px solid #e0e0e0;
		padding: 10px 15px;
		font-weight: 600;
	}

	.btn-gray:hover {
		background-color: #ffffff;
		color: #008020;
		border: 2px solid #008020;
		box-shadow: none !important;
	}

</style>

<%
	String context = request.getContextPath();
	String[] tokens = request.getRequestURL().toString().split(context);
	String urlOfThisPage = tokens[0]+context;
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
	boolean isLanguageEnglish = Language.equals("English");
//    ActiionName is used to hide the button in the view card page
	String actionName = "viewSummary";
	boolean isPermanentTable = true;


	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty()){
		failureMessage = "";
	}
//	out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");


	String value = "";
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");

	String ID = request.getParameter("ID");
	if(ID == null || ID.isEmpty()){
		ID = "0";
	}
	long id = Long.parseLong(ID);
	System.out.println("employee_record_view_pageID = " + ID);


	long empId = id;
	Employee_recordsDTO employee_recordsDTO = (Employee_recordsDTO) request.getAttribute("employee_recordsDTO");
%>


<%--Add Each Subset of Information as card body--%>
<div class="box box-primary">
	<div class="box-header with-border">
		<h1 class="box-title"><i class="fa fa-gift"></i><%= LM.getText(LC.HR_MANAGEMENT_EMPLOYEE_RECORD_SUMMARY, loginDTO) %></h1>
	</div>
	<div class="box-body">
		<div class="accordion md-accordion" id="accordion_tab1" role="tablist" aria-multiselectable="true">

<%--			Card for Personal Information--%>
			<div class="card">
				<div class="card-header" role="tab" id="personal_info_tab_item_header">
					<h2> <i class="fa fa-user"></i> <%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="personal_info_view">
					<%@include file="../employee_records/multiform/tab1/section1_personal_info_view.jsp"%>
				</div>
			</div>




<%--			Card for Family Member Information--%>
			<div class="card">
				<div class="card-header" role="tab" id="family_info_tab_item_header">
					<h2><i class="fa fa-users"></i> <%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_FAMILY_MEMBER, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="family_info_view">

<%--					Iterate the view page as the number of family member--%>
					<%
						List<Employee_family_infoDtoWithValue> employee_family_infos = (List<Employee_family_infoDtoWithValue>) request.getAttribute("employee_family_info");
						int familyInfoIndex = 0;
						if (employee_family_infos != null && employee_family_infos.size() > 0) {
							for (Employee_family_infoDtoWithValue employee_family_info : employee_family_infos) {
								++familyInfoIndex;
					%>
					<%@include file="../employee_family_info/employee_family_info_item.jsp"%>
					<%
							}
						}
					%>

				</div>
			</div>



			<%--			Card for Academic Information      --%>
			<div class="card">
				<div class="card-header" role="tab" id="educational_info_tab_item_header">
					<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_EDUCATIONAL_INFORMATION, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="educational_info_view">

					<%--					Iterate the view page as the number of Academics Info--%>
					<%
						List<EmployeeEducationInfoDTOWithValue> savedEductionInfoList = (List<EmployeeEducationInfoDTOWithValue>) request.getAttribute("savedEductionInfoList");
						System.out.println("savedEductionInfoList : "+savedEductionInfoList);
						int x = 0;
						if(savedEductionInfoList !=null && savedEductionInfoList.size()>0){
							for(EmployeeEducationInfoDTOWithValue eduInfoItem: savedEductionInfoList){
								++x;
					%>
					<%@include file="../employee_education_info/employee_education_info_item.jsp"%>
					<%} }%>

				</div>
			</div>



			<%--			Card for Language Profivciency      --%>
			<div class="card">
				<div class="card-header" role="tab" id="language_info_tab_item_header">
					<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_LINGUISTIC_INFORMATION, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="language_info_view">

					<%--					Iterate the view page as the number of Language Known--%>
					<%
						List<EmployeeLanguageProficiencyModel> savedLanguageProficiencyList = (List<EmployeeLanguageProficiencyModel>) request.getAttribute("savedLanguageProficiencyList");
						System.out.println("savedLanguageProficiencyList : "+savedLanguageProficiencyList);
						int langIndex = 0;
						if(savedLanguageProficiencyList !=null && savedLanguageProficiencyList.size()>0){
							for(EmployeeLanguageProficiencyModel languageItem: savedLanguageProficiencyList){
								++langIndex;
					%>
					<%@include file="../employee_language_proficiency/employee_language_proficiency_item.jsp"%>
					<%} }%>

				</div>
			</div>



<%--			Card for Training Information--%>
			<div class="card">
				<div class="card-header" role="tab" id="training_info_tab_item_header">
					<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_POSTING_AND_TRAINING_TRAINING_INFO, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="training_info_view">

					<%--					Iterate the view page as the number of training got--%>
						<%
							List<EmployeeTrainingDetailsModel> savedTrainingList = (List<EmployeeTrainingDetailsModel>) request.getAttribute("savedTrainingList");
							System.out.println("savedTrainingList : "+savedTrainingList);
							int trainingIndex = 0;
							if(savedTrainingList !=null && savedTrainingList.size()>0){
								for(EmployeeTrainingDetailsModel trainingItem: savedTrainingList){
									++trainingIndex;
						%>
						<%@include file="../emp_training_details/training_details_view_item.jsp"%>
						<%} }%>

				</div>
			</div>



<%--			Card for Service History--%>
			<div class="card">
				<div class="card-header" role="tab" id="service_history_tab_item_header">
					<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_POSTING_AND_TRAINING_SERVICE_HISTORY, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="service_history_info_view">

					<%--					Iterate the view page as the number of service done--%>
					<%
						Logger logger = Logger.getLogger("employee_service_history_view_page");
						List<Employee_service_historyDTO> savedServiceModelList = (List<Employee_service_historyDTO>) request.getAttribute("savedServiceModelList");
						logger.debug("savedServiceModelList : "+savedServiceModelList);
						int serviceIndex = 0;
						if(savedServiceModelList !=null && savedServiceModelList.size()>0){
							for(Employee_service_historyDTO dto: savedServiceModelList){
								++serviceIndex;
					%>
					<%@include file="../employee_service_history/employee_service_history_view_item.jsp"%>
					<%} }%>

				</div>
			</div>


<%--			Card for Travel Information--%>
			<div class="card">
				<div class="card-header" role="tab" id="travel_info_tab_item_header">
					<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_TRAVEL_AND_CERTIFICATION_TRAVEL_INFO, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="travel_info_view">

					<%--					Iterate the view page as the number of place travelled--%>
						<%
							List<Emp_travel_detailsDtoWithValue> emp_travel_details = (List<Emp_travel_detailsDtoWithValue>) request.getAttribute("emp_travel_details");
							int travelIndex = 0;
							if (emp_travel_details != null && emp_travel_details.size() > 0) {
								for (Emp_travel_detailsDtoWithValue emp_travel_detail : emp_travel_details) {
									++travelIndex;
						%>
						<%@include file="../emp_travel_details/emp_travel_details_info_view_item.jsp"%>
						<%
								}
							}
						%>

				</div>
			</div>



<%--			Card for certificate Information--%>
			<div class="card">
				<div class="card-header" role="tab" id="certificate_info_tab_item_header">
					<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_TRAVEL_AND_CERTIFICATION_CERTIFICATION_INFO, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="certificate_info_view">

				<%--					Iterate the view page as the number of place travelled--%>
					<%
						List<Employee_certificationDTO> employee_certificationDtos = (List<Employee_certificationDTO>) request.getAttribute("employee_certificationDtos");
						int certificationIndex = 0;
						if (employee_certificationDtos != null && employee_certificationDtos.size() > 0) {
							for (Employee_certificationDTO employee_certificationDTO : employee_certificationDtos) {
								++certificationIndex;
					%>
					<%@include file="/employee_certification/employee_certification_info_view_item.jsp"%>
					<%
							}
						}
					%>
				</div>
			</div>



<%--		Card for Nominee Inoformation--%>
			<div class="card">
				<div class="card-header" role="tab" id="nominee_info_tab_item_header">
					<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_OTHERS_NOMINEE_INFO, loginDTO) %> </h2>
				</div>

				<div class="card-body" id="nominee_info_view">
					<%
						List<EmployeeNomineeModel> savedNomineeModelList = (List<EmployeeNomineeModel>) request.getAttribute("savedNomineeModelList");
						System.out.println("savedNomineeInfoModelList : " + savedNomineeModelList);
						int nomineeIndex = 0;
						if(savedNomineeModelList !=null && savedNomineeModelList.size()>0){
							for(EmployeeNomineeModel nomineeInfoModel: savedNomineeModelList){
								++nomineeIndex;
					%>
					<%@include file="/employee_nominee/employee_nominee_info_item.jsp"%>
					<%} }%>
				</div>
			</div>


<%--		Card for Award Information--%>
		<div class="card">
			<div class="card-header" role="tab" id="award_info_tab_item_header">
				<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_OTHERS_AWARD_INFO, loginDTO) %> </h2>
			</div>

			<div class="card-body" id="award_info_view">
				<%
					List<EmployeeAwardHonorModel> savedAwardModelList = (List<EmployeeAwardHonorModel>) request.getAttribute("savedAwardModelList");
					System.out.println("savedLanguageProficiencyList : "+savedAwardModelList);
					int awardIndex = 0;
					if(savedAwardModelList !=null && savedAwardModelList.size()>0){
						for(EmployeeAwardHonorModel awardHonorModel: savedAwardModelList){
							++awardIndex;
				%>
				<%@include file="/employee_honors_awards/employee_honor_awards_item.jsp"%>
				<%} }%>
			</div>
		</div>




<%--		Card for Banking Information--%>
		<div class="card">
			<div class="card-header" role="tab" id="banking_info_tab_item_header">
				<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_OTHERS_BANK_INFO, loginDTO) %> </h2>
			</div>

			<div class="card-body" id="banking_info_view">
				<%
					List<EmployeeBankInfoModel> savedBankInfoModelList = (List<EmployeeBankInfoModel>) request.getAttribute("savedBankInfoModelList");
					System.out.println("savedBankInfoModelList : "+savedBankInfoModelList);
					int bankInfoIndex = 0;
					if(savedBankInfoModelList !=null && savedBankInfoModelList.size()>0){
						for(EmployeeBankInfoModel bankInfoModel: savedBankInfoModelList){
							++bankInfoIndex;
				%>
				<%@include file="/employee_bank_information/employee_bank_info_item.jsp"%>
				<%} }%>
			</div>
		</div>



<%--		Card for Employee Leave Information--%>
		<div class="card">
			<div class="card-header" role="tab" id="leave_info_tab_item_header">
				<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_OTHERS_LEAVE_DETAILS, loginDTO) %> </h2>
			</div>

			<div class="card-body" id="leave_info_view">
				<%
					List<EmpLeaveDetails> savedEmpLeaveDetailsList = (List<EmpLeaveDetails>) request.getAttribute("savedEmpLeaveDetailsList");
					System.out.println("savedEmpLeaveDetailsList : "+savedEmpLeaveDetailsList);
					int leaveDetailsIndex = 0;
					if(savedEmpLeaveDetailsList !=null && savedEmpLeaveDetailsList.size()>0){
						for(EmpLeaveDetails leaveDetailsItem: savedEmpLeaveDetailsList){
							++leaveDetailsIndex;
				%>
				<%@include file="/employee_leave_details/employee_leave_details_view_item.jsp"%>
				<%} }%>
			</div>
		</div>


<%--		Card for Employee Disciplinary Details--%>
		<div class="card">
			<div class="card-header" role="tab" id="disciplinary_info_tab_item_header">
				<h2><i class="fa fa-clock-o"></i> <%= LM.getText(LC.HR_MANAGEMENT_OTHERS_DISCIPLINARY_DETAILS, loginDTO) %> </h2>
			</div>

			<div class="card-body" id="disciplinary_info_view">
				<%
					List<DisciplinaryDetailsModelWithLogModel> savedDisciplinaryList = (List<DisciplinaryDetailsModelWithLogModel>) request.getAttribute("savedDisciplinaryList");
					System.out.println("savedDisciplinaryList : "+savedDisciplinaryList);
					int disciplinaryDetailsIndex = 0;
					if(savedDisciplinaryList !=null && savedDisciplinaryList.size()>0){
						for(DisciplinaryDetailsModelWithLogModel disciplinaryDetailsItem: savedDisciplinaryList){
							++disciplinaryDetailsIndex;
				%>
				<%@include file="/disciplinary_details/disciplinary_details_view_item.jsp"%>
				<%} }%>
			</div>
		</div>


		</div>
	</div>
</div>

<%--Include the static asset here--%>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js"
		type="text/javascript"></script>
