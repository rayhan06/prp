<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_language_proficiency.*" %>
<%@page import="employee_service_history.*" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="religion.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="employee_family_info.Employee_family_infoDtoWithValue" %>
<%@ page import="employee_education_info.EmployeeEducationInfoDTOWithValue" %>
<%@ page import="emp_travel_details.Emp_travel_detailsDtoWithValue" %>
<%@ page import="employee_certification.Employee_certificationDTO" %>
<%@ page import="employee_honors_awards.EmployeeAwardHonorModel" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="nationality.NationalityRepository" %>
<%@ page import="employee_publication.Employee_publicationDTO" %>
<%@ page import="employee_publication.Employee_publicationDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="employee_posting.Employee_local_posting_model" %>
<%@ page import="employee_posting.Employee_postingDAO" %>
<%@ page import="employee_posting.Employee_postingDTO" %>
<%@ page import="promotion_history.Promotion_historyDTOWithValues" %>
<%@ page import="promotion_history.Promotion_historyDAO" %>
<%@ page import="disciplinary_details.DisciplinaryDetailsModelWithLogModel" %>
<%@ page import="disciplinary_details.Disciplinary_detailsDAO" %>
<%@ page import="complain_action.Complain_actionRepository" %>
<%@ page import="disciplinary_details.Disciplinary_detailsDTO" %>
<%@ page import="common.NameDTO" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="geolocation.*" %>
<%@ page import="training_calendar_details.TrainingCalendarDetailsShortInfo" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="election_wise_mp.Election_wise_mpDAO" %>
<%@ page import="election_wise_mp.Election_wise_mpDTO" %>
<%@ page import="election_wise_mp.Election_wise_mpRepository" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="election_constituency.Election_constituencyDTO" %>
<%@ page import="employee_vaccination_info.Employee_vaccination_infoDTO" %>
<%@ page import="employee_vaccination_info.Employee_vaccination_infoDAO" %>
<%@ page import="pb.Utils" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="employee_attachment.Employee_attachmentDTO" %>
<%@ page import="employee_office_report.InChargeLevelEnum" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.time.ZoneId" %>
<%@ page import="java.time.Period" %>
<%@ page import="promotion_history.Promotion_historyDTO" %>
<%!
    public String calculatePeriod(long startDate, long endDate, String language) {
    LocalDate serviceFrom = new Date(startDate).toInstant()
    .atZone(ZoneId.systemDefault())
    .toLocalDate();
    LocalDate servingTo = new Date(endDate).toInstant()
    .atZone(ZoneId.systemDefault())
    .toLocalDate();
    Period p = Period.between(serviceFrom, servingTo);
    String period = "";
    if ("English".equalsIgnoreCase(language)) {
    period = p.getYears() + "y " + p.getMonths() + "m " + p.getDays() + "d";
    } else {
    period = StringUtils.convertToBanNumber(p.getYears() + "ব " + p.getMonths() + "মা " + p.getDays() + "দি");
    }
    return period;
    }
%>
<%
    Promotion_historyDAO promotion_historyDAO = Promotion_historyDAO.getInstance();
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    //long id = Long.parseLong(request.getParameter("ID"));

    String context = request.getContextPath() + "/";

    Employee_recordsDTO employee_recordsDTO = (Employee_recordsDTO) request.getAttribute("employee_recordsDTO");

    List<Employee_family_infoDtoWithValue> employee_family_info = (List<Employee_family_infoDtoWithValue>) request.getAttribute("employee_family_info");

    List<EmployeeEducationInfoDTOWithValue> savedEductionInfoList = (List<EmployeeEducationInfoDTOWithValue>) request.getAttribute("savedEductionInfoList");

    List<EmployeeLanguageProficiencyModel> languageProficiencyModelList = (List<EmployeeLanguageProficiencyModel>) request.getAttribute("savedLanguageProficiencyList");

    List<TrainingCalendarDetailsShortInfo> savedTrainingModelList = (List<TrainingCalendarDetailsShortInfo>) request.getAttribute("savedTrainingList");

    List<EmployeeServiceModel> savedServiceModelList = (List<EmployeeServiceModel>) request.getAttribute("savedServiceModelList");

    List<Emp_travel_detailsDtoWithValue> savedTravelModelList = (List<Emp_travel_detailsDtoWithValue>) request.getAttribute("emp_travel_details");

    List<Employee_certificationDTO> savedCertificateModelList = (List<Employee_certificationDTO>) request.getAttribute("employee_certificationDtos");

    List<EmployeeAwardHonorModel> savedAwardModelList = (List<EmployeeAwardHonorModel>) request.getAttribute("savedAwardModelList");

    List<EmployeeOfficeDTO> employeeOfficeDTOList = (List<EmployeeOfficeDTO>) request.getAttribute("employeeOfficeDTOList");

    List<Employee_attachmentDTO> employeeAttachmentDTOList = (List<Employee_attachmentDTO>) request.getAttribute("employeeAttachmentDTOList");


    List<Employee_publicationDTO> employee_publicationDTOList =
            Employee_publicationDAO.getInstance()
                                   .getEmployeePublicationDetailsByEmployeeRecordsId(employee_recordsDTO.iD)
                                   .stream().map(info -> info.dto).collect(Collectors.toList());

    List<Employee_local_posting_model> localPostingModelList = Employee_postingDAO.getInstance().getAllLocalPostingModelByEmployeeId(employee_recordsDTO.iD);
    List<Employee_postingDTO> foreignPostingDTOList = Employee_postingDAO.getInstance().getForeignPostingDTOsByEmployeeId(employee_recordsDTO.iD);
    List<Promotion_historyDTOWithValues> promotionHistoryModelList = Promotion_historyDAO.getInstance().getAllPromotionDetailsByEmployeeRecordsId(employee_recordsDTO.iD);
    List<DisciplinaryDetailsModelWithLogModel> disciplinaryList = new Disciplinary_detailsDAO().getDisciplinaryListModel(employee_recordsDTO.iD);

    EmployeeOfficeDTO currentOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employee_recordsDTO.iD);
    Office_unitsDTO currentOfficeUnitsDTO;
    OfficeUnitOrganograms currectOfficeUnitOrganograms;
    if (currentOfficeDTO == null) {
        currentOfficeDTO = new EmployeeOfficeDTO();
        currentOfficeUnitsDTO = new Office_unitsDTO();
        currectOfficeUnitOrganograms = new OfficeUnitOrganograms();
    } else {
        currentOfficeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(currentOfficeDTO.officeUnitId);
        if (currentOfficeUnitsDTO == null) {
            currentOfficeUnitsDTO = new Office_unitsDTO();
        }
        currectOfficeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(currentOfficeDTO.officeUnitOrganogramId);
        if (currectOfficeUnitOrganograms == null) {
            currectOfficeUnitOrganograms = new OfficeUnitOrganograms();
        }
    }
    Election_wise_mpDTO election_wise_mpDTO = null;
    if (employee_recordsDTO.isMP != 2) {
        election_wise_mpDTO = Election_wise_mpRepository.getInstance().getDTOByID(employee_recordsDTO.electionWiseMpId);
    }
    List<Employee_vaccination_infoDTO> vaccinationInfoDTOs = Employee_vaccination_infoDAO.getInstance().getByEmployeeId(employee_recordsDTO.iD);
    vaccinationInfoDTOs.sort(Comparator.comparingInt(dto -> dto.doseNumber));
    boolean isLongPDS = !"shortPDS".equalsIgnoreCase(request.getParameter("pdsType"));
%>

<style>
    td, th {
        font-size: 1.3rem;
    }

    @media print {
        td, th {
            font-size: 1.6rem;
        }
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-auto mr-2 mt-4">
    <%--Print Button--%>
    <button type="button" class="btn" id='printer2'
            onclick="printDivWithJqueryPrint('record-summary')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
        <%--        <%=LM.getText(LC.HM_PRINT, loginDTO)%>--%>
    </button>
</div>
<!-- end:: Subheader -->

<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="kt-portlet" style="background-color: #f2f2f2 !important;">
        <div class="kt-portlet__body m-2">
            <section style="background-color: white !important; padding: .75rem;" id="record-summary">
                <div class="row mt-1">
                    <div class="col-12 px-0">
                        <div class="text-center">
                            <img
                                    width="10%"
                                    src="<%=context%>assets/static/parliament_logo.png"
                                    alt="logo"
                                    class="logo-default"
                            />
                        </div>
                        <div class="text-center mt-3">
                            <h2>
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4>
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERSONAL_DATA_SHEET, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>
                <%
                    if (employee_recordsDTO.isMP != 2) {
                %>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= isLanguageEnglish ? "Constituency Details" : "নির্বাচনী এলাকার বিবরণ" %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-borderless" style="border: 1px black solid">
                                <tr>
                                    <td class="font-weight-bold">
                                        <%= isLanguageEnglish ? "Constituency Number" : "আসন সংখ্যা" %>
                                    </td>
                                    <%
                                        if (election_wise_mpDTO != null) {
                                            Election_constituencyDTO election_constituencyDTO = Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(election_wise_mpDTO.electionConstituencyId);
                                    %>
                                    <%
                                        if (election_constituencyDTO != null) {

                                            if (election_constituencyDTO.isReserved == 0) {
                                    %>
                                    <td>
                                        <%=isLanguageEnglish ? election_constituencyDTO.constituencyNumber : StringUtils.convertToBanNumber(String.valueOf(election_constituencyDTO.constituencyNumber)) %>
                                    </td>
                                    <%
                                    } else {
                                    %>
                                    <td>
                                        <%=isLanguageEnglish ? "Reserved-" + election_constituencyDTO.constituencyNumber : "সংরক্ষিত-" + StringUtils.convertToBanNumber(String.valueOf(election_constituencyDTO.constituencyNumber)) %>
                                    </td>
                                    <%
                                                }
                                            }
                                        }
                                    %>

                                    <td class="font-weight-bold">
                                        <%= isLanguageEnglish ? "Election constituency" : "নির্বাচনী এলাকা" %>
                                    </td>
                                    <td>
                                        <%=election_wise_mpDTO != null ? Election_constituencyRepository.getInstance().getText(election_wise_mpDTO.electionConstituencyId, Language) : ""%>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="font-weight-bold">
                                        <%= isLanguageEnglish ? "Political Party" : "রাজনৈতিক দল" %>
                                    </td>
                                    <td>
                                        <%=election_wise_mpDTO != null ? Political_partyRepository.getInstance().getText(election_wise_mpDTO.politicalPartyId, Language) : ""%>
                                    </td>
                                    <td class="font-weight-bold">
                                        <%= isLanguageEnglish ? "Election Date" : "নির্বাচনের তারিখ" %>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, election_wise_mpDTO != null ? election_wise_mpDTO.electionDate : SessionConstants.MIN_DATE)%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">
                                        <%= isLanguageEnglish ? "Gazette Date" : "গেজেটের তারিখ" %>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, election_wise_mpDTO != null ? election_wise_mpDTO.mpGazetteDate : SessionConstants.MIN_DATE)%>
                                    </td>
                                    <td class="font-weight-bold">
                                        <%= isLanguageEnglish ? "Oath Date" : "শপথ গ্রহণের তারিখ" %>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, election_wise_mpDTO != null ? election_wise_mpDTO.oathDate : SessionConstants.MIN_DATE)%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%= isLanguageEnglish ? "Term" : "টার্ম" %>
                                    </td>
                                    <td>
                                        <%=StringUtils.convertIntToString(Language, employee_recordsDTO.mpElectedCount)%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>
                <%--Personal Info--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTO) %>
                            </u>
                        </h3>
                        <div style="border: 1px black solid">
                            <table class="table table-borderless" id="personal_info_tbl_1">
                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_EMPLOYEE_NUMBER, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=employee_recordsDTO.employeeNumber != null ? (isLanguageEnglish ? employee_recordsDTO.employeeNumber : StringUtils.convertToBanNumber(employee_recordsDTO.employeeNumber)) : ""%>
                                    </td>
                                    <td></td>
                                    <td style="text-align: right;" rowspan="5">
                                        <%
                                            if (employee_recordsDTO.photo != null) {
                                        %>
                                        <img
                                                src='data:image/jpg;base64,<%=new String(Base64.encodeBase64(employee_recordsDTO.photo))%>'
                                                id="photo-img"
                                                class="img-fluid w-50"
                                        >
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
                                    </td>
                                    <td><%=employee_recordsDTO.nameEng%>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
                                    </td>
                                    <td><%=employee_recordsDTO.nameBng%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=isLanguageEnglish ? LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")
                                                             : LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEBNG, loginDTO).replaceAll("\\(.+\\)", "")%>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? employee_recordsDTO.fatherNameEng
                                                             : employee_recordsDTO.fatherNameBng%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=isLanguageEnglish ? LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")
                                                             : LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEBNG, loginDTO).replaceAll("\\(.+\\)", "")%>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? employee_recordsDTO.motherNameEng
                                                             : employee_recordsDTO.motherNameBng%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_DATEOFBIRTH, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, employee_recordsDTO.dateOfBirth)%>
                                    </td>

                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, employee_recordsDTO.nid)%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_BIRTH_DISTRICT, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=GeoDistrictRepository.getInstance().getText(Language, employee_recordsDTO.birthDistrict)%>
                                    </td>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_BLOODGROUP, loginDTO)%>
                                    </td>
                                    <td><%=CatRepository.getInstance().getText(Language, "blood_group", employee_recordsDTO.bloodGroup)%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_GENDER, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=CatRepository.getInstance().getText(Language, "gender", employee_recordsDTO.gender)%>
                                    </td>

                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NATIONALITY, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=NationalityRepository.getInstance().getText(Language, employee_recordsDTO.nationalityType)%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=GeoLocationRepository.getInstance().getText(Language, employee_recordsDTO.homeDistrict)%>
                                    </td>

                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FREEDOM_FIGHTER, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=employee_recordsDTO.freedomFighterInfo == null ? "" :
                                           StringUtils.getYesNo(Language, employee_recordsDTO.freedomFighterInfo.contains("1"))%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MARITALSTATUS, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=CatRepository.getInstance().getText(Language, "marital_status", employee_recordsDTO.maritalStatus)%>
                                    </td>

                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CHIND_GRAND_CHIND_OF_FREEDOM_FIGHTER, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=employee_recordsDTO.freedomFighterInfo == null ? "" : StringUtils.getYesNo(
                                                Language, employee_recordsDTO.freedomFighterInfo.contains("2")
                                                          || employee_recordsDTO.freedomFighterInfo.contains("3")
                                        )%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_RELIGION, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=ReligionRepository.getInstance().getText(Language, employee_recordsDTO.religion)%>
                                    </td>
                                    <%
                                        if (employee_recordsDTO.isMP != 2) {
                                    %>
                                    <td class="font-weight-bold">
                                        <%=isLanguageEnglish ? "Profession" : "পেশা"%>
                                    </td>
                                    <td>
                                        <%=election_wise_mpDTO != null ? election_wise_mpDTO.professionOfMP : ""%>
                                    </td>
                                    <%
                                        }
                                    %>
                                </tr>

                                <%
                                    if (employee_recordsDTO.isMP == 2) {
                                %>
                                <tr>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_FRONT_SIDE, loginDTO)%>
                                    </td>
                                    <td>
                                        <%
                                            if (employee_recordsDTO.nidFrontPhoto != null) {
                                        %>
                                        <img src='data:image/jpg;base64,<%=new String(Base64.encodeBase64(employee_recordsDTO.nidFrontPhoto))%>'
                                             class="img-fluid w-50"
                                             id="nid-front-img"
                                        >
                                        <%
                                            }
                                        %>
                                    </td>
                                    <td class="font-weight-bold">
                                        <%=LM.getText(LC.HR_MANAGEMENT_PERSONAL_INFO_NID_BACK_SIDE, loginDTO)%>
                                    </td>
                                    <td>
                                        <%
                                            if (employee_recordsDTO.nidBackPhoto != null) {
                                        %>
                                        <img src='data:image/jpg;base64,<%=new String(Base64.encodeBase64(employee_recordsDTO.nidBackPhoto))%>'
                                             id="nid-back-img"
                                             class="img-fluid w-50"
                                        >
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">
                                        <%=isLanguageEnglish ? "Quota" : "কোটা"%>
                                    </td>
                                    <td><%=CatRepository.getInstance().getText(Language, "job_quota", employee_recordsDTO.jobQuota)%>
                                    </td>

                                </tr>
                                <%
                                    }
                                %>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    if (employee_recordsDTO.isMP == 2) {
                %>
                <%--Service Info--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_SERVICE_INFORMATION, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-borderless" style="border: 1px black solid">

                                <tr>
                                    <td class="font-weight-bold">
                                        <%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO) %>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? currentOfficeUnitsDTO.unitNameEng
                                                             : currentOfficeUnitsDTO.unitNameBng%>
                                    </td>

                                    <td class="font-weight-bold">
                                        <%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_JOININGDATE, loginDTO) %>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, employee_recordsDTO.joiningDate)%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">
                                        <%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO) %>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? currectOfficeUnitOrganograms.designation_eng
                                                             : currectOfficeUnitOrganograms.designation_bng%>
                                    </td>

                                    <td class="font-weight-bold">
                                        <%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_LPR_DATE, loginDTO) %>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, employee_recordsDTO.lprDate)%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>
                <%--Family Info--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_FAMILY_MEMBER, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        <%=LM.getText(LC.USER_ADD_FULL_NAME, loginDTO)%>
                                    </th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_RELATIONTYPE, loginDTO)%>
                                    </b></th>
                                    <th>
                                        <b>
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_PROFESSIONTYPE, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b>
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_DOB, loginDTO)%>
                                        </b>
                                    </th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_HOMEDISTRICT, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_MOBILENUMBER1, loginDTO)%>
                                    </b></th>
                                </tr>
                                </thead>

                                <tbody>
                                <% for (Employee_family_infoDtoWithValue familyMember : employee_family_info) {%>
                                <tr>
                                    <td>
                                        <%=isLanguageEnglish ? familyMember.dto.firstNameEn : familyMember.dto.firstNameBn%>
                                    </td>
                                    <td><%=isLanguageEnglish ? familyMember.relationEng : familyMember.relationBng%>
                                    </td>
                                    <td><%=isLanguageEnglish ? familyMember.professionEng : familyMember.professionBng%>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, familyMember.dto.dob)%>
                                    </td>
                                    <td>
                                        <%=familyMember.dto.homeDistrict%>
                                    </td>
                                    <td><%= familyMember.dto.mobileNumber1 != null && familyMember.dto.mobileNumber1.length() > 2 ? (isLanguageEnglish ?
                                                                                                                                     (familyMember.dto.mobileNumber1).substring(2) : (StringUtils.convertToBanNumber(familyMember.dto.mobileNumber1).substring(2))) : ""%>
                                    </td>
                                </tr>
                                <%}%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <%--Education Info--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= isLanguageEnglish ? "Vaccine Information" : "ভ্যাক্সিন তথ্য" %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped" id="vaccine_info_tbl">
                                <thead>
                                <tr>
                                    <th>
                                        <b>
                                            <%=isLanguageEnglish ? "Dose Number" : "কত তম ডোস"%>
                                        </b>
                                    </th>
                                    <th>
                                        <b>
                                            <%=isLanguageEnglish ? "Date of Vaccination" : "ভ্যাক্সিন গ্রহণের তারিখ"%>
                                        </b>
                                    </th>
                                    <th>
                                        <b>
                                            <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINENAME, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b>
                                            <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_CERTIFICATENUMBER, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b>
                                            <%=LM.getText(LC.EMPLOYEE_VACCINATION_INFO_ADD_VACCINECENTER, loginDTO)%>
                                        </b>
                                    </th>
                                </tr>
                                </thead>
                                <%
                                    if (vaccinationInfoDTOs != null) {
                                        for (Employee_vaccination_infoDTO vaccinationInfoDTO : vaccinationInfoDTOs) {
                                %>
                                <tr>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%=Utils.getDigits(vaccinationInfoDTO.doseNumber, Language)%>
                                    </td>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, vaccinationInfoDTO.doseTime), Language)%>
                                    </td>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%=CatRepository.getInstance().getText(Language, "covid_vaccine", vaccinationInfoDTO.vaccineName)%>
                                    </td>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%=vaccinationInfoDTO.certificateNumber%>
                                    </td>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%=vaccinationInfoDTO.vaccineCenter%>
                                    </td>
                                </tr>
                                <%
                                        }
                                    }
                                %>
                            </table>
                        </div>
                    </div>
                </div>
                <%--Education Info--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_EDUCATIONAL_INFORMATION, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped" id="academic_info_tbl">
                                <thead>
                                <tr>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_INSTITUTIONNAME, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MAJOR, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO)%>
                                    </b></th>
                                    <th>
                                        <b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_YEAROFPASSINGNUMBER, loginDTO)%>
                                        </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_RESULTEXAMTYPE, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_CGPANUMBER, loginDTO)%>
                                    </b></th>
                                </tr>
                                </thead>
                                <% for (EmployeeEducationInfoDTOWithValue eduInfoItem : savedEductionInfoList) {%>
                                <tr>
                                    <td><%=eduInfoItem.dto.institutionName%>
                                    </td>
                                    <td><%=eduInfoItem.dto.major%>
                                    </td>
                                    <td><%=isLanguageEnglish ? eduInfoItem.degreeExamEng : eduInfoItem.degreeExamBan%>
                                    </td>
                                    <td><%=eduInfoItem.dto.yearOfPassingNumber > 0 ? (isLanguageEnglish ? eduInfoItem.dto.yearOfPassingNumber : eduInfoItem.passingYearBan) : ""%>
                                    </td>
                                    <td><%=isLanguageEnglish ? eduInfoItem.resultExamEng : eduInfoItem.resultExamBan%>
                                    </td>
                                    <td>
                                        <%=eduInfoItem.dto.resultExamType == 4 ? (isLanguageEnglish ? eduInfoItem.dto.cgpaNumber : eduInfoItem.gradePointBan) : ""%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>

                <%--Language Info--%>
                <% if (isLongPDS) {%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_LINGUISTIC_INFORMATION, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_LANGUAGECAT, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_READSKILLCAT, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_WRITESKILLCAT, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_SPEAKSKILLCAT, loginDTO)%>
                                    </b></th>
                                </tr>
                                </thead>
                                <% for (EmployeeLanguageProficiencyModel model : languageProficiencyModelList) {%>
                                <tr>
                                    <td><%=isLanguageEnglish ? model.languageEng : model.languageBan%>
                                    </td>
                                    <td><%=isLanguageEnglish ? model.readSkillEng : model.readSkillBan%>
                                    </td>
                                    <td><%=isLanguageEnglish ? model.writeSkillEng : model.writeSkillBan%>
                                    </td>
                                    <td><%=isLanguageEnglish ? model.speakSkillEng : model.speakSkillBan%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>

                <%--Contact Info--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.EMPLOYEE_RECORDS_ADD_CONTACT_INFORMATION, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <td>
                                        <%=isLanguageEnglish ? LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_ENG, loginDTO).replaceAll("\\(.+\\)", "")
                                                             : LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_BNG, loginDTO).replaceAll("\\(.+\\)", "")%>
                                    </td>
                                    <td colspan="3">
                                        <%=isLanguageEnglish ? GeoLocationUtils.getGeoLocationString(employee_recordsDTO.presentAddress, "English")
                                                             : GeoLocationUtils.getGeoLocationString(employee_recordsDTO.presentAddressBng, "Bangla")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%=isLanguageEnglish ? LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_ENG, loginDTO).replaceAll("\\(.+\\)", "")
                                                             : LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_BNG, loginDTO).replaceAll("\\(.+\\)", "")%>
                                    </td>
                                    <td colspan="3">
                                        <%=isLanguageEnglish ? GeoLocationUtils.getGeoLocationString(employee_recordsDTO.permanentAddress, "English")
                                                             : GeoLocationUtils.getGeoLocationString(employee_recordsDTO.permanentAddressBng, "Bangla")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_NUMBER, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=employee_recordsDTO.officePhoneNumber == null ? "" :
                                           isLanguageEnglish ? employee_recordsDTO.officePhoneNumber : StringUtils.convertToBanNumber(employee_recordsDTO.officePhoneNumber)%>
                                    </td>
                                    <td>
                                        <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_EXTENSION, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=employee_recordsDTO.officePhoneExtension == null ? "" :
                                           isLanguageEnglish ? employee_recordsDTO.officePhoneExtension : StringUtils.convertToBanNumber(employee_recordsDTO.officePhoneExtension)%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%=LM.getText(LC.EMPLOYEE_PERSONAL_MOBILE_NUMBER, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? employee_recordsDTO.personalMobile : StringUtils.convertToBanNumber(employee_recordsDTO.personalMobile)%>
                                    </td>
                                    <td>
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_ALTERNATIVEMOBILE, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? employee_recordsDTO.alternativeMobile : StringUtils.convertToBanNumber(employee_recordsDTO.alternativeMobile)%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERSONALEMAIL, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=employee_recordsDTO.personalEml == null ? "" : employee_recordsDTO.personalEml%>
                                    </td>
                                    <td>
                                        <%=Language.equals("English") ? "Official Email" : "অফিশিয়াল ইমেইল"%>
                                    </td>
                                    <td>
                                        <%=employee_recordsDTO.officialEml == null ? "" : employee_recordsDTO.officialEml%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_FAX_NUMBER, loginDTO)%>
                                    </td>
                                    <td>
                                        <%=employee_recordsDTO.faxNumber == null ? "" : employee_recordsDTO.faxNumber%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <% } %>


                <%--Local Training--%>
                <%--Foreign Traininig--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.HR_MANAGEMENT_POSTING_AND_TRAINING_TRAINING_INFO, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        <b><%=LM.getText(LC.DRAWING_DOCUMENTS_EDIT_DRAWING_DOCUMENTS_DETAILS_TITLE, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b><%=LM.getText(LC.TRAINING_CALENDER_EDIT_INSTITUTENAME, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b><%=LM.getText(LC.TRAINING_CALENDER_EDIT_STARTDATE, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ENDDATE, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ARRANGEDBY, loginDTO)%>
                                        </b>
                                    </th>
                                </tr>
                                </thead>
                                <% for (TrainingCalendarDetailsShortInfo model : savedTrainingModelList) {%>
                                <tr>
                                    <td>
                                        <%=isLanguageEnglish ? model.trainingCalenderDTO.nameEn
                                                             : model.trainingCalenderDTO.nameBn%>
                                    </td>
                                    <td>
                                        <%=model.trainingCalenderDTO.instituteName%>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, model.trainingCalenderDTO.startDate)%>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, model.trainingCalenderDTO.endDate)%>
                                    </td>
                                    <td>
                                        <%=CatRepository.getInstance().getText(Language, "training_arrange_by", model.trainingCalenderDTO.arrangedBy)%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>

                <% if (isLongPDS) {%>
                <%--Foreign Travel--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.HR_MANAGEMENT_TRAVEL_AND_CERTIFICATION_TRAVEL_INFO, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELTYPECAT, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELPURPOSECAT, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELDESTINATION, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_PLANNEDCITIES, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_DEPARTUREDATE, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_RETURNDATE, loginDTO)%>
                                    </b></th>
                                </tr>
                                </thead>
                                <% for (Emp_travel_detailsDtoWithValue model : savedTravelModelList) {%>
                                <tr>
                                    <td><%=isLanguageEnglish ? model.travelTypeEng : model.travelTypeBng%>
                                    </td>
                                    <td><%=isLanguageEnglish ? model.travelPurposeEng : model.travelPurposeBng%>
                                    </td>
                                    <td><%=isLanguageEnglish ? model.travelDestinationEng : model.travelDestinationBng%>
                                    </td>
                                    <td><%=model.dto.plannedCities%>
                                    </td>
                                    <td><%=isLanguageEnglish ? model.departureDateEng : model.departureDateBng%>
                                    </td>
                                    <td><%=isLanguageEnglish ? model.returnDateEng : model.returnDateBng%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>

                <%--Pulication--%>
                <div class="mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.EMPLOYEE_PUBLICATION_SEARCH_ANYFIELD, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_PUBLICATIONTITLE, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_PUBLICATIONCAT, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_BRIEFDESCRIPTION, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_PUBLICATIONDATE, loginDTO)%>
                                    </b></th>
                                </tr>
                                </thead>
                                <% for (Employee_publicationDTO employee_publicationDTO : employee_publicationDTOList) {%>
                                <tr>
                                    <td>
                                        <%=employee_publicationDTO.publicationTitle%>
                                    </td>
                                    <td>
                                        <%=CatRepository.getInstance().getText(Language, "publication", employee_publicationDTO.publicationCat)%>
                                    </td>
                                    <td>
                                        <%=employee_publicationDTO.briefDescription%>
                                    </td>
                                    <td>
                                        <%=StringUtils.getFormattedDate(Language, employee_publicationDTO.publicationDate)%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>

                <%--Honors & Awards--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.HR_MANAGEMENT_OTHERS_AWARD_INFO, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_TITLESOFAWARD, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_GROUND, loginDTO)%>
                                        </b>
                                    </th>
                                    <th>
                                        <b><%=LM.getText(LC.EMPLOYEE_HONORS_AWARDS_EDIT_AWARDDATE, loginDTO)%>
                                        </b>
                                    </th>
                                </tr>
                                </thead>
                                <% for (EmployeeAwardHonorModel model : savedAwardModelList) {%>
                                <tr>
                                    <td>
                                        <%=model.titlesOfAward%>
                                    </td>
                                    <td>
                                        <%=model.ground%>
                                    </td>
                                    <td>
                                        <%=isLanguageEnglish ? model.awardDateEng : model.awardDateBan%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>

                <%--Additional Quallification--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.HR_MANAGEMENT_TRAVEL_AND_CERTIFICATION_CERTIFICATION_INFO, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CERTIFICATIONNAME, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGORGANIZATION, loginDTO)%>
                                    </b></th>
                                    <th>
                                        <b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGINSTITUTEADDRESS, loginDTO)%>
                                        </b></th>
                                    <th style="min-width: 90px">
                                        <b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDFROM, loginDTO)%>
                                        </b></th>
                                    <th style="min-width: 90px">
                                        <b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDUPTO, loginDTO)%>
                                        </b></th>
                                </tr>
                                </thead>
                                <% for (Employee_certificationDTO model : savedCertificateModelList) {%>
                                <tr>
                                    <td><%=model.certificationName%>
                                    </td>
                                    <td><%=model.issuingOrganization%>
                                    </td>
                                    <td><%=model.issuingInstituteAddress%>
                                    </td>
                                    <td style="min-width: 90px"><%=isLanguageEnglish ? model.validFrom : StringUtils.convertToBanNumber(model.validFrom)%>
                                    </td>
                                    <td style="min-width: 90px"><%=isLanguageEnglish ? model.validUpto : StringUtils.convertToBanNumber(model.validUpto)%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>

                <%--Disciplinary Action--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARY_ACTION_ADD_FORMNAME, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        <b><%= LM.getText(LC.DISCIPLINARY_LOG_ADD_INCIDENTTYPE, loginDTO) %>
                                        </b>
                                    </th>
                                    <th>
                                        <b><%= LM.getText(LC.DISCIPLINARY_DETAILS_ADD_COMPLAINACTIONTYPE, loginDTO) %>
                                        </b>
                                    </th>
                                    <th>
                                        <b><%= LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINTRESOLVEDDATE, loginDTO) %>
                                        </b>
                                    </th>
                                    <th>
                                        <%=LM.getText(LC.DISCIPLINARY_DETAILS_ADD_REMARKS, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <% for (DisciplinaryDetailsModelWithLogModel disciplinaryDetailsItem : disciplinaryList) {%>
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <%=isLanguageEnglish ? disciplinaryDetailsItem.logModel.incidentEngText
                                                             : disciplinaryDetailsItem.logModel.incidentBangText%>
                                    </td>
                                    <td style="vertical-align: middle;">

                                        <%
                                            Disciplinary_detailsDTO disciplinary_detailsDTO = disciplinaryDetailsItem.detailsModel.dto;
                                            NameDTO actionDTO = Complain_actionRepository.getInstance().getDTOByID(disciplinary_detailsDTO.complainActionType);
                                            if (actionDTO != null) {
                                                String actionTaken = "";
                                                if (actionDTO.nameEn.equalsIgnoreCase("Other")) {
                                                    actionTaken = disciplinary_detailsDTO.complianOther;
                                                } else {
                                                    actionTaken = StringUtils.getFormattedDate(Language, disciplinary_detailsDTO.fromDate)
                                                                  + (isLanguageEnglish ? " To " : " থেকে ")
                                                                  + StringUtils.getFormattedDate(Language, disciplinary_detailsDTO.toDate);
                                                }
                                        %>
                                        <%=Complain_actionRepository.getInstance().getText(Language, disciplinary_detailsDTO.complainActionType)%>
                                        (<%=actionTaken%>)
                                        <%
                                            }
                                        %>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=isLanguageEnglish ? disciplinaryDetailsItem.logModel.complaintResolvedDateEng
                                                             : disciplinaryDetailsItem.logModel.complaintResolvedDateBan%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=disciplinaryDetailsItem.detailsModel.dto.remarks%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>

                <%--Other Services--%>
                <%--Service History--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.HR_MANAGEMENT_POSTING_AND_TRAINING_SERVICE_HISTORY, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_OFFICE, loginDTO)%>
                                        </b></th>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DEPARTMENT, loginDTO)%>
                                        </b></th>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DESIGNATION, loginDTO)%>
                                        </b></th>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.GLOBAL_FROM_DATE, loginDTO)%>
                                        </b></th>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.GLOBAL_TO_DATE, loginDTO)%>
                                        </b></th>
                                </tr>
                                </thead>
                                <% for (EmployeeServiceModel model : savedServiceModelList) {%>
                                <tr>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%=model.dto.office%>
                                    </td>

                                    <td style="vertical-align: middle;text-align: left">
                                        <%=model.dto.department%>
                                    </td>

                                    <td style="vertical-align: middle;text-align: left">
                                        <%=model.dto.designation%>
                                    </td>

                                    <td style="vertical-align: middle;text-align: left">
                                        <%=StringUtils.getFormattedDate(Language, model.dto.servingFrom)%>
                                    </td>

                                    <td style="vertical-align: middle;text-align: left">
                                        <%=StringUtils.getFormattedDate(Language, model.dto.servingTo)%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.HR_MANAGEMENT_POSTING_AND_TRAINING_PARLIAMENT_SERVICE_HISTORY, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENTOFFICEUNITID, loginDTO)%>
                                        </b></th>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_DESIGNATION, loginDTO)%>
                                        </b></th>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_INCHARGELABEL, loginDTO)%>
                                        </b></th>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_SERVINGFROM, loginDTO)%>
                                        </b></th>
                                    <th style="vertical-align: middle;text-align: left">
                                        <b><%=LM.getText(LC.EMPLOYEE_HISTORY_ADD_LASTOFFICEDATE, loginDTO)%>
                                        </b></th>

                                    <th style="vertical-align: middle;text-align: left"><b><%=isLanguageEnglish ? "Period" : "সময়কাল"%>
                                    </b></th>
                                    <th style="vertical-align: middle;text-align: left"><b><%=isLanguageEnglish ? "Previous Post" : "পূর্বের পদ"%>
                                    </b></th>
                                </tr>
                                </thead>
                                <tbody>
                                <%
                                    int index = 0;
                                    for (EmployeeOfficeDTO employeeOfficeDTO : employeeOfficeDTOList) {
                                %>
                                <tr>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%=isLanguageEnglish ? employeeOfficeDTO.unitNameEn : employeeOfficeDTO.unitNameBn%>
                                    </td>

                                    <td style="vertical-align: middle;text-align: left">
                                        <%=isLanguageEnglish ? employeeOfficeDTO.designationEn : employeeOfficeDTO.designationBn%>
                                    </td>

                                    <td style="vertical-align: middle;text-align: left">
                                        <%=InChargeLevelEnum.getTextByValue(Language, employeeOfficeDTO.inchargeLabel)%>
                                    </td>

                                    <td style="vertical-align: middle;text-align: left">
                                        <%=StringUtils.getFormattedDate(Language, employeeOfficeDTO.joiningDate)%>
                                    </td>

                                    <td style="vertical-align: middle;text-align: left">
                                        <%
                                            String lastOfficeSate = StringUtils.getFormattedDate(Language, employeeOfficeDTO.lastOfficeDate);
                                            if ("".equals(lastOfficeSate)) {
                                                lastOfficeSate = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISCURRENTLYWORKING, loginDTO);
                                            }
                                        %>
                                        <%=lastOfficeSate%>
                                    </td>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%
                                            if (employeeOfficeDTO.lastOfficeDate != SessionConstants.MIN_DATE) {
                                        %>
                                        <%=calculatePeriod(employeeOfficeDTO.joiningDate, employeeOfficeDTO.lastOfficeDate, Language)%>
                                        <%} else {%>
                                        -
                                        <%}%>
                                    </td>

                                    <td>
                                        <%if(employeeOfficeDTO.promotionHistoryId != -1)
                                        {
                                            Promotion_historyDTO promotion_historyDTO = promotion_historyDAO.getDTOByID(employeeOfficeDTO.promotionHistoryId);
                                            if(promotion_historyDTO != null)
                                            {
                                                EmployeeOfficeDTO dto = EmployeeOfficeRepository.getInstance().getById(promotion_historyDTO.previousEmployeeOfficeId);
                                                if(dto != null)
                                                {

                                                    if(isLanguageEnglish)
                                                    {
                                        %>
                                        <%=dto.unitNameEn%>, <%=dto.designationEn%>
                                        <%
                                        }
                                        else
                                        {
                                        %>
                                        <%=dto.unitNameBn%>, <%=dto.designationBn%>
                                        <%
                                                        }

                                                    }
                                                }
                                            }
                                        %>
                                    </td>
                                </tr>
                                <%}%>

                                <%
                                    for (Employee_attachmentDTO employee_attachmentDTO : employeeAttachmentDTOList) {
                                %>
                                <tr>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%=Office_unitsRepository.getInstance().geText(Language, employee_attachmentDTO.officeUnitId)%>
                                    </td>
                                    <td></td>
                                    <td><%=isLanguageEnglish ? "Attachment Responsibility" : "সংযুক্তি দায়িত্ব"%>
                                    </td>
                                    <td><%=StringUtils.getFormattedDate(Language, employee_attachmentDTO.startDate)%>
                                    </td>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%
                                            String lastOfficeSate = StringUtils.getFormattedDate(Language, employee_attachmentDTO.endDate);
                                            if ("".equals(lastOfficeSate)) {
                                                lastOfficeSate = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_ISCURRENTLYWORKING, loginDTO);
                                            }
                                        %>
                                        <%=lastOfficeSate%>
                                    </td>
                                    <td style="vertical-align: middle;text-align: left">
                                        <%
                                            if (employee_attachmentDTO.endDate != SessionConstants.MIN_DATE) {
                                        %>
                                        <%=calculatePeriod(employee_attachmentDTO.startDate, employee_attachmentDTO.endDate, Language)%>
                                        <%} else {%>
                                        -
                                        <%}%>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    if (employee_recordsDTO.isMP == 2) {
                %>
                <%--Promotion--%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.PROMOTION_HISTORY_INFO_PROMOTION_HISTORY, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        <%= LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO) %>
                                    </th>
                                    <th>
                                        <%= LM.getText(LC.PROMOTION_NEW_DESIGNATION, loginDTO) %>
                                    </th>
                                    <th>
                                        <%= LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONDATE, loginDTO) %>
                                    </th>
                                    <th>
                                        <%= LM.getText(LC.PROMOTION_HISTORY_ADD_GODATE, loginDTO) %>
                                    </th>
                                    <th>
                                        <%= LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONNATURECAT, loginDTO) %>
                                    </th>
                                </tr>
                                </thead>
                                <% for (Promotion_historyDTOWithValues promotion_history_detail : promotionHistoryModelList) {%>
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <%=isLanguageEnglish ? promotion_history_detail.newOfficeUnitEn : promotion_history_detail.newOfficeUnitBn%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=isLanguageEnglish ? promotion_history_detail.newDesignationEn : promotion_history_detail.newDesignationBn%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=isLanguageEnglish ? promotion_history_detail.promotionDateEn : promotion_history_detail.promotionDateBn%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=StringUtils.getFormattedDate(Language, promotion_history_detail.dto.gODate)%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=isLanguageEnglish ? promotion_history_detail.promotionNatureTypeEn : promotion_history_detail.promotionNatureTypeBn%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                        }
                    }
                    if (employee_recordsDTO.isMP == 2) {
                %>
                <%--Posting Abroad--%>
                <div class="mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.EMPLOYEE_POSTING_ADD_FOREIGNPOSTING, loginDTO) %>
                            </u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%>
                                    </th>
                                    <th>
                                        <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ORGANIZATION, loginDTO)%>
                                    </th>
                                    <th>
                                        <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO)%>
                                    </th>
                                    <th>
                                        <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGFROM, loginDTO)%>
                                    </th>
                                    <th>
                                        <%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGTO, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <% for (Employee_postingDTO foreignPostingItem : foreignPostingDTOList) {%>
                                <tr>
                                    <td style="vertical-align: middle;">
                                        <%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, foreignPostingItem.designation)%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=foreignPostingItem.organization%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=GeoCountryRepository.getInstance().getText(Language, foreignPostingItem.country)%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=StringUtils.getFormattedDate(Language, foreignPostingItem.postingFrom)%>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <%=StringUtils.getFormattedDate(Language, foreignPostingItem.postingTo)%>
                                    </td>
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>
                <%--Posting Record--%>
                <%--<% if(isLongPDS) {%>
                <div class=" mt-5">
                    <div class="">
                        <h3 class="table-title mb-3">
                            <u><%= LM.getText(LC.EMPLOYEE_POSTING_ADD_LOCALPOSTING, loginDTO) %></u>
                        </h3>
                        <div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POST, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_DESIGNATION, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_PLACE, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGFROM, loginDTO)%>
                                    </b></th>
                                    <th><b><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_POSTINGTO, loginDTO)%>
                                    </b></th>
                                </tr>
                                </thead>
                                <% for (Employee_local_posting_model localPostingItem : localPostingModelList) {%>
                                <tr>

                                    <td style="vertical-align: middle;"><%=isLanguageEnglish ? localPostingItem.postEng : localPostingItem.postBng%>
                                    </td>
                                    <td style="vertical-align: middle;"><%=isLanguageEnglish ? localPostingItem.designationEng : localPostingItem.designationBng%>
                                    </td>
                                    <td style="vertical-align: middle;"><%=GeoDistrictRepository.getInstance().getText(Language, localPostingItem.dto.place)%>
                                    </td>
                                    <td style="vertical-align: middle;"><%=isLanguageEnglish ?
                                                                           localPostingItem.postingFromEng : localPostingItem.postingFromBng%>
                                    </td>
                                    <td style="vertical-align: middle;"><%=isLanguageEnglish ? localPostingItem.postingToEng : localPostingItem.postingToBng%>
                                    </td>
                                    &lt;%&ndash;<td style="vertical-align: middle;"><%=isLanguageEnglish ? localPostingItem.payScaleEng : localPostingItem.payScaleBng%>&ndash;%&gt;
                                </tr>
                                <%} %>
                            </table>
                        </div>
                    </div>
                </div>
                <%}%>--%>
            </section>
        </div>

    </div>
</div>
<jsp:include page="../utility/jquery_print.jsp"/>

<script>
    $('#download-pdf').click(() => {
        const fileName = 'Employee-Record-<%=employee_recordsDTO.employeeNumber%>.pdf';
        let content = document.getElementById('record-summary');
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 2},
            html2canvas: {scale: 2},
            jsPDF: {unit: 'in', format: 'A4', orientation: 'landscape'}
        };
        html2pdf().from(content).set(opt).save();
    });

    function printDiv(divName) {
        const printContents = document.getElementById(divName).innerHTML;
        const originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
