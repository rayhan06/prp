<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_allowance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="util.StringUtils" %>


<%
    String servletName = "Medical_allowanceServlet";
    String ID = request.getParameter("ID");
    Medical_allowanceDTO medical_allowanceDTO = Medical_allowanceDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    CommonDTO commonDTO = medical_allowanceDTO;
    String context = request.getContextPath() + "/";
%>
<%@include file="../pb/viewInitializer.jsp" %>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_ANYFIELD, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                    </label>
                                    <label class="col-md-8 form-control">
                                        <b><%=isLanguageEnglish ? medical_allowanceDTO.nameEn : medical_allowanceDTO.nameBn%>
                                            ,
                                            <%=isLanguageEnglish ? " " + medical_allowanceDTO.organogramNameEn + " , " + medical_allowanceDTO.officeUnitNameEn
                                                    : " " + medical_allowanceDTO.organogramNameBn + " , " + medical_allowanceDTO.officeUnitNameBn%>
                                        </b>
                                    </label>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_ID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(medical_allowanceDTO.voucherNumber, Language)%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_VOUCHERDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=StringUtils.getFormattedDate(Language, medical_allowanceDTO.voucherDate)%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-5">
                <div class=" div_border attachement-div">
                    <h5 class="table-title">
                        <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <tr>
                                <th><%=LM.getText(LC.REGISTER_LEASE_BMD_SEARCH_SERIALNUMBER, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_DATE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_DESCRIPTION, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_ALLOWANCEAMOUNT, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_FILESDROPZONE, loginDTO)%>
                                </th>
                            </tr>
                            <%
                                List<MedicalAllowanceDetailsDTO> medicalAllowanceDetailsDTOs = MedicalAllowanceDetailsDAO.getInstance()
                                        .getByMedicalAllowanceId(medical_allowanceDTO.iD);
                                int index = 0;

                                for (MedicalAllowanceDetailsDTO medicalAllowanceDetailsDTO : medicalAllowanceDetailsDTOs) {
                                    index++;
                            %>
                            <tr>
                                <td>
                                    <%=Utils.getDigits(index, Language)%>
                                </td>
                                <td>
                                    <%=StringUtils.getFormattedDate(Language, medicalAllowanceDetailsDTO.date)%>
                                </td>
                                <td>
                                    <%=medicalAllowanceDetailsDTO.description%>
                                </td>
                                <td>
                                    <%=Utils.getDigits(medicalAllowanceDetailsDTO.allowanceAmount, Language)%>
                                </td>
                                <td>
                                    <%
                                        {
                                            List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(medicalAllowanceDetailsDTO.filesDropzone);
                                    %>
                                    <table>
                                        <tr>
                                            <%
                                                if (FilesDTOList != null) {
                                                    for (int j = 0; j < FilesDTOList.size(); j++) {
                                                        FilesDTO filesDTO = FilesDTOList.get(j);
                                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                            %>
                                            <td>
                                                <%
                                                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                %>
                                                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                     style='width:100px'/>
                                                <%
                                                    }
                                                %>
                                                <a href='Medical_allowanceServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                   download><%=filesDTO.fileTitle%>
                                                </a>
                                            </td>
                                            <%
                                                    }
                                                }
                                            %>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                </td>
                            </tr>
                            <%

                                }

                            %>
                            <tr>
                                <td colspan="3">
                                    <%=LM.getText(LC.BUDGET_SUBTOTAL, loginDTO)%>
                                </td>
                                <td>
                                    <%=Utils.getDigits(medical_allowanceDTO.totalAllowance, Language)%>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-actions text-right mt-2">
                <button id="cancel-btn" class="btn-sm shadow text-white border-0 submit-btn" type="button"
                        onclick="location.href='<%=servletName%>?actionType=viewBill&ID=<%=commonDTO.iD%>'">
                    <%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>