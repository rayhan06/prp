<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="cheque_register.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@include file="../pb/addInitializer.jsp" %>
<%
    String context = request.getContextPath() + "/";
%>

<%
    String formTitle = LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO);
    String servletName = "Medical_allowanceServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigForm" name="bigform"
              action="Medical_allowanceServlet?actionType=getBill&isPermanentTable=true"
              method="post"
              onsubmit="return PreprocessBeforeSubmiting()"
              enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='employeeRecordsId'
                                           id='employeeRecordsId' value=''
                                           tag='pb_html'>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right"><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)
                                        %><span class="required">*</span>
                                        </label>
                                        <div class="col-9">
                                            <div>
                                                <button type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius"
                                                        id="emp_modal_button">
                                                    <%=LM.getText(LC.SEARCH_BY_EMPLOYEE, loginDTO)%>
                                                </button>

                                                <table class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 35%">
                                                            <b><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 30%">
                                                            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 30%">
                                                            <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 5%"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="emp_medical_allowance_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger btn-block"
                                                                    onclick="remove_containing_row(this,'emp_medical_allowance_table');">
                                                                &times;
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right"><%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUEDATE, loginDTO)%>
                                            <span class="required">*</span> </label>
                                        <div class="col-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="HIDE_DAY" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='startDate' id='startDate'
                                                   value=''
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right"><%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUEDATE, loginDTO)%>
                                            <span class="required">*</span> </label>
                                        <div class="col-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="HIDE_DAY" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='endDate'
                                                   value=''
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUE_REGISTER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="submit">
                                <%=LM.getText(LC.CHEQUE_REGISTER_ADD_CHEQUE_REGISTER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const billGenerateForm = $('#bigForm');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    function PreprocessBeforeSubmiting(){
        if (emp_medical_allowance_map.size > 0) {
            document.getElementById("employeeRecordsId").value = emp_medical_allowance_map.values().next().value.employeeRecordId;
        }
        document.getElementById('startDate').value = getDateTimestampById('startDate_js');
        document.getElementById('endDate').value = getDateTimestampById('endDate_js');
        let msg = null;
        if (emp_medical_allowance_map.size == 0) {
            msg = isLangEng ? "Please add employee" : "কর্মকর্তা যুক্ত করুন";
        }
        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(msg, msg);
        }
        const startDateValid = dateValidator('startDate_js', true, {
            'errorEn': 'Enter a valid date',
            'errorBn': 'যথাযথ তারিখ প্রবেশ করান!'
        });
        const endDateValid = dateValidator('endDate_js', true, {
            'errorEn': 'Enter a valid date',
            'errorBn': 'যথাযথ তারিখ প্রবেশ করান!'
        });
        return msg==null && startDateValid && endDateValid;
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Cheque_registerServlet");
    }

    function init(row) {
        $("#startDate_js").on('datepicker.change', () => {
            if (getDateStringById("startDate_js") !== '') {
                setMinDateById("endDate_js", getDateStringById("startDate_js"));
            }
        });
    }
    var row = 0;
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });
    <%
        List<EmployeeSearchIds> addedEmpIdsList = null;
        // to initialize map for a Single Employee use
        // EmployeeSearchModalUtil.initJsMapSingleEmployee(employeeRecordId,officeUnitId,organogramId)
    %>
    emp_medical_allowance_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);
    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map([
        ['emp_medical_allowance_table', {
            info_map: emp_medical_allowance_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                console.log('callBackFunction called with latest added emp info JSON');
                console.log(empInfo);
            }
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#emp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'emp_medical_allowance_table';
        $('#search_emp_modal').modal();
    });
</script>






