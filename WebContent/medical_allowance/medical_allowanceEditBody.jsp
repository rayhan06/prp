<%@page import="dbm.DBMW" %>
<%@page import="files.FilesDAO" %>
<%@page import="files.FilesDTO" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="medical_allowance.MedicalAllowanceDetailsDTO" %>
<%@page import="medical_allowance.Medical_allowanceDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="java.util.List" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="medical_allowance.Medical_allowanceDAO" %>
<%@ page import="medical_allowance.MedicalAllowanceDetailsDAO" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="pb.Utils" %>
<%@ page import="common.BaseServlet" %>

<%
    String context = request.getContextPath() + "/";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String ID = (String) request.getAttribute("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    String voucherNumber = Medical_allowanceDAO.VOUCHER_PREFIX + "-" + ID;
    Medical_allowanceDTO medical_allowanceDTO;
    String actionName;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        medical_allowanceDTO = (Medical_allowanceDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        medical_allowanceDTO.medicalAllowanceDetailsDTOList = MedicalAllowanceDetailsDAO.getInstance().getByMedicalAllowanceId(medical_allowanceDTO.iD);
        ID = String.valueOf(medical_allowanceDTO.iD);
        voucherNumber = medical_allowanceDTO.voucherNumber;
    } else {
        actionName = "add";
        medical_allowanceDTO = new Medical_allowanceDTO();
    }
    int i = 0;
    int childTableStartingID = 1;
    String Language = LM.getText(LC.MEDICAL_ALLOWANCE_EDIT_LANGUAGE, loginDTO);
    String formTitle = LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_ADD_FORMNAME, loginDTO);
    String servletName = "Medical_allowanceServlet";
    boolean isPermanentTable = true;
    String tableName = "medical_allowance";
    String fileColumnName = "";
    FilesDAO filesDAO = new FilesDAO();
    String value = "";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    long curTime = Calendar.getInstance().getTimeInMillis();
%>
<%--<%@include file="../pb/addInitializer.jsp"%>--%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=ID%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='voucher_number'
                                               id='voucher_number'
                                               value='<%=voucherNumber%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='employeeRecordsId'
                                               id='employeeRecordsId_<%=i%>' value=''
                                               tag='pb_html'>
                                        <input type='hidden' class='form-control' name='totalAllowance'
                                               id='totalAllowance_number_<%=i%>' value=''
                                               tag='pb_html'>
                                        <input type='hidden' class='form-control' name='nameEn'
                                               id='nameEn_hidden_<%=i%>' value='<%=medical_allowanceDTO.nameEn%>'
                                               tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='nameBn'
                                               id='nameBn_hidden_<%=i%>' value='<%=medical_allowanceDTO.nameBn%>'
                                               tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='officeUnitId'
                                               id='officeUnitId_hidden_<%=i%>'
                                               value='<%=medical_allowanceDTO.officeUnitId%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='officeUnitNameEn'
                                               id='officeUnitNameEn_hidden_<%=i%>'
                                               value='<%=medical_allowanceDTO.officeUnitNameEn%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='officeUnitNameBn'
                                               id='officeUnitNameBn_hidden_<%=i%>'
                                               value='<%=medical_allowanceDTO.officeUnitNameBn%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='organogramId'
                                               id='organogramId_hidden_<%=i%>'
                                               value='<%=medical_allowanceDTO.organogramId%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='organogramNameEn'
                                               id='organogramNameEn_hidden_<%=i%>'
                                               value='<%=medical_allowanceDTO.organogramNameEn%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='organogramNameBn'
                                               id='organogramNameBn_hidden_<%=i%>'
                                               value='<%=medical_allowanceDTO.organogramNameBn%>' tag='pb_html'/>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <div>
                                                    <button type="button"
                                                            class="btn btn-primary btn-block shadow btn-border-radius"
                                                            id="emp_modal_button">
                                                        <%=LM.getText(LC.SEARCH_BY_EMPLOYEE, loginDTO)%>
                                                    </button>

                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped text-nowrap">
                                                            <thead>
                                                            <tr>
                                                                <th>
                                                                    <b><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                                                    </b></th>
                                                                <th>
                                                                    <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                                                    </b></th>
                                                                <th>
                                                                    <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                                                    </b></th>
                                                                <th></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="emp_medical_allowance_table">
                                                            <tr style="display: none;">
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <button type="button"
                                                                            class="btn btn-sm delete-trash-btn"
                                                                            onclick="remove_containing_row(this,'emp_medical_allowance_table');">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            <%if (actionName == "edit") {%>
                                                            <tr>
                                                                <td>
                                                                    <%--employeeNumber and user name is same--%>
                                                                    <%=Employee_recordsRepository.getInstance()
                                                                            .getById(medical_allowanceDTO.employeeRecordsId).employeeNumber%>
                                                                </td>
                                                                <td>
                                                                    <%=isLanguageEnglish ? medical_allowanceDTO.nameEn : medical_allowanceDTO.nameBn%>
                                                                </td>
                                                                <td>
                                                                    <%=isLanguageEnglish ? (medical_allowanceDTO.organogramNameEn + ", " + medical_allowanceDTO.officeUnitNameEn)
                                                                            : (medical_allowanceDTO.organogramNameBn + ", " + medical_allowanceDTO.officeUnitNameBn)%>
                                                                </td>
                                                                <td id='<%=medical_allowanceDTO.employeeRecordsId%>_td_button'>
                                                                    <button type="button"
                                                                            class="btn btn-btn-sm delete-trash-btn"
                                                                            onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                        <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            <%}%>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_ID, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <label class="col-form-label form-control">
                                                    <%=voucherNumber%>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_VOUCHERDATE, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <%
                                                value = "voucherDate_js_" + i;
                                            %>
                                            <div class="col-md-8">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='voucherDate' id='voucherDate_date_<%=i%>'
                                                       value='<%=actionName.equalsIgnoreCase("edit")?medical_allowanceDTO.voucherDate:curTime%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap" id="medical_allowance_table">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.REGISTER_LEASE_BMD_SEARCH_SERIALNUMBER, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_MEDICALALLOWANCEID, loginDTO)%>
                                    </th>
                                    <th style="min-width: 25rem"><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_DATE, loginDTO)%>
                                    </th>
                                    <th ><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_DESCRIPTION, loginDTO)%>
                                    </th>
                                    <th ><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_ALLOWANCEAMOUNT, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_FILESDROPZONE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_DETAILS_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-MedicalAllowanceDetails">


                                <%
                                    if (actionName.equals("edit")) {
                                        int index = -1;
                                        for (MedicalAllowanceDetailsDTO medicalAllowanceDetailsDTO : medical_allowanceDTO.medicalAllowanceDetailsDTOList) {
                                            index++;


                                %>

                                <tr id="MedicalAllowanceDetails_<%=index + 1%>">
                                    <td>
                                        <label class="col-form-label"
                                               name='medicalAllowanceDetails.serialNumber_text_'
                                               id='medicalAllowanceSerialNumber_text_' tag='pb_html'/>
                                        <%=Utils.getDigits(index + 1, Language)%>
                                        </label>
                                    </td>
                                    <input type='hidden' class='form-control' name='medicalAllowanceDetails.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=medicalAllowanceDetailsDTO.iD%>' tag='pb_html'/>

                                    <td>


                                        <label class="col-form-label"
                                               name='medicalAllowanceDetails.medicalAllowanceId_text_'
                                               id='medicalAllowanceId_text_' tag='pb_html'/>
                                        <%=voucherNumber%>
                                        </label>
                                        <input type='hidden' class='form-control'
                                               name='medicalAllowanceDetails.medicalAllowanceId'
                                               id='medicalAllowanceId_<%=childTableStartingID%>' value='<%=ID%>'
                                               tag='pb_html'/>
                                    </td>
                                    <td>
                                        <%
                                            value = "date_js_" + childTableStartingID;
                                        %>
                                        <jsp:include page="/date/date.jsp">
                                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type='hidden' name='medicalAllowanceDetails.date'
                                               id='date_date_<%=childTableStartingID%>'
                                               value='<%=medicalAllowanceDetailsDTO.date%>'
                                               tag='pb_html'>
                                    </td>
                                    <td>
                                        <textarea rows="3"
                                                  type='text'
                                                  class='form-control '
                                                  name='medicalAllowanceDetails.description'
                                                  placeholder="<%=isLanguageEnglish?"Enter Description":"বিবরণ"%>"
                                                  id='description_text_<%=childTableStartingID%>'
                                                  onkeyup="updateDescriptionLen(<%=childTableStartingID%>)"
                                                  style="text-align: left;resize: none; width: 100%"
                                                  tag='pb_html'><%=medicalAllowanceDetailsDTO.description%></textarea>
                                        <p id="description_len_<%=childTableStartingID%>"
                                           style="width: 100%;text-align: right;font-size: small"><%=medicalAllowanceDetailsDTO.description.length()%>
                                            /1024</p>
                                        <%--                                        <input type='text' class='form-control'--%>
                                        <%--                                               name='medicalAllowanceDetails.description'--%>
                                        <%--                                               id='description_text_<%=childTableStartingID%>'--%>
                                        <%--                                               value='<%=medicalAllowanceDetailsDTO.description%>' tag='pb_html'/>--%>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control w-auto'
                                               name='medicalAllowanceDetails.allowanceAmount'
                                               id='allowanceAmount_text_<%=childTableStartingID%>'
                                               placeholder="<%=isLanguageEnglish?"Enter Description":"বিবরণ"%>"
                                               value='<%=medicalAllowanceDetailsDTO.allowanceAmount%>' tag='pb_html'/>
                                    </td>
                                    <td>
                                        <%
                                            fileColumnName = "filesDropzone";
                                            if (actionName.equals("edit")) {
                                                List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(medicalAllowanceDetailsDTO.filesDropzone);
                                        %>
                                        <%--                                        <%@include file="../pb/dropzoneEditor.jsp" %>--%>
                                        <table>
                                            <tr>
                                                <%
                                                    if (fileList != null) {
                                                        for (int j = 0; j < fileList.size(); j++) {
                                                            FilesDTO filesDTO = fileList.get(j);
                                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                %>
                                                <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                                    <%
                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                    %>
                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                                    <%
                                                        }
                                                    %>
                                                    <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                       download><%=filesDTO.fileTitle%>
                                                    </a>
                                                    <a class='btn btn-sm cancel-btn text-light shadow btn-border-radius pl-4'
                                                       onclick='deletefile(<%=filesDTO.iD%>, "<%=fileColumnName%>_td_<%=filesDTO.iD%>", "<%=fileColumnName%>FilesToDelete_<%=childTableStartingID%>")'>
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </tr>
                                        </table>
                                        <%
                                            } else {
                                                medicalAllowanceDetailsDTO.filesDropzone = DBMW.getInstance().getNextSequenceId("fileid");
                                            }
                                        %>

                                        <div class="dropzone"
                                             action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=medicalAllowanceDetailsDTO.filesDropzone%>"
                                        >
                                            <input type='file' style="display:none"
                                                   name='medicalAllowanceDetails.<%=fileColumnName%>File'
                                                   id='<%=fileColumnName%>_dropzone_File_<%=childTableStartingID%>'
                                                   tag='pb_html'/>
                                        </div>
                                        <input type='hidden'
                                               name='medicalAllowanceDetails.<%=fileColumnName%>FilesToDelete'
                                               id='<%=fileColumnName%>FilesToDelete_<%=childTableStartingID%>' value=''
                                               tag='pb_html'/>
                                        <input type='hidden' name='medicalAllowanceDetails.<%=fileColumnName%>'
                                               id='<%=fileColumnName%>_dropzone_<%=childTableStartingID%>' tag='pb_html'
                                               value='<%=medicalAllowanceDetailsDTO.filesDropzone%>'/>
                                    </td>
                                    <%--                                    <td>--%>
                                    <%--                                        <div class='checker'>--%>
                                    <%--                                            <input type='checkbox' name='checkbox' value='' deletecb='true'--%>
                                    <%--                                                   class="form-control-sm"/>--%>
                                    <%--                                        </div>--%>
                                    <%--                                    </td>--%>
                                    <td>
                                        <button
                                                id="remove-MedicalAllowanceDetails_<%=childTableStartingID%>"
                                                name="removeMedicalAllowanceDetails.removeButton"
                                                type="button"
                                                tag="pb_html"
                                                onclick="removeRow(this)"
                                                class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="text-right mt-3">
                                <button
                                        id="add-more-MedicalAllowanceDetails"
                                        name="add-moreMedicalAllowanceDetails"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <%--                                <button--%>
                                <%--                                        id="remove-MedicalAllowanceDetails"--%>
                                <%--                                        name="removeMedicalAllowanceDetails"--%>
                                <%--                                        type="button"--%>
                                <%--                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">--%>
                                <%--                                    <i class="fa fa-trash"></i>--%>
                                <%--                                </button>--%>
                            </div>
                        </div>
                        <%MedicalAllowanceDetailsDTO medicalAllowanceDetailsDTO = new MedicalAllowanceDetailsDTO();%>
                        <template id="template-MedicalAllowanceDetails">
                            <tr>
                                <td>
                                    <label class="col-form-label"
                                           name='medicalAllowanceDetails.serialNumber_text_'
                                           id='medicalAllowanceSerialNumber_text_' tag='pb_html'/>
                                    <%=Utils.getDigits(1, Language)%>
                                    </label>
                                </td>

                                <input type='hidden' class='form-control' name='medicalAllowanceDetails.iD'
                                       id='iD_hidden_' value='<%=medicalAllowanceDetailsDTO.iD%>' tag='pb_html'/>

                                <td>

                                    <label class="col-form-label"
                                           name='medicalAllowanceDetails.medicalAllowanceId_text_'
                                           id='medicalAllowanceId_text_' tag='pb_html'/>
                                    <%=voucherNumber%>
                                    </label>
                                    <input type='hidden' class='form-control'
                                           name='medicalAllowanceDetails.medicalAllowanceId'
                                           id='medicalAllowanceId' value='<%=ID%>' tag='pb_html'/>
                                </td>
                                <td>
                                    <%
                                        value = "date_js_";
                                    %>
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' name='medicalAllowanceDetails.date' id='date_date_'
                                           value='<%=medicalAllowanceDetailsDTO.date%>'
                                           tag='pb_html'>
                                </td>
                                <td style="min-width: 20rem">


                                    <textarea rows="3" type='text'
                                              class='form-control'
                                              name='medicalAllowanceDetails.description'
                                              placeholder="<%=isLanguageEnglish?"Enter Description":"বিবরণ"%>"
                                              id='description_text_'
                                              value=''
                                              style="text-align: left;resize: none; width: 100%"
                                              tag='pb_html'></textarea>
                                    <p id="description_len_" style="width: 100%;text-align: right;font-size: small"
                                       tag="pb_html">0/1024</p>
                                    <%--                                    <input type='text' class='form-control'--%>
                                    <%--                                           name='medicalAllowanceDetails.description' id='description_text_'--%>
                                    <%--                                           value='' tag='pb_html'/>--%>

                                </td>
                                <td>


                                    <input type='text' class='form-control w-auto'
                                           name='medicalAllowanceDetails.allowanceAmount' id='allowanceAmount_text_'
                                           placeholder="<%=isLanguageEnglish?"Enter Amount":"টাকার পরিমাণ"%>"
                                           value='' tag='pb_html'/>
                                </td>
                                <%
                                    fileColumnName = "filesDropzone";

                                %>
                                <td class="groups-section" tag='pb_html' id='<%=fileColumnName%>_dropzoneDiv_'>
                                    <input type='hidden' name='medicalAllowanceDetails.<%=fileColumnName%>'
                                           id='<%=fileColumnName%>_dropzone_' tag='pb_html'
                                           value='<%=medicalAllowanceDetailsDTO.filesDropzone%>'/>


                                </td>
                                <td>
                                    <%--                                    <div>--%>
                                    <%--                                        <input type='checkbox' name='checkbox' value='' deletecb='true'--%>
                                    <%--                                               class="form-control-sm"/>--%>
                                    <%--                                    </div>--%>
                                    <button
                                            id="remove-MedicalAllowanceDetails_"
                                            name="removeMedicalAllowanceDetails.removeButton"
                                            type="button"
                                            tag="pb_html"
                                            onclick="removeRow(this)"
                                            class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center mt-5">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="submitMedicalAllowanceForm()">
                        <%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_MEDICAL_ALLOWANCE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const bigForm = $('#bigform');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() == 'english';
    const actionName = '<%=actionName%>';

    function PreprocessBeforeSubmiting(row, validate) {
        setModalValues();
        const dateValid = dateValidator("voucherDate_js_0", true, {
            'errorEn': 'Enter a valid date!',
            'errorBn': 'যথাযথ তারিখ প্রবেশ করান!'
        });
        //preprocessDateBeforeSubmitting('voucherDate', row);
        document.getElementById('voucherDate_date_0').value = getDateTimestampById('voucherDate_js_0');
        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("date_date_" + i)) {
                // preprocessDateBeforeSubmitting('date', i);
                document.getElementById("date_date_" + i).value = getDateTimestampById('date_js_' + i);
            }
        }
        if (document.getElementById("medical_allowance_table-total-col-4")) {
            document.getElementById("totalAllowance_number_0").value = document.getElementById("medical_allowance_table-total-col-4").innerHTML;
        }
        // return false;
    }

    function submitMedicalAllowanceForm() {
        PreprocessBeforeSubmiting(0, '<%=actionName%>');
        setButtonState(true);
        let params = bigForm.serialize().split('&');
        let msg = null;
        if (emp_medical_allowance_map.size == 0) {
            msg = isLangEng ? "Please add employee" : "কর্মকর্তা যুক্ত করুন";
        }
        if (document.getElementById("voucher_number").value == "" || document.getElementById("voucher_number").value == "0") {
            msg = isLangEng ? "Please add voucher number" : "ভাউচার নাম্বার যুক্ত করুন";
        }
        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(msg, msg);
            setButtonState(false);
            return;
        }
        for (let i = 0; i < params.length; i++) {
            let param = params[i].split('=');
            switch (param[0]) {
                case 'medicalAllowanceDetails.allowanceAmount':
                    if (!param[1]) {
                        msg = isLangEng ? "Bill amount missing" : "বিলের পরিমাণ পাওয়া যায়নি";
                    }
                    break;
            }
            if (msg) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(msg, msg);
                setButtonState(false);
                return;
            }
        }

        $.ajax({
            type: "POST",
            url: "Medical_allowanceServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true&id=<%=ID%>",
            data: bigForm.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    setButtonState(false);
                    console.log("response code 0");
                } else if (response.responseCode === 200) {
                    console.log("repsonse code 200");
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                setButtonState(false);
            }
        });
    }

    function setButtonState(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function setModalValues() {
        if (emp_medical_allowance_map.size > 0) {
            document.getElementById("employeeRecordsId_0").value = emp_medical_allowance_map.values().next().value.employeeRecordId;
            document.getElementById("nameEn_hidden_0").value = emp_medical_allowance_map.values().next().value.employeeNameEn;
            document.getElementById("nameBn_hidden_0").value = emp_medical_allowance_map.values().next().value.employeeNameBn;
            document.getElementById("officeUnitId_hidden_0").value = emp_medical_allowance_map.values().next().value.officeUnitId;
            document.getElementById("officeUnitNameEn_hidden_0").value = emp_medical_allowance_map.values().next().value.officeUnitNameEn;
            document.getElementById("officeUnitNameBn_hidden_0").value = emp_medical_allowance_map.values().next().value.officeUnitNameBn;
            document.getElementById("organogramId_hidden_0").value = emp_medical_allowance_map.values().next().value.organogramId;
            document.getElementById("organogramNameEn_hidden_0").value = emp_medical_allowance_map.values().next().value.organogramNameEn;
            document.getElementById("organogramNameBn_hidden_0").value = emp_medical_allowance_map.values().next().value.organogramNameBn;
        }

    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Medical_allowanceServlet");
    }

    function keyDownEvent(e) {
        console.log(e);
        let isvalid = inputValidationForPositiveValue(e, $(this), 100000000,1);
        return true == isvalid;
    }

    function init(row) {

        setDateByTimestampAndId('voucherDate_js_' + row, $('#voucherDate_date_' + row).val());

        for (i = 1; i < child_table_extra_id; i++) {
            setDateByTimestampAndId('date_js_' + i, $('#date_date_' + i).val());
            document.getElementById('allowanceAmount_text_' + i).onkeydown = keyDownEvent;
            document.getElementById('allowanceAmount_text_' + i).onkeyup = sumThisColumn;
        }

    }

    function setVoucherValuesInColumn() {
        var val = document.getElementById("voucher_number").value;
        console.log("val :" + val);
        for (i = 0; i < document.getElementById("field-MedicalAllowanceDetails").rows.length; i++) {
            var trow = document.getElementById("field-MedicalAllowanceDetails").rows[i];
            console.log(trow.querySelector('label[name="medicalAllowanceDetails.medicalAllowanceId_text_"]'));
            trow.querySelector('label[name="medicalAllowanceDetails.medicalAllowanceId_text_"]').innerHTML = val;
        }
    }

    function setSerialNumbersInColumn() {
        for (i = 0; i < document.getElementById("field-MedicalAllowanceDetails").rows.length; i++) {
            var trow = document.getElementById("field-MedicalAllowanceDetails").rows[i];
            var val = i + 1;
            if (!isLangEng)
                val = convertToBangla(val);
            trow.querySelector('label[name="medicalAllowanceDetails.serialNumber_text_"]').innerHTML = val;
        }
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        //CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
        if ('<%=actionName%>' == "edit") {

            const colIndicesToSum = [4];
            const totalTitleColSpan = 2;
            const totalTitle = '<%=LM.getText(LC.BUDGET_SUBTOTAL,loginDTO)%>';
            setupTotalRow("medical_allowance_table", totalTitle, totalTitleColSpan, colIndicesToSum);

        } else {
            addRow();
            document.getElementById("remove-MedicalAllowanceDetails_1").disabled = true;
            document.getElementById("remove-MedicalAllowanceDetails_1").style = "display:none";
        }
        let dropdownMsg=isLangEng?"Drop files here to upload":"ফাইল আপলোড করার জন্য এখানে ফাইল ড্রপ করুন";
        const dropzoneMsgArr = document.querySelectorAll('.dz-default.dz-message');
        dropzoneMsgArr.forEach((item) => {
            item.innerHTML =  "<span>"+dropdownMsg+"</span>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-MedicalAllowanceDetails").click(
        function (e) {
            e.preventDefault();
            addRow();

        });

    function convertToBangla(str) {
        str = String(str);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    function addRow() {
        var t = $("#template-MedicalAllowanceDetails");
        //t.querySelector('label[name="medicalAllowanceDetails.medicalAllowanceId_text_"]').innerHTML=document.getElementById("voucher_number").value;
        $("#field-MedicalAllowanceDetails").append(t.html());
        //SetCheckBoxValues("field-MedicalAllowanceDetails");

        var tr = $("#field-MedicalAllowanceDetails").find("tr:last-child");
        var latestrow = document.getElementById("field-MedicalAllowanceDetails").rows[document.getElementById("field-MedicalAllowanceDetails").rows.length - 1];
        latestrow.querySelector('label[name="medicalAllowanceDetails.medicalAllowanceId_text_"]').innerHTML = document.getElementById("voucher_number").value;
        var serialNumber = latestrow.rowIndex;
        if (!isLangEng) {
            serialNumber = convertToBangla(serialNumber);
            console.log(serialNumber);
        }
        latestrow.querySelector('label[name="medicalAllowanceDetails.serialNumber_text_"]').innerHTML = serialNumber;
        tr.attr("id", "MedicalAllowanceDetails_" + child_table_extra_id);
        let jsColumnID = '';

        const url = 'Medical_allowanceServlet?actionType=getFileNextSequenceId';
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                jsColumnID = response.msg;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
        console.log('js jsColumnID: ' + jsColumnID);
        tr.find("[tag='pb_html']").each(function (index) {
            var prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_extra_id);
            console.log(index + ": " + $(this).attr('id'));
            let childDivId = 'childDrozone_' + prev_id + child_table_extra_id;

            if ($(this).attr('id').includes('filesDropzone_dropzoneDiv_')) {
                var HtmlCodeOfSection = ' <div class="dropzone" id="' + childDivId + '" </div>';

                $("#" + $(this).attr('id')).append(HtmlCodeOfSection);
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone("div#" + childDivId, {
                    url: "Medical_allowanceServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=" + jsColumnID
                });
                myDropzone.dictDefaultMessage=isLangEng?"ফাইল আপলোড করার জন্য এখানে ফাইল ড্রপ করুন":"ফাইল আপলোড করার জন্য এখানে ফাইল ড্রপ করুন";
            }
            if ($(this).attr('id').includes('filesDropzone_dropzone_')) {
                $("#" + $(this).attr('id')).val(jsColumnID);
                console.log(jsColumnID);
            }
        });
        if ('<%=actionName%>' == "add") {
            if (child_table_extra_id == 1) {
                const colIndicesToSum = [4];
                const totalTitleColSpan = 2;
                const totalTitle = '<%=LM.getText(LC.BUDGET_SUBTOTAL,loginDTO)%>';
                setupTotalRow("medical_allowance_table", totalTitle, totalTitleColSpan, colIndicesToSum);
            }
        }
        //setDateByTimestampAndId('date_js_' + child_table_extra_id, $('#date_date_' + child_table_extra_id).val());
        select2SingleSelector('#daySelection'+'date_js_' + child_table_extra_id,'<%=Language%>');
        select2SingleSelector('#monthSelection'+'date_js_' + child_table_extra_id,'<%=Language%>');
        select2SingleSelector('#yearSelection'+'date_js_' + child_table_extra_id,'<%=Language%>');
        document.getElementById('allowanceAmount_text_' + child_table_extra_id).onkeydown = keyDownEvent;
        document.getElementById('description_text_' + child_table_extra_id).onkeyup = updateDescriptionLen2;
        document.getElementById('allowanceAmount_text_' + child_table_extra_id).onkeyup = sumThisColumn;
        let dropdownMsg=isLangEng?"Drop files here to upload":"ফাইল আপলোড করার জন্য এখানে ফাইল ড্রপ করুন";
        for(i=0;i<document.getElementsByClassName("dz-default dz-message").length;i++){
            document.getElementsByClassName("dz-default dz-message")[i].innerHTML="<span>"+dropdownMsg+"</span>";
        }
        child_table_extra_id++;
    }

    function removeRow(buttonName) {
        var tr = buttonName.parentNode.parentNode;
        tr.remove();
        showSumOfColumnValues("medical_allowance_table", 4);
        setSerialNumbersInColumn();
    }

    function updateDescriptionLen(rowNum) {
        $('#description_len_' + rowNum).text($('#description_text_' + rowNum).val().length + "/1024");
    }

    function updateDescriptionLen2() {
        descriptionId = $(this).attr("id");
        rowNum = descriptionId.split("_")[descriptionId.split("_").length - 1];
        $('#description_len_' + rowNum).text($('#description_text_' + rowNum).val().length + "/1024");
    }
    $("#remove-MedicalAllowanceDetails").click(function (e) {
        var tablename = 'field-MedicalAllowanceDetails';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
        showSumOfColumnValues("medical_allowance_table", 4);
        setSerialNumbersInColumn();
    });
    <%
        List<EmployeeSearchIds> addedEmpIdsList = null;
        if( actionName=="edit"){
            EmployeeSearchIds tmp=new EmployeeSearchIds(medical_allowanceDTO.employeeRecordsId,medical_allowanceDTO.officeUnitId,medical_allowanceDTO.organogramId);
            addedEmpIdsList=new ArrayList<>();
            addedEmpIdsList.add(tmp);
        }
        // to initialize map for a Single Employee use
        // EmployeeSearchModalUtil.initJsMapSingleEmployee(employeeRecordId,officeUnitId,organogramId)
    %>
    emp_medical_allowance_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);
    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map([
        ['emp_medical_allowance_table', {
            info_map: emp_medical_allowance_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                console.log('callBackFunction called with latest added emp info JSON');
                console.log(empInfo);
            }
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#emp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'emp_medical_allowance_table';
        $('#search_emp_modal').modal();
    });

</script>






