<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_allowance.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="util.StringUtils" %>


<%
    String navigator2 = "navMEDICAL_ALLOWANCE";
    String servletName = "Medical_allowanceServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_ID, loginDTO)%>
            </th>
            <th style="text-align: center"><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_EMPLOYEERECORDSID, loginDTO)%></th>
            <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_VOUCHERDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_ADD_TOTALALLOWANCE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_MEDICAL_ALLOWANCE_EDIT_BUTTON, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Medical_allowanceDTO> data = (List<Medical_allowanceDTO>) recordNavigator.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Medical_allowanceDTO medical_allowanceDTO = data.get(i);


        %>
        <tr>

            <td>

                <%=Utils.getDigits(String.valueOf(medical_allowanceDTO.voucherNumber), Language)%>
            </td>
            <td style="text-align: center">
                <%=isLanguageEnglish?medical_allowanceDTO.nameEn :medical_allowanceDTO.nameBn%>
                <br>
                <%=isLanguageEnglish?"<b>"+medical_allowanceDTO.organogramNameEn+"</b><br>"+medical_allowanceDTO.officeUnitNameEn
                        : "<b>"+medical_allowanceDTO.organogramNameBn+"</b><br>"+medical_allowanceDTO.officeUnitNameBn%>
            </td>
            <td>
                <%=StringUtils.getFormattedDate(Language, medical_allowanceDTO.voucherDate)%>
            </td>

            <td>

                <%=Utils.getDigits(medical_allowanceDTO.totalAllowance, Language)%>
            </td>
            <%CommonDTO commonDTO = medical_allowanceDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ea07d4;"
                        onclick="location.href='<%=servletName%>?actionType=viewBill&ID=<%=commonDTO.iD%>'"
                >
                    <%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
                </button>
            </td>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=medical_allowanceDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			