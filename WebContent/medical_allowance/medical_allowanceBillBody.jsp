<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="medical_allowance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="static java.util.stream.Collectors.joining" %>
<%@ page import="java.util.stream.Stream" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="budget.BudgetCategoryEnum" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_mapping.Budget_mappingDTO" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="budget.BudgetDAO" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>

<%
    String ID = request.getParameter("ID");
    Medical_allowanceDTO medical_allowanceDTO = Medical_allowanceDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    List<MedicalAllowanceDetailsDTO> medicalAllowanceDetailsDTOS = MedicalAllowanceDetailsDAO.getInstance().getByMedicalAllowanceId(medical_allowanceDTO.iD);
    String empNameEn = medical_allowanceDTO.nameEn;
    String empNameBn = medical_allowanceDTO.nameBn;
    String officeNameBn = medical_allowanceDTO.officeUnitNameBn;
    String organogramNameBn = medical_allowanceDTO.organogramNameBn;

    long budgetOfficeId = Budget_officeRepository.getInstance().getBudgetOfficeIdByOfficeUnitId(medical_allowanceDTO.officeUnitId);
    Budget_mappingDTO budgetMappingDTO = Budget_mappingRepository.getInstance()
                                                                 .getDTO(budgetOfficeId, BudgetCategoryEnum.OPERATIONAL.getValue());
    long totalExpenditure = medical_allowanceDTO.totalAllowance;
    long allocatedBudget = BudgetDAO.getInstance().getAllocatedBudget(
            System.currentTimeMillis(),
            budgetMappingDTO.iD,
            Medical_allowanceServlet.MEDICAL_ALLOWANCE_SUB_CODE
    );
    String allocatedBudgetStr = allocatedBudget <= 0L ? ""
                                                      : BangladeshiNumberFormatter.getFormattedNumber(
                                                              convertToBanNumber(String.valueOf(allocatedBudget))
                                                      );
    String remainingBudgetStr = allocatedBudget <= 0L ? ""
                                                      : BangladeshiNumberFormatter.getFormattedNumber(
                                                              convertToBanNumber(String.valueOf(allocatedBudget - totalExpenditure))
                                                      );

    String budgetOfficeNameBn = Budget_officeRepository.getInstance().getText(budgetOfficeId, "Bangla");
    String amountInWord = BangladeshiNumberInWord.convertToWord(StringUtils.convertToBanNumber(String.valueOf(medical_allowanceDTO.totalAllowance)));
    String totalAllowanceStr = BangladeshiNumberFormatter.getFormattedNumber(
            StringUtils.convertToBanNumber(String.valueOf(medical_allowanceDTO.totalAllowance))
    );
%>
<%@include file="../pb/viewInitializer.jsp" %>
<%
    Language = "ENGLISH";
%>

<style>
    .form-group {
        margin-bottom: 1rem;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .symbol-taka-amount {
        display: flex;
        justify-content: space-between;
    }

    .full-border {
        border: 2px solid black;
        padding: 5px;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td {
        padding: 5px;
    }

    .table-borderless td {
        padding: 5px;
    }
    th {
        padding: .75rem;
    }
</style>
<div class="kt-content p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=isLanguageEnglish ? "Medical Expense Bill" : "চিকিৎসা ব্যয় বিল"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body" id="bill-div">
            <div class="ml-auto m-3">
                <button type="button" class="btn" id='download-pdf'
                        onclick="downloadTemplateAsPdf('to-print-div', 'Forwading ' + '<%=empNameEn%>');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>

            <div style="margin: auto;">
                <div class="container" id="to-print-div">
                    <section class="page shadow" data-size="A4">
                        <div class="text-center">
                            <h1>MEDICAL BILL FORM</h1>
                        </div>
                        <div class=" col-10 offset-1 form-group row" style="margin-top: 30px">
                            <div class="col-6">
                                <p style="font-size: 10px ">Medical attendance bill of Mr/Mrs/Maste<br>
                                    Self/Wife/Daughter/Son of Mr<br>
                                    Parliament Secretariat, Dhaka.(In Block Letters)<br>
                                    patient</p>
                            </div>
                            <div class="col-6">
                                <p style="font-size: 10px"><b><%=empNameBn%>
                                </b> , <b><%=organogramNameBn%>
                                </b><br>
                                    <b><%=officeNameBn%>
                                    </b> of the Bangladesh<br>
                                    Separate bill should by submitted for each</p>
                            </div>
                        </div>
                        <div class="col-10 offset-1" style="padding-top: 10px">
                            <div class="mt-2 table-responsive">
                                <table class="table-borderless" style="width: 100%">
                                    <tbody>
                                    <%
                                        if (medicalAllowanceDetailsDTOS.size() == 0) {
                                    %>
                                    <tr>
                                        <td width="10%">
                                            A
                                        </td>
                                        <td width="5%"></td>
                                        <td width="15%"></td>
                                        <td width="10%"></td>
                                        <td width="10%"></td>
                                        <td width="20%"></td>
                                        <td width="10%"></td>
                                        <td width="20%"></td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                    <%
                                        int index = 1;
                                        long sum = 0;
                                        for (MedicalAllowanceDetailsDTO allowanceDTO : medicalAllowanceDetailsDTOS) {
                                            sum += allowanceDTO.allowanceAmount;
                                    %>
                                    <tr>
                                        <td width="10%">
                                            <%
                                                if (index == 1) {
                                            %>
                                            A
                                            <%
                                                }
                                            %>
                                        </td>
                                        <td width="5%">
                                            <%=index%>.
                                        </td>
                                        <td width="15%">
                                            Voucher No.
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="10%">
                                            Date
                                        </td>
                                        <td width="20%">
                                            <%=StringUtils.getFormattedDate("BANGLA", allowanceDTO.date)%>
                                        </td>
                                        <td width="10%">
                                            Taka
                                        </td>
                                        <td>
                                            <%
                                                String val = allowanceDTO.allowanceAmount + ".00";
                                            %>
                                            <%=Utils.getDigits(val, "BANGLA")%>
                                        </td>
                                    </tr>
                                    <%
                                            index++;
                                        }
                                    %>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <b>Total taka.</b>
                                        </td>
                                        <td></td>
                                        <td>
                                            <%
                                                String val = sum + ".00";
                                            %>
                                            <b><%=Utils.getDigits(val, "BANGLA")%>
                                            </b>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-10 offset-1" style="padding-top: 10px">
                            <div class="mt-2 table-responsive">
                                <table class="table-borderless" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <td width="10%">
                                            B
                                        </td>
                                        <td colspan="7">
                                            Fees for pathological, Bacteriological, Radiological etc. Examination
                                            for proper diagnosis and
                                        </td>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td width="10%"></td>
                                        <td colspan="7">
                                            treatment of the patient vide receipts detailed below :-
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                        </td>
                                        <td colspan="7">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                        </td>
                                        <td colspan="7">
                                        </td>
                                    </tr>
                                    <%
                                        index = 1;
                                        while (index <= 3) {
                                    %>
                                    <tr>
                                        <td width="10%">
                                        </td>
                                        <td width="5%">
                                            <%=index%>.
                                        </td>
                                        <td width="15%">
                                            Receipt No.
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="10%">
                                            Date
                                        </td>
                                        <td width="20%">
                                        </td>
                                        <td width="10%">
                                            Taka
                                        </td>
                                        <td width="20%">
                                        </td>
                                    </tr>
                                    <%
                                            index++;
                                        }
                                    %>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <b>Total taka.</b>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <div class="mt-3" style="border-top: 1px solid black;">
                                </div>
                            </div>
                        </div>
                        <div class="col-10 offset-1" style="padding-top: 10px">
                            <div class="mt-2 table-responsive">
                                <table class="table-borderless" style="width: 100%;">
                                    <thead>
                                    <tr>
                                        <td width="10%">
                                            C
                                        </td>
                                        <td colspan="7">
                                            Professional fees of the medical attendant for consultation on detailed
                                            below :-
                                        </td>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td width="10%">
                                        </td>
                                        <td width="5%">
                                        </td>
                                        <td width="15%">
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="20%">
                                            Vide receipts
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="20%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                        </td>
                                        <td colspan="7">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%">
                                        </td>
                                        <td colspan="7">
                                        </td>
                                    </tr>
                                    <%
                                        index = 1;
                                        while (index <= 2) {
                                    %>
                                    <tr>
                                        <td width="10%">
                                        </td>
                                        <td width="5%">
                                            <%=index%>.
                                        </td>
                                        <td width="15%">
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="10%">
                                        </td>
                                        <td width="20%">
                                        </td>
                                        <td width="10%">
                                            Taka
                                        </td>
                                        <td width="20%">
                                        </td>
                                    </tr>
                                    <%
                                            index++;
                                        }
                                    %>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="2" style="text-align: right">
                                            <b>Total taka.</b>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="2">
                                            <b>Grand Total taka.</b>
                                        </td>
                                        <td>
                                            <%
                                                val = sum + ".00";
                                            %>
                                            <%=Utils.getDigits(val, "BANGLA")%>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <div class="mt-3" style="border-top: 1px solid black;">
                                    <div class="mt-1" style="text-align: center">
                                        (In words Taka
                                        (<%=BangladeshiNumberInWord.convertToWord(String.valueOf(Utils.getDigits(sum, "BANGLA")))%>
                                        টাকা মাত্র)
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="page shadow" data-size="A4" style="margin-top: 20px">
                        <div class="col-10 offset-1" style="padding-top: 10px">
                            <div class="mt-2 table-responsive">
                                <table class="table-borderless" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <td width="10%">
                                            D
                                        </td>
                                        <td colspan="7">
                                            Certified that the distance between my residence and the hospital
                                            is............................................
                                        </td>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td width="10%"></td>
                                        <td colspan="7">
                                            .....................................................................
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="mt-2" style="padding-left: 15%">
                            <p>
                                1.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Certified that the amount claimed in
                                this bill was not drawn before.
                            </p>
                            <p>
                                2.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Certified that the amount claimed in
                                this bill was actually incurred by me for the treatment of<br>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; myself.
                            </p>
                            <p>
                                3.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Certified that the amount claimed in
                                this bill was actually incurred for the treatment of my<br>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; only wife is dependent on me
                                and is residing with me.
                            </p>
                            <p>
                                4.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Certified that the amount claimed in
                                this bill was actually incurred for the treatment of my<br>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; Son/Daughter who is/are
                                residing with me and dependent on me.
                            </p>
                            <div class="form-group row" style="padding-top: 20px">
                                <div class="col-6"></div>
                                <div class=" col-6 form-group row d-flex align-items-center">
                                    <label class="col-form-label text-left">
                                        <p>Signature of the peoples servant<br>
                                            Designation.<br>

                                            pay including special pay<br>

                                            if any .........................................<br>

                                            or No .........................................<br>
                                        </p>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="page shadow" data-size="A4" style="margin-top: 20px">

                        <div class="mt-2" style="padding-left: 5%;padding-top: 10%">
                            <p>
                                1.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Certified that medicine, drugs etc.
                                included in the voucher detailed in the pre-page for the total cost<br>
                                amounting to Tk (     <%=Utils.getDigits(sum, "Bangla")%>     )
                                &nbsp;(In words) taka
                                ................<%=BangladeshiNumberInWord.convertToWord(Utils.getDigits(sum, "Bangla"))%>
                                ...............&nbsp; only. were prescribed<br>
                                by me and were essential for the recovery and rest oration of the health of
                                Mr/Mrs/Miss/Master for<br>
                                Self/wife/daughter/son of the Government servant and their medicines, drugs, etc.
                                were not a dietary nature.
                            </p>
                            <p>
                                2.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Certified that neither these medicines,
                                drugs etc. not their effective substitutes were available at the<br>
                                time in the hospital.
                            </p>
                            <p>
                                3.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Certified that various X-Rays,
                                pathological, Bacteriological examination etc. specified in the vouchers<br>
                                detailed on the pre-page amounting to Tk ................................ (In words)<br>taka
                                ............................................. were actually necessary for the proper
                                diagnosis and the treatment of the<br>
                                patient.
                            </p>
                            <p>
                                4.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Certified that Mr/Mrs/Miss/Master
                                ......................................... Self/wife/daughter/ son of the<br>
                                Government servant whose signature is given on the pre-page was attended to by me in
                                my consulting rook for<br>
                                which fees amounting to Tk .........................................<br>(In words)
                                .............................................................. were received by me
                                vide receipt quoted on the pre-<br>page.
                            </p>
                            <p>
                                5.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Certified that Mr/Mrs/Miss/Master
                                ......................................................................
                                Self/wife/daughter/ son<br>
                                of the Government servant whose signature is given on the pre-page was referred by
                                me to <br>Dr
                                ......................................... for amounting to Tk
                                .................................................... (In words)<br>Taka
                                ...............................
                                ......................................was received by him/her vide receipt quoted on
                                the pro-page.
                            </p>
                            <p>
                                6.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Certified that the case was of emergent
                                nature and attendance at my private chamber or residence<br>
                                beyond duty hours was absolutely necessary in the interest of the health of the
                                patient.
                            </p>
                            <p>
                                7.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Certified that the case is neither of
                                pre-natural nornpost natural in nature.
                            </p>
                            <p>
                                8.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;It is also certified that the civil
                                surgeon/Assistant surgeon/sub-Assistant surgeon was not available in<br>
                                the hospital at the moment.
                            </p>
                            <div class="form-group row" style="padding-top: 20px">
                                <div class="col-6"></div>
                                <div class=" col-6 form-group row d-flex align-items-center">
                                    <label class="col-form-label text-left">
                                        <p>Signature<br>
                                            Authorized Medical Officer<br>
                                            Designation ..................... <br>
                                            Post<br>
                                        </p>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="page shadow" data-size="A4">
                        <div class="text-center">
                            <h1>বাংলাদেশ জাতীয় সংসদ</h1>
                            <h3><b>
                                দপ্তরের
                                নামঃ&nbsp;<%=budgetOfficeNameBn%>
                            </b>
                            </h3>
                            <h3>
                                <b>
                                    <%-- ১০২ সংসদ কোড-অপারেশন কোড-৩১১১৩২৭ অধিকাল ভাতা কোড --%>
                                    কোড নংঃ
                                    ১০২-<%=Budget_operationRepository.getInstance().getCode(budgetMappingDTO.budgetOperationId, "Bangla")%>
                                    -৩১১১৩১১
                                </b>
                            </h3>
                        </div>

                        <div>
                            <div class="mt-4">
                                <div class="row">
                                    <div class="col-4 fix-fill">
                                        <strong>কর্মকর্তার নামঃ <%=empNameBn%>
                                        </strong>
                                    </div>
                                    <div class="col-4 fix-fill">
                                        <strong>পদবীঃ <%=organogramNameBn%>
                                        </strong>
                                    </div>
                                    <div class="col-4 fix-fill">
                                        <strong>দপ্তরঃ <%=officeNameBn%>
                                        </strong>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 15px">
                                    <div class="col-3 fix-fill">
                                        ভবিষ্যত তহবিল নং-
                                    </div>
                                    <div class="col-1 fix-fill">
                                    </div>
                                    <div class="col-4 fix-fill">
                                        ডাক জীবন বীমা নং-
                                    </div>
                                    <div class="col-4 fix-fill">
                                        করদাতা সনাক্তকরণ নম্বর(টিআইএন)
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 10px">
                                    <div class="col-3 fix-fill">
                                        টোকেন নং
                                        <div class="blank-to-fill"></div>
                                    </div>
                                    <div class="col-3 fix-fill">
                                        তারিখ
                                        <div class="blank-to-fill">
                                        </div>
                                    </div>
                                    <div class="col-3 fix-fill">
                                        ভাউচার নং
                                        <div class="blank-to-fill">
                                            &nbsp;&nbsp;<%=Utils.getDigits(medical_allowanceDTO.voucherNumber, "Bangla")%>
                                        </div>
                                    </div>
                                    <div class="col-3 fix-fill">
                                        তারিখ
                                        <div class="blank-to-fill">
                                            &nbsp;&nbsp;<%=StringUtils.getFormattedDate("BANGLA", medical_allowanceDTO.voucherDate)%>
                                        </div>
                                    </div>
                                </div>

                                <table class="table-bordered mt-2 w-100">
                                    <thead>
                                    <tr>
                                        <th width="3%"></th>
                                        <th width="37%">নির্দেশাবলী</th>
                                        <th width="35%">বিবরণ</th>
                                        <th width="25%">টাকা</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="align-top">১</td>
                                        <td class="align-top">
                                            অবিলিকৃত/স্থ’গিত টাকা যথাযথ কলামে লাল কালিতে লিখিতে হইবে এবং যোগ দেওয়ার
                                            সময় উহা বাদ
                                            রাখিতে হইবে।
                                        </td>
                                        <td>* ৩১১১৩১১ - চিকিৎসা ভাতা</td>
                                        <td class="text-right">
                                            <%=totalAllowanceStr%>/-
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-top"> ২</td>
                                        <td class="align-top">
                                            বেতন বৃদ্ধিও সার্টিফিকেট বা অনুুপস্থিত কর্মচারীগণের তালিকায়ক স্থান পায়
                                            নাই এমন ঘটনাসমূহ যথা-মৃত্যু, অবসর গ্রহণ, স্থায়ী বদরী ও প্রথম নিয়োগ
                                            মন্তব্য কলামে
                                            লিখিতে
                                            হইবে।
                                        </td>
                                        <td class="text-right">
                                            <strong>মোট দাবী (ক)</strong>
                                        </td>
                                        <td class="text-right">
                                            <%=totalAllowanceStr%>/-
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="align-top"> ৩</td>
                                        <td class="align-top">
                                            কোন দাবিকৃত বেতন বৃদ্ধি সরকারী কর্মচারীর দক্ষতার সীমা অতিক্রম করার আওতায়
                                            পডিলে
                                            সংশ্লিষ্ট কর্মচারী উক্ত সীমা অতিক্রম করার উপযুক্ত কর্তৃপক্ষের প্রত্যায়ন
                                            দ্বারা
                                            সমর্থিত হইতে হইবে। (এস আর ১৫৬)।
                                        </td>
                                        <td class="text-center">
                                            <strong>কর্তন ও আয়ঃ</strong>
                                        </td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td class="align-top"> ৪</td>
                                        <td class="align-top">
                                            অধঃস্তন সরকারী কর্মচারী এবং এস. আর. ১৫২ তে উলি­খিত সরকারী সরকারী
                                            কর্মচারদের নাম
                                            বেতনের বিলে বাদ দেওয়া যাইতে পারে।
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td class="align-top"> ৫</td>
                                        <td class="align-top">
                                            অধঃস্তন সরকারী কর্মচারী এবং এস. আর. ১৫২ তে উলি­খিত সরকারী সরকারী
                                            কর্মচারদের নাম
                                            বেতনের বিলে বাদ দেওয়া যাইতে পারে।
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td class="align-top"> ৬</td>
                                        <td class="align-top">
                                            স্থায়ী পদে নিযুক্ত ব্যক্তিদের নাম স্থায়ী পদের বেতন গ্রহণের মাপ কাঠিতে
                                            জ্যেষ্ঠত্বের
                                            ক্রম অনুসারে লিখিতে হইবে এবং খালি পদসমূহ স্থানাপন্ন লোকদিগকে দেখাইতে
                                            হইবে।
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td class="align-top" rowspan="3"> ৭</td>
                                        <td class="align-top" rowspan="3">
                                            বেতন বিলে কর্তন ও অদায়ের পৃথক পৃথক সিডিউল বেতনের বিলে সংযুক্ত করিতে
                                            হইবে।
                                        </td>
                                        <td class="text-right">
                                            <strong>
                                                মোট কর্তন আদায় (খ)
                                            </strong>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">
                                            <strong>
                                                নীট দাবী (ক-খ)
                                            </strong>
                                        </td>
                                        <td class="text-right">
                                            <%=totalAllowanceStr%>/-
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <br>
                                            প্রদানের জন্য নীট টাকার প্রয়োজন কথায় <%=amountInWord%> টাকা (মাত্র)
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="foot-note">
                            <div>
                                * কেবল মাত্র অর্থনৈতিক কোড বুঝায়।<br>
                                ** সর্ম্পূণ ১৩ অংকের কোড দেওয়া হইয়াছে।
                            </div>
                        </div>

                        <p class="mt-2">
                            ১. (ক) বিলের টাকা বুঝিয়া পাইলাম <br>
                            (খ) প্রত্যয়ন করিতেছি যে, নিম্নে বিশদভাবে বর্ণিত টাকা (যাহা এই বিল হইতে কর্তন করিয়া ফেরত
                            দেওয়া
                            হইয়াছে) ব্যতীত এই তারিখের <br> ১* মাস/২ মাস/৩ মাস পূর্বে উত্তোলিত বিলের অন্তভূক্ত টাকা
                            যথার্থ
                            ব্যক্তিদের
                            প্রদান করা হইয়াছে। <br>
                            * প্রযোজ্য ক্ষেত্রে টিক ঢিহ্ন দিন। <br>
                            (গ) প্রত্যয়ন করিতেছি যে, কর্মচারীদের নিকট হইতে অর্থ প্রাপ্তির ষ্ট্যাম্পসহ রশিদ গ্রহন
                            করিয়া বেতন সহিত
                            সংযুক্ত করা হইয়াছে।<br>
                            ২. বিলের সাথে একটি অনুপস্থিতির তালিকা প্রদান করা হইল।<br>
                            ৩. প্রত্যয়ন করা যাইতেছে যে, এই কার্যালয়ের সকল নিয়োগ, স্থায়ী ও অস্থায়ী পদোন্নতি সংক্রান্ত
                            তথ্যাদি সংশ্লিষ্ট কর্মচারীগণের নিজ নিজ চাকুরী বহিতে আমার সত্যায়নে লিপিবদ্ধ হইয়াছে।<br>
                            ৪. প্রত্যায়ন করা যাইতেছে, চাকুরী বহিতে প্রাপ্য ছুটির হিসাব এবং প্রযোজ্য ছুটির বিধি
                            অনুয়ায়ী
                            প্রাপ্য ছুটি ছাড়া কাহাকেও কোন ছুটি মঞ্জুর করা হয় নাই। আমি নিশ্চিত যে তাহাদের ছুটি পাওনা
                            ছিল এবং সকল
                            ছুটির মঞ্জুরী ও ছুটিতে বা ছুটি হইতে ফিরিয়া আসা, সাময়িক কর্মচ্যুতি ও অন্য কাজে যাওয়া ও
                            অন্যান্য ঘটনা
                            নিয়ম
                            মোতাবেক চাকুরী বহিতে এবং ছুটির হিসাবে আমার সত্যায়নে লিপিবদ্ধ করা হইযাছে।<br>
                            ৫. প্রত্যায়ন করা যাইতেছে যে, যে সকল সরকারী কর্মচারীর নাম উলে­খ করা হয় নাই, কিন্তু এই
                            বিলে
                            বেতন দাবী করা হইয়াছে। চলতি মাসে তাহারা যথার্থই সরকারী চাকুরীতে নিয়োজিত ছিলেন।<br>
                            ৬. প্রত্যায়ন করা যাইতেছে যে, যে সকল সরকারী কর্মচারীর বাড়ী ভাড়া ভাতা এই বিলে দাবী করা
                            হইয়াছে,
                            তাহারা সরকারী কোন বাসস্থানে বসবাস করেন নাই।<br>
                            ৭. প্রত্যায়ন করা যাইতেছে, যে ক্ষেত্রে ছুটির/অস্থায়ী বদলী কালীন সময়ের জন্য ক্ষতিপূরণ ভাতা
                            দাবী
                            করা হইয়াছে, সেই ক্ষেত্রে কর্মচারীর একই বা স¦পদে ফিরিয়া আসার সম্ভাব্যতা ছুটি/অস্থায়ী
                            বদলীর মূল আদেশে
                            লিপিবদ্ধ করা হইয়াছে।<br>
                            ৮. প্রত্যায়ন করা যাইতেছে যে, কর্মচারীদের ছুটি কালীন বেতন, ছুটিতে যাওয়ার সময় যে হারে বেতন
                            গ্রহণ করিতেছিলেন, সেই হাওে দাবী করা হইয়াছে।<br>
                            ৯. প্রত্যায়ন করা যাইতেছে যে, অবসর গ্রহণ করিয়াছেন এমন কোন কর্মচারীর নাম এই বিলে
                            অন্তর্ভূক্ত
                            করা হয় নাই।<br>
                        </p>
                    </section>

                    <section class="page shadow" data-size="A4">
                        <h3 class="text-center">অনুপস্থিত ব্যক্তির ফেরত দেওয়া বেতনের বিবরণ</h3>
                        <table class="table-bordered w-100">
                            <thead>
                            <tr>
                                <th width="8%">সেকশন</th>
                                <th width="50%">নাম</th>
                                <th width="15%">সময়</th>
                                <th width="27%">টাকার অংক (টা./পয়সা)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td rowspan="4"></td>
                                <td rowspan="4"></td>
                                <td class="text-right">
                                    বাজেটে বরাদ্দ =
                                </td>
                                <td class="text-right">
                                    <%=allocatedBudgetStr%>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    মোট খরচ =
                                </td>
                                <td class="text-right">
                                    <%=totalAllowanceStr%>/-
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    অবশিষ্ট =
                                </td>
                                <td>
                                    <%=remainingBudgetStr%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="row mt-4">
                            <div class="col-6">
                                <div class="mt-1">
                                    <div class="fix-fill mt-4 w-100">
                                        স্থান
                                        <div class="blank-to-fill"></div>
                                    </div>
                                    <div class="fix-fill mt-3 w-100">
                                        তারিখ
                                        <div class="blank-to-fill"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mt-1">
                                    <div class="fix-fill mt-4 w-100">
                                        আয়ন কর্মকর্তার স্বাক্ষর
                                        <div class="blank-to-fill"></div>
                                    </div>
                                    <div class="fix-fill mt-3 w-100">
                                        নাম
                                        <div class="blank-to-fill"></div>
                                    </div>
                                    <div class="fix-fill mt-3 w-100">
                                        পদবী
                                        <div class="blank-to-fill"></div>
                                    </div>
                                    <div class="fix-fill mt-5 w-100">
                                        সীল
                                        <div class="blank-to-fill"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-3" style="border-top: 5px solid black;">
                            <div class="text-center mt-2">
                                <h2>হিসাবরক্ষণ অফিসে ব্যবহারের জন্য</h2>
                            </div>
                            <div class="row mt-4">
                                <div class="col-4 fix-fill">
                                    টাকা
                                    <div class="blank-to-fill">
                                        &nbsp;&nbsp; <%=totalAllowanceStr%>/-
                                    </div>
                                </div>
                                <div class="col-8 fix-fill">
                                    (কথায়)
                                    <div class="blank-to-fill">
                                        &nbsp;&nbsp;<%=amountInWord%> টাকা (মাত্র)
                                    </div>
                                </div>
                                প্রদানের জন্য পাস কর হল
                            </div>

                            <div class="row mt-5">
                                <div class="col-4">
                                    <div>
                                        <strong>অডিটর (স্বাক্ষর)</strong>
                                    </div>
                                    <div class="fix-fill mt-5">
                                        নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                    </div>
                                    <div class="fix-fill mt-3">
                                        তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div>
                                        <strong>সুপার (স্বাক্ষর)</strong>
                                    </div>
                                    <div class="fix-fill mt-5">
                                        নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                    </div>
                                    <div class="fix-fill mt-3">
                                        তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div>
                                        <strong>হিসাবরক্ষণ অফিসার (স্বাক্ষর)</strong>
                                    </div>
                                    <div class="fix-fill mt-5">
                                        নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                    </div>
                                    <div class="fix-fill mt-3">
                                        তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>