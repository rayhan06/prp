<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="ultrasono_type.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Ultrasono_typeDTO ultrasono_typeDTO;
    ultrasono_typeDTO = (Ultrasono_typeDTO) request.getAttribute("ultrasono_typeDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (ultrasono_typeDTO == null) {
        ultrasono_typeDTO = new Ultrasono_typeDTO();

    }
    System.out.println("ultrasono_typeDTO = " + ultrasono_typeDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ULTRASONO_TYPE_ADD_ULTRASONO_TYPE_ADD_FORMNAME, loginDTO);
    String servletName = "Ultrasono_typeServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.ULTRASONO_TYPE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Ultrasono_typeServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row mx-2 mx-md-0">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=ultrasono_typeDTO.iD%>' tag='pb_html'/>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ULTRASONO_TYPE_ADD_GENDERCAT, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='genderCat'
                                                            id='genderCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            Options = CatDAO.getOptions(Language, "gender", ultrasono_typeDTO.genderCat);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ULTRASONO_TYPE_ADD_NAMEEN, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='nameEn'
                                                           id='nameEn_text_<%=i%>' value='<%=ultrasono_typeDTO.nameEn%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ULTRASONO_TYPE_ADD_NAMEBN, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='nameBn'
                                                           id='nameBn_text_<%=i%>' value='<%=ultrasono_typeDTO.nameBn%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ULTRASONO_TYPE_ADD_HEADING, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='heading'
                                                           id='heading_text_<%=i%>'
                                                           value='<%=ultrasono_typeDTO.heading%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ULTRASONO_TYPE_ADD_DETAILS, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea class='form-control' name='details'
                                                              id='details_text_<%=i%>'
                                                              tag='pb_html'><%=ultrasono_typeDTO.details%></textarea>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>' value='<%=ultrasono_typeDTO.isDeleted%>'
                                                   tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=ultrasono_typeDTO.lastModificationTime%>' tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-right mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.ULTRASONO_TYPE_ADD_ULTRASONO_TYPE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.ULTRASONO_TYPE_ADD_ULTRASONO_TYPE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Ultrasono_typeServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        console.log("inb");
        init(row);
        CKEDITOR.replaceAll();
        console.log("inb2");

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






