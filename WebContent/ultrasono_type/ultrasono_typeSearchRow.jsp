<%@page pageEncoding="UTF-8" %>

<%@page import="ultrasono_type.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ULTRASONO_TYPE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ULTRASONO_TYPE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Ultrasono_typeDTO ultrasono_typeDTO = (Ultrasono_typeDTO) request.getAttribute("ultrasono_typeDTO");
    CommonDTO commonDTO = ultrasono_typeDTO;
    String servletName = "Ultrasono_typeServlet";


    System.out.println("ultrasono_typeDTO = " + ultrasono_typeDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Ultrasono_typeDAO ultrasono_typeDAO = (Ultrasono_typeDAO) request.getAttribute("ultrasono_typeDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_genderCat' class="text-nowrap">
    <%
        value = ultrasono_typeDTO.genderCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "gender", ultrasono_typeDTO.genderCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameEn' class="text-nowrap">
    <%
        value = ultrasono_typeDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_heading' class="text-nowrap">
    <%
        value = ultrasono_typeDTO.heading + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Ultrasono_typeServlet?actionType=view&ID=<%=ultrasono_typeDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;" type="button"
            onclick="location.href='Ultrasono_typeServlet?actionType=getEditPage&ID=<%=ultrasono_typeDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=ultrasono_typeDTO.iD%>'/>
        </span>
    </div>
</td>
																						
											

