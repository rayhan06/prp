<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="pb.*" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="political_party.Political_partyRepository" %>
<%@ page import="geolocation.GeoDivisionRepository" %>
<%@ page import="geolocation.GeoDistrictRepository" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ELECTION_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row">
    <div class="col-12">
        <div class="row mx-4 mx-md-0">

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.ELECTION_REPORT_WHERE_ELECTIONDETAILSID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' id="electionDetailsId" name="electionDetailsId">
                            <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.ELECTION_REPORT_WHERE_ELECTIONCONSTITUENCYID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='electionConstituencyId' id='electionConstituencyId'>
                            <%=Election_constituencyRepository.getInstance().buildOptions(Language, 0L)%>
                        </select>
                    </div>
                </div>
            </div>

            <%--district and division display none for now--%>
            <div class="search-criteria-div col-md-6" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODIVISIONTYPE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='divisionId' id='divisionId'>
                            <%=GeoDivisionRepository.getInstance().getOptions(Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6" style="display: none">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.ELECTION_CONSTITUENCY_ADD_GEODISTRICTTYPE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='districtId' id='districtId'>
                            <%=GeoDistrictRepository.getInstance().getDistrictOptionUpdated(Language, null)%>
                        </select>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.ELECTION_REPORT_WHERE_POLITICALPARTYID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='politicalPartyId' id='politicalPartyId'>
                            <%=Political_partyRepository.getInstance().buildOptions(Language, 0L)%>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function init() {
        dateTimeInit($("#Language").val());
        select2SingleSelector('#electionDetailsId', '<%=Language%>');
        select2SingleSelector('#electionConstituencyId', '<%=Language%>');
        select2SingleSelector('#politicalPartyId', '<%=Language%>');
        select2SingleSelector('#districtId', '<%=Language%>');
        select2SingleSelector('#divisionId', '<%=Language%>');
    }

    function PreprocessBeforeSubmiting() {
    }
</script>