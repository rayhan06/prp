<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.List" %>
<%@ page import="util.UtilCharacter" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="vm_maintenance_vehicle_item_mapping.Vm_maintenance_vehicle_item_mappingDAO" %>
<%@ page import="vm_maintenance.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
//    Vm_maintenanceDAO vm_maintenanceDAO = new Vm_maintenanceDAO();
    long ID = Long.parseLong(request.getParameter("ID"));

    Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().getVm_maintenanceDTOByID(ID);
    vm_maintenanceDTO.vmMaintenanceItemDTOList = VmMaintenanceItemRepository.getInstance().
            getVmMaintenanceItemDTOByMaintenanceId(ID);


//            (Vm_maintenanceDTO)vm_maintenanceDAO.
//            getDTOByID(ID);
//    VmMaintenanceItemDAO vmMaintenanceItemDAO = new VmMaintenanceItemDAO();
//    List<VmMaintenanceItemDTO> vmMaintenanceItemDTOs = vmMaintenanceItemDAO.
//            getVmMaintenanceItemDTOListByVmMaintenanceID(vm_maintenanceDTO.iD);

    Vm_maintenance_vehicle_item_mappingDAO item_mappingDAO = new Vm_maintenance_vehicle_item_mappingDAO();

    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);

    String formTitle = UtilCharacter.getDataByLanguage(Language, "মেকানিক অনুমোদন", "Mechanic Approve");
    String value = "";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<%@include file="mechanic_approve_form.jsp" %>

<script type="text/javascript">


    let index = <%=index%>;



    $("#add-more-VmMaintenanceItem").click(
        function(e)
        {
            e.preventDefault();
            let t = $("#template-VmMaintenanceItem");
            $("#field-VmMaintenanceItem").append(t.html());

            $('#field-VmMaintenanceItem tr:last td:first-child').html(++index);

        });


    function reject(){
        event.preventDefault();
        let url = "Vm_maintenance_request_approval_mappingServlet?actionType=rejectVm_fuel_request&ID="+'<%=ID%>';
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            success: function(fetchedData) {
                window.location = 'Vm_maintenance_request_approval_mappingServlet?actionType=search';
            },
            error: function(error) {
                console.log(error);
            }
        });

    }

    function approveMechanic(){
        event.preventDefault();
        // which items are clicked in the table
        let formData = new FormData();
        let tablename = 'field-VmMaintenanceItem';
        let i = 0;
        let element = document.getElementById(tablename);

        let j = 0;
        // formData.append("itemLength", document.getElementById(tablename).childNodes.length);
        // console.log(document.getElementById(tablename).childNodes.length);
        for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
        {
            let tr = document.getElementById(tablename).childNodes[i];
            // console.log(tr)
            if(tr.nodeType === Node.ELEMENT_NODE)
            {
                let id;
                id = 'itemId_' + j;
                // console.log(id);
                let itemId = tr.querySelector('input[name="itemId"]');
                formData.append(id, itemId.value);
                if(itemId.value == -100){
                    let parts = tr.querySelector('select[name="vmMaintenanceItem.vmVehiclePartsType"]');
                    if(!parts.value || parts.value.toString().length == 0 || parts.value/1 <= 0){
                        toastr.error("Please select parts");
                        return ;
                    }
                    id = 'parts_' + j;
                    formData.append(id, parts.value);
                    console.log(parts.value);


                    let type = tr.querySelector('select[name="vmMaintenanceItem.vehicleMaintenanceCat"]');
                    if(!type.value || type.value.toString().length == 0 || type.value/1 <= 0){
                        toastr.error("Please select type");
                        return ;
                    }
                    id = 'type_' + j;
                    formData.append(id, type.value);

                    let dRemarks = tr.querySelector('input[name="vmMaintenanceItem.remarks"]');
                    // console.log(mRemarks.value);
                    id = 'dRemarks_' + j;
                    formData.append(id, dRemarks.value);

                    let amount = tr.querySelector('input[name="vmMaintenanceItem.amount"]');
                    if(!amount.value || amount.value.toString().length == 0 || amount.value/1 <= 0){
                        toastr.error("Amount required");
                        return ;
                    }
                    id = 'amount_' + j;
                    formData.append(id, amount.value);

                }
                // console.log(itemId.value)
                let checkbox = tr.querySelector('input[deletecb="true"]');
                id = 'checked_' + j;
                formData.append(id, checkbox.checked);
                // console.log(checkbox.checked );
                let mPrice = tr.querySelector('input[name="vmMaintenanceItem.mechanicPrice"]');
                if(checkbox.checked){
                    if(!mPrice.value || mPrice.value.toString().length == 0 || mPrice.value/1 <= 0){
                        toastr.error("Please give price");
                        return ;
                    }
                }
                id = 'mPrice_' + j;
                formData.append(id, mPrice.value);
                // console.log(mPrice.value);
                let mRemarks = tr.querySelector('input[name="vmMaintenanceItem.mechanicRemarks"]');
                // console.log(mRemarks.value);
                id = 'mRemarks_' + j;
                formData.append(id, mRemarks.value);


                j ++;
            }



        }

        // console.log(j);

        formData.append("itemLength", j);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                window.location = 'Vm_maintenance_request_approval_mappingServlet?actionType=search';
            }
            else if (this.readyState == 4 && this.status != 200) {
                //alert('failed ' + this.status);
            }
        };



        let params= "Vm_maintenance_request_approval_mappingServlet?actionType=approvedVm_fuel_request&ID="+'<%=ID%>';
        xhttp.open("POST",params, true);
        xhttp.send(formData);
    }

</script>