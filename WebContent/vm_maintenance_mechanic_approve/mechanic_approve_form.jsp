<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.Utils" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="vm_vehicle_parts.Vm_vehicle_partsRepository" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%
    String context = request.getContextPath() + "/";

    value = vm_maintenanceDTO.applicationDate + "";
    String formatted_applicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));

    value = UtilCharacter.getDataByLanguage(Language,
            "গাড়ি মেরামত প্রসঙ্গে ", "Related to Vehicle Maintenance");
%>

<!-- begin:: Subheader -->
<%--<div class="ml-auto mr-3 mt-4">--%>
<%--    <%--%>
<%--        if (true) {--%>
<%--    %>--%>
<%--    <button type="button" class="btn" id='printer'--%>
<%--            onclick="downloadPdf('prescription.pdf','modalbody')">--%>
<%--        &lt;%&ndash;        <%=LM.getText(LC.HM_PDF, loginDTO)%>&ndash;%&gt;--%>
<%--        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--    </button>--%>
<%--    <button type="button" class="btn" id='printer'--%>
<%--            onclick="printDiv('modalbody')">--%>
<%--        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--        &lt;%&ndash;        <%=LM.getText(LC.HM_PRINT, loginDTO)%>&ndash;%&gt;--%>
<%--    </button>--%>
<%--    <%--%>
<%--        }--%>
<%--    %>--%>
<%--</div>--%>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;">
        <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
            <form class="form-horizontal"
                  action=""
                  id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                  onsubmit="">
                <div class="row">
                    <div class="col-12 text-center">
                        <img
                                width="10%"
                                src="<%=context%>assets/static/parliament_logo.png"
                                alt="logo"
                                class="logo-default"
                        />
                        <h2 class="mt-2">
                            <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়",
                                    "Bangladesh Parliament Secretariat")%>

                        </h2>
                        <h4>
                            <%=UtilCharacter.getDataByLanguage(Language, "পরিবহন শাখা", "Vehicle Unit")%>

                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <%=UtilCharacter.getDataByLanguage(Language, "বিষয়", "Subject")%>: <%=value%>
                    </div>
                    <div class="col-4 text-center table-title">
                        www.parliament.gov.bd
                    </div>
                    <div class="col-4 text-right">
                        <%=LM.getText(LC.VM_MAINTENANCE_ADD_APPLICATIONDATE, loginDTO)%>
                        : <%=Utils.getDigits(formatted_applicationDate, Language)%>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "বিবরণ", "Description")%>
                    </h5>
                    <div class="table responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            </thead>
                            <tbody>
                            <tr id="vehicleIdDiv">
                                <td class="font-weight-bold text-nowrap">
                                    <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEID, loginDTO)%>
                                </td>
                                <%
                                    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_maintenanceDTO.vehicleId);
                                    value = "";
                                    if (vm_vehicleDTO != null) {
                                        value = vm_vehicleDTO.regNo;
                                    }
                                %>
                                <td class="text-nowrap">
                                    <%=value%>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-nowrap">
                                    <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
                                </td>
                                <%
                                    Vm_vehicle_driver_assignmentDTO assignmentDTO = Vm_vehicle_driver_assignmentRepository.getInstance()
                                            .getVm_vehicle_driver_assignmentDTOByID(vm_maintenanceDTO.vehicleDriverAssignmentId);
                                    value = "";
                                    if (assignmentDTO != null) {
                                        value = UtilCharacter.getDataByLanguage(Language,
                                                assignmentDTO.employeeRecordNameBn, assignmentDTO.employeeRecordName);
                                    }
                                %>
                                <td class="text-nowrap">
                                    <%=value%>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-nowrap">
                                    <%=LM.getText(LC.VM_MAINTENANCE_ADD_LASTMAINTENANCEDATE, loginDTO)%>
                                </td>
                                <%
                                    value = "";
                                    if (vm_vehicleDTO != null && vm_vehicleDTO.last_maintenance_date > 0) {
                                        value = simpleDateFormat.format(new Date(Long.parseLong
                                                (vm_vehicleDTO.last_maintenance_date + "")));
                                    }
                                %>
                                <td class="text-nowrap">
                                    <%=value%>
                                </td>
                            </tr>
                            <%--                            <tr>--%>
                            <%--                                <td class="table-title text-nowrap">--%>
                            <%--                                    <%=UtilCharacter.getDataByLanguage(Language, "বিষয়", "Subject")%>--%>
                            <%--                                </td>--%>
                            <%--                                <td class="text-nowrap">--%>
                            <%--                                    <%=value%>--%>
                            <%--                                </td>--%>
                            <%--                            </tr>--%>
                            <%--                            <tr>--%>
                            <%--                                <td class="table-title text-nowrap">--%>
                            <%--                                    <%=LM.getText(LC.VM_MAINTENANCE_ADD_APPLICATIONDATE, loginDTO)%>--%>
                            <%--                                </td>--%>
                            <%--                                <td class="text-nowrap">--%>
                            <%--                                    <%=Utils.getDigits(formatted_applicationDate, Language)%>--%>
                            <%--                                </td>--%>
                            <%--                            </tr>--%>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM, loginDTO)%>
                    </h5>
                    <div class="table responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial No")%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_VMVEHICLEPARTSTYPE, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_VEHICLEMAINTENANCECAT, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_AMOUNT, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_REMARKS, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "পূর্ব মেরামতের তারিখ ", "Last Repair Date")%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_DRIVERPRICE, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "টিক দিন ", "Select")%>
                                </th>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "মন্তব্য  ", "Remarks")%>
                                </th>
                                <%--                                    <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_REMOVE, loginDTO)%></th>--%>
                            </tr>
                            </thead>
                            <tbody id="field-VmMaintenanceItem">
                            <%
                                int index = 0;
                                for (VmMaintenanceItemDTO vmMaintenanceItemDTO : vm_maintenanceDTO.vmMaintenanceItemDTOList) {
                                    index++;
                            %>
                            <tr>
                                <%--                                    <%--%>
                                <%--                                        System.out.println("#####");--%>
                                <%--                                        System.out.println(vmMaintenanceItemDTO);--%>
                                <%--                                    %>--%>

                                <td class="text-nowrap">
                                    <%=index%>
                                </td>
                                <td class="text-nowrap">
                                    <input id="" type="hidden" name="itemId" value="<%=vmMaintenanceItemDTO.iD%>">
                                    <%
                                        value = Vm_vehicle_partsRepository.getInstance().getText(Language,
                                                vmMaintenanceItemDTO.vmVehiclePartsType);
                                    %>
                                    <%
//                                        value = CommonDAO.getName(Integer.parseInt(value), "vm_vehicle_parts", Language.equals("English") ? "name_en" : "name_bn", "id");
                                    %>

                                    <%=Utils.getDigits(value, Language)%>
                                </td>
                                <td class="text-nowrap">
                                    <%
                                        value = vmMaintenanceItemDTO.vehicleMaintenanceCat + "";
                                    %>
                                    <%
                                        value = CatRepository.getInstance().getText(Language, "vehicle_maintenance", vmMaintenanceItemDTO.vehicleMaintenanceCat);
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td class="text-nowrap">
                                    <%
                                        value = vmMaintenanceItemDTO.amount + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td class="text-nowrap">
                                    <%
                                        value = vmMaintenanceItemDTO.remarks + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td class="text-nowrap">
                                    <%
                                        value = item_mappingDAO.getDate(vm_vehicleDTO.iD, vmMaintenanceItemDTO.vmVehiclePartsType);
                                    %>

                                    <%=Utils.getDigits(value, Language)%>
                                </td>


                                <td class="text-nowrap">
                                    <%
                                        value = "";
                                        if (vmMaintenanceItemDTO.driverPrice != -1) {
                                            value = vmMaintenanceItemDTO.driverPrice + "";
                                        }
                                    %>
                                    <input type='number' class='form-control' name='vmMaintenanceItem.mechanicPrice'
                                           id='mechanicPrice_number_<%=index%>' value='<%=value%>' tag='pb_html'>
                                </td>
                                <td class="text-nowrap">
										<span id='chkEdit'>
											<input type='checkbox' name='vmMaintenanceItem.checkbox' checked
                                                   deletecb="true"
                                                   class="form-control-sm"/>
										</span>
                                </td>
                                <td class="text-nowrap">
                                    <input type='text' class='form-control' name='vmMaintenanceItem.mechanicRemarks'
                                           id='mechanicRemarks_<%=index%>' value='' tag='pb_html'>

                                </td>
                            </tr>

                            <%
                                }
                            %>

                            </tbody>

                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-9 text-right">
                            <button
                                    id="add-more-VmMaintenanceItem"
                                    name="add-moreVmMaintenanceItem"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                            </button>
                        </div>
                    </div>
                    <%VmMaintenanceItemDTO vmMaintenanceItemDTO = new VmMaintenanceItemDTO();%>
                    <template id="template-VmMaintenanceItem">
                        <tr>
                            <td name='serialNo'>
                            </td>
                            <td>
                                <input id="" type="hidden" name="itemId" value="-100">
                                <select class='form-control' name='vmMaintenanceItem.vmVehiclePartsType'
                                        id='vmVehiclePartsType_select_' tag='pb_html'>
                                    <%
                                        String Options = Vm_vehicle_partsRepository.getInstance().getBuildOptions
                                                (Language, vmMaintenanceItemDTO.vmVehiclePartsType + "");

//                                        String Options = CommonDAO.getOptions(Language, "vm_vehicle_parts", vmMaintenanceItemDTO.vmVehiclePartsType);
                                    %>
                                    <%=Options%>
                                </select>
                            </td>
                            <td>
                                <select class='form-control' name='vmMaintenanceItem.vehicleMaintenanceCat'
                                        id='vehicleMaintenanceCat_category_' tag='pb_html'>
                                    <%
                                        Options = CatRepository.getInstance().buildOptions("vehicle_maintenance", Language, vmMaintenanceItemDTO.vehicleMaintenanceCat);
                                    %>
                                    <%=Options%>
                                </select>
                            </td>
                            <td>
                                <%
                                    value = "";
                                    if (vmMaintenanceItemDTO.amount != -1) {
                                        value = vmMaintenanceItemDTO.amount + "";
                                    }
                                %>
                                <input type='number' class='form-control' name='vmMaintenanceItem.amount'
                                       id='amount_number_' value='<%=value%>' tag='pb_html'>
                            </td>
                            <td>
                                <input type='text' class='form-control' name='vmMaintenanceItem.remarks'
                                       id='remarks_text_' value='<%=vmMaintenanceItemDTO.remarks%>' tag='pb_html'/>
                            </td>
                            <td></td>
                            <td>
                                <%
                                    value = "";
                                    if (vmMaintenanceItemDTO.driverPrice != -1) {
                                        value = vmMaintenanceItemDTO.driverPrice + "";
                                    }
                                %>
                                <input type='number' class='form-control' name='vmMaintenanceItem.mechanicPrice'
                                       id='' value='' tag='pb_html'>
                            </td>
                            <td>
                                    <span id=''>
											<input type='checkbox' name='vmMaintenanceItem.checkbox' checked
                                                   deletecb="true"
                                                   class="form-control-sm"/>
										</span>
                            </td>
                            <td>
                                <input type='text' class='form-control' name='vmMaintenanceItem.mechanicRemarks'
                                       id='' value='' tag='pb_html'>
                            </td>
                        </tr>
                    </template>
                </div>
                <div class="form-actions text-center mt-4">
                    <button onclick="reject()" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=UtilCharacter.getDataByLanguage(Language, "অনুমোদিত নহে ", "Reject")%>
                    </button>
                    <button onclick="approveMechanic()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                            type="submit">
                        <%=UtilCharacter.getDataByLanguage(Language, "অনুমোদন", "Approve")%>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>