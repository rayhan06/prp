
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="select_demo.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>


<%
Select_demoDTO select_demoDTO;
select_demoDTO = (Select_demoDTO)request.getAttribute("select_demoDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(select_demoDTO == null)
{
	select_demoDTO = new Select_demoDTO();
	
}
System.out.println("select_demoDTO = " + select_demoDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.SELECT_DEMO_EDIT_SELECT_DEMO_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.SELECT_DEMO_ADD_SELECT_DEMO_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Select_demoServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.SELECT_DEMO_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=select_demoDTO.iD%>' tag='pb_html'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SELECT_DEMO_EDIT_FILEINDEXTYPETYPE, loginDTO)):(LM.getText(LC.SELECT_DEMO_ADD_FILEINDEXTYPETYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'fileIndexTypeType_div_<%=i%>'>	
		<select class='form-control'  name='fileIndexTypeType' id = 'fileIndexTypeType_select_<%=i%>' onchange="onFileIndexTypeChange(this.value)"  tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "file_index_type", "fileIndexTypeType_select_" + i, "form-control", "fileIndexTypeType", select_demoDTO.fileIndexTypeType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "file_index_type", "fileIndexTypeType_select_" + i, "form-control", "fileIndexTypeType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.SELECT_DEMO_EDIT_FILEINDEXSUBTYPETYPE, loginDTO)):(LM.getText(LC.SELECT_DEMO_ADD_FILEINDEXSUBTYPETYPE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'fileIndexSubtypeType_div_<%=i%>'>	
		<select class='form-control'  name='fileIndexSubtypeType' id = 'fileIndexSubtypeType_select_<%=i%>'  tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "file_index_subtype", "fileIndexSubtypeType_select_" + i, "form-control", "fileIndexSubtypeType", select_demoDTO.fileIndexSubtypeType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "file_index_subtype", "fileIndexSubtypeType_select_" + i, "form-control", "fileIndexSubtypeType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + select_demoDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + select_demoDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	






				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.SELECT_DEMO_EDIT_SELECT_DEMO_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.SELECT_DEMO_ADD_SELECT_DEMO_CANCEL_BUTTON, loginDTO)%>
						<%
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						%>
						<%=LM.getText(LC.SELECT_DEMO_EDIT_SELECT_DEMO_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					else
					{
						%>
						<%=LM.getText(LC.SELECT_DEMO_ADD_SELECT_DEMO_SUBMIT_BUTTON, loginDTO)%>
						<%
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Select_demoServlet");	
}

function init(row)
{


	
}

var row = 0;
	

var child_table_extra_id = <%=childTableStartingID%>;

function onFileIndexTypeChange(value) {

		console.log("value = " + value);
       var xhttp = new XMLHttpRequest();
       xhttp.onreadystatechange = function () {
           if (this.readyState == 4 && this.status == 200) 
           {
              document.getElementById("fileIndexSubtypeType_select_<%=i%>").innerHTML = this.responseText;
           }
       };

       xhttp.open("POST", "Select_demoServlet?actionType=getSubIndex&Language=<%=Language%>&id=" + value, true);
       xhttp.send();
    
}



</script>






