<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_SERGEANT_AT_ARMS_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2 mx-md-4">
    <div class="col-12">
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=LM.getText(LC.GATE_PASS_SERGEANT_AT_ARMS_REPORT_WHERE_SECONDLAYERAPPROVALSTATUS, loginDTO)%>
				</label>
				<div class="col-md-9">
<%--					<input class='form-control'  name='secondLayerApprovalStatus' id = 'secondLayerApprovalStatus' value=""/>--%>
					<select class='form-control' name='secondLayerApprovalStatus' id='secondLayerApprovalStatus'
							style="width: 100%">
						<%=CatRepository.getInstance().buildOptions("sergeant_arms_approval_status", Language, CatDTO.CATDEFAULT)%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=LM.getText(LC.GATE_PASS_SERGEANT_AT_ARMS_REPORT_WHERE_GATEPASSTYPEID, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='gatePassTypeId' id = 'gatePassTypeId' value=""/>
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
	const isVisible = [true, true, true, true, true, true, true, true, true, true, true, true, false];
	
	function init()
{
    dateTimeInit($("#Language").val());
    showFooter = false;
}
function PreprocessBeforeSubmiting()
{
}
	function getLink(list)
	{
		var id = convertToEnglishNumber(list[12]);

		return "Gate_passServlet?actionType=armsOfficeView&ID=" + id;
	}
</script>