

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="nothi_history.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Nothi_historyServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Nothi_historyDTO nothi_historyDTO = Nothi_historyDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = nothi_historyDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.NOTHI_HISTORY_ADD_NOTHI_HISTORY_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.NOTHI_HISTORY_ADD_NOTHI_HISTORY_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NOTHI_HISTORY_ADD_NOTHITYPE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CommonDAO.getName(Language, "nothi", nothi_historyDTO.nothiType)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NOTHI_HISTORY_ADD_NOTHICONFIGURATIONID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(nothi_historyDTO.nothiConfigurationId, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NOTHI_HISTORY_ADD_NOTHINUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=nothi_historyDTO.nothiNumber%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NOTHI_HISTORY_ADD_DESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=nothi_historyDTO.description%>
                                    </div>
                                </div>
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NOTHI_HISTORY_ADD_FROMOFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(nothi_historyDTO.fromOfficeId, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NOTHI_HISTORY_ADD_TOOFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(nothi_historyDTO.toOfficeId, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NOTHI_HISTORY_ADD_INSERTEDBYERID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(nothi_historyDTO.insertedByErId, Language)%>
                                    </div>
                                </div>
			
			
			
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.NOTHI_HISTORY_ADD_TRANSFERDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, nothi_historyDTO.transferDate), Language)%>
                                    </div>
                                </div>
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>