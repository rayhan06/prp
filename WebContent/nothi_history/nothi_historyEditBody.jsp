<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="nothi_history.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="nothi.*"%>

<%
Nothi_historyDTO nothi_historyDTO = new Nothi_historyDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	nothi_historyDTO = Nothi_historyDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = nothi_historyDTO;
String tableName = "nothi_history";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.NOTHI_HISTORY_ADD_NOTHI_HISTORY_ADD_FORMNAME, loginDTO);
String servletName = "Nothi_historyServlet";
boolean isLangEng = Language.equalsIgnoreCase("english");
boolean isPrivilleged = Utils.isPrivillegedHRUser(userDTO);
%>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Nothi_historyServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=nothi_historyDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.NOTHI_HISTORY_ADD_NOTHITYPE, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='nothiType' 
																id = 'nothiType_select_<%=i%>'   
																onchange = 'getDetails(this.value)'
																tag='pb_html'>
																<%
																
																	Options = NothiDAO.getInstance().getOptions(isLangEng);

																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
														
																					
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.NOTHI_HISTORY_ADD_DESCRIPTION, loginDTO)%>															
															</label>
                                                            <div class="col-8" id = "descriptionDIv">
																
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_FROM, loginDTO)%>	<%=LM.getText(LC.HM_OFFICE, loginDTO)%>														
															</label>
                                                            <div class="col-8" id = "fromOfficeDiv">
																
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_TO, loginDTO)%>	<%=LM.getText(LC.HM_OFFICE, loginDTO)%>															
															</label>
                                                            <div class="col-8" >
																<%--Office Unit Modal Trigger Button--%>
								                                <button type="button"
								                                        class="btn btn-block shadow btn-primary text-white btn-border-radius"
								                                        id="office_units_id_modal_button" onclick="officeModalButtonClicked();">
								                                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
								                                </button>
								
								                                <div class="input-group" id="office_units_id_div" style="display: none">
								                                    <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
								                                    <button type="button"
								                                            class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
								                                            id="office_units_id_text" onclick="officeModalEditButtonClicked()">
								                                    </button>
								                                    <span class="input-group-btn" style="width: 5%;padding-right: 5px;" tag='pb_html'>
								                                        <button type="button" class="btn btn-outline-danger"
								                                                onclick="crsBtnClicked('office_units_id');"
								                                                id='office_units_id_crs_btn' tag='pb_html'>
								                                            x
								                                        </button>
								                                    </span>
								                                </div>
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='toOfficeId' 
														id = 'toOfficeId' value='<%=nothi_historyDTO.toOfficeId%>' tag='pb_html'/>
													
													 <div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.NOTHI_HISTORY_ADD_TRANSFERDATE, loginDTO)%>															</label>
                                                            <div class="col-8">
																<%value = "transferDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='transferDate' id = 'transferDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(nothi_historyDTO.transferDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.NOTHI_HISTORY_ADD_NOTHI_HISTORY_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.NOTHI_HISTORY_ADD_NOTHI_HISTORY_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	preprocessDateBeforeSubmitting('transferDate', row);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Nothi_historyServlet");	
}

function init(row)
{

	setDateByStringAndId('transferDate_js_' + row, $('#transferDate_date_' + row).val());

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

function getDetails(id)
{
	console.log('id: ' + id);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
        	var nothiDTO = JSON.parse(this.responseText);
        	$("#fromOfficeDiv").html(nothiDTO.fromOfficeName);
        	$("#descriptionDIv").html(nothiDTO.description);
        } else {
            //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
        }
    };

    xhttp.open("GET", "NothiServlet?actionType=getDetails&id=" + id , true);
    xhttp.send();
}
function crsBtnClicked(fieldName) {
    $('#' + fieldName + '_modal_button').show();
    $('#' + fieldName + '_div').hide();
    $('#' + fieldName + '_input').val('');
    document.getElementById(fieldName + '_text').innerHTML = '';
    document.getElementById('search_table').innerHTML = '';
    lastSelectedOfficeId = '';
    $('#toOfficeId').val('-1');
}

function viewOfficeIdInInput(selectedOffice) {
    if (selectedOffice.id === '') {
        return;
    }
    $('#office_units_id_modal_button').hide();
    $('#office_units_id_div').show();

    document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
    $('#toOfficeId').val(selectedOffice.id);

    lastSelectedOfficeId = selectedOffice.id;
}

officeSelectModalUsage = 'none';
officeSelectModalOptionsMap = new Map([
    ['officeUnitId', {
        officeSelectedCallback: viewOfficeIdInInput
    }]
]);
function officeModalButtonClicked() {
    officeSelectModalUsage = 'officeUnitId';
    $('#search_office_modal').modal();
}

function officeModalEditButtonClicked() {
    officeSelectModalUsage = 'officeUnitId';
    officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
    $('#search_office_modal').modal();
}

function buttonStateChangeFunction(value) {
    $("button").prop('disabled', value);
    $("input").prop('disabled', value);
}

</script>






