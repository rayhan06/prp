<%@page pageEncoding="UTF-8" %>

<%@page import="pharma_company_name.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PHARMA_COMPANY_NAME_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PHARMA_COMPANY_NAME;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Pharma_company_nameDTO pharma_company_nameDTO = (Pharma_company_nameDTO) request.getAttribute("pharma_company_nameDTO");
    CommonDTO commonDTO = pharma_company_nameDTO;
    String servletName = "Pharma_company_nameServlet";


    System.out.println("pharma_company_nameDTO = " + pharma_company_nameDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Pharma_company_nameDAO pharma_company_nameDAO = (Pharma_company_nameDAO) request.getAttribute("pharma_company_nameDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn' class="text-nowrap">
    <%
        value = pharma_company_nameDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn' class="text-nowrap">
    <%
        value = pharma_company_nameDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_address' class="text-nowrap">
    <%
        value = pharma_company_nameDTO.address + "";
    %>
    <%=GeoLocationDAO2.getAddressToShow(value, Language)%>


</td>

<td id='<%=i%>_contact' class="text-nowrap">
    <%
        value = pharma_company_nameDTO.contact + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_email' class="text-nowrap">
    <%
        value = pharma_company_nameDTO.email + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Pharma_company_nameServlet?actionType=view&ID=<%=pharma_company_nameDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Pharma_company_nameServlet?actionType=getEditPage&ID=<%=pharma_company_nameDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
	<div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=pharma_company_nameDTO.iD%>'/>
        </span>
	</div>
</td>
																						
											

