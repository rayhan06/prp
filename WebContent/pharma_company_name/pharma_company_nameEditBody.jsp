<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="pharma_company_name.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Pharma_company_nameDTO pharma_company_nameDTO;
    pharma_company_nameDTO = (Pharma_company_nameDTO) request.getAttribute("pharma_company_nameDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (pharma_company_nameDTO == null) {
        pharma_company_nameDTO = new Pharma_company_nameDTO();

    }
    System.out.println("pharma_company_nameDTO = " + pharma_company_nameDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PHARMA_COMPANY_NAME_ADD_PHARMA_COMPANY_NAME_ADD_FORMNAME, loginDTO);
    String servletName = "Pharma_company_nameServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.PHARMA_COMPANY_NAME_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pharma_company_nameServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white"><%=formTitle%>
                                                </h4>
                                            </div>
                                        </div>
                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=pharma_company_nameDTO.iD%>' tag='pb_html'/>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PHARMA_COMPANY_NAME_ADD_NAMEEN, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9">
                                                <input type='text' class='form-control' name='nameEn'
                                                       id='nameEn_text_<%=i%>'
                                                       value='<%=pharma_company_nameDTO.nameEn%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"><%=LM.getText(LC.PHARMA_COMPANY_NAME_ADD_NAMEBN, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9">
                                                <input type='text' class='form-control' name='nameBn'
                                                       id='nameBn_text_<%=i%>'
                                                       value='<%=pharma_company_nameDTO.nameBn%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"><%=LM.getText(LC.PHARMA_COMPANY_NAME_ADD_ADDRESS, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9">
                                                <div id='address_geoDIV_<%=i%>' tag='pb_html'>
                                                    <select class='form-control' name='address_active'
                                                            id='address_geoSelectField_<%=i%>'
                                                            onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'address', this.getAttribute('row'))"
                                                            tag='pb_html' row='<%=i%>'></select>
                                                </div>
                                                <input type='text' class='form-control' name='address_text'
                                                       id='address_geoTextField_<%=i%>'
                                                       onkeypress="return (event.charCode != 36 && event.keyCode != 36)"
                                                       value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(pharma_company_nameDTO.address)  + "'"):("'" + "" + "'")%>
                                                               placeholder='Road Number, House Number etc'
                                                tag='pb_html'>
                                                <input type='hidden' class='form-control' name='address'
                                                       id='address_geolocation_<%=i%>'
                                                       value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(pharma_company_nameDTO.address)  + "'"):("'" + "1" + "'")%>
                                                               tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                %>
                                                <label class="control-label"><%=GeoLocationDAO2.parseText(pharma_company_nameDTO.address, Language) + "," + GeoLocationDAO2.parseDetails(pharma_company_nameDTO.address)%>
                                                </label>
                                                <%
                                                    }
                                                %>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"><%=LM.getText(LC.PHARMA_COMPANY_NAME_ADD_CONTACT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9">
                                                <input type='text' class='form-control' name='contact'
                                                       id='contact_text_<%=i%>'
                                                       value='<%=pharma_company_nameDTO.contact%>'                                                                <%
                                                    if (!actionName.equals("edit")) {
                                                %>
                                                       required="required" pattern="880[0-9]{10}"
                                                       title="contact must start with 880, then contain 10 digits"
                                                        <%
                                                            }
                                                        %>
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-xl-3 col-form-label text-md-right"><%=LM.getText(LC.PHARMA_COMPANY_NAME_ADD_EMAIL, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 col-xl-9">
                                                <input type='text' class='form-control' name='email'
                                                       id='email_text_<%=i%>'
                                                       value='<%=pharma_company_nameDTO.email%>'                                                                <%
                                                    if (!actionName.equals("edit")) {
                                                %>
                                                       required="required"
                                                       pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                       title="email must be a of valid email address format"
                                                        <%
                                                            }
                                                        %>
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <input type='hidden' class='form-control' name='searchColumn'
                                               id='searchColumn_hidden_<%=i%>'
                                               value='<%=pharma_company_nameDTO.searchColumn%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='isDeleted'
                                               id='isDeleted_hidden_<%=i%>'
                                               value='<%=pharma_company_nameDTO.isDeleted%>' tag='pb_html'/>

                                        <input type='hidden' class='form-control' name='lastModificationTime'
                                               id='lastModificationTime_hidden_<%=i%>'
                                               value='<%=pharma_company_nameDTO.lastModificationTime%>'
                                               tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-actions text-right mt-3">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn btn-border-radius">
                                <%=LM.getText(LC.PHARMA_COMPANY_NAME_ADD_PHARMA_COMPANY_NAME_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius" type="submit">
                                <%=LM.getText(LC.PHARMA_COMPANY_NAME_ADD_PHARMA_COMPANY_NAME_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        return preprocessGeolocationBeforeSubmitting('address', row, false);

        // return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pharma_company_nameServlet");
    }

    function init(row) {

        initGeoLocation('address_geoSelectField_', row, "Pharma_company_nameServlet");


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






