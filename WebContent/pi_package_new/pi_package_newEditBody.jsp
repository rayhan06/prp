<%@page import="pi_package_new.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="pi_package_lot.Pi_package_lotDTO" %>

<%@include file="../pb/addInitializer2.jsp" %>
<%
    Pi_package_newDTO pi_package_newDTO = new Pi_package_newDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_package_newDTO = Pi_package_newDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_package_newDTO;
    String tableName = "pi_package_new";


    long currentOfficeUnitId = -1;

    String nameOfOffice = "";
    if (actionName.equalsIgnoreCase("ajax_edit")) {
        Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(pi_package_newDTO.officeUnitId);
        if (unit != null) {
            nameOfOffice = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
        }
    } else {
        OfficeUnitOrganograms organogramsDTO = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (organogramsDTO != null) {
            Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organogramsDTO.office_unit_id);
            if (office_unitsDTO != null) {
                nameOfOffice = UtilCharacter.getDataByLanguage(Language, office_unitsDTO.unitNameBng, office_unitsDTO.unitNameEng);
                currentOfficeUnitId = office_unitsDTO.iD;
            }
        }
    }
%>

<%
    String formTitle = LM.getText(LC.PI_PACKAGE_NEW_ADD_PI_PACKAGE_NEW_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_package_newServlet";
%>

<style>
    .hiddenElement {
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=pi_package_newDTO.iD%>'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PI_APP_REQUEST_ADD_OFFICEUNITID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8 col-md-6">
                                            <input type="hidden" name='officeUnitId'
                                                   id='office_units_id_input' value="">
                                            <button type="button"
                                                    class="btn btn-secondary form-control shadow btn-border-radius"
                                                    disabled id="office_units_id_text"></button>
                                        </div>
                                        <div class="col-4 col-md-3 text-right">
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="office_units_id_modal_button"
                                                    onclick="officeModalButtonClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নাম্বার ইংলিশ", "Package Number English")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control englishOnly noPaste'
                                                   name='packageNumberEn'
                                                   id='packageNumberEn_text_<%=i%>'
                                                   value='<%=pi_package_newDTO.packageNumberEn%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নাম্বার বাংলা", "Package Number Bangla")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control noEnglish noPaste'
                                                   name='packageNumberBn'
                                                   id='packageNumberBn_text_<%=i%>'
                                                   value='<%=pi_package_newDTO.packageNumberBn%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="packageNameEn">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PACKAGENAMEEN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control englishOnly noPaste'
                                                   name='packageNameEn'
                                                   id='packageNameEn_text_<%=i%>'
                                                   value='<%=pi_package_newDTO.packageNameEn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="packageNameBn">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PACKAGENAMEBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control noEnglish noPaste'
                                                   name='packageNameBn'
                                                   id='packageNameBn_text_<%=i%>'
                                                   value='<%=pi_package_newDTO.packageNameBn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <label class="col-4 col-form-label text-right">
                        <%=UtilCharacter.getDataByLanguage(Language, "আপনি কি লট সংযুক্ত করতে চান?", "Do you want to add lot?")%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-8">
                        <input type='checkbox' class='form-control-sm' name='hasLot' id='hasLot_checkbox_<%=i%>'
                               onclick="toggleLot()" value="<%=pi_package_newDTO.hasLot%>">
                    </div>
                </div>
                <div class="mt-4" id="lot-form" style="display: none">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=UtilCharacter.getDataByLanguage(Language, "লট", "Lot")%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "লট নাম্বার ইংলিশ", "Lot Number English")%>
                                    </th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "লট নাম্বার বাংলা", "Lot Number Bangla")%>
                                    </th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "লট নাম ইংলিশ", "Lot Name English")%>
                                    </th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "লট নাম বাংলা", "Lot Name Bangla")%>
                                    </th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "সরিয়ে দিন", "Remove")%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="lot-form-body">
                                <%
                                    if (actionName.equals("ajax_edit")) {
                                        int index = -1;
                                        for (Pi_package_lotDTO model : pi_package_newDTO.pi_package_lotDTOS) {
                                            index++;
                                %>

                                <tr id="lot_<%=index + 1%>">
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name='lot.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=model.iD%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name='insertionDate'
                                               id='insertionDate_<%=childTableStartingID%>'
                                               value='<%=model.insertionDate%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name='insertedBy'
                                               id='insertedBy_<%=childTableStartingID%>'
                                               value='<%=model.insertedBy%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name='lastModificationTime'
                                               id='lastModificationTime_<%=childTableStartingID%>'
                                               value='<%=model.lastModificationTime%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name='modifiedBy'
                                               id='modifiedBy_<%=childTableStartingID%>'
                                               value='<%=model.modifiedBy%>'/>
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' name='lotNumberEn'
                                               id='lotNumberEn_text_<%=childTableStartingID%>'
                                               value='<%=model.lotNumberEn%>'/>
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' name='lotNumberBn'
                                               id='lotNumberBn_text_<%=childTableStartingID%>'
                                               value='<%=model.lotNumberBn%>'/>
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' name='lotNameEn'
                                               id='lotNameEn_text_<%=childTableStartingID%>'
                                               value='<%=model.lotNameEn%>'/>
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' name='lotNameBn'
                                               id='lotNameBn_text_<%=childTableStartingID%>'
                                               value='<%=model.lotNameBn%>'/>
                                    </td>
                                    <td>
                                        <button
                                                id='removeLotRow_button_<%=childTableStartingID%>'
                                                name="removeLotRow"
                                                type="button"
                                                class="btn btn-sm remove-btn shadow ml-2 pl-4"
                                                onclick="removeLotRowFromList(this)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-9 text-right">
                                <button
                                        id="add-more-RecruitmentExamVenueItem"
                                        name="add-moreRecruitmentExamVenueItem"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow"
                                        onclick="addNewLotRow()">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                            </div>
                        </div>

                        <%Pi_package_lotDTO model = new Pi_package_lotDTO();%>
                        <template id="template-lot-row">
                            <tr>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control' name='lot.iD'
                                           id='iD_hidden_' value='<%=model.iD%>'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_<%=childTableStartingID%>'
                                           value='<%=model.insertionDate%>'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_<%=childTableStartingID%>'
                                           value='<%=model.insertedBy%>'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_<%=childTableStartingID%>'
                                           value='<%=model.lastModificationTime%>'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_<%=childTableStartingID%>'
                                           value='<%=model.modifiedBy%>'/>
                                <td>
                                    <input type='text' class='form-control' name='lotNumberEn'
                                           id='lotNumberEn_text' value='<%=model.lotNumberEn%>'/>
                                </td>
                                <td>
                                    <input type='text' class='form-control' name='lotNumberBn'
                                           id='lotNumberBn_text' value='<%=model.lotNumberBn%>'/>
                                </td>
                                <td>
                                    <input type='text' class='form-control' name='lotNameEn'
                                           id='lotNameEn_text' value='<%=model.lotNameEn%>'/>
                                </td>
                                <td>
                                    <input type='text' class='form-control' name='lotNameBn'
                                           id='lotNameBn_text' value='<%=model.lotNameBn%>'/>
                                </td>
                                <td>
                                    <button
                                            id='removeLotRow_button_<%=childTableStartingID%>'
                                            name="removeLotRow"
                                            type="button"
                                            class="btn btn-sm remove-btn shadow ml-2 pl-4"
                                            onclick="removeLotRowFromList(this)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        </template>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PI_PACKAGE_NEW_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                    onclick="submitForm()">
                                <%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PI_PACKAGE_NEW_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    const recruitmentTestForm = $("#bigform");
    let actionName = '<%=actionName%>';
    let language = '<%=Language%>';
    let hasLot = false;
    const lotBody = document.getElementById('lot-form-body');
    const lotRowTemplate = document.getElementById('template-lot-row');
    let packageNameEn = '';
    let packageNameBn = '';

    // Related to office selector modal start
    let officeSelectModalUsage = 'none';
    let officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    // Related to office selector modal end

    function toggleLot() {
        hasLot = !hasLot;
        if (hasLot) {
            // open div for lot entry
            hideLotForm(false);
        } else {
            // close div for lot entry
            hideLotForm(true);
        }
    }

    function hidePackageName(hide) {
        let packageNameEn = document.getElementById('packageNameEn');
        let packageNameBn = document.getElementById('packageNameBn');
        if (hide) {
            clearPackageName();
            $(packageNameEn).addClass('hiddenElement');
            $(packageNameBn).addClass('hiddenElement');
        } else {
            restorePackageName();
            $(packageNameEn).removeClass('hiddenElement');
            $(packageNameBn).removeClass('hiddenElement');
        }
    }

    function removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }

    function hideLotForm(hide) {
        let lotForm = document.getElementById('lot-form');
        let lotFormBody = document.getElementById('lot-form-body');
        if (hide) {
            lotForm.style.display = 'none';
            removeAllChildNodes(lotFormBody);
            hasLot = false;
            hidePackageName(false);
        } else {
            lotForm.style.display = 'block';
            hasLot = true;
            hidePackageName(true);
        }
    }

    function clearPackageName() {
        let packageNameEnElement = document.getElementById('packageNameEn_text_0');
        let packageNameBnElement = document.getElementById('packageNameBn_text_0');
        packageNameEn = packageNameEnElement.value;
        packageNameBn = packageNameBnElement.value;
        packageNameEnElement.value = '';
        packageNameBnElement.value = '';
    }

    function restorePackageName() {
        let packageNameEnElement = document.getElementById('packageNameEn_text_0');
        let packageNameBnElement = document.getElementById('packageNameBn_text_0');
        packageNameEnElement.value = packageNameEn;
        packageNameBnElement.value = packageNameBn;
    }

    function isLotCountValid() {
        let lotCount = document.getElementsByName('lot.iD').length;
        if (lotCount >= 5) {
            swal.fire(getValueByLanguage(language, "৫ টার বেশি লট তৈরি করা যাবে না!", "Can not create more than 5 lot!"));
            return false;
        }
        return true;
    }

    function addNewLotRow() {
        if (isLotCountValid()) {
            const newLotRow = $("#template-lot-row");
            $("#lot-form-body").append(newLotRow.html());
        }
    }

    function removeLotRowFromList(elem) {
        let tr = elem.parentNode.parentNode;
        let lotId = tr.querySelector("td>input[name='lot.iD']").value;
        if (!lotId || lotId === "-1" || lotId === "") {
            tr.remove();
            showToastSticky("লট ডিলিট হয়েছে", "Delete Successful");
        } else {
            let url = "Pi_package_newServlet?actionType=deleteLotWithValidation&lotId=" + lotId;
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    response = JSON.parse(response);
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        tr.remove();
                        showToastSticky("লট ডিলিট হয়েছে", "Delete Successful");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting()) {
            let url = "<%=servletName%>?actionType=<%=actionName%>";
            $.ajax({
                type: "POST",
                url: url,
                data: recruitmentTestForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        showToastSticky("সাবমিট সফল হয়েছে", "Submit Successful");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 3000);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function PreprocessBeforeSubmiting() {
        recruitmentTestForm.validate();
        return recruitmentTestForm.valid();
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function init(row) {
        if (actionName === 'ajax_edit') {
            viewOfficeIdInInput({
                name: '<%=nameOfOffice%>',
                id: <%=pi_package_newDTO.officeUnitId%>
            });
        } else {
            viewOfficeIdInInput({
                name: '<%=nameOfOffice%>',
                id: <%=currentOfficeUnitId%>
            });
        }
    }

    function makeLotFormVisibleIfRequired() {
        const hasLotCheckBox = document.getElementById('hasLot_checkbox_0');
        console.log("checked: ", hasLotCheckBox.checked)
        if (hasLotCheckBox.value === "true") {
            hasLotCheckBox.checked = true;
            hideLotForm(false);
        } else {
            hideLotForm(true);
        }
    }

    function loadRestoreVariableForPackageName() {
        let packageNameEnElement = document.getElementById('packageNameEn_text_0');
        let packageNameBnElement = document.getElementById('packageNameBn_text_0');
        packageNameEn = packageNameEnElement.value;
        packageNameBn = packageNameBnElement.value;
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        loadRestoreVariableForPackageName();
        makeLotFormVisibleIfRequired();
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $(".englishOnly").keypress(function (event) {
        var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
        if (allowed.includes(event.key)) return true;
        var ew = event.which;
        if (ew == 32)
            return true;
        if (48 <= ew && ew <= 57)
            return true;
        if (65 <= ew && ew <= 90)
            return true;
        if (97 <= ew && ew <= 122)
            return true;
        return false;
    });
    $(".noEnglish").keypress(function (event) {
        var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
        if (allowed.includes(event.key)) return true;
        var ew = event.which;

        if (48 <= ew && ew <= 57)
            return false;
        if (65 <= ew && ew <= 90)
            return false;
        if (97 <= ew && ew <= 122)
            return false;
        return true;
    });

    function onInputPasteNoEnglish(event) {
        var clipboardData = event.clipboardData || window.clipboardData;
        var data = clipboardData.getData("Text");
        var valid = data.toString().split('').every(char => {
            var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
            if (allowed.includes(char)) return true;
            if ('0' <= char && char <= '9')
                return false;
            if ('a' <= char && char <= 'z')
                return false;
            if ('A' <= char && char <= 'Z')
                return false;
            return true;
        });
        if (valid) return;
        event.stopPropagation();
        event.preventDefault();
    }

    function onInputPasteEnglishOnly(event) {
        var clipboardData = event.clipboardData || window.clipboardData;
        var data = clipboardData.getData("Text");
        var valid = data.toString().split('').every(char => {
            var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
            if (allowed.includes(char)) return true;
            if (char.toString().trim() == '')
                return true;
            if ('0' <= char && char <= '9')
                return true;
            if ('a' <= char && char <= 'z')
                return true;
            if ('A' <= char && char <= 'Z')
                return true;
            return false;
        });
        if (valid) return;
        event.stopPropagation();
        event.preventDefault();

        document.querySelectorAll("input.noPaste").forEach(function (input) {
            input.addEventListener("paste", onInputPaste);
        });

        var elements = document.getElementsByClassName("englishOnly");

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('paste', onInputPasteEnglishOnly);
        }

        elements = document.getElementsByClassName("noEnglish");

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('paste', onInputPasteNoEnglish);
        }

        function onInputPaste(event) {
            var clipboardData = event.clipboardData || window.clipboardData;
            if (/^[0-9০-৯]+$/g.test(clipboardData.getData("Text"))) return;
            event.stopPropagation();
            event.preventDefault();
        }
    }
</script>






