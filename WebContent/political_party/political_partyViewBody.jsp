<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="util.StringUtils" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="political_party.*" %>
<%@ page import="java.util.*" %>
<%@page import="files.*" %>
<%@ page import="geolocation.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="pb.Utils" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String value;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    Political_partyDTO political_partyDTO = (Political_partyDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    boolean isLangEnglish = Language.equalsIgnoreCase("English");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.POLITICAL_PARTY_ADD_POLITICAL_PARTY_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_NAMEEN, loginDTO)%>
                            </b></td>
                            <td><%=political_partyDTO.nameEn%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_NAMEBN, loginDTO)%>
                            </b></td>
                            <td><%=political_partyDTO.nameBn%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_ABBREVIATION, loginDTO)%>
                            </b></td>
                            <td><%= political_partyDTO.abbreviation%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=isLangEnglish ? "Registration No" : "নিবন্ধন নম্বর"%>
                            </b></td>
                            <td><%= political_partyDTO.registrationNo == 0 ? "" : Utils.getDigits(political_partyDTO.registrationNo, Language)%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=isLangEnglish ? "Registration Date" : "নিবন্ধনের তারিখ"%>
                            </b></td>
                            <td><%= political_partyDTO.registrationDate == SessionConstants.MIN_DATE ? "" : StringUtils.getFormattedDate(Language, political_partyDTO.registrationDate)%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_PRESIDENTNAME, loginDTO)%>
                            </b></td>
                            <td><%= political_partyDTO.presidentName %>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_GSNAME, loginDTO)%>
                            </b></td>
                            <td><%= political_partyDTO.gsName %>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_FOUNDATIONDATE, loginDTO)%>
                            </b></td>
                            <td>
                                <%=StringUtils.getFormattedDate(Language, political_partyDTO.foundationDate)%>

                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_OFFICEADDRESS, loginDTO)%>
                            </b></td>
                            <td>
                                <%=GeoLocationUtils.getGeoLocationString(political_partyDTO.officeAddress, Language)%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=isLangEnglish ? "Party Symbol Name" : "দলীয় প্রতীকের নাম"%>
                            </b></td>
                            <td><%=isLangEnglish ? political_partyDTO.symbolNameEng : political_partyDTO.symbolNameBng%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=isLangEnglish ? "Party Symbol" : "দলীয় প্রতীক"%>
                            </b></td>
                            <td>
                                <%
                                    if (political_partyDTO.electoralSymbolBlob != null) {
                                        byte[] encodeBase64 = Base64.encodeBase64(political_partyDTO.electoralSymbolBlob);
                                        value = Arrays.toString(political_partyDTO.electoralSymbolBlob);
                                %>
                                <img src='data:image/jpg;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                                <a href='Political_partyServlet?actionType=downloadBlob&name=electoralSymbolBlob&id=<%=political_partyDTO.iD%>'
                                   download='<%=value%>'>Download</a>
                                <% } %>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_PARTYFLAGBLOB, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    if (political_partyDTO.partyFlagBlob != null) {
                                        byte[] encodeBase64 = Base64.encodeBase64(political_partyDTO.partyFlagBlob);
                                        value = Arrays.toString(political_partyDTO.partyFlagBlob);
                                %>
                                <img src='data:image/jpg;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                                <a href='Political_partyServlet?actionType=downloadBlob&name=partyFlagBlob&id=<%=political_partyDTO.iD%>'
                                   download='<%=value%>'>Download</a>
                                <% } %>
                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=isLangEnglish ? "Phone" : "ফোন"%>
                            </b></td>
                            <td><%=Utils.getDigits(political_partyDTO.phoneNumber, Language)%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=isLangEnglish ? "Mobile" : "মোবাইল"%>
                            </b></td>
                            <td><%=Utils.getDigits("88" + political_partyDTO.mobileNumber, Language)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_WEBSITE, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    if (political_partyDTO.website != null && political_partyDTO.website.trim().length() > 0) {
                                %>
                                <a href='<%=political_partyDTO.website.trim()%>'><%=political_partyDTO.website.trim()%>
                                </a>
                                <%} %>
                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.POLITICAL_PARTY_ADD_EMAIL, loginDTO)%>
                            </b></td>
                            <td><%=political_partyDTO.email%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=Language.equalsIgnoreCase("English") ? "Attached Document" : "সংযুক্ত নথি"%>
                            </b></td>
                            <td>
                                <% {
                                    List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(political_partyDTO.filesDropzone);
                                %>
                                <table>
                                    <tr><%
                                        if (FilesDTOList != null) {
                                            for (int j = 0; j < FilesDTOList.size(); j++) {
                                                FilesDTO filesDTO = FilesDTOList.get(j);
                                                byte[] encodeBase64 = Base64.encodeBase64(filesDTO.thumbnailBlob);
                                    %>
                                        <td>
                                            <%
                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                            %>
                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                 style='width:100px'/>
                                            <%
                                                }
                                            %>
                                            <a href='Employee_education_infoServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                               download><%=filesDTO.fileTitle%>
                                            </a>
                                        </td>
                                        <%
                                                }
                                            }%>
                                    </tr>
                                </table>
                                <%}%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>