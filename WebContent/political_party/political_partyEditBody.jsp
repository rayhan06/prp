<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="political_party.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="pb.CatRepository" %>

<%
    Political_partyDTO political_partyDTO;
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String actionName;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        political_partyDTO = (Political_partyDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        political_partyDTO = new Political_partyDTO();
    }
    String formTitle = LM.getText(LC.POLITICAL_PARTY_ADD_POLITICAL_PARTY_ADD_FORMNAME, loginDTO);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    int i = 0;
    String value;
    long ColumnID;
    boolean isLangEnglish = Language.equalsIgnoreCase("English");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="bigform" name="bigform" enctype="multipart/form-data">
            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="nameEn_text_<%=i%>">
                                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_NAMEEN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='nameEn_div_<%=i%>'>
                                            <input type='text' class='englishOnly form-control'
                                                   name='nameEn' id='nameEn_text_<%=i%>'
                                                   placeholder="<%=isLangEnglish ? "Enter Party Name in English" : "রাজনৈতিক দলের নাম লিখুন (ইংরেজিতে)"%>"
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.nameEn + "'"):("''")%>
                                                       <%=actionName.equals("edit")? "" : "required='required'"%> tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="nameBn_text_<%=i%>">
                                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_NAMEBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='nameBn_div_<%=i%>'>
                                            <input type='text' class='noEnglish form-control'
                                                   name='nameBn' id='nameBn_text_<%=i%>'
                                                   placeholder="<%=isLangEnglish ? "Enter Party Name in Bangla" : "রাজনৈতিক দলের নাম লিখুন (বাংলায়)"%>"
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.nameBn + "'"):("''")%>
                                                       <%=actionName.equals("edit")? "" : "required='required'"%> tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="abbreviation_text_<%=i%>">
                                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_ABBREVIATION, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='abbreviation_div_<%=i%>'>
                                            <input type='text' class='form-control' name='abbreviation'
                                                   id='abbreviation_text_<%=i%>'
                                                   placeholder="<%=isLangEnglish ? "Enter Party Name in Abbreviation" : "রাজনৈতিক দলের সংক্ষিপ্তরূপ লিখুন"%>"
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.abbreviation + "'"):("''")%>
                                                       <%=actionName.equals("edit")? "" : "required='required'"%> tag='pb_html'/>
                                        </div>
                                    </div>

                                    <%--                                    Registration Number--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="registration_no">
                                            <%=isLangEnglish ? "Registration No" : "নিবন্ধন নম্বর"%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8" id='registration_no_div'>
                                            <input type="number" class="form-control" id="registration_no"
                                                   name="registration_no" min="1"
                                                   placeholder="<%=isLangEnglish ? "Enter registration number" : "নিবন্ধন নম্বর দিন"%>"
                                                   value="<%=actionName.equalsIgnoreCase("edit") ? political_partyDTO.registrationNo : ""%>"
                                                   required>
                                        </div>
                                    </div>

                                    <%--                                    Registration Date--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Registration Date" : "নিবন্ধনের তারিখ"%>
                                        </label>
                                        <div class="col-md-8" id='registrationDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="registration-date-js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden' class='form-control'
                                               id='registration-date' name='registration_date'
                                               value=''
                                               tag='pb_html'/>
                                    </div>

                                    <%--                                    Party Chief--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Party Leader" : "পার্টি লিডার"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='party_chair_name'
                                                    id='party_chair_name' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("political_party_chair", Language, political_partyDTO.partyChairNameCat)%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="presidentName_text_<%=i%>">
                                            <%=isLangEnglish ? "Party Leader Name" : "পার্টি লিডারের নাম"%>
                                        </label>
                                        <div class="col-md-8" id='presidentName_div_<%=i%>'>
                                            <input type='text' class='form-control' name='presidentName'
                                                   id='presidentName_text_<%=i%>'
                                                   placeholder="<%=isLangEnglish ? "Enter Party Leader Name" : "পার্টি লিডারের নাম লিখুন"%>"
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.presidentName + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Profile Picture of Leader" : "লিডারের মুখচ্ছবি"%>
                                        </label>
                                        <div class="col-md-8" id='president_profile_picture_div_<%=i%>'>
                                            <%
                                                if (political_partyDTO.presidentProfilePicture != null) {
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(political_partyDTO.presidentProfilePicture);
                                                    value = political_partyDTO.presidentProfilePicture + "";
                                            %>
                                            <img src='data:image/jpg;base64,<%=new String(encodeBase64)%>'
                                                 style='width:100px'>
                                            <a href='Political_partyServlet?actionType=downloadBlob&name=president_profile_picture&id=<%=political_partyDTO.iD%>'
                                               download='<%=value%>'>Download</a>
                                            <%
                                                }
                                            %>
                                            <input type='file' class='form-control'
                                                   name='president_profile_picture'
                                                   id='president_profile_picture_blob_jpg_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.presidentProfilePicture + "'"):("'" + "null" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <%--                                    Party Second in Command--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Second in Command" : "সেকেন্ড-ইন-কমান্ড"%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='second_in_command_name'
                                                    id='second_in_command_name' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("political_party_secretary", Language, political_partyDTO.secondInCommandNameCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="gsName_text_<%=i%>">
                                            <%=isLangEnglish ? "Second in Command Name" : "সেকেন্ড-ইন-কমান্ডের নাম"%>
                                        </label>
                                        <div class="col-md-8" id='gsName_div_<%=i%>'>
                                            <input type='text' class='form-control' name='gsName'
                                                   id='gsName_text_<%=i%>'
                                                   placeholder="<%=isLangEnglish ? "Enter Second in Command Name" : "সহ-দলপতির নাম লিখুন"%>"
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.gsName + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Profile Picture of Second In Command" : "সেকেন্ড-ইন-কমান্ডের মুখচ্ছবি"%>
                                        </label>
                                        <div class="col-md-8" id='gs_profile_picture_div_<%=i%>'>
                                            <%
                                                if (political_partyDTO.gsProfilePicture != null) {
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(political_partyDTO.gsProfilePicture);
                                                    value = political_partyDTO.gsProfilePicture + "";
                                            %>
                                            <img src='data:image/jpg;base64,<%=new String(encodeBase64)%>'
                                                 style='width:100px'>
                                            <a href='Political_partyServlet?actionType=downloadBlob&name=gs_profile_picture&id=<%=political_partyDTO.iD%>'
                                               download='<%=value%>'>Download</a>
                                            <%
                                                }
                                            %>
                                            <input type='file' class='form-control'
                                                   name='gs_profile_picture'
                                                   id='gs_profile_picture_blob_jpg_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.gsProfilePicture + "'"):("'" + "null" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_FOUNDATIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8" id='foundationDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="foundation-date-js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden' class='form-control'
                                               id='foundation-date' name='foundationDate'
                                               value=''
                                               tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Official Address" : "কেন্দ্রীয় কার্যালয়ের ঠিকানা"%>
                                        </label>
                                        <div class="col-md-8" id='officeAddress_div_<%=i%>'>
                                            <jsp:include page="/geolocation/geoLocation.jsp">
                                                <jsp:param name="GEOLOCATION_ID"
                                                           value="officeAddress_js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type="hidden" id="officeAddress" name="officeAddress">
                                        </div>
                                    </div>

                                    <%--                                    Symbol Name--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="symbol_name_eng">
                                            <%=isLangEnglish ? "Symbol Name(English)" : "প্রতীকের নাম(ইংরেজীতে)"%>
                                        </label>
                                        <div class="col-md-8" id='symbol_name_eng_div'>
                                            <input type='text' class='form-control' name='symbol_name_eng'
                                                   id='symbol_name_eng'
                                                   placeholder="<%=isLangEnglish ? "Enter Symbol Name in English" : "ইংরেজীতে প্রতিকের নাম লিখুন"%>"
                                                   value='<%=actionName.equals("edit") ? political_partyDTO.symbolNameEng : ""%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="symbol_name_bng">
                                            <%=isLangEnglish ? "Symbol Name(Bangla)" : "প্রতীকের নাম(বাংলায়)"%>
                                        </label>
                                        <div class="col-md-8" id='symbol_name_bng_div'>
                                            <input type='text' class='form-control' name='symbol_name_bng'
                                                   id='symbol_name_bng'
                                                   placeholder="<%=isLangEnglish ? "Enter Symbol Name in Bangla" : "বাংলায় প্রতীকের নাম লিখুন"%>"
                                                   value='<%=actionName.equals("edit") ? political_partyDTO.symbolNameBng : ""%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Symbol" : "প্রতীক"%>
                                        </label>
                                        <div class="col-md-8" id='electoralSymbolBlob_div_<%=i%>'>
                                            <%
                                                if (political_partyDTO.electoralSymbolBlob != null) {
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(political_partyDTO.electoralSymbolBlob);
                                                    value = political_partyDTO.electoralSymbolBlob + "";
                                            %>
                                            <img src='data:image/jpg;base64,<%=new String(encodeBase64)%>'
                                                 style='width:100px'>
                                            <a href='Political_partyServlet?actionType=downloadBlob&name=electoralSymbolBlob&id=<%=political_partyDTO.iD%>'
                                               download='<%=value%>'>Download</a>
                                            <%
                                                }
                                            %>
                                            <input type='file' class='form-control'
                                                   name='electoralSymbolBlob'
                                                   id='electoralSymbolBlob_blob_jpg_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.electoralSymbolBlob + "'"):("'" + "null" + "'")%>   tag='pb_html'/>


                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Party Flag" : "দলীয় পতাকা"%>
                                        </label>
                                        <div class="col-md-8" id='partyFlagBlob_div_<%=i%>'>
                                            <%
                                                if (political_partyDTO.partyFlagBlob != null) {
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(political_partyDTO.partyFlagBlob);
                                                    value = political_partyDTO.partyFlagBlob + "";
                                            %>
                                            <img src='data:image/jpg;base64,<%=new String(encodeBase64)%>'
                                                 style='width:100px'>
                                            <a href='Political_partyServlet?actionType=downloadBlob&name=partyFlagBlob&id=<%=political_partyDTO.iD%>'
                                               download='<%=value%>'>Download</a>
                                            <%
                                                }
                                            %>
                                            <input type='file' class='form-control' name='partyFlagBlob'
                                                   id='partyFlagBlob_blob_jpg_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + political_partyDTO.partyFlagBlob + "'"):("'" + "null" + "'")%>   tag='pb_html'/>


                                        </div>
                                    </div>


                                    <%--                                    Phone, mobile, email and website name--%>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="phone_number_<%=i%>">
                                            <%=isLangEnglish ? "Phone" : "ফোন"%>
                                        </label>
                                        <div class="col-md-8" id='phone_number_div_<%=i%>'>
                                            <input type='url' class='form-control' name='phone_number'
                                                   id='phone_number_<%=i%>'
                                                   placeholder="<%=isLangEnglish ? "Enter phone number" : "ফোন নাম্বার দিন"%>"
                                                   value='<%=political_partyDTO.phoneNumber == null ? "" : political_partyDTO.phoneNumber%>'
                                                   tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=isLangEnglish ? "Mobile" : "মোবাইল"%>
                                        </label>
                                        <div class="col-md-8" id='mobile_number_div_<%=i%>'>
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">+88</div>
                                                </div>
                                                <input type='text' class='digitOnly form-control rounded'
                                                       name='mobile_number' maxlength="11"
                                                       id='mobile_number_text_<%=i%>' required
                                                       pattern="^(01[3-9]{1}[0-9]{8})"
                                                       value='<%=political_partyDTO.mobileNumber ==null || political_partyDTO.mobileNumber.length() == 0?"":
                                                                   (political_partyDTO.mobileNumber.startsWith("88") ? political_partyDTO.mobileNumber.substring(2) : political_partyDTO.mobileNumber)%>'
                                                       placeholder='<%=Language.equalsIgnoreCase("English") ? "Example: 01791XXXXXX" : "উদাহরণঃ 01791XXXXXX"%>'
                                                       title='<%=isLangEnglish?"Mobile number must start with 01, then contain 9 digits"
                                                               :"মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="fax">
                                            <%=isLangEnglish ? "Fax Number" : "ফ্যাক্স"%>
                                        </label>
                                        <div class="col-md-8" id='fax_div'>
                                            <input type="text" class='form-control' name='fax'
                                                   id='fax'
                                                   placeholder="<%=isLangEnglish ? "Enter fax number" : "ফ্যাক্স নাম্বার দিন"%>"
                                                   value='<%=political_partyDTO.faxNumber == null ? "" : political_partyDTO.faxNumber%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="website_url_<%=i%>">
                                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_WEBSITE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8" id='website_div_<%=i%>'>
                                            <input type='url' class='form-control' name='website'
                                                   id='website_url_<%=i%>'
                                                   placeholder="<%=isLangEnglish ? "Enter website address" : "পার্টির ওয়েব এড্রেস দিন"%>"
                                                   value='<%=political_partyDTO.website == null ? "" : political_partyDTO.website%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_EMAIL, loginDTO)%>
                                        </label>
                                        <div class="col-md-8" id='email_div_<%=i%>'>
                                            <input type='email' class='form-control' name='email'
                                                   id='email_text_<%=i%>'
                                                   placeholder="<%=isLangEnglish ? "Enter email address" : "পার্টির ইমেইল এড্রেস দিন"%>"
                                                   value='<%=political_partyDTO.email == null ? "" : political_partyDTO.email%>'
                                                    <%if (!actionName.equals("edit")) {%>
                                                   pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                                   title="email must be a of valid email address format"
                                                    <%}%>
                                                   tag='pb_html'/>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8" id='filesDropzone_div_<%=i%>'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(political_partyDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                             style='width:100px'/>
                                                        <%
                                                            }
                                                        %>
                                                        <a href='Political_partyServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                           download><%=filesDTO.fileTitle%>
                                                        </a>
                                                        <a class='btn btn-danger'
                                                           onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                            <div class="dropzone"
                                                 action="Political_partyServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?political_partyDTO.filesDropzone:ColumnID%>">
                                                <input type='file' style="display:none"
                                                       name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                                   tag='pb_html'/>
                                            <input type='hidden' name='filesDropzone'
                                                   id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=actionName.equals("edit")?political_partyDTO.filesDropzone:ColumnID%>'/>

                                        </div>
                                    </div>
                                    <!-- Hidden Fields -->
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=political_partyDTO.iD%>'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_POLITICAL_PARTY_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button" onclick="submitForm()">
                            <%=LM.getText(LC.POLITICAL_PARTY_ADD_POLITICAL_PARTY_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(() => {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        select2SingleSelector('#second_in_command_name', '<%=Language%>');
        select2SingleSelector('#party_chair_name', '<%=Language%>');
    });

    function isFromValid() {
        $('#foundation-date').val(getDateStringById('foundation-date-js'));
        $('#registration-date').val(getDateStringById('registration-date-js'));
        $('#officeAddress').val(getGeoLocation('officeAddress_js'));
        const validations = [
            dateValidator('foundation-date-js', true),
            dateValidator('registration-date-js', true),
            geoLocationValidator('officeAddress_js')
        ];
        return validations.every(cond => cond === true);
    }

    $(function () {
        console.log('init(row);');
        dateTimeInit("<%=Language%>");
        basicInit();

        <%if(actionName.equals("edit")) {
            String fndDateStr= political_partyDTO.foundationDate > SessionConstants.MIN_DATE ? dateFormat.format(new Date(political_partyDTO.foundationDate)) : "";
            String registrationDateStr= political_partyDTO.registrationDate > SessionConstants.MIN_DATE ? dateFormat.format(new Date(political_partyDTO.registrationDate)) : "";
        %>
        setGeoLocation('<%=political_partyDTO.officeAddress%>', 'officeAddress_js');
        setDateByStringAndId('foundation-date-js', '<%=fndDateStr%>');
        setDateByStringAndId('registration-date-js', '<%=registrationDateStr%>');
        <%} else {
            String todayStr = dateFormat.format(new Date());
        %>
        setDateByStringAndId('foundation-date-js', '<%=todayStr%>');
        setDateByStringAndId('registration-date-js', '<%=todayStr%>');
        <%
        }
        %>
    });

    const form = $('#bigform');

    function submitForm() {
        buttonStateChange(true);
        if (isFromValid()) {
            $.ajax({
                type: "POST",
                url: "Political_partyServlet?actionType=ajax_<%=actionName%>",
                cache: false,
                data: new FormData(form[0]),
                processData: false,
                contentType: false,
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>