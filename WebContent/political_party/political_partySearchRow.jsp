<td><%= political_partyDTO.nameEn%>
</td>

<td><%= political_partyDTO.nameBn%>
</td>

<td><%= political_partyDTO.abbreviation%>
</td>

<td><%= political_partyDTO.presidentName%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Political_partyServlet?actionType=view&ID=<%=political_partyDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Political_partyServlet?actionType=getEditPage&ID=<%=political_partyDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=political_partyDTO.iD%>'/></span>
    </div>
</td>