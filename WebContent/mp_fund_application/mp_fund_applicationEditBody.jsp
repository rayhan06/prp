<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="mp_fund_application.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="mp_fund_amount.Mp_fund_amountServlet" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="election_details.Election_detailsDTO" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="budget.BudgetInfo" %>

<%@include file="../pb/addInitializer2.jsp" %>

<%
    String formTitle = LM.getText(LC.MP_FUND_APPLICATION_ADD_MP_FUND_APPLICATION_ADD_FORMNAME, loginDTO);
    String servletName = "Mp_fund_applicationServlet";
    String context = request.getContextPath() + "/";
    Election_detailsDTO electionDetailsDTO = Election_detailsRepository.getInstance().getRunningElectionDetailsDTO();
    int currentParliamentNumber = electionDetailsDTO.parliamentNumber;
    int currentEconomicStartYear = BudgetUtils.getEconomicYearBeginYear(System.currentTimeMillis());

    // always add expecting
    actionName = "add";
%>

<style>
    .template-row {
        display: none;
    }

    input[readonly],
    textarea[readonly] {
        background-color: rgba(231, 231, 231, .5) !important;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="fund-application-form" enctype="multipart/form-data">

            <input type="hidden" name="mpFundRecipients" id="mpFundRecipients-input">

            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11 m-auto">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=Budget_mappingRepository.getInstance().getOperationTextWithoutCode(
                                                        Language, Mp_fund_applicationServlet.MP_BUDGET_OPERATION_ID
                                                )%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="electionDetailsId">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ELECTIONDETAILSID, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' readonly id='electionDetailsId'
                                                       value='<%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(currentParliamentNumber))%>'>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="economicYearStart">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ECONOMICYEARSTART, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' readonly id='economicYearStart'
                                                       value='<%=StringUtils.convertBanglaIfLanguageIsBangla(Language, BudgetInfo.getEconomicYear(currentEconomicStartYear))%>'>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="electionConstituencyId">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ELECTIONCONSTITUENCYID, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select style='width: 100%' class='form-control'
                                                        name='electionConstituencyId' id='electionConstituencyId'
                                                        onchange="constituencyChanged(this);">
                                                    <%=Election_constituencyRepository.getInstance().buildOptions(Language, 0L, electionDetailsDTO.iD)%>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 row mx-0" >
                                            <label class="col-md-4 col-form-label text-md-right" for="totalAmount">
                                                <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_TOTALAMOUNT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' readonly id='totalAmount'>
                                            </div>
                                            <label class="col-md-4 col-form-label text-md-right mt-2" for="amountLeft">
                                                <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_AMOUNTLEFT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8 mt-2">
                                                <input type='text' class='form-control' readonly id='amountLeft'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form id="recipient-info-form">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11 m-auto">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_MPFUNDAMOUNTID, loginDTO)%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="name">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='name' id='name'/>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="upazilaName">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_UPAZILA_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='upazilaName'
                                                       id='upazilaName'/>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="fatherOrHusbandName">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_FATHER_OR_HUSBAND_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='fatherOrHusbandName'
                                                       id='fatherOrHusbandName'/>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="wardNumber">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_WARD_NUMBER, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='wardNumber' id='wardNumber'/>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="allocatedAmount">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ALLOCATED_AMOUNT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='allocatedAmount'
                                                       data-only-integer="true"
                                                       id='allocatedAmount'/>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="unionName">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_UNION_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='unionName'
                                                       id='unionName'/>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 row mx-0">
                                            <label class="col-md-4 col-form-label text-md-right" for="comment">
                                                <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_COMMENT, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='comment' id='comment' maxlength="1024"/>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row mt-3">
                    <div class="col-12 text-right">
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="submit">
                            <%=getDataByLanguage(Language, "প্রাপ্য যুক্ত করুন", "Add Recipient")%>
                        </button>
                    </div>
                </div>
            </div>
        </form>

        <div class="kt-portlet__body table-responsive">
            <div class="text-center text-danger" id="add-at-least-one-error" style="display: none;">
                <%=UtilCharacter.getDataByLanguage(Language, "অন্তত একজনকে বরাদ্দ তালিকায় যুক্ত করুন।", "Add at least one in allocations list.")%>
            </div>

            <div class="table-responsive">
                <table id="recipient-info-table" class="table table-striped table-bordered text-nowrap">
                    <thead>
                    <tr>
                        <th>
                            <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_NAME, loginDTO)%>
                        </th>
                        <th>
                            <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_FATHER_OR_HUSBAND_NAME, loginDTO)%>
                        </th>
                        <th>
                            <%=getDataByLanguage(Language, "ঠিকানা", "Address")%>
                        </th>
                        <th>
                            <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ALLOCATED_AMOUNT, loginDTO)%>
                        </th>
                        <th>
                            <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_COMMENT, loginDTO)%>
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="main-tbody">
                    </tbody>

                    <%--don't put these tr inside tbody--%>
                    <tr class="template-row" data-insert-index="-1">
                        <td class="row-data-name"></td>
                        <td class="row-data-fatherOrHusbandName"></td>
                        <td class="row-data-address"></td>
                        <td class="row-data-allocatedAmount"></td>
                        <td class="row-data-comment"></td>
                        <td class="row-data-remove-btn">
                            <button class='btn btn-sm cancel-btn text-white shadow pl-4'
                                    style="padding-right: 14px;" type="button"
                                    onclick="removeRow(this);">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="text-center text-danger" id="no-fund-left-error" style="display: none;">
                <%=UtilCharacter.getDataByLanguage(Language, "এই আবেদনের মোট বরাদ্দ দেবার মত ফান্ড নেই!", "Not enough fund to allocate for this bill!")%>
            </div>

            <div class="row mt-4">
                <div class="col-12">
                    <div class="form-actions text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_MP_FUND_APPLICATION_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                onclick="submitForm();">
                            <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_MP_FUND_APPLICATION_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const fundApplicationForm = $('#fund-application-form');
    const tableId = 'recipient-info-table';
    const amountLeftInput = document.getElementById('amountLeft');
    const totalAmountInput = document.getElementById('totalAmount');
    const mpFundRecipientsInput = document.getElementById('mpFundRecipients-input');
    const tbody = document.querySelector('#recipient-info-table tbody.main-tbody');
    const templateRow = document.querySelector('#recipient-info-table tr.template-row');
    const addAtLeastOneError = $('#add-at-least-one-error');
    const noFundLeftError = $('#no-fund-left-error');
    const totalColumnIndex = 3;

    let electionConstituencyDistrict = 'none';
    let amountLeft = 0;
    let insertIndex = 0;
    let mpFundRecipientMap = new Map();

    function clearData() {
        amountLeftInput.value = '';
        electionConstituencyDistrict = 'none';
        amountLeft = 0;
        insertIndex = 0;
        mpFundRecipientMap = new Map();
        tbody.innerHTML = '';
        addAtLeastOneError.hide();
        noFundLeftError.hide();
    }

    async function constituencyChanged(selectElem) {
        const electionConstituencyId = selectElem.value;
        clearData();
        if(electionConstituencyId === '') return;

        const url = 'Mp_fund_applicationServlet?actionType=ajax_getFundAmountData'
            + '&economicStartYear=' + '<%=currentEconomicStartYear%>'
            + '&electionDetailsId=' + '<%=electionDetailsDTO.iD%>'
            + '&electionConstituencyId=' + electionConstituencyId;
        const response = await fetch(url);
        try {
            const json = await response.json();
            electionConstituencyDistrict = json.electionConstituencyDistrict;
            amountLeft = Number(json.amountLeft);
            amountLeftInput.value = json.amountLeftFormatted + "/-";
            totalAmountInput.value = json.totalAmountFormatted + "/-";
        } catch (error) {
            console.log(error);
            electionConstituencyDistrict = '';
            amountLeft = 0;
            amountLeftInput.value = convertToBanglaNumIfBangla(amountLeft, '<%=Language%>');
        }
    }

    function calculateAllRowTotal(tableId) {
        const colIndicesToSum = [totalColumnIndex];
        const totalTitleColSpan = 3;
        const totalTitle = '<%=getDataByLanguage(Language, "সর্বমোট", "Total")%>';
        setupTotalRow(tableId, totalTitle, totalTitleColSpan, colIndicesToSum, 0, '<%=Language.toLowerCase()%>');
    }

    function removeRow(buttonElem) {
        const thisRow = buttonElem.closest('tr');
        const insertIndex = Number(thisRow.dataset.insertIndex);
        mpFundRecipientMap.delete(insertIndex);
        console.log(mpFundRecipientMap);
        thisRow.remove();
        if(mpFundRecipientMap.size > 0) {
            calculateAllRowTotal(tableId);
        } else {
            clearFooter(tableId);
        }
        fundAmountValidation();
    }

    function getAddress(addressObj) {
        return '<%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_WARD_NUMBER, loginDTO)%>-'
            + addressObj.wardNumber + ', '
            + addressObj.unionName + ', '
            + addressObj.upazilaName + ', '
            + addressObj.districtName;
    }

    function setRowData(tableRow, jsonData) {
        const rowDataPrefix = 'row-data-';
        for (const key in jsonData) {
            const td = tableRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = jsonData[key];
        }
    }

    function showDataInTable(tableBody, templateRow, jsonData, insertIndex) {
        const tableRow = templateRow.cloneNode(true);
        tableRow.classList.remove('template-row');
        tableRow.dataset.insertIndex = insertIndex;
        setRowData(tableRow, jsonData);
        tableBody.append(tableRow);
    }

    const recipientInfoForm = document.getElementById('recipient-info-form');
    recipientInfoForm.addEventListener('submit', (event) => {
        event.preventDefault();

        let formsValid = fundApplicationForm.valid() && $(recipientInfoForm).valid();
        if(!formsValid) return;

        const formElement = event.target;
        const formData = new FormData(formElement);

        const mpFundRecipientJson = Object.fromEntries(formData.entries());
        mpFundRecipientMap.set(insertIndex, mpFundRecipientJson);
        addAtLeastOneError.hide();

        mpFundRecipientJson.allocatedAmount = convertToBanglaNumIfBangla(mpFundRecipientJson.allocatedAmount, '<%=Language%>');
        mpFundRecipientJson.districtName = electionConstituencyDistrict;
        mpFundRecipientJson.address = getAddress(mpFundRecipientJson);

        showDataInTable(tbody, templateRow, mpFundRecipientJson, insertIndex);
        insertIndex++;
        calculateAllRowTotal(tableId);
        fundAmountValidation();
        setTimeout(_ => recipientInfoForm.reset(), 50);
    });

    function setButtonDisableState(value){
        $('#submit-btn').prop('disabled',value);
    }

    function fundAmountValidation() {
        if(hasEnoughFund())
            noFundLeftError.hide();
        else
            noFundLeftError.show();
    }

    function hasEnoughFund() {
        let applicationTotalAmount;
        try {
            applicationTotalAmount = getTotal(tableId, totalColumnIndex);
        } catch (e) {
            applicationTotalAmount = 0;
        }
        return applicationTotalAmount <= amountLeft;
    }

    function submitForm() {
        let allValid = true;
        if(!fundApplicationForm.valid()) allValid = false;

        if(mpFundRecipientMap.size === 0) {
            addAtLeastOneError.show();
            allValid = false;
        }

        if(!hasEnoughFund()) {
            noFundLeftError.show();
            allValid = false;
        }

        if(!allValid) return;

        mpFundRecipientsInput.value = JSON.stringify(Array.from(mpFundRecipientMap.values()));
        setButtonDisableState(true);
        $.ajax({
            type : "POST",
            url : "Mp_fund_applicationServlet?actionType=ajax_<%=actionName%>",
            data : fundApplicationForm.serialize(),
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    setButtonDisableState(false);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                setButtonDisableState(false);
            }
        });
    }

    function typeOnlyInteger(e) {
        return true === inputValidationForIntValue(e, $(this));
    }

    function init() {
        select2SingleSelector('#electionConstituencyId', '<%=Language%>');
        $('body').delegate('[data-only-integer="true"]', 'keydown', typeOnlyInteger);

        $.validator.addMethod('textValidator', function (value, element) {
            return value.trim() !== '';
        });

        $.validator.addMethod('banglaOrEnglishNumber', function (value, element) {
            return /^[0-9০-৯]+$/.test(value.trim());
        });

        $(recipientInfoForm).validate({
            rules: {
                name: {
                    required: true,
                    textValidator : true
                },
                fatherOrHusbandName: {
                    required: true,
                    textValidator : true
                },
                upazilaName: {
                    required: true,
                    textValidator : true
                },
                unionName: {
                    required: true,
                    textValidator : true
                },
                wardNumber: {
                    required: true,
                    banglaOrEnglishNumber: true
                },
                allocatedAmount: {
                    required: true,
                    min: 1
                }
            },
            messages: {
                name:'<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                fatherOrHusbandName: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                upazilaName: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                unionName: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                wardNumber: '<%=getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>',
                allocatedAmount: '<%=getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>'
            }
        });

        fundApplicationForm.validate({
            rules: {
                electionConstituencyId: "required"
            },
            messages: {
                electionConstituencyId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>'
            }
        });
    }

    $(document).ready(function () {
        init();
    });
</script>