<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="mp_fund_application.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget.BudgetInfo" %>
<%@ page import="util.StringUtils" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>

<%
    String navigator2 = "navMP_FUND_APPLICATION";
    String servletName = "Mp_fund_applicationServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
				<%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ECONOMICYEARSTART, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ELECTIONDETAILSID, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ELECTIONCONSTITUENCYID, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.MP_FUND_APPLICATION_ADD_MPNAMEEN, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.MP_FUND_APPLICATION_ADD_APPLICATIONTOTALAMOUNT, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
			List<Mp_fund_applicationDTO> data = (List<Mp_fund_applicationDTO>) rn2.list;
            try {
                if (data != null) {
                    for (Mp_fund_applicationDTO mp_fund_applicationDTO : data) {
        %>
        <tr>
            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                		Language,
						BudgetInfo.getEconomicYear(mp_fund_applicationDTO.economicYearStart)
				)%>
            </td>

            <td>
                <%=Election_detailsRepository.getInstance().getText(
						mp_fund_applicationDTO.electionDetailsId,
						Language
				)%>
            </td>

            <td>
                <%=Election_constituencyRepository.getInstance().getConstituencyText(
						mp_fund_applicationDTO.electionConstituencyId,
						Language
				)%>
            </td>

            <td>
                <%=UtilCharacter.getDataByLanguage(Language, mp_fund_applicationDTO.mpNameBn, mp_fund_applicationDTO.mpNameEn)%>
            </td>

            <td>
                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(mp_fund_applicationDTO.applicationTotalAmount, Language))%>/-
            </td>

			<td>
				<button type="button"
						class="btn-sm border-0 shadow bg-light btn-border-radius"
						style="color: #ff6b6b;"
						onclick="location.href='<%=servletName%>?actionType=view&ID=<%=mp_fund_applicationDTO.iD%>'">
					<i class="fa fa-eye"></i>
				</button>
			</td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>