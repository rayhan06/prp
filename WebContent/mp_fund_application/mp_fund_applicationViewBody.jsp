<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="mp_fund_application.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="mp_fund_recipient.Mp_fund_recipientDTO" %>
<%@ page import="mp_fund_recipient.Mp_fund_recipientDAO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.StringUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>

<%
    String servletName = "Mp_fund_applicationServlet";
    Mp_fund_applicationDTO mp_fund_applicationDTO = (Mp_fund_applicationDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    List<Mp_fund_recipientDTO> fundRecipientDTOs = Mp_fund_recipientDAO.getInstance().getDTOsByApplicationId(
            mp_fund_applicationDTO.iD
    );
    String applicationTotalAmountBn = StringUtils.convertToBanNumber(String.valueOf(mp_fund_applicationDTO.applicationTotalAmount));
    String context = request.getContextPath() + "/";

    String pdfFileName = "MP Fund Allocations - "
            + Election_constituencyRepository.getInstance()
                                             .getConstituencyText(mp_fund_applicationDTO.electionConstituencyId, "English")
            + " - " + mp_fund_applicationDTO.iD;

    int rowsPerPage = 12, rowsPerPageFirstPage = 10;
    int totalRows = fundRecipientDTOs.size();
    boolean isSinglePage = totalRows <= rowsPerPageFirstPage;
%>

<%@include file="../pb/viewInitializer.jsp" %>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: 2in .5in .4in .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page {
        width: 210mm;
        padding: .4in;
        background: white;
        margin-bottom: 10px;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }
</style>

<div class="kt-content p-0" id="kt_content">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">
                        ঐচ্ছিক তহবিলের বরাদ্দ
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body" id="bill-div">

                <div class="ml-auto m-5">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>')">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <section class="page shadow" data-size="A4">
                            <div>
                                ডিওএমপি: বাজাস-১৫, নীল-০৪, ২০২০: ৩৮৯
                            </div>

                            <div class="mt-3">
                                তারিখঃ <%=StringUtils.getFormattedDate("Bangla", System.currentTimeMillis())%>
                            </div>

                            <div class="mt-3" style="font-weight: bold;">
                                বিষয়ঃ&nbsp;
                                <%=StringUtils.convertToBanNumber(BudgetUtils.getRunningEconomicYear())%>&nbsp;
                                অর্থ বছরের ঐচ্ছিক তহবিলের বরাদ্দ প্রসঙ্গে
                            </div>

                            <div class="mt-3">
                                উপর্যুক্ত বিষয়ের প্রেক্ষিতে আপনাকে জানানো যাচ্ছে যে, আমার
                                অনুকূলে <%=StringUtils.convertToBanNumber(BudgetUtils.getRunningEconomicYear())%>&nbsp;
                                অর্থ বছরের বরাদ্দকৃত ঐচ্ছিক
                                তহবিলের <%=BangladeshiNumberFormatter.getFormattedNumber(applicationTotalAmountBn)%>/-&nbsp;
                                (<%=BangladeshiNumberInWord.convertToWord(applicationTotalAmountBn)%>)&nbsp;
                                টাকা নিম্ন লিখিত তালিকে অনুযায়ী প্রদান করার জন্য অনুরোধ করা হল।
                            </div>

                            <table class="table-bordered mt-3 w-100">
                                <thead>
                                <tr>
                                    <th>
                                        ক্রমিক নং
                                    </th>
                                    <th>
                                        নাম, পিতা/স্বামীর নাম, ঠিকানা
                                    </th>
                                    <th>
                                        <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_ALLOCATED_AMOUNT, "Bangla")%>
                                    </th>
                                    <th>
                                        <%=LM.getText(LC.MP_FUND_APPLICATION_ADD_RECIPIENT_COMMENT, "Bangla")%>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                <%
                                    for (int index = 0; index < Math.min(rowsPerPageFirstPage, totalRows); index++) {
                                        Mp_fund_recipientDTO fund_recipientDTO = fundRecipientDTOs.get(index);
                                %>
                                <tr>
                                    <td class="row-data-serial-no">
                                        <%=StringUtils.convertToBanNumber(String.valueOf(index + 1))%>
                                    </td>

                                    <td class="row-data-name">
                                        <%=fund_recipientDTO.name%><br>
                                        পিতা/স্বামীঃ <%=fund_recipientDTO.fatherOrHusbandName%><br>
                                        <%=fund_recipientDTO.getAddress(
                                                mp_fund_applicationDTO.electionConstituencyId,
                                                "bangla"
                                        )%>
                                    </td>
                                    <td class="row-data-allocatedAmount">
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                StringUtils.convertToBanNumber(
                                                        String.valueOf(fund_recipientDTO.allocatedAmount)
                                                )
                                        )%>/-
                                    </td>
                                    <td class="row-data-comment">
                                        <%=fund_recipientDTO.comment%>
                                    </td>
                                </tr>
                                <%}%>
                                <%if (isSinglePage) {%>
                                <tr>
                                    <td colspan="2" style="font-weight: bold;">
                                        মোট =
                                    </td>
                                    <td>
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                StringUtils.convertToBanNumber(
                                                        String.valueOf(mp_fund_applicationDTO.applicationTotalAmount)
                                                )
                                        )%>/-
                                    </td>
                                    <td></td>
                                </tr>
                                <%}%>
                                </tbody>
                            </table>

                            <%--MP Signature--%>
                            <%if (isSinglePage) {%>
                            <div class="row text-center mt-4">
                                <div class="offset-8 col-4">
                                    ধন্যবাদান্তে
                                    <div class="mt-5">
                                        <strong>(<%=mp_fund_applicationDTO.mpNameBn%>)</strong><br>
                                        সংসদ সদস্য<br>
                                        <%=Election_constituencyRepository.getInstance().getConstituencyText(mp_fund_applicationDTO.electionConstituencyId, "Bangla")%>
                                    </div>
                                </div>
                            </div>
                            <%}%>
                        </section>

                        <%
                            int currentIndex = rowsPerPageFirstPage;
                            while (currentIndex < totalRows) {
                                boolean isLastPage = (currentIndex + rowsPerPage) >= totalRows;
                                int rowsInThisPage = 0;
                        %>
                        <section class="page shadow" data-size="A4">
                            <table class="table-bordered mt-3 w-100">
                                <tbody>
                                <%
                                    while (currentIndex < totalRows && rowsInThisPage < rowsPerPage) {
                                        Mp_fund_recipientDTO fund_recipientDTO = fundRecipientDTOs.get(currentIndex);
                                %>
                                <tr>
                                    <td class="row-data-serial-no">
                                        <%=StringUtils.convertToBanNumber(String.valueOf(currentIndex + 1))%>
                                    </td>

                                    <td class="row-data-name">
                                        <%=fund_recipientDTO.name%><br>
                                        পিতা/স্বামীঃ <%=fund_recipientDTO.fatherOrHusbandName%><br>
                                        <%=fund_recipientDTO.getAddress(
                                                mp_fund_applicationDTO.electionConstituencyId,
                                                "bangla"
                                        )%>
                                    </td>
                                    <td class="row-data-allocatedAmount">
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                StringUtils.convertToBanNumber(
                                                        String.valueOf(fund_recipientDTO.allocatedAmount)
                                                )
                                        )%>/-
                                    </td>
                                    <td class="row-data-comment">
                                        <%=fund_recipientDTO.comment%>
                                    </td>
                                </tr>
                                <%
                                        currentIndex++;
                                        rowsInThisPage++;
                                    }
                                %>

                                <%if (isLastPage) {%>
                                <tr>
                                    <td colspan="2" style="font-weight: bold;">
                                        মোট =
                                    </td>
                                    <td>
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                StringUtils.convertToBanNumber(
                                                        String.valueOf(mp_fund_applicationDTO.applicationTotalAmount)
                                                )
                                        )%>/-
                                    </td>
                                    <td></td>
                                </tr>
                                <%}%>
                                </tbody>
                            </table>

                            <%--MP Signature--%>
                            <%if (isLastPage) {%>
                            <div class="row text-center mt-4">
                                <div class="offset-8 col-4">
                                    ধন্যবাদান্তে
                                    <div class="mt-5">
                                        <strong>(<%=mp_fund_applicationDTO.mpNameBn%>)</strong><br>
                                        সংসদ সদস্য<br>
                                        <%=Election_constituencyRepository.getInstance().getConstituencyText(mp_fund_applicationDTO.electionConstituencyId, "Bangla")%>
                                    </div>
                                </div>
                            </div>
                            <%}%>
                        </section>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
</div>

<script>
    function downloadTemplateAsPdf(divId, fileName) {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4'}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>