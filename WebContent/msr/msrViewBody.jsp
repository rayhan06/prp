<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="appointment.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>



<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="family.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="doctor_time_slot.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    AppointmentDAO appointmentDAO = new AppointmentDAO("appointment");

    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    String context = request.getContextPath() + "/";

%>

<div class="ml-auto mr-5 mt-4">
    <button type="button" class="btn" id='printer1'
            onclick="downloadPdf('Report.pdf','modalbody')">
        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='printer2'
            onclick="printDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
   
</div>
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
		<div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row">
                                        <div class="col-lg-12">
											
										        <div class="form-group row">
										            <label class="col-md-3 col-form-label text-md-right">
										                <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.HM_DATE, loginDTO)%>
										            </label>
										            <div class="col-md-9">
										                <jsp:include page="/date/date.jsp">
										                    <jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
										                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
										                </jsp:include>
										                <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
										                       data-label="Document Date" id='startDate' name='startDate' value=""
										                       tag='pb_html'
										                />
										            </div>
										        </div>
										    
										    
										        <div class="form-group row">
										            <label class="col-md-3 col-form-label text-md-right">
										                <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.HM_DATE, loginDTO)%>
										            </label>
										            <div class="col-md-9">
										                <jsp:include page="/date/date.jsp">
										                    <jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
										                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
										                </jsp:include>
										                <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
										                       data-label="Document Date" id='endDate' name='endDate' value=""
										                       tag='pb_html'
										                />
										            </div>
										        </div>
										        
										        <div class="form-actions text-center">
							                        <button class="btn btn-success" id = "labSubmitButton" onClick="getReportData()"
							                        type="button" ><%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%></button>
							                    </div>
									    
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
										
    <div class="" id="modalbody">
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="row text-center">
                    
                         <table class="w-100 mt-5">
				                <tr>
				                    <td align="center">
				                        <img
				                                class="parliament-logo"
				                                width="8%"
				                                src="<%=context%>assets/images/perliament_logo_final2.png"
				                                alt=""
				                        />
				                    </td>
				                </tr>
				                <tr>
				                    <td align="center">
				                        <p class="mb-0"><%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
				                        </p>
				                    </td>
				                </tr>
				                <tr>
				                    <td align="center">
				                        <a class="website-link" href="www.parliament.gov.bd"
				                        >www.parliament.gov.bd</a
				                        >
				                    </td>
				                </tr>
				            </table>
                   
                </div>
                <div class="table-section">
                    <div class="mt-5">
                        
                        <table class="table table-bordered table-striped">
                            <thead>
                            	<tr>
                            		<th><%=LM.getText(LC.HM_SL, loginDTO)%></th>
                            		<th><%=LM.getText(LC.HM_DESCRIPTION, loginDTO)%></th>
                            		<th><%=Language.equalsIgnoreCase("english")?"Activities":"কার্যক্রমসমূহ"%></th>
                            	</tr>
                            </thead>
                            <%
                            int sl = 0;
                            %>
                            <tbody>
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Outdoor Patient Service":"বহির্বিভাগ রোগী সেবা"%></td>
                            		<td id = "outdoor"></td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Emergency":"জরুরি বিভাগ"%></td>
                            		<td>
                            			<div>
                            				<%=Language.equalsIgnoreCase("english")?"Emergency Patients":"জরুরি রোগী"%>
                            				: <span id = "emergencyPatients"></</span>
                            			</div>
                            			<div>
                            				<%=Language.equalsIgnoreCase("english")?"ECG":"ইসিজি"%>
                            				: <span id = "ecg"></</span>
                            			</div>
                            			<div>
                            				<%=Language.equalsIgnoreCase("english")?"RBS":"আরবিএস"%>
                            				: <span id = "rbs"></</span>
                            			</div>
                            		</td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Emergency Call Attend":"জরুরি কল এটেন্ড"%></td>
                            		<td id = "emergencyCalls"></td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Physiotherapy Service":"ফিজিওথেরাপি সেবা"%></td>
                            		<td id = "physiotherapies"></td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Digital XRay Service":"ডিজিটাল এক্স রে সেবা"%></td>
                            		<td id = "xrays"></td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Pathology Service":"প্যাথলজি সেবা"%></td>
                            		<td id = "pathologies"></td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Dentistry Service":"দন্ত সেবা"%></td>
                            		<td id = "dentistries"></td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Covid 19 Vaccination":"কোভিড ১৯ টিকাদান সেবা"%></td>
                            		<td id = "covids"></td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Emergency Ambulance Service":"জরুরি এম্বুলেন্স সেবা"%></td>
                            		<td id = "ambulances"></td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Pharmacy":"ফার্মেসি"%></td>
                            		<td>
                            			<div>
                            				<%=Language.equalsIgnoreCase("english")?"Medicine Delivery":"ঔষধ বিতরণ"%>
                            				: <span id = "medicines"></</span>
                            			</div>
                            			<div>
                            				<%=Language.equalsIgnoreCase("english")?"Prescriptions":"প্রেসক্রিপশন"%>
                            				: <span id = "pharmacyPrescriptions"></</span>
                            			</div>
                            			<div>
                            				<%=Language.equalsIgnoreCase("english")?"Cost":"মূল্য"%>
                            				: <span id = "drugCosts"></</span>
                            			</div>
                            		</td>
                            	</tr>
                            	
                            	<tr>
                            		<td><%=Utils.getDigits(++sl, Language)%>.</td>
                            		<td><%=Language.equalsIgnoreCase("english")?"Total Prescriptions":"মোট প্রেসক্রিপশন"%></td>
                            		<td id = "prescriptions"></td>
                            	</tr>
                            
                            <tr>
                                
                              
                            </tr>
                         
                         
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
    
    
    $(document).ready(function () {
    	//$("#calendardiv").removeAttr("style");
    	
    	setDateByTimestampAndId('startDate_js', '<%=TimeConverter.getToday()%>');
    	setDateByTimestampAndId('endDate_js', '<%=TimeConverter.getToday()%>');
    	
    	getReportData();
    });
    var add1WithEnd = false;
    function getReportData()
    {
    	console.log("calendar change called");
        var startDate = getDateTimestampById('startDate_js');
		var endDate = -1;
    	if(!add1WithEnd)
    	{
    		endDate = getDateTimestampById('endDate_js');
    	}
    	else
    	{
    		
    		endDate =  parseInt(getDateTimestampById('endDate_js')) + 86400000;
    	}
        


        console.log("startDate = " + startDate);
        console.log("endDate = " + endDate);

    	var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	var msrDTO = JSON.parse(this.responseText);
            	 for (var key in msrDTO) 
            	 {
	           	       console.log(key);
	           	       console.log(msrDTO[key]);
	           	       $("#" + key ).html(msrDTO[key]);
            	 }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        var url = "MSRServlet?actionType=getMSR&startDate=" + startDate + "&endDate=" + endDate + "&Language=<%=Language%>";
        xhttp.open("GET", url, false);
        xhttp.send();
    }
</script>

               


