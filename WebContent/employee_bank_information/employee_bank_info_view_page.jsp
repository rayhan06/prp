<%@ page import="employee_bank_information.EmployeeBankInfoModel" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="java.util.List" %>
<input type="hidden" data-ajax-marker="true">
<table class="table table-bordered table-striped" style="font-size: 14px">
    <thead>
        <tr>
            <th><b><%= LM.getText(LC.EMPLOYEE_BANK_INFORMATION_EDIT_BANKNAMETYPE, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_BANK_INFORMATION_EDIT_BRANCHNAME, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_BANK_INFORMATION_EDIT_ACCOUNTCAT, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_BANK_INFORMATION_EDIT_BANKACCOUNTNUMBER, loginDTO) %></b></th>
            <th><b></b></th>
        </tr>
    </thead>
    <tbody>
        <%
            List<EmployeeBankInfoModel> savedBankInfoModelList = (List<EmployeeBankInfoModel>) request.getAttribute("savedBankInfoModelList");
            if(savedBankInfoModelList !=null && savedBankInfoModelList.size()>0){
                int bankInfoIndex = 0;
                for(EmployeeBankInfoModel bankInfoModel: savedBankInfoModelList){
                    ++bankInfoIndex;
        %>
            <tr><%@include file="/employee_bank_information/employee_bank_info_item.jsp"%></tr>
        <%} }%>
    </tbody>
</table>


<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_bank_informationServlet?actionType=getAddPage&tab=6&tab=6&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp;<%= LM.getText(LC.HR_MANAGEMENT_OTHERS_ADD_BANK, loginDTO) %></button>
    </div>
</div>

