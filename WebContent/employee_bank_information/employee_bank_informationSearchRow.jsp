<%@page import="employee_records.Employee_recordsRepository" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="bank_name.Bank_nameRepository" %>
<%@ page import="pb.CatRepository" %>

<%
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(userDTO.employee_record_id);
    }
%>
<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_bank_informationDTO.employeeRecordsId, Language)%>
</td>


<td>
    <%=Bank_nameRepository.getInstance().getText(Language, employee_bank_informationDTO.bankNameType)%>
</td>


<td>
    <%=employee_bank_informationDTO.branchName%>
</td>


<td>
    <%=CatRepository.getInstance().getText(Language, "account", employee_bank_informationDTO.accountCat)%>
</td>


<td>
    <%=employee_bank_informationDTO.bankAccountNumber%>
</td>

<td>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_bank_informationServlet?actionType=getEditPage&ID=<%=employee_bank_informationDTO.iD%>&empId=<%=empId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_bank_informationDTO.iD%>'/></span>
    </div>
</td>