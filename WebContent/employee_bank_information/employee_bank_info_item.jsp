<td style="vertical-align: middle;"><%=isLanguageEnglish ? bankInfoModel.bankNameEng : bankInfoModel.bankNameBan%></td>
<td style="vertical-align: middle;"><%=bankInfoModel.dto.branchName%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? bankInfoModel.accountCatEng : bankInfoModel.accountCatBan%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? bankInfoModel.dto.bankAccountNumber : bankInfoModel.accountNoBan%></td>
<td style="vertical-align: middle;text-align: center">
    <form action="Employee_bank_informationServlet?isPermanentTable=true&actionType=delete&tab=6&ID=<%=bankInfoModel.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>"
          method="POST" id="tableForm_bank_info<%=bankInfoIndex%>" enctype = "multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn-success" type="button" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO) %>"
                    onclick="location.href='<%=request.getContextPath()%>/Employee_bank_informationServlet?actionType=getEditPage&tab=6&iD=<%=bankInfoModel.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'"><i class="fa fa-edit"></i></button>&nbsp
            <button class="btn-danger" type="button" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                    onclick="deleteItem('tableForm_bank_info',<%=bankInfoIndex%>)"><i class="fa fa-trash"></i></button>
        </div>
    </form>
</td>