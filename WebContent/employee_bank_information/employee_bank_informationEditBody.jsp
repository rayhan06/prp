<%@page import="login.LoginDTO" %>
<%@page import="employee_bank_information.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="bank_name.Bank_nameRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    long empId;
    if (request.getParameter("empId") == null) {
        empId = userDTO.employee_record_id;
    }else{
        empId = Long.parseLong(request.getParameter("empId"));
    }
    Employee_bank_informationDTO employee_bank_informationDTO;
    String actionName;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        employee_bank_informationDTO = (Employee_bank_informationDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        employee_bank_informationDTO = new Employee_bank_informationDTO();
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.EMPLOYEE_BANK_INFORMATION_EDIT_EMPLOYEE_BANK_INFORMATION_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_EMPLOYEE_BANK_INFORMATION_ADD_FORMNAME, loginDTO);
    }
    int i = 0;
    String url = "Employee_bank_informationServlet?actionType=" + "ajax_"+ actionName + "&isPermanentTable=true&empId=" + empId+"&iD="+employee_bank_informationDTO.iD;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" action="<%=url%>"
              id="bank-information-form-id" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label text-md-right" for="bankNameType_select_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_BANKNAMETYPE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='bankNameType_div_<%=i%>'>
                                            <select class='form-control' name='bankNameType'
                                                    id='bankNameType_select_<%=i%>' tag='pb_html'>
                                                <%=Bank_nameRepository.getInstance().buildOptions(Language, employee_bank_informationDTO.bankNameType)%>
                                            </select>

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label text-md-right" for="branchName_text_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_BRANCHNAME, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='branchName_div_<%=i%>'>
                                            <input type='text' class='form-control' name='branchName'
                                                   id='branchName_text_<%=i%>'
                                                   value='<%=employee_bank_informationDTO.branchName!=null?employee_bank_informationDTO.branchName:""%>' tag='pb_html'/>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label text-md-right" for="accountCat_category_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_ACCOUNTCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='accountCat_div_<%=i%>'>
                                            <select class='form-control' name='accountCat'
                                                    id='accountCat_category_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("account", Language, employee_bank_informationDTO.accountCat)%>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-from-label text-md-right" for="bankAccountNumber_text_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_BANKACCOUNTNUMBER, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='bankAccountNumber_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='bankAccountNumber'
                                                   id='bankAccountNumber_text_<%=i%>'
                                                   value='<%=employee_bank_informationDTO.bankAccountNumber!=null?employee_bank_informationDTO.bankAccountNumber:""%>' tag='pb_html'/>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_EDIT_EMPLOYEE_BANK_INFORMATION_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit"
                                onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_EDIT_EMPLOYEE_BANK_INFORMATION_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';
    const bankForm = $('#bank-information-form-id');

    $(document).ready(function () {
        select2SingleSelector("#bankNameType_select_0",'<%=Language%>')
        select2SingleSelector("#accountCat_category_0",'<%=Language%>')
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        select2SingleSelector("#bankNameType_select_0", '<%=Language%>');
        select2SingleSelector("#accountCat_category_0", '<%=Language%>');
        dateTimeInit("<%=Language%>");
        $.validator.addMethod('bankNameTypeSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('accountCatSelection', function (value, element) {
            return value != 0;
        });
        $("#bank-information-form-id").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                branchName: "required",
                bankAccountNumber: "required",
                bankNameType: {
                    required: true,
                    bankNameTypeSelection: true
                },
                accountCat: {
                    required: true,
                    accountCatSelection: true
                }
            },

            messages: {
                branchName: "<%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_EMPLOYEE_BANK_INFORMATION_PLEASE_ENTER_BRANCH_NAME, userDTO)%>",
                bankAccountNumber: "<%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_EMPLOYEE_BANK_INFORMATION_PLEASE_ENTER_ACCOUNT_NUMBER, userDTO)%>",
                bankNameType: "<%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_EMPLOYEE_BANK_INFORMATION_PLEASE_ENTER_BANK_NAME, userDTO)%>",
                accountCat: "<%=LM.getText(LC.EMPLOYEE_BANK_INFORMATION_ADD_EMPLOYEE_BANK_INFORMATION_PLEASE_ENTER_ACCOUNT_TYPE, userDTO)%>"
            }
        });
    });

    function submitForm() {
        bankForm.validate();
        if(bankForm.valid()){
            submitAjaxForm('bank-information-form-id');
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>