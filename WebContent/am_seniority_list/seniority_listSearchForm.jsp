<%@ page import="util.UtilCharacter" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserDTO" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="am_house_allocation_approval_mapping.Am_house_allocation_approval_mappingDAO" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="pb.Utils" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="workflow.WorkflowController" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String value = "";
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = Language.equalsIgnoreCase("english");
    List<Am_house_allocation_requestDTO> dtoList = (List<Am_house_allocation_requestDTO>) request.getAttribute("dtos");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
            <tr>
                <th><%=UtilCharacter.getDataByLanguage(Language, "ইউজারনেম", "Username")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "আবেদনকারীর নাম", "Applicant Name")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "পদবি", "Designation")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "অফিস", "Office")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "আবেদনের তারিখ", "Apply Date")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "বাসার শ্রেণী ", "House Class")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "বিস্তারিত", "Details")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "নির্বাচন", "Select")%></th>
            </tr>
        </thead>
        <tbody>
        <%

            try {

                if (dtoList != null) {
                    int size = dtoList.size();
                    System.out.println("data not null and size = " + size + " data = " + dtoList);
                    for (int i = 0; i < size; i++) {
                        Am_house_allocation_requestDTO requestDTO = dtoList.get(i);


        %>
        <tr id='tr_<%=i%>'>

            <td>
                <%
                    value = WorkflowController.getUserNameFromErId(requestDTO.requesterEmpId, Language);
                    value = value.equalsIgnoreCase("") ? isLangEng ? "Username not found" : "ইউজারনেম পাওয়া যায় নি" : value;
                %>

                <%=value%>

            </td>

            <td>
                <%
                    value = UtilCharacter.getDataByLanguage(Language, requestDTO.requesterNameBn, requestDTO.requesterNameEn);
                %>

                <%=value%>

            </td>


            <td>
            <%
                value = UtilCharacter.getDataByLanguage(Language, requestDTO.requesterOfficeUnitOrgNameBn,
                        requestDTO.requesterOfficeUnitOrgNameEn);
            %>

            <%=value%>

            </td>
            <td>
                <%
                    value = UtilCharacter.getDataByLanguage(Language, requestDTO.requesterOfficeUnitNameBn,
                            requestDTO.requesterOfficeUnitNameEn);
                %>

                <%=value%>
            </td>


            <td>
                <%
                    value = requestDTO.insertionDate + "";
                    String formattedInsertDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formattedInsertDate, Language)%>
            </td>

            <td>
                <%
                    value = CatRepository.getInstance().getText(Language, "am_house_class", requestDTO.amHouseClassCat);
                %>

                <%=Utils.getDigits(value, Language)%>
            </td>

            <td>

            </td>

            <td class="text-center">
                <div class='checker'>
                    <span class='chkEdit' ><input type='checkbox' name='ID' value='<%=requestDTO.iD%>'/></span>
                </div>
            </td>



        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<div class="row mt-4">
    <div class="col-12">
            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2 float-right"
                    type="submit" onclick="getSelectedIds()">
                <%=UtilCharacter.getDataByLanguage(Language, "তালিকা দেখুন", "See List")%>
            </button>

    </div>
</div>

<script>

    let lang = '<%=Language%>';

    function getSelectedIds(){
        let selectedIds = [];
        let set = $('#tableData').find('tbody > tr > td:last-child input[type="checkbox"]');

        $(set).each(function () {
            if ($(this).prop('checked')) {
                selectedIds.push($(this).val());
            }
        });
        if(selectedIds.length < 2){
            $('#toast_message').css('background-color','#ff6063');
            let warningMsg = valueByLanguage(lang, 'নূন্যতম ২ টি নির্বাচন করুন', 'Please Select Minimum 2 Items');
            showToastSticky(warningMsg,warningMsg);
            return ;
        }

        $.ajax({
            type: "GET",
            url: "SeniorityListServlet?actionType=verifyList&selectedIds=" + selectedIds,
            // data: data,
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    buttonStateChange(false);
                }else if(response.responseCode === 200){
                    let link = "SeniorityListServlet?actionType=goToSeniorityPage&selectedIds=" + selectedIds;
                    window.open(link, "_blank");
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }

    function valueByLanguage(lang, bnValue, enValue) {
        if (lang === 'english' || lang === 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }

</script>
