<%@ page import="util.UtilCharacter" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=UtilCharacter.getDataByLanguage(Language, "অগ্রাধিকার নির্ধারণ", "Seniority Selection")%>
        </h3>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                        <jsp:include page="seniority_listSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value=''/>
                        </jsp:include>
                </div>
            </div>
        </div>
    </div>
</div>