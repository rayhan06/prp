<%@ page import="util.UtilCharacter" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>

<script
        src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
        integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer"
></script>
<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=UtilCharacter.getDataByLanguage(Language, "অগ্রাধিকার নির্ধারণ", "Seniority Selection")%>
        </h3>
    </div>
</div>

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                        <jsp:include page="seniority_list_dataSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value=''/>
                        </jsp:include>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    let language = '<%=Language.toLowerCase()%>';
    $(document).ready(function () {
        $(function () {
            $("#tableData tbody").sortable({
                axis: "y",
                containment: "parent",
                animation: 200,
                cursor: "move",
                opacity: 0.5,
                revert: true,
                tolerance: "pointer",
                update: function( ) {
                   updateSerial();
                }
            });
        });
    });

    function updateSerial(){
        let rows = document.querySelectorAll('.draggedTr');
        rows.forEach((row,index) => {
          let serialTd = row.querySelector('.serialTd');
          serialTd.innerText = language !== 'english' ? convertToBanglaNumber(index+1) : (index+1);
        })
    }

    function convertToBanglaNumber(str) {
        str = String(str);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    function exportTableToExcel(tableID, filename = ''){
        let downloadLink;
        let dataType = 'application/vnd.ms-excel';
        let tableSelect = document.getElementById(tableID);
        let tableHTML = tableSelect.outerHTML.replace(/ /g, '%20').replace(/#/g, '%23');
        filename = filename?filename+'.xls':'excel_data.xls';
        downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        downloadLink.download = filename;
        downloadLink.click();
    }

    function exportTableToPdf(printDiv, filename = ''){
        $("#titleTableDiv").css("display", "block");
        let scrollY = Math.ceil(window.scrollY);
        printAnyDiv(printDiv);
        window.scrollTo(0, scrollY);
        $("#titleTableDiv").css("display", "none");
    }

    function printAnyDiv(divId) {
        let printContents = document.getElementById(divId).innerHTML;
        let originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

</script>