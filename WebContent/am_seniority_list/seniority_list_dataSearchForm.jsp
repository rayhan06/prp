<%@ page import="util.UtilCharacter" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserDTO" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="am_house_allocation_approval_mapping.Am_house_allocation_approval_mappingDAO" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="pb.Utils" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="grade_wise_pay_scale.Grade_wise_pay_scaleRepository" %>
<%@ page import="workflow.WorkflowController" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String value = "";
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    List<Am_house_allocation_requestDTO> dtoList = (List<Am_house_allocation_requestDTO>) request.getAttribute("dtos");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<div class="table-responsive" id="printDiv">
    <div id="titleTableDiv" style="display:none">
        <div class="container my-5">
            <div class="text-center">
                <img class="parliament-logo" width="8%" src="/parliament_war_exploded/assets/images/perliament_logo_final2.png" alt="">
                <h3 class="mt-3">
                    বাংলাদেশ জাতীয় সংসদ সচিবালয়
                </h3>
                <h4 class="mt-3">
                    শের-এ-বাংলা নগর, ঢাকা-১২১৫
                </h4>
                <h5 class="mt-3">
                    টেলিফোন: +৮৮০২৫৫০২৮৯৬৪, পিএবিএক্স: ২৫৩৪
                </h5>
                <span style="text-decoration: underline;">
	                                             অগ্রাধিকার নির্ধারণ
	                                        </span>

                <div>

                </div>
            </div>
        </div>
    </div>
    <table border="1" id="tableData" class="table table-bordered table-striped text-nowrap ">
        <thead>
            <tr>
                <th><%=UtilCharacter.getDataByLanguage(Language, "নং", "Serial No")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "ইউজারনেম", "Username")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "আবেদনকারীর নাম", "Applicant Name")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "পদবি", "Designation")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "অর্জিত তারিখ", "Acquired Date")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "বর্তমান বেতন ", "Current Salary")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "চাকরির দৈর্ঘ্য সরকারি", "Govt. Job Length")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "বয়স ", "Age")%></th>
                <th><%=UtilCharacter.getDataByLanguage(Language, "ইংরেজী বর্ণমালায় প্রথম নাম ", "First Name in English Alphabet")%></th>
            </tr>
        </thead>
        <tbody>
        <%

            try {

                if (dtoList != null) {
                    int size = dtoList.size();
                    System.out.println("data not null and size = " + size + " data = " + dtoList);
                    for (int i = 0; i < size; i++) {
                        Am_house_allocation_requestDTO requestDTO = dtoList.get(i);


        %>
        <tr id='tr_<%=i%>' class="draggedTr">

            <td class="serialTd">
                <%
                    value = (i + 1) + "";
                %>

                <%=Utils.getDigits(value, Language)%>

            </td>

            <td>
                <%
                    value = WorkflowController.getUserNameFromErId(requestDTO.requesterEmpId, Language);
                    value = value.equalsIgnoreCase("") ? isLangEng ? "Username not found" : "ইউজারনেম পাওয়া যায় নি" : value;
                %>

                <%=value%>

            </td>

            <td>
                <%
                    value = UtilCharacter.getDataByLanguage(Language, requestDTO.requesterNameBn, requestDTO.requesterNameEn);
                %>

                <%=value%>

            </td>


            <td>
            <%
                value = UtilCharacter.getDataByLanguage(Language, requestDTO.requesterOfficeUnitOrgNameBn,
                        requestDTO.requesterOfficeUnitOrgNameEn);
            %>

            <%=value%>

            </td>

            <td>

            </td>

            <td>

                <%=Utils.getDigits(requestDTO.employeePayScaleTypeEn, Language)%>


            </td>

            <td>
                <%
                    value = "";
                    if(requestDTO.govtJobDays > 0){
                        value = Utils.getDigits(requestDTO.govtJobDays, Language) + UtilCharacter.
                                getDataByLanguage(Language, " দিন", " Days");
                    }

                %>

                <%=value%>

            </td>




            <td>
                <%=Utils.calculateCompleteAgeInFullFormat(requestDTO.dateOfBirth, Language)%>
            </td>

            <td>
                <%=requestDTO.requesterNameEn%>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<div id="btndiv" class="mb-2 mt-5" style="display: flex;justify-content: center;">
    <button type="button" class="btn btn-sm btn-warning text-white shadow btn-border-radius" id="reportPrinter"
            onclick="exportTableToPdf('printDiv','seniority_list')"><%=UtilCharacter.getDataByLanguage(Language,"প্রিন্ট","Print")%>
    </button>
    <a style="cursor: pointer;" class="btn btn-sm btn-success shadow btn-border-radius text-white mx-2" id="reportXlButton"
       onclick="exportTableToExcel('tableData','seniority_list')"><%=UtilCharacter.getDataByLanguage(Language,"এক্সেল","Excel")%>
    </a>
</div>

