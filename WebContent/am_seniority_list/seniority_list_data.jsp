<%@ page import="static permission.MenuConstants.AM_HOUSE_ALLOCATION" %>
<%@ page import="static permission.MenuConstants.AM_HOUSE_ALLOCATION_SENIORITY_SELECTION" %>
<%@ page import="static permission.MenuConstants.*" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>

<%
    request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(ASSET_MANAGEMENT,
            AM_HOUSE_ALLOCATION,
            AM_HOUSE_ALLOCATION_SENIORITY_SELECTION)));
%>
<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="Seniority List" />
    <jsp:param name="body" value="../am_seniority_list/seniority_list_dataSearchBody.jsp" />
</jsp:include>
