<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="house_rent_allowance.*" %>
<%@ page import="util.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="static jdk.nashorn.internal.objects.NativeMath.round" %>


<%
    String navigator2 = "navHOUSE_RENT_ALLOWANCE";
    String servletName = "House_rent_allowanceServlet";
    SimpleDateFormat simpleMonthDateFormat = new SimpleDateFormat("MMM, yyyy");
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=Language.equalsIgnoreCase("English") ? "Employee Name" : "কর্মকর্তা/কর্মচারির নাম"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Designation" : "পদবি"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Mobile No" : "মোবাইল নাম্বার"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Savings Account No" : "সঞ্চয়ী হিসাব নাম্বার"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Start Date" : "শুরুর তারিখ"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "End Date" : "শেষের তারিখ"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Month" : "মাস"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Total Amount" : "মোট পরিমান"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Revenue Deduction" : "রাজস্ব স্ট্যাম্প বাবদ কর্তন"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Net Amount" : "নীট পরিমান"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "View Details" : "বিস্তারিত দেখুন"%>
            </th>
            <th><%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<House_rent_allowanceDTO> data = (List<House_rent_allowanceDTO>) recordNavigator.list;
            try {

                if (data != null) {
                    int size = data.size();
                    String designation = "";
                    Calendar houseRentStartDateCal = Calendar.getInstance(), houseRentEndDateCal = Calendar.getInstance();
                    for (int i = 0; i < size; i++) {
                        House_rent_allowanceDTO house_rent_allowanceDTO = data.get(i);

                        try {
                            OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(house_rent_allowanceDTO.employeeRecordId).officeUnitOrganogramId);
                            designation = Language.equalsIgnoreCase("English") ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng;
                        } catch (Exception ex) {
                            System.out.println("got Exception in employee office and organogram table!");
                        }


                        houseRentStartDateCal.setTimeInMillis(house_rent_allowanceDTO.houseRentStart);
                        houseRentEndDateCal.setTimeInMillis(house_rent_allowanceDTO.houseRentEnd);
        %>
        <tr>

            <td>
                <%=Language.equalsIgnoreCase("English") ? house_rent_allowanceDTO.employeeNameEng : house_rent_allowanceDTO.employeeNameBng%>
            </td>

            <td>
                <%=designation%>
            </td>

            <td>
                <%
                    try {
                        value = house_rent_allowanceDTO.employeeMobileNumber.substring(2);
                    } catch (Exception ex) {
                        value = "";
                    }
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(house_rent_allowanceDTO.employeeSavingsAccountNo, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(simpleMonthDateFormat.format(new Date(house_rent_allowanceDTO.houseRentStart)), Language)%>
            </td>

            <td>
                <%=Utils.getDigits(simpleMonthDateFormat.format(new Date(house_rent_allowanceDTO.houseRentEnd)), Language)%>
            </td>

            <td>
                <%=Utils.getDigits(12 * (houseRentEndDateCal.get(Calendar.YEAR) - houseRentStartDateCal.get(Calendar.YEAR)) +
                        (houseRentEndDateCal.get(Calendar.MONTH) - houseRentStartDateCal.get(Calendar.MONTH)) + 1, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(String.format("%.1f", house_rent_allowanceDTO.totalAmount), Language)%>
            </td>

            <td>
                <%=Utils.getDigits(String.format("%.1f", house_rent_allowanceDTO.revenueDeduction), Language)%>
            </td>

            <td>
                <%=Utils.getDigits(String.format("%.1f", house_rent_allowanceDTO.netAmount),  Language)%>
            </td>

            <%CommonDTO commonDTO = house_rent_allowanceDTO; %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ea07d4;"
                        onclick="location.href='<%=servletName%>?actionType=viewBill&ID=<%=commonDTO.iD%>'"
                >
                    <%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
                </button>
            </td>

        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			