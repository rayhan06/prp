<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="house_rent_allowance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="util.StringUtils" %>

<%
    String servletName = "House_rent_allowanceServlet";
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    House_rent_allowanceDTO house_rent_allowanceDTO = House_rent_allowanceDAO.getInstance().getDTOFromID(id);
%>

<%@include file="../pb/viewInitializer.jsp" %>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=Language.equalsIgnoreCase("English") ? "HOUSE RENT ALLOWANCE" : "বাড়ি ভাড়া ভাতা"%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=Language.equalsIgnoreCase("English") ? "HOUSE RENT ALLOWANCE" : "বাড়ি ভাড়া ভাতা"%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Employee Name" : "কর্মকর্তা/কর্মচারির নাম"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Language.equalsIgnoreCase("English") ? house_rent_allowanceDTO.employeeNameEng : house_rent_allowanceDTO.employeeNameBng%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.HOUSE_RENT_ALLOWANCE_ADD_HOUSERENTSTART, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=DateUtils.getMonthYear(house_rent_allowanceDTO.houseRentStart, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.HOUSE_RENT_ALLOWANCE_ADD_HOUSERENTEND, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=DateUtils.getMonthYear(house_rent_allowanceDTO.houseRentEnd, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Net Amount" : "নীট পরিমান"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                StringUtils.convertBanglaIfLanguageIsBangla(
                                                        Language,
                                                        String.format("%.2f", house_rent_allowanceDTO.netAmount)
                                                )
                                        )%>/-
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right mt-3">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                onclick="location.href='<%=servletName%>?actionType=viewBill&ID=<%=house_rent_allowanceDTO.iD%>'">
                            <%=LM.getText(LC.MEDICAL_ALLOWANCE_SEARCH_BILL_GENERATE, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>