<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="house_rent_allowance.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<style>
    td.hidden {
        display: none;
    }
</style>

<%
    House_rent_allowanceDTO house_rent_allowanceDTO;
    house_rent_allowanceDTO = (House_rent_allowanceDTO) request.getAttribute("house_rent_allowanceDTO");
    CommonDTO commonDTO = house_rent_allowanceDTO;
    if (house_rent_allowanceDTO == null) {
        house_rent_allowanceDTO = new House_rent_allowanceDTO();

    }
    String tableName = "house_rent_allowance";
    String context = request.getContextPath() + "/";
%>
<%@include file="../pb/addInitializer.jsp" %>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=Language.equalsIgnoreCase("English") ? "HOUSE RENT ALLOWANCE" : "বাড়ি ভাড়া ভাতা"%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="designTemplateForm" name="designTemplateForm">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=Language.equalsIgnoreCase("English") ? "HOUSE RENT ALLOWANCE" : "বাড়ি ভাড়া ভাতা"%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=Language.equalsIgnoreCase("English") ? "Allowance Start Date" : "ভাতা শুরুর তারিখ"%>
                                            </label>
                                            <div class="col-md-8">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="houseRentStart_js"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="HIDE_DAY" value="true"/>
                                                </jsp:include>
                                                <input type='hidden' name='houseRentStart' id='houseRentStart_date'
                                                       value='' tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=Language.equalsIgnoreCase("English") ? "Allowance End Date" : "ভাতা সমাপ্তির তারিখ"%>
                                            </label>
                                            <div class="col-md-8">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="houseRentEnd_js"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="HIDE_DAY" value="true"/>
                                                </jsp:include>
                                                <input type='hidden' name='houseRentEnd' id='houseRentEnd_date' value=''
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=Language.equalsIgnoreCase("English") ? "Employee" : "কর্মকর্তা/কর্মচারি"%>
                                            </label>
                                            <div class="col-md-8">
                                                <div class="" id='select-referrer-div'>
                                                    <input type='hidden' class='form-control'
                                                           id="referrer-id" name="referrerId"
                                                           tag='pb_html'/>
                                                    <button type="button"
                                                            class="btn btn-primary shadow btn-block btn-border-radius"
                                                            id="tagEmp_modal_button">
                                                        <%=Language.equalsIgnoreCase("English") ? "Select Employee" : "কর্মকর্তা/কর্মচারি নির্বাচন করুন"%>
                                                    </button>
                                                    <div class="error-alert" id="referrer_error"
                                                         style="display: none">
                                                        <%=Language.equalsIgnoreCase("English") ? "Select Employee!" : "কর্মকর্তা/কর্মচারি নির্বাচন করুন!"%>
                                                    </div>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped text-nowrap">
                                                            <thead></thead>
                                                            <tbody id="tagged_emp_table" class="rounded">
                                                            <tr style="display: none;" class="selected_employee_row">
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <button type="button"
                                                                            class="btn btn-sm add-btn text-white shadow btn-border-radius pl-4"
                                                                            style="padding-right: 14px">
                                                                        <i class="fa fa-plus"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive mt-5">
                    <table id="tableData" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Serial No" : "ক্রমিক নং"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Employee Name" : "কর্মকর্তা/কর্মচারির নাম"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Designation" : "পদবী"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Mobile Number" : "মোবাইল নাম্বার"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Savings Account No" : "সঞ্চয়ী হিসাব নাম্বার"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Month" : "মাস"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Total Amount" : "মোট পরিমান"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Revenue Deduction" : "রাজস্ব-কর্তন"%>
                            </th>
                            <th style="text-align:center"><%=Language.equalsIgnoreCase("English") ? "Net Amount" : "নীট দাবী"%>
                            </th>
                        </tr>
                        </thead>
                        <tbody></tbody>

                    </table>
                </div>
                <div class="row">
                    <div class="col-12 mt-3 text-right">
                        <button type="button" id="receive_button"
                                class="btn shadow green-meadow btn-outline sbold uppercase advanceseach"
                                onclick="submitHouseRentModelData()"
                                style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px;">
                            <%=Language.equalsIgnoreCase("English") ? "SUBMIT" : "জমা নিন"%>
                        </button>
                    </div>
                </div>
                <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                       value='<%=house_rent_allowanceDTO.iD%>' tag='pb_html'/>
                <input type='hidden' class='form-control' name='employeeRecordId'
                       id='employeeRecordId_hidden_<%=i%>'
                       value='<%=house_rent_allowanceDTO.employeeRecordId%>' tag='pb_html'/>
                <input type='hidden' class='form-control' name='modifiedBy'
                       id='modifiedBy_hidden_<%=i%>' value='<%=house_rent_allowanceDTO.modifiedBy%>'
                       tag='pb_html'/>
                <input type='hidden' class='form-control' name='lastModificationTime'
                       id='lastModificationTime_hidden_<%=i%>'
                       value='<%=house_rent_allowanceDTO.lastModificationTime%>' tag='pb_html'/>
                <input type='hidden' class='form-control' name='insertedBy'
                       id='insertedBy_hidden_<%=i%>' value='<%=house_rent_allowanceDTO.insertedBy%>'
                       tag='pb_html'/>
                <input type='hidden' class='form-control' name='insertionTime'
                       id='insertionTime_hidden_<%=i%>'
                       value='<%=house_rent_allowanceDTO.insertionTime%>' tag='pb_html'/>
                <input type='hidden' class='form-control' name='isDeleted'
                       id='isDeleted_hidden_<%=i%>' value='<%=house_rent_allowanceDTO.isDeleted%>'
                       tag='pb_html'/>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

    let serialNo;
    let initReductionValue;
    let initTotalAMount;

    function typeOnlyDecimal(e) {
        return true === inputValidationForFloatValue(e, $(this), 4);
    }

    function init() {
        serialNo = 1;
        setDateByStringAndId('houseRentStart_js', '<%=dateFormat.format(new Date(System.currentTimeMillis()))%>');
        setDateByStringAndId('houseRentEnd_js', '<%=dateFormat.format(new Date(System.currentTimeMillis()))%>');

        $('body').delegate('[data-only-decimal="true"]', 'keydown', typeOnlyDecimal);


        let lang = '<%=Language%>';

        if (lang.toUpperCase() == "ENGLISH") {
            initReductionValue = 0;
            initTotalAMount = 0;
        } else {
            initReductionValue = '০';
            initTotalAMount = '০';
        }
    }

    $(document).ready(function () {
        init();
        $('#houseRentStart_js').on('datepicker.change', () => {
            setMinDateById('houseRentEnd_js', getDateStringById('houseRentStart_js'));
        });
    });

    const child_table_extra_id = <%=childTableStartingID%>;

    // select action of modal's add button
    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true,
                callBackFunction: function (empInfo) {
                    document.getElementById('referrer-id').value = empInfo.employeeRecordId;
                    $('.selected_employee_row').hide();
                    $('#search-result:hidden').show();
                    populate_search_result(empInfo.employeeRecordId);
                    $('#referrer-id-error').hide();
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {

        let houseRentStartDateValidation, houseRentEndDateValidation;
        houseRentStartDateValidation = dateValidator('houseRentStart_js', true, {
            'errorEn': 'Enter a valid Start Date!',
            'errorBn': 'বাড়ি ভাড়া ভাতা শুরু তারিখ প্রবেশ করান!'
        });

        houseRentEndDateValidation = dateValidator('houseRentEnd_js', true, {
            'errorEn': 'Enter a valid End Date!',
            'errorBn': 'বাড়ি ভাড়া ভাতা সমাপ্তির তারিখ প্রবেশ করান!'
        });

        if (houseRentStartDateValidation && houseRentEndDateValidation) {
            modal_button_dest_table = 'tagged_emp_table';
            $('#search_emp_modal').modal();
        }
    });

    function stringToHtmlElement(str) {
        const template = document.createElement('template');
        template.innerHTML = str;
        return template.content.childNodes;
    }

    function populate_search_result(emp_record_id) {

        // Ajax call for data
        let url = "House_rent_allowanceServlet?actionType=ajax_getHouseRentModel&empRecId=" + emp_record_id + "&language=" + '<%=Language%>' +
            "&startDate=" + getDateTimestampById('houseRentStart_js') + "&endDate=" + getDateTimestampById('houseRentEnd_js');
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                let model = JSON.parse(data);

                let lang = '<%=Language%>', modified_serial_no;
                if (lang.toUpperCase() === 'ENGLISH') {
                    modified_serial_no = serialNo++;
                } else {
                    modified_serial_no = convertToBanglaNumber(serialNo++);
                }

                const newRowStr = "<tr><td>" + modified_serial_no + "</td>" +
                    "<td>" + model.name + "</td>" +
                    "<td>" + model.designation + "</td>" +
                    "<td>" + model.mobileNumber + "</td>" +
                    "<td>" + model.savingsAccountNumber + "</td>" +
                    "<td>" + model.month + "</td>" +
                    "<td><input class='form-control' type=\"text\" data-only-decimal='true' value=\"" + initTotalAMount + "\" oninput=\"set_net_amount(" + (serialNo - 1) + ")\" id=\"tb_total_amount_" + (serialNo - 1) + "\"></input></td>" +
                    "<td><input class='form-control' type=\"text\" data-only-decimal='true' value=\"" + initReductionValue + "\" oninput=\"set_net_amount(" + (serialNo - 1) + ")\" id=\"tb_rev_deduce_" + (serialNo - 1) + "\"></input></td>" +
                    "<td id=\"tb_net_amount_" + (serialNo - 1) + "\">" + initTotalAMount + "</td>" +
                    "<td id=\"tb_start_date_" + (serialNo - 1) + "\" class=\"hidden\">" + getDateTimestampById('houseRentStart_js') + "</td>" +
                    "<td id=\"tb_end_date_" + (serialNo - 1) + "\" class=\"hidden\">" + getDateTimestampById('houseRentEnd_js') + "</td>" +
                    "<td id=\"tb_record_id_" + (serialNo - 1) + "\" class=\"hidden\">" + emp_record_id + "</td></tr>";

                $('#tableData > tbody').append(stringToHtmlElement(newRowStr));
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function convertToEnglishNumber(input) {
        let numbers = {
            '০': 0,
            '১': 1,
            '২': 2,
            '৩': 3,
            '৪': 4,
            '৫': 5,
            '৬': 6,
            '৭': 7,
            '৮': 8,
            '৯': 9
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    function convertToBanglaNumber(english_number) {
        let input = english_number + '';
        let numbers = {
            '0': '০',
            '1': '১',
            '2': '২',
            '3': '৩',
            '4': '৪',
            '5': '৫',
            '6': '৬',
            '7': '৭',
            '8': '৮',
            '9': '৯'
        };
        let output = [];
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }


    function submitHouseRentModelData() {
        let modelData = "";
        let elementName = "";
        let lang = '<%=Language%>';

        for (let k = 1; k < serialNo; k++) {
            elementName = 'tb_start_date_' + k;
            modelData += $('#' + elementName).text();

            elementName = 'tb_end_date_' + k;
            modelData += ',' + $('#' + elementName).text();

            elementName = 'tb_record_id_' + k;
            modelData += ',' + convertToEnglishNumber($('#' + elementName).text());

            elementName = 'tb_rev_deduce_' + k;
            modelData += ',' + convertToEnglishNumber($('#' + elementName).val());
            const deduceAmount = $('#' + elementName).val();

            elementName = 'tb_total_amount_' + k;
            modelData += ',' + convertToEnglishNumber($('#' + elementName).val()) + ';';
            const totalAmount = $('#' + elementName).val();

            console.log("totalAmount", totalAmount);
            console.log("totalDeduction", deduceAmount);
            if (Number(totalAmount) < Number(deduceAmount)) {
                showToast("রাজস্ব কর্তন মোট পরিমাণের চেয়ে কম হবে", "Deduction must be less than total amount");
                return;
            }
        }

        let url = "House_rent_allowanceServlet?actionType=ajax_submitHouseRentModelData&modelData=" + modelData;
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                console.log(data);
                document.location.href = '<%=request.getContextPath()%>' + '/House_rent_allowanceServlet?actionType=search';
            },
            error: function (error) {
                console.log(error);
            }
        });
    }


    function set_net_amount(serial_no) {
        console.log($('#tb_total_amount_' + serial_no).text() + '  ' + $('#tb_rev_deduce_' + serial_no).val());

        let total_amount = $('#tb_total_amount_' + serial_no).val();
        let rev_stamp_deduce = $('#tb_rev_deduce_' + serial_no).val();
        let net_amount = parseFloat(convertToEnglishNumber(total_amount)) - parseFloat(convertToEnglishNumber(rev_stamp_deduce));
        let lang = '<%=Language%>';
        if (net_amount) {
            $('#tb_net_amount_' + serial_no).text(net_amount);
        } else {
            $('#tb_net_amount_' + serial_no).text("");
        }
        // if (lang.toUpperCase() == 'ENGLISH') {
        //     $('#tb_net_amount_' + serial_no).text(net_amount);
        // } else {
        //     $('#tb_net_amount_' + serial_no).text(convertToBanglaNumber(net_amount));
        // }
    }

</script>