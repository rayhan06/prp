<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "House_rent_allowanceServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.HOUSE_RENT_ALLOWANCE_SEARCH_HOUSERENTSTART, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="house_rent_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                <jsp:param name="HIDE_DAY" value="true"/>
                            </jsp:include>
                            <input type="hidden" id="house_rent_start" name="house_rent_start">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.HOUSE_RENT_ALLOWANCE_SEARCH_HOUSERENTEND, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="house_rent_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                <jsp:param name="HIDE_DAY" value="true"/>
                            </jsp:include>
                            <input type="hidden" id="house_rent_end" name="house_rent_end">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=Language.equalsIgnoreCase("English") ? "Employee Name" : "কর্মকর্তা/কর্মচারির নাম"%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="employee_name" name="employee_name"
                                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Enter Employee Name" : "কর্মকর্তা/কর্মচারির নাম দিন"%>'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=Language.equalsIgnoreCase("English") ? "Mobile No" : "মোবাইল নাম্বার"%>
                        </label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"
                                          id="basic-addon3"><%=Language.equalsIgnoreCase("English") ? "+88" : "+৮৮"%></span>
                                </div>
                                <input type='text' class='form-control englishDigitOrCharOnly'
                                       id="employee_personal_mobile_number" name="employee_personal_mobile_number"
                                       placeholder='<%=Language.equalsIgnoreCase("English") ? "Enter your 11 digit mobile number, ex: 01710101010" : "১১ ডিজিটের মোবাইল নাম্বারটি দিন, উদাহরণঃ 01710101010"%>'
                                       tag='pb_html'/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=Language.equalsIgnoreCase("English") ? "Savings Account No" : "সঞ্চয়ী হিসাব নং"%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="employee_savings_account_number"
                                   name="employee_savings_account_number"
                                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Enter Employee's Saving Account Number" : "কর্মকর্তা/কর্মচারির সঞ্চয়ী হিসাব নাম্বার দিন"%>'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const employeeNameSelector = $('#employee_name');
    const houseRentStartSelector = $('#house_rent_start');
    const houseRentEndSelector = $('#house_rent_end');
    const mobileNumberSelector = $('#employee_personal_mobile_number');
    const savingsNumberSelector = $('#employee_savings_account_number');

    function resetInputs() {
        employeeNameSelector.val("");
        mobileNumberSelector.val("");
        savingsNumberSelector.val("");
        resetDateById('house_rent_start_js');
        resetDateById('house_rent_end_js');
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state;
            dosubmit(params, false);
            resetInputs();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {
                        case 'employee_name_eng':
                        case 'employee_name_bng':
                            employeeNameSelector.val(item[1]);
                            break;
                        case 'house_rent_start':
                            setDateByTimestampAndId('house_rent_start_js', item[1]);
                            break;
                        case 'house_rent_end':
                            setDateByTimestampAndId('house_rent_end_js', item[1]);
                            break;
                        case 'employee_personal_mobile_number':
                            mobileNumberSelector.val(item[1].substring(2, item[1].length));
                            break;
                        case 'employee_savings_account_number':
                            savingsNumberSelector.val(item[1]);
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (pushState) {
                    history.pushState(params, '', 'House_rent_allowanceServlet?actionType=search&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText ;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        let url = "<%=action%>&ajax=true&isPermanentTable=true";
        if (params) {
            url += "&" + params;
        }
        xhttp.open("GET", url, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        let params = 'AnyField=';

        params += '&house_rent_start=' + getDateTimestampById('house_rent_start_js');
        params += '&house_rent_end=' + getDateTimestampById('house_rent_end_js');
        params += '&employee_savings_account_number=' + $('#employee_savings_account_number').val();

        if (!/[^a-zA-Z0-9 ]/.test($('#employee_name').val())) {
            params += '&employee_name_eng=' + $('#employee_name').val();
        } else {
            params += '&employee_name_bng=' + $('#employee_name').val();
        }

        if ($('#employee_personal_mobile_number').val() == '') {
            params += '&employee_personal_mobile_number=';
        } else {
            params += '&employee_personal_mobile_number=88' + $('#employee_personal_mobile_number').val();
        }

        params += '&search=true';

        const extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        let pageNo = document.getElementsByName('pageno')[0].value;
        let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        let totalRecords = 0;
        let lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

