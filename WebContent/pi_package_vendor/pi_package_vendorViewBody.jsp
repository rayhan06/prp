<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_package_vendor.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="procurement_goods.Procurement_goodsDTO" %>
<%@ page import="pi_app_request.Pi_app_requestDAO" %>
<%@ page import="pi_package_vendor_items.Pi_package_vendor_itemsDAO" %>
<%@ page import="pi_package_vendor_items.Pi_package_vendor_itemsDTO" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@ page import="procurement_goods.Procurement_goodsRepository" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_package.Procurement_packageDTO" %>
<%@ page import="pi_package_new.Pi_package_newRepository" %>
<%@ page import="pi_package_new.Pi_package_newDTO" %>
<%
    String servletName = "Pi_package_vendorServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Pi_package_vendorDTO pi_package_vendorDTO = Pi_package_vendorDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = pi_package_vendorDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>
<style>
    input[type=checkbox] {
        opacity: 0.8;
    }
</style>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_FISCALYEARID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = Fiscal_yearRepository.getInstance().getText(pi_package_vendorDTO.fiscalYearId, Language) + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=UtilCharacter.getDataByLanguage(Language,"প্যাকেজ","Package")%>
                                    </label>
                                    <div class="col-md-8 form-control">

                                        <%
                                            Pi_package_newDTO pi_package_newDTO =    Pi_package_newRepository.getInstance().getPi_package_newDTOByiD(pi_package_vendorDTO.packageId);
                                        %>

                                        <%=UtilCharacter.getDataByLanguage(Language,pi_package_newDTO.packageNameBn,pi_package_newDTO.packageNameEn)%>



                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_TENDERADVERTISECAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = "";
                                            value = pi_package_vendorDTO.tenderAdvertiseCat;
                                            StringTokenizer tokenizer = new StringTokenizer(value, ",");
                                            String separator = "";
                                            value = "";
                                            while (tokenizer.hasMoreTokens()) {
                                                String val = tokenizer.nextToken().trim();
                                                if (!val.equalsIgnoreCase("")) {
                                                    Long distNum = Long.valueOf(val);
                                                    String advertiseMedium = CatRepository.getInstance().getText(Language, "tender_advertise", distNum);
                                                    value = value + separator + advertiseMedium;
                                                    separator = ", ";
                                                }
                                            }
                                        %>

                                        <%=value%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_TEMDERADVERTISEDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_package_vendorDTO.temderAdvertiseDate + "";
                                        %>
                                        <%
                                            String formatted_temderAdvertiseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            formatted_temderAdvertiseDate += " " + pi_package_vendorDTO.tenderAdvertiseTime;
                                        %>
                                        <%=Utils.getDigits(formatted_temderAdvertiseDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_TENDEROPENINGDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_package_vendorDTO.tenderOpeningDate + "";
                                        %>
                                        <%
                                            String formatted_tenderOpeningDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            formatted_tenderOpeningDate += " " + pi_package_vendorDTO.tenderOpeningTime;
                                        %>
                                        <%=Utils.getDigits(formatted_tenderOpeningDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_NOADATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_package_vendorDTO.noaDate + "";
                                        %>
                                        <%
                                            String formatted_noaDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            formatted_noaDate += " " + pi_package_vendorDTO.noaTime;
                                        %>
                                        <%=Utils.getDigits(formatted_noaDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_AGREEMENTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_package_vendorDTO.agreementDate + "";
                                        %>
                                        <%
                                            String formatted_agreementDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            formatted_agreementDate += " " + pi_package_vendorDTO.agreementTime;
                                        %>
                                        <%=Utils.getDigits(formatted_agreementDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_AGREEMENTENDINGDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = pi_package_vendorDTO.agreementEndingDate + "";
                                        %>
                                        <%
                                            String formatted_agreementEndingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            formatted_agreementEndingDate += " " + pi_package_vendorDTO.agreementEndingTime;
                                        %>
                                        <%=Utils.getDigits(formatted_agreementEndingDate, Language)%>
                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_VENDORSTATUS, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=CatRepository.getInstance().getText(Language, "pi_vendor_status", pi_package_vendorDTO.vendorStatus)%>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-5">
                <div class=" div_border attachement-div">
                    <h5 class="table-title"><%=LM.getText(LC.PI_PACKAGE_VENDOR_EDIT_PI_PACKAGE_VENDOR_CHILDREN_LASTMODIFICATIONTIME, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_NAME, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_ADDRESS, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_MOBILE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_ISCHOSEN, loginDTO)%>
                                </th>
                            </tr>
                            <%
                                PiPackageVendorChildrenDAO piPackageVendorChildrenDAO = PiPackageVendorChildrenDAO.getInstance();
                                List<PiPackageVendorChildrenDTO> piPackageVendorChildrenDTOs = (List<PiPackageVendorChildrenDTO>) piPackageVendorChildrenDAO.getDTOsByParent("pi_package_vendor_id", pi_package_vendorDTO.iD);

                                for (PiPackageVendorChildrenDTO piPackageVendorChildrenDTO : piPackageVendorChildrenDTOs) {
                            %>
                            <tr>
                                <td>
                                    <%
                                        value = piPackageVendorChildrenDTO.name + "";
                                    %>

                                    <%=value%>


                                </td>
                                <td>

                                    <%= piPackageVendorChildrenDTO.address%>


                                </td>
                                <td>
                                    <%
                                        value = piPackageVendorChildrenDTO.mobile + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td>
                                    <%
                                        value = piPackageVendorChildrenDTO.isChosen + "";
                                    %>

                                    <%=Utils.getYesNo(value, Language)%>


                                </td>

                            </tr>
                            <%

                                }

                            %>
                        </table>
                    </div>
                </div>
            </div>

            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRODUCTID, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr id="dynamicTrHeader">
                                <th><%=LM.getText(LC.PI_AUCTION_ADD_PRODUCT_NAME, loginDTO)%>
                                </th>
                                <%int headerIndex = 1;%>

                                <%
                                    for (PiPackageVendorChildrenDTO piPackageChildHeaderDTO : pi_package_vendorDTO.piPackageVendorChildrenDTOList) {
                                %>
                                <th id="vendor_header_<%=headerIndex++%>"><%=piPackageChildHeaderDTO.name%> <%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%>
                                </th>
                                <%}%>


                            </tr>
                            </thead>
                            <tbody id="NVendorProducts">


                            <%

                                List<Procurement_goodsDTO> procurement_goodsDTOS = Pi_app_requestDAO.getInstance().getDTOSByFiscalYearIdAndOfcUnitIdAndPackageId(
                                        pi_package_vendorDTO.fiscalYearId, pi_package_vendorDTO.officeUnitId, pi_package_vendorDTO.packageId
                                );

                                List<Pi_package_vendor_itemsDTO> pi_package_vendor_itemsDTOS = Pi_package_vendor_itemsDAO.getInstance()
                                        .getAllDTOByVendorAndPackageAndFiscalYearIdAndOfficeUnitId
                                                (pi_package_vendorDTO.iD, pi_package_vendorDTO.packageId, pi_package_vendorDTO.fiscalYearId, pi_package_vendorDTO.officeUnitId);


                                int pvpIndex = -1;
                                Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
                                for (Procurement_goodsDTO product : procurement_goodsDTOS) {


                                    pvpIndex++;
                                    List<Pi_package_vendor_itemsDTO> getPiPackageItemsDtoByProductId = Pi_package_vendor_itemsDAO.getInstance()
                                            .getPiPackageItemsDtoByProductId(pi_package_vendor_itemsDTOS, product.iD);

                                    if (getPiPackageItemsDtoByProductId.size() > 0) {
                            %>

                            <tr class="productsAndVendorPriceTr" id="productsAndVendorPriceTr_<%=pvpIndex%>">

                                <td class="productsAndVendorPriceTd">
                                    <%=procurement_goodsDAO.getCircularData(product.iD, Language)%>
                                </td>

                                <%
                                    int forDelIndex = 1;
                                    for (PiPackageVendorChildrenDTO piPackageChildDTO : pi_package_vendorDTO.piPackageVendorChildrenDTOList) {
                                        Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO = Pi_package_vendor_itemsDAO.getInstance()
                                                .getPiPackageItemsDtoByProductIdAndChildrenId(pi_package_vendor_itemsDTOS, product.iD, piPackageChildDTO.iD); //TODO
                                        if (pi_package_vendor_itemsDTO != null) {
                                            System.out.println("piPackageChildDTO.isChosen: " + piPackageChildDTO.isChosen);
                                %>

                                <td class="vendor_data_<%=(forDelIndex)%>"><%=pi_package_vendor_itemsDTO.price > -1 ? pi_package_vendor_itemsDTO.price : ""%>
                                </td>

                                <%} else {%>
                                <td class="vendor_data_<%=(forDelIndex)%>"></td>

                                <%}%>
                                <%}%>

                            </tr>
                            <%
                                }
                            %>
                            <%}%>

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("input:checkbox").click(function () {
        return false;
    });
</script>