<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_package_vendor.*" %>
<%@ page import="util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="java.util.*" %>
<%@ page import="pi_package_final.Pi_package_finalDTO" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_package_final.PiPackageLotFinalDTO" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navPI_PACKAGE_VENDOR";
    String servletName = "Pi_package_vendorServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_FISCALYEARID, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ", "Package")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "লট", "Lot")%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_NAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_ADDRESS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_MOBILE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_TENDERADVERTISECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_AGREEMENTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_AGREEMENTENDINGDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_VENDORSTATUS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_SEARCH_PI_PACKAGE_VENDOR_EDIT_BUTTON, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            try {
                List<Pi_package_vendorDTO> data = (List<Pi_package_vendorDTO>) rn2.list;
                if (data != null && data.size() > 0) {
                    for (Pi_package_vendorDTO pi_package_vendorDTO : data) {
                        Pi_package_finalDTO packageDTO = Pi_package_finalRepository.getInstance()
                                .getPi_package_finalDTOByiD(pi_package_vendorDTO.packageId);
                        PiPackageLotFinalDTO lotDTO = PiPackageLotFinalRepository.getInstance()
                                .getPiPackageLotFinalDTOByiD(pi_package_vendorDTO.lotId);
        %>
        <tr>
            <td>
                <%
                    value = Fiscal_yearRepository.getInstance().getText(pi_package_vendorDTO.fiscalYearId, Language) + "";
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>
            <td>
                <%
                    value = packageDTO == null ? "" : UtilCharacter.getDataByLanguage
                            (Language, packageDTO.packageNumberBn, packageDTO.packageNumberEn);
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = lotDTO == null ? "" : UtilCharacter.getDataByLanguage
                            (Language, lotDTO.lotNumberBn, lotDTO.lotNumberEn);
                %>
                <%=value%>
            </td>
            <td>
                <%=pi_package_vendorDTO.winnerVendorName%>
            </td>
            <td>
                <%=pi_package_vendorDTO.winnerVendorAddress%>
            </td>
            <td>
                <%=Utils.getDigits(pi_package_vendorDTO.winnerVendorMobile, Language)%>
            </td>
            <td>
                <%
                    value = "";
                    value = pi_package_vendorDTO.tenderAdvertiseCat;
                    StringTokenizer tokenizer = new StringTokenizer(value, ",");
                    String separator = "";
                    value = "";
                    while (tokenizer.hasMoreTokens()) {
                        String val = tokenizer.nextToken().trim();
                        if (!val.equalsIgnoreCase("")) {
                            Long distNum = Long.valueOf(val);
                            String advertiseMedium = CatRepository.getInstance().getText(Language, "tender_advertise", distNum);
                            value = value + separator + advertiseMedium;
                            separator = ", ";
                        }

                    }
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = pi_package_vendorDTO.agreementDate + "";
                %>
                <%
                    String formatted_agreementDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                    formatted_agreementDate += " " + pi_package_vendorDTO.agreementTime;
                %>
                <%=Utils.getDigits(formatted_agreementDate, Language)%>
            </td>
            <td>
                <%
                    value = pi_package_vendorDTO.agreementEndingDate + "";
                %>
                <%
                    String formatted_agreementEndingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                    formatted_agreementEndingDate += " " + pi_package_vendorDTO.agreementEndingTime;
                %>
                <%=Utils.getDigits(formatted_agreementEndingDate, Language)%>
            </td>
            <td>
                <%=CatRepository.getInstance().getText(Language, "pi_vendor_status", pi_package_vendorDTO.vendorStatus)%>
            </td>

            <%CommonDTO commonDTO = pi_package_vendorDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>
        </tr>
        <%
                    }
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>