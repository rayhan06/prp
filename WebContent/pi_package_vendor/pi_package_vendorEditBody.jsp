<%@page import="pi_package_vendor.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>

<%
    Pi_package_vendorDTO pi_package_vendorDTO = new Pi_package_vendorDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_package_vendorDTO = Pi_package_vendorDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_package_vendorDTO;
    String tableName = "pi_package_vendor";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_package_vendorServlet";

    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();

    long officeUnitId = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO.unitID;

    String levelOptions = Pi_vendor_auctioneer_detailsRepository.getInstance().getOptions(Language, -1, "1");


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_SELECT_FISCAL_YEAR, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>

                                            <div class="col-md-8">
                                                <select class='form-control' name='fiscalYearId' id='fiscalYearId'
                                                        tag='pb_html' onchange="fiscalYearChanged(this)">
                                                    <%
                                                        Fiscal_yearDTO cur_fis_dto = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());

                                                        String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                        StringBuilder option = new StringBuilder();
                                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {

                                                            if (actionName.equals("ajax_edit") && pi_package_vendorDTO != null) {
                                                                if (pi_package_vendorDTO.fiscalYearId == fiscal_yearDTO.id) {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                                } else {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                                }
                                                            } else {
                                                                if (cur_fis_dto.id == fiscal_yearDTO.id) {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                                } else {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                                }
                                                            }

                                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                        }
                                                        fiscalYearOptions += option.toString();
                                                    %>
                                                    <%=fiscalYearOptions%>
                                                </select>

                                            </div>
                                        </div>

                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=pi_package_vendorDTO.iD%>' tag='pb_html'/>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=Language.equals("English") ? "Select Package" : "প্যাকেজ নির্বাচন করুন"%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>

                                            <div class="col-md-8">
                                                <select class='form-control' name='piPackageFinalId' id='piPackageFinalId'
                                                        tag='pb_html' onchange="packageIdChanged(this)">
                                                    <%= Pi_package_finalRepository.getInstance().buildOptions(Language, pi_package_vendorDTO.packageId) %>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=Language.equals("English") ? "Select Lot" : "লট নির্বাচন করুন"%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>

                                            <div class="col-md-8">
                                                <select class='form-control' name='piLotFinalId' id='piLotFinalId'
                                                        tag='pb_html' onchange="lotIdChanged(this)">
                                                </select>

                                            </div>
                                        </div>

                                        <div class="mt-4">
                                            <div class="form-body">
                                                <h5 class="table-title">
                                                    <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN, loginDTO)%>
                                                </h5>
                                                <div id="vendorChildTableDiv" style="display: none;">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_NAME, loginDTO)%>
                                                                </th>
                                                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_ADDRESS, loginDTO)%>
                                                                </th>
                                                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_MOBILE, loginDTO)%>
                                                                </th>
                                                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_ISCHOSEN, loginDTO)%>
                                                                </th>
                                                                <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CHILDREN_REMOVE, loginDTO)%>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="field-PiPackageVendorChildren">


                                                            <%
                                                                if (actionName.equals("ajax_edit")) {
                                                                    int index = -1;


                                                                    for (PiPackageVendorChildrenDTO piPackageVendorChildrenDTO : pi_package_vendorDTO.piPackageVendorChildrenDTOList) {
                                                                        index++;

                                                            %>

                                                            <tr id="PiPackageVendorChildren_<%=index + 1%>">
                                                                <td style="display: none;">
                                                                    <input type='hidden' class='form-control'
                                                                           name='piPackageVendorChildren.iD'
                                                                           id='iD_hidden_<%=childTableStartingID%>'
                                                                           value='<%=piPackageVendorChildrenDTO.iD%>'
                                                                           tag='pb_html'/>

                                                                </td>
                                                                <td style="display: none;">
                                                                    <input type='hidden' class='form-control'
                                                                           name='piPackageVendorChildren.piPackageVendorId'
                                                                           id='piPackageVendorId_hidden_<%=childTableStartingID%>'
                                                                           value='<%=piPackageVendorChildrenDTO.piPackageVendorId%>'
                                                                           tag='pb_html'/>
                                                                </td>
                                                                <td>

                                                                </td>
                                                                <td>

                                                                    <input type='text'
                                                                           class='form-control'
                                                                           name='piPackageVendorChildren.address'
                                                                           id='address_geolocation_<%=childTableStartingID%>'
                                                                           value='<%=piPackageVendorChildrenDTO.address%>'
                                                                           tag='pb_html'/>

                                                                </td>
                                                                <td>

                                                                    <input type='text' class='form-control'
                                                                           name='piPackageVendorChildren.mobile'
                                                                           id='mobile_text_<%=childTableStartingID%>'
                                                                           required="required"
                                                                           pattern="^(01[3-9]{1}[0-9]{8})"
                                                                           value='<%=piPackageVendorChildrenDTO.mobile ==null || piPackageVendorChildrenDTO.mobile.length() == 0?"":
																	   (piPackageVendorChildrenDTO.mobile.startsWith("88") ? piPackageVendorChildrenDTO.mobile.substring(2) : piPackageVendorChildrenDTO.mobile)%>'
                                                                           placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                                           title='<%=isLanguageEnglish?"personal mobile number must start with 01, then contain 9 digits"
																   :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                                                           tag='pb_html'/>
                                                                </td>
                                                                <td>
                                                                    <input type='checkbox'
                                                                           class='form-control-sm winner-checkbox'
                                                                           name='piPackageVendorChildren.isChosen'
                                                                           id='isChosen_checkbox_<%=childTableStartingID%>'
                                                                           value='true' <%=(String.valueOf(piPackageVendorChildrenDTO.isChosen).equals("true"))?("checked"):""%>
                                                                           tag='pb_html'>
                                                                </td>
                                                                <td style="display: none;">
                                                                    <input type='hidden'
                                                                           class='form-control isChosenVal_hidden_input'
                                                                           name='piPackageVendorChildren.isChosenVal'
                                                                           id='isChosenVal_hidden_<%=childTableStartingID%>'
                                                                           value='-1' tag='pb_html'/>

                                                                </td>

                                                                </td>
                                                                <td>
																	<span id='chkEdit'>
																		<input type='checkbox' name='checkbox' value=''
                                                                               deletecb='true'
                                                                               class="form-control-sm"/>
																	</span>
                                                                </td>
                                                            </tr>
                                                            <%
                                                                        childTableStartingID++;
                                                                    }
                                                                }
                                                            %>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-xs-9 text-md-right">
                                                            <button
                                                                    id="add-more-PiPackageVendorChildren"
                                                                    name="add-morePiPackageVendorChildren"
                                                                    type="button"
                                                                    class="btn btn-sm text-white add-btn shadow">
                                                                <i class="fa fa-plus"></i>
                                                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                                            </button>
                                                            <button
                                                                    id="remove-PiPackageVendorChildren"
                                                                    name="removePiPackageVendorChildren"
                                                                    type="button"
                                                                    class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <%PiPackageVendorChildrenDTO piPackageVendorChildrenDTO = new PiPackageVendorChildrenDTO();%>

                                                <template id="template-PiPackageVendorChildren">
                                                    <tr>
                                                        <td style="display: none;">
                                                            <input type='hidden' class='form-control'
                                                                   name='piPackageVendorChildren.iD' id='iD_hidden_'
                                                                   value='<%=piPackageVendorChildrenDTO.iD%>'
                                                                   tag='pb_html'/>
                                                        </td>
                                                        <td style="display: none;">
                                                            <input type='hidden' class='form-control'
                                                                   name='piPackageVendorChildren.piPackageVendorId'
                                                                   id='piPackageVendorId_hidden_'
                                                                   value='<%=piPackageVendorChildrenDTO.piPackageVendorId%>'
                                                                   tag='pb_html'/>
                                                        </td>
                                                        <td>

                                                            <select class='form-control' onchange="vendorChanged(this)"
                                                                    name='piPackageVendorChildren.name'
                                                                    id='name_text_' tag='pb_html'>
                                                            </select>

                                                        </td>
                                                        <td>


                                                            <input type='text'
                                                                   class='form-control'
                                                                   name='piPackageVendorChildren.address'
                                                                   id='address_geoTextField_'
                                                                   value='<%=piPackageVendorChildrenDTO.address%>'
                                                                   tag='pb_html' disabled="disabled"/>

                                                        </td>
                                                        <td>

                                                            <input type='text' class='form-control'
                                                                   name='piPackageVendorChildren.mobile'
                                                                   id='mobile_text_'
                                                                   required="required"
                                                                   pattern="^(01[3-9]{1}[0-9]{8})"
                                                                   value='<%=piPackageVendorChildrenDTO.mobile ==null || piPackageVendorChildrenDTO.mobile.length() == 0?"":
                                                                   (piPackageVendorChildrenDTO.mobile.startsWith("88") ? piPackageVendorChildrenDTO.mobile.substring(2) : piPackageVendorChildrenDTO.mobile)%>'
                                                                   placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                                   title='<%=isLanguageEnglish?"personal mobile number must start with 01, then contain 9 digits"
                                                               :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                                                   tag='pb_html' disabled="disabled"/>
                                                        </td>
                                                        <td>

                                                            <input type='checkbox'
                                                                   class='form-control-sm'
                                                                   name='piPackageVendorChildren.isChosen'
                                                                   id='isChosen_checkbox_'
                                                                   value='true' <%=(String.valueOf(piPackageVendorChildrenDTO.isChosen).equals("true"))?("checked"):""%>
                                                                   tag='pb_html'
                                                                   onclick="onlyOne(this)">
                                                        </td>
                                                        <td style="display: none;">
                                                            <input type='hidden'
                                                                   class='form-control isChosenVal_hidden_input'
                                                                   name='piPackageVendorChildren.isChosenVal'
                                                                   id='isChosenVal_hidden_' value='-1' tag='pb_html'/>

                                                        </td>

                                                        <td>
															<span id='chkEdit'>
																<input type='checkbox' name='checkbox' value=''
                                                                       deletecb='true'
                                                                       class="form-control-sm"/>
															</span>
                                                        </td>
                                                    </tr>

                                                </template>
                                            </div>
                                        </div>


                                        <div class="mt-4">
                                            <div class="form-body">

                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped"
                                                           id="NVendorProducts">
                                                        <thead>
                                                        <tr id="dynamicTrHeader">
                                                            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRODUCTID, loginDTO)%>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_MOMENTCODETIME, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <%value = "momentCodeTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden'
                                                       value="<%=pi_package_vendorDTO.momentCodeTime%>"
                                                       name='momentCodeTime'
                                                       id='momentCodeTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_TENDERADVERTISECAT, loginDTO)%><span
                                                    class="required" style="color: red;"> * </span></label>
                                            <div class="col-md-8">

                                                <select multiple="multiple" class='form-control'
                                                        name='tenderAdvertiseCat'
                                                        id='tenderAdvertiseCat' tag='pb_html'>

                                                </select>


                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_TEMDERADVERTISEDATE, loginDTO)%><span
                                                    class="required" style="color: red;"> * </span></label>
                                            <div class="col-md-5">
                                                <%value = "temderAdvertiseDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='tenderAdvertiseDate'
                                                       id='temderAdvertiseDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_vendorDTO.temderAdvertiseDate))%>'
                                                       tag='pb_html'>
                                            </div>

                                            <div class="col-md-5">
                                                <%value = "tenderAdvertiseTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden'
                                                       value="<%=pi_package_vendorDTO.tenderAdvertiseTime%>"
                                                       name='tenderAdvertiseTime' id='tenderAdvertiseTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_TENDEROPENINGDATE, loginDTO)%><span
                                                    class="required" style="color: red;"> * </span></label>
                                            <div class="col-md-5">
                                                <%value = "tenderOpeningDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='tenderOpeningDate'
                                                       id='tenderOpeningDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_vendorDTO.tenderOpeningDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "tenderOpeningTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' value="<%=pi_package_vendorDTO.tenderOpeningTime%>"
                                                       name='tenderOpeningTime' id='tenderOpeningTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_NOADATE, loginDTO)%><span
                                                    class="required" style="color: red;"> * </span></label>
                                            <div class="col-md-5">
                                                <%value = "noaDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='noaDate' id='noaDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_vendorDTO.noaDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "noaTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' value="<%=pi_package_vendorDTO.noaTime%>"
                                                       name='noaTime' id='noaTime_time_<%=i%>' tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_AGREEMENTDATE, loginDTO)%><span
                                                    class="required" style="color: red;"> * </span></label>
                                            <div class="col-md-5">
                                                <%value = "agreementDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='agreementDate' id='agreementDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_vendorDTO.agreementDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "agreementTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' value="<%=pi_package_vendorDTO.agreementTime%>"
                                                       name='agreementTime' id='agreementTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_AGREEMENTENDINGDATE, loginDTO)%><span
                                                    class="required" style="color: red;"> * </span></label>
                                            <div class="col-md-5">
                                                <%value = "agreementEndingDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='agreementEndingDate'
                                                       id='agreementEndingDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_package_vendorDTO.agreementEndingDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                            <div class="col-md-5">
                                                <%value = "agreementEndingTime_js_" + i;%>
                                                <jsp:include page="/time/time.jsp">
                                                    <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                    <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden'
                                                       value="<%=pi_package_vendorDTO.agreementEndingTime%>"
                                                       name='agreementEndingTime' id='agreementEndingTime_time_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_FILESDROPZONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-10">
                                                <%
                                                    fileColumnName = "filesDropzone";
                                                    if (actionName.equals("ajax_edit")) {
                                                        List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(pi_package_vendorDTO.filesDropzone);
                                                %>
                                                <%@include file="../pb/dropzoneEditor.jsp" %>
                                                <%
                                                    } else {
                                                        ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                        pi_package_vendorDTO.filesDropzone = ColumnID;
                                                    }
                                                %>

                                                <div class="dropzone"
                                                     action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=pi_package_vendorDTO.filesDropzone%>">
                                                    <input type='file' style="display:none"
                                                           name='<%=fileColumnName%>File'
                                                           id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                       id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                       tag='pb_html'/>
                                                <input type='hidden' name='<%=fileColumnName%>'
                                                       id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=pi_package_vendorDTO.filesDropzone%>'/>


                                            </div>
                                        </div>
                                        <div class="form-group row">

                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_VENDORSTATUS, loginDTO)%><span
                                                    class="required" style="color: red;"> * </span></label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='vendorStatus'
                                                        id='vendorStatus_number_<%=i%>' tag='pb_html'>
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptions("pi_vendor_status", Language, pi_package_vendorDTO.vendorStatus);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-11 text-right">
                        <div class="form-actions  mb-2 mt-3">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitForm()">
                                <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const piPackageVendorForm = $("#bigform");

    function processMultipleSelectBoxBeforeSubmit2(name) {

        $("[name='" + name + "']").each(function (i) {
            var selectedInputs = $(this).val();
            var temp = "";
            if (selectedInputs != null) {
                selectedInputs.forEach(function (value, index, array) {
                    if (index > 0) {
                        temp += ", ";
                    }
                    temp += value;
                });
            }
            if (temp.includes(',')) {
                $(this).append('<option value="' + temp + '"></option>');
            }
            $(this).val(temp);

        });
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting(0)) {
            processMultipleSelectBoxBeforeSubmit2("tenderAdvertiseCat");
            $.ajax({
                type: "POST",
                url: "<%=servletName%>?actionType=<%=actionName%>",
                data: piPackageVendorForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    console.log(response);
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        showToastSticky("সাবমিট সফল হয়েছে", "Submit Successful");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 3000);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function clearSelects(arrayIndex, startIndex) {
        const selectIds = [['packageId'], ['']];

        selectIds[arrayIndex].forEach((item) => {
            if (selectIds[arrayIndex].indexOf(item) >= startIndex) {
                $('#' + item).html('');
            }
        });

    }

    const fiscalYearChanged = async (selectedVal) => {


        clearNVendorTable();
        document.getElementById("vendorChildTableDiv").style.display = "none";

        const fiscalYearId = selectedVal.value;
        //clearSelects(0,0);
        if (fiscalYearId === '')

        <%--const url = 'Pi_app_requestServlet?actionType=getPackageByUnitIdAndFiscalYearId&fiscalYearId='--%>
        <%--		+fiscalYearId+'&unitId='+<%=officeUnitId%>;--%>

        <%--$.ajax({--%>
        <%--	url: url,--%>
        <%--	type: "GET",--%>
        <%--	async: false,--%>
        <%--	success: function (fetchedData) {--%>
        <%--		const response = JSON.parse(fetchedData);--%>
        <%--		if ( response && response.responseCode === 0) {--%>
        <%--			$('#toast_message').css('background-color', '#ff6063');--%>
        <%--			showToastSticky(response.msg, response.msg);--%>
        <%--		}--%>
        <%--		else if (response && response.responseCode === 200) {--%>
        <%--			document.getElementById('packageId').innerHTML = response.msg;--%>
        <%--		}--%>

        <%--	},--%>
        <%--	error: function (jqXHR, textStatus, errorThrown) {--%>
        <%--		toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus--%>
        <%--				+ ", Message: " + errorThrown);--%>
        <%--	}--%>
        <%--});--%>
    }

    function packageIdChanged(element){
        let packageId = element.value;
        let lotId = '';
        <%--if (isEditPage) lotId = '<%=pi_package_item_mapDTO.piLotFinalId%>';--%>
        <%--else lotId = document.getElementById('piLotFinalId').value;--%>
        let lotOptions = document.getElementById('piLotFinalId');
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let res = JSON.parse(this.response)
                let optionCount = res.msg.split("option").length - 1;
                if(optionCount < 3){
                    document.getElementById('piLotFinalId').value = -1;
                    lotIdChanged();
                }
                lotOptions.innerHTML = res.msg;
                console.log(this.responseText)
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Pi_package_finalServlet?actionType=getLotOptionsByPackage&packageId=" + packageId + "&piLotId=" + lotId, true);
        xhttp.send();
    }

    const lotIdChanged = async (selectedVal) => {
        let lotId = document.getElementById('piLotFinalId').value;
        clearNVendorTable();
        document.getElementById("vendorChildTableDiv").style.display = "none";

        const packageId = document.getElementById('piPackageFinalId').value;
        const fiscalYearId = document.getElementById('fiscalYearId').value;
        if (fiscalYearId === '' || packageId === '') return;

        const url = 'Pi_app_requestServlet?actionType=getItemByFiscalYearIdAndPackageIdAndLotId&fiscalYearId='
            + fiscalYearId + '&packageId=' + packageId + '&lotId=' + lotId;

        const response = await fetch(url);
        const productDetailsJson = await response.json();


        productDetailsJson.forEach((product, index) => {
            addDataInTable(product, index);
        });

        if (productDetailsJson.length > 0) {
            document.getElementById("vendorChildTableDiv").style.display = "block";
        } else {
            document.getElementById("vendorChildTableDiv").style.display = "none";
        }


    }

    function addDataInTable(jsonData, index) {

        let id = "productsAndVendorPriceTr_" + index;
        let content = '<tr class="productsAndVendorPriceTr" id=' + id + '>' +
            '<td class="productsAndVendorPriceTd">' + jsonData.productName + '<input type="hidden" name="submittedProductId" value=' + jsonData.iD + ' ></td>' +
            '</tr>';
        $("#NVendorProducts > tbody").append(content);
    }

    function clearNVendorTable() {
        document.querySelector('#NVendorProducts tbody').innerHTML = '';
        document.querySelector('#field-PiPackageVendorChildren').innerHTML = '';

        if (document.getElementsByClassName('.NVendorProductsTableTh')) {

            let NVendorProductsTableTh = document.getElementsByClassName("NVendorProductsTableTh");
            NVendorProductsTableTh = Array.prototype.slice
                .call(NVendorProductsTableTh);
            NVendorProductsTableTh.forEach((item, index) => {
                item.remove();
            });
        }

    }


    function PreprocessBeforeSubmiting(row, action) {
        preprocessTimeBeforeSubmitting('momentCodeTime', row);
        preprocessDateBeforeSubmitting('temderAdvertiseDate', row);
        preprocessTimeBeforeSubmitting('tenderAdvertiseTime', row);
        preprocessDateBeforeSubmitting('tenderOpeningDate', row);
        preprocessTimeBeforeSubmitting('tenderOpeningTime', row);
        preprocessDateBeforeSubmitting('noaDate', row);
        preprocessTimeBeforeSubmitting('noaTime', row);
        preprocessDateBeforeSubmitting('agreementDate', row);
        preprocessTimeBeforeSubmitting('agreementTime', row);
        preprocessDateBeforeSubmitting('agreementEndingDate', row);
        preprocessTimeBeforeSubmitting('agreementEndingTime', row);


        for (i = 1; i < child_table_extra_id; i++) {

            if (document.getElementById("isChosen_checkbox_" + i)) {
                if (document.getElementById("isChosen_checkbox_" + i).getAttribute("processed") == null) {
                    preprocessCheckBoxBeforeSubmitting('isChosen', i);
                    document.getElementById("isChosen_checkbox_" + i).setAttribute("processed", "1");
                }
            }
        }

        let checkboxes = document.getElementsByName('submittedVendorPriceCheckbox');
        let hiddenSubmittedCheckboxVal = document.getElementsByClassName("hidden_vendor_price_checkbox");


        checkboxes.forEach((item, index) => {

            if (item.checked == true) {
                document.getElementsByClassName("hidden_vendor_price_checkbox")[index].value = "1";
            } else {
                document.getElementsByClassName("hidden_vendor_price_checkbox")[index].value = "-1";
            }

        });

        let restrictDuplicateVendorArr = [];
        let selectedVendors = document.querySelectorAll('select[name="piPackageVendorChildren.name"]');
        selectedVendors.forEach(selectedVendor => {
            restrictDuplicateVendorArr = [...restrictDuplicateVendorArr, +selectedVendor.value];
        });
        if (restrictDuplicateVendorArr.length === 0) {
            showToastSticky("অনুগ্রহপূর্বক ভেন্ডর যোগ করুন", "Please add vendor");
            return false;
        }
        let hasDuplicate = restrictDuplicateVendorArr.some((value, index) => {
            return restrictDuplicateVendorArr.indexOf(value) !== index;
        });
        if (hasDuplicate) {
            showToastSticky("ডুপ্লিকেট ভেন্ডর", "Duplicate vendor");
            return false;
        }

        let winnerVendor = document.querySelectorAll("input[type=checkbox][name='piPackageVendorChildren.isChosen']:checked");
        if (winnerVendor.length !== 1) {
            showToastSticky("অনুগ্রহপূর্বক একজন বিজয়ী নির্বাচন করুন", "Please select only one winner");
            return false;
        }

        piPackageVendorForm.validate();
        return piPackageVendorForm.valid();
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_package_vendorServlet");
    }

    function init(row) {

        setTimeById('momentCodeTime_js_' + row, $('#momentCodeTime_time_' + row).val(), true);
        setDateByStringAndId('temderAdvertiseDate_js_' + row, $('#temderAdvertiseDate_date_' + row).val());
        setTimeById('tenderAdvertiseTime_js_' + row, $('#tenderAdvertiseTime_time_' + row).val(), true);
        setDateByStringAndId('tenderOpeningDate_js_' + row, $('#tenderOpeningDate_date_' + row).val());
        setTimeById('tenderOpeningTime_js_' + row, $('#tenderOpeningTime_time_' + row).val(), true);
        setDateByStringAndId('noaDate_js_' + row, $('#noaDate_date_' + row).val());
        setTimeById('noaTime_js_' + row, $('#noaTime_time_' + row).val(), true);
        setDateByStringAndId('agreementDate_js_' + row, $('#agreementDate_date_' + row).val());
        setTimeById('agreementTime_js_' + row, $('#agreementTime_time_' + row).val(), true);
        setDateByStringAndId('agreementEndingDate_js_' + row, $('#agreementEndingDate_date_' + row).val());
        setTimeById('agreementEndingTime_js_' + row, $('#agreementEndingTime_time_' + row).val(), true);

    }

    var row = 0;
    let selectedTenderAdvertiseCat;
    let selectedTenderAdvertiseCattArr;
    var language = '<%=Language%>';

    $(document).ready(function () {

        select2SingleSelector('#fiscalYearId', '<%=Language%>');
        select2SingleSelector('#packageId', '<%=Language%>');

        select2MultiSelector("#tenderAdvertiseCat", '<%=Language%>');
        <% if(actionName.equalsIgnoreCase("ajax_edit")){ %>

        selectedTenderAdvertiseCat = '<%=pi_package_vendorDTO.tenderAdvertiseCat%>';
        selectedTenderAdvertiseCattArr = selectedTenderAdvertiseCat.split(',').map(function (item) {
            return item.trim();
        });
        <% }%>
        fetchAdvertiseMedium(selectedTenderAdvertiseCattArr);

        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        fiscalYearChanged(document.getElementById('fiscalYearId'));

        piPackageVendorDocumentReady();
    });

    function validate() {
        let formSelector = $("#bigform");
        var validate = formSelector.validate();
        var valid = formSelector.valid();

        if (!valid) {
            formSelector.find(":input.error:first").focus();
        }

        return valid;

    }

    function piPackageVendorDocumentReady() {


        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {

                fiscalYearId: {
                    required: true,
                },
                packageId: {
                    required: true,
                },
            },

            messages: {
                fiscalYearId: "<%=LM.getText(LC.VM_TAX_TOKEN__PLEASE_SELECT_A_FISCAL_YEAR, loginDTO)%>",
                packageId: "<%=LM.getText(LC.PI_PACKAGE_VENDOR_EDIT_PACKAGEID, loginDTO)%>",
            }
        });
    }

    function fetchAdvertiseMedium(selectedTenderAdvertiseCattArr) {

        let url = "Pi_package_auctioneerServlet?actionType=getAllAdvertiseMedium";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#tenderAdvertiseCat').html("");
                let o;
                let str;

                $('#tenderAdvertiseCat').append(o);
                const response = JSON.parse(fetchedData);
                if (response && response.length > 0) {
                    for (let x in response) {
                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (selectedTenderAdvertiseCattArr && selectedTenderAdvertiseCattArr.length > 0) {
                            if (selectedTenderAdvertiseCattArr.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#tenderAdvertiseCat').append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-PiPackageVendorChildren").click(
        function (e) {
            e.preventDefault();

            var t = $("#template-PiPackageVendorChildren");

            $("#field-PiPackageVendorChildren").append(t.html());
            SetCheckBoxValues("field-PiPackageVendorChildren");

            var tr = $("#field-PiPackageVendorChildren").find("tr:last-child");

            tr.attr("id", "PiPackageVendorChildren_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
            });
            initializeSingleSelector(child_table_extra_id);
            addVendorPriceTd(child_table_extra_id);

            child_table_extra_id++;

        });


    $("#remove-PiPackageVendorChildren").click(function (e) {


        var tablename = 'field-PiPackageVendorChildren';
        var i = 0;

        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {

                    tr.remove();
                    let deleteHeaderAndData = tr.querySelector('select[name="piPackageVendorChildren.name"]').id.split("_")[2];
                    $("#vendor_header_" + deleteHeaderAndData).remove();
                    $(".vendor_data_" + deleteHeaderAndData).remove();
                }
                j++;
            }

        }
    });


    let prevSelectedItem;
    let initializeSingleSelector = (child_table_extra_id) => {
        select2SingleSelector("#name_text_" + child_table_extra_id, '<%=Language%>');
        fetchVendors(prevSelectedItem, child_table_extra_id);
    }

    function fetchVendors(prevSelectedItem, child_table_extra_id) {

        let url = "Pi_vendor_auctioneer_detailsServlet?actionType=getVendorOrAuctioneer&venAucType=1";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#name_text_' + child_table_extra_id).html("");
                let o;
                let str;
                if (language === 'English') {
                    str = 'Select';
                    o = new Option('Select', '-1');
                } else {
                    o = new Option('বাছাই করুন', '-1');
                    str = 'বাছাই করুন';
                }
                $(o).html(str);
                $('#name_text_' + child_table_extra_id).append(o);

                const response = JSON.parse(fetchedData);

                if (response && response.length > 0) {
                    for (let x in response) {

                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (prevSelectedItem && prevSelectedItem.length > 0) {
                            if (prevSelectedItem.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#name_text_' + child_table_extra_id).append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }


    var addVendorPriceTd = (child_table_extra_id) => {

        let dynamicTrHeaderContent = '<th class="NVendorProductsTableTh" id="vendor_header_' + child_table_extra_id + '"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VENDORID, loginDTO)%> ' + convertBNToEN(child_table_extra_id, '<%=Language%>') + ' <%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%></th>';
        $("#dynamicTrHeader").append(dynamicTrHeaderContent);

        let products = document.getElementsByClassName("productsAndVendorPriceTr");

        let onlyProductsVal = Array.prototype.slice
            .call(products)
            .map((product) => (product.id));

        let content = '<td class="vendor_data_' + child_table_extra_id + '"><input type="number" step="0.01" id="vendor_data_' + child_table_extra_id + '" name="submittedVendorPrice" class="form-control" tag="pb_html"/></td>';
        let checkBoxes = '<td class="vendor_price_checkbox_td"> <input type="checkbox" class="form-control-sm vendor_price_checkbox" name="submittedVendorPriceCheckbox" id="vendor_price_checkbox' + child_table_extra_id + '" value="true" tag="pb_html"> </td>';

        let hiddenCheckBoxes = '<td style="display: none;" class="hidden_vendor_price_checkbox_td"> <input type="hidden" class="form-control-sm hidden_vendor_price_checkbox" name="hiddenSubmittedVendorPriceCheckbox" id="hidden_vendor_price_checkbox' + child_table_extra_id + '" value="-1" tag="pb_html"> </td>';

        $(".vendor_price_checkbox_td").remove();
        $("#vendor_price_checkbox_th").remove();

        $(".hidden_vendor_price_checkbox_td").remove();


        onlyProductsVal.forEach((item, index) => {
            $('#' + item).append(content);
            $('#' + item).append(checkBoxes);
            $('#' + item).append(hiddenCheckBoxes);
        });
        let checkBoxHeader = '<th id="vendor_price_checkbox_th"><%=LM.getText(LC.VM_TAX_TOKEN_GIVEN, loginDTO)%></th>';
        $("#dynamicTrHeader").append(checkBoxHeader);
    }


    function onlyOne(checkbox) {
        var checkboxes = document.getElementsByName('piPackageVendorChildren.isChosen');

        checkboxes.forEach((item, index) => {
            if (item !== checkbox) {
                item.checked = false;
                document.getElementsByClassName("isChosenVal_hidden_input")[index].value = "-1";
            } else {
                document.getElementsByClassName("isChosenVal_hidden_input")[index].value = "1";
            }
        });
    }

    var numbers = {
        0: "\u09E6",
        1: "\u09E7",
        2: "\u09E8",
        3: "\u09E9",
        4: "\u09EA",
        5: "\u09EB",
        6: "\u09EC",
        7: "\u09ED",
        8: "\u09EE",
        9: "\u09EF"
    };

    function convertBNToEN(input, language) {

        if (language === "English") {
            return input;
        }
        let output = [];
        input = input.toString(10).split('').map(Number);
        for (let i = 0; i < input.length; ++i) {
            if (numbers.hasOwnProperty(input[i])) {
                output.push(numbers[input[i]]);
            } else {
                output.push(input[i]);
            }
        }
        return output.join('');
    }

    const vendorChanged = async (selectedVal) => {
        const vendorAuctioneerId = selectedVal.value;

        let index = selectedVal.id.split("_")[2];
        let actual_index = +index - 1;
        let address_id = 'address_geoTextField_' + index;
        let mobile_id = 'mobile_text_' + index;

        document.getElementById(address_id).value = '';
        document.getElementById(mobile_id).value = '';
        document.getElementById('vendor_header_' + index).innerText = '<%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VENDORID, loginDTO)%> ' + convertBNToEN(index, '<%=Language%>') + ' <%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%>';

        if (vendorAuctioneerId === '') return;

        document.getElementById('vendor_header_' + index).innerText = selectedVal.options[selectedVal.selectedIndex].text + ' ' + '<%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%>';

        const url = 'Pi_vendor_auctioneer_detailsServlet?actionType=getAddressAndMobile&vendorAuctioneerId='
            + vendorAuctioneerId;

        const response = await fetch(url);
        const vendorDetailsJson = await response.json();

        document.getElementById(address_id).value = vendorDetailsJson.address;
        document.getElementById(mobile_id).value = vendorDetailsJson.mobile;

    }


</script>






