<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="other_office.*"%>
<%@ page import="pb.*" %>
<%@page contentType="text/html;charset=utf-8" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.OTHER_OFFICE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
	
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='nameEng' id = 'nameEng' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='nameBng' id = 'nameBng' value=""/>							
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control' type ="text" onkeyup = "setUserName(this.value)"  name='usernameTxt' id = 'usernameTxt' value=""/>
					<input class='form-control' type ="hidden"  name='username' id = 'username' value=""/>						
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div" >
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_PHONE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					
					<div class="input-group mb-2">
						<div class="input-group-prepend">
							<div class="input-group-text"><%=Language.equalsIgnoreCase("english") ? "+88" : "+৮৮"%></div>
						</div>
						<input type='text' class='form-control' 
							name='phone' id='phone' 
							value=''
							tag='pb_html'>
						<input type = "hidden" id = "phoneNo" name = "phoneNo" value = "" />
					</div>							
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=Language.equalsIgnoreCase("english")?"NID":"জাতীয় পরিচয়পত্র"%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='nid' id = 'nid' value=""/>							
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.OTHER_OFFICE_REPORT_WHERE_CURRENTOFFICE_4, loginDTO)%>
				</label>
				<div class="col-sm-9">
					
					<select class='form-control' name='currentOffice_4' onchange = "getDept(this.value)"
                            id='currentOffice_4' tag='pb_html'>
                        <%=Other_officeRepository.getInstance().buildOptions(Language, null)%>
                    </select>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" id = "otherOfficeDeptDiv" style = "display:none">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>
				</label>
				<div class="col-sm-9">
					
					<input class='form-control'  name='nid' id = 'nid' value=""/>								
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=Language.equalsIgnoreCase("english")?"Show Photo":"ছবি দেখুন"%>
				</label>
				<div class="col-sm-9">
					
					<input class='form-control-sm'  name='showPic' id = 'showPic' value="false" onchange='setShowPic()' type="checkbox"/>							
				</div>
			</div>
		</div>
		
		
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
    showFooter = false;
}
function PreprocessBeforeSubmiting()
{
	if($("#phone").val() != "")
	{
       convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($('#phone').val(), '<%=Language%>');
       $("#phoneNo").val(convertedPhoneNumber);
	}
}
function getDept(value)
{
	console.log("getDept = " + value);
	if(value != -1 && value != '')
	{
		$("#otherOfficeDeptDiv").removeAttr("style");
		fillSelect("department", "Other_office_departments_listServlet?actionType=getDeptReport&otherOfficeId=" + value);
	}
	else
	{
		$("#otherOfficeDeptDiv").css("display", "none");
	}
}

var isVisible = [true, true, true, true, true, true, true, true, true, false, true];
function setShowPic()
{
	if($("#showPic").prop("checked"))
	{
		$("#showPic").val("true");
		isVisible[9] = true;
	}
	else
	{
		$("#showPic").val("false");
		isVisible[9] = false;
	}
}

function setUserName(value)
{
	$("#username").val(convertToEnglishNumber(value));
}
</script>