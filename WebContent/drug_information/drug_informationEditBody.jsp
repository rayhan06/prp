<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="drug_information.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
	Drug_informationDTO drug_informationDTO;
drug_informationDTO = (Drug_informationDTO) request.getAttribute("drug_informationDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if (drug_informationDTO == null) {
	drug_informationDTO = new Drug_informationDTO();

}
Drug_configurationDAO drug_configurationDAO = new Drug_configurationDAO();
System.out.println("drug_informationDTO = " + drug_informationDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
	actionName = "add";
} else {
	actionName = "edit";
}
String formTitle = LM.getText(LC.DRUG_INFORMATION_ADD_DRUG_INFORMATION_ADD_FORMNAME, loginDTO);
String servletName = "Drug_informationServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if (ID == null || ID.isEmpty()) {
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
String Language = LM.getText(LC.DRUG_INFORMATION_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
	<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid"
	id="kt_content" style="padding: 0px !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					<%=formTitle%>
				</h3>
			</div>
		</div>
		<form class="form-horizontal"
			action="Drug_informationServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
			id="bigform" name="bigform" method="POST"
			enctype="multipart/form-data"
			onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="kt-portlet__body form-body">
				<div class="row">
					<div class="col-md-8 offset-md-2">
						<div class="onlyborder">
							<div class="row">
								<div class="col-md-10 offset-md-1">
									<div class="row mx-2 mx-md-0">
										<div class="col-lg-12">
											<div class="sub_title_top">
												<div class="sub_title">
													<h4 style="background: white"><%=formTitle%>
													</h4>
												</div>
											</div>
											<input type='hidden' class='form-control' name='iD'
												id='iD_hidden_<%=i%>' value='<%=drug_informationDTO.iD%>'
												tag='pb_html' />

											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DRUG_INFORMATION_ADD_NAMEEN, loginDTO)%>
												</label>
												<div class="col-md-8">
													<input type='text' class='form-control' name='nameEn'
														id='nameEn_text_<%=i%>'
														value='<%=drug_informationDTO.nameEn%>' tag='pb_html' />
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DRUG_INFORMATION_ADD_NAMEBN, loginDTO)%>
												</label>
												<div class="col-md-8">
													<input type='text' class='form-control' name='nameBn'
														id='nameBn_text_<%=i%>'
														value='<%=drug_informationDTO.nameBn%>' tag='pb_html' />
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DRUG_INFORMATION_ADD_MEDICINEGENERICNAMETYPE, loginDTO)%>
												</label>
												<div class="col-md-8">
													<select class='form-control' name='medicineGenericNameType'
														id='medicineGenericNameType_select_<%=i%>' tag='pb_html'>
														<%
															Options = CommonDAO.getOptions(Language, "medicine_generic_name", drug_informationDTO.medicineGenericNameType);
														%>
														<%=Options%>
													</select>

												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DRUG_INFORMATION_ADD_PHARMACOMPANYNAMETYPE, loginDTO)%>
												</label>
												<div class="col-md-8">
													<select class='form-control' name='pharmaCompanyNameType'
														id='pharmaCompanyNameType_select_<%=i%>' tag='pb_html'>
														<%
															Options = CommonDAO.getOptions(Language, "pharma_company_name", drug_informationDTO.pharmaCompanyNameType);
														%>
														<%=Options%>
													</select>

												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DRUG_INFORMATION_ADD_DRUGFORMCAT, loginDTO)%>
												</label>
												<div class="col-md-8">
													<select class='form-control' name='drugFormCat'
														id='drugFormCat_category_<%=i%>' tag='pb_html'>
														<%
															Options = CatDAO.getOptions(Language, "drug_form", drug_informationDTO.drugFormCat);
														%>
														<%=Options%>
													</select>

												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DRUG_INFORMATION_ADD_STRENGTH, loginDTO)%>
												</label>
												<div class="col-md-8">
													<input type='text' class='form-control' name='strength'
														id='strength_text_<%=i%>'
														value='<%=drug_informationDTO.strength%>' tag='pb_html' />
												</div>
											</div>
											
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right"><%=Language.equalsIgnoreCase("english")?"Unit Price":"একক মূল্য"%>
												</label>
												<div class="col-md-8">
													<input type='number' step=".01" class='form-control' name='unitPrice' min="0"
														id='unitPrice_text_<%=i%>'
														value='<%=drug_informationDTO.unitPrice%>' tag='pb_html' />
												</div>
											</div>
											<input type='hidden' class='form-control' name='currentStock'
												id='currentStock_number_<%=i%>' value='<%=value%>'
												tag='pb_html'>

											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.DRUG_INFORMATION_ADD_MINIMULLEVELFORALERT, loginDTO)%>
												</label>
												<div class="col-md-8">
													<%
														value = "";
													if (drug_informationDTO.minimulLevelForAlert != -1) {
														value = drug_informationDTO.minimulLevelForAlert + "";
													}
													%>
													<input type='number' class='form-control'
														name='minimulLevelForAlert'
														id='minimulLevelForAlert_number_<%=i%>' value='<%=value%>'
														tag='pb_html'>
												</div>
											</div>



										</div>
									</div>
								</div>
							</div>

						</div>


					</div>

				</div>

				<div class="form-group mt-5">
					
					<div class="table-responsive">
						<h3>
							<%=Language.equalsIgnoreCase("english")?"Reserved For the Following Employees:":"নিচের ব্যক্তিদের জন্য সংরক্ষিত"%>
						</h3>
						<h6>
							*<%=Language.equalsIgnoreCase("english")?"Select none to keep the drug available for everyone.":"ঔষধটি সবার জন্য উন্মুক্ত করতে হলে কাউকে বাছাই করবেন না।"%>
							
						</h6>
					
						<table class="table table-bordered">
							<thead>
								<tr>
									<th><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%></th>
									<th><i class="fa fa-trash"></i></th>
								</tr>
							</thead>
							<tbody id="field-DrugConfiguration">


								<%
									if (actionName.equals("edit")) {
									int index = -1;

									List<Drug_configurationDTO> drugConfigurationDTOs = drug_configurationDAO
									.getDTOsByDrugInfoId(drug_informationDTO.iD);
									if (drugConfigurationDTOs == null) {
										drugConfigurationDTOs = new ArrayList<Drug_configurationDTO>();
									}
									for (Drug_configurationDTO drugConfigurationDTO : drugConfigurationDTOs) 
									{
										index++;

										System.out.println("index index = " + index);
								%>

								<tr id="DrugConfiguration_<%=index + 1%>">
									<td style="display: none;"><input type='hidden'
										class='form-control' name='drugConfiguration.iD'
										id='iD_hidden_<%=childTableStartingID%>'
										value='<%=drugConfigurationDTO.iD%>' tag='pb_html' /></td>
									<td style="display: none;"><input type='hidden'
										class='form-control'
										name='drugConfiguration.drugInformationId'
										id='drugInformationId_hidden_<%=childTableStartingID%>'
										value='<%=drugConfigurationDTO.drugInformationId%>'
										tag='pb_html' /></td>
									<td>




										<table class="table table-bordered table-striped">
											<tbody id="organogramId_table_<%=childTableStartingID%>"
												tag='pb_html'>
												<%
													if (drugConfigurationDTO.organogramId != -1) {
												%>
												<tr>
													<td style="width: 20%"><%=WorkflowController.getUserNameFromOrganogramId(drugConfigurationDTO.organogramId)%></td>
													<td style="width: 40%"><%=WorkflowController.getNameFromOrganogramId(drugConfigurationDTO.organogramId, Language)%></td>
													<td><%=WorkflowController.getOrganogramName(drugConfigurationDTO.organogramId, Language)%>,
														<%=WorkflowController.getUnitNameFromOrganogramId(drugConfigurationDTO.organogramId, Language)%></td>
												</tr>
												<%
													}
												%>
											</tbody>
										</table> 
										<input type='hidden' class='form-control'
										name='drugConfiguration.organogramId'
										id='organogramId_hidden_<%=childTableStartingID%>'
										value='<%=drugConfigurationDTO.organogramId%>' tag='pb_html' />

									</td>
									<td>
										<span id='chkEdit'> 
											<input type='checkbox'
											name='checkbox' value='' deletecb='true'
											class="form-control-sm" />
										</span>
									</td>
								</tr>
								<%
									childTableStartingID++;
								}
								}
								%>

							</tbody>
						</table>
					</div>
					<div class="form-group">
						<div class="col-xs-9 text-right">
							<button id="add-more-DrugConfiguration"
								name="add-moreDrugConfiguration" type="button"
								onclick="childAdded(event, 'DrugConfiguration')"
								class="btn btn-sm text-white add-btn shadow">
								<i class="fa fa-plus"></i>
								<%=LM.getText(LC.HM_ADD, loginDTO)%>
							</button>
							<button id="remove-DrugConfiguration"
								name="removeDrugConfiguration" type="button"
								onclick="childRemoved(event, 'DrugConfiguration')"
								class="btn btn-sm remove-btn shadow ml-2 pl-4">
								<i class="fa fa-trash"></i>
							</button>
						</div>
					</div>

					<%
						Drug_configurationDTO drugConfigurationDTO = new Drug_configurationDTO();
					%>

					<template id="template-DrugConfiguration">
						<tr>
							<td style="display: none;"><input type='hidden'
								class='form-control' name='drugConfiguration.iD' id='iD_hidden_'
								value='<%=drugConfigurationDTO.iD%>' tag='pb_html' /></td>
							<td style="display: none;"><input type='hidden'
								class='form-control' name='drugConfiguration.drugInformationId'
								id='drugInformationId_hidden_'
								value='<%=drugConfigurationDTO.drugInformationId%>'
								tag='pb_html' /></td>
							<td>

								<table class="table table-bordered table-striped">
									<tbody id="organogramId_table_" tag='pb_html'>
										<%
											if (drugConfigurationDTO.organogramId != -1) {
										%>
										<tr>
											<td style="width: 20%"><%=WorkflowController.getUserNameFromOrganogramId(drugConfigurationDTO.organogramId)%></td>
											<td style="width: 40%"><%=WorkflowController.getNameFromOrganogramId(drugConfigurationDTO.organogramId, Language)%></td>
											<td><%=WorkflowController.getOrganogramName(drugConfigurationDTO.organogramId, Language)%>,
												<%=WorkflowController.getUnitNameFromOrganogramId(drugConfigurationDTO.organogramId, Language)%></td>
										</tr>
										<%
											}
										%>
									</tbody>
								</table> <input type='hidden' class='form-control'
								name='drugConfiguration.organogramId' id='organogramId_hidden_'
								value='<%=drugConfigurationDTO.organogramId%>' tag='pb_html' />

							</td>
							<td><span id='chkEdit'> <input type='checkbox'
									name='checkbox' value='' deletecb='true'
									class="form-control-sm" />
							</span></td>
						</tr>

					</template>

				</div>

				<div class="d-flex justify-content-center">
					<button id="cancel-btn"
						class="btn-sm shadow text-white border-0 cancel-btn">
						<%=LM.getText(LC.DRUG_INFORMATION_ADD_DRUG_INFORMATION_CANCEL_BUTTON, loginDTO)%>
					</button>
					<button class="btn-sm shadow text-white border-0 submit-btn ml-2"
						type="submit">
						<%=LM.getText(LC.DRUG_INFORMATION_ADD_DRUG_INFORMATION_SUBMIT_BUTTON, loginDTO)%>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Drug_informationServlet");
    }

    function init(row) {

	
	}

var row = 0;
$(document).ready(function(){
	init(row);

        $("#medicineGenericNameType_select_<%=i%>").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });
        
        $("#pharmaCompanyNameType_select_<%=i%>").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });
        
        $("#drugFormCat_category_<%=i%>").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });
        
        

        $(".cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function processRowsWhileAdding(childName)
{
	if(childName == "DrugConfiguration")
	{			
			addEmployeeWithRow("organogramId_button_" + child_table_extra_id);
	}
}

</script>






