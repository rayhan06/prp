<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="drug_information.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    String servletName = "Drug_informationServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.DRUG_INFORMATION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Drug_informationDAO drug_informationDAO = new Drug_informationDAO("drug_information");
    Drug_informationDTO drug_informationDTO = drug_informationDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.DRUG_INFORMATION_ADD_DRUG_INFORMATION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.DRUG_INFORMATION_ADD_DRUG_INFORMATION_ADD_FORMNAME, loginDTO)%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DRUG_INFORMATION_ADD_NAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = drug_informationDTO.nameEn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DRUG_INFORMATION_ADD_NAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = drug_informationDTO.nameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.DRUG_INFORMATION_ADD_MEDICINEGENERICNAMETYPE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = drug_informationDTO.medicineGenericNameType + "";
                            %>
                            <%
                                value = CommonDAO.getName(Integer.parseInt(value), "medicine_generic_name", Language.equals("English") ? "name_en" : "name_bn", "id");
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.DRUG_INFORMATION_ADD_PHARMACOMPANYNAMETYPE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = drug_informationDTO.pharmaCompanyNameType + "";
                            %>
                            <%
                                value = CommonDAO.getName(Integer.parseInt(value), "pharma_company_name", Language.equals("English") ? "name_en" : "name_bn", "id");
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DRUG_INFORMATION_ADD_DRUGFORMCAT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = drug_informationDTO.drugFormCat + "";
                            %>
                            <%
                                value = CatDAO.getName(Language, "drug_form", drug_informationDTO.drugFormCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DRUG_INFORMATION_ADD_STRENGTH, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = drug_informationDTO.strength + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DRUG_INFORMATION_ADD_CURRENTSTOCK, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = drug_informationDTO.currentStock + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.DRUG_INFORMATION_ADD_MINIMULLEVELFORALERT, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = drug_informationDTO.minimulLevelForAlert + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>
        	<%
        	if(drug_informationDTO.orgs != null)
        	{
        		%>
	        	<h5 class="table-title">
	                <%=Language.equalsIgnoreCase("english")?"Reserved for:":"নিম্নোক্ত ব্যক্তিদের জন্য সংরক্ষিত"%>
	            </h5>
	            <div class="table-responsive">
	                <table class="table table-bordered table-striped text-nowrap">
	                <%
	                boolean isLangEng = Language.equalsIgnoreCase("english");
	                for(long orgId: drug_informationDTO.orgs)
	                {
	                	%>
	                	<tr>
	                		<td><%=WorkflowController.getNameFromOrganogramId(orgId, isLangEng) %></td>
	                		<td><%=WorkflowController.getOrganogramName(orgId, isLangEng) %></td>
	                		<td><%=WorkflowController.getOfficeNameFromOrganogramId(orgId, isLangEng) %></td>
	                	</tr>
	                	<%
	                }
	                %>
	                </table>
	            </div>
        		<%
        	}
        	%>
        </div>
    </div>
</div>