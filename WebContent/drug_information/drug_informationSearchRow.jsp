<%@page pageEncoding="UTF-8" %>

<%@page import="drug_information.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.DRUG_INFORMATION_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_DRUG_INFORMATION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Drug_informationDTO drug_informationDTO = (Drug_informationDTO) request.getAttribute("drug_informationDTO");
    CommonDTO commonDTO = drug_informationDTO;
    String servletName = "Drug_informationServlet";


    System.out.println("drug_informationDTO = " + drug_informationDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Drug_informationDAO drug_informationDAO = (Drug_informationDAO) request.getAttribute("drug_informationDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    
    String color = "black";
    if(drug_informationDTO.currentStock == 0 )
    {
    	color = "brown";
    }
    else if(drug_informationDTO.currentStock <  drug_informationDTO.minimulLevelForAlert)
    {
    	color = "orange";
    }

%>

<td id='<%=i%>_nameEn' class="" style="color:<%=color%>">
    <%
        value = drug_informationDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>



<td id='<%=i%>_medicineGenericNameType' class="" style="color:<%=color%>">
    <%
        value = drug_informationDTO.medicineGenericNameType + "";
    %>
    <%
        value = CommonDAO.getName(Integer.parseInt(value), "medicine_generic_name", Language.equals("English") ? "name_en" : "name_bn", "id");
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_pharmaCompanyNameType' class="" style="color:<%=color%>">
    <%
        value = drug_informationDTO.pharmaCompanyNameType + "";
    %>
    <%
        value = CommonDAO.getName(Integer.parseInt(value), "pharma_company_name", Language.equals("English") ? "name_en" : "name_bn", "id");
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_drugFormCat' class="" style="color:<%=color%>">
    <%
        value = drug_informationDTO.drugFormCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "drug_form", drug_informationDTO.drugFormCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_currentStock' class="" style="color:<%=color%>">
    <%
        value = drug_informationDTO.currentStock + "";
    %>

    <%=Utils.getDigits(value, Language)%>
   


</td>

<td  class="" style="color:<%=color%>">
	<table>
		<tr>
		<td style = "width:12rem" id = "tdStock_<%=drug_informationDTO.iD%>">
			<div id = "stockStatus_<%=drug_informationDTO.iD%>">
			    <%
			        value = drug_informationDTO.availableStock + "";
			    %>
			
			    <%=Utils.getDigits(value, Language)%>
			    <%=(drug_informationDTO.iD == Drug_informationDTO.ACCU_CHECK_ID) ? " (" + Utils.getDigits(drug_informationDTO.detailedCount, Language) + ")" : "" %>
		    </div>
	    </td>
	    
	    <%
	    if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
	    {
	    	%>
	    	<td id = "tdEdit_<%=drug_informationDTO.iD%>">
		    	<button type="button"
		                type="button" id = "makeAvailable_<%=drug_informationDTO.iD%>"
		                class="btn-sm border-0 shadow bg-light btn-border-radius"
		                style="color: #ff6b6b;"
		                onclick="editAvailability(<%=drug_informationDTO.iD%>)">
		            <i class="fa fa-edit"></i>
		        </button>
		        
		         
		         	<div id = "divStatus_<%=drug_informationDTO.iD%>" style="display:none" onsubmit="destroySearchForm()">
		         		<table>
			         		<tr>
			         			<td>
				         			<input type = "number" name = "availableStock" id = "availableStock_<%=drug_informationDTO.iD%>" class = "form-control" 
				         			value = "<%=drug_informationDTO.availableStock%>" min = "0" max = "<%=drug_informationDTO.currentStock%>" style = "width:12rem" />
				         			
				         			<input type = "hidden" name = "currentStock" id = "currentStock_<%=drug_informationDTO.iD%>"
				         			value = "<%=drug_informationDTO.currentStock%>"  />
				         		</td>
				         		<td>
						         	<button 
						                type="button" id = "makeAvailableButton_<%=drug_informationDTO.iD%>"
						                class="btn-sm border-0 shadow bg-light btn-border-radius"
						                onclick = "makeAvailable(<%=drug_informationDTO.iD%>)"
						                style="color: #ff6b6b;">
						            	<i class="fa fa-check"></i>
						        	</button>
						        </td>
				        	</tr>
			        	</table>
		        	</div>
		         
	         </td>
	        <%
	    }
	    %>
	    </tr>
    </table>
    
   
    


</td>

<td id='<%=i%>_minimulLevelForAlert' class="" style="color:<%=color%>">
    <%
        value = drug_informationDTO.minimulLevelForAlert + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_minimulLevelForAlert' class="" style="color:<%=color%>">


    <%=Utils.getDigits(drug_informationDTO.unitPrice, Language)%>


</td>

<td id='<%=i%>_minimulLevelForAlert' class="" style="color:<%=color%>">


    <%=Utils.getDigits(drug_informationDTO.unitPrice * drug_informationDTO.currentStock, Language)%>


</td>


<td>
    <button type="button"
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Drug_informationServlet?actionType=view&ID=<%=drug_informationDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Drug_informationServlet?actionType=getEditPage&ID=<%=drug_informationDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=drug_informationDTO.iD%>'/>
        </span>
    </div>
</td>
																						
											

