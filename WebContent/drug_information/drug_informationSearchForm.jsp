<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="drug_information.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.DRUG_INFORMATION_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Drug_informationDAO drug_informationDAO = (Drug_informationDAO) request.getAttribute("drug_informationDAO");


    String navigator2 = SessionConstants.NAV_DRUG_INFORMATION;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>



<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
        	<th><%=LM.getText(LC.HM_SL, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.HM_NAME, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.DRUG_INFORMATION_ADD_MEDICINEGENERICNAMETYPE, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.DRUG_INFORMATION_ADD_PHARMACOMPANYNAMETYPE, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.DRUG_INFORMATION_ADD_DRUGFORMCAT, loginDTO)%>
            </th>

            <th class="text-nowrap"><%=LM.getText(LC.DRUG_INFORMATION_ADD_CURRENTSTOCK, loginDTO)%>
            </th>
            
            <th class="text-nowrap"><%=Language.equalsIgnoreCase("english")?"Available Stock":"ব্যবহার্য স্টক"%>
            </th>
            
            <th class="text-nowrap"><%=LM.getText(LC.DRUG_INFORMATION_ADD_MINIMULLEVELFORALERT, loginDTO)%>
            </th>
            
             <th class="text-nowrap"><%=Language.equalsIgnoreCase("english")?"Unit Price":"একক মূল্য"%>
            </th>
            
             <th class="text-nowrap"><%=Language.equalsIgnoreCase("english")?"Total Price":"মোট মূল্য"%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th class="text-nowrap"><%=LM.getText(LC.DRUG_INFORMATION_SEARCH_DRUG_INFORMATION_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
				<div class="text-center">
					<span>All</span>
				</div>
				<div class="d-flex align-items-center justify-content-between mt-3">
					<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
						<i class="fa fa-trash"></i>
					</button>
					<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
				</div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_DRUG_INFORMATION);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Drug_informationDTO drug_informationDTO = (Drug_informationDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>
			<td>
				<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>
			</td>

            <%
                request.setAttribute("drug_informationDTO", drug_informationDTO);
            %>

            <jsp:include page="./drug_informationSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			