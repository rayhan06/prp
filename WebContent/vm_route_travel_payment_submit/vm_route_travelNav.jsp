<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%
	System.out.println("Inside nav.jsp");
	String url = request.getParameter("url");
	String navigator = request.getParameter("navigator");
	String pageName = request.getParameter("pageName");
	if (pageName == null)
		pageName = "Search";
	String pageno = "";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
	pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

	System.out.println("rn " + rn);

	String action = url;
	String context = "../../.." + request.getContextPath() + "/";
	String link = context + url;
	String concat = "?";
	if (url.contains("?")) {
		concat = "&";
	}
	String[][] searchFieldInfo = rn.getSearchFieldInfo();
	String totalPage = "1";
	if (rn != null)
		totalPage = rn.getTotalPages() + "";
	int row = 0;

	String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	int pagination_number = 0;
	boolean isPermanentTable = rn.m_isPermanentTable;
	System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>





<%--<%@include file="../common/pagination_with_go2.jsp"%>--%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>


<script type="text/javascript">

	$(document).ready(function(){
		$("#status_cat option[value='1']").remove();
		$("#status_cat option[value='13']").remove();
		$("#status_cat option[value='14']").remove();
	});

	function setSearchChanged()
	{
		searchChanged = 1;
	}

	function dosubmit(params)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		//alert(params);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200)
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };

		  xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
		  xhttp.send();

	}

	function allfield_changed(go, pagination_number)
	{
		var params = 'AnyField=' + document.getElementById('anyfield').value;

		// $("#insertion_date_start").val(getDateStringById('insertion_date_start_js', 'DD/MM/YYYY'));
		// params +=  '&insertion_date_start='+ getBDFormattedDate('insertion_date_start');
		// $("#insertion_date_end").val(getDateStringById('insertion_date_end_js', 'DD/MM/YYYY'));
		// params +=  '&insertion_date_end='+ getBDFormattedDateWithOneDayAddition('insertion_date_end');
		// params +=  '&requested_stoppage_name='+ $('#requested_stoppage_name').val();
		// params +=  '&remarks='+ $('#remarks').val();
		// $("#requested_start_date_start").val(getDateStringById('requested_start_date_start_js', 'DD/MM/YYYY'));
		// params +=  '&requested_start_date_start='+ getBDFormattedDate('requested_start_date_start');
		// $("#requested_start_date_end").val(getDateStringById('requested_start_date_end_js', 'DD/MM/YYYY'));
		// params +=  '&requested_start_date_end='+ getBDFormattedDate('requested_start_date_end');
		// params +=  '&requester_phone_num='+ $('#requester_phone_num').val();
		// params +=  '&requester_name_en='+ $('#requester_name_en').val();
		// params +=  '&requester_name_bn='+ $('#requester_name_bn').val();
		// params +=  '&requester_office_name_en='+ $('#requester_office_name_en').val();
		// params +=  '&requester_office_name_bn='+ $('#requester_office_name_bn').val();
		// params +=  '&requester_office_unit_name_en='+ $('#requester_office_unit_name_en').val();
		// params +=  '&requester_office_unit_name_bn='+ $('#requester_office_unit_name_bn').val();
		// params +=  '&requester_office_unit_org_name_en='+ $('#requester_office_unit_org_name_en').val();
		// params +=  '&requester_office_unit_org_name_bn='+ $('#requester_office_unit_org_name_bn').val();
		// $("#approved_start_date_start").val(getDateStringById('approved_start_date_start_js', 'DD/MM/YYYY'));
		// params +=  '&approved_start_date_start='+ getBDFormattedDate('approved_start_date_start');
		// $("#approved_start_date_end").val(getDateStringById('approved_start_date_end_js', 'DD/MM/YYYY'));
		// params +=  '&approved_start_date_end='+ getBDFormattedDate('approved_start_date_end');
		// params +=  '&decision_remarks='+ $('#decision_remarks').val();
		// $("#approved_date_start").val(getDateStringById('approved_date_start_js', 'DD/MM/YYYY'));
		// params +=  '&approved_date_start='+ getBDFormattedDate('approved_date_start');
		// $("#approved_date_end").val(getDateStringById('approved_date_end_js', 'DD/MM/YYYY'));
		// params +=  '&approved_date_end='+ getBDFormattedDate('approved_date_end');
		// $("#withdrawal_date_start").val(getDateStringById('withdrawal_date_start_js', 'DD/MM/YYYY'));
		// params +=  '&withdrawal_date_start='+ getBDFormattedDate('withdrawal_date_start');
		// $("#withdrawal_date_end").val(getDateStringById('withdrawal_date_end_js', 'DD/MM/YYYY'));
		// params +=  '&withdrawal_date_end='+ getBDFormattedDate('withdrawal_date_end');
		// params +=  '&approver_phone_num='+ $('#approver_phone_num').val();
		// params +=  '&approver_name_en='+ $('#approver_name_en').val();
		// params +=  '&approver_name_bn='+ $('#approver_name_bn').val();
		// params +=  '&approver_office_name_en='+ $('#approver_office_name_en').val();
		// params +=  '&approver_office_name_bn='+ $('#approver_office_name_bn').val();
		// params +=  '&approver_office_unit_name_en='+ $('#approver_office_unit_name_en').val();
		// params +=  '&approver_office_unit_name_bn='+ $('#approver_office_unit_name_bn').val();
		// params +=  '&approver_office_unit_org_name_en='+ $('#approver_office_unit_org_name_en').val();
		// params +=  '&approver_office_unit_org_name_bn='+ $('#approver_office_unit_org_name_bn').val();
		// params +=  '&withdrawer_phone_num='+ $('#withdrawer_phone_num').val();
		// params +=  '&withdrawer_name_en='+ $('#withdrawer_name_en').val();
		// params +=  '&withdrawer_name_bn='+ $('#withdrawer_name_bn').val();
		// params +=  '&withdrawer_office_name_en='+ $('#withdrawer_office_name_en').val();
		// params +=  '&withdrawer_office_name_bn='+ $('#withdrawer_office_name_bn').val();
		// params +=  '&withdrawer_office_unit_name_en='+ $('#withdrawer_office_unit_name_en').val();
		// params +=  '&withdrawer_office_unit_name_bn='+ $('#withdrawer_office_unit_name_bn').val();
		// params +=  '&withdrawer_office_unit_org_name_en='+ $('#withdrawer_office_unit_org_name_en').val();
		// params +=  '&withdrawer_office_unit_org_name_bn='+ $('#withdrawer_office_unit_org_name_bn').val();
		params +=  '&status='+ $('#status_cat').val();

		params +=  '&search=true&ajax=true';
		
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged == 0)
		{
			console.log("go found");
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		dosubmit(params);
	
	}

</script>

