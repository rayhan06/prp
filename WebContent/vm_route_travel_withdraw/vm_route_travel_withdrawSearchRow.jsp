<%@page pageEncoding="UTF-8" %>

<%@page import="vm_route_travel_withdraw.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="vm_route_travel.Vm_route_travelDTO" %>
<%@ page import="vm_route_travel.Vm_route_travelDAO" %>
<%@ page import="vm_route.VmRouteStoppageDTO" %>
<%@ page import="vm_route.VmRouteStoppageDAO" %>
<%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page import="java.time.Month" %>
<%@ page import="static jdk.nashorn.internal.objects.NativeMath.round" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="vm_route.VmStoppageRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO) request.getAttribute("vm_route_travel_withdrawDTO");
    CommonDTO commonDTO = vm_route_travel_withdrawDTO;
    String servletName = "Vm_route_travel_withdrawServlet";


    System.out.println("vm_route_travel_withdrawDTO = " + vm_route_travel_withdrawDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO = (Vm_route_travel_withdrawDAO) request.getAttribute("vm_route_travel_withdrawDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");


    String routeTravelTableName = "vm_route_travel";
    Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO(routeTravelTableName);
    //Vm_route_travelDTO vm_route_travelDTO = vm_route_travelDAO.getDTOByOrganogramID(vm_route_travel_withdrawDTO.requesterOrgId);

    String filter = " requester_org_id = " + vm_route_travel_withdrawDTO.requesterOrgId + " ";
    List<Vm_route_travelDTO> vm_route_travelDTOList = vm_route_travelDAO.getDTOs(null, -100, 0, true, userDTO, filter, true);

    //System.out.println("qursy vm_route_travelDTO: "+vm_route_travelDTO);
    double totalArrearsBill = 0;
    boolean first = true;
    Vm_route_travelDTO firstRouteTravelDto = new Vm_route_travelDTO();
    for (Vm_route_travelDTO vm_route_travelDTO : vm_route_travelDTOList) {

        if (first) {
            first = false;
            firstRouteTravelDto = vm_route_travelDTO;
        }

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date today = c.getTime();
        String todayString = simpleDateFormat.format(today);
        int currentYear = Integer.parseInt(todayString.substring(6));
        int previousYear = currentYear - 1;
        int nextYear = currentYear + 1;

        String firstYear = "";
        String secondYear = "";

        int month = Integer.parseInt(todayString.substring(3, 5));
        firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
        secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);

        String reportYear = firstYear + "-" + secondYear;

        Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();


        //Fiscal_yearDTO routeFiscalYear = fiscal_yearDAO.getDTOByID(vm_route_travelDTO.fiscalYearId);

        Fiscal_yearDTO routeFiscalYear = Fiscal_yearRepository.getInstance().getFiscal_yearDTOByid(vm_route_travelDTO.fiscalYearId);

        if (routeFiscalYear != null) {
            String fiscalFilter = " name_en <= '" + reportYear + "' AND name_en >= '" + routeFiscalYear.nameEn + "' ";
            List<Fiscal_yearDTO> fiscal_yearDTOS = fiscal_yearDAO.getDTOs(null, -1, -1, true, userDTO, fiscalFilter, false);
            List<String> fiscalYears = fiscal_yearDTOS
                    .stream()
                    .map(fiscal_yearDTO ->
                            Language.equals("English") ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn)
                    .collect(Collectors.toList());

            String startDate = simpleDateFormat.format(new Date(vm_route_travelDTO.approvedStartDate));
            String withDrawalDate = simpleDateFormat.format(new Date(vm_route_travelDTO.withdrawalDate));

            int startYear = Integer.parseInt(startDate.substring(6));
            int withDrawalYear = Integer.parseInt(withDrawalDate.substring(6));

            int startMonth = Integer.parseInt(startDate.substring(3, 5));
            int withDrawalMonth = Integer.parseInt(withDrawalDate.substring(3, 5));

            String startYearString = startMonth > 6 ? String.valueOf(startYear) : String.valueOf(startYear - 1);
            String withDrawalYearString = withDrawalMonth > 6 ? String.valueOf(withDrawalYear) : String.valueOf(withDrawalYear - 1);

            startMonth = startMonth > 6 ? (startMonth - 7) : (startMonth + 5);
            withDrawalMonth = withDrawalMonth > 6 ? (withDrawalMonth - 7) : (withDrawalMonth + 5);

            HashMap<Long, Boolean[]> fiscalYearToPayment = new HashMap<>();

            int fiscalNumbers = fiscalYears.size();

            int totalMonth = 0;
            for (int fiscalIndex = 0; fiscalIndex < fiscalNumbers; fiscalIndex++) {
                Fiscal_yearDTO fiscal_yearDTO = fiscal_yearDTOS.get(fiscalIndex);
                Boolean[] payment = fiscalYearToPayment.get(fiscal_yearDTO.iD);
                if (payment == null) {
                    payment = new Boolean[12];
                    Arrays.fill(payment, Boolean.FALSE);
                }
                String paymentStr = "";
                for (int monthIndex = 0; monthIndex < 12; monthIndex++) {
                    if (!((fiscal_yearDTO.nameEn.contains(startYearString) && (monthIndex < startMonth))
                            || (fiscal_yearDTO.nameEn.contains(withDrawalYearString) && (monthIndex > withDrawalMonth)))) {
                        paymentStr += " " + (payment[monthIndex]);
                    } else paymentStr += " " + "null";
                }

                paymentStr = paymentStr.substring(1);
                String[] parts = paymentStr.split(" ");
                String[] payment2 = new String[12];
                totalMonth = 0;
                int end_month = -1;
                int start_month = -1;
                String start_fiscal_year = "";
                String end_fiscal_year = "";
                boolean start_month_f = true;
                for (int monthIndex = 0; monthIndex < 12; monthIndex++) {
//			payment2[monthIndex] = parts[monthIndex].equals("null") ? (LM.getText(LC.VM_REQUISITION_ADD_NOTAVAILBLE, loginDTO)) :
//					(Boolean.parseBoolean(parts[monthIndex]) ? LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_PAID, loginDTO) : LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_UNPAID, loginDTO));

                    if (!parts[monthIndex].equals("null")) {
                        if (!Boolean.parseBoolean(parts[monthIndex])) {
                            totalMonth += 1;
                            if (start_month_f) {
                                start_month = monthIndex + 1;
                                end_month = monthIndex + 1;
                                start_fiscal_year = fiscal_yearDTO.nameEn;
                                end_fiscal_year = fiscal_yearDTO.nameEn;
                                start_month_f = false;
                            } else {
                                end_month = monthIndex + 1;
                                end_fiscal_year = fiscal_yearDTO.nameEn;
                            }
                        }
                    }

                }

//                VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO();
                VmRouteStoppageDTO vmRouteStoppageDTO = VmStoppageRepository.getInstance().
                        getVmRouteStoppageDTOByID(vm_route_travelDTO.stoppageId);
//                        vmRouteStoppageDAO.getDTOByID(vm_route_travelDTO.stoppageId);
                double rent = 0;

                //System.out.println("start_month: "+start_month+" "+"end_month: "+end_month+" "+"start_fiscal_year: "+start_fiscal_year+" "+"end_fiscal_year: "+end_fiscal_year+" ");

                String startFiscalYear = start_fiscal_year;
                String[] startFiscalYearArr = startFiscalYear.split("-");

                String endFiscalYear = end_fiscal_year;
                String[] endFiscalYearArr = endFiscalYear.split("-");

                System.out.println("start_month: " + start_month + " " + "end_month: " + end_month + " " + "start_fiscal_year: " + startFiscalYear + " " + "end_fiscal_year: " + endFiscalYear + " ");

                if (start_month != -1 && end_month != -1 && start_month != end_month) {
                    //if(start_month!=-1 && end_month!=-1 && start_month!=end_month && !startFiscalYearArr[0].equalsIgnoreCase(endFiscalYearArr[0]) ){

                    /*find start month rent start*/
                    //Fiscal_yearDTO fiscal_yearDTO2 = fiscal_yearDTOS.get(start_fiscal_year);

//				startFiscalYear = start_month > 6 ? startFiscalYearArr[1] : startFiscalYearArr[0];
//
//				if(start_month>6){
//					start_month = 12-6+start_month;
//					startFiscalYear = "20"+startFiscalYear;
//				}
//				else{
//					start_month = 12-6-start_month;
//				}
//
//
//
//				String startMonthEndDate = "01/"+"0"+start_month+"/"+startFiscalYear;
//				//Month monthName = Month.valueOf(new SimpleDateFormat("MMMM").format(startMonthEndDate).toUpperCase());
//
//
//
//				//String monthLength = String.valueOf(monthName.length(false));
//
//				System.out.println("startMonthEndDate: "+startMonthEndDate);
//
//				/*find start month rent end*/
//
//				int temp_totalMonth = totalMonth-1;
//				rent = (temp_totalMonth)*(vmRouteStoppageDTO.rent) ;
//				long diffInMillies = Math.abs(vm_route_travelDTO.withdrawalDate - vm_route_travelDTO.approvedStartDate);
//				long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
//				rent = rent + (diff+1)*((vmRouteStoppageDTO.rent)/30) ;

                    rent = (vmRouteStoppageDTO.rent) * totalMonth;


                } else if (start_month != -1 && end_month != -1 && start_month == end_month) {
                    //else if(start_month!=-1 && end_month!=-1 && start_month==end_month && startFiscalYearArr[0].equalsIgnoreCase(endFiscalYearArr[0]) ){

                    long diffInMillies = Math.abs(vm_route_travelDTO.withdrawalDate - vm_route_travelDTO.approvedStartDate);
                    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
                    rent = (diff + 1) * ((vmRouteStoppageDTO.rent) / 30);

                }


                totalArrearsBill += rent;

            }
        }

//		String fiscalFilter = " name_en <= '" + reportYear + "' AND name_en >= '" + routeFiscalYear.nameEn + "' ";
//		List<Fiscal_yearDTO> fiscal_yearDTOS = fiscal_yearDAO.getDTOs(null, -1, -1, true, userDTO, fiscalFilter, false);
//		List<String> fiscalYears = fiscal_yearDTOS
//				.stream()
//				.map(fiscal_yearDTO ->
//						Language.equals("English") ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn)
//				.collect(Collectors.toList());
//
//		String startDate = simpleDateFormat.format(new Date(vm_route_travelDTO.approvedStartDate));
//		String withDrawalDate = simpleDateFormat.format(new Date(vm_route_travelDTO.withdrawalDate));
//
//		int startYear = Integer.parseInt(startDate.substring(6));
//		int withDrawalYear = Integer.parseInt(withDrawalDate.substring(6));
//
//		int startMonth = Integer.parseInt(startDate.substring(3, 5));
//		int withDrawalMonth = Integer.parseInt(withDrawalDate.substring(3, 5));
//
//		String startYearString = startMonth > 6 ? String.valueOf(startYear) : String.valueOf(startYear-1);
//		String withDrawalYearString = withDrawalMonth > 6 ? String.valueOf(withDrawalYear): String.valueOf(withDrawalYear-1);
//
//		startMonth = startMonth > 6 ? (startMonth-7) : (startMonth+5);
//		withDrawalMonth = withDrawalMonth > 6 ? (withDrawalMonth-7): (withDrawalMonth+5);
//
//		HashMap<Long, Boolean[]> fiscalYearToPayment = new HashMap<>();
//
//		int fiscalNumbers = fiscalYears.size();
//
//		int totalMonth=0;
//		for (int fiscalIndex=0; fiscalIndex < fiscalNumbers; fiscalIndex++) {
//			Fiscal_yearDTO fiscal_yearDTO = fiscal_yearDTOS.get(fiscalIndex);
//			Boolean[] payment = fiscalYearToPayment.get(fiscal_yearDTO.iD);
//			if (payment == null) {
//				payment = new Boolean[12];
//				Arrays.fill(payment, Boolean.FALSE);
//			}
//			String paymentStr = "";
//			for (int monthIndex = 0; monthIndex < 12; monthIndex++) {
//				if (!((fiscal_yearDTO.nameEn.contains(startYearString) && (monthIndex < startMonth))
//						|| (fiscal_yearDTO.nameEn.contains(withDrawalYearString) && (monthIndex > withDrawalMonth)))) {
//					paymentStr += " " + (payment[monthIndex]);
//				}
//				else paymentStr += " " + "null";
//			}
//
//			paymentStr = paymentStr.substring(1);
//			String[] parts = paymentStr.split(" ");
//			String[] payment2 = new String[12];
//			totalMonth=0;
//			int end_month=-1;
//			int start_month=-1;
//			String start_fiscal_year = "";
//			String end_fiscal_year = "";
//			boolean start_month_f = true;
//			for (int monthIndex = 0; monthIndex < 12; monthIndex++) {
////			payment2[monthIndex] = parts[monthIndex].equals("null") ? (LM.getText(LC.VM_REQUISITION_ADD_NOTAVAILBLE, loginDTO)) :
////					(Boolean.parseBoolean(parts[monthIndex]) ? LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_PAID, loginDTO) : LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_UNPAID, loginDTO));
//
//				if(!parts[monthIndex].equals("null")){
//					if(!Boolean.parseBoolean(parts[monthIndex])){
//						totalMonth+=1;
//						if(start_month_f){
//							start_month = monthIndex+1;
//							end_month = monthIndex+1;
//							start_fiscal_year = fiscal_yearDTO.nameEn;
//							end_fiscal_year = fiscal_yearDTO.nameEn;
//							start_month_f = false;
//						}
//						else{
//							end_month = monthIndex+1;
//							end_fiscal_year = fiscal_yearDTO.nameEn;
//						}
//					}
//				}
//
//			}
//
//			VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO();
//			VmRouteStoppageDTO vmRouteStoppageDTO = vmRouteStoppageDAO.getDTOByID(vm_route_travelDTO.stoppageId);
//			double rent = 0;
//
//			//System.out.println("start_month: "+start_month+" "+"end_month: "+end_month+" "+"start_fiscal_year: "+start_fiscal_year+" "+"end_fiscal_year: "+end_fiscal_year+" ");
//
//			String startFiscalYear = start_fiscal_year;
//			String startFiscalYearArr[] = startFiscalYear.split("-");
//
//			String endFiscalYear = end_fiscal_year;
//			String endFiscalYearArr[] = endFiscalYear.split("-");
//
//			System.out.println("start_month: "+start_month+" "+"end_month: "+end_month+" "+"start_fiscal_year: "+startFiscalYear+" "+"end_fiscal_year: "+endFiscalYear+" ");
//
//			if(start_month!=-1 && end_month!=-1 && start_month!=end_month  ){
//			//if(start_month!=-1 && end_month!=-1 && start_month!=end_month && !startFiscalYearArr[0].equalsIgnoreCase(endFiscalYearArr[0]) ){
//
//				/*find start month rent start*/
//				//Fiscal_yearDTO fiscal_yearDTO2 = fiscal_yearDTOS.get(start_fiscal_year);
//
////				startFiscalYear = start_month > 6 ? startFiscalYearArr[1] : startFiscalYearArr[0];
////
////				if(start_month>6){
////					start_month = 12-6+start_month;
////					startFiscalYear = "20"+startFiscalYear;
////				}
////				else{
////					start_month = 12-6-start_month;
////				}
////
////
////
////				String startMonthEndDate = "01/"+"0"+start_month+"/"+startFiscalYear;
////				//Month monthName = Month.valueOf(new SimpleDateFormat("MMMM").format(startMonthEndDate).toUpperCase());
////
////
////
////				//String monthLength = String.valueOf(monthName.length(false));
////
////				System.out.println("startMonthEndDate: "+startMonthEndDate);
////
////				/*find start month rent end*/
////
////				int temp_totalMonth = totalMonth-1;
////				rent = (temp_totalMonth)*(vmRouteStoppageDTO.rent) ;
////				long diffInMillies = Math.abs(vm_route_travelDTO.withdrawalDate - vm_route_travelDTO.approvedStartDate);
////				long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
////				rent = rent + (diff+1)*((vmRouteStoppageDTO.rent)/30) ;
//
//				rent = (vmRouteStoppageDTO.rent) * totalMonth ;
//
//
//			}
//			else if(start_month!=-1 && end_month!=-1 && start_month==end_month  ){
//			//else if(start_month!=-1 && end_month!=-1 && start_month==end_month && startFiscalYearArr[0].equalsIgnoreCase(endFiscalYearArr[0]) ){
//
//				long diffInMillies = Math.abs(vm_route_travelDTO.withdrawalDate - vm_route_travelDTO.approvedStartDate);
//				long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
//				rent = (diff+1)*((vmRouteStoppageDTO.rent)/30) ;
//
//			}
//
//
//
//			totalArrearsBill+=rent;
//
//		}

    }


%>


<td id='<%=i%>_hidden_totalArrearsBill' style="display: none">
    <input type="hidden" name="hidden_totalArrearsBill" value="<%=String.format("%.2f", totalArrearsBill)%>">
</td>

<td id='<%=i%>_requesterOrgId'>
    <%
        value = Language.equals("English") ?
                vm_route_travel_withdrawDTO.requesterNameEn
                : vm_route_travel_withdrawDTO.requesterNameBn
        ;
    %>

    <%=value%>


</td>


<td id='<%=i%>_driverName'>
    <%=Language.equals("English") ? (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn)
            : (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn)%>


</td>

<td id='<%=i%>_startAndEndAddress'>
    <%=Language.equals("English") ? (vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn)
            : (vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn)%>


</td>

<td id='<%=i%>_startDate'>
    <%
        value = firstRouteTravelDto.approvedStartDate + "";
    %>
    <%
        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate, Language)%>


</td>

<td id='<%=i%>_endDate'>
    <%
        value = vm_route_travel_withdrawDTO.endDate + "";
    %>
    <%
        String formatted_startDate2 = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate2, Language)%>


</td>

<td id='<%=i%>_routeName'>
    <%
        value = firstRouteTravelDto.routeId + "";
        value = CommonDAO.getName(Language, "vm_route", firstRouteTravelDto.routeId);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>
<td id='<%=i%>_stoppagePoint'>
    <%
        value = firstRouteTravelDto.stoppageId + "";
        if (value.equals("-1")) {
            value = Language.equals("English") ? "Others" : "অন্যান্য";
            value += "(" + firstRouteTravelDto.requestedStoppageName + ")";
        } else {
            value = CommonDAO.getName(firstRouteTravelDto.stoppageId, "vm_route_stoppage", "stoppage_name", "ID");
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_taotalArrearsBill'>

    <%=Utils.getDigits(String.format("%.2f", totalArrearsBill), Language)%>


</td>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Vm_route_travel_withdrawServlet?actionType=view&ID=<%=vm_route_travel_withdrawDTO.requesterOrgId%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>
	

											
											

																						
											

