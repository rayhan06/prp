<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_route_travel_withdraw.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="vm_route_travel.Vm_route_travelDAO" %>
<%@ page import="vm_route_travel.Vm_route_travelDTO" %>
<%@ page import="vm_route.*" %>


<%
    String servletName = "Vm_route_travel_withdrawServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO = new Vm_route_travel_withdrawDAO("vm_route_travel_withdraw");
    Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO) vm_route_travel_withdrawDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");


    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String routeTravelTableName = "vm_route_travel";
    String routeTableName = "vm_route";
    String routeStoppageTableName = "vm_route_stoppage";

    Vm_routeDAO vm_routeDAO = new Vm_routeDAO(routeTableName);
    Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO(routeTravelTableName);
    Vm_route_travelDTO vm_route_travelDTO = vm_route_travelDAO.getDTOByEmployeeRecordID(userDTO.employee_record_id);


%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_SEARCH_ROUTE_CANCELLATION_REQUESTS, loginDTO)%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div class="row">
                <%--						<div class="col-md-1"></div>--%>
                <div class="col-md-12">
                    <div class="onlyborder">
                        <div class="row">
                            <%--										<div class="col-md-2"></div>--%>
                            <div class="col-md-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_SEARCH_ROUTE_CANCELLATION_REQUESTS, loginDTO)%>
                                        </h4>
                                    </div>

                                    <div class="col-md-12">
                                        <table class="table table-bordered table-striped">


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_APPLICANT_OFFICER_NAME, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%
                                                        value = Language.equals("English") ?
                                                                vm_route_travel_withdrawDTO.requesterNameEn
                                                                : vm_route_travel_withdrawDTO.requesterNameBn
                                                        ;
                                                    %>

                                                    <%=value%>


                                                </td>

                                            </tr>


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.PROMOTION_HISTORY_EDIT_RANKCAT, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%=Language.equals("English") ? (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn)
                                                            : (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn)%>


                                                </td>

                                            </tr>


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%=Language.equals("English") ? (vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn)
                                                            : (vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn)%>


                                                </td>

                                            </tr>

                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%
                                                        value = vm_route_travel_withdrawDTO.requesterPhoneNum + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>


                                                </td>

                                            </tr>


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_END_DATE_OF_TRAVEL, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%
                                                        value = vm_route_travelDTO.approvedStartDate + "";
                                                    %>
                                                    <%
                                                        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(formatted_startDate, Language)%>


                                                </td>

                                            </tr>


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_LAST_DATE_OF_TRAVEL, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%
                                                        value = vm_route_travel_withdrawDTO.endDate + "";
                                                    %>
                                                    <%
                                                        formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(formatted_startDate, Language)%>


                                                </td>

                                            </tr>


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_STOPPAGE_POINT, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%
                                                        value = vm_route_travelDTO.stoppageId + "";
                                                        if (value.equals("-1")) {
                                                            value = Language.equals("English") ? "Others" : "অন্যান্য";
                                                            value += "(" + vm_route_travelDTO.requestedStoppageName + ")";
                                                        } else {
                                                            value = CommonDAO.getName(vm_route_travelDTO.stoppageId, "vm_route_stoppage", "stoppage_name", "ID");
                                                        }
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>


                                                </td>

                                            </tr>


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_RENT, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%
                                                        VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO();

                                                        VmRouteStoppageDTO vmRouteStoppageDTO = VmStoppageRepository.getInstance()
                                                                .getVmRouteStoppageDTOByID(vm_route_travelDTO.stoppageId);
//                                                                vmRouteStoppageDAO.getDTOByID(vm_route_travelDTO.stoppageId);
                                                    %>

                                                    <%=Utils.getDigits(vmRouteStoppageDTO.rent, Language)%>


                                                </td>

                                            </tr>


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_TOTAL_ARREARS_OF_RENT, loginDTO)%>
                                                    </b></td>
                                                <td>


                                                </td>

                                            </tr>


                                            <tr>
                                                <td style="width:30%">
                                                    <b><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%>
                                                    </b></td>
                                                <td>

                                                    <%--															<%--%>
                                                    <%--																value = vm_route_travel_withdrawDTO.requesterOfficeNameBn + "";--%>
                                                    <%--															%>--%>

                                                    <%--															<%=Utils.getDigits(value, Language)%>--%>


                                                </td>

                                            </tr>


                                        </table>
                                    </div>

                                </div>
                            </div>
                            <%--									<div class="col-md-1"></div>--%>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <%--						<div class="col-md-1"></div>--%>
            </div>
        </div>

    </div>
</div>