<%@page pageEncoding="UTF-8" %>

<%@page import="vm_route_travel.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_route.VmRouteStoppageDAO" %>
<%@ page import="vm_route.VmRouteStoppageDTO" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_route_travelDTO vm_route_travelDTO = (Vm_route_travelDTO)request.getAttribute("vm_route_travelDTO");
CommonDTO commonDTO = vm_route_travelDTO;
String servletName = "Vm_route_travelServlet";


System.out.println("vm_route_travelDTO = " + vm_route_travelDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
String fiscalYear = (request.getParameter("fiscalYear"));

String paymentStr = request.getParameter("payment");
String[] parts = paymentStr.split(" ");
String[] payment = new String[12];
for (int monthIndex = 0; monthIndex < 12; monthIndex++)
	payment[monthIndex] = parts[monthIndex].equals("null") ? (LM.getText(LC.VM_REQUISITION_ADD_NOTAVAILBLE, loginDTO)) :
			(Boolean.parseBoolean(parts[monthIndex]) ? LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_PAID, loginDTO) : LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_UNPAID, loginDTO));



out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_route_travelDAO vm_route_travelDAO = (Vm_route_travelDAO)request.getAttribute("vm_route_travelDAO");

FilesDAO filesDAO = new FilesDAO();

String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		

		
											<td id = '<%=i%>_fiscal_year'>
											<%
											value = fiscalYear + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		

					<%
						for (int monthIndex=0; monthIndex < 12; monthIndex++) {
					%>
										<td id = '<%=i%>_<%=monthIndex%>'>
											<%
											value = payment[monthIndex] + "";
											%>

											<%=Utils.getDigits(value, Language)%>


											</td>
					<%
						}
					%>