<%@page pageEncoding="UTF-8" %>

<%@page import="vm_route_travel_withdraw.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_route_travel.Vm_route_travelDTO" %>
<%@ page import="vm_route_travel.Vm_route_travelDAO" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO)request.getAttribute("vm_route_travel_withdrawDTO");
CommonDTO commonDTO = vm_route_travel_withdrawDTO;
String servletName = "Vm_route_travel_withdrawServlet";


System.out.println("vm_route_travel_withdrawDTO = " + vm_route_travel_withdrawDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO = (Vm_route_travel_withdrawDAO)request.getAttribute("vm_route_travel_withdrawDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
String status = "1";

	String routeTravelTableName = "vm_route_travel";
	Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO(routeTravelTableName);
	Vm_route_travelDTO vm_route_travelDTO = vm_route_travelDAO.getDTOByOrganogramIDForWithdrawal(userDTO.organogramID);

	System.out.println("withdraw vm_route_travelDTO: "+userDTO.organogramID);

%>

											<td id = '<%=i%>_status' style="display: none;">
												<input type="hidden" name="status" value="<%=status%>">
											</td>
											<td id = '<%=i%>_id' style="display: none;">

												<input type="hidden" name="iD" value="<%=vm_route_travel_withdrawDTO.id%>">
											</td>


											<td id = '<%=i%>_requesterOrgId'>
												<%
													value = Language.equals("English") ?
															vm_route_travel_withdrawDTO.requesterNameEn
															: vm_route_travel_withdrawDTO.requesterNameBn
													;
												%>

												<%=value%>


											</td>





											<td id = '<%=i%>_driverName'>
												<%=Language.equals("English") ? (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameEn )
														: (vm_route_travel_withdrawDTO.requesterOfficeUnitOrgNameBn )%>


											</td>

											<td id = '<%=i%>_startAndEndAddress'>
												<%=Language.equals("English") ? (   vm_route_travel_withdrawDTO.requesterOfficeUnitNameEn )
														: (  vm_route_travel_withdrawDTO.requesterOfficeUnitNameBn )%>


											</td>

											<td id = '<%=i%>_endDate'>
												<%
													value = vm_route_travel_withdrawDTO.endDate + "";
												%>
												<%
													String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
												%>
												<%=Utils.getDigits(formatted_startDate, Language)%>


											</td>

											<td id = '<%=i%>_routeName'>
												<%
													value = vm_route_travelDTO.routeId + "";
													value = CommonDAO.getName(Language, "vm_route", vm_route_travelDTO.routeId);
												%>

												<%=Utils.getDigits(value, Language)%>


											</td>
											<td id = '<%=i%>_stoppagePoint'>
												<%
													value = vm_route_travelDTO.stoppageId + "";
													if (value.equals("-1")) {
														value = Language.equals("English") ? "Others" : "অন্যান্য";
														value += "(" + vm_route_travelDTO.requestedStoppageName + ")";
													}
													else {
														value = CommonDAO.getName(vm_route_travelDTO.stoppageId, "vm_route_stoppage", "stoppage_name", "ID");
													}
												%>

												<%=Utils.getDigits(value, Language)%>


											</td>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	

											<td>
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_route_travel_withdrawServlet?actionType=view&ID=<%=userDTO.organogramID%>'">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<button type="submit" class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px">

											        <%=LM.getText(LC.HM_APPROVE, loginDTO)%>
											    </button>
																				
											</td>											
											
											

																						
											

