<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_route.VmRouteStoppageDTO" %>
<%@ page import="vm_route.VmRouteStoppageDAO" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%--<form class="form-horizontal"--%>
<%--	  action="Vm_route_travel_withdrawServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
<%--	  id="bigform" name="bigform" method="POST" enctype="multipart/form-data"--%>
<%--	  onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>
<form class="form-horizontal" id="bigform" name="bigform">
    <div class="kt-portlet__body form-body">
        <div class="row">
            <div class="col-12">
                <div class="onlyborder">
                    <div class="row">
                        <div class="col-12">
                            <div class="row mx-2">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>'
                                           value='<%=vm_route_travel_withdrawDTO.id%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='routeTravelId'
                                           id='routeTravelId_hidden_<%=i%>' value='<%=vm_route_travelDTO.routeId%>'
                                           tag='pb_html'/>


                                    <%
                                        //value = "0";
                                        value = String.valueOf(CommonApprovalStatus.PENDING.getValue());

                                    %>
                                    <%--									<input type='hidden' class='form-control'  name='status' id = 'status_number_<%=i%>' value='<%=value%>'  tag='pb_html'>--%>

                                    <input type='hidden' class='form-control' name='requesterOrgId'
                                           id='requesterOrgId_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOrgId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOfficeId'
                                           id='requesterOfficeId_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOfficeId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOfficeUnitId'
                                           id='requesterOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOfficeUnitId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterEmpId'
                                           id='requesterEmpId_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterEmpId%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='requesterPhoneNum'
                                           id='requesterPhoneNum_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterPhoneNum%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterNameEn'
                                           id='requesterNameEn_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterNameEn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterNameBn'
                                           id='requesterNameBn_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterNameBn%>' tag='pb_html'/>


                                    <input type='hidden' class='form-control' name='requesterOfficeNameEn'
                                           id='requesterOfficeNameEn_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOfficeNameEn%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='requesterOfficeNameBn'
                                           id='requesterOfficeNameBn_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOfficeNameBn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOfficeUnitNameEn'
                                           id='requesterOfficeUnitNameEn_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOfficeUnitNameEn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOfficeUnitNameBn'
                                           id='requesterOfficeUnitNameBn_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOfficeUnitNameBn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOfficeUnitOrgNameEn'
                                           id='requesterOfficeUnitOrgNameEn_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOfficeUnitOrgNameEn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOfficeUnitOrgNameBn'
                                           id='requesterOfficeUnitOrgNameBn_text_<%=i%>'
                                           value='<%=vm_route_travelDTO.requesterOfficeUnitOrgNameBn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_text_<%=i%>'
                                           value='<%=vm_route_travel_withdrawDTO.insertedBy%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_text_<%=i%>'
                                           value='<%=vm_route_travel_withdrawDTO.modifiedBy%>' tag='pb_html'/>


                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_NAMEOFEMPLOYEE, loginDTO)%>

                                        </label>
                                        <input type="text"
                                               class="form-control col-md-4"
                                               readonly
                                               value="<%=isLanguageEnglish ? (vm_route_travelDTO.requesterNameEn): (vm_route_travelDTO.requesterNameBn)%>"
                                        />

                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.UNIT_NAME_UPDATE_OFFICE, loginDTO)%>

                                        </label>
                                        <input type="text"
                                               class="form-control col-md-4"
                                               readonly
                                               value="<%=isLanguageEnglish ? (vm_route_travelDTO.requesterOfficeUnitNameEn): (vm_route_travelDTO.requesterOfficeUnitNameBn)%>"
                                        />
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.PROMOTION_HISTORY_EDIT_RANKCAT, loginDTO)%>

                                        </label>
                                        <input type="text"
                                               class="form-control col-md-4"
                                               readonly
                                               value="<%=isLanguageEnglish ? (vm_route_travelDTO.requesterOfficeUnitOrgNameEn): (vm_route_travelDTO.requesterOfficeUnitOrgNameBn)%>"
                                        />

                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_FARE_AMOUNT, loginDTO)%>

                                        </label>
                                        <input type="text"
                                               class="form-control col-md-4"
                                               readonly
                                               value="<%VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO();VmRouteStoppageDTO vmRouteStoppageDTO = vmRouteStoppageDAO.getDTOByID(vm_route_travelDTO.stoppageId);%><%=Utils.getDigits(vmRouteStoppageDTO.rent, Language)%>"
                                        />
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_ROUTE_NAME, loginDTO)%>

                                        </label>
                                        <input type="text"
                                               class="form-control col-md-4"
                                               readonly
                                               value="<%if (vm_routeDTO != null) {%><%=isLanguageEnglish ? (vm_routeDTO.nameEn): (vm_routeDTO.nameBn)%><%}%>"
                                        />

                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_STOPPAGE_POINT, loginDTO)%>

                                        </label>
                                        <input type="text"
                                               class="form-control col-md-4"
                                               readonly
                                               value='<%value = vm_route_travelDTO.stoppageId + "";if (value.equals("-1")) {value = Language.equals("English") ? "Others" : "অন্যান্য";value += "(" + vm_route_travelDTO.requestedStoppageName + ")";} else {value = CommonDAO.getName(vm_route_travelDTO.stoppageId, "vm_route_stoppage", "stoppage_name", "ID");}%><%=Utils.getDigits(value, Language)%>'
                                        />
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%>
                                        </label>
                                        <input type="text"
                                               class="form-control col-md-4"
                                               readonly
                                               value='<%Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_routeDTO.vehicleId);if (vm_vehicleDTO != null) {value = vm_vehicleDTO.regNo + "";} else {value = "";}%><%=Utils.getDigits(value, Language)%>'
                                        />

                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_LAST_DATE_OF_TRAVEL, loginDTO)%>

                                        </label>
                                        <div class="col-md-4">
                                            <div>
                                                <%value = "endDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='endDate' id='endDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(vm_route_travel_withdrawDTO.endDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=vm_route_travel_withdrawDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_route_travel_withdrawDTO.lastModificationTime%>' tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions text-right mt-3">
            <button class="btn btn-success btn-sm shadow btn-border-radius" type="button"
                    onclick="submitForm()"><%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_EDIT_SUBMIT_BUTTON, loginDTO)%>
            </button>
        </div>
    </div>
</form>