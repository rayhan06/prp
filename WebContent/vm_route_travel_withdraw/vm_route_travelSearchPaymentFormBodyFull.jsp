<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_route_travel.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="vm_route_travel_payment.Vm_route_travel_paymentDAO" %>
<%@ page import="vm_route_travel_payment.Vm_route_travel_paymentDTO" %>
<%@ page import="java.util.*" %>
<%@ page import="category.CategoryDTO" %>
<%@ page import="category.CategoryDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawDAO" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="vm_route.*" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawRepository" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    Vm_route_travelDTO vm_route_travelDTO = (Vm_route_travelDTO) request.getAttribute("vm_route_travelDTO");


    String navigator2 = SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    String routeWithdrawTableName = "vm_route_travel_withdraw";
    String routeTableName = "vm_route";
    String routeStoppageTableName = "vm_route_stoppage";


    Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO = new Vm_route_travel_withdrawDAO(routeWithdrawTableName);
    //Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = vm_route_travel_withdrawDAO.getDTOByRequesterOrgID(vm_route_travelDTO.requesterOrgId);

    Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO =null;
    List<Vm_route_travel_withdrawDTO> vm_route_travel_withdrawDT0s1 = Vm_route_travel_withdrawRepository.getInstance().getSatisfiedVm_route_travel_withdrawDTOByRequesterOrgID(vm_route_travelDTO.requesterOrgId);

    if(vm_route_travel_withdrawDT0s1.size()>0){
        vm_route_travel_withdrawDTO = vm_route_travel_withdrawDT0s1.get(0);
    }

    //vm_route_travel_withdrawDTO = Vm_route_travel_withdrawRepository.getInstance().getSatisfiedVm_route_travel_withdrawDTOByRequesterOrgID(vm_route_travelDTO.requesterOrgId).get(0);

%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive mb-5">
    <table id="tableData" class="table table-bordered table-striped">

        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_APPLICANT_OFFICER_NAME, loginDTO)%>
            </b></td>
            <td>

                <%
                    value = Language.equals("English") ?
                            vm_route_travelDTO.requesterNameEn
                            : vm_route_travelDTO.requesterNameBn
                    ;
                %>

                <%=value%>


            </td>

        </tr>


        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.PROMOTION_HISTORY_EDIT_RANKCAT, loginDTO)%>
            </b></td>
            <td>

                <%=Language.equals("English") ? (vm_route_travelDTO.requesterOfficeUnitOrgNameEn)
                        : (vm_route_travelDTO.requesterOfficeUnitOrgNameBn)%>


            </td>

        </tr>


        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>
            </b></td>
            <td>

                <%=Language.equals("English") ? (vm_route_travelDTO.requesterOfficeUnitNameEn)
                        : (vm_route_travelDTO.requesterOfficeUnitNameBn)%>


            </td>

        </tr>

        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>
            </b></td>
            <td>

                <%
                    value = vm_route_travelDTO.requesterPhoneNum + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

        </tr>


        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_END_DATE_OF_TRAVEL, loginDTO)%>
            </b></td>
            <td>

                <%
                    value = vm_route_travelDTO.approvedStartDate + "";
                %>
                <%
                    String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_startDate, Language)%>


            </td>

        </tr>


        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_LAST_DATE_OF_TRAVEL, loginDTO)%>
            </b></td>
            <td>

                <%
                    value = "";
                    String formatted_endDate = "";
                    if (vm_route_travel_withdrawDTO != null) {
                        value = vm_route_travel_withdrawDTO.endDate + "";
                        formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                    }


                %>
                <%

                %>
                <%=Utils.getDigits(formatted_endDate, Language)%>


            </td>

        </tr>


        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_STOPPAGE_POINT, loginDTO)%>
            </b></td>
            <td>

                <%
                    value = vm_route_travelDTO.stoppageId + "";
                    if (value.equals("-1")) {
                        value = Language.equals("English") ? "Others" : "অন্যান্য";
                        value += "(" + vm_route_travelDTO.requestedStoppageName + ")";
                    } else {
                        value = CommonDAO.getName(vm_route_travelDTO.stoppageId, "vm_route_stoppage", "stoppage_name", "ID");
                    }
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

        </tr>


        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_RENT, loginDTO)%>
            </b></td>
            <td>

                <%
                    VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO();

                    VmRouteStoppageDTO vmRouteStoppageDTO = VmStoppageRepository.getInstance()
                            .getVmRouteStoppageDTOByID(vm_route_travelDTO.stoppageId);
//                            vmRouteStoppageDAO.getDTOByID(vm_route_travelDTO.stoppageId);
                %>

                <%=Utils.getDigits(vmRouteStoppageDTO.rent, Language)%>


            </td>

        </tr>


        <%--						<tr>--%>
        <%--							<td style="width:30%"><b><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_TOTAL_ARREARS_OF_RENT, loginDTO)%></b></td>--%>
        <%--							<td>--%>


        <%--								<%--%>
        <%--									value = request.getAttribute("totalArrearsBill") + "";--%>
        <%--								%>--%>

        <%--								<%=Utils.getDigits(value, Language)%>--%>

        <%--							</td>--%>

        <%--						</tr>--%>


        <tr>
            <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%>
            </b></td>
            <td>

                <%--															<%--%>
                <%--																value = vm_route_travel_withdrawDTO.requesterOfficeNameBn + "";--%>
                <%--															%>--%>

                <%--															<%=Utils.getDigits(value, Language)%>--%>
                <%

                    Vm_routeDTO vm_routeDTO = null;
                    Vm_vehicleDTO vm_vehicleDTO = null;

                    if (vm_route_travelDTO != null && vm_route_travelDTO.routeId > 0) {
                        //vm_routeDTO =   vm_routeDAO.getDTOByID(vm_route_travelDTO.routeId);
                        vm_routeDTO = Vm_routeRepository.getInstance().getVm_routeDTOByID(vm_route_travelDTO.routeId);
                    }

                    if (vm_routeDTO != null) {
                        //vm_vehicleDTO = vm_vehicleDAO.getDTOByID(vm_routeDTO.vehicleId);
                        vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_routeDTO.vehicleId);
                    }


                    if (vm_vehicleDTO != null) {
                        value = vm_vehicleDTO.regNo + "";

                    } else {
                        value = "";
                    }


                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

        </tr>


    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>