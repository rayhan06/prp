<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_route_travel_withdraw.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="vm_route_travel.Vm_route_travelDTO" %>
<%@ page import="vm_route_travel.Vm_route_travelDAO" %>
<%@ page import="vm_route.*" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>


<%
    Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO;
    vm_route_travel_withdrawDTO = (Vm_route_travel_withdrawDTO) request.getAttribute("vm_route_travel_withdrawDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_route_travel_withdrawDTO == null) {
        vm_route_travel_withdrawDTO = new Vm_route_travel_withdrawDTO();

    }
    System.out.println("vm_route_travel_withdrawDTO = " + vm_route_travel_withdrawDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_VM_ROUTE_TRAVEL_WITHDRAW_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_route_travel_withdrawServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    boolean withdrawFlag = false;
    int withdrawF = 0;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String routeTravelTableName = "vm_route_travel";
    String routeTableName = "vm_route";
    String routeStoppageTableName = "vm_route_stoppage";

    Vm_routeDAO vm_routeDAO = new Vm_routeDAO(routeTableName);
    Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO(routeTravelTableName);


    Vm_route_travelDTO vm_route_travelDTO = vm_route_travelDAO.getDTOByOrganogramID(userDTO.organogramID);

    Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO = new Vm_route_travel_withdrawDAO();
    //Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTOIsPending = vm_route_travel_withdrawDAO.getDTOForRouteEnrollmentByRequesterOrgID(userDTO.organogramID);

    Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTOIsPending =null;

    List<Vm_route_travel_withdrawDTO> vm_route_travel_withdrawDT0s1 = Vm_route_travel_withdrawRepository.getInstance().getPendingVm_route_travel_withdrawDTOByRequesterOrgID(userDTO.organogramID);
    if(vm_route_travel_withdrawDT0s1.size()>0) {
        vm_route_travel_withdrawDTOIsPending = vm_route_travel_withdrawDT0s1.get(0);
    }

    //Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTOIsPending = Vm_route_travel_withdrawRepository.getInstance().getPendingVm_route_travel_withdrawDTOByRequesterOrgID(userDTO.organogramID).get(0);

    Vm_routeDTO vm_routeDTO = new Vm_routeDTO();

    System.out.println("vm_route_travelDTO fff: " + vm_route_travelDTO);

    if (vm_route_travelDTO != null) {
        if (vm_route_travelDTO.status == CommonApprovalStatus.SATISFIED.getValue() && vm_route_travel_withdrawDTOIsPending == null) {
            withdrawF = 1;
            withdrawFlag = true;

            //vm_routeDTO =   vm_routeDAO.getDTOByID(vm_route_travelDTO.routeId);
            vm_routeDTO = Vm_routeRepository.getInstance().getVm_routeDTOByID(vm_route_travelDTO.routeId);


        }
    }


%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <input type="hidden" id="withdrawVal" value="<%=withdrawF%>">

        <%

            if (!withdrawFlag) {

        %>


        <%

        } else {%>
        <%@ include file="widrawerContent.jsp" %>
        <% }

        %>


    </div>
</div>


<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        preprocessDateBeforeSubmitting('endDate', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_route_travel_withdrawServlet");
    }

    function init(row) {

        setDateByStringAndId('endDate_js_' + row, $('#endDate_date_' + row).val());


    }

    var row = 0;
    $(document).ready(function () {


        let val = document.getElementById('withdrawVal').value;

        if (val == 0) {
            setTimeout(function () {
                swal.fire({
                    title: '<%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_VM_ROUTE_TRAVEL_WITHDRAW_ADD_FORMNAME, loginDTO)%>',
                    text: '<%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_NO_ROOT_IS_ACTIVE_FOR_YOU, loginDTO)%>',
                    type: "error",
                    allowOutsideClick: false,
                    confirmButtonColor: "#00B4B4"
                }, function () {
                    window.location.href = "";
                });
            }, 0);


        }

        init(row);
        CKEDITOR.replaceAll();

    });

    var child_table_extra_id = <%=childTableStartingID%>;

    const vehicleReceiveForm = $('#bigform');

    function submitForm() {


        if ($("#bigform").valid()) {
            $.ajax({
                type: "POST",
                url: "Vm_route_travel_withdrawServlet?actionType=<%=actionName%>",
                data: vehicleReceiveForm.serialize(),
                dataType: 'JSON',
                success: function (response) {

                    if (response.responseCode === 0) {
                        showToastSticky(response.msg, response.msg);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                    toastr.error('Can not accept request');
                }
            });
        } else {

        }
    }


</script>






