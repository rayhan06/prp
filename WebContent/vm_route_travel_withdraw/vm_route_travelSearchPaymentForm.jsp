<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_route_travel.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="vm_route_travel_payment.Vm_route_travel_paymentDAO" %>
<%@ page import="vm_route_travel_payment.Vm_route_travel_paymentDTO" %>
<%@ page import="java.util.*" %>
<%@ page import="category.CategoryDTO" %>
<%@ page import="category.CategoryDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    Vm_route_travelDTO vm_route_travelDTO = (Vm_route_travelDTO) request.getAttribute("vm_route_travelDTO");

    String filter = " route_travel_id = " + vm_route_travelDTO.iD + " ";

    Vm_route_travel_paymentDAO vm_route_travel_paymentDAO = new Vm_route_travel_paymentDAO();
    List<Vm_route_travel_paymentDTO> vm_route_travel_paymentDTOList = vm_route_travel_paymentDAO.getDTOs(null, -100, 0, true, userDTO, filter, true);


    String navigator2 = SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>
<%

                }
            }


        }
    }

%>

<h5 class="table-title">
    <%=LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LIST_OF_PAYMENT, loginDTO)%>
</h5>
<div class="table-responsive">
    <table class="table table-bordered table-striped text-nowrap">

        <th>
            <%
                value = LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FISCALYEARID, loginDTO) + "";
            %>

            <%=Utils.getDigits(value, Language)%>


        </th>

        <%
            String filterMonths = " domain_name = 'month' ";
            CategoryDAO categoryDAO = new CategoryDAO();
            List<CategoryDTO> monthsRaw = categoryDAO.getDTOs(null, -1, -1, true, userDTO, filterMonths, false);
            List<CategoryDTO> months = monthsRaw.stream()
                    .sorted(Comparator.comparingLong(cat -> cat.value))
                    .filter(categoryDTO -> categoryDTO.value != 0)
                    .collect(Collectors.toList());

            for (int monthIndex = 6; monthIndex < 12; monthIndex++) {
                CategoryDTO cat = months.get(monthIndex);

        %>
        <th style="width: 8%">
            <%
                value = Language.equals("English") ? cat.nameEn : cat.nameBn + "";
            %>

            <%=Utils.getDigits(value, Language)%>


        </th>
        <%
            }
        %>

        <%
            for (int monthIndex = 0; monthIndex < 6; monthIndex++) {
                CategoryDTO cat = months.get(monthIndex);

        %>
        <th style="width: 8%">
            <%
                value = Language.equals("English") ? cat.nameEn : cat.nameBn + "";
            %>

            <%=Utils.getDigits(value, Language)%>


        </th>
        <%
            }
        %>

        <%

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
//	c.add(Calendar.YEAR, i);
            Date today = c.getTime();
            SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
            String todayString = sf.format(today);
            int currentYear = Integer.parseInt(todayString.substring(6));
            int previousYear = currentYear - 1;
            int nextYear = currentYear + 1;

            String firstYear = "";
            String secondYear = "";

            int month = Integer.parseInt(todayString.substring(3, 5));
            firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
            secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);

            String reportYear = firstYear + "-" + secondYear;
//	String reportYearByLanguage = Utils.getDigits(reportYear, Language);


            Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();

        %>
        <%

            if (vm_route_travelDTO.fiscalYearId > 0) {
                Fiscal_yearDTO routeFiscalYear = fiscal_yearDAO.getDTOByID(vm_route_travelDTO.fiscalYearId);
                String fiscalFilter = " name_en <= '" + reportYear + "' AND name_en >= '" + routeFiscalYear.nameEn + "' ";
                List<Fiscal_yearDTO> fiscal_yearDTOS = fiscal_yearDAO.getDTOs(null, -1, -1, true, userDTO, fiscalFilter, false);


                List<String> fiscalYears = fiscal_yearDTOS
                        .stream()
                        .map(fiscal_yearDTO ->
                                Language.equals("English") ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn)
                        .collect(Collectors.toList());


                String startDate = sf.format(new Date(vm_route_travelDTO.approvedStartDate));
                String withDrawalDate = sf.format(new Date(vm_route_travelDTO.withdrawalDate));

                int startYear = Integer.parseInt(startDate.substring(6));
                int withDrawalYear = Integer.parseInt(withDrawalDate.substring(6));

                int startMonth = Integer.parseInt(startDate.substring(3, 5));
                int withDrawalMonth = Integer.parseInt(withDrawalDate.substring(3, 5));

                String startYearString = startMonth > 6 ? String.valueOf(startYear) : String.valueOf(startYear - 1);
                String withDrawalYearString = withDrawalMonth > 6 ? String.valueOf(withDrawalYear) : String.valueOf(withDrawalYear - 1);

                startMonth = startMonth > 6 ? (startMonth - 7) : (startMonth + 5);
                withDrawalMonth = withDrawalMonth > 6 ? (withDrawalMonth - 7) : (withDrawalMonth + 5);

                HashMap<Long, Boolean[]> fiscalYearToPayment = new HashMap<>();


                for (Vm_route_travel_paymentDTO vm_route_travel_paymentDTO : vm_route_travel_paymentDTOList) {
//		request.setAttribute("vm_route_travel_paymentDTO",vm_route_travel_paymentDTO);
                    if (!fiscalYearToPayment.containsKey(vm_route_travel_paymentDTO.fiscalYearId)) {
                        Boolean[] payment = new Boolean[12];
//			Arrays.fill(payment, Boolean.FALSE);
                        payment[vm_route_travel_paymentDTO.month] = vm_route_travel_paymentDTO.isPaid;

                        fiscalYearToPayment.put(vm_route_travel_paymentDTO.fiscalYearId, payment);
                    }
                }
                int fiscalNumbers = fiscalYears.size();
                for (int fiscalIndex = 0; fiscalIndex < fiscalNumbers; fiscalIndex++) {
                    Fiscal_yearDTO fiscal_yearDTO = fiscal_yearDTOS.get(fiscalIndex);
                    Boolean[] payment = fiscalYearToPayment.get(fiscal_yearDTO.id);
                    if (payment == null) {
                        payment = new Boolean[12];
                        Arrays.fill(payment, Boolean.FALSE);
                    }
                    String paymentStr = "";
                    for (int monthIndex = 0; monthIndex < 12; monthIndex++) {
                        if (!((fiscal_yearDTO.nameEn.contains(startYearString) && (monthIndex < startMonth))
                                || (fiscal_yearDTO.nameEn.contains(withDrawalYearString) && (monthIndex > withDrawalMonth)))) {
                            paymentStr += " " + (payment[monthIndex]);
                        } else paymentStr += " " + "null";
                    }
                    paymentStr = paymentStr.substring(1);

        %>
        <tr>
            <jsp:include page="./vm_route_travelSearchPaymentRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=fiscalIndex%>"/>
                <jsp:param name="fiscalYear" value="<%=fiscalYears.get(fiscalIndex)%>"/>
                <jsp:param name="payment" value="<%=paymentStr%>"/>
            </jsp:include>
        </tr>
        <%
            }
        %>
        <%}%>

    </table>
</div>