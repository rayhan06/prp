<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="budget_operation.BudgetOperationModel" %>
<%@ page import="economic_code.Economic_codeDTO" %>
<%@ page import="common.NameDTO" %>
<%@ page import="economic_group.EconomicGroupRepository" %>
<%@ page import="economic_code.Economic_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeDTO" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="java.util.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="static java.util.stream.Collectors.toList" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="budget.*" %>

<%
    String context = request.getContextPath() + "/";
    String currentYear = BudgetUtils.getRunningEconomicYear();
    Map<Long, List<BudgetDTO>> mapByBudgetMappingId;
    String errorMessage = null;
    if (budgetDTOS != null) {
        currentYear = BudgetSelectionInfoRepository.getInstance().getDTOByID(budgetDTOS.get(0).budgetSelectionInfoId).economicYear;
        mapByBudgetMappingId = budgetDTOS.stream()
                .collect(Collectors.groupingBy(dto -> dto.budgetMappingId));
    } else {
        mapByBudgetMappingId = new HashMap<>();
        errorMessage = UtilCharacter.getDataByLanguage(
                Language, "এই সার্চ ক্রাইটেরিয়া তে কোনো ডাটা দেওয়া হয় নি।",
                "No Data entried for these search criteria."
        );
    }
    String economicYear = Utils.getDigits(currentYear, Language);
%>

<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-auto mr-3 mt-4">
    <button type="button" class="btn" id='printer'
            onclick="printDiv('kt_content')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet" style="background: #f9f9f9!important;">
            <div class="kt-portlet__body m-4" style="background: #fff!important;">
                <div class="row">
                    <div class="col-12 row">
                        <div class="offset-3 col-6 text-center">
                            <img width="20%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>

                <%if (errorMessage != null) {%>
                <div class="container text-center m-4">
                    <h5>
                        <%=errorMessage%>
                    </h5>
                </div>
                <%}%>

                <%
                    for (Map.Entry<Long, List<BudgetDTO>> entry : mapByBudgetMappingId.entrySet()) {
                        Long budgetMappingId = entry.getKey();
                        List<BudgetDTO> budgetDTOsByMapping = entry.getValue();
                        BudgetOperationModel model = Budget_mappingRepository.getInstance()
                                .getBudgetOperationModelWithoutSubCode(budgetMappingId, currentYear, Language);
                %>
                <div class="row mt-5 mx-0 py-3 rounded" style="border: 1px solid #00ACD8;">
                    <div class="col-9 text-left">
                        <h5 class="text-left mt-3 top-section-font" id="budgetCatHeading">
                            <%=model.budgetCatName + " - " + model.officeCode%>
                        </h5>
                        <h5 class="text-left mt-3 top-section-font" id="operationalCodeHeading">
                            <%=model.description + " - " + model.operationCode%>
                        </h5>
                    </div>
                    <div class="col-3">
                        <h5 class="text-right mt-3 top-section-font" id="economicYearHeading">
                            <%=model.economicYear%>
                        </h5>
                    </div>
                </div>

                <div class="row mt-5 page-break">
                    <div class="w-100">
                        <h5 class="text-right">(<%=LM.getText(LC.BUDGET_MONEY_INPUT_UNIT_HEADER, loginDTO)%>)</h5>
                        <table class="table text-nowrap" id="budget-view-table">
                            <thead>
                            <tr>
                                <th>
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </th>
                                <th>
                                    <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                </th>

                                <%--Conditional Heading--%>
                                <th>
                                    <%=CatRepository.getInstance().getText(Language, "budget_type", BudgetTypeEnum.BUDGET.getValue())%>
                                    <br>
                                    <%=economicYear%>
                                </th>
                                <th>
                                    <%=LM.getText(LC.BUDGET_SEARCH_FINAL_BUDGET_COMMENT, loginDTO)%>
                                </th>
                                <th>
                                    <%=CatRepository.getInstance().getText(Language, "budget_type", BudgetTypeEnum.REVISED_BUDGET.getValue())%>
                                    <br>
                                    <%=economicYear%>
                                </th>
                                <th>
                                    <%=LM.getText(LC.BUDGET_SEARCH_REVISED_BUDGET_COMMENT, loginDTO)%>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <%--Economic Groups--%>
                            <%
                                TreeMap<Long, List<BudgetDTO>> sortedMapByEconomicGroup =
                                        budgetDTOsByMapping.stream()
                                                           .collect(Collectors.groupingBy(
                                                                   dto -> dto.economicGroupId,
                                                                   TreeMap::new,
                                                                   toList()
                                                           ));

                                for (Map.Entry<Long, List<BudgetDTO>> groupBudgedPair : sortedMapByEconomicGroup.entrySet()) {
                                    Long economicGroupId = groupBudgedPair.getKey();
                                    List<BudgetDTO> budgetDTOsByGroup = groupBudgedPair.getValue();
                                    NameDTO groupDTO = EconomicGroupRepository.getInstance().getDTOByID(economicGroupId);
                                    if (groupDTO == null) {
                                        groupDTO = new NameDTO();
                                    }
                            %>
                            <tr>
                                <td style=" font-weight: bold" colspan="100%">
                                    <%
                                        String groupID = StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(groupDTO.iD));%>
                                    <%=groupID + " - " + UtilCharacter.getDataByLanguage(Language, groupDTO.nameBn, groupDTO.nameEn)%>
                                </td>
                            </tr>

                            <%--Economic Code--%>
                            <%
                                Map<Long, List<BudgetDTO>> mapByEconomicCode = budgetDTOsByGroup.stream()
                                                                                                .collect(Collectors.groupingBy(dto -> dto.economicCodeId));

                                List<Long> economicCodeIdSortedByCode = mapByEconomicCode.keySet()
                                                                                         .stream().sorted(BudgetUtils::compareEconomicCodeId)
                                                                                         .collect(toList());

                                for (Long economicCodeId : economicCodeIdSortedByCode) {
                                    List<BudgetDTO> budgetDTOsByCode = mapByEconomicCode.get(economicCodeId);
                                    Economic_codeDTO codeDTO = Economic_codeRepository.getInstance().getById(economicCodeId);
                                    if (codeDTO == null) {
                                        codeDTO = new Economic_codeDTO();
                                    }
                            %>
                            <tr>
                                <td style="font-weight: bold" colspan="200%">
                                    <%
                                        String economicCode = StringUtils.convertBanglaIfLanguageIsBangla(Language, codeDTO.code);%>
                                    <%= economicCode + " - "
                                        + UtilCharacter.getDataByLanguage(Language, codeDTO.descriptionBn, codeDTO.descriptionEn)%>
                                </td>
                            </tr>

                            <%--Economic Sub Code--%>
                            <%
                                budgetDTOsByCode.sort(BudgetUtils::compareBudgetDTOonSubCode);

                                for (BudgetDTO budgetDTOofSubCode : budgetDTOsByCode) {
                                    Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance()
                                                                                                 .getDTOByID(budgetDTOofSubCode.economicSubCodeId);
                                    if (subCodeDTO == null) {
                                        subCodeDTO = new Economic_sub_codeDTO();
                                    }
                            %>
                            <tr>
                                <td>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, subCodeDTO.code)%>
                                </td>
                                <td>
                                    <%=UtilCharacter.getDataByLanguage(Language, subCodeDTO.descriptionBn,
                                            subCodeDTO.descriptionEn)%>
                                </td>

                                <%--Budget Info Goes here--%>
                                <%--Conditional Heading--%>
                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            budgetDTOofSubCode.finalAmount
                                    )%>
                                </td>
                                <td>
                                    <%=budgetDTOofSubCode.finalAmountComment%>
                                </td>
                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            budgetDTOofSubCode.revisedFinalAmount
                                    )%>
                                </td>
                                <td>
                                    <%=budgetDTOofSubCode.revisedFinalComment%>
                                </td>
                            </tr>
                            <%}%> <%--end for economic sub group--%>
                            <%--Sub Total of Economic Codes--%>
                            <tr>
                                <td colspan="2" style="font-weight: bold" class="text-right">
                                    <%=LM.getText(LC.BUDGET_SUBTOTAL, loginDTO)%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double amountSumByCode = budgetDTOsByCode.stream()
                                                                                 .mapToDouble(dto -> dto.finalAmount)
                                                                                 .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            amountSumByCode
                                    )%>
                                </td>
                                <td>
                                    <%--Place horder on comment--%>
                                </td>
                                <td class="text-right">
                                    <%
                                        Double revisedAmountSumByCode = budgetDTOsByCode.stream()
                                                                                        .mapToDouble(dto -> dto.revisedFinalAmount)
                                                                                        .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            revisedAmountSumByCode
                                    )%>
                                </td>
                                <td>
                                    <%--Place horder on comment--%>
                                </td>
                            </tr>
                            <%}%> <%--end for economic code--%>
                            <%}%> <%--end for economic group--%>
                            <tr>
                                <td colspan="2" style="font-weight: bold" class="text-right">
                                    <%=LM.getText(LC.BUDGET_TOTAL, loginDTO)%> - <%=model.description%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double amountSumByMapping = budgetDTOsByMapping.stream()
                                                                                       .mapToDouble(dto -> dto.finalAmount)
                                                                                       .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            amountSumByMapping
                                    )%>
                                </td>
                                <td>
                                    <%--Place horder on comment--%>
                                </td>
                                <td class="text-right">
                                    <%
                                        Double revisedAmountSumByMapping = budgetDTOsByMapping.stream()
                                                                                              .mapToDouble(dto -> dto.revisedFinalAmount)
                                                                                              .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            revisedAmountSumByMapping
                                    )%>
                                </td>
                                <td>
                                    <%--Place horder on comment--%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%}%> <%--end for office--%>
            </div>
        </div>
    </div>
</div>

<script>
    function printDiv(divName) {
        let button = $("#button-div");
        if (button.length) {
            button.hide();
        }
        let printContents = document.getElementById(divName).innerHTML;
        let originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;

        if (button.length) {
            $("#button-div").show();
        }
    }
</script>