

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="budget.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
String servletName = "BudgetServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.BUDGET_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
BudgetDAO budgetDAO = BudgetDAO.getInstance();
BudgetDTO budgetDTO = budgetDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title"><%=LM.getText(LC.BUDGET_ADD_BUDGET_ADD_FORMNAME, loginDTO)%></h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.BUDGET_ADD_BUDGET_ADD_FORMNAME, loginDTO)%></h3>
						<table class="table table-bordered table-striped">
									
			
			
			
			
			
			
			
			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_AMOUNT, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.amount + "";
											%>
											<%
											value = String.format("%.1f", budgetDTO.amount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_AMOUNTINSERTBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.amountInsertBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_AMOUNTINSERTIONTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.amountInsertionTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_FINALAMOUNT, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.finalAmount + "";
											%>
											<%
											value = String.format("%.1f", budgetDTO.finalAmount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_FINALAMOUNTINSERTBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.finalAmountInsertBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_FINALAMOUNTINSERTIONTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.finalAmountInsertionTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDAMOUNT, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedAmount + "";
											%>
											<%
											value = String.format("%.1f", budgetDTO.revisedAmount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDCOMMENT, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedComment + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDAMOUNTINSERTBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedAmountInsertBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDAMOUNTINSERTIONTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedAmountInsertionTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDFINALAMOUNT, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedFinalAmount + "";
											%>
											<%
											value = String.format("%.1f", budgetDTO.revisedFinalAmount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDFINALCOMMENT, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedFinalComment + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDFINALAMOUNTINSERTBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedFinalAmountInsertBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDFINALAMOUNTINSERTIONTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedFinalAmountInsertionTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_EXPENDITUREAMOUNT, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.expenditureAmount + "";
											%>
											<%
											value = String.format("%.1f", budgetDTO.expenditureAmount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_EXPENDITUREINSERTBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.expenditureInsertBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_EXPENDITUREINSERTIONTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.expenditureInsertionTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDEXPENDITUREAMOUNT, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedExpenditureAmount + "";
											%>
											<%
											value = String.format("%.1f", budgetDTO.revisedExpenditureAmount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDEXPENDITUREINSERTBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedExpenditureInsertBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.BUDGET_ADD_REVISEDEXPENDITUREINSERTIONTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = budgetDTO.revisedExpenditureInsertionTime + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			
			
			
		
						</table>
                    </div>
			






			</div>	

               


        </div>
	</div>