<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="budget.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>

<%
    BudgetDTO budgetDTO;
    budgetDTO = (BudgetDTO) request.getAttribute("budgetDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (budgetDTO == null) {
        budgetDTO = new BudgetDTO();

    }
    System.out.println("budgetDTO = " + budgetDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.BUDGET_ADD_BUDGET_ADD_FORMNAME, loginDTO);
    String servletName = "BudgetServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.BUDGET_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="BudgetServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-10  offset-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10  offset-1">
                                    <input type='hidden' class='form-control' name='budgetOfficeId' id='budgetOfficeId'
                                           value='1' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-sm-4 col-xl-3 col-form-label text-right">
                                            Budget Operational Group
                                        </label>
                                        <div class="col-sm-8 col-xl-9">
                                            <select id="budgetOperationId" class='form-control rounded'>
                                                <%=Budget_mappingRepository.getInstance().buildBudgetOperationCat(Language, 1, 0L)%>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-4 col-xl-3 col-form-label text-right">
                                            Economic Year
                                        </label>
                                        <div class="col-sm-8 col-xl-9">
                                            <select id="budgetSelectionInfoId" class='form-control rounded'
                                                    onchange="budgetSelectionChanged(this);">
                                                <%=BudgetSelectionInfoRepository.getInstance().buildEconomicYears(Language, 0L)%>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-xl-3 col-form-label text-right">
                                            Economic Group
                                        </label>
                                        <div class="col-sm-8 col-xl-9">
                                            <select id="economicGroup" class='form-control rounded'
                                                    onchange="economicGroupChanged(this);">
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-4 col-xl-3 col-form-label text-right">
                                            Economic Codes
                                        </label>
                                        <div class="col-sm-8 col-xl-9">
                                            <select id="economicCodes" class='form-control rounded'
                                                    onchange="economicCodesChanged(this);">
                                                <%--Dynamically Added with AJAX--%>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row mt-5">
                                        <div class="table-responsive">
                                            <table class="table" id="budget-view-table">
                                                <thead>
                                                <tr>
                                                    <th class="row-data-code">
                                                        <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                                    </th>
                                                    <th class="row-data-name">
                                                        <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                                    </th>
                                                    <th class="row-data-budget">
                                                        Budget Amount
                                                    </th>

                                                </tr>
                                                </thead>

                                                <tbody></tbody>

                                                <tr class="template-row">
                                                    <td class="row-data-code"></td>
                                                    <td class="row-data-name"></td>
                                                    <td class="row-data-budget">
                                                    </td>

                                                </tr>
                                            </table>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const budgetModelMap = new Map();

    function PreprocessBeforeSubmiting(row, validate) {


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "BudgetServlet");
    }

    function init(row) {
        clearTable();

    }

    function clearSelects(startIndex) {
        const selectIds = ['economicGroup', 'economicCodes'];
        for (let i = startIndex; i < selectIds.length; i++) {
            const selectElement = document.getElementById(selectIds[i]);
            selectElement.innerHTML = '';
        }
    }

    function addDataInTable(tableId, jsonData, toInsertInEconomicGroup) {
        const table = document.getElementById(tableId);

        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.id = jsonData.economicSubCode;
        templateRow.classList.remove('template-row');

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-code').innerText = jsonData.code;
        templateRow.querySelector('td.row-data-name').innerText = jsonData.name;
        templateRow.querySelector('td.row-data-budget').innerHTML = '<input type=\'text\' id = \'budget_' + jsonData.budgetId + '\' tag=\'pb_html\'/>'

        const tableBody = table.querySelector('tbody');
        tableBody.append(templateRow);
    }

    function clearTable() {
        budgetModelMap.clear();
        document.getElementById('budget-view-table').querySelector('tbody').innerHTML = '<tr><td colspan="4" class="text-center">No data found!</td></tr>';
    }

    async function budgetSelectionChanged(selectElement) {
        const selectedSelectionCat = selectElement.value;
        clearSelects(0);
        if (selectedSelectionCat === '') return;

        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOfficeId').value;
        const url = 'BudgetServlet?actionType=buildEconomicGroups&budgetSelectionInfoId='
            + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup;
        const response = await fetch(url);
        document.getElementById('economicGroup').innerHTML = await response.text();
    }


    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroup = selectElement.value;
        clearSelects(1);
        if (selectedEconomicGroup === '') return;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOfficeId').value;
        const url = 'BudgetServlet?actionType=buildEconomicCodes&budgetSelectionInfoId='
            + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
            + '&economicGroupId=' + selectedEconomicGroup;
        const response = await fetch(url);
        document.getElementById('economicCodes').innerHTML = await response.text();
    }


    async function economicCodesChanged(selectElement) {
        const selectedEconomicCode = selectElement.value;
        clearTable();
        if (selectedEconomicCode === '') return;
        const selectedEconomicGroup = document.getElementById('economicGroup').value;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOfficeId').value;
        const url = 'BudgetServlet?actionType=getEconomicCodesForOffice&budgetSelectionInfoId='
            + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
            + '&economicGroupId=' + selectedEconomicGroup + '&economicCodeId=' + selectedEconomicCode;
        const response = await fetch(url);
        const budgetModelsJson = await response.json();
        const budgetModels = budgetModelsJson;
        console.log(budgetModels)
        document.getElementById('budget-view-table').querySelector('tbody').innerHTML = '';
        if (Array.isArray(budgetModels)) {
            budgetModels.forEach(budgetModel => {
                addDataInTable('budget-view-table', budgetModel, true);
                budgetModelMap.set(
                    Number(budgetModel.budgetId),
                    budgetModel
                );

            });
        }
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






