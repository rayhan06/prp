<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeDTO" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="util.StringUtils" %>
<%
    Economic_sub_codeDTO sub_codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(budgetDTO.economicSubCodeId);
%>
<td>
    <%=BudgetSelectionInfoRepository.getInstance().getEconomicYearById(Language, budgetDTO.budgetSelectionInfoId)%>
</td>


<td>
    <%=Budget_operationRepository.getInstance().getTextById(Language, budgetDTO.budgetOperationId)%>
</td>


<td>
    <%
        String value;
        String subCodeId = sub_codeDTO.code;
        if (!Language.equalsIgnoreCase("ENGLISH")) {
            subCodeId = StringUtils.convertToBanNumber(subCodeId);
            value=subCodeId+"-"+sub_codeDTO.descriptionBn;
        }else{
            value=subCodeId+"-"+sub_codeDTO.descriptionEn;
        }
    %>
    <%=value%>
</td>
<td>
    <%=Utils.getDigits(budgetDTO.amount, Language)%>
</td>

<td>
    <%=Utils.getDigits(budgetDTO.finalAmount, Language)%>
</td>

<td>
    <%=Utils.getDigits(budgetDTO.revisedAmount, Language)%>
</td>

<td>
    <%=Utils.getDigits(budgetDTO.revisedFinalAmount, Language)%>
</td>

<td>
    <%=Utils.getDigits(budgetDTO.expenditureAmount, Language)%>
</td>

<td>
    <%=Utils.getDigits(budgetDTO.revisedExpenditureAmount, Language)%>
</td>

																						
											

