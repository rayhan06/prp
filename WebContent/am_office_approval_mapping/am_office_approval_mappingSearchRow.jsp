<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.CommonDTO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="am_office_assignment_request.AmOfficeAssignmentRequestStatus" %>

<td>
    <%=UtilCharacter.getDataByLanguage(Language,am_office_assignment_requestDTO.requesterNameBn,am_office_assignment_requestDTO.requesterNameEn)%>
</td>

<td>
    <%=UtilCharacter.getDataByLanguage(Language,am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn,am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn)%>
</td>

<td>
    <%=UtilCharacter.getDataByLanguage(Language,am_office_assignment_requestDTO.requesterOfficeUnitNameBn,am_office_assignment_requestDTO.requesterOfficeUnitNameEn)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language,am_office_assignment_requestDTO.insertionDate)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language,am_office_assignment_requestDTO.assignmentDate)%>
</td>

<td>
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=AmOfficeAssignmentRequestStatus.getColor(am_office_approval_mappingDTO.amOfficeAssignmentStatusCat)%>; color: white; border-radius: 8px;cursor: text">
            <%=CatRepository.getInstance().getText(
                    Language, "am_office_assignment_request_status", am_office_approval_mappingDTO.amOfficeAssignmentStatusCat
            )%>
    </span>
</td>

<%CommonDTO commonDTO = am_office_approval_mappingDTO; %>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amOfficeAssignmentRequestId=<%=am_office_assignment_requestDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>
