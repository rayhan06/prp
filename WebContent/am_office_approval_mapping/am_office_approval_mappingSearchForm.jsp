
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_office_approval_mapping.*"%>
<%@ page import="util.*"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDTO" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDAO" %>


<%
String navigator2 = "navAM_OFFICE_APPROVAL_MAPPING";
String servletName = "Am_office_approval_mappingServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_APPLICATIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ALLOCATION_REQUEST_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
							</tr>
						</thead>
						<tbody>

							<%
								List<Am_office_approval_mappingDTO> data = (List<Am_office_approval_mappingDTO>) rn2.list;
								if (data != null && data.size() > 0) {
									for (Am_office_approval_mappingDTO am_office_approval_mappingDTO : data) {

										System.out.println("am_office_approval_mappingDTO in jsp:" +am_office_approval_mappingDTO);

										Am_office_assignment_requestDTO am_office_assignment_requestDTO =  Am_office_assignment_requestDAO.getInstance().getDTOFromID(am_office_approval_mappingDTO.amOfficeAssignmentRequestId);


							%>          <%if(am_office_assignment_requestDTO!=null){%>
											<tr>
												<%@include file="am_office_approval_mappingSearchRow.jsp" %>
											</tr>
										<%}%>
								<% }
							} %>

						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			