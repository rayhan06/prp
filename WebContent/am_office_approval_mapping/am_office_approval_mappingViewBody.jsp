

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_office_approval_mapping.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Am_office_approval_mappingServlet";
String ID = request.getParameter("ID");
Am_office_approval_mappingDAO am_office_approval_mappingDAO = new Am_office_approval_mappingDAO("am_office_approval_mapping");
long id = Long.parseLong(ID);
Am_office_approval_mappingDTO am_office_approval_mappingDTO = (Am_office_approval_mappingDTO)am_office_approval_mappingDAO.getDTOByID(id);
CommonDTO commonDTO = am_office_approval_mappingDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_AM_OFFICE_APPROVAL_MAPPING_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_AM_OFFICE_APPROVAL_MAPPING_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_AMOFFICEASSIGNMENTREQUESTID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.amOfficeAssignmentRequestId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_CARDAPPROVALID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.cardApprovalId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_SEQUENCE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.sequence + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_AMOFFICEASSIGNMENTSTATUSCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.amOfficeAssignmentStatusCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "am_office_assignment_status", am_office_approval_mappingDTO.amOfficeAssignmentStatusCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_TASKTYPEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.taskTypeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.employeeRecordsId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_APPROVEREMPLOYEERECORDSID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.approverEmployeeRecordsId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_INSERTEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.insertedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_APPROVAL_MAPPING_ADD_MODIFIEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_office_approval_mappingDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>