<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="employee_absent_approved_history.Employee_absent_approved_historyDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = (Employee_absent_approved_historyDTO)request.getAttribute("employee_absent_approved_historyDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(employee_absent_approved_historyDTO == null)
{
	employee_absent_approved_historyDTO = new Employee_absent_approved_historyDTO();
	
}
System.out.println("employee_absent_approved_historyDTO = " + employee_absent_approved_historyDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

String servletName = "Employee_absent_approved_historyServlet";
String fileColumnName = "";
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsId'>")%>
			

		<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.employeeRecordsId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_approvedByEmpRecordsId'>")%>
			

		<input type='hidden' class='form-control'  name='approvedByEmpRecordsId' id = 'approvedByEmpRecordsId_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.approvedByEmpRecordsId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_absentDate'>")%>
			
	
	<div class="form-inline" id = 'absentDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'absentDate_date_<%=i%>' name='absentDate' value=	<%
	String formatted_absentDate = dateFormat.format(new Date(employee_absent_approved_historyDTO.absentDate));
	%>
	'<%=formatted_absentDate%>'
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_approvalDate'>")%>
			
	
	<div class="form-inline" id = 'approvalDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'approvalDate_date_<%=i%>' name='approvalDate' value=	<%
	String formatted_approvalDate = dateFormat.format(new Date(employee_absent_approved_historyDTO.approvalDate));
	%>
	'<%=formatted_approvalDate%>'
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_comments'>")%>
			
	
	<div class="form-inline" id = 'comments_div_<%=i%>'>
		<input type='text' class='form-control'  name='comments' id = 'comments_text_<%=i%>' value='<%=employee_absent_approved_historyDTO.comments%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy'>")%>
			
	
	<div class="form-inline" id = 'insertedBy_div_<%=i%>'>
		<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=employee_absent_approved_historyDTO.insertedBy%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy'>")%>
			
	
	<div class="form-inline" id = 'modifiedBy_div_<%=i%>'>
		<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=employee_absent_approved_historyDTO.modifiedBy%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=employee_absent_approved_historyDTO.isDeleted%>' tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Employee_absent_approved_historyServlet?actionType=view&ID=<%=employee_absent_approved_historyDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Employee_absent_approved_historyServlet?actionType=view&modal=1&ID=<%=employee_absent_approved_historyDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	