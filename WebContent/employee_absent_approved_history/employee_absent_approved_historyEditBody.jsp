
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="employee_absent_approved_history.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>

<%
Employee_absent_approved_historyDTO employee_absent_approved_historyDTO;
employee_absent_approved_historyDTO = (Employee_absent_approved_historyDTO)request.getAttribute("employee_absent_approved_historyDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(employee_absent_approved_historyDTO == null)
{
	employee_absent_approved_historyDTO = new Employee_absent_approved_historyDTO();
	
}
System.out.println("employee_absent_approved_historyDTO = " + employee_absent_approved_historyDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_FORMNAME, loginDTO);
String servletName = "Employee_absent_approved_historyServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Employee_absent_approved_historyServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.employeeRecordsId%>' tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='approvedByEmpRecordsId' id = 'approvedByEmpRecordsId_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.approvedByEmpRecordsId%>' tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_ABSENTDATE, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'absentDate_div_<%=i%>'>	
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'absentDate_date_<%=i%>' name='absentDate' value=	<%
	String formatted_absentDate = dateFormat.format(new Date(employee_absent_approved_historyDTO.absentDate));
	%>
	'<%=formatted_absentDate%>'
%>
   tag='pb_html'>
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_APPROVALDATE, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'approvalDate_div_<%=i%>'>	
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'approvalDate_date_<%=i%>' name='approvalDate' value=	<%
	String formatted_approvalDate = dateFormat.format(new Date(employee_absent_approved_historyDTO.approvalDate));
	%>
	'<%=formatted_approvalDate%>'
%>
   tag='pb_html'>
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_COMMENTS, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'comments_div_<%=i%>'>	
		<input type='text' class='form-control'  name='comments' id = 'comments_text_<%=i%>' value='<%=employee_absent_approved_historyDTO.comments%>'   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.insertionDate%>' tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_INSERTEDBY, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'insertedBy_div_<%=i%>'>	
		<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=employee_absent_approved_historyDTO.insertedBy%>'   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_MODIFIEDBY, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'modifiedBy_div_<%=i%>'>	
		<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=employee_absent_approved_historyDTO.modifiedBy%>'   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=employee_absent_approved_historyDTO.isDeleted%>' tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=employee_absent_approved_historyDTO.lastModificationTime%>' tag='pb_html'/>
												
					
	






				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">					
						<%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_EMPLOYEE_ABSENT_APPROVED_HISTORY_CANCEL_BUTTON, loginDTO)%>						
					</a>
					<button class="btn btn-success" type="submit">
					
						<%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_ADD_EMPLOYEE_ABSENT_APPROVED_HISTORY_SUBMIT_BUTTON, loginDTO)%>						
					
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script type="text/javascript">


$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Employee_absent_approved_historyServlet");	
}

function init(row)
{


	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






