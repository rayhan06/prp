<%@page pageEncoding="UTF-8" %>

<%@page import="employee_absent_approved_history.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_EMPLOYEE_ABSENT_APPROVED_HISTORY;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Employee_absent_approved_historyDTO employee_absent_approved_historyDTO = (Employee_absent_approved_historyDTO)request.getAttribute("employee_absent_approved_historyDTO");
CommonDTO commonDTO = employee_absent_approved_historyDTO;
String servletName = "Employee_absent_approved_historyServlet";


System.out.println("employee_absent_approved_historyDTO = " + employee_absent_approved_historyDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Employee_absent_approved_historyDAO employee_absent_approved_historyDAO = (Employee_absent_approved_historyDAO)request.getAttribute("employee_absent_approved_historyDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_employeeRecordsId'>
											<%
											value = employee_absent_approved_historyDTO.employeeRecordsId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_approvedByEmpRecordsId'>
											<%
											value = employee_absent_approved_historyDTO.approvedByEmpRecordsId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_absentDate'>
											<%
											value = employee_absent_approved_historyDTO.absentDate + "";
											%>
											<%
											String formatted_absentDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_absentDate, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_approvalDate'>
											<%
											value = employee_absent_approved_historyDTO.approvalDate + "";
											%>
											<%
											String formatted_approvalDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_approvalDate, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_comments'>
											<%
											value = employee_absent_approved_historyDTO.comments + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
											<td id = '<%=i%>_insertedBy'>
											<%
											value = employee_absent_approved_historyDTO.insertedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_modifiedBy'>
											<%
											value = employee_absent_approved_historyDTO.modifiedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
	

											<td>
												<a href='Employee_absent_approved_historyServlet?actionType=view&ID=<%=employee_absent_approved_historyDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Employee_absent_approved_historyServlet?actionType=getEditPage&ID=<%=employee_absent_approved_historyDTO.iD%>'><%=LM.getText(LC.EMPLOYEE_ABSENT_APPROVED_HISTORY_SEARCH_EMPLOYEE_ABSENT_APPROVED_HISTORY_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=employee_absent_approved_historyDTO.iD%>'/></span>
												</div>
											</td>
																						
											

