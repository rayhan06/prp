<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="payroll_daily_configuration.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="employee_records.EmploymentEnum" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="employee_records.Employee_recordsRepository" %>

<%
    String context = request.getContextPath() + "/";
    Payroll_daily_configurationDTO payroll_daily_configurationDTO = new Payroll_daily_configurationDTO();
    long ID = 0;
    if (request.getParameter("ID") != null) {
        payroll_daily_configurationDTO = (Payroll_daily_configurationDTO)request.getAttribute(BaseServlet.DTO_FOR_JSP);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = payroll_daily_configurationDTO;
    String tableName = "payroll_daily_configuration";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_PAYROLL_DAILY_CONFIGURATION_ADD_FORMNAME, loginDTO);
    String servletName = "Payroll_daily_configurationServlet";
    int DAILY_EMPLOYEE_CAT= EmploymentEnum.DAILY_BASIS.getValue();
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform"  enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=payroll_daily_configurationDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_ORGANOGRAMKEY, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='organogramId'
                                                    id='organogramKey_select2_<%=i%>' tag='pb_html'>
                                                <%=Employee_recordsRepository.getInstance().buildDesignationOption(
                                                        DAILY_EMPLOYEE_CAT,
                                                        payroll_daily_configurationDTO.organogramKey,
                                                        Language
                                                )%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_DAILYAMOUNT, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='dailyAmount'
                                                   id='dailyAmount_number_<%=i%>' value='<%=payroll_daily_configurationDTO.dailyAmount%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_TAXDEDUCTIONAMOUNTTYPE, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='taxDeductionAmountType'
                                                    id='taxDeductionAmountType_select2_<%=i%>' tag='pb_html' onchange="changeKeyDownEvent()">
                                                <%=
                                                    CatRepository.getInstance().buildOptions("allowance_amount", Language, payroll_daily_configurationDTO.taxDeductionAmountType)
                                                %>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_TAXDEDUCTIONAMOUNT, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='taxDeductionAmount'
                                                   id='taxDeductionAmount_text_<%=i%>'
                                                   value='<%=payroll_daily_configurationDTO.taxDeductionAmount%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                                <%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_PAYROLL_DAILY_CONFIGURATION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitPayrollDailyConfigurationForm()">
                                <%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_PAYROLL_DAILY_CONFIGURATION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const bigForm = $('#bigform');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    function PreprocessBeforeSubmiting(row, action) {

    }
    function submitPayrollDailyConfigurationForm() {
        if (bigForm.valid()) {
            setButtonState(true);
            $.ajax({
                type: "POST",
                url: "Payroll_daily_configurationServlet?actionType=<%=actionName%>&isPermanentTable=true&id=<%=ID%>",
                data: bigForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        setButtonState(false);
                        console.log("response code 0");
                    } else if (response.responseCode === 200) {
                        console.log("repsonse code 200");
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    setButtonState(false);
                }
            });
        }
    }

    function setButtonState(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Payroll_daily_configurationServlet");
    }
    function keyDownEvent(e) {
        console.log(e);
        let isvalid = inputValidationForIntValue(e, $(this), 100000000);
        return true == isvalid;
    }
    function keyDownEvent2(e) {
        let isvalid = inputValidationForIntValue(e, $(this), 100);
        return true == isvalid;
    }
    function changeKeyDownEvent(){
        document.getElementById('taxDeductionAmount_text_0').value = '';
        if($("#taxDeductionAmountType_select2_0").val()=='1'){
            document.getElementById('taxDeductionAmount_text_0').onkeydown = keyDownEvent;
        }else{
            document.getElementById('taxDeductionAmount_text_0').onkeydown = keyDownEvent2;
        }
    }
    function init(row) {
        select2SingleSelector("#organogramKey_select2_0", '<%=Language%>');
        select2SingleSelector("#taxDeductionAmountType_select2_0", '<%=Language%>');
        document.getElementById('dailyAmount_number_0').onkeydown = keyDownEvent;
        document.getElementById('taxDeductionAmount_text_0').onkeydown = keyDownEvent2;
        $.validator.addMethod('organogramIdSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('taxDeductionAmountTypeSelection', function (value, element) {
            return value != 0;
        });
        bigForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                organogramId: {
                    required: true,
                    organogramIdSelection: true
                },
                taxDeductionAmountType: {
                    required: true,
                    taxDeductionAmountTypeSelection: true
                },
                dailyAmount: {
                    required: true,
                },
                taxDeductionAmount: {
                    required: true,
                },
            },

            messages: {
                organogramId: isLangEng ? "Please Select Designation" : "অনুগ্রহ করে পদবী নির্বাচন করুন",
                taxDeductionAmountType: isLangEng ? "Please Select Amount Type" : "অনুগ্রহ করে পরিমাণের ধরণ প্রবেশ করুন",
                dailyAmount: isLangEng ? "Please Enter Amount" : "অনুগ্রহ করে পরিমাণ প্রবেশ করুন",
                taxDeductionAmount: isLangEng ? "Please Enter Amount(%)" : "অনুগ্রহ করে পরিমাণ(%) প্রবেশ করুন",
            }
        });
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






