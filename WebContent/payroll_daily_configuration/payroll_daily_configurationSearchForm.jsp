<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="payroll_daily_configuration.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="office_unit_organograms.SameDesignationGroup" %>
<%@ page import="java.util.List" %>
<%@ page import="allowance_configure.AllowanceTypeEnum" %>

<%
    String navigator2 = "navPAYROLL_DAILY_CONFIGURATION";
    String servletName = "Payroll_daily_configurationServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_ORGANOGRAMKEY, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_ADD_DAILYAMOUNT, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.RELIGION_ALLOWANCE_SEARCH_TAXDEDUCTION, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.PAYROLL_DAILY_CONFIGURATION_SEARCH_PAYROLL_DAILY_CONFIGURATION_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Payroll_daily_configurationDTO> data = (List<Payroll_daily_configurationDTO>) rn2.list;
            try {
                if (data != null) {
                    for (Payroll_daily_configurationDTO payroll_daily_configurationDTO : data) {
        %>
        <tr>
            <td>
                <%
                    SameDesignationGroup group = new SameDesignationGroup(payroll_daily_configurationDTO.organogramId);
                %>
                <%=isLanguageEnglish ? group.nameEn : group.nameBn%>
            </td>
            <td>
                <%=Utils.getDigits(payroll_daily_configurationDTO.dailyAmount, Language)%>/-
            </td>
            <td>
                <%=Utils.getDigits(payroll_daily_configurationDTO.taxDeductionAmount, Language)%><%=AllowanceTypeEnum.getSymbol(payroll_daily_configurationDTO.taxDeductionAmountType)%>
            </td>

            <%CommonDTO commonDTO = payroll_daily_configurationDTO; %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=payroll_daily_configurationDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>