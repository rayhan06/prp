<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.Utils" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="javax.sound.midi.Soundbank" %>
<%@ page import="vm_maintenance.Vm_maintenanceRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="vm_vehicle_parts.Vm_vehicle_partsRepository" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%
    String context = request.getContextPath() + "/";

    value = vm_maintenanceDTO.applicationDate + "";
    String formatted_applicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));

    value = UtilCharacter.getDataByLanguage(Language, "গাড়ি মেরামত প্রসঙ্গে ", "Related to Vehicle Maintenance");
    Vm_maintenanceDAO vm_maintenanceDAO = new Vm_maintenanceDAO();
%>

<!-- begin:: Subheader -->
<%--<div class="ml-auto mr-3 mt-4">--%>
<%--    <%--%>
<%--        if (true) {--%>
<%--    %>--%>
<%--    <button type="button" class="btn" id='printer'--%>
<%--            onclick="downloadPdf('prescription.pdf','modalbody')">--%>
<%--        &lt;%&ndash;        <%=LM.getText(LC.HM_PDF, loginDTO)%>&ndash;%&gt;--%>
<%--        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--    </button>--%>
<%--    <button type="button" class="btn" id='printer'--%>
<%--            onclick="printDiv('modalbody')">--%>
<%--        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--        &lt;%&ndash;        <%=LM.getText(LC.HM_PRINT, loginDTO)%>&ndash;%&gt;--%>
<%--    </button>--%>
<%--    <%--%>
<%--        }--%>
<%--    %>--%>
<%--</div>--%>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;">
        <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">
            <form class="form-horizontal"
                  action=""
                  id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                  onsubmit="">
                <div class="row">
                    <div class="col-12 text-center">
                        <img
                                width="10%"
                                src="<%=context%>assets/static/parliament_logo.png"
                                alt="logo"
                                class="logo-default"
                        />
                        <h2 class="mt-2">
                            <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়",
                                    "Bangladesh Parliament Secretariat")%>
                        </h2>
                        <h4>
                            <%=UtilCharacter.getDataByLanguage(Language, "পরিবহন শাখা", "Vehicle Unit")%>
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <%=UtilCharacter.getDataByLanguage(Language, "বিষয়", "Subject")%>: <%=value%>
                    </div>
                    <div class="col-4 text-center table-title">
                        www.parliament.gov.bd
                    </div>
                    <div class="col-4 text-right">
                        <%=LM.getText(LC.VM_MAINTENANCE_ADD_APPLICATIONDATE, loginDTO)%>
                        : <%=Utils.getDigits(formatted_applicationDate, Language)%>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "বিবরণ", "Description")%>
                    </h5>
                    <div class="table responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            </thead>
                            <tbody>
                            <tr id="vehicleIdDiv">
                                <td class="font-weight-bold text-nowrap">
                                    <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEID, loginDTO)%>
                                </td>
                                <%
                                    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_maintenanceDTO.vehicleId);
                                    value = "";
                                    if (vm_vehicleDTO != null) {
                                        value = vm_vehicleDTO.regNo;
                                    }
                                %>
                                <td class="text-nowrap">
                                    <%=value%>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-nowrap">
                                    <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
                                </td>
                                <%
                                    Vm_vehicle_driver_assignmentDTO assignmentDTO = Vm_vehicle_driver_assignmentRepository.getInstance()
                                            .getVm_vehicle_driver_assignmentDTOByID(vm_maintenanceDTO.vehicleDriverAssignmentId);
                                    value = "";
                                    if (assignmentDTO != null) {
                                        value = UtilCharacter.getDataByLanguage(Language,
                                                assignmentDTO.employeeRecordNameBn, assignmentDTO.employeeRecordName);
                                    }
                                %>
                                <td class="text-nowrap">
                                    <%=value%>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-nowrap">
                                    <%=LM.getText(LC.VM_MAINTENANCE_ADD_LASTMAINTENANCEDATE, loginDTO)%>
                                </td>
                                <%
                                    value = "";
                                    if (vm_vehicleDTO != null && vm_vehicleDTO.last_maintenance_date > 0) {
                                        value = simpleDateFormat.format(new Date(Long.parseLong
                                                (vm_vehicleDTO.last_maintenance_date + "")));
                                    }
                                %>
                                <td class="text-nowrap">
                                    <%=Utils.getDigits(value, Language)%>
                                </td>
                            </tr>
                            <%--                            <tr>--%>
                            <%--                                <td class="table-title text-nowrap">--%>
                            <%--                                    <%=UtilCharacter.getDataByLanguage(Language, "বিষয়", "Subject")%>--%>
                            <%--                                </td>--%>
                            <%--                                <td class="text-nowrap">--%>
                            <%--                                    <%=value%>--%>
                            <%--                                </td>--%>
                            <%--                            </tr>--%>
                            <%--                            <tr>--%>
                            <%--                                <td class="table-title text-nowrap">--%>
                            <%--                                    <%=LM.getText(LC.VM_MAINTENANCE_ADD_APPLICATIONDATE, loginDTO)%>--%>
                            <%--                                </td>--%>
                            <%--                                <td class="text-nowrap">--%>
                            <%--                                    <%=Utils.getDigits(formatted_applicationDate, Language)%>--%>
                            <%--                                </td>--%>
                            <%--                            </tr>--%>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial No")%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_VMVEHICLEPARTSTYPE, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_VEHICLEMAINTENANCECAT, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_AMOUNT, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_REMARKS, loginDTO)%>
                                </th>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "পূর্ব মেরামতের তারিখ ", "Last Repair Date")%>
                                </th>
                                <th class="text-nowrap"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_DRIVERPRICE, loginDTO)%>
                                </th>
                                <%--                                    <th><%=UtilCharacter.getDataByLanguage(Language, "টিক দিন ", "Select")%></th>--%>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "মন্তব্য  ", "Remarks")%>
                                </th>
                                <%--                                    <th><%=LM.getText(LC.VM_MAINTENANCE_ADD_VM_MAINTENANCE_ITEM_REMOVE, loginDTO)%></th>--%>
                            </tr>
                            </thead>
                            <tbody id="field-VmMaintenanceItem">

                            <%
                                List<VmMaintenanceItemDTO> items = vm_maintenanceDTO.vmMaintenanceItemDTOList.
                                        stream().filter(i -> i.selected).collect(Collectors.toList());
                                int index = 0;
                                for (VmMaintenanceItemDTO vmMaintenanceItemDTO : items) {
                                    index++;
                            %>

                            <tr>
                                <%--                                    <%--%>
                                <%--                                        System.out.println("#####");--%>
                                <%--                                        System.out.println(vmMaintenanceItemDTO);--%>
                                <%--                                    %>--%>

                                <td class="text-nowrap">
                                    <%=index%>

                                </td>
                                <td>
                                    <input id="" type="hidden" name="itemId" value="<%=vmMaintenanceItemDTO.iD%>">
                                    <%
                                        value = Vm_vehicle_partsRepository.getInstance().getText
                                                (Language, vmMaintenanceItemDTO.vmVehiclePartsType);
                                    %>
                                    <%
//                                        value = CommonDAO.getName(Integer.parseInt(value), "vm_vehicle_parts", Language.equals("English") ? "name_en" : "name_bn", "id");
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td class="text-nowrap">
                                    <%
                                        value = vmMaintenanceItemDTO.vehicleMaintenanceCat + "";
                                    %>
                                    <%
                                        value = CatRepository.getInstance().getText(Language, "vehicle_maintenance", vmMaintenanceItemDTO.vehicleMaintenanceCat);
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td class="text-nowrap">
                                    <%
                                        value = vmMaintenanceItemDTO.amount + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td class="text-nowrap">
                                    <%
                                        value = vmMaintenanceItemDTO.remarks + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td class="text-nowrap">
                                    <%
                                        value = item_mappingDAO.getDate(vm_vehicleDTO.iD, vmMaintenanceItemDTO.vmVehiclePartsType);
                                    %>

                                    <%=Utils.getDigits(value, Language)%>
                                </td>


                                <td class="text-nowrap">
                                    <%
                                        value = "";
                                        if (vmMaintenanceItemDTO.mechanic_price != -1) {
                                            value = vmMaintenanceItemDTO.mechanic_price + "";
                                        }
                                    %>
                                    <input type='number' class='form-control' name='vmMaintenanceItem.mechanicPrice'
                                           id='mechanicPrice_number_<%=index%>' value='<%=value%>' onkeyup="totalSum()"
                                           onchange="totalSum()" tag='pb_html'>
                                </td>
                                <%--                                    <td>--%>
                                <%--										<span id='chkEdit'>--%>
                                <%--											<input type='checkbox' name='vmMaintenanceItem.checkbox' checked deletecb="true"--%>
                                <%--                                                   class="form-control-sm"/>--%>
                                <%--										</span>--%>
                                <%--                                    </td>--%>
                                <td class="text-nowrap">
                                    <%
                                        value = vmMaintenanceItemDTO.mechanic_remarks + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>
                                </td>
                            </tr>

                            <%
                                }
                            %>

                            <tr>

                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <%=UtilCharacter.getDataByLanguage(Language, "মোট আনুমানিক মূল্য ",
                                            "Total estimated value")%>
                                </td>
                                <td id='totalEstimatedValue'>

                                </td>
                                <td></td>

                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <td>
                                    <%=UtilCharacter.getDataByLanguage(Language, "ভ্যাট", "Vat")%>
                                </td>
                                <td>
                                    <input type="number" class='form-control' id='vatValue' value="7.5"
                                           onkeyup="totalSum()" onchange="totalSum()">
                                </td>
                                <td id='totalVatValue'></td>
                                <td></td>


                            </tr>

                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <%=UtilCharacter.getDataByLanguage(Language, "মোট ব্যায় ", "Total cost")%>
                                </td>
                                <td id='totalSumValue'></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%
                    //                    System.out.println("HISTORY");
                    String lastMaintenanceDate = "";
                    String lastMaintenanceCost = "";
                    int currentYearCount = 0;
                    int prevYearCount = 0;
                    double currentYearCost = 0.0;
                    double prevYearCost = 0.0;

                    if (vm_vehicleDTO != null) {
                        if (vm_vehicleDTO.last_maintenance_date > 0) {
                            lastMaintenanceDate = simpleDateFormat.format(new Date(Long.parseLong
                                    (vm_vehicleDTO.last_maintenance_date + "")));
                        }
                        long maintenanceId = vm_vehicleDTO.last_maintenance_id;
                        if (maintenanceId > 0) {
                            Vm_maintenanceDTO mDTO = Vm_maintenanceRepository.getInstance().
                                    getVm_maintenanceDTOByID(maintenanceId);
                            if (mDTO != null) {
                                lastMaintenanceCost = Utils.getDigits(mDTO.totalWithVat + "", Language);

                                long fiscalYearId = mDTO.fiscalYearId;
                                System.out.println(fiscalYearId);
                                if (fiscalYearId > 0) {
                                    List<Vm_maintenanceDTO> currentYearDTOs = vm_maintenanceDAO.
                                            getAllVm_maintenanceByVehicleIdAndFiscalYear(vm_vehicleDTO.iD, fiscalYearId).
                                            stream().filter(i -> i.status == 5 || i.status == 6).collect(Collectors.toList());
                                    currentYearCount = currentYearDTOs.size();
                                    for (Vm_maintenanceDTO item : currentYearDTOs) {
                                        currentYearCost += item.totalWithVat;
                                    }

                                    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
                                    long prevFiscalYearId = fiscal_yearDAO.getPrevFiscalYearID(fiscalYearId);
                                    if (prevFiscalYearId != fiscalYearId) {
                                        List<Vm_maintenanceDTO> prevYearDTOs = vm_maintenanceDAO.
                                                getAllVm_maintenanceByVehicleIdAndFiscalYear(vm_vehicleDTO.iD, prevFiscalYearId).
                                                stream().filter(i -> i.status == 5 || i.status == 6).collect(Collectors.toList());
                                        prevYearCount = prevYearDTOs.size();
                                        for (Vm_maintenanceDTO item : prevYearDTOs) {
                                            prevYearCost += item.totalWithVat;
                                        }
                                    }

                                }
                            }
                        }
                    }

//                    System.out.println(lastMaintenanceDate);
//                    System.out.println(lastMaintenanceCost);
//                    System.out.println(currentYearCount);
//                    System.out.println(currentYearCost);
//                    System.out.println(prevYearCount);
//                    System.out.println(prevYearCost);
//
//
//                    System.out.println("HISTORY");
                %>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "গাড়ির মেরামত/রক্ষনাবেক্ষন হিস্ট্রি ",
                                "Maintenance History")%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "সর্বশেষ অন্য মেরামতের তারিখ ",
                                        "Last Maintenance Date")%>
                                </th>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "সর্বশেষ মেরামতের ব্যায়  ",
                                        "Last Maintenance Cost")%>
                                </th>

                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "গত অর্থবছরে কতবার মেরামত করা হয়েছে  ",
                                        "Last Fiscal Year Maintenance Count")%>
                                </th>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "গত অর্থবছরে কত টাকা মেরামত করা হয়েছে  ",
                                        "Last Fiscal Year Maintenance Cost")%>
                                </th>

                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "এ অর্থবছরে কতবার মেরামত করা হয়েছে  ",
                                        "Current Fiscal Year Maintenance Count")%>
                                </th>
                                <th class="text-nowrap"><%=UtilCharacter.getDataByLanguage(Language, "এ অর্থবছরে কত ব্যায় হয়েছে   ",
                                        "Current Fiscal Year Maintenance Cost")%>
                                </th>


                            </tr>
                            </thead>
                            <tbody id="">

                            <td>
                                <%=Utils.getDigits(lastMaintenanceDate, Language)%>
                            </td>
                            <td>
                                <%=Utils.getDigits(lastMaintenanceCost, Language)%>
                            </td>
                            <td>
                                <%=Utils.getDigits(currentYearCount, Language)%>
                            </td>
                            <td>
                                <%=Utils.getDigits(currentYearCost, Language)%>
                            </td>
                            <td>
                                <%=Utils.getDigits(prevYearCount, Language)%>
                            </td>
                            <td>
                                <%=Utils.getDigits(prevYearCost, Language)%>
                            </td>


                            <tr>


                            </tr>


                            </tbody>

                        </table>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12 form-actions text-right mt-3">
                        <button onclick="reject()" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=UtilCharacter.getDataByLanguage(Language, "অনুমোদিত নহে ", "Reject")%>
                        </button>
                        <button onclick="approveAO()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="submit">
                            <%=UtilCharacter.getDataByLanguage(Language, "অনুমোদন", "Approve")%>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>