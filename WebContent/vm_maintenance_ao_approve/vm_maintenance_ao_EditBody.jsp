<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.List" %>
<%@ page import="util.UtilCharacter" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="vm_maintenance_vehicle_item_mapping.Vm_maintenance_vehicle_item_mappingDAO" %>
<%@ page import="vm_maintenance.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
//    Vm_maintenanceDAO vm_maintenanceDAO = new Vm_maintenanceDAO();
    long ID = Long.parseLong(request.getParameter("ID"));

    Vm_maintenanceDTO vm_maintenanceDTO = Vm_maintenanceRepository.getInstance().getVm_maintenanceDTOByID(ID);
    vm_maintenanceDTO.vmMaintenanceItemDTOList = VmMaintenanceItemRepository.getInstance().
            getVmMaintenanceItemDTOByMaintenanceId(vm_maintenanceDTO.iD);


//            (Vm_maintenanceDTO)vm_maintenanceDAO.
//            getDTOByID(ID);
//    VmMaintenanceItemDAO vmMaintenanceItemDAO = new VmMaintenanceItemDAO();
//    List<VmMaintenanceItemDTO> vmMaintenanceItemDTOs = VmMaintenanceItemRepository.getInstance().
//            getVmMaintenanceItemDTOByMaintenanceId(vm_maintenanceDTO.iD);
//            vmMaintenanceItemDAO.
//            getVmMaintenanceItemDTOListByVmMaintenanceID(vm_maintenanceDTO.iD);

    Vm_maintenance_vehicle_item_mappingDAO item_mappingDAO = new Vm_maintenance_vehicle_item_mappingDAO();
    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);

    String formTitle = UtilCharacter.getDataByLanguage(Language, "মেকানিক অনুমোদন", "Mechanic Approve");
    String value = "";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<%@include file="ao_approve_form.jsp" %>

<script type="text/javascript">


    let index = <%=index%>;

    $(document).ready(function(){
        totalSum();
    });

    function round(num){
        return Math.round(num * 100) / 100;
    }

    function totalSum(){
        var sum = 0;
        for(let i = 1 ; i <= index ; i++){
            let el = document.getElementById("mechanicPrice_number_" + i);
            if(el){
                sum += el.value/1;
            }
        }

        $("#totalEstimatedValue").text(round(sum));

        let vat = document.getElementById('vatValue').value;

        let totalVat = (sum * vat) / 100;

        $("#totalVatValue").text(round(totalVat));
        $("#totalSumValue").text(round(sum +totalVat));
        // console.log(sum);
    }

    function reject(){
        event.preventDefault();
        let url = "Vm_maintenance_request_approval_mappingServlet?actionType=rejectVm_fuel_request&ID="+'<%=ID%>';
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            success: function(fetchedData) {
                window.location = 'Vm_maintenance_request_approval_mappingServlet?actionType=search';
            },
            error: function(error) {
                console.log(error);
            }
        });

    }



    function approveAO(){
        event.preventDefault();
        // which items are clicked in the table
        let formData = new FormData();
        let tablename = 'field-VmMaintenanceItem';
        let i = 0;
        let element = document.getElementById(tablename);



        // vat validation
        let vat = document.getElementById('vatValue').value;
        // console.log(vat)
        if(!vat || vat.toString().length == 0 || vat/1 <= 0){
            toastr.error("Please give vat");
            return ;
        }

        formData.append("vat", vat);


        let totalEstimatedValue = $("#totalEstimatedValue").text();
        formData.append("totalEstimatedValue", totalEstimatedValue);

        let totalVatValue = $("#totalVatValue").text();
        formData.append("totalVatValue", totalVatValue);

        let totalSumValue = $("#totalSumValue").text();
        formData.append("totalSumValue", totalSumValue);

        // console.log({vat, totalEstimatedValue, totalVatValue, totalSumValue});





        let j = 0;
        for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
        {
            let tr = document.getElementById(tablename).childNodes[i];
            if(tr.nodeType === Node.ELEMENT_NODE)
            {
                let id;
                id = 'itemId_' + j;
                let itemId = tr.querySelector('input[name="itemId"]');
                if(!itemId) continue;
                formData.append(id, itemId.value);
                let mPrice = tr.querySelector('input[name="vmMaintenanceItem.mechanicPrice"]');

                if(!mPrice.value || mPrice.value.toString().length == 0 || mPrice.value/1 <= 0){
                    toastr.error("Please give price");
                    return ;
                }

                id = 'mPrice_' + j;
                formData.append(id, mPrice.value);
                j ++;
            }



        }

        formData.append("itemLength", j);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                window.location = 'Vm_maintenance_request_approval_mappingServlet?actionType=search';
            }
            else if (this.readyState == 4 && this.status != 200) {
            }
        };



        let params= "Vm_maintenance_request_approval_mappingServlet?actionType=approvedVm_fuel_request&ID="+'<%=ID%>';
        xhttp.open("POST",params, true);
        xhttp.send(formData);



    }

</script>