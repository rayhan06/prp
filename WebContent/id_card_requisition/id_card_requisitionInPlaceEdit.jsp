<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="id_card_requisition.Id_card_requisitionDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>

<%
Id_card_requisitionDTO id_card_requisitionDTO = (Id_card_requisitionDTO)request.getAttribute("id_card_requisitionDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(id_card_requisitionDTO == null)
{
	id_card_requisitionDTO = new Id_card_requisitionDTO();
	
}
System.out.println("id_card_requisitionDTO = " + id_card_requisitionDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ID_CARD_REQUISITION_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=id_card_requisitionDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsId'>")%>
			

		<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + id_card_requisitionDTO.employeeRecordsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobCat" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value='<%=id_card_requisitionDTO.jobCat%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isLost'>")%>
			
	
	<div class="form-inline" id = 'isLost_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isLost' id = 'isLost_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(id_card_requisitionDTO.isLost).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_filesDropzone'>")%>
			
	
	<div class="form-inline" id = 'filesDropzone_div_<%=i%>'>
		<%
		if(actionName.equals("edit"))
		{
			List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(id_card_requisitionDTO.filesDropzone);
			%>			
			<table>
				<tr>
			<%
			if(filesDropzoneDTOList != null)
			{
				for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
				{
					FilesDTO filesDTO = filesDropzoneDTOList.get(j);
					byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
					%>
					<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
					{
						%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64) 
						%>' style='width:100px' />
						<%
					}
					%>
					<a href = 'Id_card_requisitionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>		
					</td>
					<%
				}
			}
			%>
			</tr>
			</table>
			<%
		}
		%>
		
		<%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>			
		<div class="dropzone" action="Id_card_requisitionServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?id_card_requisitionDTO.filesDropzone:ColumnID%>">
			<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?id_card_requisitionDTO.filesDropzone:ColumnID%>'/>		


	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=id_card_requisitionDTO.insertedByUserId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=id_card_requisitionDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=id_card_requisitionDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + id_card_requisitionDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=id_card_requisitionDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Id_card_requisitionServlet?actionType=view&ID=<%=id_card_requisitionDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Id_card_requisitionServlet?actionType=view&modal=1&ID=<%=id_card_requisitionDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	