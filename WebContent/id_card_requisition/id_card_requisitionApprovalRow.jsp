<%@page pageEncoding="UTF-8" %>

<%@page import="id_card_requisition.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="workflow.*"%>
<%@page import="dbm.*" %>
<%@page import="holidays.*" %>
<%@page import="approval_summary.*" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.ID_CARD_REQUISITION_EDIT_LANGUAGE, loginDTO);

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_ID_CARD_REQUISITION;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Id_card_requisitionDTO id_card_requisitionDTO = (Id_card_requisitionDTO)request.getAttribute("id_card_requisitionDTO");

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;
boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
boolean isInPreviousOffice = false;
String Message = "Done";

approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("id_card_requisition", id_card_requisitionDTO.iD);
Approval_summaryDTO approval_summaryDTO = approval_summaryDAO.getDTOByTableNameAndTableID("id_card_requisition", approval_execution_tableDTO.previousRowId);
System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
{
	canApprove = true;
	if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
	{
		canValidate = true;
	}
}

isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

canTerminate = isInitiator && id_card_requisitionDTO.isDeleted == 2;

Approval_pathDAO approval_pathDAO = new Approval_pathDAO();
Approval_pathDTO approval_pathDTO = approval_pathDAO.getDTOByID(approval_execution_tableDTO.approvalPathId);
	

System.out.println("id_card_requisitionDTO = " + id_card_requisitionDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Id_card_requisitionDAO id_card_requisitionDAO = (Id_card_requisitionDAO)request.getAttribute("id_card_requisitionDAO");

FilesDAO filesDAO = new FilesDAO();

SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

boolean isOverDue = false;

Date fromDate = new Date(approval_execution_tableDTO.lastModificationTime);
Date dueDate = null;
if(fromDate != null && approvalPathDetailsDTO!= null)
{
	dueDate = CalenderUtil.getDateAfter(fromDate, approvalPathDetailsDTO.daysRequired);
	long today = System.currentTimeMillis();
	boolean timeOver = today > dueDate.getTime();
	isOverDue  =  (approval_execution_tableDTO.approvalStatusCat == Approval_execution_tableDTO.PENDING) && timeOver;
	
	System.out.println("time dif = " + (today - dueDate.getTime()));
}
String formatted_dueDate;

System.out.println("i = " + i  + " OVERDUE = " + isOverDue);

if(isOverDue)
{
%>
<style>
#tr_<%=i%> {
  color: red;
}
</style>
<%
}
%>

											
		
											
											<td id = '<%=i%>_employeeRecordsId'>
											<%
											value = id_card_requisitionDTO.employeeRecordsId + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
		
											
											<td id = '<%=i%>_isLost'>
											<%
											value = id_card_requisitionDTO.isLost + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_filesDropzone'>
											<%
											value = id_card_requisitionDTO.filesDropzone + "";
											%>
											<%
											{
												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(id_card_requisitionDTO.filesDropzone);
												%>
												<table>
												<tr>
												<%
												if(FilesDTOList != null)
												{
													for(int j = 0; j < FilesDTOList.size(); j ++)
													{
														FilesDTO filesDTO = FilesDTOList.get(j);
														byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
														%>
														<td>
														<%
														if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
														{
															%>
															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
															<%
														}
														%>
														<a href = 'Id_card_requisitionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
														</td>
													<%
													}
												}
												%>
												</tr>
												</table>
											<%												
											}
											%>
				
			
											</td>
		
											
		
											
		
											
		
											
		
											
		
	

<td>
											<a href='Id_card_requisitionServlet?actionType=view&ID=<%=id_card_requisitionDTO.iD%>'&isPermanentTable=false>View</a>
											
											<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
											
											<div class='modal fade' id='viedFileModal_<%=i%>'>
											  <div class='modal-dialog modal-lg' role='document'>
											    <div class='modal-content'>
											      <div class='modal-body'>
											        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
											          <span aria-hidden='true'>&times;</span>
											        </button>											        
											        
											        <object type='text/html' data='Id_card_requisitionServlet?actionType=view&modal=1&ID=<%=id_card_requisitionDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
											        
											      </div>
											    </div>
											  </div>
											</div>
											</td>
											
											<td>
												<%
												value = approval_pathDTO.nameEn;
												%>											
															
												<%=value%>
											</td>
											
											<td>
												<%
												value = WorkflowController.getNameFromUserId(approval_summaryDTO.initiator, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td id = '<%=i%>_dateOfInitiation'>
												<%
												value = approval_summaryDTO.dateOfInitiation + "";
												%>
												<%

												String formatted_dateOfInitiation = simpleDateFormat.format(new Date(Long.parseLong(value)));
												%>
												<%=formatted_dateOfInitiation%>
				
			
											</td>
											
											
											<td>
												<%
												value = WorkflowController.getNameFromOrganogramId(approval_summaryDTO.assignedTo, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td >
												<%
												String formatted_dateOfAssignment = simpleDateFormat.format(new Date(approval_execution_tableDTO.lastModificationTime));
												%>
												<%=formatted_dateOfAssignment%>
				
			
											</td>
											
											<td>
											
												<%
												
												
												if(dueDate!= null)
												{
													formatted_dueDate = simpleDateFormat.format(dueDate);
												}
												else
												{
													formatted_dueDate = "";
												}
												%>
												<%=formatted_dueDate%>
				
			
											</td>
		
											
											
											<td>
												<%
												if(isOverDue)
												{
													value = "OVERDUE";
												}
												else
												{
													value = CatDAO.getName(Language, "approval_status", approval_summaryDTO.approvalStatusCat);
												}
												
												%>											
															
												<%=value%>
											</td>
	
											<td>
											<%
											if(canApprove || canTerminate)
											{
												%>
												<a href='Id_card_requisitionServlet?actionType=view&ID=<%=id_card_requisitionDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'>View</a>
												
												<%
											}
											else
											{
											
											 	%>
											 	No Action is required.
											 	<%
											}
											 %>																						
											</td>
											
											<td>
											<%
											if(canValidate)
											{
												%>
												<a href='Id_card_requisitionServlet?actionType=getEditPage&ID=<%=id_card_requisitionDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'>View</a>
												
												<%
											}
											else
											{
											
											 	%>
											 	No Action is required.
											 	<%
											}
											 %>																						
											</td>
											
											
											<td>
												<a href="Approval_execution_tableServlet?actionType=search&tableName=id_card_requisition&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"  >History</a>
											</td>
											

