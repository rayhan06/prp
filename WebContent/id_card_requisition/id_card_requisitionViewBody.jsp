<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="id_card_requisition.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.ID_CARD_REQUISITION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Id_card_requisitionDAO id_card_requisitionDAO = new Id_card_requisitionDAO("id_card_requisition");
    Id_card_requisitionDTO id_card_requisitionDTO = (Id_card_requisitionDTO) id_card_requisitionDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    Id Card Requisition Details
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">Id Card Requisition</h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.ID_CARD_REQUISITION_ADD_EMPLOYEERECORDSID, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = id_card_requisitionDTO.employeeRecordsId + "";
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.ID_CARD_REQUISITION_ADD_ISLOST, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = id_card_requisitionDTO.isLost + "";
                                %>

                                <%=value%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.ID_CARD_REQUISITION_ADD_FILESDROPZONE, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = id_card_requisitionDTO.filesDropzone + "";
                                %>
                                <%
                                    {
                                        List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(id_card_requisitionDTO.filesDropzone);
                                %>
                                <table>
                                    <tr>
                                        <%
                                            if (FilesDTOList != null) {
                                                for (int j = 0; j < FilesDTOList.size(); j++) {
                                                    FilesDTO filesDTO = FilesDTOList.get(j);
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                        %>
                                        <td>
                                            <%
                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                            %>
                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                 style='width:100px'/>
                                            <%
                                                }
                                            %>
                                            <a href='Id_card_requisitionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                               download><%=filesDTO.fileTitle%>
                                            </a>
                                        </td>
                                        <%
                                                }
                                            }
                                        %>
                                    </tr>
                                </table>
                                <%
                                    }
                                %>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>