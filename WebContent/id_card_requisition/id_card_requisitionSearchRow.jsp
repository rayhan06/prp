<%@page pageEncoding="UTF-8" %>

<%@page import="id_card_requisition.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ID_CARD_REQUISITION_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ID_CARD_REQUISITION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Id_card_requisitionDTO id_card_requisitionDTO = (Id_card_requisitionDTO) request.getAttribute("id_card_requisitionDTO");
    CommonDTO commonDTO = id_card_requisitionDTO;
    String servletName = "Id_card_requisitionServlet";

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    String Message = "Done";
    approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("id_card_requisition", id_card_requisitionDTO.iD);

    System.out.println("id_card_requisitionDTO = " + id_card_requisitionDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Id_card_requisitionDAO id_card_requisitionDAO = (Id_card_requisitionDAO) request.getAttribute("id_card_requisitionDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_employeeRecordsId'>
    <%
        value = id_card_requisitionDTO.employeeRecordsId + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_isLost'>
    <%
        value = id_card_requisitionDTO.isLost + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_filesDropzone'>
    <%
        value = id_card_requisitionDTO.filesDropzone + "";
    %>
    <%
        {
            List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(id_card_requisitionDTO.filesDropzone);
    %>
    <table>
        <tr>
            <%
                if (FilesDTOList != null) {
                    for (int j = 0; j < FilesDTOList.size(); j++) {
                        FilesDTO filesDTO = FilesDTOList.get(j);
                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
            %>
            <td>
                <%
                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                %>
                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                <%
                    }
                %>
                <a href='Id_card_requisitionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                   download><%=filesDTO.fileTitle%>
                </a>
            </td>
            <%
                    }
                }
            %>
        </tr>
    </table>
    <%
        }
    %>


</td>


<td>
	<button type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Id_card_requisitionServlet?actionType=view&ID=<%=id_card_requisitionDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>

</td>

<td id='<%=i%>_Edit'>
<%--    <a href=''><%=LM.getText(LC.ID_CARD_REQUISITION_SEARCH_ID_CARD_REQUISITION_EDIT_BUTTON, loginDTO)%>--%>
<%--    </a>--%>
	<button
			type="button"
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Id_card_requisitionServlet?actionType=getEditPage&ID=<%=id_card_requisitionDTO.iD%>'">
		<i class="fa fa-edit"></i>
	</button>

</td>


<td>
    <%
        if (id_card_requisitionDTO.isDeleted == 0 && approval_execution_tableDTO != null) {
    %>
    <a href="Approval_execution_tableServlet?actionType=search&tableName=id_card_requisition&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"><%=LM.getText(LC.HM_HISTORY, loginDTO)%>
    </a>
    <%
    } else {
    %>
    <%=LM.getText(LC.HM_NO_HISTORY_IS_AVAILABLE, loginDTO)%>
    <%
        }
    %>
</td>

<td>
    <%
        if (id_card_requisitionDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {
    %>
    <button type="button" class="btn btn-sm btn-success shadow btn-border-radius" data-toggle="modal" data-target="#sendToApprovalPathModal">
        <%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>
    </button>
    <%@include file="../inbox_internal/sendToApprovalPathModal.jsp" %>
    <%
    } else {
    %>
    <%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
    <%
        }
    %>
</td>
<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=id_card_requisitionDTO.iD%>'/></span>
    </div>
</td>


<script>
    window.onload = function () {
        console.log("using ckEditor");
        CKEDITOR.replaceAll();
    }

</script>	

