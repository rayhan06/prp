
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="id_card_requisition.*"%>
<%@page import="employee_records.*" %>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="user.*"%>

<%@page import="workflow.*"%>
<%@page import="util.TimeFormat"%>

<%
Id_card_requisitionDTO id_card_requisitionDTO;
id_card_requisitionDTO = (Id_card_requisitionDTO)request.getAttribute("id_card_requisitionDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
if(id_card_requisitionDTO == null)
{
	id_card_requisitionDTO = new Id_card_requisitionDTO();
	
}
System.out.println("id_card_requisitionDTO = " + id_card_requisitionDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.ID_CARD_REQUISITION_ADD_ID_CARD_REQUISITION_ADD_FORMNAME, loginDTO);


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
if(request.getParameter("isPermanentTable") != null)
{
	isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
}

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

String tableName = "id_card_requisition";

boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

if(!isPermanentTable)
{
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("id_card_requisition", id_card_requisitionDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("id_card_requisition", id_card_requisitionDTO.iD);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
	
	isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
	
	canTerminate = isInitiator && id_card_requisitionDTO.isDeleted == 2;
}
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Id_card_requisitionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ID_CARD_REQUISITION_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
Employee_recordsDTO employee_recordsDTO = (Employee_recordsDTO) request.getAttribute("employee_records_dto");
String employeeDesignationEn = (String) request.getAttribute("employee_designation_en");
String employeeDesignationBn = (String) request.getAttribute("employee_designation_bn");
String officeNameEn = (String) request.getAttribute("office_name_en");
String officeNameBn = (String) request.getAttribute("office_name_bn");

%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=id_card_requisitionDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + id_card_requisitionDTO.employeeRecordsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + id_card_requisitionDTO.jobCat + "'"):("'" + "-1" + "'")%> tag='pb_html'/>
             
                      <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NAMEENG, loginDTO)) : (LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;">
	                            <%=employee_recordsDTO.nameEng%>
                           </label>
                       </div>
                     <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NAMEBNG, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;">
	                            <%=employee_recordsDTO.nameBng%>
                           </label>
                       </div>               
                        <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_RECORDS_EDIT_FATHERNAMEENG, loginDTO)) : (LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEENG, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.fatherNameEng%>
                           </label>
                       </div>   
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_FATHERNAMEBNG, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.fatherNameBng%>
                           </label>
                       </div>
                       
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_MOTHERNAMEENG, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.motherNameEng%>
                           </label>
                       </div>   
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_MOTHERNAMEBNG, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.motherNameBng%>
                           </label>
                       </div>
                       
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.USER_ORGANOGRAM_EDIT_DESIGNATION, loginDTO))%>
                            </label>
                            <% if(Language.equals("English")) {
                            %>
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employeeDesignationEn%>
                            <%} else {%>
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employeeDesignationBn%>
                            <%} %>
                           </label>
                       </div>
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.OFFICES_EDIT_OFFICENAMEENG, loginDTO))%>
                            </label>
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=officeNameEn%>
                           </label>
                       </div>     
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.OFFICES_EDIT_OFFICENAMEBNG, loginDTO))%>
                            </label>
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=officeNameBn%>
                           </label>
                       </div>  
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_NID, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.nid%>
                           </label>
                       </div>
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_DOB, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> 
                              <% String formatted_dob = dateFormat.format(new Date(employee_recordsDTO.dateOfBirth));%>
                                <%=formatted_dob%>
                           </label>
                       </div>
                        <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_IDENTIFICATION_MARK, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.identificationMark%>
                           </label>
                       </div>
                     <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_HEIGHT, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.height%>
                           </label>
                       </div>
                          
<%--                        <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_PRESENTADDRESS, loginDTO)) : (LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_PRESENTADDRESS, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.presentAddress%>
                           </label>
                       </div>  
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_PERMANENTADDRESS, loginDTO)) : (LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_PERMANENTADDRESS, loginDTO))%>
                            </label>
                      
                            <label class="col-lg-6 control-label" style="text-align: left;"> <%=employee_recordsDTO.permanentAddress%>
                           </label>
                       </div>    --%>
                       
                       <div class="col-lg-12">
                            <label class="col-lg-3 control-label">
                                <%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_BLOODGROUP, loginDTO))%>
                            </label>
                            <label class="col-lg-6 control-label" style="text-align: left;">
								<%=CatRepository.getInstance().getText(Language,"blood_group",employee_recordsDTO.bloodGroup)%>
                           </label>
                       </div>  
<div class="col-lg-12">
<div>
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.ID_CARD_REQUISITION_EDIT_ISLOST, loginDTO)):(LM.getText(LC.ID_CARD_REQUISITION_ADD_ISLOST, loginDTO))%>
</label>
</div>
<div class="form-group ">					
	<div class="col-lg-6 " style="padding-top: 7px;" id = 'isLost_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isLost' id = 'isLost_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(id_card_requisitionDTO.isLost).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
</div>
</div>			
				
<div class="col-lg-12">
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.ID_CARD_REQUISITION_EDIT_FILESDROPZONE, loginDTO)):(LM.getText(LC.ID_CARD_REQUISITION_ADD_FILESDROPZONE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'filesDropzone_div_<%=i%>'>	
		<%
		if(actionName.equals("edit"))
		{
			List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(id_card_requisitionDTO.filesDropzone);
			%>			
			<table>
				<tr>
			<%
			if(filesDropzoneDTOList != null)
			{
				for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
				{
					FilesDTO filesDTO = filesDropzoneDTOList.get(j);
					byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
					%>
					<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
					{
						%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64) 
						%>' style='width:100px' />
						<%
					}
					%>
					<a href = 'Id_card_requisitionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>		
					</td>
					<%
				}
			}
			%>
			</tr>
			</table>
			<%
		}
		%>
		
		<%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>			
		<div class="dropzone" action="Id_card_requisitionServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?id_card_requisitionDTO.filesDropzone:ColumnID%>">
			<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?id_card_requisitionDTO.filesDropzone:ColumnID%>'/>		


	</div>
</div>	
</div>		
				

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + id_card_requisitionDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + id_card_requisitionDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + id_card_requisitionDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + id_card_requisitionDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + id_card_requisitionDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	






				<%if(canValidate)
				{
				%>	
				<div class="row div_border attachement-div">
							<div class="col-md-12">
								<h5>Attachments</h5>
								<%
									ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
								%>
								<div class="dropzone"
									 action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
									<input type='file' style="display: none"
										   name='approval_attached_fileDropzoneFile'
										   id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
										   tag='pb_html' />
								</div>
								<input type='hidden'
									   name='approval_attached_fileDropzoneFilesToDelete'
									   id='approval_attached_fileDropzoneFilesToDelete_<%=i%>' value=''
									   tag='pb_html' /> <input type='hidden'
															   name='approval_attached_fileDropzone'
															   id='approval_attached_fileDropzone_dropzone_<%=i%>' tag='pb_html'
															   value='<%=ColumnID%>' />
							</div>
				
							<div class="col-md-12">
								<h5>Remarks</h5>
				
								<textarea class='form-control'  name='remarks' id = '<%=i%>_remarks'   tag='pb_html'></textarea>
							</div>
						</div>
				<%
				}
				%>
				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">					
						<%=LM.getText(LC.ID_CARD_REQUISITION_ADD_ID_CARD_REQUISITION_CANCEL_BUTTON, loginDTO)%>						
					</a>
					<button class="btn btn-success" type="submit">
					
					<%=LM.getText(LC.ID_CARD_REQUISITION_ADD_ID_CARD_REQUISITION_SUBMIT_BUTTON, loginDTO)%>						
					
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	preprocessCheckBoxBeforeSubmitting('isLost', row);		

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Id_card_requisitionServlet");	
}

function init(row)
{


	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






