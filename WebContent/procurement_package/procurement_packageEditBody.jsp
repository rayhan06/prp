<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="procurement_package.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Procurement_packageDTO procurement_packageDTO;
    procurement_packageDTO = (Procurement_packageDTO) request.getAttribute("procurement_packageDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (procurement_packageDTO == null) {
        procurement_packageDTO = new Procurement_packageDTO();

    }
    System.out.println("procurement_packageDTO = " + procurement_packageDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_ADD_FORMNAME, loginDTO);
    String servletName = "Procurement_packageServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.PROCUREMENT_PACKAGE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Procurement_packageServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="row mx-2 mx-md-0">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=procurement_packageDTO.iD%>' tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_NAMEEN, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='nameEn'
                                                           id='nameEn_text_<%=i%>'
                                                           value='<%=procurement_packageDTO.nameEn%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_NAMEBN, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='nameBn'
                                                           id='nameBn_text_<%=i%>'
                                                           value='<%=procurement_packageDTO.nameBn%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=procurement_packageDTO.insertedByUserId%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=procurement_packageDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=procurement_packageDTO.insertionDate%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=procurement_packageDTO.isDeleted%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=procurement_packageDTO.lastModificationTime%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   id='searchColumn_hidden_<%=i%>'
                                                   value='<%=procurement_packageDTO.searchColumn%>' tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <h5 class="table-title mt-4">
                        <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_GOODS_TYPE_NAMEEN, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_GOODS_TYPE_NAMEBN, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_GOODS_TYPE_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-ProcurementGoodsType">


                            <%
                                if (actionName.equals("edit")) {
                                    int index = -1;


                                    for (ProcurementGoodsTypeDTO procurementGoodsTypeDTO : procurement_packageDTO.procurementGoodsTypeDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>

                            <tr id="ProcurementGoodsType_<%=index + 1%>">
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='procurementGoodsType.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td>


                                    <input type='text' class='form-control noDollar'
                                           name='procurementGoodsType.nameEn'
                                           id='nameEn_text_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.nameEn%>' tag='pb_html'/>
                                    <label class="error-msg" style="color: red;display: none"></label>

                                </td>
                                <td>


                                    <input type='text' class='form-control noDollar'
                                           name='procurementGoodsType.nameBn'
                                           id='nameBn_text_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.nameBn%>' tag='pb_html'/>
                                    <label class="error-msg" style="color: red;display: none"></label>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='procurementGoodsType.procurementPackageId'
                                           id='procurementPackageId_hidden_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.procurementPackageId%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='procurementGoodsType.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.insertedByUserId%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='procurementGoodsType.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='procurementGoodsType.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.insertionDate%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='procurementGoodsType.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.isDeleted%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='procurementGoodsType.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.lastModificationTime%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='procurementGoodsType.searchColumn'
                                           id='searchColumn_hidden_<%=childTableStartingID%>'
                                           value='<%=procurementGoodsTypeDTO.searchColumn%>' tag='pb_html'/>
                                </td>
                                <td>
                                    <div class='checker'><span id='chkEdit'><input type='checkbox'
                                                                                   id='procurementGoodsType_cb_<%=index%>'
                                                                                   name='checkbox' value=''/></span>
                                    </div>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>

                            </tbody>
                        </table>
                    </div>
                    <div class="form-actions">
                        <div class="text-right">
                            <button
                                    id="add-more-ProcurementGoodsType"
                                    name="add-more-ProcurementGoodsType"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST_ADD_MORE, loginDTO)%>
                            </button>
                            <button
                                    id="remove-ProcurementGoodsType"
                                    name="remove-ProcurementGoodsType"
                                    type="button"
                                    class="btn btn-sm remove-btn shadow ml-2">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <%ProcurementGoodsTypeDTO procurementGoodsTypeDTO = new ProcurementGoodsTypeDTO();%>

                    <template id="template-ProcurementGoodsType">
                        <tr>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='procurementGoodsType.iD'
                                       id='iD_hidden_' value='<%=procurementGoodsTypeDTO.iD%>' tag='pb_html'/>
                            </td>
                            <td>


                                <input type='text' class='form-control noDollar' name='procurementGoodsType.nameEn'
                                       id='nameEn_text_' value='<%=procurementGoodsTypeDTO.nameEn%>' tag='pb_html'/>
                                <label class="error-msg" style="color: red;display: none"></label>

                            </td>
                            <td>


                                <input type='text' class='form-control noDollar' name='procurementGoodsType.nameBn'
                                       id='nameBn_text_' value='<%=procurementGoodsTypeDTO.nameBn%>' tag='pb_html'/>
                                <label class="error-msg" style="color: red;display: none"></label>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='procurementGoodsType.procurementPackageId'
                                       id='procurementPackageId_hidden_'
                                       value='<%=procurementGoodsTypeDTO.procurementPackageId%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='procurementGoodsType.insertedByUserId' id='insertedByUserId_hidden_'
                                       value='<%=procurementGoodsTypeDTO.insertedByUserId%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='procurementGoodsType.insertedByOrganogramId'
                                       id='insertedByOrganogramId_hidden_'
                                       value='<%=procurementGoodsTypeDTO.insertedByOrganogramId%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='procurementGoodsType.insertionDate'
                                       id='insertionDate_hidden_' value='<%=procurementGoodsTypeDTO.insertionDate%>'
                                       tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='procurementGoodsType.isDeleted'
                                       id='isDeleted_hidden_' value='<%=procurementGoodsTypeDTO.isDeleted%>'
                                       tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='procurementGoodsType.lastModificationTime'
                                       id='lastModificationTime_hidden_'
                                       value='<%=procurementGoodsTypeDTO.lastModificationTime%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='procurementGoodsType.searchColumn'
                                       id='searchColumn_hidden_' value='<%=procurementGoodsTypeDTO.searchColumn%>'
                                       tag='pb_html'/>
                            </td>
                            <td>
                                <div><span id='chkEdit'><input type='checkbox' name='checkbox' value=''/></span>
                                </div>
                            </td>
                        </tr>

                    </template>

                    <div class="form-actions text-center mt-5">
                        <a class="btn btn-sm shadow text-white btn-border-radius cancel-btn "
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm shadow text-white btn-border-radius submit-btn"
                                onclick="event.preventDefault();submitBigForm()"><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<script type="text/javascript">


    let duplicatePackageErr;
    let requiredMsg, duplicateMsg;

    function resetErrorMsg() {
        var inp = document.getElementsByClassName("error-msg");
        for (var i = 0; i < inp.length; ++i) {
            inp[i].style.display = 'none';
        }
    }

    function noDuplicate() {

        var nameEns = document.getElementsByName("procurementGoodsType.nameEn");
        var nameBns = document.getElementsByName("procurementGoodsType.nameBn");

        var noDuplicate = true;
        const concatedDataSet = new Set();

        for (var i = 0; i < nameBns.length; ++i) {
            var concatedData = nameEns[i].value + '$' + nameBns[i].value;
            if (concatedDataSet.has(concatedData)) {
                nameBns[i].parentNode.childNodes.item(3).style.display = 'block';
                nameBns[i].parentNode.childNodes.item(3).innerText = duplicateMsg;

                nameEns[i].parentNode.childNodes.item(3).style.display = 'block';
                nameEns[i].parentNode.childNodes.item(3).innerText = duplicateMsg;
                noDuplicate = false;
            }
            concatedDataSet.add(concatedData);
        }

        return noDuplicate;

    }

    function validateString() {
        var foundErr = false;
        var fields = ['procurementGoodsType.nameEn', 'procurementGoodsType.nameBn'];

        for (var field = 0; field < fields.length; ++field) {
            var inp = document.getElementsByName(fields[field]);
            for (var i = 0; i < inp.length; ++i) {
                if (!(inp[i].value && inp[i].value != undefined && inp[i].value.toString().trim().length > 0)) {
                    inp[i].parentNode.childNodes.item(3).style.display = 'block';
                    inp[i].parentNode.childNodes.item(3).innerText = requiredMsg;
                    foundErr = true;
                }
            }
        }

        return !foundErr;
    }


    function processResponse(response) {
        if (response.responseCode === 0) {
            showError(response.msg, response.msg);
        } else if (response.responseCode === 200) {
            showToastSticky("সাবমিট সফল হয়েছে","Submit Successful");
            setTimeout(() => {
                window.location = 'Procurement_packageServlet?actionType=search';
            }, 3000);
        } else {
            console.log("Error: " + response.responseCode);
        }
    }

    function processError (jqXHR, textStatus, errorThrown) {
        toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
            + ", Message: " + errorThrown);
    }

    function ajaxPost(url, accept, send, postData, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: accept,
            contentType: send,
            data: postData,
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


    function submitBigForm() {

        if (PreprocessBeforeSubmiting(0, '<%=actionName%>')) {
            var form = $("#bigform");

            var actionUrl = form.attr("action");
            var postData = (form.serialize());

            ajaxPost(actionUrl, "json", "application/x-www-form-urlencoded", postData, processResponse, processError);
        }


    }


    function PreprocessBeforeSubmiting(row, validate) {


        for (i = 1; i < child_table_extra_id; i++) {
        }

        resetErrorMsg();
        let valid = validateString() && noDuplicate();
        let form = $("#bigform");
        form.validate();
        valid = valid && form.valid();

        return valid;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Procurement_packageServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
        }

        let lang = '<%=Language%>';

        if (lang == 'English') {
            requiredMsg = 'This is required';
            duplicateMsg = 'This is duplicated';

        } else {
            requiredMsg = '<%="এই তথ্যটি আবশ্যক"%>';
            duplicateMsg = '<%="এই নামটি পূর্বে ব্যবহৃত হয়েছে"%>';

        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();


        let lang = '<%=Language%>';
        let nameErr;

        if (lang == 'English') {
            nameErr = 'Please provide name';
            duplicatePackageErr = 'Another package exists with this name';

        } else {
            nameErr = 'নাম প্রদান করুন';
            duplicatePackageErr = 'এই নাম দিয়ে অন্য একটি প্যাকেজ বিদ্যমান';

        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                nameEn: {
                    required: true,
                },
                nameBn: {
                    required: true,
                },

            },
            messages: {
                nameEn: nameErr,
                nameBn: nameErr,

            }
        });

    });


    $(".noDollar").keypress(function (evt) {
        let keycode = evt.charCode || evt.keyCode;
        if (keycode == 36) {
            return false;
        }
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-ProcurementGoodsType").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-ProcurementGoodsType");

            $("#field-ProcurementGoodsType").append(t.html());
            SetCheckBoxValues("field-ProcurementGoodsType");

            var tr = $("#field-ProcurementGoodsType").find("tr:last-child");

            tr.attr("id", "ProcurementGoodsType_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });


            child_table_extra_id++;

            $(".noDollar").keypress(function (evt) {
                let keycode = evt.charCode || evt.keyCode;
                if (keycode == 36) {
                    return false;
                }
            });

        });


    $("#remove-ProcurementGoodsType").click(function (e) {
        var tablename = 'field-ProcurementGoodsType';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


</script>






