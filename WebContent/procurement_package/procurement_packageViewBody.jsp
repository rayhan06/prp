<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="procurement_package.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Procurement_packageServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.PROCUREMENT_PACKAGE_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Procurement_packageDTO procurement_packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_NAMEEN, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = procurement_packageDTO.nameEn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_NAMEBN, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = procurement_packageDTO.nameBn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
            <div class="mt-5">
                <h5 class="table-title">
                    <%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_GOODS_TYPE, loginDTO)%>
                </h5>
                <div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_GOODS_TYPE_NAMEEN, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_GOODS_TYPE_NAMEBN, loginDTO)%>
                            </th>
                        </tr>
                        <%
                            List<ProcurementGoodsTypeDTO> procurementGoodsTypeDTOs = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByprocurementPackageId(procurement_packageDTO.iD);

                            for (ProcurementGoodsTypeDTO procurementGoodsTypeDTO : procurementGoodsTypeDTOs) {
                        %>
                        <tr>
                            <td>
                                <%
                                    value = procurementGoodsTypeDTO.nameEn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                            <td>
                                <%
                                    value = procurementGoodsTypeDTO.nameBn + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                        </tr>
                        <%

                            }

                        %>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>