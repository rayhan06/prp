<%@page pageEncoding="UTF-8" %>

<%@page import="procurement_package.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PROCUREMENT_PACKAGE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PROCUREMENT_PACKAGE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Procurement_packageDTO procurement_packageDTO = (Procurement_packageDTO) request.getAttribute("procurement_packageDTO");
    CommonDTO commonDTO = procurement_packageDTO;
    String servletName = "Procurement_packageServlet";


    System.out.println("procurement_packageDTO = " + procurement_packageDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Procurement_packageDAO procurement_packageDAO = (Procurement_packageDAO) request.getAttribute("procurement_packageDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td>

    <%=Utils.getDigits(i + 1, Language)%>


</td>


<td id='<%=i%>_nameEn'>
    <%
        value = procurement_packageDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn'>
    <%
        value = procurement_packageDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="event.preventDefault();location.href='Procurement_packageServlet?actionType=view&ID=<%=procurement_packageDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="event.preventDefault();location.href='Procurement_packageServlet?actionType=getEditPage&ID=<%=procurement_packageDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=procurement_packageDTO.iD%>'/></span>
    </div>
</td>
																						
											

