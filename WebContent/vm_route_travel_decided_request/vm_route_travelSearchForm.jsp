<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_route_travel.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Vm_route_travelDAO vm_route_travelDAO = (Vm_route_travelDAO) request.getAttribute("vm_route_travelDAO");


    String navigator2 = SessionConstants.NAV_VM_ROUTE_TRAVEL;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTOPPAGEID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTOPPAGENAME, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REMARKS, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTARTDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERORGID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEREMPID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERPHONENUM, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERNAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERNAMEBN, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_STATUS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_ROUTEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_STOPPAGEID, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FISCALYEARID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEDSTARTDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_DECISIONREMARKS, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEDDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWALDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVERORGID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEREMPID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVERPHONENUM, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVERNAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVERNAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICENAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICENAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITNAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITNAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWERORGID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEREMPID, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWERPHONENUM, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWERNAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWERNAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICENAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICENAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITNAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITNAMEBN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_UPDATE, loginDTO)%>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_VM_ROUTE_TRAVEL);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Vm_route_travelDTO vm_route_travelDTO = (Vm_route_travelDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("vm_route_travelDTO", vm_route_travelDTO);
            %>

            <jsp:include page="./vm_route_travelSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			