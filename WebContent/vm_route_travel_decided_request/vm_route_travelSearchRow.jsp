<%@page pageEncoding="UTF-8" %>

<%@page import="vm_route_travel.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_ROUTE_TRAVEL;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_route_travelDTO vm_route_travelDTO = (Vm_route_travelDTO) request.getAttribute("vm_route_travelDTO");
    CommonDTO commonDTO = vm_route_travelDTO;
    String servletName = "Vm_route_travel_decided_requestServlet";


    System.out.println("vm_route_travelDTO = " + vm_route_travelDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_route_travelDAO vm_route_travelDAO = (Vm_route_travelDAO) request.getAttribute("vm_route_travelDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    String status = "5";
%>


<%--											<td id = '<%=i%>_requestedStoppageId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requestedStoppageId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requestedStoppageName'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requestedStoppageName + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_remarks'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.remarks + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requestedStartDate'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requestedStartDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_requestedStartDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_requestedStartDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requesterOrgId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterOrgId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requesterOfficeId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterOfficeId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requesterOfficeUnitId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterOfficeUnitId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requesterEmpId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterEmpId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requesterPhoneNum'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterPhoneNum + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<td id='<%=i%>_status' style="display: none;">
    <input type="hidden" name="status" value="<%=status%>">
</td>
<td id='<%=i%>_id' style="display: none;">

    <input type="hidden" name="iD" value="<%=vm_route_travelDTO.iD%>">
</td>
<td id='<%=i%>_requesterOrgId' style="display: none;">

    <input type="hidden" name="requesterOrgId" value="<%=vm_route_travelDTO.requesterOrgId%>">
</td>

<td id='<%=i%>_requesterNameEn'>
    <%
        value = vm_route_travelDTO.requesterNameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_requesterNameBn'>
    <%
        value = vm_route_travelDTO.requesterNameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<%--											<td id = '<%=i%>_requesterOfficeNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterOfficeNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requesterOfficeNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterOfficeNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requesterOfficeUnitNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterOfficeUnitNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_requesterOfficeUnitNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.requesterOfficeUnitNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<td id='<%=i%>_requesterOfficeUnitOrgNameEn'>
    <%
        value = vm_route_travelDTO.requesterOfficeUnitOrgNameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_requesterOfficeUnitOrgNameBn'>
    <%
        value = vm_route_travelDTO.requesterOfficeUnitOrgNameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_status'>
    <%
        value = CatRepository.getName(Language, "vm_requisition_status", vm_route_travelDTO.status);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_routeId'>
    <%
        value = vm_route_travelDTO.routeId + "";
        value = CommonDAO.getName(Language, "vm_route", vm_route_travelDTO.routeId);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_stoppageId'>
    <%
        long stoppageId = vm_route_travelDTO.status == CommonApprovalStatus.DISSATISFIED.getValue() ? vm_route_travelDTO.requestedStoppageId : vm_route_travelDTO.stoppageId;
        value = stoppageId + "";
        if (value.equals("-1")) {
            value = Language.equals("English") ? "Others" : "অন্যান্য";
            value += "(" + vm_route_travelDTO.requestedStoppageName + ")";
        } else {
            value = CommonDAO.getName(stoppageId, "vm_route_stoppage", "stoppage_name", "ID");
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<%--											<td id = '<%=i%>_fiscalYearId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.fiscalYearId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approvedStartDate'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approvedStartDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_approvedStartDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_approvedStartDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_decisionRemarks'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.decisionRemarks + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--		--%>
<%--											<td id = '<%=i%>_approvedDate'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approvedDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_approvedDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_approvedDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawalDate'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawalDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_withdrawalDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_withdrawalDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOrgId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOrgId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOfficeId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOfficeId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOfficeUnitId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOfficeUnitId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverEmpId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverEmpId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverPhoneNum'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverPhoneNum + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOfficeNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOfficeNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOfficeNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOfficeNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOfficeUnitNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOfficeUnitNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOfficeUnitNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOfficeUnitNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOfficeUnitOrgNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOfficeUnitOrgNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approverOfficeUnitOrgNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.approverOfficeUnitOrgNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOrgId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOrgId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOfficeId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOfficeId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOfficeUnitId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOfficeUnitId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerEmpId'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerEmpId + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerPhoneNum'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerPhoneNum + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOfficeNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOfficeNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOfficeNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOfficeNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOfficeUnitNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOfficeUnitNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOfficeUnitNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOfficeUnitNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOfficeUnitOrgNameEn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_withdrawerOfficeUnitOrgNameBn'>--%>
<%--											<%--%>
<%--											value = vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Vm_route_travel_decided_requestServlet?actionType=view&ID=<%=vm_route_travelDTO.requesterOrgId%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>

    <%if (vm_route_travelDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {%>

    <button type="submit" class="btn btn-sm border-0 shadow btn-border-radius"
            style="background-color: #cc22c1; color: white; border-radius: 8px">

        <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_VM_ROUTE_TRAVEL_WITHDRAW_ADD_FORMNAME, loginDTO)%>
    </button>

    <%}%>


</td>
																						
											

