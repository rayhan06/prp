

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="therapy_type.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Therapy_typeServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Therapy_typeDTO therapy_typeDTO = Therapy_typeDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = therapy_typeDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_TYPE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_TYPE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.THERAPY_TYPE_ADD_NAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = therapy_typeDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.THERAPY_TYPE_ADD_NAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = therapy_typeDTO.nameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
                                
                                	<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=Language.equalsIgnoreCase("english")?"Order":"ক্রম"%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(therapy_typeDTO.ordering, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE_NAMEBN, loginDTO)%></th>
							</tr>
							<%
                        	TherapyDiseaseDAO therapyDiseaseDAO = TherapyDiseaseDAO.getInstance();
                         	List<TherapyDiseaseDTO> therapyDiseaseDTOs = (List<TherapyDiseaseDTO>)therapyDiseaseDAO.getDTOsByParent("therapy_type_id", therapy_typeDTO.iD);
                         	
                         	for(TherapyDiseaseDTO therapyDiseaseDTO: therapyDiseaseDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = therapyDiseaseDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = therapyDiseaseDTO.nameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>