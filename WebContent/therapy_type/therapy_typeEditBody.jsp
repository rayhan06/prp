<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="therapy_type.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Therapy_typeDTO therapy_typeDTO = new Therapy_typeDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	therapy_typeDTO = Therapy_typeDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = therapy_typeDTO;
String tableName = "therapy_type";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_TYPE_ADD_FORMNAME, loginDTO);
String servletName = "Therapy_typeServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Therapy_typeServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=therapy_typeDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.THERAPY_TYPE_ADD_NAMEEN, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=therapy_typeDTO.nameEn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.THERAPY_TYPE_ADD_NAMEBN, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=therapy_typeDTO.nameBn%>'   tag='pb_html'/>					
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=Language.equalsIgnoreCase("english")?"Order":"ক্রম"%></label>
                                                            <div class="col-md-8">
																<input type='number' class='form-control'  name='ordering' id = 'ordering_text_<%=i%>' value='<%=therapy_typeDTO.ordering%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE_NAMEEN, loginDTO)%></th>
										<th><%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE_NAMEBN, loginDTO)%></th>
										<th><%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_DISEASE_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-TherapyDisease">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(TherapyDiseaseDTO therapyDiseaseDTO: therapy_typeDTO.therapyDiseaseDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "TherapyDisease_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='therapyDisease.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=therapyDiseaseDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='therapyDisease.therapyTypeId' id = 'therapyTypeId_hidden_<%=childTableStartingID%>' value='<%=therapyDiseaseDTO.therapyTypeId%>' tag='pb_html'/>
									</td>
									<td>										





																<input type='text' class='form-control'  name='therapyDisease.nameEn' id = 'nameEn_text_<%=childTableStartingID%>' value='<%=therapyDiseaseDTO.nameEn%>'   tag='pb_html'/>					
									</td>
									<td>										





																<input type='text' class='form-control'  name='therapyDisease.nameBn' id = 'nameBn_text_<%=childTableStartingID%>' value='<%=therapyDiseaseDTO.nameBn%>'   tag='pb_html'/>					
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='therapyDisease.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=therapyDiseaseDTO.isDeleted%>' tag='pb_html'/>
											
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='therapyDisease.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=therapyDiseaseDTO.lastModificationTime%>' tag='pb_html'/>
									</td>
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="text-right">
									<button
											id="add-more-TherapyDisease"
											name="add-moreTherapyDisease"
											type="button"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-TherapyDisease"
											name="removeTherapyDisease"
											type="button"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%TherapyDiseaseDTO therapyDiseaseDTO = new TherapyDiseaseDTO();%>
					
							<template id="template-TherapyDisease" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='therapyDisease.iD' id = 'iD_hidden_' value='<%=therapyDiseaseDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='therapyDisease.therapyTypeId' id = 'therapyTypeId_hidden_' value='<%=therapyDiseaseDTO.therapyTypeId%>' tag='pb_html'/>
									</td>
									<td>





																<input type='text' class='form-control'  name='therapyDisease.nameEn' id = 'nameEn_text_' value='<%=therapyDiseaseDTO.nameEn%>'   tag='pb_html'/>					
									</td>
									<td>





																<input type='text' class='form-control'  name='therapyDisease.nameBn' id = 'nameBn_text_' value='<%=therapyDiseaseDTO.nameBn%>'   tag='pb_html'/>					
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='therapyDisease.isDeleted' id = 'isDeleted_hidden_' value= '<%=therapyDiseaseDTO.isDeleted%>' tag='pb_html'/>
											
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='therapyDisease.lastModificationTime' id = 'lastModificationTime_hidden_' value='<%=therapyDiseaseDTO.lastModificationTime%>' tag='pb_html'/>
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>               
                <div class="form-actions text-center mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_TYPE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.THERAPY_TYPE_ADD_THERAPY_TYPE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Therapy_typeServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-TherapyDisease").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-TherapyDisease");

            $("#field-TherapyDisease").append(t.html());
			SetCheckBoxValues("field-TherapyDisease");
			
			var tr = $("#field-TherapyDisease").find("tr:last-child");
			
			tr.attr("id","TherapyDisease_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			

			child_table_extra_id ++;

        });

    
      $("#remove-TherapyDisease").click(function(e){    	
	    var tablename = 'field-TherapyDisease';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[deletecb="true"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






