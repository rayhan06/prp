
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="organogram_role_mapping.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getInstance().getUserDtoByUserId(loginDTO.userID);


Organogram_role_mappingDAO organogram_role_mappingDAO = (Organogram_role_mappingDAO)request.getAttribute("organogram_role_mappingDAO");


String navigator2 = SessionConstants.NAV_ORGANOGRAM_ROLE_MAPPING;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";
%>				
				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_EDIT_ORGANOGRAMID, loginDTO)%></th>
								<th><%=LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_EDIT_ROLEID, loginDTO)%></th>
								<%
								if(isPermanentTable)
								{
									out.println("<th>" + LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_SEARCH_ORGANOGRAM_ROLE_MAPPING_EDIT_BUTTON, loginDTO) + "</th>");
								}
								else if (!isPermanentTable && userDTO.approvalRole == SessionConstants.VALIDATOR)
								{
									out.println("<th><%=LM.getText(LC.HM_VALIDATE, loginDTO)%></th>");
								}
								%>
								
								
								<%
								if(!isPermanentTable && userDTO.approvalOrder > -1)
								{
									out.println("<th><%=LM.getText(LC.HM_APPROVE, loginDTO)%></th>");
									out.println("<th>Operation Type</th>");
									out.println("<th>Original Value</th>");
								}								
								%>
								<th><input type="submit" class="btn btn-xs btn-danger" value="
								<%
								if(isPermanentTable)
								{
									out.println(LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_SEARCH_ORGANOGRAM_ROLE_MAPPING_DELETE_BUTTON, loginDTO));
								}
								else
								{
									out.println("REJECT");
								}
								%>
								" /></th>
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_ORGANOGRAM_ROLE_MAPPING);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Organogram_role_mappingDTO row = (Organogram_role_mappingDTO) data.get(i);
											TempTableDTO tempTableDTO = null;
											if(!isPermanentTable)
											{
												tempTableDTO = organogram_role_mappingDAO.getTempTableDTOFromTableById(tableName, row.iD);
											}																						
											
											out.println("<tr id = 'tr_" + i + "'>");
											
								%>
											
		
								<%  								
								    request.setAttribute("organogram_role_mappingDTO",row);
								%>  
								
								 <jsp:include page="./organogram_role_mappingSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											out.println("</tr>");
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">

function getOriginal(i, tempID, parentID, ServletName)
{
	console.log("getOriginal called");
	var idToSubmit;
	var isPermanentTable;
	var state = document.getElementById(i + "_original_status").value;
	if(state == 0)
	{
		idToSubmit = parentID;
		isPermanentTable = true;
	}
	else
	{
		idToSubmit = tempID;
		isPermanentTable = false;
	}
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{			
			var response = JSON.parse(this.responseText);
			document.getElementById(i + "_organogramId").innerHTML = response.organogramId;
			document.getElementById(i + "_roleId").innerHTML = response.roleId;
					
			if(state == 0)
			{
				document.getElementById(i + "_getOriginal").innerHTML = "View Edited";
				state = 1;
			}
			else
			{
				document.getElementById(i + "_getOriginal").innerHTML = "View Original";
				state = 0;
			}
			
			document.getElementById(i + "_original_status").value = state;
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	xhttp.open("POST", ServletName + "?actionType=getDTO&ID=" + idToSubmit + "&isPermanentTable=" + isPermanentTable, true);
	xhttp.send();
}





function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	return true;
}

function PostprocessAfterSubmiting(row)
{
}


function init(row)
{
}


function submitAjax(i, deletedStyle)
{
	console.log('submitAjax called');
	var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
	if(isSubmittable == false)
	{
		return;
	}
	var formData = new FormData();
	var value;
	value = document.getElementById('iD_hidden_' + i).value;
	console.log('submitAjax i = ' + i + ' id = ' + value);
	formData.append('iD', value);
	formData.append("identity", value);
	formData.append("ID", value);
	formData.append('organogramId', document.getElementById('organogramId_hidden_' + i).value);
	formData.append('roleId', document.getElementById('roleId_hidden_' + i).value);
	formData.append('isDeleted', document.getElementById('isDeleted_hidden_' + i).value);
	formData.append('lastModificationTime', document.getElementById('lastModificationTime_hidden_' + i).value);

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				document.getElementById('tr_' + i).innerHTML = this.responseText ;
				ShowExcelParsingResult(i);
			}
			else
			{
				console.log("No Response");
				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	xhttp.open("POST", 'Organogram_role_mappingServlet?actionType=edit&inplacesubmit=true&isPermanentTable=<%=isPermanentTable%>&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
	xhttp.send(formData);
}

window.onload =function ()
{
	ShowExcelParsingResult('general');
}	
</script>
			