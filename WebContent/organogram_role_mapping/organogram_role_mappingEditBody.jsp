
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="organogram_role_mapping.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
Organogram_role_mappingDTO organogram_role_mappingDTO;
organogram_role_mappingDTO = (Organogram_role_mappingDTO)request.getAttribute("organogram_role_mappingDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(organogram_role_mappingDTO == null)
{
	organogram_role_mappingDTO = new Organogram_role_mappingDTO();
	
}
System.out.println("organogram_role_mappingDTO = " + organogram_role_mappingDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_EDIT_ORGANOGRAM_ROLE_MAPPING_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_ADD_ORGANOGRAM_ROLE_MAPPING_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
Organogram_role_mappingDTO row = organogram_role_mappingDTO;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Organogram_role_mappingServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												

		<input type='hidden' class='form-control'  name='organogramId' id = 'organogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + organogram_role_mappingDTO.organogramId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='roleId' id = 'roleId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + organogram_role_mappingDTO.roleId + "'"):("'" + "0" + "'")%>/>
												

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + organogram_role_mappingDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + organogram_role_mappingDTO.lastModificationTime + "'"):("'" + "0" + "'")%>/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_EDIT_ORGANOGRAM_ROLE_MAPPING_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_ADD_ORGANOGRAM_ROLE_MAPPING_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_EDIT_ORGANOGRAM_ROLE_MAPPING_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.ORGANOGRAM_ROLE_MAPPING_ADD_ORGANOGRAM_ROLE_MAPPING_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	return true;
}

function PostprocessAfterSubmiting(row)
{
}


function init(row)
{
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}





</script>






