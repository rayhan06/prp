<%@page import="user.UserRepository"%>
<%@page import="employee_records.*"%>
<%@page import="test_lib.EmployeeRecordDTO"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="appointment.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.Utils" %>
<%@ page import="employee_records.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="family.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="user.*" %>
<%@page import="doctor_time_slot.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    AppointmentDAO appointmentDAO = new AppointmentDAO("appointment");
    AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    String context = request.getContextPath() + "/";
    Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
    Doctor_time_slotDTO doctor_time_slotDTO = doctor_time_slotDAO.get1stDTOByDoctor(appointmentDTO.doctorId);
    UserDTO patientDTO = UserRepository.getUserDTOByUserName(appointmentDTO.employeeUserName);
    if(patientDTO == null)
    {
    	patientDTO = new UserDTO();
    }
    Employee_recordsDTO employee_recordsDTO;
    employee_recordsDTO = Employee_recordsRepository.getInstance().getById(patientDTO.employee_record_id);
    if(employee_recordsDTO == null)
    {    	
    	employee_recordsDTO = new Employee_recordsDTO();
    }
    boolean isLangEng = Language.equalsIgnoreCase("english");
    
%>

<div class="ml-auto mr-5 mt-4">
    <button type="button" class="btn" id='printer1'
            onclick="downloadPdf('Appointment.pdf','modalbody')">
        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='printer2'
            onclick="printDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='cross'
            onclick="location.href='AppointmentServlet?actionType=search'">
        <i class="fa fa-times fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="" id="modalbody">
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="d-flex align-items-center">
                            <div>
                                <img
                                        width="30%"
                                        src="<%=context%>assets/static/parliament_logo.png"
                                        alt="logo"
                                        class="logo-default"
                                />
                            </div>
                            <div class="prescription-parliament-info">
                                <h3 class="text-left">
                                    <%=LM.getText(LC.HM_PARLIAMENT_MEDICAL_CENTRE, loginDTO)%>
                                </h3>
                                <h5 class="text-left">
                                    <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                                </h5>
                                <h5 class="text-left">
                                    <%=LM.getText(LC.HM_PARLIAMENT_PHONE, loginDTO)%>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-section">
                    <div class="mt-5">
                        <h5 class="table-title"><%=LM.getText(LC.APPOINTMENT_ADD_APPOINTMENT_ADD_FORMNAME, loginDTO)%>:<%=appointmentDTO.sl%></h5>
                        <h6 class="table-title">
                        <span style="float:left">
                         <%
                                        SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm aa");
                                        value = appointmentDTO.visitDate + "";
                                    %>
                                    <%
                                        String formatted_visitDate = simpleDateFormat.format(new Date(Long.parseLong(value))) + " " +
                                                hourFormat.format(new Date(appointmentDTO.availableTimeSlot));
                                    %>
                                    <%=Utils.getDigits(formatted_visitDate, Language)%>
                        </span>
                        </h6>
                        
                        <table class="table table-bordered table-striped">
                            <thead></thead>
                            <tbody>
                            
                            <tr>
                                <td ><b><%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
                                </b></td>
                                <td>

                                    <%=WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, Language)%>
                                    <%
									 if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
									 {
									 %> 
                                    <br>
                                    <%=LM.getText(LC.HM_ID, loginDTO)%>: <%=Utils.getDigits(appointmentDTO.drUserName , Language)%>
                                    <%
									 }
                                    %>
                                </td>
                                
                                <td ><b><%=LM.getText(LC.HM_SPECIALITY, loginDTO)%>
                                </b></td>
                                <td>
									<%
									String dept = CatDAO.getName(Language, "department", doctor_time_slotDTO.deptCat);
									if(!dept.equalsIgnoreCase(""))
									{
										%>
										<%=dept%>
										<%
									}
									
									%>
								</td>
								
								<td ><b><%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>
                                </b></td>
                                <td>
                                	<%
									String medicalDept = CatDAO.getName(Language, "medical_dept", doctor_time_slotDTO.medicalDeptCat);
									if(!medicalDept.equalsIgnoreCase(""))
									{
										%>
										<%=medicalDept%>
										<%
									}
									%>

                                </td>
                              
                            </tr>
                            <tr>
                                <td ><b><%=LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)%>
                                </b></td>
                                <td>

									<%
					                    if (employee_recordsDTO.photo != null) {
					                %>
					                <img width="75px" height="75px"
					                     src='data:image/jpg;base64,<%=new String(Base64.encodeBase64(employee_recordsDTO.photo))%>'
					                     id="photo-img"
					
					                ><br>
					                <%
					                    }
					                %>
                                    <%=LM.getText(LC.HM_ID, loginDTO)%>: <%=Utils.getDigits(appointmentDTO.employeeUserName , Language)%><br>
    
    								<%=WorkflowController.getNameFromUserName(appointmentDTO.employeeUserName, Language) %>


                                </td>
                                
                                <td ><b><%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                
                                </b></td>
                                
                                <td><%=WorkflowController.getOrganogramName(appointmentDTO.employeeUserName, Language) %>
                                <%=appointmentDTO.othersDesignationAndId%>
                                </td>
                                
                                <td ><b><%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                                </b></td>
                                
                                <td><%=WorkflowController.getOfficeNameFromOrganogramId(appointmentDTO.patientId, Language) %>
                                <%=CommonDAO.getName(Language, "other_office", appointmentDTO.othersOfficeId)%>
                                </td>
                                
                            </tr>
                           


                            <tr>

                                <td ><b><%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
                                </b></td>
                                <td>

                                    <%
                                        value = appointmentDTO.patientName + "";
                                    %>

                                    <%=value%><br>
                                     <%
                                        value = appointmentDTO.phoneNumber + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>
                                <td ><b><%=LM.getText(LC.HM_RELATIONSHIP, loginDTO)%>
                                </b></td>
                                <td>

                                    <%=appointmentDAO.getRelation(appointmentDTO, isLangEng)%>


                                </td>


                            
                                


                                <td style="width:10%"><b><%=LM.getText(LC.APPOINTMENT_EDIT_AGE, loginDTO)%>
                                </b></td>
                                <td>
                                
                                <%
							        value = appointmentDTO.dateOfBirth + "";
							    %>
							    <%
							        String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
							    %>
							
							    <%=Utils.getDigits(formatted_dateOfBirth, Language)%><br>
							     <%
							        value = TimeFormat.getAge(appointmentDTO.dateOfBirth, Language);
							    %>
							
							    <%=value%>


                                </td>


                            </tr>
                            
                            <%
                            if(appointmentDTO.bearerName != null && !appointmentDTO.bearerName.equalsIgnoreCase(""))
                            {
                            %>


                            <tr>

                                <td style="width:20%"><b><%=Language.equalsIgnoreCase("english")?"Bearer's Name":"বাহকের নাম"%>

                                </b></td>
                                <td>

                                    

                                    <%=appointmentDTO.bearerName%>


                                </td>

                                <td style="width:10%"><b><%=Language.equalsIgnoreCase("english")?"Bearer's Phone Number":"বাহকের ফোন নাম্বার"%>
                                </b></td>
                                <td style="width:30%">

                                    

                                    <%=Utils.getDigits(appointmentDTO.bearerPhone, Language)%>


                                </td>
                            </tr>
                            
                            <%
                            }
                            %>

                            <%-- <tr>
                                <td style="width:20%"><b><%=LM.getText(LC.HM_ID, loginDTO)%>
                                </b></td>
                                <td colspan="3">

                                    <%
                                        if (appointmentDTO.sl != null) {
                                    %>
                                    <%=Utils.getDigits(appointmentDTO.sl, Language)%><br>
                                    <%
                                        }
                                    %>


                                </td>
                            </tr> --%>
                            
                            <%
                            	if(appointmentDTO.othersOfficeBn != null && !appointmentDTO.othersOfficeBn.equalsIgnoreCase("") && appointmentDTO.othersOfficeId != -1)
                                {
                            %>
                            	<tr>
                            	 <td style="width:20%"><b><%=LM.getText(LC.HM_OFFICE, loginDTO)%>
	                                </b></td>
	                                <td>
	                                    <%=Language.equalsIgnoreCase("english")?appointmentDTO.othersOfficeEn:appointmentDTO.othersOfficeBn%>
	                                </td>

                                <td style="width:10%"><b><%=Language.equalsIgnoreCase("english")?"Designation and Id":"পদবী এবং আইডি"%>

                                </b></td>
                                <td style="width:30%">
										<%=appointmentDTO.othersDesignationAndId%>
                                </td>
                            	</tr>
                            	<%
                            }
                            %>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>

               


