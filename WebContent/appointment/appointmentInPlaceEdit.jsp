<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="appointment.AppointmentDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
AppointmentDTO appointmentDTO = (AppointmentDTO)request.getAttribute("appointmentDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(appointmentDTO == null)
{
	appointmentDTO = new AppointmentDTO();
	
}
System.out.println("appointmentDTO = " + appointmentDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=appointmentDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_visitDate'>")%>
			
	
	<div class="form-inline" id = 'visitDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'visitDate_date_<%=i%>' name='visitDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_visitDate = dateFormat.format(new Date(appointmentDTO.visitDate));
	%>
	'<%=formatted_visitDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_specialityType'>")%>
			
	
	<div class="form-inline" id = 'specialityType_div_<%=i%>'>
		<select class='form-control'  name='specialityType' id = 'specialityType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "speciality", "specialityType_select_" + i, "form-control", "specialityType", appointmentDTO.specialityType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "speciality", "specialityType_select_" + i, "form-control", "specialityType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_doctorId'>")%>
			

		<input type='hidden' class='form-control'  name='doctorId' id = 'doctorId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + appointmentDTO.doctorId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_shiftCat'>")%>
			
	
	<div class="form-inline" id = 'shiftCat_div_<%=i%>'>
		<select class='form-control'  name='shiftCat' id = 'shiftCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "shift", appointmentDTO.shiftCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "shift", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_availableTimeSlot'>")%>
			
	
	<div class="form-inline" id = 'availableTimeSlot_div_<%=i%>'>
		<input type='text' class='form-control'  name='availableTimeSlot' id = 'availableTimeSlot_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + appointmentDTO.availableTimeSlot + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_remarks'>")%>
			
	
	<div class="form-inline" id = 'remarks_div_<%=i%>'>
		<input type='text' class='form-control'  name='remarks' id = 'remarks_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + appointmentDTO.remarks + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_patientId'>")%>
			

		<input type='hidden' class='form-control'  name='patientId' id = 'patientId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + appointmentDTO.patientId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_patientName'>")%>
			
	
	<div class="form-inline" id = 'patientName_div_<%=i%>'>
		<input type='text' class='form-control'  name='patientName' id = 'patientName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + appointmentDTO.patientName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_phoneNumber'>")%>
			
	
	<div class="form-inline" id = 'phoneNumber_div_<%=i%>'>
		<input type='number' class='form-control'  name='phoneNumber' id = 'phoneNumber_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + appointmentDTO.phoneNumber + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_dateOfBirth'>")%>
			
	
	<div class="form-inline" id = 'dateOfBirth_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'dateOfBirth_date_<%=i%>' name='dateOfBirth' value=<%
if(actionName.equals("edit"))
{
	String formatted_dateOfBirth = dateFormat.format(new Date(appointmentDTO.dateOfBirth));
	%>
	'<%=formatted_dateOfBirth%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_age'>")%>
			
	
	<div class="form-inline" id = 'age_div_<%=i%>'>
		<input type='text' class='form-control'  name='age' id = 'age_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + appointmentDTO.age + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_genderCat'>")%>
			
	
	<div class="form-inline" id = 'genderCat_div_<%=i%>'>
		<select class='form-control'  name='genderCat' id = 'genderCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "gender", appointmentDTO.genderCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "gender", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_whoIsThePatientCat'>")%>
			
	
	<div class="form-inline" id = 'whoIsThePatientCat_div_<%=i%>'>
		<select class='form-control'  name='whoIsThePatientCat' id = 'whoIsThePatientCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "who_is_the_patient", appointmentDTO.whoIsThePatientCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "who_is_the_patient", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isChargeNeeded'>")%>
			
	
	<div class="form-inline" id = 'isChargeNeeded_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isChargeNeeded' id = 'isChargeNeeded_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(appointmentDTO.isChargeNeeded).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_doctorName'>")%>
			
	
	<div class="form-inline" id = 'doctorName_div_<%=i%>'>
		<input type='text' class='form-control'  name='doctorName' id = 'doctorName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + appointmentDTO.doctorName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_deptName'>")%>
			
	
	<div class="form-inline" id = 'deptName_div_<%=i%>'>
		<input type='text' class='form-control'  name='deptName' id = 'deptName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + appointmentDTO.deptName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + appointmentDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=appointmentDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='AppointmentServlet?actionType=view&ID=<%=appointmentDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='AppointmentServlet?actionType=view&modal=1&ID=<%=appointmentDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	