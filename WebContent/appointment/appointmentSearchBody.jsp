<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "AppointmentServlet?actionType=search";

    String navigator = SessionConstants.NAV_APPOINTMENT;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.APPOINTMENT_SEARCH_APPOINTMENT_SEARCH_FORMNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="shadow-none border-0">
        <jsp:include page="./appointmentNav.jsp" flush="true">
            <jsp:param name="url" value="<%=url%>"/>
            <jsp:param name="navigator" value="<%=navigator%>"/>
            <jsp:param name="pageName"
                       value="<%=LM.getText(LC.APPOINTMENT_SEARCH_APPOINTMENT_SEARCH_FORMNAME, loginDTO)%>"/>
        </jsp:include>
        <div style="height: 1px; background: #ecf0f5"></div>
        <div class="kt-portlet shadow-none">
            <div class="kt-portlet__body">
                <form action="AppointmentServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                      method="POST"
                      id="tableForm" enctype="multipart/form-data">
                    <jsp:include page="appointmentSearchForm.jsp" flush="true">
                        <jsp:param name="pageName"
                                   value="<%=LM.getText(LC.APPOINTMENT_SEARCH_APPOINTMENT_SEARCH_FORMNAME, loginDTO)%>"/>
                    </jsp:include>
                </form>
            </div>
        </div>
    </div>
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>

<!-- begin:: Content -->


<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">

	function showRescheduleModal(modalId)
	{
		var id = modalId.split("_")[1];
		setDateByStringAndId('visitDate_date_js_' + id, $("#visitDate_date_" + id).val());
        
        $('#visitDate_date_js_' + id).on('datepicker.change', () => {
       	 console.log("onchange called");
       		getAvailableShifts(id);
        });
        
        getAvailableShifts(id);

		$('#' + modalId).modal('toggle');
	}
	
	function getAvailableShifts(id)
    {
    	var drId = $("#doctorId_select2_" + id).val();
    	var visitDate = getDateStringById('visitDate_date_js_' + id, 'DD/MM/YYYY');
        $("#visitDate_date_" + id).val(visitDate);
        
        var shiftCat = $("#shiftCat_category_" + id).val();
        console.log("shiftCat= " + shiftCat);
        
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var options = this.responseText;
                console.log(options);
                $("#shiftCat_category_" + id).html(options);
                shiftSelected(id);
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("GET", "AppointmentServlet?actionType=getAvailableShifts&doctor=" + drId + "&language=<%=Language%>&date=" + visitDate + "&shiftCat=" + shiftCat, true);
        xhttp.send();
    }


    $(document).ready(function () {

        


        $(document).on("click", '.chkEdit', function () {

            $(this).toggleClass("checked");
        });
        
        //initRescheduleModals();

        
    });

    function cancel(id, remarksId) {
        console.log("returning");
        var remarks = $("#" + remarksId).val();

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                location.replace("AppointmentServlet?actionType=search");
            }

        };
        var url = "AppointmentServlet?actionType=cancel"
            + "&id=" + id + "&remarks="
            + remarks;

        xhttp.open("GET", url, true);
        xhttp.send();

    }
    
    function reschedule(id, remarksId) {
        console.log("reschedule");
        var date = getDateStringById('visitDate_date_js_' + id, 'DD/MM/YYYY');
        var shift = $("#shiftCat_category_" + id).val();
        var slot = $("#availableTimeSlot_text_" + id).val();

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                location.replace("AppointmentServlet?actionType=search");
            }

        };
        var url = "AppointmentServlet?actionType=reschedule"
            + "&id=" + id + "&shift="
            + shift + "&slot=" + slot
            + "&date=" + date;

        xhttp.open("GET", url, true);
        xhttp.send();

    }
    
    function shiftSelected(id) {
        console.log("shiftSelected");
        
        var visitDate = getDateStringById('visitDate_date_js_' + id, 'DD/MM/YYYY');
        $("#visitDate_date_" + id).val(visitDate);

        var shift = $("#shiftCat_category_" + id).val();
        $("#availableTimeSlot_text_" + id).html();
        var doctor = $("#doctorId_select2_" + id).val();
        var date = $("#visitDate_date_" + id).val();

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response");
                    $("#availableTimeSlot_text_" + id).html(this.responseText);
                    $("#reschedule_modal_dutton_" + id).attr("disabled", false);
                } else {
                    console.log("got errror response");
                    $("#availableTimeSlot_text_" + id).html("");
                    $("#reschedule_modal_dutton_" + id).attr("disabled", true);
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
                $("#availableTimeSlot_text_" + id).html("");
                $("#reschedule_modal_dutton_" + id).attr("disabled", true);
            }
        };
        xhttp.open("GET", "AppointmentServlet?actionType=getSlot&date=" + date
            + "&doctor=" + doctor
            + "&shift=" + shift
            , true);
        xhttp.send();
    }

</script>


