<%@ page import="pb.*"%>							
<!-- Modal -->
<%@page import="workflow.WorkflowController"%>


<style>

.samewidth textarea {
    width: 100%;
    border: 1px solid #ddd;
    min-height: 100px;
}

</style>

<div id="rescheduleModal_<%=appointmentDTO.iD%>" class="modal fade" role="dialog">
  <div class="container">
  	<div>
  		<div class="modal-dialog">

    <!-- Modal content-->
   <div class="modal-content modal-xl">
     <div class="modal-header d-flex justify-content-between">       
       <h4 class="modal-title"><%=LM.getText(LC.HM_RESCHEDULE, loginDTO)%></h4>
       <button type="button" class="close text-danger" data-dismiss="modal"></button>
     </div>
     <div class="modal-body">

	<input type="hidden" name='doctorId'  id='doctorId_select2_<%=appointmentDTO.iD%>' tag='pb_html' value="<%=appointmentDTO.doctorId%>"/>
    <input type="hidden" name='currentSlotLong'  id='currentSlotLong_<%=appointmentDTO.iD%>' tag='pb_html' value="<%=appointmentDTO.availableTimeSlot%>"/>
    <input type="hidden" name='currentSlotStr'  id='currentSlotStr_<%=appointmentDTO.iD%>' tag='pb_html' value="<%=hourFormat.format(new Date(appointmentDTO.availableTimeSlot))%>"/>
                                                            
    
	<div class="form-group row">
		<label class="col-4 col-form-label">
			<%=LM.getText(LC.APPOINTMENT_ADD_VISITDATE, loginDTO)%>									
		</label>					
		<div class="col-8 samewidth">
			<%
			value= "visitDate_date_js_" + appointmentDTO.iD;
			SimpleDateFormat visitDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			formatted_visitDate = visitDateFormat.format(new Date(appointmentDTO.visitDate));
			%>	
			 <jsp:include page="/date/date.jsp">
                     <jsp:param name="DATE_ID"
                                value="<%=value%>"></jsp:param>
                     <jsp:param name="LANGUAGE"
                                value="<%=Language%>"></jsp:param>
                 </jsp:include>

                 <input type='hidden'
                        class='form-control formRequired datepicker'
                        name="visitDate" readonly="readonly"
                        data-label="Document Date" id='visitDate_date_<%=appointmentDTO.iD%>'
                        value='<%=formatted_visitDate%>'
                 
                 tag='pb_html'>								
		</div>
	</div>
    
	<div class="form-group row">
		<label class="col-4 col-form-label">
			<%=LM.getText(LC.HM_SHIFT, loginDTO)%>								
		</label>					
		<div class="col-8 samewidth">
			 <select class='form-control' required name='shiftCat'
                                                                id='shiftCat_category_<%=appointmentDTO.iD%>' onchange="shiftSelected(<%=appointmentDTO.iD%>)"
                                                                tag='pb_html'>
                                                                
                  <%
                  for(Shift_slotDTO shift_slotDTO: shift_slotDTOs)
                  {
                	  %>
                	  <option value = "<%=shift_slotDTO.shiftCat%>" <%=appointmentDTO.shiftCat == shift_slotDTO.shiftCat?"selected":""%> >
                	  <%=Utils.getDigits(TimeFormat.getInAmPmFormat(shift_slotDTO.startTime), Language)%> - <%=Utils.getDigits(TimeFormat.getInAmPmFormat(shift_slotDTO.endTime), Language)%>
                	  </option>
                	  <%
                  }
                  %>
                 
             </select>					
		</div>
	</div>
	
	<div class="form-group row">
		<label class="col-4 col-form-label">
			<%=LM.getText(LC.APPOINTMENT_ADD_AVAILABLETIMESLOT, loginDTO)%>								
		</label>					
		<div class="col-8 samewidth">
			  <select class='form-control' required name='availableTimeSlot'
                                                                id='availableTimeSlot_text_<%=appointmentDTO.iD%>'
                                                                tag='pb_html'>
               </select>			
		</div>
	</div>
	

     </div>
     <div class="modal-footer border-top-0">
       <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
       	id = "reschedule_modal_dutton_<%=appointmentDTO.iD%>"
         onclick='reschedule(<%=appointmentDTO.iD%>)'><%=LM.getText(LC.HM_RESCHEDULE, loginDTO)%></button>
      </div>
    </div>

  </div>
  	</div>
  </div>
</div>








