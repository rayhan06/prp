<%@ page import="pb.*" %>
<!-- Modal -->
<%@page import="workflow.WorkflowController" %>


<style>

    .samewidth textarea {
        width: 100%;
        border: 1px solid #ddd;
        min-height: 100px;
    }

</style>

<div id="cancelModal_<%=appointmentDTO.iD%>" class="modal fade" role="dialog">
    <div class="container">
        <div class="">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"></button>
                        <h4 class="modal-title"><%=LM.getText(LC.HM_CANCEL, loginDTO)%>
                        </h4>
                    </div>
                    <div class="modal-body">

                        <label class="col-lg-3 control-label" style="text-align:right">
                            <%=LM.getText(LC.HM_REMARKS, loginDTO)%>
                        </label>
                        <div class="form-group ">
                            <div class="col-lg-9 samewidth">
                                <textarea id="remarks_<%=appointmentDTO.iD%>" ></textarea>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger"
                                onclick='cancel("<%=appointmentDTO.iD%>", "remarks_<%=appointmentDTO.iD%>")'><%=LM.getText(LC.HM_CANCEL, loginDTO)%>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>








