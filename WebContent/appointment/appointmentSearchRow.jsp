<%@page import="shift_slot.Shift_slotDTO"%>
<%@page import="shift_slot.Shift_slotRepository"%>
<%@page import="physiotherapy_plan.Physiotherapy_planDAO"%>
<%@page import="physiotherapy_plan.PhysiotherapyScheduleDAO"%>
<%@page import="doctor_time_slot.Doctor_time_slotDAO"%>
<%@page pageEncoding="UTF-8" %>

<%@page import="appointment.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@page import="workflow.WorkflowController" %>
<%@page import="family.*" %>
<%@page import="prescription_details.*" %>
<%@page import="patient_measurement.*" %>
<%
    FamilyMemberDAO familyMemberDAO = new FamilyMemberDAO("family_member");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_APPOINTMENT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    AppointmentDTO appointmentDTO = (AppointmentDTO) request.getAttribute("appointmentDTO");
    CommonDTO commonDTO = appointmentDTO;
    String servletName = "AppointmentServlet";


    System.out.println("appointmentDTO = " + appointmentDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    AppointmentDAO appointmentDAO = (AppointmentDAO) request.getAttribute("appointmentDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat hourFormat = new SimpleDateFormat("hh:mm aa");

    Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO();
    Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByappointmentId(appointmentDTO.iD);

    boolean hasPrescription = (prescription_detailsDTO != null);
	Doctor_time_slotDAO doctor_time_slotDAO = new Doctor_time_slotDAO();
	
	Physiotherapy_planDAO physiotherapy_planDAO = new Physiotherapy_planDAO();
	UserDTO drDTO = UserRepository.getUserDTOByOrganogramID(appointmentDTO.doctorId);
	
	List<Shift_slotDTO> shift_slotDTOs = Shift_slotRepository.getInstance().getShift_slotList();
	boolean isLangEng = Language.equalsIgnoreCase("english");

%>



<td id='<%=i%>_visitDate' >
	
    <%
        value = appointmentDTO.visitDate + "";
    %>
    <%
        String formatted_visitDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>

    <%=Utils.getDigits(formatted_visitDate, Language)%><br> 
    
    <%
    if(drDTO != null && drDTO.roleID == SessionConstants.DOCTOR_ROLE)
    {
    %>
    
     <%

        value = appointmentDTO.availableTimeSlot + "";
        value = hourFormat.format(new Date(Long.parseLong(value)));
    %>


    <%=Utils.getDigits(value, Language)%> <br>
    <%=LM.getText(LC.HM_SL, loginDTO)%>:<%=Utils.getDigits(appointmentDAO.getDaysSerial(appointmentDTO), Language)%><br>
    <%
    }
    %>
    <%=Utils.getDigits(appointmentDTO.sl, Language)%>
    <%
    if(appointmentDTO.insertedByUserName != null && !appointmentDTO.insertedByUserName.equalsIgnoreCase(""))
    {
    %>
    <br>
    <%=Language.equalsIgnoreCase("english")?"Created By":"অ্যাপয়েন্টমেন্টকর্তা"%>
	:
	<%=WorkflowController.getNameFromUserName(appointmentDTO.insertedByUserName, Language) %>
	<%
    }
	%>
	
	<%
	long lastVisitDate = prescription_detailsDAO.getLastVisitDate(appointmentDTO.visitDate, appointmentDTO.erId);
	if(lastVisitDate != -1)
	{
		%>
		 <br><%=Language.equalsIgnoreCase("english")?"Last Visit Date":"শেষ ভিজিটের তারিখ"%>
		:<%=Utils.getDigits(simpleDateFormat.format(new Date(lastVisitDate)), Language)%><br> 
		<%
	}
	%>

</td>
<td id='<%=i%>_doctorId' >
    

    <b> <%=WorkflowController.getNameFromEmployeeRecordID(appointmentDTO.drEmployeeRecordId, Language)%></b>
 
 <%
 if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE)
 {
 %>  
    <br><%=LM.getText(LC.HM_ID, loginDTO)%>:<%=Utils.getDigits(appointmentDTO.drUserName, Language)%>
 <%
 }
 %>   


</td>

<td >

	<%
        value = appointmentDTO.patientName + "";
    %>
	

    <b><%=value%></b><br> 
    <%
    value = "Relation: ";
	if(!isLangEng)
	{
		value =  "সম্পর্ক: ";
	}
    %>
    <b><%=value%></b>
    
    <%=appointmentDAO.getRelation(appointmentDTO, isLangEng)%><br>
    
	
    <%=Utils.getDigits(appointmentDTO.phoneNumber, Language)%>
    <%
    if(appointmentDTO.alternatePhone != null && !appointmentDTO.alternatePhone.equalsIgnoreCase(""))
    {
    	%>
    	<br><%=Utils.getDigits(appointmentDTO.alternatePhone, Language)%>
    	<%
    }
    %>
    
	<%
	 if(appointmentDTO.bearerName != null && !appointmentDTO.bearerName.equalsIgnoreCase(""))
	 {
		 %>
		 <br><%=Language.equalsIgnoreCase("english")?"Bearer's Name":"বাহকের নাম"%>
		 : <%=appointmentDTO.bearerName%>
		 <br><%=Language.equalsIgnoreCase("english")?"Bearer's Phone Number":"বাহকের ফোন নাম্বার"%>
		 : <%=Utils.getDigits(appointmentDTO.bearerPhone, Language)%>
		 
		 <%
	 }
	 %>
                            


</td>

<td >

	

    <%=LM.getText(LC.HM_ID, loginDTO)%>: <%=WorkflowController.getUserNameFromErId(appointmentDTO.erId, isLangEng)%><br>
    
    <%=WorkflowController.getNameFromUserName(appointmentDTO.employeeUserName, Language) %><br>
    <%=WorkflowController.getOrganogramName(appointmentDTO.employeeUserName, Language) %>
    <%=appointmentDTO.othersDesignationAndId%>
    <br>
    <%=WorkflowController.getOfficeNameFromOrganogramId(appointmentDTO.patientId, Language) %>
    <%=CommonDAO.getName(Language, "other_office", appointmentDTO.othersOfficeId)%>
</td>

<td id='<%=i%>_dateOfBirth'>
    <%
        value = appointmentDTO.dateOfBirth + "";
    %>
    <%
        String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>

    <%=Utils.getDigits(formatted_dateOfBirth, Language)%><br>
     <%
        value = TimeFormat.getAge(appointmentDTO.dateOfBirth, Language);
    %>

    <%=value%>


</td>

<td class="">
	<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='AppointmentServlet?actionType=view&ID=<%=appointmentDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>
 <%
			if(appointmentDAO.canChangeAnyAppointments(userDTO))
			{
			%>
<td>
    <%
        if (!hasPrescription && !appointmentDTO.isCancelled  && 
        		appointmentDAO.canAddPatientMeasurement(userDTO, appointmentDTO.doctorId)) 
        {
    %>
     <button type="button" type="button" class="btn-sm border-0 shadow text-white btn-border-radius text-nowrap" style="background-color: #68cc6c;"
            onclick="location.href='Patient_measurementServlet?actionType=getFormattedAddPage&userName=<%=appointmentDTO.employeeUserName%>&whoIsThePatientCat=<%=appointmentDTO.whoIsThePatientCat%>'">
        <div class="d-flex justify-content-center align-items-center">
            <i class="fa fa-plus"></i>
        </div>
    </button>
    <%
        }
    %>
</td>
<%
    }
%>
<td id='<%=i%>_Addp'>

    <%
        if (!hasPrescription && userDTO.organogramID == appointmentDTO.doctorId 
        && !appointmentDTO.isCancelled && appointmentDTO.visitDate >=  TimeConverter.getToday()
        && doctor_time_slotDAO.isAvailable(appointmentDTO.doctorId)) 
        {
            if (userDTO.roleID == SessionConstants.DOCTOR_ROLE) 
            {
    %>
    
    <button type="button" type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius text-nowrap" style="background-color: #bf00ff;"
            onclick="location.href='Prescription_detailsServlet?actionType=getAddPage&appointmentId=<%=appointmentDTO.iD%>'">
        <i class="fa fa-plus"></i>&nbsp;
       <%=LM.getText(LC.HM_ADD, loginDTO)%> <%=LM.getText(LC.HM_PRESCRIPTION, loginDTO)%>
    </button>

   
    <%
    	}
            
        else  if (userDTO.roleID == SessionConstants.PHYSIOTHERAPIST_ROLE && physiotherapy_planDAO.getDTOByappointmentId(appointmentDTO.iD) == null) 
    	{
    %>
    
    <button type="button" type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius text-nowrap" style="background-color: #bf00ff;"
            onclick="location.href='Physiotherapy_planServlet?actionType=getAddPage&appointmentId=<%=appointmentDTO.iD%>'">
        <i class="fa fa-plus"></i>&nbsp;
        <%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_PLAN_ADD_FORMNAME, loginDTO)%>
    </button>
   

    <%
        }
    %>
    <%
    } else if (hasPrescription) {
    %>
   
    <button type="button" type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius text-nowrap" style="background-color: #22ccc1;"
            onclick="location.href='Prescription_detailsServlet?justView=1&actionType=view&ID=<%=prescription_detailsDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
    <%
        }
    %>

</td>
 <%
			if(appointmentDAO.canChangeAnyAppointments(userDTO))
			{
			%>
<td>
    <%
        if (!appointmentDTO.isCancelled && !hasPrescription && appointmentDAO.canChangeAnyAppointments(userDTO)) 
        {
    %>
    <div class="text-nowrap">
	    <button type="button" type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius text-nowrap" style="background-color: #ff6b6b;"
	            data-toggle="modal" data-target="#cancelModal_<%=appointmentDTO.iD%>">
	        <i class="fa fa-times"></i>
	    </button>
	    
	    
	    <button type="button" type="button" class="btn btn-sm border-0 shadow text-white btn-border-radius text-nowrap" style="background-color: #cc6b9b;"
	             onclick="showRescheduleModal('rescheduleModal_<%=appointmentDTO.iD%>', <%=appointmentDTO.doctorId%>)">
	        	<i class="fa fa-edit"></i>
	    </button>
    </div>
    <%@include file="../appointment/cancelModal.jsp" %>
    <%@include file="../appointment/rescheduleModal.jsp" %>
    <%
    } else if (appointmentDTO.isCancelled) {
    %>
    <b><%=LM.getText(LC.HM_CANCELLED_BY, loginDTO)%>: </b><%=WorkflowController.getNameFromUserName(appointmentDTO.cancellerUserName, Language)%><br>
    <b><%=LM.getText(LC.HM_ID, loginDTO)%>: </b><%=Utils.getDigits(appointmentDTO.cancellerUserName, Language)%><br>
    <b><%=LM.getText(LC.HM_REASON, loginDTO)%>: </b><%=appointmentDTO.cancellingRemarks%>
    <%
        }

    %>

	<input type='hidden' name='hiddenId' value='<%=appointmentDTO.iD%>'/>
</td>
<%
			}
%>

																						
											

