<%@page import="workflow.WorkflowController"%>
<%@page import="user.UserRepository"%>
<%@page import="user.UserDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>
<%@page import="appointment.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@page import="family.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="pb.*"%>
<%@page import="other_office.*"%>
<%
FamilyMemberDAO familyMemberDAO = new FamilyMemberDAO("family_member");
AppointmentDTO appointmentDTO;
appointmentDTO = (AppointmentDTO) request.getAttribute("appointmentDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
boolean dtoFound = true;
if (appointmentDTO == null) {
	appointmentDTO = new AppointmentDTO();
	appointmentDTO.employeeUserName = userDTO.userName;
	FamilyMemberDTO userMoreInfoDTO = WorkflowController.getFamilyMemberDTOFromOrganogramId(userDTO.organogramID);
	appointmentDTO.whoIsThePatientCat = FamilyDTO.OWN;
	if(userMoreInfoDTO != null)
	{
		appointmentDTO.dateOfBirth = userMoreInfoDTO.dateOfBirth;
		appointmentDTO.genderCat = userMoreInfoDTO.genderCat;
		appointmentDTO.patientId = userDTO.organogramID;
		appointmentDTO.phoneNumber = userMoreInfoDTO.mobile;
		if (Language.equalsIgnoreCase("bangla")) {
			appointmentDTO.patientName = userMoreInfoDTO.nameBn;
		} else {
			appointmentDTO.patientName = userMoreInfoDTO.nameEn;
		}
	}
	

	dtoFound = false;

}
System.out.println("appointmentDTO = " + appointmentDTO);

String fromPrescription = request.getParameter("fromPrescription");
System.out.println("fromPrescription = " + fromPrescription);

if (fromPrescription == null) {
	fromPrescription = "";
}

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
	actionName = "add";
} else {
	actionName = "edit";
}
System.out.println("actionName = " + actionName);

String formTitle = LM.getText(LC.APPOINTMENT_ADD_APPOINTMENT_ADD_FORMNAME, loginDTO);

String ID = request.getParameter("ID");
if (ID == null || ID.isEmpty()) {
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
CatDAO.language = Language;

String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

<%--
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<h3 class="kt-subheader__title"> Asset Management </h3>
	</div>
</div>
<!-- end:: Subheader -->--%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid"
	id="kt_content" style="padding: 0px !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<%=formTitle%>
				</h3>
			</div>
		</div>
		<form class="form-horizontal kt-form"
			action="AppointmentServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
			id="bigform" name="bigform" method="POST"
			enctype="multipart/form-data"
			onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">

			<input type='hidden' class='form-control' name='iD'
				id='iD_hidden_<%=i%>' value='<%=appointmentDTO.iD%>' tag='pb_html' />
			<input type='hidden' class='form-control' name='fromPrescription'
				id='fromPrescription' value='<%=fromPrescription%>' tag='pb_html' />
			<div class="kt-portlet__body form-body">
				<div class="row">
					<div class="col-12">
						<div class="onlyborder">
							<div class="row">
								<div class="col-md-6">
									<div class="sub_title_top">
										<div class="sub_title">
											<h4 style="background-color: white">
												<%=formTitle%>
											</h4>
										</div>
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.APPOINTMENT_ADD_VISITDATE, loginDTO)%>
											</label>
											<div class="col-md-8">

												<jsp:include page="/date/date.jsp">
													<jsp:param name="DATE_ID" value="visitDate_date_js"></jsp:param>
													<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
												</jsp:include>

												<input type='hidden'
													class='form-control formRequired datepicker'
													name="visitDate" readonly="readonly"
													data-label="Document Date" id='visitDate_date_<%=i%>'
													value=<%
													if (actionName.equals("edit")) 
													{
														String formatted_visitDate = dateFormat.format(new Date(appointmentDTO.visitDate));%>
														'<%=formatted_visitDate%>'
                                                <%
                                                	}
													else 
													{%>
                                                		'<%=datestr%>'
                                                <%
                                                	}
                                                	%>
													tag='pb_html'>

											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.HM_SPECIALITY, loginDTO)%>
											</label>
											<div class="col-md-8">
												<select class='form-control' name='specialityType'
													id='specialityType_select_<%=i%>'
													onchange="dept_selected(this, 'doctorId_select2_<%=i%>')"
													tag='pb_html'>
													<%
														if (dtoFound) {
														Options = CatRepository.getOptions(Language, "department", (int) appointmentDTO.specialityType, CatRepository.SORT_BY_ORDERING);
													} else {

														Options = CatRepository.getOptions(Language, "department", -1, CatRepository.SORT_BY_ORDERING);
													}
													%>
													<%=Options%>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.HM_DOCTOR, loginDTO)%> /<%=LM.getText(LC.PHYSIOTHERAPY_PLAN_ADD_PHYSIOTHERAPY_SCHEDULE_THERAPISTUSERID, loginDTO)%>
											</label>
											<div class="col-md-8">
												<select class='form-control' required name='doctorId'
													onchange="drSelected()" id='doctorId_select2_<%=i%>'
													tag='pb_html'>
													<%
														if (dtoFound) {
														Options = CommonDAO.getDoctorsAndTherapistsByOrganogramID(appointmentDTO.doctorId);
													} else {
														Options = CommonDAO.getDoctorsAndTherapistsByOrganogramID(-1);
													}
													%>
													<%=Options%>
												</select>
											</div>
										</div>
										
										<div class="form-group row" id="shiftDiv">
											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.HM_SHIFT, loginDTO)%>
											</label>
											<div class="col-md-8">
												<select class='form-control' required name='shiftCat'
													id='shiftCat_category_<%=i%>' onchange="shiftSelected()"
													tag='pb_html'>
													<option value = ""></option>
												</select>
											</div>
										</div>
										<div class="form-group row" id="slotDiv">
											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.APPOINTMENT_ADD_AVAILABLETIMESLOT, loginDTO)%>
											</label>
											<div class="col-md-8">
												<select class='form-control' required
													name='availableTimeSlot' id='availableTimeSlot_text_<%=i%>'
													tag='pb_html'></select>
											</div>
										</div>
										
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.APPOINTMENT_ADD_REMARKS, loginDTO)%>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' name='remarks'
													id='remarks_text_<%=i%>'
													value=<%=dtoFound ? ("'" + appointmentDTO.remarks + "'") : ("'" + "" + "'")%>
													tag='pb_html' />
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="sub_title_top">
										<div class="sub_title">
											<h4 style="background-color: white"><%=LM.getText(LC.APPOINTMENT_ADD_PATIENTNAME, loginDTO)%>
											</h4>
										</div>
										<%
											if (dtoFound && (appointmentDTO.employeeUserName.equalsIgnoreCase("")
												|| appointmentDTO.employeeUserName.equalsIgnoreCase(FamilyDTO.defaultUser))) {
										%>
										<input type='hidden' class='form-control' name='userName'
											onfocusout="appointing_patient_inputted(this.value)" id='userName'
											value='<%=Utils.getDigits(appointmentDTO.employeeUserName, Language)%>'
											tag='pb_html' />
										<%
											} else {
										%>
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.APPOINTMENT_ADD_PATIENTID, loginDTO)%>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' name='userName'
													onfocusout="appointing_patient_inputted(this.value)"
													onkeyup="$('#showDetailButton').css('display','block')"
													id='userName'
													value='<%=Utils.getDigits(appointmentDTO.employeeUserName, Language)%>'
													tag='pb_html' />
												<button type="button" id="showDetailButton"
													class="btn btn-info" style="display: none"
													onclick="appointing_patient_inputted($('#userName').val())"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
												</button>
											</div>
										</div>
										<%
											}
										%>
										<div class="form-group row">

											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.SELECT_PATIENT_HM, loginDTO)%>
											</label>
											<div class="col-md-8">
												<select class='form-control' required
													name='whoIsThePatientCat'
													onchange="getPatientInfo(this.value)"
													<%=dtoFound && appointmentDTO.whoIsThePatientCat == FamilyDTO.OTHER ? "disabled" : ""%>
													id='whoIsThePatientCat_category_<%=i%>' tag='pb_html'>
												</select>
											</div>

										</div>


										<div class="form-group row">

											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.APPOINTMENT_ADD_PATIENTNAME, loginDTO)%>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' required
													name='patientName' id='patientName_text_<%=i%>'
													value='<%=appointmentDTO.patientName%>'
													<%=(dtoFound && appointmentDTO.whoIsThePatientCat != -3) ? "readonly" : ""%>
													tag='pb_html' />
											</div>

										</div>
										
										<div id = "extraDiv" style = "display:none">
											<div class="form-group row">
												<label class="col-md-4 col-form-label text-md-right">
														<%=Language.equalsIgnoreCase("english")?"Relation":"সম্পর্ক"%>
												</label>
												<div class="col-md-8">
													<input type='text' class='form-control'
														name='extraRelation' id='extraRelation'
														value='<%=appointmentDTO.extraRelation%>'
														tag='pb_html' />
												</div>
											</div>	
										</div>
										
										
										
										<div id = "othersOfficeDiv" style = "display:none">
										
											<div class="form-group row">
	
												<label class="col-md-4 col-form-label text-md-right">
													<%=LM.getText(LC.HM_OFFICE, loginDTO)%>
												</label>
												<div class="col-md-8">																											
													<select class='form-control' name='othersOfficeId'
                                                            id='currentOffice_category' tag='pb_html'>
                                                        <%=Other_officeRepository.getInstance().buildOptions(Language, null)%>
                                                    </select>
												</div>	
											</div>
											
											<div class="form-group row">
	
												<label class="col-md-4 col-form-label text-md-right">
													<%=Language.equalsIgnoreCase("english")?"Designation and Id":"পদবী এবং আইডি"%>
												</label>
												<div class="col-md-8">
													<input type='text' class='form-control'
														name='othersDesignationAndId' id='othersDesignationAndId'
														value='<%=appointmentDTO.othersDesignationAndId%>'
														tag='pb_html' />
												</div>	
											</div>
											
											
										</div>

										<div class="form-group row">

											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.APPOINTMENT_ADD_PHONENUMBER, loginDTO)%>
											</label>
											<div class="col-md-8">
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text"><%=Language.equalsIgnoreCase("english") ? "+88" : "+৮৮"%></div>
													</div>
													<input type='text' class='form-control' required
														name='phoneNumber' id='phoneNumber_number_<%=i%>' required
														value='<%=Utils.getPhoneNumberWithout88(appointmentDTO.phoneNumber, Language)%>'
														<%=(dtoFound && appointmentDTO.whoIsThePatientCat != -3) ? "readonly" : ""%>
														tag='pb_html'>
												</div>

											</div>

										</div>

										<div class="form-group row">

											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.APPOINTMENT_ADD_DATEOFBIRTH, loginDTO)%>
											</label>
											<div class="col-md-8">
												<jsp:include page="/date/date.jsp">
													<jsp:param name="DATE_ID" value="dateOfBirth_date_js"></jsp:param>
													<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
												</jsp:include>
												<input type='hidden'
													class='form-control
                                                            <%=(appointmentDTO.whoIsThePatientCat < 0 && appointmentDTO.whoIsThePatientCat != FamilyDTO.OWN)
															? "formRequired datepicker"
															: ""%>'
													required name="dateOfBirth" readonly="readonly"
													data-label="Document Date" id='dateOfBirth_date_<%=i%>'
													"readonly" value=<%if (!dtoFound || appointmentDTO.whoIsThePatientCat != -3) {
													String formatted_dateOfBirth = dateFormat.format(new Date(appointmentDTO.dateOfBirth));%>
													'<%=formatted_dateOfBirth%>'
                                                <%} else {%>
                                                ''
                                                <%}%>
													tag='pb_html'>
											</div>

										</div>

										<div class="form-group row">

											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.APPOINTMENT_ADD_GENDERCAT, loginDTO)%>
											</label>
											<div class="col-md-8">
												<select class='form-control' required name='genderCat'
													id='genderCat_category_<%=i%>'
													<%=(dtoFound && appointmentDTO.whoIsThePatientCat != FamilyDTO.OTHER) ? "disabled" : "required"%>
													tag='pb_html'>
													<%
														System.out.println("appointmentDTO.genderCat = " + appointmentDTO.genderCat);
													Options = CatDAO.getOptions(Language, "gender", appointmentDTO.genderCat);
													%>
													
													<%=Options%>
												</select>
											</div>

										</div>
										
										
										
										<div class="form-group row">

											<div class="col-md-4 col-form-label text-md-right">
												<input type='checkbox' name='bearer' id='bearer' class='form-control-sm' onchange='showHideBearer()'/>
											</div>
											<label class="col-md-8 col-form-label text-md-left">
												<b><%=Language.equalsIgnoreCase("english")?"Bearer's Details":"বাহকের বিস্তারিত"%>
												</b>
											</label>
											
										</div>
										
										
										
										<div id = "bearerDiv" style = "display:none">
										
											
											
											
											<div class="form-group row">
	                                            <label class="col-md-4 col-form-label text-md-right">
	                                                <%=Language.equalsIgnoreCase("english")?"Select Bearer":"বাহক বাছাই করুন"%>
	
	                                            </label>
	                                            <div class="col-md-8">
	                                                <button type="button"
	                                                        class="btn text-white shadow form-control btn-border-radius"
	                                                        style="background-color: #4a87e2"
	                                                        onclick="addEmployee()"
	                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
	                                                </button>
	                                                <table class="table table-bordered table-striped"
	                                                       style="display:none">
	                                                    <tbody id="employeeToSet"></tbody>
	                                                </table>
	                                                <input type='hidden' class='form-control'
                                                               
                                                               name='bearerUserName' id='bearerUserName'
                                                               tag='pb_html'/>
	                                            </div>
	                                        </div>
										
											<div class="form-group row">
	
												<label class="col-md-4 col-form-label text-md-right">
													<%=Language.equalsIgnoreCase("english")?"Bearer's Name":"বাহকের নাম"%>
												</label>
												<div class="col-md-8">																											
													<input type='text' class='form-control'
														name='bearerName' id='bearerName'
														value='<%=appointmentDTO.bearerName%>'
														tag='pb_html' />
												</div>	
											</div>
											
											<div class="form-group row">
	
												<label class="col-md-4 col-form-label text-md-right">
													<%=Language.equalsIgnoreCase("english")?"Bearer's Phone Number":"বাহকের ফোন নাম্বার"%>
												</label>
												
												
												<div class="col-md-8">
													<div class="input-group mb-2">
														<div class="input-group-prepend">
															<div class="input-group-text"><%=Language.equalsIgnoreCase("english") ? "+88" : "+৮৮"%></div>
														</div>
														<input type='text' class='form-control' 
															name='bearerPhone' id='bearerPhone_number_<%=i%>' 
															value=''
															tag='pb_html'>
													</div>
													
												</div>	
											</div>
											
											
										</div>
									</div>
								</div>
								<input type='hidden' class='form-control' name='isChargeNeeded'
									id='isChargeNeeded_checkbox_<%=i%>' value='true' tag='pb_html'><br>
								<input type='hidden' class='form-control' name='isDeleted'
									id='isDeleted_hidden_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + appointmentDTO.isDeleted + "'") : ("'" + "false" + "'")%>
									tag='pb_html' /> <input type='hidden' class='form-control'
									name='lastModificationTime'
									id='lastModificationTime_hidden_<%=i%>'
									value=<%=actionName.equals("edit") ? ("'" + appointmentDTO.lastModificationTime + "'") : ("'" + "0" + "'")%>
									tag='pb_html' />
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions text-right mt-3">
					<button id="cancel-btn"
						class="btn-sm shadow text-white border-0 cancel-btn">
						<%
							if (actionName.equals("edit")) {
						%>
						<%=LM.getText(LC.APPOINTMENT_EDIT_APPOINTMENT_CANCEL_BUTTON, loginDTO)%>
						<%
							} else {
						%>
						<%=LM.getText(LC.APPOINTMENT_ADD_APPOINTMENT_CANCEL_BUTTON, loginDTO)%>
						<%
							}
						%>
					</button>
					<button class="btn-sm shadow text-white border-0 submit-btn ml-2"
						type="submit" id = "submitButton">
						<%
							if (actionName.equals("edit")) {
						%>
						<%=LM.getText(LC.APPOINTMENT_EDIT_APPOINTMENT_SUBMIT_BUTTON, loginDTO)%>
						<%
							} else {
						%>
						<%=LM.getText(LC.APPOINTMENT_ADD_APPOINTMENT_SUBMIT_BUTTON, loginDTO)%>
						<%
							}
						%>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    function patientSelected(select) {
        console.log($("#" + select).find('option:selected').attr('empname'));
        var name = $("#" + select).find('option:selected').attr('empname');
        $("#patientName_text_0").val(name);
    }
    
    

    function shiftSelected() {
        console.log("shiftSelected");
        
        <%
        if(Language.equalsIgnoreCase("english"))
        {
        	%>
        	$("#availableTimeSlot_text_<%=i%>").html("<option value = ''>Loading ...</option>");
        	<%
        }
        else
        {
        %>
        	$("#availableTimeSlot_text_<%=i%>").html("<option value = ''>লোড হচ্ছে ...</option>");
        <%
        }
        %>
        

        var visitDate = getDateStringById('visitDate_date_js', 'DD/MM/YYYY');
        $("#visitDate_date_0").val(visitDate);

        var shift = $("#shiftCat_category_<%=i%>").val();
        var doctor = $("#doctorId_select2_<%=i%>").val();
        var date = $("#visitDate_date_<%=i%>").val();

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.includes('option')) {
                    console.log("got response");
                    $("#availableTimeSlot_text_<%=i%>").html(this.responseText);
                } else {
                    console.log("got errror response : " + this.responseText);
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "AppointmentServlet?actionType=getSlot&date=" + date
            + "&doctor=" + doctor
            + "&shift=" + shift
            , true);
        xhttp.send();
    }


    $(document).ready(function () {
        birthDateTimeInit("<%=Language%>");

        init(row);
        CKEDITOR.replaceAll();
        /*$("#doctorId_select2_0").select2({
            dropdownAutoWidth: true
        });*/
        appointing_patient_inputted($("#userName").val());

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        
		$("#visitDate_date_js").on('datepicker.change', () => {
			drSelected();
        });
    });

    function PreprocessBeforeSubmiting(row, validate) {
    	if($("#patientName_text_0").val() == "")
   		{
   			toastr.error("Name is required");
   			return false;
   		}
    	if($("#phoneNumber_number_0").val() == "")
   		{
   			toastr.error("Phone Number is required");
   			return false;
   		}
    	$("#submitButton").prop("disabled",true);

        $("select").prop("disabled", false);

       	var convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($('#phoneNumber_number_<%=i%>').val(), '<%=Language%>');
        $("#phoneNumber_number_<%=i%>").val(convertedPhoneNumber);
        
        if($("#bearerPhone_number_0").val() != "")
   		{
	        convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($('#bearerPhone_number_<%=i%>').val(), '<%=Language%>');
	        $("#bearerPhone_number_<%=i%>").val(convertedPhoneNumber);
   		}

        <%if (userDTO.roleID == SessionConstants.NURSE_ROLE || userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE
		|| userDTO.roleID == SessionConstants.DOCTOR_ROLE
		|| userDTO.roleID == SessionConstants.MEDICAL_RECEPTIONIST_ROLE
		|| userDTO.roleID == SessionConstants.ADMIN_ROLE) {

		} else {%>
        if (convertToEnglishNumber($('#doctorId_select2_<%=i%>').val()) != <%=userDTO.organogramID%>
            && convertToEnglishNumber($('#userName').val()) != <%=userDTO.userName%>) {
            toastr.error("You cannot create appointment for others");
            return false;
        }
        <%}%>
        preprocessCheckBoxBeforeSubmitting('isChargeNeeded', row);
        var visitDate = getDateStringById('visitDate_date_js', 'DD/MM/YYYY');
        $("#visitDate_date_0").val(visitDate);


        //$("#dateOfBirth_date_js :select").removeAttr('disabled');
        var birthDate = getDateStringById('dateOfBirth_date_js', 'DD/MM/YYYY');
        $("#dateOfBirth_date_0").val(birthDate);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "AppointmentServlet");
    }

    function init(row) {

        setDateByStringAndId('dateOfBirth_date_js', $("#dateOfBirth_date_<%=i%>").val());
        setDateByStringAndId('visitDate_date_js', $("#visitDate_date_<%=i%>").val());
        setMinDateById('visitDate_date_js', $("#visitDate_date_<%=i%>").val());
        drSelected();

    }

    function disableFields() {
        console.log("disabling fields");
        $('#patientName_text_<%=i%>').prop("readonly", true);
        $('#phoneNumber_number_<%=i%>').prop("readonly", true);
        $('#genderCat_category_<%=i%>').prop("disabled", true);
        $("#dateOfBirth_date_js").find('select').attr('disabled', 'disabled');

    }

    function enableFields() {
        console.log("enabling fields");
        $('#patientName_text_<%=i%>').prop("readonly", false);
        $('#phoneNumber_number_<%=i%>').prop("readonly", false);
        $('#genderCat_category_<%=i%>').prop("disabled", false);
        $("#dateOfBirth_date_js").find('select').removeAttr('disabled');

    }

    function clearFields() {
        $('#patientName_text_<%=i%>').val("");
        $('#phoneNumber_number_<%=i%>').val("");
        $('#othersOfficeId').val("-1");
        $('#othersDesignationAndId').val("");
        $('#genderCat_category_<%=i%>').val("");
        resetDateById('dateOfBirth_date_js');
       

    }

    function getPatientInfo(value) {
        console.log('changed value: ' + value);

        var familyMemberId = value;
        var userName = $("#userName").val();

        if (value == <%=FamilyDTO.OWN%>) {
            disableFields();
        } else {
            enableFields();
        }
        
        if (value == <%=FamilyDTO.OTHER%>) {
            $("#extraDiv").removeAttr("style");
        } else {
        	$("#extraDiv").css("display", "block");
        }

        clearFields();

        if (value == <%=FamilyDTO.OWN%> || value >= 0) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var familyMemberDTO = JSON.parse(this.responseText);
                    <%if (Language.equalsIgnoreCase("english")) {%>
                    $('#patientName_text_<%=i%>').val(familyMemberDTO.nameEn);
                    <%} else {%>
                    $('#patientName_text_<%=i%>').val(familyMemberDTO.nameBn);
                    <%}%>
                    $('#phoneNumber_number_<%=i%>').val(phoneNumberRemove88ConvertLanguage(familyMemberDTO.mobile, '<%=Language%>'));
                    $('#dateOfBirth_date_<%=i%>').val(familyMemberDTO.dateOfBirthFormatted);
                    setDateByStringAndId('dateOfBirth_date_js', $("#dateOfBirth_date_<%=i%>").val());
                    $('#genderCat_category_<%=i%>').val(familyMemberDTO.genderCat);
                } else {
                    //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
                }
            };

            xhttp.open("POST", "FamilyServlet?actionType=getPatientDetails&familyMemberId=" + familyMemberId + "&userName=" + userName + "&language=<%=Language%>", true);
            xhttp.send();
        }

    }

    function appointing_patient_inputted(value) {
        console.log('changed value: ' + value);
        
        if(convertToEnglishNumber(value) == '<%=SessionConstants.OUTSIDER_PATIENT%>')
       	{
        	console.log("Other user detected");
       		var html = '<option value="<%=FamilyDTO.OTHER%>" selected><%=LM.getText(LC.HM_OTHERS, loginDTO)%></option>';
       		$("#whoIsThePatientCat_category_<%=i%>").html(html);
       		$("#othersOfficeDiv").removeAttr("style");
       		$("#currentOffice_category").attr("required", "required");
       		//getPatientInfo($("#whoIsThePatientCat_category_<%=i%>").val());
       		enableFields();
       		clearFields();
       	}
        else
       	{
        	console.log("Other user not detected");
        	$("#othersOfficeDiv").css("display", "none");
        	$("#currentOffice_category").removeAttr("required");
        	var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var options = this.responseText;
                    $("#whoIsThePatientCat_category_<%=i%>").html(options);
                    getPatientInfo($("#whoIsThePatientCat_category_<%=i%>").val());
                } else {
                    //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
                }
            };

            xhttp.open("POST", "FamilyServlet?actionType=getFamily&userName=" + value + "&language=<%=Language%>&defaultOption=<%=appointmentDTO.whoIsThePatientCat%>", true);
            xhttp.send();
       	}
    }

    var row = 0;

    window.onload = function () {

    }

    var child_table_extra_id = <%=childTableStartingID%>;

    function drSelected() {
        var element = $("#doctorId_select2_<%=i%>").find('option:selected');
        var role = element.attr("role");
        
        console.log("role found = " + role);

        if (role == <%=SessionConstants.DOCTOR_ROLE%>) {
            $("#shiftDiv").removeAttr("style");
            $("#slotDiv").removeAttr("style");
            $("#availableTimeSlot_text_<%=i%>").prop('required', true);
            $("#shiftCat_category_<%=i%>").prop('required', true);
            getAvailableShifts();
        } else {
            $("#shiftDiv").css("display", "none");
            $("#slotDiv").css("display", "none");
            $("#availableTimeSlot_text_<%=i%>").prop('required', false);
            $("#shiftCat_category_<%=i%>").prop('required', false);
        }

    }
    
    function getAvailableShifts()
    {
    	var drId = $("#doctorId_select2_<%=i%>").val();
    	var visitDate = getDateStringById('visitDate_date_js', 'DD/MM/YYYY');
        $("#visitDate_date_0").val(visitDate);
        
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var options = this.responseText;
                console.log(options);
                $("#shiftCat_category_<%=i%>").html(options);
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("GET", "AppointmentServlet?actionType=getAvailableShifts&doctor=" + drId + "&language=<%=Language%>&date=" + visitDate, true);
        xhttp.send();
    }
    
    function patient_inputted(userName, orgId) {
        console.log("patient_inputted " + userName);
        $("#bearerUserName").val(userName);
        bearer_inputted(userName);
        
    }
    
    
    function bearer_inputted(value) {
        console.log('changed bearer: ' + value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	var userNameAndDetails = JSON.parse(this.responseText);
            	$("#bearerName").val(convertNumberToLanguage(userNameAndDetails.name));
                $("#bearerPhone_number_0").val(phoneNumberRemove88ConvertLanguage(userNameAndDetails.phone, '<%=Language%>'));
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getEmployeeInfo&userName=" + value + "&language=<%=Language%>", true);
        xhttp.send();
    }
    
    function showHideBearer()
    {
    	if($("#bearer").prop("checked"))
   		{
   			$("#bearerDiv").removeAttr("style");
   		}
    	else
   		{
    		$("#bearerDiv").css("display", "none");
   		}
    }


</script>






