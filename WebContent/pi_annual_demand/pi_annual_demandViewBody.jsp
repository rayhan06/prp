<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="util.CommonDTO" %>
<%@ page import="pi_annual_demand.*" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="pi_package_new.Pi_package_newDTO" %>
<%@ page import="pi_package_new.Pi_package_newRepository" %>
<%@ page import="pi_package_lot.Pi_package_lotDTO" %>
<%@ page import="pi_package_lot.Pi_package_lotDAO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="pi_unit.Pi_unitDTO" %>
<%@ page import="pi_unit.Pi_unitRepository" %>

<%@include file="../pb/viewInitializer.jsp" %>
<%
    String servletName = "Pi_annual_demandServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Pi_annual_demandDTO pi_annual_demandDTO = Pi_annual_demandDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = pi_annual_demandDTO;

    String nameOfOffice = "";
    Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(pi_annual_demandDTO.officeUnitId);
    if (unit != null) {
        nameOfOffice = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
    }

    String packageLotName = "";
    Pi_package_newDTO packageModel = Pi_package_newRepository.getInstance()
            .getPi_package_newDTOByiD(pi_annual_demandDTO.piPackageNewId);
    Pi_package_lotDTO lotModel = Pi_package_lotDAO.getInstance().getDTOByID(pi_annual_demandDTO.piLotId);
    if (packageModel != null) {
        packageLotName = UtilCharacter.getDataByLanguage(Language, packageModel.packageNumberBn, packageModel.packageNumberEn);
    }
    if (lotModel != null) {
        packageLotName += "(" + UtilCharacter.getDataByLanguage(Language, lotModel.lotNumberBn, lotModel.lotNumberEn) + ")-";
        packageLotName += UtilCharacter.getDataByLanguage(Language, lotModel.lotNameBn, lotModel.lotNameEn);
    }
    if (packageModel != null) {
        packageLotName += UtilCharacter.getDataByLanguage(Language, packageModel.packageNameBn, packageModel.packageNameEn);
    }

    String fiscalYear = UtilCharacter.getDataByLanguage(Language, "অর্থ বছর-", "Fiscal Year-");
    Fiscal_yearDTO fiscalYearModel = Fiscal_yearRepository.getInstance().getFiscal_yearDTOByid(pi_annual_demandDTO.fiscalYear);
    fiscalYear += UtilCharacter.getDataByLanguage(Language, fiscalYearModel.nameBn, fiscalYearModel.nameEn);

    List<PiAnnualDemandChildDTO> childModels = PiAnnualDemandChildRepository.getInstance()
            .getPiAnnualDemandChildDTOBypiAnnualDemandId(id);
    childModels = childModels.stream().sorted(Comparator.comparingLong(dto -> dto.serialNumber)).collect(Collectors.toList());
%>

<style>
    .form-group label {
        font-weight: 600 !important;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    th {
        text-align: center;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 4px;
    }

    .center {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
    }

    .page {
        background: rgba(255, 255, 255, 0.85);
        padding: .5in;
        margin-bottom: 5px;
        page-break-after: always;
        box-shadow: rgba(131, 131, 109, 0.85);
    }

    @media print {
        @page {
            size: landscape;
            margin: .25in;
        }
    }
</style>

<div class="kt-content p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">

                </h3>
            </div>
        </div>

        <div class="kt-portlet__body" id="bill-div">
            <%--            DOWNLOAD BUTTON--%>
            <div class="ml-auto m-5">
                <button type="button" class="btn" id='download-pdf'
                        onclick="printDivWithJqueryPrint('to-print-div');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>

            <div style="margin: auto;">
                <div class="container shadow p-4">
                    <div id="to-print-div">
                        <%--                        <section class="page" data-size="A4-landscape">--%>
                        <div class="">
                            <%------------------------------------------------------------------------------------------------------------------%>
                            <%--DYNAIMICALLY GENERATE TABLE WHICH FITS DATA IN MULTIPLE PAGE--%>
                            <%--TABLE CONFIG--%>
                            <%
                                boolean isLastPage = false;
                                final int rowsPerPage = 20;
                                int index = 0;
                                while (index < childModels.size()) {
                                    boolean isFirstPage = (index == 0);
                            %>
                            <section class="page" data-size="A4-landscape">
                                <%if (isFirstPage) {%>
                                <div class="text-center">
                                    <h1 class="font-weight-bold text-center">
                                        <h3>
                                            <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                        </h3>
                                        <h5><%=nameOfOffice%>
                                        </h5>
                                        <h5><%=packageLotName%>
                                        </h5>
                                        <h5><%=fiscalYear%>
                                        </h5>
                                    </h1>
                                </div>
                                <%}%>
                                <div>
                                    <table class="table table-bordered text-center">
                                        <thead style="background-color:lightgrey">
                                        <tr>
                                            <th rowspan="2"
                                                style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                                            </th>
                                            <th rowspan="2"
                                                style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মালামালের বিবরন", "Item Description")%>
                                            </th>
                                            <th rowspan="1"
                                                colspan="2"
                                                style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "পরিমাণ", "Quantity")%>
                                            </th>
                                            <th rowspan="2"
                                                style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "একক দর (টাকায়)", "Unit Price (BDT)")%>
                                            </th>
                                            <th rowspan="2"
                                                style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মোট দর (টাকায়)", "Total Price (BDT)")%>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "সংখ্যা", "Number")%>
                                            </th>
                                            <th style="text-align: center;"><%=UtilCharacter.getDataByLanguage(Language, "একক", "Unit")%>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%if (!isFirstPage) {%>
                                        <%}%>

                                        <%
                                            int rowsInThisPage = 0;
                                            while (index < childModels.size() && rowsInThisPage < rowsPerPage) {
                                                isLastPage = (index == (childModels.size() - 1));
                                                rowsInThisPage++;
                                                PiAnnualDemandChildDTO model = childModels.get(index++);
                                        %>
                                        <tr>
                                            <td>
                                                <%=Utils.getDigits(model.serialNumber, Language)%>
                                            </td>
                                            <td>
                                                <%=model.description%>
                                            </td>
                                            <td>
                                                <%=Utils.getDigits(model.itemQuantity, Language)%>
                                            </td>
                                            <td>
                                                <%
                                                    String unitName = "";
                                                    Pi_unitDTO unitModel = Pi_unitRepository.getInstance()
                                                            .getPi_unitDTOByiD(model.piUnitId);
                                                    unitName = unitModel == null ? "" :
                                                            UtilCharacter.getDataByLanguage(Language, unitModel.nameBn, unitModel.nameEn);
                                                %>
                                                <%=unitName%>
                                            </td>
                                            <td>
                                                <%=Utils.getDigits(model.itemUnitPrice, Language)%>
                                            </td>
                                            <td>
                                                <%=Utils.getDigits(model.itemTotalPrice, Language)%>
                                            </td>
                                        </tr>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                        <tfoot>
                                        <%if (isLastPage) {%>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><%=UtilCharacter.getDataByLanguage(Language, "সর্বমোট-", "Grand Total-")%>
                                            </td>
                                            <td>
                                                <%
                                                    long grandTotal = 0;
                                                    for (PiAnnualDemandChildDTO model : childModels) {
                                                        grandTotal += model.itemUnitPrice * model.itemQuantity;
                                                    }
                                                %>
                                                <%=Utils.getDigits(grandTotal, Language)%>
                                            </td>
                                        </tr>
                                        <%}%>
                                        </tfoot>
                                    </table>
                                </div>

                                <%if (isLastPage) {%>
                                <%}%>
                            </section>
                                <%
                                    }
                                %>
                        </div>
                        <%--                        </section>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../utility/jquery_print.jsp"/>