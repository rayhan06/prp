<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_annual_demand.*" %>
<%@ page import="util.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="pi_package_new.Pi_package_newRepository" %>
<%@ page import="pi_package_lot.Pi_package_lotDAO" %>

<%
    String navigator2 = "navPI_ANNUAL_DEMAND";
    String servletName = "Pi_annual_demandServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_NOTHINO, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_CREATIONDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_FISCALYEAR, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "দপ্তরের নাম", "Office Name")%>
            </th>
            <th><%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_HASPACKAGELOT, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নাম্বার", "Package Number")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "লট নাম্বার", "Lot Number")%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_ANNUAL_DEMAND_SEARCH_PI_ANNUAL_DEMAND_EDIT_BUTTON, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ডিলিট করুন", "Delete")%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Pi_annual_demandDTO>) rn2.list;
            try {
                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Pi_annual_demandDTO model = (Pi_annual_demandDTO) data.get(i);
        %>
        <tr>
            <input type="hidden" id="annualDemandId" value="<%=model.iD%>">
            <td>
                <%
                    value = model.nothiNo + "";
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = model.creationDate + "";
                %>
                <%
                    String formatted_creationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_creationDate, Language)%>
            </td>
            <td>
                <%
                    value = Fiscal_yearRepository.getInstance().getText(model.fiscalYear, Language);
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>
            <td>
                <%
                    value = Office_unitsRepository.getInstance().geText(Language, model.officeUnitId) + "";
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>
            <td>
                <%
                    value = model.hasPackageLot + "";
                %>
                <%=Utils.getYesNo(value, Language)%>
            </td>
            <td>
                <%
                    value = model.piPackageNewId == -1 ? "" :
                            Pi_package_newRepository.getInstance().getText(Language, model.piPackageNewId) + "";
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>
            <td>
                <%
                    value = model.piLotId == -1 ? "" :
                            Pi_package_lotDAO.getInstance().getText(Language, model.piLotId) + "";
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>

            <%CommonDTO commonDTO = model; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-center">
                <button
                        type="button"
                        class="btn btn-sm remove-btn shadow ml-2 pl-4"
                        onclick="deleteAnnualDemand(this)">
                    <i class="fa fa-trash"></i>
                </button>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<script type="text/javascript">
    function deleteAnnualDemand(element) {
        let tr = element.parentNode.parentNode;
        let annualDemandId = tr.querySelector("#annualDemandId").value;
        let url = "Pi_annual_demandServlet?actionType=deleteAnnualDemandWithValidation&annualDemandId=" + annualDemandId;
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                response = JSON.parse(response);
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    tr.remove();
                    showToastSticky("ডিলিট সফল হয়েছে", "Delete Successful");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }
</script>