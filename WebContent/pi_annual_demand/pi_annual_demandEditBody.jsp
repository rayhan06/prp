<%@page import="pi_annual_demand.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="pi_package_new.Pi_package_newDTO" %>
<%@ page import="pi_package_new.Pi_package_newRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pi_unit.Pi_unitRepository" %>
<%@ page import="pi_unit.Pi_unitDTO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="dbm.DBMW" %>

<%@include file="../pb/addInitializer2.jsp" %>

<%
    Pi_annual_demandDTO pi_annual_demandDTO = new Pi_annual_demandDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_annual_demandDTO = Pi_annual_demandDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_annual_demandDTO;
    String tableName = "pi_annual_demand";
    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
    Fiscal_yearDTO cur_fis_dto = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());

    long currentOfficeUnitId = -1;

    String nameOfOffice = "";
    if (actionName.equalsIgnoreCase("ajax_edit")) {
        Office_unitsDTO unit = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(pi_annual_demandDTO.officeUnitId);
        if (unit != null) {
            nameOfOffice = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
        }
    } else {
        OfficeUnitOrganograms organogramsDTO = OfficeUnitOrganogramsRepository.getInstance().getById(userDTO.organogramID);
        if (organogramsDTO != null) {
            Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(organogramsDTO.office_unit_id);
            if (office_unitsDTO != null) {
                nameOfOffice = UtilCharacter.getDataByLanguage(Language, office_unitsDTO.unitNameBng, office_unitsDTO.unitNameEng);
                currentOfficeUnitId = office_unitsDTO.iD;
            }
        }
    }
%>
<%
    String formTitle = UtilCharacter.getDataByLanguage(Language, "বার্ষিক চাহিদা", "Annual Demand");
    String servletName = "Pi_annual_demandServlet";
%>

<style>
    .hiddenElement {
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigForm" name="bigForm">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=pi_annual_demandDTO.iD%>' tag='pb_html'/>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right">
                                                <%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_NOTHINO, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='nothiNo'
                                                       id='nothiNo_text_<%=i%>' value='<%=pi_annual_demandDTO.nothiNo%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right">
                                                <%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_CREATIONDATE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-8">
                                                <%value = "creationDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='creationDate' id='creationDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_annual_demandDTO.creationDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right">
                                                <%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_FISCALYEAR, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-8">
                                                <select class='form-control' name='fiscalYear' id='fiscalYear'
                                                        tag='pb_html'>
                                                    <%
                                                        String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                        StringBuilder option = new StringBuilder();
                                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {

                                                            if (actionName.equals("ajax_edit")) {
                                                                if (pi_annual_demandDTO.fiscalYear == fiscal_yearDTO.id) {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                                } else {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                                }
                                                            } else {
                                                                if (cur_fis_dto.id == fiscal_yearDTO.id) {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                                } else {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                                }
                                                            }

                                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                        }
                                                        fiscalYearOptions += option.toString();
                                                    %>
                                                    <%=fiscalYearOptions%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.PI_APP_REQUEST_ADD_OFFICEUNITID, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-6">
                                                <input type="hidden" name='officeUnitId'
                                                       id='office_units_id_input' value="">
                                                <button type="button"
                                                        class="btn btn-secondary form-control shadow btn-border-radius"
                                                        disabled id="office_units_id_text"></button>
                                            </div>
                                            <div class="col-2 text-right">
                                                <button type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius"
                                                        id="office_units_id_modal_button"
                                                        onclick="officeModalButtonClicked();">
                                                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "আইটেম বাছাই করুন", "SELECT ITEM")%>
                                            </label>
                                            <div class="col-8">
                                                <button type="button"
                                                        class="btn btn-primary form-control"
                                                        id="item_select_button"
                                                        onclick="itemSelectClicked();">
                                                    <%=UtilCharacter.getDataByLanguage(Language, "অনুসন্ধান", "Search")%>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=UtilCharacter.getDataByLanguage(Language, "আইটেম তালিকা", "Item List")%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মালামালের বিবরন", "Item Description")%>
                                    </th>
                                    <th rowspan="1"
                                        colspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "পরিমাণ", "Quantity")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "একক দর (টাকায়)", "Unit Price (BDT)")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মোট দর (টাকায়)", "Total Price (BDT)")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_PI_ANNUAL_DEMAND_CHILD_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "সংখ্যা", "Number")%>
                                    </th>
                                    <th style="text-align: center;"><%=UtilCharacter.getDataByLanguage(Language, "একক", "Unit")%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="item-row-body">
                                <%
                                    if (actionName.equals("ajax_edit")) {
                                        int index = -1;
                                        for (PiAnnualDemandChildDTO piAnnualDemandChildDTO : pi_annual_demandDTO.piAnnualDemandChildDTOList) {
                                            index++;
                                            System.out.println("index index = " + index);
                                %>

                                <tr id="PiAnnualDemandChild_<%=index + 1%>">
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name='piAnnualDemandChild.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=piAnnualDemandChildDTO.iD%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               name='piAnnualDemandChild.piUnitId'
                                               id='piUnitId_hidden_<%=childTableStartingID%>'
                                               value='<%=piAnnualDemandChildDTO.piUnitId%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               name='piAnnualDemandChild.piItemGroupId'
                                               id='piItemGroupId_hidden_<%=childTableStartingID%>'
                                               value='<%=piAnnualDemandChildDTO.itemGroupId%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               name='piAnnualDemandChild.piItemTypeId'
                                               id='piItemTypeId_hidden_<%=childTableStartingID%>'
                                               value='<%=piAnnualDemandChildDTO.itemTypeId%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               name='piAnnualDemandChild.piItemId'
                                               id='piItemId_hidden_<%=childTableStartingID%>'
                                               value='<%=piAnnualDemandChildDTO.itemId%>' tag='pb_html'/>
                                    </td>
                                    <td>
                                        <input type="number" class="form-control"
                                               name="piAnnualDemandChild.serialNumber"
                                               id="serialNumber_<%=childTableStartingID%>"
                                               type="number"
                                               min="0" max="99999999"
                                               value='<%=piAnnualDemandChildDTO.serialNumber%>' tag='pb_html'>
                                    </td>
                                    <td>
                                        <textarea class='form-control' rows="3"
                                                  name='piAnnualDemandChild.itemDescription'
                                                  id='itemDescription_text_<%=childTableStartingID%>' readonly
                                                  tag='pb_html'><%=piAnnualDemandChildDTO.description%></textarea>
                                    </td>
                                    <td>
                                        <input class='form-control' name='piAnnualDemandChild.itemQuantity'
                                               id='itemQuantity_<%=childTableStartingID%>'
                                               value='<%=piAnnualDemandChildDTO.itemQuantity%>'
                                               type="number"
                                               min="0" max="99999999"
                                               oninput="this.value = Math.round(this.value * 100) / 100;calculateTotalPrice(this)"
                                               tag='pb_html'/>
                                    </td>
                                    <td>
                                        <%
                                            Pi_unitDTO piUnitDTO = Pi_unitRepository.getInstance().getPi_unitDTOByiD(piAnnualDemandChildDTO.piUnitId);
                                            String piUnitText = UtilCharacter.getDataByLanguage(Language, piUnitDTO.nameBn, piUnitDTO.nameEn);
                                        %>
                                        <input type='text' class='form-control' name='piAnnualDemandChild.piUnitIdText'
                                               id='piUnitId_text_<%=childTableStartingID%>'
                                               value='<%=piUnitText%>'
                                               tag='pb_html' readonly/>
                                    </td>
                                    <td>
                                        <input class='form-control' name='piAnnualDemandChild.itemUnitPrice'
                                               id='itemUnitPrice_<%=childTableStartingID%>'
                                               value='<%=piAnnualDemandChildDTO.itemUnitPrice%>'
                                               type="number"
                                               min="0" max="99999999"
                                               oninput="this.value = Math.round(this.value * 100) / 100;calculateTotalPrice(this)"
                                               tag='pb_html'/>
                                    </td>
                                    <td>
                                        <input class='form-control' name='piAnnualDemandChild.itemTotalPrice'
                                               id='itemTotalPrice_<%=childTableStartingID%>'
                                               value='<%=piAnnualDemandChildDTO.itemTotalPrice%>'
                                               type="number"
                                               min="0" max="99999999" readonly
                                               tag='pb_html'/>
                                    </td>
                                    <td>
                                        <button
                                                id="remove-item"
                                                name="removeItem"
                                                type="button"
                                                class="btn btn-sm remove-btn shadow ml-2 pl-4"
                                                onclick="removeItemRow(this)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>

                        <%PiAnnualDemandChildDTO piAnnualDemandChildDTO = new PiAnnualDemandChildDTO();%>

                        <template id="template-item-row">
                            <tr>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control' name='piAnnualDemandChild.iD'
                                           id='iD_hidden_' value='<%=piAnnualDemandChildDTO.iD%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control'
                                           name='piAnnualDemandChild.piUnitId' id='piUnitId_hidden_'
                                           value='<%=piAnnualDemandChildDTO.piUnitId%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control'
                                           name='piAnnualDemandChild.piItemGroupId' id='piItemGroupId_hidden_'
                                           value='<%=piAnnualDemandChildDTO.itemGroupId%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control'
                                           name='piAnnualDemandChild.piItemTypeId' id='piItemTypeId_hidden_'
                                           value='<%=piAnnualDemandChildDTO.itemTypeId%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control'
                                           name='piAnnualDemandChild.piItemId' id='piItemId_hidden_'
                                           value='<%=piAnnualDemandChildDTO.itemId%>' tag='pb_html'/>
                                </td>
                                <td>
                                    <input type="number" class="form-control" name="piAnnualDemandChild.serialNumber"
                                           id="serialNumber_"
                                           type="number"
                                           min="0" max="99999999"
                                           value='<%=piAnnualDemandChildDTO.serialNumber%>' tag='pb_html'>
                                </td>
                                <td>
                                    <textarea class='form-control' name='piAnnualDemandChild.itemDescription'
                                              id='itemDescription_text_' readonly
                                              tag='pb_html'><%=piAnnualDemandChildDTO.description%></textarea>
                                </td>
                                <td>
                                    <input class='form-control' name='piAnnualDemandChild.itemQuantity'
                                           id='itemQuantity_' value='<%=piAnnualDemandChildDTO.itemQuantity%>'
                                           type="number"
                                           min="0" max="99999999"
                                           oninput="this.value = Math.round(this.value * 100) / 100;calculateTotalPrice(this)"
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <input type='text' class='form-control' name='piAnnualDemandChild.piUnitIdText'
                                           id='piUnitId_text_' value='<%=piAnnualDemandChildDTO.piUnitId%>'
                                           tag='pb_html' readonly/>
                                </td>
                                <td>
                                    <input class='form-control' name='piAnnualDemandChild.itemUnitPrice'
                                           id='itemUnitPrice_' value='<%=piAnnualDemandChildDTO.itemUnitPrice%>'
                                           type="number"
                                           min="0" max="99999999"
                                           oninput="this.value = Math.round(this.value * 100) / 100;calculateTotalPrice(this)"
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <input class='form-control' name='piAnnualDemandChild.itemTotalPrice'
                                           id='itemTotalPrice_' value='<%=piAnnualDemandChildDTO.itemTotalPrice%>'
                                           type="number"
                                           min="0" max="99999999" readonly
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <button
                                            id="remove-item"
                                            name="removeItem"
                                            type="button"
                                            class="btn btn-sm remove-btn shadow ml-2 pl-4"
                                            onclick="removeItemRow(this)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="row mt-5">
                    <label class="col-4 col-form-label text-right">
                        <%=UtilCharacter.getDataByLanguage(Language, "আপনি কি প্যাকেজ/লট সংযুক্ত করতে চান?", "Do you want to add package/lot?")%>
                    </label>
                    <div class="col-8">
                        <input type='checkbox' class='form-control-sm' name='hasPackageLot'
                               id='hasPackageLot_checkbox_<%=i%>'
                               onclick="toggleLot()" value="<%=pi_annual_demandDTO.hasPackageLot%>">
                    </div>
                </div>
                <div class="form-group row hiddenElement" id="packageListDiv">
                    <label class="col-2 col-form-label text-right">
                        <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নির্বাচন করুন", "Package Selection")%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-4">
                        <select class='form-control' name='packageList' id='packageList'
                                tag='pb_html' onchange="filterLotOptionsByPackage(this)">
                            <%
                                List<Pi_package_newDTO> package_newDTOS = Pi_package_newRepository.getInstance().getPi_package_newList()
                                        .stream().filter(dto -> dto.officeUnitId == userDTO.unitID).collect(Collectors.toList());

                                String packageOptions = Utils.buildSelectOption(isLanguageEnglish);
                                option = new StringBuilder();
                                for (Pi_package_newDTO dto : package_newDTOS) {

                                    if (actionName.equals("ajax_edit")) {
                                        if (pi_annual_demandDTO.piPackageNewId == dto.iD) {
                                            option.append("<option value = '").append(dto.iD).append("' selected>");
                                        } else {
                                            option.append("<option value = '").append(dto.iD).append("'>");
                                        }
                                    } else {
                                        option.append("<option value = '").append(dto.iD).append("'>");
                                    }

                                    option.append(isLanguageEnglish ? dto.packageNumberEn : dto.packageNumberBn).append("</option>");
                                }
                                packageOptions += option.toString();
                            %>
                            <%=packageOptions%>
                        </select>
                    </div>
                </div>
                <div class="form-group row hiddenElement" id="lotListDiv">
                    <label class="col-2 col-form-label text-right">
                        <%=UtilCharacter.getDataByLanguage(Language, "লট নির্বাচন করুন", "Lot Selection")%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-4">
                        <select class='form-control' name='lotList' id='lotList'
                                tag='pb_html'>

                        </select>
                        <input type="hidden" id="piLotId" value="<%=pi_annual_demandDTO.piLotId%>">
                    </div>
                </div>
                <div class="form-group row hiddenDropzone fileDropzoneCls mt-5">
                    <div class="col-md-12">
                        <%
                            fileColumnName = "filesDropzone";
                            if (actionName.equals("ajax_edit")) {
                                List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(pi_annual_demandDTO.filesDropZone);
                        %>
                        <%@include file="../pb/dropzoneEditor.jsp" %>
                        <%
                            } else {
                                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                pi_annual_demandDTO.filesDropZone = ColumnID;
                            }
                        %>

                        <div class="dropzone"
                             action="Pi_annual_demandServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=pi_annual_demandDTO.filesDropZone%>">
                            <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                   id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                        </div>
                        <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                               id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                        <input type='hidden' name='<%=fileColumnName%>'
                               id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                               value='<%=pi_annual_demandDTO.filesDropZone%>'/>


                    </div>
                </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_PI_ANNUAL_DEMAND_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="submitForm()">
                        <%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_PI_ANNUAL_DEMAND_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<jsp:include page="./pi_annual_demand_ItemSelectionModal.jsp">
    <jsp:param name="index" value="1"/>
</jsp:include>
<%@ include file="../recruitment_seat_plan/manualLoader.jsp" %>

<script type="text/javascript">
    const fullPageLoader = $('#full-page-loader');
    let annualDemandForm = $("#bigForm");
    let actionName = '<%=actionName%>';
    let child_table_extra_id = <%=childTableStartingID%>;
    let lang = '<%=Language%>';
    let itemIds = [];
    let duplicateItemsAdded = false;
    let tableBody = $("#item-row-body");
    let hasPackageLot = <%=pi_annual_demandDTO.hasPackageLot%>;

    // Related to office selector modal start
    let officeSelectModalUsage = 'none';
    let officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    // Related to office selector modal end

    // related to item modal start
    function itemSelectClicked() {
        resetModal();
        $('#search_proc_modal').modal();
    }

    necessaryCollectionForProcurementModal = {
        callBackFunction: function (items) {
            duplicateItemsAdded = false;
            for (let item of items) {
                getItem(item[1]);
            }
            if (duplicateItemsAdded) {
                swal.fire(getValueByLanguage(lang, "ইতিমধ্যে যোগ করা আইটেম লিস্ট এ অন্তর্ভুক্ত করা হয়নি!", "Already added items  is not added to list!"));
            }
        }
    };


    function exists(arr, target) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === target) {
                return true;
            }
        }
        return false;
    }

    function isItemValid(item) {
        return item && item.iD !== '';
    }

    <%--function getItem(item) {--%>
    <%--    if (!isItemValid(item)) {--%>
    <%--        return;--%>
    <%--    }--%>

    <%--    if (exists(itemIds, item.iD)) {--%>
    <%--        swal.fire(getValueByLanguage(lang, "এই আইটেম টি ইতিমধ্যে যোগ করেছেন!", "This item is already added!"));--%>
    <%--        return;--%>
    <%--    } else {--%>
    <%--        itemIds.push(item.iD);--%>
    <%--    }--%>

    <%--    let t = $("#template-item-row");--%>
    <%--    tableBody.append(t.html());--%>
    <%--    let tr = tableBody.find("tr:last-child");--%>

    <%--    tr.attr("id", "item_" + child_table_extra_id);--%>
    <%--    tr.attr("id", "itemId_text_" + child_table_extra_id);--%>

    <%--    tr.find("[tag='pb_html']").each(function (index) {--%>
    <%--        let prev_id = $(this).attr('id');--%>
    <%--        $(this).attr('id', prev_id + child_table_extra_id);--%>
    <%--    });--%>

    <%--    tr.find("#itemDescription_text_" + child_table_extra_id)--%>
    <%--        .val(getValueByLanguage(lang, item.nameBn, item.nameEn));--%>
    <%--    tr.find("#serialNumber_" + child_table_extra_id).val(child_table_extra_id);--%>
    <%--    tr.find("#itemQuantity_" + child_table_extra_id).val('');--%>
    <%--    tr.find("#piUnitId_text_" + child_table_extra_id)--%>
    <%--        .val(getValueByLanguage(lang, item.piUnitNameBn, item.piUnitNameEn));--%>
    <%--    tr.find("#itemUnitPrice_" + child_table_extra_id).val('');--%>
    <%--    tr.find("#itemTotalPrice_" + child_table_extra_id).val('');--%>
    <%--    tr.find("#piUnitId_hidden_" + child_table_extra_id).val(item.piUnitId);--%>
    <%--    tr.find("#piItemGroupId_hidden_" + child_table_extra_id).val(item.procurementPackageId);--%>
    <%--    tr.find("#piItemTypeId_hidden_" + child_table_extra_id).val(item.procurementGoodsTypeId);--%>
    <%--    tr.find("#piItemId_hidden_" + child_table_extra_id).val(item.iD);--%>

    <%--    select2MultiSelector("#procurementMethod_" + child_table_extra_id, '<%=Language%>');--%>
    <%--    child_table_extra_id++;--%>
    <%--}--%>

    function getItem(item) {
        if (!isItemValid(item)) {
            return;
        }

        if (exists(itemIds, item.itemId)) {
            duplicateItemsAdded = true;
            return;
        } else {
            itemIds.push(item.itemId);
        }

        let t = $("#template-item-row");
        tableBody.append(t.html());
        let tr = tableBody.find("tr:last-child");

        tr.attr("id", "item_" + child_table_extra_id);
        tr.attr("id", "itemId_text_" + child_table_extra_id);

        tr.find("[tag='pb_html']").each(function (index) {
            let prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_extra_id);
        });

        tr.find("#itemDescription_text_" + child_table_extra_id)
            .val(item.itemName);
        tr.find("#serialNumber_" + child_table_extra_id).val(child_table_extra_id);
        tr.find("#itemQuantity_" + child_table_extra_id).val('');
        tr.find("#piUnitId_text_" + child_table_extra_id).val(item.unit);
        tr.find("#itemUnitPrice_" + child_table_extra_id).val('');
        tr.find("#itemTotalPrice_" + child_table_extra_id).val('');
        tr.find("#piUnitId_hidden_" + child_table_extra_id).val(item.unitId);
        tr.find("#piItemGroupId_hidden_" + child_table_extra_id).val(item.procurementPackageId);
        tr.find("#piItemTypeId_hidden_" + child_table_extra_id).val(item.procurementGoodsTypeId);
        tr.find("#piItemId_hidden_" + child_table_extra_id).val(item.itemId);

        child_table_extra_id++;
    }

    // related to item modal end

    function toggleLot() {
        hasPackageLot = !hasPackageLot;
        let hasPackageLotCheckBox = document.getElementById('hasPackageLot_checkbox_<%=i%>');
        if (hasPackageLot) {
            // open div for lot entry
            hasPackageLotCheckBox.value = true;
            hidePackageSelector(false);
        } else {
            // close div for lot entry
            hasPackageLotCheckBox.value = false;
            hidePackageSelector(true);
        }
    }

    function clearPackageSelection() {
        let packageList = document.getElementById('packageList');
        packageList.value = '';
    }

    function clearLotSelection() {
        let lotList = document.getElementById('lotList');
        lotList.length = 0;
    }

    function hidePackageSelector(status) {
        let packageList = document.getElementById('packageListDiv');
        if (status) {
            packageList.classList.add('hiddenElement');
            clearPackageSelection();
        } else {
            packageList.classList.remove('hiddenElement');
        }
    }

    function hideLotSelector(status) {
        let lotList = document.getElementById('lotListDiv');
        if (status) {
            lotList.classList.add('hiddenElement');
            clearLotSelection();
        } else {
            lotList.classList.remove('hiddenElement');
        }
    }

    function calculateTotalPrice(element) {
        let el = document.getElementById(element.id);
        let selectedIdIndex = element.id.split('_')[1];
        let rowEl = el.parentElement.parentElement;
        let quantity = rowEl.querySelector('#itemQuantity_' + selectedIdIndex).value;
        let unitPrice = rowEl.querySelector('#itemUnitPrice_' + selectedIdIndex).value;
        if (quantity && unitPrice && quantity.trim().length > 0 && unitPrice.trim().length > 0) {
            let val = (quantity / 1) * (unitPrice / 1);
            rowEl.querySelector('#itemTotalPrice_' + selectedIdIndex).value = val.toFixed(0);
        } else {
            rowEl.querySelector('#itemTotalPrice_' + selectedIdIndex).value = ""
        }
    }

    function filterLotOptionsByPackage(element) {
        let packageId = document.getElementById(element.id).value;
        let lotId = document.getElementById('piLotId').value;
        let lotOptions = document.getElementById('lotList');
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let res = JSON.parse(this.response)
                lotOptions.innerHTML = res.msg;
                let optionCount = res.msg.split("option").length - 1;
                if (optionCount > 2) {
                    hideLotSelector(false);
                } else {
                    hideLotSelector(true);
                }
                console.log(this.responseText)
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Pi_package_lotServlet?actionType=getLotOptionsByPackage&packageId=" + packageId + "&piLotId=" + lotId, true);
        xhttp.send();
    }

    function submitForm() {
        // buttonStateChange(true);
        if (true) {
            let url = "<%=servletName%>?actionType=<%=actionName%>";
            $.ajax({
                type: "POST",
                url: url,
                data: annualDemandForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        showToastSticky("সাবমিট সফল হয়েছে", "Submit Successful");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 3000);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_annual_demandServlet");
    }

    function init(row) {
        if (actionName === 'ajax_edit') {
            viewOfficeIdInInput({
                name: '<%=nameOfOffice%>',
                id: <%=pi_annual_demandDTO.officeUnitId%>
            });
        } else {
            viewOfficeIdInInput({
                name: '<%=nameOfOffice%>',
                id: <%=currentOfficeUnitId%>
            });
        }
        setDateByStringAndId('creationDate_js_' + row, $('#creationDate_date_' + row).val());

        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    function convertToSelect2() {
        select2SingleSelector('#fiscalYear', '<%=Language%>');
    }

    function makeLotFormVisibleIfRequired() {
        const hasLotCheckBox = document.getElementById('hasPackageLot_checkbox_0');
        console.log("checked: ", hasLotCheckBox.checked)
        if (hasLotCheckBox.value === "true") {
            hasLotCheckBox.checked = true;
            hidePackageSelector(false);
        } else {
            hidePackageSelector(true);
        }
    }

    function loadLotIfRequired() {
        let packageList = document.getElementById('packageList');
        if (packageList.value) {
            filterLotOptionsByPackage(packageList);
        }
    }

    function getDigitsByLanguage(element, language) {
        if (language === 'English') {
            return;
        }
        let numberText = element.value;
        let numberBanglaText = "";
        for (let i of numberText) {
            if (i === '0') numberBanglaText += '০';
            else if (i === '1') numberBanglaText += '১';
            else if (i === '2') numberBanglaText += '২';
            else if (i === '3') numberBanglaText += '৩';
            else if (i === '4') numberBanglaText += '৪';
            else if (i === '5') numberBanglaText += '৫';
            else if (i === '6') numberBanglaText += '৬';
            else if (i === '7') numberBanglaText += '৭';
            else if (i === '8') numberBanglaText += '৮';
            else if (i === '9') numberBanglaText += '৯';
            else numberBanglaText += i;
        }
    }

    function loadItemIds() {
        let elements = document.getElementsByName('piAnnualDemandChild.piItemId');

        elements.forEach(item => {
            if (item.value !== "" && item.value !== "-1")
                itemIds.push(parseInt(item.value));
        })
    }

    let row = 0;
    $(document).ready(function () {
        init(row);
        fullPageLoader.hide();
        convertToSelect2();
        makeLotFormVisibleIfRequired();
        loadLotIfRequired();
        loadItemIds();
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
        if ('<%=actionName%>' === 'ajax_edit') {
            loadAllItems(-1, -1, '<%=actionName%>');
        }
    });

    function removeItemRow(elem) {
        let tr = elem.parentNode.parentNode;
        let annualDemandChildId = tr.querySelector("td>input[name='piAnnualDemandChild.iD']").value;
        if (!annualDemandChildId || annualDemandChildId === "-1" || annualDemandChildId === "") {
            tr.remove();
            showToastSticky("ডিলিট হয়েছে", "Delete Successful");
        } else {
            let url = "Pi_annual_demandServlet?actionType=deleteAnnualDemandChildWithValidation&annualDemandChildId=" + annualDemandChildId;
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    response = JSON.parse(response);
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        tr.remove();
                        showToastSticky("ডিলিট হয়েছে", "Delete Successful");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }

    }
</script>