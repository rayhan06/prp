<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
%>

<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="row" style="margin: 0px !important;">
    <div class="col-md-12" style="padding: 5px !important;">
        <div id="ajax-content">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-check-square-o"></i><%=pageTitle%>
                    </div>
                </div>
                <label>অফিস কর্মকর্তাদের ইংরেজি পদবি সংশোধন</label>

                <div>
                    <table id="designation_table" style="width:100%">
                        <tr id="table_head">
                            <th>Name</th>
                            <th>Designation (Bangla)</th>
                            <th>Designation (English)</th>
                            <th>Activity</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="snackbar">Success</div>

<div id="edit_body" style="display: none">
    <input type="hidden" id="id">
    <label>Designation (English)</label>
    <br>
    <input id="designation_eng_edit" onchange="onEnglishEdit(this.value)"
           style="min-width: 190px">
    <br>
    <button class="btn btn-success" onclick="onEditDone(event)">
        Submit
    </button>
</div>


<script type="text/javascript">

    var edit_designation_name_en;

    function onEnglishEdit(valueEn) {
        document.getElementById('designation_eng_edit').setAttribute("value", valueEn);
    }

    function getDesignation() {

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                JSON.parse(this.responseText).forEach(function (val) {

                    var html = "<tr>\n" +
                        "<th>\n" +
                        "" + val.name_bng + "\n" +
                        "</th>\n" +
                        "<th>\n" +
                        "" + val.designation_bng + "\n" +
                        "</th>\n" +
                        "<th>\n" +
                        "" + val.designation_eng + "\n" +
                        "</th>\n" +
                        "<th>\n" +
                        "<button onclick=\"onEditEnglishDesignation(" + val.id + "," + "'" + val.designation_eng + "')\">Update</button>\n" +
                        "</th>\n" +
                        "</tr>";
                    document.getElementById('designation_table').innerHTML += html;
                });
            }
        };
        xhttp.open("Get", "EnglishDesignationChangeServlet?actionType=getDesignation", true);
        xhttp.send();
    }

    function onEditEnglishDesignation(id, english_name) {
        document.getElementById('id').value = id;
        document.getElementById('designation_eng_edit').setAttribute('value', english_name);
        edit_designation_name_en = document.getElementById('designation_eng_edit').value;

        bootbox.dialog({
            message: document.getElementById('edit_body').innerHTML
        });
    }

    function onEditDone(e) {
        var id = document.getElementById("id").value;
        var en = document.getElementById("designation_eng_edit").value;

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                var x = document.getElementById("snackbar");
                x.className = "show";
                setTimeout(function () {
                    x.className = x.className.replace("show", "");
                }, 3000);
                bootbox.hideAll();
            }
        };
        xhttp.open("Post", "EnglishDesignationChangeServlet?actionType=updateEngName&id=" + id + "&name_en=" + en, true);
        xhttp.send();
    }

    window.onload = function (ev) {
        getDesignation();
    };
</script>

<style>
    #snackbar {
        visibility: hidden; /* Hidden by default. Visible on click */
        min-width: 500px; /* Set a default minimum width */
        margin-left: -125px; /* Divide value of min-width by 2 */
        background-color: #10ca08; /* Black background color */
        color: #fff; /* White text color */
        text-align: center; /* Centered text */
        border-radius: 2px; /* Rounded borders */
        padding: 16px; /* Padding */
        position: fixed; /* Sit on top of the screen */
        z-index: 1; /* Add a z-index if needed */
        left: 50%; /* Center the snackbar */
        bottom: 30px; /* 30px from the bottom */
    }

    /* Show the snackbar when clicking on a button (class added with JavaScript) */
    #snackbar.show {
        visibility: visible; /* Show the snackbar */
        /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
        However, delay the fade out process for 2.5 seconds */
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    /* Animations to fade the snackbar in and out */
    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }
        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }
        to {
            bottom: 0;
            opacity: 0;
        }
    }
</style>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

