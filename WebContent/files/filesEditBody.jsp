
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="files.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>


<%
FilesDTO filesDTO;
filesDTO = (FilesDTO)request.getAttribute("filesDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(filesDTO == null)
{
	filesDTO = new FilesDTO();
	
}
System.out.println("filesDTO = " + filesDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.FILES_EDIT_FILES_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.FILES_ADD_FILES_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
FilesDTO row = filesDTO;
int childTableStartingID = 1;
long ColumnID;
FilesDAO filesDAO = new FilesDAO();
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="FilesServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.FILES_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=filesDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='fileId' id = 'fileId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.fileID + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.FILES_EDIT_FILETYPES, loginDTO)):(LM.getText(LC.FILES_ADD_FILETYPES, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'fileTypes_div_<%=i%>'>	
		<input type='text' class='form-control'  name='fileTypes' id = 'fileTypes_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.fileTypes + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.FILES_EDIT_FILEBLOB, loginDTO)):(LM.getText(LC.FILES_ADD_FILEBLOB, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'fileBlob_div_<%=i%>'>	
		<%
		if(filesDTO.fileBlob != null)
		{
			byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.fileBlob);
			value = filesDTO.fileBlob + "";
			
			out.println("<img src='" + "data:image/jpg;base64," +  new String(encodeBase64) + "' style='width:100px' >");
			out.println("<a href = 'FilesServlet?actionType=downloadBlob&name=fileBlob&id=" + filesDTO.iD + "' download='" + value + "'>Download</a>");
		}
		%>	
		<input type='file' class='form-control'  name='fileBlob' id = 'fileBlob_blob_jpg_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.fileBlob + "'"):("'" + "null" + "'")%>   tag='pb_html'/>
			
						
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.FILES_EDIT_THUMBNAILBLOB, loginDTO)):(LM.getText(LC.FILES_ADD_THUMBNAILBLOB, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'thumbnailBlob_div_<%=i%>'>	
		<%
		if(filesDTO.thumbnailBlob != null)
		{
			byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
			value = filesDTO.thumbnailBlob + "";
			
			out.println("<img src='" + "data:image/jpg;base64," +  new String(encodeBase64) + "' style='width:100px' >");
			out.println("<a href = 'FilesServlet?actionType=downloadBlob&name=thumbnailBlob&id=" + filesDTO.iD + "' download='" + value + "'>Download</a>");
		}
		%>	
		<input type='file' class='form-control'  name='thumbnailBlob' id = 'thumbnailBlob_blob_jpg_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.thumbnailBlob + "'"):("'" + "null" + "'")%>   tag='pb_html'/>
			
						
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.FILES_EDIT_FILETAG, loginDTO)):(LM.getText(LC.FILES_ADD_FILETAG, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'fileTag_div_<%=i%>'>	
		<input type='text' class='form-control'  name='fileTag' id = 'fileTag_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.fileTag + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.FILES_EDIT_FILETITLE, loginDTO)):(LM.getText(LC.FILES_ADD_FILETITLE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'fileTitle_div_<%=i%>'>	
		<input type='text' class='form-control'  name='fileTitle' id = 'fileTitle_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.fileTitle + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='userId' id = 'userId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.userId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.FILES_EDIT_SOURCE, loginDTO)):(LM.getText(LC.FILES_ADD_SOURCE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'source_div_<%=i%>'>	
		<input type='text' class='form-control'  name='source' id = 'source_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.source + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.FILES_EDIT_CREATEDAT, loginDTO)):(LM.getText(LC.FILES_ADD_CREATEDAT, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'createdAt_div_<%=i%>'>	
		<input type='text' class='form-control'  name='createdAt' id = 'createdAt_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.createdAt + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + filesDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + filesDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.FILES_EDIT_FILES_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.FILES_ADD_FILES_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.FILES_EDIT_FILES_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.FILES_ADD_FILES_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "FilesServlet");	
}

function init(row)
{


	
}

var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






