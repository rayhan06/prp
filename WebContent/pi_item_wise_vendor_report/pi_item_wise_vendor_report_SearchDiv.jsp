<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%@ page import="pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		<div  class="search-criteria-div" >
			<div class="form-group row">
				<label class="col-3 control-label text-right">
					<%=LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_WHERE_ACTUALVENDORID, loginDTO)%>
				</label>
				<div class="col-7 offset-1" >

					<select class='form-control' name='actualVendorId'
							id='actualVendorId'
							 tag='pb_html'>
						<%= Pi_vendor_auctioneer_detailsRepository.getInstance().buildOptions(Language, -1L) %>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" >
			<div class="form-group row">
				<label class="col-3 control-label text-right">
					<%=LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_WHERE_FISCALYEARID, loginDTO)%>
				</label>
				<div class="col-7 offset-1">
					<select class='form-control' name='fiscalYearId'
							id='fiscalYearId'
							tag='pb_html'>
						<%= Fiscal_yearRepository.getInstance().buildOptions(Language, -1L) %>
					</select>
				</div>


			</div>
		</div>
		<div  class="search-criteria-div" style="display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_WHERE_ISWINNER, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='isWinner' id = 'isWinner' value="1"/>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.PI_ITEM_WISE_VENDOR_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}

$(document).ready(function () {
	select2SingleSelector('#actualVendorId', '<%=Language%>');
	select2SingleSelector('#fiscalYearId', '<%=Language%>');
});

</script>