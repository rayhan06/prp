<%@page pageEncoding="UTF-8" %>

<%@page import="vm_requisition.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_REQUISITION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_requisitionDTO vm_requisitionDTO = (Vm_requisitionDTO) request.getAttribute("vm_requisitionDTO");
    CommonDTO commonDTO = vm_requisitionDTO;
    String servletName = "Vm_requisition_pendingServlet";


    System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_requisitionDAO vm_requisitionDAO = (Vm_requisitionDAO) request.getAttribute("vm_requisitionDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_requesterOrgId'>
    <%
        value = Language.equals("English") ?
                vm_requisitionDTO.requesterNameEn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameEn
                : vm_requisitionDTO.requesterNameBn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameBn
        ;
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_startDate'>
    <%
        value = vm_requisitionDTO.startDate + "";
    %>
    <%
        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate, Language)%>


</td>

<td id='<%=i%>_endDate'>
    <%
        value = vm_requisitionDTO.endDate + "";
    %>
    <%
        String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_endDate, Language)%>


</td>

<td id='<%=i%>_startAddress'>
    <%
        value = vm_requisitionDTO.startAddress + "";
        value = value.replaceAll("\\$", " -> ");
    %>
    <%=value%>


</td>

<td id='<%=i%>_endAddress'>
    <%
        value = vm_requisitionDTO.endAddress + "";
        value = value.replaceAll("\\$", " -> ");
    %>
    <%=value%>


</td>

<td id='<%=i%>_vehicleTypeCat'>
    <%
        value = vm_requisitionDTO.vehicleTypeCat + "";
    %>
    <%
        value = CatRepository.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_status'>

    <%
         int curStatus = -1;
         if(vm_requisitionDTO.status == CommonApprovalStatus.PRE_PENDING.getValue()){
             curStatus = CommonApprovalStatus.PRE_PENDING.getValue();
         }
         else if(vm_requisitionDTO.status == CommonApprovalStatus.PENDING.getValue()){
             curStatus = CommonApprovalStatus.PENDING.getValue();
         }
         else if(vm_requisitionDTO.status == CommonApprovalStatus.SATISFIED.getValue()){
             curStatus = CommonApprovalStatus.SATISFIED.getValue();
         }
         else if(vm_requisitionDTO.status == CommonApprovalStatus.DISSATISFIED.getValue()){
             curStatus = CommonApprovalStatus.DISSATISFIED.getValue();
         }
    %>

    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=CommonApprovalStatus.getColor(vm_requisitionDTO.status)%>; color: white; border-radius: 8px;cursor: text">
            <%=CatRepository.getInstance().getText(
                    Language, "vm_requisition_status", vm_requisitionDTO.status
            )%>
    </span>

</td>

<td>

        <button
                type="button"
                class="btn-sm border-0 shadow bg-light btn-border-radius"
                style="color: #ff6b6b;"
                onclick="event.preventDefault();location.href='Vm_requisitionServlet?actionType=getPrePendingPage&ID=<%=vm_requisitionDTO.iD%>'">
            <i class="fa fa-eye"></i>
        </button>
</td>

																						
											

