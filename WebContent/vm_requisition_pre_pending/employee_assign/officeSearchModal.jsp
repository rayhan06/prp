<!-- Modal -->
<%--
  Author: Moaz Mahmud
  USAGE:
--%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String isHierarchyNeededStr = request.getParameter("isHierarchyNeeded");
    boolean isHierarchyNeeded = isHierarchyNeededStr != null && Boolean.parseBoolean(isHierarchyNeededStr);
%>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_office_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h3 class="caption">
                    <%=isLanguageEnglish? "Find Office" : "দপ্তর খুঁজুন"%>
                </h3>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body">
                <%@include file="officeSearchModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer">
                <button type="button" class="btn btn-success"
                        id="selectOffice_submit_button" onclick="useOfficeSubmitButton();">
                    Select Office
                </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    language = '<%=Language%>';

    function officeSearchInitLayer1(){
        // fetch options for new select
        let url = "Office_unitsServlet?actionType=<%=isHierarchyNeeded? "ajax_employee_office" : "ajax_office&parent_id=0"%>&language=" + language;
        // console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                document.getElementById('officeLayer_select_1_searchOffice').innerHTML = data;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    // modal on load event
    $('#search_office_modal').on('show.bs.modal', function () {
        $('#officeLayer_select_1_searchOffice').val('');
        // fetch layer 1 options
        officeSearchInitLayer1();
        officeSearchClearNextLayersFrom(1);
    });

    function officeSearchGetLayerId(officeLayerIdText) {
        // fromat: officeLayer_{select or div}_{layer id}_searchOffice
        return parseInt(officeLayerIdText.split('_')[2]);
    }

    function officeSearchClearNextLayersFrom(currentLayerId) {
        for (let selectElement of document.querySelectorAll('.office-layer_searchOffice')) {
            let layerId = officeSearchGetLayerId(selectElement.id);
            if (layerId > currentLayerId) {
                selectElement.remove();
            }
        }
    }

    newSelect = null;
    selectedOfficeId = null;
    newDiv = null;
    function officeSearchLayerChange(selectElement) {
        let layerId = officeSearchGetLayerId(selectElement.id);
        officeSearchClearNextLayersFrom(layerId);

        selectedOfficeId = selectElement.value;
        if (selectedOfficeId === '') {
            return;
        }

        // clone the first div
        newDiv = document.getElementById('officeLayer_div_1_searchOffice').cloneNode(true);
        newDiv.id = 'officeLayer_div_' + (layerId + 1) + '_searchOffice';
        newSelect = newDiv.getElementsByTagName('select')[0];
        newSelect.id = 'officeLayer_select_' + (layerId + 1) + '_searchOffice';

        // fetch options for new select
        let url = "Office_unitsServlet?actionType=<%=isHierarchyNeeded? "ajax_employee_office" : "ajax_office"%>&parent_id="
            + selectedOfficeId + "&language=" + language;
        // console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                // console.log(data);
                if (newSelect !== null && data !== '') {
                    newSelect.innerHTML = data;
                    document.getElementById('officeLayer_dropdown_div_searchOffice').appendChild(newDiv);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function useOfficeSubmitButton(){
        let lastSelectedOffice = {
            id: '',
            name: ''
        }
        for (let selectElement of document.querySelectorAll('.office-layer_searchOffice')) {
            let selectElemSelected = $('#' + selectElement.id + ' option:selected');
            let value = selectElemSelected.val();
            if(value !== ''){
                lastSelectedOffice.id = value;
                lastSelectedOffice.name = selectElemSelected.text();
            }
        }
        $('#search_office_modal').modal('hide');

        if(officeSelectModalUsage !== 'none'){
            let options = officeSelectModalOptionsMap.get(officeSelectModalUsage);
            if(options.officeSelectedCallback){
                options.officeSelectedCallback(lastSelectedOffice);
            }
        }
    }
</script>

<%-- Author: Moaz Mahmud --%>