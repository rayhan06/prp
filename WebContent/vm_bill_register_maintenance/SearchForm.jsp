<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_job_description.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="vm_maintenance.BillRegisterItemDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    List<BillRegisterItemDTO> billRegisterItemDTOS =
            (List<BillRegisterItemDTO>) request.getAttribute("billRegisterItemDTOS");
//	System.out.println(billRegisterItemDTOS);
    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);

    String value = "";


%>

<div class="table-responsive mt-5">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="thead-light">
        <tr>
            <th>
                <%=UtilCharacter.getDataByLanguage(Language, "নং ", "Serial")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "স্মারক নং ", "Sarok Number")%>
            </th>
            <th>
                <%=UtilCharacter.getDataByLanguage(Language, "বিল পরিশোধের তারিখ ", "Payment Date")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "গাড়ি নম্বর ", "Vehicle")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "বরাদ্দ ", "Allocated")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ব্যায় ", "Cost")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "মোট খরচ", "Total Cost")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "অবশিষ্ট", "Remaining")%>
            </th>

        </tr>
        </thead>
        <tbody>
        <%


            try {

                if (billRegisterItemDTOS != null) {
                    int size = billRegisterItemDTOS.size();
                    System.out.println("data not null and size = " + size + " billRegisterItemDTOS = " + billRegisterItemDTOS);
                    for (int i = 0; i < size; i++) {
                        BillRegisterItemDTO dto =
                                billRegisterItemDTOS.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <td>
                <%
                    value = (i + 1) + "";
                %>
                <%=Utils.getDigits(value, Language)%>

            </td>


            <td>
                <%=dto.sarokNumber%>
            </td>

            <td>
                <%=dto.paymentDate%>
            </td>

            <td>
                <%=dto.vehicleNumber%>
            </td>

            <td>
                <%=dto.allocated%>
            </td>

            <td>
                <%=dto.cost%>
            </td>

            <td>
                <%=dto.totalCost%>
            </td>

            <td>
                <%=dto.remaining%>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>


			