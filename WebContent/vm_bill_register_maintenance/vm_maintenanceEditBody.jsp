<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_maintenance.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDAO" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="language.LanguageDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }

    String servletName = "Vm_maintenanceServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_MAINTENANCE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String formTitle = UtilCharacter.getDataByLanguage(Language, "মেরামত ও সংরক্ষণ ", "Repair and Maintenance");
    String officeName = request.getParameter("office");
    String officeIds = "0";
    if (officeName != null) {
        if (officeName.equalsIgnoreCase("songshodSochibaloy")) {
            officeIds = "10";
        } else if (officeName.equalsIgnoreCase("speakerAndDeputySpeaker")) {
            officeIds = "1,2";
        } else if (officeName.equalsIgnoreCase("healthCenter")) {
            officeIds = "55";
        } else if (officeName.equalsIgnoreCase("upoNeta")) {
            officeIds = "3";
        } else if (officeName.equalsIgnoreCase("opposition")) {
            officeIds = "4,6";
        } else if (officeName.equalsIgnoreCase("chiefWhip")) {
            officeIds = "5,7";
        }
    }

    System.out.println(officeIds);
//    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
//            fiscal_yearDAO.getAllFiscal_year(true);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    // office=
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action=""
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-2">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_SELECT_FISCAL_YEAR, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='fiscalYearId'
                                                    id='fiscalYearId_hidden_<%=i%>' tag='pb_html' required>
                                                <%
                                                    String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                    StringBuilder option = new StringBuilder();

                                                    for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {
                                                        //System.out.println("fiscal dto id: "+fiscal_yearDTO.id);
                                                        option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                        option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                    }
                                                    fiscalYearOptions += option.toString();
                                                %>
                                                <%=fiscalYearOptions%>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <button onclick="loadData()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="submit">
                            <%=UtilCharacter.getDataByLanguage(Language, "লোড", "Load")%>
                        </button>
                    </div>
                </div>
                <div id="data-div" class="  mb-4" style="display: none">
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    let row = 0;
    let language = '<%=Language%>';

    function validation() {

        let fiscalYearIds = $("#fiscalYearId_hidden_0").val();
        if (!fiscalYearIds || fiscalYearIds.length == 0) {
            toastr.error("Please select fiscal year");
            return false;
        }
        // console.log(fiscalYearIds);
        // console.log(fiscalYearIds.length);
        return true;
    }

    function loadData() {


        event.preventDefault();
        $("#data-div").hide();
        if (validation()) {
            // console.log("Load Data")
            <%--let vehicleId = $("#givenVehicleId_select_0").val();--%>
            let fiscalYearIds = $("#fiscalYearId_hidden_0").val();
            let officeIds = '<%=officeIds%>';
            let url = "Vm_maintenanceServlet?actionType=getBillRegisterData&officeIds=" + officeIds + "&language="
                + '<%=Language%>' + "&fiscalYearIds=" + fiscalYearIds;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    // console.log(fetchedData);
                    $("#data-div").html(fetchedData);
                    $("#data-div").show();


                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

    }

    $(document).ready(function () {

    });


</script>






