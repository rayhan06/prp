

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="department_requisition_lot.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@page import="drug_information.*" %>




<%
String servletName = "Department_requisition_lotServlet";
Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Department_requisition_lotDTO department_requisition_lotDTO = Department_requisition_lotDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = department_requisition_lotDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_LOT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_LOT_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_MEDICALDEPTCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "medical_dept", department_requisition_lotDTO.medicalDeptCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=department_requisition_lotDTO.description%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_ISAPPROVED, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getApprovalStatus(department_requisition_lotDTO.isApproved, Language)%>
                                    </div>
                                </div>
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>	
            
            <form class="form-horizontal"
              action="Department_requisition_lotServlet?actionType=approve"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">		

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_DRUGINFORMATIONTYPE, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_STOCK_STATUS, loginDTO)%></th>
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_QUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_REMARKS, loginDTO)%></th>

							</tr>
							<%
                        	DepartmentRequisitionItemDAO departmentRequisitionItemDAO = DepartmentRequisitionItemDAO.getInstance();
                         	List<DepartmentRequisitionItemDTO> departmentRequisitionItemDTOs = (List<DepartmentRequisitionItemDTO>)departmentRequisitionItemDAO.getDTOsByParent("department_requisition_lot_id", department_requisition_lotDTO.iD);
                         	
                         	for(DepartmentRequisitionItemDTO departmentRequisitionItemDTO: departmentRequisitionItemDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											Drug_informationDTO drug_informationDTO = drug_informationDAO.getDrugInfoDetailsById(departmentRequisitionItemDTO.drugInformationType);
		                                
		                                    %>
		                                    <%=drug_informationDTO.drugText %>
										</td>
										<td>
											<%=drug_informationDTO.availableStock%>
										</td>
										
										<td>
											<input type='hidden' class='form-control'  name='departmentRequisitionItem.iD'  value='<%=departmentRequisitionItemDTO.iD%>' tag='pb_html'/>
	
											<input type='number' class='form-control'  name='departmentRequisitionItem.quantity' 
											max="<%=drug_informationDTO.currentStock%>" min="1"
											 value='<%=departmentRequisitionItemDTO.quantity%>'  tag='pb_html'>
										</td>
										<td>
											<%=departmentRequisitionItemDTO.remarks%>
										</td>
										
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
                        
						</table>
						<input type='hidden' class='form-control'  name='id' id = 'id' value='<%=department_requisition_lotDTO.iD%>' tag='pb_html'/>
						<input type = 'hidden' id = 'approvalAction' name = 'approvalAction' value = '1'>
						
						<table class="table table-bordered table-striped">
							<tr>
								<td><%=LM.getText(LC.HM_REMARKS, loginDTO)%></td>
								<td><textarea class='form-control'  name='approvalMessage' ></textarea></td>
							</tr>
						</table>
						
                    </div>                    
                </div>
                
                 <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="submit" onclick = "PreprocessBeforeSubmiting(1)">
                        <%=LM.getText(LC.HM_APPROVE, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit" onclick = "PreprocessBeforeSubmiting(2)">
                        <%=LM.getText(LC.HM_REJECT
                        		, loginDTO)%>
                    </button>
                </div>
              </form>
        </div>
    </div>
</div>

<script type="text/javascript">
function PreprocessBeforeSubmiting(approvalAction)
{
	console.log("approvalAction = " + approvalAction);
	$("#approvalAction").val(approvalAction);
	
	submitApprovalForm();

	return false;
}

function submitApprovalForm() {
    buttonStateChange(true);
    console.log("submitting");
    var form = $("#bigform");
    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: 'JSON',
        success: function (response) {
            if (response.responseCode === 0) {
                console.log("Failed");
                showToastSticky(response.msg, response.msg);
                buttonStateChange(false);
            } else if (response.responseCode === 200) {
                console.log("Successfully added");
                window.location.replace(getContextPath() + "Department_requisition_lotServlet?actionType=search");
            } else {
                console.log("Error: " + response.responseCode);
                buttonStateChange(false);
            }
        }
        ,
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                + ", Message: " + errorThrown);
            buttonStateChange(false);
        }
    });
}
</script>
