<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="department_requisition_lot.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@page import="drug_information.*" %>

<%
Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
Department_requisition_lotDTO department_requisition_lotDTO = new Department_requisition_lotDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	department_requisition_lotDTO = Department_requisition_lotDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = department_requisition_lotDTO;
String tableName = "department_requisition_lot";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_LOT_ADD_FORMNAME, loginDTO);
String servletName = "Department_requisition_lotServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Department_requisition_lotServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=department_requisition_lotDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='medicalDeptCat' id = 'medicalDeptCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getOptions( Language, "medical_dept", department_requisition_lotDTO.medicalDeptCat, CatRepository.SORT_BY_NAMEEN);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DESCRIPTION, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='description' id = 'description_text_<%=i%>' value='<%=department_requisition_lotDTO.description%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
																					
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_DRUGINFORMATIONTYPE, loginDTO)%></th>

										<th><%=LM.getText(LC.HM_STOCK_STATUS, loginDTO)%></th>
										<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_QUANTITY, loginDTO)%></th>
										<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_REMARKS, loginDTO)%></th>

										<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-DepartmentRequisitionItem">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(DepartmentRequisitionItemDTO departmentRequisitionItemDTO: department_requisition_lotDTO.departmentRequisitionItemDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "DepartmentRequisitionItem_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='departmentRequisitionItem.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=departmentRequisitionItemDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='departmentRequisitionItem.departmentRequisitionLotId' id = 'departmentRequisitionLotId_hidden_<%=childTableStartingID%>' value='<%=departmentRequisitionItemDTO.departmentRequisitionLotId%>' tag='pb_html'/>
									</td>
									<td>	
									
										<%
										Drug_informationDTO drug_informationDTO = drug_informationDAO.getDrugInfoDetailsById(departmentRequisitionItemDTO.drugInformationType);
	                                
	                                    %>
	                                    <%=drug_informationDTO.drugText %>
	                                    	
	                                       <input name='departmentRequisitionItem.drugInformationType'
	                                              id='drugInformationType_select_<%=childTableStartingID%>' type="hidden"
	                                              tag='pb_html'
	                                              value='<%=departmentRequisitionItemDTO.drugInformationType%>'></input>									

		
									</td>
									<td>
											<%=drug_informationDTO.availableStock%>
									</td>
							
									<td>										





																<%
																	value = "";
																	if(departmentRequisitionItemDTO.quantity != -1)
																	{
																	value = departmentRequisitionItemDTO.quantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='departmentRequisitionItem.quantity' id = 'quantity_number_<%=childTableStartingID%>' value='<%=value%>'  tag='pb_html'>		
									</td>
									<td>										





																<input type='text' class='form-control'  name='departmentRequisitionItem.remarks' id = 'remarks_text_<%=childTableStartingID%>' value='<%=departmentRequisitionItemDTO.remarks%>'   tag='pb_html'/>					
									</td>
									
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-DepartmentRequisitionItem"
											name="add-moreDepartmentRequisitionItem"
											type="button"
											onclick="childAdded(event, 'DepartmentRequisitionItem')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-DepartmentRequisitionItem"
											name="removeDepartmentRequisitionItem"
											type="button"
											onclick="childRemoved(event, 'DepartmentRequisitionItem')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%DepartmentRequisitionItemDTO departmentRequisitionItemDTO = new DepartmentRequisitionItemDTO();%>
					
							<template id="template-DepartmentRequisitionItem" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='departmentRequisitionItem.iD' id = 'iD_hidden_' value='<%=departmentRequisitionItemDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='departmentRequisitionItem.departmentRequisitionLotId' id = 'departmentRequisitionLotId_hidden_' value='<%=departmentRequisitionItemDTO.departmentRequisitionLotId%>' tag='pb_html'/>
									</td>
									<td>
									
											<div class="input-group">
													        <input
													          class="form-control py-2  border searchBox w-auto"
													          type="search"
													          placeholder="Search"
													          name='suggested_drug'
		                                                      id='suggested_drug_'
		                                                      tag='pb_html'
		                                                      onkeyup="getDrugs(this.id)"
		                                                      autocomplete="off"
													        />
													        
													      </div>
													      <div
													        id="searchSgtnSection_"
													        class="search-sgtn-section shadow-sm bg-white"
															tag='pb_html'
													      >
													        <div class="pt-3">
													          <ul class="px-0" style="list-style: none" id="drug_ul_" tag='pb_html'>
													          </ul>
													        </div>
													      </div>
													      
													      <input name='formStr'
		                                                   id='formStr_' type="hidden"
		                                                   tag='pb_html'
		                                                   value=''></input>
		                                                   
		                                    <input name='departmentRequisitionItem.drugInformationType'
		                                              id='drugInformationType_select_' type="hidden"
		                                              tag='pb_html'
		                                              value='<%=departmentRequisitionItemDTO.drugInformationType%>'></input>

		
									</td>
									
									<td id="stock_" tag='pb_html'>
									</td>
									
									<td>





																<%
																	value = "";
																	
																%>		
																<input type='number' class='form-control'  name='departmentRequisitionItem.quantity' id = 'quantity_number_' value='1' min='1'  tag='pb_html'>		
									</td>
									<td>





																<input type='text' class='form-control'  name='departmentRequisitionItem.remarks' id = 'remarks_text_' value='<%=departmentRequisitionItemDTO.remarks%>'   tag='pb_html'/>					
									</td>
									
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_LOT_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_LOT_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	
	if($("#medicalDeptCat_category_0").val() == '-1')
	{
		toastr.error("Department must be selected");
		return false;
	}
	else
	{
		var valid = true;
		var count = 0;
		var accuCheckFound = false;
		$("[name = 'departmentRequisitionItem.drugInformationType']").each(function() {
		  if($( this ).val() == "" || $( this ).val() == "-1")
		  {
		  	valid = false;
		  	return;
		  }
		  if($( this ).val() == "<%=Drug_informationDTO.ACCU_CHECK_ID%>")
		  {
			  accuCheckFound = true;
		  }
		  count ++;
		});
		if(accuCheckFound)
		{
			if(<%=userDTO.roleID != SessionConstants.NURSE_ROLE && userDTO.roleID != SessionConstants.ADMIN_ROLE%>)
			{
				toastr.error("You cannot request for accu-check");
				return false;
			}
			if($("#medicalDeptCat_category_0").val() != '<%=Department_requisition_lotDTO.EMERGENCY%>')
			{
				toastr.error("Select Emergency Department for Accu Check");
				return false;
			}
		}
		if(!valid)
		{
			toastr.error("Invalid medicine");
			return false;
		}
		if(count == 0)
		{
			toastr.error("No medicine selected");
			return false;
		}
	}
	
	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Department_requisition_lotServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function processRowsWhileAdding(childName)
{
	if(childName == "DepartmentRequisitionItem")
	{			
	}
}

function drugClicked(rowId, drugText, drugId, form, stock)
{
	console.log("drugClicked with " + rowId + ", drugId = " + drugId + ", form = " + form);
	$("#suggested_drug_" + rowId).val(drugText);
	$("#suggested_drug_" + rowId).attr("style", "width: " + ($("#suggested_drug_" + rowId).val().length + 1)*8 + "px !important");
	
	$("#drug_modal_textdiv_" + rowId).html(stock);
     $("#totalCurrentStock_text_" + rowId).val(stock);

	$("#drugInformationType_select_" + rowId).val(drugId);
	
	$("#drug_ul_" + rowId).html("");
	//$("#drug_ul_" + rowId).css("display", "none");
	$("#searchSgtnSection_"+ rowId).css("display", "none");
	console.log("stock = " + stock);
	$("#stock_"+ rowId).html(stock);
	$("#quantity_number_" + rowId).attr("max", stock);
	$("#quantity_number_" + rowId).attr("min", 1);
	
}

</script>






