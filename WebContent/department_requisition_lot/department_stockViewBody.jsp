

<%@page import="drug_information.Drug_informationDTO"%>
<%@page import="borrow_medicine.PersonalStockDTO"%>
<%@page import="borrow_medicine.PersonalStockDAO"%>
<%@page import="medical_inventory_out.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="department_requisition_lot.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>

<%@ page import="family.*"%>


<%
String servletName = "Department_requisition_lotServlet";
int medicalDeptCat = (int)request.getAttribute("medicalDeptCat");
boolean isDelivery = (boolean)request.getAttribute("isDelivery");
Medical_inventory_outDAO medical_inventory_outDAO = new Medical_inventory_outDAO();
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=CatDAO.getName(Language, "medical_dept", medicalDeptCat)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
        
        <%
        if(isDelivery)
        {
        %>
        <form class="form-horizontal"
              action="Department_requisition_lotServlet?actionType=deliver&medicalDeptCat=<%=medicalDeptCat%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0, 'deliver')">

                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=CatDAO.getName(Language, "medical_dept", medicalDeptCat)%>
                                            </h4>
                                        </div>


										<div class="form-group row" id="patientDiv"
											>
											<label class="col-md-4 col-form-label text-md-right">
												<%=LM.getText(LC.HM_DEPARTMENT, loginDTO)%>
											</label>
											<div class="col-md-8">
												<button type="button"
													class="btn text-white shadow form-control btn-border-radius"
													style="background-color: #4a87e2" onclick="addEmployee()"
													id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
												</button>
												<table class="table table-bordered table-striped">
													<tbody id="employeeToSet"></tbody>
												</table>
												<input type="hidden" name='patientId' id='patientId'
													value="" /> 
											    <input type="hidden" name='userName'
													id='userName' value="" />
											</div>
										</div>

										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_NAME, loginDTO)%></label>
											<div class="col-md-8">
												<input type='text' class='form-control' name='name'
													id='name' value='' tag='pb_html' />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.NURSE_ACTION_ADD_PHONE, loginDTO)%></label>
											<div class="col-md-8">
												<div class="input-group mb-2">
													<div class="input-group-prepend">
														<div class="input-group-text"><%=Language.equalsIgnoreCase("english") ? "+88" : "+৮৮"%></div>
													</div>
													<input type='text' class='form-control' required
														name='phone' id='phone'
														value=''
														tag='pb_html'>
												</div>
											</div>
										</div>

									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
        <%
                	}
                %>
            
             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.HM_STOCK_STATUS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_DRUGINFORMATIONTYPE, loginDTO)%></th>
								
								<th><%=Language.equalsIgnoreCase("english")?"Total Requisition":"মোট রিকুইজিশন"%></th>

								<th><%=LM.getText(LC.HM_STOCK_STATUS, loginDTO)%></th>
								
								<%
						        if(isDelivery)
						        {
						        %>
								<th><%=LM.getText(LC.HM_QUANTITY, loginDTO)%></th>
								<%
						        }
								%>

							</tr>
							<%
								PersonalStockDAO personalStockDAO = PersonalStockDAO.getInstance();
								List<PersonalStockDTO> personalStockDTOs = new ArrayList<PersonalStockDTO> ();
								if(medicalDeptCat >= 0)
								{
									personalStockDTOs = (List<PersonalStockDTO>) personalStockDAO.getDTOsByParent("medical_dept_cat",
											medicalDeptCat);
								}
										

							for (PersonalStockDTO personalStockDTO : personalStockDTOs) {
							%>
                         			<tr>
										<td>
											<%=CommonDAO.getName(Language, "drug_information", personalStockDTO.drugInformationType)%>
										</td>
										
										<td>
											<%=Utils.getDigits(medical_inventory_outDAO.getSumBorrowed(medicalDeptCat, personalStockDTO.drugInformationType), Language)%>
										</td>
										
										<td>
											<%=Utils.getDigits(personalStockDTO.quantity, Language)%>
											<%=personalStockDTO.drugInformationType == Drug_informationDTO.ACCU_CHECK_ID?" (" + personalStockDTO.detailedCount + ")" :""%>
										</td>
									
										
										<%
								        if(isDelivery)
								        {
								        %>
										<td>
											
											<input type='number' class='form-control'  name='quantity' 
											max="<%=personalStockDTO.quantity%>" min="0"
											 value='0'  tag='pb_html'>
										</td>
										<%
								        }
										%>
										
										
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
                
                <%
		        if(isDelivery)
		        {
		        %>
		        <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_MEDICINE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.DELIVER_OR_RETURN_MEDICINE_ADD_DELIVER_OR_RETURN_MEDICINE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
              </form>
		        <%
		        }
		        %>
        </div>
    </div>
</div>


<script type="text/javascript">

function patient_inputted(userName, orgId) {
    console.log('changed value: ' + userName);
    $("#userName").val(userName);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var familyMemberDTO = JSON.parse(this.responseText);
            <%
            if(Language.equalsIgnoreCase("english"))
            {
            %>
            $('#name').val(familyMemberDTO.nameEn);
            <%
            }
            else
            {
            %>
            $('#name').val(familyMemberDTO.nameBn);
            <%
            }
            %>
            $('#phone').val(phoneNumberRemove88ConvertLanguage(familyMemberDTO.mobile, '<%=Language%>'));
           
        } else {
            //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
        }
    };

    xhttp.open("POST", "FamilyServlet?actionType=getPatientDetails&familyMemberId=<%=FamilyDTO.OWN%>&userName=" + userName + "&language=<%=Language%>", true);
    xhttp.send();
}

function PreprocessBeforeSubmiting(row, action) {
    console.log("action = " + action);
	if($("#userName").val() == "")
	{
		toastr.error("Patient Cannot be empty");
	}
	else
	{
		submitAddForm2();
	}

    
    return false;
}
</script>