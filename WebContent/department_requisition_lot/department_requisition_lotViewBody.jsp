

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="department_requisition_lot.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>





<%
String servletName = "Department_requisition_lotServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Department_requisition_lotDTO department_requisition_lotDTO = Department_requisition_lotDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = department_requisition_lotDTO;
boolean isDeliveryFromShop = (boolean)request.getAttribute("isDeliveryFromShop");
String context = request.getContextPath() + "/";

%>
<%@include file="../pb/viewInitializer.jsp"%>
<%
boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
    .signature-image {
        border-bottom: 2px solid #a406dc !important;
        width: 200px !important;
        height: 53px !important;
    }

    .signature-div {

        font-size: 10px !important;
    }

    .signature-div * {
        font-size: 10px !important;
    }
</style>
<div class="ml-auto mr-2 mt-4">
	<button type="button" class="btn" id='pdf'
            onclick="downloadPdf('requisition.pdf','modalbody')">
        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='printer'
            onclick="printAnyDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>

<div class="kt-content prescription-details-report-page-for-browser" id="kt_content">
    <div class="kt-portlet" id="modalbody" style="background-color: #f2f2f2!important;">
        <div class="kt-portlet__body m-3" style="background-color: #f6f9fb!important;">
            <table class="w-100 mt-5">
                <tr>
                    <td align="center">
                        <img
                                class="parliament-logo"
                                width="8%"
                                src="<%=context%>assets/images/perliament_logo_final2.png"
                                alt=""
                        />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <p class="mb-0"><%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <a class="website-link" href="www.parliament.gov.bd"
                        >www.parliament.gov.bd</a
                        >
                    </td>
                </tr>
            </table>
               <div class="mb-5">
                <table class="w-100 mt-5">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="pt-1">
                                        <img width="65%" src="<%=context%>assets/images/prp_logo.png"
                                             alt="parliament logo"/>
                                    </td>
                                    <td>
                                        <table style="margin-left: -60px">
                                            <tr>
                                                <td>
                                                    <p class="mb-0">

                                                        <%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_LOT_ADD_FORMNAME, loginDTO)%>
                                                    </p>
                                                   
                                                    <p class="mb-0"><%=Utils.capitalizeFirstLetter(LM.getText(LC.APPOINTMENT_ADD_APPOINTMENT_ADD_FORMNAME, loginDTO))%>
                                                        : prp.parliament.gov.bd</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <table>
                                <tr>
                                    <td>
                                        <p class="mb-0"><%=Utils.capitalizeFirstLettersOfWords(LM.getText(LC.HM_PARLIAMENT_MEDICAL_CENTRE, loginDTO))%>
                                        </p>
                                        <p class="mb-0"><%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                                        </p>
                                        <p class="mb-0"><%=LM.getText(LC.HM_PARLIAMENT_PHONE, loginDTO)%>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="border-top border-dark mt-3"></div>
                <table class="w-100 mt-4">
                
                	<tr>
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Application Date":"আবেদনের তারিখ"%>
                                    :</strong> <%=Utils.getDigits(simpleDateFormat.format(new Date(department_requisition_lotDTO.insertionDate)), Language)%>
                            
                        </td>
                    
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                 <strong><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_MEDICALDEPTCAT, loginDTO)%>
                                    :</strong> <%=CatRepository.getInstance().getText(Language, "medical_dept", department_requisition_lotDTO.medicalDeptCat)%>

                                    
                            
                        </td>
						
						<td style = "width:34%; padding-bottom:1.5rem; vertical-align:top">
								<strong><%=Language.equalsIgnoreCase("english")?"Requested By":"আবেদনকারী"%>:</strong>
								<%=WorkflowController.getNameFromUserId(department_requisition_lotDTO.insertedByUserId, Language)%><br>
                                    <%=WorkflowController.getOrganogramName(department_requisition_lotDTO.insertedByOrganogramId, Language)%><br>
                                	<%=WorkflowController.getUnitNameFromOrganogramId(department_requisition_lotDTO.insertedByOrganogramId, Language)%>
								
						</td>
					</tr>
					
						<%
					if(department_requisition_lotDTO.isApproved != 0)
					{
					%>
					
					<tr>
					 	<td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Approval/Rejection Date":"অনুমোদন/বাতিলের তারিখ"%>
                                    :</strong> <%=Utils.getDigits(simpleDateFormat.format(new Date(department_requisition_lotDTO.approvalTime)), Language)%>
                            
                        </td>
                        
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Approval Status":"অনুমোদনের অবস্থা"%>
                                    :</strong> <%=Utils.getApprovalStatus(department_requisition_lotDTO.isApproved, Language)%>
                            
                        </td>
                        
                        <td style = "width:34%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Action taken By":"পদক্ষেপগ্রহীতা"%>
                                    :</strong>  <%=WorkflowController.getNameFromUserName(department_requisition_lotDTO.approverUserName, Language)%><br>
                                    <%=WorkflowController.getOrganogramName(department_requisition_lotDTO.approverUserName, Language)%>
                                    <br><%=WorkflowController.getUnitNameFromUserName(department_requisition_lotDTO.approverUserName, Language)%>
                            
                        </td>
                    
                       
                        
                       
                    </tr>
                    <%
					}
                    %>
                    
                    		<%
					if(department_requisition_lotDTO.isDelivered)
					{
					%>
					<tr>
                        
                        <td style = "width:33%; padding-bottom:1.5rem; vertical-align:top">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Delivery Date":"ডেলিভারির  তারিখ"%>
                                    :</strong> <%=Utils.getDigits(simpleDateFormat.format(new Date(department_requisition_lotDTO.deliveryDate)), Language)%>
                            
                        </td>
                        
                        <td style = "padding-bottom:1.5rem; vertical-align:top" colspan = "2">
                            
                                <strong><%=Language.equalsIgnoreCase("english")?"Delivered By":"সরবরাহকারী"%>
                                    :</strong>  <%=WorkflowController.getNameFromUserName(department_requisition_lotDTO.delivererUserName, Language)%><br>
                                    <%=WorkflowController.getOrganogramName(department_requisition_lotDTO.delivererUserName, Language)%>
                                    <br><%=WorkflowController.getUnitNameFromUserName(department_requisition_lotDTO.delivererUserName, Language)%>
                            
                        </td>
                    
                        
                        
                       
                    </tr>
					<%
					}
					%>
                   
                </table>
                
               <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_DRUGINFORMATIONTYPE, loginDTO)%></th>

								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_QUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DEPARTMENT_REQUISITION_ITEM_REMARKS, loginDTO)%></th>

							</tr>
							<%
                        	DepartmentRequisitionItemDAO departmentRequisitionItemDAO = DepartmentRequisitionItemDAO.getInstance();
                         	List<DepartmentRequisitionItemDTO> departmentRequisitionItemDTOs = (List<DepartmentRequisitionItemDTO>)departmentRequisitionItemDAO.getDTOsByParent("department_requisition_lot_id", department_requisition_lotDTO.iD);
                         	
                         	for(DepartmentRequisitionItemDTO departmentRequisitionItemDTO: departmentRequisitionItemDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=CommonDAO.getName(Language, "drug_information", departmentRequisitionItemDTO.drugInformationType)%>
										</td>
										
										<td>
											<%=Utils.getDigits(departmentRequisitionItemDTO.quantity, Language)%>
										</td>
										<td>
											<%=departmentRequisitionItemDTO.remarks%>
										</td>
										
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
                <%
		        if(isDeliveryFromShop)
		        {
		        %>
		        <div class="form-actions text-right mb-2 mt-4">
                    
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" 
                    onclick="location.href='Department_requisition_lotServlet?actionType=confirmDelivery&id=<%=department_requisition_lotDTO.iD%>'">
                        <%=LM.getText(LC.HM_DELIVER, loginDTO)%>
                    </button>
                </div>

		        <%
		        }
		        %>
		        <%
		        if(department_requisition_lotDTO.isApproved == 1)
		        {
		        %>
		     
                <div class=" mt-2">
                    <div class="w-100 d-flex justify-content-end">
		                <div class="signature-div text-center">
		                    <div>
		                        <%
			                    byte [] encodeBase64Photo = WorkflowController.getBase64SignatureFromUserName(department_requisition_lotDTO.approverUserName);
			                    %>
			                    <img src='data:image/jpg;base64,<%=encodeBase64Photo != null ? new String(encodeBase64Photo) : ""%>' style="width:150px"
			                                     id="signature-img">
		                        <br>

		                    </div>
		                    <div>
		                        <%=WorkflowController.getNameFromUserName(department_requisition_lotDTO.approverUserName, Language)%><br>
		                        <%=WorkflowController.getOrganogramName(department_requisition_lotDTO.approverOrganogramId, Language)%><br>
		                        <%=WorkflowController.getOfficeNameFromOrganogramId(department_requisition_lotDTO.approverOrganogramId, Language)%><br>
		                    </div>
		                </div>
		            </div>
		        </div>
                <%
		        }
                %>
		    </div>
		</div>
	</div>
</div>
		  
            
            
            
            
