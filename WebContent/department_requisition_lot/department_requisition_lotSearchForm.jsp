
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="department_requisition_lot.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page pageEncoding="UTF-8" %>


<%
String navigator2 = "navDEPARTMENT_REQUISITION_LOT";
String servletName = "Department_requisition_lotServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_SL, loginDTO)%>
            					</th>
								<th><%=Language.equalsIgnoreCase("english")?"Requisition By":"রিকুইজিশন্দাতা"%></th>		
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_MEDICALDEPTCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_DESCRIPTION, loginDTO)%></th>
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_ADD_ISAPPROVED, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=Language.equalsIgnoreCase("english")?"Departmental Stock":"ডিপার্টমেন্টাল স্টক"%></th>									
								<th><%=LM.getText(LC.DEPARTMENT_REQUISITION_LOT_SEARCH_DEPARTMENT_REQUISITION_LOT_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Department_requisition_lotDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Department_requisition_lotDTO department_requisition_lotDTO = (Department_requisition_lotDTO) data.get(i);
																																
											
											%>
											<tr>
											<td>
												<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>
											</td>
											
											<td>
											<%=WorkflowController.getNameFromOrganogramId(department_requisition_lotDTO.insertedByOrganogramId, "english")%>
											</td>
								
		
											<td>
											<%=CatRepository.getInstance().getText(Language, "medical_dept", department_requisition_lotDTO.medicalDeptCat)%>
											</td>
		
											<td>
											<%=department_requisition_lotDTO.description%>
											</td>
		
											<td>
											<%=Utils.getApprovalStatus(department_requisition_lotDTO.isApproved, Language)%>
											
											<%
											if(department_requisition_lotDTO.approvalMessage != null && !department_requisition_lotDTO.approvalMessage.equalsIgnoreCase(""))
											{
												%>
												<br><%=department_requisition_lotDTO.approvalMessage%>
												<%
											}
											%>
											</td>
		
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = department_requisition_lotDTO; %>
											<td>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											    
											    <%
											    if(department_requisition_lotDTO.isApproved == 0 && (userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
														|| userDTO.roleID == SessionConstants.ADMIN_ROLE ))
											    {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=getApprovalPage&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-stamp"></i>
											    </button>
											    <%
											    }
											    %>
											    
											    <%
											    if(department_requisition_lotDTO.isApproved == 1 && !department_requisition_lotDTO.isDelivered && ( 
														userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.PHARMACY_PERSON ))
											    {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #886b6b;"
											            onclick="location.href='<%=servletName%>?actionType=deliverFromShop&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-plus"></i>
											    </button>
											    <%
											    }
											    %>
											</td>
											
											<td>
												<button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #997b7b;"
											            onclick="location.href='<%=servletName%>?actionType=viewDepartmentalStock&medicalDeptCat=<%=department_requisition_lotDTO.medicalDeptCat%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											    
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #997b7b;"
											            onclick="location.href='<%=servletName%>?actionType=deliverDepartmentalStock&medicalDeptCat=<%=department_requisition_lotDTO.medicalDeptCat%>'"
											    >
											        <i class="fas fa-briefcase-medical"></i>
											    </button>
											</td>											
													
											
											<td>
											<%
											    if(department_requisition_lotDTO.isApproved == 0 && (userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
														|| userDTO.roleID == SessionConstants.ADMIN_ROLE ))
											    {
											    %>
											    
											    <button
											            type="button"
											            class="btn-sm border-0 shadow btn-border-radius text-white"
											            style="background-color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-edit"></i>
											    </button>
											    <%
											    }
											    %>
											   
											</td>
											
																				
											<td class="text-right">
											<%
											    if(department_requisition_lotDTO.isApproved == 0 && (userDTO.roleID == SessionConstants.MEDICAL_ADMIN_ROLE 
														|| userDTO.roleID == SessionConstants.ADMIN_ROLE ))
											    {
											    %>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=department_requisition_lotDTO.iD%>'/></span>
												</div>
												<%
											    }
												%>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			