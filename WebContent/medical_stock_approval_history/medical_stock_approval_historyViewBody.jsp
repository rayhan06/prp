

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="medical_stock_approval_history.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Medical_stock_approval_historyServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Medical_stock_approval_historyDTO medical_stock_approval_historyDTO = Medical_stock_approval_historyDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = medical_stock_approval_historyDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_MEDICAL_STOCK_APPROVAL_HISTORY_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_MEDICAL_STOCK_APPROVAL_HISTORY_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_DRUGINFORMATIONID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(medical_stock_approval_historyDTO.drugInformationId, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_EXISTINGQUANTITY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(medical_stock_approval_historyDTO.existingQuantity, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVEDQUANTITY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(medical_stock_approval_historyDTO.approvedQuantity, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVALDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, medical_stock_approval_historyDTO.approvalDate), Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVERORGANOGRAMID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=WorkflowController.getNameFromOrganogramId(medical_stock_approval_historyDTO.approverOrganogramId, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVEREMPLOYEEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(medical_stock_approval_historyDTO.approverEmployeeId, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVERUSERNAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=medical_stock_approval_historyDTO.approverUserName%>
                                    </div>
                                </div>
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>