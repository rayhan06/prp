<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="medical_stock_approval_history.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Medical_stock_approval_historyDTO medical_stock_approval_historyDTO = new Medical_stock_approval_historyDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	medical_stock_approval_historyDTO = Medical_stock_approval_historyDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = medical_stock_approval_historyDTO;
String tableName = "medical_stock_approval_history";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_MEDICAL_STOCK_APPROVAL_HISTORY_ADD_FORMNAME, loginDTO);
String servletName = "Medical_stock_approval_historyServlet";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Medical_stock_approval_historyServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=medical_stock_approval_historyDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='drugInformationId' id = 'drugInformationId_hidden_<%=i%>' value='<%=medical_stock_approval_historyDTO.drugInformationId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_EXISTINGQUANTITY, loginDTO)%>															</label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(medical_stock_approval_historyDTO.existingQuantity != -1)
																	{
																	value = medical_stock_approval_historyDTO.existingQuantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='existingQuantity' id = 'existingQuantity_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVEDQUANTITY, loginDTO)%>															</label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(medical_stock_approval_historyDTO.approvedQuantity != -1)
																	{
																	value = medical_stock_approval_historyDTO.approvedQuantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approvedQuantity' id = 'approvedQuantity_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVALDATE, loginDTO)%>															</label>
                                                            <div class="col-8">
																<%value = "approvalDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='approvalDate' id = 'approvalDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(medical_stock_approval_historyDTO.approvalDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVERORGANOGRAMID, loginDTO)%>															</label>
                                                            <div class="col-8">
																<button type="button" class="btn btn-primary form-control" onclick="addEmployeeWithRow(this.id)" id="approverOrganogramId_button_<%=i%>" tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%></button>
																<table class="table table-bordered table-striped">
																	<tbody id="approverOrganogramId_table_<%=i%>" tag='pb_html'>
																	<%
																	if(medical_stock_approval_historyDTO.approverOrganogramId != -1)
																	{
																		%>
																		<tr>
																			<td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(medical_stock_approval_historyDTO.approverOrganogramId)%></td>
																			<td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(medical_stock_approval_historyDTO.approverOrganogramId, Language)%></td>
																			<td><%=WorkflowController.getOrganogramName(medical_stock_approval_historyDTO.approverOrganogramId, Language)%>, <%=WorkflowController.getUnitNameFromOrganogramId(medical_stock_approval_historyDTO.approverOrganogramId, Language)%></td>
																		</tr>
																		<%
																	}
																	%>
																	</tbody>
																</table>
																<input type='hidden' class='form-control'  name = 'approverOrganogramId' id = 'approverOrganogramId_hidden_<%=i%>' value='<%=medical_stock_approval_historyDTO.approverOrganogramId%>' tag='pb_html'/>
			
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='approverEmployeeId' id = 'approverEmployeeId_hidden_<%=i%>' value='<%=medical_stock_approval_historyDTO.approverEmployeeId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_APPROVERUSERNAME, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverUserName' id = 'approverUserName_text_<%=i%>' value='<%=medical_stock_approval_historyDTO.approverUserName%>'  required="required"  pattern="^[A-Za-z0-9]{5,}" title="approverUserName must contain at least 5 letters"   tag='pb_html'/>					
															</div>
                                                      </div>									
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_MEDICAL_STOCK_APPROVAL_HISTORY_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.MEDICAL_STOCK_APPROVAL_HISTORY_ADD_MEDICAL_STOCK_APPROVAL_HISTORY_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	preprocessDateBeforeSubmitting('approvalDate', row);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Medical_stock_approval_historyServlet");	
}

function init(row)
{

	setDateByStringAndId('approvalDate_js_' + row, $('#approvalDate_date_' + row).val());

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	


</script>






