<%@page pageEncoding="UTF-8" %>

<%@page import="employee_license_key.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.EMPLOYEE_LICENSE_KEY_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_EMPLOYEE_LICENSE_KEY;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Employee_license_keyDTO employee_license_keyDTO = (Employee_license_keyDTO)request.getAttribute("employee_license_keyDTO");
CommonDTO commonDTO = employee_license_keyDTO;
String servletName = "Employee_license_keyServlet";


System.out.println("employee_license_keyDTO = " + employee_license_keyDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Employee_license_keyDAO employee_license_keyDAO = (Employee_license_keyDAO)request.getAttribute("employee_license_keyDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_assetLicenseKeyId'>
											<%
											value = employee_license_keyDTO.assetLicenseKeyId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_employeeRecordsId'>
											<%
											value = employee_license_keyDTO.employeeRecordsId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_remarks'>
											<%
											value = employee_license_keyDTO.remarks + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	

											<td>
												<a href='Employee_license_keyServlet?actionType=view&ID=<%=employee_license_keyDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Employee_license_keyServlet?actionType=getEditPage&ID=<%=employee_license_keyDTO.iD%>'><%=LM.getText(LC.EMPLOYEE_LICENSE_KEY_SEARCH_EMPLOYEE_LICENSE_KEY_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=employee_license_keyDTO.iD%>'/></span>
												</div>
											</td>
																						
											

