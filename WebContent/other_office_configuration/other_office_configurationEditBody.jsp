<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="other_office_configuration.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@page import="other_office.*"%>

<%
Other_office_configurationDTO other_office_configurationDTO = new Other_office_configurationDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	other_office_configurationDTO = Other_office_configurationDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = other_office_configurationDTO;
String tableName = "other_office_configuration";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.OTHER_OFFICE_CONFIGURATION_ADD_OTHER_OFFICE_CONFIGURATION_ADD_FORMNAME, loginDTO);
String servletName = "Other_office_configurationServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Other_office_configurationServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=other_office_configurationDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.OTHER_OFFICE_CONFIGURATION_ADD_OTHEROFFICETYPE, loginDTO)%>
															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='otherOfficeType' id = 'otherOfficeType'   tag='pb_html' onchange = "redirectIfAvailable(this.value)">
																	<%=Other_officeRepository.getInstance().buildOptions(Language, (long)other_office_configurationDTO.otherOfficeType)%>
																</select>
		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.OTHER_OFFICE_CONFIGURATION_ADD_CONFIGURTIONLIST, loginDTO)%>
															</label>
                                                            <div class="col-8">
																
																<select class='form-control'  name='configurtionList' id = 'configurtionList'   tag='pb_html' multiple = "multiple">
																	<option value = ''><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
																	<%List <CatDTO>  permissions = CatRepository.getDTOs("other_office_permissions");%>
																	<%
																	for(CatDTO permission: permissions)
																	{
																		%>
																		<option value = '<%=permission.value%>'
																		<%=other_office_configurationDTO.existingPermissions.contains(permission.value + "")? "selected":""%>
																		>
																		<%=Language.equalsIgnoreCase("english")?permission.nameEn:permission.nameBn%>
																		</option>
																		<%
																	}
																	%>
																</select>				
															</div>
                                                      </div>									
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.OTHER_OFFICE_CONFIGURATION_ADD_OTHER_OFFICE_CONFIGURATION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.OTHER_OFFICE_CONFIGURATION_ADD_OTHER_OFFICE_CONFIGURATION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

function redirectIfAvailable(otherOfficeType)
{
	
	if(otherOfficeType == "-1")
	{
		return;
	}
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) 
		{
			
			if(this.responseText != "-1")
			{
				console.log("redirecting");
				location.href="Other_office_configurationServlet?actionType=getEditPage&ID=" + this.responseText;
			}
		}
			
	};
	xhttp.open("GET", "Other_office_configurationServlet?actionType=getByOfficeId&otherOfficeType=" + otherOfficeType, true);
	xhttp.send();
}

function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Other_office_configurationServlet");	
}

function init(row)
{

	$("#configurtionList").select2({
        dropdownAutoWidth: true,
        theme: "classic"
    });
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	


</script>






