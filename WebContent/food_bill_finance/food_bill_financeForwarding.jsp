<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    String context = "../../.." + request.getContextPath() + "/";
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String title = isLangEng ? "Food Bill Forwarding" : "খাবার বিল ফরওয়ার্ডিং";
%>
<link href="<%=context%>/assets/css/pagecustom.css" rel="stylesheet" type="text/css"/>
<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="<%=title%>"/>
    <jsp:param name="body" value="../food_bill_finance/food_bill_financeForwardingBody.jsp"/>
</jsp:include> 