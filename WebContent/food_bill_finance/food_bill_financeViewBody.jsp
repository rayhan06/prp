<%@ page import="java.util.*" %>
<%@page import="util.*" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="food_bill_finance.Food_bill_financeDTO" %>
<%@ page import="food_bill.Food_billSummaryModel" %>
<%@ page import="food_bill.Food_billDAO" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = "english".equalsIgnoreCase(Language);
    String formTitle = isLangEn ? "Food Bill" : "খাবার ভাতা";

    Food_bill_financeDTO foodBillFinanceDTO = (Food_bill_financeDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);

    String formattedTotalBill = BangladeshiNumberFormatter.getFormattedNumber(
            StringUtils.convertBanglaIfLanguageIsBangla(
                    Language,
                    String.format("%d", foodBillFinanceDTO.totalBillAmount)
            )
    );
    String formattedEmployeeCount = BangladeshiNumberFormatter.getFormattedNumber(
            StringUtils.convertBanglaIfLanguageIsBangla(
                    Language,
                    String.format("%d", foodBillFinanceDTO.totalEmployeeCount)
            )
    );

    List<Food_billSummaryModel> foodBillSummaryModels =
            Food_billDAO.getInstance()
                        .findByFoodBillFinanceId(foodBillFinanceDTO.iD)
                        .stream()
                        .sorted(Comparator.comparingInt(billDTO -> billDTO.financeSerialNumber))
                        .map(foodBillDTO -> new Food_billSummaryModel(foodBillDTO, Language))
                        .collect(Collectors.toList());

    String forwardingViewUrl = "Food_bill_financeServlet?actionType=getForwardingPage&ID=" + foodBillFinanceDTO.iD;
    String bankStatementViewUrl = "Food_bill_financeServlet?actionType=getBankStatementPage&ID=" + foodBillFinanceDTO.iD;
    String foodBillViewUrlFormat = "Food_billServlet?actionType=view&ID=%s";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="overtime-allowance-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row mt-2">
                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5">
                                <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                            </label>
                            <div class='form-control rounded shadow-sm'>
                                <%=Budget_mappingRepository.getInstance().getOperationText(
                                        Language,
                                        foodBillFinanceDTO.budgetMappingId
                                )%>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5">
                                <%=isLangEn ? "Economic Code" : "অর্থনৈতিক কোড"%>
                            </label>
                            <div class='form-control rounded shadow-sm'>
                                <%=Economic_sub_codeRepository.getInstance().getText(
                                        Language,
                                        foodBillFinanceDTO.economicSubCodeId
                                )%>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5">
                                <%=isLangEn ? "Parliament Session" : "সংসদ অধিবেশন"%>
                            </label>
                            <div class='form-control rounded shadow-sm'>
                                <%=foodBillFinanceDTO.getText(isLangEn)%>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="action-btn-div">
                    <div class="col-12 mt-3 text-right">
                        <a class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                           href="<%=forwardingViewUrl%>"
                        >
                            <%=isLangEn ? "See Forwarding" : "ফরওয়ার্ডিং দেখুন"%>
                        </a>
                        <a class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                           href="<%=bankStatementViewUrl%>"
                        >
                            <%=isLangEn ? "See Bank Statement" : "ব্যাংক স্টেটমেন্ট দেখুন"%>
                        </a>
                    </div>
                </div>

                <div class="mt-4 table-responsive">
                    <table id="overtime-allowance-table" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr class="text-center">
                            <th style="width:10%;">নং</th>
                            <th style="width:20%;">কার্যালয়</th>
                            <th style="width:40%;">মাসের নাম</th>
                            <th style="width:10%;">জনবল</th>
                            <th style="width:10%;">টাকার পরিমাণ</th>
                            <th style="width:10%;"></th>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        <%for (Food_billSummaryModel model : foodBillSummaryModels) {%>
                        <tr class="template-row">
                            <td class="text-center">
                                <%=StringUtils.convertToBanNumber(String.format("%d", model.financeSerialNumber))%>
                            </td>
                            <td class="text-center">
                                <%=model.officeName%>
                            </td>
                            <td class="text-center">
                                <%=model.sessionAndMonths%>
                            </td>
                            <td class="text-right">
                                <%=model.employeeCount%>
                            </td>
                            <td class="text-right">
                                <%=model.billAmount%>
                            </td>
                            <td class="details-button text-center">
                                <a class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
                                   href="<%=String.format(foodBillViewUrlFormat, model.foodBillId)%>"
                                >
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        <%}%>
                        <tr>
                            <td colspan="3" class="text-right" style="font-weight: bold">
                                <%=isLangEn ? "Total" : "মোট"%>
                            </td>
                            <td class="text-right">
                                <%=formattedEmployeeCount%>
                            </td>
                            <td class="text-right">
                                <%=formattedTotalBill%>
                            </td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>