<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.*" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="pb.Utils" %>
<%@ page import="budget_register.Budget_registerDAO" %>
<%@ page import="budget_register.Budget_registerDTO" %>
<%@ page import="food_allowance.Food_allowanceModel" %>
<%@page pageEncoding="UTF-8" %>

<%
    List<Food_allowanceModel> allowanceModels = (List<Food_allowanceModel>) request.getAttribute("allowanceModels");
    long budgetRegisterId = (long) request.getAttribute("budgetRegisterId");
    Budget_registerDTO budgetRegisterDTO = Budget_registerDAO.getInstance().getDTOFromID(budgetRegisterId);

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);
    long foodBillFinanceId = (long) request.getAttribute("foodBillFinanceId");
    String excelDownloadUrl = "Food_bill_financeServlet?actionType=getBankStatementXl&ID=" + foodBillFinanceId;
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    #to-print-div * {
        font-size: 12pt;
    }

    #to-print-div h1 {
        font-size: 15pt;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14pt;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        background: white;
        padding: .4in;
        margin-bottom: 5px;
        page-break-after: always;
    }

    .align-top {
        vertical-align: top;
    }

    thead th,
    thead td {
        text-align: center;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 4px;
    }

    @media print {
        @page {
            margin: .15in;
        }
        .page[data-size="A4"] {
            margin-bottom: 0 !important;
        }
    }
</style>
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg" id="bill-div">
                <div class="kt-subheader__main ml-4">
                    <label class="h2 kt-subheader__title" style="color: #00a1d4;">
                        <%=UtilCharacter.getDataByLanguage(
                                Language,
                                "অধিকাল ভাতার ব্যাংক স্টেটমেন্ট",
                                "Overtime Bill Bank Statement"
                        )%>
                    </label>
                </div>
                <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="printDivWithJqueryPrint('to-print-div');"
                            title="Print"
                    >
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>

                    <button type="button" class="btn" id='download-excel'
                            onclick="location.href = '<%=excelDownloadUrl%>'"
                            title="Download Excel"
                    >
                        <i class="fa fa-file-excel fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <%
                            long totalAmountRunningTotal = 0;
                            long deductionRunningTotal = 0;
                            long netAmountRunningTotal = 0;
                            boolean isLastPage = false;
                            final int rowsPerPage = 24;
                            int index = 0;
                            while (index < allowanceModels.size()) {
                                boolean isFirstPage = (index == 0);
                        %>
                        <section class="page" data-size="A4">
                            <%if (isFirstPage) {%>
                            <div class="text-center">
                                <h1 style="font-weight: normal">বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">অর্থ শাখা-১</h2>
                                <h2 style="font-weight: normal">
                                    <%=budgetRegisterDTO.description%>
                                </h2>
                            </div>
                            <%}%>

                            <div>
                                <div class="mt-1">
                                    <table class="table-bordered-custom w-100">
                                        <thead>
                                        <tr style="height: 50px;background-color:lightgrey">
                                            <th style="width:5%;">ক্র. নং</th>
                                            <th style="width:25%;">নাম</th>
                                            <th style="width:20%;">পদবী</th>
                                            <th style="width:10%;">মোবাইল নম্বর</th>
                                            <th style="width:10%;">সঞ্চয়ী হিসাব নম্বর</th>
                                            <th style="width:10%;">টাকার পরিমাণ</th>
                                            <th style="width:10%;">রাজস্ব স্ট্যাম্প বাবদ কর্তন</th>
                                            <th style="width:10%;">নীট দাবী</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <%if (!isFirstPage) {%>
                                        <tr>
                                            <td colspan="3" class="text-right">
                                                পূর্ব পৃষ্ঠার জের=
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(deductionRunningTotal))
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(netAmountRunningTotal))
                                                )%>
                                            </td>
                                        </tr>
                                        <%}%>

                                        <%
                                            int rowsInThisPage = 0;
                                            while (index < allowanceModels.size() && rowsInThisPage < rowsPerPage) {
                                                isLastPage = (index == (allowanceModels.size() - 1));
                                                rowsInThisPage++;
                                                Food_allowanceModel model = allowanceModels.get(index++);
                                                totalAmountRunningTotal += model.totalAmount;
                                                deductionRunningTotal += model.revenueStampDeduction;
                                                netAmountRunningTotal += model.netAmount;
                                        %>
                                        <tr style="height: 40px">
                                            <td class="text-center"><%=Utils.getDigits(index, "BANGLA")%>
                                            </td>
                                            <td class="align-top">
                                                <%=model.name%>
                                            </td>
                                            <td class="align-top text-center">
                                                <%=model.organogramName%>
                                            </td>
                                            <td style="text-align: center">
                                                <%=StringUtils.convertToBanNumber(model.mobileNumber)%>
                                            </td>
                                            <td style="text-align: center">
                                                <%=StringUtils.convertToBanNumber(model.savingAccountNumber)%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.totalAmount, "BANGLA"))%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.revenueStampDeduction, "BANGLA"))%>
                                            </td class="text-right">
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(Utils.getDigits(model.netAmount, "BANGLA"))%>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                <%=isLastPage ? "সর্বমোট" : "উপমোট"%>=
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(deductionRunningTotal))
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(netAmountRunningTotal))
                                                )%>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <%if (isLastPage) {%>
                                    <div class="mt-1 offset-4 ml-auto text-center">
                                        কথায়:
                                        <strong><%=BangladeshiNumberInWord.convertToWord(Utils.getDigits(totalAmountRunningTotal, "Bangla"))%>
                                        </strong> টাকা মাত্র
                                    </div>
                                    <%}%>
                                </div>
                            </div>
                        </section>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../utility/jquery_print.jsp"/>