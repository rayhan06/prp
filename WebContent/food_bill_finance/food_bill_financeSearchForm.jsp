<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="overtime_allowance.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="util.StringUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="food_bill_finance.Food_bill_financeDTO" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navOVERTIME_ALLOWANCE";
    String servletName = "Food_bill_financeServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="text-center">
        <tr>
            <th>
                <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Economic Code" : "অর্থনৈতিক কোড"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Bill Type" : "বিলের ধরণ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Employee Count" : "কর্মকর্তা/কর্মচারীর সংখ্যা"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Total Bill" : "মোট বিলের পরিমাণ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "View Details" : "বিস্তারিত দেখুন"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Edit" : "পরিবর্তন"%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Food_bill_financeDTO> data = (List<Food_bill_financeDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Food_bill_financeDTO foodBillFinanceDTO : data) {
        %>
        <tr>
            <td>
                <%=Budget_mappingRepository.getInstance().getOperationText(
                        Language,
                        foodBillFinanceDTO.budgetMappingId
                )%>
            </td>
            <td>
                <%=Economic_sub_codeRepository.getInstance().getText(
                        Language,
                        foodBillFinanceDTO.economicSubCodeId
                )%>
            </td>
            <td>
                <%=foodBillFinanceDTO.getText(isLanguageEnglish)%>
            </td>
            <td class="text-center">
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.format("%d", foodBillFinanceDTO.totalEmployeeCount))%>
            </td>
            <td class="text-right">
                <%=BangladeshiNumberFormatter.getFormattedNumber(
                        StringUtils.convertBanglaIfLanguageIsBangla(
                                Language,
                                String.format("%d", foodBillFinanceDTO.totalBillAmount)
                        )
                )%>/-
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=foodBillFinanceDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>
            <td>
                <%if (foodBillFinanceDTO.isDeletableByUser(userDTO)) {%>
                <button type="button"
                        data-delete-btn
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="openDeleteWarning(<%=foodBillFinanceDTO.iD%>)"
                >
                    <i class="fa fa-trash"></i>
                </button>
                <%}%>
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<script>
    function openDeleteWarning(overtimeBillId) {
        const message = utilIsBangla ? "আপনি কি রেকর্ড মুছে ফেলার ব্যাপারে নিশ্চিত?"
                                     : "Are you sure you want to delete the record?";
        deleteDialog(
            message,
            () => deleteOvertimeBillDTO(overtimeBillId),
            () => {
            }
        );
    }

    async function deleteOvertimeBillDTO(id) {
        const url = '<%=servletName%>?actionType=ajax_deleteBill&ID=' + id;
        try {
            const res = await fetch(url, {method: 'POST'});
            const resJson = await res.json();
            console.log(resJson);
            if (resJson.success === true) {
                location.reload();
                return;
            }
            $('#toast_message').css('background-color', '#ff6063');
            showToast(resJson.message, resJson.message);
        } catch (error) {
            console.log(error);
            $('#toast_message').css('background-color', '#ff6063');
            showToast('<%=isLanguageEnglish? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে"%>');
        }
    }
</script>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>