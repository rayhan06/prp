<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="budget.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget_submission_info.Budget_submission_infoDTO" %>
<%@ page import="budget_submission_info.Budget_submission_infoDAO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDAO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.BUDGET_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String formTitle = LM.getText(LC.BUDGET_SUBMISSION_FORM_NAME, loginDTO);

    String context = request.getContextPath() + "/";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);
    BudgetTypeEnum budgetTypeEnum = budgetInfo.budgetTypeEnum;
    Long budgetOfficeId = (Long) request.getAttribute("budgetOfficeId");
    Budget_submission_infoDTO budgetSubmissionInfoDTO = null;
    if (budgetSelectionInfoDTO != null && budgetOfficeId != null) {
        budgetSubmissionInfoDTO = Budget_submission_infoDAO.getInstance().getByBudgetTypeAndBudgetOffice(
                budgetSelectionInfoDTO.iD, budgetTypeEnum.getValue(), budgetOfficeId
        );
    }
    boolean isBudgetSubmitted = budgetSubmissionInfoDTO != null
                                && budgetSubmissionInfoDTO.isSubmit == SubmissionStatusEnum.SUBMITTED.getValue();

    String economicYearText = StringUtils.convertBanglaIfLanguageIsBangla(Language, budgetInfo.economicYear);
%>
<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }

    .template-row {
        display: none;
    }

    tfoot td {
        text-align: right;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Budget_submissionServlet?actionType=insertBudgetAmount"
              id="budgetform" name="budgetform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="kt-portlet__body form-body">
                    <div>
                        <div class="row">
                            <div class="col-12 row">
                                <div class="offset-3 col-6 text-center">
                                    <img width="15%"
                                         src="<%=context%>assets/static/parliament_logo.png"
                                         alt="logo"
                                         class="logo-default"
                                    />
                                    <h2 class="text-center mt-3 top-section-font">
                                        <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                                    </h2>
                                    <h4 class="text-center mt-2 top-section-font">
                                        <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="budgetType" id="budgetType"
                               value="<%=budgetTypeEnum.getValue()%>">
                        <input type='hidden' class='form-control' name='budgetOfficeId' id='budgetOfficeId'
                               value=<%=budgetOfficeId%>>
                        <input type='hidden' class='form-control' name='budgetSelectionInfoId'
                               id='budgetSelectionInfoId'
                               value=<%=budgetSelectionInfoDTO.iD%>>
                        <input type="hidden" name="budgetModels" id="budgetModels">

                        <div class="row mt-5">
                            <div class="col-6 col-lg-3 my-3">
                                <label for="budgetCat" class="h5">
                                    <%=LM.getText(LC.BUDGET_BUDGET_TYPE, loginDTO)%>
                                </label>
                                <select id="budgetCat" class='form-control rounded' onchange="budgetCatChanged(this);">
                                    <%=Budget_mappingRepository.getInstance().buildBudgetCatByOfficeId(Language, budgetOfficeId, 0L)%>
                                </select>
                            </div>

                            <div class="col-6 col-lg-3 my-3">
                                <label for="budgetOperationId" class="h5">
                                    <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                                </label>
                                <select id="budgetOperationId" class='form-control rounded'
                                        onchange="budgetSelectionChanged(this);">
                                    <%--Dynamically Added with AJAX--%>
                                </select>
                            </div>

                            <div class="col-6 col-lg-3 my-3">
                                <label for="economicGroup" class="h5">
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_GROUP, loginDTO)%>
                                </label>
                                <select id="economicGroup" class='form-control rounded'
                                        onchange="economicGroupChanged(this);">
                                    <%--Dynamically Added with AJAX--%>
                                </select>
                            </div>

                            <div class="col-6 col-lg-3 my-3">
                                <label for="economicCodes" class="h5">
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </label>
                                <select id="economicCodes" class='form-control rounded'
                                        onchange="economicCodesChanged(this);">
                                    <%--Dynamically Added with AJAX--%>
                                </select>
                            </div>
                        </div>

                        <div class="mt-5">
                            <h5 class="table-title text-right">
                                (<%=LM.getText(LC.BUDGET_MONEY_INPUT_UNIT_HEADER, loginDTO)%>)
                            </h5>
                            <div class="w-100">
                                <table class="table table-bordered table-striped text-nowrap" id="budget-view-table">
                                    <thead>
                                    <tr>
                                        <th class="row-data-code">
                                            <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                        </th>
                                        <th class="row-data-name">
                                            <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                        </th>
                                        <th class="row-data-budget">
                                            <%=BudgetTypeEnum.BUDGET.getText(Language)%>
                                            <br>
                                            <%=economicYearText%>
                                        </th>
                                        <th class="row-data-budget-comment">
                                            <%=LM.getText(LC.BUDGET_COMMENT, loginDTO)%>
                                        </th>
                                        <%if (budgetTypeEnum == BudgetTypeEnum.REVISED_BUDGET) {%>
                                        <th class="row-data-budget">
                                            <%=BudgetTypeEnum.REVISED_BUDGET.getText(Language)%>
                                            <br>
                                            <%=economicYearText%>
                                        </th>
                                        <th class="row-data-budget">
                                            <%=LM.getText(LC.BUDGET_COMMENT, loginDTO)%>
                                        </th>
                                        <%}%>
                                    </tr>
                                    </thead>

                                    <tbody></tbody>

                                    <tr class="template-row">
                                        <td class="row-data-code"></td>
                                        <td class="row-data-name"></td>
                                        <td class="row-data-budget text-right"></td>
                                        <td class="row-data-budget-comment"></td>
                                        <td class="row-data-revised-budget text-right"></td>
                                        <td class="row-data-comment"></td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-12 form-actions text-right">
                                <button class="btn btn-sm shadow text-white border-0 btn-border-radius" type="button"
                                        style="background: #4db2d1;"
                                        onclick="location.href='Budget_submissionServlet?actionType=getPreviewPage'">
                                    <%=LM.getText(LC.BUDGET_PREVIEW, loginDTO)%>
                                </button>

                                <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn mx-2"
                                        type="button"
                                        onclick="resetPage();">
                                    <%=LM.getText(LC.BUDGET_ADD_BUDGET_CANCEL_BUTTON, loginDTO)%>
                                </button>

                                <%if (!isBudgetSubmitted) {%>
                                <button class="btn-sm shadow text-white border-0 submit-btn" type="submit">
                                    <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                                </button>
                                <%}%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const budgetModelMap = new Map();
    const isBudgetSubmitted = <%=isBudgetSubmitted%>;
    const REVISED_BUDGET = '<%=BudgetTypeEnum.REVISED_BUDGET.getValue()%>';

    function resetPage() {
        document.getElementById('budgetOperationId').value = '';
        clearSelects(0);
        clearTable();
    }

    $(() => {
        clearTable();

        $('#budgetform').submit(async (event) => {
            event.preventDefault();

            if (isBudgetSubmitted) return;

            const budgetModels = Array.from(budgetModelMap.values());
            const budgetType = document.getElementById('budgetType').value;

            for (let budgetModel of budgetModels) {
                if (budgetType === REVISED_BUDGET) {
                    budgetModel.revisedBudgetAmount = document.getElementById('revised_budget_' + budgetModel.id).value;
                    budgetModel.revisedBudgetComment = document.getElementById('comment_' + budgetModel.id).value;
                } else {
                    budgetModel.budgetAmount = document.getElementById('budget_' + budgetModel.id).value;
                    budgetModel.budgetComment = document.getElementById('budget_comment_' + budgetModel.id).value;
                }
                budgetModelMap.set(Number(budgetModel.id), budgetModel);
            }

            $('#budgetModels').val(JSON.stringify(Array.from(budgetModelMap.values())));

            const formElement = event.target;
            const formData = new FormData(formElement);
            const url = formElement.action;
            const searchParam = new URLSearchParams(formData);

            try {
                const response = await fetch(url, {
                    method: 'post',
                    body: searchParam
                });
                const json = await response.json();
                if (!json.success) throw new Error(json);

                $('#toast_message').css('background-color', '#04c73c');
                showToast(
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVED,"Bangla")%>',
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVED,"English")%>'
                );
            } catch (error) {
                console.error(error);
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVING_FAILED,"Bangla")%>',
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVING_FAILED,"English")%>'
                );
            }
        });
    });

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "BudgetServlet");
    }

    function clearSelects(startIndex) {
        const selectIds = ['budgetOperationId', 'economicGroup', 'economicCodes'];
        for (let i = startIndex; i < selectIds.length; i++) {
            const selectElement = document.getElementById(selectIds[i]);
            selectElement.innerHTML = '';
        }
    }

    function addDataInTable(tableId, jsonData) {
        const table = document.getElementById(tableId);
        const budgetType = document.getElementById('budgetType').value;


        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.id = jsonData.economicSubCode;
        templateRow.classList.remove('template-row');

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-code').innerText = jsonData.code;
        templateRow.querySelector('td.row-data-name').innerText = jsonData.name;
        if (budgetType === REVISED_BUDGET) {
            if (!isBudgetSubmitted) {
                templateRow.querySelector('td.row-data-revised-budget').append(
                    getBudgetInputElement('revised_budget_' + jsonData.id, jsonData.revisedBudgetAmount, function () {
                        sumInputElementColumn(this, 2);
                    })
                );

                templateRow.querySelector('td.row-data-comment').append(
                    getBudgetCommentElement('comment_' + jsonData.id, jsonData.revisedBudgetComment)
                );
            } else {
                templateRow.querySelector('td.row-data-revised-budget').innerText = jsonData.revisedBudgetAmount;
                templateRow.querySelector('td.row-data-comment').innerText = jsonData.revisedBudgetComment;
            }
            templateRow.querySelector('td.row-data-budget').innerText = jsonData.budgetAmount;
            templateRow.querySelector('td.row-data-budget-comment').innerText = jsonData.budgetComment;

        } else {
            templateRow.querySelector('td.row-data-revised-budget').remove();
            templateRow.querySelector('td.row-data-comment').remove();
            if (!isBudgetSubmitted) {
                templateRow.querySelector('td.row-data-budget').append(
                    getBudgetInputElement('budget_' + jsonData.id, jsonData.budgetAmount, function () {
                        sumInputElementColumn(this, 2);
                    })
                );
                templateRow.querySelector('td.row-data-budget-comment').append(
                    getBudgetCommentElement('budget_comment_' + jsonData.id, jsonData.budgetComment)
                );
            } else {
                templateRow.querySelector('td.row-data-budget').innerText = jsonData.budgetAmount;
                templateRow.querySelector('td.row-data-budget-comment').innerText = jsonData.budgetComment;
            }
        }

        const tableBody = table.querySelector('tbody');
        tableBody.append(templateRow);
    }

    function getBudgetInputElement(id, value, onKeyUp) {
        const inputElement = document.createElement('input');
        inputElement.classList.add('form-control');
        inputElement.type = 'text';
        inputElement.id = id;
        inputElement.value = value;
        inputElement.onkeydown = keyDownEvent;
        if (onKeyUp) inputElement.onkeyup = onKeyUp;
        return inputElement;
    }

    function keyDownEvent(e) {
        return true == inputValidationForFloatValue(e, $(this), 2, null);
    }

    function getBudgetCommentElement(id, value) {
        const textAreaElement = document.createElement('textarea');
        textAreaElement.classList.add('form-control');
        textAreaElement.id = id;
        textAreaElement.rows = 4;
        textAreaElement.cols = 30;
        textAreaElement.style.resize = 'none';
        textAreaElement.style.width = 'auto';
        textAreaElement.append(value);
        return textAreaElement;
    }

    function clearTable(hideNotFoundText) {
        budgetModelMap.clear();
        const noDataText = '<%=isLanguageEnglish? "No data found!" : "কোনো তথ্য পাওয়া যায় নি!"%>';
        const table = document.querySelector('#budget-view-table');
        if(hideNotFoundText !== true) {
            table.querySelector('tbody').innerHTML = '<tr><td colspan="100%" class="text-center">' + noDataText + '</td></tr>';
        } else {
            table.querySelector('tbody').innerHTML = '';
        }
        table.querySelectorAll('tfoot').forEach(tfoot => tfoot.remove());
    }

    async function budgetCatChanged(selectElement) {
        const selectedBudgetCat = selectElement.value;
        clearSelects(0);
        clearTable();

        if (selectedBudgetCat === '') return;

        const url = 'Budget_submissionServlet?actionType=getBudgetOperation&withCode=true&budget_cat=' + selectedBudgetCat;
        const response = await fetch(url);
        document.getElementById('budgetOperationId').innerHTML = await response.text();
    }

    async function fetchAndShowBudgetModels(url) {
        const response = await fetch(url);
        const budgetModels = await response.json();
        const hasData = Array.isArray(budgetModels) && budgetModels.length > 0;
        clearTable(hasData);
        if (hasData) {
            for (const budgetModel of budgetModels) {
                addDataInTable('budget-view-table', budgetModel);
                budgetModelMap.set(
                    Number(budgetModel.id),
                    budgetModel
                );
            }
            // columns [EconomicSubCode Description Budget RevisedBudget(on condition)]
            const colIndicesToSum = [2];
            const budgetType = document.getElementById('budgetType').value;
            if (budgetType === REVISED_BUDGET) colIndicesToSum.push(3);
            const totalTitleColSpan = 2;
            const totalTitle = '<%=LM.getText(LC.BUDGET_SUBTOTAL,loginDTO)%>';
            setupTotalRow('budget-view-table', totalTitle, totalTitleColSpan, colIndicesToSum, undefined, undefined, 2);
        }
    }

    async function budgetSelectionChanged() {
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOfficeId').value;
        clearSelects(1);
        clearTable();
        if (selectedOperationGroup !== '') {
            const url = 'BudgetServlet?actionType=buildEconomicGroups&budgetSelectionInfoId='
                        + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup;
            const response = await fetch(url);
            document.getElementById('economicGroup').innerHTML = await response.text();
        }
        const budgetModelUrl =
            'BudgetServlet?actionType=getEconomicCodesForOffice&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup;
        await fetchAndShowBudgetModels(budgetModelUrl);
    }


    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroup = selectElement.value;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOfficeId').value;
        clearSelects(2);
        clearTable();
        if (selectedEconomicGroup !== '') {
            const url = 'BudgetServlet?actionType=buildEconomicCodes&budgetSelectionInfoId='
                        + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
                        + '&economicGroupId=' + selectedEconomicGroup;
            const response = await fetch(url);
            document.getElementById('economicCodes').innerHTML = await response.text();
        }
        const budgetModelUrl =
            'BudgetServlet?actionType=getEconomicCodesForOffice&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
            + '&economicGroupId=' + selectedEconomicGroup;
        await fetchAndShowBudgetModels(budgetModelUrl);
    }


    async function economicCodesChanged(selectElement) {
        const selectedEconomicCode = selectElement.value;
        const selectedEconomicGroup = document.getElementById('economicGroup').value;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOfficeId').value;
        clearTable();
        const budgetModelUrl =
            'BudgetServlet?actionType=getEconomicCodesForOffice&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
            + '&economicGroupId=' + selectedEconomicGroup + '&economicCodeId=' + selectedEconomicCode;
        await fetchAndShowBudgetModels(budgetModelUrl);
    }
</script>






