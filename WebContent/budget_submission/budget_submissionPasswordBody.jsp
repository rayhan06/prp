<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="budget.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDAO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String formTitle = LM.getText(LC.BUDGET_SUBMISSION_FORM_NAME, loginDTO);

    String Language = LM.getText(LC.BUDGET_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    String context = request.getContextPath() + "/";

    BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetSubmission(System.currentTimeMillis());
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = new BudgetSelectionInfoDAO().getDTOByEconomicYear(budgetInfo.economicYear);
    boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
%>

<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    #togglePassword{
        cursor: pointer;
    }

    @media (max-width: 375px) {
        #togglePassword{
            position: relative;
            top: -28px!important;
            left: 91%!important;
        }
    }

    @media(min-width: 376px) and (max-width: 575.98px) {
        #togglePassword{
            position: relative;
            top: -28px!important;
            left: 93%!important;
        }
    }


    @media (min-width: 576px) and (max-width: 767.98px) {
        #togglePassword{
            position: relative;
            top: -28px;
            left: 93%;
        }
    }


    @media (min-width: 768px) and (max-width: 991.98px) {
        #togglePassword{
            position: relative;
            top: -28px;
            left: 93%;
        }
    }


    @media (min-width: 992px) and (max-width: 1199.98px) {
        #togglePassword{
            position: relative;
            top: -28px;
            left: 93%;
        }
    }

    @media (min-width: 1200px) and  (max-width: 1399.98px){
        #togglePassword{
            position: relative;
            top: -28px;
            left: 93%;
        }
    }

    @media (min-width: 1400px) and  (max-width: 1599.98px){
        #togglePassword{
            position: relative;
            top: -28px;
            left: 95%;
        }
    }

    @media (min-width: 1600px) and  (max-width: 1799.98px){
        #togglePassword{
            position: relative;
            top: -28px;
            left: 96%;
        }
    }


    @media (min-width: 1800px) {
        #togglePassword{
            position: relative;
            top: -28px;
            left: 97%;
        }
    }

</style>

<div class="kt-content p-0" id="kt_content">
    <div class="kt-portlet" style="background: #f9f9f9!important">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Budget_submissionServlet?actionType=checkPassword"
              id="budgetForm" name="budgetform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body m-4" style="background: #fff">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="row mb-4">
                            <div class="col-12 row">
                                <div class="offset-md-3 col-md-6 text-center">
                                    <img width="19%"
                                         src="<%=context%>assets/static/parliament_logo.png"
                                         alt="logo"
                                         class="logo-default"
                                    />
                                    <h4 class="text-center mt-3 top-section-font">
                                        <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                                    </h4>
                                    <h5 class="text-center mt-2 top-section-font">
                                        <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                                    </h5>
                                    <h4 class="m-4">
                                        <%=budgetInfo.getText(Language)%>
                                    </h4>
                                </div>
                            </div>
                        </div>

                        <%if (isCodeSelectionDone) {%>
                        <input type="hidden" name="budgetSelectionId" value="<%=budgetSelectionInfoDTO.iD%>">

                        <div class="form-group row">
                            <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="budgetOfficeId">
                                <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%><span style="color:red;">*</span>
                            </label>
                            <div class="col-md-8 col-xl-9">
                                <select id="budgetOfficeId" class='form-control rounded' name="budgetOfficeId">
                                    <%=Budget_officeRepository.getInstance().buildOptions(Language, 0L)%>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="passwordId">
                                <%=LM.getText(LC.EMPLOYEE_RECORD_PASSWORD, loginDTO)%><span style="color:red;">*</span>
                            </label>
                            <div class="col-md-8 col-xl-9">
                                <input type="password" class="form-control" name="password" id="passwordId">
                                <span
                                        class="fa fa-eye-slash"
                                        id="togglePassword"
                                ></span>
                            </div>
                        </div>
                        <%} else {%>
                        <div class="container text-danger text-center" style="font-weight: bold">
                                                <span class="h4">
                                                    <%=isLanguageEnglish ? "Budget code selection process has not yet been completed for this year!"
                                                            : "এই অর্থ বছরের বাজেটের কোড বাছাইকরণ প্রক্রিয়া এখনো সম্পন্ন হয়নি!"%>
                                                </span>
                        </div>
                        <%}%>
                    </div>
                </div>

                <%if (isCodeSelectionDone) {%>
                <div class="row mt-4">
                    <div class="col-11">
                        <div class="form-actions text-right">
                            <button id="budget-cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    type="button"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.BUDGET_ADD_BUDGET_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                    id="budget-submit-btn" onclick="formSubmit()">
                                <%=LM.getText(LC.REPORT_ADD_REPORT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
                <%}%>
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const budgetForm = $('#budgetForm');
    const budgetSubmitBtn = $('#budget-submit-btn');
    const budgetCancelBtn = $('#budget-cancel-btn');
    const budgetOfficeId = $('#budgetOfficeId');
    const passwordId = $('#passwordId');
    let contextPath2 = '<%=context.replace("../","")%>';
    if (!contextPath2.startsWith("/")) {
        contextPath2 = "/" + contextPath;
    }
    if (!contextPath2.endsWith("/")) {
        contextPath2 += "/";
    }

    function formSubmit() {
        buttonStateChange(true);
        if (!budgetOfficeId.val()) {
            showErrorMsg("অনুগ্রহ করে অফিস বাছাই করুন", "Please select office");
            buttonStateChange(false);
            return;
        }
        if (!passwordId.val()) {
            showErrorMsg("অনুগ্রহ করে পাসওয়ার্ড দিন", "Please enter password");
            buttonStateChange(false);
            return;
        }
        $.ajax({
            type: "POST",
            url: budgetForm.attr('action'),
            data: budgetForm.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    showErrorMsg(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    window.location = contextPath2 + response.payload + "&budget_session_token=" + response.msg;
                    buttonStateChange(false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }

    function buttonStateChange(value) {
        budgetSubmitBtn.prop('disabled', value);
        budgetCancelBtn.prop('disabled', value);
    }

    function showErrorMsg(msgBn, msgEn) {
        $('#toast_message').css('background-color', '#ff6063');
        showToastSticky(msgBn, msgEn);
    }
</script>

<script>
    $(document).ready(() => {
        const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#passwordId");

        togglePassword.addEventListener("click", function (e) {
            // toggle the type attribute
            const type =
                password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            // toggle the eye / eye slash icon

            if (password.getAttribute("type") === "password") {
                $("#togglePassword").removeClass("fa fa-eye");
                $("#togglePassword").addClass("fa fa-eye-slash");
            } else {
                $("#togglePassword").removeClass("fa fa-eye-slash");
                $("#togglePassword").addClass("fa fa-eye");
            }
        });
    });

</script>