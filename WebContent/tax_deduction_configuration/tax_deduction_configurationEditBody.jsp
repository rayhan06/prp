<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="tax_deduction_configuration.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="payroll_deduction_type.Payroll_deduction_typeDTO" %>
<%@ page import="payroll_deduction_type.Payroll_deduction_typeDAO" %>
<%@ page import="employee_records.EmploymentEnum" %>
<%@ page import="payroll_deduction_type.Payroll_deduction_typeRepository" %>

<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.TAX_DEDUCTION_CONFIGURATION_ADD_TAX_DEDUCTION_CONFIGURATION_ADD_FORMNAME, loginDTO);
    String servletName = "Tax_deduction_configurationServlet";
    int PRIVILEGED_EMPLOYEE_CAT = EmploymentEnum.PRIVILEGED.getValue();
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Tax_deduction_configurationServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mt-4 mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="employmentType">
                                            <%=LM.getText(LC.PAYROLL_ALLOWANCE_CONFIGURATION_ADD_EMPLOYMENTCAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class='form-control' readonly
                                                   value="<%=CatRepository.getName(Language, "employment", PRIVILEGED_EMPLOYEE_CAT)%>">
                                            <input type='hidden' name='employmentType' id="employmentType"
                                                   value='<%=PRIVILEGED_EMPLOYEE_CAT%>'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="mt-5">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_CODE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_DESCRIPTIONEN, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_DESCRIPTIONBN, loginDTO)%>
                                    </th>
                                    <th></th>
                                </tr>
                                </thead>

                                <%
                                    List<Payroll_deduction_typeDTO> payroll_deduction_typeDTOList = Payroll_deduction_typeRepository.getInstance().getPayroll_deduction_typeList();
                                    int idx = 0;
                                    for (Payroll_deduction_typeDTO payroll_deduction_typeDTO : payroll_deduction_typeDTOList) {
                                        Tax_deduction_configurationDTO taxDeductionConfigurationDTO = Tax_deduction_configurationDAO.getInstance().getDTOByPayrollId(payroll_deduction_typeDTO.iD, EmploymentEnum.PRIVILEGED.getValue());
                                        if (taxDeductionConfigurationDTO == null) {
                                            taxDeductionConfigurationDTO = new Tax_deduction_configurationDTO();
                                        }
                                %>

                                <input type='hidden' class='form-control' name='iD_<%=idx%>' id='iD_<%=idx%>'
                                       value='<%=taxDeductionConfigurationDTO.iD%>' tag='pb_html'/>
                                <input type='hidden' class='form-control' name='payrollID_<%=idx%>'
                                       id='payroll_ID_<%=idx%>'
                                       value='<%=taxDeductionConfigurationDTO.taxDeductionID%>'
                                       tag='pb_html'/>
                                <tr>
                                    <td><input type='text' class='form-control' name='code_<%=idx%>'
                                               id='code_text_<%=idx%>'
                                               value='<%=Utils.getDigits(payroll_deduction_typeDTO.code, Language)%>'
                                               tag='pb_html' readonly/></td>
                                    <td><input type='text' class='form-control' name='nameEn_<%=idx%>'
                                               id='nameEn_text_<%=idx%>'
                                               value='<%=payroll_deduction_typeDTO.descriptionEn%>'
                                               tag='pb_html' readonly/></td>
                                    <td><input type='text' class='form-control' name='nameBn_<%=idx%>'
                                               id='nameBn_text_<%=idx%>'
                                               value='<%=payroll_deduction_typeDTO.descriptionBn%>'
                                               tag='pb_html' readonly/></td>
                                    <td>
                                        <input type='checkbox' class='form-control-sm'
                                               name='isActive_<%=idx%>'
                                               id='isActive_checkbox_<%=idx%>'
                                               value='true' <%=(String.valueOf(taxDeductionConfigurationDTO.isActive).equals("true"))?("checked"):""%>
                                               tag='pb_html'>
                                    </td>
                                </tr>
                                <% idx++;
                                }
                                %>
                            </table>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-12">
                            <div class="form-actions text-right">
                                <button id="cancel-btn"
                                        class="btn-sm shadow text-white border-0 cancel-btn">
                                    <%=LM.getText(LC.TAX_DEDUCTION_CONFIGURATION_ADD_TAX_DEDUCTION_CONFIGURATION_CANCEL_BUTTON, loginDTO)%>
                                </button>
                                <button class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                        type="submit">
                                    <%=LM.getText(LC.TAX_DEDUCTION_CONFIGURATION_ADD_TAX_DEDUCTION_CONFIGURATION_SUBMIT_BUTTON, loginDTO)%>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </form>

    </div>
</div>
<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        preprocessCheckBoxBeforeSubmitting('isActive', row);

        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Tax_deduction_configurationServlet");
    }

    function init(row) {


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






