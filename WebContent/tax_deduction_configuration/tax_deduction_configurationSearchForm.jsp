<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="tax_deduction_configuration.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>


<%
    String navigator2 = "navTAX_DEDUCTION_CONFIGURATION";
    String servletName = "Tax_deduction_configurationServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PAYROLL_DEDUCTION_TYPE_ADD_CODE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TAX_DEDUCTION_CONFIGURATION_ADD_EMPLOYEMENTTYPE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TAX_DEDUCTION_CONFIGURATION_ADD_NAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TAX_DEDUCTION_CONFIGURATION_ADD_NAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TAX_DEDUCTION_CONFIGURATION_ADD_ISACTIVE, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Tax_deduction_configurationDTO>) rn2.list;
            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Tax_deduction_configurationDTO tax_deduction_configurationDTO = (Tax_deduction_configurationDTO) data.get(i);
        %>
        <tr>
            <td>
                <%=Utils.getDigits(tax_deduction_configurationDTO.code, Language)%>
            </td>
            <td>
                <%
                    value = CatRepository.getInstance().getText(Language, "employment", tax_deduction_configurationDTO.employmentType);
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(tax_deduction_configurationDTO.nameEn, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(tax_deduction_configurationDTO.nameBn, Language)%>
            </td>

            <td>
                <%=Utils.getYesNo(tax_deduction_configurationDTO.isActive, Language)%>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			