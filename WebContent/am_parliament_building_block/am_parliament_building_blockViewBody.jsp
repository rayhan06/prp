<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="am_parliament_building_block.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.*"%>
<%@ page import="am_parliament_building_level.Am_parliament_building_levelRepository" %>
<%@ page import="common.BaseServlet" %>


<%
LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
String ID = request.getParameter("ID");
if (ID == null || ID.isEmpty()) {
    ID = "0";
}
long id = Long.parseLong(ID);
Am_parliament_building_blockDTO am_parliament_building_blockDTO = (Am_parliament_building_blockDTO)
        request.getAttribute(BaseServlet.DTO_FOR_JSP);
String value;
%>




<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_AM_PARLIAMENT_BUILDING_BLOCK_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_AM_PARLIAMENT_BUILDING_BLOCK_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_PARLIAMENTBLOCKCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = CatRepository.getInstance().getText(Language, "parliament_block", am_parliament_building_blockDTO.parliamentBlockCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_AMPARLIAMENTBUILDINGLEVELTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%

											value = Am_parliament_building_levelRepository.getInstance().getText
                                                    (am_parliament_building_blockDTO.amParliamentBuildingLevelType);
											%>

				
											<%=value%>
				
			
                                    </div>
                                </div>
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>