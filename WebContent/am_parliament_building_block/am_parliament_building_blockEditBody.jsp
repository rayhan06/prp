<%@page import="am_parliament_building_block.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@page import="am_parliament_building_level.*" %>
<%@page import="login.LoginDTO" %>
<%@ page import="common.BaseServlet" %>


<%


    String servletName = "Am_parliament_building_blockServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    String actionName;
    Am_parliament_building_blockDTO am_parliament_building_blockDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        am_parliament_building_blockDTO = (Am_parliament_building_blockDTO)
                request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        am_parliament_building_blockDTO = new Am_parliament_building_blockDTO();
    }

    String formTitle = LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_AM_PARLIAMENT_BUILDING_BLOCK_ADD_FORMNAME, loginDTO);
    int i = 0;


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=am_parliament_building_blockDTO.iD%>' tag='pb_html'/>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_AMPARLIAMENTBUILDINGLEVELTYPE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='amParliamentBuildingLevelType'
                                                    id='amParliamentBuildingLevelType_select_<%=i%>' tag='pb_html'>
                                                <%
                                                    String Options = Am_parliament_building_levelRepository.getInstance().getOptions(Language, am_parliament_building_blockDTO.amParliamentBuildingLevelType);
                                                    //                                                                            CommonDAO.getOptions(Language, "am_parliament_building_level", am_parliament_building_blockDTO.amParliamentBuildingLevelType);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_PARLIAMENTBLOCKCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='parliamentBlockCat'
                                                    id='parliamentBlockCat_category_<%=i%>' tag='pb_html' onchange="isOtherSelected(this)">
                                                <%

                                                    Options = CatRepository.getInstance().buildOptions("parliament_block",
                                                            Language, am_parliament_building_blockDTO.parliamentBlockCat);

                                                    String otherText = Language.equalsIgnoreCase("english") ? "Others" : "অন্যান্য";
                                                    Options += "<option value='-2'>" + otherText + "</option>";
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row hiddenOtherField otherElement">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "অন্যান্য ব্লকের নাম ইংরেজি",
                                                    "Other Block Name English")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control'  name='otherBlockEn' id = 'otherBlockEn_<%=i%>'
                                                      tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row hiddenOtherField otherElement">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "অন্যান্য ব্লকের নাম বাংলা",
                                                    "Other Block Name Bengali")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control'  name='otherBlockBn' id = 'otherBlockBn_<%=i%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=am_parliament_building_blockDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=am_parliament_building_blockDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>


                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=am_parliament_building_blockDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=am_parliament_building_blockDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=am_parliament_building_blockDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=am_parliament_building_blockDTO.lastModificationTime%>'
                                           tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_AM_PARLIAMENT_BUILDING_BLOCK_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="submit" onclick="event.preventDefault();submitForm()">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_BLOCK_ADD_AM_PARLIAMENT_BUILDING_BLOCK_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const blockForm = $("#bigform");

    function PreprocessBeforeSubmiting() {
        blockForm.validate();
        return blockForm.valid();
    }

    let otherElements = document.querySelectorAll('.otherElement');


    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting()) {
            $.ajax({
                type: "POST",
                url: "Am_parliament_building_blockServlet?actionType=ajax_<%=actionName%>",
                data: blockForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }


    var row = 0;
    $(document).ready(function () {

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1 && value.toString().trim().length > 0;
        });

        let lang = '<%=Language%>';
        let levelErr;
        let blockErr;
        if (lang.toUpperCase() === 'ENGLISH') {
            levelErr = 'Level Required';
            blockErr = 'Block Required';

        } else {
            levelErr = 'লেভেল প্রয়োজনীয়';
            blockErr = 'ব্লক প্রয়োজনীয়';
        }

        blockForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                amParliamentBuildingLevelType: {
                    validSelector: true,
                },
                parliamentBlockCat: {
                    validSelector: true,
                },
            },
            messages: {
                amParliamentBuildingLevelType: levelErr,
                parliamentBlockCat: blockErr,
            }
        });
    });

    function isOtherSelected(selectedSelector){
       let otherVal = selectedSelector.value;
       if(otherVal === '-2' ){
           otherElements.forEach(item => {
              item.classList.remove('hiddenOtherField');
          });
       }
       else{
           otherElements.forEach(item => {
               item.classList.add('hiddenOtherField');
           });
       }
    }


</script>






