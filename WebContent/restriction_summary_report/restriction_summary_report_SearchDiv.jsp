<%@page import="common.RoleEnum"%>
<%@page import="workflow.WorkflowController"%>
<%@page import="restriction_summary.Restriction_summaryDAO"%>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_offices.*" %>
<%@ page import="pb.*" %>
<%@page pageEncoding="UTF-8"%>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RESTRICTION_SUMMARY_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		<div  class="search-criteria-div" >
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='employeeRecordId' id = 'employeeRecordId' >
						<%
						List<Long> erIds = Restriction_summaryDAO.getInstance().getAllEmployees();
						String options = "<option value = ''>" + LM.getText(LC.HM_SELECT, Language) + "</option>";
						for(Long erId: erIds)
						{
							String name = WorkflowController.getNameFromEmployeeRecordID(erId, isLangEng);
							if(!name.equalsIgnoreCase(""))
							{
								options += "<option value = '" + erId + "'>"
										+ WorkflowController.getNameFromEmployeeRecordID(erId, isLangEng) + ", "
										+ WorkflowController.getOrganogramNameFromEmployeeRecordID(erId, isLangEng) + ", "
										+ WorkflowController.getOfficeNameFromEmployeeRecordID(erId, isLangEng) + " ("
										+ WorkflowController.getUserNameFromErId(erId, isLangEng) +") </option>";
							}
						}
						%>	
						<%=options%>
					</select>				
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=isLangEng?"Restriction Category":"রেস্ট্রিকশনের ধরণ"%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='restrictionCat' id = 'restrictionCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "restriction", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=isLangEng?"Action taken By":"পদক্ষেপগ্রহীতা"%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='insertedBy' id = 'insertedBy' >
					 	<option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
                          <%
                          Set<Long> emps = EmployeeOfficeRepository.getInstance().getErIdByRole(SessionConstants.ADMIN_ROLE);
                          
                          emps.addAll(EmployeeOfficeRepository.getInstance().getByRole(RoleEnum.HR_ADMIN.getRoleId()));
                          
                          for(Long em: emps)
                          {
                        	  String name = WorkflowController.getNameFromEmployeeRecordID(em, isLangEng);
                        	  if(!name.equalsIgnoreCase(""))
                        	  {
                          %>
	                          <option value = "<%=em%>">
	                          <%=name%>
	                          </option>
                          <%
                        	  }
                          }
                          %>
                      </select>						
				</div>
			</div>
		</div>
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=isLangEng?"Is Restricted":"রেস্ট্রিক্টেড আছেন কি"%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='restrictionAdded' id = 'restrictionAdded' >
					 	<option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
					 	<option value="1"><%=isLangEng?"Yes":"হ্যাঁ"%></option>
					 	<option value="0"><%=isLangEng?"No":"না"%></option>
                      </select>						
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
    </div>
</div>
<script type="text/javascript">
var isVisible = [true, true, true, true, true, true, true, false];
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
function getLink(list)
{
	var erId = list[7];

	return "Restriction_configServlet?actionType=history&erId=" + erId;
}
</script>