<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="util.*" %>
<%@ page import="geolocation.*" %>
<%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="mp_travel_allowance.Mp_travel_allowanceDAO" %>
<%@ page import="mp_travel_allowance.Mp_travel_allowanceDTO" %>
<%@ page import="mp_travel_allowance.MpTravelDestinationDTO" %>
<%@ page import="mp_travel_allowance.MpTravelMeetingDTO" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="election_details.Election_detailsDTO" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="parliament_session.Parliament_sessionDTO" %>
<%@ page import="parliament_session.Parliament_sessionRepository" %>
<%@ page import="employee_attendance.Employee_attendanceDAO" %>
<%@ page import="employee_attendance.Employee_attendanceDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@include file="../pb/viewInitializer.jsp" %>
<%
    String ID = request.getParameter("ID");

    long id = Long.parseLong(ID);
    Mp_travel_allowanceDTO mp_travel_allowanceDTO = Mp_travel_allowanceDAO.getInstance().getDTOByID(id);
    String startAddress = mp_travel_allowanceDTO.startAddress;
    long startDate = mp_travel_allowanceDTO.startDate;
    int startTimeCat = mp_travel_allowanceDTO.startTimeCat;
    String pdfFileName = "MP_Travel_bill_form_ "
            + mp_travel_allowanceDTO.mpUserName + "_" + simpleDateFormat.format(new Date(mp_travel_allowanceDTO.startDate));
    int rowspan = mp_travel_allowanceDTO.mpTravelDestinationDTOList.size() + 3;

%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: 0.2in;
        background: white;
        margin-bottom: 10px;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > > {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    th {
        text-align: center;
    }
</style>

<div class="kt-content p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=UtilCharacter.getDataByLanguage(Language, "ভ্রমন ভাতা বিল ফর্ম", "Travel Allowance Bill Form")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body" id="bill-div">

            <div class="ml-auto m-5">
                <button type="button" class="btn" id='download-pdf'
                        onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>
            <div style="margin: auto;">
                <div class="container" id="to-print-div">
                    <section class="page shadow" data-size="A4-landscape">
                        <div class="row">
                            <div class="offset-3 col-6">
                                <h2>ভ্রমণ ভাতা বিল</h2>
                            </div>
                        </div>
                        <div>

                            <table class="table table-bordered">
                                <tr>
                                    <td><b>নির্বাচনী এলাকা
                                        নম্বরঃ <%=Utils.getDigits(Election_constituencyRepository.getInstance().getElection_constituencyDTOByID(mp_travel_allowanceDTO.electionConstituencyId).constituencyNumber, "Bangla")%>
                                    </b></td>
                                    <%
                                        Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(mp_travel_allowanceDTO.employeeRecordsId);
                                        int gender = employee_recordsDTO != null ? employee_recordsDTO.gender : 0;
                                    %>
                                    <td colspan="6"><b>
                                        নামঃ <%=gender == 2 ? "জনাবা" : "জনাব"%> <%=mp_travel_allowanceDTO.mpNameBn%>
                                        <br>পদবীঃ সদস্য, বাংলাদেশ জাতীয় সংসদ</b>
                                    </td>
                                    <td colspan="5">হিসাবের শিরোনামঃ<br>
                                        <br>
                                        যাতায়াত ভাতা-৩১১১৩০২<br>
                                        দৈনিক ভাতা-৩১১১৩০৩<br>
                                        ভ্রমণ ভাতা (বি,টি, এ)-৩১১১৩২৬<br>
                                        ভ্রমণ ব্যয়- ৩২৪৪১০১
                                    </td>
                                    <td colspan="2">
                                        <%=DateUtils.getMonthYear(mp_travel_allowanceDTO.lastModificationTime, "Bangla", ", ")%>
                                        <br>
                                        পরিশোধ তালিকা ............ এর প্রমাণক<br>
                                        নম্বর ............২০............ এর জন্য
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="6">ভ্রমন ও বিরতির বিবরণ</th>
                                    <th rowspan="4">ভ্রমণের প্রকৃতি, অর্থাৎ রেলপথে,<br> স্টীমারযোগে,
                                        বিমানযোগে<br>লঞ্চযোগে বা সড়কপথে
                                    </th>
                                    <th colspan="3" rowspan="2">আকাশ, পথ, রেল-স্টীমারের ভাড়া</th>
                                    <th rowspan="2">সড়ক পথে ভ্রমন</th>
                                    <th rowspan="4">কত দিনের জন্য দৈনিক ভাতা দাবী করা হচ্ছে</th>
                                    <th rowspan="4">ভ্রমন বা যাত্রা বিরতির উদ্দেশ্য</th>
                                    <th rowspan="4">
                                        <%
                                            Election_detailsDTO election_detailsDTO = Election_detailsRepository.getInstance().getElectionDetailsDTOByID(mp_travel_allowanceDTO.electionDetailsId);
                                        %>
                                        <%=Utils.getDigits(election_detailsDTO.parliamentNumber, "Bangla")%> তম সংসদ<br>
                                    </th>

                                </tr>
                                <tr>
                                    <th colspan="3">গমন</th>
                                    <th colspan="3">আগমন</th>
                                </tr>
                                <tr>
                                    <th rowspan="2">স্থান</th>
                                    <th rowspan="2">তারিখ</th>
                                    <th rowspan="2">পূর্বাহ্ন/অপরাহ্ন</th>
                                    <th rowspan="2">স্থান</th>
                                    <th rowspan="2">তারিখ</th>
                                    <th rowspan="2">পূর্বাহ্ন/অপরাহ্ন</th>
                                    <th rowspan="2">শ্রেণী</th>
                                    <th rowspan="2">ভাড়ার সংখ্যা</th>
                                    <th rowspan="2">পরিমাণ</th>
                                    <th>যে পরিমাণ দূরত্ব অনুমোধনযোগ্য</th>
                                </tr>
                                <tr>
                                    <th>সাধারণ হারে</th>
                                </tr>
                                <tr>
                                    <%
                                        for (i = 1; i <= 14; i++) {
                                    %>
                                    <th><%=Utils.getDigits(i, "Bangla")%>
                                    </th>
                                    <%}%>
                                </tr>
                                <%
                                    double totalCost = 0;
                                    int totalStay = 0;

                                    long diff = mp_travel_allowanceDTO.endDate - mp_travel_allowanceDTO.startDate;
                                    long totalDays = TimeUnit.MILLISECONDS.toDays(diff) + 1;

                                    double totalDist = 0;
                                    List<MpTravelDestinationDTO> mpTravelDestinationDTOList = mp_travel_allowanceDTO.mpTravelDestinationDTOList;
                                    i = 0;
                                    for (MpTravelDestinationDTO travelDestinationDTO : mpTravelDestinationDTOList) {
                                        i++;

                                %>
                                <tr>

                                    <td><%=GeoLocationUtils.getGeoLocationString(startAddress, "Bangla")%>
                                    </td>
                                    <td>
                                        <%=Utils.getDigits(simpleDateFormat.format(new Date(startDate)), "Bangla")%>
                                    </td>
                                    <td>
                                        <%=CatRepository.getInstance().getText("Bangla", "travel_time", startTimeCat)%>
                                    </td>
                                    <td><%=GeoLocationUtils.getGeoLocationString(travelDestinationDTO.destinationAddress, "Bangla")%>
                                    </td>
                                    <td><%
                                        String reachDate = simpleDateFormat.format(new Date(travelDestinationDTO.reachDate));
                                    %>
                                        <%=Utils.getDigits(reachDate, "Bangla")%>
                                    </td>
                                    <td><%=CatRepository.getInstance().getText("Bangla", "travel_time", travelDestinationDTO.timeCat)%>
                                    </td>
                                    <td>
                                        <%=CatRepository.getInstance().getText("Bangla", "travel_medium", travelDestinationDTO.travelMediumCat)%>
                                    </td>
                                    <%
                                        if (travelDestinationDTO.travelMediumCat != 1) {
                                            totalCost += travelDestinationDTO.cost;
                                    %>
                                    <td>
                                        <%=CatRepository.getInstance().getText("Bangla", "travel_type", travelDestinationDTO.travelTypeCat)%>
                                    </td>
                                    <td>
                                        ১.৫
                                    </td>
                                    <td><%=Utils.getDigits(String.format("%.2f", travelDestinationDTO.cost), "Bangla")%>
                                        /=
                                    </td>

                                    <td></td>
                                    <%} else {%>
                                    <td style="text-align: center"> -
                                    </td>
                                    <td style="text-align: center"> -
                                    </td>
                                    <td>
                                    </td>
                                    <%
                                        totalDist += travelDestinationDTO.distanceOrSubCost;
                                    %>
                                    <td><%=Utils.getDigits(String.format("%.2f", travelDestinationDTO.distanceOrSubCost), "Bangla")%>
                                        কিঃ মিঃ
                                    </td>
                                    <%}%>
                                    <%
                                        if (i == 1) {
                                    %>
                                    <td rowspan="<%=rowspan-3%>"><%=Utils.getDigits(totalDays, "Bangla")%> দিন
                                    </td>
                                    <td rowspan="<%=rowspan%>">
                                        <%
                                            if (mp_travel_allowanceDTO.isSession) {
                                                Parliament_sessionDTO parliament_sessionDTO = Parliament_sessionRepository.getInstance().getParliament_sessionDTOByID(mp_travel_allowanceDTO.parliamentSessionId);
                                        %>
                                        জাতীয়
                                        সংসদের <%=Utils.getDigits(parliament_sessionDTO.sessionNumber, "Bangla")%>
                                        নং
                                        অধিবেশনে যোগদানের উদ্দেশ্যে। <br><br>
                                        <%}%>
                                        <%
                                            if (mp_travel_allowanceDTO.isMeeting) {
                                        %>
                                        সভায় যোগদানের উদ্দেশ্যেঃ<br>
                                        <%
                                            int idx = 0;

                                            for (MpTravelMeetingDTO mpTravelMeetingDTO : mp_travel_allowanceDTO.mpTravelMeetingDTOList) {
                                                idx++;
                                        %>
                                        <%=Utils.getDigits(idx, "Bangla")%>. <%=mpTravelMeetingDTO.purpose%><br>
                                        <%
                                                }
                                            }
                                        %>
                                    </td>
                                    <td rowspan="<%=rowspan%>">
                                        <%
                                            int totalSessionAttendance;
                                            if (mp_travel_allowanceDTO.isSession) {
                                                List<Employee_attendanceDTO> employee_attendanceDTOList = new Employee_attendanceDAO().getEmployeeAttendanceInRange(mp_travel_allowanceDTO.startDate, mp_travel_allowanceDTO.endDate, mp_travel_allowanceDTO.employeeRecordsId);
                                                Parliament_sessionDTO parliament_sessionDTO = Parliament_sessionRepository.getInstance().getParliament_sessionDTOByID(mp_travel_allowanceDTO.parliamentSessionId);
                                                totalSessionAttendance = employee_attendanceDTOList.size();
                                                totalStay += totalSessionAttendance;
                                        %>
                                        <b><%=Utils.getDigits(parliament_sessionDTO.sessionNumber, "Bangla")%>
                                            নং অধিবেশনঃ</b><br>
                                        <%
                                            SimpleDateFormat attendanceDateFormat = new SimpleDateFormat("dd/MM");
                                            int idx = 0;
                                            for (Employee_attendanceDTO employee_attendanceDTO : employee_attendanceDTOList) {
                                                idx++;
                                        %>
                                        <%=Utils.getDigits(attendanceDateFormat.format(new Date(employee_attendanceDTO.inTimestamp)), "Bangla")%><% if (idx != totalSessionAttendance) {%>
                                        , <%
                                        }%>
                                        <%}%>
                                        =  <%=Utils.getDigits(totalSessionAttendance, "Bangla")%> দিন<br><br>
                                        <%
                                            }
                                            if (mp_travel_allowanceDTO.isMeeting) {
                                                List<Long> meetingDates = mp_travel_allowanceDTO.mpTravelMeetingDTOList.stream()
                                                        .map(dto -> dto.meetingDate)
                                                        .distinct()
                                                        .collect(Collectors.toList());
                                                int meetingStay = meetingDates.size();
                                                totalStay += meetingStay;
                                                ;
                                        %>
                                        <b>কমিটিঃ</b><br>
                                        <%

                                            for (Long meetingDate : meetingDates) {
                                        %>
                                        <%=Utils.getDigits(simpleDateFormat.format(new Date(meetingDate)), "Bangla")%>
                                        <br>
                                        <%
                                            }%>
                                        =  <%=Utils.getDigits(meetingStay, "Bangla")%>
                                        দিন<br><br>
                                        <%
                                            }
                                        %>

                                        <b>মোটঃ=  <%=Utils.getDigits(totalStay, "Bangla")%>
                                            দিন</b><br>
                                    </td>
                                    <%} else {%>
                                    <%}%>


                                    <%
                                            startAddress = travelDestinationDTO.destinationAddress;
                                            startDate = travelDestinationDTO.reachDate;
                                            startTimeCat = travelDestinationDTO.timeCat;
                                        }%>
                                </tr>
                                <tr>
                                    <td colspan="9">মোট</td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalCost), "Bangla")%>/=
                                    </td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalDist), "Bangla")%> কিঃ মিঃ
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9">অবস্থানঃ
                                        (<%=Utils.getDigits(simpleDateFormat.format(new Date(mp_travel_allowanceDTO.startDate)), "Bangla")%>
                                        হতে
                                        <%=Utils.getDigits(simpleDateFormat.format(new Date(mp_travel_allowanceDTO.endDate)), "Bangla")%>
                                        তারিখ পর্যন্ত) এবং প্রত্যাবর্তন (আগমনের অনুরূপ)

                                    </td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalCost), "Bangla")%>/=
                                    </td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalDist), "Bangla")%> কিঃ মিঃ
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9"><b>সর্বমোট</b></td>
                                    <td><b><%=Utils.getDigits(String.format("%.2f", totalCost * 2), "Bangla")%>/=</b>
                                    </td>
                                    <td><b><%=Utils.getDigits(String.format("%.2f", totalDist * 2), "Bangla")%> কিঃ
                                        মিঃ</b>
                                    <td>
                                        <b><%=Utils.getDigits(totalDays, "Bangla")%> দিন</b>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <ul>
                                <li>
                                    <div class="row col-12"><b>স্টিমার আহারযুক্ত ও আহার বর্জিত এই দুই প্রকারের ভাড়ার
                                        ব্যবস্থা থাকলে ভাড়া কথাটির অর্থ হবে 'আহার বর্জিত ভাড়া'।</b>
                                    </div>
                                </li>
                                <li>
                                    <div class="row col-12"><b>'সড়কপথে ভ্রমণ ' বলতে স্টীমার ব্যতীত বাষ্পচালিত লঞ্চ
                                        বা অন্য কোন যানযোগে সমুদ্রপথ বা নদীপথে ভ্রমণও অন্তর্ভুক্ত হবে (কোন মাধ্যম
                                        গ্রহণ হয়েছে, তা এই বিলে উল্লেখ করতে হবে)।</b>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </section>
                    <section class="page shadow" data-size="A4-landscape">
                        <div class="row">
                            <div class="offset-3 col-6">
                                <h2>ভ্রমণ ভাতা বিল</h2>
                            </div>
                        </div>
                        <div>
                            <%
                                double totalDistValue = totalDist * 10.0;
                                double totalCurrentAmount;
                            %>
                            <table class="table table-bordered">
                                <tr>
                                    <th rowspan="2">বিলের ধরণ</th>
                                    <th colspan="2" rowspan="2">অবস্থান(দিন)</th>
                                    <th rowspan="2">একক হার</th>
                                    <th rowspan="2">উপমোট</th>
                                    <th rowspan="2">মোট</th>
                                    <th colspan="3">কর্তন</th>
                                </tr>
                                <tr>
                                    <th>বাড়ি ভাড়া</th>
                                    <th>অন্যান্য কর্তন</th>
                                    <th>মোট কর্তন</th>
                                </tr>
                                <tr>
                                    <td rowspan="2">ভ্রমণ ভাতা</td>
                                    <td>রেল, বিমান, স্টিমার বা লঞ্চ ভাড়া(স্তম্ভ ১০)</td>
                                    <td style="text-align: center"> -</td>
                                    <td style="text-align: center"> -</td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalCost * 2), "Bangla")%>
                                    </td>
                                    <td>
                                    </td>
                                    <td rowspan="6"><%=Utils.getDigits(String.format("%.2f", 0.0), "Bangla")%>
                                    </td>
                                    <td rowspan="6"><%=Utils.getDigits(String.format("%.2f", 0.0), "Bangla")%>
                                    </td>
                                    <td rowspan="6"><%=Utils.getDigits(String.format("%.2f", 0.0), "Bangla")%>
                                    </td>
                                </tr>
                                <%
                                    int otherAmount = mp_travel_allowanceDTO.dailyAttendantAmount + mp_travel_allowanceDTO.dailyStayAmount + mp_travel_allowanceDTO.travelAttendantAmount + mp_travel_allowanceDTO.travelStayAmount;
                                %>
                                <tr>
                                    <td> কিলোমিটার ভাড়া সড়কপথে(স্তম্ভ ১১)</td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalDist * 2.0), "Bangla")%> কিঃ
                                        মিঃ
                                    </td>
                                    <td> ১০</td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalDistValue * 2.0), "Bangla")%>
                                    </td>
                                    <td><%=Utils.getDigits(String.format("%.2f", (mp_travel_allowanceDTO.netAmount - otherAmount) * 1.0), "Bangla")%>
                                    </td>

                                </tr>
                                <tr>
                                    <td rowspan="2">দৈনিক ভাতা</td>
                                    <td>উপস্থিতি</td>
                                    <td><%=Utils.getDigits(totalStay, "Bangla")%> দিন
                                    </td>

                                    <td><%=Utils.getDigits(mp_travel_allowanceDTO.dailyAttendant, "Bangla")%>
                                    </td>
                                    <%
                                        totalCurrentAmount = mp_travel_allowanceDTO.dailyAttendantAmount * 1.0;
                                    %>
                                    <td><%=Utils.getDigits(String.format("%.2f", mp_travel_allowanceDTO.dailyAttendantAmount * 1.0), "Bangla")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td> অবস্থান</td>
                                    <td><%=Utils.getDigits(totalDays - totalStay, "Bangla")%> দিন
                                    </td>

                                    <td><%=Utils.getDigits(mp_travel_allowanceDTO.dailyStay, "Bangla")%>
                                    </td>
                                    <%
                                        totalCurrentAmount += mp_travel_allowanceDTO.dailyStayAmount * 1.0;
                                    %>
                                    <td><%=Utils.getDigits(String.format("%.2f", mp_travel_allowanceDTO.dailyStayAmount * 1.0), "Bangla")%>
                                    </td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalCurrentAmount), "Bangla")%>
                                    </td>
                                </tr>


                                <tr>
                                    <td rowspan="2">যাতায়াত ভাতা</td>
                                    <td>উপস্থিতি</td>
                                    <td><%=Utils.getDigits(totalStay, "Bangla")%> দিন
                                    </td>

                                    <td><%=Utils.getDigits(mp_travel_allowanceDTO.travelAttendant, "Bangla")%>
                                    </td>
                                    <%
                                        totalCurrentAmount = mp_travel_allowanceDTO.travelAttendantAmount * 1.0;
                                    %>
                                    <td><%=Utils.getDigits(String.format("%.2f", mp_travel_allowanceDTO.travelAttendantAmount * 1.0), "Bangla")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td> অবস্থান</td>
                                    <td><%=Utils.getDigits(totalDays - totalStay, "Bangla")%> দিন
                                    </td>

                                    <td><%=Utils.getDigits(mp_travel_allowanceDTO.travelStay, "Bangla")%>
                                    </td>
                                    <%
                                        totalCurrentAmount += mp_travel_allowanceDTO.travelStayAmount * 1.0;
                                    %>
                                    <td><%=Utils.getDigits(String.format("%.2f", mp_travel_allowanceDTO.travelStayAmount * 1.0), "Bangla")%>
                                    </td>
                                    <td><%=Utils.getDigits(String.format("%.2f", totalCurrentAmount), "Bangla")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                    <td>সর্বমোট=</td>
                                    <td><%=Utils.getDigits(String.format("%.2f", mp_travel_allowanceDTO.netAmount * 1.0), "Bangla")%>
                                    </td>
                                    <td></td>
                                    <td>সর্বমোট=</td>
                                    <td><%=Utils.getDigits(String.format("%.2f", 0.00), "Bangla")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                    <td>নীট দাবি=</td>
                                    <td><%=Utils.getDigits(String.format("%.2f", mp_travel_allowanceDTO.netAmount * 1.0), "Bangla")%>
                                    </td>
                                    <td colspan="3"></td>

                                </tr>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function downloadTemplateAsPdf(divId, fileName, orientation = 'landscape') {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4', orientation: orientation}
        };
        html2pdf().from(content).set(opt).save();
    }
</script>