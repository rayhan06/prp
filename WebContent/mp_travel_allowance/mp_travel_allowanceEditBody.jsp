<%@page import="mp_travel_allowance.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="election_details.Election_detailsRepository" %>

<%
    Mp_travel_allowanceDTO mp_travel_allowanceDTO = new Mp_travel_allowanceDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        mp_travel_allowanceDTO = Mp_travel_allowanceDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    String context = request.getContextPath() + "/";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_ALLOWANCE_ADD_FORMNAME, loginDTO);
    int childTableStartingIDMeeting = 1;
    int childTableStartingIDDestination = 1;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="travelAllowanceForm" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=mp_travel_allowanceDTO.iD%>' tag='pb_html'/>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"
                                                   for="election_details_id">
                                                <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONDETAILSID, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select style='width: 100%' class='form-control'
                                                        name='election_details_id' id='election_details_id'
                                                        onchange='onElectionDetailsChanged()'>
                                                    <%=Election_detailsRepository.getInstance().buildOptions(Language, mp_travel_allowanceDTO.electionDetailsId)%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"
                                                   for="election_constituency_id">
                                                <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONCONSTITUENCYID, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='election_constituency_id'
                                                        id='election_constituency_id'
                                                        onchange='onChanged()'>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right" for="MPName">
                                                <%=isLanguageEnglish ? "Name" : "নাম"%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' id="MPName" name="MPName"
                                                       readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ISSESSION, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='checkbox' class='form-control-sm' name='isSession'
                                                       id='isSession_checkbox'
                                                       value='true'                                                                <%=(String.valueOf(mp_travel_allowanceDTO.isSession).equals("true"))?("checked"):""%>
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="session_div">
                                            <label class="col-md-4 col-form-label text-md-right"
                                                   for="parliament_session">
                                                <%=isLanguageEnglish ? "Parliament Session" : "সংসদ অধিবেশন"%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select style='width: 100%' class='form-control'
                                                        name='parliament_session' id='parliament_session'
                                                >
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=isLanguageEnglish ? "For Meeting" : "মিটিং এর উদ্দেশ্যে"%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='checkbox' class='form-control-sm' name='isMeeting'
                                                       id='isMeeting_checkbox'
                                                       value='true'  <%=(String.valueOf(mp_travel_allowanceDTO.isMeeting).equals("true"))?("checked"):""%>
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTDATE, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <%value = "startDate_js";%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='startDate' id='startDate_date'
                                                       value='<%=dateFormat.format(new Date(mp_travel_allowanceDTO.startDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTTIMECAT, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='startTimeCat'
                                                        id='startTimeCat_category' tag='pb_html'>
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptions("travel_time", Language, mp_travel_allowanceDTO.startTimeCat);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ENDDATE, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <%value = "endDate_js";%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='endDate' id='endDate_date'
                                                       value='<%=dateFormat.format(new Date(mp_travel_allowanceDTO.endDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ENDTIMECAT, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='endTimeCat'
                                                        id='endTimeCat_category' tag='pb_html'>
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptions("travel_time", Language, mp_travel_allowanceDTO.endTimeCat);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTADDRESS, loginDTO)%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <jsp:include page="/geolocation/geoLocation.jsp">
                                                    <jsp:param name="GEOLOCATION_ID"
                                                               value="startAddress_js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type="hidden" id="startAddress"
                                                       name="startAddress">
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5" id="meeting_div">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_MEETING, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_MEETING_MEETINGDATE, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_MEETING_PURPOSE, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_MEETING_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-MpTravelMeeting">


                                <%
                                    if (actionName.equals("ajax_edit")) {
                                        int index = -1;


                                        for (MpTravelMeetingDTO mpTravelMeetingDTO : mp_travel_allowanceDTO.mpTravelMeetingDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="MpTravelMeeting_<%=index + 1%>">

                                    <td>


                                        <%value = "meetingDate_js_" + childTableStartingIDMeeting;%>
                                        <jsp:include page="/date/date.jsp">
                                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type='hidden' name='mpTravelMeeting.meetingDate'
                                               id='meetingDate_date_<%=childTableStartingIDMeeting%>'
                                               value='<%=mpTravelMeetingDTO.meetingDate%>'
                                               tag='pb_html'>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control' name='mpTravelMeeting.purpose'
                                               id='purpose_text_<%=childTableStartingIDMeeting%>'
                                               value='<%=mpTravelMeetingDTO.purpose%>' tag='pb_html'/>
                                    </td>
                                    <td>
										<span id='meeting_chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='mpTravelMeeting.iD'
                                               id='meeting_iD_hidden_<%=childTableStartingIDMeeting%>'
                                               value='<%=mpTravelMeetingDTO.iD%>' tag='pb_html'/>

                                    </td>
                                </tr>
                                <%
                                            childTableStartingIDMeeting++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="text-right">
                                <button
                                        id="add-more-MpTravelMeeting"
                                        name="add-moreMpTravelMeeting"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-MpTravelMeeting"
                                        name="removeMpTravelMeeting"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%MpTravelMeetingDTO mpTravelMeetingDTO = new MpTravelMeetingDTO();%>

                        <template id="template-MpTravelMeeting">
                            <tr>

                                <td>

                                    <%value = "meetingDate_js_";%>
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' name='mpTravelMeeting.meetingDate' id='meetingDate_date_'
                                           value='<%=mpTravelMeetingDTO.meetingDate%>'
                                           tag='pb_html'>
                                </td>
                                <td>


                                    <input type='text' class='form-control' name='mpTravelMeeting.purpose'
                                           id='purpose_text_' value='<%=mpTravelMeetingDTO.purpose%>' tag='pb_html'/>
                                </td>
                                <td>
											<span id='meeting_chkEdit_'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='mpTravelMeeting.iD'
                                           id='meeting_iD_hidden_'
                                           value='<%=mpTravelMeetingDTO.iD%>' tag='pb_html'/>

                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="mt-5">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap" id="travel-destination-table">
                                <thead>
                                <tr>
                                    <th><%=isLanguageEnglish ? "Arrival Date" : "পৌছানোর তারিখ"%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_ENDTIMECAT, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_DESTINATIONADDRESS, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_TRAVELMEDIUMCAT, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_TRAVELTYPECAT, loginDTO)%>
                                        <span
                                                class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION_DISTANCEORSUBCOST, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_COST, loginDTO)%><span
                                            class="required">*</span>
                                    </th>
                                    <th><%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_DESTINATION_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-MpTravelDestination">


                                <%
                                    if (actionName.equals("ajax_edit")) {
                                        int index = -1;


                                        for (MpTravelDestinationDTO mpTravelDestinationDTO : mp_travel_allowanceDTO.mpTravelDestinationDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="MpTravelDestination_<%=index + 1%>">

                                    <td>
                                        <%
                                            value = "date_js_" + childTableStartingIDDestination;
                                        %>
                                        <jsp:include page="/date/date.jsp">
                                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type='hidden' name='mpTravelDestination.reachDate'
                                               id='date_date_<%=childTableStartingIDDestination%>'
                                               value='<%=mpTravelDestinationDTO.reachDate%>'
                                               tag='pb_html'>
                                    </td>
                                    <td>


                                        <select class='form-control' name='mpTravelDestination.timeCat'
                                                id='timeCat_category_<%=childTableStartingIDDestination%>'
                                                tag='pb_html'>
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("travel_time", Language, mpTravelDestinationDTO.timeCat);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>
                                            <%
                                            value = "destinationAddress_geolocation_js_" + childTableStartingIDDestination;
                                        %>
                                        <jsp:include page="/geolocation/geoLocation.jsp">
                                            <jsp:param name="GEOLOCATION_ID"
                                                       value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE"
                                                       value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type="hidden"
                                               id="destinationAddress_geolocation_<%=childTableStartingIDDestination%>"
                                               name="mpTravelDestination.destinationAddress" tag='pb_html'>
                                    <td>


                                        <select class='form-control' name='mpTravelDestination.travelMediumCat'
                                                id='travelMediumCat_category_<%=childTableStartingIDDestination%>'
                                                tag='pb_html'>
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("travel_medium", Language, mpTravelDestinationDTO.travelMediumCat);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>
                                        <select class='form-control' name='mpTravelDestination.travelTypeCat'
                                                id='travelTypeCat_category_<%=childTableStartingIDDestination%>'
                                                tag='pb_html'>
                                            <%
                                                Options = CatRepository.getInstance().buildOptions("travel_type", Language, mpTravelDestinationDTO.travelTypeCat);
                                            %>
                                            <%=Options%>
                                        </select>

                                    </td>
                                    <td>
                                        <input type='text' class='form-control'
                                               name='mpTravelDestination.distanceOrSubCost'
                                               data-only-number="true"
                                               id='distanceOrSubCost_number_<%=childTableStartingIDDestination%>'
                                               value='<%=String.format("%.2f", mpTravelDestinationDTO.distanceOrSubCost)%>'
                                               tag='pb_html'>
                                        <div id="unit_<%=childTableStartingIDDestination%>" tag='pb_html'></div>
                                    </td>
                                    <td>
                                        <div id="cost_<%=childTableStartingIDDestination%>"
                                             tag='pb_html'><%=String.format("%.2f", mpTravelDestinationDTO.cost)%>
                                        </div>
                                    </td>
                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   id="field-MpTravelDestination_cb_<%=index%>"
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               name='mpTravelDestination.iD'
                                               id='iD_hidden_<%=childTableStartingIDDestination%>'
                                               value='<%=mpTravelDestinationDTO.iD%>'
                                               tag='pb_html'/>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingIDDestination++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="text-right">
                                <button
                                        id="add-more-MpTravelDestination"
                                        name="add-moreMpTravelDestination"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-MpTravelDestination"
                                        name="removeMpTravelDestination"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                        <%MpTravelDestinationDTO mpTravelDestinationDTO = new MpTravelDestinationDTO();%>
                        <template id="template-MpTravelDestination">
                            <tr>
                                <td>
                                    <%
                                        value = "date_js_";
                                    %>
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' name='mpTravelDestination.reachDate' id='date_date_'
                                           value='<%=mpTravelDestinationDTO.reachDate%>'
                                           tag='pb_html'>
                                </td>
                                <td>


                                    <select class='form-control' name='mpTravelDestination.timeCat'
                                            id='timeCat_category_' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("travel_time", Language, mpTravelDestinationDTO.timeCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>
                                    <jsp:include page="/geolocation/geoLocation.jsp">
                                        <jsp:param name="GEOLOCATION_ID"
                                                   value="destinationAddress_geolocation_js_"></jsp:param>
                                        <jsp:param name="LANGUAGE"
                                                   value="<%=Language%>"></jsp:param>
                                    </jsp:include>
                                    <input type="hidden" id="destinationAddress_geolocation_"
                                           name="mpTravelDestination.destinationAddress" tag='pb_html'>
                                </td>
                                <td>


                                    <select class='form-control' name='mpTravelDestination.travelMediumCat'
                                            id='travelMediumCat_category_' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("travel_medium", Language, mpTravelDestinationDTO.travelMediumCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>


                                    <select class='form-control' name='mpTravelDestination.travelTypeCat'
                                            id='travelTypeCat_category_' tag='pb_html'>
                                        <%
                                            Options = CatRepository.getInstance().buildOptions("travel_type", Language, mpTravelDestinationDTO.travelTypeCat);
                                        %>
                                        <%=Options%>
                                    </select>

                                </td>
                                <td>
                                    <input type='text' class='form-control' name='mpTravelDestination.distanceOrSubCost'
                                           data-only-number="true"
                                           id='distanceOrSubCost_number_'
                                           value='<%=mpTravelDestinationDTO.distanceOrSubCost%>' tag='pb_html'>
                                    <div id="unit_" tag='pb_html'></div>
                                </td>
                                <td>
                                    <div id="cost_" tag='pb_html'><%=mpTravelDestinationDTO.cost%>
                                    </div>
                                </td>
                                <td>
											<span id='chkEdit_'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>

                                <td style="display: none;">
                                    <input type='hidden' class='form-control' name='mpTravelDestination.iD'
                                           id='iD_hidden_'
                                           value='-1' tag='pb_html'/>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-center mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_ALLOWANCE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="submitTravelAllowanceForm()">
                        <%=LM.getText(LC.TRAVEL_ALLOWANCE_ADD_TRAVEL_ALLOWANCE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<%@include file="../common/table-sum-utils.jsp" %>
<script type="text/javascript">

    const travelAllowanceForm = $('#travelAllowanceForm');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    function isFromValid() {


        const jQueryValid = travelAllowanceForm.valid();
        const fromDateValid = dateValidator('startDate_js', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        });
        const toDateValid = dateValidator('endDate_js', true, {
            'errorEn': '<%=LM.getInstance().getText("English", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>',
            'errorBn': '<%=LM.getInstance().getText("Bangla", LC.EMPLOYEE_ACR_ADD_ENTER_VALID_DATE)%>'
        });
        const startAddressValid = geoLocationValidator('startAddress_js');
        return jQueryValid && fromDateValid && toDateValid && startAddressValid;
    }

    function submitTravelAllowanceForm() {
        document.getElementById("startDate_date").value = getDateTimestampById('startDate_js');
        document.getElementById("endDate_date").value = getDateTimestampById('endDate_js');
        $('#startAddress').val(getGeoLocation('startAddress_js'));
        for (i = 1; i < child_table_extra_id_destination; i++) {
            if (document.getElementById("date_js_" + i)) {
                document.getElementById("date_date_" + i).value = getDateTimestampById('date_js_' + i);
            }
            if (document.getElementById("destinationAddress_geolocation_js_" + i)) {
                //preprocessGeolocationBeforeSubmitting('destinationAddress', i, false);
                $('#destinationAddress_geolocation_' + i).val(getGeoLocation('destinationAddress_geolocation_js_' + i));
            }

        }


        let msg = null;

        setButtonState(true);
        let isSession = document.getElementById("isSession_checkbox");
        let isMeeting = document.getElementById("isMeeting_checkbox");
        if (isMeeting.checked === false && isSession.checked === false) {
            msg = isLangEng ? "Purpose for travel is missing" : "ভ্রমনের উদ্দেশ্য পাওয়া যায়নি";
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(msg, msg);
            setButtonState(false);
            return;
        }

        if (isMeeting.checked === true) {
            for (i = 1; i < child_table_extra_id_meeting; i++) {
                if (document.getElementById("meetingDate_js_" + i)) {
                    document.getElementById("meetingDate_date_" + i).value = getDateTimestampById('meetingDate_js_' + i);
                }
            }
        }

        let params = travelAllowanceForm.serialize().split('&');
        //console.log(travelAllowanceForm)

        for (let i = 0; i < params.length; i++) {
            let param = params[i].split('=');
            switch (param[0]) {
                case 'startTimeCat':
                    if (!param[1]) {
                        msg = isLangEng ? "Start time category is not selected" : "ভ্রমণ শুরুর ক্যাটাগরি বাছাই করা হয় নি";
                    }
                    break;
                case 'endTimeCat':
                    if (!param[1]) {
                        msg = isLangEng ? "End time category is not selected" : "ভ্রমণ শেষের ক্যাটাগরি বাছাই করা হয় নি";
                    }
                    break;
                case 'mpTravelDestination.travelTypeCat':
                    if (!param[1]) {
                        msg = isLangEng ? "Travel type category is not selected" : "ভ্রমণ টাইপ ক্যাটাগরি বাছাই করা হয় নি";
                    }
                    break;

                case 'mpTravelDestination.travelMediumCat':
                    if (!param[1]) {
                        msg = isLangEng ? "Travel medium category is not selected" : "ভ্রমণ মাধ্যম ক্যাটাগরি বাছাই করা হয় নি";
                    }
                    break;
                case 'mpTravelDestination.distanceOrSubCost':
                    if (!param[1]) {
                        msg = isLangEng ? "Travel cost is missing" : "ভ্রমণের খরচ পাওয়া যায়নি";
                    }
                    break;

            }
            if (msg) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(msg, msg);
                setButtonState(false);
                return;
            }
        }


        if (isMeeting.checked === true && child_table_extra_id_meeting == 1) {
            msg = isLangEng ? "Meeting details are missing" : "মিটিং এর বিস্তারিত পাওয়া যায়নি";
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(msg, msg);
            setButtonState(false);
            return;
        } else if (isMeeting.checked === true && child_table_extra_id_meeting > 1) {
            for (let i = 0; i < params.length; i++) {
                let param = params[i].split('=');
                switch (param[0]) {
                    case 'mpTravelMeeting.purpose':
                        if (!param[1]) {
                            msg = isLangEng ? "Meeting purpose is not found" : "মিটিং এর উদ্দেশ্য পাওয়া যায় নি";
                        }
                        break;
                }
                if (msg) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToast(msg, msg);
                    setButtonState(false);
                    return;
                }
            }
        }
        // let tablename = 'field-MpTravelDestination';
        // let rowNumber = document.getElementById(tablename).childNodes.length;
        // if (rowNumber <= 3) {
        //     msg = isLangEng ? "Travel destination is missing" : "ভ্রমণের গন্তব্য পাওয়া যায়নি";
        //     $('#toast_message').css('background-color', '#ff6063');
        //     showToastSticky(msg, msg);
        //     setButtonState(false);
        //     return;
        // }
        if (isFromValid()) {
            $.ajax({
                type: "POST",
                url: "Mp_travel_allowanceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>",
                data: travelAllowanceForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToast(response.msg, response.msg);
                        setButtonState(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    setButtonState(false);
                }
            });
        } else {
            setButtonState(false);
        }

    }

    function updateDescriptionLen() {
        $('#purpose_len').text($('#purpose_text').val().length + "/1024");
    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Travel_allowanceServlet");
    }

    function setButtonState(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function keyDownEvent(e) {
        let isvalid = inputValidationForFloatValue(e, $(this), 2, null);
        return true == isvalid;
    }

    function init() {
        select2SingleSelector('#election_details_id', '<%=Language%>');
        select2SingleSelector('#election_constituency_id', '<%=Language%>');
        select2SingleSelector('#parliament_session', '<%=Language%>');
        setDateByStringAndId('startDate_js', $('#startDate_date').val());
        setDateByStringAndId('endDate_js', $('#endDate_date').val());
        setMinDateById("endDate_js", getDateStringById("startDate_js"));
        setGeoLocation('<%=mp_travel_allowanceDTO.startAddress%>', 'startAddress_js');
        showOrHideSessionDiv();
        showOrHideMeetingDiv();
        <%
    if(actionName.equals("ajax_edit")) {
       %>
        onFirstElectionDetailsChanged();
        <%}else{%>
        addDestinationRow();
        <%}%>
        document.getElementById("field-MpTravelDestination_cb_0").disabled = true;
        document.getElementById("field-MpTravelDestination_cb_0").style = "display:none";
        $('#isSession_checkbox').change(function () {
            showOrHideSessionDiv();
        });
        $('#isMeeting_checkbox').change(function () {
            showOrHideMeetingDiv();
        });
        $("#startDate_js").on('datepicker.change', () => {

            setMinDateById("endDate_js", getDateStringById("startDate_js"));

            for (i = 1; i < child_table_extra_id_meeting; i++) {
                setMinDateById('meetingDate_js_' + i, getDateStringById("startDate_js"));
            }
            for (i = 1; i < child_table_extra_id_destination; i++) {
                setMinDateById('date_js_' + i, getDateStringById("startDate_js"));
            }
        });
        $("#endDate_js").on('datepicker.change', (event, param) => {
            setMaxDateById("startDate_js", getDateStringById("endDate_js"));
            for (i = 1; i < child_table_extra_id_meeting; i++) {
                setMaxDateById('meetingDate_js_' + i, getDateStringById("endDate_js"));
            }
            for (i = 1; i < child_table_extra_id_destination; i++) {
                setMaxDateById('date_js_' + i, getDateStringById("endDate_js"));
            }
        });
        initTable();
        <%
        if(mp_travel_allowanceDTO.mpTravelMeetingDTOList.size()>0) {
        %>

        let travelMedium = '';
        <%}
            int idx = 1;
            for(MpTravelDestinationDTO dto:mp_travel_allowanceDTO.mpTravelDestinationDTOList){
        %>
        travelMedium = document.getElementById('travelMediumCat_category_<%=idx%>').value;
        if (travelMedium == 1) {
            if (isLangEng) {
                document.getElementById('unit_<%=idx%>').innerHTML = '<b>Kilometer</b>'
            } else {
                document.getElementById('unit_<%=idx%>').innerHTML = '<b>কিলোমিটার</b>'
            }
        } else {
            if (isLangEng) {
                document.getElementById('unit_<%=idx%>').innerHTML = '<b>Taka</b>'
            } else {
                document.getElementById('unit_<%=idx%>').innerHTML = '<b>টাকা</b>'
            }
        }
        document.getElementById('travelMediumCat_category_<%=idx%>').addEventListener('change', function () {
            const rowId = this.id.split("_")[2];
            let distOrSubCost = document.getElementById('distanceOrSubCost_number_' + rowId).value;
            if (distOrSubCost === '') {
                return
            }
            if (this.value == 1) {
                document.getElementById('cost_' + rowId).innerText = (Number(distOrSubCost) * 10.0).toFixed(2);
                if (isLangEng) {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>Kilometer</b>'
                } else {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>কিলোমিটার</b>'
                }
            } else {
                document.getElementById('cost_' + rowId).innerText = (Number(distOrSubCost) * 1.5).toFixed(2);
                if (isLangEng) {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>Taka</b>'
                } else {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>টাকা</b>'
                }
            }
            showSumOfColumnValues('travel-destination-table', 6, null, 2);
        });

        setGeoLocation('<%=dto.destinationAddress%>', 'destinationAddress_geolocation_js_<%=idx%>');
        // initGeoLocation('destinationAddress_geoSelectField_', i, "Travel_allowanceServlet");
        setDateByTimestampAndId('date_js_<%=idx%>', $('#date_date_<%=idx%>').val());
        setMinDateById('date_js_<%=idx%>', getDateStringById("startDate_js"));
        setMaxDateById('date_js_<%=idx%>', getDateStringById("endDate_js"));
        $('input#distanceOrSubCost_number_<%=idx%>').on('keyup', function () {
            const rowId = this.id.split("_")[2];
            const travelMedium = document.getElementById('travelMediumCat_category_' + rowId).value;
            console.log(travelMedium)
            if (travelMedium === '') {
                return;
            }
            if (travelMedium == 1) {
                document.getElementById('cost_' + rowId).innerText = (Number(this.value) * 10.0).toFixed(2);
                if (isLangEng) {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>Kilometer</b>';
                } else {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>কিলোমিটার</b>';

                }
            } else {
                document.getElementById('cost_' + rowId).innerText = (Number(this.value) * 1.5).toFixed(2);
                if (isLangEng) {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>Taka</b>';
                } else {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>টাকা</b>';
                }
            }
            showSumOfColumnValues('travel-destination-table', 6, null, 2);
        });

        <%
            idx++;
        }
            idx = 1;
            for(MpTravelMeetingDTO dto:mp_travel_allowanceDTO.mpTravelMeetingDTOList){
        %>
        setDateByTimestampAndId('meetingDate_js_<%=idx%>', $('#meetingDate_date_<%=idx%>').val());
        setMinDateById('meetingDate_js_<%=idx%>', getDateStringById("startDate_js"));
        setMaxDateById('meetingDate_js_<%=idx%>', getDateStringById("endDate_js"));

        <%
        idx++;
        }%>
    }

    function initTable() {
        const colIndicesToSum = [6];
        const totalTitleColSpan = 6;
        const totalTitle = '<%=LM.getText(LC.BUDGET_SUBTOTAL,loginDTO)%>';
        setupTotalRow('travel-destination-table', totalTitle, totalTitleColSpan, colIndicesToSum, 1, null, 2);
    }

    function initNumberInput() {
        document.querySelectorAll('[data-only-number="true"]')
            .forEach(inputField => inputField.onkeydown = keyDownEvent);
    }

    async function onChanged() {
        let election_details_id = $("#election_details_id").val();
        let election_constituency_id = $("#election_constituency_id").val();
        if (election_details_id !== -1 && election_details_id !== "" && election_constituency_id !== -1 && election_constituency_id !== "") {
            const url = 'Mp_travel_allowanceServlet?actionType=getMPInfo&election_details_id=' + election_details_id
                + '&election_constituency_id=' + election_constituency_id;

            const response = await fetch(url);
            const {employeeName} = await response.json();
            document.getElementById('MPName').value = employeeName;
        }

    }

    async function onFirstElectionDetailsChanged() {


        const url = 'Mp_travel_allowanceServlet?actionType=getEditPageRecord&election_details_id=' + <%=mp_travel_allowanceDTO.electionDetailsId%>
            +'&election_constituency_id=' + <%=mp_travel_allowanceDTO.electionConstituencyId%>+"&parliamentSessionId=" +<%=mp_travel_allowanceDTO.parliamentSessionId%>;

        const response = await fetch(url);
        const {getElectionConstituency, employeeName, getParliamentSession} = await response.json();
        document.getElementById('election_constituency_id').innerHTML = getElectionConstituency;
        document.getElementById('MPName').value = employeeName;
        document.getElementById('parliament_session').innerHTML = getParliamentSession;

    }

    async function onElectionDetailsChanged() {
        let election_details_id = $("#election_details_id").val();

        if (election_details_id !== -1 && election_details_id !== "") {
            const url = 'Mp_travel_allowanceServlet?actionType=getElectionConstituency&election_details_id=' + election_details_id;
            const response = await fetch(url);
            document.getElementById('election_constituency_id').innerHTML = await response.text();
            document.getElementById('MPName').value = '';
            showOrHideSessionDiv();
        }

    }

    async function showOrHideSessionDiv() {
        let isSession = document.getElementById("isSession_checkbox");
        console.log("here", isSession.checked)
        if (isSession.checked === true) {
            let election_details_id = $("#election_details_id").val();
            let url = 'Mp_travel_allowanceServlet?actionType=getParliamentSession&election_details_id=' + election_details_id;
            if (election_details_id !== -1 && election_details_id !== "") {
                <%
                    if(actionName.equals("ajax_edit")) {
                %>
                url += "&parliamentSessionId=" +<%=mp_travel_allowanceDTO.parliamentSessionId%>;
                <%}%>


                const response = await fetch(url);
                document.getElementById('parliament_session').innerHTML = await response.text();
                $('#session_div').show();
            }

        } else {
            $('#session_div').hide();
        }

    }

    function showOrHideMeetingDiv() {
        let isMeeting = document.getElementById("isMeeting_checkbox");
        if (isMeeting.checked === true) {
            $('#meeting_div').show();
            if (child_table_extra_id_meeting === 1) {
                addMeetingRow();
            }
        } else {
            $('#meeting_div').hide();
        }
    }

    var row = 0;
    $(document).ready(function () {

        init();
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id_meeting = <%=childTableStartingIDMeeting%>;
    var child_table_extra_id_destination = <%=childTableStartingIDDestination%>;

    function addMeetingRow() {
        var t = $("#template-MpTravelMeeting");

        $("#field-MpTravelMeeting").append(t.html());
        SetCheckBoxValues("field-MpTravelMeeting");

        var tr = $("#field-MpTravelMeeting").find("tr:last-child");

        tr.attr("id", "MpTravelMeeting_" + child_table_extra_id_meeting);

        tr.find("[tag='pb_html']").each(function (index) {
            var prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_extra_id_meeting);
            console.log(index + ": " + $(this).attr('id'));
        });

        setMinDateById('meetingDate_js_' + child_table_extra_id_meeting, getDateStringById("startDate_js"));
        setMaxDateById('meetingDate_js_' + child_table_extra_id_meeting, getDateStringById("endDate_js"));
        select2SingleSelector('#daySelectionmeetingDate_js_' + child_table_extra_id_meeting, '<%=Language%>');
        select2SingleSelector('#monthSelectionmeetingDate_js_' + child_table_extra_id_meeting, '<%=Language%>');
        select2SingleSelector('#yearSelectionmeetingDate_js_' + child_table_extra_id_meeting, '<%=Language%>');
        child_table_extra_id_meeting++;
    }

    $("#add-more-MpTravelMeeting").click(
        function (e) {
            e.preventDefault();
            addMeetingRow();

        });


    $("#remove-MpTravelMeeting").click(function (e) {
        var tablename = 'field-MpTravelMeeting';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked === true) {
                    tr.remove();
                }
                j++;
            }

        }
    });

    function addDestinationRow() {
        var t = $("#template-MpTravelDestination");

        $("#field-MpTravelDestination").append(t.html());
        SetCheckBoxValues("field-MpTravelDestination");

        var tr = $("#field-MpTravelDestination").find("tr:last-child");

        tr.attr("id", "MpTravelDestination_" + child_table_extra_id_destination);

        tr.find("[tag='pb_html']").each(function (index) {
            var prev_id = $(this).attr('id');
            $(this).attr('id', prev_id + child_table_extra_id_destination);
            //$(this).attr('name', prev_id + child_table_extra_id);
            console.log(index + ": " + $(this).attr('id'));
        });

        $('input#distanceOrSubCost_number_' + child_table_extra_id_destination).on('keyup', function () {
            const rowId = this.id.split("_")[2];
            document.getElementById('cost_' + rowId).innerText = (Number(this.value) * 2.0).toFixed(2)
            // showSumOfColumnValues('travel-destination-table', 6);
        });
        document.getElementById('travelMediumCat_category_' + child_table_extra_id_destination).addEventListener('change', function () {

            const rowId = this.id.split("_")[2];
            let distOrSubCost = document.getElementById('distanceOrSubCost_number_' + rowId).value;
            if (distOrSubCost === '') {
                return
            }
            console.log(this.value, rowId, isLangEng, 1);
            if (this.value == 1) {
                console.log(this.value, rowId, isLangEng, 2);
                document.getElementById('cost_' + rowId).innerText = (Number(distOrSubCost) * 10.0).toFixed(2)
                if (isLangEng) {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>Kilometer</b>'
                } else {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>কিলোমিটার</b>'
                }
            } else {
                document.getElementById('cost_' + rowId).innerText = (Number(distOrSubCost) * 1.5).toFixed(2)
                if (isLangEng) {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>Taka</b>'
                } else {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>টাকা</b>'
                }
            }
            showSumOfColumnValues('travel-destination-table', 6, null, 2);
        });
        $('input#distanceOrSubCost_number_' + child_table_extra_id_destination).on('keyup', function () {
            const rowId = this.id.split("_")[2];
            const travelMedium = document.getElementById('travelMediumCat_category_' + rowId).value;
            console.log(travelMedium)
            if (travelMedium === '') {
                return;
            }
            if (travelMedium == 1) {
                document.getElementById('cost_' + rowId).innerText = (Number(this.value) * 10.0).toFixed(2)
                if (isLangEng) {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>Kilometer</b>'

                } else {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>কিলোমিটার</b>'
                }
            } else {
                document.getElementById('cost_' + rowId).innerText = (Number(this.value) * 1.5).toFixed(2)
                if (isLangEng) {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>Taka</b>'
                } else {
                    document.getElementById('unit_' + rowId).innerHTML = '<b>টাকা</b>'
                }
            }
            showSumOfColumnValues('travel-destination-table', 6, null, 2);
        });


        setMinDateById('date_js_' + child_table_extra_id_destination, getDateStringById("startDate_js"));
        setMaxDateById('date_js_' + child_table_extra_id_destination, getDateStringById("endDate_js"));
        select2SingleSelector('#daySelection' + 'date_js_' + child_table_extra_id_destination, '<%=Language%>');
        select2SingleSelector('#monthSelection' + 'date_js_' + child_table_extra_id_destination, '<%=Language%>');
        select2SingleSelector('#yearSelection' + 'date_js_' + child_table_extra_id_destination, '<%=Language%>');
        if (child_table_extra_id_destination == 1) {
            initTable()
        }

        child_table_extra_id_destination++;

        initNumberInput();
    }

    $("#add-more-MpTravelDestination").click(
        function (e) {
            e.preventDefault();
            addDestinationRow();

        });


    $("#remove-MpTravelDestination").click(function (e) {
        var tablename = 'field-MpTravelDestination';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked === true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


</script>






