<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="mp_travel_allowance.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>

<%@ page import="geolocation.*" %>


<%
    String servletName = "Mp_travel_allowanceServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Mp_travel_allowanceDTO mp_travel_allowanceDTO = Mp_travel_allowanceDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = mp_travel_allowanceDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_ALLOWANCE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_ALLOWANCE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ISSESSION, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.isSession + "";
                                        %>

                                        <%=Utils.getYesNo(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=isLanguageEnglish ? "For Meeting" : "মিটিং এর উদ্দেশ্যে"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.isMeeting + "";
                                        %>

                                        <%=Utils.getYesNo(value, Language)%>


                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.startDate + "";
                                        %>
                                        <%
                                            String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_startDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTTIMECAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.startTimeCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "travel_time", mp_travel_allowanceDTO.startTimeCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ENDDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.endDate + "";
                                        %>
                                        <%
                                            String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_endDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ENDTIMECAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.endTimeCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "travel_time", mp_travel_allowanceDTO.endTimeCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTADDRESS, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.startAddress + "";
                                        %>
                                        <%=GeoLocationUtils.getGeoLocationString(value, Language)%>


                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MPNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.mpNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MPNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.mpNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Election Constituency En
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.electionConstituencyEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>


                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ELECTIONCONSTITUENCYBN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = mp_travel_allowanceDTO.electionConstituencyBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-5">
                <div class=" div_border attachement-div">
                    <h5 class="table-title"><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_MEETING, loginDTO)%>
                    </h5>
                    <table class="table table-bordered table-striped">
                        <tr>

                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_MEETING_MEETINGDATE, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_MEETING_PURPOSE, loginDTO)%>
                            </th>
                        </tr>
                        <%
                            MpTravelMeetingDAO mpTravelMeetingDAO = MpTravelMeetingDAO.getInstance();
                            List<MpTravelMeetingDTO> mpTravelMeetingDTOs = (List<MpTravelMeetingDTO>) mpTravelMeetingDAO.getDTOsByParent("mp_travel_allowance_id", mp_travel_allowanceDTO.iD);

                            for (MpTravelMeetingDTO mpTravelMeetingDTO : mpTravelMeetingDTOs) {
                        %>
                        <tr>

                            <td>
                                <%
                                    value = mpTravelMeetingDTO.meetingDate + "";
                                %>
                                <%
                                    String formatted_meetingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_meetingDate, Language)%>


                            </td>
                            <td>
                                <%
                                    value = mpTravelMeetingDTO.purpose + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>
                        <%

                            }

                        %>
                    </table>
                </div>
            </div>
            <div class="mt-5">
                <div class=" div_border attachement-div">
                    <h5 class="table-title"><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION, loginDTO)%>
                    </h5>
                    <table class="table table-bordered table-striped">
                        <tr>

                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION_DESTINATIONADDRESS, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION_TRAVELMEDIUMCAT, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION_TRAVELTYPECAT, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION_COST, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION_REACHDATE, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION_DISTANCEORSUBCOST, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MP_TRAVEL_DESTINATION_TIMECAT, loginDTO)%>
                            </th>
                        </tr>
                        <%
                            MpTravelDestinationDAO mpTravelDestinationDAO = MpTravelDestinationDAO.getInstance();
                            List<MpTravelDestinationDTO> mpTravelDestinationDTOs = (List<MpTravelDestinationDTO>) mpTravelDestinationDAO.getDTOsByParent("mp_travel_allowance_id", mp_travel_allowanceDTO.iD);

                            for (MpTravelDestinationDTO mpTravelDestinationDTO : mpTravelDestinationDTOs) {
                        %>
                        <tr>

                            <td>
                                <%
                                    value = mpTravelDestinationDTO.destinationAddress + "";
                                %>
                                <%=GeoLocationUtils.getGeoLocationString(value, Language)%>


                            </td>
                            <td>
                                <%
                                    value = mpTravelDestinationDTO.travelMediumCat + "";
                                %>
                                <%
                                    value = CatRepository.getInstance().getText(Language, "travel_medium", mpTravelDestinationDTO.travelMediumCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                            <td>

                                <%
                                    value = CatRepository.getInstance().getText(Language, "travel_type", mpTravelDestinationDTO.travelTypeCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                            <td>

                                <%
                                    value = String.format("%.1f", mpTravelDestinationDTO.cost);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                            <td>
                                <%
                                    value = mpTravelDestinationDTO.reachDate + "";
                                %>
                                <%
                                    String formatted_reachDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_reachDate, Language)%>


                            </td>
                            <td>
                                <%
                                    value = mpTravelDestinationDTO.distanceOrSubCost + "";
                                %>
                                <%
                                    value = String.format("%.1f", mpTravelDestinationDTO.distanceOrSubCost);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>
                            <td>
                                <%
                                    value = mpTravelDestinationDTO.timeCat + "";
                                %>
                                <%
                                    value = CatRepository.getInstance().getText(Language, "travel_time", mpTravelDestinationDTO.timeCat);
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>
                        <%

                            }

                        %>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>