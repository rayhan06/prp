<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="mp_travel_allowance.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="geolocation.GeoLocationUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%
    String navigator2 = "navMP_TRAVEL_ALLOWANCE";
    String servletName = "Mp_travel_allowanceServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ISSESSION, loginDTO)%>
            </th>
            <th><%=isLanguageEnglish ? "For Meeting" : "মিটিং এর উদ্দেশ্যে"%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTTIMECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ENDTIMECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_STARTADDRESS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MPNAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_MPNAMEBN, loginDTO)%>
            </th>
            <th><%=isLanguageEnglish ? "Election Constituency Name English" : "নির্বাচন এলাকার নাম ইংরেজি"%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_ADD_ELECTIONCONSTITUENCYBN, loginDTO)%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Bill Form" : "বিল ফর্ম"%>
            </th>
            <th><%=LM.getText(LC.MP_TRAVEL_ALLOWANCE_SEARCH_MP_TRAVEL_ALLOWANCE_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Mp_travel_allowanceDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Mp_travel_allowanceDTO mp_travel_allowanceDTO = (Mp_travel_allowanceDTO) data.get(i);


        %>
        <tr>


            <td>
                <%
                    value = mp_travel_allowanceDTO.isSession + "";
                %>

                <%=Utils.getYesNo(value, Language)%>


            </td>
            <td>
                <%
                    value = mp_travel_allowanceDTO.isMeeting + "";
                %>

                <%=Utils.getYesNo(value, Language)%>


            </td>

            <td>
                <%
                    value = mp_travel_allowanceDTO.startDate + "";
                %>
                <%
                    String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_startDate, Language)%>


            </td>

            <td>
                <%
                    value = mp_travel_allowanceDTO.startTimeCat + "";
                %>
                <%
                    value = CatRepository.getInstance().getText(Language, "travel_time", mp_travel_allowanceDTO.startTimeCat);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = mp_travel_allowanceDTO.endDate + "";
                %>
                <%
                    String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_endDate, Language)%>


            </td>

            <td>
                <%
                    value = mp_travel_allowanceDTO.endTimeCat + "";
                %>
                <%
                    value = CatRepository.getInstance().getText(Language, "travel_time", mp_travel_allowanceDTO.endTimeCat);
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <td>
                <%
                    value = mp_travel_allowanceDTO.startAddress + "";
                %>
                <%=GeoLocationUtils.getGeoLocationString(value, Language)%>

            </td>


            <td>
                <%
                    value = mp_travel_allowanceDTO.mpNameEn + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = mp_travel_allowanceDTO.mpNameBn + "";
                %>

                <%=Utils.getDigits(value, Language)%>

            </td>


            <td>
                <%
                    value = mp_travel_allowanceDTO.electionConstituencyEn + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <td>
                <%
                    value = mp_travel_allowanceDTO.electionConstituencyBn + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=form&ID=<%=mp_travel_allowanceDTO.iD%>'"
                >
                    <%=Language.equalsIgnoreCase("English") ? "Bill Form" : "বিল ফর্ম"%>
                </button>
            </td>
            <%CommonDTO commonDTO = mp_travel_allowanceDTO; %>
            <td>
                <%
                    if (commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {
                %>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
                <%
                    }
                %>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=mp_travel_allowanceDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			