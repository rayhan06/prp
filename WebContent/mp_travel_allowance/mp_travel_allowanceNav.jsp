<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Mp_travel_allowanceServlet?actionType=search";
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String electionId = request.getParameter("election_details_id");
    String electionConstituencyId = request.getParameter("election_constituency_id");
    String toolbarOpen = request.getParameter("toolbarOpen") == null ? "0" : request.getParameter("toolbarOpen");


%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <%--        <div class="kt-portlet__head-toolbar">--%>
        <%--            <div class="kt-portlet__head-group">--%>
        <%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
        <%--                     aria-hidden="true" x-placement="top"--%>
        <%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
        <%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
        <%--                    <div class="tooltip-inner">Collapse</div>--%>
        <%--                </div>--%>
        <%--            </div>--%>
        <%--        </div>--%>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="election_details_id">
                            <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONDETAILSID, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select style='width: 100%' class='form-control' name='election_details_id'
                                    id='election_details_id'
                                    onSelect='setSearchChanged()' onchange="electionDetailsChanged(this);">
                                <%=Election_detailsRepository.getInstance().buildOptions(Language, (electionId == null || electionId.length() == 0) ? null : Long.parseLong(electionId))%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="election_constituency_id">
                            <%=LM.getText(LC.MP_FUND_AMOUNT_ADD_ELECTIONCONSTITUENCYID, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='election_constituency_id' id='election_constituency_id'
                                    onSelect='setSearchChanged()'>
                                <%
                                    if (electionId != null && electionId.length() != 0) {
                                %>
                                <%=Election_constituencyRepository.getInstance().buildOptions(Language, (electionConstituencyId == null || electionConstituencyId.length() == 0) ? null : Long.parseLong(electionConstituencyId), Long.parseLong(electionId))%>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0,true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>
<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const electionConstituencyInput = $('#election_constituency_id');
    const electionDetailsIdInput = $('#election_details_id');
    const anyFieldSelector = $('#anyfield');
    let previousConstituencyVal = null;

    function resetInputs() {
        electionConstituencyInput.select2("val", '-1');
        electionDetailsIdInput.select2("val", '-1');
        previousConstituencyVal = null;
    }

    $(document).ready(() => {
        readyInit('Mp_travel_allowanceServlet');
        if ('<%=toolbarOpen%>' == '1') {
            document.querySelector('.kt-portlet__body').style.display = 'block';
            document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse')
        }
        select2SingleSelector('#election_details_id', '<%=Language%>');
        select2SingleSelector('#election_constituency_id', '<%=Language%>');
    });
    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state;
            dosubmit(params, false);
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {
                        case 'election_details_id':
                            electionDetailsIdInput.val(item[1]).trigger('change');
                            break;
                        case 'election_constituency_id':
                            previousConstituencyVal = item[1];
                            electionConstituencyInput.val(item[1]).trigger('change');
                            // electionConstituencyInput.select2("val", item[1]);
                            break;
                        case 'AnyField':
                            anyFieldSelector.val(item[1]);
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    async function electionDetailsChanged(selectElem) {
        const electionDetailsId = $(selectElem).val();
        electionConstituencyInput.innerHTML = '';
        $(electionConstituencyInput).val('');

        if (!electionDetailsId) return;

        const url = "Election_constituencyServlet?actionType=buildElectionConstituency&election_id=" + electionDetailsId
            + "&language=" + '<%=Language%>';
        const res = await fetch(url);
        document.getElementById('election_constituency_id').innerHTML = await res.text();
        if (previousConstituencyVal != null) {
            electionConstituencyInput.val(previousConstituencyVal).trigger('change');
        }
        previousConstituencyVal = null;
    }

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (pushState) {
                    history.pushState(params, '', 'Mp_travel_allowanceServlet?actionType=search&' + params);
                }
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText ;
                    setPageNo();
                    searchChanged = 0;
                }, 500);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Mp_travel_allowanceServlet?actionType=search&ajax=true&isPermanentTable=<%=isPermanentTable%>&" + params, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, pushState = true) {
        let params = 'search=true';
        if (document.getElementById('anyfield').value) {
            params += '&AnyField=' + document.getElementById('anyfield').value
        }
        params += '&election_details_id=' + $('#election_details_id').val();
        const electionConstituency = $('#election_constituency_id').val();
        if (electionConstituency) params += '&election_constituency_id=' + electionConstituency;
        if (document.getElementsByClassName("kt-portlet__body")[0].style['display'] != 'none') {
            params += '&toolbarOpen=1';
        }
        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })
        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }
        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params, pushState);

    }
</script>