<%@ page import="util.HttpRequestUtils" %>
<%@ page import="util.UtilCharacter" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String formTitle = UtilCharacter.getDataByLanguage(Language, "ভ্রমন ভাতা বিল ফর্ম", "Travel Allowance Bill Form");
    String context = "../../.." + request.getContextPath() + "/";
%>

<link href="<%=context%>/assets/css/pagecustom.css" rel="stylesheet" type="text/css"/>
<jsp:include page="../common/layout.jsp" flush="true">
    <jsp:param name="title" value="<%=formTitle%>"/>
    <jsp:param name="body" value="../mp_travel_allowance/mp_travel_allowanceFormBody.jsp"/>
</jsp:include> 