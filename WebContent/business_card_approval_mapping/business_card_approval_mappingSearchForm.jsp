<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="card_approval_mapping.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.*" %>
<%@ page import="business_card_approval_mapping.Business_card_approval_mappingDTO" %>
<%@ page import="business_card_info.Business_card_infoDTO" %>
<%@ page import="card_info.*" %>
<%@ page import="business_card_info.Business_card_infoDAO" %>
<%@ page import="common.BaseServlet" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String value = "";
    String Language = LM.getText(LC.CARD_APPROVAL_MAPPING_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    Map<Long, CardEmployeeInfoDTO> mapByCardEmployeeInfoId;
    Map<Long, CardEmployeeOfficeInfoDTO> mapByCardEmployeeOfficeInfoDTOId;
    Map<Long, CardApprovalDTO> mapByCardApprovalDTOId;
    long cardInfoId=-1;
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr class="text-nowrap">
            <th style="text-align: center"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_CARDHOLDER_INFORMATION, loginDTO)%></th>
            <th style="text-align: center"><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_TOTALCOUNT, loginDTO)%></th>
            <th style="text-align: center"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARDAPPROVALSTATUSCAT, loginDTO)%></th>
            <th style="text-align: center"><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_CARDSTATUSCAT, loginDTO)%></th>
            <th style="text-align: center"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Business_card_approval_mappingDTO> data = (List<Business_card_approval_mappingDTO>) rn2.list;
                if (data != null && data.size()>0) {
                    List<Long> cardEmployeeInfoIds = new ArrayList<>();
                    List<Long> cardEmployeeOfficeInfoIds = new ArrayList<>();
                    List<Long> cardApprovalIds = new ArrayList<>();
                    List<Long> cardInfoIds = new ArrayList<>();
                    data.forEach(dto->{
                        cardApprovalIds.add(dto.cardApprovalId);
                        cardInfoIds.add(dto.businessCardInfoId);
                    });
                    List<CardApprovalDTO> cardApprovalDTOList = CardApprovalRepository.getInstance().getByIds(cardApprovalIds);
                    List<Business_card_infoDTO> cardInfoDTOList = Business_card_infoDAO.getInstance().getDTOs(cardInfoIds);
                    Map<Long,Business_card_infoDTO> mapByCardInfoId = new HashMap<>();
                    for(Business_card_infoDTO dto : cardInfoDTOList){
                        cardEmployeeInfoIds.add(dto.cardEmployeeInfoId);
                        cardEmployeeOfficeInfoIds.add(dto.cardEmployeeOfficeInfoId);
                        mapByCardInfoId.put(dto.iD,dto);
                    }
                    
                    mapByCardEmployeeInfoId = CardEmployeeInfoRepository.getInstance().getByIds(cardEmployeeInfoIds).stream()
                            .collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
                    mapByCardEmployeeOfficeInfoDTOId = CardEmployeeOfficeInfoRepository.getInstance().getByIds(cardEmployeeOfficeInfoIds).stream()
                            .collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
                    mapByCardApprovalDTOId = cardApprovalDTOList.stream()
                            .collect(Collectors.toMap(dto->dto.iD,dto->dto,(e1,e2)->e1));
                    for (int i = 0; i < data.size(); i++) {
                        Business_card_approval_mappingDTO businessCardApprovalMappingDTO = data.get(i);
        %>
        <tr id='tr_<%=i%>'>
            <%@include file="business_card_approval_mappingSearchRow.jsp"%>
        </tr>
        <%}}%>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>