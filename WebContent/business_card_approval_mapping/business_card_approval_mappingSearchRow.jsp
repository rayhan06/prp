<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="card_info.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="business_card_info.BusinessCardStatusEnum" %>

<%
    Business_card_infoDTO card_infoDTO = mapByCardInfoId.get(businessCardApprovalMappingDTO.businessCardInfoId);
%>
<td style='display:none;' class="text-nowrap"><input type='hidden' id='failureMessage_" + i + "' value=''/></td>

<td id='<%=i%>_cardHolder' class="text-nowrap" style="text-align: center">
    <%
        CardEmployeeInfoDTO cardEmployeeInfoDTO = mapByCardEmployeeInfoId.get(card_infoDTO.cardEmployeeInfoId);
        String str = null;
        if (cardEmployeeInfoDTO != null) {
            str = isLanguageEnglish ? cardEmployeeInfoDTO.nameEn : cardEmployeeInfoDTO.nameBn;
        }
        CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = mapByCardEmployeeOfficeInfoDTOId.get(card_infoDTO.cardEmployeeOfficeInfoId);
        if (cardEmployeeOfficeInfoDTO != null) {
            String s1 = isLanguageEnglish ? "<b>"+cardEmployeeOfficeInfoDTO.organogramEng + "</b><br>" + cardEmployeeOfficeInfoDTO.officeUnitEng
                    : "<b>"+cardEmployeeOfficeInfoDTO.organogramBng + "</b><br>" + cardEmployeeOfficeInfoDTO.officeUnitBng;
            if (str == null) {
                str = s1;
            } else {
                str += "<br>" + s1;
            }
        }
        if (str == null) {
            str = "";
        }
    %>
    <%=str%>
</td>

<td id='<%=i%>_totalCount' style="text-align: center">
    <%=isLanguageEnglish?card_infoDTO.totalCount: StringUtils.convertToBanNumber(String.valueOf(card_infoDTO.totalCount))%>
</td>

<td id='<%=i%>_cardApprovalStatusCat' class="text-nowrap" style="text-align: center">
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=ApprovalStatus.getColor(businessCardApprovalMappingDTO.cardApprovalStatusCat)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language, "card_approval_status", businessCardApprovalMappingDTO.cardApprovalStatusCat)%>
    </span>
</td>

<td id='<%=i%>_cardStatusCat' style="text-align: center">
    <span class="btn btn-sm border-0 shadow" style="background-color:<%=BusinessCardStatusEnum.getColor(businessCardApprovalMappingDTO.cardStatus)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language,"business_card_status", businessCardApprovalMappingDTO.cardStatus)%>
    </span>
</td>

<td class="text-nowrap" style="text-align: center">
    <span class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px">
        <a href='Business_card_approval_mappingServlet?actionType=getApprovalPage&cardInfoId=<%=businessCardApprovalMappingDTO.businessCardInfoId%>&language=<%=Language%>'
           style="color: #FFFFFF"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
    </span>
</td>

																						
											

