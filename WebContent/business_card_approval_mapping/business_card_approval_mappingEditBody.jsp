<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="task_type_approval_path.TaskTypeApprovalPathRepository" %>
<%@ page import="java.util.List" %>
<%@ page import="card_info.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.Map" %>
<%@ page import="business_card_approval_mapping.Business_card_approval_mappingDTO" %>
<%@ page import="business_card_approval_mapping.Business_card_approval_mappingDAO" %>
<%@ page import="business_card_info.Business_card_infoDTO" %>
<%@ page import="business_card_info.BusinessCardStatusEnum" %>
<%@ page import="business_card_info.Business_card_infoDAO" %>
<%@ page import="task_type_approval_path.TaskTypeApprovalPathDTO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    long cardInfoId = Long.parseLong(request.getParameter("cardInfoId"));
    Business_card_approval_mappingDTO cardApprovalMappingDTO = Business_card_approval_mappingDAO.getInstance().getByCardInfoIdAndApproverEmployeeRecordId(cardInfoId, userDTO.employee_record_id);
    boolean hasNextApprover = TaskTypeApprovalPathRepository.getInstance().hasNextApprover(cardApprovalMappingDTO.taskTypeId, cardApprovalMappingDTO.sequence + 1);
    System.out.println("hasNextApprover : " + hasNextApprover);
    String formTitle = LM.getText(LC.CARD_INFO_ADD_CARD_INFO_ADD_FORMNAME, loginDTO);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    Business_card_infoDTO card_infoDTO = Business_card_infoDAO.getInstance().getDTOFromID(cardInfoId);
    CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeInfoId);
    CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(card_infoDTO.cardEmployeeOfficeInfoId);
    int sequence = cardApprovalMappingDTO.sequence;
    List<Business_card_approval_mappingDTO> cardApprovalMappingDTOList = Business_card_approval_mappingDAO.getInstance().getAllApprovalDTOByCardInfoIdNotPending(cardInfoId);
    List<Long> cardApprovalIds = cardApprovalMappingDTOList.stream()
            .map(e -> e.cardApprovalId)
            .collect(Collectors.toList());
    Map<Long, CardApprovalDTO> mapByCardApprovalDTOId = CardApprovalRepository.getInstance().getByIds(cardApprovalIds)
            .stream()
            .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
    String options = null;
%>
<style>
    .fly-in-from-down {
        animation: flyFromDown 1s ease-out;
    }

    @keyframes flyFromDown {
        0% {
            transform: translateY(200%);
        }
        100% {
            transform: translateY(0%);
        }
    }

    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-4 mt-4">
    <div class="kt-subheader__main">
        <h2 class="kt-subheader__title" style="color: #00a1d4;">
            <i class="far fa-address-card"></i> <%=formTitle%>
        </h2>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div id="employee_info">
                    <div class="col-12">
                        <h3 class="table-title"><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_BUSINESS_CARD_INFO_ADD_FORMNAME, loginDTO)%>
                        </h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <%
                                    if (card_infoDTO.cardStatusCat == BusinessCardStatusEnum.REJECTED.getValue()) {
                                %>
                                <tr>
                                    <td style="width:15%">
                                        <b><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>
                                        </b>
                                    </td>
                                    <td style="color: red" colspan="3"><%=card_infoDTO.comment%>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>

                                <tr>
                                    <td style="width:15%"><b><%=LM.getText(LC.CARD_INFO_ADD_CARDSTATUSCAT, loginDTO)%>
                                    </b></td>
                                    <td style="width: 35%">
								<span class="btn btn-sm border-0 shadow"
                                      style="background-color: <%=BusinessCardStatusEnum.getColor(card_infoDTO.cardStatusCat)%>; color: white; border-radius: 8px;cursor: text">
											<%=CatRepository.getInstance().getText(Language, "business_card_status", card_infoDTO.cardStatusCat)%>
								</span>
                                    </td>
                                    <td style="width:15%">
                                        <b><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_TOTALCOUNT, loginDTO)%>
                                        </b></td>
                                    <td style="width: 35%">
                                        <%=isLanguageEnglish ? card_infoDTO.totalCount : StringUtils.convertToBanNumber(String.valueOf(card_infoDTO.totalCount))%>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width:15%"><b><%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                                    </b></td>
                                    <td style="width: 35%">
                                        <%=isLanguageEnglish ? cardEmployeeInfoDTO.nameEn : cardEmployeeInfoDTO.nameBn%>
                                    </td>

                                    <td style="width:15%"><b><%=LM.getText(LC.GLOBAL_EMAIL, loginDTO)%>
                                    </b></td>
                                    <td style="width: 35%">
                                        <%=cardEmployeeInfoDTO.email%>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width:15%">
                                        <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHER_NAME, loginDTO)%>
                                        </b></td>
                                    <td style="width: 35%">
                                        <%=isLanguageEnglish ? cardEmployeeInfoDTO.fatherNameEn : cardEmployeeInfoDTO.fatherNameBn%>
                                    </td>

                                    <td style="width:15%">
                                        <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHER_NAME, loginDTO)%>
                                        </b></td>
                                    <td style="width: 35%">
                                        <%=isLanguageEnglish ? cardEmployeeInfoDTO.motherNameEn : cardEmployeeInfoDTO.motherNameBn%>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width:15%">
                                        <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                                        </b></td>
                                    <td style="width: 35%">
                                        <%=isLanguageEnglish ? cardEmployeeOfficeInfoDTO.organogramEng : cardEmployeeOfficeInfoDTO.organogramBng%>
                                    </td>

                                    <td style="width:15%">
                                        <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                                        </b></td>
                                    <td style="width: 35%">
                                        <%=isLanguageEnglish ? cardEmployeeOfficeInfoDTO.officeUnitEng : cardEmployeeOfficeInfoDTO.officeUnitBng%>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width:15%">
                                        <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO)%>
                                        </b></td>
                                    <td style="width: 35%">
                                        <%=isLanguageEnglish ? cardEmployeeInfoDTO.presentAddressEn : cardEmployeeInfoDTO.presentAddressBn%>
                                    </td>

                                    <td style="width:15%">
                                        <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO)%>
                                        </b></td>
                                    <td style="width: 35%">
                                        <%=isLanguageEnglish ? cardEmployeeInfoDTO.permanentAddressEn : cardEmployeeInfoDTO.permanentAddressBn%>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
                <%
                    List<TaskTypeApprovalPathDTO> nextApprovalPath = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeIdAndLevel(cardApprovalMappingDTO.taskTypeId, cardApprovalMappingDTO.sequence + 1);
                    boolean isLastSequence = (nextApprovalPath == null || nextApprovalPath.size() == 0);

                %>
                <%
                    if (cardApprovalMappingDTO.cardApprovalStatusCat == ApprovalStatus.PENDING.getValue() &&
                            card_infoDTO.cardStatusCat == BusinessCardStatusEnum.WAITING_FOR_APPROVAL.getValue()) {
                %>
                <div class="row my-5">
                    <div class="col-12 text-right">
                        <button type="button" class="btn btn-success shadow ml-2" style="border-radius: 8px;"
                                id="approve_btn"
                                onclick="approveCard()"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_APPROVAL, loginDTO)%>
                        </button>
                        <button type="button" class="btn btn-danger shadow ml-2" style="border-radius: 8px;"
                                id="reject_btn"
                                onclick="reject()"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_DO_NOT_APPOVE, loginDTO)%>
                        </button>

                    </div>
                </div>
                <%
                    } else if (isLastSequence) {
                        options = BusinessCardStatusEnum.buildNextOptions(card_infoDTO.cardStatusCat, Language);
                    }
                %>
                <%
                    if (options != null) {
                %>
                <div id="card_status">
                    <div class="col-12">
                        <h3 class="table-title"><%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION, loginDTO)%>
                        </h3>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_CARDSTATUSCAT, loginDTO)%>
                                </td>
                                <td id="card_status_option">
                                    <select id="businessCardStatus" class='form-control rounded'
                                            onchange="businessCardStatusChanged(this);">
                                        <%=options%>
                                    </select>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success shadow ml-2"
                                            style="border-radius: 8px;" id="status_change_btn"
                                            onclick="changeStatus()"><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_BUSINESS_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%
                    }
                %>

                <div id="card_requester_info">
                    <div class="col-12">
                        <h3 class="table-title"><%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION, loginDTO)%>
                        </h3>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.CARD_INFO_NAME, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_DESIGNATION_OFFICE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_REQUEST_CREATE_DATE_TIME, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="width: 35%"><%=isLanguageEnglish ? card_infoDTO.insertByNameEng : card_infoDTO.insertByNameBng%>
                                </td>
                                <td style="width: 35%">
                                    <b><%=isLanguageEnglish ? card_infoDTO.insertByOfficeUnitOrganogramEng : card_infoDTO.insertByOfficeUnitOrganogramBng%>
                                    </b>
                                    <br><%=isLanguageEnglish ? card_infoDTO.insertByOfficeUnitEng : card_infoDTO.insertByOfficeUnitBng%>
                                </td>
                                <td style="width: 35%"><%=StringUtils.convertToDateAndTime(isLanguageEnglish, card_infoDTO.insertionTime)%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="approval_info_div">
                    <div class="col-md-12">
                        <h3 class="table-title"><%=LM.getText(LC.CARD_INFO_APPROVAL_INFORMATION, loginDTO)%>
                        </h3>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVER_OFFICE_INFORMATION, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVAL_STATUS, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVAL_DATE_TIME, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                for (Business_card_approval_mappingDTO approvalMappingDTO : cardApprovalMappingDTOList) {
                                    CardApprovalDTO cardApprovalDTO = mapByCardApprovalDTOId.get(approvalMappingDTO.cardApprovalId);
                            %>
                            <tr>
                                <td>
                                    <b><%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                    </b></td>
                                <td>
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=ApprovalStatus.getColor(approvalMappingDTO.cardApprovalStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "card_approval_status", approvalMappingDTO.cardApprovalStatusCat)%>
                                    </span>
                                </td>
                                <td>
                                    <%=StringUtils.convertToDateAndTime(isLanguageEnglish, approvalMappingDTO.lastModificationTime)%>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none">
    <form class="form-horizontal text-right"
          action="Business_card_approval_mappingServlet?actionType=rejectCard&cardInfoId=<%=cardInfoId%>"
          id="rejectForm" name="rejectForm" method="POST"
          enctype="multipart/form-data">
        <input type="hidden" name="reject_reason" id="reject_reason">
    </form>
</div>

<div style="display: none">
    <form class="form-horizontal text-right"
          action="Business_card_approval_mappingServlet?actionType=approved_card&cardInfoId=<%=cardInfoId%>"
          id="approveForm" name="approveForm" method="POST"
          enctype="multipart/form-data">
    </form>
</div>

<div style="display: none">
    <form class="form-horizontal text-right"
          action="Business_card_infoServlet?actionType=cardStatusChange&cardInfoId=<%=cardInfoId%>"
          id="cardStatusChange" name="cardStatusChange" method="POST"
          enctype="multipart/form-data">
        <input type="text" id="card_status_value" name="cardStatusValue">
    </form>
</div>

<script>
    let cardStatus, cardStatusText;
    let lang = '<%=Language%>'.toLowerCase();
    $(document).ready(function () {
        if (document.getElementById("status_change_btn")) {
            $('#status_change_btn').prop('disabled', true);
        }
    });


    function PreprocessBeforeSubmiting() {
    }

    function changeStatus() {
        console.log('change status');
        let msg, title;
        if (lang === 'english') {
            msg = "Do you want to change card status as " + cardStatusText.toLowerCase() + "?";
            title = "You won't be able to revert this !";
        } else {
            msg = "আপনি কি কার্ডের অবস্থা " + cardStatusText + " হিসেবে পরিবর্তন করতে চান?";
            title = 'পরবর্তিতে আর পরিবর্তন করতে পারবেন না !';
        }
        messageDialog(msg, title, 'success', true, '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_YES_SUBMIT, loginDTO))%>',
            '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_CANCEL, loginDTO))%>', () => {
                $('#status_change_btn').prop('disabled', true);
                $('#businessCardStatus').prop('disabled', true);
                $('#card_status_value').val(cardStatus);
                $('#cardStatusChange').submit();
            }, () => {
            });
    }

    function businessCardStatusChanged(e) {
        cardStatus = e.value;
        cardStatusText = $('#businessCardStatus option:selected').text();
        if (!cardStatus) {
            $('#status_change_btn').prop('disabled', true);
        } else {
            $('#status_change_btn').prop('disabled', false);
        }
    }

    function reject() {
        let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>';
        let placeHolder = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_TYPE_DISSATISFACTION_REASON_HERE, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_DISSATISFIED, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';
        dialogMessageWithTextBox(msg, placeHolder, confirmButtonText, cancelButtonText, emptyReasonText, (reason) => {
            $('#reject_btn').prop("disabled", true);
            $('#reject_reason').val(reason);
            $('#rejectForm').submit();
        }, () => {
        });
    }

    function approveCard() {
        let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_ARE_YOU_SATISFIED, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_SATISFIED, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText, () => {
            $('#approve_btn').prop("disabled", true);
            $('#reject_btn').prop("disabled", true);
            $('#approveForm').submit();
        }, () => {
        });
    }

    function submitValidate() {
        $('#reject_btn').prop("disabled", true);
        $('#modal_btn').prop("disabled", true);
        if (PreprocessBeforeSubmiting()) {
            $('#permanentCardForm').submit();
        } else {
            $('#reject_btn').prop("disabled", false);
            $('#modal_btn').prop("disabled", false);
        }
    }
</script>

