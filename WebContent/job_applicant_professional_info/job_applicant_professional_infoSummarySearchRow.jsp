<%@page pageEncoding="UTF-8" %>

<%@page import="job_applicant_professional_info.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="javax.rmi.CORBA.Util" %>
<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_LANGUAGE, loginDTO);
	String Language2 = Language;

	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);




	Job_applicant_professional_infoDTO job_applicant_professional_infoDTO = (Job_applicant_professional_infoDTO)request.getAttribute("job_applicant_professional_infoDTO");

	Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();

	System.out.println("job_applicant_professional_infoDTO = " + job_applicant_professional_infoDTO);


	int i = Integer.parseInt(request.getParameter("rownum"));
	out.println("<td style=\"  width: 20%; height: auto; border: 1px solid gray; display:none;\"><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

	String value = "";


	Job_applicant_professional_infoDAO job_applicant_professional_infoDAO = new Job_applicant_professional_infoDAO();


	String Options = "";
	boolean formSubmit = false;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>



<%--
<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_jobApplicantId'>
<%
value = job_applicant_professional_infoDTO.jobApplicantId + "";
%>

<%=value%>


</td>
 --%>






<td style="  width: 15%; height: auto; border: 1px solid gray;"  id = '<%=i%>_designation'>
	<%
		value = job_applicant_professional_infoDTO.designation + "";
	%>

	<%=value%>


</td>


<td style="  width: 19%; height: auto; border: 1px solid gray;"  id = '<%=i%>_nameOfEmployee'>
	<%
		value = job_applicant_professional_infoDTO.nameOfEmployee + "";
	%>

	<%=value%>


</td>

<td style="  width: 13%; height: auto; border: 1px solid gray;"  id = '<%=i%>_department'>
	<%
		value = job_applicant_professional_infoDTO.department + "";
	%>

	<%=value%>


</td>

<td style="  width: 13%; height: auto; border: 1px solid gray;"  id = '<%=i%>_gazettedDate'>
	<%
		Options = "";
		System.out.println("#####");
		System.out.println(job_applicant_professional_infoDTO.jobApplicantCandidateCat);
		System.out.println("#####");
		if (job_applicant_professional_infoDTO.jobApplicantCandidateCat != 0) {
			Options = CatRepository.getName(Language, "job_applicant_candidate_type", job_applicant_professional_infoDTO.jobApplicantCandidateCat);
		}
	%>
	<%=Options%>
	<%
		if (job_applicant_professional_infoDTO.jobApplicantCandidateCat == 5) {
	%>
	(<%=job_applicant_professional_infoDTO.jobApplicantCandidateText%>)
	<%
		}
	%>


</td>


<td style="  width: 10%; height: auto; border: 1px solid gray;"  id = '<%=i%>_servingFrom'>
	<%
		value = job_applicant_professional_infoDTO.servingFrom + "";
	%>
	<%
		String formatted_servingFrom = simpleDateFormat.format(new Date(Long.parseLong(value)));
	%>
	<%=UtilCharacter.convertDataByLanguage(Language, formatted_servingFrom)%>


</td>


<td style="  width: 10%; height: auto; border: 1px solid gray;"  id = '<%=i%>_servingTo'>
	<%
		value = job_applicant_professional_infoDTO.servingTo + "";
	%>
	<%
		String formatted_servingTo = simpleDateFormat.format(new Date(Long.parseLong(value)));

		if (job_applicant_professional_infoDTO.isCurrentlyWorking == 0 && job_applicant_professional_infoDTO.servingTo != 0) {
	%>
	<%=UtilCharacter.convertDataByLanguage(Language, formatted_servingTo)%>

	<%
	}
	else {
	%>
	N/A
	<%
		}
	%>


</td>



<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_jobResponsibility'>
	<%
		value = job_applicant_professional_infoDTO.jobResponsibility + "";
	%>

	<%=value%>


</td>




<%--
<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_insertedBy'>
<%
value = job_applicant_professional_infoDTO.insertedBy + "";
%>

<%=value%>


</td>


<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_modifiedBy'>
<%
value = job_applicant_professional_infoDTO.modifiedBy + "";
%>

<%=value%>


</td>
 --%>













<script>
	window.onload =function ()
	{
		console.log("using ckEditor");
		CKEDITOR.replaceAll();
	}

</script>	

