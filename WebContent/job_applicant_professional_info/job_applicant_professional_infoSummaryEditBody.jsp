
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="job_applicant_professional_info.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="user.*"%>

<%@page import="workflow.*"%>
<%@page import="util.TimeFormat"%>

<%
	Job_applicant_professional_infoDAO job_applicant_professional_infoDAO = new Job_applicant_professional_infoDAO();

	Job_applicant_professional_infoDTO job_applicant_professional_infoDTO = job_applicant_professional_infoDAO.getDTOByJobApplicantID(600);
//	job_applicant_professional_infoDTO = (Job_applicant_professional_infoDTO)request.getAttribute("job_applicant_professional_infoDTO");
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
	if(job_applicant_professional_infoDTO == null)
	{
		job_applicant_professional_infoDTO = new Job_applicant_professional_infoDTO();

	}
	System.out.println("job_applicant_professional_infoDTO = " + job_applicant_professional_infoDTO);

	String actionName;
	System.out.println("actionType = " + request.getParameter("actionType"));
	if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
	{
		actionName = "add";
	}
	else
	{
		actionName = "edit";
	}
	String formTitle;
	if(actionName.equals("edit"))
	{
		formTitle = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_EMPLOYEE_SERVICE_HISTORY_EDIT_FORMNAME, loginDTO);
	}
	else
	{
		formTitle = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_EMPLOYEE_SERVICE_HISTORY_ADD_FORMNAME, loginDTO);
	}

	String ID = request.getParameter("ID");
	if(ID == null || ID.isEmpty())
	{
		ID = "0";
	}
	System.out.println("ID = " + ID);
	int i = 0;

	String value = "";

	int childTableStartingID = 1;

	long ColumnID;
	FilesDAO filesDAO = new FilesDAO();
	boolean isPermanentTable = true;
	if(request.getParameter("isPermanentTable") != null)
	{
		isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
	}

	Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
	ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
	Approval_execution_tableDTO approval_execution_tableDTO = null;
	Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
	ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

	String tableName = "job_applicant_professional_info";

	boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

	if(!isPermanentTable)
	{
		approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("job_applicant_professional_info", job_applicant_professional_infoDTO.iD);
		System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
		approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
		approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("job_applicant_professional_info", job_applicant_professional_infoDTO.iD);
		if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
		{
			canApprove = true;
			if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
			{
				canValidate = true;
			}
		}

		isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

		canTerminate = isInitiator && job_applicant_professional_infoDTO.isDeleted == 2;
	}
%>

<input type="hidden" id="servingTo_date_initial"/>
<input type="hidden" id="gazetted_date_initial"/>

<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">








				<%@ page import="java.text.SimpleDateFormat"%>
				<%@ page import="java.util.Date"%>

				<%@ page import="pb.*"%>
				<%@ page import="category.CategoryDAO" %>
				<%@ page import="category.CategoryDTO" %>

				<%
					String Language = LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_LANGUAGE, loginDTO);
					String Options;
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					Date date = new Date();
					String datestr = dateFormat.format(date);

					String employeeEmploymentType = CommonDAO.getNameDecidedByLanguage(loginDTO, "employee_employment", job_applicant_professional_infoDTO.employeeEmploymentType);
					String employeeWorkAreaType = CommonDAO.getNameDecidedByLanguage(loginDTO, "employee_work_area", job_applicant_professional_infoDTO.employeeWorkAreaType);
					String serviceType = CommonDAO.getNameDecidedByLanguage(loginDTO, "service", job_applicant_professional_infoDTO.serviceType);

					String formatted_gazettedDate = dateFormat.format(new Date(job_applicant_professional_infoDTO.gazettedDate));
					String formatted_servingFrom = dateFormat.format(new Date(job_applicant_professional_infoDTO.servingFrom));
					String formatted_servingTo = dateFormat.format(new Date(job_applicant_professional_infoDTO.servingTo));

				%>


				<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=job_applicant_professional_infoDTO.iD%>' tag='pb_html'/>



				<input type='hidden' class='form-control'  name='jobApplicantId' id = 'jobApplicantId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.jobApplicantId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
				<input type='hidden' class='form-control'  name='jobId' id = 'jobId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.jobId + "'"):("'" + request.getParameter("jobId") + "'")%> tag='pb_html'/>


				<table style="border: 1px solid gray; width: 100%;">


					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_DESIGNATION, loginDTO)):(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DESIGNATION, loginDTO))%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_professional_infoDTO.designation%>
							</div>

						</td>

					</tr>

					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=(actionName.equals("edit"))?(LM.getText(LC.JOB_APPLICANT_PROFESSIONAL_INFO_EDIT_NAMEOFEMPLOYEE, loginDTO)):(LM.getText(LC.JOB_APPLICANT_PROFESSIONAL_INFO_ADD_NAMEOFEMPLOYEE, loginDTO))%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_professional_infoDTO.nameOfEmployee%>
							</div>

						</td>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=(actionName.equals("edit"))?(LM.getText(LC.JOB_APPLICANT_PROFESSIONAL_INFO_EDIT_DEPARTMENT, loginDTO)):(LM.getText(LC.JOB_APPLICANT_PROFESSIONAL_INFO_ADD_DEPARTMENT, loginDTO))%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_professional_infoDTO.department%>
							</div>

						</td>

					</tr>

					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_ISGOVTJOB, loginDTO)):(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_ISGOVTJOB, loginDTO))%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_professional_infoDTO.isGovtJob%>
							</div>

						</td>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_GAZETTEDDATE, loginDTO)):(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_GAZETTEDDATE, loginDTO))%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=formatted_gazettedDate%>
							</div>

						</td>

					</tr>

					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_SERVINGFROM, loginDTO)):(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_SERVINGFROM, loginDTO))%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=formatted_servingFrom%>
							</div>

						</td>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_SERVINGTO, loginDTO)):(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_SERVINGTO, loginDTO))%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=formatted_servingTo%>
							</div>

						</td>

					</tr>

					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_JOBRESPONSIBILITY, loginDTO)):(LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_JOBRESPONSIBILITY, loginDTO))%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_professional_infoDTO.jobResponsibility%>
							</div>

						</td>


					</tr>

				</table>

				<input type='hidden' class='form-control'  name='employeeCadreId' id = 'employeeCadreId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.employeeCadreId + "'"):("'" + "0" + "'")%> tag='pb_html'/>


				<input type='hidden' class='form-control'  name='employeeBatchesId' id = 'employeeBatchesId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.employeeBatchesId + "'"):("'" + "0" + "'")%> tag='pb_html'/>





				<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>


				<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.insertedBy + "'"):("'" + "0" + "'")%> tag='pb_html'/>


				<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.modifiedBy + "'"):("'" + "0" + "'")%> tag='pb_html'/>


				<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.jobCat + "'"):("'" + "-1" + "'")%> tag='pb_html'/>


				<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>



				<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_professional_infoDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>









				<%if(canValidate)
				{
				%>
				<div class="row div_border attachement-div">
					<div class="col-md-12">
						<h5><%=LM.getText(LC.HM_ATTACHMENTS, loginDTO )%></h5>
						<%
							ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
						%>
						<div class="dropzone"
							 action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
							<input type='file' style="display: none"
								   name='approval_attached_fileDropzoneFile'
								   id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
								   tag='pb_html' />
						</div>
						<input type='hidden'
							   name='approval_attached_fileDropzoneFilesToDelete'
							   id='approval_attached_fileDropzoneFilesToDelete_<%=i%>' value=''
							   tag='pb_html' /> <input type='hidden'
													   name='approval_attached_fileDropzone'
													   id='approval_attached_fileDropzone_dropzone_<%=i%>' tag='pb_html'
													   value='<%=ColumnID%>' />
					</div>

					<div class="col-md-12">
						<h5><%=LM.getText(LC.HM_REMARKS, loginDTO )%></h5>

						<textarea class='form-control'  name='remarks' id = '<%=i%>_remarks'   tag='pb_html'></textarea>
					</div>
				</div>
				<%
					}
				%>




	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


	$(document).ready( function(){

		basicInit();
	});

	function PreprocessBeforeSubmiting(row, validate)
	{
		if(validate == "report")
		{
		}
		else
		{
			var empty_fields = "";
			var i = 0;
			if(!document.getElementById('servingFrom_date_' + row).checkValidity())
			{
				empty_fields += "servingFrom";
				if(i > 0)
				{
					empty_fields += ", ";
				}
				i ++;
			}


			if(empty_fields != "")
			{
				if(validate == "inplaceedit")
				{
					$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
					return false;
				}
			}

		}

		preprocessCheckBoxBeforeSubmitting('isGovtJob', row);
		preprocessGeolocationBeforeSubmitting('employeeAddress', row, false);
		preprocessCheckBoxBeforeSubmitting('isCurrentlyWorking', row);

		return true;
	}


	function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
	{
		addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Job_applicant_professional_infoServlet");
	}

	function init(row)
	{

		initGeoLocation('employeeAddress_geoSelectField_', row, "Job_applicant_professional_infoServlet");


	}

	var row = 0;

	window.onload =function ()
	{
		init(row);
		CKEDITOR.replaceAll();
	}

	var child_table_extra_id = <%=childTableStartingID%>;


	function toggleGazettedDate(e) {
		var initial = document.getElementById('gazetted_date_initial').value;
		document.getElementById('gazetted_date_initial').value = initial==''?document.getElementById('servingTo_date_0').value:initial;
		document.getElementById('gazetted_date_initial').value = e.checked==true?document.getElementById('gazetted_date_initial').value:document.getElementById('gazettedDate_date_0').value;
		document.getElementById('gazettedDate_date_0').value = e.checked==true?document.getElementById('gazetted_date_initial').value:'';

		document.getElementById('gazetted_label_id').style.display = e.checked==true?'block':'none';
		document.getElementById('gazetted_div_id').style.display = e.checked==true?'block':'none';

		// document.getElementById('gazetted_label_id').style.display = e.checked==true?'none':'block';
		// document.getElementById('gazetted_div_id').style.display = e.checked==true?'none':'block';
	}

	function updateToDate(e) {
		document.getElementById('servingTo_date_initial').value = e.checked==true?document.getElementById('servingTo_date_0').value:document.getElementById('servingTo_date_initial').value;
		document.getElementById('servingTo_date_0').value = e.checked==true?'':document.getElementById('servingTo_date_initial').value;

		document.getElementById('servingTo_label_id').style.display = e.checked==true?'none':'block';
		document.getElementById('servingTo_div_id').style.display = e.checked==true?'none':'block';
	}

</script>






