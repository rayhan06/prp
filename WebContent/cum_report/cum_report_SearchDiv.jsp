<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page contentType="text/html;charset=utf-8" %>


<%@ page import="pb.*" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_MEDICAL_REPORT_EDIT_LANGUAGE, loginDTO);
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2">
     <div class="col-md-12">
        <%@include file="../pbreport/yearmonth.jsp" %>
    </div>
    <div class="col-md-12">
        <%@include file="../pbreport/calendar.jsp" %>
    </div>
</div>

<script type="text/javascript">
    function init() {
        addables = [0, 0, 1, 1, 1];
        var dateObj = new Date();
        var month = dateObj.getUTCMonth(); //months from 1-12
        var year = dateObj.getUTCFullYear();
        $("#dYear").val(year);
        $("#dMonth").val(month);
        processYMAndSubmit();
    }
    
    function setCDate(name) {
        console.log("calendar change called");
        $("#" + name).val(getDateTimestampById(name + '_js'));


        console.log("startdate = " + $("#" + name).val());


        //ajaxSubmit();
    }

    function PreprocessBeforeSubmiting() {
    	
    }

    function patient_inputted(userName, orgId) {
        console.log("patient_inputted " + userName);
        $("#userName").val(userName);
    }
    function setPhone(value)
    {
    	var convertedPhone = phoneNumberAdd88ConvertLanguage(value, '<%=Language%>');
    	$("#phone").val(convertedPhone);
    }
</script>