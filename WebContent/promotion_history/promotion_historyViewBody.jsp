<%@page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="promotion_history.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.StringUtils" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.PROMOTION_HISTORY_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Promotion_historyDTO promotion_historyDTO = Promotion_historyDAO.getInstance().getDTOFromID(id);
    FilesDAO filesDAO = new FilesDAO();
%>

<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTION_HISTORY_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background-color: white"><%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTION_HISTORY_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped text-nowrap">
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_EMPLOYEE_NAME, loginDTO)%>
                                                </b></td>
                                            <td><%=Employee_recordsRepository.getInstance().getEmployeeName(promotion_historyDTO.employeeRecordsId, Language)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO)%>
                                                </b></td>

                                            <%
                                                if (promotion_historyDTO.officeUnitsId != 0) {
                                            %>
                                            <td><%=Office_unitsRepository.getInstance().geText(Language, promotion_historyDTO.officeUnitsId)%>
                                            </td>
                                            <%} else {%>

                                            <td><%=promotion_historyDTO.officeUnitsText%>
                                            </td>
                                            <%
                                                }
                                            %>

                                        </tr>


                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.PROMOTION_NEW_DESIGNATION, loginDTO)%>
                                                </b></td>

                                            <%
                                                if (promotion_historyDTO.officeUnitsId != 0) {
                                            %>
                                            <td><%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, promotion_historyDTO.rankCat)%>
                                            </td>
                                            <%} else {%>

                                            <td><%=promotion_historyDTO.rankCatText%>
                                            </td>
                                            <%
                                                }
                                            %>

                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONDATE, loginDTO)%>
                                                </b></td>
                                            <td><%=StringUtils.getFormattedDate(Language, promotion_historyDTO.promotionDate)%>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.PROMOTION_HISTORY_ADD_GODATE, loginDTO)%>
                                                </b></td>
                                            <td><%=StringUtils.getFormattedDate(Language, promotion_historyDTO.gODate)%>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.PROMOTION_HISTORY_ADD_JOININGDATE, loginDTO)%>
                                                </b></td>
                                            <td><%=StringUtils.getFormattedDate(Language, promotion_historyDTO.joiningDate)%>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONNATURECAT, loginDTO)%>
                                                </b></td>
                                            <td><%=CatRepository.getInstance().getText(Language, "promotion_nature", promotion_historyDTO.promotionNatureCat)%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.PROMOTION_HISTORY_ADD_FILESDROPZONE, loginDTO)%>
                                                </b></td>
                                            <td>
                                                <%
                                                    {
                                                        List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(promotion_historyDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (FilesDTOList != null) {
                                                                for (int j = 0; j < FilesDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = FilesDTOList.get(j);
                                                                    byte[] encodeBase64 = Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                 style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Promotion_historyServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>