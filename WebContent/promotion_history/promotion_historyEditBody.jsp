<%@page import="employee_records.Employee_recordsDAO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="promotion_history.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>

<%@ page import="pb.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="common.BaseServlet" %>

<%
    Promotion_historyDTO promotion_historyDTO;
    promotion_historyDTO = (Promotion_historyDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    if (promotion_historyDTO == null) {
        promotion_historyDTO = new Promotion_historyDTO();
    }
    System.out.println("promotion_historyDTO = " + promotion_historyDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTION_HISTORY_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    int childTableStartingID = 1;

    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();

    String Language = LM.getText(LC.PROMOTION_HISTORY_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    CommonDAO.language = Language;
    CatDAO.language = Language;
    String empId = request.getParameter("empId") != null ? request.getParameter("empId") : null;
    long employeeRecordsId;
    if (empId == null) {
        employeeRecordsId = UserRepository.getUserDTOByUserID(loginDTO).employee_record_id;

    } else {
        employeeRecordsId = Long.parseLong(empId);
    }

    Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeRecordsId);
    EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employeeRecordsDTO.iD);
    String office = Office_unitsRepository.getInstance().geText(Language, employeeOfficeDTO.officeUnitId);
    String designation = OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, employeeOfficeDTO.officeUnitOrganogramId);
    String officeAndDesignation = designation + "<br>" + office;

    String url = "Promotion_historyServlet?actionType=ajax_" + actionName + "&isPermanentTable=true&empId=" + employeeRecordsId + "&iD=" + ID;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab") + "&userId=" + request.getParameter("userId");
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form" id="promotion-history" enctype="multipart/form-data" name="bigform"
              action="<%=url%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                        <%--Input Fields--%>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_EMPLOYEERECORDSID, loginDTO)%>
                                            </label>
                                            <label class="col-md-9 col-form-label text-md-left" id='rankCat_div_<%=i%>'>
                                                <%=Employee_recordsDAO.getEmployeeName(employeeRecordsId, Language)%>
                                            </label>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_DATEOFBIRTH, loginDTO)%>
                                            </label>
                                            <label class="col-md-9 col-form-label text-md-left" id='rankCat_div_<%=i%>'>
                                                <%=Employee_recordsDAO.getEmployeeDOBbyId(employeeRecordsId, Language)%>
                                            </label>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.EMPLOYEE_RECORD_JOINING_DATE, loginDTO)%>
                                            </label>
                                            <label class="col-md-9 col-form-label text-md-left" id='rankCat_div_<%=i%>'>
                                                <%=StringUtils.getFormattedDate(Language, employeeOfficeDTO.joiningDate)%>
                                            </label>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.CARD_INFO_DESIGNATION_OFFICE, loginDTO)%>
                                            </label>
                                            <label class="col-md-9 col-form-label text-md-left" id='rankCat_div_<%=i%>'>
                                                <%=officeAndDesignation%>
                                            </label>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                            </label>
                                            <div class="col-md-9" id='office-unit_button_<%=i%>'>
                                                <button type="button" class="btn btn-primary form-control"
                                                        id="addOffice_modal_button">
                                                    <%=isLanguageEnglish ? "Add Office and Designation" : "অফিস এবং পদবি যুক্ত করুন"%>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%= LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO) %>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='office-unit_div_<%=i%>'>
                                                <input type="text" class="form-control" id="office-unit"
                                                       name="officeUnitsId" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%= LM.getText(LC.PROMOTION_NEW_DESIGNATION, loginDTO) %>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='rankCat_div_<%=i%>'>
                                                <input type="text" class="form-control" id="designation"
                                                       name="rankCat" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%= isLanguageEnglish ? "Grade" : "গ্রেড" %>
                                            </label>
                                            <div class="col-md-9" id='rankCat_div_<%=i%>'>
                                                <input type="text" class="form-control" id="gradeCat"
                                                       name="gradeCat" readonly>
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=isLanguageEnglish ? "Grade Level" : "গ্রেড স্তর"%>
                                            </label>
                                            <div class="col-md-9" id='gradeCatLevelDiv'>
                                                <select class='form-control' name='gradeCatLevel'
                                                        id='gradeCatLevel'
                                                        tag='pb_html'>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONDATE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='promotionDate_div_<%=i%>'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID"
                                                               value="promotion-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' class='form-control'
                                                       id='promotion-date' name='promotionDate' value=''
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PROMOTION_HISTORY_ADD_GODATE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='gODate_div_<%=i%>'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID"
                                                               value="gO-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>

                                                </jsp:include>
                                                <input type='hidden' class='form-control' id='gO-date'
                                                       name='gODate' value='' tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.EMPLOYEE_RECORD_JOINING_DATE, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='joiningDate_div_<%=i%>'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID"
                                                               value="joining-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>

                                                </jsp:include>
                                                <input type='hidden' class='form-control' id='joining-date'
                                                       name='joiningDate' value=''
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONNATURECAT, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='promotionNatureCat_div_<%=i%>'>
                                                <select class='form-control' name='promotionNatureCat'
                                                        id='promotionNatureCat'
                                                        tag='pb_html'>
                                                    <%=CatRepository.getInstance().buildOptions("promotion_nature", Language, promotion_historyDTO.promotionNatureCat)%>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PROMOTION_HISTORY_ADD_FILESDROPZONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9" id='filesDropzone_div_<%=i%>'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(promotion_historyDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (filesDropzoneDTOList != null) {
                                                                for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                 style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Promotion_historyServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                            <a class='btn btn-danger'
                                                               onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>

                                                <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                                <div class="dropzone"
                                                     action="Promotion_historyServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?promotion_historyDTO.filesDropzone:ColumnID%>">
                                                    <input type='file' style="display:none"
                                                           name='filesDropzoneFile'
                                                           id='filesDropzone_dropzone_File_<%=i%>'
                                                           tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                                       id='filesDropzoneFilesToDelete_<%=i%>'
                                                       value='' tag='pb_html'/>
                                                <input type='hidden' name='filesDropzone'
                                                       id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=actionName.equals("edit")?promotion_historyDTO.filesDropzone:ColumnID%>'/>
                                            </div>
                                        </div>

                                        <%--Template--%>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                            </label>
                                            <div class="col-md-9">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Hidden Fields--%>
            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                   value='<%=promotion_historyDTO.iD%>' tag='pb_html'/>
            <input type='hidden' class='form-control' name='employeeRecordsId'
                   id='employeeRecordsId_hidden_<%=i%>'
                   value=<%=employeeRecordsId%> tag='pb_html'/>
            <input type='hidden' class='form-control' name='previousRankType'
                   id='previousRankType_hidden_<%=i%>'
                   value='<%=designation%>' tag='pb_html'/>

            <div class="row mt-3">
                <div class="col-md-10 text-right">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"
                       id="cancel-btn">
                        <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTION_HISTORY_CANCEL_BUTTON, loginDTO)%>
                    </a>
                    <button class="btn btn-success" onclick="submitItem('promotion-history')"
                            id="submit-btn" type="button">
                        <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTION_HISTORY_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../office_assign/officeSearchModal.jsp" %>

<script type="text/javascript">

    var child_table_extra_id = <%=childTableStartingID%>;
    let isEnglish = <%=isLanguageEnglish%>;
    let officeId = <%=promotion_historyDTO.officeUnitsId%>;
    let isShowTable = 1;
    let officeName = '';
    let officeUnitOrganogramId = <%=promotion_historyDTO.rankCat%>;
    let officeUnitOrganogramName = '';

    function submitItem(id) {
        let submitFunction = () => {
            let formName = '#' + id;
            const form = $(formName);

            buttonStateChangeFunction(true);
            if (isFormValid(form)) {
                submitAjaxForm(id);

            } else {
                buttonStateChangeFunction(false);
            }
        }
        if (isEnglish) {
            messageDialog('Do you want to submit?', "You won't be able to revert this!", 'success', true, 'Submit', 'Cancel', submitFunction);
        } else {
            messageDialog('সাবমিট করতে চান?', "সাবমিটের পর পরিবর্তনযোগ্য না!", 'success', true, 'সাবমিট', 'বাতিল', submitFunction);
        }
    }

    $(document).ready(function () {

        select2SingleSelector("#promotionNatureCat", "<%=Language%>");
        <%
            long curTime=Calendar.getInstance().getTimeInMillis();
        %>
        setMaxDateByTimestampAndId("promotion-date-js", <%=curTime%>);
        setMaxDateByTimestampAndId("gO-date-js", <%=curTime%>);
        setMaxDateByTimestampAndId("joining-date-js", <%=curTime%>);
        $.validator.addMethod('promotionNatureSelection', function (value, element) {
            return value != 0;
        });
        $("#promotion-history").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                promotionDate: "required",
                gODate: "required",
                joiningDate: "required",
                officeUnitsId: "required",
                rankCat: "required",
                promotionNatureCat: {
                    required: true,
                    promotionNatureSelection: true
                }
            },

            messages: {
                promotionDate: "<%=LM.getText(LC.PROMOTION_HISTORY_ADD_ENTER_PROMOTION_DATE, loginDTO)%>",
                gODate: "<%=LM.getText(LC.PROMOTION_HISTORY_ADD_ENTER_GO_DATE, loginDTO)%>",
                joiningDate: "<%=LM.getText(LC.PROMOTION_HISTORY_ADD_ENTER_NEW_JOINING_DATE, loginDTO)%>",
                officeUnitsId: "<%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ENTER_OFFICE, loginDTO)%>",
                rankCat: "<%=LM.getText(LC.EMPLOYEE_POSTING_ADD_ENTER_POST, loginDTO)%>",
                promotionNatureCat: "<%=LM.getText(LC.PROMOTION_HISTORY_ADD_ENTER_PROMOTION_NATURE, loginDTO)%>"
            }
        });
    });

    function isFormValid($form) {
        if (officeId > 0)
            $("#office-unit").val(officeId);
        if (officeUnitOrganogramId > 0)
            $("#designation").val(officeUnitOrganogramId);

        $('#promotion-date').val(getDateStringById('promotion-date-js'));
        $('#joining-date').val(getDateStringById('joining-date-js'));
        $('#gO-date').val(getDateStringById('gO-date-js'));

        const jQueryValid = $form.valid();
        const promotionDateValid = dateValidator('promotion-date-js', true);
        const joiningDateValid = dateValidator('joining-date-js', true);
        const goDateValid = dateValidator('gO-date-js', true);

        return jQueryValid && promotionDateValid && joiningDateValid && goDateValid;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Promotion_historyServlet");
    }

    function init(row) {

    }

    let row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    $('#addOffice_modal_button').on('click', function () {
        //alert('CLICKED');
        $('#search_office_modal').modal();
    });

    function triggerOfficeUnitOrganogramId() {

        console.log(officeId + "  " + officeUnitOrganogramId);
        $("#office-unit").val(officeName)
        $("#designation").val(officeUnitOrganogramName);
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    let result = JSON.parse(this.responseText);
                    $("#gradeCat").val(result.gradeTypeCatText);
                    document.getElementById('gradeCatLevel').innerHTML = result.option;
                    $("#gradeCatLevel").select2();

                } else {
                    showToast(failed_message_bng, failed_message_eng);
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("get", "Office_unit_organogramServlet?actionType=getOrganograms" +
            '&officeUnitOrganogramId=' + officeUnitOrganogramId, true);
        xhttp.send();


    }

    $(function () {

        $('#promotion-date-js').on('datepicker.change', () => {
            setMinDateById('joining-date-js', getDateStringById('promotion-date-js'));
        });

        <%
      if(actionName.equals("edit")) {
          Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(promotion_historyDTO.officeUnitsId);
          String newOfficeName = isLanguageEnglish?office_unitsDTO.unitNameEng:office_unitsDTO.unitNameBng;

          OfficeUnitOrganograms office_unit_organogram = OfficeUnitOrganogramsRepository.getInstance().getById(promotion_historyDTO.rankCat);
          String newDesignation = isLanguageEnglish?office_unit_organogram.designation_eng:office_unit_organogram.designation_bng;
         %>

        setDateByTimestampAndId('promotion-date-js', <%=promotion_historyDTO.promotionDate%>);
        setDateByTimestampAndId('joining-date-js', <%=promotion_historyDTO.joiningDate%>);
        setDateByTimestampAndId('gO-date-js', <%=promotion_historyDTO.gODate%>);
        $("#office-unit").val('<%=newOfficeName%>');
        $("#designation").val('<%=newDesignation%>');

        <%}
        %>
    });

    function buttonStateChangeFunction(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

</script>

<style>
    .promotion-cross-picker {
        height: 34px;
        width: 41px;
        border: 1px solid #c2cad8;
    }

    .input-width89 {
        width: 92%;
        float: left;
    }

    .padding-top7 {
        padding-top: 7px;
    }
</style>