<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="dbm.*" %>
<%@ page import="pb.*" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="user.UserDTO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String actionName = "add";
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    int i = 0;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String empId = request.getParameter("empId") != null ? request.getParameter("empId") : null;
    long employeeRecordsId;
    if (empId == null) {
        employeeRecordsId = UserRepository.getUserDTOByUserID(loginDTO).employee_record_id;
    } else {
        employeeRecordsId = Long.parseLong(empId);
    }

    String url = "Promotion_historyServlet?actionType=ajax_addPreviousPromotion&isPermanentTable=true&empId=" + employeeRecordsId + "&iD=" + ID;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab") + "&userId=" + request.getParameter("userId");
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=isLanguageEnglish ? "PREVIOUS PROMOTION HISTORY" : "পূর্ববর্তী পদন্নোতি ইতিহাস"%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form" id="promotion-history" enctype="multipart/form-data" name="bigform"
              action="<%=url%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: white"><%=isLanguageEnglish ? "PREVIOUS PROMOTION HISTORY" : "পূর্ববর্তী পদন্নোতি ইতিহাস"%>
                                            </h4>
                                        </div>
                                        <%--Input Fields--%>
                                        <%--                                        Previous Office and Designation--%>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                            </label>
                                            <div class="col-md-9" id='previous_office-unit_button_<%=i%>'>
                                                <button type="button" class="btn btn-primary form-control"
                                                        id="addPreviousOffice_modal_button">
                                                    <%=isLanguageEnglish ? "Add Previous Office and Designation" : "পূর্ববর্তী অফিস এবং পদবি যুক্ত করুন"%>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=isLanguageEnglish ? "Previous Office" : "পূর্ববর্তী অফিস"%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='previous-office-unit_div_<%=i%>'>
                                                <input type="text" class="form-control" id="previous-office-unit"
                                                       name="previousOfficeUnitsId">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=isLanguageEnglish ? "Previous Designation" : "পূর্ববর্তী পদবি"%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='previousRankCat_div_<%=i%>'>
                                                <input type="text" class="form-control" id="previousDesignation"
                                                       name="previousRankCat">
                                                <input type="text" class="form-control" id="previousRank"
                                                       name="previousRank" hidden>


                                            </div>
                                        </div>
                                        <%--                                        Previous Office and Designation--%>


                                        <%--                                        New Office and Designation--%>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                            </label>
                                            <div class="col-md-9" id='office-unit_button_<%=i%>'>
                                                <button type="button" class="btn btn-primary form-control"
                                                        id="addOffice_modal_button">
                                                    <%=isLanguageEnglish ? "Add Following Office and Designation" : "পরবর্তী অফিস এবং পদবি যুক্ত করুন"%>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=isLanguageEnglish ? "Following Office" : "পরবর্তী অফিস"%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='office-unit_div_<%=i%>'>
                                                <input type="text" class="form-control" id="office-unit"
                                                       name="officeUnitsId" hidden>
                                                <input type="text" class="form-control" id="office-unit-text"
                                                       name="officeUnitsText">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=isLanguageEnglish ? "Following Designation" : "পরবর্তী পদবি"%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9" id='rankCat_div_<%=i%>'>
                                                <input type="text" class="form-control" id="rankCat"
                                                       name="rankCat" hidden>
                                                <input type="text" class="form-control" id="designation"
                                                       name="rankText">
                                            </div>
                                        </div>
                                        <%--                                        New Office and Designation--%>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONDATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9" id='promotionDate_div_<%=i%>'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID"
                                                               value="promotion-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' class='form-control'
                                                       id='promotion-date' name='promotionDate' value=''
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PROMOTION_HISTORY_ADD_GODATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9" id='gODate_div_<%=i%>'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID"
                                                               value="gO-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>

                                                </jsp:include>
                                                <input type='hidden' class='form-control' id='gO-date'
                                                       name='gODate' value='' tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.EMPLOYEE_RECORD_JOINING_DATE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9" id='joiningDate_div_<%=i%>'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID"
                                                               value="joining-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE"
                                                               value="<%=Language%>"></jsp:param>

                                                </jsp:include>
                                                <input type='hidden' class='form-control' id='joining-date'
                                                       name='joiningDate' value=''
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONNATURECAT, loginDTO)%>
                                                <span>*</span>
                                            </label>
                                            <div class="col-md-9" id='promotionNatureCat_div_<%=i%>'>
                                                <select class='form-control' name='promotionNatureCat'
                                                        id='promotionNatureCat'
                                                        tag='pb_html'>
                                                    <%=CatRepository.getInstance().buildOptions("promotion_nature", Language, null)%>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.PROMOTION_HISTORY_ADD_FILESDROPZONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9" id='filesDropzone_div_<%=i%>'>
                                                <div class="dropzone"
                                                     action="Promotion_historyServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=DBMW.getInstance().getNextSequenceId("fileid")%>">
                                                    <input type='file' style="display:none"
                                                           name='filesDropzoneFile'
                                                           id='filesDropzone_dropzone_File_<%=i%>'
                                                           tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                                       id='filesDropzoneFilesToDelete_<%=i%>'
                                                       value='' tag='pb_html'/>
                                                <input type='hidden' name='filesDropzone'
                                                       id='filesDropzone_dropzone_<%=i%>' tag='pb_html'/>
                                            </div>
                                        </div>

                                        <%--Template--%>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                            </label>
                                            <div class="col-md-9">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Hidden Fields--%>
            <input type='hidden' class='form-control' name='employeeRecordsId'
                   id='employeeRecordsId_hidden_<%=i%>'
                   value=<%=employeeRecordsId%> tag='pb_html'/>
            <%--            <input type='hidden' class='form-control' name='previousOffice'--%>
            <%--                   id='previousOffice'--%>
            <%--                   value='<%=promotion_historyDTO.previousOffice%>' tag='pb_html'/>--%>

            <div class="row mt-3">
                <div class="col-md-10 text-right">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"
                       id="cancel-btn">
                        <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTION_HISTORY_CANCEL_BUTTON, loginDTO)%>
                    </a>
                    <button class="btn btn-success" onclick="submitItem('promotion-history')"
                            id="submit-btn" type="button">
                        <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTION_HISTORY_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="../office_assign/officeSearchModal.jsp" %>

<script type="text/javascript">
    let isEnglish = <%=isLanguageEnglish%>;
    let isShowTable = 1;
    let officeName = '';
    let officeUnitOrganogramName = '';
    let officeModalIdentifier = '';
    let officeId = -1;
    let officeUnitOrganogramId = -1;

    function submitItem(id) {
        let submitFunction = () => {
            let formName = '#' + id;
            const form = $(formName);

            buttonStateChangeFunction(true);
            if (isFormValid(form)) {
                submitAjaxForm(id);

            } else {
                buttonStateChangeFunction(false);
            }
        }
        if (isEnglish) {
            messageDialog('Do you want to submit?', "You won't be able to revert this!", 'success', true, 'Submit', 'Cancel', submitFunction);
        } else {
            messageDialog('সাবমিট করতে চান?', "সাবমিটের পর পরিবর্তনযোগ্য না!", 'success', true, 'সাবমিট', 'বাতিল', submitFunction);
        }
    }

    $(document).ready(function () {

        select2SingleSelector("#promotionNatureCat", "<%=Language%>");
        <%
            long curTime=Calendar.getInstance().getTimeInMillis();
        %>
        setMaxDateByTimestampAndId("promotion-date-js", <%=curTime%>);
        setMaxDateByTimestampAndId("gO-date-js", <%=curTime%>);
        setMaxDateByTimestampAndId("joining-date-js", <%=curTime%>);
        $.validator.addMethod('promotionNatureSelection', function (value, element) {
            return value != 0;
        });
        $("#promotion-history").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                promotionNatureCat: {
                    required: true,
                    promotionNatureSelection: true
                }
            },
            messages: {
                promotionNatureCat: "<%=LM.getText(LC.PROMOTION_HISTORY_ADD_ENTER_PROMOTION_NATURE, loginDTO)%>"
            }
        });
    });

    function isFormValid($form) {
        if (officeId > 0)
            $("#office-unit").val(officeId);
        if (officeUnitOrganogramId > 0)
            $("#rankCat").val(officeUnitOrganogramId);

        $('#promotion-date').val(getDateStringById('promotion-date-js'));
        $('#joining-date').val(getDateStringById('joining-date-js'));
        $('#gO-date').val(getDateStringById('gO-date-js'));

        const jQueryValid = $form.valid();

        return jQueryValid;
    }

    $('#addOffice_modal_button').on('click', function () {
        officeModalIdentifier = 'addOffice_modal_button';
        $('#search_office_modal').modal();
    });

    $('#addPreviousOffice_modal_button').on('click', function () {
        officeModalIdentifier = 'addPreviousOffice_modal_button';
        $('#search_office_modal').modal();
    });

    function triggerOfficeUnitOrganogramId() {
        if (officeModalIdentifier === 'addOffice_modal_button') {
            $("#office-unit-text").val(officeName);
            $("#designation").val(officeUnitOrganogramName);
        } else if (officeModalIdentifier === 'addPreviousOffice_modal_button') {
            $("#previous-office-unit").val(officeName);
            $("#previousDesignation").val(officeUnitOrganogramName);
            // $("#previousOffice").val(officeId);
            $("#previousRank").val(officeUnitOrganogramId);
        }
        officeModalIdentifier = '';
    }

    $(function () {

        $('#promotion-date-js').on('datepicker.change', () => {
            setMinDateById('joining-date-js', getDateStringById('promotion-date-js'));
        });
    });

    function buttonStateChangeFunction(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

</script>