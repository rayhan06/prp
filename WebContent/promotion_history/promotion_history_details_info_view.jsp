<%@page pageEncoding="UTF-8" %>
<%@page import="java.util.List" %>
<%@page import="promotion_history.Promotion_historyDTOWithValues" %>
<%@ page import="employee_offices.EmployeeOfficesDAO" %>
<%@ page import="employee_office_report.InChargeLevelEnum" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th><b><%= LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONDATE, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.PROMOTION_NEW_DESIGNATION, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONNATURECAT, loginDTO) %>
        </b></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<EmployeeOfficeDTO> currentActiveOfficeList = EmployeeOfficesDAO.getInstance()
                .getActiveByEmployeeRecordIdAndInChargeLevel(Long.parseLong(ID), InChargeLevelEnum.ROUTINE_RESPONSIBILITY);
        System.out.println(currentActiveOfficeList.size());
        List<Promotion_historyDTOWithValues> promotion_history_details = (List<Promotion_historyDTOWithValues>) request.getAttribute("promotionHistoryDetails");
        int promotionIndex = 0;
        if (promotion_history_details != null && promotion_history_details.size() > 0) {
            for (Promotion_historyDTOWithValues promotion_history_detail : promotion_history_details) {
                ++promotionIndex;

    %>
    <tr>
        <%@include file="/promotion_history/promotion_history_details_info_view_item.jsp" %>
    </tr>
    <% } } %>
    </tbody>
</table>

<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='Promotion_historyServlet?actionType=getPreviousPromotionAddPage&empId=<%=ID%>&tab=3&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp; <%=isLanguageEnglish ? "Add Previous Promotion" : "পূর্ববর্তী পদন্নোতি যোগ করুন"%>
        </button>
        <%
            if(!currentActiveOfficeList.isEmpty()){
        %>
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='Promotion_historyServlet?actionType=getAddPage&empId=<%=ID%>&tab=3&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp; <%= LM.getText(LC.PROMOTION_HISTORY_INFO_ADD_PROMOTION, loginDTO)%>
        </button>
        <%
            }
        %>

    </div>
</div>

<%
    if(currentActiveOfficeList.isEmpty()){
%>
<div class="row">
    <p style="color: red;margin: 1rem;font-size: medium"><%=isLanguageEnglish
            ?"Current this employee is not assigned as "+InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getEngText().toLowerCase()+" in any post !!"
            :"বর্তমানে এই কর্মকর্তা কোন পদে "+InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getBngText()+ " হিসেবে নিযুক্ত নন !!"%>
    </p>
</div>
<%
    }
%>