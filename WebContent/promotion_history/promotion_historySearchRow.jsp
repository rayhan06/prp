<%@page import="employee_records.Employee_recordsDAO" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="util.StringUtils" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="pb.CatRepository" %>

<td>
    <%=Employee_recordsDAO.getEmployeeName(promotion_historyDTO.employeeRecordsId, Language)%>
</td>

<td>
    <%
        if (promotion_historyDTO.officeUnitsId != 0) {
    %>
    <%=Office_unitsRepository.getInstance().geText(Language, promotion_historyDTO.officeUnitsId)%>
    <%} else {%>
    <%=promotion_historyDTO.officeUnitsText%>
    <%
        }
    %>
</td>

<td>

    <%
        if (promotion_historyDTO.rankCat != 0) {
    %>
    <%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, promotion_historyDTO.rankCat)%>
    <%} else {%>
    <%=promotion_historyDTO.rankCatText%>
    <%
        }
    %>

</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "promotion_nature", promotion_historyDTO.promotionNatureCat)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, promotion_historyDTO.promotionDate)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, promotion_historyDTO.joiningDate)%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Promotion_historyServlet?actionType=view&ID=<%=promotion_historyDTO.iD%>&empId=<%=promotion_historyDTO.employeeRecordsId%>'">
        <i class="fa fa-eye"></i>
    </button>

</td>