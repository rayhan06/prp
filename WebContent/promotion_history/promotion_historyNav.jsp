<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="pb.*" %>

<%@page pageEncoding="UTF-8" %>

<%
    String url = "Promotion_historyServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <div class="ml-1">
            <!-- Each Row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <%--Office Select Modal Button--%>
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="newOfficeUnit_modal_button">
                                <%=LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO)%>
                            </button>
                            <div class="input-group" id="office_units_id_div" style="display: none">
                                <input type="hidden" id='office_units_id' name="office_units_id" value='' tag='pb_html'>
                                <button type="button"
                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                        id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger"
                                            id='office_units_id_crs_btn' tag='pb_html'>
                                        x
                                    </button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.PROMOTION_HISTORY_ADD_RANKCAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="organogram_id_modal_button" onclick="organogramIdModalBtnClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <div class="input-group" id="organogram_id_div" style="display: none">
                                <input type="hidden" name='officeUnitOrganogramId' id='organogram_id_input' value="">
                                <button type="button" class="btn btn-secondary form-control" disabled
                                        id="organogram_id_text"></button>
                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                <button type="button" class="btn btn-outline-danger"
                                        onclick="crsBtnClicked('organogram_id');"
                                        id='organogram_id_crs_btn' tag='pb_html'>
                                    x
                                </button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="promotion_date_start_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' id="promotion_date_start" name="promotion_date_start" value=''
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="promotion_date_end_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' id="promotion_date_end" name="promotion_date_end" value=''
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.PROMOTION_HISTORY_ADD_JOININGDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="joining_date_start_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' id="joining_date_start" name="joining_date_start" value=''
                                   tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.PROMOTION_HISTORY_ADD_JOININGDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="joining_date_end_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden' id="joining_date_end" name="joining_date_end" value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONNATURECAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' style="width: 100%" name='promotion_nature_cat'
                                    id='promotion_nature_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("promotion_nature", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<%
    String modalTitle = Language.equalsIgnoreCase("English") ? "Find Designation" : "পদবী খুঁজুন";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
    <jsp:param name="modalTitle" value="<%=modalTitle%>"/>
</jsp:include>

<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    function fetchDesignation(officeUnitId) {
        let url = "Promotion_historyServlet?actionType=getDesignationOption&office_unit_id=" + officeUnitId + "&language=<%=Language%>";
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                console.log(fetchedData);
                document.getElementById('rank_cat').innerHTML = fetchedData;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#office_units_id").val()) {
            params += '&office_units_id=' + $("#office_units_id").val();
        }

        if ($("#organogram_id_input").val()) {
            params += '&rank_cat=' + $("#organogram_id_input").val();
        }

        $("#promotion_date_start").val(getDateStringById("promotion_date_start_date_js"));
        if ($('#promotion_date_start').val()) {
            params += '&promotion_date_start=' + getBDFormattedDate('promotion_date_start');
        }

        $("#promotion_date_end").val(getDateStringById("promotion_date_end_date_js"));
        if ($('#promotion_date_end').val()) {
            params += '&promotion_date_end=' + getBDFormattedDate('promotion_date_end');
        }

        $("#joining_date_start").val(getDateStringById("joining_date_start_date_js"));
        if ($('#joining_date_start').val()) {
            params += '&joining_date_start=' + getBDFormattedDate('joining_date_start');
        }

        $("#joining_date_end").val(getDateStringById("joining_date_end_date_js"));
        if ($('#joining_date_end').val()) {
            params += '&joining_date_end=' + getBDFormattedDate('joining_date_end');
        }

        if ($("#promotion_nature_cat").val()) {
            params += '&promotion_nature_cat=' + $("#promotion_nature_cat").val();
        }


        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    $(function () {
        select2SingleSelector('#promotion_nature_cat', '<%=Language%>');
    });

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }

        console.log(selectedOffice);

        $('#newOfficeUnit_modal_button').hide();
        $('#office_units_id_div').show();

        $('#office_units_id_div').show();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id').val(selectedOffice.id);

        fetchDesignation(selectedOffice.id);
    }

    $('#office_units_id_crs_btn').on('click', function () {
        $('#newOfficeUnit_modal_button').show();
        $('#office_units_id_div').hide();

        $('#office_units_id').val('');
        document.getElementById('office_units_id_text').innerHTML = '';
        document.getElementById('rank_cat').innerHTML = '';
    });

    // Office Select Modal
    // modal trigger button
    // this part is needed because if one may have more than one place to select office
    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['newOfficeUnit', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    $('#newOfficeUnit_modal_button').on('click', function () {
        officeSelectModalUsage = 'newOfficeUnit';
        $('#search_office_modal').modal();
    });

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'newOfficeUnit';
        officeSearchSetSelectedOfficeLayers($('#office_units_id').val());
        $('#search_office_modal').modal();
    }

    function viewOgranogramIdInInput(empInfo) {
        console.log(empInfo);

        $('#organogram_id_modal_button').hide();
        $('#organogram_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            designation = empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('organogram_id_text').innerHTML = designation;
        $('#organogram_id_input').val(empInfo.organogramId);
    }

    table_name_to_collcetion_map = new Map([
        ['organogramId', {
            isSingleEntry: true,
            callBackFunction: viewOgranogramIdInInput
        }]
    ]);

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function organogramIdModalBtnClicked() {
        modal_button_dest_table = 'organogramId';
        $('#search_emp_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        ajaxSubmit();
    }
</script>

