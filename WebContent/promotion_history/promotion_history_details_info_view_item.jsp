<td style="vertical-align: middle;"><%=isLanguageEnglish ? promotion_history_detail.newOfficeUnitEn : promotion_history_detail.newOfficeUnitBn%>
</td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? promotion_history_detail.promotionDateEn : promotion_history_detail.promotionDateBn%>
</td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? promotion_history_detail.newDesignationEn : promotion_history_detail.newDesignationBn%>
</td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? promotion_history_detail.promotionNatureTypeEn : promotion_history_detail.promotionNatureTypeBn%>
</td>
<%
    if (!actionType.equals("viewSummary")) {
%>
<td style="text-align: center; vertical-align: middle;">
    <form action="Promotion_historyServlet?isPermanentTable=true&actionType=delete&tab=5&ID=<%=promotion_history_detail.dto.iD%>&empId=<%=ID%>"
          method="POST" id="promotionHistoryTableForm<%=promotionIndex%>" enctype="multipart/form-data">

        <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn-primary" title="View" type="button"
                  onclick="location.href='Promotion_historyServlet?actionType=view&ID=<%=promotion_history_detail.dto.iD%>&empId=<%=promotion_history_detail.dto.employeeRecordsId%>&userId=<%=request.getParameter("userId")%>'"><i
                    class="fa fa-eye"></i></button>
            &nbsp;
<%--            <button class="btn-success" title="Edit" type="button"--%>
<%--                  onclick="location.href='Promotion_historyServlet?actionType=getEditPage&ID=<%=promotion_history_detail.dto.iD%>&empId=<%=ID%>&tab=3'"><i--%>
<%--                    class="fa fa-edit"></i></button>--%>
            &nbsp;
<%--            <button class="btn-danger" title="Delete" type="button"--%>
<%--                  onclick="deleteItem('promotionHistoryTableForm',<%=promotionIndex%>)"><i--%>
<%--                    class="fa fa-trash"></i></button></div>--%>
    </form>
</td>
<%
    }
%>