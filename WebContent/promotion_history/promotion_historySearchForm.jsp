<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="promotion_history.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "";
    String servletName = "Promotion_historyServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<Promotion_historyDTO> data = (List<Promotion_historyDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_NAMEOFEMPLOYEE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PROMOTION_NEW_OFFICE_UNIT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PROMOTION_HISTORY_ADD_RANKCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONNATURECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PROMOTION_HISTORY_ADD_PROMOTIONDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PROMOTION_HISTORY_ADD_JOININGDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            for (Promotion_historyDTO promotion_historyDTO : data) {
        %>
        <tr>
            <%@include file="promotion_historySearchRow.jsp" %>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>