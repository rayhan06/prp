<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.StringUtils" %>

<%@ page import="training_calendar_details.Training_calendar_detailsDTO" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDAO" %>

<td >
    <%=training_calenderDTO.nameEn%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "training_type", training_calenderDTO.trainingTypeCat)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, training_calenderDTO.startDate)%>
</td>


<td>
    <%=StringUtils.getFormattedDate(Language, training_calenderDTO.endDate)%>
</td>

<td>
    <%=training_calenderDTO.arrangedBy == 0? "" : CatRepository.getInstance().getText(Language,"training_arrange_by",training_calenderDTO.arrangedBy)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "training_mode", training_calenderDTO.trainingModeCat)%>
</td>
<td>
    <%=CatRepository.getInstance().getText(Language, "training_status", training_calenderDTO.status)%>
</td>

<td>
    <%
        Training_calendar_detailsDTO training_calendar_detailsDTO = new Training_calendar_detailsDAO().getDTOByCalendarIdAndEmployeeRecordId(training_calenderDTO.iD, userDTO.employee_record_id);
        boolean isAbleToSubmitFeedback = training_calenderDTO.status == TrainingCalendarStatusEnum.COMPLETED.getValue()
                                        && !training_calendar_detailsDTO.isFeedBackDone
                                        && !training_calenderDTO.isPrivate;
        if (isAbleToSubmitFeedback) {
    %>
    <a href='FeedbackQuestionAnswerServlet?trainCalId=<%=training_calenderDTO.iD%>'>Feedback</a>
    <%
        }
    %>

</td>

<td>
    <a href='Training_calenderServlet?actionType=view&ID=<%=training_calenderDTO.iD%>'>View</a>
</td>