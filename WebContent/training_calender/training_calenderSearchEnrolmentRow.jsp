<%@page pageEncoding="UTF-8" %>
<%@ page import="util.StringUtils" %>
<%@ page import="pb.CatRepository" %>

<%if ("English".equalsIgnoreCase(Language)) {%>

<td>
    <%=training_calenderDTO.nameEn%>
</td>

<%} else {%>

<td>
    <%=training_calenderDTO.nameBn%>
</td>

<%}%>

<td>
    <%=CatRepository.getInstance().getText(Language, "training_type", training_calenderDTO.trainingTypeCat)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, training_calenderDTO.startDate)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, training_calenderDTO.endDate)%>
</td>

<td>
    <%=training_calenderDTO.arrangedBy == 0? "" : CatRepository.getInstance().getText(Language,"training_arrange_by",training_calenderDTO.arrangedBy)%>
</td>


<td>
    <%=CatRepository.getInstance().getText(Language, "training_mode", training_calenderDTO.trainingModeCat)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "training_status", training_calenderDTO.status)%>
</td>
<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Training_calenderServlet?actionType=view&ID=<%=training_calenderDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <a href='Training_calenderServlet?actionType=addEnrolment&ID=<%=training_calenderDTO.iD%>'>
        <%=LM.getText(LC.TRAINING_CALENDER_ENROLL, loginDTO)%>
    </a>
</td>