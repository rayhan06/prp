<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="training_calender.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>

<%@ page import="pb.*" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="training_calendar_details.TrainerDAO" %>
<%@ page import="training_calendar_details.TrainerDTO" %>
<%@ page import="java.util.stream.Collectors" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    Training_calenderDTO training_calenderDTO = (Training_calenderDTO) request.getAttribute("training_calenderDTO");
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (training_calenderDTO == null) {
        training_calenderDTO = new Training_calenderDTO();
    }
    String context = request.getContextPath() + "/";

    String actionName;
    List<TrainerDTO> trainerDTOList = new ArrayList<>();
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
        String ID = request.getParameter("ID");
        if (ID == null || ID.isEmpty()) {
            ID = "0";
        }
        long id = Long.parseLong(ID);
        training_calenderDTO = Training_calenderDAO.getInstance().getDTOFromID(id);
        trainerDTOList = new TrainerDAO().getByForeignKey(String.valueOf(id));
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.TRAINING_CALENDER_EDIT_TRAINING_CALENDER_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.TRAINING_CALENDER_ADD_TRAINING_CALENDER_ADD_FORMNAME, loginDTO);
    }

    String trainingInfoTitle = LM.getText(LC.TRAINING_CALENDER_ADD_TRAINING_CALENDER_TRAINING_INFO, loginDTO);

    String trainingDateTimeTitle = LM.getText(LC.TRAINING_CALENDER_ADD_TRAINING_CALENDER_DATE_AND_TIME, loginDTO);

    String bondTitle = LM.getText(LC.TRAINING_CALENDER_ADD_TRAINING_CALENDER_BOND, loginDTO);

    String enrollmentTitle = LM.getText(LC.TRAINING_CALENDER_ADD_TRAINING_CALENDER_ENROLLMENT, loginDTO);

    String trainerInfoTitle = LM.getText(LC.TRAINING_CALENDER_TRAINER_INFO, loginDTO);

    String miscellaneousTitle = LM.getText(LC.TRAINING_CALENDER_ADD_TRAINING_CALENDER_MISCELLANEOUS, loginDTO);
    int year = Calendar.getInstance().get(Calendar.YEAR);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    String empId = request.getParameter("empId");
    if (empId == null || empId.isEmpty()) {
        empId = "0";
    }
    int i = 0;
    String value;
    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();

    boolean canValidate = false;

    String url = "Training_calenderServlet?actionType=ajax_" + actionName + "&isPermanentTable=true&empId=" + empId;

    String tab = request.getParameter("tab");
    if (tab != null) url += "&tab=" + tab + "&userId=" + request.getParameter("userId");

    String data = request.getParameter("data");
    if (data != null) url += "&data=" + data;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="training-calender" name="training-calender">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <input type='hidden' class='form-control' name='iD'
                                               id='iD_hidden_<%=i%>'
                                               value='<%=training_calenderDTO.iD%>'/>
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h3 style="margin-left: 1%;"><%=trainingInfoTitle%>
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_NAMEEN, loginDTO)%>
                                                        <span class="required"> *</span>
                                                    </label>
                                                    <div class="col-md-10" id='nameEn_div_<%=i%>'>
                                                        <input type='text' class='form-control' required
                                                               name='nameEn' id='nameEn_text_<%=i%>'
                                                               placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_TRAINING_CALENDAR_NAME_IN_ENGLISH, loginDTO)%>'
                                                               value='<%=training_calenderDTO.nameEn == null ? "" : training_calenderDTO.nameEn%>'/>

                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_NAMEBN, loginDTO)%>
                                                        <span class="required"> *</span>
                                                    </label>
                                                    <div class="col-md-10" id='nameBn_div_<%=i%>'>
                                                        <input type='text' class='form-control'
                                                               name='nameBn' required
                                                               placeholder='<%=LM.getText(LC.PALCEHOLDER_ENTER_TRAINING_CALENDAR_NAME_IN_BANGLA, loginDTO)%>'
                                                               value='<%=training_calenderDTO.nameBn == null ? "" : training_calenderDTO.nameBn%>'/>

                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right" for="trainingCategoryCat">
                                                        <%=isLanguageEnglish ? "Training Category" : "প্রশিক্ষণ ক্যাটাগরি"%>
                                                        <span class="required"> *</span>
                                                    </label>
                                                    <div class="col-md-10">
                                                        <select class='form-control'
                                                                name='trainingCategoryCat' required
                                                                id='trainingCategoryCat'>
                                                            <%=CatRepository.getInstance().buildOptions("training_category", Language, training_calenderDTO.trainingModeCat)%>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_TRAININGMODECAT, loginDTO)%>
                                                        <span class="required"> *</span>
                                                    </label>
                                                    <div class="col-md-10">
                                                        <select class='form-control'
                                                                name='trainingModeCat' required
                                                                id='trainingModeCat_category_<%=i%>'>
                                                            <%=CatRepository.getInstance().buildOptions("training_mode", Language, training_calenderDTO.trainingModeCat)%>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_TRAININGTYPECAT, loginDTO)%>
                                                        <span class="required"> *</span>
                                                    </label>
                                                    <div class="col-md-10">
                                                        <select class='form-control' name='trainingTypeCat' required
                                                                id='trainingTypeCat_category_<%=i%>'>
                                                            <%=CatRepository.getInstance().buildOptions("training_type", Language, training_calenderDTO.trainingTypeCat)%>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_VENUE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-10">
                                                                <textarea
                                                                        type='text'
                                                                        class='form-control'
                                                                        name='venue'
                                                                        id='venue_text_<%=i%>'
                                                                        rows="4"
                                                                        placeholder='<%=LM.getText(LC.PLACEHOLDER_ENTER_VENUE, loginDTO)%>'
                                                                        style="text-align: left;resize: none; width: 100%"><%=training_calenderDTO.venue == null ? "" : training_calenderDTO.venue.trim()%></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right"
                                                           id='instituteName_label'>
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_INSTITUTENAME, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-10">
                                                        <input type='text' class='form-control'
                                                               name='instituteName'
                                                               id='instituteName_text'
                                                               placeholder='<%=LM.getText(LC.PLACEHOLDER_ENTER_INSTITUTE_NAME, loginDTO)%>'
                                                               value='<%=training_calenderDTO.instituteName == null ? "" :training_calenderDTO.instituteName%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                </div>

                                                <div class="form-group row" id="country_div">
                                                    <label class="col-md-2 col-form-label text-md-right"
                                                           id='country_label'><%=LM.getText(LC.EMPLOYEE_POSTING_ADD_COUNTRY, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-10">
                                                        <select class='form-control' name='country'
                                                                style="width: 100%" onchange="countrySelect()"
                                                                id='country_select' tag='pb_html'>
                                                            <%=GeoCountryRepository.getInstance().buildOptions(Language, training_calenderDTO.country)%>
                                                        </select>
                                                        <span id="country_error"
                                                              style="color:#fd397a; text-align:center;margin-top: 1%; font-size: smaller;display: none">
                                                            <%=isLanguageEnglish ? "Please select country" : "অনুগ্রহ করে দেশ বাছাই করুন"%>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h3 style="margin-left: 1%;"><%=trainingDateTimeTitle%>
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group row">

                                                    <label class="col-xl-2 col-form-label text-xl-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_STARTDATE, loginDTO)%>
                                                        <%
                                                            if (empId.equalsIgnoreCase("0")) {
                                                        %>
                                                        <span class="required"> *</span>
                                                        <%
                                                            }
                                                        %>
                                                    </label>
                                                    <div class="col-xl-4">
                                                        <div id='startDate_div_<%=i%>'>
                                                            <div class="">
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="startDate_js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                    <jsp:param name="END_YEAR"
                                                                               value="<%=year + 5%>"></jsp:param>
                                                                </jsp:include>
                                                            </div>
                                                            <input type='hidden' class='form-control'
                                                                   id='startDate' name='startDate' value=''
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>


                                                    <label class="col-xl-2 col-form-label text-xl-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_ENDDATE, loginDTO)%>
                                                        <%
                                                            if (empId.equalsIgnoreCase("0")) {
                                                        %>
                                                        <span class="required"> *</span>
                                                        <%
                                                            }
                                                        %>
                                                    </label>
                                                    <div class="col-xl-4">
                                                        <div id='endDate_div_<%=i%>'>
                                                            <div class="">
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="endDate_js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                    <jsp:param name="END_YEAR"
                                                                               value="<%=year + 5%>"/>
                                                                </jsp:include>
                                                            </div>
                                                            <input type='hidden' class='form-control'
                                                                   id='endDate' name='endDate' value=''
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="form-group row">

                                                    <label class="col-xl-2 col-form-label text-xl-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_STARTTIME, loginDTO)%>
                                                    </label>
                                                    <div class="col-xl-4">
                                                        <div class="" id='startTime_div_<%=i%>'>
                                                            <%
                                                                value = "";
                                                                if (actionName.equals("edit")) {
                                                                    value = TimeFormat.getInAmPmFormatUpdated(training_calenderDTO.startTime);
                                                                }
                                                            %>
                                                            <div class="">
                                                                <jsp:include page="/time/time.jsp">
                                                                    <jsp:param name="TIME_ID"
                                                                               value="startTime_js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                    <jsp:param name="IS_AMPM"
                                                                               value="false"></jsp:param>
                                                                </jsp:include>
                                                            </div>

                                                            <input type='hidden' class='form-control'
                                                                   name='startTime' id='startTime_time_'
                                                                   value='<%=value%>' tag='pb_html'/>


                                                        </div>
                                                    </div>


                                                    <label class="col-xl-2 col-form-label text-xl-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_ENDTIME, loginDTO)%>
                                                    </label>
                                                    <div class="col-xl-4">
                                                        <div class="" id='endTime_div_<%=i%>'>
                                                            <%
                                                                value = "";
                                                                if (actionName.equals("edit")) {
                                                                    value = TimeFormat.getInAmPmFormatUpdated(training_calenderDTO.endTime);
                                                                }
                                                            %>
                                                            <div class="">
                                                                <jsp:include page="/time/time.jsp">
                                                                    <jsp:param name="TIME_ID"
                                                                               value="endTime_js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                    <jsp:param name="IS_AMPM"
                                                                               value="false"></jsp:param>
                                                                </jsp:include>
                                                            </div>
                                                            <input type='hidden' class='form-control'
                                                                   name='endTime' id='endTime_time_'
                                                                   value='<%=value%>' tag='pb_html'/>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <%
                                            if (empId.equalsIgnoreCase("0")) {
                                        %>
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h3 style="margin-left: 1%;">
                                                    <%=bondTitle%>
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_ISBONDAPPLICABLE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-1">
                                                        <div class="" id='isBondApplicable_div_<%=i%>'>
                                                            <input type='checkbox' class='form-control-sm mt-1'
                                                                   name='isBondApplicable'
                                                                   id='isBondApplicable_checkbox_<%=i%>'
                                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(training_calenderDTO.isBondApplicable).equals("true"))?("checked"):""%>
                                                                   tag='pb_html'><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2"></div>


                                                    <label class="col-md-2 col-form-label text-md-right"
                                                           id='yearOfBond_number_label'>
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_YEAROFBOND, loginDTO)%>
                                                    </label>

                                                    <div class="col-md-4">
                                                        <div class="" id='yearOfBond_div'>
                                                            <input type='text' class='form-control'
                                                                   name='yearOfBond'
                                                                   id='yearOfBond_number'
                                                                   value='<%= training_calenderDTO.yearOfBond==null?  "": training_calenderDTO.yearOfBond%>'
                                                                   tag='pb_html'>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h3 style="margin-left: 1%;"><%=enrollmentTitle%>
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_ISENROLLMENTAPPLICALE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-1">
                                                        <div class="" id='isEnrollmentApplicale_div_<%=i%>'>
                                                            <input type='checkbox' class='form-control-sm mt-1'
                                                                   name='isEnrollmentApplicale'
                                                                   id='isEnrollmentApplicale_checkbox_<%=i%>'
                                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(training_calenderDTO.isEnrollmentApplicale).equals("true"))?("checked"):""%>
                                                                   tag='pb_html'><br>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2"></div>


                                                    <label class="col-md-2 col-form-label text-md-right"
                                                           id='lastEnrollmentDate_label_<%=i%>'>
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_LASTENROLLMENTDATE, loginDTO)%>
                                                        <span class="required"> *</span>
                                                    </label>


                                                    <div class="col-md-4">
                                                        <div id='lastEnrollmentDate_div_<%=i%>'>
                                                            <div class="container">
                                                                <jsp:include page="/date/date.jsp">
                                                                    <jsp:param name="DATE_ID"
                                                                               value="lastEnrollmentDate_js"></jsp:param>
                                                                    <jsp:param name="LANGUAGE"
                                                                               value="<%=Language%>"></jsp:param>
                                                                </jsp:include>
                                                            </div>
                                                            <input type='hidden' class='form-control'
                                                                   id='lastEnrollmentDate'
                                                                   name='lastEnrollmentDate' value=''
                                                                   tag='pb_html'/>
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h3 style="margin-left: 1%;"><%=trainerInfoTitle%>
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=isLanguageEnglish? "Internal Trainer" : "অভ্যন্তরীণ প্রশিক্ষক"%>
                                                    </label>
                                                    <div class="col-md-10" id="trainer_Div">
                                                        <!-- Button trigger modal -->
                                                        <button type="button"
                                                                class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                                id="addToTable_modal_button">
                                                            <%=LM.getText(LC.TRAINING_CALENDER_ADD_ADD_TRAINER, userDTO)%>
                                                        </button>

                                                        <div class="table-responsive">
                                                            <table class="table table-bordered table-striped text-nowrap">
                                                                <thead>
                                                                <tr>
                                                                    <th>
                                                                        <b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%>
                                                                        </b></th>
                                                                    <th>
                                                                        <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%>
                                                                        </b></th>
                                                                    <th>
                                                                        <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%>
                                                                        </b></th>
                                                                    <th></th>
                                                                </tr>
                                                                </thead>

                                                                <tbody id="tagged_emp_table">
                                                                <tr style="display: none;">
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>
                                                                        <button type="button"
                                                                                class="btn btn-danger btn-block"
                                                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                            x
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                                <%if (trainerDTOList != null) {%>
                                                                <%for (TrainerDTO detailsDTO : trainerDTOList) {%>
                                                                <tr>
                                                                    <td>
                                                                        <%=detailsDTO.employeeRecordsId%>
                                                                    </td>
                                                                    <td>
                                                                        <%=isLanguageEnglish ? detailsDTO.employeeNameEn : detailsDTO.employeeNameBn%>
                                                                    </td>
                                                                    <td>
                                                                        <%=isLanguageEnglish ? (detailsDTO.organogramNameEn + ", " + detailsDTO.officeNameEn)
                                                                                : (detailsDTO.organogramNameBn + ", " + detailsDTO.officeNameBn)%>
                                                                    </td>
                                                                    <td id='<%=detailsDTO.employeeRecordsId%>_td_button'>
                                                                        <button type="button"
                                                                                class="btn btn-danger btn-block"
                                                                                onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                            x
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                                <%}%>
                                                                <%}%>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <input type='hidden' class='form-control'
                                                               name='addedEmployees'
                                                               id='addedEmployees' value='' tag='pb_html'/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <%--20/12/2021: cleint asked for mixed type trainer.
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDAR_ADD_TRAINER_TYPE, loginDTO)%>
                                                        <span class="required"> *</span>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="" id='trainerCat_div_<%=i%>'>
                                                            <select class='form-control' name='trainerCat' required
                                                                    id='trainerCat_category_<%=i%>'
                                                                    tag='pb_html'>
                                                                <%=CatRepository.getInstance().buildOptions("trainer", Language, training_calenderDTO.trainerCat)%>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    --%>
                                                    <label class="col-md-2 col-form-label text-md-right external_trainer"
                                                           id='ext_trainer_info_<%=i%>'>
                                                        <%=LM.getText(LC.TRAINING_CALENDAR_ADD_EXTERNAL_TRAINER_INFO, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-10 external_trainer">
                                                        <textarea type='text'
                                                                  class='form-control'
                                                                  name='extTrainerInfo' rows="4" maxlength="4096"
                                                                  id='extTrainerInfo_id'
                                                                  placeholder='<%=LM.getText(LC.TRAINING_CALENDAR_ADD_ENTER_TRAINING_DETAILS,loginDTO)%>'
                                                                  style="text-align: left;resize: none"
                                                        ><%=training_calenderDTO.externalTrainerInfo%></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%
                                            }
                                        %>
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <h3 style="margin-left: 1%;"><%=miscellaneousTitle%>
                                                </h3>
                                            </div>

                                            <div class="card-body">
                                                <%
                                                    if (empId.equalsIgnoreCase("0")) {
                                                %>
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right" for="arrangedBy_text_<%=i%>">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_ARRANGEDBY, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <div class="" id='arrangedBy_div_<%=i%>'>
                                                            <select class='form-control' name='arrangedBy'
                                                                    id='arrangedBy_text_<%=i%>'
                                                                    tag='pb_html'>
                                                                <%=CatRepository.getInstance().buildOptions("training_arrange_by", Language, training_calenderDTO.arrangedBy)%>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDAR_ADD_TRAINING_COSTSTAKA, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-4" id='nameBn_div_<%=i%>'>

                                                        <input type='text' class='form-control'
                                                               name='trainingCost'
                                                               id='trainingCost_text_<%=i%>'
                                                               placeholder='<%=LM.getText(LC.TRAINING_CALENDAR_ADD_ENTER_TRAINING_COST,loginDTO)%>'
                                                               value='<%=actionName.equals("edit") ? training_calenderDTO.trainingCost : ""%>'
                                                               tag='pb_html'/>

                                                    </div>

                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right pr-0">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_MAXNUMBEROFPARTICIPANTS, loginDTO)%>
                                                        <span class="required"> *</span>
                                                    </label>
                                                    <div class="col-md-4"
                                                         id='maxNumberOfParticipants_div_<%=i%>'>
                                                        <input type='text' class='form-control'
                                                               name='maxNumberOfParticipants'
                                                               id='maxNumberOfParticipants_number'
                                                               value='<%=training_calenderDTO.maxNumberOfParticipants>0?training_calenderDTO.maxNumberOfParticipants:""%>'
                                                               tag='pb_html'>
                                                    </div>


                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_TARGETGROUPTYPE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-4" id='targetGroupType_div_<%=i%>'>

                                                        <input type='text' class='form-control'
                                                               name='targetGroupType'
                                                               id='targetGroupType_text_<%=i%>'
                                                               placeholder='<%=LM.getText(LC.PLACEHOLDER_ENTER_TARGET_GROUP, loginDTO)%>'
                                                               value='<%= training_calenderDTO.targetGroupType == null ? "" : training_calenderDTO.targetGroupType%>'
                                                               tag='pb_html'/>

                                                    </div>

                                                </div>
                                                <%-- 20/12/2021: Client Told To Remove: trainingPrerequisite, trainingObjective
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_TRAININGPREREQUISITE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-4" id='trainingPrerequisite_div_<%=i%>'>
                                                          <textarea type='text' class='form-control'
                                                                    name='trainingPrerequisite' rows="4"
                                                                    id='trainingPrerequisite_textarea_<%=i%>' style="text-align: left;
                                                             resize: none"><%=training_calenderDTO.trainingPrerequisite == null ? "" : training_calenderDTO.trainingPrerequisite%></textarea>
                                                    </div>

                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_TRAININGOBJECTIVE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-4" id='trainingObjective_div_<%=i%>'>
                                                        <textarea type='text' class='form-control'
                                                                  name='trainingObjective' rows="4"
                                                                  maxlength="2048"
                                                                  id='trainingObjective_textarea_<%=i%>' style="text-align: left;
                                                                  resize: none"><%=training_calenderDTO.trainingObjective == null ? "" : training_calenderDTO.trainingObjective%></textarea>
                                                    </div>
                                                </div>
                                                --%>
                                                <%
                                                    }
                                                %>
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=LM.getText(LC.TRAINING_CALENDER_ADD_TRAININGMATERIALSDROPZONE, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-10"
                                                         id='trainingMaterialsDropzone_div_<%=i%>'>
                                                        <%
                                                            if (actionName.equals("edit")) {
                                                                List<FilesDTO> trainingMaterialsDropzoneDTOList = filesDAO.getMiniDTOsByFileID(training_calenderDTO.trainingMaterialsDropzone);
                                                        %>
                                                        <table>
                                                            <tr>
                                                                <%
                                                                    if (trainingMaterialsDropzoneDTOList != null) {
                                                                        for (int j = 0; j < trainingMaterialsDropzoneDTOList.size(); j++) {
                                                                            FilesDTO filesDTO = trainingMaterialsDropzoneDTOList.get(j);
                                                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                                %>
                                                                <td id='trainingMaterialsDropzone_td_<%=filesDTO.iD%>'>
                                                                    <%
                                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                                    %>
                                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' alt=""/>
                                                                    <%
                                                                        }
                                                                    %>
                                                                    <a href='Training_calenderServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                                       download><%=filesDTO.fileTitle%>
                                                                    </a>
                                                                    <a class='btn btn-danger'
                                                                       onclick='deletefile(<%=filesDTO.iD%>, "trainingMaterialsDropzone_td_<%=filesDTO.iD%>", "trainingMaterialsDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                                </td>
                                                                <%
                                                                        }
                                                                    }
                                                                %>
                                                            </tr>
                                                        </table>
                                                        <%
                                                            }
                                                        %>

                                                        <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                                        <div class="dropzone"
                                                             action="Training_calenderServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=trainingMaterialsDropzone&ColumnID=<%=actionName.equals("edit")?training_calenderDTO.trainingMaterialsDropzone:ColumnID%>">
                                                            <input type='file' style="display:none"
                                                                   name='trainingMaterialsDropzoneFile'
                                                                   id='trainingMaterialsDropzone_dropzone_File_<%=i%>'
                                                                   tag='pb_html'/>
                                                        </div>
                                                        <input type='hidden'
                                                               name='trainingMaterialsDropzoneFilesToDelete'
                                                               id='trainingMaterialsDropzoneFilesToDelete_<%=i%>'
                                                               value='' tag='pb_html'/>
                                                        <input type='hidden'
                                                               name='trainingMaterialsDropzone'
                                                               id='trainingMaterialsDropzone_dropzone_<%=i%>'
                                                               tag='pb_html'
                                                               value='<%=actionName.equals("edit")?training_calenderDTO.trainingMaterialsDropzone:ColumnID%>'/>

                                                    </div>
                                                </div>


                                                <%
                                                    if (canValidate) {
                                                %>
                                                <div class="row div_border attachement-div">
                                                    <div class="col-md-12">
                                                        <h5><%=LM.getText(LC.HM_ATTACHMENTS, loginDTO)%>
                                                        </h5>
                                                        <%
                                                            ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                        %>
                                                        <div class="dropzone"
                                                             action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
                                                            <input type='file' style="display: none"
                                                                   name='approval_attached_fileDropzoneFile'
                                                                   id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
                                                                   tag='pb_html'/>
                                                        </div>
                                                        <input type='hidden'
                                                               name='approval_attached_fileDropzoneFilesToDelete'
                                                               id='approval_attached_fileDropzoneFilesToDelete_<%=i%>'
                                                               value=''
                                                               tag='pb_html'/> <input type='hidden'
                                                                                      name='approval_attached_fileDropzone'
                                                                                      id='approval_attached_fileDropzone_dropzone_<%=i%>'
                                                                                      tag='pb_html'
                                                                                      value='<%=ColumnID%>'/>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h5><%=LM.getText(LC.HM_REMARKS, loginDTO)%>
                                                        </h5>

                                                        <textarea class='form-control' name='remarks'
                                                                  id='<%=i%>_remarks'
                                                                  tag='pb_html'></textarea>
                                                    </div>
                                                </div>
                                                <%
                                                    }
                                                %>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3 text-right">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"
                           id="cancel-btn">
                            <%=LM.getText(LC.TRAINING_CALENDER_ADD_TRAINING_CALENDER_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                onclick="submitItem()"
                                id="submit-btn" type="button">
                            <%=LM.getText(LC.TRAINING_CALENDER_ADD_TRAINING_CALENDER_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    let isEnglish = <%=isLanguageEnglish%>;
    const trainingCalendarForm = $('#training-calender');
    const msg = isEnglish ? "Do you want to submit?" : "সাবমিট করতে চান?";
    const submitBtnText = isEnglish ? "Submit" : "সাবমিট";
    const cancelBtnText = isEnglish ? "Cancel" : "বাতিল";

    function submitItem() {
        buttonStateChange(true);
        if (isFormValid()) {
            messageDialog('', msg, 'success', true, submitBtnText, cancelBtnText, () => {
                $.ajax({
                    type: "POST",
                    url: "<%=url%>",
                    data: trainingCalendarForm.serialize(),
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.responseCode === 0) {
                            $('#toast_message').css('background-color', '#ff6063');
                            showToastSticky(response.msg, response.msg);
                            buttonStateChange(false);
                        } else if (response.responseCode === 200) {
                            window.location.replace(getContextPath() + response.msg);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                            + ", Message: " + errorThrown);
                        buttonStateChange(false);
                    }
                });
            });
        } else {
            buttonStateChange(false);
        }
    }

    $(document).ready(function () {


        showOrHideCountryDiv();
        <%
            if(empId.equalsIgnoreCase("0")){
        %>
        $('#maxNumberOfParticipants_number').keydown(e => {
            return inputValidationForIntValue(e, $('#maxNumberOfParticipants_number'), 20000);
        });
        $('#yearOfBond_number').keydown(e => {
            return inputValidationForIntValue(e, $('#yearOfBond_number'), 100);
        });

        // showOrHideTrainerDiv();
        showOrHideYearOfBondDiv();
        showOrHideLastEnrollmentDateDiv();
        $('#isBondApplicable_checkbox_0').change(function () {
            showOrHideYearOfBondDiv();
        });
        $('#isEnrollmentApplicale_checkbox_0').change(function () {
            showOrHideLastEnrollmentDateDiv();
        });
        select2SingleSelector('#trainerCat_category_0', '<%=Language%>');
        $.validator.addMethod('maxNumberOfParticipantsValue', function (value, element) {
            return value > 0;
        });
        <%
        }
        %>

        select2SingleSelector('#trainingCategoryCat', '<%=Language%>');
        select2SingleSelector('#trainingModeCat_category_0', '<%=Language%>');
        select2SingleSelector('#trainingTypeCat_category_0', '<%=Language%>');

        select2SingleSelector('#country_select', '<%=Language%>');
        // $('#trainerCat_category_0').change(function () {
        //     showOrHideTrainerDiv();
        // });
        $('#trainingTypeCat_category_0').change(function () {
            showOrHideCountryDiv();
        });


        dateTimeInit("<%=Language%>");


        $("#training-calender").validate({
            <% if(empId.equalsIgnoreCase("0")){ %>
            rules: {
                maxNumberOfParticipants: {
                    required: true,
                    maxNumberOfParticipantsValue: true
                }
            },
            <%}%>
            messages: {
                nameEn: '<%=LM.getText(LC.PALCEHOLDER_ENTER_TRAINING_CALENDAR_NAME_IN_ENGLISH, loginDTO)%>',
                nameBn: '<%=LM.getText(LC.PALCEHOLDER_ENTER_TRAINING_CALENDAR_NAME_IN_BANGLA, loginDTO)%>',
                trainingTypeCat: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_TYPE, loginDTO)%>',
                trainingCategoryCat: '<%=isLanguageEnglish ? "Select Training Category" : "প্রশিক্ষণ ক্যাটাগরি বাছাই করুন"%>',
                trainerCat: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINER_TYPE, loginDTO)%>',
                trainingModeCat: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_MODE, loginDTO)%>'
                <% if(empId.equalsIgnoreCase("0")){ %>
                ,
                maxNumberOfParticipants: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_VALID_MAXIMUM_NUMBER_OF_PARTICIPANTS, loginDTO)%>'
                <%}%>
            }
        });
    });

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }


    function isFormValid() {
        document.getElementById("startTime_time_").value = (getTimeById('startTime_js', false));
        document.getElementById("endTime_time_").value = (getTimeById('endTime_js', false));
        document.getElementById("startDate").value = (getDateStringById('startDate_js'));
        document.getElementById("endDate").value = (getDateStringById('endDate_js'));
        const jQueryValid = trainingCalendarForm.valid();


        <%
            if(empId.equalsIgnoreCase("0")){
        %>
        document.getElementById("lastEnrollmentDate").value = (getDateStringById('lastEnrollmentDate_js'));
        document.getElementById("addedEmployees").value = JSON.stringify(
            Array.from(added_employee_info_map.values())
        );

        preprocessCheckBoxBeforeSubmitting('isEnrollmentApplicale', row);
        preprocessCheckBoxBeforeSubmitting('isBondApplicable', row);


        const validEnrollmentDate = (!isEnrollmentApplicable() || dateValidator('lastEnrollmentDate_js', true, {
            errorEn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_LAST_ENROLMENT_DATE, "English")%>',
            errorBn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_LAST_ENROLMENT_DATE, "Bangla")%>'
        }));
        <%
        }
        %>
        if ($('#trainingTypeCat_category_0').val() == 2) {
            if (document.querySelector('#select2-country_select-container .select2-selection__placeholder') != null) {
                $('#country_error').show();
                return false;
            }
        }

        let currentValid = jQueryValid;
        <%
            if(empId.equalsIgnoreCase("0")){
        %>
        const validStartDate = dateValidator('startDate_js', true, {
            errorEn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_START_DATE, "English")%>',
            errorBn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_START_DATE, "Bangla")%>'
        });

        const validEndDate = dateValidator('endDate_js', true, {
            errorEn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_END_DATE, "English")%>',
            errorBn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_END_DATE, "Bangla")%>'
        });
        currentValid = currentValid && validEnrollmentDate && validStartDate && validEndDate;
        <%
        }
        %>
        return currentValid;
    }

    function processMultipleSelectBoxBeforeSubmit(field, row) {
        var selector = "#" + field + "_select2_" + row;
        var selectedInputs = $(selector).val();
        var temp = "";
        if (selectedInputs != null) {
            selectedInputs.forEach(function (value, index, array) {
                if (index > 0) {
                    temp += ", ";
                }
                temp += value;
            });
        }
        $(selector).append('<option value="' + temp + '"></option>');
        $(selector).val(temp);

    }

    function showOrHideCountryDiv() {
        if ($('#trainingTypeCat_category_0').val()) {
            if ($('#trainingTypeCat_category_0').val() == 2) {
                $('#country_div').show();
            } else {
                $('#country_div').hide();
            }
            if ($('#trainingTypeCat_category_0').val() == 3) {
                $('#instituteName_label').hide();
                $('#instituteName_text').hide();
                $('#instituteName_text').val('');
            } else {
                $('#instituteName_label').show();
                $('#instituteName_text').show();
            }
        } else {
            $('#country_div').hide();
            $('#instituteName_label').hide();
            $('#instituteName_text').hide();
        }
    }

    function showOrHideYearOfBondDiv() {
        let isBondApplicable = document.getElementById("isBondApplicable_checkbox_0");
        if (isBondApplicable.checked === true) {
            $('#yearOfBond_number_label').show();
            $('#yearOfBond_div').show();
        } else {
            $('#yearOfBond_number_label').hide();
            $('#yearOfBond_div').hide();
        }

    }

    function isEnrollmentApplicable() {
        return document.getElementById("isEnrollmentApplicale_checkbox_0").checked === true;
    }

    function showOrHideLastEnrollmentDateDiv() {
        let isEnrollmentApplicale = document.getElementById("isEnrollmentApplicale_checkbox_0");
        if (isEnrollmentApplicale.checked === true) {
            $('#lastEnrollmentDate_label_0').show();
            $('#lastEnrollmentDate_js').show();
        } else {
            $('#lastEnrollmentDate_label_0').hide();
            $('#lastEnrollmentDate_js').hide();
        }

    }

    function showOrHideTrainerDiv() {
        if ($('#trainerCat_category_0').val()) {
            if ($('#trainerCat_category_0').val() == 1) {
                $('.external_trainer').hide();
                $('#trainer_Div').show();
                $('#extTrainerInfo_id').val('');
            } else {
                $('.external_trainer').show();
                $('#trainer_Div').hide();
            }

        } else {
            $('.external_trainer').hide();
            $('#trainer_Div').hide();
            $('#extTrainerInfo_id').val('');
        }
    }


    $(function () {
        <%
            long curTime=Calendar.getInstance().getTimeInMillis();
        %>


        <%
            if(empId.equalsIgnoreCase("0") && !actionName.equals("edit")){
        %>
        //setMinDateByTimestampAndId("startDate_js", <%=curTime%>);
        //setMinDateByTimestampAndId("endDate_js", <%=curTime%>);
        setMinDateByTimestampAndId("lastEnrollmentDate_js", <%=curTime%>);
        <%}%>
        $("#startDate_js").on('datepicker.change', () => {
            if (getDateStringById("startDate_js") !== '') {
                setMinDateById("endDate_js", getDateStringById("startDate_js"));
                <%
            if(empId.equalsIgnoreCase("0")){
        %>
                setMaxDateById("lastEnrollmentDate_js", getDateStringById("startDate_js"));
                <%}%>
            }

        });
        $("#startTime_js").on('timepicker.change', (event, param) => {
            setMinTimeById("endTime_js", getTimeById("startTime_js", false));
        });
        <%
      if(actionName.equals("edit")) {
         String startTimestr=training_calenderDTO.startTime;
         String endTimestr=training_calenderDTO.endTime;
         System.out.println("StartTime: "+startTimestr);
         System.out.println("End time: "+endTimestr);
         %>
        setTimeById("startTime_js", '<%=startTimestr%>');
        setTimeById("endTime_js", '<%=endTimestr%>');
        setDateByTimestampAndId('startDate_js', <%=training_calenderDTO.startDate%>);
        setDateByTimestampAndId('endDate_js', <%=training_calenderDTO.endDate%>);

        setMinDateById("endDate_js", getDateStringById("startDate_js"));
        setMinTimeById("endTime_js", getTimeById("startTime_js", false));

        <%
            if(empId.equalsIgnoreCase("0")){
        %>
        setDateByTimestampAndId('lastEnrollmentDate_js', <%=training_calenderDTO.lastEnrollmentDate%>);
        setMaxDateById("lastEnrollmentDate_js", getDateStringById("startDate_js"));

        <%}}
        %>
    });

    function countrySelect() {
        if ($('#country_select')) {
            $('#country_error').hide();
        }
    }

    function PreprocessBeforeSubmiting(row, validate) {
        document.getElementById("startTime_time_").value = (getTimeById('startTime_js', false));
        document.getElementById("endTime_time_").value = (getTimeById('endTime_js', false));
        document.getElementById("startDate").value = (getDateStringById('startDate_js'));
        document.getElementById("endDate").value = (getDateStringById('endDate_js'));
        document.getElementById("lastEnrollmentDate").value = (getDateStringById('lastEnrollmentDate_js'));
        if (validate === "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields !== "") {
                if (validate === "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }
        document.getElementById("addedEmployees").value = JSON.stringify(
            Array.from(added_employee_info_map.values())
        );
        preprocessCheckBoxBeforeSubmitting('isEnrollmentApplicale', row);
        preprocessCheckBoxBeforeSubmitting('isBondApplicable', row);

        const validStartDate = dateValidator('startDate_js', true, {
            errorEn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_START_DATE, "English")%>',
            errorBn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_START_DATE, "Bangla")%>'
        });

        const validEndDate = dateValidator('endDate_js', true, {
            errorEn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_END_DATE, "English")%>',
            errorBn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_TRAINING_END_DATE, "Bangla")%>'
        });

        const validEnrollmentDate = (!isEnrollmentApplicable() || dateValidator('lastEnrollmentDate_js', true, {
            errorEn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_LAST_ENROLMENT_DATE, "English")%>',
            errorBn: '<%=LM.getText(LC.TRAINING_CALENDER_ADD_ENTER_LAST_ENROLMENT_DATE, "Bangla")%>'
        }));
        return validStartDate && validEndDate && validEnrollmentDate;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Training_calenderServlet");
    }

    function init(row) {
        var isEditPage = <%=actionName.equals("edit")%>;
    }

    var row = 0;

    window.onload = function () {
        init(row);

        CKEDITOR.replace('trainingObjective_textarea_' + row, {
            toolbar: [
                ['Cut', 'Copy', 'Paste', 'Undo', 'Redo'],			// Defines toolbar group without name.
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']},
                {name: 'morestyles', items: ['Strike', 'Subscript', 'Superscript']},
                {
                    name: 'paragraph',
                    items: ['NumberedList', 'BulletedList']
                },
                {name: 'justify', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                {name: 'colors', items: ['TextColor', 'BGColor']}
            ],
            height: '5em',
            width: 600
        });
        CKEDITOR.replace('trainingPrerequisite_textarea_' + row, {
            toolbar: [
                ['Cut', 'Copy', 'Paste', 'Undo', 'Redo'],			// Defines toolbar group without name.
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']},
                {name: 'morestyles', items: ['Strike', 'Subscript', 'Superscript']},
                {
                    name: 'paragraph',
                    items: ['NumberedList', 'BulletedList']
                },
                {name: 'justify', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                {name: 'colors', items: ['TextColor', 'BGColor']}
            ],
            height: '5em',
            width: 600
        });
    };

    added_employee_info_map = new Map(
        <%if(trainerDTOList != null){%>
        <%=trainerDTOList.stream()
                        .map(detailsDTO -> "['" + detailsDTO.employeeRecordsId + "'," + detailsDTO.getIdsStrInJsonString() + "]")
                        .collect(Collectors.joining(",","[","]"))
        %>
        <%}%>
    );

    /* IMPORTANT
        * This map is converts table name to the table's added employees map
        */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: false
            }]
        ]
    );
    modal_button_dest_table = 'none';

    $('#addToTable_modal_button').on('click', function () {
        //alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });
</script>