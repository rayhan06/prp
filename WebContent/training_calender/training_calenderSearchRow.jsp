<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.StringUtils" %>

<%
    //New requirement : To create training calendar on older date
    //boolean canModify = TrainingCalendarStatusEnum.getByValue(training_calenderDTO.status) != TrainingCalendarStatusEnum.COMPLETED;
%>
<td>
	<%=Utils.getDigits(iCount + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>
</td>

<td>
    <%=isEnglish ? training_calenderDTO.nameEn : training_calenderDTO.nameBn%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "training_category", training_calenderDTO.trainingCategoryCat)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "training_type", training_calenderDTO.trainingTypeCat)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, training_calenderDTO.startDate)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, training_calenderDTO.endDate)%>
</td>

<td>
    <%=training_calenderDTO.arrangedBy == 0 ? "" : CatRepository.getInstance().getText(Language, "training_arrange_by", training_calenderDTO.arrangedBy)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "training_mode", training_calenderDTO.trainingModeCat)%>
</td>
<td>
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=TrainingCalendarStatusEnum.getByValue(training_calenderDTO.status).getColor()%> ; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language, "training_status", training_calenderDTO.status)%>
    </span>
</td>

<td>
    <div class="d-flex justify-content-around align-items-center">
        <button title="View Details" type="button" class="btn-sm border-0 shadow bg-light btn-border-radius mx-2" style="color: #ff6b6b;"
                onclick="location.href='Training_calenderServlet?actionType=view&ID=<%=training_calenderDTO.iD%>'">
            <i class="fa fa-eye"></i>
        </button>
        <%--<%
            if (isAdmin) {
        %>--%>
        <a href='Training_calendar_detailsServlet?ID=<%=training_calenderDTO.iD%>'>

            <button title="Tag" type="button" class="btn-sm border-0 shadow btn-border-radius mx-2"
                    style="color: #fff; background: #0d638f"
                    onclick="location.href='Training_calenderServlet?actionType=view&ID=<%=training_calenderDTO.iD%>'">
                <i class="fa fa-tag" aria-hidden="true"></i>
            </button>
        </a>

        <a href='FeedbackQuestionTrainingCalenderMappingServlet?training_calendar_id=<%=training_calenderDTO.iD%>'>
            <button title="Feedback Question" type="button" class="btn-sm border-0 shadow btn-border-radius mx-2"
                    style="color: #fff; background: #ff842b"
                    onclick="location.href='Training_calenderServlet?actionType=view&ID=<%=training_calenderDTO.iD%>'">
                <i class="fa fa-question" aria-hidden="true"></i>
            </button>
        </a>

        <button title="Edit" type="button" class="btn-sm border-0 shadow btn-border-radius text-white mx-2"
                style="background-color: #ff6b6b;"
                onclick="location.href='Training_calenderServlet?actionType=getEditPage&ID=<%=training_calenderDTO.iD%>'">
            <i class="fa fa-edit"></i>
        </button>
        <%--<%
            }
        %>--%>
    </div>
</td>


<%--<%
    if (isAdmin) {
%>--%>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=training_calenderDTO.iD%>'/></span>
    </div>
</td>

<%--
<%
    }
%>--%>
