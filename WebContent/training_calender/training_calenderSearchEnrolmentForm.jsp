<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="training_calender.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
    String navigator2 = SessionConstants.NAV_TRAINING_CALENDER;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <%if ("English".equalsIgnoreCase(Language)) {%>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_NAMEEN, loginDTO)%>
            </th>
            <%} else {%>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_NAMEBN, loginDTO)%>
            </th>
            <%}%>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGTYPECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ARRANGEDBY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGMODECAT, loginDTO)%>
            </th>
            <th><%=Language.equals("English") ? "Status" : "অবস্থা"%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_ENROLL, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<Training_calenderDTO> data = (List<Training_calenderDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                for (Training_calenderDTO training_calenderDTO : data) {
        %>
        <tr>
            <%@include file="training_calenderSearchEnrolmentRow.jsp" %>
        </tr>
        <% } } %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>