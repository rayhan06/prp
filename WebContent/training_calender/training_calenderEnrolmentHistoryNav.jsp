<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="training_calendar_details.TrainerDTO" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="common.BaseServlet" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "EnrollmentServlet?actionType=searchEnrolmentHistory";
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
    List<TrainerDTO> trainerDTOList = new ArrayList<>();
    List<Training_calendar_detailsDTO> traineeDTOList = new ArrayList<>();

//    TrainerDTO trainerDTO = null;
//    Training_calendar_detailsDTO traineeDTO = null;
    int childTableStartingID = 1;
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0' onKeyUp='allfield_changed("",0)' id='anyfield'
                       value = '<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_TRAININGTYPECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='training_type_cat' id='training_type_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("training_type", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label
                                class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_INSTITUTENAME, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="institute_name" placeholder=""
                                   name="institute_name"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_STARTDATE, loginDTO)%>
                        </label>

                        <div class="col-md-9">

                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="start_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden'
                               class='form-control'
                               id='start_date'
                               name='start_date'
                               value='' tag='pb_html'/>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_ENDDATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">

                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="end_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden'
                               class='form-control'
                               id='end_date'
                               name='end_date'
                               value='' tag='pb_html'/>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_TRAININGMODECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='training_mode_cat' id='training_mode_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("training_mode", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=Language.equals("English") ? "Status" : "অবস্থা"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='training_status' id='training_status'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("training_status", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->




<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        $("#start_date").val(getDateStringById("start_date_js"));
        $("#end_date").val(getDateStringById("end_date_js"));
        var params = 'AnyField=' + document.getElementById('anyfield').value;


        params += '&training_type_cat=' + document.getElementById('training_type_cat').value;
        params += '&institute_name=' + document.getElementById('institute_name').value;

        var date_elements = document.getElementById('start_date').value.split("/");
        var date = new Date(date_elements[2], date_elements[1] - 1, date_elements[0]);
        if (date_elements != "") {
            params += '&start_date=' + date.getTime();
        }
        date_elements = document.getElementById('end_date').value.split("/");
        date = new Date(date_elements[2], date_elements[1] - 1, date_elements[0]);
        if (date_elements != "") {
            params += '&end_date=' + date.getTime();
        }

        params += '&training_mode_cat=' + document.getElementById('training_mode_cat').value;
        params += '&status=' + document.getElementById('training_status').value;

        params += '&search=true&ajax=true';


        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>",
            onSelect: function (dateText) {
                $("#end_date").datepicker('option', 'minDate', dateText);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>"
        });
    });


</script>

