<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="training_calender.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.List" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="common.RoleEnum" %>
<%@ page import="user.UserDTO" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String navigator2 = SessionConstants.NAV_TRAINING_CALENDER;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    boolean isAdmin = userDTO.roleID == RoleEnum.ADMIN.getRoleId() || userDTO.roleID == RoleEnum.HR_ADMIN.getRoleId();

%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped ">
        <thead class="text-nowrap">
        <tr>
        	<th ><%=LM.getText(LC.HM_SL, loginDTO)%>
            <th style="min-width: 20rem; max-width: 20rem"><%=LM.getText(LC.HM_NAME, loginDTO)%>
            </th>
            <th>
                <%=isEnglish ? "Training Category" : "প্রশিক্ষণ ক্যাটাগরি"%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGTYPECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ARRANGEDBY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGMODECAT, loginDTO)%>
            </th>
            <th><%=isEnglish ? "Status" : "অবস্থা"%>
            </th>
            <th><%=isEnglish ? "Action" : "ক্রিয়া"%></th>
            <%--<%
                if (isAdmin) {
            %>--%>
            <th class="text-center">
                <span><%=isEnglish ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
            <%--<%
                }
            %>--%>
        </tr>
        </thead>
        <tbody>
        <%
            List<Training_calenderDTO> data = (List<Training_calenderDTO>) rn2.list;
            if (data != null && data.size() > 0) {
            	int iCount = 0;
                for (Training_calenderDTO training_calenderDTO : data) {
                	iCount ++;
        %>
        <tr>
            <%@include file="training_calenderSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>
    </table>
</div>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>