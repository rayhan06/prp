<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="training_calendar_details.TrainerDTO" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="common.BaseServlet" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
    List<TrainerDTO> trainerDTOList = new ArrayList<>();
    List<Training_calendar_detailsDTO> traineeDTOList = new ArrayList<>();

//    TrainerDTO trainerDTO = null;
//    Training_calendar_detailsDTO traineeDTO = null;
    int childTableStartingID = 1;
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0' onKeyUp='allfield_changed("",0)' id='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="training_category_cat">
                            <%=isLanguageEnglish ? "Training Category" : "প্রশিক্ষণ ক্যাটাগরি"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='training_category_cat' id='training_category_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("training_category", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.TRAINING_CALENDER_SEARCH_TRAININGTYPECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='training_type_cat' id='training_type_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("training_type", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_INSTITUTENAME, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="institute_name" placeholder=""
                                   name="institute_name"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_STARTDATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="start_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type='hidden'
                                   class='form-control'
                                   id='start_date'
                                   name='start_date'
                                   value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_ENDDATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="end_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>

                            <input type='hidden'
                                   class='form-control'
                                   id='end_date'
                                   name='end_date'
                                   value='' tag='pb_html'/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.TRAINING_CALENDER_SEARCH_TRAININGMODECAT, loginDTO)%>
                        </label>

                        <div class="col-md-9">
                            <select class='form-control' name='training_mode_cat' id='training_mode_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("training_mode", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=Language.equals("English") ? "Status" : "অবস্থা"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='training_status' id='training_status'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("training_status", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" id="trainee_Div">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                            id="addToTrainee_modal_button">
                        <%=isLanguageEnglish ? "Add Trainee" : "প্রশিক্ষণার্থী যুক্ত করুন"%>
                    </button>
                    <table class="table table-bordered table-striped">
                        <thead></thead>

                        <tbody id="tagged_trainee_table">
                        <tr style="display: none;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="button" class="btn btn-sm delete-trash-btn"
                                        onclick="remove_containing_row(this,'tagged_trainee_table');">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <%if (traineeDTOList != null) {%>
                        <%for (Training_calendar_detailsDTO traineeDTO : traineeDTOList) {%>
                        <tr>
                            <td>
                                <%=traineeDTO.employeeUserName%>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? (traineeDTO.officeNameEn + "(" + traineeDTO.organogramNameEn + ")")
                                        : (traineeDTO.officeNameBn + "(" + traineeDTO.organogramNameBn + ")")%>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? traineeDTO.employeeNameEn : traineeDTO.employeeNameBn%>
                            </td>
                            <td id='<%=traineeDTO.employeeRecordsId%>_td_button'>
                                <button type="button" class="btn btn-sm delete-trash-btn"
                                        onclick="remove_containing_row(this,'tagged_trainee_table');">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <%}%>
                        <%}%>
                        </tbody>
                    </table>
                    <input type='hidden' class='form-control' name='trainer'
                           id='trainee' value='' tag='pb_html'/>
                </div>
                <div class="col-md-6" id="trainer_Div">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                            id="addToTrainer_modal_button">
                        <%=isLanguageEnglish ? "Add Trainer" : "প্রশিক্ষক যুক্ত করুন"%>
                    </button>
                    <table class="table table-bordered table-striped">
                        <thead></thead>

                        <tbody id="tagged_trainer_table">
                        <tr style="display: none;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="button" class="btn btn-sm delete-trash-btn"
                                        onclick="remove_containing_row(this,'tagged_trainer_table');">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <%if (trainerDTOList != null) {%>
                        <%for (TrainerDTO trainerDTO : trainerDTOList) {%>
                        <tr>
                            <td>
                                <%=trainerDTO.employeeUserName%>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? (trainerDTO.officeNameEn + "(" + trainerDTO.organogramNameEn + ")")
                                        : (trainerDTO.officeNameBn + "(" + trainerDTO.organogramNameBn + ")")%>
                            </td>
                            <td>
                                <%=isLanguageEnglish ? trainerDTO.employeeNameEn : trainerDTO.employeeNameBn%>
                            </td>
                            <td id='<%=trainerDTO.employeeRecordsId%>_td_button'>
                                <button type="button" class="btn btn-sm delete-trash-btn"
                                        onclick="remove_containing_row(this,'tagged_trainer_table');">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <%}%>
                        <%}%>
                        </tbody>
                    </table>
                    <input
                            type='hidden'
                            class='form-control'
                            name='trainer'
                            id='trainer'
                            value=''
                            tag='pb_html'
                    />
                </div>


            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>
<script type="text/javascript">

    $(document).ready(() => {
        select2SingleSelector('#training_category_cat', '<%=Language%>');
        select2SingleSelector('#training_type_cat', '<%=Language%>');
        select2SingleSelector('#training_mode_cat', '<%=Language%>');
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        $("#start_date").val(getDateStringById("start_date_js"));
        $("#end_date").val(getDateStringById("end_date_js"));
        var params = 'AnyField=' + document.getElementById('anyfield').value;


        params += '&training_category_cat=' + document.getElementById('training_category_cat').value;
        params += '&training_type_cat=' + document.getElementById('training_type_cat').value;
        params += '&institute_name=' + document.getElementById('institute_name').value;
        params += '&status=' + document.getElementById('training_status').value;

        var date_elements = document.getElementById('start_date').value.split("/");
        var date = new Date(date_elements[2], date_elements[1] - 1, date_elements[0]);
        if (date_elements != "") {
            params += '&start_date=' + date.getTime();
        }
        date_elements = document.getElementById('end_date').value.split("/");
        date = new Date(date_elements[2], date_elements[1] - 1, date_elements[0]);
        if (date_elements != "") {
            params += '&end_date=' + date.getTime();
        }
        let added_trainer = '';
        if (typeof added_trainer_info_map.keys().next().value !== 'undefined') {
            added_trainer = added_trainer_info_map.keys().next().value;
        }
        let added_trainee = '';
        if (typeof added_trainee_info_map.keys().next().value !== 'undefined') {
            added_trainee = Array.from(added_trainee_info_map.keys()).join(",");
        }

        params += '&training_mode_cat=' + document.getElementById('training_mode_cat').value;
        params += '&trainer=' + added_trainer;
        params += '&trainee=' + added_trainee;


        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    $(function () {
        $("#start_date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>",
            onSelect: function (dateText) {
                $("#end_date").datepicker('option', 'minDate', dateText);
            }
        });
        $("#end_date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>"
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    // map to store and send added employee data
    added_trainer_info_map = new Map(
        <%if(trainerDTOList != null){%>
        <%=trainerDTOList.stream()
                        .map(detailsDTO -> "['" + detailsDTO.employeeRecordsId + "'," + detailsDTO.getIdsStrInJsonString() + "]")
                        .collect(Collectors.joining(",","[","]"))
        %>
        <%}%>
    );
    // map to store and send added employee data
    added_trainee_info_map = new Map(
        <%if(traineeDTOList != null){%>
        <%=traineeDTOList.stream()
                        .map(complainByDTO -> "['" + complainByDTO.employeeRecordsId + "'," + complainByDTO.getIdsStrInJsonString() + "]")
                        .collect(Collectors.joining(",","[","]"))
        %>
        <%}%>
    );

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_trainer_table', {
                info_map: added_trainer_info_map,
                isSingleEntry: true
            }],
            ['tagged_trainee_table', {
                info_map: added_trainee_info_map,
                isSingleEntry: false
            }]
        ]
    );


    // TODO: dynamic modal
    // select action of modal's add button
    modal_button_dest_table = 'none';

    $('#addToTrainer_modal_button').on('click', function () {
        //alert('CLICKED');
        modal_button_dest_table = 'tagged_trainer_table';
        $('#search_emp_modal').modal();
    });

    $('#addToTrainee_modal_button').on('click', function () {
        //alert('CLICKED');
        modal_button_dest_table = 'tagged_trainee_table';
        $('#search_emp_modal').modal();
    });


</script>

