<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="training_calender.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDTO" %>
<%@ page import="training_calendar_details.Training_calendar_detailsDAO" %>
<%@ page import="training_calendar_details.TrainerDTO" %>
<%@ page import="training_calendar_details.TrainerDAO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);
    boolean isBangla = "Bangla".equalsIgnoreCase(Language);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Training_calenderDAO training_calenderDAO = Training_calenderDAO.getInstance();
    Training_calenderDTO training_calenderDTO = training_calenderDAO.getDTOFromID(id);
    FilesDAO filesDAO = new FilesDAO();
    List<Training_calendar_detailsDTO> trainingCalendarDetailsDTOList = new Training_calendar_detailsDAO().getByForeignKey(ID);
    List<TrainerDTO> trainerDTOList = new TrainerDAO().getByForeignKey(ID);
%>

<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=isBangla ? "ট্রেনিং ক্যালেন্ডার বিস্তারিত" : "Training Calender Details"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class=" div_border office-div">
                <div class="">
                    <h5 class="table-title">
                        <%=isBangla ? "ট্রেনিং ক্যালেন্ডার" : "Training Calender"%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_NAMEEN, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.nameEn%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_NAMEBN, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.nameBn%>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGTYPECAT, loginDTO)%>
                                    </b></td>
                                <td><%=CatRepository.getInstance().getText(Language, "training_type", training_calenderDTO.trainingTypeCat)%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_INSTITUTENAME, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.instituteName%>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.START_DATE_TIME, loginDTO)%>
                                    </b></td>
                                <td><%=StringUtils.getFormattedDate(Language, training_calenderDTO.startDate)%> <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, training_calenderDTO.startTime)%>
                                </td>
                                <td><b class="table-title font-weight-bold"><%=LM.getText(LC.END_DATE_TIME, loginDTO)%>
                                </b></td>
                                <td><%=StringUtils.getFormattedDate(Language, training_calenderDTO.endDate)%> <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, training_calenderDTO.endTime)%>
                                </td>
                            </tr>


                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ARRANGEDBY, loginDTO)%>
                                    </b></td>
                                <td>
                                    <%=training_calenderDTO.arrangedBy == 0? "" : CatRepository.getInstance().getText(Language,"training_arrange_by",training_calenderDTO.arrangedBy)%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGMODECAT, loginDTO)%>
                                    </b></td>
                                <td><%=CatRepository.getInstance().getText(Language, "training_mode", training_calenderDTO.trainingModeCat)%>
                                </td>
                            </tr>

                            <%if (!training_calenderDTO.isPrivate) {%>
                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_VENUE, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.venue%>
                                </td>

                                <td>
                                    <b class="table-title font-weight-bold"><%=isBangla ? "প্রশিক্ষণের ব্যয়" : "Training Cost"%>
                                    </b></td>
                                <td><%=training_calenderDTO.trainingCost == null ? "" : training_calenderDTO.trainingCost%>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TARGETGROUPTYPE, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.targetGroupType%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_MAXNUMBEROFPARTICIPANTS, loginDTO)%>
                                    </b></td>
                                <td><%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(training_calenderDTO.maxNumberOfParticipants))%>
                                </td>
                            </tr>
                            <%if (training_calenderDTO.isEnrollmentApplicale) {%>
                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ISENROLLMENTAPPLICALE, loginDTO)%>
                                    </b></td>
                                <td><%=StringUtils.getYesNo(Language, training_calenderDTO.isEnrollmentApplicale)%>
                                </td>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_LASTENROLLMENTDATE, loginDTO)%>
                                    </b></td>
                                <td><%=StringUtils.getFormattedDate(Language, training_calenderDTO.lastEnrollmentDate)%>
                                </td>
                            </tr>
                            <%}%>


                            <%if (training_calenderDTO.isBondApplicable) {%>
                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ISBONDAPPLICABLE, loginDTO)%>
                                    </b></td>
                                <td><%=StringUtils.getYesNo(Language, training_calenderDTO.isBondApplicable)%>
                                </td>

                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_YEAROFBOND, loginDTO)%>
                                    </b></td>
                                <td><%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(training_calenderDTO.yearOfBond))%>
                                </td>
                            </tr>
                            <%}%>

                            <%if (!"".equals(training_calenderDTO.externalTrainerInfo)) {%>
                            <tr>
                                <td>
                                    <b class="table-title font-weight-bold"><%=isBangla ? "বহিরাগত ট্রেনারের তথ্য" : "External Trainer Info"%>
                                    </b></td>
                                <td colspan="3">
                                    <%=training_calenderDTO.externalTrainerInfo%>
                                </td>
                            </tr>
                            <%}%>


                            <tr><%if (!training_calenderDTO.trainingObjective.equals("")) {%>

                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGOBJECTIVE, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.trainingObjective%>
                                </td>
                                <%}%>
                                <%if (!training_calenderDTO.trainingPrerequisite.equals("")) {%>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGPREREQUISITE, loginDTO)%>
                                    </b></td>
                                <td><%=training_calenderDTO.trainingPrerequisite%>
                                </td>
                                <%}%>
                            </tr>
                            <%}%>


                            <tr>
                                <%
                                    {
                                        List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(training_calenderDTO.trainingMaterialsDropzone);
                                        if (FilesDTOList != null && FilesDTOList.size() != 0) {
                                %>
                                <td>
                                    <b class="table-title font-weight-bold"><%=LM.getText(LC.TRAINING_CALENDER_EDIT_TRAININGMATERIALSDROPZONE, loginDTO)%>
                                    </b></td>
                                <td colspan="3">

                                    <table>
                                        <tr>
                                            <%

                                                for (FilesDTO filesDTO : FilesDTOList) {
                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                            %>
                                            <td>
                                                <%
                                                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                %>
                                                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                     style='width:100px'/>
                                                <%
                                                    }
                                                %>
                                                <a href='Training_calenderServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                   download><%=filesDTO.fileTitle%>
                                                </a>
                                            </td>
                                            <%
                                                    }
                                                }
                                            %>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                </td>
                            </tr>


                        </table>
                    </div>
                    <%if (trainerDTOList.size() > 0) {%>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=isBangla ? "অভ্যন্তরীণ প্রশিক্ষকের তালিকা" : "List of Internal Trainers"%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th class="table-title"><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                </th>
                                <th class="table-title"><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                </th>
                                <th class="table-title"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%
                                if (trainerDTOList.size() > 0) {
                                    for (TrainerDTO dto : trainerDTOList) {
                            %>
                            <tr>
                                <td>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, dto.employeeUserName)%>
                                </td>
                                <td>
                                    <%=isBangla ? dto.organogramNameBn + ", " + dto.officeNameBn : dto.organogramNameEn + ", " + dto.officeNameEn %>
                                </td>
                                <td>
                                    <%=isBangla ? dto.employeeNameBn : dto.employeeNameEn%>
                                </td>
                            </tr>
                            <%
                                    }
                                }
                            %>

                            </tbody>
                        </table>
                    </div>
                </div>
                <%}%>
                <%if (trainingCalendarDetailsDTOList.size() > 0) {%>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=isBangla ? "প্রশিক্ষণার্থী কর্মচারীদের তালিকা" : "List of Trainee Employees"%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th class="table-title"><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                </th>
                                <th class="table-title"><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                </th>
                                <th class="table-title"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%

                                for (Training_calendar_detailsDTO dto : trainingCalendarDetailsDTOList) {
                            %>
                            <tr>
                                <td><%=StringUtils.convertBanglaIfLanguageIsBangla(Language, dto.employeeUserName)%>
                                </td>
                                <td><%=isBangla ? dto.organogramNameBn + ", " + dto.officeNameBn : dto.organogramNameEn + ", " + dto.officeNameEn%>
                                </td>
                                <td><%=isBangla ? dto.employeeNameBn : dto.employeeNameEn%>
                                </td>
                            </tr>
                            <%

                                }
                            %>

                            </tbody>
                        </table>
                    </div>
                </div>
                <%}%>
            </div>
        </div>
    </div>
</div>