var currentPageNo = 1;
var totalRecord = 0;
var totalPageNo = 0;
var recordPerPage = 0;

var reportTable = $('#reportTable');
var reportForm = $("#ReportForm");
//init DataTable first time without any data
let columnsArray = [];
let columnsArrayMapByDisplayIndex = new Map(); // (key: int, val: ReportColumn), keeps the references of objects in columnsArray
let isDraggable = false;
let criteriaArray = [];
let showFooter = true;

function setEngUserName(username, field) {
    $("#" + field).val(convertToEnglishNumber(username));
}

function getParamValue(param) {
    var url_string = window.location.href; // www.test.com?filename=test
    var url = new URL(url_string);
    var paramValue = url.searchParams.get(param);
    return paramValue;
}

function isNumeric(n) {
    n = n.getDigitEnglishFromBangla();
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isDate(n) {
    var date = new Date(n);
    return date instanceof Date && !isNaN(date.valueOf());
}

function searchByDateChanged() {
    if ($('#search_by_date').prop('checked')) {
        resetDateById('startDate_js');
        resetDateById('endDate_js');
        $("#ymdiv").css("display", "none");
        $("#month").css("display", "none");
        $("#calendardiv").css("display", "block");
    } else {
        $("#ymdiv").css("display", "block");
        $("#month").css("display", "block");
        $("#calendardiv").css("display", "none");
        $("#dYear").val('');
        $("#dMonth").val('');
    }
    $("#startDate").val('0');
    $("#endDate").val('91605117600000');
	$("#dateText").html("");
}

function processYMAndSubmit() {
    if ($("#dYear").val() != "" && $("#dMonth").val() != "") {
        var year = parseInt($("#dYear").val());
        var month = parseInt($("#dMonth").val());
        var day = 1;
        console.log("year = " + year + " month = " + month + " day = " + day);
		var normalStartDate = new Date(year, month, day);
		var normalStartTime = normalStartDate.getTime();
        $("#startDate").val(normalStartTime);

        month++;
        if (month == 12) {
            month = 0;
            year++;
        }
        day = 0;

		var normalEndDate = new Date(year, month, day);
		var normalEndTime = normalEndDate.getTime();
        if (add1WithEnd) {
            $("#endDate").val(normalEndTime + 86400000);
        } else {
            $("#endDate").val(normalEndTime);
        }

		var dateText = normalStartDate.customFormat( "#DD#/#MM#/#YYYY#" ) + " - " + normalEndDate.customFormat( "#DD#/#MM#/#YYYY#" );
		if($("#Language").val() == "Bangla")
		{
			dateText = convertToBanglaNumber(dateText);
		}
		
		$("#dateText").html(dateText);

    } else {
        $("#startDate").val('0');
        $("#endDate").val('91605117600000');
		$("#dateText").html("");
    }

    console.log("startdate = " + $("#startDate").val());
    console.log("enddate = " + $("#endDate").val());
    //ajaxSubmit();
}

function processCalendarAndSubmit(startDateInput, endDateInput) {
    console.log("calendar change called");
    var startDate = getBDFormattedDate(startDateInput);
    if (startDate == '') {
        startDate = '0';
    }
    $("#startDate").val(startDate);

    var endDate = getBDFormattedDate(endDateInput);
    if (endDate == '') {
        endDate = '91605117600000';
    }
    $("#endDate").val(endDate);

    console.log("startdate = " + $("#startDate").val());
    console.log("enddate = " + $("#endDate").val());

	

    //ajaxSubmit();
}

var add1WithEnd = false;

function processNewCalendarDateAndSubmit() {
    console.log("new calendar change called");
	var normalStartDate = new Date(getDateTimestampById('startDate_js'));
    $("#startDate").val(getDateTimestampById('startDate_js'));

	var normalEndDate = new Date(getDateTimestampById('endDate_js'));
    if (!add1WithEnd) {
        $("#endDate").val(getDateTimestampById('endDate_js'));
    } else {

        $("#endDate").val(parseInt(getDateTimestampById('endDate_js')) + 86400000);
    }

    $("#add1WithEnd").val(true);

	if(normalStartDate !== null && normalEndDate!== null && !isNaN(normalStartDate) && !isNaN(normalEndDate))
	{
		var dateText = normalStartDate.customFormat( "#DD#/#MM#/#YYYY#" ) + " - " + normalEndDate.customFormat( "#DD#/#MM#/#YYYY#" );
		if($("#Language").val() == "Bangla")
		{
			dateText = convertToBanglaNumber(dateText);
		}
		
		$("#dateText").html(dateText);
	}
	else
	{
		$("#dateText").html("");
	}
	

    console.log("startdate = " + $("#startDate").val());
    console.log("enddate = " + $("#endDate").val());

    //ajaxSubmit();
}

function customadd(x, y) {
    x = (x + "").replaceAll(",", "");
    y = (y + "").replaceAll(",", "");
    var z;
    x = convertToEnglishNumber(x);
    y = convertToEnglishNumber(y);
    if (isNumber(x) && isNumber(y)) {
        if (x.includes(".") || y.includes(".")) {
            z = parseFloat(x) + parseFloat(y);
        } else {
            z = parseInt(x) + parseInt(y);
        }
    } else if (isNumber(x) && y == "") {
        z = x;
    } else if (isNumber(y) && x == "") {
        z = y;
    } else {
        z = "";
    }
    //console.log("x = " + x + " y = " + y + " z = " + z)
    return z;
}

function isNumber(string) {
    if (string === null || string === undefined)
        return false;

    string = string.trim();
    if (string === '')
        return false;

    return !isNaN(string);
}

function getNumber(string) {
    if (isNumber(string))
        return Number(string.trim());
}

function isBdDate(string) {
    if (string === null || string === undefined)
        return false;

    string = string.trim();
    if (string === '')
        return false;

    const dateRegex = /^[0-9০-৯]{1,2}[-/][0-9০-৯]{1,2}[-/][0-9০-৯]{2,4}$/;
    return dateRegex.test(string);
}

function convertToEnglishNumber(str) {
    str = String(str);
    str = str.replaceAll('০', '0');
    str = str.replaceAll('১', '1');
    str = str.replaceAll('২', '2');
    str = str.replaceAll('৩', '3');
    str = str.replaceAll('৪', '4');
    str = str.replaceAll('৫', '5');
    str = str.replaceAll('৬', '6');
    str = str.replaceAll('৭', '7');
    str = str.replaceAll('৮', '8');
    str = str.replaceAll('৯', '9');
    return str;
}

function convertToBanglaNumber(str) {
    str = String(str);
    str = str.replaceAll('0', '০');
    str = str.replaceAll('1', '১');
    str = str.replaceAll('2', '২');
    str = str.replaceAll('3', '৩');
    str = str.replaceAll('4', '৪');
    str = str.replaceAll('5', '৫');
    str = str.replaceAll('6', '৬');
    str = str.replaceAll('7', '৭');
    str = str.replaceAll('8', '৮');
    str = str.replaceAll('9', '৯');
    return str;
}

// bdDateStr -> day/month/year format
function getTimeStamp(bdDateStr) {
    bdDateStr = convertToEnglishNumber(bdDateStr);
    bdDateStr = bdDateStr.replaceAll('-', '/');
    const dayMonthYear = bdDateStr.split('/');
    const date = new Date(dayMonthYear[2], dayMonthYear[1] - 1, dayMonthYear[0]);
    return Number(date.getTime());
}

function getSortableData(string) {
    if (isNumber(string)) {
        return getNumber(string);
    }

    if (isBdDate(string)) {
        return getTimeStamp(string);
    }


    return String(string).toLowerCase();
}

function sortTableRows(columnIndex, isAscending = true) {
    /*const tableBody = document.getElementById("reportTable").querySelector('tbody');

    const rowsObj = Array.from(tableBody.rows).map(row => {
        const columnText = row.cells[columnIndex].innerText.trim();
        return {
            data: getSortableData(columnText),
            rowElement: row
        };
    });

    const direction = isAscending ? 1 : -1;
    rowsObj.sort((a, b) => {
        if (a.data === b.data) return 0;
        return direction * (a.data < b.data ? -1 : 1);
    });

    tableBody.innerHTML = '';
    rowsObj.forEach(rowObj => tableBody.append(rowObj.rowElement));*/
    $("#orderByCol").val(parseInt(columnIndex));
    $("#isAscending").val(isAscending);
    ajaxSubmit();
}

function getArrows(columnIndex) {
    const arrows = "<span class=\"up-down-link\"> " +
                   "<a class=\"up-link\"><span><i onclick='sortTableRows(" + columnIndex + ", true)' class=\"fa fa-arrow-circle-up\"></i></span></a>" +
                   "<a class=\"down-link\"><span><i onclick='sortTableRows(" + columnIndex + ", false)' class=\"fa fa-arrow-circle-down\"></i></span></a>" +
                   "</span>";

    return arrows;
}

function formatpagenoAndRPP() {
    var pageno = document.getElementById("pageno").value;
    var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
    if (pageno == "" || pageno < 1) {
        document.getElementById("pageno").value = 1;
    }
    if (RECORDS_PER_PAGE == "" || RECORDS_PER_PAGE < 1) {
        document.getElementById("RECORDS_PER_PAGE").value = 100;
    }
}

function ajaxSubmit() {
    console.log("Ajax default submitting");
    formatpagenoAndRPP();
    var pageno = document.getElementById("pageno").value;
    var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
    if (pageno > document.getElementById("tatalPageNo").innerHTML) {
        pageno = 1;
    }
    $("input[name=pageno]").val(pageno);
    $("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

    ajaxMiniSubmit();
}

function ajaxMiniSubmit() {
    console.log("Ajax submitting 2");

    console.log("params: " + reportForm.serialize());

    // callAjax(context + $("#countURL").val(), reportForm.serialize(), getTotalDataCountCallBack, "GET");
    let firstTimeLoad = columnsArray.length === 0;
    if (!firstTimeLoad) {
        $(".loader-container-circle").css("visibility", "visible");
        $(".loader-container-circle").css("z-index", "1000");
    }
    submit();
}

function firstloadSubmit() {
    console.log("Ajax firstload submitting");
    formatpagenoAndRPP();
    var pageno = 1;
    var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
    $("input[name=pageno]").val(pageno);
    $("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

    ajaxMiniSubmit();
}

function lastloadSubmit() {
    console.log("Ajax lastload submitting");
    formatpagenoAndRPP();
    var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
    var pageno = document.getElementById("tatalPageNo").innerHTML;
    $("input[name=pageno]").val(pageno);
    $("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

    ajaxMiniSubmit();
}

function nextloadSubmit() {
    console.log("Ajax nextload submitting");
    formatpagenoAndRPP();
    var pageno = parseInt(document.getElementById("pageno").value) + 1;
    var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
    /*if (pageno > document.getElementById("tatalPageNo").innerHTML) {
        pageno = 1;
    }*/
    $("input[name=pageno]").val(pageno);
    $("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

    ajaxMiniSubmit();
}

function prevloadSubmit() {
    console.log("Ajax prevload submitting");
    formatpagenoAndRPP();
    var pageno = parseInt(document.getElementById("pageno").value) - 1;
    var RECORDS_PER_PAGE = document.getElementById("RECORDS_PER_PAGE").value;
    if (pageno < 1) {
        pageno = document.getElementById("tatalPageNo").innerHTML;
    }
    $("input[name=pageno]").val(pageno);
    $("input[name=RECORDS_PER_PAGE]").val(RECORDS_PER_PAGE);

    ajaxMiniSubmit();
}

var columnDataFromater =
    {
        exportOptions:
            {
                format:
                    {
                        body: function (data, row, column, node) {
                            /*data.replace( /[$,.]/g, '' )*/
                            return data.replace(/(&nbsp;|<([^>]+)>)/ig, "");/*remove html tag from data when exporting*/
                        }
                    }
            }
    };

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

var RIGHT_ALIGN_INT = 1;
var RIGHT_ALIGN_FLOAT = 2;

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function isInt(n) {
    return Number(n) === n && n % 1 === 0;
}

function isFloat(n) {
    return Number(n) === n && n % 1 !== 0;
}

function createTable(data, dataCount, reportColumns) {
    let firstTimeLoad = columnsArray.length === 0;
    console.log("firstTimeLoad : " + firstTimeLoad);
    var createdHTML = "";

    var thead = "<thead class='text-nowrap' ><tr>";

    var j = 0;
    var list = data[0];
    var sums = [];
    isDraggable = reportColumns != null;
    if (isDraggable) {
        columnsArray = [...reportColumns];
        columnsArrayMapByDisplayIndex = new Map(columnsArray.map(reportColumn => [+reportColumn.reportDisplayIndex, reportColumn]));
    }
    $.each(list, function (innerIndex, listItem) {
        if (!isDraggable && firstTimeLoad) {
            let obj = {
                'reportDisplayIndex': j,
                'viewIndex': j,
                'isVisible': typeof isVisible == 'undefined' || isVisible[j],
                'show' : true,
                'title': listItem
            };
            columnsArray.push(obj);
            columnsArrayMapByDisplayIndex.set(+obj.reportDisplayIndex, obj);
        }
        const reportDisplayIndex = columnsArray[j].reportDisplayIndex;
        if (columnsArray[j].show && columnsArray[j].isVisible) {
            thead += "<th ";
            if ($("#colFormat_" + (reportDisplayIndex - 1)).length > 0) {
                const isRightAlign = $("#colFormat_" + (reportDisplayIndex - 1)).val() == RIGHT_ALIGN_INT || $("#colFormat_" + (reportDisplayIndex - 1)).val() == RIGHT_ALIGN_FLOAT;
                if (isRightAlign) {
                    thead += " style='text-align: right' ";
                }
            }
        } else {
            thead += "<th style = 'display:none'";
        }
        thead += " class='reporttable-header table-title col_" + reportDisplayIndex + "'>" + listItem;
        if (reportDisplayIndex > 0) {
            thead += getArrows(reportDisplayIndex);
        }
        thead += "</th>";
        sums[j] = 0;
        j++;
    });

    thead += "</tr></thead>";

    var onlyData = data.slice(1);


    var tbody = "<tbody class=''>";
    $.each(onlyData, function (index, list) {

        var href = "";
        if (typeof getLink != 'undefined') {
            //console.log("function found");
            href = getLink(list);
        }


        tbody += "<tr>";

        var tdStyle = "";
        if (typeof getStyle != 'undefined') {
            //console.log("function found");
            tdStyle = getStyle(list);
        }


        j = 0;
        $.each(list, function (innerIndex, listItem) {
            const reportDisplayIndex = columnsArray[j].reportDisplayIndex;
            if (columnsArray[j].show && columnsArray[j].isVisible) {
                tbody += "<td ";
                var tdTotStyle = tdStyle;
                if ($("#colFormat_" + (reportDisplayIndex - 1)).length > 0) {
                    const isRightAlign =
                        $("#colFormat_" + (reportDisplayIndex - 1)).val() == RIGHT_ALIGN_INT
                        || $("#colFormat_" + (reportDisplayIndex - 1)).val() == RIGHT_ALIGN_FLOAT;
                    if (isRightAlign) {
                        tdTotStyle += " text-align: right;";
                    }
                }
                if (tdTotStyle != "") {
                    tbody += " style='" + tdTotStyle + "'";
                }
            } else {
                tbody += "<td style = 'display:none'";
            }
            tbody += " class='text-center-report reporttable-body col_" + reportDisplayIndex + "'>" + listItem;
            if (href != "" && reportDisplayIndex == 0) {
                tbody += "<button type=\"button\" class=\"btn-sm border-0 shadow bg-light btn-border-radius viewButton\" style=\"color: #ff6b6b;\" onclick=\"location.href='" + href + "'\"><i class=\"fa fa-eye\"></i></button>";
            }
            tbody += "</td>";
            sums[j] = customadd(sums[j], listItem);
            j++;
        })
        // console.log(" ");
        tbody += "</tr>";


    });
    totalRecord = dataCount; //Recored count updated
    console.log("totalRecord = " + totalRecord);

    tbody += "</tbody>";
    let tfoot = '';
    if (showFooter) {
        tfoot = "<tfoot style='background-color: aliceblue'><tr>";
        j = 0;
        $.each(sums, function (innerIndex, listItem) {
            const reportDisplayIndex = columnsArray[j].reportDisplayIndex;
            if ($("#Language").val() == "Bangla") {
                if ($("#colFormat_" + (reportDisplayIndex - 1)).length > 0) {
                    if ($("#colFormat_" + (reportDisplayIndex - 1)).val() == RIGHT_ALIGN_INT) {
                        listItem = parseInt(listItem);
                    } else if ($("#colFormat_" + (reportDisplayIndex - 1)).val() == RIGHT_ALIGN_FLOAT) {
                        listItem = parseFloat(listItem);
                        listItem = listItem.toFixed(2);
                    }
                }
                listItem = convertToBanglaNumber(numberWithCommas(listItem));
            }
            if (addables[innerIndex] != null && addables[innerIndex] == 0) {
                // console.log("innerIndex = " + innerIndex + " addables[innerIndex] = " + addables[innerIndex]);
                if (j === 0) {
                    listItem = $("#Language").val() == "Bangla" ? 'যোগফল' : 'Sum';
                } else {
                    listItem = "";
                }
            } else {
                // console.log("null addable for innerIndex = " + innerIndex);
            }
            if (columnsArray[j].show && columnsArray[j].isVisible) {
                tfoot += "<td ";
                if ($("#colFormat_" + (reportDisplayIndex - 1)).val() == RIGHT_ALIGN_INT || $("#colFormat_" + (reportDisplayIndex - 1)).val() == RIGHT_ALIGN_FLOAT) {
                    tfoot += " style='text-align: right' ";
                }
            } else {
                tfoot += "<td style = 'display:none'";
            }
            if (isFloat(listItem)) {
                listItem = parseFloat(listItem).toFixed(2);
            }
            tfoot += " class='text-center-report  reporttable-footer table-title font-weight-bold col_" + reportDisplayIndex + "'>" + listItem + "</td>";
            j++;
        });
        tfoot += "</tr></tfoot>";
    }
    $(".loader-container-circle").css("visibility", "hidden");
    createdHTML = thead + tbody + tfoot
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    if ($("#Language").val() == "Bangla") {
        today = convertToBanglaNumber(today);
    }

    reportTable.html(createdHTML);

    $("#report-div").show();

    if ($("#Language").val() == "English") {
        console.log("English language found");
        $("#reportTable_info").html("Total " + totalRecord + " entries");
    } else {
        console.log("Bangla language found");
        var recordStr = totalRecord + "";
        $("#reportTable_info").html("সর্বমোট " + convertToBanglaNumber(recordStr) + "টি রেকর্ড");
    }

}

var addables = [0];

function createTableCallback(data) {
    console.log(data);
    if (data['responseCode'] == 200) {
        createTable(data.payload[0], data.payload[1], data.payload[2]);
        getTotalDataCountCallBack(data.payload[1])

    } else {
        toastr.error(data['msg'] + " code: " + data['responseCode']);
    }
}


var updateRecordInfo = function (totalRecord, recordPerPage) {
    totalPageNo = Math.ceil(parseFloat(totalRecord) / parseFloat(recordPerPage));
    $("#tatalPageNo").html(totalPageNo);
    $("#tatalPageNoMobile").html(totalPageNo);
    $("input[name=totalRecord]").val(totalRecord);
}

var getTotalDataCountCallBack = function (data) {
    recordPerPage = $("input[name=RECORDS_PER_PAGE]").val();
    /*totalRecord = data;
    if ($("#Language").val() == "English") {
        console.log("English language found");
        $("#reportTable_info").html("Total " + totalRecord + " entries");
    } else {
        console.log("Bangla language found");
        var recordStr = totalRecord + "";
        $("#reportTable_info").html("সর্বমোট " + convertToBanglaNumber(recordStr) + "টি রেকর্ড");
    }*/
    updateRecordInfo(totalRecord, recordPerPage);
}

var submit = function () {
    $(".navigator").show();
    PreprocessBeforeSubmiting(0, "report");
    let data = reportForm.serialize()
                         .split('&')
                         .filter(e => e.split('=')[1])
                         .join('&')
               + "&" + getReportColumnsParam();
    callAjax(reportForm.attr('action'), data, createTableCallback, "GET");
    if (document.getElementById("searchCriteria").getAttribute("hide") == "true") {
        document.getElementById("searchCriteria").remove();
        document.getElementById("button-div").remove();
    }
}

$(document).ready(function () {
    /*alert("change bottom -24")
    $(".dt-buttons").css("margin-top","-2px !important")*/
    console.log("we r ready 2");
    $('#startDate_js').on('datepicker.change', (event, param) => {
        processNewCalendarDateAndSubmit();
    });
    $('#endDate_js').on('datepicker.change', (event, param) => {
        processNewCalendarDateAndSubmit();
    });
    init(0);
    ajaxSubmit();
});

let dragContent = null;

function dragStart(event) {
    dragContent = event.target;
}

function dragEnter(event) {
    event.target.classList.add('over');
}

function dragLeave(event) {
    event.target.classList.remove('over');
}

function dragOver(event) {
    event.preventDefault();
    event.stopPropagation();
}

function reindexDraggableContents(parentContentContainer) {
    parentContentContainer
        .querySelectorAll('.draggable-content')
        .forEach((item, index) => {
            item.dataset.viewIndex = index;
            columnsArrayMapByDisplayIndex.get(+item.dataset.reportDisplayIndex).viewIndex = index;
        });
    columnsArray = columnsArray.sort((a, b) => a.viewIndex - b.viewIndex);
}

function getReportColumnsParam() {
    if (columnsArray == null || columnsArray.length === 0) {
        return '';
    }
    const reportColumns = columnsArray.sort((a, b) => a.viewIndex - b.viewIndex);

    return "reportColumnDisplayIndexValues=" + reportColumns.map(reportColumn => reportColumn.reportDisplayIndex).join(",")
           + "&reportColumnShowValues=" + reportColumns.map(reportColumn => reportColumn.show).join(",");
}

function dragDrop(event) {
    const dropContent = event.target;
    const dropTrThArray = $('.col_' + dropContent.dataset.reportDisplayIndex).toArray();
    const dragTrThArray = $('.col_' + dragContent.dataset.reportDisplayIndex).toArray();
    dropContent.classList.remove('over');
    const dragContentContainer = dragContent.closest('.draggable-content-container');
    const dropContentContainer = dropContent.closest('.draggable-content-container');
    if (dragContent.dataset.viewIndex < dropContent.dataset.viewIndex) {
        dropContent.after(dragContent);
        for (let i = 0; i < dragTrThArray.length; ++i) {
            dropTrThArray[i].after(dragTrThArray[i]);
        }
    } else {
        dropContentContainer.insertBefore(dragContent, dropContent);
        for (let i = 0; i < dragTrThArray.length; ++i) {
            dropTrThArray[i].before(dragTrThArray[i]);
        }
    }
    reindexDraggableContents(dropContentContainer);
    if (dragContentContainer !== dropContentContainer) {
        reindexDraggableContents(dragContentContainer);
    }
}

function modalCheckBoxToggle(draggableContent, reportDisplayIndex) {
    columnsArrayMapByDisplayIndex.get(+reportDisplayIndex).show = $('#c' + reportDisplayIndex).is(':checked');
    $('.col_' + reportDisplayIndex).toggle();
}

function buildModalBody() {
    console.log('rebuild modal body');
    //selectColumnModalBody.html('');
    let modalBody = '<div class="container draggable-content-container" id="select-column-container"> <form>';
    for (j = 1; j < columnsArray.length; j++) {
        const reportDisplayIndex = columnsArray[j].reportDisplayIndex;
        const viewIndex = columnsArray[j].viewIndex;
        modalBody += '<div class="checkbox draggable-content" draggable="' + isDraggable + '" '
                     + 'data-view-index="' + viewIndex + '" '
                     + 'data-report-display-index="' + reportDisplayIndex + '" ';

        if (typeof isVisible != 'undefined' && !isVisible[j]) {
            modalBody += 'style = "display:none"';
        }
        modalBody += ' onchange="modalCheckBoxToggle(this,' + reportDisplayIndex + ')">';
        modalBody += '<div><input type="checkbox" id="c' + reportDisplayIndex + '"';
        if (columnsArray[j].show) {
            modalBody += " checked "
        }
        modalBody += '/>&nbsp; ' + columnsArray[j].title + '</div> <i class="fas fa-grip-lines"></i> </div>';
    }
    modalBody += '</form></div>';
    //console.log(modalBody);
    $('#select_columns_body').html(modalBody);
}


function buildAllSelectedModalBody(isAllSelect) {
    console.log('rebuild all selected modal body', isAllSelect);
    for (j = 1; j < columnsArray.length; j++) {
        columnsArray[j].show = isAllSelect;
    }
    buildModalBody();
    for (j = 1; j < columnsArray.length; j++) {
        if (columnsArray[j].show) {
            $('.col_' + columnsArray[j].reportDisplayIndex).show();
        } else {
            $('.col_' + columnsArray[j].reportDisplayIndex).hide();
        }
    }
}

function beforeOpenCriteriaModal() {
    buildCriteriaModalBody();
}

function modalCriteriaCheckBoxToggle(index) {
    criteriaArray[index].show = $('#c' + index).is(':checked');
    $('.' + criteriaArray[index].class).toggle();
    if (!criteriaArray[index].show)
        clearFields(criteriaArray[index]);
}

function clearFields(divClass) {
    if (divClass.specialCategory == 'officeModal') {
        clearAllOfficeTags();
        $('.' + divClass.class + ' input[type=checkbox]').prop("checked", false)
    }
    if (divClass.specialCategory == 'checkBox') {
        $('.' + divClass.class + ' input[type=checkbox]').prop("checked", false)
    }
    if (divClass.specialCategory == 'employeeModal') {
        if ($('.' + divClass.class).find('.modalCross')) {
            $('.' + divClass.class).find('.modalCross').click();
        }
    }
    if (divClass.specialCategory == 'date') {
        $.each(divClass.datejsIds, function (innerIndex, e) {
            resetDateById(e);
        });
        clearInputs(divClass);
        return;
    }
    clearInputs(divClass);
    clearSelects(divClass);

}

function clearInputs(divClass) {
    $.each($('.' + divClass.class + ' input'), function (innerIndex, e) {
        console.log("input id", e.id);
        if (e.id.length > 0)
            $('#' + e.id).val('');
    });
}

function clearSelects(divClass) {
    $.each($('.' + divClass.class + ' select'), function (innerIndex, e) {
        console.log("select id", e.id);
        $('#' + e.id).select2("val", "-1");
    });
}

function selectAllCriteriaChange() {
    let isAllSelect = document.getElementById('selectAllCriteria_checkbox').checked;
    buildAllCriteriaModalBody(isAllSelect);
}

function buildCriteriaModalBody() {
    console.log('rebuild modal body');
    //selectColumnModalBody.html('');
    let modalBody = '<div class="container"> <form>';
    for (j = 0; j < criteriaArray.length; j++) {
        modalBody += '<div class="checkbox"';
        if (typeof isVisible != 'undefined' && !isVisible[j]) {
            modalBody += 'style = "display:none"';
        }
        modalBody += ' onchange="modalCriteriaCheckBoxToggle(' + j + ')">';
        modalBody += '<label><input type="checkbox" id="c' + j + '"';
        if (criteriaArray[j].show) {
            modalBody += " checked "
        }
        modalBody += '/>&nbsp; ' + criteriaArray[j].title + '</label></div>';
    }
    modalBody += '</form></div>';
    console.log(modalBody);
    $('#select_criteria_body').html(modalBody);
}


function buildAllCriteriaModalBody(isAllSelect) {
    console.log('rebuild all selected modal body', isAllSelect);
    for (j = 0; j < criteriaArray.length; j++) {
        criteriaArray[j].show = isAllSelect;
    }
    buildCriteriaModalBody();
    for (j = 0; j < criteriaArray.length; j++) {
        if (criteriaArray[j].show) {
            $('.' + criteriaArray[j].class).show();
        } else {
            $('.' + criteriaArray[j].class).hide();
            clearFields(criteriaArray[j]);
        }
    }
}