<%@page pageEncoding="UTF-8" %>
<div id="calendardiv" >
    <div id="visitDate" class="search-criteria-div">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=Language.equalsIgnoreCase("english")?"From Transaction Date":"লেনদেনের তারিখ শুরু"%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="startTrDate_js"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
                       data-label="Document Date" id='startTrDate' name='startTrDate' value=""
                       tag='pb_html'
                />
            </div>
        </div>
    </div>
    <div id="visitDate_3" class="search-criteria-div">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
               <%=Language.equalsIgnoreCase("english")?"To Transaction Date":"লেনদেনের তারিখ পর্যন্ত"%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="endTrDate_js"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
                       data-label="Document Date" id='endTrDate' name='endTrDate' value=""
                       tag='pb_html'
                />
            </div>
        </div>
    </div>
</div>