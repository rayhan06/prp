<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>

<div class="kt-portlet">
    <div class="kt-portlet__body py-0">
        <div class="d-flex justify-content-end align-items-center flex-wrap">
            <div class="d-flex align-items-center">
                <div class="d-flex align-items-center custom-align-content desktop-only justify-content-end">
                    <div class="custom-align-content">
                        <label class="w-100 mt-1 text-nowrap">
                            <%=LM.getText(LC.PBREPORT_OTHER_RECORD_PER_PAGE, loginDTO)%>
                        </label>
                    </div>
                    <div class="ml-2">
                        <input
                                type="text"
                                style="border-radius: 8px"
                                class="form-control text-center"
                                name="RECORDS_PER_PAGE"
                                id="RECORDS_PER_PAGE"
                                placeholder=""
                                value="<%=SessionConstants.REPORT_ROWS_PER_PAGE%>"
                                onKeyUp="ajaxSubmit();"
                        >
                    </div>
                </div>
                <div class="mx-2">
                    <nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: -1px 0px 0px 2px;">
                            <li class="page-item" style="display:table-cell !important;">
                                <a
                                        class="page-link"
                                        style="border-top-left-radius: 8px; border-bottom-left-radius: 8px"
                                        onclick="firstloadSubmit();"
                                        id="firstLoad"
                                        aria-label="First"
                                        title="Left"
                                >
                                    <i class="fa fa-angle-double-left" aria-hidden="true"
                                       style="padding: 4px;"></i>
                                    <span class="sr-only">
                                                <%=LM.getText(LC.PBREPORT_OTHER_FIRST, loginDTO)%>
                                            </span>
                                </a></li>
                            <li class="page-item" style="display:table-cell !important;">
                                <a class="page-link"
                                   onclick="prevloadSubmit();"
                                   id="previousLoad"
                                   aria-label="Previous"
                                   title="Previous"
                                >
                                    <i class="fa fa-angle-left" aria-hidden="true"
                                       style="padding: 4px;"></i>
                                    <span class="sr-only">
                                                <%=LM.getText(LC.PBREPORT_OTHER_PREVIOUS, loginDTO)%>
                                            </span>
                                </a>
                            </li>

                            <li class="page-item" style="display:table-cell !important;">
                                <a class="page-link"
                                   onclick="nextloadSubmit();"
                                   id="nextLoad"
                                   aria-label="Next"
                                   title="Next"
                                >
                                    <i class="fa fa-angle-right" aria-hidden="true"
                                       style="padding: 4px;"></i>
                                    <span class="sr-only">
                                                <%=LM.getText(LC.PBREPORT_OTHER_NEXT, loginDTO)%>
                                            </span>
                                </a></li>
                            <li class="page-item" style="display:table-cell !important;">
                                <a
                                        class="page-link"
                                        style="border-top-right-radius: 8px; border-bottom-right-radius: 8px"
                                        onclick="lastloadSubmit();"
                                        id="lastLoad"
                                        aria-label="Last"
                                        title="Last"
                                >
                                    <i class="fa fa-angle-double-right" aria-hidden="true"
                                       style="padding: 4px;"></i>
                                    <span class="sr-only">
                                                <%=LM.getText(LC.PBREPORT_OTHER_LAST, loginDTO)%>
                                            </span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="d-flex align-items-center custom-align-content justify-content-end" >
                    <div>
                        <input type="text" style="border-radius: 8px"
                               class="form-control custom-from-control text-center"
                               name="pageno"
                               ID="pageno"
                               value="1" size="15" onKeyUp="ajaxSubmit();">
                    </div>
                    <label class="hidden-xs mx-2">
                        <%=LM.getText(LC.PBREPORT_OTHER_OF, loginDTO)%>
                    </label>
                    <label class="hidden-xs" ID="tatalPageNo"></label>
                    <input type="hidden"
                           name="totalRecord"/>
                    <input type="hidden"
                           name="tatalPageNo"/>
                </div>
            </div>
        </div>
    </div>
</div>

