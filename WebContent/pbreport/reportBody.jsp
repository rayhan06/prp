<%@page import="util.ActionTypeConstant" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="centre.CentreDAO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.UtilCharacter" %>
<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="org.apache.commons.lang3.tuple.*" %>
<%@ page import="java.util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String userfullName = userDTO.fullName;
    int userCentre = userDTO.centreType;
    CentreDAO centreDAO = new CentreDAO();
    String centreName = centreDAO.getCentreNameByCentreID(userCentre);
    String tableName = (String) request.getAttribute("tableName");
    String tableNameUp = tableName.substring(0, 1).toUpperCase() + tableName.substring(1).toLowerCase();
    String servletName = tableNameUp + "_Servlet";
    String searchDiv = "../" + tableName + "/" + tableName + "_SearchDiv.jsp";
    request.setAttribute("searchDiv", searchDiv);
    System.out.println("searchDiv = " + searchDiv);
    System.out.println("servletName = " + servletName);
    Boolean dontUseOutDatedModal = (Boolean) request.getAttribute("dontUseOutDatedModal");
    String context = request.getContextPath() + "/";
    ArrayList<Pair<Integer, Integer>> clientSideDataFormatting = (ArrayList<Pair<Integer, Integer>>)request.getAttribute("clientSideDataFormatting");
    String extraText = (String)request.getAttribute("extraText");
    System.out.println("extraText = " + extraText);
%>

<%if (dontUseOutDatedModal == null) {%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<%}%>

<style>

    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 3;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }

    .draggable-content {
        cursor: pointer;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 10px;
        margin: 5px 0;
        box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
    }

    .draggable-content.over {
        background-color: #dedede;
    }
</style>

<div class="loader-container-circle">
    <div class="loader-circle"></div>
</div>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-cubes"></i>
                    ${reportName}
                </h3>

            </div>
        </div>
        <%
        if(clientSideDataFormatting != null)
        {
        	for(Pair<Integer, Integer> colFormat: clientSideDataFormatting)
        	{
        		%>
        		<input type = "hidden" id = "colFormat_<%=colFormat.getLeft()%>" value = "<%=colFormat.getRight()%>" />
        		<%
        	}
        }
        %>
        <form
                class="form-horizontal"
                id="ReportForm"
                action="<%=request.getContextPath()%>/<%=servletName%>?actionType=<%=ActionTypeConstant.REPORT_RESULT%>"
        >
            <input type="hidden" name="orderByCol" id="orderByCol" value=""/>
            <input type="hidden" name="isAscending" id="isAscending" value=""/>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                ${reportName}
                                            </h4>
                                        </div>
                                    </div>

                                    <div id="searchCriteria" class="">
                                        <jsp:include page="${searchDiv}"/>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right mt-3">
                        <button type="button" class="btn btn-sm btn-info shadow btn-border-radius"
                                id='reportSearchButton'
                                onclick="ajaxSubmit()"><%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%>
                        </button>
                    </div>
                </div>
                <div class="">
                    <div class="form-body">
                        <div class="d-flex flex-column flex-md-row align-items-md-end justify-content-md-between mt-5">
                            <h5 class="table-title">
                                ${reportName}
                            </h5>
                            <div id="btndiv" class="mb-2">
                                <button type="button"
                                        class="btn btn-sm btn-warning text-white shadow btn-border-radius"
                                        id='reportPrinter'
                                        onclick="localPrint()"><%=LM.getText(LC.HM_PRINT, loginDTO)%>
                                </button>
                                <a class="btn btn-sm btn-success shadow btn-border-radius text-white mx-2"
                                   id='reportXlButton' onclick="getXl()"
                                ><%=LM.getText(LC.HM_EXCEL, loginDTO)%>
                                </a>
                                <button type="button"
                                        class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                                        data-toggle="modal" data-target="#select_column_div" id="select_columns_btn"
                                        onclick="beforeOpenModal()">
                                    <%=LM.getText(LC.HM_SELECT_COLUMNS, loginDTO)%>
                                </button>
                            </div>
                        </div>
                        <div class="modal fade" id="select_column_div" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content modal-md">
                                    <div class="modal-header">
                                        <div class="col-6">
                                            <h4 class="table-title">
                                                <%=LM.getText(LC.HM_SELECT_COLUMNS, loginDTO)%>
                                            </h4></div>
                                        <div class="col-2"></div>
                                        <label class="col-3"
                                               for="selectAll_checkbox">
                                            <%=LM.getText(LC.HM_SELECT_SELECT_ALL, loginDTO)%>
                                        </label>
                                        <div class="col-1" id='selectAll'>
                                            <input type='checkbox' name='selectAll'
                                                   id='selectAll_checkbox' checked
                                                   onchange="selectAllChange()" value='false'>
                                        </div>


                                    </div>

                                    <div class="modal-body" id="select_columns_body">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="select_criteria_div" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content modal-md">
                                    <div class="modal-header">
                                        <div class="col-6">
                                            <h4 class="table-title">
                                                <%=LM.getText(LC.HM_SELECT_COLUMNS, loginDTO)%>
                                            </h4></div>
                                        <div class="col-2"></div>
                                        <label class="col-3"
                                               for="selectAllCriteria_checkbox">
                                            <%=LM.getText(LC.HM_SELECT_SELECT_ALL, loginDTO)%>
                                        </label>
                                        <div class="col-1" id='selectAllCriteria'>
                                            <input type='checkbox' name='selectAllCriteria'
                                                   id='selectAllCriteria_checkbox'
                                                   onchange="selectAllCriteriaChange()" value='false'>
                                        </div>
                                    </div>
                                    <div class="modal-body" id="select_criteria_body">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            
                            <div class="table-responsive" id="pritDiv">
                            	<div id="titleTableDiv" style = "display:none">
	                                <div class="container my-5">
	                                    <div class="text-center">
	                                        <img
	                                                class="parliament-logo"
	                                                width="8%"
	                                                src="<%=context%>assets/images/perliament_logo_final2.png"
	                                                alt=""
	                                        />
	                                        <h3 class="mt-3">
	                                            <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
	                                        </h3>
	                                        <h4 class="mt-3">
	                                            <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
	                                        </h4>
	                                        <h5 class="mt-3">
	                                            <%=LM.getText(LC.HM_PARLIAMENT_PHONE, loginDTO)%>
	                                        </h5>
	                                        <span style="text-decoration: underline;">
	                                            ${reportName}
	                                        </span>
	                                        
	                                        <div >
	                                           <%=extraText != null?extraText:""%>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div id = "dateText" class="my-3" style = "font-weight:bold">
	                             </div>
                                <div class="d-flex justify-content-between align-items-center">
                                	
                                    <div id="reportTable_info" class="my-3">
                                    </div>
                                    <h6 class="mr-2 font-weight-bold mb-0" id="not-senior-wise">জ্যেষ্ঠতার ভিত্তিতে নয়</h6>
                                </div>
                                <table
                                        class="table table-striped table-bordered"
                                        id="reportTable">
                                    <thead>
                                    <tr>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type = "hidden" name = "add1WithEnd" id = "add1WithEnd" value="false" />
            <%@include file="../pbreport/pagination.jsp" %>
        </form>
    </div>
</div>

<script>
    let notSeniorBase = true;
    $(document).ready(() => {
        if(!notSeniorBase){
            $('#not-senior-wise').addClass("d-none");
        }
        let $html = $('html');
        $html.on('dragstart', '.draggable-content', dragStart);
        $html.on('dragover', '.draggable-content', dragOver);
        $html.on('dragenter', '.draggable-content', dragEnter);
        $html.on('dragleave', '.draggable-content', dragLeave);
        $html.on('drop', '.draggable-content', dragDrop);
    });

    function getXl() {
        var url = "<%=servletName%>?actionType=xl";

        if ($("#orderByCol").length && $("#orderByCol").val() != "") {
            url += "&orderByCol=" + $("#orderByCol").val();
        }

        if ($("#isAscending").length && $("#isAscending").val() != "") {
            url += "&isAscending=" + $("#isAscending").val();
        }


        url += "&" + $("form").serialize();
        let columns = null;
        for (let i = 0; i < columnsArray.length; i++) {
            if (columnsArray[i].show) {
                if (columns != null) {
                    columns += ",";
                }
                columns += i;
            }
        }
        url += "&columns=" + columns;
        url += "&" + getReportColumnsParam();
        window.location.href = url;
    }

    function localPrint() {
        $("#titleTableDiv").css("display", "block");
        let scrollY = Math.ceil(window.scrollY);
        $('.up-down-link').hide();
        $('#select_columns_btn').hide();
        printAnyDiv('pritDiv');

        $('.up-down-link').show();
        $('#select_columns_btn').show();
        window.scrollTo(0, scrollY);
        $("#titleTableDiv").css("display", "none");
    }

    /*
        Always rebuild modal body ui, because unchecked checkbox are become selected automatically after clicking on print button
     */
    function beforeOpenModal() {
        buildModalBody();
    }

    function selectAllChange() {
        let isAllSelect = document.getElementById('selectAll_checkbox').checked;
        buildAllSelectedModalBody(isAllSelect);
    }
</script>