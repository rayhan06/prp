<%@page pageEncoding="UTF-8" %>
<div id="calendardiv" >
    <div id="visitDate" class="search-criteria-div">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=Language.equalsIgnoreCase("english")?"From Visit Date":"ভিজিটের তারিখ শুরু"%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="startVisitDate_js"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
                       data-label="Document Date" id='startVisitDate' name='startVisitDate' value=""
                       tag='pb_html'
                />
            </div>
        </div>
    </div>
    <div id="visitDate_3" class="search-criteria-div">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
               <%=Language.equalsIgnoreCase("english")?"To Visit Date":"ভিজিটের তারিখ পর্যন্ত"%>
            </label>
            <div class="col-md-9">
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="endVisitDate_js"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
                <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
                       data-label="Document Date" id='endVisitDate' name='endVisitDate' value=""
                       tag='pb_html'
                />
            </div>
        </div>
    </div>
</div>