<%@page pageEncoding="UTF-8" %>

<%@page import="time_permission_request.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_TIME_PERMISSION_REQUEST;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Time_permission_requestDTO time_permission_requestDTO = (Time_permission_requestDTO) request.getAttribute("time_permission_requestDTO");
    CommonDTO commonDTO = time_permission_requestDTO;
    String servletName = "Time_permission_requestServlet";

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    String Message = "Done";
    approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("time_permission_request", time_permission_requestDTO.iD);

    System.out.println("time_permission_requestDTO = " + time_permission_requestDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Time_permission_requestDAO time_permission_requestDAO = (Time_permission_requestDAO) request.getAttribute("time_permission_requestDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_employeeRecordsId'>
    <%
        value = time_permission_requestDTO.employeeRecordsId + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_timeRequestCat'>
    <%
        value = time_permission_requestDTO.timeRequestCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "time_request", time_permission_requestDTO.timeRequestCat);
    %>

    <%=value%>


</td>


<td id='<%=i%>_reasonTypeCat'>
    <%
        value = time_permission_requestDTO.reasonTypeCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "reason_type", time_permission_requestDTO.reasonTypeCat);
    %>

    <%=value%>


</td>


<td id='<%=i%>_isApproved'>
    <%
        value = time_permission_requestDTO.isApproved + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_approvalDate'>
    <%
        value = time_permission_requestDTO.approvalDate + "";
    %>
    <%
        String formatted_approvalDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=formatted_approvalDate%>


</td>


<td id='<%=i%>_insertedBy'>
    <%
        value = time_permission_requestDTO.insertedBy + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_modifiedBy'>
    <%
        value = time_permission_requestDTO.modifiedBy + "";
    %>

    <%=value%>


</td>


<td>
	<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
			onclick="location.href='Time_permission_requestServlet?actionType=view&ID=<%=time_permission_requestDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td id='<%=i%>_Edit'>
	<button type="button"
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Time_permission_requestServlet?actionType=getEditPage&ID=<%=time_permission_requestDTO.iD%>'">
		<i class="fa fa-edit"></i>
	</button>
</td>


<td>
    <%
        if (time_permission_requestDTO.isDeleted == 0 && approval_execution_tableDTO != null) {
    %>
    <a href="Approval_execution_tableServlet?actionType=search&tableName=time_permission_request&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"><%=LM.getText(LC.HM_HISTORY, loginDTO)%>
    </a>
    <%
    } else {
    %>
    <%=LM.getText(LC.HM_NO_HISTORY_IS_AVAILABLE, loginDTO)%>
    <%
        }
    %>
</td>

<td>
    <%
        if (time_permission_requestDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {
    %>
    <button type="button" class="btn btn-sm btn-success shadow btn-border-radius" data-toggle="modal" data-target="#sendToApprovalPathModal">
        <%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>
    </button>
    <%@include file="../inbox_internal/sendToApprovalPathModal.jsp" %>
    <%
    } else {
    %>
    <%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
    <%
        }
    %>
</td>
<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=time_permission_requestDTO.iD%>'/></span>
    </div>
</td>


<script>
    window.onload = function () {
        console.log("using ckEditor");
        CKEDITOR.replaceAll();
    }

</script>	

