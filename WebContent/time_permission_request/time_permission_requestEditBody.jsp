<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="time_permission_request.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@ page import="user.*" %>

<%@page import="workflow.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%
    Time_permission_requestDTO time_permission_requestDTO;
    time_permission_requestDTO = (Time_permission_requestDTO) request.getAttribute("time_permission_requestDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (time_permission_requestDTO == null) {
        time_permission_requestDTO = new Time_permission_requestDTO();

    }
    System.out.println("time_permission_requestDTO = " + time_permission_requestDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_TIME_PERMISSION_REQUEST_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.TIME_PERMISSION_REQUEST_ADD_TIME_PERMISSION_REQUEST_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    if (request.getParameter("isPermanentTable") != null) {
        isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
    }

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
    ApprovalPathDetailsDTO approvalPathDetailsDTO = null;

    String tableName = "time_permission_request";

    boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

    if (!isPermanentTable) {
        approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("time_permission_request", time_permission_requestDTO.iD);
        System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
        approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
        approval_execution_table_initiationDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("time_permission_request", time_permission_requestDTO.iD);
        if (approvalPathDetailsDTO != null && approvalPathDetailsDTO.organogramId == userDTO.organogramID) {
            canApprove = true;
            if (approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR) {
                canValidate = true;
            }
        }

        isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

        canTerminate = isInitiator && time_permission_requestDTO.isDeleted == 2;
    }
    String Language = LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
%>
<%--
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<h3 class="kt-subheader__title"> Asset Management </h3>
	</div>
</div>

<!-- end:: Subheader -->--%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Time_permission_requestServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>'
                                           value='<%=time_permission_requestDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='employeeRecordsId'
                                           id='employeeRecordsId_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.employeeRecordsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_TIMEREQUESTCAT, loginDTO)) : (LM.getText(LC.TIME_PERMISSION_REQUEST_ADD_TIMEREQUESTCAT, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 " id='timeRequestCat_div_<%=i%>'>
                                            <select class='form-control' name='timeRequestCat'
                                                    id='timeRequestCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        Options = CatDAO.getOptions(Language, "time_request", time_permission_requestDTO.timeRequestCat);
                                                    } else {
                                                        Options = CatDAO.getOptions(Language, "time_request", -1);
                                                    }
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_REASONTYPECAT, loginDTO)) : (LM.getText(LC.TIME_PERMISSION_REQUEST_ADD_REASONTYPECAT, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 " id='reasonTypeCat_div_<%=i%>'>
                                            <select class='form-control' name='reasonTypeCat'
                                                    id='reasonTypeCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        Options = CatDAO.getOptions(Language, "reason_type", time_permission_requestDTO.reasonTypeCat);
                                                    } else {
                                                        Options = CatDAO.getOptions(Language, "reason_type", -1);
                                                    }
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_ISAPPROVED, loginDTO)) : (LM.getText(LC.TIME_PERMISSION_REQUEST_ADD_ISAPPROVED, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 " id='isApproved_div_<%=i%>'>
                                            <input type='checkbox' class='form-control-sm mt-1'
                                                   name='isApproved' id='isApproved_checkbox_<%=i%>'
                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(time_permission_requestDTO.isApproved).equals("true"))?("checked"):""%>
                                                   tag='pb_html'><br>
                                            <div class="col-8"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_APPROVALDATE, loginDTO)) : (LM.getText(LC.TIME_PERMISSION_REQUEST_ADD_APPROVALDATE, loginDTO))%>
                                        </label>
                                        <div class="col-md-9 d-flex justify-content-between align-items-center" id='approvalDate_div_<%=i%>'>
                                            <input type='text'
                                                   class='input-width92 form-control formRequired'
                                                   readonly="readonly"
                                                   data-label="Document Date" id='approval-date'
                                                   name='approvalDate' value='' tag='pb_html'>
                                            <button type="button" class="btn btn-danger shadow tp-button-cross-picker"
                                                    id='app-date-crs-but'>X
                                            </button>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.insertedBy + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.modifiedBy + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='jobCat'
                                           id='jobCat_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.jobCat + "'"):("'" + "-1" + "'")%> tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    <%
                                        if (canValidate) {
                                    %>
                                    <div class="row div_border attachement-div">
                                        <div class="col-md-12">
                                            <h5><%=LM.getText(LC.HM_ATTACHMENTS, loginDTO)%>
                                            </h5>
                                            <%
                                                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                            %>
                                            <div class="dropzone"
                                                 action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
                                                <input type='file' style="display: none"
                                                       name='approval_attached_fileDropzoneFile'
                                                       id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden'
                                                   name='approval_attached_fileDropzoneFilesToDelete'
                                                   id='approval_attached_fileDropzoneFilesToDelete_<%=i%>'
                                                   value=''
                                                   tag='pb_html'/> <input type='hidden'
                                                                          name='approval_attached_fileDropzone'
                                                                          id='approval_attached_fileDropzone_dropzone_<%=i%>'
                                                                          tag='pb_html'
                                                                          value='<%=ColumnID%>'/>
                                        </div>

                                        <div class="col-md-12">
                                            <h5><%=LM.getText(LC.HM_REMARKS, loginDTO)%>
                                            </h5>

                                            <textarea class='form-control' name='remarks'
                                                      id='<%=i%>_remarks' tag='pb_html'></textarea>
                                        </div>
                                    </div>
                                    <%
                                        }
                                    %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%
                                if (actionName.equals("edit")) {
                            %>
                            <%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_TIME_PERMISSION_REQUEST_CANCEL_BUTTON, loginDTO)%>
                            <%
                            } else {
                            %>
                            <%=LM.getText(LC.TIME_PERMISSION_REQUEST_ADD_TIME_PERMISSION_REQUEST_CANCEL_BUTTON, loginDTO)%>
                            <%
                                }

                            %>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                            <%
                                if (actionName.equals("edit")) {
                            %>
                            <%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_TIME_PERMISSION_REQUEST_SUBMIT_BUTTON, loginDTO)%>
                            <%
                            } else {
                            %>
                            <%=LM.getText(LC.TIME_PERMISSION_REQUEST_ADD_TIME_PERMISSION_REQUEST_SUBMIT_BUTTON, loginDTO)%>
                            <%
                                }
                            %>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    $(document).ready(function () {

        dateTimeInit("<%=Language%>");

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }

        preprocessCheckBoxBeforeSubmitting('isApproved', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Time_permission_requestServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


    $(function () {

        $("#approval-date").datepicker({
            dateFormat: 'dd/mm/yy',
            yearRange: '1900:2100',
            changeYear: true,
            changeMonth: true,
            buttonText: "<i class='fa fa-calendar'></i>"
        });

        $('#app-date-crs-but').click(() => {
            $('#approval-date').val('');
        });


        $("#approval-date").datepicker('option', 'maxDate', new Date());


        $("#approval-date").val('');

        <%
     if(actionName.equals("edit")) {
    	String fndDateStr= dateFormat.format(new Date(time_permission_requestDTO.approvalDate));
    	%>
        $("#approval-date").val('<%=fndDateStr%>');
        <%}
        %>
    });
</script>
<style>
    .input-width92 {
        width: 92%;
        float: left;
    }
</style>




