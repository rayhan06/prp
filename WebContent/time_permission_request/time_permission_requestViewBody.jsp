

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="time_permission_request.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>

<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Time_permission_requestDAO time_permission_requestDAO = new Time_permission_requestDAO("time_permission_request");
Time_permission_requestDTO time_permission_requestDTO = (Time_permission_requestDTO)time_permission_requestDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					Time Permission Request Details
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body form-body">
			<h5 class="table-title">
				Time Permission Request
			</h5>
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_EMPLOYEERECORDSID, loginDTO)%></b></td>
						<td>

							<%
								value = time_permission_requestDTO.employeeRecordsId + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_TIMEREQUESTCAT, loginDTO)%></b></td>
						<td>

							<%
								value = time_permission_requestDTO.timeRequestCat + "";
							%>
							<%
								value = CatDAO.getName(Language, "time_request", time_permission_requestDTO.timeRequestCat);
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_REASONTYPECAT, loginDTO)%></b></td>
						<td>

							<%
								value = time_permission_requestDTO.reasonTypeCat + "";
							%>
							<%
								value = CatDAO.getName(Language, "reason_type", time_permission_requestDTO.reasonTypeCat);
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_ISAPPROVED, loginDTO)%></b></td>
						<td>

							<%
								value = time_permission_requestDTO.isApproved + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_APPROVALDATE, loginDTO)%></b></td>
						<td>

							<%
								value = time_permission_requestDTO.approvalDate + "";
							%>
							<%
								String formatted_approvalDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
							%>
							<%=formatted_approvalDate%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_INSERTEDBY, loginDTO)%></b></td>
						<td>

							<%
								value = time_permission_requestDTO.insertedBy + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_MODIFIEDBY, loginDTO)%></b></td>
						<td>

							<%
								value = time_permission_requestDTO.modifiedBy + "";
							%>

							<%=value%>


						</td>

					</tr>









				</table>
			</div>
		</div>
	</div>
</div>