<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="time_permission_request.Time_permission_requestDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Time_permission_requestDTO time_permission_requestDTO = (Time_permission_requestDTO)request.getAttribute("time_permission_requestDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(time_permission_requestDTO == null)
{
	time_permission_requestDTO = new Time_permission_requestDTO();
	
}
System.out.println("time_permission_requestDTO = " + time_permission_requestDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.TIME_PERMISSION_REQUEST_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=time_permission_requestDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeeRecordsId'>")%>
			

		<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.employeeRecordsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_timeRequestCat'>")%>
			
	
	<div class="form-inline" id = 'timeRequestCat_div_<%=i%>'>
		<select class='form-control'  name='timeRequestCat' id = 'timeRequestCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "time_request", time_permission_requestDTO.timeRequestCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "time_request", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_reasonTypeCat'>")%>
			
	
	<div class="form-inline" id = 'reasonTypeCat_div_<%=i%>'>
		<select class='form-control'  name='reasonTypeCat' id = 'reasonTypeCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "reason_type", time_permission_requestDTO.reasonTypeCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "reason_type", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isApproved'>")%>
			
	
	<div class="form-inline" id = 'isApproved_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='isApproved' id = 'isApproved_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(time_permission_requestDTO.isApproved).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_approvalDate'>")%>
			
	
	<div class="form-inline" id = 'approvalDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'approvalDate_date_<%=i%>' name='approvalDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_approvalDate = dateFormat.format(new Date(time_permission_requestDTO.approvalDate));
	%>
	'<%=formatted_approvalDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.insertedBy + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + time_permission_requestDTO.modifiedBy + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobCat" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value='<%=time_permission_requestDTO.jobCat%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + time_permission_requestDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=time_permission_requestDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Time_permission_requestServlet?actionType=view&ID=<%=time_permission_requestDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Time_permission_requestServlet?actionType=view&modal=1&ID=<%=time_permission_requestDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	