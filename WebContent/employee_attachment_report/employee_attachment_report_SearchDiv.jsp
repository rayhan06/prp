<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.Calendar" %>
<%@page pageEncoding="UTF-8" %>

<%
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>

<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp" />

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row">
    <div class="col-12">
        <div class="row mx-2 mx-md-0">
            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <%--Office Unit Modal Trigger Button--%>
                        <button type="button" class="btn btn-block shadow btn-primary text-white btn-border-radius"
                                id="office_units_id_modal_button" onclick="officeModalButtonClicked();">
                            <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                        </button>
                        <div class="input-group" id="office_units_id_div" style="display: none">
                            <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                            <button type="button" class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                    id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                            </button>
                            <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                        <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস"%>
                    </label>
                    <div class="col-md-9" id='onlySelectedOffice'>
                        <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                               id='onlySelectedOffice_checkbox'
                               onchange="this.value = this.checked;" value='false'>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
                <div class="form-group row">
                    <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                        <div class="tag template-tag">
                            <span class="tag-name"></span>
                            <i class="fas fa-times-circle tag-remove-btn"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right" for="status">
                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_STATUS, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <select class='form-control' name='status' id='status'>
                            <%=CatRepository.getOptions(Language, "attachment_status", CatDTO.CATDEFAULT)%>
                        </select>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_EMPLOYEERECORDID, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                id="employee_record_id_modal_button"
                                onclick="employeeRecordIdModalBtnClicked();">
                            <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                        </button>
                        <div class="input-group" id="employee_record_id_div" style="display: none">
                            <input type="hidden" name='employeeRecordId' id='employee_record_id_input' value="">
                            <button type="button" class="btn btn-secondary form-control" disabled
                                    id="employee_record_id_text"></button>
                            <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger"
                                    onclick="crsBtnClicked('employee_record_id');"
                                    id='employee_record_id_crs_btn' tag='pb_html'>
                                x
                            </button>
                        </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_STARTDATE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="startDateFrom-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                        </jsp:include>
                        <input type="hidden" name='startDateFrom' id='startDateFrom' value=""/>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_STARTDATE_5, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="startDateTo-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                        </jsp:include>
                        <input type="hidden" name='startDateTo' id='startDateTo' value=""/>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_ENDDATE_FROM, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="endDateFrom-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                        </jsp:include>
                        <input type="hidden" name='endDateFrom' id='endDateFrom' value=""/>
                    </div>
                </div>
            </div>

            <div class="search-criteria-div col-md-6">
                <div class="form-group row">
                    <label class="col-md-3 control-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_REPORT_WHERE_ENDDATE_TO, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="endDateTo-js"/>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            <jsp:param name="END_YEAR" value="<%=year + 5%>"/>
                        </jsp:include>
                        <input type="hidden" name='endDateTo' id='endDateTo' value=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });
    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInTags,
            isMultiSelect: true,
            keepLastSelectState: true
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.employeeNameEn + ', ' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + ', ' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
        $('#employee_record_id_input').val(empInfo.employeeRecordId);
    }


    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: viewEmployeeRecordIdInInput
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    function init() {
        dateTimeInit($("#Language").val());
    }

    function PreprocessBeforeSubmiting() {
        $('#startDateFrom').val(getDateTimestampById('startDateFrom-js'));
        $('#startDateTo').val(getDateTimestampById('startDateTo-js'));
        $('#endDateFrom').val(getDateTimestampById('endDateFrom-js'));
        $('#endDateTo').val(getDateTimestampById('endDateTo-js'));
    }
</script>