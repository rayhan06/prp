
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="job_applicant_certification.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat"%>
<style>
	.ui-datepicker-calendar {
		display: none;
	}
	.ui-datepicker-current {
		display: none;
	}
</style>
<%
	Job_applicant_certificationDTO job_applicant_certificationDTO;
	job_applicant_certificationDTO = (Job_applicant_certificationDTO)request.getAttribute("job_applicant_certificationDTO");
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	if(job_applicant_certificationDTO == null)
	{
		job_applicant_certificationDTO = new Job_applicant_certificationDTO();

	}
	System.out.println("job_applicant_certificationDTO = " + job_applicant_certificationDTO);

	String actionName;
	System.out.println("actionType = " + request.getParameter("actionType"));
	if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
	{
		actionName = "add";
	}
	else
	{
		actionName = "edit";
	}
	String formTitle;
	if(actionName.equals("edit"))
	{
		formTitle = LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEE_CERTIFICATION_EDIT_FORMNAME, loginDTO);
	}
	else
	{
		formTitle = LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_EMPLOYEE_CERTIFICATION_ADD_FORMNAME, loginDTO);
	}

	String ID = request.getParameter("ID");
	if(ID == null || ID.isEmpty())
	{
		ID = "0";
	}
	System.out.println("ID = " + ID);
	int i = 0;

	String value = "";

	int childTableStartingID = 1;

	long ColumnID;
	FilesDAO filesDAO = new FilesDAO();
	boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Job_applicant_certificationServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
			  id="employee-certification" name="employee-certification"  method="POST" enctype = "multipart/form-data"
			  onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">















































				<%@ page import="java.text.SimpleDateFormat"%>
				<%@ page import="java.util.Date"%>

				<%@ page import="pb.*"%>

				<%
					String Language = LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_LANGUAGE, loginDTO);
					String Options;
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					Date date = new Date();
					String datestr = dateFormat.format(date);
				%>


				<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=job_applicant_certificationDTO.iD%>' tag='pb_html'/>



				<input type='hidden' class='form-control'  name='jobApplicantId' id = 'jobApplicantId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.jobApplicantId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
				<input type='hidden' class='form-control'  name='jobId' id = 'jobId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.jobId + "'"):("'" + request.getParameter("jobId") + "'")%> tag='pb_html'/>


				<label class="col-lg-3 control-label">
					<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CERTIFICATIONNAME, loginDTO)):(LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_CERTIFICATIONNAME, loginDTO))%>
				</label>
				<div class="form-group ">
					<div class="col-lg-6 " id = 'certificationName_div_<%=i%>'>
						<input type='text' class='form-control'  name='certificationName' id = 'certificationName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.certificationName + "'"):("'" + "" + "'")%>   tag='pb_html'/>
					</div>
				</div>


				<label class="col-lg-3 control-label">
					<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGORGANIZATION, loginDTO)):(LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_ISSUINGORGANIZATION, loginDTO))%>
				</label>
				<div class="form-group ">
					<div class="col-lg-6 " id = 'issuingOrganization_div_<%=i%>'>
						<input type='text' class='form-control'  name='issuingOrganization' id = 'issuingOrganization_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.issuingOrganization + "'"):("'" + "" + "'")%>   tag='pb_html'/>
					</div>
				</div>


				<label class="col-lg-3 control-label">
					<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGINSTITUTEADDRESS, loginDTO)):(LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_ISSUINGINSTITUTEADDRESS, loginDTO))%>
				</label>
				<div class="form-group ">
					<div class="col-lg-6 " id = 'issuingInstituteAddress_div_<%=i%>'>
						<input type='text' class='form-control'  name='issuingInstituteAddress' id = 'issuingInstituteAddress_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.issuingInstituteAddress + "'"):("'" + "" + "'")%>   tag='pb_html'/>
					</div>
				</div>


				<label class="col-lg-3 control-label">
					<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDFROM, loginDTO)):(LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_VALIDFROM, loginDTO))%>
				</label>
				<div class="form-group ">
					<div class="col-lg-6 " id = 'validFrom_div_<%=i%>'>
						<input type='text' class='form-control date-picker validFrom' autocomplete='off'  name='validFrom' id = 'validFrom_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.validFrom + "'"):("'" + "" + "'")%>   tag='pb_html'/>
					</div>
				</div>


				<label class="col-lg-3 control-label">
					<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_HASEXPIRYCAT, loginDTO)):(LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_HASEXPIRYCAT, loginDTO))%>
				</label>
				<div class="form-group ">
					<div class="col-lg-6 " id = 'hasExpiryCat_div_<%=i%>'>
						<select class='form-control hasExpiry'  name='hasExpiryCat' id = 'hasExpiryCat_category_<%=i%>'   tag='pb_html'>
							<%
								if(actionName.equals("edit"))
								{
									Options = CatDAO.getOptions(Language, "has_expiry", job_applicant_certificationDTO.hasExpiryCat);
								}
								else
								{
									Options = CatDAO.getOptions(Language, "has_expiry", -1);
								}
							%>
							<%=Options%>
						</select>

					</div>
				</div>


				<label class="col-lg-3 control-label validUpto">
					<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDUPTO, loginDTO)):(LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_VALIDUPTO, loginDTO))%>
				</label>
				<div class="form-group validUpto">
					<div class="col-lg-6 " id = 'validUpto_div_<%=i%>'>
						<input type='text' class='form-control date-picker-to validUpto' autocomplete='off' name='validUpto' id = 'validUpto_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.validUpto + "'"):("'" + "" + "'")%>   tag='pb_html'/>
					</div>
				</div>


				<label class="col-lg-3 control-label">
					<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CREDENTIALID, loginDTO)):(LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_CREDENTIALID, loginDTO))%>
				</label>
				<div class="form-group ">
					<div class="col-lg-6 " id = 'credentialId_div_<%=i%>'>
						<input type='text' class='form-control'  name='credentialId' id = 'credentialId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.credentialId + "'"):("'" + "" + "'")%>   tag='pb_html'/>
					</div>
				</div>


				<label class="col-lg-3 control-label">
					<%=(actionName.equals("edit"))?(LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_FILESDROPZONE, loginDTO)):(LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_FILESDROPZONE, loginDTO))%>
				</label>
				<div class="form-group ">
					<div class="col-lg-8 " id = 'filesDropzone_div_<%=i%>'>
						<%
							if(actionName.equals("edit"))
							{
								List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(job_applicant_certificationDTO.filesDropzone);
						%>
						<table>
							<tr>
								<%
									if(filesDropzoneDTOList != null)
									{
										for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
										{
											FilesDTO filesDTO = filesDropzoneDTOList.get(j);
											byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
								%>
								<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
									<%
										if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
										{
									%>
									<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px' />
									<%
										}
									%>
									<a href = 'Job_applicant_certificationServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
									<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
								</td>
								<%
										}
									}
								%>
							</tr>
						</table>
						<%
							}
						%>

						<%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
						<div class="dropzone" action="Job_applicant_certificationServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?job_applicant_certificationDTO.filesDropzone:ColumnID%>">
							<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>
						</div>
						<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
						<input type='hidden' name='filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?job_applicant_certificationDTO.filesDropzone:ColumnID%>'/>


					</div>
				</div>


				<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>


				<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>


				<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>


				<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>



				<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_certificationDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>









				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
						<%
							if(actionName.equals("edit"))
							{
						%>
						<%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEE_CERTIFICATION_CANCEL_BUTTON, loginDTO)%>
						<%
						}
						else
						{
						%>
						<%=LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_EMPLOYEE_CERTIFICATION_CANCEL_BUTTON, loginDTO)%>
						<%
							}

						%>
					</a>
					<button class="btn btn-success" type="submit">
						<%
							if(actionName.equals("edit"))
							{
						%>
						<%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEE_CERTIFICATION_SUBMIT_BUTTON, loginDTO)%>
						<%
						}
						else
						{
						%>
						<%=LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_EMPLOYEE_CERTIFICATION_SUBMIT_BUTTON, loginDTO)%>
						<%
							}
						%>
					</button>
				</div>

			</div>

		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


	$(document).ready( function(){

		basicInit();
		$(".hasExpiry").change(function(){
			$(this).find("option:selected").each(function(){
				var optionValue = $(this).text();
				if(optionValue == 'No'){
					$(".validUpto").hide();
				} else {
					$(".validUpto").show();
				}
			});
		}).change();
		$("#employee-certification").validate({
			errorClass: 'error is-invalid',
			validClass: 'is-valid',
			rules: {
				certificationName: "required",
				issuingOrganization: "required",
				validFrom: "required",
				validUpto: {
					required: function(element) {
						return $(".hasExpiry").find("option:selected").text() == 'Yes'
					}
				}
			},
			messages: {
				certificationName: "Please enter certification name",
				issuingOrganization: "Please enter issuing organization",
				validFrom: "Please enter valid from",
				validUpto: "Please enter valid up to"
			}
		});
	});

	function PreprocessBeforeSubmiting(row, validate)
	{
		$('#employee-certification').validate();
		if(validate == "report")
		{
		}
		else
		{
			var empty_fields = "";
			var i = 0;


			if(empty_fields != "")
			{
				if(validate == "inplaceedit")
				{
					$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
					return false;
				}
			}

		}


		return true;
	}


	function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
	{
		addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Job_applicant_certificationServlet");
	}

	function init(row)
	{



	}

	var row = 0;

	window.onload =function ()
	{
		init(row);
		CKEDITOR.replaceAll();
	}

	var child_table_extra_id = <%=childTableStartingID%>;

	$(function() {
		$('.date-picker').datepicker( {
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'MM yy',
			onClose: function(dateText, inst) {
				$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));

				var dt2 = $('.validUpto');

				dt2.datepicker('option', 'minDate', new Date(inst.selectedYear, inst.selectedMonth, 1));

			}
		});
	});

	$(function() {
		$('.date-picker-to').datepicker( {
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			dateFormat: 'MM yy',
			onClose: function(dateText, inst) {
				$(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
			}
		});
	});

</script>






