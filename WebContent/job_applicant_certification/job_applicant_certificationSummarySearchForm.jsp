
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_certification.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>
<%@ page import="com.google.gson.Gson" %>


<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty())
	{
		failureMessage = "";
	}

	String value = "";
	String Language = LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_LANGUAGE, loginDTO);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


	Job_applicant_certificationDAO job_applicant_certificationDAO = new Job_applicant_certificationDAO();


	String successMessageForwarded = "Forwarded to your Senior Office";
	String successMessageApproved = "Approval Done";

	String ajax = request.getParameter("ajax");
	boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

	if(hasAjax == false)
	{
		Enumeration<String> parameterNames = request.getParameterNames();

		while (parameterNames.hasMoreElements())
		{

			String paramName = parameterNames.nextElement();

			if(!paramName.equalsIgnoreCase("actionType"))
			{
				String[] paramValues = request.getParameterValues(paramName);
				for (int i = 0; i < paramValues.length; i++)
				{
					String paramValue = paramValues[i];

%>

<%

				}
			}


		}
	}

	String formTitle = LM.getText(LC.JOB_APPLICANT_CERTIFICATION_ADD_JOB_APPLICANT_CERTIFICATION_ADD_FORMNAME, loginDTO);

%>

<%--<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>--%>

<div class="col-lg-12">
	<div class="kt-portlet shadow-none" style="margin-top: -20px">

		<div class="kt-portlet__body form-body">


<div class="table-responsive">
	<table id="tableData" class="table table-bordered table-striped">
		<thead class="thead-light">
		<tr>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEERECORDSID, loginDTO)%></th>--%>
			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CERTIFICATIONNAME, loginDTO)%></th>
			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGORGANIZATION, loginDTO)%></th>
			<th style="  width: 25%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGINSTITUTEADDRESS, loginDTO)%></th>
			<th style="  width: 10%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDFROM, loginDTO)%></th>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_HASEXPIRYCAT, loginDTO)%></th>--%>
			<th style="  width: 10%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDUPTO, loginDTO)%></th>
			<th style="  width: 15%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CREDENTIALID, loginDTO)%></th>
<%--			<th class="filesColumn" style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_FILESDROPZONE, loginDTO)%></th>--%>

<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_FILESDROPZONE, loginDTO)%></th>--%>


		</tr>
		</thead>
		<tbody>
		<%
			List<Job_applicant_certificationDTO> data = new ArrayList<>();

			if (request.getParameter("summary") != null && Boolean.parseBoolean(request.getParameter("summary"))) {

				List<Job_applicant_certificationSummaryDTO> summaryData = new ArrayList<>();

				Gson gson = new Gson();
				summaryData = (List<Job_applicant_certificationSummaryDTO>)request.getSession(true).getAttribute("job_applicant_certificationSummaryDTOList");

				if (summaryData != null) {
					for (Job_applicant_certificationSummaryDTO summary: summaryData) {

						String json = gson.toJson(summary);
						Job_applicant_certificationDTO dto = gson.fromJson(json, Job_applicant_certificationDTO.class);
						data.add(dto);
					}
				}

			}

			else {
				data = job_applicant_certificationDAO.getDTOByJobApplicantID(Long.parseLong(request.getParameter("ID")));
			}
			try
			{

				if (data != null)
				{
					int size = data.size();
					System.out.println("data not null and size = " + size + " data = " + data);
					for (int i = 0; i < size; i++)
					{
						Job_applicant_certificationDTO job_applicant_certificationDTO = data.get(i);


		%>
		<tr id = 'tr_<%=i%>'>
			<%

			%>


			<%
				request.setAttribute("job_applicant_certificationDTO",job_applicant_certificationDTO);
			%>

			<jsp:include page="./job_applicant_certificationSummarySearchRow.jsp">
				<jsp:param name="pageName" value="searchrow" />
				<jsp:param name="rownum" value="<%=i%>" />
			</jsp:include>


			<%

			%>
		</tr>
		<%
					}

					System.out.println("printing done");
				}
				else
				{
					System.out.println("data  null");
				}
			}
			catch(Exception e)
			{
				System.out.println("JSP exception " + e);
			}
		%>



		</tbody>

	</table>
</div>

		</div>
	</div>
</div>



			