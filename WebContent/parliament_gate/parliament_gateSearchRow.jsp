<%@page pageEncoding="UTF-8" %>

<%@page import="parliament_gate.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PARLIAMENT_GATE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PARLIAMENT_GATE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Parliament_gateDTO parliament_gateDTO = (Parliament_gateDTO) request.getAttribute("parliament_gateDTO");
    CommonDTO commonDTO = parliament_gateDTO;
    String servletName = "Parliament_gateServlet";


    System.out.println("parliament_gateDTO = " + parliament_gateDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Parliament_gateDAO parliament_gateDAO = (Parliament_gateDAO) request.getAttribute("parliament_gateDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_gateNumber'>
    <%
        value = parliament_gateDTO.gateNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_gatePabxExtNumber'>
    <%
        value = parliament_gateDTO.gatePabxExtNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameEng'>
    <%
        value = parliament_gateDTO.nameEng + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBng'>
    <%
        value = parliament_gateDTO.nameBng + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_locationEng'>
    <%
        value = parliament_gateDTO.locationEng + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_locationBng'>
    <%
        value = parliament_gateDTO.locationBng + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Parliament_gateServlet?actionType=view&ID=<%=parliament_gateDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Parliament_gateServlet?actionType=getEditPage&ID=<%=parliament_gateDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox'>
    <div class='checker text-right'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=parliament_gateDTO.iD%>'/>
        </span>
    </div>
</td>
											

