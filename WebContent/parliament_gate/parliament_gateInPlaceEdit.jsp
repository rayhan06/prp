<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="parliament_gate.Parliament_gateDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>

<%
Parliament_gateDTO parliament_gateDTO = (Parliament_gateDTO)request.getAttribute("parliament_gateDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(parliament_gateDTO == null)
{
	parliament_gateDTO = new Parliament_gateDTO();
	
}
System.out.println("parliament_gateDTO = " + parliament_gateDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
String servletName = "Parliament_gateServlet";
String fileColumnName = "";
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.PARLIAMENT_GATE_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=parliament_gateDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gateNumber'>")%>
			
	
	<div class="form-inline" id = 'gateNumber_div_<%=i%>'>
		<input type='text' class='form-control'  name='gateNumber' id = 'gateNumber_text_<%=i%>' value='<%=parliament_gateDTO.gateNumber%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gatePabxExtNumber'>")%>
			
	
	<div class="form-inline" id = 'gatePabxExtNumber_div_<%=i%>'>
		<input type='text' class='form-control'  name='gatePabxExtNumber' id = 'gatePabxExtNumber_text_<%=i%>' value='<%=parliament_gateDTO.gatePabxExtNumber%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_nameEng'>")%>
			
	
	<div class="form-inline" id = 'nameEng_div_<%=i%>'>
		<input type='text' class='form-control'  name='nameEng' id = 'nameEng_text_<%=i%>' value='<%=parliament_gateDTO.nameEng%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_nameBng'>")%>
			
	
	<div class="form-inline" id = 'nameBng_div_<%=i%>'>
		<input type='text' class='form-control'  name='nameBng' id = 'nameBng_text_<%=i%>' value='<%=parliament_gateDTO.nameBng%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_locationEng'>")%>
			
	
	<div class="form-inline" id = 'locationEng_div_<%=i%>'>
		<input type='text' class='form-control'  name='locationEng' id = 'locationEng_text_<%=i%>' value='<%=parliament_gateDTO.locationEng%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_locationBng'>")%>
			
	
	<div class="form-inline" id = 'locationBng_div_<%=i%>'>
		<input type='text' class='form-control'  name='locationBng' id = 'locationBng_text_<%=i%>' value='<%=parliament_gateDTO.locationBng%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_description'>")%>
			
	
	<div class="form-inline" id = 'description_div_<%=i%>'>
		<input type='text' class='form-control'  name='description' id = 'description_text_<%=i%>' value='<%=parliament_gateDTO.description%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_imageDropzone" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='imageDropzone' id = 'imageDropzone_dropzone_<%=i%>' value='<%=parliament_gateDTO.imageDropzone%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=parliament_gateDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=parliament_gateDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=parliament_gateDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=parliament_gateDTO.isDeleted%>' tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=parliament_gateDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Parliament_gateServlet?actionType=view&ID=<%=parliament_gateDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Parliament_gateServlet?actionType=view&modal=1&ID=<%=parliament_gateDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	