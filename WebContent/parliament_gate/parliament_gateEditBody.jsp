<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="parliament_gate.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>

<%
    Parliament_gateDTO parliament_gateDTO;
    parliament_gateDTO = (Parliament_gateDTO) request.getAttribute("parliament_gateDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (parliament_gateDTO == null) {
        parliament_gateDTO = new Parliament_gateDTO();

    }
    System.out.println("parliament_gateDTO = " + parliament_gateDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PARLIAMENT_GATE_ADD_PARLIAMENT_GATE_ADD_FORMNAME, loginDTO);
    String servletName = "Parliament_gateServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Parliament_gateServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <%@ page import="java.text.SimpleDateFormat" %>
                                    <%@ page import="java.util.Date" %>
                                    <%@ page import="pb.*" %>
                                    <%
                                        String Language = LM.getText(LC.PARLIAMENT_GATE_EDIT_LANGUAGE, loginDTO);
                                        String Options;
                                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                        Date date = new Date();
                                        String datestr = dateFormat.format(date);
                                        CommonDAO.language = Language;
                                        CatDAO.language = Language;
                                    %>
                                    <input type='hidden' class='form-control' name='ID'
                                           id='id_hidden_<%=i%>'
                                           value='<%=parliament_gateDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_GATE_ADD_GATENUMBER, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <div class="" id='gateNumber_div_<%=i%>'>
                                                <input type='text' class='form-control'
                                                       name='gateNumber' id='gateNumber'
                                                       value='<%=parliament_gateDTO.gateNumber%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_GATE_ADD_GATEPABXEXTNUMBER, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <div class="" id='gateNumber_div_<%=i%>'>
                                                <input type='text' class='form-control'
                                                       name='gatePabxExtNumber' id='gatePabxExtNumber'
                                                       value='<%=parliament_gateDTO.gatePabxExtNumber%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_GATE_ADD_NAMEENG, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='nameEng_div_<%=i%>'>
                                                <input type='text' class='form-control' name='nameEng'
                                                       id='nameEng_text_<%=i%>'
                                                       value='<%=parliament_gateDTO.nameEng%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_GATE_ADD_NAMEBNG, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='nameBng_div_<%=i%>'>
                                                <input type='text' class='form-control' name='nameBng'
                                                       id='nameBng_text_<%=i%>'
                                                       value='<%=parliament_gateDTO.nameBng%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_GATE_ADD_LOCATIONENG, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='locationEng_div_<%=i%>'>
                                                <input type='text' class='form-control'
                                                       name='locationEng' id='locationEng_text_<%=i%>'
                                                       value='<%=parliament_gateDTO.locationEng%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_GATE_ADD_LOCATIONBNG, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div class="" id='locationBng_div_<%=i%>'>
                                                <input type='text' class='form-control'
                                                       name='locationBng' id='locationBng_text_<%=i%>'
                                                       value='<%=parliament_gateDTO.locationBng%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"></label>
                                        <div class="col-md-8 d-flex align-items-center">
                                            <input type='checkbox' class='form-control-sm form-check-input ml-1'
                                                   name='car_entry' id='car_entry'
                                                   value='true' <%=(actionName.equals("edit") && parliament_gateDTO.carEntry == 1) ? ("checked") : ""%>
                                                   tag='pb_html'/>
                                            <label class="form-check-label ml-4 mt-1"
                                                   for="car_entry"
                                                   style="font-size:small;font-weight: bolder; padding-left: 5px">
                                                <%=Language.equalsIgnoreCase("English") ? "Car entry permission" : "গাড়ি প্রবেশের অনুমতি"%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"></label>
                                        <div class="col-md-8 d-flex align-items-center">
                                            <input type='checkbox' class='form-control-sm form-check-input ml-1'
                                                   name='public_entry' id='public_entry'
                                                   value='true' <%=(actionName.equals("edit") && parliament_gateDTO.publicEntry == 1) ? ("checked") : ""%>
                                                   tag='pb_html'/>
                                            <label class="form-check-label ml-4 mt-1"
                                                   for="public_entry"
                                                   style="font-size:small;font-weight: bolder; padding-left: 5px">
                                                <%=Language.equalsIgnoreCase("English") ? "General public entry permission" : "সাধারন জনগণের প্রবেশের অনুমতি"%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"></label>
                                        <div class="col-md-8 d-flex align-items-center">
                                            <input type='checkbox' class='form-control-sm form-check-input ml-1'
                                                   name='only_vip_entry' id='only_vip_entry'
                                                   value='true' <%=(actionName.equals("edit") && parliament_gateDTO.onlyVipEntry == 1) ? ("checked") : ""%>
                                                   tag='pb_html'/>
                                            <label class="form-check-label ml-4 mt-1"
                                                   for="only_vip_entry"
                                                   style="font-size:small;font-weight: bolder; padding-left: 5px">
                                                <%=Language.equalsIgnoreCase("English") ? "Only MP and VIP entry permission" : "শুধুমাত্র এমপি এবং ভিআইপি প্রবেশের অনুমতি"%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_GATE_ADD_ENTRY_PERMISSION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8" id='entry_permission_div'>
                                            <select class='form-control' name='entry_permission'
                                                    id='entry_permission' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("entry_permission", Language, parliament_gateDTO.entryPermission)%>
                                            </select>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=parliament_gateDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>'
                                           value='<%=parliament_gateDTO.insertedBy%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>'
                                           value='<%=parliament_gateDTO.modifiedBy%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=parliament_gateDTO.isDeleted%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=parliament_gateDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-actions text-right mt-3">
                            <button class="btn-sm shadow text-white border-0 cancel-btn btn-border-radius">
                                <%=LM.getText(LC.PARLIAMENT_GATE_ADD_PARLIAMENT_GATE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn btn-border-radius ml-2"
                                    type="submit">
                                <%=LM.getText(LC.PARLIAMENT_GATE_ADD_PARLIAMENT_GATE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(() => {
        $(".cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    $(document).ready(function () {
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                gateNumber: "required",
                post: {
                    required: true,
                    postSelection: true
                }
            },

            messages: {
                gateNumber: "Please enter gate number"
            }
        });

        dateTimeInit("<%=Language%>");
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Parliament_gateServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






