<%@page pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.*" %>

<%
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<td>
    <%=Utils.getDigits(parliament_sessionDTO.sessionNumber + "", Language)%>
</td>

<td>
    <%=Utils.getDigits(simpleDateFormat.format(new Date(Long.parseLong(parliament_sessionDTO.startDate + ""))), Language)%>
</td>

<td>
    <%=Utils.getDigits(simpleDateFormat.format(new Date(Long.parseLong(parliament_sessionDTO.endDate + ""))), Language)%>
</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Parliament_sessionServlet?actionType=view&ID=<%=parliament_sessionDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Parliament_sessionServlet?actionType=getEditPage&ID=<%=parliament_sessionDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>

</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=parliament_sessionDTO.iD%>'/></span>
    </div>
</td>
