<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="parliament_session.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="election_details.Election_detailsRepository" %>

<%
    Parliament_sessionDTO parliament_sessionDTO;
    parliament_sessionDTO = (Parliament_sessionDTO) request.getAttribute("parliament_sessionDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (parliament_sessionDTO == null) {
        parliament_sessionDTO = new Parliament_sessionDTO();

    }
    System.out.println("parliament_sessionDTO = " + parliament_sessionDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_ADD_FORMNAME, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    int childTableStartingID = 1;


    String Language = LM.getText(LC.PARLIAMENT_SESSION_EDIT_LANGUAGE, loginDTO);

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String data = request.getParameter("data");
    String URL = "Parliament_sessionServlet?actionType=ajax_" + actionName + "&isPermanentTable=true"
                 + (data == null ? "" : "&data=" + data);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="parliament_session_form" name="bigform" action="<%=URL%>"
              enctype="multipart/form-data">
            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.ELECTION_WISE_MP_ELECTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='electionDetailsId<%=i%>'>
                                            <select onchange="fetchSelectors()" class='form-control'
                                                    name='electionDetailsId'
                                                    id='electionDetailsId' tag='pb_html'>
                                                <%= Election_detailsRepository.getInstance().buildOptions(Language, parliament_sessionDTO.electionDetailsId)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_SESSIONNUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='sessionNumber_div_<%=i%>'>
                                            <input type='number' class='form-control' name='sessionNumber'
                                                   id='sessionNumber_number_<%=i%>'
                                                   value='<%=parliament_sessionDTO.sessionNumber%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_STARTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='startDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="start_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="START_YEAR" value="1971"></jsp:param>
                                            </jsp:include>

                                            <input type='hidden' class='form-control' name='startDate'
                                                   id='start-date' value=""
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_ENDDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9" id='endDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="end_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="START_YEAR" value="1971"></jsp:param>
                                            </jsp:include>

                                            <input type='hidden' class='form-control' name='endDate'
                                                   id='end-date' value="" tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                        </label>
                                        <div class="col-md-9">
                                        </div>
                                    </div>
                                    <!-- Hidden Fields -->
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=parliament_sessionDTO.iD%>' tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_DATE, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_DATE_SESSIONDATE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_DATE_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-ParliamentSessionDate">
                            <%
                                if (actionName.equals("edit")) {
                                    int index = -1;


                                    for (ParliamentSessionDateDTO parliamentSessionDateDTO : parliament_sessionDTO.parliamentSessionDateDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>
                            <tr id="ParliamentSessionDate_<%=index + 1%>">
                                <td style="display: none;">
                                    <input type='hidden' class='form-control'
                                           name='parliamentSessionDate.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=parliamentSessionDateDTO.iD%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">
                                    <input type='hidden' class='form-control'
                                           name='parliamentSessionDate.parliamentSessionId'
                                           id='parliamentSessionId_hidden_<%=childTableStartingID%>'
                                           value='<%=parliamentSessionDateDTO.parliamentSessionId%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <input type='text'
                                           class='form-control formRequired datepicker'
                                           readonly="readonly"
                                           data-label="Document Date"
                                           id='sessionDate_date_<%=childTableStartingID%>'
                                           name='parliamentSessionDate.sessionDate'
                                           value=<%String formatted_sessionDate = dateFormat.format(new Date(parliamentSessionDateDTO.sessionDate));%>
                                                   '<%=formatted_sessionDate%>'
                                           tag='pb_html'>
                                </td>

                                <td>
                                    <div class='checker'><span id='chkEdit'><input
                                            type='checkbox'
                                            id='parliamentSessionDate_cb_<%=index%>'
                                            name='checkbox' value=''/></span>
                                    </div>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right mt-3">
                            <button
                                    id="add-more-ParliamentSessionDate"
                                    name="add-moreParliamentSessionDate"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                            </button>
                            <button
                                    id="remove-ParliamentSessionDate"
                                    name="removeParliamentSessionDate"
                                    type="button"
                                    class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    <%ParliamentSessionDateDTO parliamentSessionDateDTO = new ParliamentSessionDateDTO();%>
                    <template id="template-ParliamentSessionDate">
                        <tr>
                            <td style="display: none;">
                                <input type='hidden' class='form-control'
                                       name='parliamentSessionDate.iD'
                                       id='iD_hidden_'
                                       value='<%=parliamentSessionDateDTO.iD%>'
                                       tag='pb_html'/>
                            </td>
                            <td style="display: none;">
                                <input type='hidden' class='form-control'
                                       name='parliamentSessionDate.parliamentSessionId'
                                       id='parliamentSessionId_hidden_'
                                       value='<%=parliamentSessionDateDTO.parliamentSessionId%>'
                                       tag='pb_html'/>
                            </td>
                            <td>
                                <input type='text'
                                       class='form-control formRequired datepicker'
                                       readonly="readonly"
                                       data-label="Document Date"
                                       id='sessionDate_date_'
                                       name='parliamentSessionDate.sessionDate'
                                       value='<%=dateFormat.format(new Date(parliamentSessionDateDTO.sessionDate))%>'
                                       tag='pb_html'>

                            </td>

                            <td>
                                <div><span><input type='checkbox'
                                                  name='checkbox'
                                                  value=''/></span></div>
                            </td>
                        </tr>
                    </template>
                </div>
                <div class="mt-4 row">
                    <div class="col-12 text-center">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                type="button" onclick="submitItem('parliament_session_form')">
                            <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    let isEnglish = <%=isLanguageEnglish%>;

    function submitItem(id) {
        let submitFunction = () => {
            let formName = '#' + id;
            const form = $(formName);

            buttonStateChangeFunctionFunctionFunctionFunction(true);
            if (isFormValid(form)) {
                submitAjaxForm(id);

            } else {
                buttonStateChangeFunctionFunctionFunctionFunction(false);
            }
        }
        if (isEnglish) {
            messageDialog('Do you want to submit?', "You won't be able to revert this!", 'success', true, 'Submit', 'Cancel', submitFunction);
        } else {
            messageDialog('সাবমিট করতে চান?', "সাবমিটের পর পরিবর্তনযোগ্য না!", 'success', true, 'সাবমিট', 'বাতিল', submitFunction);
        }
    }

    function buttonStateChangeFunctionFunctionFunctionFunction(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    $(document).ready(function () {

        dateTimeInit("<%=Language%>");
        $('#start_date_js').on('datepicker.change', () => {
            setMinDateById('end_date_js', getDateStringById('start_date_js'));
        });
        $.validator.addMethod('electionDetailsValidator', function (value, element) {
            return value != 0;
        });

        $("#parliament_session_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                electionDetailsId: {
                    required: true,
                    electionDetailsValidator: true
                },
                sessionNumber: "required"

            },
            messages: {
                electionDetailsId: '<%=Language.equalsIgnoreCase("English") ? "Kindly select election!" : "অনুগ্রহ করে ইলেকশন নির্বাচন করুন"%>',
                sessionNumber: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter session number!" : "অনুগ্রহ করে সেশন নাম্বার দিন"%>'
            }
        });
    });


    function isFormValid($form) {
        $('#start-date').val(getDateStringById('start_date_js'));
        $('#end-date').val(getDateStringById('end_date_js'));

        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("sessionDate_date_" + i)) {
                if (document.getElementById("sessionDate_date_" + i).getAttribute("processed") == null) {
                    //preprocessDateBeforeSubmitting('sessionDate', i);
                    document.getElementById("sessionDate_date_" + i).setAttribute("processed", "1");
                }
            }
        }
        const jQueryValid = $form.valid();
        return jQueryValid && dateValidator('start_date_js', true) && dateValidator('end_date_js', true);
    }

    $(function () {
        $("#start-date").val('');
        $("#end-date").val('');

        <%
     if(actionName.equals("edit")) {
        String startDate= parliament_sessionDTO.startDate > SessionConstants.MIN_DATE ? dateFormat.format(new Date(parliament_sessionDTO.startDate)) : "";
        String endDate = parliament_sessionDTO.endDate > SessionConstants.MIN_DATE ? dateFormat.format(new Date(parliament_sessionDTO.endDate)) : "";
        %>
        setDateByStringAndId('start_date_js', '<%=startDate%>');
        setDateByStringAndId('end_date_js', '<%=endDate%>');
        <%}
        %>
    });

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Parliament_sessionServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-ParliamentSessionDate").click(
        function (e) {
            e.preventDefault();
            let sessionStartDate = getDateStringById('start_date_js');
            let sessionEndDate = getDateStringById('end_date_js');
            if (!sessionStartDate || !sessionEndDate) {
                alert("<%=LM.getText(LC.SESSION_PLEASE_SELECT_SESSION_START_AND_END_DATE, loginDTO)%>");
                return;
            }

            var t = $("#template-ParliamentSessionDate");

            $("#field-ParliamentSessionDate").append(t.html());
            SetCheckBoxValues("field-ParliamentSessionDate");

            var tr = $("#field-ParliamentSessionDate").find("tr:last-child");

            tr.attr("id", "ParliamentSessionDate_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });

            dateTimeInit();
            child_table_extra_id++;

        });


    $("#remove-ParliamentSessionDate").click(function (e) {
        var tablename = 'field-ParliamentSessionDate';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });

    function dateTimeInit() {

        $('.edms-datetimepicker').datetimepicker({
            format: 'LT'
        });


        $('.datepicker').datepicker({

            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '1970:+100',
            todayHighlight: true,
            showButtonPanel: true
        });
        $(".datepicker").datepicker('option', 'minDate', getDateStringById('start_date_js'));
        $(".datepicker").datepicker('option', 'maxDate', getDateStringById('end_date_js'));


    }
</script>






