<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="parliament_session.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.PARLIAMENT_SESSION_EDIT_LANGUAGE, loginDTO);
    String navigator2 = SessionConstants.NAV_PARLIAMENT_SESSION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();

%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_SESSIONNUMBER, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_STARTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_ENDDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PARLIAMENT_SESSION_SEARCH_PARLIAMENT_SESSION_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<Parliament_sessionDTO> data = (List<Parliament_sessionDTO>) session.getAttribute(SessionConstants.VIEW_PARLIAMENT_SESSION);

            if (data != null && data.size() > 0) {
                for (Parliament_sessionDTO parliament_sessionDTO : data) {
        %>
        <tr>
            <%@include file="parliament_sessionSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>
        <%--        <tbody>--%>
        <%--        <%--%>
        <%--            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_PARLIAMENT_SESSION);--%>

        <%--            try {--%>

        <%--                if (data != null) {--%>
        <%--                    int size = data.size();--%>
        <%--                    System.out.println("data not null and size = " + size + " data = " + data);--%>
        <%--                    for (int i = 0; i < size; i++) {--%>
        <%--                        Parliament_sessionDTO parliament_sessionDTO = (Parliament_sessionDTO) data.get(i);--%>


        <%--        %>--%>
        <%--        <tr id='tr_<%=i%>'>--%>
        <%--            <%--%>

        <%--            %>--%>


        <%--            <%--%>
        <%--                request.setAttribute("parliament_sessionDTO", parliament_sessionDTO);--%>
        <%--            %>--%>

        <%--            <jsp:include page="./parliament_sessionSearchRow.jsp">--%>
        <%--                <jsp:param name="pageName" value="searchrow"/>--%>
        <%--                <jsp:param name="rownum" value="<%=i%>"/>--%>
        <%--            </jsp:include>--%>


        <%--            <%--%>

        <%--            %>--%>
        <%--        </tr>--%>
        <%--        <%--%>
        <%--                    }--%>

        <%--                    System.out.println("printing done");--%>
        <%--                } else {--%>
        <%--                    System.out.println("data  null");--%>
        <%--                }--%>
        <%--            } catch (Exception e) {--%>
        <%--                System.out.println("JSP exception " + e);--%>
        <%--            }--%>
        <%--        %>--%>


        <%--        </tbody>--%>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			