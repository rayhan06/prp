<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="parliament_session.Parliament_sessionDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Parliament_sessionDTO parliament_sessionDTO = (Parliament_sessionDTO)request.getAttribute("parliament_sessionDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(parliament_sessionDTO == null)
{
	parliament_sessionDTO = new Parliament_sessionDTO();
	
}
System.out.println("parliament_sessionDTO = " + parliament_sessionDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

String servletName = "Parliament_sessionServlet";
String fileColumnName = "";
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.PARLIAMENT_SESSION_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=parliament_sessionDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_electionDetailsId'>")%>
			

		<input type='hidden' class='form-control'  name='electionDetailsId' id = 'electionDetailsId_hidden_<%=i%>' value='<%=parliament_sessionDTO.electionDetailsId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_sessionNumber'>")%>
			
	
	<div class="form-inline" id = 'sessionNumber_div_<%=i%>'>
		<input type='number' class='form-control'  name='sessionNumber' id = 'sessionNumber_number_<%=i%>' value='<%=parliament_sessionDTO.sessionNumber%>'  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_startDate'>")%>
			
	
	<div class="form-inline" id = 'startDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'startDate_date_<%=i%>' name='startDate' value=	<%
	String formatted_startDate = dateFormat.format(new Date(parliament_sessionDTO.startDate));
	%>
	'<%=formatted_startDate%>'
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_endDate'>")%>
			
	
	<div class="form-inline" id = 'endDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'endDate_date_<%=i%>' name='endDate' value=	<%
	String formatted_endDate = dateFormat.format(new Date(parliament_sessionDTO.endDate));
	%>
	'<%=formatted_endDate%>'
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=parliament_sessionDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=parliament_sessionDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=parliament_sessionDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=parliament_sessionDTO.isDeleted%>' tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=parliament_sessionDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Parliament_sessionServlet?actionType=view&ID=<%=parliament_sessionDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Parliament_sessionServlet?actionType=view&modal=1&ID=<%=parliament_sessionDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	