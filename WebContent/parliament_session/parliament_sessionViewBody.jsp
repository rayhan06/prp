<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="parliament_session.*" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String value;
    String Language = LM.getText(LC.PARLIAMENT_SESSION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Parliament_sessionDAO parliament_sessionDAO = new Parliament_sessionDAO("parliament_session");
    Parliament_sessionDTO parliament_sessionDTO = parliament_sessionDAO.getDTOByID(id);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div>
                <h5 class="table-title">
                    <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_ELECTIONDETAILSID, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = parliament_sessionDTO.electionDetailsId + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_SESSIONNUMBER, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = parliament_sessionDTO.sessionNumber + "";
                                %>

                                <%=Utils.getDigits(value, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_STARTDATE, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = parliament_sessionDTO.startDate + "";
                                %>
                                <%
                                    String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_startDate, Language)%>


                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_ENDDATE, loginDTO)%>
                            </b></td>
                            <td>

                                <%
                                    value = parliament_sessionDTO.endDate + "";
                                %>
                                <%
                                    String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_endDate, Language)%>


                            </td>

                        </tr>


                    </table>
                </div>
            </div>
            <div class="mt-5">
                <h5 class="table-title">
                    <%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_DATE, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th><%=LM.getText(LC.PARLIAMENT_SESSION_ADD_PARLIAMENT_SESSION_DATE_SESSIONDATE, loginDTO)%>
                            </th>
                        </tr>
                        <%
                            ParliamentSessionDateDAO parliamentSessionDateDAO = new ParliamentSessionDateDAO();
                            List<ParliamentSessionDateDTO> parliamentSessionDateDTOs = parliamentSessionDateDAO.getParliamentSessionDateDTOListByParliamentSessionID(parliament_sessionDTO.iD);

                            for (ParliamentSessionDateDTO parliamentSessionDateDTO : parliamentSessionDateDTOs) {
                        %>
                        <tr>
                            <td>
                                <%
                                    value = parliamentSessionDateDTO.sessionDate + "";
                                %>
                                <%
                                    String formatted_sessionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_sessionDate, Language)%>


                            </td>
                        </tr>
                        <%

                            }

                        %>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>