<%@ page import="util.UtilCharacter" %>
<%@ page import="dashboard.DashboardDTO" %>
<%@ page import="dashboard.DashboardService" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="pb.Utils" %>

<div class="kt-portlet__body" style="background-color: white">
    <div id="weekly_req_count" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{

        getDataOfLastSevenDaysRequistion();
    });

    let lastSevenDaysRequisitionData = [] ;

    function drawChartDays() {
        let dataArray = [];

        for(let i = 0; i < lastSevenDaysRequisitionData.length; i++){
            if(lastSevenDaysRequisitionData[i] && lastSevenDaysRequisitionData[i].typeName) {
                dataArray.push([lastSevenDaysRequisitionData[i].typeName,
                    (i == 0 ? lastSevenDaysRequisitionData[i].typeValue :
                        lastSevenDaysRequisitionData[i].typeValue / 1)]);
            }
        }

        var data = google.visualization.arrayToDataTable(dataArray);

        var options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "Requisition Count of Last 7 Days",
             "Requisition Count of Last 7 Days")%>',
            legend: {position: 'left'}
        };

        var chart = new google.visualization.LineChart(document.getElementById('weekly_req_count'));
        chart.draw(data, options);
    }

    function getDataOfLastSevenDaysRequistion(){
        let url = "VehicleDashboardServlet?actionType=last_seven_days_req_count";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if(fetchedData){
                    lastSevenDaysRequisitionData = fetchedData;
                    google.charts.setOnLoadCallback(drawChartDays);
                }
                // console.log(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>