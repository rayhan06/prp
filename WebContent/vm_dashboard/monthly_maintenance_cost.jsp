<%@ page import="util.UtilCharacter" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="pb.Utils" %>
<%
//    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
%>
<div class="kt-portlet__body" style="background-color: white">
    <div id="monthly_maintenance_cost" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        // google.charts.setOnLoadCallback(drawChartMonth);
        getDataOfLastSixMonthMaintenance();
    });

    let lastSixMonthMaintenanceData = [] ;



    function drawChartMonth() {

        let dataArray = [];

        for(let i = 0; i < lastSixMonthMaintenanceData.length; i++){
            // console.log(lastSixMonthMaintenanceData[i])
            if(lastSixMonthMaintenanceData[i] && lastSixMonthMaintenanceData[i].typeName) {
                dataArray.push([lastSixMonthMaintenanceData[i].typeName,
                    (i == 0 ? lastSixMonthMaintenanceData[i].driverValue :
                        lastSixMonthMaintenanceData[i].driverValue / 1),
                    (i == 0 ? lastSixMonthMaintenanceData[i].typeValue :
                        lastSixMonthMaintenanceData[i].typeValue / 1)
                ]);
            }
        }

        // console.log(dataArray)

        var data = google.visualization.arrayToDataTable(dataArray);

        var options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "Last 6 Months Maintenance Cost",
             "Last 6 Months Maintenance Cost")%>',
            legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('monthly_maintenance_cost'));

        chart.draw(data, options);
    }

    function getDataOfLastSixMonthMaintenance(){
        let url = "VehicleDashboardServlet?actionType=last_six_month_main_cost";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if(fetchedData){
                    console.log(fetchedData)
                    lastSixMonthMaintenanceData = fetchedData;
                    google.charts.setOnLoadCallback(drawChartMonth);
                }
                // console.log(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

</script>