<%@ page import="util.UtilCharacter" %>
<div class="kt-portlet__body" style="background-color: white">
    <div id="pie_chart_vehicle_count_by_office" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        vehicleCountByOffice();
    })
    let vehicleCountByOfficeData;



    function vehicleCountByOffice(){
            let url = "VehicleDashboardServlet?actionType=vehicle_count_by_office&language=<%=language%>";
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    vehicleCountByOfficeData = fetchedData;
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawVehicleCountByOfficeChart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    }

    function drawVehicleCountByOfficeChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Office');
        data.addColumn('number', 'Count');
        if(vehicleCountByOfficeData){
            for(let i in vehicleCountByOfficeData){
                data.addRows([[String(i),vehicleCountByOfficeData[i]]]);
            }
        }

        const view = new google.visualization.DataView(data);

        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "Vehicle Count By Office", "Vehicle Count By Office")%>',
            // is3D: true,
            // legend: { position: "none" }
        };
        const chart = new google.visualization.PieChart(document.getElementById('pie_chart_vehicle_count_by_office'));
        chart.draw(view, options);
    }
</script>