<%@ page import="util.UtilCharacter" %>
<div class="kt-portlet__body" style="background-color: white">
    <div id="pie_chart_requisition_count_by_vehicle_type" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        requisitionCountByVehicleType();
    })
    let requisitionCountByVehicleTypeData;



    function requisitionCountByVehicleType(){
            let url = "VehicleDashboardServlet?actionType=requisition_count_by_vehicle_type&language=<%=language%>";
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    requisitionCountByVehicleTypeData = fetchedData;
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawRequisitionCountByVehicleTypeChart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    }

    function drawRequisitionCountByVehicleTypeChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Vehicle Type');
        data.addColumn('number', 'Count');
        if(requisitionCountByVehicleTypeData){
            for(let i in requisitionCountByVehicleTypeData){
                data.addRows([[String(i),requisitionCountByVehicleTypeData[i]]]);
            }
        }

        const view = new google.visualization.DataView(data);

        const options = {
            title: '<%=UtilCharacter.getDataByLanguage(language, "Requisition Count By Vehicle Type", "Requisition Count By Vehicle Type")%>',
            // is3D: true,
            // legend: { position: "none" }
        };
        const chart = new google.visualization.PieChart(document.getElementById('pie_chart_requisition_count_by_vehicle_type'));
        chart.draw(view, options);
    }
</script>