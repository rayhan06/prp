﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="util.*" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<%

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(language, "ড্যাশবোর্ড", "Dashboard")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body" style="background-color: #f2f2f2">
            <div class="row">
                <div class="col-12">
                    <div class="row" id = 'countDiv'>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-12 col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="requisition_count_by_vehicle_type.jsp" %>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="vehicle_count_by_office.jsp" %>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="weekly_requisition_count.jsp" %>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="monthly_maintenance_cost.jsp" %>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    $(document).ready(()=>{
        vehicleAndDriverCount();
    });

    function vehicleAndDriverCount(){

        let url = "VehicleDashboardServlet?actionType=vehicle_and_driver_count";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                let html = "";
                if(fetchedData){
                    for (let i = 0; i < fetchedData.length; ++i) {
                        if(fetchedData[i] && fetchedData[i].typeName) {
                            html += createTemplate(fetchedData[i].typeName, fetchedData[i].typeValue);
                        }
                    }
                    $('#countDiv').html(html);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });


    }


    function createTemplate(typeName, typeValue){
        let template = "<div class=\"col-md-6 col-lg-4 mt-2\">\n" +
            "                            <div class=\"d-flex justify-content-center align-items-center\">\n" +
            "                                <div class=\"shadow\"\n" +
            "                                     style=\"width: 100%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;\">\n" +
            "                                    <div class=\"d-flex justify-content-between align-items-center mx-2 my-4 px-5\">\n" +
            "                                        <div class=\"\">\n" +
            "                                            <span class=\"h2 text-white\">\n" +
                                                            typeName +
            "                                            </span>\n" +
            "                                        </div>\n" +
            "                                        <div class=\"h2 mb-0 ml-5 \">\n" +
            "                                            <span class=\" text-white\">\n" +
                                                            typeValue +
            "                                            </span>\n" +
            "                                        </div>\n" +
            "                                    </div>\n" +
            "                                </div>\n" +
            "                            </div>\n" +
            "                        </div>";

        return template;
    }

</script>




