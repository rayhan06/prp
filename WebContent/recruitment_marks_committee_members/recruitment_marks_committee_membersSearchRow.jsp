<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_marks_committee_members.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_RECRUITMENT_MARKS_COMMITTEE_MEMBERS;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO = (Recruitment_marks_committee_membersDTO)request.getAttribute("recruitment_marks_committee_membersDTO");
CommonDTO commonDTO = recruitment_marks_committee_membersDTO;
String servletName = "Recruitment_marks_committee_membersServlet";


System.out.println("recruitment_marks_committee_membersDTO = " + recruitment_marks_committee_membersDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Recruitment_marks_committee_membersDAO recruitment_marks_committee_membersDAO = (Recruitment_marks_committee_membersDAO)request.getAttribute("recruitment_marks_committee_membersDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_recruitmentMarksCommitteeId'>
											<%
											value = recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_mobile'>
											<%
											value = recruitment_marks_committee_membersDTO.mobile + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_otp'>
											<%
											value = recruitment_marks_committee_membersDTO.otp + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_employeeRecordId'>
											<%
											value = recruitment_marks_committee_membersDTO.employeeRecordId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_unitId'>
											<%
											value = recruitment_marks_committee_membersDTO.unitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_postId'>
											<%
											value = recruitment_marks_committee_membersDTO.postId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_employeeRecordName'>
											<%
											value = recruitment_marks_committee_membersDTO.employeeRecordName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_employeeRecordNameBn'>
											<%
											value = recruitment_marks_committee_membersDTO.employeeRecordNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_unitName'>
											<%
											value = recruitment_marks_committee_membersDTO.unitName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_unitNameBn'>
											<%
											value = recruitment_marks_committee_membersDTO.unitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_postName'>
											<%
											value = recruitment_marks_committee_membersDTO.postName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_postNameBn'>
											<%
											value = recruitment_marks_committee_membersDTO.postNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
											<td id = '<%=i%>_insertedBy'>
											<%
											value = recruitment_marks_committee_membersDTO.insertedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_modifiedBy'>
											<%
											value = recruitment_marks_committee_membersDTO.modifiedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
	

											<td>
												<button class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="location.href='Recruitment_marks_committee_membersServlet?actionType=view&ID=<%=recruitment_marks_committee_membersDTO.iD%>'">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<button class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
											            onclick="location.href='Recruitment_marks_committee_membersServlet?actionType=getEditPage&ID=<%=recruitment_marks_committee_membersDTO.iD%>'">
											        <%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_RECRUITMENT_MARKS_COMMITTEE_MEMBERS_EDIT_BUTTON, loginDTO)%>
											    </button>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=recruitment_marks_committee_membersDTO.iD%>'/></span>
												</div>
											</td>
																						
											

