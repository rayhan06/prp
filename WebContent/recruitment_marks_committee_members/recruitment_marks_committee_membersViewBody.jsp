

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_marks_committee_members.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
String servletName = "Recruitment_marks_committee_membersServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Recruitment_marks_committee_membersDAO recruitment_marks_committee_membersDAO = new Recruitment_marks_committee_membersDAO("recruitment_marks_committee_members");
Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO = recruitment_marks_committee_membersDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_FORMNAME, loginDTO)%></h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_FORMNAME, loginDTO)%></h3>
						<table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_RECRUITMENTMARKSCOMMITTEEID, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_MOBILE, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.mobile + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_OTP, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.otp + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_EMPLOYEERECORDID, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.employeeRecordId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_UNITID, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.unitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_POSTID, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.postId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_EMPLOYEERECORDNAME, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.employeeRecordName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_EMPLOYEERECORDNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.employeeRecordNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_UNITNAME, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.unitName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_UNITNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.unitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_POSTNAME, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.postName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_POSTNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.postNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_INSERTEDBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.insertedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_MODIFIEDBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_marks_committee_membersDTO.modifiedBy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			
			
		
						</table>
                    </div>
			






			</div>	

               


        </div>
	</div>