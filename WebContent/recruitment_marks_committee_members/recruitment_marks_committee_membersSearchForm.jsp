
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_marks_committee_members.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Recruitment_marks_committee_membersDAO recruitment_marks_committee_membersDAO = (Recruitment_marks_committee_membersDAO)request.getAttribute("recruitment_marks_committee_membersDAO");


String navigator2 = SessionConstants.NAV_RECRUITMENT_MARKS_COMMITTEE_MEMBERS;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_RECRUITMENTMARKSCOMMITTEEID, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_MOBILE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_OTP, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_EMPLOYEERECORDID, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_UNITID, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_POSTID, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_EMPLOYEERECORDNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_EMPLOYEERECORDNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_UNITNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_UNITNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_POSTNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_POSTNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_INSERTEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_MODIFIEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SEARCH_RECRUITMENT_MARKS_COMMITTEE_MEMBERS_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<span class="ml-4">All</span>
									<div class="d-flex align-items-center mr-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>&nbsp;
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_RECRUITMENT_MARKS_COMMITTEE_MEMBERS);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO = (Recruitment_marks_committee_membersDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("recruitment_marks_committee_membersDTO",recruitment_marks_committee_membersDTO);
								%>  
								
								 <jsp:include page="./recruitment_marks_committee_membersSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			