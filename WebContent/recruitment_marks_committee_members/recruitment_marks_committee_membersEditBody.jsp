<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="recruitment_marks_committee_members.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
Recruitment_marks_committee_membersDTO recruitment_marks_committee_membersDTO;
recruitment_marks_committee_membersDTO = (Recruitment_marks_committee_membersDTO)request.getAttribute("recruitment_marks_committee_membersDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(recruitment_marks_committee_membersDTO == null)
{
	recruitment_marks_committee_membersDTO = new Recruitment_marks_committee_membersDTO();

}
System.out.println("recruitment_marks_committee_membersDTO = " + recruitment_marks_committee_membersDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_FORMNAME, loginDTO);
String servletName = "Recruitment_marks_committee_membersServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
String Language = LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Recruitment_marks_committee_membersServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                     </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.iD%>' tag='pb_html'/>

														<input type='hidden' class='form-control'  name='recruitmentMarksCommitteeId' id = 'recruitmentMarksCommitteeId_hidden_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.recruitmentMarksCommitteeId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_MOBILE, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='mobile' id = 'mobile_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.mobile%>' 																<%
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="880[0-9]{10}" title="mobile must start with 880, then contain 10 digits"
																<%
																	}
																%>
  tag='pb_html'/>
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_OTP, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='otp' id = 'otp_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.otp%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
														<input type='hidden' class='form-control'  name='employeeRecordId' id = 'employeeRecordId_hidden_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.employeeRecordId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='unitId' id = 'unitId_hidden_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.unitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='postId' id = 'postId_hidden_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.postId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_EMPLOYEERECORDNAME, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='employeeRecordName' id = 'employeeRecordName_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.employeeRecordName%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_EMPLOYEERECORDNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='employeeRecordNameBn' id = 'employeeRecordNameBn_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.employeeRecordNameBn%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_UNITNAME, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='unitName' id = 'unitName_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.unitName%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_UNITNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='unitNameBn' id = 'unitNameBn_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.unitNameBn%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_POSTNAME, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='postName' id = 'postName_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.postName%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_POSTNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='postNameBn' id = 'postNameBn_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.postNameBn%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.insertionDate%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_INSERTEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.insertedBy%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_MODIFIEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.modifiedBy%>' 																  tag='pb_html'/>
															</div>
                                                      </div>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=recruitment_marks_committee_membersDTO.isDeleted%>' tag='pb_html'/>

														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=recruitment_marks_committee_membersDTO.lastModificationTime%>' tag='pb_html'/>

														</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                          </div>
                       </div>
                       <div class="form-actions text-center mb-5">
	                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_RECRUITMENT_MARKS_COMMITTEE_MEMBERS_CANCEL_BUTTON, loginDTO)%></a>
							<button class="btn btn-success" type="submit"><%=LM.getText(LC.RECRUITMENT_MARKS_COMMITTEE_MEMBERS_ADD_RECRUITMENT_MARKS_COMMITTEE_MEMBERS_SUBMIT_BUTTON, loginDTO)%></button>
					   </div>
                   </form>
               </div>
          </div>
      </div>
 </div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{



	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Recruitment_marks_committee_membersServlet");
}

function init(row)
{



}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
});

var child_table_extra_id = <%=childTableStartingID%>;



</script>






