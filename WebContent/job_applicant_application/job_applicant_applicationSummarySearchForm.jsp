
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_application.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}


Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOBAPPLICANTID, loginDTO)%></th>
								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOBID, loginDTO)%></th>
								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ACCEPTANCESTATUS, loginDTO)%></th>
								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_INSERTIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_INSERTEDBYUSERID, loginDTO)%></th>
								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_MODIFIEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ROLLNUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_JOB_APPLICANT_APPLICATION_EDIT_BUTTON, loginDTO)%></th>
								<th><input type="submit" class="btn btn-xs btn-danger" value="<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_JOB_APPLICANT_APPLICATION_DELETE_BUTTON, loginDTO)%>" /></th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_JOB_APPLICANT_APPLICATION);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Job_applicant_applicationDTO job_applicant_applicationDTO = (Job_applicant_applicationDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("job_applicant_applicationDTO",job_applicant_applicationDTO);
								%>  
								
								 <jsp:include page="./job_applicant_applicationSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>



			