<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_application.Job_applicant_applicationDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Job_applicant_applicationDTO job_applicant_applicationDTO = (Job_applicant_applicationDTO)request.getAttribute("job_applicant_applicationDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(job_applicant_applicationDTO == null)
{
	job_applicant_applicationDTO = new Job_applicant_applicationDTO();
	
}
System.out.println("job_applicant_applicationDTO = " + job_applicant_applicationDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.JOB_APPLICANT_APPLICATION_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=job_applicant_applicationDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobApplicantId'>")%>
			

		<input type='hidden' class='form-control'  name='jobApplicantId' id = 'jobApplicantId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.jobApplicantId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobId'>")%>
			

		<input type='hidden' class='form-control'  name='jobId' id = 'jobId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.jobId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_acceptanceStatus'>")%>
			
	
	<div class="form-inline" id = 'acceptanceStatus_div_<%=i%>'>
		<input type='text' class='form-control'  name='acceptanceStatus' id = 'acceptanceStatus_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.acceptanceStatus + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy'>")%>
			
	
	<div class="form-inline" id = 'modifiedBy_div_<%=i%>'>
		<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.modifiedBy + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=job_applicant_applicationDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_rollNumber'>")%>
			
	
	<div class="form-inline" id = 'rollNumber_div_<%=i%>'>
		<input type='number' class='form-control'  name='rollNumber' id = 'rollNumber_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.rollNumber + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
					
	
											<td>
												<a href='Job_applicant_applicationServlet?actionType=view&ID=<%=job_applicant_applicationDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Job_applicant_applicationServlet?actionType=view&modal=1&ID=<%=job_applicant_applicationDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	