

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_application.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.JOB_APPLICANT_APPLICATION_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO("job_applicant_application");
Job_applicant_applicationDTO job_applicant_applicationDTO = job_applicant_applicationDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title">Job Applicant Application Details</h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3>Job Applicant Application</h3>
						<table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOBAPPLICANTID, loginDTO)%></b></td>
								<td>
						
											<%
											value = job_applicant_applicationDTO.jobApplicantId + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOBID, loginDTO)%></b></td>
								<td>
						
											<%
											value = job_applicant_applicationDTO.jobId + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ACCEPTANCESTATUS, loginDTO)%></b></td>
								<td>
						
											<%
											value = job_applicant_applicationDTO.acceptanceStatus + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_INSERTIONDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = job_applicant_applicationDTO.insertionDate + "";
											%>
											<%
											String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_insertionDate%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_INSERTEDBYUSERID, loginDTO)%></b></td>
								<td>
						
											<%
											value = job_applicant_applicationDTO.insertedByUserId + "";
											%>
											<%
											value = WorkflowController.getNameFromUserId(job_applicant_applicationDTO.insertedByUserId, Language);
											if(value.equalsIgnoreCase(""))
											{
												value = "superman";
											}
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_MODIFIEDBY, loginDTO)%></b></td>
								<td>
						
											<%
											value = job_applicant_applicationDTO.modifiedBy + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			
			
			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ROLLNUMBER, loginDTO)%></b></td>
								<td>
						
											<%
											value = job_applicant_applicationDTO.rollNumber + "";
											%>
														
											<%=value%>
				
			
								</td>
						
							</tr>

				


			
		
						</table>
                    </div>
			






			</div>	

               


        </div>