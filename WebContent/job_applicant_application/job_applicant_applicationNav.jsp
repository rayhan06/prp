<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%
	System.out.println("Inside nav.jsp");
	String url = request.getParameter("url");
	String navigator = request.getParameter("navigator");
	String pageName = request.getParameter("pageName");
	if (pageName == null)
		pageName = "Search";
	String pageno = "";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
	pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

	System.out.println("rn " + rn);

	String action = url;
	String context = "../../.." + request.getContextPath() + "/";
	String link = context + url;
	String concat = "?";
	if (url.contains("?")) {
		concat = "&";
	}
	String[][] searchFieldInfo = rn.getSearchFieldInfo();
	String totalPage = "1";
	if (rn != null)
		totalPage = rn.getTotalPages() + "";
	int row = 0;

	String Language = LM.getText(LC.JOB_APPLICANT_APPLICATION_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	int pagination_number = 0;
	boolean isPermanentTable = rn.m_isPermanentTable;
	System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>





<!-- search control -->
<div class="box box-primary">
	
	<div class="box-body">
		
		<!-- BEGIN FORM-->
		<div class="form-horizontal">
		
			<div class="form-body">
			
				<div class="row">

					<div class="col-lg-3">
						<label for="" class="control-label col-4"><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_ANYFIELD, loginDTO)%>:</label>
						<div class="col-8">
							<input type="text" class="form-control" id="anyfield"
								placeholder="" name="anyfield" onKeyUp='allfield_changed("",0)'>
						</div>
					</div>
					<div class="col-lg-3">
						<label for="" class="control-label col-4"><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_JOBAPPLICANTID, loginDTO)%></label>						
						<div class="col-8">
							<input type="text" class="form-control" id="job_applicant_id" placeholder="" name="job_applicant_id" onkeyup="allfield_changed('',0)">
						</div>
					</div>
					<div class="col-lg-3">
						<label for="" class="control-label col-4"><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_JOBID, loginDTO)%></label>						
						<div class="col-8">
							<input type="text" class="form-control" id="job_id" placeholder="" name="job_id" onkeyup="allfield_changed('',0)">
						</div>
					</div>
					<div class="col-lg-3">
						<label for="" class="control-label col-4"><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_ACCEPTANCESTATUS, loginDTO)%></label>						
						<div class="col-8">
							<input type="text" class="form-control" id="acceptance_status" placeholder="" name="acceptance_status" onkeyup="allfield_changed('',0)">
						</div>
					</div>
					<div class="col-lg-3">
						<label for="" class="control-label col-4"><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_INSERTIONDATE, loginDTO)%></label>						
						<div class="col-8">
							<input type="text" class="form-control" id="insertion_date" placeholder="" name="insertion_date" onkeyup="allfield_changed('',0)">
						</div>
					</div>
					<div class="col-lg-3">
						<label for="" class="control-label col-4"><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_INSERTEDBYUSERID, loginDTO)%></label>						
						<div class="col-8">
							<input type="text" class="form-control" id="inserted_by_user_id" placeholder="" name="inserted_by_user_id" onkeyup="allfield_changed('',0)">
						</div>
					</div>
					<div class="col-lg-3">
						<label for="" class="control-label col-4"><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_MODIFIEDBY, loginDTO)%></label>						
						<div class="col-8">
							<input type="text" class="form-control" id="modified_by" placeholder="" name="modified_by" onkeyup="allfield_changed('',0)">
						</div>
					</div>
					<div class="col-lg-3">
						<label for="" class="control-label col-4"><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_ROLLNUMBER, loginDTO)%></label>						
						<div class="col-8">
							<input type="number" class="form-control" id="roll_number" placeholder="" name="roll_number" onkeyup="allfield_changed('',0)" onmouseup="allfield_changed('',0)">
						</div>
					</div>
					
					
				</div>
				
			</div>
		</div>
		<!-- END FORM-->
		
	</div>
</div>




<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>


<script type="text/javascript">

	function dosubmit(params)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		//alert(params);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) 
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		  
		  xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
		  xhttp.send();
		
	}

	function allfield_changed(go, pagination_number)
	{
		console.log('came here')
		var params = 'AnyField=' + document.getElementById('anyfield').value;

		params +=  '&acceptance_status='+ $('#acceptance_status').val();
		// params +=  '&insertion_date_start='+ getBDFormattedDate('insertion_date');
		// params +=  '&insertion_date_end='+ getBDFormattedDateWithOneDayAddition('insertion_date_end');
		params +=  '&modified_by='+ $('#modified_by').val();
		
		params +=  '&search=true&ajax=true';
		
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged == 0)
		{
			console.log("go found");
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		dosubmit(params);
	
	}

</script>

