
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="parliament_job_applicant.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="job_applicant_certification.Job_applicant_certificationDTO" %>
<%@ page import="job_applicant_documents.Job_applicant_documentsDTO" %>
<%@ page import="job_applicant_education_info.Job_applicant_education_infoDTO" %>
<%@ page import="job_applicant_photo_signature.Job_applicant_photo_signatureDTO" %>
<%@ page import="job_applicant_professional_info.Job_applicant_professional_infoDTO" %>
<%@ page import="job_applicant_reference.Job_applicant_referenceDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="job_applicant_certification.Job_applicant_certificationDAO" %>
<%@ page import="job_applicant_education_info.Job_applicant_education_infoDAO" %>
<%@ page import="job_applicant_professional_info.Job_applicant_professional_infoDAO" %>
<%@ page import="job_applicant_documents.Job_applicant_documentsDAO" %>
<%@ page import="job_applicant_photo_signature.Job_applicant_photo_signatureDAO" %>
<%@ page import="job_applicant_reference.Job_applicant_referenceDAO" %>
<%@ page import="category.CategoryDTO" %>
<%@ page import="category.CategoryDAO" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="util.CommonConstant" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="job_applicant_application.SummaryDTO" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="job_applicant_certification.Job_applicant_certificationSummaryDTO" %>
<%@ page import="job_applicant_photo_signature.Job_applicant_photo_signatureSummaryDTO" %>
<%@ page import="job_applicant_education_info.Job_applicant_education_infoSummaryDTO" %>
<%@ page import="job_applicant_professional_info.Job_applicant_professional_infoSummaryDTO" %>
<%@ page import="job_applicant_documents.Job_applicant_documentsSummaryDTO" %>
<%@ page import="job_applicant_reference.Job_applicant_referenceSummaryDTO" %>
<%@ page import="job_applicant_qualifications.Job_applicant_qualificationSummaryDTO" %>
<%@ page import="config.GlobalConfigurationRepository" %>
<%@ page import="config.GlobalConfigConstants" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="religion.ReligionRepository" %>

<%


	Job_applicant_photo_signatureDAO job_applicant_photo_signatureDAO = new Job_applicant_photo_signatureDAO();


	Job_applicant_applicationDTO job_applicant_applicationDTO = (Job_applicant_applicationDTO) request.getAttribute("job_applicant_applicationDTO");


	String summary = job_applicant_applicationDTO.summary;

	Gson gson = new Gson();
	SummaryDTO summaryDTO = gson.fromJson(summary, SummaryDTO.class);

	List<Job_applicant_certificationSummaryDTO> job_applicant_certificationSummaryDTOList = summaryDTO.job_applicant_certificationSummaryDTOList;
	List<Job_applicant_photo_signatureSummaryDTO> job_applicant_photo_signatureSummaryDTOList =
			summaryDTO.job_applicant_photo_signatureSummaryDTOList;

//	System.out.println("######");
//	System.out.println(job_applicant_photo_signatureSummaryDTOList);
//	System.out.println("######");
	List<Job_applicant_education_infoSummaryDTO> job_applicant_education_infoSummaryDTOList = summaryDTO.job_applicant_education_infoSummaryDTOList;
	List<Job_applicant_professional_infoSummaryDTO> job_applicant_professional_infoSummaryDTOList = summaryDTO.job_applicant_professional_infoSummaryDTOList;
	List<Job_applicant_documentsSummaryDTO> job_applicant_documentsSummaryDTOList = summaryDTO.job_applicant_documentsSummaryDTOList;
	List<Job_applicant_referenceSummaryDTO> job_applicant_referenceSummaryDTOList = summaryDTO.job_applicant_referenceSummaryDTOList;
	List<Job_applicant_qualificationSummaryDTO> job_applicant_qualificationSummaryDTOList = summaryDTO.job_applicant_qualificationSummaryDTOList;

	request.getSession(true).setAttribute("job_applicant_certificationSummaryDTOList", job_applicant_certificationSummaryDTOList);
	request.getSession(true).setAttribute("job_applicant_photo_signatureSummaryDTOList", job_applicant_photo_signatureSummaryDTOList);
	request.getSession(true).setAttribute("job_applicant_education_infoSummaryDTOList", job_applicant_education_infoSummaryDTOList);
	request.getSession(true).setAttribute("job_applicant_professional_infoSummaryDTOList", job_applicant_professional_infoSummaryDTOList);
	request.getSession(true).setAttribute("job_applicant_documentsSummaryDTOList", job_applicant_documentsSummaryDTOList);
	request.getSession(true).setAttribute("job_applicant_referenceSummaryDTOList", job_applicant_referenceSummaryDTOList);
	request.getSession(true).setAttribute("job_applicant_qualificationSummaryDTOList", job_applicant_qualificationSummaryDTOList);

	Parliament_job_applicantDTO parliament_job_applicantDTO = summaryDTO.parliament_job_applicantDTO;

	Recruitment_job_descriptionDAO recruitmentJobDescriptionDAO = new Recruitment_job_descriptionDAO();
	Recruitment_job_descriptionDTO recruitmentJobDescriptionDTO = Recruitment_job_descriptionRepository.getInstance().
			getRecruitment_job_descriptionDTOByID(job_applicant_applicationDTO.jobId);
//			recruitmentJobDescriptionDAO.getDTOByID(job_applicant_applicationDTO.jobId);




	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO currentUserDTO = UserRepository.getUserDTOByUserID(loginDTO);

	if(parliament_job_applicantDTO == null)
	{
		parliament_job_applicantDTO = new Parliament_job_applicantDTO();

	}
	System.out.println("parliament_job_applicantDTO = " + parliament_job_applicantDTO);

	String actionName;
	System.out.println("actionType = " + request.getParameter("actionType"));
	if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
	{
		actionName = "add";
	}
	else
	{
		actionName = "edit";
	}
	String formTitle = LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_PARLIAMENT_JOB_APPLICANT_ADD_FORMNAME, loginDTO);


	String ID = request.getParameter("ID");
	if(ID == null || ID.isEmpty())
	{
		ID = "0";
	}
	System.out.println("ID = " + ID);
	int i = 0;

	String value = "";

	int childTableStartingID = 1;

	boolean isPermanentTable = true;

	String nameOfPost = LM.getLanguageIDByUserDTO(currentUserDTO)== CommonConstant.Language_ID_Bangla?recruitmentJobDescriptionDTO.jobTitleBn:recruitmentJobDescriptionDTO.jobTitleEn;

	String Language = LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_LANGUAGE, loginDTO);

	String freedomQuota = CatRepository.getName(Language, "freedom_fighter", parliament_job_applicantDTO.freedomFighterCat);

	String languageForQuery = Language.equals("English")?"languageTextEnglish":"languageTextBangla";

	String quota = CatRepository.getName(Language, "recruitment_quota", parliament_job_applicantDTO.specialQuotaCat);

//	if (parliament_job_applicantDTO.freedomFighterCat != 0 && parliament_job_applicantDTO.freedomFighterCat != 3) {
//		quota += "," + freedomQuota;
//	}
//
//	if (parliament_job_applicantDTO.ethnicMinorityCat == 1) {
//		quota += "," + CommonDAO.getName(LC.PARLIAMENT_JOB_APPLICANT_ADD_ETHNICMINORITYCAT,"language_text", languageForQuery, "id");
//	}
//
//	if (parliament_job_applicantDTO.specialQuotaCat == 1) {
//		quota += "," + CommonDAO.getName(LC.PARLIAMENT_JOB_APPLICANT_ADD_SPECIALQUOTACAT, "language_text", languageForQuery, "id");
//	}
//
//	if (parliament_job_applicantDTO.disabilityCat == 1) {
//		quota += "," + CommonDAO.getName(LC.PARLIAMENT_JOB_APPLICANT_ADD_DISABILITYCAT, "language_text", languageForQuery, "id");
//	}
//
//	if (!quota.isEmpty()) {
//		quota = quota.substring(1);
//	}
//	else quota = UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");

	if (parliament_job_applicantDTO.specialQuotaCat == 0 || parliament_job_applicantDTO.specialQuotaCat == 8){
		quota = UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
	}



	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);

//					CategoryDTO categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("marital_status",parliament_job_applicantDTO.maritalStatusCat );
	String maritalStatus = CatRepository.getName(Language, "marital_status", parliament_job_applicantDTO.maritalStatusCat);
	if (parliament_job_applicantDTO.maritalStatusCat ==0)maritalStatus=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");

	String maritalStatusFormatted = maritalStatus;

	if (parliament_job_applicantDTO.maritalStatusCat == 2) {
		maritalStatusFormatted += "(" + LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_SPOUSENAME, loginDTO) + ":" + parliament_job_applicantDTO.spouseName + ")";
	}

//					categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("gender",parliament_job_applicantDTO.genderCat );
	String gender = CatRepository.getName(Language, "gender", parliament_job_applicantDTO.genderCat);
	if (parliament_job_applicantDTO.genderCat ==0)gender=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
//					categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("job_applicant_employment_status",parliament_job_applicantDTO.employmentStatusCat );
	String employmentStatus = CatRepository.getName(Language, "job_applicant_employment_status", parliament_job_applicantDTO.employmentStatusCat);
	if (parliament_job_applicantDTO.employmentStatusCat ==0)employmentStatus=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
//					categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("ethnic_minority",parliament_job_applicantDTO.ethnicMinorityCat );
//	String ethnicMinority = CatRepository.getInstance().getName(Language, "ethnic_minority", parliament_job_applicantDTO.ethnicMinorityCat);
//	if (parliament_job_applicantDTO.ethnicMinorityCat ==0)ethnicMinority=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
//					categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("freedom_fighter",parliament_job_applicantDTO.freedomFighterCat );
//	String freedomFighter = CatRepository.getInstance().getName(Language, "freedom_fighter", parliament_job_applicantDTO.freedomFighterCat);
//	if (parliament_job_applicantDTO.freedomFighterCat ==0)freedomFighter=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
//					categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("nationality",parliament_job_applicantDTO.nationalityCat );
	String nationality = CatRepository.getName(Language, "nationality", parliament_job_applicantDTO.nationalityCat);
	if (parliament_job_applicantDTO.nationalityCat ==0)nationality=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
//					categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("job_applicant_disability",parliament_job_applicantDTO.disabilityCat );
//	String disability = CatRepository.getInstance().getName(Language, "job_applicant_disability", parliament_job_applicantDTO.disabilityCat);
//	if (parliament_job_applicantDTO.disabilityCat ==0)disability=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
//					categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("job_applicant_special_quota",parliament_job_applicantDTO.specialQuotaCat );
//	String specialQuota = CatRepository.getInstance().getName(Language, "job_applicant_special_quota", parliament_job_applicantDTO.specialQuotaCat);
//	if (parliament_job_applicantDTO.specialQuotaCat ==0)specialQuota=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");
//					categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("job_applicant_identification_type",parliament_job_applicantDTO.identificationTypeCat );
//	String identificationType = CatRepository.getInstance().getName(Language, "job_applicant_identification_type", parliament_job_applicantDTO.identificationTypeCat);
//	if (parliament_job_applicantDTO.identificationTypeCat ==0)identificationType=UtilCharacter.getDataByLanguage(Language, "প্রযোজ্য নহে", "N/A");

	String bloodGroup = CatRepository.getName(Language, "blood_group", parliament_job_applicantDTO.bloodGroupCat);

	String formatted_dateOfBirth = dateFormat.format(new Date(parliament_job_applicantDTO.dateOfBirth));

	FilesDAO filesDAO = new FilesDAO();

	Long id = Long.parseLong( ID );

	String homeDistrict = "";

	if (!parliament_job_applicantDTO.permanentAddress.isEmpty()) {
		homeDistrict = GeoLocationDAO2.parseText(parliament_job_applicantDTO.permanentAddress, Language);
		String[] list = homeDistrict.split(",");
		if (list.length > 1)homeDistrict = list[1];
	}

//	Job_applicant_photo_signatureDTO photo = job_applicant_photo_signatureDAO.getDTOByJobApplicantIDAndFileType(parliament_job_applicantDTO.iD, 1).get(0);
//	Job_applicant_photo_signatureDTO signature = job_applicant_photo_signatureDAO.getDTOByJobApplicantIDAndFileType(parliament_job_applicantDTO.iD, 2).get(0);
//
//	List<FilesDTO> filesDTOs = filesDAO.getDTOsByFileID(photo.filesDropzone);
//	FilesDTO photoFile = filesDTOs.isEmpty()?null:filesDTOs.get(0);
//
//	filesDTOs = filesDAO.getDTOsByFileID(signature.filesDropzone);
//	FilesDTO signatureFile = filesDTOs.isEmpty()?null:filesDTOs.get(0);




	Optional<Job_applicant_photo_signatureSummaryDTO> pho = job_applicant_photo_signatureSummaryDTOList.stream()
			.filter(j -> j.photoOrSignature == 1).findFirst();
	List<FilesDTO> filesDTOs = new ArrayList<>();
	FilesDTO photoFile = null;
	if(pho.isPresent()){
		filesDTOs = filesDAO.getDTOsByFileID(pho.get().filesDropzone);
		photoFile = filesDTOs.isEmpty()?null:filesDTOs.get(0);
	}

	Optional<Job_applicant_photo_signatureSummaryDTO> sig = job_applicant_photo_signatureSummaryDTOList.stream()
			.filter(j -> j.photoOrSignature == 2).findFirst();
	filesDTOs = new ArrayList<>();
	FilesDTO signatureFile = null;
	if(sig.isPresent()){
		filesDTOs = filesDAO.getDTOsByFileID(sig.get().filesDropzone);
		signatureFile = filesDTOs.isEmpty()?null:filesDTOs.get(0);
	}

%>

<div id="summary_id">
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important; margin-bottom: -18px">
	<div class="row">


		<div class="col-lg-12">

			<div class="kt-portlet">

				<div class="kt-portlet__head">

					<div class="kt-portlet__head-label">

<%--						<h3 class="kt-portlet__head-title prp-page-title">--%>
<%--							<i class="fa fa-gift"></i>&nbsp;--%>

<%--							<%=formTitle%>--%>

<%--						</h3>--%>

					</div>

				</div>

				<div class="kt-portlet__body form-body">

					<div class="row">

						<div class="col-md-12">

							<div class="onlyborder">

								<div class="row">


									<div class="col-md-12">

										<div class="sub_title_top">

											<div class="sub_title">
												<h4 style="background: white">

													<%=formTitle%>

												</h4>
											</div>

											<div class="col-lg-12">
												<div class="kt-portlet shadow-none" style="margin-top: -20px">

													<div class="kt-portlet__body form-body">


														<div class="table-responsive">

															<table style="border: 1px solid gray; width: 100%;" class="table table-striped ">


																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;background-color: #e9ecef"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_POST_NAME, loginDTO)%>

																	</td>

																	<td valign="top" colspan="2" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;background-color: #e9ecef">

																		<div id="">
																			<%=nameOfPost%>
																		</div>

																	</td>



																	<td align="center" valign="center" class="form-group col-lg-3 font18 noBorder" style=" width: 20%; height: auto; border: 1px;">


																	</td>
																</tr>
																<%
																	long internShipId = Long.parseLong(GlobalConfigurationRepository.
																			getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);

																	if(internShipId != job_applicant_applicationDTO.jobId){
																%>

																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;background-color: #e9ecef"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_CATEGORY, loginDTO)%>

																	</td>

																	<td valign="top" colspan="2" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;background-color: #e9ecef">

																		<div id="">
																			<%
																				Options = CatRepository.getName(Language, "job_applicant_category_type", parliament_job_applicantDTO.jobApplicantCategoryCat);
																			%>
																			<%=Options%>
																			<%
																				if (parliament_job_applicantDTO.jobApplicantCategoryCat != 0) {

																					Date dateObjectView;
																					String formatted_startApplicationDateView = "";
																					if(parliament_job_applicantDTO.jobReferenceNoDate == 0){
																					} else {
																						dateObjectView = new Date(parliament_job_applicantDTO.jobReferenceNoDate);
																						SimpleDateFormat dateFormatView = new SimpleDateFormat("dd/MM/yyyy");
																						formatted_startApplicationDateView = dateFormatView.format(dateObjectView);
																					}
																			%>
																			<br>
																			<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_JOB_REFERNCE_NO, loginDTO)%>: <%=parliament_job_applicantDTO.jobReferenceNo%>, <%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_JOB_REFERNCE_NO_DATE, loginDTO)%>: <%=formatted_startApplicationDateView%>, <%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_CURRENT_POSITION, loginDTO)%>: <%=parliament_job_applicantDTO.currentPosition%>
																			<%
																				}
																			%>
																		</div>

																	</td>

																	<td align="center" valign="center" class="form-group col-lg-3 font18 noBorder" style=" width: 20%; height: auto; border: 1px;">

																	</td>
																</tr>

																<%
																	}
																%>


																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NAMEBN, loginDTO)%>

																	</td>

																	<td valign="top" colspan="2" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=parliament_job_applicantDTO.nameBn%>
																		</div>

																	</td>



																	<td rowspan="6" align="center" valign="center" class="form-group col-lg-3 font18 noBorder" style=" width: 20%; height: auto; border: 1px;">

																		<%
																			if(photoFile!=null && photoFile.fileBlob != null)
																			{
																				byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(photoFile.fileBlob);
																				value = photoFile.fileBlob + "";

																				out.println("<img src='" + "data:" + photoFile.fileTypes + ";base64," +  new String(encodeBase64) + "'  style='width:30%' >");

																				out.println("<a href = 'Job_applicant_photo_signatureServlet?actionType=downloadDropzoneFile&id=" + photoFile.fileID + "' download>" + "</a>");

																			}
																		%>


																	</td>
																</tr>


																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NAMEEN, loginDTO)%>

																	</td>

																	<td valign="top" colspan="2" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=parliament_job_applicantDTO.nameEn%>
																		</div>

																	</td>
																</tr>











																<tr>
																	<td valign="top"  class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_FATHERNAME, loginDTO)%>

																	</td>

																	<td valign="top" colspan="2" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=parliament_job_applicantDTO.fatherName%>
																		</div>

																	</td>

																</tr>
																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_MOTHERNAME, loginDTO)%>

																	</td>

																	<td valign="top" colspan="2" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=parliament_job_applicantDTO.motherName%>
																		</div>

																	</td>

																</tr>


																<tr>

																	<td valign="top"  class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_PRESENTADDRESS, loginDTO)%>

																	</td>

																	<td valign="top" colspan="2" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%
																				value = parliament_job_applicantDTO.presentAddress + "";
																			%>
																			<%--						<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_CARE_OF_PRESENT, loginDTO)%>: <%=parliament_job_applicantDTO.careOfPresent%>,--%>

																			<%
																				String addressParsed = GeoLocationDAO2.parseText(value, Language);
																				StringBuilder addressReversed = new StringBuilder();

																				if(!addressParsed.equals("")) {
																					String[] addresses = addressParsed.split(",");

																					for (int p = addresses.length - 1; p >= 0; p--) {
																						addressReversed.append("," + addresses[p]);
																					}
																				}

																			%>
																			<%

																				String addressdetails = GeoLocationDAO2.parseDetails(value);
																				if(!addressdetails.equals(""))
																				{
																			%>
																			<%=addressdetails%>,
																			<%
																				}
																			%>
																			<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_POST_CODE_PRESENT, loginDTO)%>: <%=parliament_job_applicantDTO.postCodePresent%>
																			<%=addressReversed%>
																		</div>

																	</td>


																</tr>

																<tr>
																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_PERMANENTADDRESS, loginDTO)%>

																	</td>

																	<td valign="top" colspan="2" class="form-group col-lg-6 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%
																				value = parliament_job_applicantDTO.permanentAddress + "";
																			%>
																			<%--						<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_CARE_OF_PRESENT, loginDTO)%>: <%=parliament_job_applicantDTO.careOfPresent%>,--%>

																			<%
																				addressParsed = GeoLocationDAO2.parseText(value, Language);
																				addressReversed = new StringBuilder();

																				if(!addressParsed.equals("")) {
																					String[] addresses = addressParsed.split(",");

																					for (int p = addresses.length - 1; p >= 0; p--) {
																						addressReversed.append("," + addresses[p]);
																					}
																				}

																			%>
																			<%

																				addressdetails = GeoLocationDAO2.parseDetails(value);
																				if(!addressdetails.equals(""))
																				{
																			%>
																			<%=addressdetails%>,
																			<%
																				}
																			%>
																			<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_POST_CODE_PERMANENT, loginDTO)%>: <%=parliament_job_applicantDTO.postCodePermanent%>
																			<%=addressReversed%>
																		</div>

																	</td>


																</tr>

																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_MARITALSTATUSCAT, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=maritalStatusFormatted%>
																		</div>

																	</td>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_RELIGION, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%
																				Options = ReligionRepository.getInstance().getText
																						(Language, parliament_job_applicantDTO.jobApplicantReligionCat);
//																						CommonDAO.getName(Language, "religion", parliament_job_applicantDTO.jobApplicantReligionCat);
																			%>
																			<%=Options%>
																		</div>

																	</td>



																</tr>

																<tr>


																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_DATEOFBIRTH, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=formatted_dateOfBirth%>
																		</div>

																	</td>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_GENDERCAT, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=gender%>
																		</div>

																	</td>


																</tr>



																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NATIONALITYCAT, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=nationality%>
																		</div>

																	</td>


																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_QUOTA, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=quota%>
																		</div>

																	</td>



																</tr>


																<tr style="display: none">

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_DEPARTMENTAL_CANDIDATE_STATUS, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%
																				Options = "";
																				if (parliament_job_applicantDTO.jobApplicantCandidateCat != 0) {
																					Options = CatDAO.getName(Language, "job_applicant_candidate_type", parliament_job_applicantDTO.jobApplicantCandidateCat);
																				}
																			%>
																			<%=Options%>
																			<%
																				if (parliament_job_applicantDTO.jobApplicantCandidateCat == 5) {
																			%>
																			(<%=parliament_job_applicantDTO.jobApplicantCandidateText%>)
																			<%
																				}
																			%>
																		</div>

																	</td>


																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=employmentStatus%>
																		</div>

																	</td>



																</tr>


																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_NID, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=parliament_job_applicantDTO.nid%>
																		</div>

																	</td>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_NID_REGISTRATION_NO, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=parliament_job_applicantDTO.identificationNo%>
																		</div>

																	</td>


																</tr>

																<tr>




																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_HOMEDISTRICTNAME, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=homeDistrict%>
																		</div>

																	</td>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_CONTACTNUMBER, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=UtilCharacter.convertDataByLanguage(Language, parliament_job_applicantDTO.contactNumber)%>
																		</div>

																	</td>



																</tr>

																<tr>


																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_EMAIL, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=parliament_job_applicantDTO.email%>
																		</div>

																	</td>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_HEIGHT, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(parliament_job_applicantDTO.height))%>
																		</div>

																	</td>



																</tr>

																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_WEIGHT, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(parliament_job_applicantDTO.weight))%>
																		</div>

																	</td>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.HM_BLOOD_GROUP, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div>
																			<%=bloodGroup%>
																		</div>

																	</td>



																</tr>

																<tr>

																	<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >

																		<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_CHEST, loginDTO)%>

																	</td>

																	<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">

																		<div id="">
																			<%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(parliament_job_applicantDTO.chest))%>
																		</div>

																	</td>



																</tr>


															</table>

														</div>
													</div>
												</div>
											</div>





										</div>

									</div>


								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="kt-portlet__body form-body">

					<div class="row">

						<div class="col-md-12">

							<div class="onlyborder">

								<div class="row">

									<div class="col-md-12">

										<div class="sub_title_top">

											<div class="sub_title">
												<h4 style="background: white">

													<%=LM.getText(LC.JOB_APPLICANT_EDUCATION_INFO_ADD_JOB_APPLICANT_EDUCATION_INFO_ADD_FORMNAME, loginDTO)%>

												</h4>
											</div>

											<jsp:include page="../job_applicant_education_info/job_applicant_education_infoSummarySearchForm.jsp">
												<jsp:param name="pageName" value="<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_SEARCH_JOB_APPLICANT_REFERENCE_SEARCH_FORMNAME, loginDTO)%>" />
												<jsp:param name="ID" value="<%=parliament_job_applicantDTO.iD%>" />
												<jsp:param name="summary" value="true" />
											</jsp:include>

										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="kt-portlet__body form-body">

					<div class="row">

						<div class="col-md-12">

							<div class="onlyborder">

								<div class="row">

									<div class="col-md-12">

										<div class="sub_title_top">

											<div class="sub_title">
												<h4 style="background: white">

													<%=LM.getText(LC.JOB_APPLICANT_PROFESSIONAL_INFO_ADD_JOB_APPLICANT_PROFESSIONAL_INFO_ADD_FORMNAME, loginDTO)%>

												</h4>
											</div>

											<jsp:include page="../job_applicant_professional_info/job_applicant_professional_infoSummarySearchForm.jsp">
												<jsp:param name="pageName" value="<%=LM.getText(LC.JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_JOB_APPLICANT_PROFESSIONAL_INFO_SEARCH_FORMNAME, loginDTO)%>" />
												<jsp:param name="ID" value="<%=parliament_job_applicantDTO.iD%>" />
												<jsp:param name="summary" value="true" />
											</jsp:include>

										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="kt-portlet__body form-body">

					<div class="row">

						<div class="col-md-12">

							<div class="onlyborder">

								<div class="row">

									<div class="col-md-12">

										<div class="sub_title_top">

											<div class="sub_title">
												<h4 style="background: white">

													<%=LM.getText(LC.JOB_APPLICANT_CERTIFICATION_ADD_JOB_APPLICANT_CERTIFICATION_ADD_FORMNAME, loginDTO)%>

												</h4>
											</div>

											<jsp:include page="../job_applicant_certification/job_applicant_certificationSummarySearchForm.jsp">
												<jsp:param name="pageName" value="<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_SEARCH_ANYFIELD, loginDTO)%>" />
												<jsp:param name="ID" value="<%=parliament_job_applicantDTO.iD%>" />
												<jsp:param name="summary" value="true" />
											</jsp:include>

										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="kt-portlet__body form-body">

					<div class="row">

						<div class="col-md-12">

							<div class="onlyborder">

								<div class="row">

									<div class="col-md-12">

										<div class="sub_title_top">

											<div class="sub_title">
												<h4 style="background: white">

													<%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_FILES, loginDTO)%>

												</h4>
											</div>

											<jsp:include page="../job_applicant_documents/job_applicant_documentsSummarySearchForm.jsp">
												<jsp:param name="pageName" value="<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_SEARCH_JOB_APPLICANT_REFERENCE_SEARCH_FORMNAME, loginDTO)%>" />
												<jsp:param name="ID" value="<%=parliament_job_applicantDTO.iD%>" />
												<jsp:param name="jobId" value="<%=job_applicant_applicationDTO.jobId%>" />
												<jsp:param name="summary" value="true" />
											</jsp:include>

										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="kt-portlet__body form-body">

					<div class="row">

						<div class="col-md-12">

							<div class="onlyborder">

								<div class="row">

									<div class="col-md-12">

										<div class="sub_title_top">

											<div class="sub_title">
												<h4 style="background: white">

													<%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_QUALIFICATION, loginDTO)%>

												</h4>
											</div>

											<jsp:include page="../job_applicant_qualifications/job_applicant_qualificationsSummarySearchForm.jsp">
												<jsp:param name="pageName" value="<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_SEARCH_JOB_APPLICANT_REFERENCE_SEARCH_FORMNAME, loginDTO)%>" />
												<jsp:param name="ID" value="<%=parliament_job_applicantDTO.iD%>" />
												<jsp:param name="jobId" value="<%=job_applicant_applicationDTO.jobId%>" />
												<jsp:param name="summary" value="true" />
											</jsp:include>

										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

<%--				<div class="kt-portlet__body form-body">--%>

<%--					<div class="row">--%>

<%--						<div class="col-md-12">--%>

<%--							<div class="onlyborder">--%>

<%--								<div class="row">--%>

<%--									<div class="col-md-12">--%>

<%--										<div class="sub_title_top">--%>

<%--											<div class="sub_title">--%>
<%--												<h4 style="background: white">--%>

<%--													<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_SEARCH_ANYFIELD, loginDTO)%>--%>

<%--												</h4>--%>
<%--											</div>--%>

<%--											<jsp:include page="../job_applicant_reference/job_applicant_referenceSummarySearchForm.jsp">--%>
<%--												<jsp:param name="pageName" value="<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_SEARCH_ANYFIELD, loginDTO)%>" />--%>
<%--												<jsp:param name="ID" value="<%=parliament_job_applicantDTO.iD%>" />--%>
<%--												<jsp:param name="jobId" value="<%=job_applicant_applicationDTO.jobId%>" />--%>
<%--												<jsp:param name="summary" value="true" />--%>
<%--											</jsp:include>--%>

<%--										</div>--%>

<%--									</div>--%>

<%--								</div>--%>
<%--							</div>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--				</div>--%>



			</div>
		</div>
	</div>


	<div class="box box-primary">

		<div class="">







		</div>
	</div>



	<div class="row">
		<div class="col-9">

		</div>

		<div class="col-3">
			<br>

			<div class="col-12">
				<%

//					if(signature != null){

						if(signatureFile != null && signatureFile.fileBlob != null)
						{
							byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(signatureFile.fileBlob);
							value = signatureFile.fileBlob + "";

							out.println("<img id='summary_signature_id' src='" + "data:" + signatureFile.fileTypes + ";base64," +  new String(encodeBase64) + "'  style='width:65%; float: right;' >");

							out.println("<a href = 'Job_applicant_photo_signatureServlet?actionType=downloadDropzoneFile&id=" + signatureFile.fileID + "' download>" + "</a>");

						}

//					}


				%>

			</div>


		</div>

	</div>

</div>
	</div>

<br>

<div class="form-actions text-center">
	<button onclick="printSummary()" class="btn btn-success">
		<%=LM.getText(LC.HM_PRINT, loginDTO)%>
	</button>
</div>

<%--<div id="summary_id">--%>

<%--<div class="box box-primary">--%>
<%--	<div class=" ">--%>
<%--		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>--%>
<%--	</div>--%>
<%--	<div class="">--%>



<%--		<table style="border: 1px solid gray; width: 100%;">--%>


<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;background-color: #e9ecef"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_POST_NAME, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" colspan="2" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;background-color: #e9ecef">--%>

<%--					<div id="">--%>
<%--						<%=nameOfPost%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--				<td align="center" valign="center" class="form-group col-lg-3 font18 noBorder" style=" width: 20%; height: auto; border: 1px;">--%>


<%--				</td>--%>
<%--			</tr>--%>
<%--			<%--%>
<%--				long internShipId = Long.parseLong(GlobalConfigurationRepository.getInstance().--%>
<%--						getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);--%>

<%--				if(internShipId != job_applicant_applicationDTO.jobId){--%>
<%--			%>--%>

<%--					<tr>--%>

<%--						<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;background-color: #e9ecef"  >--%>

<%--							<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_CATEGORY, loginDTO)%>--%>

<%--						</td>--%>

<%--						<td valign="top" colspan="2" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;background-color: #e9ecef">--%>

<%--							<div id="">--%>
<%--								<%--%>
<%--									Options = CatDAO.getName(Language, "job_applicant_category_type", parliament_job_applicantDTO.jobApplicantCategoryCat);--%>
<%--								%>--%>
<%--								<%=Options%>--%>
<%--								<%--%>
<%--									if (parliament_job_applicantDTO.jobApplicantCategoryCat != 0) {--%>

<%--										Date dateObjectView;--%>
<%--										String formatted_startApplicationDateView = "";--%>
<%--										if(parliament_job_applicantDTO.jobReferenceNoDate == 0){--%>
<%--										} else {--%>
<%--											dateObjectView = new Date(parliament_job_applicantDTO.jobReferenceNoDate);--%>
<%--											SimpleDateFormat dateFormatView = new SimpleDateFormat("dd/MM/yyyy");--%>
<%--											formatted_startApplicationDateView = dateFormatView.format(dateObjectView).toString();--%>
<%--										}--%>
<%--								%>--%>
<%--								<br>--%>
<%--								<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_JOB_REFERNCE_NO, loginDTO)%>: <%=parliament_job_applicantDTO.jobReferenceNo%>, <%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_JOB_REFERNCE_NO_DATE, loginDTO)%>: <%=formatted_startApplicationDateView%>, <%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_CURRENT_POSITION, loginDTO)%>: <%=parliament_job_applicantDTO.currentPosition%>--%>
<%--								<%--%>
<%--									}--%>
<%--								%>--%>
<%--							</div>--%>

<%--						</td>--%>

<%--						<td align="center" valign="center" class="form-group col-lg-3 font18 noBorder" style=" width: 20%; height: auto; border: 1px;">--%>

<%--						</td>--%>
<%--					</tr>--%>

<%--			<%--%>
<%--				}--%>
<%--			%>--%>


<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NAMEBN, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" colspan="2" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=parliament_job_applicantDTO.nameBn%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--				<td rowspan="6" align="center" valign="center" class="form-group col-lg-3 font18 noBorder" style=" width: 20%; height: auto; border: 1px;">--%>

<%--					<%--%>
<%--						if(photoFile!=null && photoFile.fileBlob != null)--%>
<%--						{--%>
<%--							byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(photoFile.fileBlob);--%>
<%--							value = photoFile.fileBlob + "";--%>

<%--							out.println("<img src='" + "data:" + photoFile.fileTypes + ";base64," +  new String(encodeBase64) + "'  style='width:30%' >");--%>

<%--							out.println("<a href = 'Job_applicant_photo_signatureServlet?actionType=downloadDropzoneFile&id=" + photoFile.fileID + "' download>" + "</a>");--%>

<%--						}--%>
<%--					%>--%>


<%--				</td>--%>
<%--			</tr>--%>


<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NAMEEN, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" colspan="2" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=parliament_job_applicantDTO.nameEn%>--%>
<%--					</div>--%>

<%--				</td>--%>
<%--			</tr>--%>











<%--			<tr>--%>
<%--				<td valign="top"  class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_FATHERNAME, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" colspan="2" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=parliament_job_applicantDTO.fatherName%>--%>
<%--					</div>--%>

<%--				</td>--%>

<%--			</tr>--%>
<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_MOTHERNAME, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" colspan="2" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=parliament_job_applicantDTO.motherName%>--%>
<%--					</div>--%>

<%--				</td>--%>

<%--			</tr>--%>


<%--			<tr>--%>

<%--				<td valign="top"  class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_PRESENTADDRESS, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" colspan="2" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%--%>
<%--							value = parliament_job_applicantDTO.presentAddress + "";--%>
<%--						%>--%>
<%--						&lt;%&ndash;						<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_CARE_OF_PRESENT, loginDTO)%>: <%=parliament_job_applicantDTO.careOfPresent%>,&ndash;%&gt;--%>

<%--						<%--%>
<%--							String addressParsed = GeoLocationDAO2.parseText(value, Language);--%>
<%--							StringBuilder addressReversed = new StringBuilder("");--%>

<%--							if(!addressParsed.equals("")) {--%>
<%--								String[] addresses = addressParsed.split(",");--%>

<%--								for (int p = addresses.length - 1; p >= 0; p--) {--%>
<%--									addressReversed.append("," + addresses[p]);--%>
<%--								}--%>
<%--							}--%>

<%--						%>--%>
<%--						<%--%>

<%--							String addressdetails = GeoLocationDAO2.parseDetails(value);--%>
<%--							if(!addressdetails.equals(""))--%>
<%--							{--%>
<%--						%>--%>
<%--						<%=addressdetails%>,--%>
<%--						<%--%>
<%--							}--%>
<%--						%>--%>
<%--						<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_POST_CODE_PRESENT, loginDTO)%>: <%=parliament_job_applicantDTO.postCodePresent%>--%>
<%--						<%=addressReversed%>--%>
<%--					</div>--%>

<%--				</td>--%>


<%--			</tr>--%>

<%--			<tr>--%>
<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_PERMANENTADDRESS, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" colspan="2" class="form-group col-lg-6 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%--%>
<%--							value = parliament_job_applicantDTO.permanentAddress + "";--%>
<%--						%>--%>
<%--						&lt;%&ndash;						<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_CARE_OF_PRESENT, loginDTO)%>: <%=parliament_job_applicantDTO.careOfPresent%>,&ndash;%&gt;--%>

<%--						<%--%>
<%--							addressParsed = GeoLocationDAO2.parseText(value, Language);--%>
<%--							addressReversed = new StringBuilder("");--%>

<%--							if(!addressParsed.equals("")) {--%>
<%--								String[] addresses = addressParsed.split(",");--%>

<%--								for (int p = addresses.length - 1; p >= 0; p--) {--%>
<%--									addressReversed.append("," + addresses[p]);--%>
<%--								}--%>
<%--							}--%>

<%--						%>--%>
<%--						<%--%>

<%--							addressdetails = GeoLocationDAO2.parseDetails(value);--%>
<%--							if(!addressdetails.equals(""))--%>
<%--							{--%>
<%--						%>--%>
<%--						<%=addressdetails%>,--%>
<%--						<%--%>
<%--							}--%>
<%--						%>--%>
<%--						<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_POST_CODE_PERMANENT, loginDTO)%>: <%=parliament_job_applicantDTO.postCodePermanent%>--%>
<%--						<%=addressReversed%>--%>
<%--					</div>--%>

<%--				</td>--%>


<%--			</tr>--%>

<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_MARITALSTATUSCAT, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=maritalStatusFormatted%>--%>
<%--					</div>--%>

<%--				</td>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_RELIGION, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%--%>
<%--							Options = CommonDAO.getName(Language, "religion", parliament_job_applicantDTO.jobApplicantReligionCat);--%>
<%--						%>--%>
<%--						<%=Options%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--			</tr>--%>

<%--			<tr>--%>


<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_DATEOFBIRTH, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=formatted_dateOfBirth%>--%>
<%--					</div>--%>

<%--				</td>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_GENDERCAT, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=gender%>--%>
<%--					</div>--%>

<%--				</td>--%>


<%--			</tr>--%>



<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_NATIONALITYCAT, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=nationality%>--%>
<%--					</div>--%>

<%--				</td>--%>


<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_QUOTA, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=quota%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--			</tr>--%>


<%--			<tr style="display: none">--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_EDIT_DEPARTMENTAL_CANDIDATE_STATUS, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%--%>
<%--							Options = "";--%>
<%--							if (parliament_job_applicantDTO.jobApplicantCandidateCat != 0) {--%>
<%--								Options = CatDAO.getName(Language, "job_applicant_candidate_type", parliament_job_applicantDTO.jobApplicantCandidateCat);--%>
<%--							}--%>
<%--						%>--%>
<%--						<%=Options%>--%>
<%--						<%--%>
<%--							if (parliament_job_applicantDTO.jobApplicantCandidateCat == 5) {--%>
<%--						%>--%>
<%--						(<%=parliament_job_applicantDTO.jobApplicantCandidateText%>)--%>
<%--						<%--%>
<%--							}--%>
<%--						%>--%>
<%--					</div>--%>

<%--				</td>--%>


<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style=" width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style=" width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=employmentStatus%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--			</tr>--%>


<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_NID, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=parliament_job_applicantDTO.nid%>--%>
<%--					</div>--%>

<%--				</td>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_NID_REGISTRATION_NO, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=parliament_job_applicantDTO.identificationNo%>--%>
<%--					</div>--%>

<%--				</td>--%>


<%--			</tr>--%>

<%--			<tr>--%>




<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_HOMEDISTRICTNAME, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=homeDistrict%>--%>
<%--					</div>--%>

<%--				</td>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_CONTACTNUMBER, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=UtilCharacter.convertDataByLanguage(Language, parliament_job_applicantDTO.contactNumber)%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--			</tr>--%>

<%--			<tr>--%>


<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_EMAIL, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=parliament_job_applicantDTO.email%>--%>
<%--					</div>--%>

<%--				</td>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_HEIGHT, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(parliament_job_applicantDTO.height))%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--			</tr>--%>

<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_WEIGHT, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(parliament_job_applicantDTO.weight))%>--%>
<%--					</div>--%>

<%--				</td>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.HM_BLOOD_GROUP, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div>--%>
<%--						<%=bloodGroup%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--			</tr>--%>

<%--			<tr>--%>

<%--				<td valign="top" class="col-lg-3 control-label-2" name="labels" style="  width: 20%; height: auto; border: 1px solid gray;"  >--%>

<%--					<%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_CHEST, loginDTO)%>--%>

<%--				</td>--%>

<%--				<td valign="top" class="form-group col-lg-3 font18" style="  width: 30%; height: auto; border: 1px solid gray;">--%>

<%--					<div id="">--%>
<%--						<%=UtilCharacter.convertDataByLanguage(Language, String.valueOf(parliament_job_applicantDTO.chest))%>--%>
<%--					</div>--%>

<%--				</td>--%>



<%--			</tr>--%>


<%--		</table>--%>



<%--	</div>--%>
<%--</div>--%>




<%--	<div class="row">--%>
<%--		<div class="col-9">--%>

<%--		</div>--%>

<%--		<div class="col-3">--%>
<%--			<br>--%>

<%--			<div class="col-12">--%>
<%--				<%--%>

<%--					if(signature != null){--%>

<%--						if(signatureFile != null && signatureFile.fileBlob != null)--%>
<%--						{--%>
<%--							byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(signatureFile.fileBlob);--%>
<%--							value = signatureFile.fileBlob + "";--%>

<%--							out.println("<img id='summary_signature_id' src='" + "data:" + signatureFile.fileTypes + ";base64," +  new String(encodeBase64) + "'  style='width:65%; float: right;' >");--%>

<%--							out.println("<a href = 'Job_applicant_photo_signatureServlet?actionType=downloadDropzoneFile&id=" + signatureFile.fileID + "' download>" + "</a>");--%>

<%--						}--%>

<%--					}--%>


<%--				%>--%>

<%--			</div>--%>


<%--		</div>--%>

<%--	</div>--%>

<%--</div>--%>

<%--<br>--%>

<%--<div class="form-actions text-center">--%>
<%--	<button onclick="printSummary()" class="btn btn-success">--%>
<%--		<%=LM.getText(LC.HM_PRINT, loginDTO)%>--%>
<%--	</button>--%>
<%--</div>--%>


<%--<jsp:include page="../job_applicant_photo_signature/job_applicant_photo_signatureSummarySearchForm.jsp">--%>
<%--	<jsp:param name="pageName" value="<%=LM.getText(LC.JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_JOB_APPLICANT_PHOTO_SIGNATURE_SEARCH_FORMNAME, loginDTO)%>" />--%>
<%--</jsp:include>--%>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


	$(document).ready( function(){

		basicInit();

		var language = <%=LM.getLanguageIDByUserDTO(currentUserDTO)%>;
		if (language == 1) {
			$("#bigform").validate({
				errorClass: 'error is-invalid',
				validClass: 'is-valid',
				rules: {
					// nationalIdNo: {
					// 	digits: true
					// },
					nameBn: "required",
					permanentAddress_active: "required",
					email: "required",
					contactNumber: "required",
				},
				messages: {
					// nationalIdNo: {
					// 	digits: "Please enter digits only"
					// },
					nameBn: "Please enter name",
					permanentAddress_active: "Please enter permanent address",
					email: "Please enter email",
					contactNumber: "Please enter contact number",
				}
			});
		} else {
			$("#bigform").validate({
				errorClass: 'error is-invalid',
				validClass: 'is-valid',
				rules: {
					// nationalIdNo: {
					// 	digits: true
					// },
					nameBn: "required",
					permanentAddress_active: "required",
					email: "required",
					contactNumber: "required",
				},
				messages: {
					// nationalIdNo: {
					// 	digits: "Please enter digits only"
					// },
					nameBn: "অনুগ্রহপূর্বক নাম প্রদান করুন",
					permanentAddress_active: "অনুগ্রহপূর্বক স্থায়ী ঠিকানা প্রদান করুন",
					email: "অনুগ্রহপূর্বক ইমেইল প্রদান করুন",
					contactNumber: "অনুগ্রহপূর্বক যোগাযোগের নম্বর প্রদান করুন",
				}
			});
		}


	});

	function PreprocessBeforeSubmiting(row, validate)
	{
		$("#bigform").validate();
		if(validate == "report")
		{
		}
		else
		{
			var empty_fields = "";
			var i = 0;


			if(empty_fields != "")
			{
				if(validate == "inplaceedit")
				{
					$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
					return false;
				}
			}

		}

		preprocessGeolocationBeforeSubmitting('permanentAddress', row, false);
		preprocessGeolocationBeforeSubmitting('presentAddress', row, false);

		return true;
	}


	function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
	{
		addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Parliament_job_applicantServlet");
	}

	function init(row)
	{

		initGeoLocation('permanentAddress_geoSelectField_', row, "Parliament_job_applicantServlet");
		initGeoLocation('presentAddress_geoSelectField_', row, "Parliament_job_applicantServlet");


	}

	var row = 0;

	window.onload =function ()
	{
		init(row);
		CKEDITOR.replaceAll();
	}

	var child_table_extra_id = <%=childTableStartingID%>;

	function printSummary() {

		//console.log('vdvvbxvbxdxb');
		event.preventDefault();
		var filesColumns = document.querySelectorAll(".filesColumn");
		for (var i=0; i<filesColumns.length; i++) {
			filesColumns[i].style.display = "none";
		}
		//document.getElementById('summary_signature_id').style.width = '30%';
		document.getElementById('summary_id').style.opacity = 0.5;

		openPrintPreview();
		//document.getElementById('summary_signature_id').style.width = '65%';
		document.getElementById('summary_id').style.opacity = 1;

		for (var i=0; i<filesColumns.length; i++) {
			filesColumns[i].style.display = "";
		}

	}

	function openPrintPreview() {
		var mywindow = window.open('', 'PRINT', 'height=400,width=600');

		mywindow.document.write('<html><head><style>table, th, td { border: 1px solid black; vertical-align: top; text-align: center; border-collapse: collapse; font-size: 9px} table { page-break-inside:auto; table-layout: fixed; width: 100%; } tr { page-break-inside:avoid; page-break-after:auto } thead { display:table-header-group } tfoot { display:table-footer-group }</style><title></title>');
		mywindow.document.write('</head><body >');
		// mywindow.document.write('<h1>' + document.title  + '</h1>');

		var tempStr = "<div>" + document.getElementById('summary_id').innerHTML + "</div>";

		tempStr = tempStr.replace(/&lt;/g, "<");
		tempStr = tempStr.replace(/&gt;/g, ">");


		mywindow.document.write(tempStr);


		mywindow.document.write('</body></html>');

		mywindow.document.close(); // necessary for IE >= 10
		mywindow.focus(); // necessary for IE >= 10*/

		mywindow.print();
		mywindow.close();

		return true;
	}

</script>


<style>
	tr.noBorder td {
		border: 0;
	}
</style>



