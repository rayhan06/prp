
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="job_applicant_application.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>

<%
Job_applicant_applicationDTO job_applicant_applicationDTO;
job_applicant_applicationDTO = (Job_applicant_applicationDTO)request.getAttribute("job_applicant_applicationDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(job_applicant_applicationDTO == null)
{
	job_applicant_applicationDTO = new Job_applicant_applicationDTO();
	
}
System.out.println("job_applicant_applicationDTO = " + job_applicant_applicationDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOB_APPLICANT_APPLICATION_ADD_FORMNAME, loginDTO);


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Job_applicant_applicationServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.JOB_APPLICANT_APPLICATION_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=job_applicant_applicationDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='jobApplicantId' id = 'jobApplicantId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.jobApplicantId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='jobId' id = 'jobId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.jobId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ACCEPTANCESTATUS, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'acceptanceStatus_div_<%=i%>'>	
		<input type='text' class='form-control'  name='acceptanceStatus' id = 'acceptanceStatus_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.acceptanceStatus + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_MODIFIEDBY, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'modifiedBy_div_<%=i%>'>	
		<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.modifiedBy + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_ROLLNUMBER, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'rollNumber_div_<%=i%>'>	
		<input type='number' class='form-control'  name='rollNumber' id = 'rollNumber_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + job_applicant_applicationDTO.rollNumber + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
</div>			
				
					
	






				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">					
						<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOB_APPLICANT_APPLICATION_CANCEL_BUTTON, loginDTO)%>						
					</a>
					<button class="btn btn-success" type="submit">
					
						<%=LM.getText(LC.JOB_APPLICANT_APPLICATION_ADD_JOB_APPLICANT_APPLICATION_SUBMIT_BUTTON, loginDTO)%>						
					
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    basicInit();
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Job_applicant_applicationServlet");	
}

function init(row)
{


	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






