<%@page pageEncoding="UTF-8" %>

<%@page import="job_applicant_application.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.JOB_APPLICANT_APPLICATION_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_JOB_APPLICANT_APPLICATION;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Job_applicant_applicationDTO job_applicant_applicationDTO = (Job_applicant_applicationDTO)request.getAttribute("job_applicant_applicationDTO");
CommonDTO commonDTO = job_applicant_applicationDTO;
String servletName = "Job_applicant_applicationServlet";


System.out.println("job_applicant_applicationDTO = " + job_applicant_applicationDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Job_applicant_applicationDAO job_applicant_applicationDAO = (Job_applicant_applicationDAO)request.getAttribute("job_applicant_applicationDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

											
		
											
											<td id = '<%=i%>_jobApplicantId'>
											<%
											value = job_applicant_applicationDTO.jobApplicantId + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_jobId'>
											<%
											value = job_applicant_applicationDTO.jobId + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_acceptanceStatus'>
											<%
											value = job_applicant_applicationDTO.acceptanceStatus + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_insertionDate'>
											<%
											value = job_applicant_applicationDTO.insertionDate + "";
											%>
											<%
											String formatted_insertionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_insertionDate%>
				
			
											</td>
		
											
											<td id = '<%=i%>_insertedByUserId'>
											<%
											value = job_applicant_applicationDTO.insertedByUserId + "";
											%>
											<%
											value = WorkflowController.getNameFromUserId(job_applicant_applicationDTO.insertedByUserId, Language);
											if(value.equalsIgnoreCase(""))
											{
												value = "superman";
											}
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_modifiedBy'>
											<%
											value = job_applicant_applicationDTO.modifiedBy + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
		
											
		
											
											<td id = '<%=i%>_rollNumber'>
											<%
											value = job_applicant_applicationDTO.rollNumber + "";
											%>
														
											<%=value%>
				
			
											</td>
		
	

											<td>
												<a href='Job_applicant_applicationServlet?actionType=view&ID=<%=job_applicant_applicationDTO.iD%>'>View</a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Job_applicant_applicationServlet?actionType=getEditPage&ID=<%=job_applicant_applicationDTO.iD%>'><%=LM.getText(LC.JOB_APPLICANT_APPLICATION_SEARCH_JOB_APPLICANT_APPLICATION_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=job_applicant_applicationDTO.iD%>'/></span>
												</div>
											</td>
																						
											

