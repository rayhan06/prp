<%@page pageEncoding="UTF-8" %>
<%@ page import="util.*" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="ot_bill_submission_config.OT_bill_submission_configDAO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    boolean isLangEn = "english".equalsIgnoreCase(Language);
    String formTitle = isLangEn ? "PREPARE NOTE FOR OVERTIME BILL" : "অধিকাল ভাতার নোট প্রস্তুতি";
%>

<style>
    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }

    .no-data-found-div {
        text-align: center;
        background-color: #eee;
        padding: .875rem;
    }

    .merged-rows {
        height: 50px;
        vertical-align: middle !important;
    }

    th {
        vertical-align: top !important;
    }

    @media print {
        @page {
            size: landscape;
            margin: .25in;
        }

        table.table-bordered th,
        table.table-bordered td {
            border: 1px solid #000 !important;
            padding: 2px !important;
        }
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <div class="kt-portlet__body form-body">
            <div class="ml-auto m-3">
                <button type="button" class="btn" id='download-pdf'
                        onclick="printDivWithJqueryPrint('to-print-div');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>
            <div id="to-print-div">
                <div class="w-100">
                    <div class="w-50">
                        <div class="form-group">
                            <label class="h5" for="otBillSubmissionConfigIds">
                                <%=isLangEn ? "Bill Date" : "বিলের তারিখ"%>
                            </label>
                            <select class='form-control rounded shadow-sm'
                                    name="otBillSubmissionConfigIds"
                                    id="otBillSubmissionConfigIds"
                                    multiple="multiple"
                                    onchange="otBillSubmissionConfigIdsChanged();"
                            >
                                <%=OT_bill_submission_configDAO.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="no-data-found-div" class="no-data-found-div">
                    <%=isLangEn ? "No Data Found" : "কোন তথ্য পাওয়া যায়নি"%>
                </div>

                <div class="mt-2 w-100" id="noteSummaryDataDiv" style="display:none;">
                </div>
            </div>
        </div>

    </div>
</div>

<jsp:include page="../utility/jquery_print.jsp"/>

<script>
    const fullPageLoader = $('#full-page-loader');
    const $noteSummaryDataDiv = $('#noteSummaryDataDiv');
    const $noDataFoundDiv = $('#no-data-found-div');
    const backEndSuccessIndicator = 'data-note-summary-api-success-true';
    const otBillSubmissionConfigIdsInput = $('#otBillSubmissionConfigIds');

    async function loadAndShowNoteSummaryData(url) {
        const res = await fetch(url);
        const htmlText = await res.text();
        if (!htmlText.includes(backEndSuccessIndicator)) {
            throw new Error("back end responded with failure");
        }
        $noteSummaryDataDiv.html(htmlText);
        $noteSummaryDataDiv.show();
    }

    async function getNoteSummary(otBillSubmissionConfigIds) {
        const url = 'Overtime_allowanceServlet?actionType=ajax_prepareNote'
                    + '&otBillSubmissionConfigIds=' + otBillSubmissionConfigIds;
        fullPageLoader.show();
        $noDataFoundDiv.hide();
        $noteSummaryDataDiv.hide();
        $noteSummaryDataDiv.html('');
        try {
            await loadAndShowNoteSummaryData(url);
        } catch (e) {
            console.error(e);
            $noDataFoundDiv.html('<%=isLangEn ? "No data found due to server error" : "সার্ভারে সমস্যার জন্য কোন তথ্য পাওয়া যায়নি"%>');
            $noDataFoundDiv.show();
        }
        fullPageLoader.hide();
    }

    function otBillSubmissionConfigIdsChanged() {
        const otBillSubmissionConfigIds = otBillSubmissionConfigIdsInput.val();
        if (!Array.isArray(otBillSubmissionConfigIds) || otBillSubmissionConfigIds.length === 0) {
            $noteSummaryDataDiv.hide();
            $noteSummaryDataDiv.html('');
            $noDataFoundDiv.show();
            console.warn('mandatory param missing. not going to ajax');
            return;
        }
        getNoteSummary(otBillSubmissionConfigIds);
    }

    $(document).ready(() => {
        fullPageLoader.hide();
        select2MultiSelector("#otBillSubmissionConfigIds", '<%=Language%>');
    });
</script>