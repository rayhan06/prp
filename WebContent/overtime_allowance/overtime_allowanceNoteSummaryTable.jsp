<%@page import="budget_register.Budget_registerDTO" %>
<%@page import="util.StringUtils" %>
<%@page import="budget_mapping.Budget_mappingRepository" %>
<%@page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="overtime_bill.Overtime_billBudgetRegisterMetadata" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.*" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@page pageEncoding="UTF-8" %>


<%
    final String language = "Bangla";
    List<Budget_registerDTO> budgetRegisterDTOs = (List<Budget_registerDTO>) request.getAttribute("budgetRegisterDTOs");
    String errorMessage = null;
    if (budgetRegisterDTOs == null || budgetRegisterDTOs.isEmpty()) {
        errorMessage = "কোন তথ্য পাওয়া যায়নি";
    }
%>

<input type="hidden" data-note-summary-api-success-true name="success" value="true">

<%if (errorMessage != null) {%>
<div class="no-data-found-div">
    <%=errorMessage%>
</div>
<%
} else {
    String economicYear =
            BudgetSelectionInfoRepository
                    .getInstance()
                    .getEconomicYearById(language, budgetRegisterDTOs.get(0).budgetSelectionInfoId);
%>
<table class="table table-bordered">
    <thead>
    <tr class="text-center">
        <th>ক্রমিক নং</th>
        <th colspan="2">অর্থনৈতিক কোড</th>
        <th>
            বাজেট বরাদ্দ<br>
            (<%=economicYear%>) অর্থবছর
        </th>
        <th>মোট খরচ</th>
        <th>অবশিষ্ট</th>
        <th>বিল দাবীর মাস,কর্মকর্তা,কর্মচারীর সংখা ও বিবরণ</th>
        <th>দাবীকৃত টাকার পরিমাণ</th>
    </tr>
    </thead>
    <tbody>
    <%
        int serial = 0;
        long totalBillAmount = 0;
        int totalEmployeeCount = 0;
        TreeMap<Long, List<Budget_registerDTO>> budgetRegisterDTOBySortedBudgetMappingId =
                budgetRegisterDTOs.stream()
                                  .collect(Collectors.groupingBy(
                                          budgetRegisterDTO -> budgetRegisterDTO.budgetMappingId,
                                          TreeMap::new,
                                          Collectors.toList()
                                  ));
        for (Map.Entry<Long, List<Budget_registerDTO>> longListEntry : budgetRegisterDTOBySortedBudgetMappingId.entrySet()) {
            long budgetMappingId = longListEntry.getKey();
            List<Budget_registerDTO> budgetRegisterDTOsOfBudgetMappingId = longListEntry.getValue();
            ++serial;

            boolean isFirstRowForBudgetMappingId = true;
            int numberOfRowsForBudgetMappingId = budgetRegisterDTOsOfBudgetMappingId.size();
            TreeMap<Long, List<Budget_registerDTO>> budgetRegisterDTOBySortedEconomicSubCodeId =
                    budgetRegisterDTOsOfBudgetMappingId
                            .stream()
                            .collect(Collectors.groupingBy(
                                    budgetRegisterDTO -> budgetRegisterDTO.economicSubCodeId,
                                    () -> new TreeMap<>(BudgetUtils::compareEconomicSubCodeId),
                                    Collectors.toList()
                            ));
            for (Map.Entry<Long, List<Budget_registerDTO>> economicSubCodeIdListEntry : budgetRegisterDTOBySortedEconomicSubCodeId.entrySet()) {
                long economicSubCodeId = economicSubCodeIdListEntry.getKey();
                List<Budget_registerDTO> budgetRegisterDTOsOfEconomicSubCodeId = economicSubCodeIdListEntry.getValue();

                Budget_registerDTO latestBudgetRegisterDTO =
                        budgetRegisterDTOsOfEconomicSubCodeId
                                .stream()
                                .max(Comparator.comparingLong(budgetRegisterDTO -> budgetRegisterDTO.insertionTime))
                                .orElseThrow(NoSuchElementException::new);
                boolean isFirstRowForEconomicSubCodeId = true;
                int numberOfRowsForEconomicSubCodeId = budgetRegisterDTOsOfEconomicSubCodeId.size();
                for (Budget_registerDTO budgetRegisterDTO : budgetRegisterDTOsOfEconomicSubCodeId) {
                    totalBillAmount += budgetRegisterDTO.billAmount;
                    Overtime_billBudgetRegisterMetadata overtimeBillBudgetRegisterMetadata =
                            (Overtime_billBudgetRegisterMetadata) budgetRegisterDTO.budgetRegisterMetadata.object;

                    String monthYearStr = DateUtils.getMonthYear(overtimeBillBudgetRegisterMetadata.billStartDate, "Bangla", "/");
                    String employeeCountStr = convertToBanNumber(String.format("%02d", overtimeBillBudgetRegisterMetadata.employeeCount));
                    totalEmployeeCount += overtimeBillBudgetRegisterMetadata.employeeCount;
    %>
    <tr>
        <%if (isFirstRowForBudgetMappingId) {%>
        <td rowspan="<%=numberOfRowsForBudgetMappingId%>"
            class="merged-rows text-center"
        >
            <%=StringUtils.convertToBanNumber(String.format("%d", serial))%>
        </td>
        <td rowspan="<%=numberOfRowsForBudgetMappingId%>"
            class="merged-rows"
        >
            <%=Budget_mappingRepository.getInstance().getOperationText(language, budgetMappingId)%>
        </td>
        <%}%>
        <%if (isFirstRowForEconomicSubCodeId) {%>
        <td rowspan="<%=numberOfRowsForEconomicSubCodeId%>"
            class="merged-rows"
        >
            <%=Economic_sub_codeRepository.getInstance().getText(language, economicSubCodeId)%>
        </td>
        <td rowspan="<%=numberOfRowsForEconomicSubCodeId%>"
            class="text-right merged-rows">
            <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                    language,
                    String.format("%d", latestBudgetRegisterDTO.allocatedBudget)
            ))%>/-
        </td>
        <td rowspan="<%=numberOfRowsForEconomicSubCodeId%>"
            class="text-right merged-rows">
            <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                    language,
                    String.format("%d", latestBudgetRegisterDTO.totalCost)
            ))%>/-
        </td>
        <td rowspan="<%=numberOfRowsForEconomicSubCodeId%>"
            class="text-right merged-rows">
            <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                    language,
                    String.format("%d", latestBudgetRegisterDTO.remainingBudget)
            ))%>/-
        </td>
        <%}%>
        <td>
            <%=monthYearStr%> মাসের <%=employeeCountStr%> জনের নিয়মিত অধিকাল ভাতা
        </td>
        <td class="text-right merged-rows">
            <%=BangladeshiNumberFormatter.getFormattedNumber(convertBanglaIfLanguageIsBangla(
                    language,
                    String.format("%d", budgetRegisterDTO.billAmount)
            ))%>/-
        </td>
    </tr>
    <%
                    isFirstRowForBudgetMappingId = false;
                    isFirstRowForEconomicSubCodeId = false;
                }
            }
        }
    %>
    </tbody>
    <tfoot>
    <%
        String totalBillAmountStr = convertToBanNumber(String.format("%d", totalBillAmount));
        String totalEmployeeCountStr = convertToBanNumber(String.format("%d", totalEmployeeCount));
    %>
    <tr>
        <td colspan="6" class="text-right">
            সর্বমোট=
        </td>
        <td class="text-center">
            <%=totalEmployeeCountStr%> জন
        </td>
        <td class="text-right">
            <%=BangladeshiNumberFormatter.getFormattedNumber(totalBillAmountStr)%>/-
        </td>
    </tr>
    <tr>
        <td colspan="100%" class="text-right">
            কথায়ঃ (<%=BangladeshiNumberInWord.convertToWord(totalBillAmountStr)%>) টাকা মাত্র
        </td>
    </tr>
    </tfoot>
</table>
<%}%>
