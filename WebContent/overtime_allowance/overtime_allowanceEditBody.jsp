<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="overtime_allowance.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>
<%@ page import="ot_bill_submission_config.OT_bill_submission_configDAO" %>
<%@ page import="ot_bill_type_config.OT_bill_type_configRepository" %>

<%
    Overtime_allowanceDTO overtime_allowanceDTO;
    overtime_allowanceDTO = (Overtime_allowanceDTO) request.getAttribute("overtime_allowanceDTO");
    CommonDTO commonDTO = overtime_allowanceDTO;
    if (overtime_allowanceDTO == null) {
        overtime_allowanceDTO = new Overtime_allowanceDTO();
    }
    String tableName = "overtime_allowance";
%>

<%@include file="../pb/addInitializer.jsp" %>

<%
    actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    String formTitle = LM.getText(LC.OVERTIME_ALLOWANCE_ADD_OVERTIME_ALLOWANCE_ADD_FORMNAME, loginDTO);
    String servletName = "Overtime_allowanceServlet";
    String context = request.getContextPath() + "/";
    boolean isLangEn = "english".equalsIgnoreCase(Language);
%>

<style>
    .template-row {
        display: none;
    }

    #overtime-allowance-table tfoot {
        text-align: right;
    }

    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="overtime-allowance-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row mt-2">
                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="budgetMappingId">
                                <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                            </label>
                            <select id="budgetMappingId" name='budgetMappingId'
                                    class='form-control rounded shadow-sm'
                                    onchange="budgetMappingIdChanged(this);">
                                <%=OT_bill_type_configRepository.getInstance().buildBudgetMappingDropDown(
                                        Language,
                                        null,
                                        true
                                )%>
                            </select>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="economicSubCodeId">
                                <%=isLangEn ? "Economic Code" : "অর্থনৈতিক কোড"%>
                            </label>
                            <select class='form-control rounded shadow-sm'
                                    name='economicSubCodeId' id='economicSubCodeId'
                                    onchange="economicSubCodeIdChanged()">
                                <%--Dynamically added by ajax on budget office change--%>
                            </select>
                        </div>

                        <div class="col-lg-4 col-md-6 form-group">
                            <label class="h5" for="otBillSubmissionConfigId">
                                <%=isLangEn ? "Bill Date" : "বিলের তারিখ"%>
                            </label>
                            <select class='form-control rounded shadow-sm'
                                    name="otBillSubmissionConfigId"
                                    id="otBillSubmissionConfigId"
                                    onchange="otBillSubmissionConfigIdChanged();">
                                <%=OT_bill_submission_configDAO.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="mt-4 table-responsive">
                    <table id="overtime-allowance-table" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr class="text-center">
                            <th><%=isLangEn ? "Serial No" : "ক্রমিক নং"%>
                            </th>
                            <th><%=isLangEn ? "Bill Description" : "বিলের বিবরণ"%>
                            </th>
                            <th><%=isLangEn ? "Office/Section" : "কার্যালয়/শাখা"%>
                            </th>
                            <th><%=isLangEn ? "Bill Amount" : "টাকার পরিমাণ"%>
                            </th>
                            <th><%=isLangEn ? "View Details" : "বিস্তারিত দেখুন"%>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        </tbody>

                        <tr class="template-row">
                            <input type="hidden" name="overtimeBillId" value="-1">
                            <td class="row-data-serialNo text-center"></td>
                            <td class="row-data-billDescription"></td>
                            <td class="row-data-officeName text-center"></td>
                            <td class="row-data-billAmount text-right"></td>
                            <td class="details-button text-center">
                                <button type="button"
                                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                                        style="color: #ff6b6b;"
                                >
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="row" id="action-btn-div">
                    <div class="col-12 mt-3 text-right">
                        <button id="submit-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="submitForm('prepareBill')">
                            <%=isLangEn ? "Prepare Bill" : "বিল প্রস্তুত করুন"%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    let actionName = 'add';
    let overtimeBillSummaryModels = [];
    const otEmployeeTypeIdSelect = $('#otEmployeeTypeId');
    const $economicSubCodeIdSelect = $('#economicSubCodeId');

    const otBillSubmissionConfigIdInput = $('#otBillSubmissionConfigId');
    const budgetMappingIdSelect = document.getElementById('budgetMappingId');

    const isLangEn = <%=isLangEn%>;

    const form = $('#overtime-allowance-form');
    const fullPageLoader = $('#full-page-loader');
    const actionBtnDiv = $('#action-btn-div');

    function clearEconomicSubCode() {
        $economicSubCodeIdSelect.html('');
    }

    function clearOtBillSubmissionConfigIdInput() {
        otBillSubmissionConfigIdInput.val('');
    }

    function clearTable() {
        const tableBody = document.querySelector('#overtime-allowance-table tbody.main-tbody');
        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
        tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
                              + emptyTableText + '</td></tr>';
        actionBtnDiv.hide();
    }

    function clearNextLevels(startIndex) {
        const toClearFunctions = [
            clearEconomicSubCode, clearOtBillSubmissionConfigIdInput, clearTable
        ];
        for (let i = startIndex; i < toClearFunctions.length; i++) {
            toClearFunctions[i]();
        }
    }

    async function budgetMappingIdChanged(selectElem) {
        clearNextLevels(0);
        const budgetMappingId = selectElem.value;
        if (budgetMappingId === '') {
            return;
        }
        const url = 'OT_bill_type_configServlet?actionType=ajax_getEconomicSubCodeDropDown'
                    + '&budgetMappingId=' + budgetMappingId;
        const response = await fetch(url);
        $economicSubCodeIdSelect.html(await response.text());
    }

    function economicSubCodeIdChanged() {
        clearNextLevels(1);
    }

    function otBillSubmissionConfigIdChanged() {
        clearNextLevels(2);
        loadData();
    }

    async function loadData() {
        const budgetMappingId = budgetMappingIdSelect.value;
        const economicSubCodeId = $economicSubCodeIdSelect.val();
        const otBillSubmissionConfigId = otBillSubmissionConfigIdInput.val();

        if (budgetMappingId === '' || economicSubCodeId === '' || otBillSubmissionConfigId === '') {
            console.warn('mandatory param missing. not going to ajax');
            return;
        }

        const url = 'Overtime_allowanceServlet?actionType=ajax_getOvertimeAllowanceData'
                    + '&otBillSubmissionConfigId=' + otBillSubmissionConfigId
                    + '&budgetMappingId=' + budgetMappingId
                    + '&economicSubCodeId=' + economicSubCodeId;
        const tableBody = document.querySelector('#overtime-allowance-table tbody');
        const templateRow = document.querySelector('#overtime-allowance-table tr.template-row');

        try {
            fullPageLoader.show();
            actionBtnDiv.hide();
            setButtonDisableState(true);
            clearTable();

            const response = await fetch(url);
            const resJson = await response.json();
            if (resJson.success == null || resJson.success === false) {
                let errorMessage = isLangEn ? 'Server Error' : 'সার্ভারে সমস্যা';
                if (resJson.error != null) {
                    errorMessage = resJson.error;
                }
                tableBody.innerHTML =
                    '<tr><td colspan="100%" class="text-center" style="font-weight: bold">'
                    + errorMessage
                    + '</td></tr>';
                return;
            }
            console.log({resJson});

            overtimeBillSummaryModels = resJson.overtimeBillSummaryModels;

            if (overtimeBillSummaryModels.length === 0) {
                clearTable();
            } else {
                tableBody.innerHTML = '';
                for (let i = 0; i < overtimeBillSummaryModels.length; ++i) {
                    overtimeBillSummaryModels[i].serialNo = convertToBanglaNumIfBangla(String(i + 1), '<%=Language%>');
                    showModelInTable(tableBody, templateRow, overtimeBillSummaryModels[i]);
                }
                appendTotalRow(tableBody, resJson.billAmount);
                setButtonDisableState(false);
                actionBtnDiv.show();
            }
        } catch (error) {
            console.error(error);
        } finally {
            fullPageLoader.hide();
        }
    }

    function appendTotalRow(tbody, billTotal) {
        const tr = document.createElement('tr');
        tr.innerHTML =
            '<td colspan="3" class="text-right" style="font-weight: bold">'
            + (isLangEn ? 'Total' : 'মোট')
            + '</td>'
            + '<td class="text-right">' + billTotal + '</td>'
            + '<td></td>';
        tbody.append(tr);
    }

    function setRowData(templateRow, overtimeBillSummaryModel) {
        const rowDataPrefix = 'row-data-';
        for (const key in overtimeBillSummaryModel) {
            const td = templateRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = overtimeBillSummaryModel[key];
        }
    }

    function showModelInTable(tableBody, templateRow, overtimeBillSummaryModel) {
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        modelRow.querySelector('input[name="overtimeBillId"]').value = overtimeBillSummaryModel.overtimeBillId;
        const detailsButton = modelRow.querySelector('td.details-button button');
        detailsButton.onclick = function () {
            window.open(
                'Overtime_billServlet?actionType=view&ID=' + overtimeBillSummaryModel.overtimeBillId,
                "_blank"
            );
        }
        setRowData(modelRow, overtimeBillSummaryModel);
        tableBody.append(modelRow);
    }

    function init() {
        fullPageLoader.hide();
        actionBtnDiv.hide();
        form.validate({
            rules: {
                budgetInstitutionalGroupId: "required",
                budgetOfficeId: "required",
                otBillSubmissionConfigId: "required"
            },
            messages: {
                budgetInstitutionalGroupId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                budgetOfficeId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                otBillSubmissionConfigId: '<%=getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>'
            }
        });
    }

    function submitForm(source) {
        if (overtimeBillSummaryModels.length === 0) {
            console.warn("overtimeBillSummaryModels is empty! Unable to submit");
            return;
        }
        let message;
        if (isLangEn) {
            message = 'Overtime Bill for '
                      + overtimeBillSummaryModels.length
                      + ' offices shall be recorded in Budget Register and Bill Register. Are you sure?'
        } else {
            message = 'মোট '
                      + convertToBanglaNumIfBangla(String(overtimeBillSummaryModels.length), 'bangla')
                      + ' টি দপ্তরের অধিকাল ভাতা বাজেট রেজিস্টার ও বিল রেজিস্টারে নিবন্ধিত হতে যাচ্ছে। আপনি কি নিশ্চিত?'
        }
        const confirmButtonText = '<%=StringUtils.getYesNo(Language, true)%>';
        const cancelButtonText = '<%=StringUtils.getYesNo(Language, false)%>';
        messageDialog('', message, 'success', true, confirmButtonText, cancelButtonText,
            () => submitAfterConfirmation(source),
            () => setButtonDisableState(false)
        );
    }

    function submitAfterConfirmation(source) {
        if (!form.valid()) {
            console.warn("form not valid. not submitting");
            return;
        }
        setButtonDisableState(true);
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Overtime_allowanceServlet?actionType=ajax_" + actionName + "&source=" + source,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                fullPageLoader.hide();
                if (response.responseCode === 0) {
                    setButtonDisableState(false);
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response.responseCode === 200) {
                    window.open(getContextPath() + response.msg, "_blank");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                             + ", Message: " + errorThrown);
                setButtonDisableState(false);
                fullPageLoader.hide();
            }
        });
    }

    $(document).ready(function () {
        init();
        setButtonDisableState(false);
        fullPageLoader.hide();
    });

    function setButtonDisableState(value) {
        $('#submit-btn').prop('disabled', value);
    }
</script>






