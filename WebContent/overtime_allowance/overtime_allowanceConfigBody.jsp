<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    String context = request.getContextPath() + "/";
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;

    String formTitle = isLangEn ? "Configure Overtime Allowance" : "অধিকাল ভাতা কনফিগারেশন";

    Integer kaDailyRate = (Integer) request.getAttribute("kaDailyRate");
    if (kaDailyRate == null) {
        kaDailyRate = 0;
    }
    String ordinanceText = (String) request.getAttribute("ordinanceText");
    if (ordinanceText == null) {
        ordinanceText = "";
    }
    Integer revenueStampDeduction = (Integer) request.getAttribute("revenueStampDeduction");
    if (revenueStampDeduction == null) {
        revenueStampDeduction = 0;
    }
    Integer khaDailyRate = (Integer) request.getAttribute("khaDailyRate");
    if (khaDailyRate == null) {
        khaDailyRate = 0;
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Overtime_allowanceServlet?actionType=ajax_configure"
              id="bigform" name="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="kaDailyRate">
                                            <%=isLangEn ? "ক Daily Rate" : "ক-এর দৈনিক হার"%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control'
                                                   id="kaDailyRate"
                                                   name='kaDailyRate'
                                                   data-only-int="true"
                                                   value='<%=kaDailyRate%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="khaDailyRate">
                                            <%=isLangEn ? "খ Daily Rate" : "খ-এর দৈনিক হার"%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control'
                                                   id="khaDailyRate"
                                                   name='khaDailyRate'
                                                   data-only-int="true"
                                                   value='<%=khaDailyRate%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="revenueStampDeduction">
                                            <%=isLangEn ? "Revenue Stamp Deduction" : "রাজস্ব স্ট্যাম্প বাবদ কর্তন"%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control'
                                                   id="revenueStampDeduction"
                                                   name='revenueStampDeduction'
                                                   data-only-int="true"
                                                   value='<%=revenueStampDeduction%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="ordinanceText">
                                            <%=isLangEn ? "ordinance Description" : "আদেশের বিবরণ"%>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class="form-control"
                                                      name="ordinanceText"
                                                      id="ordinanceText"
                                                      style="resize: none"
                                                      rows="5"
                                                      maxlength="4096"
                                            ><%=ordinanceText%></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitAllowanceConfigureForm()">
                                <%=LM.getText(LC.ALLOWANCE_CONFIGURE_ADD_ALLOWANCE_CONFIGURE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    $(() => {
        document.querySelectorAll('input[data-only-int="true"]')
                .forEach(input => input.onkeydown = keyDownEvent);
    })

    function submitAllowanceConfigureForm() {
        submitAjaxForm();
    }

    function buttonStateChange(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function keyDownEvent(e) {
        return true === inputValidationForIntValue(e, $(this), <%=Integer.MAX_VALUE%>);
    }
</script>






