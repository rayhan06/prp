<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="overtime_allowance.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>
<%@ page import="ot_bill_submission_config.OT_bill_submission_configDAO" %>

<%@include file="../pb/addInitializer.jsp" %>

<%
    actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    String servletName = "Overtime_allowanceServlet";
    String context = request.getContextPath() + "/";
    boolean isLangEn = "english".equalsIgnoreCase(Language);
    String formTitle = isLangEn ? "OFFICES NOT SUBMITTED OVERTIME BILL" : "অধিকাল ভাতা জমা দেয়নি এমন অফিস";
%>

<style>
    .template-row {
        display: none;
    }

    #overtime-allowance-table tfoot {
        text-align: right;
    }

    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }

    .page {
        background: white;
        padding: .05in;
        page-break-after: always;
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="overtime-allowance-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="printDivWithJqueryPrint('to-print-div');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>
                <section class="page" id="to-print-div">
                    <div class="row">
                        <div class="col-12 row mt-2">
                            <div class="col-lg-3 col-md-6 form-group">
                                <label class="h5" for="budgetInstitutionalGroup">
                                    <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                                </label>
                                <select id="budgetInstitutionalGroup"
                                        name='budgetInstitutionalGroupId'
                                        class='form-control rounded shadow-sm'
                                        onchange="institutionalGroupChanged(this);"
                                >
                                    <%=Budget_institutional_groupRepository.getInstance().buildOptions(
                                            Language, 0L, false
                                    )%>
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-6 form-group">
                                <label class="h5" for="budgetOffice">
                                    <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                                </label>
                                <select id="budgetOffice" name='budgetOfficeId'
                                        class='form-control rounded shadow-sm'
                                        onchange="budgetOfficeChanged(this);">
                                    <%--Dynamically Added with AJAX--%>
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-6 form-group">
                                <label class="h5" for="otEmployeeTypeId">
                                    <%=isLangEn ? "Employee Type" : "কর্মকর্তা/কর্মচারীর ধরণ"%>
                                </label>
                                <select class='form-control rounded shadow-sm'
                                        name='otEmployeeTypeId' id='otEmployeeTypeId'
                                        onchange="otEmployeeTypeChanged()"
                                >
                                    <%--Dynamically Added with AJAX--%>
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-6 form-group">
                                <label class="h5" for="otBillSubmissionConfigId">
                                    <%=isLangEn ? "Bill Date" : "বিলের তারিখ"%>
                                </label>
                                <select class='form-control rounded shadow-sm'
                                        name="otBillSubmissionConfigId"
                                        id="otBillSubmissionConfigId"
                                        onchange="otBillSubmissionConfigIdChanged();">
                                    <%=OT_bill_submission_configDAO.getInstance().buildOptions(Language, null)%>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mt-4 w-100">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <%=formTitle%>
                        </h3>
                        <table id="overtime-allowance-table" class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr class="text-center">
                                <th>
                                    <%=isLangEn ? "Serial No" : "ক্রমিক নং"%>
                                </th>
                                <th>
                                    <%=isLangEn ? "Office/Section" : "কার্যালয়/শাখা"%>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="main-tbody">
                            <tr>
                                <td colspan="100%" class="text-center">
                                    কোন তথ্য পাওয়া যায়নি
                                </td>
                            </tr>
                            </tbody>

                            <%--don't put these tr inside tbody--%>
                            <tr class="loading-gif" style="display: none;">
                                <td class="text-center" colspan="100%">
                                    <img alt="" class="loading"
                                         src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                    <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
                                </td>
                            </tr>

                            <tr class="template-row">
                                <input type="hidden" name="overtimeBillId" value="-1">
                                <td class="row-data-serialNo text-center"></td>
                                <td class="row-data-officeName text-center"></td>
                            </tr>
                        </table>
                    </div>
                </section>
            </div>
        </form>
    </div>
</div>

<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<jsp:include page="../utility/jquery_print.jsp"/>

<script type="text/javascript">
    const fullPageLoader = $('#full-page-loader');
    const otEmployeeTypeIdSelect = $('#otEmployeeTypeId');
    const otBillSubmissionConfigIdInput = $('#otBillSubmissionConfigId');
    const budgetOfficeSelect = document.getElementById('budgetOffice');

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if (toShow) {
            document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
            document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
            fullPageLoader.show();
        } else fullPageLoader.hide();
    }

    function clearOffice() {
        budgetOfficeSelect.innerHTML = '';
    }

    function clearEmployeeType() {
        otEmployeeTypeIdSelect.html('');
    }

    function clearOtBillSubmissionConfigId() {
        otBillSubmissionConfigIdInput.val('');
    }

    function clearTable(hideEmptyTableText) {
        const tableBody = document.querySelector('#overtime-allowance-table tbody.main-tbody');
        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
        if (hideEmptyTableText === true) {
            tableBody.innerHTML = '';
        } else {
            tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
                                  + emptyTableText + '</td></tr>';
        }
        document.querySelectorAll('#overtime-allowance-table tfoot')
                .forEach(tfoot => tfoot.remove());
    }

    function clearNextLevels(startIndex) {
        const toClearFunctions = [
            clearOffice, clearEmployeeType, clearOtBillSubmissionConfigId, clearTable
        ];
        for (let i = startIndex; i < toClearFunctions.length; i++) {
            toClearFunctions[i]();
        }
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearNextLevels(0);
        if (selectedInstitutionalGroupId === '') return;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=false'
                    + '&budget_instituitional_group_id=' + selectedInstitutionalGroupId;

        const response = await fetch(url);
        budgetOfficeSelect.innerHTML = await response.text();
    }

    async function budgetOfficeChanged() {
        clearNextLevels(1);
        const budgetOfficeId = budgetOfficeSelect.value;
        if (budgetOfficeId === '') {
            return;
        }
        const url = 'OT_employee_typeServlet?actionType=ajax_getOptionsWithBudgetOfficeId'
                    + '&budgetOfficeId=' + budgetOfficeId;
        const response = await fetch(url);
        otEmployeeTypeIdSelect.html(await response.text());
    }

    function otEmployeeTypeChanged() {
        clearTable();
        loadData();
    }

    function otBillSubmissionConfigIdChanged() {
        clearTable();
        loadData();
    }

    async function loadData() {
        const budgetOfficeId = budgetOfficeSelect.value;
        const otEmployeeTypeId = otEmployeeTypeIdSelect.val();
        const otBillSubmissionConfigId = otBillSubmissionConfigIdInput.val();
        if (budgetOfficeId === '' || otEmployeeTypeId === '' || otBillSubmissionConfigId === '') {
            console.warn('mandatory param missing. not going to ajax');
            return;
        }
        showOrHideLoadingGif('overtime-allowance-table', true);
        clearTable();
        try {
            const url = 'Overtime_allowanceServlet?actionType=ajax_getBillNotSubmittedOffices'
                        + '&budgetOfficeId=' + budgetOfficeId
                        + '&otEmployeeTypeId=' + otEmployeeTypeId
                        + '&otBillSubmissionConfigId=' + otBillSubmissionConfigId;
            const response = await fetch(url);
            const resJson = await response.json();
            const missingOfficesName = resJson.missingOfficesName;
            const hasData = Array.isArray(missingOfficesName) && missingOfficesName.length > 0;
            if (hasData) {
                clearTable(true);
                const tableBody = document.querySelector('#overtime-allowance-table tbody');
                const templateRow = document.querySelector('#overtime-allowance-table tr.template-row');
                for (let i = 0; i < missingOfficesName.length; ++i) {
                    const missingOfficesNameModel = {
                        serialNo: convertToBanglaNumIfBangla(String(i + 1), '<%=Language%>'),
                        officeName: missingOfficesName[i]
                    }
                    showModelInTable(tableBody, templateRow, missingOfficesNameModel);
                }
            }
        } catch (error) {
            console.log(error);
        }
        showOrHideLoadingGif('overtime-allowance-table', false);
    }

    function setRowData(templateRow, overtimeBillSummaryModel) {
        console.log(overtimeBillSummaryModel);
        const rowDataPrefix = 'row-data-';
        for (const key in overtimeBillSummaryModel) {
            const td = templateRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = overtimeBillSummaryModel[key];
        }
    }

    function showModelInTable(tableBody, templateRow, overtimeBillSummaryModel) {
        console.log(overtimeBillSummaryModel);
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        setRowData(modelRow, overtimeBillSummaryModel);
        tableBody.append(modelRow);
    }

    function init() {
        fullPageLoader.hide();
    }

    $(document).ready(function () {
        init();
        fullPageLoader.hide();
    });
</script>