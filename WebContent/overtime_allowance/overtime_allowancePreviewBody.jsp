<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="util.*" %>
<%@ page import="budget_mapping.Budget_mappingDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="static util.StringUtils.convertToBanNumber" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pb.Utils" %>
<%@ page import="budget_register.Budget_registerDAO" %>
<%@ page import="budget_register.Budget_registerDTO" %>
<%@ page import="overtime_bill.Overtime_billDTO" %>
<%@ page import="overtime_bill.Overtime_billSummaryModel" %>
<%@ page import="budget.BudgetInfo" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="finance.FinanceUtil" %>
<%@ page import="bangla_date_converter.BanglaDateConverter" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="overtime_bill_finance.Overtime_bill_financeDTO" %>
<%@page pageEncoding="UTF-8" %>

<%
    List<Overtime_billDTO> billDTOs = (List<Overtime_billDTO>) request.getAttribute("billDTOs");
    Overtime_bill_financeDTO billFinanceDTO = (Overtime_bill_financeDTO) request.getAttribute("billFinanceDTO");

    String employeeCountStr = StringUtils.convertToBanNumber(String.format("%d", billFinanceDTO.totalEmployeeCount));

    List<Overtime_billSummaryModel> summaryModels =
            billDTOs.stream()
                    .sorted(Comparator.comparingInt(billDTO -> billDTO.financeSerialNumber))
                    .map(billDTO -> new Overtime_billSummaryModel(billDTO, "Bangla"))
                    .collect(Collectors.toList());

    Budget_registerDTO budgetRegisterDTO = Budget_registerDAO.getInstance().getDTOFromID(billFinanceDTO.budgetRegisterId);
    String formattedBillDate = StringUtils.getFormattedDate("bangla", budgetRegisterDTO.lastModificationTime);
    String formattedBillDateInWord = DateUtils.getDateInWord("bangla", budgetRegisterDTO.lastModificationTime);
    String formattedBillBanglaDateInWord = BanglaDateConverter.getFormattedBanglaDate(budgetRegisterDTO.lastModificationTime, true);
    String issueNumber = StringUtils.convertToBanNumber(String.format("%d", budgetRegisterDTO.iD));
    String convertedTotalAmount = convertToBanNumber(String.format("%d", budgetRegisterDTO.billAmount));
    String formattedTotalAmount = BangladeshiNumberFormatter.getFormattedNumber(convertedTotalAmount);
    String sumTotalInWord = BangladeshiNumberInWord.convertToWord(convertedTotalAmount);


    long billStartDate = billFinanceDTO.billStartDate;
    Budget_mappingDTO budgetMappingDTO = Budget_mappingRepository.getInstance().getById(budgetRegisterDTO.budgetMappingId);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getLanguage(loginDTO);

    String budgetOfficeNameBn = Budget_officeRepository.getInstance().getText(budgetRegisterDTO.budgetOfficeId, "Bangla");

    String lastTwoDigitsOfYear = StringUtils.convertToBanNumber(String.format("%d", DateUtils.getYear(billStartDate) % 100));
    String economicYearStr = convertToBanNumber(BudgetInfo.getEconomicYear(BudgetUtils.getEconomicYearBeginYear(billStartDate)));
    Employee_recordsDTO finance1HeadEmployeeRecords = FinanceUtil.getFinance1HeadEmployeeRecords();
    String finance1HeadName = "";
    String finance1HeadSignature = "";
    if(finance1HeadEmployeeRecords != null) {
        finance1HeadName = finance1HeadEmployeeRecords.nameBng;
        finance1HeadSignature = StringUtils.getBase64EncodedImageStr(finance1HeadEmployeeRecords.signature);
    }
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12pt;
    }

    #to-print-div h1 {
        font-size: 16pt;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14pt;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13pt;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        background: white;
        padding: .5in;
        margin-bottom: 5px;
        page-break-after: always;
    }

    .foot-note {
        font-size: 11pt !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 4px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }

    span.tab {
        display: inline-block;
        width: 5ch;
    }
    .signature-image {
        width: 150px !important;
        height: 50px !important;
    }
</style>

<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "অধিকাল ভাতা বিল", "Overtime Allowance Bill")%>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body page-bg" id="bill-div">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="printDivWithJqueryPrint('to-print-div');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <section class="page" data-size="A4">
                            <div class="text-center">
                                <h1>অধিকাল ভাতার বিল</h1>
                                <h2>
                                    <%=budgetRegisterDTO.description%>
                                </h2>
                                <h3>
                                    দপ্তরের
                                    নামঃ&nbsp;<%=budgetOfficeNameBn%>
                                </h3>
                                <h3>
                                    কোড নংঃ
                                    ১০২
                                    -<%=Budget_operationRepository.getInstance().getCode(budgetMappingDTO.budgetOperationId, "Bangla")%>
                                    -<%=Economic_sub_codeRepository.getInstance().getCode(budgetRegisterDTO.economicSubCodeId, "Bangla")%>
                                </h3>
                            </div>

                            <div>
                                <div class="mt-4">
                                    <div class="row">
                                        <div class="col-3 fix-fill">
                                            টোকেন নং
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            ভাউচার নং
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="col-3 fix-fill">
                                            তারিখ
                                            <div class="blank-to-fill">
                                                &nbsp;&nbsp;<%=formattedBillDate%>
                                            </div>
                                        </div>
                                    </div>

                                    <table class="table-bordered-custom mt-2 w-100">
                                        <thead>
                                        <tr>
                                            <th style="width:3%;"></th>
                                            <th style="width:37%;">নির্দেশাবলী</th>
                                            <th style="width:35%;">বিবরণ</th>
                                            <th style="width:25%;">টাকা</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="align-top"> ১</td>
                                            <td class="align-top">
                                                অবিলিকৃত/স্থ’গিত টাকা যথাযথ কলামে লাল কালিতে লিখিতে হইবে এবং যোগ দেওয়ার
                                                সময় উহা বাদ
                                                রাখিতে হইবে।
                                            </td>
                                            <td>
                                                <%=Economic_sub_codeRepository.getInstance().getText(
                                                        Language,
                                                        budgetRegisterDTO.economicSubCodeId
                                                )%>
                                            </td>
                                            <td class="text-right">
                                                <%=formattedTotalAmount%>/-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="align-top"> ২</td>
                                            <td class="align-top">
                                                বেতন বৃদ্ধিও সার্টিফিকেট বা অনুুপস্থিত কর্মচারীগণের তালিকায়ক স্থান পায়
                                                নাই এমন ঘটনাসমূহ যথা-মৃত্যু, অবসর গ্রহণ, স্থায়ী বদরী ও প্রথম নিয়োগ
                                                মন্তব্য কলামে
                                                লিখিতে
                                                হইবে।
                                            </td>
                                            <td class="text-right">
                                                <strong>
                                                    মোট দাবী (ক) =
                                                </strong>
                                            </td>
                                            <td class="text-right">
                                                <%=formattedTotalAmount%>/-
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="align-top"> ৩</td>
                                            <td class="align-top">
                                                কোন দাবিকৃত বেতন বৃদ্ধি সরকারী কর্মচারীর দক্ষতার সীমা অতিক্রম করার আওতায়
                                                পডিলে
                                                সংশ্লিষ্ট কর্মচারী উক্ত সীমা অতিক্রম করার উপযুক্ত কর্তৃপক্ষের প্রত্যায়ন
                                                দ্বারা
                                                সমর্থিত হইতে হইবে। (এস আর ১৫৬)।
                                            </td>
                                            <td class="text-center">
                                                <strong>কর্তন ও আয়ঃ</strong>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr></tr>

                                        <tr>
                                            <td class="align-top"> ৪</td>
                                            <td class="align-top">
                                                অধঃস্তন সরকারী কর্মচারী এবং এস. আর. ১৫২ তে উলি­খিত সরকারী সরকারী
                                                কর্মচারদের নাম
                                                বেতনের বিলে বাদ দেওয়া যাইতে পারে।
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td class="align-top"> ৫</td>
                                            <td class="align-top">
                                                অধঃস্তন সরকারী কর্মচারী এবং এস. আর. ১৫২ তে উলি­খিত সরকারী সরকারী
                                                কর্মচারদের নাম
                                                বেতনের বিলে বাদ দেওয়া যাইতে পারে।
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="align-top"> ৬</td>
                                            <td class="align-top">
                                                স্থায়ী পদে নিযুক্ত ব্যক্তিদের নাম স্থায়ী পদের বেতন গ্রহণের মাপ কাঠিতে
                                                জ্যেষ্ঠত্বের
                                                ক্রম অনুসারে লিখিতে হইবে এবং খালি পদসমূহ স্থানাপন্ন লোকদিগকে দেখাইতে
                                                হইবে।
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="align-top" rowspan="3"> ৭</td>
                                            <td class="align-top" rowspan="3">
                                                বেতন বিলে কর্তন ও অদায়ের পৃথক পৃথক সিডিউল বেতনের বিলে সংযুক্ত করিতে
                                                হইবে।
                                            </td>
                                            <td class="text-right">
                                                <strong>
                                                    মোট কর্তন আদায় (খ)
                                                </strong>
                                            </td>
                                            <td class="text-right"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <strong>
                                                    নীট দাবী (ক-খ)
                                                </strong>
                                            </td>
                                            <td class="text-right">
                                                <%=formattedTotalAmount%>/-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <br>
                                                প্রদানের জন্য নীট টাকার প্রয়োজন কথায়
                                                <%=BangladeshiNumberInWord.convertToWord(convertedTotalAmount)%>
                                                টাকা (মাত্র)
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <p class="mt-3">
                                ১. (ক) বিলের টাকা বুঝিয়া পাইলাম <br>
                                (খ) প্রত্যয়ন করিতেছি যে, নিম্নে বিশদভাবে বর্ণিত টাকা (যাহা এই বিল হইতে কর্তন করিয়া ফেরত
                                দেওয়া
                                হইয়াছে) ব্যতীত এই তারিখের <br> ১* মাস/২ মাস/৩ মাস পূর্বে উত্তোলিত বিলের অন্তভূক্ত টাকা
                                যথার্থ
                                ব্যক্তিদের
                                প্রদান করা হইয়াছে। <br>
                                * প্রযোজ্য ক্ষেত্রে টিক ঢিহ্ন দিন। <br>
                                (গ) প্রত্যয়ন করিতেছি যে, কর্মচারীদের নিকট হইতে অর্থ প্রাপ্তির ষ্ট্যাম্পসহ রশিদ গ্রহন
                                করিয়া বেতন সহিত
                                সংযুক্ত করা হইয়াছে।<br>
                                ২. বিলের সাথে একটি অনুপস্থিতির তালিকা প্রদান করা হইল।<br>
                                ৩. প্রত্যয়ন করা যাইতেছে যে, এই কার্যালয়ের সকল নিয়োগ, স্থায়ী ও অস্থায়ী পদোন্নতি সংক্রান্ত
                                তথ্যাদি সংশ্লিষ্ট কর্মচারীগণের নিজ নিজ চাকুরী বহিতে আমার সত্যায়নে লিপিবদ্ধ হইয়াছে।<br>
                                ৪. প্রত্যায়ন করা যাইতেছে, চাকুরী বহিতে প্রাপ্য ছুটির হিসাব এবং প্রযোজ্য ছুটির বিধি
                                অনুয়ায়ী
                                প্রাপ্য ছুটি ছাড়া কাহাকেও কোন ছুটি মঞ্জুর করা হয় নাই। আমি নিশ্চিত যে তাহাদের ছুটি পাওনা
                                ছিল এবং সকল
                                ছুটির মঞ্জুরী ও ছুটিতে বা ছুটি হইতে ফিরিয়া আসা, সাময়িক কর্মচ্যুতি ও অন্য কাজে যাওয়া ও
                                অন্যান্য ঘটনা
                                নিয়ম
                                মোতাবেক চাকুরী বহিতে এবং ছুটির হিসাবে আমার সত্যায়নে লিপিবদ্ধ করা হইযাছে।<br>
                                ৫. প্রত্যায়ন করা যাইতেছে যে, যে সকল সরকারী কর্মচারীর নাম উলে­খ করা হয় নাই, কিন্তু এই
                                বিলে
                                বেতন দাবী করা হইয়াছে। চলতি মাসে তাহারা যথার্থই সরকারী চাকুরীতে নিয়োজিত ছিলেন।<br>
                                ৬. প্রত্যায়ন করা যাইতেছে যে, যে সকল সরকারী কর্মচারীর বাড়ী ভাড়া ভাতা এই বিলে দাবী করা
                                হইয়াছে,
                                তাহারা সরকারী কোন বাসস্থানে বসবাস করেন নাই।<br>
                                ৭. প্রত্যায়ন করা যাইতেছে, যে ক্ষেত্রে ছুটির/অস্থায়ী বদলী কালীন সময়ের জন্য ক্ষতিপূরণ ভাতা
                                দাবী
                                করা হইয়াছে, সেই ক্ষেত্রে কর্মচারীর একই বা স¦পদে ফিরিয়া আসার সম্ভাব্যতা ছুটি/অস্থায়ী
                                বদলীর মূল আদেশে
                                লিপিবদ্ধ করা হইয়াছে।<br>
                                ৮. প্রত্যায়ন করা যাইতেছে যে, কর্মচারীদের ছুটি কালীন বেতন, ছুটিতে যাওয়ার সময় যে হারে বেতন
                                গ্রহণ করিতেছিলেন, সেই হাওে দাবী করা হইয়াছে।<br>
                                ৯. প্রত্যায়ন করা যাইতেছে যে, অবসর গ্রহণ করিয়াছেন এমন কোন কর্মচারীর নাম এই বিলে
                                অন্তর্ভূক্ত
                                করা হয় নাই।<br>
                            </p>
                        </section>

                        <section class="page" data-size="A4">
                            <h3 class="text-center">অনুপস্থিত ব্যক্তির ফেরত দেওয়া বেতনের বিবরণ</h3>
                            <table class="table-bordered-custom w-100">
                                <thead>
                                <tr>
                                    <th style="width: 8%;">সেকশন</th>
                                    <th style="width: 50%;">নাম</th>
                                    <th style="width: 15%;">সময়</th>
                                    <th style="width: 27%;">টাকার অংক (টা./পয়সা)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td rowspan="4"></td>
                                    <td rowspan="4"></td>
                                    <td class="text-right">
                                        বাজেটে বরাদ্দ =
                                    </td>
                                    <td class="text-right">
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                StringUtils.convertToBanNumber(
                                                        String.format("%d", budgetRegisterDTO.allocatedBudget)
                                                )
                                        )%>/-
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        মোট খরচ =
                                    </td>
                                    <td class="text-right">
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                StringUtils.convertToBanNumber(
                                                        String.format("%d", budgetRegisterDTO.totalCost)
                                                )
                                        )%>/-
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        অবশিষ্ট =
                                    </td>
                                    <td class="text-right">
                                        <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                StringUtils.convertToBanNumber(
                                                        String.format("%d", budgetRegisterDTO.remainingBudget)
                                                )
                                        )%>/-
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="row mt-4">
                                <div class="col-6">
                                    <div class="mt-1">
                                        <div class="fix-fill mt-4 w-100">
                                            স্থান
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            তারিখ
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mt-1">
                                        <div class="fix-fill mt-4 w-100">
                                            আয়ন কর্মকর্তার স্বাক্ষর
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            নাম
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-3 w-100">
                                            পদবী
                                            <div class="blank-to-fill"></div>
                                        </div>
                                        <div class="fix-fill mt-5 w-100">
                                            সীল
                                            <div class="blank-to-fill"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-3" style="border-top: 5px solid black;">
                                <div class="text-center mt-2">
                                    <h2>হিসাবরক্ষণ অফিসে ব্যবহারের জন্য</h2>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-3 fix-fill">
                                        টাকা
                                        <div class="blank-to-fill">
                                            &nbsp;&nbsp;<%=formattedTotalAmount%>/-
                                        </div>
                                    </div>
                                    <div class="col-9 fix-fill">
                                        (কথায়)
                                        <div class="blank-to-fill">
                                            &nbsp;&nbsp;<%=BangladeshiNumberInWord.convertToWord(convertedTotalAmount)%>
                                            টাকা
                                            (মাত্র)
                                        </div>
                                    </div>
                                    প্রদানের জন্য পাস কর হল
                                </div>

                                <div class="row mt-5">
                                    <div class="col-4">
                                        <div>
                                            <strong>অডিটর (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>সুপার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div>
                                            <strong>হিসাবরক্ষণ অফিসার (স্বাক্ষর)</strong>
                                        </div>
                                        <div class="fix-fill mt-5">
                                            নাম........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                        <div class="fix-fill mt-3">
                                            তাং........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>


                        <section class="page" data-size="A4">
                            <div class="text-center">
                                <h1 style="font-weight: normal">বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">অর্থ শাখা-১</h2>
                                <br>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">www.parliament.gov.bd</h2>
                            </div>

                            <div class="row mt-4">
                                <div class="col-6">
                                    নম্বর: ১১.০০.০০০০.৬৫৯.৩৩.০১৫.<%=lastTwoDigitsOfYear%>.<%=issueNumber%>
                                </div>
                                <div class="col-6" style="display: flex; justify-content: right;">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td rowspan="2">তারিখ:</td>
                                            <td style="text-align: center;"><%=formattedBillBanglaDateInWord%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; text-align: center;">
                                                <%=formattedBillDateInWord%>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="mt-4">
                                <span class="tab">প্রাপক:</span> চিফ একাউন্টস এন্ড ফিন্যান্স অফিসার<br>
                                <span class="tab"></span> বাংলাদেশ জাতীয় সংসদ সচিবালয়<br>
                                <span class="tab"></span> হিসাব ভবন, সেগুনবাগিচা<br>
                                <span class="tab"></span> ঢাকা।<br>
                            </div>

                            <div class="mt-4">
                                <span class="tab">বিষয়:</span>
                                <strong style="border-bottom: 1px solid black;">
                                    <%=budgetRegisterDTO.description%> মঞ্জুরী প্রসংগে।
                                </strong>
                            </div>

                            <div class="mt-5">
                                মহোদয়,<br>
                                <div>
                                    <span class="tab"></span>
                                    <%=budgetOfficeNameBn%>-এর কর্মকর্তা ও কর্মচারীদের সংসদ অধিবেশন, কমিটি বৈঠক এবং
                                    অন্যান্য জরুরি কাজের
                                    প্রয়োজনে কর্মরত <%=employeeCountStr%>
                                    জনের <%=billFinanceDTO.getBillDateText(false)%>
                                    তারিখের নিয়মিত/বকেয়া অধিকাল ভাতা বিল প্রদান বাবদ অনধিক <%=formattedTotalAmount%>/-
                                    (<%=sumTotalInWord%>) টাকা ব্যয়ের নিমিত্তে আমি মাননীয় স্পীকারের মঞ্জুরী করতে আদিষ্ট
                                    হয়েছি।
                                </div>


                                <div class="mt-4">
                                    <span class="tab">২।</span>
                                    উল্লিখিত ব্যয় <%=economicYearStr%> অর্থ বছরের
                                    ১০২ - বাংলাদেশ জাতীয় সংসদ,
                                    <%=Budget_institutional_groupRepository.getInstance().getText(
                                            budgetMappingDTO.budgetInstitutionalGroupId,
                                            true,
                                            "Bangla"
                                    )%>, &nbsp;
                                    <%=Budget_officeRepository.getInstance().getText(
                                            budgetMappingDTO.budgetOfficeId,
                                            true,
                                            "Bangla"
                                    )%> এর পরিচালন কোড-
                                    <%=Budget_operationRepository.getInstance().getCode(
                                            budgetMappingDTO.budgetOperationId,
                                            "Bangla"
                                    )%> এর
                                    <%=Economic_sub_codeRepository.getInstance().getText(
                                            "Bangla",
                                            budgetRegisterDTO.economicSubCodeId
                                    )%> খাত হতে বহনযোগ্য।
                                </div>

                                <div class="mt-4">
                                    <span class="tab">৩।</span> যথাযথ কর্তৃপক্ষের অনুমোদনক্রমে ব্যয়/আর্থিক মঞ্জুরী আদেশ
                                    জারী করা হল।
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-8"></div>
                                <div class="col-4 text-center">
                                    আপনার একান্ত
                                    <div>
                                        <img class="signature-image" src='<%=finance1HeadSignature%>'/>
                                    </div>
                                    <div class="mt-2">
                                        <%=finance1HeadName%><br>
                                        <%=FinanceUtil.getFinance1HeadDesignation("Bangla")%><br>
                                        ফোন: ৫৫০২৯০০৮<br>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 50px;">
                                <div class="col-6">
                                    নম্বর: ১১.০০.০০০০.৬৫৯.৩৩.০১৫.<%=lastTwoDigitsOfYear%>.<%=issueNumber%>
                                </div>
                                <div class="col-6" style="display: flex; justify-content: right;">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td rowspan="2">তারিখ:</td>
                                            <td style="text-align: center;"><%=formattedBillBanglaDateInWord%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; text-align: center;">
                                                <%=formattedBillDateInWord%>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="mt-2">
                                <%
                                    String psSecretary = OfficeUnitOrganogramsRepository.getInstance().getDesignation("bangla", 264000);
                                    String psFAndPrDesignation = OfficeUnitOrganogramsRepository.getInstance().getDesignation("bangla", 261765);
                                    String additionalSecretaryFAndPrDesignation = OfficeUnitOrganogramsRepository.getInstance().getDesignation("bangla", 261764);
                                %>
                                <div>
                                    অনুলিপি: সদয় জ্ঞাতার্থে
                                </div>
                                <div>
                                    <span class="tab">১।</span>সচিব মহোদয়ের <%=psSecretary%>, বাংলাদেশ জাতীয় সংসদ সচিবালয়, ঢাকা
                                </div>
                                <div>
                                    <span class="tab">২।</span><%=psFAndPrDesignation%>, <%=additionalSecretaryFAndPrDesignation%> (এফএন্ডপিআর), বাংলাদেশ জাতীয় সংসদ সচিবালয়, ঢাকা
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-8"></div>
                                <div class="col-4 text-center">
                                    <div>
                                        <img class="signature-image" src='<%=finance1HeadSignature%>'/>
                                    </div>
                                    <div class="mt-2">
                                        <%=finance1HeadName%><br>
                                        <%=FinanceUtil.getFinance1HeadDesignation("Bangla")%><br>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <%
                            long totalAmountRunningTotal = 0;
                            boolean isLastPage = false;
                            final int rowsPerPage = 20;
                            int index = 0;
                            while (index < summaryModels.size()) {
                                boolean isFirstPage = (index == 0);
                        %>
                        <section class="page" data-size="A4">
                            <%if (isFirstPage) {%>
                            <div class="text-center">
                                <h1 style="font-weight: normal">বাংলাদেশ জাতীয় সংসদ সচিবালয়</h1>
                                <h2 style="display: inline-block; border-bottom: 1px solid black">অর্থ শাখা-১</h2>
                                <h2 style="font-weight: normal">
                                    <%=budgetRegisterDTO.description%>
                                </h2>
                            </div>
                            <%}%>

                            <div>
                                <div class="mt-4">
                                    <table class="table-bordered-custom mt-2 w-100">
                                        <thead>
                                        <tr style="height: 50px;background-color:lightgrey">
                                            <th style="width:5%;">ক্র. নং</th>
                                            <th style="width:25%;">বিলের বিবরণ</th>
                                            <th style="width:15%;">কার্যালয়</th>
                                            <th style="width:10%;">টাকার পরিমাণ</th>
                                            <th style="width:10%;">মন্তব্য</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <%if (!isFirstPage) {%>
                                        <tr>
                                            <td colspan="3" class="text-right">
                                                পূর্ব পৃষ্ঠার জের=
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                                )%>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <%}%>

                                        <%
                                            int rowsInThisPage = 0;
                                            while (index < summaryModels.size() && rowsInThisPage < rowsPerPage) {
                                                isLastPage = (index == (summaryModels.size() - 1));
                                                rowsInThisPage++;
                                                Overtime_billSummaryModel model = summaryModels.get(index++);
                                                totalAmountRunningTotal += model.billAmountLong;
                                        %>
                                        <tr style="height: 40px">
                                            <td class="text-center">
                                                <%=Utils.getDigits(model.financeSerialNumber, "BANGLA")%>
                                            </td>
                                            <td class="align-top">
                                                <%=model.getBillDescriptionHtml()%>
                                            </td>
                                            <td style="text-align: center">
                                                <%=model.officeName%>
                                            </td>
                                            <td class="align-top text-right">
                                                <%=model.billAmount%>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="3" class="text-right">
                                                <%=isLastPage ? "সর্বমোট" : "উপমোট"%>=
                                            </td>
                                            <td class="text-right">
                                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                        StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                                )%>
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    <%if (isLastPage) {%>
                                    <div class="mt-2 offset-4 ml-auto text-center">
                                        কথায়:
                                        <strong><%=BangladeshiNumberInWord.convertToWord(Utils.getDigits(totalAmountRunningTotal, "Bangla"))%>
                                        </strong> টাকা মাত্র
                                    </div>
                                    <%}%>
                                </div>
                            </div>
                        </section>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../utility/jquery_print.jsp"/>