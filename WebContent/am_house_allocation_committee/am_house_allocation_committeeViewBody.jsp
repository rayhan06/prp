

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_house_allocation_committee.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Am_house_allocation_committeeServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Am_house_allocation_committeeDTO am_house_allocation_committeeDTO = Am_house_allocation_committeeDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = am_house_allocation_committeeDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_ISACTIVE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.isActive + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANORGID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOrgId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANEMPID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanEmpId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANPHONENUM, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICENAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICENAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITORGNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITORGNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYORGID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOrgId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYEMPID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryEmpId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYPHONENUM, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICENAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICENAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITORGNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITORGNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_MODIFIEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = am_house_allocation_committeeDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>