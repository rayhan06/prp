
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_house_allocation_committee.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navAM_HOUSE_ALLOCATION_COMMITTEE";
String servletName = "Am_house_allocation_committeeServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_ISACTIVE, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANORGID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANEMPID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANPHONENUM, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICENAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICENAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITORGNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_CHAIRMANOFFICEUNITORGNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYORGID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYEMPID, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYPHONENUM, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICENAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICENAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITORGNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_SECRETARYOFFICEUNITORGNAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_MODIFIEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_SEARCH_AM_HOUSE_ALLOCATION_COMMITTEE_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Am_house_allocation_committeeDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Am_house_allocation_committeeDTO am_house_allocation_committeeDTO = (Am_house_allocation_committeeDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.isActive + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOrgId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanEmpId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOrgId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryEmpId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryPhoneNum + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
											<td>
											<%
											value = am_house_allocation_committeeDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
	
											<%CommonDTO commonDTO = am_house_allocation_committeeDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=am_house_allocation_committeeDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			