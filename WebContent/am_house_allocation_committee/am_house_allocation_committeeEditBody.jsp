<%@page import="login.LoginDTO"%>
<%@page import="am_house_allocation_committee.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="util.*"%>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="am_house_allocation_committee_member.Am_house_allocation_committee_memberDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="user.UserDTO" %>
<%@ page import="am_house_allocation_committee_member.Am_house_allocation_committee_memberDAO" %>

<%
    Am_house_allocation_committeeDTO am_house_allocation_committeeDTO;
    String actionName = "";
    List<Am_house_allocation_committeeDTO> activeDTOs = Am_house_allocation_committeeDAO.getInstance().getAllActiveCommittee();
    if(activeDTOs.size() > 0)
    {
        am_house_allocation_committeeDTO = activeDTOs.get(0);
        actionName = "edit";
    } else {
        am_house_allocation_committeeDTO = new Am_house_allocation_committeeDTO();
        actionName = "add";
    }

    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;



    String formTitle = LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_ADD_FORMNAME, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    List<Am_house_allocation_committee_memberDTO> memberDTOS = null;
    if (actionName.equalsIgnoreCase("edit")) {
        memberDTOS = Am_house_allocation_committee_memberDAO.getInstance().
                getDTOsByCommitteeId(am_house_allocation_committeeDTO.iD);
    }

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform" >
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "সভাপতি", "Chairman")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="hidden" class='form-control' name='chairmanId'
                                                   id='chairmanId'
                                                   value=''>
                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tagChairman_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <thead></thead>

                                                <tbody id="chairman_table">
                                                <tr style="display: none;">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type="button"                                                                 class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                onclick="remove_containing_row(this,'chairman_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>

                                                <% if (actionName.equals("edit")) { %>
                                                <tr>
                                                    <td><%=Employee_recordsRepository.getInstance()
                                                            .getById(am_house_allocation_committeeDTO.chairmanEmpId).employeeNumber%>
                                                    </td>
                                                    <td>
                                                        <%=isLanguageEnglish ? (am_house_allocation_committeeDTO.chairmanNameEn)
                                                                : (am_house_allocation_committeeDTO.chairmanNameBn)%>
                                                    </td>

                                                    <%
                                                        String postName = isLanguageEnglish ? (am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameEn +
                                                                ", " + am_house_allocation_committeeDTO.chairmanOfficeUnitNameEn)
                                                                : (am_house_allocation_committeeDTO.chairmanOfficeUnitOrgNameBn + ", "
                                                                + am_house_allocation_committeeDTO.chairmanOfficeUnitNameBn);
                                                    %>

                                                    <td><%=postName%>
                                                    </td>
                                                    <td id='<%=am_house_allocation_committeeDTO.chairmanEmpId%>_td_button'>
                                                        <button type="button"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                onclick="remove_containing_row(this,'chairman_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%}%>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "সদস্য সচিব", "Member Secretary")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="hidden" class='form-control' name='secId'
                                                   id='secId'
                                                   value=''>
                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tagSec_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <thead></thead>

                                                <tbody id="sec_table">
                                                <tr style="display: none;">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type="button"                                                                 class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                onclick="remove_containing_row(this,'sec_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>

                                                <% if (actionName.equals("edit")) { %>
                                                <tr>
                                                    <td><%=Employee_recordsRepository.getInstance()
                                                            .getById(am_house_allocation_committeeDTO.secretaryEmpId).employeeNumber%>
                                                    </td>
                                                    <td>
                                                        <%=isLanguageEnglish ? (am_house_allocation_committeeDTO.secretaryNameEn)
                                                                : (am_house_allocation_committeeDTO.secretaryNameBn)%>
                                                    </td>

                                                    <%
                                                        String postName = isLanguageEnglish ? (am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameEn +
                                                                ", " + am_house_allocation_committeeDTO.secretaryOfficeNameEn)
                                                                : (am_house_allocation_committeeDTO.secretaryOfficeUnitOrgNameBn + ", "
                                                                + am_house_allocation_committeeDTO.secretaryOfficeUnitNameBn);
                                                    %>

                                                    <td><%=postName%>
                                                    </td>
                                                    <td id='<%=am_house_allocation_committeeDTO.secretaryEmpId%>_td_button'>
                                                        <button type="button"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                onclick="remove_containing_row(this,'sec_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%}%>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "সদস্যগণ ", "Members")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="hidden" class='form-control' name='memberId'
                                                   id='memberId'
                                                   value=''>
                                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tag_member_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <thead></thead>

                                                <tbody id="tagged_member_table">
                                                <tr style="display: none;">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <button type="button"                                                                 class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                onclick="remove_containing_row(this,'tagged_member_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>

                                                <% if (memberDTOS != null) { %>
                                                <%for (Am_house_allocation_committee_memberDTO memberDTO : memberDTOS) {%>
                                                <tr>
                                                    <td><%=Employee_recordsRepository.getInstance()
                                                            .getById(memberDTO.memberEmpId).employeeNumber%>
                                                    </td>
                                                    <td>
                                                        <%=isLanguageEnglish ? (memberDTO.memberNameEn)
                                                                : (memberDTO.memberNameBn)%>
                                                    </td>

                                                    <%
                                                        String postName = isLanguageEnglish ? (memberDTO.memberOfficeUnitOrgNameEn +
                                                                ", " + memberDTO.memberOfficeUnitNameEn)
                                                                : (memberDTO.memberOfficeUnitOrgNameBn + ", " + memberDTO.memberOfficeUnitNameBn);
                                                    %>

                                                    <td><%=postName%>
                                                    </td>
                                                    <td id='<%=memberDTO.memberEmpId%>_td_button'>
                                                        <button type="button"
                                                                class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4 pb-2"
                                                                onclick="remove_containing_row(this,'tagged_member_table');">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <%}%>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>


					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id = "submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="submit" onclick="submitForm()">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_COMMITTEE_ADD_AM_HOUSE_ALLOCATION_COMMITTEE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

const comForm = $("#bigform");
const lang = '<%=Language%>';

function valueByLanguage(lang, bnValue, enValue) {
    if (lang === 'english' || lang === 'English') {
        return enValue;
    } else {
        return bnValue;
    }
}

function PreprocessBeforeSubmiting()
{
    let data = added_chairman_info_map.keys().next();
    if (!(data && data.value)) {
        let errMsg = valueByLanguage(lang, "সভাপতি প্রয়োজনীয়", "Please Select Chairman");
        toastr.error(errMsg);
        return false;
    }

    data = added_sec_info_map.keys().next();
    if (!(data && data.value)) {
        let errMsg = valueByLanguage(lang, "সদস্য সচিব প্রয়োজনীয়", "Please Select Member Secretary");
        toastr.error(errMsg);
        return false;
    }

    if(added_member_info_map.size < 3){
        let errMsg = valueByLanguage(lang, "নূন্যতম ৩ জন সদস্য প্রয়োজনীয় ", "Minimum 3 members needed.");
        toastr.error(errMsg);
        return false;
    }

    document.getElementById('chairmanId').value = JSON.stringify(
        Array.from(added_chairman_info_map.values()));

    document.getElementById('secId').value = JSON.stringify(
        Array.from(added_sec_info_map.values()));

    document.getElementById('memberId').value = JSON.stringify(
        Array.from(added_member_info_map.values()));

    return true;
}

function buttonStateChange(value){
    $('#submit-btn').prop('disabled',value);
    $('#cancel-btn').prop('disabled',value);
}

function submitForm(){
    event.preventDefault();
    buttonStateChange(true);
    if(PreprocessBeforeSubmiting()){
        $.ajax({
            type : "POST",
            url : "Am_house_allocation_committeeServlet?actionType=ajax_add",
            data : comForm.serialize(),
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    buttonStateChange(false);
                }else if(response.responseCode === 200){
                    let successMsg = valueByLanguage(lang, "সফলভাবে সংরক্ষিত হয়েছে ", "Successfully Saved");
                    showToastSticky(successMsg,successMsg);
                    setTimeout(() => {
                        window.location.replace(getContextPath()+response.msg);
                    }, 3000);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }else{
        buttonStateChange(false);
    }
}



$(document).ready(function(){
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	});
});



//

<%
List<EmployeeSearchIds> addedChairmanIdsList = null;
if(actionName.equals("edit")) {
    addedChairmanIdsList = Arrays.asList(new EmployeeSearchIds(am_house_allocation_committeeDTO.chairmanEmpId,
    am_house_allocation_committeeDTO.chairmanOfficeUnitId,am_house_allocation_committeeDTO.chairmanOrgId));
}
%>

let added_chairman_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedChairmanIdsList)%>);

<%
List<EmployeeSearchIds> addedSecIdsList = null;
if(actionName.equals("edit")) {
    addedSecIdsList = Arrays.asList(new EmployeeSearchIds(am_house_allocation_committeeDTO.secretaryEmpId,
    am_house_allocation_committeeDTO.secretaryOfficeUnitId,am_house_allocation_committeeDTO.secretaryOrgId));
}
%>

let added_sec_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedSecIdsList)%>);


<%
List<EmployeeSearchIds> addedMembersList = null;
        if(memberDTOS != null){
            addedMembersList = memberDTOS.stream()
                                                   .map(dto -> new EmployeeSearchIds(dto.memberEmpId,dto.memberOfficeUnitId,dto.memberOrgId))
                                                   .collect(Collectors.toList());
        }
%>
let added_member_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedMembersList)%>);


/* IMPORTANT
 * This map is converts table name to the table's added employees map
 */
table_name_to_collcetion_map = new Map(
    [
        ['chairman_table', {
            info_map: added_chairman_info_map,
            isSingleEntry: true
        }],
        ['sec_table', {
            info_map: added_sec_info_map,
            isSingleEntry: true
        }],
        ['tagged_member_table', {
            info_map: added_member_info_map,
            isSingleEntry: false
        }],

    ]
);

// modal row button desatination table in the page
modal_button_dest_table = 'none';

// modal trigger button
$('#tagChairman_modal_button').on('click', function () {
    modal_button_dest_table = 'chairman_table';
    $('#search_emp_modal').modal();
});

// modal trigger button
$('#tagSec_modal_button').on('click', function () {
    modal_button_dest_table = 'sec_table';
    $('#search_emp_modal').modal();
});

$('#tag_member_modal_button').on('click', function () {
    // alert('CLICKED');
    modal_button_dest_table = 'tagged_member_table';
    $('#search_emp_modal').modal();
});




</script>

<style>
    .required{
        color: red;
    }
</style>






