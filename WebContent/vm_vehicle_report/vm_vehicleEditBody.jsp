<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_vehicle.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);


    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_VEHICLE_ADD_VM_VEHICLE_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_vehicleServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_VEHICLE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
//            fiscal_yearDAO.getAllFiscal_year(true);


//	System.out.println(Vm_vehicleDAO.getStatus(0, "english"));


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(Language, "গাড়ির রিপোর্ট", "Vehicle Report")%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action=""
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=UtilCharacter.getDataByLanguage(Language, "গাড়ির রিপোর্ট", "Vehicle Report")%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_SELECT_FISCAL_YEAR, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <select class='form-control' name='fiscalYearId'
                                                        id='fiscalYearId_hidden_<%=i%>' tag='pb_html' required>
                                                    <%
                                                        //														String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                        StringBuilder selectOption = new StringBuilder();
                                                        selectOption.append("<option value = '-100'>");
                                                        if (isLanguageEnglish) {
                                                            selectOption.append("All</option>");
                                                        } else {
                                                            selectOption.append("সকল </option>");
                                                        }
                                                        String fiscalYearOptions = selectOption.toString();

                                                        StringBuilder option = new StringBuilder();
                                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {
                                                            //System.out.println("fiscal dto id: "+fiscal_yearDTO.id);
                                                            option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                        }
                                                        fiscalYearOptions += option.toString();
                                                    %>
                                                    <%=fiscalYearOptions%>
                                                </select>

                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLETYPECAT, loginDTO)%>
                                                <%--												<span class="required"> * </span>--%>
                                            </label>
                                            <div class="col-md-9">
                                                <select class='form-control' name='vehicleTypeCat'
                                                        id='vehicleTypeCat_category_<%=i%>' tag='pb_html'
                                                        onchange="loadVehicleList()">
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptions("vehicle_type", Language, -1);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group row" id="vehicleIdDiv">
                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEID, loginDTO)%>
                                                <%--												<span class="required"> * </span>--%>
                                            </label>
                                            <div class="col-md-9">
                                                <select class='form-control' name='vehicleId'
                                                        id='givenVehicleId_select_<%=i%>' tag='pb_html'
                                                        onchange="vehicleSelect()">
                                                </select>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
					<div class="col-md-10 form-actions text-right mt-3">
						<button onclick="loadData()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
								type="submit">
							<%=UtilCharacter.getDataByLanguage(Language, "লোড", "Load")%>
						</button>
					</div>
				</div>
                <div id="data-div" class="  mb-4" style="display: none">
                    <div class="kt-portlet"
                         id="vehicle-info" data-ktportlet="true">
                        <%-- this kt-portlet__head  class should be removed--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class=" table-title">
                                    <%= UtilCharacter.getDataByLanguage(Language, "গাড়ির বিবরণ ও ক্রয়মূল্য",
                                            "Vehicle Description and Purchase Price") %>
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-group">
                                    <a href="#" data-ktportlet-tool="toggle"
                                       class="btn btn-sm btn-icon btn-default btn-icon-md"
                                       aria-describedby="tooltip_orzs7rbf8d"><i
                                            class="la la-angle-down"></i></a>
                                    <div class="tooltip tooltip-portlet tooltip bs-tooltip-top"
                                         role="tooltip" id="tooltip_orzs7rbf8d"
                                         aria-hidden="true" x-placement="top"
                                         style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                                        <div class="tooltip-arrow arrow"
                                             style="left: 34px;"></div>
                                        <div class="tooltip-inner">Collapse</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <%-- this kt-portlet__body  class should be removed--%>
                        <div class="kt-portlet__body" style="" kt-hidden-height="226">
                            <div class="kt-portlet__content">
                                <div class="table-responsive">
                                    <table class="table table-bordered ">
                                        <tr>
                                            <td style="width:30%"><b><%=LM.getText(LC.VM_VEHICLE_ADD_REGNO, loginDTO)%>
                                            </b></td>

                                            <td id="regNumber"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_CHASISNO, loginDTO)%>
                                                </b></td>
                                            <td id="chasisNumber"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_ENGINENO, loginDTO)%>
                                                </b></td>
                                            <td id="engineNumber"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_SUPPLIERTYPE, loginDTO)%>
                                                </b></td>
                                            <td id="supplierName"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_MANUFACTUREYEAR, loginDTO)%>
                                                </b></td>
                                            <td id="manuYear"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_PURCHASEAMOUNT, loginDTO)%>
                                                </b></td>
                                            <td id="purAmount"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:30%">
                                                <b><%=LM.getText(LC.VM_VEHICLE_ADD_PURCHASEDATE, loginDTO)%>
                                                </b></td>
                                            <td id="purDate"></td>
                                        </tr>
                                    </table>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="kt-portlet kt-portlet--transparent kt-portlet--collapsed"
                         id="vehicle-maintenance" data-ktportlet="true">
                        <%-- this kt-portlet__head  class should be removed--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class=" table-title">
                                    <%= UtilCharacter.getDataByLanguage(Language, "মোট মেরামত ও রক্ষনাবেক্ষন ব্যায় ",
                                            "Total Maintenance and Repair Cost") %>
                                    <label id="totalMaintenanceCost"></label>
                                </h3>

                            </div>
                            <%-- this kt-portlet__head-toolbar div should be removed fully--%>

                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-group">
                                    <a href="#" data-ktportlet-tool="toggle"
                                       class="btn btn-sm btn-icon btn-default btn-icon-md"
                                       aria-describedby="tooltip_orzs7rbf8d"><i
                                            class="la la-angle-down"></i></a>
                                    <div class="tooltip tooltip-portlet tooltip bs-tooltip-top"
                                         role="tooltip" id=""
                                         aria-hidden="true" x-placement="top"
                                         style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                                        <div class="tooltip-arrow arrow"
                                             style="left: 34px;"></div>
                                        <div class="tooltip-inner">Collapse</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <%-- this kt-portlet__body  class should be removed--%>
                        <div class="kt-portlet__body" style="" kt-hidden-height="226">

                            <div class="kt-portlet__content">
                                <div class="table-responsive">
                                    <h5 class="table-title">
                                        <%=UtilCharacter.getDataByLanguage(Language, "রক্ষনাবেক্ষন ", "Maintenance")%>
                                    </h5>
                                    <table id='maintenance-items-table' class="table table-bordered table-striped">
                                    </table>
                                </div>

                                <div class="table-responsive">
                                    <h5 class="table-title">
                                        <%=UtilCharacter.getDataByLanguage(Language, "মেরামত ", "Repair")%>
                                    </h5>
                                    <table id='repair-items-table' class="table table-bordered table-striped">
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="kt-portlet kt-portlet--transparent kt-portlet--collapsed"
                         id="vehicle-taxToken" data-ktportlet="true">
                        <%-- this kt-portlet__head  class should be removed--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class=" table-title">
                                    <%= LM.getText(LC.VM_TAX_TOKEN_TAX_TOKEN_AND_FITNESS_FEES, loginDTO) %>
                                    <label id="totalTaxTokenCost"></label>
                                </h3>

                            </div>
                            <%-- this kt-portlet__head-toolbar div should be removed fully--%>

                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-group">
                                    <a href="#" data-ktportlet-tool="toggle"
                                       class="btn btn-sm btn-icon btn-default btn-icon-md"
                                       aria-describedby="tooltip_orzs7rbf8d"><i
                                            class="la la-angle-down"></i></a>
                                    <div class="tooltip tooltip-portlet tooltip bs-tooltip-top"
                                         role="tooltip" id=""
                                         aria-hidden="true" x-placement="top"
                                         style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                                        <div class="tooltip-arrow arrow"
                                             style="left: 34px;"></div>
                                        <div class="tooltip-inner">Collapse</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <%-- this kt-portlet__body  class should be removed--%>
                        <div class="kt-portlet__body" style="" kt-hidden-height="226">

                            <div class="kt-portlet__content">
                                <div class="table-responsive">
                                    <%--									<h5 class="table-title">--%>
                                    <%--										<%=UtilCharacter.getDataByLanguage(Language, "রক্ষনাবেক্ষন ", "Maintenance")%>--%>
                                    <%--									</h5>--%>
                                    <table id='taxToken-items-table' class="table table-bordered table-striped">
                                    </table>
                                </div>

                                <%--								<div class="table-responsive">--%>
                                <%--									<h5 class="table-title">--%>
                                <%--										<%=UtilCharacter.getDataByLanguage(Language, "মেরামত ", "Repair")%>--%>
                                <%--									</h5>--%>
                                <%--									<table id = 'repair-items-table2' class="table table-bordered table-striped">--%>
                                <%--									</table>--%>
                                <%--								</div>--%>
                            </div>

                        </div>

                    </div>


                    <div class="kt-portlet kt-portlet--transparent kt-portlet--collapsed"
                         id="vehicle-fuel-request" data-ktportlet="true">
                        <%-- this kt-portlet__head  class should be removed--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class=" table-title">
                                    <%= UtilCharacter.getDataByLanguage(Language, "পে: অয়েল & লুব্রিক্যান্ট ব্যায় ",
                                            "Pay: Oil & Lubricant Cost") %>
                                    <label id="totalFuelRequestCost"></label>
                                </h3>

                            </div>
                            <%-- this kt-portlet__head-toolbar div should be removed fully--%>

                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-group">
                                    <a href="#" data-ktportlet-tool="toggle"
                                       class="btn btn-sm btn-icon btn-default btn-icon-md"
                                       aria-describedby="tooltip_orzs7rbf8d"><i
                                            class="la la-angle-down"></i></a>
                                    <div class="tooltip tooltip-portlet tooltip bs-tooltip-top"
                                         role="tooltip" id=""
                                         aria-hidden="true" x-placement="top"
                                         style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                                        <div class="tooltip-arrow arrow"
                                             style="left: 34px;"></div>
                                        <div class="tooltip-inner">Collapse</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <%-- this kt-portlet__body  class should be removed--%>
                        <div class="kt-portlet__body" style="" kt-hidden-height="226">

                            <div class="kt-portlet__content">
                                <div class="table-responsive">
                                    <table id='fuel-request-items-table' class="table table-bordered table-striped">
                                    </table>
                                </div>
                            </div>

                        </div>

                    </div>


                    <div class="kt-portlet kt-portlet--transparent kt-portlet--collapsed"
                         id="total-request" data-ktportlet="true">
                        <%-- this kt-portlet__head  class should be removed--%>
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class=" table-title">
                                    <%= UtilCharacter.getDataByLanguage(Language, "মোট ব্যায় ",
                                            "Total Cost = ") %>
                                    <label id="totalCost"></label>
                                </h3>

                            </div>
                            <%-- this kt-portlet__head-toolbar div should be removed fully--%>

                        </div>
                        <%-- this kt-portlet__body  class should be removed--%>

                    </div>


                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    var totalMaintenanceCost = 0;
    var totalTaxTokenCost = 0;
    var totalFuelRequestCost = 0;

    $(document).ready(function () {
        select2MultiSelector("#fiscalYearId_hidden_0", '<%=Language%>');
    });

    function valueByLanguage(language, bnValue, enValue) {
        if (language == 'english' || language == 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }

    function loadVehicleList() {

        let typeValue = document.getElementById('vehicleTypeCat_category_0').value;
        let url = "Vm_vehicleServlet?actionType=getByVehicleTypeForAssignment&type=" + typeValue;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // const response = JSON.parse(fetchedData);
                $("#givenVehicleId_select_0").html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }

    function validation() {
        let vehicleId = $("#givenVehicleId_select_0").val();
        if (!vehicleId || vehicleId.length == 0) {
            toastr.error("Please select vehicle");
            return false;
        }

        let fiscalYearIds = $("#fiscalYearId_hidden_0").val();
        if (!fiscalYearIds || fiscalYearIds.length == 0) {
            toastr.error("Please select fiscal year");
            return false;
        }
        // console.log(fiscalYearIds);
        // console.log(fiscalYearIds.length);
        return true;
    }

    function loadData() {

        event.preventDefault();
        if (validation()) {
            // console.log("Load Data")
            let vehicleId = $("#givenVehicleId_select_0").val();
            let fiscalYearIds = $("#fiscalYearId_hidden_0").val();
            let url = "Vm_vehicleServlet?actionType=getReportData&vehicleId=" + vehicleId + "&language="
                + '<%=Language%>' + "&fiscalYearIds=" + fiscalYearIds;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    // console.log(fetchedData);
                    if (fetchedData && fetchedData.vm_vehicleDTO) {
                        vehicleDataSet(fetchedData.vm_vehicleDTO);
                        setMaintenanceData(fetchedData);
                        setTaxTokenFeesData(fetchedData);
                        setTotalCost();
                    }
                    $('#data-div').show();

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        if (validation()) {
            console.log("Load Data")
            let vehicleId = $("#givenVehicleId_select_0").val();
            let fiscalYearIds = $("#fiscalYearId_hidden_0").val();
            let url = "Vm_fuel_requestServlet?actionType=getReportDataFuelRequest&vehicleId=" + vehicleId + "&language="
                + '<%=Language%>' + "&fiscalYearIds=" + fiscalYearIds;
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    console.log(fetchedData);
                    if (fetchedData && fetchedData.vm_vehicleDTO) {
                        setFuelRequestData(fetchedData);
                        setTotalCost();
                    }
                    // $('#data-div').show();

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

    }

    function setTotalCost() {
        var totalCost = totalMaintenanceCost + totalTaxTokenCost + totalFuelRequestCost + valueByLanguage('<%=Language%>', " টাকা", " Taka");
        $("#totalCost").text(totalCost);
    }

    function vehicleDataSet(vehicle) {
        $("#regNumber").text(vehicle.regNo);
        $("#chasisNumber").text(vehicle.chasisNo);
        $("#engineNumber").text(vehicle.engineNo);
        $("#supplierName").text(vehicle.supplierName);
        $("#manuYear").text(vehicle.manufactureYear);
        $("#purAmount").text(vehicle.purchaseAmount);
        $("#purDate").text(vehicle.purchaseDateInString);

        let personPortlet = new KTPortlet('vehicle-info');
        personPortlet.expand();

    }


    function setMaintenanceData(data) {
        $("#totalMaintenanceCost").text(data.totalMaintenanceCost + valueByLanguage('<%=Language%>', " টাকা", " Taka"));
        totalMaintenanceCost = data.totalMaintenanceCost;

        let mData = data.maintenanceDTOListForMaintenance;
        let rData = data.maintenanceDTOListForRepair;

        let mHtml = "<tr>" +
            "<th>" +
            valueByLanguage('<%=Language%>', "তারিখ", "Date") +
            "</th>" +
            "<th>" +
            valueByLanguage('<%=Language%>', "ব্যায়(টাকা)", "Expense(Taka)") +
            "</th>\n" +
            "</tr>";

        let rHtml = mHtml;


        if (mData && mData.length > 0) {
            for (var i = 0; i < mData.length; i++) {
                let dValue = "<tr>" +
                    "<td>" +
                    mData[i].date +
                    "</td>" +
                    "<td>" +
                    mData[i].amount +
                    "</td>\n" +
                    "</tr>";
                mHtml += dValue;
            }
        }

        if (rData && rData.length > 0) {
            for (var i = 0; i < rData.length; i++) {
                let dValue = "<tr>" +
                    "<td>" +
                    rData[i].date +
                    "</td>" +
                    "<td>" +
                    rData[i].amount +
                    "</td>\n" +
                    "</tr>";
                rHtml += dValue;
            }
        }

        $('#maintenance-items-table').html(mHtml);
        $('#repair-items-table').html(rHtml);

    }

    function setFuelRequestData(data) {
        $("#totalFuelRequestCost").text(data.totalMaintenanceCost + valueByLanguage('<%=Language%>', " টাকা", " Taka"));
        totalFuelRequestCost = data.totalMaintenanceCost;

        let mData = data.maintenanceDTOListForMaintenance;

        let mHtml = "<tr>" +
            "<th>" +
            valueByLanguage('<%=Language%>', "তারিখ", "Date") +
            "</th>" +
            "<th>" +
            valueByLanguage('<%=Language%>', "ব্যায়(টাকা)", "Expense(Taka)") +
            "</th>\n" +
            "</tr>";

        if (mData && mData.length > 0) {
            for (var i = 0; i < mData.length; i++) {
                let dValue = "<tr>" +
                    "<td>" +
                    mData[i].date +
                    "</td>" +
                    "<td>" +
                    mData[i].amount +
                    "</td>\n" +
                    "</tr>";
                mHtml += dValue;
            }
        }

        $('#fuel-request-items-table').html(mHtml);

    }

    function setTaxTokenFeesData(data) {
        $("#totalTaxTokenCost").text(data.totalTaxTokenAndFitnessCost + valueByLanguage('<%=Language%>', " টাকা", " Taka"));
        totalTaxTokenCost = data.totalTaxTokenAndFitnessCost;

        let mData = data.VmTaxTokenItemListForTax;
        //let rData = data.maintenanceDTOListForRepair;

        let mHtml = "<tr>" +
            "<th>" +
            valueByLanguage('<%=Language%>', "অর্থবছর", "Fiscal Year") +
            "</th>" +
            "<th>" +
            valueByLanguage('<%=Language%>', "ব্যয়(টাকা)", "Expense(Taka)") +
            "</th>\n" +
            "</tr>";

        let rHtml = mHtml;


        if (mData && mData.length > 0) {
            for (var i = 0; i < mData.length; i++) {
                let dValue = "<tr>" +
                    "<td>" +
                    valueByLanguage('<%=Language%>', mData[i].fiscalYearBn, mData[i].fiscalYearEn) +
                    "</td>" +
                    "<td>" +
                    mData[i].taxTokenFiscalYearCost +
                    "</td>\n" +
                    "</tr>";
                mHtml += dValue;
            }
        }

        // if(rData && rData.length > 0){
        // 	for(var i = 0; i < rData.length ; i++){
        // 		let dValue = "<tr>" +
        // 				"<td>" +
        // 				rData[i].date +
        // 				"</td>" +
        // 				"<td>" +
        // 				rData[i].amount +
        // 				"</td>\n" +
        // 				"</tr>";
        // 		rHtml += dValue;
        // 	}
        // }

        $('#taxToken-items-table').html(mHtml);
        //$('#repair-items-table').html(rHtml);

    }


</script>






