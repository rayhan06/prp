<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="file_tracker.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="no_objection_certificate.No_objection_certificateDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="marquee_text.Marquee_textDTO" %>
<%@ page import="util.StringUtils" %>


<%
    String servletName = "No_objection_certificateServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Marquee_textDTO marqueeTextDTO = (Marquee_textDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    CommonDTO commonDTO = marqueeTextDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=isLanguageEnglish ? "MARQUEE TEXT" : "Marquee টেক্সট"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-10 offset-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=isLanguageEnglish ? "MARQUEE TEXT" : "Marquee টেক্সট"%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Serial" : "ক্রম"%>
                                    </label>
                                    <div class="col-9">
                                        <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.format("%d", marqueeTextDTO.serial))%>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Description (English)" : "বিবরণ (ইংরেজি)"%>
                                    </label>
                                    <div class="col-9">
                                        <%=marqueeTextDTO.textEn%>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Description (Bangla)" : "বিবরণ (বাংলা)"%>
                                    </label>
                                    <div class="col-9">
                                        <%=marqueeTextDTO.textBn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Link" : "লিঙ্ক"%>
                                    </label>
                                    <div class="col-9">
                                        <a href="<%=marqueeTextDTO.getUrl()%>">
                                            <%=marqueeTextDTO.url%>
                                        </a>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Visible From Time" : "যে সময় থেকে দৃশ্যমান"%>
                                    </label>
                                    <div class="col-9">
                                        <%=DateTimeUtil.getViewString(marqueeTextDTO.showFromTime, isLanguageEnglish)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Is Visible?" : "দৃশ্যমান কিনা?"%>
                                    </label>
                                    <div class="col-9">
                                        <%=StringUtils.getYesNo(isLanguageEnglish, marqueeTextDTO.visible)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Visible In Login Page?" : "লগিন পেজে দৃশ্যমান কিনা?"%>
                                    </label>
                                    <div class="col-9">
                                        <%=StringUtils.getYesNo(isLanguageEnglish, marqueeTextDTO.visibleInLoginPage)%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>