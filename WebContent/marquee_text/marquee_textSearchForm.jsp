<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="file_tracker.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%@ page import="no_objection_certificate.No_objection_certificateDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="marquee_text.Marquee_textServlet" %>
<%@ page import="marquee_text.Marquee_textDTO" %>
<%@ page import="util.StringUtils" %>


<%
    String navigator2 = "navFILE_TRACKER";
    String servletName = "Marquee_textServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>
                <%=isLanguageEnglish ? "Serial" : "ক্রম"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Description (English)" : "বিবরণ (ইংরেজি)"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Description (Bangla)" : "বিবরণ (বাংলা)"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Visible From Time" : "যে সময় থেকে দৃশ্যমান"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Is Visible?" : "দৃশ্যমান কিনা?"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Visible In Login Page?" : "লগিন পেজে দৃশ্যমান কিনা?"%>
            </th>
            <th>
                <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.FILE_TRACKER_SEARCH_FILE_TRACKER_EDIT_BUTTON, loginDTO)%>
            </th>
            <th>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <span><%=isLanguageEnglish ? "All" : "সব"%></span>
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Marquee_textDTO> data = (List<Marquee_textDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Marquee_textDTO marqueeTextDTO : data) {
        %>
        <tr>
            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.format("%d", marqueeTextDTO.serial))%>
            </td>
            <td>
                <%=marqueeTextDTO.textEn%>
            </td>
            <td>
                <%=marqueeTextDTO.textBn%>
            </td>
            <td>
                <%=DateTimeUtil.getViewString(marqueeTextDTO.showFromTime, isLanguageEnglish)%>
            </td>
            <td>
                <%=StringUtils.getYesNo(isLanguageEnglish, marqueeTextDTO.visible)%>
            </td>
            <td>
                <%=StringUtils.getYesNo(isLanguageEnglish, marqueeTextDTO.visibleInLoginPage)%>
            </td>
            <%
                CommonDTO commonDTO = marqueeTextDTO;
            %>
            <%@include file="../pb/searchAndViewButton.jsp" %>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'>
                        <input type='checkbox' name='ID' value='<%=marqueeTextDTO.iD%>'/>
                    </span>
                </div>
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			