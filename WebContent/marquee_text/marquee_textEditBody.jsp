<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="file_tracker.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.OfficeUnitTypeEnum" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="no_objection_certificate.No_objection_certificateDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="no_objection_certificate.No_objection_certificateDAO" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="marquee_text.Marquee_textDTO" %>


<%
    Marquee_textDTO marqueeTextDTO = (Marquee_textDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    CommonDTO commonDTO = marqueeTextDTO;
    if (marqueeTextDTO == null) {
        marqueeTextDTO = new Marquee_textDTO();
        marqueeTextDTO.showFromTime = System.currentTimeMillis();
    }
    String tableName = No_objection_certificateDAO.getInstance().getTableName();
    String context = request.getContextPath() + "/";
%>
<%@include file="../pb/addInitializer2.jsp" %>

<%
    String formTitle = isLanguageEnglish ? "MARQUEE TEXT" : "Marquee টেক্সট";
    String servletName = "Marquee_textServlet";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=marqueeTextDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="serial">
                                            <%=isLanguageEnglish ? "Serial" : "ক্রম"%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-9">
                                            <input type="text" class="form-control" name="serial" id="serial"
                                                   value="<%=marqueeTextDTO.serial < 0? "" : marqueeTextDTO.serial%>"
                                                   data-only-integer="true"
                                            >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="textEn">
                                            <%=isLanguageEnglish ? "Description (English)" : "বিবরণ (ইংরেজি)"%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-9">
                                            <textarea class='form-control' name='textEn' style="resize: none"
                                                      rows="3" id='textEn'
                                                      maxlength="2048"><%=marqueeTextDTO.textEn%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="textBn">
                                            <%=isLanguageEnglish ? "Description (Bangla)" : "বিবরণ (বাংলা)"%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-9">
                                            <textarea class='form-control' name='textBn' style="resize: none"
                                                      rows="3" id='textBn'
                                                      maxlength="2048"><%=marqueeTextDTO.textBn%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="url">
                                            <%=isLanguageEnglish ? "Link" : "লিঙ্ক"%>
                                        </label>
                                        <div class="col-9">
                                            <input type='text' class='form-control' name='url'
                                                   id='url'
                                                   value='<%=marqueeTextDTO.url%>'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="showFromTime">
                                            <%=isLanguageEnglish ? "Visible From Time" : "যে সময় থেকে দৃশ্যমান"%>
                                        </label>
                                        <div class="col-9">
                                            <input type="datetime-local" class='form-control' name="showFromTime"
                                                   id="showFromTime"
                                                   value="<%=DateTimeUtil.getDateTimeIsoString(marqueeTextDTO.showFromTime)%>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="visibleInLoginPage">
                                            <%=isLanguageEnglish ? "Is Visible?" : "দৃশ্যমান কিনা?"%>
                                        </label>
                                        <div class="col-1">
                                            <input type='checkbox' class='form-control-sm mt-1' name='visible'
                                                   id='visible'
                                                <%=marqueeTextDTO.visible ? "checked" : ""%>
                                                   onchange="visibleValueChanged(this)" value='<%=marqueeTextDTO.visible%>'>
                                        </div>
                                        <div class="col-8"></div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="visibleInLoginPage">
                                            <%=isLanguageEnglish ? "Visible In Login Page?" : "লগিন পেজে দৃশ্যমান কিনা?"%>
                                        </label>
                                        <div class="col-1">
                                            <input type='checkbox' class='form-control-sm mt-1' name='visibleInLoginPage'
                                                   id='visibleInLoginPage'
                                                   <%=marqueeTextDTO.visible ? "" : "disabled"%>
                                                   <%=marqueeTextDTO.visibleInLoginPage ? "checked" : ""%>
                                                   onchange="this.value = this.checked;" value='<%=marqueeTextDTO.visibleInLoginPage%>'>
                                        </div>
                                        <div class="col-8"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'"
                            >
                                <%=LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitForm()">
                                <%=LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const form = $('#bigform');
    const visible = document.getElementById('visible');
    const visibleInLoginPage = document.getElementById('visibleInLoginPage');

    function typeOnlyInteger(e) {
        return true === inputValidationForPositiveValue(e, $(this), <%=Integer.MAX_VALUE%>, 0);
    }

    $(document).ready(function () {
        $('body').delegate('[data-only-integer="true"]', 'keydown', typeOnlyInteger);
    });

    function visibleValueChanged(element) {
        element.value = element.checked;
        visibleInLoginPage.value = visibleInLoginPage.checked = element.checked;
        visibleInLoginPage.disabled = !element.checked;
    }

    function submitForm() {
        submitAjaxByData(form.serialize(), "<%=servletName%>?actionType=<%=actionName%>");
    }

    function buttonStateChange(value) {
        $(':button').prop('disabled', value);
    }
</script>