<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="manually_deliver_medicine.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@page import="drug_information.*" %>
<%@page import="employee_offices.*" %>
<%@page import="family.*" %>

<%
Manually_deliver_medicineDTO manually_deliver_medicineDTO = new Manually_deliver_medicineDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	manually_deliver_medicineDTO = Manually_deliver_medicineDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = manually_deliver_medicineDTO;
Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
String tableName = "manually_deliver_medicine";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MANUALLY_DELIVER_MEDICINE_ADD_FORMNAME, loginDTO);
String servletName = "Manually_deliver_medicineServlet";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Manually_deliver_medicineServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=manually_deliver_medicineDTO.iD%>' tag='pb_html'/>
	
												
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=Language.equalsIgnoreCase("English")?"Employee Type":"কর্মকর্তার ধরন"%>														
															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='employeeType' id = 'employeeType_select_<%=i%>'   tag='pb_html' onchange="setRef(this.value)">
																<%
																	Options = CatDAO.getOptions(Language, "employee", manually_deliver_medicineDTO.employeeType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>
                                									
													<div class="form-group row" id = "refDiv" style= "display:none">
                                                            <label class="col-4 col-form-label text-right">
															<%=Language.equalsIgnoreCase("English")?"Reference Employee":"রেফারেন্স কর্মকর্তা"%>														
															</label>
                                                            <div class="col-8">
																<button type="button" class="btn btn-primary form-control" onclick="addEmployeeWithRow(this.id)" id="employeeOrganogramId_button_<%=i%>" tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%></button>
																<table class="table table-bordered table-striped">
																	<tbody id="employeeOrganogramId_table_<%=i%>" tag='pb_html'>
																	<%
																	if(manually_deliver_medicineDTO.employeeOrganogramId != -1)
																	{
																		%>
																		<tr>
																			<td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(manually_deliver_medicineDTO.employeeOrganogramId)%></td>
																			<td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(manually_deliver_medicineDTO.employeeOrganogramId, Language)%></td>
																			<td><%=WorkflowController.getOrganogramName(manually_deliver_medicineDTO.employeeOrganogramId, Language)%>, <%=WorkflowController.getUnitNameFromOrganogramId(manually_deliver_medicineDTO.employeeOrganogramId, Language)%></td>
																		</tr>
																		<%
																	}
																	%>
																	</tbody>
																</table>
																<input type='hidden' class='form-control'  name = 'employeeOrganogramId' id = 'employeeOrganogramId_hidden_<%=i%>' value='<%=manually_deliver_medicineDTO.employeeOrganogramId%>' tag='pb_html'/>
																
			
															</div>
                                                      </div>
                                                      
                                                      <div class="form-group row" id = "userDiv" style= "display:none">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_USER_NAME, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name = 'employeeUserName'
																onkeyup = 'getFamily(this.value)'
																 id = 'employeeUserName' value='' tag='pb_html'/>
															</div>
                                                      </div>									
																					
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='patientName' id = 'patientName' value='<%=manually_deliver_medicineDTO.patientName%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_PHONE, loginDTO)%>*
															</label>
                                                            <div class="col-8">
																<div class="input-group mb-2">
																	<div class="input-group-prepend">
																		<div class="input-group-text"><%=LM.getText(LC.HM_EIGHT_EIGHT, loginDTO)%></div>
																	</div>
																	<input type='text' class='form-control' required  name='phoneNumber' id = 'phoneNumber'
																		value='<%=Utils.getPhoneNumberWithout88(manually_deliver_medicineDTO.phoneNumber, Language)%>'
																		tag='pb_html'>
																</div>
											
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_DOCTOR, loginDTO)%>															</label>
                                                            <div class="col-8">
																 <select  class='form-control' name='doctorOrganogramId'
					                                                   id='doctorOrganogramId' 
					                                                   tag='pb_html' required>
					                                                   <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
					                                                   <%
					                                                   Set<Long> drs = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.DOCTOR_ROLE);
					                                                   for(Long dr: drs)
					                                                   {
					                                                   %>
					                                                   <option value = "<%=dr%>" <%=manually_deliver_medicineDTO.doctorOrganogramId == dr? "selected":"" %>>
					                                                   <%=WorkflowController.getNameFromOrganogramId(dr, Language)%>
					                                                   </option>
					                                                   <%
					                                                   }
					                                                   %>
					                                             </select>
																
															</div>
                                                      </div>									
																					
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MEDICINE_DETAILS, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MEDICINE_DETAILS_DRUGINFORMATIONTYPE, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_STOCK_STATUS, loginDTO)%></th>
										<th><%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MEDICINE_DETAILS_QUANTITY, loginDTO)%></th>
										<th><%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MEDICINE_DETAILS_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-MedicineDetails">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(MedicineDetailsDTO medicineDetailsDTO: manually_deliver_medicineDTO.medicineDetailsDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "MedicineDetails_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='medicineDetails.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=medicineDetailsDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='medicineDetails.manuallyDeliverMedicineId' id = 'manuallyDeliverMedicineId_hidden_<%=childTableStartingID%>' value='<%=medicineDetailsDTO.manuallyDeliverMedicineId%>' tag='pb_html'/>
									</td>
									<td>										


												<div class="input-group">
											        <input
											          class="form-control py-2  border searchBox w-auto"
											          type="search"
											          placeholder="Search"
											          name='suggested_drug'
                                                      id='suggested_drug_'
                                                      tag='pb_html'
                                                      onkeyup="getDrugs(this.id)"
                                                      autocomplete="off"
											        />
											        
											      </div>
											      <div
											        id="searchSgtnSection_"
											        class="search-sgtn-section shadow-sm bg-white"
													tag='pb_html'
											      >
											        <div class="pt-3">
											          <ul class="px-0" style="list-style: none" id="drug_ul_" tag='pb_html'>
											          </ul>
											        </div>
											      </div>
											      <div id = "drug_modal_textdiv_" tag='pb_html'></div>
											      
											      
											      <input name='formStr'
                                                   id='formStr_' type="hidden"
                                                   tag='pb_html'
                                                   value=''></input>
                                                   
                                    <input name='medicineDetails.drugInformationType'
                                              id='drugInformationType_select_' type="hidden"
                                              tag='pb_html'
                                              value='<%=medicineDetailsDTO.drugInformationType%>'></input>
		
									</td>
									<td>
									<div id = "drug_modal_textdiv_" tag='pb_html'></div>
									</td>
									<td>										





																<%
																	value = "";
																	if(medicineDetailsDTO.quantity != -1)
																	{
																	value = medicineDetailsDTO.quantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='medicineDetails.quantity' id = 'quantity_number_<%=childTableStartingID%>' value='<%=value%>'  tag='pb_html'>		
									</td>
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-MedicineDetails"
											name="add-moreMedicineDetails"
											type="button"
											onclick="childAdded(event, 'MedicineDetails')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-MedicineDetails"
											name="removeMedicineDetails"
											type="button"
											onclick="childRemoved(event, 'MedicineDetails')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%MedicineDetailsDTO medicineDetailsDTO = new MedicineDetailsDTO();%>
					
							<template id="template-MedicineDetails" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='medicineDetails.iD' id = 'iD_hidden_' value='<%=medicineDetailsDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='medicineDetails.manuallyDeliverMedicineId' id = 'manuallyDeliverMedicineId_hidden_' value='<%=medicineDetailsDTO.manuallyDeliverMedicineId%>' tag='pb_html'/>
									</td>
									<td>


										<div class="input-group">
											        <input
											          class="form-control py-2  border searchBox w-auto"
											          type="search"
											          placeholder="Search"
											          name='suggested_drug'
                                                      id='suggested_drug_'
                                                      tag='pb_html'
                                                      onkeyup="getDrugs(this.id)"
                                                      autocomplete="off"
											        />
											        
											      </div>
											      <div
											        id="searchSgtnSection_"
											        class="search-sgtn-section shadow-sm bg-white"
													tag='pb_html'
											      >
											        <div class="pt-3">
											          <ul class="px-0" style="list-style: none" id="drug_ul_" tag='pb_html'>
											          </ul>
											        </div>
											      </div>
											      
											      <input name='formStr'
                                                   id='formStr_' type="hidden"
                                                   tag='pb_html'
                                                   value=''></input>
                                            
                                                   
                                    <input name='medicineDetails.drugInformationType'
                                              id='drugInformationType_select_' type="hidden"
                                              tag='pb_html'
                                              value='<%=medicineDetailsDTO.drugInformationType%>'></input>


															
		
									</td>
									<td>
									<div id = "drug_modal_textdiv_" tag='pb_html'></div>
									</td>
									<td>





																<%
																	value = "";
																	if(medicineDetailsDTO.quantity != -1)
																	{
																	value = medicineDetailsDTO.quantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='medicineDetails.quantity' id = 'quantity_number_' value='<%=value%>'  tag='pb_html'>		
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MANUALLY_DELIVER_MEDICINE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MANUALLY_DELIVER_MEDICINE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	
	if($("#employeeUserName").val() == "")
	{
		toastr.error("Reference Employee must be selected");
		return false;
	}
	if(child_table_extra_id <= 1)
	{
		toastr.error("At least one medicine must be selected");
		return false;
	}

	
	for(i = 1; i < child_table_extra_id; i ++)
	{
		if($("#quantity_number_" + i).val() == 0)
		{
			toastr.error("Quantity cannot be zero");
			return false;
		}
		
		var drugType = $("#drugInformationType_select_" + i).val();
		for(j = i + 1; j < child_table_extra_id; j ++)
		{
			if($("#drugInformationType_select_" + j).val() == drugType)
			{
				toastr.error("Duplicate Drugs Not allowed");
				return false;
			}
		}
	}
	if(processPhoneById('#phoneNumber', '<%=Language%>') == false)
	{
		console.log("invalid phone");
		toastr.error(getJsError('phone', '<%=Language%>', 0));
		$("#phoneNumber").val("");
		return false;
	}
	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Manually_deliver_medicineServlet");	
}

function init(row)
{

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function processRowsWhileAdding(childName)
{
	if(childName == "MedicineDetails")
	{			
	}
}

function drugClicked(rowId, drugText, drugId, form, stock)
{
	console.log("drugClicked with " + rowId + ", drugId = " + drugId + ", form = " + form);
	$("#suggested_drug_" + rowId).val(drugText);
	$("#suggested_drug_" + rowId).attr("style", "width: " + ($("#suggested_drug_" + rowId).val().length + 1)*8 + "px !important");
	
	 $("#drug_modal_textdiv_" + rowId).html(stock);
     $("#totalCurrentStock_text_" + rowId).val(stock);

	$("#drugInformationType_select_" + rowId).val(drugId);
	$("#quantity_number_" + rowId).attr("max", stock);
	
	$("#drug_ul_" + rowId).html("");
	//$("#drug_ul_" + rowId).css("display", "none");
}

function getFamily(userName)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var familyMemberDTO = JSON.parse(this.responseText);
            <%
            if(Language.equalsIgnoreCase("english"))
            {
            %>
            $('#patientName').val(familyMemberDTO.nameEn);
            <%
            }
            else
            {
            %>
            $('#patientName').val(familyMemberDTO.nameBn);
            <%
            }
            %>
            $('#phoneNumber').val(phoneNumberRemove88ConvertLanguage(familyMemberDTO.mobile, '<%=Language%>'));
           
        } else {
            //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
        }
    };

    xhttp.open("POST", "FamilyServlet?actionType=getPatientDetails&familyMemberId=<%=FamilyDTO.OWN%>&userName=" + userName + "&language=<%=Language%>", true);
    xhttp.send();
}

function patient_inputted(userName, orgId) {
    console.log('changed value: ' + userName);
    $("#employeeUserName").val(userName);
    getFamily(userName);

}

function setRef(employeeType)
{
	console.log(employeeType);
	if(employeeType == <%=Manually_deliver_medicineDTO.EMPLOYEE_SECURITY%>
	|| employeeType == <%=Manually_deliver_medicineDTO.EMPLOYEE_EMERGENCY%>)
	{
		console.log("setting medical director");
		$("#employeeOrganogramId_hidden_<%=i%>").val('<%=SessionConstants.MEDICAL_DIRECTOR_ORGANOGRAM_ID%>');
		$("#refDiv").css("display", "none");
		$("#userDiv").css("display", "none");
		
	}
	else
	{
		$("#refDiv").removeAttr("style");
		$("#userDiv").removeAttr("style");
	}
}

</script>






