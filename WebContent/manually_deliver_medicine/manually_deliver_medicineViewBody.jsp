

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="manually_deliver_medicine.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Manually_deliver_medicineServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Manually_deliver_medicineDTO manually_deliver_medicineDTO = Manually_deliver_medicineDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = manually_deliver_medicineDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MANUALLY_DELIVER_MEDICINE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MANUALLY_DELIVER_MEDICINE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_DELIVERYDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, manually_deliver_medicineDTO.deliveryDate), Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=Language.equalsIgnoreCase("English")?"Employee Type":"কর্মকর্তার ধরন"%>														
															
                                    </label>
                                    <div class="col-8">
											<%=CommonDAO.getName(Language, "employee", manually_deliver_medicineDTO.employeeType)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=Language.equalsIgnoreCase("English")?"Reference Employee":"রেফারেন্স কর্মকর্তা"%>														

                                    </label>
                                    <div class="col-8">
											<%=WorkflowController.getNameFromUserName(manually_deliver_medicineDTO.employeeUserName, Language)%>
											<br>
											<%=manually_deliver_medicineDTO.employeeUserName%>                                    </div>
                                </div>
			
							
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=manually_deliver_medicineDTO.patientName%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
										<%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=manually_deliver_medicineDTO.phoneNumber%>
                                    </div>
                                </div>
			
								
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_DOCTOR, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=WorkflowController.getNameFromOrganogramId(manually_deliver_medicineDTO.doctorOrganogramId, Language)%>
											<br>
											<%=manually_deliver_medicineDTO.doctorUserName%>
                                    </div>
                                </div>
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MEDICINE_DETAILS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MEDICINE_DETAILS_DRUGINFORMATIONTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_MEDICINE_DETAILS_QUANTITY, loginDTO)%></th>
							</tr>
							<%
                        	MedicineDetailsDAO medicineDetailsDAO = MedicineDetailsDAO.getInstance();
                         	List<MedicineDetailsDTO> medicineDetailsDTOs = (List<MedicineDetailsDTO>)medicineDetailsDAO.getDTOsByParent("manually_deliver_medicine_id", manually_deliver_medicineDTO.iD);
                         	
                         	for(MedicineDetailsDTO medicineDetailsDTO: medicineDetailsDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=CommonDAO.getName(Language, "drug_information", medicineDetailsDTO.drugInformationType)%>
										</td>
										<td>
											<%=Utils.getDigits(medicineDetailsDTO.quantity, Language)%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>