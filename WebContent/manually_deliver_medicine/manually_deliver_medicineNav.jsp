<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page contentType="text/html;charset=utf-8" %>


<%
	System.out.println("Inside nav.jsp");
	String url = "Manually_deliver_medicineServlet?actionType=search";	
%>
<%@include file="../pb/navInitializer.jsp"%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
				<input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text' class='form-control border-0'
					   onKeyUp='allfield_changed(false)' id='anyfield'  name='anyfield'
				>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>        
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

			<div class="row">
				<div class="col-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.HM_DATE, loginDTO)%> <%=LM.getText(LC.HM_FROM, loginDTO)%></label>					
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="delivery_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="delivery_date_start" name="delivery_date_start">

						</div>
					</div>
				</div>			

				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_DATE, loginDTO)%> <%=LM.getText(LC.HM_TO, loginDTO)%></label>
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="delivery_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="delivery_date_end" name="delivery_date_end">

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_EMPLOYEETYPE, loginDTO)%></label>
						<div class="col-9">
							<select class='form-control'  name='employee_type' id = 'employee_type' onSelect='setSearchChanged()'>
								<%
								Options = CatDAO.getOptions(Language, "employee", -2);
																%>
								<%=Options%>
							</select>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="employee_user_name" placeholder="" name="employee_user_name" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="patient_name" placeholder="" name="patient_name" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_PHONE, loginDTO)%></label>
						<div class="col-9">
							<div class="input-group mb-2">
								<div class="input-group-prepend">
									<div class="input-group-text"><%=LM.getText(LC.HM_EIGHT_EIGHT, loginDTO)%></div>
								</div>
								<input type="text" class="form-control" id="phone_number" placeholder="" name="phone_number" onChange='setSearchChanged()'>
							</div>

						</div>
					</div>
				</div>
				<div class="col-6" style = "display:none">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_DOCTORUSERNAME, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="doctor_user_name" placeholder="" name="doctor_user_name" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_DOCTOR, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="doctor_name" placeholder="" name="doctor_name" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6" style = "display:none">
                    <div class="form-group row">
                        <label class="col-3 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_INSERTIONDATE, loginDTO)%></label>					
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="insertion_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="insertion_date_start" name="insertion_date_start">

						</div>
					</div>
				</div>			

				<div class="col-6" style = "display:none">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.MANUALLY_DELIVER_MEDICINE_ADD_INSERTIONDATE, loginDTO)%></label>
						<div class="col-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="insertion_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="insertion_date_end" name="insertion_date_end">

						</div>
					</div>
				</div>
		</div>
		<div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed(true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">

	function getParams()
	{
		var params = 'AnyField=' + document.getElementById('anyfield').value;

		$("#delivery_date_start").val(getDateStringById('delivery_date_start_js', 'DD/MM/YYYY'));
		params +=  '&delivery_date_start='+ getBDFormattedDate('delivery_date_start');
		$("#delivery_date_end").val(getDateStringById('delivery_date_end_js', 'DD/MM/YYYY'));
		params +=  '&delivery_date_end='+ getBDFormattedDate('delivery_date_end');		
		if($("#employee_type").val() != -1 && $("#employee_type").val() != "")
		{
			params +=  '&employee_type='+ $("#employee_type").val();
		}
		params +=  '&employee_user_name='+ $('#employee_user_name').val();
		params +=  '&patient_name='+ $('#patient_name').val();
		if($('#phone_number').val() != "")
       	{
        	params += '&phone_number=' + phoneNumberAdd88ConvertLanguage($('#phone_number').val(), '<%=Language%>');
       	}
		params +=  '&doctor_user_name='+ $('#doctor_user_name').val();
		params +=  '&doctor_name='+ $('#doctor_name').val();
		$("#insertion_date_start").val(getDateStringById('insertion_date_start_js', 'DD/MM/YYYY'));
		params +=  '&insertion_date_start='+ getBDFormattedDate('insertion_date_start');
		$("#insertion_date_end").val(getDateStringById('insertion_date_end_js', 'DD/MM/YYYY'));
		params +=  '&insertion_date_end='+ getBDFormattedDate('insertion_date_end');		
		
		params +=  '&search=true';
		
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}

		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		
		return params;
	}

	function allfield_changed(setPushState)
	{
		var params = getParams();
		navsubmit(params, "Manually_deliver_medicineServlet", setPushState);
	}
	
	function resetFields()
	{
		$("#anyfield").val("");
		resetDateById('delivery_date_start_js');
        resetDateById('delivery_date_end_js');		
		$("#employee_type").val("-1");
		$("#employee_user_name").val("");
		$("#patient_name").val("");
		$("#phone_number").val("");
		$("#doctor_user_name").val("");
		$("#doctor_name").val("");
		resetDateById('insertion_date_start_js');
        resetDateById('insertion_date_end_js');		
	}
	
	 window.addEventListener('popstate',e=>{
		if(e.state){
			let params = e.state;
			navsubmit(params, "Test_moduleServlet", false);
			resetFields();
			let arr = params.split('&');
			arr.forEach(e=>{
				let item = e.split('=');
				if(item.length === 2){
					//console.log("param " + item[0] + " = " + item[1]);
					switch (item[0]){
						case 'anyfield':	                        	
							$("#anyfield").val(item[1]);
							break;
						case 'AnyField':
							$("#anyfield").val(item[1]);
							break;
						case 'delivery_date_start':
							setDateByTimestampAndId('delivery_date_start_js',item[1]);
							break;
						case 'delivery_date_end':
							setDateByTimestampAndId('delivery_date_end_js',item[1]);
							break;
						case 'employee_type':
							$("#employee_type").val(item[1]);
							break;
						case 'employee_user_name':
							$("#employee_user_name").val(item[1]);
							break;
						case 'patient_name':
							$("#patient_name").val(item[1]);
							break;
						case 'phone_number':
							$("#phone_number").val(phoneNumberRemove88ConvertLanguage(item[1], '<%=Language%>'));
							break;
						case 'doctor_user_name':
							$("#doctor_user_name").val(item[1]);
							break;
						case 'doctor_name':
							$("#doctor_name").val(item[1]);
							break;
						case 'insertion_date_start':
							setDateByTimestampAndId('insertion_date_start_js',item[1]);
							break;
						case 'insertion_date_end':
							setDateByTimestampAndId('insertion_date_end_js',item[1]);
							break;
					}
				}
			});
		}else{
			navsubmit(null, "Test_moduleServlet", false);
		}
	});
	
	window.onbeforeunload = function(){
		 var params = getParams();
		 history.pushState(params,'', 'Manually_deliver_medicineServlet?actionType=search&'+params);
	};
	
	 $(document).ready(() => {
        readyInit('Manually_deliver_medicineServlet');
    });

</script>

