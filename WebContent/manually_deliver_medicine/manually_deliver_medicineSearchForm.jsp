
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="manually_deliver_medicine.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page pageEncoding="UTF-8" %>


<%
String navigator2 = "navMANUALLY_DELIVER_MEDICINE";
String servletName = "Manually_deliver_medicineServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=Language.equalsIgnoreCase("English")?"Employee Type":"কর্মকর্তার ধরণ"%>														
								</th>
								<th><%=Language.equalsIgnoreCase("English")?"Reference Employee":"রেফারেন্স কর্মকর্তা"%>	</th>
								<th><%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_DOCTOR, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								
								
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Manually_deliver_medicineDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Manually_deliver_medicineDTO manually_deliver_medicineDTO = (Manually_deliver_medicineDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, manually_deliver_medicineDTO.deliveryDate), Language)%>
											</td>
		
											<td>
											<%=CatDAO.getName(Language, "employee", manually_deliver_medicineDTO.employeeType)%>
											</td>
		
											<td>
											<%=WorkflowController.getNameFromUserName(manually_deliver_medicineDTO.employeeUserName, Language)%>
											<br>
											<%=manually_deliver_medicineDTO.employeeUserName%>
											<br>
											<%=WorkflowController.getOfficeNameFromOrganogramId(manually_deliver_medicineDTO.employeeOrganogramId, Language)%>
											</td>
		
											
		
											<td>
											<%=manually_deliver_medicineDTO.patientName%>
											<br>
											<%=manually_deliver_medicineDTO.phoneNumber%>
											
											</td>
		
											
											<td>
											<%=WorkflowController.getNameFromOrganogramId(manually_deliver_medicineDTO.doctorOrganogramId, Language)%>
											<br>
											<%=manually_deliver_medicineDTO.doctorUserName%>
											</td>

	
											<%CommonDTO commonDTO = manually_deliver_medicineDTO; %>
											<td>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											    
											    <button type="button" type="button" class="btn btn-sm cancel-btn text-white border-0 shadow btn-border-radius pl-4 ml-2"
											            onclick="location.href='Deliver_or_return_medicineServlet?actionType=getAddPage&userName=<%=manually_deliver_medicineDTO.employeeUserName%>&medicalItemType=<%=SessionConstants.MEDICAL_ITEM_DRUG%>'">
											        <i class="fa fa-minus"></i>
											    </button>
											</td>										
																						
											
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			