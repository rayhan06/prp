<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="ministry_office_mapping.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="ministry_office.Ministry_officeRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="election_wise_mp.Election_wise_mpRepository" %>
<%@ page import="election_constituency.Election_constituencyRepository" %>


<%
    String servletName = "Ministry_office_mappingServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Ministry_office_mappingDTO ministry_office_mappingDTO = Ministry_office_mappingDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = ministry_office_mappingDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRY_OFFICE_MAPPING_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRY_OFFICE_MAPPING_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>


                                <div class="form-group row  align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Ministry" : "মন্ত্রণালয়"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Ministry_officeRepository.getInstance().getText(Language, ministry_office_mappingDTO.ministryOfficeId)%>
                                    </div>
                                </div>

                                <div class="form-group row  align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Minister" : "মন্ত্রী"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Employee_recordsRepository.getInstance().getEmployeeName(ministry_office_mappingDTO.employeeRecordsId, Language)%>
                                    </div>
                                </div>

                                <div class="form-group row  align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Parliament Number" : "পার্লামেন্ট নাম্বার"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Utils.getDigits(ministry_office_mappingDTO.electionDetailsId, Language)%>
                                    </div>
                                </div>

                                <%
                                    if (ministry_office_mappingDTO.electionWiseMpId!=-1) {
                                %>
                                <div class="form-group row  align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Election Constituency" : "নির্বাচনী এলাকা"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=Election_constituencyRepository.getInstance().getConstituencyText(Election_wise_mpRepository.getInstance().getDTOByID(ministry_office_mappingDTO.electionWiseMpId).electionConstituencyId, Language)%>
                                    </div>
                                </div>
                                <%
                                    }
                                %>

                                <div class="form-group row  align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=Language.equalsIgnoreCase("English") ? "Designation" : "পদবি"%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%=CatRepository.getInstance().getText(Language, "ministers", ministry_office_mappingDTO.ministersCat)%>
                                    </div>
                                </div>

                                <div class="form-group row  align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_STARTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = ministry_office_mappingDTO.startDate + "";
                                        %>
                                        <%
                                            String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_startDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row  align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_ENDDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
                                        <%
                                            value = ministry_office_mappingDTO.endDate + "";
                                        %>
                                        <%
                                            String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_endDate, Language)%>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>