<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="ministry_office_mapping.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="ministry_office.Ministry_officeRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>


<%
    String navigator2 = "navMINISTRY_OFFICE_MAPPING";
    String servletName = "Ministry_office_mappingServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead class="text-nowrap">
        <tr>
            <th><%=Language.equalsIgnoreCase("English") ? "Ministry" : "মন্ত্রণালয়"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Minister" : "মন্ত্রী"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Designation" : "পদবি"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "Start Date" : "মন্ত্রীত্ব শুরুর দিন"%>
            </th>
            <th><%=Language.equalsIgnoreCase("English") ? "End Date" : "মন্ত্রীত্ব বিলুপ্তির দিন"%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_SEARCH_MINISTRY_OFFICE_MAPPING_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span><%=Language.equalsIgnoreCase("English") ? "All" : "সবগুলো"%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Ministry_office_mappingDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Ministry_office_mappingDTO ministry_office_mappingDTO = (Ministry_office_mappingDTO) data.get(i);


        %>
        <tr>


            <td>
                <%=Ministry_officeRepository.getInstance().getText(Language, ministry_office_mappingDTO.ministryOfficeId)%>
            </td>

            <td>
                <%=Employee_recordsRepository.getInstance().getEmployeeName(ministry_office_mappingDTO.employeeRecordsId, Language)%>
            </td>

            <td>
                <%=CatRepository.getInstance().getText(Language, "ministers", ministry_office_mappingDTO.ministersCat)%>
            </td>

            <td>
                <%
                    value = ministry_office_mappingDTO.startDate + "";
                %>
                <%
                    String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_startDate, Language)%>


            </td>

            <td>
                <%
                    value = ministry_office_mappingDTO.endDate + "";
                %>
                <%
                    String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_endDate, Language)%>


            </td>


            <%CommonDTO commonDTO = ministry_office_mappingDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=ministry_office_mappingDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			