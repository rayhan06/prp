<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="ministry_office_mapping.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="ministry_office.Ministry_officeRepository" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_offices.Employee_officesDTO" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>

<%
    Ministry_office_mappingDTO ministry_office_mappingDTO = new Ministry_office_mappingDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        ministry_office_mappingDTO = Ministry_office_mappingDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = ministry_office_mappingDTO;
    String tableName = "ministry_office_mapping";
    long today = System.currentTimeMillis();
    int endYear = Calendar.getInstance().get(Calendar.YEAR) + 10;
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRY_OFFICE_MAPPING_ADD_FORMNAME, loginDTO);
    String servletName = "Ministry_office_mappingServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Ministry_office_mappingServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=ministry_office_mappingDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='electionWiseMpId'
                                           id='electionWiseMpId'
                                           value='<%=ministry_office_mappingDTO.electionWiseMpId%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Ministry Office" : "মন্ত্রনালয় দপ্তর"%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='ministryOfficeId'
                                                    id='ministryOfficeId' tag='pb_html'>
                                                <%--                                                <%= actionName.equalsIgnoreCase("ajax_edit") ? CommitteesRepository.getInstance().buildOptions(Language, committees_mappingDTO.committeesId) : CommitteesRepository.getInstance().buildOptions(Language, null) %>--%>
                                                <%=Ministry_officeRepository.getInstance().buildOptions(Language, ministry_office_mappingDTO.ministryOfficeId)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Minister Type": "মন্ত্রীত্বের ধরণ"%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='ministersCat'
                                                    id='ministersCat' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("ministers", Language, ministry_office_mappingDTO.ministersCat)%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Parliament Number" : "পার্লামেন্ট নাম্বার"%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='electionDetailsId' id='electionDetailsId'
                                                    onchange="onElectionDetailsChanged()" tag='pb_html'>
                                                <%= Election_detailsRepository.getInstance().buildOptions(Language, ministry_office_mappingDTO.electionDetailsId) %>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Types of Employment" : "কর্মকর্তার ধরন"%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9 d-flex">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="typeOfEmployee" id="typeOfEmployee1" value="1">
                                                <label class="form-check-label" for="typeOfEmployee1">
                                                    <%=Language.equalsIgnoreCase("English") ? "MP" : "সাংসদ"%>
                                                </label>
                                            </div>
                                            <div class="form-check ml-3">
                                                <input class="form-check-input" type="radio" name="typeOfEmployee" id="typeOfEmployee2" value="2">
                                                <label class="form-check-label" for="typeOfEmployee2">
                                                    <%=Language.equalsIgnoreCase("English") ? "Officer" : "কর্মকর্তা"%>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row" id="technocrat_div" style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=Language.equalsIgnoreCase("English") ? "Officer Name" : "কর্মকর্তার নাম"%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="" id='select-technocrat-div'>
                                                <button type="button"
                                                        class="btn btn-primary shadow btn-block btn-border-radius"
                                                        id="tagEmp_modal_button">
                                                    <%=Language.equalsIgnoreCase("English") ? "Select Technocrat Minister" : "টেকনোক্র্যাট মিনিস্টার নির্বাচন করুন"%>
                                                </button>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped text-nowrap">
                                                        <thead></thead>
                                                        <tbody id="tagged_emp_table" class="rounded">
                                                        <tr style="display: none;" class="selected_employee_row">
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <button type="button"
                                                                        class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                        style="padding-right: 14px"
                                                                        onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>

                                                        <% if (actionName.equals("ajax_edit") && ministry_office_mappingDTO.electionWiseMpId==-1) {
                                                            Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(ministry_office_mappingDTO.employeeRecordsId);
                                                        %>
                                                        <tr id="savedEmployeeRecordId">
                                                            <td><%=Utils.getDigits(employee_recordsDTO.employeeNumber, Language)%>
                                                            </td>
                                                            <td>
                                                                <%=isLanguageEnglish ? employee_recordsDTO.nameEng: employee_recordsDTO.nameBng%>
                                                            </td>

                                                            <%
                                                                EmployeeOfficeDTO employeeOfficeDTO;
                                                                String officeUnitName = "", OfficeUnitOrganogramsName = "", postName = "";
                                                                try {
                                                                    employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(ministry_office_mappingDTO.employeeRecordsId);
                                                                    OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
                                                                    officeUnitName = Office_unitsRepository.getInstance().geText(Language, employeeOfficeDTO.officeUnitId);
                                                                    OfficeUnitOrganogramsName = isLanguageEnglish ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng;
                                                                    postName = OfficeUnitOrganogramsName + "<br>" + officeUnitName;
                                                                } catch (Exception ex) {
                                                                    postName = isLanguageEnglish ? "No Current Responsibility!" : "কোনো দপ্তরের নিয়মিত দায়িত্বে নেই!";
                                                                }

                                                            %>

                                                            <td><%=postName%>
                                                            </td>
                                                            <td id='<%=ministry_office_mappingDTO.employeeRecordsId%>_td_button'>
                                                                <button type="button"
                                                                        class="btn btn-sm delete-trash-btn"
                                                                        onclick="remove_containing_row(this,'tagged_emp_table');">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <%}%>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row" id="MP_DIV" style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="electionConstituencyId">
                                            <%=Language.equalsIgnoreCase("English") ? "MP Name" : "সাংসদ"%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='electionConstituencyId'
                                                    id='electionConstituencyId'
                                                    onchange='onConstituencyChanged()'>
                                            </select>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control d-flex align-items-center' name='employeeRecordsId' id='employeeRecordsId'
                                           value='<%=ministry_office_mappingDTO.employeeRecordsId%>' tag='pb_html'/>


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_STARTDATE, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="start-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='startDate' id='start-date'
                                                   value=''
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_ENDDATE, loginDTO)%>
                                        <span class="required">*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="end-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR" value="<%=endYear%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='end-date'
                                                   value=''
                                                   tag='pb_html'>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=ministry_office_mappingDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedBy'
                                           id='insertedBy_hidden_<%=i%>'
                                           value='<%=ministry_office_mappingDTO.insertedBy%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='modifiedBy'
                                           id='modifiedBy_hidden_<%=i%>'
                                           value='<%=ministry_office_mappingDTO.modifiedBy%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=ministry_office_mappingDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=ministry_office_mappingDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=ministry_office_mappingDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRY_OFFICE_MAPPING_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit" onclick="submitCommittee()">
                                <%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRY_OFFICE_MAPPING_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<script type="text/javascript">

    function onConstituencyChanged() {
        $('#employeeRecordsId').val($("#electionConstituencyId").val());
    }

    function onElectionDetailsChanged() {
        let electionDetailsId = $("#electionDetailsId").val();

        if (electionDetailsId != -1 && electionDetailsId != "") {
            const url = 'Committees_mappingServlet?actionType=getElectionConstituencyListWithMpName&electionDetailsId=' + electionDetailsId;

            $.ajax({
                url: url,
                type: "GET",
                async: true,
                success: function (response) {
                    document.getElementById('electionConstituencyId').innerHTML = response;
                    document.getElementById('employeeRecordsId').value = '';
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    async function onEditPage() {

        const url = 'Ministry_office_mappingServlet?actionType=getEditPageRecord&electionDetailsId=' + <%=ministry_office_mappingDTO.electionDetailsId%>
            +'&employeeRecordsId=' + <%=ministry_office_mappingDTO.employeeRecordsId%>;

        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (response) {
                document.getElementById('electionConstituencyId').innerHTML = response;
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    const row = 0;

    function submitCommittee() {

        $('#electionWiseMpId').val('-1');

        $('#start-date').val(getDateStringById('start-date-js'));
        $('#end-date').val(getDateStringById('end-date-js'));
        let startDateValid = dateValidator('start-date-js', true, {
            'errorEn': 'Enter a valid Ministry responsibility start date!',
            'errorBn': 'মন্ত্রীত্ব শুরু দিন টুকে ফেলুন!'
        });
        let endDateValid = dateValidator('end-date-js', true, {
            'errorEn': 'Enter a valid ministry responsibility end date!',
            'errorBn': 'মন্ত্রীত্ব বিলুপ্তির তারিখ টুকুন!'
        });

        buttonStateChange(true);
        if($('#bigform').valid() && startDateValid && endDateValid){
            $.ajax({
                type : "POST",
                url : "Ministry_office_mappingServlet?actionType=<%=actionName%>",
                data : $('#bigform').serialize(),
                dataType : 'JSON',
                success : function(response) {
                    if(response.responseCode === 0){
                        $('#toast_message').css('background-color','#ff6063');
                        showToastSticky(response.msg,response.msg);
                        buttonStateChange(false);
                    }else if(response.responseCode === 200){
                        window.location.replace(getContextPath()+response.msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }else{
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }

    $(document).ready(function () {

        init();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $.validator.addMethod('employeeRecordsIdValidator', function (value, element) {
            return value;
        });

        $.validator.addMethod('ministryOfficeIdValidator', function (value, element) {
            return value;
        });

        $.validator.addMethod('ministersCatValidator', function (value, element) {
            return value;
        });


        $.validator.addMethod('electionDetailsIdValidator', function (value, element) {
            return value;
        });


        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                employeeRecordsId: {
                    required: false,
                    employeeRecordsIdValidator: true
                },
                ministryOfficeId: {
                    required: false,
                    ministryOfficeIdValidator: true
                },
                ministersCat: {
                    required: false,
                    ministersCatValidator: true
                },
                electionDetailsId: {
                    required: false,
                    electionDetailsIdValidator: true
                }
            },
            messages: {
                employeeRecordsId: '<%=Language.equalsIgnoreCase("English") ? "Select Minister!" : "মন্ত্রী বাছাই করুন!"%>',
                ministryOfficeId: '<%=Language.equalsIgnoreCase("English") ? "Select Ministry!" : "মন্ত্রণালয় বাছাই করুন!"%>',
                ministersCat: '<%=Language.equalsIgnoreCase("English") ? "Select designation in the ministry!" : "পদবি বাছাই করুন!"%>',
                electionDetailsId: '<%=Language.equalsIgnoreCase("English") ? "Select parliament number!" : "কততম নির্বাচন বাছাই করুন!"%>'
            }
        });

        $('#start-date-js').on('datepicker.change', () => {
            setMinDateById('end-date-js', getDateStringById('start-date-js'));
        });


        <%
            if (actionName.equalsIgnoreCase("ajax_add")) {
        %>
        setDateByTimestampAndId('start-date-js', <%=today%>);
        setDateByTimestampAndId('end-date-js', <%=today%>);
        <%
         } else {
        %>
        setDateByTimestampAndId('start-date-js', <%=ministry_office_mappingDTO.startDate%>);
        setDateByTimestampAndId('end-date-js', <%=ministry_office_mappingDTO.endDate%>);
        <%
        }
        %>

    });

    function init() {
        select2SingleSelector('#electionDetailsId', '<%=Language%>');
        select2SingleSelector('#electionConstituencyId', '<%=Language%>');
        select2SingleSelector('#ministryOfficeId', '<%=Language%>');
        select2SingleSelector('#ministersCat', '<%=Language%>');

        <%
            if (actionName.equalsIgnoreCase("ajax_edit")) {
                if (ministry_office_mappingDTO.electionWiseMpId==-1) {
        %>
        $("#typeOfEmployee2").prop("checked", true);
        $("#technocrat_div").show();
        <%
        } else {
        %>
        $("#typeOfEmployee1").prop("checked", true);
        $("#MP_DIV").show();
        <%
        }
        %>

        onEditPage();
        <%
            } else {
        %>
        $("#typeOfEmployee1").prop("checked", true);
        $("#MP_DIV").show();
        <%
        }
        %>


        $('input:radio').change(function() {
            let type_of_emp = $("input[name='typeOfEmployee']:checked").val();

            if (type_of_emp==1) {
                $("#MP_DIV").show();
                $("#technocrat_div").hide();
                $('#employeeRecordsId').val('');
            } else {
                $("#MP_DIV").hide();
                $("#technocrat_div").show();
                $('#employeeRecordsId').val('');
            }
        });

    }


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    added_employee_info_map = new Map();

    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_emp_table', {
                info_map: added_employee_info_map,
                isSingleEntry: true,
                callBackFunction: function (empInfo) {
                    document.getElementById('employeeRecordsId').value = empInfo.employeeRecordId;
                    $('#savedEmployeeRecordId').remove();
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagEmp_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_emp_table';
        $('#search_emp_modal').modal();
    });


    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        console.log("delete (employee_record_id)");
        added_info_map.delete(employee_record_id);
        console.log(added_info_map);

        $('#employeeRecordsId').val('');
    }
</script>






