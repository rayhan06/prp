<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="feedback_question.Feedback_questionDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="feedback_question.Feedback_questionRepository" %>
<%@ page import="feedback_question_training_calendar_mapping.FeedbackQuestionTrainingCalenderMappingDAO" %>
<%@ page import="feedback_question_training_calendar_mapping.FeedbackQuestionTrainingCalenderMappingDTO" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pb.Utils" %>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.FAMILY_EDIT_LANGUAGE, loginDTO);
    String formTitle = LM.getText(LC.FEEDBACK_QUESTION_ADD_FEEDBACK_QUESTION_ADD_FORMNAME, loginDTO);
    long trainingCalendarId = Long.parseLong(request.getParameter("training_calendar_id"));
    List<Feedback_questionDTO> feedbackQuestionDTOList = Feedback_questionRepository.getInstance().getFeedback_questionList();
    Map<Long, Feedback_questionDTO> questionDTOMapById = feedbackQuestionDTOList.stream()
            .collect(Collectors.toMap(dto -> dto.iD, dto -> dto));
    String jsonMap = new ObjectMapper().writeValueAsString(questionDTOMapById);
    List<FeedbackQuestionTrainingCalenderMappingDTO> mappingDTOList = FeedbackQuestionTrainingCalenderMappingDAO.getInstance().getByTrainingId(trainingCalendarId);
    List<Long> taggedQuestionList = mappingDTOList.stream().map(dto -> dto.feedbackQuestionId).collect(Collectors.toList());
    List<Feedback_questionDTO> unTaggedFeedbackQuestionDTOList = Utils.getFilteredIdList(feedbackQuestionDTOList, dto -> !taggedQuestionList.contains(dto.iD));
%>

<style>
    .btn-gray,
    .btn-gray:focus {
        background-color: #ffffff;
        color: #008020;
        margin: 20px 10px !important;
        border: 2px solid #e0e0e0;
        padding: 10px 15px;
        font-weight: 600;
    }

    .btn-gray:hover {
        background-color: #ffffff;
        color: #008020;
        border: 2px solid #008020;
        box-shadow: none !important;
    }
</style>

<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="kt-portlet shadow-none">
        <div class="kt-portlet__body">
            <div class="modal fade" id="untaggedFeedbackQuestionModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content modal-lg">
                        <div class="modal-header">
                            <h4 class="modal-title"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_FEEDBACK_QUESTIONS,loginDTO)%></h4>
                        </div>
                        <div class="modal-body">
                            <div id="untagged_questions">
                                <button style="float: right;margin-bottom: 5px" class="btn-success" data-dismiss="modal" onclick="addToAll()"
                                        title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_ADD_ALL,loginDTO)%>"><i
                                        class="fa fa-plus"></i>&nbsp;<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_ALL,loginDTO)%>&nbsp;</button>

                                <div class="table-responsive">
                                    <table id="tableData1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th style="width: 47%"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_QUESTION_ENGLISH,loginDTO)%></th>
                                            <th style="width: 47%"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_QUESTION_BANGLA,loginDTO)%></th>
                                            <th style="width: 6%"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_ACTION,loginDTO)%></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                            for (Feedback_questionDTO unTagDTO : unTaggedFeedbackQuestionDTOList) {
                                        %>
                                        <tr id="ques_"<%=unTagDTO.iD%>>
                                            <td><%=unTagDTO.questionEng%>
                                            </td>
                                            <td><%=unTagDTO.questionBng%>
                                            </td>
                                            <td>
                                                <button class="btn-success" onclick="addToTagList(this)" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_ADD,loginDTO)%>"><i
                                                        class="fa fa-plus"></i></button>
                                            </td>
                                        </tr>
                                        <%} %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_CLOSE,loginDTO)%></button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tagged_questions">
                <h3 style="margin: 20px 0 0 20px"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_TAGGED_FEEDBACK_QUESTIONS,loginDTO)%></h3>
                <button class="btn btn-gray rounded-pill" data-toggle="modal" data-target="#untaggedFeedbackQuestionModal">
                    <i class="fa fa-plus"></i><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_TAG_FEEDBACK_QUESTION,loginDTO)%>
                </button>
                <div class="table-responsive">
                    <table id="tableData2" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th style="width: 45%"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_QUESTION_ENGLISH,loginDTO)%></th>
                            <th style="width: 45%"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_QUESTION_BANGLA,loginDTO)%></th>
                            <th style="width: 10%"><%=LM.getText(LC.PORTAL_FEEDBACK_ADD_ACTION,loginDTO)%></th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            for (FeedbackQuestionTrainingCalenderMappingDTO mappingDTO : mappingDTOList) {
                                Feedback_questionDTO tagDTO = questionDTOMapById.get(mappingDTO.feedbackQuestionId);
                                if (tagDTO != null) {
                        %>
                        <tr>
                            <td><%=tagDTO.questionEng%>
                            </td>
                            <td><%=tagDTO.questionBng%>
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <button class="btn-info" onclick="upCall(this)" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_UP,loginDTO)%>"><i class="fa fa-arrow-up"></i>
                                    </button>
                                    <button class="btn-primary" onclick="downCall(this)" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_DOWN,loginDTO)%>"><i
                                            class="fa fa-arrow-down"></i></button>
                                    <button class="btn-danger" onclick="removeCall(this)" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_DELETE,loginDTO)%>" style="color: whitesmoke;font-weight: bold">X</button>
                                </div>
                            </td>
                        </tr>
                        <%
                                }
                            }
                        %>
                        </tbody>
                    </table>
                </div>
            </div>

            <div>
                <form class="form-horizontal"
                      action="FeedbackQuestionTrainingCalenderMappingServlet"
                      id="disciplinary_form" name="bigform" method="POST"
                      enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting()">
                    <div class="form-body">
                        <input type='hidden' class='form-control' name='question_ids' id='feedbackQuestions_id' value=''
                               tag='pb_html'/>
                        <input type='hidden' class='form-control' name='training_calendar_id' value='<%=trainingCalendarId%>'
                               tag='pb_html'/>
                        <div class="form-actions text-right mt-3">
                            <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius" href="<%=request.getHeader("referer")%>">
                                <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_DISCIPLINARY_LOG_CANCEL_BUTTON, userDTO)%>
                            </a>
                            <button class="btn btn-sm submit-btn text-white shadow btn-border-radius ml-2" type="submit">
                                <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_DISCIPLINARY_LOG_SUBMIT_BUTTON, userDTO)%>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script>
    const allQuestionArr = <%=jsonMap%>;
    let untaggedList = [];
    let taggedList = [];

    function PreprocessBeforeSubmiting() {
        document.getElementById("feedbackQuestions_id").value = taggedList.join();
        return true;
    }

    function addToAll(){
        let thirdTd = '<td> ' +
            '<div class="btn-group" role="group" aria-label="Basic example"> ' +
            '<button class="btn-info" onclick="upCall(this)" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_UP,loginDTO)%>"><i class="fa fa-arrow-up"></i></button>' +
            '<button class="btn-primary" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_DOWN,loginDTO)%>" onclick="downCall(this)"><i class="fa fa-arrow-down"></i></button>' +
            '<button class="btn-danger" style="color: whitesmoke;font-weight: bold" onclick="removeCall(this)" ' +
            'title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_DELETE,loginDTO)%>">X</button> </div></td>';
        let table = $('#tableData1');
        console.log(table);
        while(table[0].rows.length>1){
            table[0].deleteRow(1);
            remove2(0, untaggedList, taggedList, "tableData2", thirdTd);
        }
    }
    function upCall(button) {
        let index = button.parentNode.parentNode.parentNode.rowIndex;
        if (index > 1) {
            index = index - 1;
            let curValue = taggedList[index];
            taggedList[index] = taggedList[index - 1];
            taggedList[index - 1] = curValue;
            let row = $(button).parents("tr:first");
            row.insertBefore(row.prev());
        }
    }

    function downCall(button) {
        let len = $('#tableData2')[0].rows.length;
        len = len - 1; // reduce title's row length
        let index = button.parentNode.parentNode.parentNode.rowIndex;
        if (index < len) {
            let row = $(button).parents("tr:first");
            row.insertAfter(row.next());
            let downValue = taggedList[index];
            taggedList[index] = taggedList[index - 1];
            taggedList[index - 1] = downValue;
        }
    }

    function removeCall(button) {
        let thirdTd = '<td> <button class="btn-success" onclick="addToTagList(this)">' +
            '<i class="fa fa-plus" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_ADD,loginDTO)%>"></i></button> </td>';
        remove(button.parentNode.parentNode.parentNode, taggedList, untaggedList, "tableData1", thirdTd);
    }

    function addToTagList(button) {
        let thirdTd = '<td> ' +
            '<div class="btn-group" role="group" aria-label="Basic example"> ' +
            '<button class="btn-info" onclick="upCall(this)" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_UP,loginDTO)%>"><i class="fa fa-arrow-up"></i></button>' +
            '<button class="btn-primary" title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_DOWN,loginDTO)%>" onclick="downCall(this)"><i class="fa fa-arrow-down"></i></button>' +
            '<button class="btn-danger" style="color: whitesmoke;font-weight: bold" onclick="removeCall(this)" ' +
            'title="<%=LM.getText(LC.PORTAL_FEEDBACK_ADD_DELETE,loginDTO)%>">X</button> </div></td>';
        remove(button.parentNode.parentNode, untaggedList, taggedList, "tableData2", thirdTd);
    }

    function remove(containing_row, srcList, destList, destTableId, thirdTd) {
        let containing_table = containing_row.parentNode;
        let index = containing_row.rowIndex - 1;
        console.log("index : "+index);
        containing_table.deleteRow(index);
        remove2(index,srcList,destList,destTableId,thirdTd);
    }

    function remove2(index,srcList,destList,destTableId,thirdTd){
        let questionId = srcList[index];
        destList.push(questionId);
        srcList.splice(index, 1);
        let firstTd = "<td>" + allQuestionArr[questionId].questionEng + "</td>"
        let secondTd = "<td>" + allQuestionArr[questionId].questionBng + "</td>"
        let newRow = "<tr>" + firstTd + " " + secondTd + " " + thirdTd + " </tr>";
        $('#' + destTableId).append(newRow);
    }



    $(document).ready(() => {
        <%
            for(Feedback_questionDTO dto : unTaggedFeedbackQuestionDTOList){
        %>
        untaggedList.push(<%=dto.iD%>)
        <%
        }
        %>
        <%
            for(Long questionId : taggedQuestionList){
        %>
        taggedList.push(<%=questionId%>)
        <%
        }
        %>
    });
</script>
