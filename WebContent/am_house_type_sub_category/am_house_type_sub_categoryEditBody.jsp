<%@page import="am_house_type_sub_category.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>


<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Am_house_type_sub_categoryDTO am_house_type_sub_categoryDTO = new Am_house_type_sub_categoryDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        am_house_type_sub_categoryDTO = Am_house_type_sub_categoryDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = am_house_type_sub_categoryDTO;
    String tableName = "am_house_type_sub_category";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AM_HOUSE_TYPE_SUB_CATEGORY_ADD_FORMNAME, loginDTO);
    String servletName = "Am_house_type_sub_categoryServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigForm" name="bigForm">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <%--                                    FORM FIELD STARTS HERE--%>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=am_house_type_sub_categoryDTO.iD%>'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "House Category")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select name='amHouseCatList'
                                                    id='amHouseCatId_<%=i%>'>
                                                <%
                                                    Options = CatRepository.getOptions(Language, "am_house_class",
                                                            am_house_type_sub_categoryDTO.amHouseCatVal);
                                                %>
                                                <%=Options%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AMHOUSESUBCATNAMEEN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='amHouseSubCatNameEn'
                                                   id='amHouseSubCatNameEn_text_<%=i%>'
                                                   value='<%=am_house_type_sub_categoryDTO.amHouseSubCatNameEn%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AMHOUSESUBCATNAMEBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='amHouseSubCatNameBn'
                                                   id='amHouseSubCatNameBn_text_<%=i%>'
                                                   value='<%=am_house_type_sub_categoryDTO.amHouseSubCatNameBn%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                                <%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AM_HOUSE_TYPE_SUB_CATEGORY_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button"
                                    onclick="submitForm()">
                                <%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AM_HOUSE_TYPE_SUB_CATEGORY_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const form = $("#bigForm");

    $(document).ready(function () {
        init();
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    function init() {
        select2SingleSelector("#amHouseCatId_<%=i%>", '<%=Language%>');
    }

    function submitForm() {
        if (isFormValid()) {
            buttonStateChange(true); // DISABLE THE SUBMIT & CANCEL BUTTON
            doAjaxCall();
        } else {
            buttonStateChange(false); // ENABLE THE SUBMIT & CANCEL BUTTON
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function isFormValid() {
        form.validate();
        return form.valid();
    }

    function doAjaxCall(){
        let url = "<%=servletName%>?actionType=<%=actionName%>";
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }
</script>






