<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="am_house_type_sub_category.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.*" %>

<%
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Am_house_type_sub_categoryDTO am_house_type_sub_categoryDTO = Am_house_type_sub_categoryDAO.getInstance().getDTOByID(id);
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AM_HOUSE_TYPE_SUB_CATEGORY_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AM_HOUSE_TYPE_SUB_CATEGORY_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাসার শ্রেণীর নাম", "House Category Name")%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = isLanguageEnglish ? am_house_type_sub_categoryDTO.amHouseCatNameEn :
                                                    am_house_type_sub_categoryDTO.amHouseCatNameBn + "";
                                        %>
                                        <%=value%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AMHOUSESUBCATNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_type_sub_categoryDTO.amHouseSubCatNameEn + "";
                                        %>
                                        <%=value%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_ADD_AMHOUSESUBCATNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_type_sub_categoryDTO.amHouseSubCatNameBn + "";
                                        %>
                                        <%=value%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>