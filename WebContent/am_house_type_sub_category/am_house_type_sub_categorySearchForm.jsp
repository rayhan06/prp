<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="am_house_type_sub_category.*" %>
<%@ page import="util.*" %>
<%@ page import="java.util.ArrayList" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%
    String navigator2 = "navAM_HOUSE_TYPE_SUB_CATEGORY";
    String servletName = "Am_house_type_sub_categoryServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=UtilCharacter.getDataByLanguage(Language, "বাসার শ্রেণীর", "House Category")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "House Sub Category")%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.AM_HOUSE_TYPE_SUB_CATEGORY_SEARCH_AM_HOUSE_TYPE_SUB_CATEGORY_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
            <%
                ArrayList data = (ArrayList<Am_house_type_sub_categoryDTO>) rn2.list;
                try {

                    if (data != null) {
                        int size = data.size();
                        for (int i = 0; i < size; i++) {
                            Am_house_type_sub_categoryDTO am_house_type_sub_categoryDTO = (Am_house_type_sub_categoryDTO) data.get(i);

            %>
            <tr>
                <td>
                    <%
                        value = isLanguageEnglish ? am_house_type_sub_categoryDTO.amHouseCatNameEn + "" :
                                am_house_type_sub_categoryDTO.amHouseCatNameBn;
                    %>
                    <%=value%>
                </td>
                <td>
                    <%
                        value = isLanguageEnglish ? am_house_type_sub_categoryDTO.amHouseSubCatNameEn + "" :
                                am_house_type_sub_categoryDTO.amHouseSubCatNameBn;
                    %>
                    <%=value%>
                </td>


                <%CommonDTO commonDTO = am_house_type_sub_categoryDTO; %>
                <%@include file="../pb/searchAndViewButton.jsp" %>

                <td class="text-right">
                    <div class='checker'>
                        <span class='chkEdit'><input type='checkbox' name='ID'
                                                     value='<%=am_house_type_sub_categoryDTO.iD%>'/></span>
                    </div>
                </td>

            </tr>
            <%
                        }
                    }
                } catch (Exception e) {
                    System.out.println("JSP exception " + e);
                }
            %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			