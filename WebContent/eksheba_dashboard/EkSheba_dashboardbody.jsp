
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>
<%@page import="eksheba_dashboard.EkSheba_dashboardDTO"%>
<%@page import="eksheba_dashboard.EkSheba_dashboardDAO"%>
<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	EkSheba_dashboardDAO semen_dashboardDAO = new EkSheba_dashboardDAO();
	EkSheba_dashboardDTO ekSheba_dashboardDTO = semen_dashboardDAO.getDashboardDTO();
	if (ekSheba_dashboardDTO == null) {
		System.out.println("making new dto");
		ekSheba_dashboardDTO = new EkSheba_dashboardDTO();
	}
%>
<%-- <jsp:include page='../skeleton/banner.jsp' /> --%>
<!-- <div class="box box-primary"> -->
	<!-- <div class="box-header with-border">
		<h3 class="box-title uppercase">
			<i class="fa fa-gift"></i>DLS Dashboard
		</h3>
	</div> -->

<div class="box-body">

		<%-- <div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
							<h4 style="font-size: 25px;font-weight: bold;">Semen Collection</h4>

						<h4><span id="TEXT_1_0_0">Actual Collection: </span>
						<span class="counter"><%=ekSheba_dashboardDTO.actual_semen_collection%></span>
						</h4>

						<h4> <span id="TEXT_2_0_1">Percentage Achieved: </span>
						<span class="counter"><%=(int) ekSheba_dashboardDTO.semen_collection_percentage%>%</span>
						</h4>
					</div>
					<div class="icon" style='top: 5px;'>
					  <img src="<%=request.getContextPath()%>/assets/images/grs/semen.png" alt="Chicago" style="height:50px;opacity: 0.4;">
					</div>
					
				</div>
			</div>
			
			<!-- ./col -->
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	          <!-- small box -->
	          <div class="small-box bg-green">
	            <div class="inner">
	              <h4 style="font-size: 25px;font-weight: bold;">Artificial Insemination</h4>
	
					<h4><span id="TEXT_1_0_0">Actual AI: </span>
					<span class="counter"><%=ekSheba_dashboardDTO.actual_ai%></span>
					</h4>
	
					<h4><span id="TEXT_2_0_1">Percentage Achieved: </span>
					<span class="counter"><%=(int) ekSheba_dashboardDTO.ai_percentage%>%</span>
					</h4>
	            </div>
	            <div class="icon" style='top: 5px;'>
	              <img src="<%=request.getContextPath()%>/assets/images/grs/ai.png" alt="Chicago" style="height:50px;opacity: 0.4;">
	            </div>
	           
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	          <!-- small box -->
	          <div class="small-box bg-yellow">
	            <div class="inner">
             		 <h4 style="font-size: 25px;font-weight: bold;">Progeny</h4>

					<h4><span id="TEXT_1_0_0">Actual Progeny: </span>
					<span class="counter"><%=ekSheba_dashboardDTO.actual_progeny%></span>
					</h4>

					<h4> <span id="TEXT_2_0_1">Percentage Achieved: </span>
					<span class="counter"><%=(int) ekSheba_dashboardDTO.progeny_percentage%>%</span>
					</h4>
	            </div>
	            <div class="icon" style='top: 5px;'>
	              <img src="<%=request.getContextPath()%>/assets/images/grs/progenyte.png" alt="Chicago" style="height:50px;opacity: 0.4;">
	            </div>
	            
	          </div>
	        </div>
	        <!-- ./col -->
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	          <!-- small box -->
	          <div class="small-box bg-red">
	            <div class="inner">
	               <h4 style="font-size: 20px;font-weight: bold;">Candidate Bull Production</h4>

					<h4><span id="TEXT_1_0_0">Actual Production: </span>
					<span class="counter"><%=ekSheba_dashboardDTO.actual_cb%></span>
					</h4>

					<h4> <span id="TEXT_2_0_1">Percentage Achieved: </span>
					<span class="counter"><%=(int) ekSheba_dashboardDTO.cb_percentage%>%</span>
					</h4>
	            </div>
	            <div class="icon" style='top: 5px;'>
	            <img src="<%=request.getContextPath()%>/assets/images/grs/bull.png" alt="Chicago" style="height:50px;opacity: 0.4;">
	              
	            </div>
	            
	          </div>
	        </div>
		</div> --%>
</div>	
<!-- 	<div clasas="container">
		<div class="row">
			<div class='col-xs-12 col-sm-12 col-md-6 col-sm-6'>
					<canvas id="progenyBull"></canvas>
					
					<h3 style="text-align:center">Most Productive Bulls(According to progeny)</h3>
			</div>
			<div class='col-xs-12 col-sm-12 col-md-6 col-sm-6'>
			
			<canvas id="progenyCenter"></canvas>
				<h3 style="text-align:center">Most Productive Centers (According to progeny)</h3>
			</div>
		</div>
		</div>
	</div> -->

<script>
<%
String[] ithProgenyBull = ekSheba_dashboardDTO.top_5_progeny_bulls;
String jsArray="[";
for(int i = 0; i < ithProgenyBull.length; i++){      
	jsArray+="'"+ithProgenyBull[i]+"'";    
	    if(i != ithProgenyBull.length -1){
	    	jsArray+=",";
	    }
}
jsArray+="]";
%>

<%
int[] ithProgenyBullCalf = ekSheba_dashboardDTO.top_5_progeny_bulls_calfs;
String jsArraycalf="[";
for(int i = 0; i < ithProgenyBullCalf.length; i++){      
	jsArraycalf+=ithProgenyBullCalf[i];    
	    if(i != ithProgenyBullCalf.length -1){
	    	jsArraycalf+=",";
	    }
}
jsArraycalf+="]";
%>
</script>

<script>
var canvas = document.getElementById('progenyBull');
var data = {
    labels: <%=jsArray%>,
    datasets: [
        {
            label: "Most Productive Bulls",
            backgroundColor: "#0d7c98",
            borderColor: "#ffffff",
            borderWidth: 2,
            hoverBackgroundColor: "#00a65a",
            hoverBorderColor: "#ffffff",
            data: <%=jsArraycalf%>,
        }
    ]
};
var option = {
	scales: {
  	yAxes:[{
    		stacked:true,
        gridLines: {
        	display:true,
          color:"rgba(255,99,132,0.2)"
        }
    }],
    xAxes:[{
    		gridLines: {
        	display:false
        }
    }]
  }
};

// var myBarChart = Chart.Bar(canvas,{
// 	data:data,
//     options:option
// });
</script>

<script>
<%
String[] ithProgenyCenter = ekSheba_dashboardDTO.top_5_progeny_centers;
String jsArrayCenter="[";
for(int i = 0; i < ithProgenyCenter.length; i++){      
	jsArrayCenter+="'"+ithProgenyCenter[i]+"'";    
	    if(i != ithProgenyCenter.length -1){
	    	jsArrayCenter+=",";
	    }
}
jsArrayCenter+="]";
%>

<%
int[] ithProgenyCentreCalf = ekSheba_dashboardDTO.top_5_progeny_centers_calfs;
String jsArrayCenterCalf="[";
for(int i = 0; i < ithProgenyCentreCalf.length; i++){      
	jsArrayCenterCalf+=ithProgenyCentreCalf[i];    
	    if(i != ithProgenyCentreCalf.length -1){
	    	jsArrayCenterCalf+=",";
	    }
}
jsArrayCenterCalf+="]";
%>
</script>

<script>
var canvas = document.getElementById('progenyCenter');
var data = {
    labels: <%=jsArrayCenter%>,
    datasets: [
        {
            label: "Most Productive Centers",
            backgroundColor: "#00a65a",
            borderColor: "#ffffff",
            borderWidth: 2,
            hoverBackgroundColor: "#0d7c98",
            hoverBorderColor: "#ffffff",
            data: <%=jsArrayCenterCalf%>,
        }
    ]
};
var option = {
	scales: {
  	yAxes:[{
    		stacked:true,
        gridLines: {
        	display:true,
          color:"rgba(255,99,132,0.2)"
        }
    }],
    xAxes:[{
    		gridLines: {
        	display:false
        }
    }]
  }
};

// var myBarChart = Chart.Bar(canvas,{
// 	data:data,
//     options:option
// });
</script>







