<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="util.*"%>

<%@ page import="pb.*" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.AM_OFFICE_UNIT_TYPE_REPORT_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row">
    <div class="col-12">

        <div id="calendardiv">
            <div id="visitDate" class="search-criteria-div">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_ADD_STARTDATE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                        </jsp:include>
                        <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
                               data-label="Document Date" id='startDate' name='startDate' value=""
                               tag='pb_html'
                        />
                    </div>
                </div>
            </div>
            <div id="visitDate_3" class="search-criteria-div">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">
                        <%=LM.getText(LC.TRAINING_CALENDER_EDIT_ENDDATE, loginDTO)%>
                    </label>
                    <div class="col-md-9">
                        <jsp:include page="/date/date.jsp">
                            <jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                        </jsp:include>
                        <input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
                               data-label="Document Date" id='endDate' name='endDate' value=""
                               tag='pb_html'
                        />
                    </div>
                </div>
            </div>


        </div>

        <div class="search-criteria-div">

            <div class="form-group row ">
                <label class="col-md-3 col-form-label text-md-right">
                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_CHOOSE_THE_TYPE_OF_OFFICE, loginDTO)%>

                </label>

                <div class="col-md-4">
                    <input type="radio" id="parliamentOffice" name="officeType" value="1">
                    <label for="parliamentOffice"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_OFFICE_OF_PARLIAMENT, loginDTO)%>
                    </label><br>
                </div>
                <div class="col-md-4">
                    <input type="radio" id="otherOffice" name="officeType" value="2">
                    <label for="otherOffice"><%=LM.getText(LC.LANGUAGE_OTHERS, loginDTO)%>
                    </label><br>
                </div>


            </div>
        </div>

        <div class="form-group row " style="display: none;" id="selectParliamentOffice">
            <label class="col-md-2 col-form-label text-md-right">
                <%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_OFFICEID, loginDTO)%>
                <span class="required" style="color: red;"> * </span>
            </label>

            <div class="col-md-8">
                <input type="hidden" name='officeUnitId'
                       id='office_units_id_input' value="">
                <button type="button" class="btn btn-secondary form-control shadow btn-border-radius"
                        disabled id="office_units_id_text"></button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="office_units_id_modal_button"
                        onclick="officeModalButtonClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                </button>
            </div>


        </div>


        <div class="form-group row " style="display: none;" id="selectOtherOffice">
            <label class="col-md-2 col-form-label text-md-right">
                <%=LM.getText(LC.LANGUAGE_OTHERS, loginDTO)%>
                <span class="required" style="color: red;"> * </span>
            </label>
            <div class="col-md-8">
                <select class='form-control' name='select2OtherOffice'
                        id='select2OtherOffice' tag='pb_html' onchange="otherOfficeSelected(this)">
                </select>
            </div>


        </div>


    </div>
</div>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    let otherOffices;
    //let language = "<%=Language%>";

    let otherOfficeSelected =  (selectedElement) => {
         if(selectedElement !== null && selectedElement.value !== -1){
             document.getElementById("office_units_id_input").value =  selectedElement.value;
         }
         else{
             document.getElementById("office_units_id_input").value ="";
         }
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        // console.log(selectedOffice);
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        // console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }
    function fetchOtherOffices(otherOffices) {

        let url = "Am_other_office_unitServlet?actionType=getAllOtherOffices";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {

                $('#select2OtherOffice').html("");
                let o;
                let str;
                // if (language === 'English') {
                //     str = 'Select';
                //     o = new Option('Select', '-1');
                // } else {
                    o = new Option('বাছাই করুন', '-1');
                    str = 'বাছাই করুন';
                //}
                $(o).html(str);
                $('#select2OtherOffice').append(o);

                const response = JSON.parse(fetchedData);

                if (response && response.length > 0) {
                    for (let x in response) {

                        if (language === 'English') {
                            str = response[x].englishText;
                        } else {
                            str = response[x].banglaText;
                        }

                        if (otherOffices && otherOffices.length > 0) {
                            if (otherOffices.includes(response[x].value + '')) {
                                o = new Option(str, response[x].value, false, true);
                            } else {
                                o = new Option(str, response[x].value);
                            }
                        } else {
                            o = new Option(str, response[x].value);
                        }
                        $(o).html(str);
                        $('#select2OtherOffice').append(o);
                    }
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    function init() {
        dateTimeInit($("#Language").val());
        select2SingleSelector("#select2OtherOffice",'<%=Language%>');
        fetchOtherOffices(otherOffices);
    }

    function PreprocessBeforeSubmiting() {
    }

    $('input[type=radio][name=officeType]').change(function () {
        if (this.value == '1') {
            $("#selectParliamentOffice").show();
            $("#selectOtherOffice").hide();
        } else if (this.value == '2') {
            $("#selectOtherOffice").show();
            $("#selectParliamentOffice").hide();
        }
    });

</script>