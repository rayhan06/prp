<%@page import="login.LoginDTO" %>
<%@page import="task_type_approval_path.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="user.UserDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="task_type_level.TaskTypeLevelDTO" %>
<%@ page import="task_type_level.TaskTypeLevelRepository" %>
<%@ page import="task_type_level.TaskTypeLevelUtil" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="util.StringUtils" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String actionName;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
    } else {
        actionName = "add";
    }
    String formTitle = LM.getText(LC.TASK_TYPE_APPROVAL_PATH_ADD_TASK_TYPE_APPROVAL_PATH_ADD_FORMNAME, loginDTO);
    int i = 0;
    Long taskTypeLevelId = Long.parseLong(request.getParameter("taskTypeLevelId"));
    List<TaskTypeApprovalPathDTO> taskApprovalList = (List<TaskTypeApprovalPathDTO>) request.getAttribute("taskApprovalList");
    TaskTypeLevelDTO taskTypeLevelDTO = null;
    if (taskTypeLevelId != null) {
        taskTypeLevelDTO = TaskTypeLevelRepository.getInstance().getById(taskTypeLevelId);
    }
    String data = request.getParameter("data");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="onlyborder">
                            <div class="row mx-2  mx-md-0">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label style="width: 100%;text-align: center">
                                                    <%=TaskTypeLevelUtil.getTaskTypeName(Language, taskTypeLevelDTO.taskTypeId)%>&nbsp;| <%=isLangEng ? " Level - " + taskTypeLevelDTO.level : " স্তর - " + StringUtils.convertToBanNumber(String.valueOf(taskTypeLevelDTO.level))%>
                                                </label>
                                            </div>

                                            <div class="form-group row">
                                                <button type="button" class="btn btn-primary shadow btn-border-radius btn-block"
                                                        id="officeUnitOrganogramId_button"
                                                        tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%>
                                                </button>

                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                <b><%=LM.getText(LC.USER_ADD_USER_NAME, userDTO)%>
                                                                </b></th>
                                                            <th>
                                                                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, userDTO)%>
                                                                </b></th>
                                                            <th>
                                                                <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, userDTO)%>
                                                                </b></th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>

                                                        <tbody id="organogramTable">
                                                        <tr style="display: none;">
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <button type="button"
                                                                        class="btn btn-sm delete-trash-btn"
                                                                        onclick="remove_containing_row(this,'organogramTable');">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                        <%if (taskApprovalList != null) {%>
                                                        <%
                                                            for (TaskTypeApprovalPathDTO approvalDTO : taskApprovalList) {
                                                                OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(approvalDTO.officeUnitOrganogramId);
                                                                EmployeeOfficeDTO employeeOfficeDTO = new EmployeeOfficeDTO();
                                                                if(officeUnitOrganograms != null) {
                                                                    employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(approvalDTO.officeUnitOrganogramId);
                                                                }
                                                                Employee_recordsDTO employeeRecordDTO =  new Employee_recordsDTO();
                                                                if(employeeOfficeDTO != null) {
                                                                    employeeRecordDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
                                                                }
                                                                Office_unitsDTO officeUnitsDTO = new Office_unitsDTO();
                                                                if(employeeOfficeDTO != null) {
                                                                    officeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(employeeOfficeDTO.officeUnitId);
                                                                }
                                                        %>
                                                        <tr>
                                                            <td>
                                                                <%=employeeRecordDTO.employeeNumber%>
                                                            </td>
                                                            <td>
                                                                <%=isLangEng ? employeeRecordDTO.nameEng : employeeRecordDTO.nameBng%>
                                                            </td>
                                                            <td>
                                                                <%=isLangEng ? officeUnitsDTO.unitNameEng + "\n" + officeUnitOrganograms.designation_eng : officeUnitsDTO.unitNameBng + "\n" + officeUnitOrganograms.designation_bng%>
                                                            </td>

                                                            <td id='<%=employeeRecordDTO.iD%>_td_button'>
                                                                <button type="button"
                                                                        class="btn btn-sm delete-trash-btn"
                                                                        onclick="remove_containing_row(this,'organogramTable');">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <%}%>
                                                        <%}%>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10 form-actions text-right mb-5">
                        <button class="btn btn-sm text-white shadow cancel-btn btn-border-radius" id="cancel-btn"
                                onclick="history.back()"><%=LM.getText(LC.TASK_TYPE_APPROVAL_PATH_ADD_TASK_TYPE_APPROVAL_PATH_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn btn-sm text-white shadow submit-btn btn-border-radius ml-2" id="submit-btn" onclick="submitApprovalForm()"
                                type="submit"><%=LM.getText(LC.TASK_TYPE_APPROVAL_PATH_ADD_TASK_TYPE_APPROVAL_PATH_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<%@include file="selectModal.jsp" %>

<script type="text/javascript">
    const errorMsgEn = 'Please add employee';
    const errorMsgBn = 'অনুগ্রহ করে কর্মকর্তা যোগ করুন';
    <%
       // add a list of employees
       // make a list of EmployeeSearchIds
       List<EmployeeSearchIds> addedEmpIdsList = null;
       if(taskApprovalList != null){
           addedEmpIdsList = taskApprovalList.stream()
                                        .map(dto -> {
                                            try{
                                                OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(dto.officeUnitOrganogramId);
                                                EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(dto.officeUnitOrganogramId);
                                                return new EmployeeSearchIds(employeeOfficeDTO.employeeRecordId, officeUnitOrganograms.office_unit_id, dto.officeUnitOrganogramId);
                                            }catch (Exception ex){
                                                ex.printStackTrace();
                                                return null;
                                            }
                                        }).filter(Objects::nonNull)
                                        .collect(Collectors.toList());
       }
   %>
    added_employee_info_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);
    table_name_to_collcetion_map = new Map([
        ['organogramTable', {
            info_map: added_employee_info_map,
            isSingleEntry: false,
            callBackFunction: function (empInfo) {
                console.log('callBackFunction called with latest added emp info JSON');
                console.log(empInfo);
            }
        }]
    ]);

    modal_button_dest_table = 'none';
    $('#officeUnitOrganogramId_button').on('click', function () {
        modal_button_dest_table = 'organogramTable';
        $('#search_emp_modal').modal();
    });

    function submitApprovalForm() {
        buttonStateChange(true);
        event.preventDefault();
        let data = Array.from(added_employee_info_map.values())
            .map(data => data.organogramId)
            .join(',');
        if (data) {
            $.ajax({
                type: "POST",
                url: "Task_type_approval_pathServlet?actionType=ajax_<%=actionName%>&<%=data != null ? ("data=" + data) : ""%>&taskTypeLevelId=<%=taskTypeLevelDTO.iD%>",
                data: {'officeUnitOrganogramId': data},
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(errorMsgBn, errorMsgEn);
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
        $('#officeUnitOrganogramId_button').prop('disabled', value);
        $('.remove-emp').prop('disabled', value);
    }
</script>