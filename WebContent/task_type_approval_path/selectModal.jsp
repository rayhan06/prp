<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@ page import="task_type.Task_typeRepository" %>
<%@ page import="task_type.Task_typeServlet" %>
<%@page pageEncoding="UTF-8" %>

<%
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
%>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_emp_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf; margin-left: 10px">
                    <%=LM.getText(LC.EMPLOYEE_SEARCH_MODAL_FIND_EMPLOYEE, userDTO)%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <form class="kt-form">
                    <div class="kt-form" id="officeLayer_dropdown_div">
                        <%-- BEGIN: Making Dreopdown Dynamic --%>
                        <div id="officeLayer_div_1" class="form-group col-lg-10 office-layer" style="margin-top: 10px">
                            <label><%=(isLanguageEnglish ? "Office" : "দপ্তর")%>
                            </label>
                            <div>
                                <select class='form-control' data-label="Office"
                                        id='officeLayer_select' tag='pb_html'
                                        onchange="officeSelected(this);">
                                    <%=taskTypeLevelDTO != null? TaskTypeLevelRepository.getInstance().buildOfficeOptionFromTaskTypeId(Language, taskTypeLevelDTO.taskTypeId, taskTypeLevelDTO.level)
                                                               : ""%>
                                </select>
                            </div>
                        </div>
                        <%-- END: Making Dreopdown Dynamic --%>
                    </div>
                </form>

                <hr style="border-top: 1px solid rgba(0, 0, 0, 0.1); margin: 10px 10px">

                <div style="margin: 5px 10px" class="" id="employeeSearchModal_table_div">
                </div>
            </div>
            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn btn-secondary shadow" data-dismiss="modal"
                        style="border-radius: 6px; margin-right: 10px">Close
                </button>
            </div>
        </div>
    </div>
</div>

<script>

    function officeSelected(selectElement) {
        let selectedOfficeId = selectElement.value;
        if (selectedOfficeId === '') {
            return;
        }
        showEmployeeList(selectedOfficeId);
    }

    function showEmployeeList(officeId) {
        let baseUrl = "EmployeeAssignServlet?actionType=getDisciplinaryEmployeeList";
        let options = table_name_to_collcetion_map.get(modal_button_dest_table);
        if(options.employeeSearchApiUrl){
            baseUrl = options.employeeSearchApiUrl;
        }
        let url = baseUrl + '&officeId=' + officeId;
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            success: showInSearchTable,
            error: function (error) {
                console.log(error);
            }
        });
    }

    function showInSearchTable(data) {
        document.getElementById('employeeSearchModal_table_div').innerHTML = '';
        document.getElementById('employeeSearchModal_table_div').innerHTML = data;
        // TODO: filter result here
        let info_map = table_name_to_collcetion_map.get(modal_button_dest_table).info_map;
        if(info_map) {
            hide_selected_employees('employeeSearchModal_tableData', info_map);
        }
    }

    function hide_selected_employees(filter_from_table_name, selected_employees_map_or_set) {
        let table_obj = document.getElementById(filter_from_table_name);
        let n_row = table_obj.rows.length;
        for (let i = 0; i < n_row; i++) {
            let row = table_obj.rows[i];
            let employee_record_id = row.id.split('_')[0];
            if (selected_employees_map_or_set.has(employee_record_id)) {
                hide(row);
            } else {
                un_hide(row);
            }
        }
    }

    function use_row_button(button) {
        if (modal_button_dest_table !== 'none') {
            let emp_info = add_containing_row_to_dest_table(button, modal_button_dest_table);
            let options = table_name_to_collcetion_map.get(modal_button_dest_table);
            if (options.isSingleEntry) {
                $('#search_emp_modal').modal('hide');
            }
            if (options.callBackFunction) {
                options.callBackFunction(emp_info);
            }
        }
    }

    function remove_containing_row(button, table_name) {
        // td > button id = "<employee record id>_td_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        added_info_map.delete(employee_record_id);
        console.log('Employee to Remove', employee_record_id);
        console.log('Map After Removal',added_info_map);

        let containing_row = button.parentNode.parentNode;
        // console.log('containing_row.remove()');
        containing_row.remove();
    }

    function add_containing_row_to_dest_table(button, dest_table_name) {
        let dest_table_info = table_name_to_collcetion_map.get(dest_table_name);
        if (dest_table_info === undefined || dest_table_info == null) {
            return;
        }

        let this_row = button.parentNode.parentNode;
        let dest_table = document.getElementById(dest_table_name);

        // if no table destination table found, return the employee info json
        if(!dest_table){
            return JSON.parse(
                $('#' + this_row.id + '_data').val()
            );
        }

        // clone the dummy template row
        let new_row = dest_table.rows[0].cloneNode(true);

        let n_col = this_row.cells.length;
        // copy every cell apart from last one
        // which is the remove button
        for (let i = 0; i < n_col - 1; i++) {
            new_row.cells[i].innerHTML = this_row.cells[i].innerHTML.trim();
        }

        let record_officeUnit_organogram_id_array = this_row.id.split('_');
        let employee_record_id = record_officeUnit_organogram_id_array[0];

        // skip if already added
        let added_info_map = dest_table_info.info_map;
        if (added_info_map.has(employee_record_id)) {
            return;
        }

        if (dest_table_info.isSingleEntry && added_info_map.size > 0) {
            dest_table.removeChild(dest_table.lastElementChild);
            added_info_map.clear();
        }

        // td that contains button : id = "<employee record id>_td_button"
        new_row.cells[n_col - 1].id = employee_record_id + "_td_button";

        dest_table.appendChild(new_row);
        un_hide(new_row);

        let emp_info = JSON.parse(
            $('#' + this_row.id + '_data').val()
        );

        added_info_map.set(employee_record_id, emp_info);
        hide(this_row);

        return emp_info;
    }

    function hide(elememt) {
        elememt.style.display = "none";
    }

    function un_hide(elememt) {
        elememt.style.display = "";
    }
</script>