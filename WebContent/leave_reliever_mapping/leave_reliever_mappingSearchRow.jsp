<%@page pageEncoding="UTF-8" %>
<%@page import="leave_reliever_mapping.*" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%

    EmployeeFlatInfoDTO leaveRequesterDTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.organogramId);
    EmployeeFlatInfoDTO leaveReliever1DTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever1);
    EmployeeFlatInfoDTO leaveReliever2DTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever2);
%>
<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>
<td>
    <%=leaveRequesterDTO.getCommaFormattedInfo(Language)%>
</td>

<td>
    <%=leaveReliever1DTO.getCommaFormattedInfo(Language)%>
</td>

<td>
    <%=leaveReliever2DTO.getCommaFormattedInfo(Language)%>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Leave_reliever_mappingServlet?actionType=getEditPage&ID=<%=leave_reliever_mappingDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=leave_reliever_mappingDTO.iD%>'/></span>
    </div>
</td>