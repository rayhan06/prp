<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="leave_reliever_mapping.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>


<%
    String servletName = "Leave_reliever_mappingServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value = "";
    String Language = LM.getText(LC.LEAVE_RELIEVER_MAPPING_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Leave_reliever_mappingDTO leave_reliever_mappingDTO = Leave_reliever_mappingRepository.getInstance().getLeave_reliever_mappingDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    EmployeeFlatInfoDTO leaveRequesterDTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.organogramId);
    EmployeeFlatInfoDTO leaveReliever1DTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever1);
    EmployeeFlatInfoDTO leaveReliever2DTO = Leave_reliever_mappingDAO.buildFlatDTO(leave_reliever_mappingDTO.leaveReliever2);
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<div class="container">
    <h3><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVE_RELIEVER_MAPPING_ADD_FORMNAME, loginDTO)%>
    </h3>
    <hr>
    <table class="table table-bordered table-striped">
        <thead>
            <th style="width: 15%"></th>
            <th style="width: 30%">
                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%></b>
            </th>
            <th style="width: 30%">
                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DESIGNATION, loginDTO)%></b>
            </th>
            <th style="width: 25%">
                <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_ADD_DEPARTMENT, loginDTO)%></b>
            </th>
        </thead>
        <tbody>
            <tr>
                <td><b><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_ORGANOGRAMID, loginDTO)%></b></td>
                <td>
                    <%=isLanguageEnglish? leaveRequesterDTO.employeeNameEn : leaveRequesterDTO.employeeNameBn%>
                </td>
                <td>
                    <%=isLanguageEnglish? leaveRequesterDTO.organogramNameEn : leaveRequesterDTO.organogramNameBn%>
                </td>
                <td>
                    <%=isLanguageEnglish? leaveRequesterDTO.officeNameEn : leaveRequesterDTO.officeNameBn%>
                </td>
            </tr>

            <tr>
                <td><b><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVERELIEVER1, loginDTO)%></b></td>
                <td><%=isLanguageEnglish? leaveReliever1DTO.employeeNameEn : leaveReliever1DTO.employeeNameBn%></td>
                <td>
                    <%=isLanguageEnglish? leaveReliever1DTO.organogramNameEn : leaveReliever1DTO.organogramNameBn%>
                </td><td>
                    <%=isLanguageEnglish? leaveReliever1DTO.officeNameEn : leaveReliever1DTO.officeNameBn%>
                </td>
            </tr>

            <tr>
                <td><b><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVERELIEVER2, loginDTO)%></b></td>
                <td><%=isLanguageEnglish? leaveReliever2DTO.employeeNameEn : leaveReliever2DTO.employeeNameBn%></td>
                <td>
                    <%=isLanguageEnglish? leaveReliever2DTO.organogramNameEn : leaveReliever2DTO.organogramNameBn%>
                </td>
                <td>
                    <%=isLanguageEnglish? leaveReliever2DTO.officeNameEn : leaveReliever2DTO.officeNameBn%>
                </td>
            </tr>
        </tbody>
    </table>
</div>
