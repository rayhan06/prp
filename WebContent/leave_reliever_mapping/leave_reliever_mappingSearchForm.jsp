<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="leave_reliever_mapping.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.LEAVE_RELIEVER_MAPPING_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Leave_reliever_mappingDAO leave_reliever_mappingDAO = (Leave_reliever_mappingDAO) request.getAttribute("leave_reliever_mappingDAO");


    String navigator2 = SessionConstants.NAV_LEAVE_RELIEVER_MAPPING;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <b><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_ORGANOGRAMID, loginDTO)%>
                </b>
            </th>
            <th>
                <b><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVERELIEVER1, loginDTO)%>
                </b>
            </th>
            <th>
                <b><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVERELIEVER2, loginDTO)%>
                </b>
            </th>

            <th>
                <b><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_SEARCH_LEAVE_RELIEVER_MAPPING_EDIT_BUTTON, loginDTO)%>
                </b>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Leave_reliever_mappingDTO> data = (List<Leave_reliever_mappingDTO>) recordNavigator.list;
            System.out.println("-- -- -- > "+data);
            try {

                if (data != null && data.size()>0) {
                    for (int i = 0; i < data.size(); i++) {
                        Leave_reliever_mappingDTO leave_reliever_mappingDTO = data.get(i);
        %>
                    <tr id='tr_<%=i%>'>
                        <%@include file="leave_reliever_mappingSearchRow.jsp"%>
                    </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			