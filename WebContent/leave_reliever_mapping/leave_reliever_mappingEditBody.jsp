<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="leave_reliever_mapping.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>

<%
    Leave_reliever_mappingDTO leave_reliever_mappingDTO = new Leave_reliever_mappingDTO();
//    leave_reliever_mappingDTO = (Leave_reliever_mappingDTO) request.getAttribute("leave_reliever_mappingDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String actionName;
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVE_RELIEVER_MAPPING_ADD_FORMNAME, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        leave_reliever_mappingDTO = new Leave_reliever_mappingDTO();
    } else {
        leave_reliever_mappingDTO = Leave_reliever_mappingDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    }
    int i = 0;

    String Language = LM.getText(LC.LEAVE_RELIEVER_MAPPING_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String errorMessage = LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_SELECT_THEN_SUBMIT_FROM, loginDTO);


    EmployeeFlatInfoDTO leaveRequesterDTO = (EmployeeFlatInfoDTO) request.getAttribute("requesterDTO");
    EmployeeFlatInfoDTO leaveReliever1DTO = (EmployeeFlatInfoDTO) request.getAttribute("leaveReliever1DTO");
    EmployeeFlatInfoDTO leaveReliever2DTO = (EmployeeFlatInfoDTO) request.getAttribute("leaveReliever2DTO");
%>

<style>
    .error-alert {
        color: red;
        margin: 10px 0;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="leaveRelieverMapping_form" name="bigform" action="Leave_reliever_mappingServlet?actionType=ajax_<%=actionName%>"
              enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>'
                                           value='<%=leave_reliever_mappingDTO.iD%>'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_ORGANOGRAMID, loginDTO)%>
                                        </label>
                                        <div class="col-md-9 " id='organogramId_div_<%=i%>'>
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="organogramId_btn">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                            <div class="error-alert" id="organogramId_error"
                                                 style="display: none">
                                                <%=errorMessage%>
                                            </div>
                                            <input type='hidden' name='organogramId'
                                                   id='organogramId_input'
                                                   value='<%=leave_reliever_mappingDTO.organogramId%>'/>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 15%">
                                                            <b><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 30%">
                                                            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 50%">
                                                            <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 5%"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="organogramId_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-sm delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'organogramId_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%if (leaveRequesterDTO != null) {%>
                                                    <tr>
                                                        <td><%=leaveRequesterDTO.employeeUserName%>
                                                        </td>
                                                        <td><%=isLanguageEnglish ? leaveRequesterDTO.employeeNameEn : leaveRequesterDTO.employeeNameBn%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (leaveRequesterDTO.organogramNameEn + "," + leaveRequesterDTO.officeNameEn)
                                                                    : (leaveRequesterDTO.organogramNameBn + "," + leaveRequesterDTO.officeNameBn)%>
                                                        </td>
                                                        <td id='<%=leaveRequesterDTO.employeeRecordsId%>_td_button'>
                                                            <button type="button"
                                                                    class="btn btn-sm delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'organogramId_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%}%>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVERELIEVER1, loginDTO)%>
                                        </label>
                                        <div class="col-md-9 " id='leaveReliever1_div_<%=i%>'>
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="leaveReliever1_btn">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                            <div class="error-alert" id="leaveReliever1_error"
                                                 style="display: none">
                                                <%=errorMessage%>
                                            </div>
                                            <input type='hidden' name='leaveReliever1'
                                                   id='leaveReliever1_input'
                                                   value='<%=leave_reliever_mappingDTO.leaveReliever1%>'/>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 15%">
                                                            <b><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 30%">
                                                            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 50%">
                                                            <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 5%"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="leaveReliever1_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-sm delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'leaveReliever1_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                        <%if (leaveReliever1DTO != null) {%>
                                                    <tr>
                                                        <td><%=leaveReliever1DTO.employeeUserName%>
                                                        </td>
                                                        <td><%=isLanguageEnglish ? leaveReliever1DTO.employeeNameEn : leaveReliever1DTO.employeeNameBn%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (leaveReliever1DTO.organogramNameEn + "," + leaveReliever1DTO.officeNameEn)
                                                                    : (leaveReliever1DTO.organogramNameBn + "," + leaveReliever1DTO.officeNameBn)%>
                                                        </td>
                                                        <td id='<%=leaveReliever1DTO.employeeRecordsId%>_td_button'>
                                                            <button type="button"
                                                                    class="btn btn-sm delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'leaveReliever1_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                        <%}%>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVERELIEVER2, loginDTO)%>
                                        </label>
                                        <div class="col-md-9 " id='leaveReliever2_div_<%=i%>'>
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="leaveReliever2_btn">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>
                                            <div class="error-alert" id="leaveReliever2_error"
                                                 style="display: none">
                                                <%=errorMessage%>
                                            </div>
                                            <input type='hidden' name='leaveReliever2'
                                                   id='leaveReliever2_input'
                                                   value='<%=leave_reliever_mappingDTO.leaveReliever2%>'/>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 15%">
                                                            <b><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 30%">
                                                            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 50%">
                                                            <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 5%"></th>
                                                    </tr>
                                                    </thead>

                                                    <tbody id="leaveReliever2_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-sm delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'leaveReliever2_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%if (leaveReliever2DTO != null) {%>
                                                    <tr>
                                                        <td><%=leaveReliever2DTO.employeeUserName%>
                                                        </td>
                                                        <td><%=isLanguageEnglish ? leaveReliever2DTO.employeeNameEn : leaveReliever2DTO.employeeNameBn%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (leaveReliever2DTO.organogramNameEn + "," + leaveReliever2DTO.officeNameEn)
                                                                    : (leaveReliever2DTO.organogramNameBn + "," + leaveReliever2DTO.officeNameBn)%>
                                                        </td>
                                                        <td id='<%=leaveReliever2DTO.employeeRecordsId%>_td_button'>
                                                            <button type="button"
                                                                    class="btn btn-sm delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'leaveReliever2_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%}%>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVE_RELIEVER_MAPPING_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                type="button" onclick="submitForm()">
                            <%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVE_RELIEVER_MAPPING_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    const form = $('#leaveRelieverMapping_form');

    function submitForm() {
        buttonStateChangeFunction(true);
        if (isMandatoryFieldsSet()) {
            submitAjaxForm('leaveRelieverMapping_form');
        
        } else {
            buttonStateChangeFunction(false);
        }
    }

    function buttonStateChangeFunction(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function isMandatoryFieldsSet() {
        let isAllValid = true;
        let upperMostErrorElem = null;

        if (leaveReliever2_info_map.size !== 0) {
            $('#leaveReliever2_input').val(
                leaveReliever2_info_map.values().next().value.organogramId
            );
            $('#leaveReliever2_error').hide();
        } else {
            isAllValid = false;
            upperMostErrorElem = document.getElementById('leaveReliever2_error');
            $('#leaveReliever2_error').show();
        }

        if (leaveReliever1_info_map.size !== 0) {
            $('#leaveReliever1_input').val(
                leaveReliever1_info_map.values().next().value.organogramId
            );
            $('#leaveReliever1_error').hide();
        } else {
            isAllValid = false;
            upperMostErrorElem = document.getElementById('leaveReliever1_error');
            $('#leaveReliever1_error').show();
        }

        if (organogramId_info_map.size !== 0) {
            $('#organogramId_input').val(
                organogramId_info_map.values().next().value.organogramId
            );
            $('#organogramId_error').hide();
        } else {
            isAllValid = false;
            upperMostErrorElem = document.getElementById('organogramId_error');
            $('#organogramId_error').show();
        }
        if (upperMostErrorElem) {
            // console.log(upperMostErrorElem);
            upperMostErrorElem.scrollIntoView();
        }
        return isAllValid;
    }

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';
    // modal trigger button
    $('#organogramId_btn').on('click', function () {
        modal_button_dest_table = 'organogramId_table';
        $('#search_emp_modal').modal();
    });

    $('#leaveReliever1_btn').on('click', function () {
        modal_button_dest_table = 'leaveReliever1_table';
        $('#search_emp_modal').modal();
    });

    $('#leaveReliever2_btn').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'leaveReliever2_table';
        $('#search_emp_modal').modal();
    });

    organogramId_info_map = new Map(
        <%if(leaveRequesterDTO != null){%>
        <%=EmployeeSearchModalUtil.initJsMapSingleEmployee(
            leaveRequesterDTO.employeeRecordsId,
            leaveRequesterDTO.officeUnitId,
            leaveRequesterDTO.organogramId
        )%>
        <%}%>
    );

    leaveReliever1_info_map = new Map(
        <%if(leaveReliever1DTO != null){%>
        <%=EmployeeSearchModalUtil.initJsMapSingleEmployee(
            leaveReliever1DTO.employeeRecordsId,
            leaveReliever1DTO.officeUnitId,
            leaveReliever1DTO.organogramId
        )%>
        <%}%>
    );

    leaveReliever2_info_map = new Map(
        <%if(leaveReliever2DTO != null){%>
        <%=EmployeeSearchModalUtil.initJsMapSingleEmployee(
            leaveReliever2DTO.employeeRecordsId,
            leaveReliever2DTO.officeUnitId,
            leaveReliever2DTO.organogramId
        )%>
        <%}%>
    );

    table_name_to_collcetion_map = new Map([
        ['organogramId_table', {
            info_map: organogramId_info_map,
            isSingleEntry: true,
            employeeSearchApiUrl: "EmployeeAssignServlet?actionType=getEmployeeListFilteredByLeaveReliever",
            callBackFunction: function (empInfo) {
                // console.log('callBackFunction called with latest added emp info JSON');
                // console.log(empInfo);
                $('#organogramId_error').hide();
            }
        }],
        ['leaveReliever1_table', {
            info_map: leaveReliever1_info_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                // console.log('callBackFunction called with latest added emp info JSON');
                // console.log(empInfo);
                $('#leaveReliever1_error').hide();
            }
        }],
        ['leaveReliever2_table', {
            info_map: leaveReliever2_info_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                // console.log('callBackFunction called with latest added emp info JSON');
                // console.log(empInfo);
                $('#leaveReliever2_error').hide();
            }
        }]
    ]);
</script>






