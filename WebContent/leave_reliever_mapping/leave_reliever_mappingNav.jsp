<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    if (rn == null) {
        rn = (RecordNavigator) request.getAttribute("recordNavigator");
    }
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";

    int row = 0;
    int pagination_number = 0;
%>
<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">

                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_ORGANOGRAMID, loginDTO)%>
                        </label>

                        <div class="col-md-8">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="organogramId_btn">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <input type="hidden" id="organogram_id" name="organogram_id" onChange='setSearchChanged()'>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <tbody id="organogramId_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-sm delete-trash-btn"
                                                onclick="remove_containing_row(this,'organogramId_table');">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVERELIEVER1, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="leaveReliever1_btn">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <input type="hidden" id="leave_reliever_1" name="leave_reliever_1">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <tbody id="leaveReliever1_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-sm delete-trash-btn"
                                                onclick="remove_containing_row(this,'leaveReliever1_table');">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.LEAVE_RELIEVER_MAPPING_ADD_LEAVERELIEVER2, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="leaveReliever2_btn">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <input type="hidden" id="leave_reliever_2" name="leave_reliever_2">
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped text-nowrap">
                                <tbody id="leaveReliever2_table">
                                <tr style="display: none;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <button type="button" class="btn btn-sm delete-trash-btn"
                                                onclick="remove_containing_row(this,'leaveReliever2_table');">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';
    // modal trigger button
    $('#organogramId_btn').on('click', function () {
        modal_button_dest_table = 'organogramId_table';
        $('#search_emp_modal').modal();
    });

    $('#leaveReliever1_btn').on('click', function () {
        modal_button_dest_table = 'leaveReliever1_table';
        $('#search_emp_modal').modal();
    });

    $('#leaveReliever2_btn').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'leaveReliever2_table';
        $('#search_emp_modal').modal();
    });

    organogramId_info_map = new Map();
    leaveReliever1_info_map = new Map();
    leaveReliever2_info_map = new Map();

    table_name_to_collcetion_map = new Map([
        ['organogramId_table', {
            info_map: organogramId_info_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                console.log('callBackFunction called with latest added emp info JSON');
                console.log(empInfo);
            }
        }],
        ['leaveReliever1_table', {
            info_map: leaveReliever1_info_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                console.log('callBackFunction called with latest added emp info JSON');
                console.log(empInfo);
            }
        }],
        ['leaveReliever2_table', {
            info_map: leaveReliever2_info_map,
            isSingleEntry: true,
            callBackFunction: function (empInfo) {
                console.log('callBackFunction called with latest added emp info JSON');
                console.log(empInfo);
            }
        }]
    ]);

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        // var params = 'AnyField=' + document.getElementById('anyfield').value;
        let params = '';

        if (organogramId_info_map.size !== 0) {
            $('#organogram_id').val(
                organogramId_info_map.values().next().value.organogramId
            );
        } else {
            $('#organogram_id').val('');
        }
        params += '&organogram_id=' + $('#organogram_id').val();

        if (leaveReliever1_info_map.size !== 0) {
            $('#leave_reliever_1').val(
                leaveReliever1_info_map.values().next().value.organogramId
            );
        } else {
            $('#leave_reliever_1').val('');
        }
        params += '&leave_reliever_1=' + $('#leave_reliever_1').val();

        if (leaveReliever2_info_map.size !== 0) {
            $('#leave_reliever_2').val(
                leaveReliever2_info_map.values().next().value.organogramId
            );
        } else {
            $('#leave_reliever_2').val('');
        }
        params += '&leave_reliever_2=' + $('#leave_reliever_2').val();

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);
    }
</script>

