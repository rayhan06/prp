<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="dpm.*"%>
<%@page import="employee_offices.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>


<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="drug_information.*"%>

<%
DpmDTO dpmDTO = new DpmDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	dpmDTO = DpmDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = dpmDTO;
String tableName = "dpm";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.DPM_ADD_DPM_ADD_FORMNAME, loginDTO);
String servletName = "DpmServlet";
boolean isLangEng = Language.equalsIgnoreCase("english");
long approverOrgId = WorkflowController.getOrganogramIDFromErID(dpmDTO.approverErid);
Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="DpmServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=dpmDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_PATIENT_ID, loginDTO)%>															
															</label>
                                                            <div class="col-8">
																<button type="button"
				                                                        class="btn text-white shadow form-control btn-border-radius"
				                                                        style="background-color: #4a87e2"
				                                                        onclick="addEmployee()"
				                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
				                                                </button>
				                                                <table class="table table-bordered table-striped">
				                                                    <tbody id="employeeToSet">
				                                                    	<tr>
				                                                    		<td><%=WorkflowController.getNameFromEmployeeRecordID(dpmDTO.patientErid) %></td>
				                                                    	<tr>
				                                                    </tbody>
				                                                </table>
				                                                <input type='hidden' class='form-control'  name='patientErid' id = 'patientErid' value='<%=dpmDTO.patientErid%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=isLangEng?"Approver":"অনুমোদনকারী"%>		
															</label>
                                                            <div class="col-8">
																<select  class='form-control' name='approverOrgId'
											                          id='approverOrgId' 
											                          tag='pb_html' >
											                          <option value="-1"><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
											                          <%
											                          Set<Long> emps = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.MEDICAL_ADMIN_ROLE);
											                          
											                       
											                          for(Long em: emps)
											                          {
											                          %>
											                          <option value = "<%=em%>" <%=em == approverOrgId?"selected":""%>>
											                          <%=WorkflowController.getNameFromOrganogramId(em, Language)%>
											                          </option>
											                          <%
											                          }
											                          %>
											                    </select>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DPM_ADD_PURCHASEMETHODCAT, loginDTO)%>															
															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='purchaseMethodCat' id = 'purchaseMethodCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatDAO.getOptions( Language, "purchase_method", dpmDTO.purchaseMethodCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DPM_ADD_STORENAME, loginDTO)%>															
															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='storeName' id = 'storeName_text_<%=i%>' value='<%=dpmDTO.storeName%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DPM_ADD_STOREADDRESS, loginDTO)%>															
															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='storeAddress' id = 'storeAddress_text_<%=i%>' value='<%=dpmDTO.storeAddress%>'   tag='pb_html'/>							
															</div>
                                                      </div>	
                                                      
                                                      <div class="form-group row">
		                                                       <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_DATE, loginDTO)%></label>
		                                                            
	                                                            <div class="col-md-8">
			                                                        <jsp:include page="/date/date.jsp">
		                                                            <jsp:param name="DATE_ID"
		                                                                       value="entryDate_date_js"></jsp:param>
		                                                            <jsp:param name="LANGUAGE"
		                                                                       value="<%=Language%>"></jsp:param>
		                                                       		 </jsp:include>
		
			                                                        <input type='hidden'
			                                                               class='form-control formRequired datepicker'
			                                                               readonly="readonly" data-label="Document Date"
			                                                               id='entryDate_date_<%=i%>' name='entryDate' value=<%
																				String formatted_entryDate = dateFormat.format(new Date(dpmDTO.entryDate));
																			%>
			                                                                '<%=formatted_entryDate%>'/>
			                                                    </div>
				                                                    
		                                                           
		                                                </div>								
														
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.DPM_ADD_DPM_DETAILS, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.DPM_ADD_DPM_DETAILS_DRUGINFORMATIONID, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_QUANTITY, loginDTO)%></th>
										<th><%=LM.getText(LC.HM_COST, loginDTO)%></th>
										<th><%=LM.getText(LC.DPM_ADD_DPM_DETAILS_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-DpmDetails">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(DpmDetailsDTO dpmDetailsDTO: dpmDTO.dpmDetailsDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "DpmDetails_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dpmDetails.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=dpmDetailsDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dpmDetails.dpmId' id = 'dpmId_hidden_<%=childTableStartingID%>' value='<%=dpmDetailsDTO.dpmId%>' tag='pb_html'/>
									</td>
									<td >


													<%
													if(dpmDetailsDTO.drugInformationId >= 0)
													{
														Drug_informationDTO drDTO = drug_informationDAO.getDrugInfoDetailsById(dpmDetailsDTO.drugInformationId);
				                                        if(drDTO != null)
				                                        {
				                                        	%>
				                                        	<%=drDTO.drugText%>
				                                        	<%
				                                        	
				                                        }
													}
													
													%>
														<input type='hidden' class='form-control'  name='dpmDetails.drugInformationId' id = 'drugInformationId_hidden_<%=childTableStartingID%>' value='<%=dpmDetailsDTO.drugInformationId%>' tag='pb_html'/>
									</td>
									<td>										

											<%
												value = "";
												if(dpmDetailsDTO.quantity != -1)
												{
												value = dpmDetailsDTO.quantity + "";
												}
											%>		
											<input type='number' class='form-control'  name='dpmDetails.quantity' id = 'quantity_number_<%=childTableStartingID%>' value='<%=value%>' min = '1' tag='pb_html'>		
									</td>
									
									<td>											
											<input type='number' class='form-control'  name='dpmDetails.totalPrice' id = 'totalPrice_number_<%=childTableStartingID%>' value='<%=dpmDetailsDTO.totalPrice%>' min = '0' step ='0.01' tag='pb_html'>		
									</td>
									
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-DpmDetails"
											name="add-moreDpmDetails"
											type="button"
											onclick="childAdded(event, 'DpmDetails')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-DpmDetails"
											name="removeDpmDetails"
											type="button"
											onclick="childRemoved(event, 'DpmDetails')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%DpmDetailsDTO dpmDetailsDTO = new DpmDetailsDTO();%>
					
							<template id="template-DpmDetails" >						
								<tr>
									<td style="display: none;">

														<input type='hidden' class='form-control'  name='dpmDetails.iD' id = 'iD_hidden_' value='<%=dpmDetailsDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dpmDetails.dpmId' id = 'dpmId_hidden_' value='<%=dpmDetailsDTO.dpmId%>' tag='pb_html'/>
									</td>
									<td >


												<div class="input-group">
											        <input
											          class="form-control py-2  border searchBox w-auto"
											          type="search"
											          placeholder="Search"
											          name='suggested_drug'
                                                      id='suggested_drug_'
                                                      tag='pb_html'
                                                      onkeyup="getDrugs(this.id)"
                                                      autocomplete="off"
											        />
											        
											      </div>
											      <div
											        id="searchSgtnSection_"
											        class="search-sgtn-section shadow-sm bg-white"
													tag='pb_html'
											      >
											        <div class="">
											          <ul class="px-0" style="list-style: none" id="drug_ul_" tag='pb_html'>
											          </ul>
											        </div>
											      </div>
											      
											      <input name='formStr'
                                                   id='formStr_' type="hidden"
                                                   tag='pb_html'
                                                   value=''></input>



												<input type='hidden' class='form-control'  name='dpmDetails.drugInformationId' id = 'drugInformationType_select_' value='<%=dpmDetailsDTO.drugInformationId%>' tag='pb_html'/>
									</td>
									<td>





												<%
													value = "";
													if(dpmDetailsDTO.quantity != -1)
													{
													value = dpmDetailsDTO.quantity + "";
													}
												%>		
												<input type='number' class='form-control'  name='dpmDetails.quantity' id = 'quantity_number_' value='<%=value%>' min = '1'  tag='pb_html'>		
									</td>
									<td>											
											<input type='number' class='form-control'  name='dpmDetails.totalPrice' id = 'totalPrice_number_' value='<%=dpmDetailsDTO.totalPrice%>' min = '0' step ='0.01' tag='pb_html'>		
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dpmDetails.insertedByErId' id = 'insertedByErId_hidden_' value='<%=dpmDetailsDTO.insertedByErId%>' tag='pb_html'/>
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.DPM_ADD_DPM_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.DPM_ADD_DPM_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	if($("#patientErid").val() == -1)
	{
		toastr.error("Patient must be selected");
		return false;
	}
	var entryDate = getDateStringById('entryDate_date_js', 'DD/MM/YYYY');
    $("#entryDate_date_<%=i%>").val(entryDate);

	
	submitAddForm2();
	return false;
}

function drugClicked(rowId, drugText, drugId, form, stock)
{
	console.log("drugClicked with " + rowId + ", drugId = " + drugId + ", form = " + form);
	$("#suggested_drug_" + rowId).val(drugText);
	$("#suggested_drug_" + rowId).attr("style", "width: " + ($("#suggested_drug_" + rowId).val().length + 1)*8 + "px !important");


	$("#drugInformationType_select_" + rowId).val(drugId);

	
	$("#drug_ul_" + rowId).html("");

}
function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "DpmServlet");	
}

function init(row)
{
	setDateByTimestampAndId('entryDate_date_js', '<%=dpmDTO.entryDate%>');
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function processRowsWhileAdding(childName)
{
	if(childName == "DpmDetails")
	{			
	}
}

function patient_inputted(userName, orgId, erId) {
	
	console.log("patient_inputted " + userName);
	$("#patientErid").val(erId);
}

</script>






