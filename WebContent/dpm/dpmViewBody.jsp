

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="dpm.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>

<%@ page import="geolocation.*"%>


<%@ page import="drug_information.*"%>
<%
String servletName = "DpmServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
DpmDTO dpmDTO = DpmDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = dpmDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>
<%
boolean isLangEng = Language.equalsIgnoreCase("english");
Drug_informationDAO drug_informationDAO = new Drug_informationDAO();
%>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.DPM_ADD_DPM_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.DPM_ADD_DPM_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_INSERTED_BY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(simpleDateFormat.format(new Date(dpmDTO.insertionDate)), Language)%>
											<br>
											<%=WorkflowController.getNameFromEmployeeRecordID(dpmDTO.insertedByErId, isLangEng)%>
											</td>
                                    </div>
                                </div>
                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_DATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(simpleDateFormat.format(new Date(dpmDTO.entryDate)), Language)%>
										
											</td>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=WorkflowController.getNameFromEmployeeRecordID(dpmDTO.patientErid, isLangEng)%>
											<br>
											<%=WorkflowController.getUserNameFromErId(dpmDTO.patientErid, Language)%>
                                    </div>
                                </div>
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=isLangEng?"Approver":"অনুমোদনকারী"%>
                                    </label>
                                    <div class="col-8">
										<%=WorkflowController.getNameFromEmployeeRecordID(dpmDTO.approverErid, isLangEng)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DPM_ADD_PURCHASEMETHODCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "purchase_method", dpmDTO.purchaseMethodCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DPM_ADD_STORENAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=dpmDTO.storeName%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DPM_ADD_STOREADDRESS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
										<%=dpmDTO.storeAddress%>
                                    </div>
                                </div>
			

		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.DPM_ADD_DPM_DETAILS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.HM_SL, loginDTO)%></th>
								<th><%=LM.getText(LC.DPM_ADD_DPM_DETAILS_DRUGINFORMATIONID, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_QUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_COST, loginDTO)%></th>
							</tr>
							<%
                        	DpmDetailsDAO dpmDetailsDAO = DpmDetailsDAO.getInstance();
                         	List<DpmDetailsDTO> dpmDetailsDTOs = (List<DpmDetailsDTO>)dpmDetailsDAO.getDTOsByParent("dpm_id", dpmDTO.iD);
                         	int j = 1;
                         	int count = 0;
                         	double sum = 0;
                         	
                         	for(DpmDetailsDTO dpmDetailsDTO: dpmDetailsDTOs)
                         	{
                         		%>
                         			<tr>
                         				<td><%=Utils.getDigits(j++, Language)%></td>
                         				<td>
											<%
											if(dpmDetailsDTO.drugInformationId >= 0)
											{
												Drug_informationDTO drDTO = drug_informationDAO.getDrugInfoDetailsById(dpmDetailsDTO.drugInformationId);
		                                        if(drDTO != null)
		                                        {
		                                        	%>
		                                        	<%=drDTO.drugText%>
		                                        	<%
		                                        	
		                                        }
											}
											%>
										</td>
										<td>
											<%=Utils.getDigits(dpmDetailsDTO.quantity, Language)%>
											<%
											count+= dpmDetailsDTO.quantity;
											%>
										</td>
										<td>
											<%=Utils.getDigits(dpmDetailsDTO.totalPrice, Language)%>
											<%
											sum += dpmDetailsDTO.totalPrice;
											%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
                        	<tr >
                        		<td style="font-weight: bold;"><%=isLangEng?"Total":"মোট"%></td>
                        		<td></td>
                        		<td style="font-weight: bold;"><%=Utils.getDigits(count, Language)%></td>
                        		<td style="font-weight: bold;"><%=Utils.getDigits(sum, Language)%></td>
                        	</tr>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>