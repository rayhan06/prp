
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="dpm.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page contentType="text/html;charset=utf-8" %>


<%
String navigator2 = "navDPM";
String servletName = "DpmServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
<%
boolean isLangEng = Language.equalsIgnoreCase("english");
%>			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_SL, loginDTO)%>
            					</th>
								<th><%=LM.getText(LC.HM_INSERTED_BY, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%></th>
								<th><%=isLangEng?"Approver":"অনুমোদনকারী"%></th>
								<th><%=LM.getText(LC.DPM_ADD_PURCHASEMETHODCAT, loginDTO)%></th>
								<th><%=isLangEng?"Pharmacy":"ফার্মাসি"%></th>
								
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.DPM_SEARCH_DPM_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<DpmDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											DpmDTO dpmDTO = (DpmDTO) data.get(i);
																																
											
											%>
											<tr>
								
											<td>
												<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language) %>
											</td>
											<td>
											
											<%=Utils.getDigits(simpleDateFormat.format(new Date(dpmDTO.insertionDate)), Language)%>
											<br>
											<%=WorkflowController.getNameFromEmployeeRecordID(dpmDTO.insertedByErId, isLangEng)%>
											</td>
											
											<td>
											<%=Utils.getDigits(simpleDateFormat.format(new Date(dpmDTO.entryDate)), Language)%>
											</td>
		
											<td>
											<%=WorkflowController.getNameFromEmployeeRecordID(dpmDTO.patientErid, isLangEng)%>
											<br>
											<%=WorkflowController.getUserNameFromErId(dpmDTO.patientErid, Language)%>
											</td>
		
											<td>
											<%=WorkflowController.getNameFromEmployeeRecordID(dpmDTO.approverErid, isLangEng)%>
											
											</td>
		
											<td>
											<%=CatRepository.getInstance().getText(Language, "purchase_method", dpmDTO.purchaseMethodCat)%>
											</td>
		
											<td>
											<%=dpmDTO.storeName%><br><%=dpmDTO.storeAddress%>
											</td>
		
		
	
											<%CommonDTO commonDTO = dpmDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=dpmDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			