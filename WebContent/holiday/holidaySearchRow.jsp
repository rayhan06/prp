<%@page pageEncoding="UTF-8" %>

<%@page import="holiday.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.HOLIDAY_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_HOLIDAY;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
HolidayDTO holidayDTO = (HolidayDTO)request.getAttribute("holidayDTO");
CommonDTO commonDTO = holidayDTO;
String servletName = "HolidayServlet";


System.out.println("holidayDTO = " + holidayDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


HolidayDAO holidayDAO = (HolidayDAO)request.getAttribute("holidayDAO");


String Options = "";
boolean formSubmit = false;

%>

											
		
											
											<td id = '<%=i%>_currentDay'>
											<%
											value = holidayDTO.currentDay + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_isHoliday'>
											<%
											value = holidayDTO.isHoliday + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_description'>
											<%
											value = holidayDTO.description + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_holidayCat'>
											<%
											value = holidayDTO.holidayCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "holiday", holidayDTO.holidayCat);
											%>											
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_officeStartTime'>
											<%
											value = holidayDTO.officeStartTime + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_officeEndTime'>
											<%
											value = holidayDTO.officeEndTime + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
		
											
		
	

											<td>
											<a href='HolidayServlet?actionType=view&ID=<%=holidayDTO.iD%>'>View</a>
											
											<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
											
											<div class='modal fade' id='viedFileModal_<%=i%>'>
											  <div class='modal-dialog modal-lg' role='document'>
											    <div class='modal-content'>
											      <div class='modal-body'>
											        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
											          <span aria-hidden='true'>&times;</span>
											        </button>											        
											        
											        <object type='text/html' data='HolidayServlet?actionType=view&modal=1&ID=<%=holidayDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
											        
											      </div>
											    </div>
											  </div>
											</div>
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
												<a href="#"  onclick='fixedToEditable(<%=i%>,"", "<%=holidayDTO.iD%>", <%=isPermanentTable%>, "HolidayServlet")'><%=LM.getText(LC.HOLIDAY_SEARCH_HOLIDAY_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span id='chkEdit' ><input type='checkbox' name='ID' value='<%=holidayDTO.iD%>'/></span>
												</div>
											</td>
																						
											

