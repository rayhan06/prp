<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="holidays.Holiday" %>
<%@ page import="holiday.HolidayDTO" %>

<%

    Holiday holiday = new Holiday();
    String holidayArray = holiday.getHolidayForDNCalendar();
    //String notesArray = holiday.getNotesForDNCalendar();
    String note = "";


    HolidayDTO holidayDTO;
    holidayDTO = (HolidayDTO) request.getAttribute("holidayDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (holidayDTO == null) {
        holidayDTO = new HolidayDTO();

    }
    System.out.println("holidayDTO = " + holidayDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.HOLIDAY_EDIT_HOLIDAY_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.HOLIDAY_ADD_HOLIDAY_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="HolidayServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div id="dncalendar-container"></div>
            </div>
        </form>
    </div>
</div>

<%--<div class="box box-primary">--%>
<%--    <div class="box-header with-border">--%>
<%--        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>--%>
<%--        </h3>--%>
<%--    </div>--%>
<%--    <div class="box-body">--%>
<%--        <form class="form-horizontal"--%>
<%--              action="HolidayServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
<%--              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"--%>
<%--              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>
<%--            <div class="form-body">--%>
<%--                <div id="dncalendar-container"></div>--%>
<%--            </div>--%>
<%--        </form>--%>
<%--    </div>--%>
<%--</div>--%>
<script src="nicEdit.js" type="text/javascript"></script>
<%@include file="/dncalendar/dncalendarJs.jsp" %>
<%@include file="/dncalendar/dncalendarCss.jsp" %>
<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "HolidayServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>
<script type="text/javascript">
    var note2, note2Bn;

    function toggleHoliday(holiday, note, thisClass) {
        $.ajax({
            type: 'post',
            url: 'HolidayAjaxCallServlet',
            dataType: 'json',
            data: {
                "actionType": "toggle",
                "date": holiday,
                "note": note
            },
            async: false,
            success: function (response, note) {
                console.log("----------- response is -------------------");
                console.log(response);
                if (1 == response) {
                    thisClass.addClass("holiday");
                    //thisClass.removeClass("default-date");
                } else if (0 == response) {
                    //thisClass.addClass("default-date");
                    thisClass.removeClass("holiday");
                } else if (-1 == response) {
                    //thisClass.addClass("default-date");
                }
            },
            error: function (xhrObject) {
                console.log(xhrObject);
                console.log(xhrObject.responseText);
            }
        });
    }

    function noteHoliday(holiday) {
        $.ajax({
            type: 'post',
            url: 'HolidayAjaxCallServlet',
            dataType: 'json',
            data: {
                "actionType": "check_status",
                "date": holiday
            },
            async: false,
            success: function (response) {
                console.log("----------- response is -------------------");
                console.log(response);
                if (response == -1 || response == 0) {
                    note2 = prompt("Holiday Description : ", "");
                    // note2Bn = prompt("ছুটির বিবরণ : ", "");
                }
            },
            error: function (xhrObject) {
                console.log(xhrObject);
                console.log(xhrObject.responseText);
            }
        });
    }

    $(document).ready(function () {
        var my_calendar = $("#dncalendar-container").dnCalendar({
            dataTitles: {defaultDate: 'default', today: 'Today'},
            notes: <%=holiday.getNotesForDNCalendar()%>,
            showNotes: true,
            dayClick: function (date, view) {
                //$(this).addClass("holiday");
                //alert(date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear());
                noteHoliday(date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear());
                toggleHoliday(date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear(), note2, $(this));
            },
            holidays: <%=holidayArray%>
        });

        // init calendar
        my_calendar.build();

        // update calendar
        my_calendar.update({
            minDate: "2000-01-01"
        });
    });

</script>