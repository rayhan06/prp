<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="holiday.HolidayDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
HolidayDTO holidayDTO = (HolidayDTO)request.getAttribute("holidayDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(holidayDTO == null)
{
	holidayDTO = new HolidayDTO();
	
}
System.out.println("holidayDTO = " + holidayDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.HOLIDAY_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=holidayDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_currentDay'>")%>
			
	
	<div class="form-inline" id = 'currentDay_div_<%=i%>'>
		<input type='text' class='form-control'  name='currentDay' id = 'currentDay_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + holidayDTO.currentDay + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isHoliday'>")%>
			
	
	<div class="form-inline" id = 'isHoliday_div_<%=i%>'>
		<input type='text' class='form-control'  name='isHoliday' id = 'isHoliday_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + holidayDTO.isHoliday + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_description'>")%>
			
	
	<div class="form-inline" id = 'description_div_<%=i%>'>
		<input type='text' class='form-control'  name='description' id = 'description_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + holidayDTO.description + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_holidayCat'>")%>
			
	
	<div class="form-inline" id = 'holidayCat_div_<%=i%>'>
		<select class='form-control'  name='holidayCat' id = 'holidayCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "holiday", holidayDTO.holidayCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "holiday", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeStartTime'>")%>
			
	
	<div class="form-inline" id = 'officeStartTime_div_<%=i%>'>
		<input type='text' class='form-control'  name='officeStartTime' id = 'officeStartTime_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + holidayDTO.officeStartTime + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_officeEndTime'>")%>
			
	
	<div class="form-inline" id = 'officeEndTime_div_<%=i%>'>
		<input type='text' class='form-control'  name='officeEndTime' id = 'officeEndTime_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + holidayDTO.officeEndTime + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + holidayDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=holidayDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='HolidayServlet?actionType=view&ID=<%=holidayDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='HolidayServlet?actionType=view&modal=1&ID=<%=holidayDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	