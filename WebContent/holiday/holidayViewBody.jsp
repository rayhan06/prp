

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="holiday.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.HOLIDAY_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
HolidayDAO holidayDAO = new HolidayDAO("holiday");
HolidayDTO holidayDTO = (HolidayDTO)holidayDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
%>


<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title">Holiday Details</h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5>Holiday</h5>
                    </div>
			



			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.HOLIDAY_EDIT_CURRENTDAY, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="currentDay">
						
											<%
											value = holidayDTO.currentDay + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.HOLIDAY_EDIT_ISHOLIDAY, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="isHoliday">
						
											<%
											value = holidayDTO.isHoliday + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.HOLIDAY_EDIT_DESCRIPTION, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="description">
						
											<%
											value = holidayDTO.description + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.HOLIDAY_EDIT_HOLIDAYCAT, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="holidayCat">
						
											<%
											value = holidayDTO.holidayCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "holiday", holidayDTO.holidayCat);
											%>											
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.HOLIDAY_EDIT_OFFICESTARTTIME, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="officeStartTime">
						
											<%
											value = holidayDTO.officeStartTime + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.HOLIDAY_EDIT_OFFICEENDTIME, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="officeEndTime">
						
											<%
											value = holidayDTO.officeEndTime + "";
											%>
														
											<%=value%>
				
			
						
                        </label>
                    </div>

				


			
			
			
		


			</div>	

               


        </div>