<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page contentType="text/html;charset=utf-8"%>
<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.SOFTWARE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.SOFTWARE_REPORT_WHERE_SOFTWARECAT, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='softwareCat' id = 'softwareCat' onchange="getSubCat(this.value)">		
						<%		
						Options = CatDAO.getOptions(Language, "software", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=Language.equalsIgnoreCase("english")?"Software Version":"সফটওয়্যার ভার্শন"%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='softwareSubCat' id = 'softwareSubCat' >		
						<option value = ""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.SOFTWARE_REPORT_WHERE_HASLICENSE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='hasLicense' id = 'hasLicense' >		
						<option value = ""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
						<option value = "1"><%=Language.equalsIgnoreCase("english")?"Yes":"হ্যাঁ"%></option>
						<option value = "0"><%=Language.equalsIgnoreCase("english")?"No":"না"%></option>
					</select>						
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}
function getSubCat(cat)
{
	console.log(" cat = " + cat);
	var qs = "Software_typeServlet?actionType=getSubTypeForReport&cat=" + cat;
	fillSelect("softwareSubCat", qs);
}
</script>