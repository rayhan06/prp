
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="early_out.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navEARLY_OUT";
String servletName = "Early_outServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead class="text-nowrap">
							<tr>
								<th><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_SUPERIOR_EMPLOYEE, loginDTO)%></th>								
								<th><%=LM.getText(LC.EARLY_OUT_ADD_LEAVEDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.EARLY_OUT_ADD_STARTTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.EARLY_OUT_ADD_ENDTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.EARLY_OUT_ADD_REASON, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<%
								if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE)
								{
								%>								
								<th><%=LM.getText(LC.EARLY_OUT_SEARCH_EARLY_OUT_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								<%
								}
								%>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Early_outDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Early_outDTO early_outDTO = (Early_outDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											
											<%
											value = WorkflowController.getNameFromOrganogramId(early_outDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(early_outDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(early_outDTO.organogramId, Language);
											%>											
				
											<%=Utils.getDigits(value, Language)%>
											
											<%
											value = early_outDTO.userId + "";
											%>
				
											<br><%=Utils.getDigits(value, Language)%>
				
			
											</td>
											
											<td>
											
											<%
											value = WorkflowController.getNameFromOrganogramId(early_outDTO.superiorOrgId, Language) + ", " + WorkflowController.getOrganogramName(early_outDTO.superiorOrgId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(early_outDTO.superiorOrgId, Language);
											%>											
				
											<%=Utils.getDigits(value, Language)%>
											
											<%
											UserDTO superiorDTO = UserRepository.getUserDTOByOrganogramID(early_outDTO.superiorOrgId);
											if(superiorDTO != null)
											{
											%>
				
											<br><%=Utils.getDigits(superiorDTO.userName, Language)%>
											<%
											}
											%>
				
			
											</td>
		
										
		
											<td>
											
											<%=Utils.getDigits(simpleDateFormat.format(new Date(early_outDTO.leaveDate)), Language)%> - 
											<%=Utils.getDigits(simpleDateFormat.format(new Date(early_outDTO.endDate)), Language)%>
				
			
											</td>
		
											<td>
										
				
											<%=Utils.getDigits(TimeConverter.hourMinuteFormat.format(new Date(early_outDTO.startTime)), Language)%>
				
			
											</td>
		
											<td>
											<%=Utils.getDigits(TimeConverter.hourMinuteFormat.format(new Date(early_outDTO.endTime)), Language)%>
				
			
											</td>
		
											<td>
											<%
											value = early_outDTO.reason + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = early_outDTO; %>
											<td>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											</td>
											
											<%
											if(userDTO.roleID == SessionConstants.ADMIN_ROLE || userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE)
											{
											%>
											<td>
											    <%
											        if (commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {
											    %>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow btn-border-radius text-white"
											            style="background-color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-edit"></i>
											    </button>
											    <%
											        }
											    %>
											</td>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=early_outDTO.iD%>'/></span>
												</div>
											</td>
											<%
											}
											%>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			