<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="early_out.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Early_outDTO early_outDTO = new Early_outDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        early_outDTO = Early_outDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = early_outDTO;
    String tableName = "early_out";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.EARLY_OUT_ADD_EARLY_OUT_ADD_FORMNAME, loginDTO);
    String servletName = "Early_outServlet";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Early_outServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=early_outDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.EARLY_OUT_ADD_LEAVEDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "leaveDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='leaveDate' id='leaveDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(early_outDTO.leaveDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_END_DATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "endDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='endDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(early_outDTO.endDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.EARLY_OUT_ADD_STARTTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "startTime_js_" + i;%>
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' value="<%=early_outDTO.startTime%>" name='startTime'
                                                   id='startTime_time_<%=i%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.EARLY_OUT_ADD_ENDTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "endTime_js_" + i;%>
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' value="<%=early_outDTO.endTime%>" name='endTime'
                                                   id='endTime_time_<%=i%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.EARLY_OUT_ADD_REASON, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <textarea class='form-control' name='reason' id='reason_text_<%=i%>'
                                                      tag='pb_html'><%=early_outDTO.reason%></textarea>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>' value='<%=early_outDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=early_outDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=early_outDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>' value='<%=early_outDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=early_outDTO.lastModifierUser%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=early_outDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=early_outDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.EARLY_OUT_ADD_EARLY_OUT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.EARLY_OUT_ADD_EARLY_OUT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        preprocessDateBeforeSubmitting('leaveDate', row);
        preprocessDateBeforeSubmitting('endDate', row);
        preprocessTimeBeforeSubmitting('startTime', row);
        preprocessTimeBeforeSubmitting('endTime', row);

        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Early_outServlet");
    }

    function init(row) {

        setDateByStringAndId('leaveDate_js_' + row, $('#leaveDate_date_' + row).val());
        setDateByStringAndId('endDate_js_' + row, $('#endDate_date_' + row).val());
        setTimeById('startTime_js_' + row, $('#startTime_time_' + row).val(), true);
        setTimeById('endTime_js_' + row, $('#endTime_time_' + row).val(), true);


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






