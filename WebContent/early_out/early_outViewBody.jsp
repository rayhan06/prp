

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="early_out.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@page import="user.*"%>




<%
String servletName = "Early_outServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Early_outDTO early_outDTO = Early_outDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = early_outDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EARLY_OUT_ADD_EARLY_OUT_ADD_FORMNAME, loginDTO)%>
                </h3>
                <div class="ml-auto mr-3">
				    <button type="button" class="btn" id='printer2'
				            onclick="location.href='Early_outServlet?actionType=search'">
				        <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.EARLY_OUT_ADD_EARLY_OUT_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-md-form-label text-md-right">
                                        <%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%
											value = WorkflowController.getNameFromOrganogramId(early_outDTO.organogramId, Language) + ", " + WorkflowController.getOrganogramName(early_outDTO.organogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(early_outDTO.organogramId, Language);
											%>											
				
											<%=Utils.getDigits(value, Language)%>
											
											<%
											value = early_outDTO.userId + "";
											%>
				
											<br><%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-md-form-label text-md-right">
                                        <%=LM.getText(LC.HM_SUPERIOR_EMPLOYEE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%
											value = WorkflowController.getNameFromOrganogramId(early_outDTO.superiorOrgId, Language) + ", " + WorkflowController.getOrganogramName(early_outDTO.superiorOrgId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(early_outDTO.superiorOrgId, Language);
											%>											
				
											<%=Utils.getDigits(value, Language)%>
											
											<%
											UserDTO superiorDTO = UserRepository.getUserDTOByOrganogramID(early_outDTO.superiorOrgId);
											if(superiorDTO != null)
											{
												value = UserRepository.getUserDTOByOrganogramID(early_outDTO.superiorOrgId).userName;
											%>
				
											<br><%=Utils.getDigits(value, Language)%>
											<%
											}
											%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-md-form-label text-md-right">
                                        <%=LM.getText(LC.EARLY_OUT_ADD_LEAVEDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%=Utils.getDigits(simpleDateFormat.format(new Date(early_outDTO.leaveDate)), Language)%> - 
											<%=Utils.getDigits(simpleDateFormat.format(new Date(early_outDTO.endDate)), Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-md-form-label text-md-right">
                                        <%=LM.getText(LC.EARLY_OUT_ADD_STARTTIME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											
				
											<%=Utils.getDigits(TimeConverter.hourMinuteFormat.format(new Date(early_outDTO.startTime)), Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-md-form-label text-md-right">
                                        <%=LM.getText(LC.EARLY_OUT_ADD_ENDTIME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%=Utils.getDigits(TimeConverter.hourMinuteFormat.format(new Date(early_outDTO.endTime)), Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-start">
                                    <label class="col-md-4 col-md-form-label text-md-right">
                                        <%=LM.getText(LC.EARLY_OUT_ADD_REASON, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%
											value = early_outDTO.reason + "";
											%>
				
											<%=value%>
				
			
                                    </div>
                                </div>
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>