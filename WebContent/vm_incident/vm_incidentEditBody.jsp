<%@page import="login.LoginDTO" %>
<%@page import="vm_incident.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    String servletName = "Vm_incidentServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    String actionName;
    Vm_incidentDTO vm_incidentDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        vm_incidentDTO = Vm_incidentDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    } else {
        actionName = "add";
        vm_incidentDTO = new Vm_incidentDTO();
    }

    String formTitle = LM.getText(LC.VM_INCIDENT_ADD_VM_INCIDENT_ADD_FORMNAME, loginDTO);
    String fileColumnName = "";

    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=vm_incidentDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_incidentDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_incidentDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>' value='<%=vm_incidentDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_incidentDTO.lastModificationTime%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>' value='<%=vm_incidentDTO.searchColumn%>'
                                           tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="vehicleType_select_<%=i%>"><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLETYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleTypeCat'
                                                    id='vehicleType_select_<%=i%>' tag='pb_html'
                                                    onchange="loadVehicleList()">
                                                <%=CatRepository.getInstance().buildOptions("vehicle_type",Language,vm_incidentDTO.vehicleTypeCat)%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row" id="vehicleIdDiv">
                                        <label class="col-md-3 col-form-label text-md-right" for="vehicleId_select_<%=i%>"><%=LM.getText(LC.VM_INCIDENT_ADD_VEHICLEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleId' id='vehicleId_select_<%=i%>'
                                                    tag='pb_html'>
                                            </select>

                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='regNo' id='regNo_text_<%=i%>'
                                           value='<%=vm_incidentDTO.regNo%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="incidentCat_category_<%=i%>"><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='incidentCat'
                                                    id='incidentCat_category_<%=i%>' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("incident", Language, vm_incidentDTO.incidentCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "incidentDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='incidentDate' id='incidentDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(vm_incidentDTO.incidentDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTTIME, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "incidentTime_js_" + i;%>
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' value="<%=vm_incidentDTO.incidentTime%>"
                                                   name='incidentTime' id='incidentTime_time_<%=i%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="incidentPlace_text_<%=i%>"><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTPLACE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
																<textarea class='form-control' name='incidentPlace' rows="4" style="resize: none"
                                                                          id='incidentPlace_text_<%=i%>'><%=vm_incidentDTO.incidentPlace%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="incidentDetails_text_<%=i%>"><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTDETAILS, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
																<textarea class='form-control' name='incidentDetails' rows="4" style="resize: none"
                                                                          id='incidentDetails_text_<%=i%>'><%=vm_incidentDTO.incidentDetails%></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                fileColumnName = "filesDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_incidentDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    vm_incidentDTO.filesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="Vm_incidentServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_incidentDTO.filesDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=vm_incidentDTO.filesDropzone%>'/>


                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='driverOrgId'
                                           id='driverOrgId_hidden_<%=i%>' value='<%=vm_incidentDTO.driverOrgId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeId'
                                           id='driverOfficeId_hidden_<%=i%>' value='<%=vm_incidentDTO.driverOfficeId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeUnitId'
                                           id='driverOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_incidentDTO.driverOfficeUnitId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverEmpId'
                                           id='driverEmpId_hidden_<%=i%>' value='<%=vm_incidentDTO.driverEmpId%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=vm_incidentDTO.isDeleted%>'
                                           tag='pb_html'/>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius" id="cancel-btn"
                               href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_CANCEL_BUTTON, loginDTO)%>
                            </a>
                            <button id="submit-btn" class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2" type="submit"
                                    onclick="event.preventDefault();submitForm()"><%=LM.getText(LC.VM_FUEL_REQUEST_ADD_VM_FUEL_REQUEST_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const vehicleForm = $("#bigform");
    function loadVehicleList() {

        var value = $("#vehicleType_select_0").val();
        let url = "Vm_vehicleServlet?actionType=getAllByVehicleType&ID=" + value + "&vehicleId=" + '<%=vm_incidentDTO.vehicleId%>';
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                $("#vehicleId_select_0").html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }

    function submitForm(){
        buttonStateChange(true);
        if(PreprocessBeforeSubmiting(0)){
            $.ajax({
                type : "POST",
                url : "Vm_incidentServlet?actionType=ajax_<%=actionName%>",
                data : vehicleForm.serialize(),
                dataType : 'JSON',
                success : function(response) {
                    if(response.responseCode === 0){
                        $('#toast_message').css('background-color','#ff6063');
                        showToastSticky(response.msg,response.msg);
                        buttonStateChange(false);
                    }else if(response.responseCode === 200){
                        window.location.replace(getContextPath()+response.msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }else{
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }

    function processResponse(data) {

        if (data.includes('No driver')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, No driver found for this vehicle!" : "দুঃখিত, এই গাড়ির কোনো চালক পাওয়া যায়নি "%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        } else if (data.includes('Invalid Input')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, missing required field!" : "দুঃখিত, আবশ্যকীয় তথ্য প্রদান করুন "%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        } else {
            window.location = 'Vm_incidentServlet?actionType=search';
        }
    }

    function ajaxPost(url, accept, send, postData, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: accept,
            contentType: send,
            data: postData,
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


    function submitBigForm() {

        if (PreprocessBeforeSubmiting(0, '<%=actionName%>')) {
            var actionUrl = vehicleForm.attr("action");
            var postData = (vehicleForm.serialize());
            ajaxPost(actionUrl, "json", "application/x-www-form-urlencoded", postData, processResponse, processResponse);
        }
    }


    function PreprocessBeforeSubmiting(row, validate) {
        preprocessDateBeforeSubmitting('incidentDate', row);
        preprocessTimeBeforeSubmitting('incidentTime', row);
        vehicleForm.validate();
        return vehicleForm.valid();
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_incidentServlet");
    }

    function init(row) {

        setDateByStringAndId('incidentDate_js_' + row, $('#incidentDate_date_' + row).val());
        setTimeById('incidentTime_js_' + row, $('#incidentTime_time_' + row).val(), true);

        loadVehicleList();
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });


        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1 && value.toString().trim().length > 0;
        });

        $.validator.addMethod('nonEmpty', function (value, element) {
            return value.toString().trim().length > 0;
        });

        let lang = '<%=Language%>';
        let vehicleIdErr;
        let incidentCatErr;
        let incidentTimeErr;
        let incidentPlaceErr;
        let incidentDetailsErr;

        if (lang == 'english') {
            vehicleIdErr = 'Please provide vehicle';
            incidentCatErr = 'Please provide incident type';
            incidentTimeErr = 'Please provide incident time';
            incidentPlaceErr = 'Please provide incident place';
            incidentDetailsErr = 'Please provide incident details';

        } else {
            vehicleIdErr = 'গাড়ি সিলেক্ট করুন';
            incidentCatErr = 'ঘটনার ধরণ প্রদান করুন';
            incidentTimeErr = 'ঘটনার সময় প্রদান করুন';
            incidentPlaceErr = 'ঘটনাস্থল প্রদান করুন';
            incidentDetailsErr = 'ঘটনার বিবরণ প্রদান করুন';

        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vehicleId: {
                    validSelector: true,
                },
                incidentCat: {
                    validSelector: true,
                },
                incidentTime: {
                    nonEmpty: true,
                },
                incidentPlace: {
                    nonEmpty: true,
                },
                incidentDetails: {
                    nonEmpty: true,
                }

            },
            messages: {
                vehicleId: vehicleIdErr,
                incidentCat: incidentCatErr,
                incidentTime: incidentTimeErr,
                incidentPlace: incidentPlaceErr,
                incidentDetails: incidentDetailsErr,

            }
        });

    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>


<style>
    .required {
        color: red;
    }
</style>



