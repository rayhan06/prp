<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String url = request.getParameter("url");
    String pageName = request.getParameter("pageName");
    if (pageName == null) {
        pageName = "Search";
    }
    String pageno = "";
    RecordNavigator rn = (RecordNavigator) request.getAttribute("recordNavigator");
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
%>


<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0' onKeyUp='allfield_changed("",0)' id='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
<%--        <div class="kt-portlet__head-toolbar">--%>
<%--            <div class="kt-portlet__head-group">--%>
<%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
<%--                     aria-hidden="true" x-placement="top"--%>
<%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
<%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
<%--                    <div class="tooltip-inner">Collapse</div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"
                               for="vehicleType_select_0"><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLETYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='vehicleTypeCat' id='vehicleType_select_0' tag='pb_html'
                                    onchange="loadVehicleList()">
                                <%=CatRepository.getInstance().buildOptions("vehicle_type", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"
                               for="vehicleId_select_0"><%=LM.getText(LC.VM_INCIDENT_ADD_VEHICLEID, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='vehicleId' id='vehicleId_select_0' tag='pb_html'>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"
                               for="incident_cat"><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTCAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='incident_cat' id='incident_cat'
                                    onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("incident", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_EDIT_SERVINGFROM, loginDTO)%>
                            (<%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTDATE, loginDTO)%>)</label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="incident_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="incident_date_start" name="incident_date_start">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%>
                            (<%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTDATE, loginDTO)%>)</label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="incident_date_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input type="hidden" id="incident_date_end" name="incident_date_end">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">


    function loadVehicleList() {

        var value = $("#vehicleType_select_0").val();
        let url = "Vm_vehicleServlet?actionType=getAllByVehicleType&ID=" + value + "&addDefault=1&vehicleId=" + '-1';
        console.log("url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // const response = JSON.parse(fetchedData);
                $("#vehicleId_select_0").html(fetchedData);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }


    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#vehicleType_select_0").val()) {
            params += '&vehicle_type_cat=' + $("#vehicleType_select_0").val();
        }
        if ($("#vehicleId_select_0").val()) {
            params += '&vehicle_id=' + $("#vehicleId_select_0").val();
        }
        if ($("#incident_cat").val()) {
            params += '&incident_cat=' + $("#incident_cat").val();
        }
        $("#incident_date_start").val(getDateStringById('incident_date_start_js', 'DD/MM/YYYY'));
        params += '&incident_date_start=' + getBDFormattedDate('incident_date_start');
        $("#incident_date_end").val(getDateStringById('incident_date_end_js', 'DD/MM/YYYY'));
        params += '&incident_date_end=' + getBDFormattedDate('incident_date_end');

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

