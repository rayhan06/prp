<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="vm_incident.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="files.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="util.StringUtils" %>


<%
    String servletName = "Vm_incidentServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    Vm_incidentDTO vm_incidentDTO = Vm_incidentDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    FilesDAO filesDAO = new FilesDAO();
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.VM_INCIDENT_ADD_VM_INCIDENT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="form-body">
                <h5 class="table-title">
                    <%=LM.getText(LC.VM_INCIDENT_ADD_VM_INCIDENT_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLETYPE, loginDTO)%>
                            </b></td>
                            <td>
                                <%=CatRepository.getInstance().getText(Language, "vehicle_type", vm_incidentDTO.vehicleTypeCat)%>
                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_VEHICLEID, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_incidentDTO.vehicleId);
                                    String brand = CatRepository.getInstance().getText(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
                                    String seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
                                    String option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo;
                                %>
                                <%=Utils.getDigits(option + "(" + seats + vm_vehicleDTO.numberOfSeats + ")", Language)%>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_SEARCH_DRIVERNAMEEN, loginDTO)%>
                            </b></td>
                            <td>
                                <%=Utils.getDigits(vm_incidentDTO.driverNameEn + ", " + vm_incidentDTO.driverOfficeUnitOrgNameEn, Language)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_SEARCH_DRIVERNAMEBN, loginDTO)%>
                            </b></td>
                            <td>
                                <%=Utils.getDigits(vm_incidentDTO.driverNameBn + ", " + vm_incidentDTO.driverOfficeUnitOrgNameBn, Language)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTCAT, loginDTO)%>
                            </b></td>
                            <td>
                                <%=CatRepository.getInstance().getText(Language, "incident", vm_incidentDTO.incidentCat)%>
                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTDATE, loginDTO)%>
                            </b></td>
                            <td>
                                <%=StringUtils.getFormattedDate(Language, vm_incidentDTO.incidentDate)%>
                            </td>

                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTTIME, loginDTO)%>
                            </b></td>
                            <td>
                                <%=Utils.getDigits(vm_incidentDTO.incidentTime, Language)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTPLACE, loginDTO)%>
                            </b></td>
                            <td>
                                <%=Utils.getDigits(vm_incidentDTO.incidentPlace, Language)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTDETAILS, loginDTO)%>
                            </b></td>
                            <td>
                                <%=Utils.getDigits(vm_incidentDTO.incidentDetails, Language)%>
                            </td>
                        </tr>


                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, loginDTO)%>
                            </b></td>
                            <td>
                                <%
                                    {
                                        List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(vm_incidentDTO.filesDropzone);
                                %>
                                <%@include file="../pb/dropzoneViewer.jsp" %>
                                <%
                                    }
                                %>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>