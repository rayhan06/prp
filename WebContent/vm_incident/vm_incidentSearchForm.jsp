<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_incident.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="java.util.List" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String value = "";
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;

    String navigator2 = SessionConstants.NAV_VM_INCIDENT;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute("recordNavigator");
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String tableName = rn2.m_tableName;

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLETYPE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_INCIDENT_ADD_VEHICLEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_REQUISITION_SEARCH_DRIVERNAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_REQUISITION_SEARCH_DRIVERNAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTPLACE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_INCIDENT_ADD_INCIDENTDETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_INCIDENT_SEARCH_VM_INCIDENT_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            List<Vm_incidentDTO> data = (List<Vm_incidentDTO>) rn2.list;
            if (data != null && data.size() > 0) {
                for (Vm_incidentDTO vm_incidentDTO : data) {
        %>
        <tr>
            <%@include file="vm_incidentSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>