<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="util.StringUtils" %>

<td>
    <%=CatRepository.getInstance().getText(Language, "vehicle_type", vm_incidentDTO.vehicleTypeCat)%>
</td>

<td>
    <%
        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_incidentDTO.vehicleId);
        String brand = CatRepository.getInstance().getText(Language, "vehicle_brand", vm_vehicleDTO.vehicleBrandCat);
        String seats = Language.equals("English") ? "Number of seats: " : "আসন সংখ্যা: ";
        String option = vm_vehicleDTO.regNo + " " + brand + " " + vm_vehicleDTO.modelNo;
    %>
    <%=Utils.getDigits(option + "(" + seats + vm_vehicleDTO.numberOfSeats + ")", Language)%>
</td>

<td>
    <%=vm_incidentDTO.driverNameEn + ", " + vm_incidentDTO.driverOfficeUnitOrgNameEn%>
</td>

<td>
    <%=vm_incidentDTO.driverNameBn + ", " + vm_incidentDTO.driverOfficeUnitOrgNameBn%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "incident", vm_incidentDTO.incidentCat)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language,vm_incidentDTO.incidentDate)%>
</td>

<td>
    <%=Utils.getDigits(vm_incidentDTO.incidentPlace, Language)%>
</td>

<td>
    <%=Utils.getDigits(vm_incidentDTO.incidentTime, Language)%>
</td>

<td>
    <%=Utils.getDigits(vm_incidentDTO.incidentDetails, Language)%>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Vm_incidentServlet?actionType=view&ID=<%=vm_incidentDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Vm_incidentServlet?actionType=getEditPage&ID=<%=vm_incidentDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=vm_incidentDTO.iD%>'/></span>
    </div>
</td>