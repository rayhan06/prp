
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_documents.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.text.SimpleDateFormat"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="job_applicant_documents.Job_applicant_documentsDAO" %>
<%@ page import="job_applicant_documents.Job_applicant_documentsDTO" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="recruitment_job_required_files.Recruitment_job_required_filesDTO" %>
<%@ page import="recruitment_job_required_files.Recruitment_job_required_filesDAO" %>
<%@ page import="recruitment_job_req_trains_and_certs.Recruitment_job_req_trains_and_certsDAO" %>
<%@ page import="recruitment_job_req_trains_and_certs.Recruitment_job_req_trains_and_certsDTO" %>
<%@ page import="job_applicant_qualifications.Job_applicant_qualificationDTO" %>
<%@ page import="job_applicant_qualifications.Job_applicant_qualificationDAO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="recruitment_job_description.*" %>
<%@ page import="recruitment_job_req_trains_and_certs.Recruitment_job_req_trains_and_certsRepository" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="job_applicant_qualifications.Job_applicant_qualificationSummaryDTO" %>


<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty())
	{
		failureMessage = "";
	}

	String Language = LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_LANGUAGE, loginDTO);


	Job_applicant_qualificationDAO job_applicant_qualificationDAO = new Job_applicant_qualificationDAO();


	String ajax = request.getParameter("ajax");
	boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%


	String formTitle = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_QUALIFICATION, loginDTO);
//	String formTitle = "";

%>

<%--<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>--%>

<div class="col-lg-12">
	<div class="kt-portlet shadow-none" style="margin-top: -20px">

		<div class="kt-portlet__body form-body">


<div class="table-responsive">
	<table id="tableData" class="table table-bordered table-striped">
		<thead class="thead-light">
		<tr class="">
			<%--                                    <th style="  width: 20%; height: auto; border: 1px solid gray;" scope="row">1</th>--%>
			<th style="  width: 30%; height: auto; border: 1px solid gray;" class="">
				<%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NAME, loginDTO)%>
			</th>
			<th style="  width: 10%; height: auto; border: 1px solid gray;" class=""><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_ISMANDATORYCERTS, loginDTO)%></th>
				<th style="  width: 10%; height: auto; border: 1px solid gray;" class=""><%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_FILE_REQUIRED, loginDTO)%></th>
			<th style="  width: 10%; height: auto; border: 1px solid gray;" class=""> <%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_HAS, loginDTO)%></th>
				<th style="  width: 20%; height: auto; border: 1px solid gray;" class=""> <%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_CREDENTIAL, loginDTO)%></th>
				<th style="  width: 20%; height: auto; border: 1px solid gray;" class=""> <%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_VALID_UPTO, loginDTO)%></th>
<%--			<th class="filesColumn" style="  width: 20%; height: auto; border: 1px solid gray;" class="">--%>
<%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FILESDROPZONE, loginDTO)%>--%>
<%--			</th>--%>
			<%--										<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_MODIFIEDBY, loginDTO)%></th>--%>
			<%--                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_REMOVE, loginDTO)%></th>--%>
		</tr>
		</thead>
		<tbody>
		<%
//			RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO();
			long jobId = Long.parseLong(request.getParameter("jobId"));
			List<RecruitmentJobSpecificCertsDTO> data = RecruitmentJobSpecificCertsRepository.getInstance().
					getRecruitmentJobSpecificCertsDTOByJobDescriptionId(jobId);

//					(List<RecruitmentJobSpecificCertsDTO>) recruitmentJobSpecificCertsDAO.
//					getRecruitmentJobSpecificCertsDTOListByRecruitmentJobDescriptionID(jobId);

			List<Long> reqCertIds = data.stream().map(i -> i.recruitmentJobReqTrainsAndCertsType).collect(Collectors.toList());

//			Recruitment_job_req_trains_and_certsDAO recruitment_job_req_trains_and_certsDAO =
//					new Recruitment_job_req_trains_and_certsDAO();

			List<Recruitment_job_req_trains_and_certsDTO> recruitment_job_req_trains_and_certsDTOS = new ArrayList<>();

			reqCertIds.forEach(i -> {
				Recruitment_job_req_trains_and_certsDTO dto = Recruitment_job_req_trains_and_certsRepository.
						getInstance().getRecruitment_job_req_trains_and_certsDTOByID(i);
				if(dto != null){
					recruitment_job_req_trains_and_certsDTOS.add(dto);
				}
			});


//					(List<Recruitment_job_req_trains_and_certsDTO>)
//					recruitment_job_req_trains_and_certsDAO.getDTOs(reqCertIds);
			Map<Long, Recruitment_job_req_trains_and_certsDTO> recruitment_job_req_trains_and_certsDTOMap = new HashMap<>(
					recruitment_job_req_trains_and_certsDTOS.stream().collect(Collectors.toMap(s -> s.iD, s -> s))) ;

//			List<Job_applicant_qualificationDTO> job_applicant_qualificationDTOS = job_applicant_qualificationDAO.
//					getJob_applicant_qualificationDTOListByjobApplicantIdAndJobId(Long.parseLong(request.getParameter("ID")), jobId);



			List<Job_applicant_qualificationSummaryDTO> summaryDTOS = (List<Job_applicant_qualificationSummaryDTO>)
					request.getSession(true).getAttribute("job_applicant_qualificationSummaryDTOList");
			Gson gson = new Gson();

			List<Job_applicant_qualificationDTO> job_applicant_qualificationDTOS = new ArrayList<>();
			if(summaryDTOS != null){
				summaryDTOS.forEach(i -> {
					String json = gson.toJson(i);
					Job_applicant_qualificationDTO dto = gson.fromJson(json, Job_applicant_qualificationDTO.class);
					job_applicant_qualificationDTOS.add(dto);
				});
			}


			data.forEach(i -> {
				if(recruitment_job_req_trains_and_certsDTOMap.containsKey(i.recruitmentJobReqTrainsAndCertsType)){
					i.nameBn = recruitment_job_req_trains_and_certsDTOMap.get(i.recruitmentJobReqTrainsAndCertsType).nameBn;
					i.nameEn = recruitment_job_req_trains_and_certsDTOMap.get(i.recruitmentJobReqTrainsAndCertsType).nameEn;
				}
			});

			Map<Long, Job_applicant_qualificationDTO> job_applicant_qualificationDTOMap = new HashMap();
			HashSet<Long> foundRecruitementJobSpecificCertIds = new HashSet();

			for (Job_applicant_qualificationDTO innerDTO: job_applicant_qualificationDTOS) {
				job_applicant_qualificationDTOMap.put(innerDTO.recruitmentJobSpecificCertId, innerDTO);
				foundRecruitementJobSpecificCertIds.add(innerDTO.recruitmentJobSpecificCertId);
			}

			request.setAttribute("job_applicant_qualificationDTOMap",job_applicant_qualificationDTOMap);

			try
			{

				if (data != null)
				{
					int size = data.size();
					System.out.println("data not null and size = " + size + " data = " + data);

					for(int i = 0; i < data.size(); i++){

						RecruitmentJobSpecificCertsDTO dto = data.get(i);

						if (!foundRecruitementJobSpecificCertIds.contains(dto.iD))continue;


		%>
		<tr id = 'tr_<%=i%>'>
			<%

			%>

			<td style="  height: auto; border: 1px solid gray;"  class="">
				<%
					String name = "";
					if(Language.equalsIgnoreCase("english")){
						name = dto.nameEn;
					} else {
						name = dto.nameBn;
					}
				%>
				<%= name%>
			</td>
			<td style="   height: auto; border: 1px solid gray;"  class="">
				<%
					String text = "";

					if(dto.isMandatoryCerts){
						text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_YES, loginDTO);
					} else {
						text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NO, loginDTO);
					}
				%>
				<%= text%>
			</td>

			<td style="   height: auto; border: 1px solid gray;"  class="">
				<%
					text = "";

					if(dto.file_required){
						text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_YES, loginDTO);
					} else {
						text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NO, loginDTO);
					}
				%>
				<%= text%>
			</td>

			<td style="   height: auto; border: 1px solid gray;"  class="">

				<%
					Job_applicant_qualificationDTO job_applicant_qualificationDTO =
							job_applicant_qualificationDTOMap.get(dto.iD);
				%>

				<%=(String.valueOf(job_applicant_qualificationDTO.isChecked).equals("true"))?(LM.getText(LC.ETHNIC_MINORITY_YES, loginDTO)):(LM.getText(LC.ETHNIC_MINORITY_NO, loginDTO))%>

			</td>

			<td style=" height: auto; border: 1px solid gray;" >
				<%=job_applicant_qualificationDTO.credential_id%>
			</td>

			<td style=" height: auto; border: 1px solid gray;">
				<%
					String value = "";
					if(job_applicant_qualificationDTO.valid_upto != 0){
						value =dateFormat.format(new Date(job_applicant_qualificationDTO.valid_upto));
					}
				%>
				<%=UtilCharacter.convertDataByLanguage(Language,  value)%>
			</td>


<%--			<%--%>
<%--				request.setAttribute("recruitment_job_specific_filesDTO",dto);--%>
<%--			%>--%>

<%--			<jsp:include page="job_applicant_documentsSummarySearchRow.jsp">--%>
<%--				<jsp:param name="pageName" value="searchrow" />--%>
<%--				<jsp:param name="rownum" value="<%=i%>" />--%>
<%--			</jsp:include>--%>


			<%

			%>
		</tr>
		<%
					}

					System.out.println("printing done");
				}
				else
				{
					System.out.println("data  null");
				}
			}
			catch(Exception e)
			{
				System.out.println("JSP exception " + e);
			}
		%>



		</tbody>

	</table>
</div>

		</div>
	</div>
</div>



			