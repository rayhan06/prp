

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_exam_venue.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>

<%@ page import="geolocation.*"%>



<%
String servletName = "Recruitment_exam_venueServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Recruitment_exam_venueDTO recruitment_exam_venueDTO = Recruitment_exam_venueDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = recruitment_exam_venueDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBTITLEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = recruitment_exam_venueDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBTITLEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = recruitment_exam_venueDTO.nameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_ADDRESS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = recruitment_exam_venueDTO.address + "";
											%>
											<%=value%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_MOBILE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = recruitment_exam_venueDTO.phone + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.USER_ADD_EMAIL, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = recruitment_exam_venueDTO.email + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_BUILDING, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_FLOOR, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_ROOMNO, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_CAPACITY, loginDTO)%></th>
							</tr>
							<%
                        	RecruitmentExamVenueItemDAO recruitmentExamVenueItemDAO = RecruitmentExamVenueItemDAO.getInstance();
                         	List<RecruitmentExamVenueItemDTO> recruitmentExamVenueItemDTOs = (List<RecruitmentExamVenueItemDTO>)recruitmentExamVenueItemDAO.getDTOsByParent("recruitment_exam_venue_id", recruitment_exam_venueDTO.iD);
                         	
                         	for(RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO: recruitmentExamVenueItemDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = recruitmentExamVenueItemDTO.building + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentExamVenueItemDTO.floor + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentExamVenueItemDTO.roomNo + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentExamVenueItemDTO.capacity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>