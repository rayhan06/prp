
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_exam_venue.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navRECRUITMENT_EXAM_VENUE";
String servletName = "Recruitment_exam_venueServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBTITLEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBTITLEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_ADDRESS, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_MOBILE, loginDTO)%></th>
								<th><%=LM.getText(LC.USER_ADD_EMAIL, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_SEARCH_RECRUITMENT_EXAM_VENUE_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Recruitment_exam_venueDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Recruitment_exam_venueDTO recruitment_exam_venueDTO = (Recruitment_exam_venueDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = recruitment_exam_venueDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = recruitment_exam_venueDTO.nameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = recruitment_exam_venueDTO.address + "";
											%>
											<%=value%>
				
			
											</td>
		
											<td>
											<%
											value = recruitment_exam_venueDTO.phone + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = recruitment_exam_venueDTO.email + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = recruitment_exam_venueDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=recruitment_exam_venueDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			