<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="recruitment_exam_venue.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Recruitment_exam_venueDTO recruitment_exam_venueDTO = new Recruitment_exam_venueDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        recruitment_exam_venueDTO = Recruitment_exam_venueDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = recruitment_exam_venueDTO;
    String tableName = "recruitment_exam_venue";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ADD_FORMNAME, loginDTO);
    String servletName = "Recruitment_exam_venueServlet";
%>
<style>

    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }

</style>
<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>


                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=recruitment_exam_venueDTO.iD%>' tag='pb_html'/>

                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBTITLEEN, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>
                                            <div class="col-8">
                                                <input type='text'
                                                       autocomplete="off"
                                                       class='form-control englishOnly noPaste'
                                                       name='nameEn'
                                                       id='nameEn_text_<%=i%>'
                                                       value='<%=recruitment_exam_venueDTO.nameEn%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBTITLEBN, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>
                                            <div class="col-8">
                                                <input type='text'
                                                       autocomplete="off"
                                                       class='form-control noEnglish noPaste'
                                                       name='nameBn'
                                                       id='nameBn_text_<%=i%>'
                                                       value='<%=recruitment_exam_venueDTO.nameBn%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_ADDRESS, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>
                                            <div class="col-8">
                                                <input type='text'
                                                       autocomplete="off"
                                                       class='form-control'
                                                       name='address'
                                                       id='address_geolocation_<%=i%>'
                                                       value='<%=recruitment_exam_venueDTO.address%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_VENDOR_AUCTIONEER_DETAILS_ADD_MOBILE, loginDTO)%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control'
                                                       name='phone'
                                                       id='phone_text_<%=i%>'
                                                       required="required"
                                                       value='<%=recruitment_exam_venueDTO.phone%>'

                                                       tag='pb_html' autocomplete="off"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.USER_ADD_EMAIL, loginDTO)%>

                                            </label>
                                            <div class="col-8">
                                                <input type='text'
                                                       class='form-control'
                                                       placeholder='email@address.com'
                                                       name='email'
                                                       id='email_text_<%=i%>'
                                                       value='<%=recruitment_exam_venueDTO.email%>'
                                                       required="required"
                                                       title="email must be a of valid email address format"
                                                       tag='pb_html' autocomplete="off"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_BUILDING, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_FLOOR, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_ROOMNO, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_CAPACITY, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_ITEM_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="field-RecruitmentExamVenueItem">


                                <%
                                    if (actionName.equals("ajax_edit")) {
                                        int index = -1;


                                        for (RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO : recruitment_exam_venueDTO.recruitmentExamVenueItemDTOList) {
                                            index++;

                                            System.out.println("index index = " + index);

                                %>

                                <tr id="RecruitmentExamVenueItem_<%=index + 1%>">
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control' name='recruitmentExamVenueItem.iD'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               value='<%=recruitmentExamVenueItemDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">


                                        <input type='hidden' class='form-control'
                                               name='recruitmentExamVenueItem.recruitmentExamVenueId'
                                               id='recruitmentExamVenueId_hidden_<%=childTableStartingID%>'
                                               value='<%=recruitmentExamVenueItemDTO.recruitmentExamVenueId%>'
                                               tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control' name='recruitmentExamVenueItem.building'
                                               id='building_text_<%=childTableStartingID%>'
                                               value='<%=recruitmentExamVenueItemDTO.building%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control' name='recruitmentExamVenueItem.floor'
                                               id='floor_text_<%=childTableStartingID%>'
                                               value='<%=recruitmentExamVenueItemDTO.floor%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control' name='recruitmentExamVenueItem.roomNo'
                                               id='roomNo_text_<%=childTableStartingID%>'
                                               value='<%=recruitmentExamVenueItemDTO.roomNo%>' tag='pb_html'/>
                                    </td>
                                    <td>


                                        <input type='text' class='form-control englishOnly noPaste' name='recruitmentExamVenueItem.capacity'
                                               id='capacity_text_<%=childTableStartingID%>'
                                               value='<%=recruitmentExamVenueItemDTO.capacity%>' tag='pb_html'/>
                                    </td>


                                    <td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                   class="form-control-sm"/>
										</span>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-9 text-right">
                                <button
                                        id="add-more-RecruitmentExamVenueItem"
                                        name="add-moreRecruitmentExamVenueItem"
                                        type="button"
                                        class="btn btn-sm text-white add-btn shadow">
                                    <i class="fa fa-plus"></i>
                                    <%=LM.getText(LC.HM_ADD, loginDTO)%>
                                </button>
                                <button
                                        id="remove-RecruitmentExamVenueItem"
                                        name="removeRecruitmentExamVenueItem"
                                        type="button"
                                        class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>

                        <%RecruitmentExamVenueItemDTO recruitmentExamVenueItemDTO = new RecruitmentExamVenueItemDTO();%>

                        <template id="template-RecruitmentExamVenueItem">
                            <tr>
                                <td style="display: none;">

                                    <input type='hidden' class='form-control' name='recruitmentExamVenueItem.iD'
                                           id='iD_hidden_' value='<%=recruitmentExamVenueItemDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">

                                    <input type='hidden' class='form-control'
                                           name='recruitmentExamVenueItem.recruitmentExamVenueId'
                                           id='recruitmentExamVenueId_hidden_'
                                           value='<%=recruitmentExamVenueItemDTO.recruitmentExamVenueId%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <input type='text' class='form-control' name='recruitmentExamVenueItem.building'
                                           id='building_text_' value='<%=recruitmentExamVenueItemDTO.building%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <input type='text' class='form-control' name='recruitmentExamVenueItem.floor'
                                           id='floor_text_' value='<%=recruitmentExamVenueItemDTO.floor%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <input type='text' class='form-control' name='recruitmentExamVenueItem.roomNo'
                                           id='roomNo_text_' value='<%=recruitmentExamVenueItemDTO.roomNo%>'
                                           tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='text' class='form-control englishOnly noPaste' name='recruitmentExamVenueItem.capacity'
                                           id='capacity_text_' value='<%=recruitmentExamVenueItemDTO.capacity%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
                                                       class="form-control-sm"/>
											</span>
                                </td>
                            </tr>

                        </template>
                    </div>
                </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitForm()">
                        <%=LM.getText(LC.RECRUITMENT_EXAM_VENUE_ADD_RECRUITMENT_EXAM_VENUE_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const recruitmentVenueForm = $("#bigform");
    const fullPageLoader = $('#full-page-loader');

    function submitForm(){
        buttonStateChange(true);
        if(PreprocessBeforeSubmiting()){
            fullPageLoader.show();
            let url =  "<%=servletName%>?actionType=<%=actionName%>";
            $.ajax({
                type : "POST",
                url : url,
                data : recruitmentVenueForm.serialize(),
                dataType : 'JSON',
                success : function(response) {
                    if(response.responseCode === 0){
                        fullPageLoader.hide();
                        $('#toast_message').css('background-color','#ff6063');
                        showToastSticky(response.msg,response.msg);
                        buttonStateChange(false);
                    }else if(response.responseCode === 200){
                        fullPageLoader.hide();
                        window.location.replace(getContextPath()+response.msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    fullPageLoader.hide();
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }else{
            buttonStateChange(false);
            fullPageLoader.hide();
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }


    function PreprocessBeforeSubmiting() {

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Recruitment_exam_venueServlet");
    }

    function init(row) {

        initGeoLocation('address_geoSelectField_', row, "Recruitment_exam_venueServlet");

        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        fullPageLoader.hide();
        banglaEnglishInputValidation();

    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-RecruitmentExamVenueItem").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-RecruitmentExamVenueItem");

            $("#field-RecruitmentExamVenueItem").append(t.html());
            SetCheckBoxValues("field-RecruitmentExamVenueItem");

            var tr = $("#field-RecruitmentExamVenueItem").find("tr:last-child");

            tr.attr("id", "RecruitmentExamVenueItem_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
            });

            banglaEnglishInputValidation();
            child_table_extra_id++;

        });


    $("#remove-RecruitmentExamVenueItem").click(function (e) {
        var tablename = 'field-RecruitmentExamVenueItem';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[deletecb="true"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });

    function banglaEnglishInputValidation(){
        $(".englishOnly").keypress(function(event){
            var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
            if (allowed.includes(event.key))return true;
            var ew = event.which;
            if(ew == 32)
                return true;
            if(48 <= ew && ew <= 57)
                return true;
            if(65 <= ew && ew <= 90)
                return true;
            if(97 <= ew && ew <= 122)
                return true;
            return false;
        });
        $(".noEnglish").keypress(function(event){
            var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
            if (allowed.includes(event.key))return true;
            var ew = event.which;

            if(48 <= ew && ew <= 57)
                return false;
            if(65 <= ew && ew <= 90)
                return false;
            if(97 <= ew && ew <= 122)
                return false;
            return true;
        });
    }


    function onInputPasteNoEnglish(event) {
        var clipboardData = event.clipboardData || window.clipboardData;
        var data = clipboardData.getData("Text");
        var valid = data.toString().split('').every(char => {
            var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
            if (allowed.includes(char)) return true;
            if ('0' <= char && char <= '9')
                return false;
            if ('a' <= char && char <= 'z')
                return false;
            if ('A' <= char && char <= 'Z')
                return false;
            return true;
        });
        if (valid) return;
        event.stopPropagation();
        event.preventDefault();
    }

    function onInputPasteEnglishOnly(event) {
        var clipboardData = event.clipboardData || window.clipboardData;
        var data = clipboardData.getData("Text");
        var valid = data.toString().split('').every(char => {
            var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
            if (allowed.includes(char)) return true;
            if (char.toString().trim() == '')
                return true;
            if ('0' <= char && char <= '9')
                return true;
            if ('a' <= char && char <= 'z')
                return true;
            if ('A' <= char && char <= 'Z')
                return true;
            return false;
        });
        if (valid) return;
        event.stopPropagation();
        event.preventDefault();

        document.querySelectorAll("input.noPaste").forEach(function (input) {
            input.addEventListener("paste", onInputPaste);
        });

        var elements = document.getElementsByClassName("englishOnly");

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('paste', onInputPasteEnglishOnly);
        }

        elements = document.getElementsByClassName("noEnglish");

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('paste', onInputPasteNoEnglish);
        }

        function onInputPaste(event) {
            var clipboardData = event.clipboardData || window.clipboardData;
            if (/^[0-9০-৯]+$/g.test(clipboardData.getData("Text"))) return;
            event.stopPropagation();
            event.preventDefault();
        }
    }


</script>






