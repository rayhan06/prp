<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@page import="java.util.Calendar" %>
<%@page import="pbReport.DateUtils" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2 mx-md-0">
    <div class="col-12">
		<div id="calendardiv" >
			<div id="visitDate" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_STARTTIME, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='startDate' name='startDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>
			<div id="visitDate_3" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.VM_VEHICLE_DRIVER_ASSIGNMENT_REPORT_WHERE_ENDTIME, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="endDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='endDate' name='endDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

			<div id="visitDate_4" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_WHERE_WITHDRAWALDATE, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="withdrawalStartDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='withdrawalStartDate' name='withdrawalStartDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

			<div id="visitDate_5" class="search-criteria-div">
				<div class="form-group row">
					<label class="col-md-3 col-form-label text-md-right">
						<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_WHERE_WITHDRAWALDATE_3, loginDTO)%>
					</label>
					<div class="col-md-9">
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="withdrawalStartDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='withdrawalEndDate' name='withdrawalEndDate' value=""
							   tag='pb_html'
						/>
					</div>
				</div>
			</div>

		</div>
<%--		<div  class="search-criteria-div" style = "display: none;">--%>
<%--			<div class="form-group row">--%>
<%--				<label class="col-md-3 control-label text-md-right">--%>
<%--					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_REQUESTEREMPID, loginDTO)%>--%>
<%--				</label>--%>
<%--				<div class="col-md-9">--%>
<%--					<input class='form-control'  name='requesterEmpId' id = 'requesterEmpId' value=""/>							--%>
<%--				</div>--%>
<%--			</div>--%>
<%--		</div>--%>
		<div  class="search-criteria-div" >
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_REQUESTEREMPID, loginDTO)%>
				</label>
				<div class="col-md-9">
					<%--					<input class='form-control'  name='driverId' id = 'driverId' value=""/>							--%>

					<button type="button" class="btn btn-primary btn-block shadow btn-border-radius" id="employee_record_id_modal_button"
							onclick="employeeRecordIdModalBtnClicked();">
						<%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
					</button>
					<div class="input-group" id="employee_record_id_div" style="display: none">
						<input type="hidden" name='requesterEmpId' id='requesterEmpId' value="">
						<button type="button" class="btn btn-secondary form-control" disabled
								id="employee_record_id_text"></button>
						<span class="input-group-btn" style="width: 5%" tag='pb_html'>
												<button type="button" class="btn btn-outline-danger"
														onclick="crsBtnClicked('employee_record_id');"
														id='employee_record_id_crs_btn' tag='pb_html'>
													x
												</button>
											</span>
					</div>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_BUILDINGTYPECAT, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='buildingTypeCat' id = 'buildingTypeCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "building_type", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(() => {
	showFooter = false;
});
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}

function crsBtnClicked(fieldName) {
	$('#' + fieldName + '_modal_button').show();
	$('#' + fieldName + '_div').hide();
	$('#' + fieldName + '_input').val('');
	document.getElementById(fieldName + '_text').innerHTML = '';
}

function viewEmployeeRecordIdInInput(empInfo) {
	$('#employee_record_id_modal_button').hide();
	$('#employee_record_id_div').show();

	let language = '<%=Language.toLowerCase()%>'
	let employeeView;
	if (language === 'english') {
		employeeView = empInfo.employeeNameEn + ', ' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
	} else {
		employeeView = empInfo.employeeNameBn + ', ' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
	}
	document.getElementById('employee_record_id_text').innerHTML = employeeView;
	$('#requesterEmpId').val(empInfo.employeeRecordId);
}

table_name_to_collcetion_map = new Map([

	['employeeRecordId', {
		isSingleEntry: true,
		callBackFunction: viewEmployeeRecordIdInInput
	}]
]);
modal_button_dest_table = 'none';
function employeeRecordIdModalBtnClicked() {
	modal_button_dest_table = 'employeeRecordId';
	$('#search_emp_modal').modal();
}
</script>