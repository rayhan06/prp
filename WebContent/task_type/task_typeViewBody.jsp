<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="task_type.*" %>
<%@page import="workflow.WorkflowController" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="task_type_approval_path.TaskTypeApprovalPathDTO" %>
<%@ page import="task_type_approval_path.TaskTypeApprovalPathRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="task_type_assign.Task_type_assignRepository" %>
<%@ page import="task_type_level.TaskTypeLevelDTO" %>
<%@ page import="task_type_level.TaskTypeLevelRepository" %>
<%@ page import="task_type_level.TaskTypeLevelUtil" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    Task_typeDTO task_typeDTO = (Task_typeDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    List<TaskTypeLevelDTO> taskTypeLevelDTOList = TaskTypeLevelRepository.getInstance().getByTaskTypeId(task_typeDTO.iD);
%>

<div class="menubottom">
    <div class="modal-body">
        <div class="row div_border office-div">
            <div class="col-md-12">
                <h5 class="table-title">
                    <%=LM.getText(LC.TASK_TYPE_ADD_TASK_TYPE_ADD_FORMNAME, loginDTO)%>
                </h5>

                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td>
                            <b><%=LM.getText(LC.TASK_TYPE_ADD_NAMEEN, loginDTO)%>
                            </b>
                        </td>
                        <td>
                            <%=task_typeDTO.nameEn%>
                        </td>

                        <td>
                            <b><%=LM.getText(LC.TASK_TYPE_ADD_NAMEBN, loginDTO)%>
                            </b>
                        </td>
                        <td>
                            <%=task_typeDTO.nameBn%>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row div_border office-div mt-5">
            <div class="col-md-12">
                <h5 class="table-title">
                    <%=LM.getText(LC.TASK_TYPE_ASSIGN_ADD_TASK_TYPE_ASSIGN_ADD_FORMNAME, loginDTO)%>
                </h5>

                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td style="width: 20%">
                            <b><%=LM.getText(LC.TASK_TYPE_ASSIGN_ADD_ROLEID, loginDTO)%>
                            </b>
                        </td>
                        <td>
                            <%=Task_type_assignRepository.getInstance().getRoleNamesByTaskTypeId(task_typeDTO.iD)%>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row div_border office-div mt-5">
            <div class="col-md-12">
                <h5 class="table-title">
                    <%=LM.getText(LC.TASK_TYPE_APPROVAL_PATH_ADD_TASK_TYPE_APPROVAL_PATH_ADD_FORMNAME, loginDTO)%>
                </h5>

                <table class="table table-bordered table-striped">
                    <thead>
                    <th><%=LM.getText(LC.TASK_TYPE_ADD_LEVEL, loginDTO)%>
                    </th>
                    <th><%=LM.getText(LC.TASK_TYPE_ADD_OFFICEUNITID, loginDTO)%>
                    </th>
                    <th></th>
                    </thead>
                    <tbody>
                    <%
                        if (taskTypeLevelDTOList != null) {
                            taskTypeLevelDTOList.sort(Comparator.comparingInt(a -> a.level));
                            for (TaskTypeLevelDTO taskTypeLevelDTO : taskTypeLevelDTOList) {
                    %>

                    <tr>
                        <td>
                            <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(taskTypeLevelDTO.level))%>
                        </td>
                        <td>
                            <%=TaskTypeLevelUtil.showOfficesName(Language, taskTypeLevelDTO.officeUnitIds)%>
                        </td>
                        <td>
                                <%
                                List<TaskTypeApprovalPathDTO> taskApprovalList = TaskTypeApprovalPathRepository.getInstance().getByTaskTypeLevelId(taskTypeLevelDTO.iD);
                                if(taskApprovalList != null){
                                    for (TaskTypeApprovalPathDTO approvalDTO : taskApprovalList) {
                                        Long employeeRecordId = WorkflowController.getEmployeeRecordIDFromOrganogramID(approvalDTO.officeUnitOrganogramId);
                                        OfficeUnitOrganograms officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(approvalDTO.officeUnitOrganogramId);
                                        Long officeUnitId = officeUnitOrganograms.office_unit_id;
                            %>
                            <b>
                                <%=WorkflowController.getNameFromEmployeeRecordID(employeeRecordId, Language)%>
                            </b>
                            <br>
                                <%=WorkflowController.getOrganogramName(approvalDTO.officeUnitOrganogramId, Language)%>
                            <br>
                                <%=WorkflowController.getOfficeUnitName(officeUnitId, Language)%>
                            <br>
                            <br>
                                <%
                                    }
                                }
                            %>
                    </tr>
                    <%
                            }
                        }
                    %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>