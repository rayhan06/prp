<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="task_type.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="task_type_level.TaskTypeLevelDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="task_type_level.TaskTypeLevelDAO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    Task_typeDTO task_typeDTO;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    List<TaskTypeLevelDTO> taskTypeLevelDTOList = null;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        task_typeDTO = (Task_typeDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        taskTypeLevelDTOList = TaskTypeLevelDAO.getInstance().getByTaskInfoId(task_typeDTO.iD);
    } else {
        actionName = "add";
        task_typeDTO = new Task_typeDTO();
    }
    String formTitle = LM.getText(LC.TASK_TYPE_ADD_TASK_TYPE_ADD_FORMNAME, loginDTO);
    if (taskTypeLevelDTOList == null || taskTypeLevelDTOList.size() == 0) {
        taskTypeLevelDTOList = Collections.singletonList(new TaskTypeLevelDTO(task_typeDTO.iD, 1));
    }
    taskTypeLevelDTOList.sort(Comparator.comparingInt(a -> a.level));
    String context = request.getContextPath() + "/";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row mx-2 mx-md-0">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white">
                                                        <%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>

                                            <input type='hidden' class='form-control' name='iD'
                                                   id='iD_hidden' value='<%=task_typeDTO.iD%>'
                                                   tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TASK_TYPE_ADD_NAMEEN, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='nameEn'
                                                           id='nameEn_text'
                                                           value='<%=task_typeDTO.nameEn%>' tag='pb_html'/>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TASK_TYPE_ADD_NAMEBN, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='nameBn'
                                                           id='nameBn_text'
                                                           value='<%=task_typeDTO.nameBn%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive mt-5">
                    <table class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <th><%=LM.getText(LC.TASK_TYPE_ADD_LEVEL, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.TASK_TYPE_ADD_OFFICEUNITID, loginDTO)%>
                        </th>
                        <th></th>
                        </thead>
                        <tbody id="tableHead">
                        <tr id="tableRow" class="custom-hidden">
                            <input type="hidden" id="idRow" name="idRow" value="-1">
                            <td>
                                <input type='text' class='form-control w-auto' name='levelRow'
                                       id='levelRow' value=''
                                       onkeydown="return levelKeyDown(event,this)"
                                       tag='pb_html'>
                            </td>
                            <td>
                                <select multiple="multiple" class='form-control rounded'
                                        name='officeUnitIdsRow'
                                        id='officeUnitIdsRow'>
                                    <%=Task_typeServlet.buildAllOfficeUnitMultiSelectOption(Language, "")%>
                                </select>
                            </td>
                            <td>
                                <button type="button"
                                        class="btn btn-sm delete-trash-btn"
                                        onclick="removeContainingRow(this);">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>

                        <% int index = 0;
                            for (TaskTypeLevelDTO taskTypeLevelDTO : taskTypeLevelDTOList) {
                                index++;
                        %>
                        <tr id="tableRow_<%=index%>">
                            <input type="hidden" id="idRow<%=index%>" name="idRow"
                                   value="<%=taskTypeLevelDTO.iD%>">
                            <td>
                                <input type='text' class='form-control' name='levelRow'
                                       onkeydown="return levelKeyDown(event,this)"
                                       id='levelRow<%=index%>'
                                       value='<%=taskTypeLevelDTO.level == 0? "" : taskTypeLevelDTO.level%>'
                                       tag='pb_html'>
                            </td>
                            <td>
                                <select multiple="multiple" class='form-control rounded'
                                        name='officeUnitIdsRow'
                                        id='officeUnitIdsRow<%=index%>'>
                                    <%=Task_typeServlet.buildAllOfficeUnitMultiSelectOption(Language, taskTypeLevelDTO.officeUnitIds)%>
                                </select>
                            </td>
                            <td>
                                <%if (index > 1) {%>
                                <button type="button"
                                        class="btn btn-danger btn-block remove-office"
                                        onclick="removeContainingRow(this);">
                                    x
                                </button>
                                <%}%>
                            </td>
                        </tr>
                        <%}%>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right mt-3">
                        <button type="button" id="add-level"
                                class="btn btn-success btn-sm shadow btn-border-radius mb-4"
                                onclick="addLevel();">
                            <%=isLangEng ? "Add Level" : "স্তর যোগ"%>
                        </button>
                    </div>
                </div>
                <input type="hidden" name="levelData" id="levelData">
                <div class="row mt-3">
                    <div class="col-12 form-actions text-center">
                        <button class="btn btn-sm cancel-btn text-white shadow btn-border-radius" type="button" id="cancel-btn"
                                onclick=window.location.replace(getContextPath()+"/Task_typeServlet?actionType=search")><%=LM.getText(LC.TASK_TYPE_ADD_TASK_TYPE_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2" onclick="submitForm(event)" id="submit-btn"
                                type="submit"><%=LM.getText(LC.TASK_TYPE_ADD_TASK_TYPE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>


        </form>
    </div>
</div>

<style>
    .custom-hidden {
        display: none;
    }
</style>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    function levelKeyDown(e, thiz) {
        return inputValidationForIntValue(e, $(thiz), 99);
    }

    $(() => {
        <%for(int i = 1;i <= taskTypeLevelDTOList.size();i++){%>
        select2MultiSelector("#officeUnitIdsRow<%=i%>", '<%=Language%>');
        <%}%>
    });

    function submitForm(event) {
        event.preventDefault();
        let data= {
            nameEn: $('#nameEn_text').val(),
            nameBn: $('#nameBn_text').val(),
            levelData: JSON.stringify(getLevelData())
        };
        submitAjaxByData(data,"Task_typeServlet?actionType=ajax_<%=actionName%>&iD=<%=task_typeDTO.iD%>");
    }

    let currentRowIndex = <%=taskTypeLevelDTOList.size()%>;
    let rowIdsSet = new Set([
        <%for(int i = 1;i <= taskTypeLevelDTOList.size();i++){%>
        'tableRow_<%=i%>'<%=i != taskTypeLevelDTOList.size()? "," : ""%>
        <%}%>
    ]);

    function removeContainingRow(buttonElement) {
        const containingRow = buttonElement.parentNode.parentNode;
        rowIdsSet.delete(containingRow.id);
        containingRow.remove();
    }

    function addLevel() {
        const newRow = document.getElementById('tableRow').cloneNode(true);
        const newRowId = newRow.querySelector('#idRow');
        const newRowLevel = newRow.querySelector('#levelRow');
        const newOfficeSelect = newRow.querySelector('#officeUnitIdsRow');
        const newRowIndex = currentRowIndex + 1;
        currentRowIndex++;
        // row id form -> tableRow_<index number>
        newRow.id += ('_' + newRowIndex);
        newRow.classList.remove('custom-hidden');

        // id's id form -> idRow<index number>
        newRowId.id += newRowIndex;

        // level id form -> levelRow<index number>
        newRowLevel.id += newRowIndex;
        newRowLevel.name += newRowIndex;

        // select id form -> officeUnitIdsRow<index number>
        newOfficeSelect.id += newRowIndex;
        newOfficeSelect.name += newRowIndex;

        document.getElementById('tableHead').append(newRow);
        rowIdsSet.add(newRow.id);
        select2MultiSelector("#" + newOfficeSelect.id, '<%=Language%>');
    }

    function getLevelData() {
        let data = [];
        for (let rowId of rowIdsSet) {
            const rowIndex = rowId.split('_')[1];
            const level = document.getElementById('levelRow' + rowIndex).value;
            const officeUnitIds = $('#officeUnitIdsRow' + rowIndex).select2("val").join(',');
            const id = document.getElementById('idRow' + rowIndex).value;
            data.push({
                iD: id,
                level: level,
                officeUnitIds: officeUnitIds
            });
        }
        return data;
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
        $('#add-level').prop('disabled', value);
        $('.remove-office').prop('disabled', value);
    }
</script>