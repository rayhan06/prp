<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="task_type_assign.Task_type_assignDTO" %>
<%@ page import="task_type_assign.Task_type_assignRepository" %>

<%
    Task_type_assignDTO taskTypeAssignDTO = Task_type_assignRepository.getInstance().getByTaskTypeId(task_typeDTO.iD);
    boolean hasAssignDTO = taskTypeAssignDTO != null;
%>

<td id='<%=i%>_nameEn'>
    <%=task_typeDTO.nameEn%>
</td>

<td id='<%=i%>_nameBn'>
    <%=task_typeDTO.nameBn%>
</td>

<td id='<%=i%>_Edit'>
    <button type="button" class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
            onclick="location.href='Task_typeServlet?actionType=getEditPage&ID=<%=task_typeDTO.iD%>'">
        <%=LM.getText(LC.TASK_TYPE_SEARCH_TASK_TYPE_EDIT_BUTTON, loginDTO)%>
    </button>
</td>


<td>
    <%--TODO: task type approvassign yrl--%>
    <button type="button" class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
            onclick="location.href='Task_type_assignServlet?actionType=get<%=hasAssignDTO?"Edit":"Add"%>Page&taskTypeId=<%=task_typeDTO.iD%>&ID=<%=hasAssignDTO? taskTypeAssignDTO.iD : 0L%>'">
        <%=LM.getText(hasAssignDTO? LC.TASK_TYPE_SEARCH_TASK_TYPE_EDIT_BUTTON : LC.TASK_TYPE_ADD_TASK_TYPE_ADD_BUTTON, loginDTO)%>
    </button>
</td>

<td>
    <button type="button" class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
            onclick="location.href='Task_typeServlet?actionType=view&ID=<%=task_typeDTO.iD%>'">
        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
    </button>
</td>

<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=task_typeDTO.iD%>'/></span>
    </div>
</td>