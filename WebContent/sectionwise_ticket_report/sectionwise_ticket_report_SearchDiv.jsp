<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.SECTIONWISE_TICKET_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
        
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_OFFICE, loginDTO)%>
				</label>
				<div class="col-md-9">
                    <%--					<input class='form-control'  name='officeUnitId' id = 'officeUnitId' value=""/>							--%>
                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                            id="officeUnitId_modal_button"
                            onclick="officeModalButtonClicked();">
                        <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                    </button>

                    <div class="input-group" id="officeUnitId_div" style="display: none">
                        <input type="hidden" name='officeUnitId'
                               id='office_units_id_input' value="">
                        <button type="button" class="btn btn-secondary form-control shadow btn-border-radius"
                                disabled id="office_units_id_text"></button>
                        <span class="input-group-btn" style="width: 5%" tag='pb_html'>
							<button type="button" class="btn btn-outline-danger"
									onclick="crsBtnClicked('officeUnitId');"
									id='officeUnitId_crs_btn' tag='pb_html'>
								x
							</button>
						</span>
                    </div>
                </div>
			</div>
		</div>
		<%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
		
    </div>
</div>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = true;
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}
/*Office unit modal start*/
function viewOfficeIdInInput(selectedOffice) {
    if (selectedOffice.id === '') {
        return;
    }
    $('#officeUnitId_modal_button').hide();
    $('#officeUnitId_div').show();
    document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
    $('#office_units_id_input').val(selectedOffice.id);
}

officeSelectModalUsage = 'none';
officeSelectModalOptionsMap = new Map([
    ['officeUnitId', {
        officeSelectedCallback: viewOfficeIdInInput
    }]
]);

function officeModalButtonClicked() {
    // console.log('Button Clicked!');
    officeSelectModalUsage = 'officeUnitId';
    $('#search_office_modal').modal();
}

function crsBtnClicked(fieldName) {
    $('#' + fieldName + '_modal_button').show();
    $('#' + fieldName + '_div').hide();
    $('#' + fieldName + '_input').val('');
    document.getElementById(fieldName + '_text').innerHTML = '';
}
</script>