<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="fund_management.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="fund_management_question.FundManagementQuestionRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="static util.StringUtils.convertBanglaIfLanguageIsBangla" %>
<%@ page import="static util.UtilCharacter.*" %>
<%@ page import="fund_approval_mapping.Fund_approval_mappingDTO" %>
<%@ page import="fund_approval_mapping.Fund_approval_mappingDAO" %>
<%@ page import="card_info.CardApprovalDTO" %>
<%@ page import="card_info.CardApprovalRepository" %>

<%
    String servletName = "Fund_managementServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Fund_managementDTO fund_managementDTO = Fund_managementDAO.getInstance().getDTOByID(id);
    Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(fund_managementDTO.employeeRecordsId);
    String nid = employeeRecordsDTO == null ? "" : employeeRecordsDTO.nid;

%>

<%@include file="../pb/viewInitializer.jsp" %>

<%
    String fundType =
            "<b>".concat(CatRepository.getName(Language, "fund_type", fund_managementDTO.fundTypeCat)).concat("</b>");

    String bodyTextBn =
            "আমার সাধারণ %s তহবিলের সিজিএস/সিএও/প্রশাসন/জাসস <b>%s</b> নং হিসাব গচ্ছিত অংক থেকে আমি <b>%s</b> টাকা অগ্রিম গ্রহণের জন্য আবেদন করছি।"
                    .concat(" আমি নিম্নবর্ণিত তথসমূহের সঠিক ও যথাযথ জবাব প্রদান করছি।");

    bodyTextBn = String.format(
            bodyTextBn,
            fundType,
            fund_managementDTO.cgsNumber,
            StringUtils.convertToBanNumber(String.valueOf(fund_managementDTO.advanceAmount))
    );

    String bodyTextEn =
            "I am applying to take advance of <b>%d</b> Taka from my General %s Fund deposited under CSS/SAO/administration/JSS No. "
                    .concat("<b>%s</b>. I am giving the correct and appropriate answer to the following information.");
    bodyTextEn = String.format(
            bodyTextEn,
            fund_managementDTO.advanceAmount,
            fundType,
            fund_managementDTO.cgsNumber
    );
    List<String> fundManagementQuestions =
            FundManagementQuestionRepository.getInstance()
                    .getNameDTOList()
                    .stream()
                    .map(nameDTO -> nameDTO.getText(Language))
                    .collect(Collectors.toList());
    int quesNumber = 0;

    List<Fund_approval_mappingDTO> fundApprovalMappingDTOs =
            Fund_approval_mappingDAO.getInstance().getAllApprovalDTOByFundId(id);

    List<Long> cardApprovalIds = fundApprovalMappingDTOs.stream()
            .map(e -> e.cardApprovalId)
            .collect(Collectors.toList());

    List<CardApprovalDTO> cardApprovalDTOList = CardApprovalRepository.getInstance().getByIds(cardApprovalIds);

    Map<Long, CardApprovalDTO> mapCardApprovalDTOById =
            cardApprovalDTOList.stream()
                    .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));

    Map<Boolean, List<Fund_approval_mappingDTO>> partitionByIsPending =
            fundApprovalMappingDTOs.stream()
                    .collect(Collectors.partitioningBy(
                            e -> e.fundApplicationStatusCat == FundApplicationStatus.WAITING_FOR_APPROVAL.getValue()
                    ));
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }

    input[readonly],
    textarea[readonly] {
        background-color: rgba(231, 231, 231, .5) !important;
        font-weight: bold;
        font-size: 1.1rem;
    }

    .view-application-text {
        margin-bottom: 3rem;
        font-size: 1.2rem;
    }
</style>


<div class="kt-content p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="ml-auto m-4">
            <button type="button" class="btn" id='printer' onclick="printDiv('fund-application')">
                <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
            </button>
        </div>
        <div class="kt-portlet__body m-4" style="background: #f9f9f9!important" id="fund-application">
            <div class="m-1 p-3" style="background: #fff!important">
                <div class="d-flex flex-column flex-md-row justify-content-md-between align-items-md-center my-5">
                    <div>
                        <%=LM.getText(LC.HM_DATE, loginDTO)%>
                        :
                        <%=convertBanglaIfLanguageIsBangla(
                                Language,
                                new SimpleDateFormat("dd/MM/yyyy").format(new Date()))%>
                    </div>
                    <div class="mt-4 mt-md-0">
                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO)%>
                        :
                        <%=convertBanglaIfLanguageIsBangla(Language, nid)%>
                    </div>
                </div>

                <%--Application Body--%>
                <div class="mb-4">
                    <div class="text-center mb-4">
                        <h4>
                            <%=String.format(LM.getText(LC.FUND_MANAGEMENT_VIEW_HEADING, loginDTO), fundType)%>
                        </h4>
                    </div>

                    <div class="view-application-text">
                        <%=getDataByLanguage(Language, bodyTextBn, bodyTextEn)%>
                    </div>

                    <div class="container view-application-text">
                        <div class="col-md-6 offset-md-6 px-0">
                            <div class="row">
                                <div class="col-6 text-md-right">
                                    <%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                                </div>
                                <div class="col-6 text-md-right">
                                    <%=getDataByLanguage(Language, fund_managementDTO.employeeNameBn, fund_managementDTO.employeeNameEn)%>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6 text-md-right">
                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                                </div>
                                <div class="col-6 text-md-right">
                                    <%=getDataByLanguage(Language, fund_managementDTO.organogramNameBn, fund_managementDTO.organogramNameEn)%>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6 text-md-right">
                                    <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                                </div>
                                <div class="col-6 text-md-right">
                                    <%=getDataByLanguage(Language, fund_managementDTO.officeNameBn, fund_managementDTO.officeNameEn)%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8">
                            <%
                                value = convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.valueOf(fund_managementDTO.q1Response)
                                );
                            %>
                            <input type='text' class='form-control' readonly
                                   value='<%=value%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8">
                            <textarea class='form-control' readonly><%=fund_managementDTO.q2Response%></textarea>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8">
                            <%
                                value = convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.valueOf(fund_managementDTO.q3Response)
                                );
                            %>
                            <input type='text' class='form-control' readonly
                                   value='<%=value%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right" for="q4Response">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8">
                            <input type='text' class='form-control' id="q4Response" readonly
                                   value='<%=fund_managementDTO.q4Response%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right" for="q5Response">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8">
                            <input type='text' class='form-control' id="q5Response" readonly
                                   value='<%=fund_managementDTO.q5Response%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8 row">
                            <div class="col-12 col-md-6">
                                <%
                                    value = convertBanglaIfLanguageIsBangla(
                                            Language,
                                            String.valueOf(fund_managementDTO.q6aResponse)
                                    );
                                %>
                                <input type='text' class='form-control'
                                       value='<%=value%> <%=getDataByLanguage(Language, "  টাকা মাসে", "  Taka monthly")%>'
                                       readonly>
                            </div>
                            <div class="col-12 col-md-6">
                                <%
                                    value = convertBanglaIfLanguageIsBangla(
                                            Language,
                                            String.valueOf(fund_managementDTO.q6bResponse)
                                    );
                                %>
                                <input type='text' class='form-control'
                                       value='<%=value%> <%=getDataByLanguage(Language, "  কিস্তি", "  installments")%>'
                                       readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8 row">
                            <div class="col-12 col-md-6">
                                <%
                                    value = convertBanglaIfLanguageIsBangla(
                                            Language,
                                            String.valueOf(fund_managementDTO.q7aResponse)
                                    );
                                %>
                                <input type='text' class='form-control'
                                       value='<%=value%> <%=getDataByLanguage(Language, "  টাকা মাসে", "  Taka monthly")%>'
                                       readonly>
                            </div>
                            <div class="col-12 col-md-6">
                                <%
                                    value = convertBanglaIfLanguageIsBangla(
                                            Language,
                                            String.valueOf(fund_managementDTO.q7bResponse)
                                    );
                                %>
                                <input type='text' class='form-control'
                                       value='<%=value%> <%=getDataByLanguage(Language, " কিস্তি", "  installments")%>'
                                       readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right" for="q8Response">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8">
                            <input type='text' class='form-control' id="q8Response" readonly
                                   value='<%=fund_managementDTO.q8Response%>'>
                        </div>
                    </div>

                    <div class="row mt-3 mx-0">
                        <label class="col-md-4 col-form-label text-md-right" for="q9Response">
                            <%=fundManagementQuestions.get(quesNumber++)%>
                        </label>
                        <div class="col-md-8">
                            <%
                                value = convertBanglaIfLanguageIsBangla(
                                        Language,
                                        String.valueOf(fund_managementDTO.q9Response)
                                );
                            %>
                            <input type='text' class='form-control' id="q9Response" readonly
                                   value='<%=value%>'>
                        </div>
                    </div>
                </div>

                <%--Applicant Information--%>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th>
                                    <%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                                </th>
                                <th>
                                    <%=LM.getText(LC.CARD_INFO_DESIGNATION_OFFICE, loginDTO)%>
                                </th>
                                <th>
                                    <%=LM.getText(LC.CARD_INFO_REQUEST_CREATE_DATE_TIME, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <%=getDataByLanguage(Language, fund_managementDTO.employeeNameBn, fund_managementDTO.employeeNameEn)%>
                                </td>
                                <td>
                                    <b>
                                        <%=getDataByLanguage(Language, fund_managementDTO.organogramNameBn, fund_managementDTO.organogramNameEn)%>
                                    </b>
                                    <br>
                                    <%=getDataByLanguage(Language, fund_managementDTO.officeNameBn, fund_managementDTO.officeNameEn)%>
                                </td>
                                <td>
                                    <%=StringUtils.convertToDateAndTime(isLanguageEnglish, fund_managementDTO.insertionDate)%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%--Approver Information--%>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.CARD_INFO_APPROVAL_INFORMATION, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVER_OFFICE_INFORMATION, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVAL_STATUS, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.CARD_INFO_APPROVAL_DATE_TIME, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <%--Pending Approvals--%>
                            <%
                                for (Fund_approval_mappingDTO fundApprovalMappingDTO : partitionByIsPending.getOrDefault(true, new ArrayList<>())) {
                                    CardApprovalDTO cardApprovalDTO = mapCardApprovalDTOById.get(fundApprovalMappingDTO.cardApprovalId);
                            %>
                            <tr>
                                <td style="width: 35%">
                                    <b><%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                    </b></td>
                                <td style="width: 35%">
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=FundApplicationStatus.getColor(fundApprovalMappingDTO.fundApplicationStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "fund_application_status", fundApprovalMappingDTO.fundApplicationStatusCat)%>
                                    </span>
                                </td>
                                <td style="width: 35%">
                                    <%if (fundApprovalMappingDTO.fundApplicationStatusCat != FundApplicationStatus.WAITING_FOR_APPROVAL.getValue()) {%>
                                    <%=StringUtils.convertToDateAndTime(isLanguageEnglish, fundApprovalMappingDTO.lastModificationTime)%>
                                    <%}%>
                                </td>
                            </tr>
                            <%}%>

                            <%--Not Approvals--%>
                            <%
                                for (Fund_approval_mappingDTO fundApprovalMappingDTO : partitionByIsPending.getOrDefault(false, new ArrayList<>())) {
                                    CardApprovalDTO cardApprovalDTO = mapCardApprovalDTOById.get(fundApprovalMappingDTO.cardApprovalId);
                            %>
                            <tr>
                                <td style="width: 35%">
                                    <b><%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                    </b></td>
                                <td style="width: 35%">
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=FundApplicationStatus.getColor(fundApprovalMappingDTO.fundApplicationStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "fund_application_status", fundApprovalMappingDTO.fundApplicationStatusCat)%>
                                    </span>
                                    <%if (fundApprovalMappingDTO.fundApplicationStatusCat == FundApplicationStatus.REJECTED.getValue()) {%>
                                    <div class="container mt-3">
                                        <b><%=LM.getText(LC.BUDGET_COMMENT, loginDTO)%>
                                        </b>
                                        <br>
                                        <%=fundApprovalMappingDTO.comment%>
                                    </div>
                                    <%}%>
                                </td>
                                <td style="width: 35%">
                                    <%if (fundApprovalMappingDTO.fundApplicationStatusCat != FundApplicationStatus.WAITING_FOR_APPROVAL.getValue()) {%>
                                    <%=StringUtils.convertToDateAndTime(isLanguageEnglish, fundApprovalMappingDTO.lastModificationTime)%>
                                    <%}%>
                                </td>
                            </tr>
                            <%}%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function printDiv(divId) {
        let printContents = document.getElementById(divId).innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
