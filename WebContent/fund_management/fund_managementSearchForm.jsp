<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="fund_management.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>

<%
    String navigator2 = "navFUND_MANAGEMENT";
    String servletName = "Fund_managementServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
			<th>
				<%=LM.getText(LC.FUND_MANAGEMENT_ADD_EMPLOYEERECORDSID, loginDTO)%>
			</th>
            <th>
				<%=LM.getText(LC.FUND_MANAGEMENT_ADD_FUNDTYPECAT, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.FUND_MANAGEMENT_ADD_CGSNUMBER, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.FUND_MANAGEMENT_ADD_ADVANCEAMOUNT, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%>
            </th>
            <th>
				<%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Fund_managementDTO> data = (List<Fund_managementDTO>) recordNavigator.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (Fund_managementDTO fund_managementDTO : data) {
        %>
                        <tr>
                            <td>
                                <%=getDataByLanguage(Language, fund_managementDTO.employeeNameBn, fund_managementDTO.employeeNameEn)%>
                                <br>
                                <b>
                                    <%=getDataByLanguage(Language, fund_managementDTO.organogramNameBn, fund_managementDTO.organogramNameEn)%>
                                </b>
                                <br>
                                <%=getDataByLanguage(Language, fund_managementDTO.officeNameBn, fund_managementDTO.officeNameEn)%>
                            </td>

                            <td>
                                <%=CatRepository.getInstance().getText(Language, "fund_type", fund_managementDTO.fundTypeCat)%>
                            </td>

                            <td>
                                <%=fund_managementDTO.cgsNumber%>
                            </td>

                            <td>
                                <%=Utils.getDigits(fund_managementDTO.advanceAmount, Language)%>
                            </td>

                            <td>
                                <span class="btn btn-sm border-0 shadow"
                                      style="background-color: <%=FundApplicationStatus.getColor(fund_managementDTO.fundApplicationStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(
                                                Language, "fund_application_status", fund_managementDTO.fundApplicationStatusCat
                                        )%>
                                </span>
                            </td>

                            <td>
                                <button
                                        type="button"
                                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                                        style="color: #ff6b6b;"
                                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=fund_managementDTO.iD%>'"
                                >
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>