<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="fund_management.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="fund_management_question.FundManagementQuestionRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getLanguage(loginDTO);

    String actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";

    Fund_managementDTO fund_managementDTO;
    fund_managementDTO = (Fund_managementDTO) request.getAttribute("fund_managementDTO");
    if (fund_managementDTO == null) {
        fund_managementDTO = new Fund_managementDTO();
    }

    String value = "";
    String context = request.getContextPath() + "/";

    String formTitle = LM.getText(LC.FUND_MANAGEMENT_ADD_FUND_MANAGEMENT_ADD_FORMNAME, loginDTO);

    List<String> fundManagementQuestions =
            FundManagementQuestionRepository.getInstance()
                    .getNameDTOList()
                    .stream()
                    .map(nameDTO -> nameDTO.getText(Language))
                    .collect(Collectors.toList());
    int quesNumber = 0;

    EmployeeFlatInfoDTO employeeFlatInfoDTO;
    try {
        employeeFlatInfoDTO = EmployeeFlatInfoDTO.getFlatInfoOfDefaultOffice(userDTO.employee_record_id, Language);
    } catch (Exception ex) {
        employeeFlatInfoDTO = new EmployeeFlatInfoDTO();
        ex.printStackTrace();
    }
%>

<style>
    .employee-info {
        padding: 1rem 2rem;
        background: #f2fcff;
    }

    .employee-info-heading {
        display: block;
        font-size: 1.25rem;
    }

    .employee-info-data {
        color: #8bc9b0;
        font-weight: bold;
        font-size: 1.4rem;
    }

    .required {
        font-size: 1.5rem;
        color: red;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mt-3 employee-info">
                <div class="col-md-6 col-lg-3">
                    <label class="employee-info-heading">
                        <%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                    </label>
                    <label class="employee-info-data">
                        <%=UtilCharacter.getDataByLanguage(Language, employeeFlatInfoDTO.employeeNameBn, employeeFlatInfoDTO.employeeNameEn)%>
                    </label>
                </div>
                <div class="col-md-6 col-lg-3">
                    <label class="employee-info-heading">
                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                    </label>
                    <label class="employee-info-data">
                        <%=UtilCharacter.getDataByLanguage(Language, employeeFlatInfoDTO.organogramNameBn, employeeFlatInfoDTO.organogramNameEn)%>
                    </label>
                </div>
                <div class="col-md-6 col-lg-3">
                    <label class="employee-info-heading">
                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                    </label>
                    <label class="employee-info-data">
                        <%=UtilCharacter.getDataByLanguage(Language, employeeFlatInfoDTO.officeNameBn, employeeFlatInfoDTO.officeNameEn)%>
                    </label>
                </div>
                <div class="col-md-6 col-lg-3">
                    <label class="employee-info-heading">
                        <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NID, loginDTO)%>
                    </label>
                    <label class="employee-info-data">
                        <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, employeeFlatInfoDTO.nid)%>
                    </label>
                </div>
            </div>
            <form id="bigform" name="bigform" enctype="multipart/form-data" 
                  action="Fund_managementServlet?actionType=ajax_<%=actionName%>">
                <div class="row mt-3 mx-0">
                    <input type='hidden' class='form-control' name='iD' value='<%=fund_managementDTO.iD%>'/>

                    <div class="col-md-6 col-lg-4 pl-0 pr-0 pr-md-1 form-group">
                        <label for="fundTypeCat" class="h5">
                            <%=LM.getText(LC.FUND_MANAGEMENT_ADD_FUNDTYPECAT, loginDTO)%>
                            <span class="required"> * </span>
                        </label>
                        <select class='form-control rounded shadow-sm w-100' name='fundTypeCat' id='fundTypeCat'>
                            <%=CatRepository.getInstance().buildOptions("fund_type", Language, fund_managementDTO.fundTypeCat)%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 pl-0 pr-0 pr-md-1 form-group">
                        <label for="cgsNumber" class="h5">
                            <%=LM.getText(LC.FUND_MANAGEMENT_ADD_CGSNUMBER, loginDTO)%>
                            <span class="required"> * </span>
                        </label>
                        <input type='text' class='form-control rounded shadow-sm w-100' name='cgsNumber'
                               id='cgsNumber' value='<%=fund_managementDTO.cgsNumber%>'/>
                    </div>

                    <div class="col-md-6 col-lg-4 px-0 form-group">
                        <label for="advanceAmount" class="h5">
                            <%=LM.getText(LC.FUND_MANAGEMENT_ADD_ADVANCEAMOUNT, loginDTO)%>
                            <span class="required"> * </span>
                        </label>
                        <input type='text' class='form-control rounded shadow-sm w-100' name='advanceAmount'
                               id='advanceAmount' value='<%=value%>'>
                    </div>
                </div>

                <div class="row mt-3">
                    <label class="col-md-4 col-form-label text-md-right" for="q1Response">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8">
                        <%
                            value = fund_managementDTO.q1Response == 0 ? "" : fund_managementDTO.q1Response + "";
                        %>
                        <input type='text' class='form-control' name='q1Response' id='q1Response'
                               data-only-integer="true"
                               value='<%=value%>'>
                    </div>
                </div>

                <div class="row mt-3">
                    <label class="col-md-4 col-form-label text-md-right" for="q2Response">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8">
                        <textarea class='form-control' name='q2Response'
                                  maxlength="<%=Fund_managementServlet.TEXT_AREA_MAX_LEN%>"
                                  id='q2Response'
                        ><%=fund_managementDTO.q2Response%></textarea>
                    </div>
                </div>

                <div class="row mt-3">
                    <label class="col-md-4 col-form-label text-md-right" for="q3Response">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8">
                        <%
                            value = fund_managementDTO.q3Response == 0 ? "" : fund_managementDTO.q3Response + "";
                        %>
                        <input type='text' class='form-control' name='q3Response' id='q3Response'
                               data-only-integer="true"
                               value='<%=value%>'>
                    </div>
                </div>

                <div class="row mt-3">
                    <label class="col-md-4 col-form-label text-md-right" for="q4Response">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8">
                        <input name="q4Response" type="text" class='form-control' id="q4Response">
                    </div>
                </div>

                <div class="row mt-3">
                    <label class="col-md-4 col-form-label text-md-right" for="q5Response">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8">
                        <input name="q5Response" type="text" class='form-control' id="q5Response">
                    </div>
                </div>

                <div class="row mt-3">
                    <label class="col-md-4 col-form-label text-md-right">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                    </label>
                    <div class="col-md-8 row pr-0">
                        <div class="col-md-6 pr-0">
                            <%
                                value = fund_managementDTO.q6aResponse == 0 ? "" : fund_managementDTO.q6aResponse + "";
                            %>
                            <input type='text' class='form-control' name='q6aResponse' data-only-integer="true"
                                   placeholder="<%=UtilCharacter.getDataByLanguage(Language, "মাসিক হার (টাকায়)", "Monthly Rate (in Taka)")%>"
                                   value='<%=value%>'>
                        </div>
                        <div class="col-md-6 pr-0">
                            <%
                                value = fund_managementDTO.q6bResponse == 0 ? "" : fund_managementDTO.q6bResponse + "";
                            %>
                            <input type='text' class='form-control' name='q6bResponse' data-only-integer="true"
                                   placeholder="<%=UtilCharacter.getDataByLanguage(Language, "কিস্তি সংখ্যা", "Number of Installments")%>"
                                   value='<%=value%>'>
                        </div>
                    </div>
                </div>

                <div class="row mt-3 ">
                    <label class="col-md-4 col-form-label text-md-right">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8 row pr-0">
                        <div class="col-12 col-md-6 pr-0">
                            <%
                                value = fund_managementDTO.q7aResponse == 0 ? "" : fund_managementDTO.q7aResponse + "";
                            %>
                            <input type='text' class='form-control' name='q7aResponse' data-only-integer="true"
                                   placeholder="<%=UtilCharacter.getDataByLanguage(Language, "মাসিক হার (টাকায়)", "Monthly Rate (in Taka)")%>"
                                   value='<%=value%>'>
                        </div>
                        <div class="col-12 col-md-6 pr-0">
                            <%
                                value = fund_managementDTO.q7bResponse == 0 ? "" : fund_managementDTO.q7bResponse + "";
                            %>
                            <input type='text' class='form-control' name='q7bResponse' data-only-integer="true"
                                   placeholder="<%=UtilCharacter.getDataByLanguage(Language, "কিস্তি সংখ্যা", "Number of Installments")%>"
                                   value='<%=value%>'>
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <label class="col-md-4 col-form-label text-md-right" for="q8Response">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                    </label>
                    <div class="col-md-8">
                        <input name="q8Response" type="text" class='form-control' id="q8Response">
                    </div>
                </div>

                <div class="row mt-3">
                    <label class="col-md-4 col-form-label text-md-right" for="q9Response">
                        <%=fundManagementQuestions.get(quesNumber++)%>
                    </label>
                    <div class="col-md-8">
                        <input type='text' class='form-control' name='q9Response' id="q9Response"
                               value='<%=fund_managementDTO.q9Response%>'>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-md-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.FUND_MANAGEMENT_ADD_FUND_MANAGEMENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitForm()">
                                <%=LM.getText(LC.FUND_MANAGEMENT_ADD_FUND_MANAGEMENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const form = $('#bigform');

    function typeOnlyInteger(e) {
        return true === inputValidationForPositiveValue(e, $(this), <%=Integer.MAX_VALUE%>, 0);
    }

    $(() => {
        document.querySelectorAll('[data-only-integer="true"]')
                .forEach(inputField => inputField.onkeydown = typeOnlyInteger);

        $.validator.addMethod('textAreaValidator', function (value, element) {
            return value.trim() !== '';
        });

        form.validate({
            rules: {
                fundTypeCat: "required",
                cgsNumber: "required",
                advanceAmount: {
                    required: true,
                    min: 1
                },
                q1Response: {
                    required: true,
                    min: 1
                },
                q2Response: {
                    textAreaValidator: true
                },
                q3Response: {
                    required: true,
                    min: 1
                },
                q4Response: "required",
                q5Response: "required",
                q7aResponse: {
                    required: true,
                    min: 1
                },
                q7bResponse: {
                    required: true,
                    min: 1
                }
            },
            messages: {
                fundTypeCat: '<%=UtilCharacter.getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                cgsNumber: '<%=UtilCharacter.getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                advanceAmount: '<%=UtilCharacter.getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>',
                q1Response: '<%=UtilCharacter.getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>',
                q2Response: '<%=UtilCharacter.getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                q3Response: '<%=UtilCharacter.getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>',
                q4Response: '<%=UtilCharacter.getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                q5Response: '<%=UtilCharacter.getDataByLanguage(Language, "আবশ্যক", "Mendatory")%>',
                q7aResponse: '<%=UtilCharacter.getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>',
                q7bResponse: '<%=UtilCharacter.getDataByLanguage(Language, "সঠিক সংখ্যা দিন", "Enter valid number")%>',
            }
        });
    });

    function submitForm() {
        if (form.valid()) {
            submitAjaxForm();
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>