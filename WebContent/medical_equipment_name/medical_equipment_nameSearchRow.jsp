<%@page pageEncoding="UTF-8" %>

<%@page import="medical_equipment_name.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.MEDICAL_EQUIPMENT_NAME_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_MEDICAL_EQUIPMENT_NAME;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Medical_equipment_nameDTO medical_equipment_nameDTO = (Medical_equipment_nameDTO) request.getAttribute("medical_equipment_nameDTO");
    CommonDTO commonDTO = medical_equipment_nameDTO;
    String servletName = "Medical_equipment_nameServlet";


    System.out.println("medical_equipment_nameDTO = " + medical_equipment_nameDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Medical_equipment_nameDAO medical_equipment_nameDAO = (Medical_equipment_nameDAO) request.getAttribute("medical_equipment_nameDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn' class="text-nowrap">
    <%
        value = medical_equipment_nameDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn' class="text-nowrap">
    <%
        value = medical_equipment_nameDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td>
<%=CatDAO.getName(Language, "medical_dept", medical_equipment_nameDTO.medicalDeptCat) %>
</td>

<td>
<%=CatDAO.getName(Language, "medical_equipment", medical_equipment_nameDTO.medicalEquipmentCat) %>
</td>

<td id='<%=i%>_quantityUnit' class="text-nowrap">
    <%
        value = medical_equipment_nameDTO.quantityUnit + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_currentStock' class="text-nowrap">
    <%
        value = medical_equipment_nameDTO.currentStock + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_stockAlertRequired' class="text-nowrap">
    <%
        value = medical_equipment_nameDTO.stockAlertRequired + "";
    %>

    <%=Utils.getYesNo(value, Language)%>


</td>

<td id='<%=i%>_stockAlertQuantity' class="text-nowrap">
    <%
        value = medical_equipment_nameDTO.stockAlertQuantity + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Medical_equipment_nameServlet?actionType=view&ID=<%=medical_equipment_nameDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Medical_equipment_nameServlet?actionType=getEditPage&ID=<%=medical_equipment_nameDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
	<div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=medical_equipment_nameDTO.iD%>'/>
        </span>
	</div>
</td>
											

