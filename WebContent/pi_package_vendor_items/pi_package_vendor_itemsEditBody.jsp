<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="pi_package_vendor_items.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Pi_package_vendor_itemsDTO pi_package_vendor_itemsDTO = new Pi_package_vendor_itemsDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	pi_package_vendor_itemsDTO = Pi_package_vendor_itemsDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = pi_package_vendor_itemsDTO;
String tableName = "pi_package_vendor_items";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PI_PACKAGE_VENDOR_ITEMS_ADD_FORMNAME, loginDTO);
String servletName = "Pi_package_vendor_itemsServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_package_vendor_itemsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='piPackageVendorId' id = 'piPackageVendorId_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.piPackageVendorId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='piPackageVendorChildrenId' id = 'piPackageVendorChildrenId_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.piPackageVendorChildrenId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='packageId' id = 'packageId_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.packageId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='fiscalYearId' id = 'fiscalYearId_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.fiscalYearId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='productId' id = 'productId_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.productId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PRICE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_package_vendor_itemsDTO.price != -1)
																	{
																	value = pi_package_vendor_itemsDTO.price + "";
																	}
																%>		
																<input type='number' class='form-control'  name='price' id = 'price_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.insertionDate%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_INSERTEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=pi_package_vendor_itemsDTO.insertedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_MODIFIEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=pi_package_vendor_itemsDTO.modifiedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=pi_package_vendor_itemsDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.lastModificationTime%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=pi_package_vendor_itemsDTO.searchColumn%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PI_PACKAGE_VENDOR_ITEMS_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PI_PACKAGE_VENDOR_ITEMS_ADD_PI_PACKAGE_VENDOR_ITEMS_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Pi_package_vendor_itemsServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






