<%@page pageEncoding="UTF-8" %>
<%@ page import="util.*" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="overtime_bill.Overtime_billServlet" %>
<%@ page import="overtime_bill.Overtime_billDTO" %>
<%@ page import="overtime_allowance.Overtime_allowanceModel" %>
<%@ page import="java.util.List" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="ot_bill_submission_config.OT_bill_submission_configDAO" %>
<%@ page import="overtime_bill.OvertimeType" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    Office_unitsDTO officeUnitsDTO = Overtime_billServlet.getOfficeUnitFromUser(userDTO);
    boolean canSelectOffice = Overtime_billServlet.isAllowedToSeeAllOfficeBill(userDTO);

    String context = request.getContextPath() + "/";
    boolean isLanguageEnglish = "english".equalsIgnoreCase(Language);
    String formTitle = isLanguageEnglish ? "OVERTIME BILL SUBMIT (OFFICE/SECTION)" : "অধিকাল ভাতার বিল জমাদান (কার্যালয়/শাখা)";

    String actionName = "ajax_add";
    boolean isEditPage = false;

    Overtime_billDTO overtimeBillDTO = (Overtime_billDTO) request.getAttribute("overtimeBillDTO");

    if ("getEditPage".equalsIgnoreCase(request.getParameter("actionType"))) {
        actionName = "ajax_edit";
        isEditPage = true;
    }
    String notEditableCause = (String) request.getAttribute("notEditableCause");
%>

<style>
    #overtime-bill-table th {
        vertical-align: top;
        text-align: center;
    }

    .loader-container-circle {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .2);
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 10;
        visibility: visible;
    }

    @media (min-width: 1015px) {
        .loader-container-circle {
            width: calc(100% + 260px);
            height: 100%;
            background-color: rgba(0, 0, 0, 0.1);
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 3;
            visibility: visible;
        }
    }

    .loader-circle {
        width: 50px;
        height: 50px;
        border: 5px solid;
        color: #3498db;
        border-radius: 50%;
        border-top-color: transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        25% {
            color: #2ecc71;
        }
        50% {
            color: #f1c40f;
        }
        75% {
            color: #e74c3c;
        }
        to {
            transform: rotate(360deg);
        }
    }

    .no-data-found-div {
        text-align: center;
        background-color: #eee;
        padding: .875rem;
    }

    .template-div {
        display: none !important;
    }

    .height-auto {
        height: auto !important;
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="overtime-bill-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-4 form-group">
                        <label class="h5" for="office_units_id_input">
                            <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                        </label>
                        <div>
                            <%if (isEditPage) {%>
                            <div class="form-control height-auto">
                                <%=Office_unitsRepository.getInstance().geText(Language, overtimeBillDTO.officeUnitId)%>
                                <input type="hidden" name='officeUnitId' id='office_units_id_input'
                                       value="<%=overtimeBillDTO.officeUnitId%>">
                            </div>
                            <%} else if (canSelectOffice) {%>
                            <button type="button"
                                    class="btn btn-block shadow btn-primary text-white btn-border-radius"
                                    id="office_units_id_modal_button"
                                    onclick="officeModalButtonClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <div class="input-group" id="office_units_id_div" style="display: none">
                                <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                                <button type="button"
                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control height-auto"
                                        id="office_units_id_text"
                                        onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn" style="width: 5%;padding-right: 5px;">
                                        <button type="button" class="btn btn-outline-danger"
                                                style="height: 100%;"
                                                onclick="crsBtnClicked('office_units_id');"
                                                id='office_units_id_crs_btn' tag='pb_html'>
                                            &times;
                                        </button>
                                </span>
                            </div>
                            <%} else if (officeUnitsDTO != null) {%>
                            <div class="form-control height-auto">
                                <%=isLanguageEnglish ? officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng%>
                                <input type="hidden" name='officeUnitId' id='office_units_id_input'
                                       value="<%=officeUnitsDTO.iD%>">
                            </div>
                            <%} else {%>
                            <div class="form-control height-auto">
                                <%=isLanguageEnglish ? "Could not find users Office" : "ইউজারের দপ্তর পাওয়া যায়নি"%>
                                <input type="hidden" name='officeUnitId' id='office_units_id_input' value="-1">
                            </div>
                            <%}%>
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <label class="h5" for="otEmployeeTypeId">
                            <%=isLanguageEnglish ? "Employee Type" : "কর্মকর্তা/কর্মচারীর ধরণ"%>
                        </label>
                        <div>
                            <%if (isEditPage) {%>
                            <div class="form-control">
                                <%=OT_employee_typeRepository.getInstance().getTextById(overtimeBillDTO.otEmployeeTypeId, isLanguageEnglish)%>
                                <input type='hidden' name='otEmployeeTypeId' id='otEmployeeTypeId'
                                       value='<%=overtimeBillDTO.otEmployeeTypeId%>'>
                            </div>
                            <%} else {%>
                            <select class='form-control rounded shadow-sm'
                                    name='otEmployeeTypeId' id='otEmployeeTypeId'
                                    onchange="loadData();">
                                <%if (officeUnitsDTO != null) {%>
                                <%=OT_employee_typeRepository.getInstance().buildOptionWithOfficeUnitId(
                                        Language,
                                        officeUnitsDTO.iD,
                                        null
                                )%>
                                <%}%>
                            </select>
                            <%}%>
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <label class="h5">
                            <%=isLanguageEnglish ? "Bill Date" : "বিলের তারিখ"%>
                        </label>
                        <div>
                            <%if (isEditPage) {%>
                            <div class="form-control">
                                <%=overtimeBillDTO.getBillDateTypeText(isLanguageEnglish)%>
                                <input type='hidden' name='otBillSubmissionConfigId' id='otBillSubmissionConfigId'
                                       value='<%=overtimeBillDTO.otBillSubmissionConfigId%>'>
                            </div>
                            <%} else {%>
                            <select class='form-control rounded shadow-sm'
                                    name="otBillSubmissionConfigId"
                                    id="otBillSubmissionConfigId"
                                    onchange="loadData();">
                                <%=OT_bill_submission_configDAO.getInstance().buildOptionsOfSubmittableDTOs(
                                        officeUnitsDTO == null ? null : officeUnitsDTO.iD,
                                        System.currentTimeMillis(),
                                        Language
                                )%>
                            </select>
                            <%}%>
                        </div>
                    </div>
                </div>

                <%if (notEditableCause != null) {%>
                <div class="container text-center m-4 text-danger">
                    <h5>
                        <%=notEditableCause%>
                    </h5>
                </div>
                <%}%>

                <div id="overtime-bill-ajax-data-table-div"
                     class="mt-4 w-100"
                     style="overflow-x: scroll">
                    <%if (isEditPage) {%>
                    <jsp:include page="overtime_billTable.jsp"/>
                    <%} else {%>
                    <div class="no-data-found-div">
                        <%=isLanguageEnglish ? "No Data Found" : "কোন তথ্য পাওয়া যায়নি"%>
                    </div>
                    <%}%>
                </div>

                <div id="no-data-found-div" class="template-div no-data-found-div">
                    <%=isLanguageEnglish ? "No Data Found" : "কোন তথ্য পাওয়া যায়নি"%>
                </div>

                <div id="action-btn-div" class="row" style="display: none">
                    <div class="col-12 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=isLanguageEnglish ? "Cancel" : "বাতিল করুন"%>
                        </button>
                        <button id="preview-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="submitForm('preview')">
                            <%=isLanguageEnglish ? "See Preview" : "প্রিভিউ দেখুন"%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script>
    const id = '<%=isEditPage? overtimeBillDTO.iD : -1%>';
    const isEditPage = <%=isEditPage%>;
    const otBillSubmissionConfigIdInput = $('#otBillSubmissionConfigId');
    const officeUnitsIdInput = $('#office_units_id_input');
    const otEmployeeTypeIdInput = $('#otEmployeeTypeId');
    const fullPageLoader = $('#full-page-loader');
    const KA_VALUE = '<%=OvertimeType.KA.getValue()%>';
    const KHA_VALUE = '<%=OvertimeType.KHA.getValue()%>';
    const actionBtnDiv = $('#action-btn-div');
    const ajaxDataTableDiv = document.getElementById('overtime-bill-ajax-data-table-div');
    const noDataFoundDiv = document.getElementById('no-data-found-div');

    function eraseBtnClicked(btnElement) {
        const nearestTr = btnElement.closest('tr.has-overtime-allowance-model');
        const overtimeAllowanceModel = JSON.parse(nearestTr.dataset.model);
        if (overtimeAllowanceModel.overtimeAllowanceId > 0) {
            const deletedOvertimeAllowanceIdsInput = document.getElementById('deletedOvertimeAllowanceIds');
            const deletedOvertimeAllowanceIds = JSON.parse(deletedOvertimeAllowanceIdsInput.value);
            deletedOvertimeAllowanceIds.push(overtimeAllowanceModel.overtimeAllowanceId);
            deletedOvertimeAllowanceIdsInput.value = JSON.stringify(deletedOvertimeAllowanceIds);
        }
        nearestTr.remove();
        [...document.querySelectorAll('input[name="serialNo"]')]
            .forEach((serialNoInput, index) => {
                serialNoInput.value = index + 1;
                changeSerialNumber(serialNoInput);
            });
    }

    function submitForm(source) {
        const officeUnitsId = officeUnitsIdInput.val();
        if (officeUnitsId === '') {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("দপ্তর বাছাই করুন", "Select Office");
            return;
        }
        const otEmployeeTypeId = otEmployeeTypeIdInput.val();
        if (otEmployeeTypeId === '') {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("কর্মকর্তা/কর্মচারীর ধরণ বাছাই করুন", "Select Employee Type");
            return;
        }
        const otBillSubmissionConfigId = otBillSubmissionConfigIdInput.val();
        if (otBillSubmissionConfigId === '') {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("বিলের তারিখ বাছাই করুন", "Select Bill Date");
            return;
        }

        const overtimeAllowanceModels = Array.from(document.querySelectorAll('tr.has-overtime-allowance-model'))
                                             .map(tableTr => JSON.parse(tableTr.dataset.model));
        if (overtimeAllowanceModels.length === 0) {
            $('#toast_message').css('background-color', '#ff6063');
            showToast("জমা দেবার মত কোন তথ্য নেই", "No Data To Submit");
            return;
        }
        const data = {
            id: id,
            officeUnitsId: officeUnitsId,
            otEmployeeTypeId: otEmployeeTypeId,
            otBillSubmissionConfigId: otBillSubmissionConfigId,
            overtimeAllowanceModels: JSON.stringify(overtimeAllowanceModels),
            deletedOvertimeAllowanceIds: document.getElementById('deletedOvertimeAllowanceIds').value,
            source: source
        };
        console.log({data});

        setButtonDisableState(true);
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Overtime_billServlet?actionType=<%=actionName%>",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                             + ", Message: " + errorThrown);
            }
        });
    }

    function setButtonDisableState(value) {
        $('#submit-btn').prop('disabled', value);
        $('#preview-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function calculateAndSetKaKhaDays(overtimeAllowanceModel) {
        let kaDaysInt = 0, khaDaysInt = 0;
        for (let overtimeType of overtimeAllowanceModel.overtimeTypes) {
            if (overtimeType === KA_VALUE) ++kaDaysInt;
            else if (overtimeType === KHA_VALUE) ++khaDaysInt;
        }
        overtimeAllowanceModel.kaDays = "" + kaDaysInt;
        overtimeAllowanceModel.khaDays = "" + khaDaysInt;
    }

    function changeOvertimeTypeDaysCount(selectItem, dayIndex) {
        const nearestTr = selectItem.closest('tr.has-overtime-allowance-model');
        const overtimeAllowanceModel = JSON.parse(nearestTr.dataset.model);
        overtimeAllowanceModel.overtimeTypes[dayIndex] = selectItem.value;
        calculateAndSetKaKhaDays(overtimeAllowanceModel);

        const kaDaysCountTd = nearestTr.querySelector('td.ka-days-count');
        kaDaysCountTd.innerText = convertToBanglaNumIfBangla(String(overtimeAllowanceModel.kaDays), '<%=Language%>');

        const khaDaysCountTd = nearestTr.querySelector('td.kha-days-count');
        khaDaysCountTd.innerText = convertToBanglaNumIfBangla(String(overtimeAllowanceModel.khaDays), '<%=Language%>');

        nearestTr.dataset.model = JSON.stringify(overtimeAllowanceModel);
    }

    function changeSerialNumber(serialNoInput) {
        const nearestTr = serialNoInput.closest('tr.has-overtime-allowance-model');
        const overtimeAllowanceModel = JSON.parse(nearestTr.dataset.model);
        overtimeAllowanceModel.serialNo = serialNoInput.value;
        nearestTr.dataset.model = JSON.stringify(overtimeAllowanceModel);
    }

    function clearTable() {
        ajaxDataTableDiv.innerHTML = '';
        const noDataFoundDivClone = noDataFoundDiv.cloneNode(true);
        noDataFoundDivClone.classList.remove('template-div');
        ajaxDataTableDiv.append(noDataFoundDivClone);
    }

    async function loadData() {
        const otBillSubmissionConfigId = otBillSubmissionConfigIdInput.val();
        const officeUnitsId = officeUnitsIdInput.val();
        const otEmployeeTypeId = otEmployeeTypeIdInput.val();

        clearTable();
        if (otBillSubmissionConfigId === '' || officeUnitsId === '' || otEmployeeTypeId === '') {
            console.warn('mandatory data missing. not doing ajax call');
            return;
        }
        fullPageLoader.show();

        const url = 'Overtime_billServlet?actionType=ajax_getOvertimeBillData'
                    + '&otBillSubmissionConfigId=' + otBillSubmissionConfigId
                    + '&officeUnitsId=' + officeUnitsId
                    + '&otEmployeeTypeId=' + otEmployeeTypeId;
        const response = await fetch(url);
        ajaxDataTableDiv.innerHTML = await response.text();
        const isEditableInput = ajaxDataTableDiv.querySelector('input[name="isEditable"]');
        const isEditable = isEditableInput && (isEditableInput.value === 'true');
        if (isEditable) {
            actionBtnDiv.show();
        } else {
            actionBtnDiv.hide();
        }
        fullPageLoader.hide();
    }

    $(() => {
        fullPageLoader.hide();
        if (isEditPage) {
            actionBtnDiv.show();
        } else {
            actionBtnDiv.hide();
        }
    });

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        otEmployeeTypeIdInput.html('');
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();
        $('#office_error_id').hide();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        officeUnitsIdInput.val(selectedOffice.id);
    }

    async function officeUnitIdChanged(selectedOffice) {
        viewOfficeIdInInput(selectedOffice);
        otEmployeeTypeIdInput.html('');
        const officeUnitId = selectedOffice.id;
        const url = 'OT_employee_typeServlet?actionType=ajax_getOptionsWithOfficeUnitId'
                    + '&officeUnitId=' + officeUnitId;
        const response = await fetch(url);
        otEmployeeTypeIdInput.html(await response.text());
        await loadData();
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: officeUnitIdChanged
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers(officeUnitsIdInput.val());
        $('#search_office_modal').modal();
    }
</script>