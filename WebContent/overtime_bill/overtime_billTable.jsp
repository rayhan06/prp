<%@ page import="overtime_allowance.Overtime_allowanceModel" %>
<%@ page import="util.StringUtils" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="overtime_bill.OvertimeType" %>
<%@ page import="overtime_bill.Overtime_billTableData" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="java.util.Map" %>
<%@page pageEncoding="UTF-8" %>

<%
    String language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEn = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

    Overtime_billTableData overtimeBillTableData = (Overtime_billTableData) request.getAttribute("overtimeBillTableData");
    Overtime_allowanceModel.fixOrdering(overtimeBillTableData.allowanceModels);
    boolean setEmptyDataError = overtimeBillTableData.errorMessage == null &&
                                (overtimeBillTableData.allowanceModels == null || overtimeBillTableData.allowanceModels.isEmpty());
    if (setEmptyDataError) {
        overtimeBillTableData.errorMessage = isLangEn ? "No new information found to add" : "যোগ করার মত নতুন কোন তথ্য পাওয়া যায়নি";
    }
    LinkedHashMap<Long, Long> dateCountBySortedMonth =
            overtimeBillTableData
                    .sortedBillDates
                    .stream()
                    .sequential()
                    .collect(Collectors.groupingBy(
                            DateUtils::get1stDayOfMonth,
                            LinkedHashMap::new,
                            Collectors.counting()
                    ));
    Gson gson = new Gson();
    int numberOfDays = overtimeBillTableData.sortedBillDates.size();
%>

<%if (overtimeBillTableData.errorMessage != null) {%>

<input type="hidden" name="isEditable" value="false">
<div id="no-data-found-div" class="no-data-found-div">
    <%=overtimeBillTableData.errorMessage%>
</div>

<%} else {%>

<input type="hidden" name="isEditable" value="<%=overtimeBillTableData.isEditable%>">

<style>
    .serial-column {
        max-width: 12ch;
    }

    .erase-column {
        max-width: 10ch;
    }
</style>

<div class="table-responsive">
    <table id="overtime-bill-table" class="table table-bordered table-bordered-custom text-nowrap">
        <thead>
        <tr>
            <%if (Boolean.TRUE.equals(overtimeBillTableData.isEditable)) {%>
            <th rowspan="2" class="erase-column">
                <%=isLangEn ? "Erase" : "মুছুন"%>
            </th>
            <%}%>
            <th rowspan="2" class="serial-column">
                <%=isLangEn ? "Serial<br>No." : "ক্রমিক<br>নং"%>
            </th>
            <th rowspan="2">
                <%=isLangEn ? "Name & Designation" : "নাম ও পদবী"%>
            </th>
            <%
                for (Map.Entry<Long, Long> monthDatesCount : dateCountBySortedMonth.entrySet()) {
                    String monthYearName = DateUtils.getMonthYear(
                            monthDatesCount.getKey(),
                            "bangla"
                    );
                    long colSpan = monthDatesCount.getValue();
            %>
            <th colspan="<%=colSpan%>">
                <%=monthYearName%>
            </th>
            <%}%>
            <th rowspan="2">
                'ক' <%=isLangEn ? "<br>Days" : "<br>দিন<br>সংখ্যা"%>
            </th>
            <th rowspan="2">
                'খ' <%=isLangEn ? "<br>Days" : "<br>দিন<br>সংখ্যা"%>
            </th>
        </tr>
        <tr>
            <%for (Long billDate : overtimeBillTableData.sortedBillDates) {%>
            <th>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(
                        language,
                        String.format("%d", DateUtils.getDayOfTheMonth(billDate))
                )%>
            </th>
            <%}%>
        </tr>
        </thead>


        <tbody>
        <%if (Boolean.TRUE.equals(overtimeBillTableData.isEditable)) {%>
        <input type="hidden"
               id="deletedOvertimeAllowanceIds"
               value="<%=gson.toJson(overtimeBillTableData.deletedOvertimeAllowanceIds)%>">
        <%}%>
        <%for (Overtime_allowanceModel model : overtimeBillTableData.allowanceModels) {%>
        <tr
                <%if (Boolean.TRUE.equals(overtimeBillTableData.isEditable)) {%>
                data-model='<%=gson.toJson(model)%>' class="has-overtime-allowance-model"
                <%}%>
        >
            <%if (Boolean.TRUE.equals(overtimeBillTableData.isEditable)) {%>
            <td class="erase-column">
                <button type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="eraseBtnClicked(this)">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
            <%}%>
            <td class="text-center serial-column">
                <%if (Boolean.FALSE.equals(overtimeBillTableData.isEditable)) {%>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(language, String.format("%d", model.serialNo))%>.
                <%} else {%>
                <input type="number" min="1"
                       class='form-control rounded shadow-sm' style="min-width: 10ch; max-width: 12ch;"
                       name="serialNo"
                       value="<%=model.serialNo%>" onchange="changeSerialNumber(this)"/>
                <%}%>
            </td>
            <td style="position: sticky; left: 0; background-color: white">
                <%=model.name%><br>
                <%=model.organogramName%>
            </td>
            <%
                for (int dayIndex = 0; dayIndex < numberOfDays; ++dayIndex) {
                    OvertimeType overtimeType = OvertimeType.BLANK;
                    if (dayIndex < model.overtimeTypes.size()) {
                        overtimeType = OvertimeType.getByValue(model.overtimeTypes.get(dayIndex));
                    }
                    String otTypeOptions = OvertimeType.buildOption(overtimeType, language);
                    boolean emptyOptions = otTypeOptions.isEmpty();
            %>
            <td class="text-center">
                <%if (Boolean.FALSE.equals(overtimeBillTableData.isEditable)) {%>
                <%=overtimeType.getName()%>
                <%} else if (emptyOptions) {%>
                <%=OvertimeType.NONE.getName()%>
                <%} else {%>
                <select class='form-control rounded shadow-sm' style="min-width: 8ch;"
                        onchange="changeOvertimeTypeDaysCount(this, <%=dayIndex%>)">
                    <%=otTypeOptions%>
                </select>
                <%}%>
            </td>
            <%}%>
            <td class="ka-days-count text-center">
                <%=StringUtils.convertBanglaIfLanguageIsBangla(language, model.kaDays)%>
            </td>
            <td class="kha-days-count text-center">
                <%=StringUtils.convertBanglaIfLanguageIsBangla(language, model.khaDays)%>
            </td>
        </tr>
        <%}%>
        </tbody>
    </table>
</div>
<%}%>