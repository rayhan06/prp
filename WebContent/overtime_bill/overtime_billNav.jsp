<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Overtime_billServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-right" for="office_units_id_input">
                            <%=LM.getText(LC.HM_OFFICE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <button type="button"
                                    class="btn btn-block shadow btn-primary text-white btn-border-radius"
                                    id="office_units_id_modal_button"
                                    onclick="officeModalButtonClicked();">
                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                            </button>
                            <div class="input-group" id="office_units_id_div" style="display: none">
                                <input type="hidden" name='officeUnitId' id='office_units_id_input' value="">
                                <button type="button"
                                        class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                        id="office_units_id_text"
                                        onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn"
                                      style="width: 5%;padding-right: 5px;height: 40px" tag='pb_html'>
                                    <button type="button" class="btn btn-outline-danger"
                                            style="height: 40px;border-radius: 0"
                                            onclick="crsBtnClicked('office_units_id');"
                                            id='office_units_id_crs_btn' tag='pb_html'>
                                        &times;
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="otEmployeeTypeId">
                            <%=isLanguageEnglishNav ? "Employee Type" : "কর্মকর্তা/কর্মচারীর ধরণ"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' style="width: 100%"
                                    name='otEmployeeTypeId' id='otEmployeeTypeId'
                            >
                                <%=OT_employee_typeRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.OVERTIME_ALLOWANCE_ADD_MONTHYEAR, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="monthYear_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                <jsp:param name="HIDE_DAY" value="true"/>
                            </jsp:include>
                            <input type='hidden' id='monthYearData' value='null'>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=isLanguageEnglishNav ? "Finance Serial Number" : "অর্থ শাখার ক্রমিক নং"%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="financeSerialNumber" id="financeSerialNumber">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="overtimeBillTypeCat">
                            <%=isLanguageEnglishNav ? "Bill Type" : "বিলের ধরণ"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' style="width: 100%"
                                    name='overtimeBillTypeCat' id='overtimeBillTypeCat'
                            >
                                <%=CatRepository.getInstance().buildOptions(
                                        SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
                                        Language,
                                        null
                                )%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="isInPreview">
                            <%=isLanguageEnglishNav ? "Submission Status" : "জমা দেওয়ার অবস্থা"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' style="width: 100%"
                                    name='isInPreview' id='isInPreview'
                            >
                                <option value=""><%=isLanguageEnglishNav ? "All" : "সব"%>
                                </option>
                                <option value="1"><%=isLanguageEnglishNav ? "Draft" : "খসড়া"%>
                                </option>
                                <option selected value="0"><%=isLanguageEnglishNav ? "Submitted" : "জমা দেওয়া"%>
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>


<script type="text/javascript">

    $(document).ready(() => {
        select2SingleSelector('#budgetOfficeId', '<%=Language%>');
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 200);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "<%=url%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        let params = 'office_unit_id=' + $('#office_units_id_input').val();
        params += '&otEmployeeTypeId=' + $('#otEmployeeTypeId').val();
        params += '&monthYear=' + $('#monthYear').val();
        params += '&financeSerialNumber=' + convertToEnglishNumber($('#financeSerialNumber').val());
        const monthYearData = JSON.parse($('#monthYearData').val());
        params += getMonthYearFromParam(monthYearData);
        params += '&overtimeBillTypeCat=' + $('#overtimeBillTypeCat').val();
        params += '&isInPreview=' + $('#isInPreview').val();


        params += '&search=true&ajax=true';
        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    $('#monthYear_js').on('datepicker.change', (event, param) => {
        const isValidDate = dateValidator('monthYear_js', true);
        if (isValidDate) {
            $('#monthYearData').val(JSON.stringify(param.data));
        } else {
            $('#monthYearData').val('null');
        }
    });

    function stringIsBlank(string) {
        return string == null || string === '';
    }

    function getMonthYearFromParam(monthYearData) {
        if (monthYearData == null) {
            return '';
        }
        const invalidData = stringIsBlank(monthYearData.month)
                            || stringIsBlank(monthYearData.year);
        if (invalidData) return '';

        let month = +monthYearData.month;
        let year = +monthYearData.year;

        let paramString = '&monthYearFrom=' + new Date(year, month - 1, 1).getTime();

        let toTime = month === 11 // month is december
                     ? new Date(year + 1, 0, 1).getTime()
                     : new Date(year, month, 1).getTime();
        paramString += ('&monthYearTo=' + (toTime - 1));

        return paramString;
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#office_units_id_modal_button').hide();
        $('#office_units_id_div').show();
        $('#office_error_id').hide();

        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnitId';
        officeSearchSetSelectedOfficeLayers($('#office_units_id_input').val());
        $('#search_office_modal').modal();
    }
</script>