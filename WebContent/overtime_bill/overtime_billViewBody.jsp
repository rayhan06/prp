<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="util.*" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberInWord" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="overtime_allowance.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="finance.FinanceUtil" %>
<%@ page import="user.UserDTO" %>
<%@ page import="overtime_bill.*" %>
<%@ page import="bill_approval_history.Bill_approval_historyDTO" %>
<%@ page import="bill_approval_history.BillApprovalStatus" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="bill_approval_history.Bill_approval_historyDAO" %>
<%@page pageEncoding="UTF-8" %>

<%
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = "english".equalsIgnoreCase(Language);

    Overtime_billDTO overtimeBillDTO = (Overtime_billDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    List<Long> sortedBillDates = overtimeBillDTO.getSortedBillDates();
    int numberOfDays = sortedBillDates.size();
    LinkedHashMap<Long, Long> dateCountBySortedMonth =
            sortedBillDates.stream()
                           .sequential()
                           .collect(Collectors.groupingBy(
                                   DateUtils::get1stDayOfMonth,
                                   LinkedHashMap::new,
                                   Collectors.counting()
                           ));
    String billDateRangeText = DateUtils.getDateRangeString(
            overtimeBillDTO.billStartDate,
            overtimeBillDTO.billEndDate,
            false
    );
    String billTypeText = CatRepository.getInstance().getText(
            isLanguageEnglish,
            SessionConstants.OT_BILL_TYPE_CAT_DOMAIN_NAME,
            overtimeBillDTO.overtimeBillTypeCat
    );

    List<Overtime_allowanceDTO> allowanceDTOs = Overtime_allowanceDAO.getInstance().findByOvertimeBillId(overtimeBillDTO.iD);
    List<Overtime_allowanceModel> allowanceModels =
            allowanceDTOs.stream()
                         .map(allowanceDTO -> new Overtime_allowanceModel(allowanceDTO, "Bangla"))
                         .collect(Collectors.toList());
    Overtime_allowanceModel.fixOrdering(allowanceModels);

    String convertedBillTotal = StringUtils.convertToBanNumber(String.format("%d", overtimeBillDTO.billAmount));
    String formattedBillTotal = BangladeshiNumberFormatter.getFormattedNumber(convertedBillTotal);

    String kaDailyRateFormatted = BangladeshiNumberFormatter.getFormattedNumber(StringUtils.convertToBanNumber(
            String.format("%d", overtimeBillDTO.kaDailyRate)
    ));
    String khaDailyRateFormatted = BangladeshiNumberFormatter.getFormattedNumber(StringUtils.convertToBanNumber(
            String.format("%d", overtimeBillDTO.khaDailyRate)
    ));
    TreeMap<Integer, Bill_approval_historyDTO> approvalHistoryBySortedLevel =
            Bill_approval_historyDAO.getInstance().getApprovalHistoryBySortedLevel(overtimeBillDTO, userDTO);
    String financeSerialNumber = "";
    if (overtimeBillDTO.financeSerialNumber > 0) {
        financeSerialNumber = StringUtils.convertToBanNumber(String.format("%d", overtimeBillDTO.financeSerialNumber));
    }
    String employeeType = OT_employee_typeRepository.getInstance().getTextById(overtimeBillDTO.otEmployeeTypeId, false);
%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .30in .40in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape-width"] {
        width: 297mm;
        padding: .30in .40in;
        background: white;
        margin-bottom: 10px;
    }

    .page {
        background: white;
        padding: .05in;
        page-break-after: always;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 2px;
    }

    th {
        text-align: center;
    }

    .signature-image {
        width: 200px !important;
        height: 53px !important;
    }

    .signature-div {
        color: #a406dc;
        font-size: 10px !important;
    }

    .signature-div * {
        font-size: 10px !important;
    }

    @media print {
        @page {
            size: landscape;
            margin: .25in;
        }
    }

    div.bill-heading {
        font-weight: bold;
        font-size: 1.1rem;
    }

    td {
        white-space: nowrap;
    }

    div.finance-serial-number {
        font-weight: bold;
        font-size: 1.8rem;
    }

    .signature-container-div {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 1rem;
    }
</style>

<div class="loader-container-circle" id="full-page-loader">
    <div class="loader-circle"></div>
</div>

<div class="kt-content p-0" id="kt_content">
    <div class="">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title prp-page-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "অধিকাল ভাতা বিল", "Overtime Allowance Bill")%>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body page-bg" id="bill-div">

                <div class="ml-auto m-3">
                    <button type="button" class="btn" id='download-pdf'
                            onclick="printDivWithJqueryPrint('to-print-div');">
                        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                    </button>
                </div>

                <div style="margin: auto;">
                    <div class="container" id="to-print-div">
                        <%
                            int rowsPerPage = 7;
                            int index = 0;
                            boolean isLastPage = false;
                            while (index < allowanceModels.size()) {
                        %>
                        <section class="page">
                            <div class="text-right finance-serial-number">
                                <%=financeSerialNumber%>
                            </div>
                            <div class="text-center">
                                <div class="bill-heading">বাংলাদেশ জাতীয় সংসদ সচিবালয়</div>
                                <div class="bill-heading">
                                    <%=Office_unitsRepository.getInstance().geText("Bangla", overtimeBillDTO.officeUnitId)%>
                                </div>
                                <div class="bill-heading">
                                    <%=billDateRangeText%> তারিখের <%=employeeType%> কর্মকর্তা/কর্মচারীর <%=billTypeText%> অধিকাল ভাতা
                                </div>
                            </div>
                            <p class="mt-2">
                                <%=overtimeBillDTO.ordinanceText%> মোতাবেক
                                <%=kaDailyRateFormatted%>/- টাকা ও <%=khaDailyRateFormatted%>/- টাকা হারে
                                ছকে বর্ণিত তারিখ সমূহের জন্য অতিরিক্ত কাজের উপস্থিতি বিবরণ: ক=সংসদ অধিবেশনের জন্য
                                অতিরিক্ত কাজ, খ=সংসদীয় কমিটি বৈঠকের ও অন্যান্য অতিরিক্ত কাজ।
                            </p>

                            <div class="mt-3">
                                <table id="overtime-bill-table" class="table-bordered-custom w-100">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">
                                            <%=isLanguageEnglish ? "Serial<br>No." : "ক্রমিক<br>নং"%>
                                        </th>
                                        <th rowspan="2">
                                            <%=isLanguageEnglish ? "Name & Designation" : "নাম ও পদবী"%>
                                        </th>
                                        <%
                                            for (Map.Entry<Long, Long> monthDatesCount : dateCountBySortedMonth.entrySet()) {
                                                String monthYearName = DateUtils.getMonthYear(
                                                        monthDatesCount.getKey(),
                                                        "bangla"
                                                );
                                                long colSpan = monthDatesCount.getValue();
                                        %>
                                        <th colspan="<%=colSpan%>">
                                            <%=monthYearName%>
                                        </th>
                                        <%}%>
                                        <th rowspan="2">
                                            'ক' <%=isLanguageEnglish ? "<br>Days" : "<br>দিন<br>সংখ্যা"%>
                                        </th>
                                        <th rowspan="2">
                                            'খ' <%=isLanguageEnglish ? "<br>Days" : "<br>দিন<br>সংখ্যা"%>
                                        </th>
                                    </tr>
                                    <tr>
                                        <%for (Long billDate : sortedBillDates) {%>
                                        <th>
                                            <%=StringUtils.convertToBanNumber(String.format(
                                                    "%d",
                                                    DateUtils.getDayOfTheMonth(billDate)
                                            ))%>
                                        </th>
                                        <%}%>
                                    </tr>
                                    </thead>
                                    <tbody class="main-tbody">
                                    <%
                                        int rowsInThisPage = 0;
                                        while (index < allowanceModels.size() && rowsInThisPage < rowsPerPage) {
                                            isLastPage = (index == (allowanceModels.size() - 1));
                                            rowsInThisPage++;
                                            Overtime_allowanceModel model = allowanceModels.get(index++);
                                    %>
                                    <tr>
                                        <td class="text-center">
                                            <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.format("%d", model.serialNo))%>
                                            .
                                        </td>
                                        <td>
                                            <%=model.name%><br>
                                            <%=model.organogramName%>
                                        </td>
                                        <%
                                            for (int dayIndex = 0; dayIndex < numberOfDays; ++dayIndex) {
                                                String overtimeTypeName = OvertimeType.BLANK.getName();
                                                if (dayIndex < model.overtimeTypes.size()) {
                                                    overtimeTypeName = OvertimeType.getNameByValue(model.overtimeTypes.get(dayIndex));
                                                }
                                        %>
                                        <td class="text-center">
                                            <%=overtimeTypeName%>
                                        </td>
                                        <%}%>
                                        <td class="ka-days-count text-center">
                                            <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, model.kaDays)%>
                                        </td>
                                        <td class="kha-days-count text-center">
                                            <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, model.khaDays)%>
                                        </td>
                                    </tr>
                                    <%}%>
                                    </tbody>
                                </table>
                            </div>

                            <%if (isLastPage) {%>
                            <p class="mt-2">
                                প্রত্যয়ন করা যাচ্ছে যে, উপরোল্লিখিত কর্মকর্তা/কর্মচারীগণ জাতীয় সংসদের অধিবেশন/সংসদীয়
                                কমিটি বৈঠক ও অন্যান্য জরুরী কাজের জন্য
                                অফিস সময়ের পরে জনস্বার্থে কর্তৃপক্ষের অনুমতিক্রমে অতিরিক্ত সময় কর্মরত ছিলেন।
                            </p>

                            <%--Signatures--%>
                            <div class="row mt-1">
                                <div class="signature-container-div">
                                    <%--Prepared By--%>
                                    <div class="text-center">
                                        <%if (!overtimeBillDTO.isInPreviewStage) {%>
                                        <div class="bill-heading">প্রস্তুতকারক</div>
                                        <div class="signature-div">
                                            <div>
                                                <img class="signature-image"
                                                     src='<%=StringUtils.getBase64EncodedImageStr(overtimeBillDTO.preparedSignature)%>'
                                                     alt=""
                                                />
                                                <br>
                                                <%=StringUtils.getFormattedTime(false, overtimeBillDTO.preparedTime)%>
                                            </div>
                                            <div>
                                                <%=overtimeBillDTO.preparedNameBn%><br>
                                                <%=overtimeBillDTO.preparedOrgNameBn%><br>
                                                <%=overtimeBillDTO.preparedOfficeNameBn%><br>
                                            </div>
                                        </div>
                                        <%}%>
                                    </div>
                                    <%
                                        for (Map.Entry<Integer, Bill_approval_historyDTO> entry : approvalHistoryBySortedLevel.entrySet()) {
                                            Bill_approval_historyDTO approvalHistoryDTO = entry.getValue();
                                    %>
                                    <div class="text-center">
                                        <div class="bill-heading">&nbsp;</div>
                                        <%if (approvalHistoryDTO != null) {%>
                                        <div class="signature-div">
                                            <%if (approvalHistoryDTO.billApprovalStatus == BillApprovalStatus.APPROVED) {%>
                                            <div>
                                                <img class="signature-image"
                                                     src='<%=StringUtils.getBase64EncodedImageStr(approvalHistoryDTO.signature)%>'
                                                     alt=""/>
                                                <br>
                                                <%=StringUtils.getFormattedTime(false, approvalHistoryDTO.approvalTime)%>
                                            </div>
                                            <%}%>
                                            <div>
                                                <%=approvalHistoryDTO.nameBn%><br>
                                                <%=approvalHistoryDTO.orgNameBn%><br>
                                                <%=approvalHistoryDTO.officeNameBn%><br>
                                            </div>
                                        </div>
                                        <%}%>
                                    </div>
                                    <%}%>
                                </div>
                            </div>
                            <%}%>
                        </section>
                        <%
                            }
                        %>

                        <%
                            index = 0;
                            isLastPage = false;
                            long totalAmountRunningTotal = 0;
                            while (index < allowanceModels.size()) {
                                boolean isFirstPage = (index == 0);
                        %>
                        <section class="page">
                            <div class="text-right finance-serial-number">
                                <%=financeSerialNumber%>
                            </div>
                            <div class="text-center">
                                <div class="bill-heading">বাংলাদেশ জাতীয় সংসদ সচিবালয়</div>
                                <div class="bill-heading">
                                    <%=Office_unitsRepository.getInstance().geText("Bangla", overtimeBillDTO.officeUnitId)%>
                                </div>
                                <div class="bill-heading">
                                    <%=billDateRangeText%> তারিখের <%=employeeType%> কর্মকর্তা/কর্মচারীর <%=billTypeText%> অধিকাল ভাতা
                                </div>
                            </div>

                            <div class="mt-1">
                                <table class="table-bordered-custom w-100">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">ক্রমিক নং</th>
                                        <th rowspan="2">নাম ও পদবী</th>
                                        <th colspan="2">সংসদ অধিবেশনের কাজ</th>
                                        <th colspan="2">অন্যান্য জরুরি কাজ</th>
                                        <th rowspan="2">সর্বমোট টাকা</th>
                                        <th rowspan="2">প্রাপকের স্বাক্ষর</th>
                                    </tr>
                                    <tr>
                                        <th>ক-দিন সংখ্যা</th>
                                        <th><%=kaDailyRateFormatted%>
                                            টাকা হারে মোট টাকা
                                        </th>
                                        <th>খ-দিন সংখ্যা</th>
                                        <th><%=khaDailyRateFormatted%>
                                            টাকা হারে মোট টাকা
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <%if (!isFirstPage) {%>
                                    <tr>
                                        <td></td>
                                        <td colspan="5" class="text-right">
                                            পূর্ব পৃষ্ঠার জের=
                                        </td>
                                        <td class="text-right">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                    StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                            )%>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <%}%>

                                    <%
                                        int rowsInThisPage = 0;
                                        while (index < allowanceModels.size() && rowsInThisPage < rowsPerPage) {
                                            isLastPage = (index == (allowanceModels.size() - 1));
                                            rowsInThisPage++;
                                            Overtime_allowanceModel model = allowanceModels.get(index++);
                                            totalAmountRunningTotal += Long.parseLong(model.totalAmount);
                                    %>
                                    <tr>
                                        <td class="text-center">
                                            <%=StringUtils.convertToBanNumber(String.format("%d", model.serialNo))%>.
                                        </td>
                                        <td>
                                            <%=model.name%><br>
                                            <%=model.organogramName%>
                                        </td>
                                        <td class="text-center">
                                            <%=model.kaDays%>
                                        </td>
                                        <td class="text-right">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(model.kaAmount)%>
                                        </td>
                                        <td class="text-center">
                                            <%=model.khaDays%>
                                        </td>
                                        <td class="text-right">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(model.khaAmount)%>
                                        </td>
                                        <td class="text-right">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(model.totalAmount)%>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td></td>
                                        <td colspan="5" class="text-right">
                                            <%=isLastPage ? "সর্বমোট" : "উপমোট"%>=
                                        </td>
                                        <td class="text-right">
                                            <%=BangladeshiNumberFormatter.getFormattedNumber(
                                                    StringUtils.convertToBanNumber(String.valueOf(totalAmountRunningTotal))
                                            )%>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <%if (isLastPage) {%>
                            <div>
                                প্রত্যয়ন করা যাচ্ছে যে, উপরোল্লিখিত কর্মকর্তা/কর্মচারীগণ জাতীয় সংসদের অধিবেশন/সংসদীয়
                                কমিটি বৈঠক ও অন্যান্য জরুরী কাজের জন্য
                                অফিস সময়ের পরে জনস্বার্থে কর্তৃপক্ষের অনুমতিক্রমে অতিরিক্ত সময় কর্মরত ছিলেন।
                            </div>

                            <div class="text-center w-100">
                                বিল ঠিক আছে এবং পাস করা হল।<br>
                                টাকা=<%=formattedBillTotal%>/-<br>
                                কথায়ঃ <%=BangladeshiNumberInWord.convertToWord(convertedBillTotal)%> টাকা মাত্র
                            </div>

                            <%--Signature Div--%>
                            <div class="row col-12">
                                <div class="signature-container-div">
                                    <%--prepared by--%>
                                    <div class="text-center">
                                        <%if (!overtimeBillDTO.isInPreviewStage) {%>
                                        <div class="bill-heading">বিল প্রস্তুতকারক</div>
                                        <div class="signature-div">
                                            <div>
                                                <img class="signature-image"
                                                     src='<%=StringUtils.getBase64EncodedImageStr(overtimeBillDTO.preparedSignature)%>'
                                                     alt=""/>
                                                <br>
                                                <%=StringUtils.getFormattedTime(false, overtimeBillDTO.preparedTime)%>
                                            </div>
                                            <div>
                                                <%=overtimeBillDTO.preparedNameBn%><br>
                                                <%=overtimeBillDTO.preparedOrgNameBn%><br>
                                                <%=overtimeBillDTO.preparedOfficeNameBn%><br>
                                            </div>
                                        </div>
                                        <%}%>
                                    </div>
                                    <%
                                        for (Map.Entry<Integer, Bill_approval_historyDTO> entry : approvalHistoryBySortedLevel.entrySet()) {
                                            Integer level = entry.getKey();
                                            Bill_approval_historyDTO approvalHistoryDTO = entry.getValue();
                                    %>
                                    <div class="text-center">
                                        <div class="bill-heading">&nbsp;</div>
                                        <%if (approvalHistoryDTO != null) {%>
                                        <div class="signature-div">
                                            <%if (!overtimeBillDTO.isInPreviewStage) {%>
                                            <%if (approvalHistoryDTO.billApprovalStatus == BillApprovalStatus.APPROVED) {%>
                                            <div>
                                                <img class="signature-image"
                                                     src='<%=StringUtils.getBase64EncodedImageStr(approvalHistoryDTO.signature)%>'
                                                     alt=""/>
                                                <br>
                                                <%=StringUtils.getFormattedTime(false, approvalHistoryDTO.approvalTime)%>
                                            </div>
                                            <%} else if (approvalHistoryDTO.isThisUsersHistory(userDTO)) {%>
                                            <button id="layer1-approve-btn"
                                                    class="btn-sm shadow text-white border-0 submit-btn mt-3 btn-border-radius"
                                                    type="button"
                                                    onclick="approveApplication(<%=overtimeBillDTO.iD%>,<%=level%>)">
                                                <%=isLanguageEnglish ? "Approve" : "অনুমোদন করুন"%>
                                            </button>
                                            <%} else {%>
                                            <div class="btn btn-sm border-0 shadow mt-2"
                                                 style="background-color: #bb0a0a; color: white; border-radius: 8px;cursor: text">
                                                <%=isLanguageEnglish ? "Waiting for Approval" : "অনুমোদনের অপেক্ষায়"%>
                                            </div>
                                            <%}%>
                                            <%}%>
                                            <div style="margin-top: 5px;">
                                                <%=approvalHistoryDTO.nameBn%><br>
                                                <%=approvalHistoryDTO.orgNameBn%><br>
                                                <%=approvalHistoryDTO.officeNameBn%><br>
                                            </div>
                                        </div>
                                        <%}%>
                                    </div>
                                    <%}%>
                                </div>
                            </div>

                            <div class="row col-12">
                                <div class="col-4 text-center"></div>
                                <div class="col-4 text-center">
                                    <%=FinanceUtil.getFinance1HeadDesignation("Bangla")%><br>
                                    বাংলাদেশ জাতীয় সংসদ সচিবালয়
                                </div>
                                <div class="col-4 text-center"></div>
                            </div>
                            <%}%>
                        </section>
                        <%}%>
                    </div>

                    <%if (overtimeBillDTO.isInPreviewStage) {%>
                    <%--Action Button Div--%>
                    <div id="action-btn-div" class="row">
                        <div class="col-12 mt-3 text-right">
                            <button id="edit-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                    onclick="location.href = 'Overtime_billServlet?actionType=getEditPage&ID=<%=overtimeBillDTO.iD%>'">
                                <%=isLanguageEnglish ? "Edit" : "এডিট করুন"%>
                            </button>
                            <button id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                    type="button" onclick="submitOvertimeBill()">
                                <%=isLanguageEnglish ? "Submit" : "জমা দিন"%>
                            </button>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../utility/jquery_print.jsp"/>

<script>
    const layer1ApproveBtn = $('#layer1-approve-btn');
    const layer2ApproveBtn = $('#layer2-approve-btn');
    const fullPageLoader = $('#full-page-loader');

    function setButtonDisableState(value) {
        layer1ApproveBtn.prop('disabled', value);
        layer1ApproveBtn.prop('disabled', value);
        $('#submit-btn').prop('disabled', value);
        $('#edit-btn').prop('disabled', value);
    }

    function downloadTemplateAsPdf(divId, fileName, orientation = 'portrait') {
        let content = document.getElementById(divId);
        const opt = {
            margin: [.1, .2],
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4', orientation: orientation}
        };
        html2pdf().from(content).set(opt).save();
    }

    function approveApplication(id, level) {
        setButtonDisableState(true);
        let msg = '<%=isLanguageEnglish? "Are you sure to approve application?" : "আপনি কি আবেদন অনুমোদনের ব্যাপারে নিশ্চিত?"%>';
        let confirmButtonText = '<%=StringUtils.getYesNo(Language, true)%>';
        let cancelButtonText = '<%=StringUtils.getYesNo(Language, false)%>';
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText,
            () => {
                approveOTBill(id, level);
            }, () => {
                setButtonDisableState(false);
            }
        );
    }

    function approveOTBill(id, level) {
        const data = {
            id: id, level: level
        };

        $.ajax({
            type: "POST",
            url: "Overtime_billServlet?actionType=ajax_approve",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                if (response.success) {
                    location.reload();
                } else {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToast(response.message, response.message);
                }
            },
            error: function (/*jqXHR, textStatus, errorThrown*/) {
                $('#toast_message').css('background-color', '#ff6063');
                showToast("সার্ভারে সমস্যা", "Server Error");
                setButtonDisableState(false);
            }
        });
    }

    function submitOvertimeBill() {
        const data = {
            id: <%=overtimeBillDTO.iD%>
        };
        console.log({data});
        setButtonDisableState(true);
        fullPageLoader.show();
        $.ajax({
            type: "POST",
            url: "Overtime_billServlet?actionType=ajax_submit",
            data: data,
            dataType: 'JSON',
            success: function (response) {
                setButtonDisableState(false);
                fullPageLoader.hide();
                if (response.success) {
                    window.location.assign(getContextPath() + response.message);
                } else {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.message, response.message);
                }
            },
            error: function () {
                setButtonDisableState(false);
                fullPageLoader.hide();
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky("সাবমিট করতে ব্যর্থ হয়েছে", "Failed to submit");
            }
        });
    }
</script>