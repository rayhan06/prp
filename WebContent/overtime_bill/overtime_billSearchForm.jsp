<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="overtime_allowance.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="overtime_bill.Overtime_billDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="bangladehi_number_format_util.BangladeshiNumberFormatter" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="overtime_bill.Overtime_billStatus" %>
<%@ page import="ot_employee_type.OT_employee_typeRepository" %>
<%@ page import="bill_approval_history.BillApprovalStatus" %>
<%@ page import="overtime_bill.Overtime_billServlet" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navOVERTIME_ALLOWANCE";
    String servletName = "Overtime_billServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="text-center">
        <tr>
            <th>
                <%=isLanguageEnglish ? "Bill Date (Type)" : "বিলের তারিখে (ধরন)"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Serial No" : "ক্রমিক নং"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Type" : "ধরণ"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Office/Section" : "কার্যালয়/শাখা"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Manpower" : "জনবল"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Amount" : "পরিমাণ"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Preparer & Date" : "প্রস্তুতকারী ও সময়"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Approval Status" : "অনুমোদনের অবস্থা"%>
            </th>
            <th>
                <%=isLanguageEnglish? "Action" : "কার্যকলাপ"%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Overtime_billDTO> data = (List<Overtime_billDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Overtime_billDTO overtimeBillDTO :  data) {
                        BillApprovalStatus billApprovalStatus = BillApprovalStatus.getFromValue(overtimeBillDTO.approvalStatus);
        %>
                        <tr>
                            <td>
                                <%=overtimeBillDTO.getBillDateTypeText(isLanguageEnglish)%>
                            </td>
                            <td>
                                <%=overtimeBillDTO.financeSerialNumber < 0 ? "" : Utils.getDigits(overtimeBillDTO.financeSerialNumber, "BANGLA")%>
                            </td>
                            <td>
                                <%=OT_employee_typeRepository.getInstance().getTextById(overtimeBillDTO.otEmployeeTypeId, isLanguageEnglish)%>
                            </td>
                            <td>
                                <%=Office_unitsRepository.getInstance().geText(Language, overtimeBillDTO.officeUnitId)%>
                            </td>
                            <td>
                                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, String.format("%d", overtimeBillDTO.employeeCount))%>
                            </td>
                            <td>
                                <%=BangladeshiNumberFormatter.getFormattedNumber(
                                        StringUtils.convertBanglaIfLanguageIsBangla(
                                                Language,
                                                String.format("%d", overtimeBillDTO.billAmount)
                                        )
                                )%>/-
                            </td>
                            <td>
                                <%if(overtimeBillDTO.isInPreviewStage){%>
                                <div class="btn btn-sm border-0 shadow mt-2"
                                     style="background-color: <%=billApprovalStatus.getColor()%>; color: white; border-radius: 8px;cursor: text">
                                    <%=isLanguageEnglish ? "Draft" : "খসড়া"%>
                                </div>
                                <%} else {%>
                                <%=overtimeBillDTO.preparedNameBn%><br>
                                <%=StringUtils.getFormattedTime(false, overtimeBillDTO.preparedTime)%>
                                <%}%>
                            </td>
                            <td>
                                <%if(!overtimeBillDTO.isInPreviewStage){%>
                                <div class="btn btn-sm border-0 shadow mt-2"
                                     style="background-color: <%=billApprovalStatus.getColor()%>; color: white; border-radius: 8px;cursor: text">
                                    <%=billApprovalStatus.getName(isLanguageEnglish)%>
                                </div>
                                <%}%>
                            </td>
                            <td>
                                <button
                                        type="button"
                                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                                        style="color: #ff6b6b;"
                                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=overtimeBillDTO.iD%>'"
                                >
                                    <i class="fa fa-eye"></i>
                                </button>

                                <%if(overtimeBillDTO.isEditable(userDTO, Language)){%>
                                <button
                                        type="button"
                                        class="btn-sm border-0 shadow btn-border-radius text-white"
                                        style="background-color: #ff6b6b;"
                                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=overtimeBillDTO.iD%>'"
                                >
                                    <i class="fa fa-edit"></i>
                                </button>
                                <%}%>
                                <%if(overtimeBillDTO.isDeletableByUser(userDTO)){%>
                                <button type="button"
                                        data-delete-btn
                                        class="btn-sm border-0 shadow btn-border-radius text-white"
                                        style="background-color: #ff6b6b;"
                                        onclick="openDeleteWarning(<%=overtimeBillDTO.iD%>)"
                                >
                                    <i class="fa fa-trash"></i>
                                </button>
                                <%}%>
                            </td>
                        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<script>
    function openDeleteWarning(overtimeBillId) {
        const message = utilIsBangla ? "আপনি কি রেকর্ড মুছে ফেলার ব্যাপারে নিশ্চিত?"
                                     : "Are you sure you want to delete the record?";
        deleteDialog(
            message,
            () => deleteOvertimeBillDTO(overtimeBillId),
            () => {}
        );
    }

    async function deleteOvertimeBillDTO(id) {
        const url = '<%=servletName%>?actionType=ajax_deleteBill&ID=' + id;
        try {
            const res = await fetch(url, {method: 'POST'});
            const resJson = await res.json();
            console.log(resJson);
            if (resJson.success === true) {
                location.reload();
                return;
            }
            $('#toast_message').css('background-color', '#ff6063');
            showToast(resJson.message, resJson.message);
        } catch (error) {
            console.log(error);
            $('#toast_message').css('background-color', '#ff6063');
            showToast('<%=isLanguageEnglish? "Failed to delete" : "ডিলিট করতে ব্যর্থ হয়েছে"%>');
        }
    }
</script>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>