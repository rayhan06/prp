
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_parliament_building_room.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>
<%@ page import="am_parliament_building_level.Am_parliament_building_levelRepository" %>
<%@ page import="am_parliament_building_block.Am_parliament_building_blockRepository" %>


<%
	String navigator2 = "navAM_PARLIAMENT_BUILDING_ROOM";
	String servletName = "Am_parliament_building_roomServlet";
	LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
	String value = "";
	String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
	UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;

	RecordNavigator rn2 = (RecordNavigator) request.getAttribute("recordNavigator");
	System.out.println("rn2 = " + rn2);
	String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
	String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
	String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
	String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
	String tableName = rn2.m_tableName;

	String ajax = request.getParameter("ajax");
	boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>

			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_BLOCKTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_ROOMNO, loginDTO)%></th>
<%--								<th><%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_REMARKS, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_MODIFIEDBY, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_SEARCH_AM_PARLIAMENT_BUILDING_ROOM_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								List<Am_parliament_building_roomDTO> data =
										(List<Am_parliament_building_roomDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Am_parliament_building_roomDTO am_parliament_building_roomDTO = data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = Am_parliament_building_levelRepository.getInstance().getText(am_parliament_building_roomDTO.levelType);
											%>
				
											<%=value%>
				
			
											</td>
		
											<td>
											<%
											value = Am_parliament_building_blockRepository.getInstance().getText
													(am_parliament_building_roomDTO.blockType, Language);
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = am_parliament_building_roomDTO.roomNo + "";
											%>
				
											<%=value%>
				
			
											</td>
		
<%--											<td>--%>
<%--											<%--%>
<%--											value = am_parliament_building_roomDTO.remarks + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--		--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = am_parliament_building_roomDTO.modifiedBy + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
		
		
		
		
		
	
											<%CommonDTO commonDTO = am_parliament_building_roomDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=am_parliament_building_roomDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=true%>" />


			