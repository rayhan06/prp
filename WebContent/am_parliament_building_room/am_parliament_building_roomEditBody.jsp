<%@page import="login.LoginDTO"%>
<%@page import="am_parliament_building_room.*"%>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="util.*"%>
<%@ page import="am_parliament_building_level.Am_parliament_building_levelRepository" %>
<%@ page import="common.BaseServlet" %>

<%



    String servletName = "Am_parliament_building_roomServlet";

    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    String actionName;
    Am_parliament_building_roomDTO am_parliament_building_roomDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        am_parliament_building_roomDTO = (Am_parliament_building_roomDTO)
                request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        am_parliament_building_roomDTO = new Am_parliament_building_roomDTO();
    }

    String formTitle = LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_AM_PARLIAMENT_BUILDING_ROOM_ADD_FORMNAME, loginDTO);
    int i = 0;



%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform" >
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=am_parliament_building_roomDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%>
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-8">
																<select class='form-control'  onchange="loadBlockList()" name='levelType' id = 'levelType_select_<%=i%>'   tag='pb_html'>
																<%
                                                                    String Options = Am_parliament_building_levelRepository.getInstance().getOptions(Language, am_parliament_building_roomDTO.levelType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_BLOCKTYPE, loginDTO)%>
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='blockType' id = 'blockType_select_<%=i%>'   tag='pb_html'>
																</select>
		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right">
                                                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_ROOMNO, loginDTO)%>
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='roomNo' id = 'roomNo_text_<%=i%>' value='<%=am_parliament_building_roomDTO.roomNo%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_REMARKS, loginDTO)%></label>
                                                            <div class="col-md-8">
                                                                <textarea class='form-control'  name='remarks' id = 'remarks_text_<%=i%>' tag='pb_html'><%=am_parliament_building_roomDTO.remarks%></textarea>
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=am_parliament_building_roomDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=am_parliament_building_roomDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=am_parliament_building_roomDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=am_parliament_building_roomDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=am_parliament_building_roomDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=am_parliament_building_roomDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_AM_PARLIAMENT_BUILDING_ROOM_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="submit" onclick="event.preventDefault();submitForm()">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_AM_PARLIAMENT_BUILDING_ROOM_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

const roomForm = $("#bigform");

function buttonStateChange(value){
    $('#submit-btn').prop('disabled',value);
    $('#cancel-btn').prop('disabled',value);
}

function init(){
    if('<%=actionName%>' === 'edit'){
        loadBlockList();
    }
    // loadRoomList()
}

function submitForm(){
    buttonStateChange(true);
    if(PreprocessBeforeSubmiting()){
        $.ajax({
            type : "POST",
            url : "Am_parliament_building_roomServlet?actionType=ajax_<%=actionName%>",
            data : roomForm.serialize(),
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    buttonStateChange(false);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }else{
        buttonStateChange(false);
    }
}

function PreprocessBeforeSubmiting()
{
    roomForm.validate();
    return roomForm.valid();
}


function loadBlockList() {

    let levelId = document.getElementById('levelType_select_0').value;

    if(levelId === ''){
        $("#blockType_select_0").html("");
        return ;
    }


    let blockId = '<%=am_parliament_building_roomDTO.blockType%>';
    let url = "Am_parliament_building_blockServlet?actionType=getBlockByLevelId&levelId=" +
        levelId + "&defaultValue=" + blockId;
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: function (fetchedData) {
            const response = JSON.parse(fetchedData);
            if ( response && response.responseCode === 0) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(response.msg, response.msg);
            }
            else if (response && response.responseCode === 200) {
                $("#blockType_select_0").html(response.msg);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                + ", Message: " + errorThrown);
        }
    });

}

function loadRoomList() {

    let url = "Am_parliament_building_roomServlet?actionType=getRoomByBlockId&blockId=201&defaultValue=-1";
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: function (fetchedData) {
            const response = JSON.parse(fetchedData);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                + ", Message: " + errorThrown);
        }
    });

}



$(document).ready(function(){

	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	});

    $.validator.addMethod('validSelector', function (value, element) {
        return value && value !== -1 && value.toString().trim().length > 0;
    });

    $.validator.addMethod('nonEmpty', function (value, element) {
        return value.toString().trim().length > 0;
    });

    let lang = '<%=Language%>';
    let levelErr;
    let blockErr;
    let roomErr;
    if (lang.toUpperCase() === 'ENGLISH') {
        levelErr = 'Level Required';
        blockErr = 'Block Required';
        roomErr = 'Room number Required';
    } else {
        levelErr = 'লেভেল প্রয়োজনীয়';
        blockErr = 'ব্লক প্রয়োজনীয়';
        roomErr = 'রুম নম্বর প্রয়োজনীয় ';
    }

    roomForm.validate({
        errorClass: 'error is-invalid',
        validClass: 'is-valid',
        rules: {
            levelType: {
                validSelector: true,
            },
            blockType: {
                validSelector: true,
            },
            roomNo: {
                nonEmpty: true,
            }
        },
        messages: {
            levelType: levelErr,
            blockType: blockErr,
            roomNo: roomErr,

        }
    });

    init();
});	





</script>






