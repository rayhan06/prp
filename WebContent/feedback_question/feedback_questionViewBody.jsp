<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="feedback_question.*" %>


<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String value;
    String Language = LM.getText(LC.FEEDBACK_QUESTION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Feedback_questionDTO feedback_questionDTO = Feedback_questionRepository.getInstance().getById(id);

%>

<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title"><%=LM.getText(LC.FEEDBACK_QUESTION_ADD_FEEDBACK_QUESTION_ADD_FORMNAME, loginDTO)%>
                    </h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <h3><%=LM.getText(LC.FEEDBACK_QUESTION_ADD_FEEDBACK_QUESTION_ADD_FORMNAME, loginDTO)%>
                </h3>
                <table class="table table-bordered table-striped">


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.FEEDBACK_QUESTION_ADD_QUESTIONEN, loginDTO)%>
                        </b></td>
                        <td>
                            <%=feedback_questionDTO.questionEng%>
                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.FEEDBACK_QUESTION_ADD_QUESTIONBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = feedback_questionDTO.questionBng + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>


        </div>


    </div>
</div>