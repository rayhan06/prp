<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    if (rn == null) {
        rn = (RecordNavigator) request.getAttribute("recordNavigator");
    }

    System.out.println("rn " + rn);

    String context = "../../.." + request.getContextPath() + "/";

    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>


<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body" >
        <!-- BEGIN FORM-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" style="padding-right: 0px;">
                            <%=LM.getText(LC.FEEDBACK_QUESTION_ADD_QUESTIONEN, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="QUESTION_EN" name="QUESTION_EN"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"
                               style="padding-right: 0px;"><%=LM.getText(LC.FEEDBACK_QUESTION_ADD_QUESTIONBN, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="QUESTION_BN" name="QUESTION_BN"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    // const anyFieldSelector = $('#anyfield');
    const questionEnSelector = $('#QUESTION_EN');
    const questionBnSelector = $('#QUESTION_BN');

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=url%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        // var params = 'AnyField=' + anyFieldSelector.val();
        var params = '';
        if (questionEnSelector.val()) {
            params += 'QUESTION_EN=' + questionEnSelector.val();
        }
        if (questionBnSelector.val()) {
            params += '&QUESTION_BN=' + questionBnSelector.val();
        }

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged === 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

