<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="feedback_question.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.*" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String Language = LM.getText(LC.FEEDBACK_QUESTION_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    String navigator2 = SessionConstants.NAV_FEEDBACK_QUESTION;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap" style="width: 100%">
        <thead class="text-nowrap">
        <tr>
            <th><%=LM.getText(LC.FEEDBACK_QUESTION_ADD_QUESTIONEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.FEEDBACK_QUESTION_ADD_QUESTIONBN, loginDTO)%>
            </th>
            <th>
                <div class="text-center">
                    <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Feedback_questionDTO> data = (List<Feedback_questionDTO>) recordNavigator.list;
            if (data != null && data.size() > 0) {
                for (Feedback_questionDTO feedback_questionDTO : data) {
        %>
        <tr>
            <%@include file="feedback_questionSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			