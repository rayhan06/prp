<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.FAMILY_EDIT_LANGUAGE, loginDTO);
    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.FEEDBACK_QUESTION_ADD_FEEDBACK_QUESTION_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="bigForm" name="bigForm" enctype="multipart/form-data"
              action="Feedback_questionServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right" for='question_eng'>
                                                <%=LM.getText(LC.FEEDBACK_QUESTION_ADD_QUESTIONEN, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <textarea
                                                        type='text' class='form-control'
                                                        name='question_eng' id='question_eng'
                                                        style="resize: none" rows="3" maxlength="1024"
                                                ></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right" for='question_bng'>
                                                <%=LM.getText(LC.FEEDBACK_QUESTION_ADD_QUESTIONBN, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <textarea
                                                        type='text' class='form-control'
                                                        name='question_bng'
                                                        style="resize: none" rows="3" maxlength="2048"
                                                        id='question_bng'
                                                ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-actions text-right mb-3">
                            <button id="cancel-btn" type="button" class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.FEEDBACK_QUESTION_ADD_FEEDBACK_QUESTION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button"
                                    onclick="submitForm()">
                                <%=LM.getText(LC.FEEDBACK_QUESTION_ADD_FEEDBACK_QUESTION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const form = $('#bigForm');

    $(document).ready(function () {
        $.validator.addMethod('textAreaValidation', function (value) {
            return value.trim() !== '';
        });

        form.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                question_eng: {
                    required: true,
                    textAreaValidation: true
                },
                question_bng: {
                    required: true,
                    textAreaValidation: true
                }
            },
            messages: {
                question_eng: isLangEng ? "Please write English description" : "অনুগ্রহ করে ইংরেজীতে বিবরণ লিখুন",
                question_bng: isLangEng ? "Please write Bangla description" : "অনুগ্রহ করে বাংলায় বিবরণ লিখুন"
            }
        });
    });

    function submitForm() {
        if (form.valid()) {
            submitAjaxForm();
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>






