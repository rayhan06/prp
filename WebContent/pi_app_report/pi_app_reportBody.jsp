<%@ page import="login.LoginDTO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
    String formTitle = UtilCharacter.getDataByLanguage(Language, "বার্ষিক ক্রয় পরিকল্পনা অনুযায়ী আইটেম সংখ্যা ",
            "ANNUAL PURCHASE PLAN SPECIFIC ITEM COUNT");
    int i = 0;



%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action=""
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-3 col-form-label text-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "অর্থবছর", "Economic Year")%>
                                            </label>
                                            <div class="col-9">
                                                <select class='form-control' name='fiscalYearId'
                                                        id='fiscalYearId' tag='pb_html' required>
                                                    <%

                                                        StringBuilder option = new StringBuilder();
                                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {
                                                            option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                        }
                                                    %>
                                                    <%=option.toString()%>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-3 col-form-label text-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "আইটেম", "Item")%>
                                            </label>
                                            <div class="col-9">
                                                <button id="item_id_modal_button" type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius mb-3 ">
                                                    <%=UtilCharacter.getDataByLanguage(Language, "বাছাই করুন", "Select")%>
                                                </button>

                                                <div class="input-group" id="item_id_div" style="display: none">
                                                    <input type="hidden" name='itemId'
                                                           id='item_id_input' value="">
                                                    <button type="button" class="btn btn-secondary form-control shadow btn-border-radius"
                                                            disabled id="item_id_text"></button>
                                                    <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                                        <button type="button" class="btn btn-outline-danger"
                                                                onclick="crsBtnClicked('item_id');"
                                                                id='item_id_crs_btn' tag='pb_html'>
                                                            x
                                                        </button>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10 form-actions text-right mt-3">
                        <button onclick="loadData()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="submit">
                            <%=UtilCharacter.getDataByLanguage(Language, "লোড", "Load")%>
                        </button>
                    </div>
                </div>
                <div id="data-div" class="  mb-4" style="display: none">
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../procurement_goods/procurementGoodsModal.jsp">
    <jsp:param name="index" value="1"/>
</jsp:include>

<script type="text/javascript">
    let lang = '<%=Language%>';
    let dataDiv = $("#data-div")

    $('#item_id_modal_button').on('click', function () {
        loadModal();
    });
    function loadModal() {
        $('#search_proc_modal').modal();
    }

    necessaryCollectionForProcurementModal = {
        callBackFunction: function (item) {
            getItem(item);
        }
    };

    function getItem(item) {
        if (!item || item.iD === '') {
            return;
        }

        $('#item_id_modal_button').hide();
        $('#item_id_div').show();
        document.getElementById('item_id_text').innerHTML = item.circularData;
        $('#item_id_input').val(item.iD);
    }

    function dataFill(){
        const fiscalYearId = document.getElementById("fiscalYearId").value;
        const itemId = document.getElementById("item_id_input").value;
        let url = "Pi_app_requestServlet?actionType=getItemSpecificFiscalYearData&fiscalYearId="
            + fiscalYearId + "&itemId=" + itemId;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                dataDiv.html(fetchedData);
                dataDiv.show();
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    async function loadData(){
        event.preventDefault();
        dataDiv.hide();
        const fiscalYearId = document.getElementById("fiscalYearId").value;
        const itemId = document.getElementById("item_id_input").value;
        const url = 'Pi_app_requestServlet?actionType=isAvailableAppReport'
            + '&fiscalYearId=' + fiscalYearId
            + '&itemId=' + itemId;
        const response = await fetch(url);
        let result = await response.json();
        if(result){
            if(!result.appAvailable){
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(result.appMsg, result.appMsg);

            } else {
                dataFill();
            }
        }
    }


    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

</script>
