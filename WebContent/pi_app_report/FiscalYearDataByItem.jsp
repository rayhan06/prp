<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_job_description.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="vm_maintenance.BillRegisterItemDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="pi_app_request.DataByFiscalYearDTO" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDTO" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDAO" %>
<%@ page import="pi_app_request.Pi_app_requestDAO" %>
<%@ page import="pi_purchase.Pi_purchaseDAO" %>


<%

    List<Pi_app_request_detailsDTO>  data = (List<Pi_app_request_detailsDTO>) request.getAttribute("data");
    long fiscalYearId = (long) request.getAttribute("fiscalYearId");
    long officeUnitId = (long) request.getAttribute("officeUnitId");
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String value = "";
    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
    Pi_purchaseDAO purchaseDAO = Pi_purchaseDAO.getInstance();



%>

<div class="table-responsive mt-5">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="thead-light">
        <tr>
            <th>
                <%=UtilCharacter.getDataByLanguage(Language, "আইটেম", "Item")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "অনুমোদিত পরিমান", "Approved Amount")%></th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ক্রয়কৃত পরিমান", "Purchased Amount")%></th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "অবশিষ্ট পরিমান ", "Remaining Amount")%></th>

        </tr>
        </thead>
        <tbody>
        <%


            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Pi_app_request_detailsDTO dto = data.get(i);


        %>
        <tr id='tr_<%=i%>'>


            <td>
                <%
                    value = procurement_goodsDAO.getCircularData(dto.itemId, Language);
                %>
                <%=value%>
            </td>

            <td>
                <%
                    value = Utils.getDigits((int)dto.approveThreeQuantity, Language);
                %>
                <%=value%>
            </td>

            <td>
                <%
                    double purchasedAmount = purchaseDAO.getPurchaseCountByFiscalAndUnitAndItem(fiscalYearId, officeUnitId, dto.itemId);
                    value = Utils.getDigits((int)purchasedAmount, Language);

                %>

                <%=value%>

            </td>

            <td>
                <%
                    double remainingAmount = dto.approveThreeQuantity - purchasedAmount;
                    value = Utils.getDigits((int)remainingAmount, Language);

                %>

                <%=value%>
            </td>


        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>


			