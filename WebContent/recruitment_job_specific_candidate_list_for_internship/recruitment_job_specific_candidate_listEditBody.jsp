﻿<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeDao" %>
<%@ page import="pb.CatDAO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="university_info.University_infoDAO" %>
<%@ page import="config.GlobalConfigurationRepository" %>
<%@ page import="config.GlobalConfigConstants" %>
<%@ page import="education_level.Education_levelRepository" %>


<%


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);


    Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();
    Job_applicant_applicationDAO jobApplicantApplicationDAO = new Job_applicant_applicationDAO();


    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date todayDate = new Date();

    int pagination_number = 0;
    RecordNavigator rn = new RecordNavigator();
    request.getSession().setAttribute(SessionConstants.NAV_JOB_APPLICANT_APPLICATION, rn);
    String context = "../../.." + request.getContextPath() + "/";


//    System.out.println("#####");
//    System.out.println(new JobSpecificExamTypeDao().getNextLevelFromJobIdAndCurrentLevel(2000, 1));
//    System.out.println(new JobSpecificExamTypeDao().getPreviousLevelFromJobIdAndCurrentLevel(2000, 1));
//    System.out.println("#####");

    String formTitle = UtilCharacter.getDataByLanguage(Language, "ইন্টার্নশিপের প্রার্থী তালিকা",
            "Internship Candidate List");
    long internShipId = Long.parseLong(GlobalConfigurationRepository.
            getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form id="candidate-form" action="RecruitmentJobSpecificCandidateListServlet?actionType=DownloadTemplate"
              name="candidate-form" method="POST">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <div class="row">

                                            <input type="hidden" name="job" value="<%=internShipId%>">
                                            <input type="hidden" name="level" value="0">

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=LM.getText(LC.CANDIDATE_LIST_VIEW_STATUS, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <select onchange="" class='form-control' name='view' id='view'
                                                                tag='pb_html'>

                                                            <%=jobApplicantApplicationDAO.getViewList(Language)%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=LM.getText(LC.CANDIDATE_LIST_APPROVE_STATUS, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <select onchange="" class='form-control' name='approve'
                                                                id='approve' tag='pb_html'>

                                                            <%=jobApplicantApplicationDAO.getApproveList(Language)%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=LM.getText(LC.PARLIAMENT_JOB_APPLICANT_ADD_SPECIALQUOTACAT, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <select class='form-control' name='quota' id='quota'
                                                                tag='pb_html'>
                                                            <%=jobApplicantApplicationDAO.getQuotaList(Language)%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <input class='form-control' type="hidden" id='ageFrom'
                                                   name='ageFrom' value="10">
                                            <input class='form-control' type="hidden" id='ageTo'
                                                   name='ageTo' value="10">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=UtilCharacter.getDataByLanguage(Language, "অভিজ্ঞতা (থেকে )", "Experience (From)")%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input class='form-control' type="number" min="0"
                                                               id='qualificationFrom' name='qualificationFrom'>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=UtilCharacter.getDataByLanguage(Language, "অভিজ্ঞতা (পর্যন্ত )", "Experience (To)")%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input class='form-control' type="number" min="0"
                                                               id='qualificationTo' name='qualificationTo'>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=UtilCharacter.getDataByLanguage(Language, "শিক্ষাগত যোগ্যতা", "Education Level")%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <select class='form-control' id='eduLevel' name='eduLevel'
                                                                tag='pb_html'>
                                                            <%
                                                                String sOptions = Education_levelRepository.getInstance().buildOptions
                                                                        (Language, 0L);
                                                            %>
                                                            <%=sOptions%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label text-md-right">
                                                        <%=UtilCharacter.getDataByLanguage(Language, "বিশ্ববিদ্যালয় ", "University")%>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <select class='form-control' id='university' name='university'
                                                                tag='pb_html'>
                                                            <%
                                                                sOptions = new University_infoDAO().getUniversityList(Language, -5);
                                                            %>

                                                            <%=sOptions%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-actions text-right mt-3" id="submit-button-div" style="">
                        <a style=" color: white;background-color: #00a1d4;cursor: pointer"
                           class="btn btn-sm text-white shadow btn-border-radius" onclick="onSubmit('')">
                            <i class="fas fa-spinner"></i>
                            <%=LM.getText(LC.CANDIDATE_LIST_LOAD, loginDTO)%>
                            <%--                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_SUBMIT_BUTTON, loginDTO)%>--%>

                        </a>
                        <button type="submit" class="btn btn-sm btn-success text-white shadow btn-border-radius"
                                id="download_excel" name="download_excel"><i
                                class="fas fa-file-excel"></i><%=LM.getText(LC.CANDIDATE_LIST_DOWNLOAD_EXCEL, loginDTO) %>
                        </button>
                        <input type="hidden" id="job_name" name="job_name" value=""/>
                        <input type="hidden" id="level_name" name="level_name" value=""/>
                    </div>

                </div>
                <div class="mt-5" id='total-item-count-div' style="display: none">
                    <label class="mr-4" for=""><%=LM.getText(LC.CANDIDATE_LIST_TOTAL, loginDTO)%>: <label
                            id="total-count"></label>,</label>
                    <label class="mr-4" for=""><%=LM.getText(LC.CANDIDATE_LIST_VIEWED, loginDTO)%>: <label
                            id="view-count"></label>,</label>
                    <label class="mr-4" for=""><%=LM.getText(LC.CANDIDATE_LIST_SHORTLISTED, loginDTO)%>: <label
                            id="shortlisted-count"></label>,</label>
                    <label for=""><%=LM.getText(LC.CANDIDATE_LIST_REJECTED, loginDTO)%>: <label
                            id="rejected-count"></label></label>
                </div>
                <div id='data-div-parent' style="display: none">
                    <div id='data-div' class="text-nowrap">
                    </div>
                    <% pagination_number = 1;%>
                    <%@include file="../common/pagination_with_go2.jsp" %>
                </div>
            </div>
        </form>
    </div>
</div>


<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">

    let language = '<%=Language%>';

    function convertToBn(value) {
        if (language == 'English') {
            return value;
        }

        return toBn(value);
    }

    function showItemCountDiv(total, view, select, reject) {
        document.getElementById('total-count').innerText = convertToBn(total);
        document.getElementById('view-count').innerText = convertToBn(view);
        document.getElementById('shortlisted-count').innerText = convertToBn(select);
        document.getElementById('rejected-count').innerText = convertToBn(reject);
        document.getElementById('total-item-count-div').style.display = 'block';
    }

    function hideItemCountDiv() {
        document.getElementById('total-item-count-div').style.display = 'none';
    }

    function statusChange() {
        hideLoadButton();
        let statusValue = document.getElementById('status').value;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('job').innerHTML = this.responseText;
            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Recruitment_job_descriptionServlet?actionType=getJobListByStatusWithoutInternship";
        params = params + "&language=" + language + "&status=" + statusValue;
        xhttp.open("Get", params, true);
        xhttp.send();
    }

    function showLoadButton() {
        document.getElementById('submit-button-div').style.display = 'block';
        // document.getElementById('submit-download-div').style.display = 'block';
        var job_name = $("#job>option:selected").text();
        var level_name = $("#level>option:selected").text();
        document.getElementById("job_name").value = job_name;
        document.getElementById("level_name").value = level_name;
    }

    function hideLoadButton() {
        document.getElementById('submit-button-div').style.display = 'none';
        // document.getElementById('submit-download-div').style.display = 'none';
    }

    function showDataDiv() {
        document.getElementById('data-div-parent').style.display = 'block';
    }

    function hideDataDiv() {
        document.getElementById('data-div-parent').style.display = 'none';
    }

    function onJobChange() {
        let jobValue = document.getElementById('job').value;
        if (jobValue && jobValue.length > 0) {
            showLoadButton();
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('level').innerHTML = this.responseText;
                } else if (this.readyState == 4 && this.status != 200) {
                }
            };

            let params = "Recruitment_job_descriptionServlet?actionType=getExamTypesByJobId";
            params = params + "&language=" + language + "&jobId=" + jobValue;
            xhttp.open("Get", params, true);
            xhttp.send();

        } else {
            hideLoadButton();
            document.getElementById('level').innerHTML = '';
        }
    }

    function getAllDataCount() {
        let jobValue = '<%=internShipId%>';
        let levelValue = '0';

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let responseCount = JSON.parse(this.responseText);
                showItemCountDiv
                (responseCount.totalCount, responseCount.viewCount, responseCount.selectedCount, responseCount.rejectedCount);

            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "&jobId=" + jobValue + "&level=" + levelValue;
        let prefixParam = "RecruitmentJobSpecificCandidateListServlet?actionType=getAllDataCount" + params;

        xhttp.open("Get", prefixParam, true);
        xhttp.send();

    }

    function onDownload() {
        toastr.success("Download Started!");
        let jobValue = '<%=internShipId%>';
        // let statusValue = document.getElementById('status').value;
        let levelValue = '0';
        let viewValue = document.getElementById('view').value;
        let approveValue = document.getElementById('approve').value;
        // console.log({jobValue, statusValue, levelValue, viewValue, approveValue})

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                toastr.success("Download Completed!");

            } else if (this.readyState == 4 && this.status != 200) {
                toastr.error("Download Failed!");
            }
        };

        let params = "&jobId=" + jobValue
            + "&view=" + viewValue
            + "&approve=" + approveValue
            + "&level=" + levelValue;
        let prefixParam = "RecruitmentJobSpecificCandidateListServlet?actionType=DownloadTemplate" + params;
        //console.log(prefixParam)

        xhttp.open("POST", prefixParam, true);
        xhttp.send();
    }

    function onSubmit(params) {
        hideItemCountDiv();
        let jobValue = '<%=internShipId%>';
        let statusValue = 'CREATED';
        let levelValue = '0';
        let viewValue = document.getElementById('view').value;
        let approveValue = document.getElementById('approve').value;
        let quotaValue = document.getElementById('quota').value;
        // console.log({jobValue, statusValue, levelValue, viewValue, approveValue})
        // console.log(quotaValue);

        let ageFrom = document.getElementById('ageFrom').value;
        let ageTo = document.getElementById('ageTo').value;

        let exFrom = document.getElementById('qualificationFrom').value;
        let exTo = document.getElementById('qualificationTo').value;

        let eduLevel = document.getElementById('eduLevel').value;
        let university = document.getElementById('university').value;

        console.log({ageFrom, ageTo, exFrom, exTo, eduLevel, university})


        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('data-div').innerHTML = this.responseText;
                setPageNo();
                showDataDiv();
                getAllDataCount();

            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        params = params + "&jobId=" + jobValue
            + "&view=" + viewValue
            + "&approve=" + approveValue
            + "&quota=" + quotaValue
            + "&level=" + levelValue
            + "&ageFrom=" + ageFrom
            + "&ageTo=" + ageTo
            + "&exFrom=" + exFrom
            + "&exTo=" + exTo
            + "&eduLevel=" + eduLevel
            + "&university=" + university;
        let prefixParam = "RecruitmentJobSpecificCandidateListServlet?actionType=getData" + params;


        xhttp.open("Get", prefixParam, true);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number) {
        // console.log('came here')


        let params = '&search=true&ajax=true';

        // var extraParams = document.getElementsByName('extraParam');
        // extraParams.forEach((param) => {
        //     params += "&" + param.getAttribute("tag") + "=" + param.value;
        // })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            // console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[0].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        // console.log(params)
        onSubmit(params);

    }

    function validationBeforeShortList(selectedId) {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText.trim() == 'true') {
                    getPopUpForShortList(selectedId);
                } else {
                    toastr.error("Can't ShortList, Post Count Exceeds");
                }

            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=validationForShortList&jobAppAppId=" + selectedId;
        xhttp.open("Get", params, true);
        xhttp.send();
    }

    function shortList(selectedId) {
        event.preventDefault();
        validationBeforeShortList(selectedId);

    }

    function getPopUpForShortList(selectedId) {
        let remarksLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REMARKS, loginDTO)%>' + ":";
        let saveLabel = '<%=LM.getText(LC.CANDIDATE_LIST_YES, loginDTO)%>';
        let cancelLabel = '<%=LM.getText(LC.CANDIDATE_LIST_NO, loginDTO)%>';
        let headingLabel = '<%=LM.getText(LC.CANDIDATE_LIST_SHOTLIST_REMARKS, loginDTO)%>';

        bootbox.dialog({
            message: "<div class='row'>" +
                "<div class='col-lg-2'>" + remarksLabel + "</div>" +
                "<textarea class='col-lg-8' style='border: 1px solid' id = 'shortlist-remarks'></textarea>" +
                "</div>",
            title: headingLabel,
            closeButton: false,
            centerVertical: true,
            buttons: [
                {
                    label: cancelLabel,
                    className: "btn btn-sm btn-danger text-white shadow",
                    callback: function () {
                        // console.log( $('#shortlist-remarks').val());
                        // console.log('nothing happened')
                    }
                },
                {
                    label: saveLabel,
                    className: "btn btn-sm btn-success text-white shadow",
                    callback: function () {
                        let remarks = $('#shortlist-remarks').val();
                        makeShortlisted(selectedId, remarks);
                    }
                }

            ]
        });
    }

    function makeShortlisted(id, remarks) {
        let formData = new FormData();
        formData.append('id', id);
        formData.append('remarks', remarks);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");
                toastr.success("Success");
                onSubmit('');
            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeShortlist";
        xhttp.open("POST", params, true);
        xhttp.send(formData);
    }

    function reject(selectedId) {
        event.preventDefault();
        let remarksLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REMARKS, loginDTO)%>' + ":";
        let saveLabel = '<%=LM.getText(LC.CANDIDATE_LIST_YES, loginDTO)%>';
        let cancelLabel = '<%=LM.getText(LC.CANDIDATE_LIST_NO, loginDTO)%>';
        let headingLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REJECT_REMARKS, loginDTO)%>';

        bootbox.dialog({
            message: "<div class='row'>" +
                "<div class='col-lg-2'>" + remarksLabel + "</div>" +
                "<textarea class='col-lg-8' style='border: 1px solid' id = 'reject-remarks'></textarea>" +
                "</div>",
            title: headingLabel,
            centerVertical: true,
            closeButton: false,
            buttons: [

                {
                    label: cancelLabel,
                    className: "btn btn-sm btn-danger text-white shadow",
                    callback: function () {
                        // console.log( $('#shortlist-remarks').val());
                        // console.log('nothing happened')
                    }
                },
                {
                    label: saveLabel,
                    className: "btn btn-sm btn-success text-white shadow",
                    callback: function () {
                        let remarks = $('#reject-remarks').val();
                        makeRejected(selectedId, remarks);
                    }
                }
            ]
        });

    }

    function makeRejected(id, remarks) {
        let formData = new FormData();
        formData.append('id', id);
        formData.append('remarks', remarks);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");
                toastr.success("Success");
                onSubmit('');
            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeRejected";
        xhttp.open("POST", params, true);
        xhttp.send(formData);
    }

    function makeViewed(id) {
        event.preventDefault();
        let formData = new FormData();
        formData.append('id', id);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");
                let link = "Job_applicant_applicationServlet?actionType=getSummaryPage&ID=" + id;
                window.open(link, "_blank");

            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeViewed";
        xhttp.open("POST", params, true);
        xhttp.send(formData);
    }

    let numberBangla = {
        '0': '০',
        '1': '১',
        '2': '২',
        '3': '৩',
        '4': '৪',
        '5': '৫',
        '6': '৬',
        '7': '৭',
        '8': '৮',
        '9': '৯'
    };

    function toBn(retStr) {
        retStr = retStr + "";
        for (let x in numberBangla) {
            retStr = retStr.replace(new RegExp(x, 'g'),
                numberBangla[x]);
        }
        return retStr;
    }

</script>
