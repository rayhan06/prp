<%@page import="login.LoginDTO" %>
<%@page import="employee_publication.*" %>
<%@page import="java.util.*" %>
<%@ page import="pb.*" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="geolocation.GeoCountryRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    Employee_publicationDTO employee_publicationDTO;
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String actionName,formTitle;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        formTitle = LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_EMPLOYEE_PUBLICATION_EDIT_FORMNAME, loginDTO);
        employee_publicationDTO = (Employee_publicationDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        formTitle = LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_EMPLOYEE_PUBLICATION_ADD_FORMNAME, loginDTO);
        employee_publicationDTO = new Employee_publicationDTO();
    }

    long ColumnID;
    int i = 0;
    long empRecordsId ;
    if (request.getParameter("empId") == null) {
        empRecordsId = userDTO.employee_record_id;
    }else{
        empRecordsId = Long.parseLong(request.getParameter("empId"));
    }
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String select2PlaceHolder = LM.getText(LC.GLOBAL_TYPE_TO_SELECT, Language);
    String url = "Employee_publicationServlet?actionType=" + "ajax_" + actionName + "&isPermanentTable=true&empId=" + empRecordsId+"&iD="+employee_publicationDTO.iD;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" action="<%=url%>"
              id="employee-publication-id" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="publicationCat_category_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_PUBLICATIONCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='publicationCat_div_<%=i%>'>
                                            <select class='form-control' name='publicationCat'
                                                    id='publicationCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("publication", Language, employee_publicationDTO.publicationCat)%>
                                            </select>

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="contributionCat_category_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_CONTRIBUTIONCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='contributionCat_div_<%=i%>'>
                                            <select class='form-control' name='contributionCat'
                                                    id='contributionCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("contribution", Language, employee_publicationDTO.contributionCat)%>
                                            </select>

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="conferenceCat_category_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_CONFERENCECAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='conferenceCat_div_<%=i%>'>
                                            <select class='form-control' name='conferenceCat'
                                                    id='conferenceCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("conference", Language, employee_publicationDTO.conferenceCat)%>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group row" id="countries_div">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="geoCountriesType_select_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_GEOCOUNTRIESTYPE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='geoCountriesType_div_<%=i%>'>
                                            <select class='form-control' name='geoCountriesType'
                                                    id='geoCountriesType_select_<%=i%>'
                                                    tag='pb_html'>
                                                <%=GeoCountryRepository.getInstance().buildOptions(Language, employee_publicationDTO.geoCountriesType)%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="publicationTitle_text_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_PUBLICATIONTITLE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='publicationTitle_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='publicationTitle'
                                                   id='publicationTitle_text_<%=i%>'
                                                   value='<%=employee_publicationDTO.publicationTitle!=null?employee_publicationDTO.publicationTitle:""%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="conferenceTitle_text_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_CONFERENCETITLE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='conferenceTitle_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='conferenceTitle'
                                                   id='conferenceTitle_text_<%=i%>'
                                                   value='<%=employee_publicationDTO.conferenceTitle!=null?employee_publicationDTO.conferenceTitle:""%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="location_text_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_LOCATION, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='location_div_<%=i%>'>
                                            <input type='text' class='form-control' name='location'
                                                   id='location_text_<%=i%>'
                                                   value='<%=employee_publicationDTO.location!=null?employee_publicationDTO.location:""%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_PUBLICATIONDATE, loginDTO)) : (LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_PUBLICATIONDATE, loginDTO))%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='publicationDate_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="publication-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' class='form-control'
                                                   id='publication-date' name='publicationDate' value=''
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="briefDescription_textarea_<%=i%>">
                                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_BRIEFDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='briefDescription_div_<%=i%>'>
                                                            <textarea class='form-control' name='briefDescription'
                                                                      maxlength="1024"
                                                                      id='briefDescription_textarea_<%=i%>' rows="4"
                                                                      style="resize: none"
                                                                      tag='pb_html'><%=employee_publicationDTO.briefDescription != null ? employee_publicationDTO.briefDescription : ""%></textarea>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_FILESDROPZONE, loginDTO)) : (LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_FILESDROPZONE, loginDTO))%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='filesDropzone_div_<%=i%>'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(employee_publicationDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                                        <%
                                                            }
                                                        %>
                                                        <a href='Employee_publicationServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                           download><%=filesDTO.fileTitle%>
                                                        </a>
                                                        <a class='btn btn-danger'
                                                           onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                            <div class="dropzone"
                                                 action="Employee_publicationServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_publicationDTO.filesDropzone:ColumnID%>">
                                                <input type='file' style="display:none"
                                                       name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete_<%=i%>'
                                                   value='' tag='pb_html'/>
                                            <input type='hidden' name='filesDropzone'
                                                   id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=actionName.equals("edit")?employee_publicationDTO.filesDropzone:ColumnID%>'/>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-3 row">
                    <div class="col-md-11 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_EMPLOYEE_PUBLICATION_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_EMPLOYEE_PUBLICATION_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';
    const publicationForm = $('#employee-publication-id');

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        select2SingleSelector("#publicationCat_category_0", '<%=Language%>');
        select2SingleSelector("#contributionCat_category_0", '<%=Language%>');
        select2SingleSelector("#conferenceCat_category_0", '<%=Language%>');
        select2SingleSelector("#geoCountriesType_select_0", '<%=Language%>');
        $('#countries_div').hide();
        dateTimeInit("<%=Language%>");
        $('#conferenceCat_category_0').change(function () {
            showOrHideCountriesType();
        });
        $.validator.addMethod('publicationCatSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('contributionCatSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('conferenceCatSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('geoCountriesTypeSelection', function (value, element) {
            if ($('#conferenceCat_category_0').val() == 1)
                return true;
            return value != 0;
        });
        $("#employee-publication-id").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                publicationTitle: "required",
                conferenceTitle: "required",
                location: "required",
                publicationCat: {
                    required: true,
                    publicationCatSelection: true
                },
                contributionCat: {
                    required: true,
                    contributionCatSelection: true
                },
                conferenceCat: {
                    required: true,
                    conferenceCatSelection: true
                },
                geoCountriesType: {
                    geoCountriesTypeSelection: true
                }
            },

            messages: {
                publicationTitle: "<%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_EMPLOYEE_PUBLICATION_PLEASE_ENTER_PUBLICATION_TITLE, userDTO)%>",
                conferenceTitle: "<%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_EMPLOYEE_PUBLICATION_PLEASE_ENTER_CONFERENCE_TITLE, userDTO)%>",
                location: "<%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_EMPLOYEE_PUBLICATION_PLEASE_ENTER_LOCATION, userDTO)%>",
                publicationCat: "<%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_EMPLOYEE_PUBLICATION_PLEASE_ENTER_PUBLICATION_TYPE, userDTO)%>",
                contributionCat: "<%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_EMPLOYEE_PUBLICATION_PLEASE_ENTER_CONTRIBUTION_TYPE, userDTO)%>",
                conferenceCat: "<%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_EMPLOYEE_PUBLICATION_PLEASE_ENTER_CONFERENCE_TYPE, userDTO)%>",
                geoCountriesType: "<%=LM.getText(LC.EMPLOYEE_PUBLICATION_ADD_EMPLOYEE_PUBLICATION_PLEASE_ENTER_LOCATION_OF_PUBLICATION, userDTO)%>"
            }
        });
    });

    function isFormValid() {
        publicationForm.validate();
        const jQueryValid = publicationForm.valid();
        $('#publication-date').val(getDateStringById('publication-date-js'));
        const dateValid = dateValidator('publication-date-js', true, {
            'errorEn': 'Enter valid publication date!',
            'errorBn': 'প্রকাশনার তারিখ প্রবেশ করান!'
        });
        return jQueryValid && dateValid;
    }

    function showOrHideCountriesType() {
        if ($('#conferenceCat_category_0').val() && $('#conferenceCat_category_0').val() == 2) {
            $('#countries_div').show();
        } else {
            $('#countries_div').hide();
            $('#geoCountriesType_select_0').val('');
            $('#select2-geoCountriesType_select_0-container').text('<%=select2PlaceHolder%>');
            $('#select2-geoCountriesType_select_0-container').css('color', '#999999');

        }
    }

    $(function () {
        <%if (actionName.equals("edit")) {%>
        setDateByTimestampAndId('publication-date-js', <%=employee_publicationDTO.publicationDate%>);
        <%}%>

    });

    function submitForm() {
        buttonStateChange(true);
        if(isFormValid()){
            submitAjaxForm('employee-publication-id');
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>