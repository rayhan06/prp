<%@page import="util.StringUtils" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_publication.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="files.*" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    Employee_publicationDTO employee_publicationDTO = (Employee_publicationDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.EMPLOYEE_PUBLICATION_SEARCH_EMPLOYEE_PUBLICATION_DETAILS, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h4 class="table-title">
                <%=LM.getText(LC.EMPLOYEE_PUBLICATION_SEARCH_ANYFIELD, loginDTO)%>
            </h4>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_EMPLOYEERECORDSID, loginDTO)%>
                            </b></td>
                        <td><%=Employee_recordsRepository.getInstance().getEmployeeName(employee_publicationDTO.employeeRecordsId, Language)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_PUBLICATIONCAT, loginDTO)%>
                            </b></td>
                        <td><%=CatRepository.getInstance().getText(Language, "publication", employee_publicationDTO.publicationCat)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_CONTRIBUTIONCAT, loginDTO)%>
                            </b></td>
                        <td><%=CatRepository.getInstance().getText(Language, "contribution", employee_publicationDTO.contributionCat)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_CONFERENCECAT, loginDTO)%>
                            </b></td>
                        <td><%=CatRepository.getInstance().getText(Language, "conference", employee_publicationDTO.conferenceCat)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_GEOCOUNTRIESTYPE, loginDTO)%>
                            </b></td>
                        <td><%=GeoCountryRepository.getInstance().getText(Language, employee_publicationDTO.geoCountriesType)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_PUBLICATIONTITLE, loginDTO)%>
                            </b></td>
                        <td><%=employee_publicationDTO.publicationTitle == null ? "" : StringEscapeUtils.escapeHtml4(employee_publicationDTO.publicationTitle)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_CONFERENCETITLE, loginDTO)%>
                            </b></td>
                        <td><%=employee_publicationDTO.conferenceTitle == null ? "" : StringEscapeUtils.escapeHtml4(employee_publicationDTO.conferenceTitle)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_LOCATION, loginDTO)%>
                            </b></td>
                        <td><%=employee_publicationDTO.location == null ? "" : StringEscapeUtils.escapeHtml4(employee_publicationDTO.location)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_PUBLICATIONDATE, loginDTO)%>
                            </b></td>
                        <td><%=StringUtils.getFormattedDate(Language, employee_publicationDTO.publicationDate)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_BRIEFDESCRIPTION, loginDTO)%>
                            </b></td>
                        <td><%=employee_publicationDTO.briefDescription == null ? "" : StringEscapeUtils.escapeHtml4(employee_publicationDTO.briefDescription)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, loginDTO)%>
                            </b></td>
                        <td>
                            <%
                                {
                                    List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(employee_publicationDTO.filesDropzone);
                            %>
                            <table>
                                <tr>
                                    <%
                                        if (FilesDTOList != null) {
                                            for (int j = 0; j < FilesDTOList.size(); j++) {
                                                FilesDTO filesDTO = FilesDTOList.get(j);
                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                    %>
                                    <td>
                                        <%
                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                        %>
                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                             style='width:100px'/>
                                        <%
                                            }
                                        %>
                                        <a href='Employee_publicationServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                           download><%=filesDTO.fileTitle%>
                                        </a>
                                    </td>
                                    <%
                                            }
                                        }
                                    %>
                                </tr>
                            </table>
                            <%
                                }
                            %>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>