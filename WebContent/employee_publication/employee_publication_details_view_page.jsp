<%@ page import="employee_publication.*" %>
<%@ page import="java.util.List" %>
<input type="hidden" data-ajax-marker="true">
<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
        <tr>
            <th><b><%= LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_PUBLICATIONCAT, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_CONTRIBUTIONCAT, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_PUBLICATIONTITLE, loginDTO) %></b></th>
            <th><b><%= LM.getText(LC.EMPLOYEE_PUBLICATION_EDIT_CONFERENCETITLE, loginDTO) %></b></th>
            <th><b></b></th>
        </tr>
    </thead>
    <tbody>
        <%
            List<Employee_publicationDetails> savedEmpPublicationDetailsList = (List<Employee_publicationDetails>) request.getAttribute("savedEmpPublicationDetailsList");
            if(savedEmpPublicationDetailsList !=null && savedEmpPublicationDetailsList.size()>0){
                int publicationIndex = 0;
                for(Employee_publicationDetails savedEmpPublicationDetails: savedEmpPublicationDetailsList){
                    ++publicationIndex;
        %>
            <tr>
                <%@include file="/employee_publication/employee_publication_details_view_item.jsp"%>
            </tr>
        <%} }%>
    </tbody>
</table>



<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_publicationServlet?actionType=getAddPage&tab=6&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp;<%= LM.getText(LC.HR_MANAGEMENT_OTHERS_ADD_PUBLICATION, loginDTO) %></button>
    </div>
</div>
