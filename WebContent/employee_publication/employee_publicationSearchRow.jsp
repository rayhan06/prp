<%@page import="employee_records.Employee_recordsRepository" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="util.StringUtils" %>
<%
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(employee_publicationDTO.employeeRecordsId);
    }
%>

<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_publicationDTO.employeeRecordsId, Language)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "publication", employee_publicationDTO.publicationCat)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "contribution", employee_publicationDTO.contributionCat)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language, "conference", employee_publicationDTO.conferenceCat)%>
</td>

<td>
    <%=GeoCountryRepository.getInstance().getText(Language, employee_publicationDTO.geoCountriesType)%>
</td>


<td>
    <%=StringEscapeUtils.escapeHtml4(employee_publicationDTO.publicationTitle)%>
</td>


<td>
    <%=StringUtils.getFormattedDate(Language, employee_publicationDTO.publicationDate)%>
</td>

<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Employee_publicationServlet?actionType=view&iD=<%=employee_publicationDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button" class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_publicationServlet?actionType=getEditPage&iD=<%=employee_publicationDTO.iD%>&empId=<%=empId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_publicationDTO.iD%>'/></span>
    </div>
</td>