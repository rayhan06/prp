<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<td style="vertical-align: middle;"><%=StringEscapeUtils.escapeHtml4(isLanguageEnglish ? savedEmpPublicationDetails.publicationEng : savedEmpPublicationDetails.publicationBng)%>
</td>
<td style="vertical-align: middle;"><%=StringEscapeUtils.escapeHtml4(isLanguageEnglish ? savedEmpPublicationDetails.contributionEng : savedEmpPublicationDetails.contributionBng)%>
</td>
<td style="vertical-align: middle;"><%=StringEscapeUtils.escapeHtml4(savedEmpPublicationDetails.dto.publicationTitle)%>
</td>
<td style="vertical-align: middle;"><%=StringEscapeUtils.escapeHtml4(savedEmpPublicationDetails.dto.conferenceTitle)%>
</td>
<td style="vertical-align: middle;text-align: center">
    <form action="Employee_publicationServlet?isPermanentTable=true&actionType=delete&tab=6&ID=<%=savedEmpPublicationDetails.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>"
          method="POST" id="tableForm_publication_details<%=publicationIndex%>" enctype="multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn-primary" type="button"
                    title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                    onclick="location.href='Employee_publicationServlet?actionType=view&iD=<%=savedEmpPublicationDetails.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-eye"></i></button>
            &nbsp
            <button class="btn-success" type="button" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO) %>"
                    onclick="location.href='<%=request.getContextPath()%>/Employee_publicationServlet?actionType=getEditPage&tab=6&iD=<%=savedEmpPublicationDetails.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-edit"></i></button>
            &nbsp
            <button class="btn-danger" type="button" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                    onclick="deleteItem('tableForm_publication_details',<%=publicationIndex%>)"><i
                    class="fa fa-trash"></i></button>
        </div>
    </form>
</td>