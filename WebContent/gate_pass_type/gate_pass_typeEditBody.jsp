<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="gate_pass_type.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%
    Gate_pass_typeDTO gate_pass_typeDTO;
    gate_pass_typeDTO = (Gate_pass_typeDTO) request.getAttribute("gate_pass_typeDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (gate_pass_typeDTO == null) {
        gate_pass_typeDTO = new Gate_pass_typeDTO();

    }
    System.out.println("gate_pass_typeDTO = " + gate_pass_typeDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.GATE_PASS_TYPE_ADD_GATE_PASS_TYPE_ADD_FORMNAME, loginDTO);
    String servletName = "Gate_pass_typeServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Gate_pass_typeServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-8 offset-1">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <%@ page import="java.text.SimpleDateFormat" %>
                                                    <%@ page import="java.util.Date" %>
                                                    <%@ page import="pb.*" %>
                                                    <%
                                                        String Language = LM.getText(LC.GATE_PASS_TYPE_EDIT_LANGUAGE, loginDTO);
                                                        String Options;
                                                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                                        Date date = new Date();
                                                        String datestr = dateFormat.format(date);
                                                        CommonDAO.language = Language;
                                                        CatDAO.language = Language;
                                                    %>
                                                    <input type='hidden' class='form-control' name='ID'
                                                           id='id_hidden_<%=i%>'
                                                           value='<%=gate_pass_typeDTO.iD%>' tag='pb_html'/>
                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=LM.getText(LC.GATE_PASS_TYPE_ADD_NAMEENG, loginDTO)%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div id='nameEng_div_<%=i%>'>
                                                                <input type='text' class='form-control' name='nameEng'
                                                                       id='nameEng_text_<%=i%>'
                                                                       value='<%=gate_pass_typeDTO.nameEng%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=LM.getText(LC.GATE_PASS_TYPE_ADD_NAMEBNG, loginDTO)%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div id='nameBng_div_<%=i%>'>
                                                                <input type='text' class='form-control' name='nameBng'
                                                                       id='nameBng_text_<%=i%>'
                                                                       value='<%=gate_pass_typeDTO.nameBng%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=LM.getText(LC.GATE_PASS_TYPE_ADD_DESCRIPTIONENG, loginDTO)%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div id='descriptionEng_div_<%=i%>'>
                                                                <input type='text' class='form-control'
                                                                       name='descriptionEng'
                                                                       id='descriptionEng_text_<%=i%>'
                                                                       value='<%=gate_pass_typeDTO.descriptionEng%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-4 col-form-label text-right">
                                                            <%=LM.getText(LC.GATE_PASS_TYPE_ADD_DESCRIPTIONBNG, loginDTO)%>
                                                        </label>
                                                        <div class="col-8">
                                                            <div id='descriptionBng_div_<%=i%>'>
                                                                <input type='text' class='form-control'
                                                                       name='descriptionBng'
                                                                       id='descriptionBng_text_<%=i%>'
                                                                       value='<%=gate_pass_typeDTO.descriptionBng%>'
                                                                       tag='pb_html'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type='hidden' class='form-control' name='insertionDate'
                                                           id='insertionDate_hidden_<%=i%>'
                                                           value='<%=gate_pass_typeDTO.insertionDate%>' tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='insertedBy'
                                                           id='insertedBy_hidden_<%=i%>'
                                                           value='<%=gate_pass_typeDTO.insertedBy%>' tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='modifiedBy'
                                                           id='modifiedBy_hidden_<%=i%>'
                                                           value='<%=gate_pass_typeDTO.modifiedBy%>' tag='pb_html'/>
                                                    <input type='hidden' class='form-control' name='isDeleted'
                                                           id='isDeleted_hidden_<%=i%>'
                                                           value='<%=gate_pass_typeDTO.isDeleted%>' tag='pb_html'/>
                                                    <input type='hidden' class='form-control'
                                                           name='lastModificationTime'
                                                           id='lastModificationTime_hidden_<%=i%>'
                                                           value='<%=gate_pass_typeDTO.lastModificationTime%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-10">
                                <div class="form-actions text-right mt-4 mb-5">
                                    <button class="btn-sm shadow text-white border-0 cancel-btn"
                                            style="border-radius: 8px">
                                        <%=LM.getText(LC.GATE_PASS_TYPE_ADD_GATE_PASS_TYPE_CANCEL_BUTTON, loginDTO)%>
                                    </button>
                                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                            type="submit">
                                        <%=LM.getText(LC.GATE_PASS_TYPE_ADD_GATE_PASS_TYPE_SUBMIT_BUTTON, loginDTO)%>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(() => {
        $(".cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    $(document).ready(function () {

        dateTimeInit("<%=Language%>");
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Gate_pass_typeServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






