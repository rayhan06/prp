
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
String url = "Gate_pass_typeServlet?actionType=search";
String navigator = SessionConstants.NAV_GATE_PASS_TYPE;
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

String pageno = "";

RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
boolean isPermanentTable = rn.m_isPermanentTable;

System.out.println("rn " + rn);

String action = url;
String context = "../../.." + request.getContextPath() + "/";
String link = context + url;
String concat = "?";
if (url.contains("?")) {
	concat = "&";
}
int pagination_number = 0;
%>

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;&nbsp;
		<h3 class="kt-subheader__title">
			<%=LM.getText(LC.GATE_PASS_TYPE_SEARCH_GATE_PASS_TYPE_SEARCH_FORMNAME, loginDTO)%>
		</h3>
	</div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
	<div class="row shadow-none border-0">
		<div class="col-lg-12">
			<jsp:include page="./gate_pass_typeNav.jsp" flush="true">
				<jsp:param name="url" value="<%=url%>" />
				<jsp:param name="navigator" value="<%=navigator%>" />
				<jsp:param name="pageName" value="<%=LM.getText(LC.GATE_PASS_TYPE_SEARCH_GATE_PASS_TYPE_SEARCH_FORMNAME, loginDTO)%>" />
			</jsp:include>
			<div style="height: 1px; background: #ecf0f5"></div>
			<div class="kt-portlet shadow-none">
				<div class="kt-portlet__body">
					<form action="Gate_pass_typeServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete" method="POST" id="tableForm" enctype = "multipart/form-data">
						<jsp:include page="gate_pass_typeSearchForm.jsp" flush="true">
							<jsp:param name="pageName" value="<%=LM.getText(LC.GATE_PASS_TYPE_SEARCH_GATE_PASS_TYPE_SEARCH_FORMNAME, loginDTO)%>" />
						</jsp:include>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<% pagination_number = 1;%>
<%@include file="../common/pagination_with_go2.jsp"%>
<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	initDeleteCheckBoxes();	
	dateTimeInit("<%=Language%>");
});

</script>


