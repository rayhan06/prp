<%@page import="login.LoginDTO"%>

<%@page import="pi_auction_items.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="org.jsoup.Jsoup" %>
<%@ page import="org.jsoup.safety.Whitelist" %>
<%@ page import="com.google.gson.Gson" %>

<%
	String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
	Pi_auction_itemsDTO pi_auction_itemsDTO = new Pi_auction_itemsDTO();
	long ID = -1;
	if(request.getParameter("ID") != null)
	{
		ID = Long.parseLong(request.getParameter("ID"));
		pi_auction_itemsDTO = Pi_auction_itemsDAO.getInstance().getDTOByID(ID);
	}

	LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;

	String Value = request.getParameter("productIds");
	if (Value != null) {
		Value = Jsoup.clean(Value, Whitelist.simpleText());
	}
	Gson gson = new Gson();
	PiProductModel[] piProductModels = gson.fromJson(Value, PiProductModel[].class);

%>


<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
	<div class="kt-subheader__main">
		<i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
		<h3 class="kt-subheader__title">
			&nbsp; <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PRODUCTNAME, loginDTO)%>
		</h3>
	</div>
</div>
<!-- end:: Subheader -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__body">

			<div class="mt-5">
				<form class="form-horizontal" id="bigform" name="bigform">

					<div class="form-group row" >
						<label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.PI_AUCTION_ADD_AUCTIONPACKAGENAME, loginDTO)%></label>
						<div class="col-md-7">

							<input type='text' name='auctionPackageName'class='form-control' tag='pb_html'>
						</div>
					</div>
					<div class="table-responsive">

						<table id="tableData" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PRODUCTNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_SUPPLIERCOMPANYNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_BUYINGDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_FISCALYEARID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_UNITBUYINGPRICE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_STOCK, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_SELLINGUNIT, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_UNITSELLINGPRICE, loginDTO)%></th>



							</tr>
							</thead>
							<tbody>
							<%
								for(PiProductModel piProductModel : piProductModels){
							%>
							<tr>



								<input type='hidden' class='form-control'  name='piPurchaseItemsId' value="<%=piProductModel.iD%>"     tag='pb_html'/>
								<input type='hidden' class='form-control'  name='itemId' value="<%=piProductModel.itemId%>"     tag='pb_html'/>
								<input type='hidden' class='form-control'  name='productName' value="<%=piProductModel.productName%>"     tag='pb_html'/>
								<input type='hidden' class='form-control'  name='supplierCompanyName' value="<%=piProductModel.supplierCompanyName%>"     tag='pb_html'/>
								<input type='hidden' class='form-control'  name='buyingDate' value="<%=piProductModel.hiddenBuyingDate%>"     tag='pb_html'/>
								<input type='hidden' class='form-control'  name='fiscalYearId' value="<%=piProductModel.hiddenFiscalYear%>"     tag='pb_html'/>
								<input type='hidden' class='form-control'  name='unitBuyingPrice' value="<%=piProductModel.unitBuyingPrice%>"     tag='pb_html'/>
								<input type='hidden' class='form-control'  name='stock' value="<%=piProductModel.stock%>"     tag='pb_html'/>
								<input type='hidden' class='form-control'  name='officeUnitId' value="<%=piProductModel.officeUnitId%>"     tag='pb_html'/>



								<td>

									<%=piProductModel.productName%>


								</td>

								<td>
									<%=piProductModel.supplierCompanyName%>


								</td>

								<td>
									<%=piProductModel.buyingDate%>
								</td>

								<td>
									<%=piProductModel.fiscalYearId%>
								</td>

								<td>
									<%=Utils.getDigits(piProductModel.unitBuyingPrice, Language)%>


								</td>

								<td>
									<%=Utils.getDigits(piProductModel.stock, Language)%>


								</td>

								<td>

									<input type='number' class='form-control' min="0" max="<%=piProductModel.stock%>"
										   name='sellingUnit' onchange="sellingUnitValidation(this)" tag='pb_html'/>

								</td>

								<td>

									<input type='number' class='form-control' min="0" name='unitSellingPrice' tag='pb_html'/>

								</td>


							</tr>
							<%
								}
							%>



							</tbody>

						</table>


					</div>


					<div class="text-right mt-3">
						<button class="btn btn-sm shadow text-white submit-btn "
								type="button" id="submit-btn" onclick="submitForm()">
							<%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_EDIT_SUBMIT_BUTTON, loginDTO)%></button>
					</div>


				</form>
			</div>

		</div>
	</div>
</div>


<script type="text/javascript">
	const piAuctionPreviewForm = $("#bigform");
	const language = '<%=Language%>';

	function submitForm(){
		buttonStateChange(true);
		if(PreprocessBeforeSubmiting(0)){
			$.ajax({
				type : "POST",
				url : "Pi_auctionServlet?actionType=ajax_add",
				data : piAuctionPreviewForm.serialize(),
				dataType : 'JSON',
				success : function(response) {
					console.log(response);
					if(response.responseCode === 0){
						$('#toast_message').css('background-color','#ff6063');
						showToastSticky(response.msg,response.msg);
						buttonStateChange(false);
					}else if(response.responseCode === 200){
						window.location.replace(getContextPath()+response.msg);
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
					toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
							+ ", Message: " + errorThrown);
					buttonStateChange(false);
				}
			});
		}else{
			buttonStateChange(false);
		}
	}

	function buttonStateChange(value){
		$('#submit-btn').prop('disabled',value);
		$('#cancel-btn').prop('disabled',value);
	}


	function PreprocessBeforeSubmiting(row, action)
	{
		return true;
	}


	function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
	{
		addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Pi_auction_itemsServlet");
	}

	function init(row)
	{

		setDateByStringAndId('buyingDate_js_' + row, $('#buyingDate_date_' + row).val());


	}

	var row = 0;
	$(document).ready(function(){

		console.log($(document).prop('title'));
		if($(document).prop('title') === 'Edit User' || $(document).prop('title') === 'View')
		{
			$(document).prop('title', '<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_SELECT_ASSIGNMENTDETAILSEN, loginDTO)%>');
		}
		init(row);
		CKEDITOR.replaceAll();
		$("#cancel-btn").click(e => {
			e.preventDefault();
			location.href = "<%=request.getHeader("referer")%>";
		})
	});


	function sellingUnitValidation(input){
		let value = input.value;
		let min = input.min;
		let max = input.max;
		if(value < min){
			input.value = 0;
			swal.fire(getValueByLanguage(language, 'অবৈধ মান!', 'Invalid Value!'));
		} else if(value > max){
			input.value = max;
			swal.fire(getValueByLanguage(language, 'অবৈধ মান!', 'Invalid Value!'))
		}
	}


</script>






