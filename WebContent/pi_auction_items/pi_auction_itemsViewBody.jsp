

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_auction_items.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Pi_auction_itemsServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_auction_itemsDTO pi_auction_itemsDTO = Pi_auction_itemsDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pi_auction_itemsDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PI_AUCTION_ITEMS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PI_AUCTION_ITEMS_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PRODUCTNAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_auction_itemsDTO.productName + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_SUPPLIERCOMPANYNAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_auction_itemsDTO.supplierCompanyName + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_BUYINGDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_auction_itemsDTO.buyingDate + "";
											%>
											<%
											String formatted_buyingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_buyingDate, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_FISCALYEARID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_auction_itemsDTO.fiscalYearId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_UNITBUYINGPRICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_auction_itemsDTO.unitBuyingPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_auction_itemsDTO.unitBuyingPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_STOCK, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_auction_itemsDTO.stock + "";
											%>
											<%
											value = String.format("%.1f", pi_auction_itemsDTO.stock);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_SELLINGUNIT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_auction_itemsDTO.sellingUnit + "";
											%>
											<%
											value = String.format("%.1f", pi_auction_itemsDTO.sellingUnit);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_UNITSELLINGPRICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_auction_itemsDTO.unitSellingPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_auction_itemsDTO.unitSellingPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>