<!-- Modal -->
<%--
  Author: Moaz Mahmud
  USAGE: see disciplinary_log/disciplinary_logEditBody.jsp
--%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String isHierarchyNeededStr = request.getParameter("isHierarchyNeeded");
    boolean isHierarchyNeeded = isHierarchyNeededStr != null && Boolean.parseBoolean(isHierarchyNeededStr);
%>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_emp_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf; margin-left: 10px">
                    <%=LM.getText(LC.EMPLOYEE_SEARCH_MODAL_FIND_EMPLOYEE, userDTO)%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-xl">
                <%@include file="employeeSearchModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn btn-secondary shadow" data-dismiss="modal" style="border-radius: 6px; margin-right: 10px">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    language = '<%=Language%>';

    function initLayer1(){
        // fetch options for new select
        let url = "Office_unitsServlet?actionType=<%=isHierarchyNeeded? "ajax_employee_office" : "ajax_office&parent_id=0"%>&language=" + language;
        console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                document.getElementById('officeLayer_select_1').innerHTML = data;
                <%if(isHierarchyNeeded){%>
                let url = "EmployeeAssignServlet?actionType=getAllDescentEmployeeList&pb=1";
                $.ajax({
                    type: "GET",
                    url: url,
                    async: true,
                    success: function (data) {
                        if (this.responseText !== '') {
                            showInSearchTable(data);
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                <%}%>
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function clearSearchByInfo() {
        // clear search table
        document.getElementById('employeeSearchModal_table_div').innerHTML = "";
        $('#search_by_userName').val('');
        $('#search_by_nameEn').val('');
        $('#search_by_nameBn').val('');
    }

    // modal on load event
    $('#search_emp_modal').on('show.bs.modal', function () {
        $('#officeLayer_select_1').val('');
        // fetch layer 1 options
        initLayer1();
        clearNextLayersFrom(1);
        clearSearchByInfo();
    });

    $('input:radio[name="search_by"]').change(function () {
        // TODO: confused on to clear organogram search on tab switch
        clearSearchByInfo();
        initLayer1()
        clearNextLayersFrom(1);

        $(".search_by_div").hide();
        let selectedSearchByType = $(this).val();
        $('#' + selectedSearchByType + "_div").show();
    });

    function hide_selected_employees(filter_from_table_name, selected_employees_map_or_set) {
        console.log("filter function");
        console.log("selected_employees_map_or_set");
        console.log(selected_employees_map_or_set);
        let table_obj = document.getElementById(filter_from_table_name);
        let n_row = table_obj.rows.length;
        for (let i = 0; i < n_row; i++) {
            let row = table_obj.rows[i];
            let employee_record_id = row.id.split('_')[0];
            if (selected_employees_map_or_set.has(employee_record_id)) {
                hide(row);
            } else {
                un_hide(row);
            }
        }
    }

    function showInSearchTable(data) {
        document.getElementById('employeeSearchModal_table_div').innerHTML = '';
        document.getElementById('employeeSearchModal_table_div').innerHTML = data;

        //console.log('AJAX Fetched Data:\n' + data);

        // TODO: filter result here
        if (table_name_to_collcetion_map === undefined || table_name_to_collcetion_map === null) {
            console.log('table_name_to_collcetion_map not set!');
        } else {
            hide_selected_employees(
                'tableData', table_name_to_collcetion_map.get(modal_button_dest_table).info_map
            );
        }
    }

    function showEmployeeList(officeId) {
        let url = "EmployeeAssignServlet?actionType=getDisciplinaryEmployeeList&pb=1" + '&officeId=' + officeId;
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            success: function (data) {
                if (this.responseText !== '') {
                    showInSearchTable(data);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    $("#onOtherInfoSearch_form").submit(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        let userName = $('#search_by_userName').val();
        let nameEn = $('#search_by_nameEn').val();
        let nameBn = $('#search_by_nameBn').val();

        let url = 'EmployeeAssignServlet?actionType=searchEmployeeOfficeInfo&pb=1&userName='
            + userName + '&nameEn=' + nameEn + '&nameBn=' + nameBn;

        console.log('AJAX URL ' + url);
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            success: function (data) {
                if (this.responseText !== '') {
                    showInSearchTable(data);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    function getLayerId(officeLayerIdText) {
        // fromat: officeLayer_{select or div}_{layer id}
        return parseInt(officeLayerIdText.split('_')[2]);
    }
    
    function addEmployee()
    {
    	modal_button_dest_table = 'employeeToSet';
        $('#search_emp_modal').modal();
    }
    
    function addEmployeeWithTableId(tableId)
    {
    	modal_button_dest_table = tableId;
        $('#search_emp_modal').modal();
    }
    
    function addEmployeeWithRow(buttonId)
    {
    	var rowId = buttonId.split("_")[2];
    	var name = buttonId.split("_")[0];
    	var tableId = name + "_table_" + rowId;
    	modal_button_dest_table = tableId;
        $('#search_emp_modal').modal();
    }

    function clearNextLayersFrom(currentLayerId) {
        for (let selectElement of document.querySelectorAll('.office-layer')) {
            let layerId = getLayerId(selectElement.id);
            if (layerId > currentLayerId) {
                selectElement.remove();
            }
        }
    }

    newSelect = null;
    selectedOfficeId = null;
    newDiv = null;
    function layerChange(selectElement) {
        let layerId = getLayerId(selectElement.id);
        var text = $( "#" + selectElement.id + " option:selected" ).text();
        console.log("layerId = " + text);
        console.log(`layer ${layerId}: changed to : ${selectElement.value}`);
        clearNextLayersFrom(layerId);

        selectedOfficeId = selectElement.value;
        if($("#officeUnitType").length)
    	{
    		console.log("Setting office id = " + selectedOfficeId);
    		$("#officeUnitType").val(selectedOfficeId);
    		
    		if($("#employeeToSet").length)
   			{
    			var office = "office";
    			if(language == "Bangla")
   				{
    				office = "দপ্তর";
   				}
   				var tr = "<tr><td></td><td></td><td>" + office + ": " + text + "</td></td>";
   				$("#employeeToSet").html(tr);
   			}
    		//ajaxSubmit();
    	}
        if (selectedOfficeId === '') {
            return;
        }
        

        // clone the first div
        newDiv = document.getElementById('officeLayer_div_1').cloneNode(true);
        newDiv.id = 'officeLayer_div_' + (layerId + 1);
        newSelect = newDiv.getElementsByTagName('select')[0];
        newSelect.id = 'officeLayer_select_' + (layerId + 1);

        // fetch options for new select
        let url = "Office_unitsServlet?actionType=<%=isHierarchyNeeded? "ajax_employee_office" : "ajax_office"%>&parent_id="
                + selectedOfficeId + "&language=" + language;
        console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                console.log(data);
                if (newSelect !== null && data !== '') {
                    newSelect.innerHTML = data;
                    document.getElementById('officeLayer_dropdown_div').appendChild(newDiv);
                    showEmployeeList(selectedOfficeId);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function use_row_button(button, rowId) {
    	let containing_row = $("#" + rowId);
    	console.log("id " +  containing_row.attr("id"));
    	containing_row.find("td:last").remove();
    	console.log(modal_button_dest_table);
    	var x = containing_row.wrapAll('<div>').parent().html(); 
    	//console.log("setting: " + x);
    	$("#" + modal_button_dest_table).html(x);
    	
    	console.log("calling patient inputted with value = " + $("#userName_" + rowId).val());
    	var orgId = $("#employeeId_" + rowId).val();
    	
    	if (typeof patient_inputted == 'function')
   		{
    		patient_inputted($("#userName_" + rowId).val(), orgId);
   		}
    	
    	var hiddenOrgRow = modal_button_dest_table.split("_")[2];
    	var hiddenOrgName = modal_button_dest_table.split("_")[0];
    	var hiddenOrgField = hiddenOrgName + "_hidden_" + hiddenOrgRow; 
    	
    	if($("#" + hiddenOrgField).length)
   		{
    		$("#" + hiddenOrgField).val(orgId);
   		} 
    	
    	$("#search_emp_modal").modal('hide');
    }
    
    

    function remove_containing_row(button, table_name) {
        let containing_row = button.parentNode.parentNode;
        let containing_table = containing_row.parentNode;
        containing_table.deleteRow(containing_row.rowIndex);

        // button id = "<employee record id>_button"
        let td_button = button.parentNode;
        let employee_record_id = td_button.id.split("_")[0];

        let added_info_map = table_name_to_collcetion_map.get(table_name).info_map;
        added_info_map.delete(employee_record_id);
        console.log('Employee to Remove', employee_record_id);
        console.log('Map After Removal',added_info_map);
    }
    var table_name_to_collcetion_map;
    var modal_button_dest_table = 'none';
 

    function hide(elememt) {
        elememt.style.display = "none";
    }

    function un_hide(elememt) {
        elememt.style.display = "";
    }
</script>

<%-- Author: Moaz Mahmud --%>