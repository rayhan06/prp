<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="geolocation.GeoLocationRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="employee_office_report.InChargeLevelEnum" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="util.HttpRequestUtils" %>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = LM.getText(LC.EMPLOYEE_OFFICE_REPORT_EDIT_LANGUAGE, loginDTO);
    String context = "../../.." + request.getContextPath() + "/assets/";
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Office_unitsDTO officeUnitsDTO = null;
    String officeUnitIdStr = request.getParameter("officeUnitId");
    if (officeUnitIdStr != null) {
        officeUnitsDTO = Office_unitsRepository.getInstance()
                .getOffice_unitsDTOByID(Long.parseLong(officeUnitIdStr));
    }


    Integer gender = StringUtils.parseNullableInteger(request.getParameter("gender"));
    Integer employeeClass = StringUtils.parseNullableInteger(request.getParameter("employeeClass"));
    Integer employmentCat = StringUtils.parseNullableInteger(request.getParameter("employmentCat"));
    Integer empOfficerCat = StringUtils.parseNullableInteger(request.getParameter("empOfficerCat"));
    String incharge_label = request.getParameter("incharge_label");
    Integer permanentDivisionId = StringUtils.parseNullableInteger(request.getParameter("permanent_division"));
    String onlySelectedOffice = request.getParameter("onlySelectedOffice");
%>
<script src="<%=context%>scripts/util1.js"></script>
<jsp:include page="../employee_assign/officeMultiSelectTagsUtil.jsp"/>
<div class="row mx-2">
    <div id="criteriaSelectionId" class="search-criteria-div col-md-6">
        <div class="form-group row">
            <div class="col-md-3"></div>
            <div class="col-md-9">
                <button type="button"
                        class="btn btn-sm btn-info btn-info text-white shadow btn-border-radius"
                        data-toggle="modal" data-target="#select_criteria_div" id="select_criteria_btn"
                        onclick="beforeOpenCriteriaModal()">
                    <%=isLangEng ? "Criteria Select" : "ক্রাইটেরিয়া বাছাই"%>
                </button>
            </div>
        </div>
    </div>
    <div id="criteriaPaddingId" class="search-criteria-div col-md-6">
    </div>
    <div id="officeUnitId" class="search-criteria-div col-md-6 officeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <%--Office Unit Modal Trigger Button--%>
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="office_units_id_modal_button"
                        onclick="officeModalButtonClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                </button>
                <input type="hidden" name='officeUnitIds' id='officeUnitIds_input' value="">
            </div>
        </div>
    </div>

    <div class="search-criteria-div col-md-6 officeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-sm-4 col-xl-3 col-form-label text-md-right" for="onlySelectedOffice_checkbox">
                <%=isLangEng ? "Only Selected Office" : "শুধুমাত্র নির্বাচিত অফিস"%>
            </label>
            <div class="col-1" id='onlySelectedOffice'>
                <input type='checkbox' class='form-control-sm mt-1' name='onlySelectedOffice'
                       id='onlySelectedOffice_checkbox'
                       onchange="this.value = this.checked;" value="<%=onlySelectedOffice!=null && onlySelectedOffice.equals("true")%>">
            </div>
            <div class="col-8"></div>
        </div>
    </div>

    <div class="search-criteria-div col-12" id="selected-offices" style="display: none;">
        <div class="form-group row">
            <div class="offset-1 col-10 tag-container" id="selected-offices-tag-container">
                <div class="tag template-tag">
                    <span class="tag-name"></span>
                    <i class="fas fa-times-circle tag-remove-btn"></i>
                </div>
            </div>
        </div>
    </div>

    <div id="officeUnitOrganogramId" class="search-criteria-div col-md-6 employeeModalClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>
            </label>
            <div class="col-md-9">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                        id="organogram_id_modal_button"
                        onclick="organogramIdModalBtnClicked();">
                    <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                </button>
                <div class="input-group" id="organogram_id_div" style="display: none">
                    <input type="hidden" name='officeUnitOrganogramId' id='organogram_id_input' value="">
                    <button type="button" class="btn btn-secondary form-control" disabled
                            id="organogram_id_text"></button>
                    <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                    <button type="button" class="btn btn-outline-danger" onclick="crsBtnClicked('organogram_id');"
                            id='organogram_id_crs_btn' tag='pb_html'>
                        x
                    </button>
                </span>
                </div>

            </div>
        </div>
    </div>

    <div id="gender_div" class="search-criteria-div col-md-6 genderClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="gender">
                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_GENDER, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='gender' id='gender' style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("gender", Language, gender)%>
                </select>
            </div>
        </div>
    </div>

    <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass"
         style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameEng">
                <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
            </label>
            <div class="col-md-9">
                <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng" style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in English" : "কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>

    <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass"
         style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="nameBng">
                <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
            </label>
            <div class="col-md-9">
                <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng" style="width: 100%"
                       placeholder="<%=isLangEng ? "Enter Employee Name in Bangla" : "কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                       value="">
            </div>
        </div>
    </div>

    <div id="employeeClass_div" class="search-criteria-div col-md-6 employeeClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="employeeClass">
                <%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='employeeClass' id='employeeClass' style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("employee_class", Language, employeeClass)%>
                </select>
            </div>
        </div>
    </div>


    <div id="employmentCat_div" class="search-criteria-div col-md-6 employmentCatClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="employmentCat">
                <%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='employmentCat' id='employmentCat' style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("employment", Language, employmentCat)%>
                </select>
            </div>
        </div>
    </div>

    <div id="emp_officer_div" class="search-criteria-div col-md-6 empOfficerClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="empOfficerCat">
                <%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='empOfficerCat' id='empOfficerCat' style="width: 100%">
                    <%=CatRepository.getInstance().buildOptions("emp_officer", Language, empOfficerCat)%>
                </select>
            </div>
        </div>
    </div>

    <div id="present_division_div" class="search-criteria-div col-md-6 presentDivisionClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_PRESENT_DIVISION, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='present_division' id='present_division' style="width: 100%">
                    <%=GeoLocationRepository.getInstance().buildOptionByLevel(Language, 1, null)%>
                </select>
            </div>
        </div>
    </div>

    <div id="permanent_division_div" class="search-criteria-div col-md-6 permanentDivisionClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="permanent_division">
                <%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_PERMANENT_DIVISION, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='permanent_division' id='permanent_division' style="width: 100%">
                    <%=GeoLocationRepository.getInstance().buildOptionByLevel(Language, 1, permanentDivisionId)%>
                </select>
            </div>
        </div>
    </div>

    <div id="permanent_district_div" class="search-criteria-div col-md-6 permanentDistrictClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='home_district' id='home_district' style="width: 100%">
                    <%=GeoLocationRepository.getInstance().buildOptionByLevel(Language, 2, null)%>
                </select>
            </div>
        </div>
    </div>

    <div id="age_div" class="search-criteria-div col-md-6 ageClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>
            </label>
            <div class="col-6 col-md-5">
                <input class='form-control' type="text" name='age_start' id='age_start'
                       placeholder='<%=LM.getText(LC.HR_REPORT_FROM, loginDTO)%>' style="width: 100%">
            </div>
            <div class="col-6 col-md-4">
                <input class='form-control' type="text" name='age_end' id='age_end'
                       placeholder='<%=LM.getText(LC.HR_REPORT_TO, loginDTO)%>' style="width: 100%">
            </div>
        </div>
    </div>
    <div id="incharge_label_div" class="search-criteria-div col-md-6 inchargeClass" style="display: none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right" for="incharge_label_select">
                <%=isLangEng ? "In charge Level" : "ইনচার্জ লেভেল"%>
            </label>
            <div class="col-md-9">
                <select multiple="multiple" class='form-control rounded' name='incharge_label_select'
                        id='incharge_label_select' style="width: 100%">
                        <%=InChargeLevelEnum.getBuildOptionsWithSelectedItem(Language,incharge_label==null?null:InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue())%>
                    <input type="hidden" name="incharge_label" id="incharge_label" value="">
            </div>
        </div>
    </div>

</div>

<%@include file="../common/hr_report_modal_util.jsp" %>
<script src="<%=context%>scripts/input_validation.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(() => {
        <%
            if(onlySelectedOffice!=null && onlySelectedOffice.equals("true")){
         %>
                $('#onlySelectedOffice_checkbox').prop('checked',true);
        <%
            }
        %>
        showFooter = false;
        $('#age_start').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 999);
        });
        $('#age_end').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 999);
        });
        <%if(officeUnitsDTO != null) {%>
        const officeUnitModel = {
            name: '<%=isLangEng?officeUnitsDTO.unitNameEng : officeUnitsDTO.unitNameBng%>',
            id: <%=officeUnitsDTO.iD%>
        };
        viewOfficeIdInTags(officeUnitModel);
        <%}%>
    });

    function init() {
        select2MultiSelector('#incharge_label_select', '<%=Language%>');
        select2SingleSelector('#gender', '<%=Language%>');
        select2SingleSelector('#employeeClass', '<%=Language%>');
        select2SingleSelector('#employmentCat', '<%=Language%>');
        select2SingleSelector('#empOfficerCat', '<%=Language%>');
        select2SingleSelector('#present_division', '<%=Language%>');
        select2SingleSelector('#permanent_division', '<%=Language%>');
        select2SingleSelector('#home_district', '<%=Language%>');
        dateTimeInit('<%=Language%>');
        criteriaArray = [{
            class: 'officeModalClass',
            title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_OFFICEUNITID, loginDTO)%>',
            show: false,
            specialCategory: 'officeModal',
            parameterVal:<%=officeUnitIdStr%>
        },
            {
                class: 'employeeModalClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_WHERE_OFFICEUNITORGANOGRAMID, loginDTO)%>',
                show: false,
                specialCategory: 'employeeModal'
            },
            {
                class: 'genderClass',
                title: '<%=LM.getText(LC.EMPLOYEE_INFO_REPORT_WHERE_GENDERCAT, loginDTO)%>',
                show: false,
                parameterVal:<%=gender%>
            },
            {
                class: 'employeeNameClass',
                title: '<%=isLangEng ? "Employee Name" : "কর্মকর্তা/কর্মচারীর নাম"%>',
                show: false
            },
            {
                class: 'employeeClass',
                title: '<%=LM.getText(LC.EMPLOYEE_EMPLOYEE_CLASS, loginDTO)%>',
                show: false,
                parameterVal:<%=employeeClass%>
            },
            {
                class: 'employmentCatClass',
                title: '<%=LM.getText(LC.EMPLOYMENT_CATEGORY_EMPLOYMENTCATEGORY, loginDTO)%>',
                show: false,
                parameterVal:<%=employmentCat%>
            },
            {
                class: 'empOfficerClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICER_TYPE, loginDTO)%>',
                show: false,
                parameterVal:<%=empOfficerCat%>
            },
            {
                class: 'presentDivisionClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_PRESENT_DIVISION, loginDTO)%>',
                show: false,

            },
            {
                class: 'permanentDivisionClass',
                title: '<%=LM.getText(LC.EMPLOYEE_OFFICE_REPORT_PERMANENT_DIVISION, loginDTO)%>',
                show: false,
                parameterVal:<%=permanentDivisionId%>
            },
            {
                class: 'permanentDistrictClass',
                title: '<%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_HOME_DISTRICT, loginDTO)%>',
                show: false
            },
            {class: 'ageClass', title: '<%=LM.getText(LC.HR_REPORT_AGE_RANGE, loginDTO)%>', show: false},
            {
                class: 'inchargeClass',
                title: '<%=isLangEng ? "In charge Level" : "ইনচার্জ লেভেল"%>',
                show: false,
                <%
                if(incharge_label!=null){
                %>
                parameterVal: '<%=InChargeLevelEnum.ROUTINE_RESPONSIBILITY.getValue()%>'
                <%
                }
                %>
            }

        ]

        $.each(criteriaArray, function (innerIndex, e) {
            if (e.parameterVal != undefined && e.parameterVal != null) {
                e.show = true;
                $('.' + e.class).show();
            }
        });
    }

    function PreprocessBeforeSubmiting() {
        ProcessMultipleSelectBoxBeforeSubmit("incharge_label_select", "incharge_label");
        let labelValue = document.getElementById("incharge_label").value;

        if (labelValue.trim().length == 0) {
            <%
            String value = Arrays.stream(InChargeLevelEnum.values()).map(levelValue->levelValue.getValue()).collect(Collectors.joining(","));
            %>
            labelValue = '<%=value%>';
        }

        let newLabelValue = labelValue.split(",").map(value => {
            value = "\'" + value.trim() + "\'";
            return value;
        }).join(",");

        document.getElementById("incharge_label").value = newLabelValue;
    }

    function ProcessMultipleSelectBoxBeforeSubmit(name, hiddenFieldName) {
        $("[name='" + name + "']").each(function (i) {
            var selectedInputs = $(this).val();
            var temp = "";
            if (selectedInputs != null) {
                selectedInputs.forEach(function (value, index, array) {
                    if (index > 0) {
                        temp += ", ";
                    }
                    temp += value;
                });
            }
            document.getElementById(hiddenFieldName).value = temp;
        });
    }
</script>