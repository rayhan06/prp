
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_bulk_attendance.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Recruitment_bulk_attendanceDAO recruitment_bulk_attendanceDAO = (Recruitment_bulk_attendanceDAO)request.getAttribute("recruitment_bulk_attendanceDAO");


String navigator2 = SessionConstants.NAV_RECRUITMENT_BULK_ATTENDANCE;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.EMPLOYEE_LEAVE_DETAILS_SEARCH_JOBCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_ROLL, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_STATUS, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_RECRUITMENT_BULK_ATTENDANCE_EDIT_BUTTON, loginDTO)%></th>
<%--								<th><input type="submit" class="btn btn-xs btn-danger" value="<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_RECRUITMENT_BULK_ATTENDANCE_DELETE_BUTTON, loginDTO)%>" /></th>--%>
<%--								<th><button type="submit" class="btn d-flex justify-content-around" ><i class="fa fa-trash" style="color:red"></i> </button></th>--%>

								<th class="d-flex justify-content-center">
									<button type="submit" class="btn d-flex justify-content-center" style="color: #ff6a6a">
										<i class="fa fa-trash"></i>&nbsp;
									</button>
									<%--									<input type="submit" class="btn btn-xs btn-danger" value="<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_RECRUITMENT_JOB_DESCRIPTION_DELETE_BUTTON, loginDTO)%>" />--%>
								</th>
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_RECRUITMENT_BULK_ATTENDANCE);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = (Recruitment_bulk_attendanceDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("recruitment_bulk_attendanceDTO",recruitment_bulk_attendanceDTO);
								%>  
								
								 <jsp:include page="./recruitment_bulk_attendanceSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			