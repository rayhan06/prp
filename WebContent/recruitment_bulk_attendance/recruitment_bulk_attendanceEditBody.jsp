
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="recruitment_bulk_attendance.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>

<%
Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO;
recruitment_bulk_attendanceDTO = (Recruitment_bulk_attendanceDTO)request.getAttribute("recruitment_bulk_attendanceDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(recruitment_bulk_attendanceDTO == null)
{
	recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();

}
System.out.println("recruitment_bulk_attendanceDTO = " + recruitment_bulk_attendanceDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_RECRUITMENT_BULK_ATTENDANCE_ADD_FORMNAME, loginDTO);


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="static sessionmanager.SessionConstants.AttendanceStatusMap" %>
<%
	String Language = LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	CommonDAO.language = Language;
	CatDAO.language = Language;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important; margin-bottom: -18px">


	<div class="row">


		<div class="col-lg-12">

			<div class="kt-portlet">

				<div class="kt-portlet__head">

					<div class="kt-portlet__head-label">

						<h3 class="kt-portlet__head-title prp-page-title">
							<i class="fa fa-gift"></i>&nbsp;

							<%=formTitle%>

						</h3>

					</div>

				</div>

				<form class="form-horizontal" action="Recruitment_bulk_attendanceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
					  id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
					  onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">


					<div class="kt-portlet__body form-body">

						<div class="row">

							<div class="col-md-1"></div>
							<div class="col-md-10">

								<div class="onlyborder">

									<div class="row">

										<div class="col-md-1"></div>
										<div class="col-md-11">

											<div class="sub_title_top">

												<div class="sub_title">
													<h4 style="background: white">

														<%=formTitle%>

													</h4>
												</div>

												<div class="form-group row">



													<label class="col-lg-3 control-label">
														<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_ROLL, loginDTO)%>
													</label>

													<div class="col-8" id = 'roll_div_<%=i%>'>

														<input type='text' class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.roll + "'"):("'" + "0" + "'")%>
																tag='pb_html'/>

													</div>


												</div>

												<div class="form-group row">

													<label class="col-lg-3 control-label">
														<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_STATUS, loginDTO)%>
													</label>

													<div class="col-8" id = 'status_div_<%=i%>'>

														<select class='form-control'  name='status' id = 'status_text_<%=i%>'>
															<%
																String selectLang=LM.getText(LC.LANGUAGE_SELECT,loginDTO);
																String statusDropDown="<option value=''>"+selectLang+"</option>";
																String selected="";
																for(Map.Entry<Integer, String> entry:AttendanceStatusMap.entrySet()){
																	String val = entry.getKey()==1?LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT,loginDTO):LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT,loginDTO) ;
																	if(entry.getKey()==recruitment_bulk_attendanceDTO.status){
																		statusDropDown += "<option value = '" + entry.getKey() + "' selected>" + val+"</option>";
																	}
																	else{
																		statusDropDown += "<option value = '" + entry.getKey() + "'>" + val+"</option>";
																	}
																	//statusDropDown += "<option value = '" + entry.getKey() + "'>" + val+ "</option>";
																	//actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.status + "'"):("'" + "0" + "'")

																}
															%>

															<%=statusDropDown%>

														</select>

													</div>


												</div>





												<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.id%>' tag='pb_html'/>
												<input type='hidden' class='form-control'  name='jobId' id = 'jobId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.jobId + "'"):("'" + "0" + "'")%>
														tag='pb_html'/>
												<input type='hidden' class='form-control'  name='levelId' id = 'levelId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.levelId + "'"):("'" + "0" + "'")%>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.insertionDate + "'"):("'" + "0" + "'")%>
															   tag='pb_html'/>


												<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.insertedBy + "'"):("'" + "" + "'")%>
														tag='pb_html'/>


												<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.modifiedBy + "'"):("'" + "" + "'")%>
														tag='pb_html'/>


												<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.isDeleted + "'"):("'" + "false" + "'")%>
														tag='pb_html'/>



												<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
														tag='pb_html'/>

											</div>


										</div>


									</div>

								</div>



							</div>
							<div class="col-md-1"></div>

						</div>

						<div class="mt-5">

							<div class="form-actions text-center">

								<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
									<i class="fas fa-window-close"></i>
									<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_RECRUITMENT_BULK_ATTENDANCE_CANCEL_BUTTON, loginDTO)%>
								</a>
								<button class="btn btn-success" type="submit">
									<i class="fas fa-save"></i>

									<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_RECRUITMENT_BULK_ATTENDANCE_SUBMIT_BUTTON, loginDTO)%>

								</button>

<%--								<button type="submit" class="btn btn-sm btn-success text-white shadow "--%>
<%--										id="download_excel" name="download_excel"><i class="fas fa-file-excel"></i><%=LM.getText(LC.CANDIDATE_LIST_DOWNLOAD_EXCEL, loginDTO) %></button>--%>


<%--								<button type="button" class="btn btn-sm submit-btn text-white shadow ml-2 " style="color: white;" id="loadFileXml"--%>
<%--										onclick="document.getElementById('testing_excelDatabase').click();" ><i class="fas fa-cloud-upload-alt"></i><%=LM.getText(LC.CANDIDATE_LIST_UPLOAD_EXCEL, loginDTO) %></button>--%>
<%--								<button type="button" class="btn btn-sm btn-danger text-white shadow" style="color: white;text-align:center;display: inline-block;"--%>
<%--										id="load_data" name="load_data" onclick="loadDataClicked()"><i class="fas fa-spinner"></i><%=LM.getText(LC.CANDIDATE_LIST_LOAD_DATA, loginDTO) %> </button>--%>


							</div>

						</div>



					</div>




				</form>


			</div>

		</div>



	</div>


</div>



<%--<div class="box box-primary">--%>
<%--	<div class="box-header with-border">--%>
<%--		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>--%>
<%--	</div>--%>
<%--	<div class="box-body">--%>
<%--		<form class="form-horizontal" action="Recruitment_bulk_attendanceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
<%--		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"--%>
<%--		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>
<%--			<div class="form-body">--%>




<%--		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.id%>' tag='pb_html'/>--%>



<%--<label class="col-lg-3 control-label">--%>
<%--&lt;%&ndash;	<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_JOBID, loginDTO)%>&ndash;%&gt;--%>
<%--</label>--%>
<%--<div class="form-group ">--%>
<%--	<div class="col-lg-6 " id = 'jobId_div_<%=i%>'>--%>
<%--&lt;%&ndash;		<input type='text' class='form-control'  name='jobId' id = 'jobId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.jobId + "'"):("'" + "0" + "'")%>&ndash;%&gt;--%>
<%--&lt;%&ndash;   tag='pb_html'/>					&ndash;%&gt;--%>
<%--	<input type='hidden' class='form-control'  name='jobId' id = 'jobId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.jobId + "'"):("'" + "0" + "'")%>--%>
<%--			tag='pb_html'/>--%>
<%--	</div>--%>
<%--</div>--%>


<%--<label class="col-lg-3 control-label">--%>
<%--&lt;%&ndash;	<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_LEVELID, loginDTO)%>&ndash;%&gt;--%>
<%--</label>--%>
<%--<div class="form-group ">--%>
<%--	<div class="col-lg-6 " id = 'levelId_div_<%=i%>'>--%>
<%--		<input type='hidden' class='form-control'  name='levelId' id = 'levelId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.levelId + "'"):("'" + "0" + "'")%>--%>
<%--   tag='pb_html'/>--%>
<%--	</div>--%>
<%--</div>--%>


<%--<label class="col-lg-3 control-label">--%>
<%--	<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_ROLL, loginDTO)%>--%>
<%--</label>--%>
<%--<div class="form-group ">--%>
<%--	<div class="col-lg-6 " id = 'roll_div_<%=i%>'>--%>
<%--		<input type='text' class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.roll + "'"):("'" + "0" + "'")%>--%>
<%--   tag='pb_html'/>--%>
<%--	</div>--%>
<%--</div>--%>


<%--<label class="col-lg-3 control-label">--%>
<%--	<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_STATUS, loginDTO)%>--%>
<%--</label>--%>
<%--<div class="form-group ">--%>
<%--	<div class="col-lg-6 " id = 'status_div_<%=i%>'>--%>
<%--&lt;%&ndash;		<input type='text' class='form-control'  name='status' id = 'status_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.status + "'"):("'" + "0" + "'")%>&ndash;%&gt;--%>
<%--&lt;%&ndash;   tag='pb_html'/>					&ndash;%&gt;--%>
<%--	<select class='form-control'  name='status' id = 'status_text_<%=i%>'>--%>
<%--		<%--%>
<%--			String selectLang=LM.getText(LC.LANGUAGE_SELECT,loginDTO);--%>
<%--			String statusDropDown="<option value=''>"+selectLang+"</option>";--%>
<%--			String selected="";--%>
<%--			for(Map.Entry<Integer, String> entry:AttendanceStatusMap.entrySet()){--%>
<%--				String val = entry.getKey()==1?LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT,loginDTO):LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT,loginDTO) ;--%>
<%--				if(entry.getKey()==recruitment_bulk_attendanceDTO.status){--%>
<%--					statusDropDown += "<option value = '" + entry.getKey() + "' selected>" + val+"</option>";--%>
<%--				}--%>
<%--				else{--%>
<%--					statusDropDown += "<option value = '" + entry.getKey() + "'>" + val+"</option>";--%>
<%--				}--%>
<%--				//statusDropDown += "<option value = '" + entry.getKey() + "'>" + val+ "</option>";--%>
<%--				//actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.status + "'"):("'" + "0" + "'")--%>

<%--			}--%>
<%--		%>--%>

<%--		<%=statusDropDown%>--%>

<%--	</select>--%>
<%--	</div>--%>
<%--</div>--%>


<%--		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.insertionDate + "'"):("'" + "0" + "'")%>--%>
<%-- tag='pb_html'/>--%>


<%--		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.insertedBy + "'"):("'" + "" + "'")%>--%>
<%-- tag='pb_html'/>--%>


<%--		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.modifiedBy + "'"):("'" + "" + "'")%>--%>
<%-- tag='pb_html'/>--%>


<%--		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.isDeleted + "'"):("'" + "false" + "'")%>--%>
<%-- tag='pb_html'/>--%>



<%--		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.lastModificationTime + "'"):("'" + "0" + "'")%>--%>
<%-- tag='pb_html'/>--%>









<%--				<div class="form-actions text-center">--%>
<%--					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">--%>
<%--						<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_RECRUITMENT_BULK_ATTENDANCE_CANCEL_BUTTON, loginDTO)%>--%>
<%--					</a>--%>
<%--					<button class="btn btn-success" type="submit">--%>

<%--						<%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_ADD_RECRUITMENT_BULK_ATTENDANCE_SUBMIT_BUTTON, loginDTO)%>--%>

<%--					</button>--%>
<%--				</div>--%>

<%--			</div>--%>

<%--		</form>--%>

<%--	</div>--%>
<%--</div>--%>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Recruitment_bulk_attendanceServlet");
}

function init(row)
{



}

var row = 0;

window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






