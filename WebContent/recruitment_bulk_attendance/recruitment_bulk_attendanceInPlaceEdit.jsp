<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_bulk_attendance.Recruitment_bulk_attendanceDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = (Recruitment_bulk_attendanceDTO)request.getAttribute("recruitment_bulk_attendanceDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(recruitment_bulk_attendanceDTO == null)
{
	recruitment_bulk_attendanceDTO = new Recruitment_bulk_attendanceDTO();
	
}
System.out.println("recruitment_bulk_attendanceDTO = " + recruitment_bulk_attendanceDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobId'>")%>
			
	
	<div class="form-inline" id = 'jobId_div_<%=i%>'>
		<input type='text' class='form-control'  name='jobId' id = 'jobId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.jobId + "'"):("'" + "0" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_levelId'>")%>
			
	
	<div class="form-inline" id = 'levelId_div_<%=i%>'>
		<input type='text' class='form-control'  name='levelId' id = 'levelId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.levelId + "'"):("'" + "0" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_roll'>")%>
			
	
	<div class="form-inline" id = 'roll_div_<%=i%>'>
		<input type='text' class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.roll + "'"):("'" + "0" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_status'>")%>
			
	
	<div class="form-inline" id = 'status_div_<%=i%>'>
		<input type='text' class='form-control'  name='status' id = 'status_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.status + "'"):("'" + "0" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Recruitment_bulk_attendanceServlet?actionType=view&ID=<%=recruitment_bulk_attendanceDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Recruitment_bulk_attendanceServlet?actionType=view&modal=1&ID=<%=recruitment_bulk_attendanceDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	