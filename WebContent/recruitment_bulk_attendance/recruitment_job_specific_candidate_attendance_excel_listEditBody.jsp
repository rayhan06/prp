﻿<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="java.util.Map" %>
<%@ page import="static sessionmanager.SessionConstants.excelUploadChoice" %>


<%


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);


    Recruitment_job_descriptionDAO jobDescriptionDAO = new Recruitment_job_descriptionDAO();
    Job_applicant_applicationDAO jobApplicantApplicationDAO = new Job_applicant_applicationDAO();

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date todayDate = new Date();

    int pagination_number = 0;
    RecordNavigator rn = new RecordNavigator();
    request.getSession().setAttribute(SessionConstants.NAV_JOB_APPLICANT_APPLICATION, rn);
    String context = "../../.." + request.getContextPath() + "/";

    String actionName;
    boolean isPermanentTable = true;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
    {
        actionName = "add";
    }
    else if (request.getParameter("actionType").equalsIgnoreCase("getCandidateAttendanceExcelList"))
    {
        actionName = "DownloadTemplate";
    }
    else
    {
        actionName = "edit";
    }
    long ColumnID;
    int i=0;

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important; margin-bottom: -18px">


    <div class="row">


        <div class="col-lg-12">

            <div class="kt-portlet">

                <div class="kt-portlet__head">

                    <div class="kt-portlet__head-label">

                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;

                            <%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_UPLOAD_UPLOAD, loginDTO)%>

                        </h3>

                    </div>

                </div>

                <form id = "candidate-form" name = "candidate-form" method="POST" enctype = "multipart/form-data" action="Recruitment_bulk_attendanceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>" >


                    <div class="kt-portlet__body form-body">

                        <div class="row">

                            <div class="col-md-8 offset-md-2">

                                <div class="onlyborder">

                                    <div class="row">

                                        <div class="col-md-10">

                                            <div class="sub_title_top">

                                                <div class="sub_title">
                                                    <h4 style="background: white">

                                                        <%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_UPLOAD_UPLOAD, loginDTO)%>

                                                    </h4>
                                                </div>

                                                <div class="form-group row">



                                                    <label class="col-sm-3 control-label text-right">
                                                        <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>
                                                    </label>

                                                    <div class="col-sm-9">

                                                        <select onchange="statusChange()" class='form-control'  name='status' id = 'status'   tag='pb_html'>

                                                            <%=jobDescriptionDAO.getStatusList(Language)%>
                                                        </select>

                                                    </div>


                                                </div>

                                                <div class="form-group row">

                                                    <label class="col-sm-3 control-label text-right">
                                                        <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>
                                                    </label>

                                                    <div class="col-sm-9" id = 'job_div'>

                                                        <select onchange="onJobChange()" class='form-control'  name='job' id = 'job'   tag='pb_html'>

                                                            <%=jobDescriptionDAO.getJobListWithoutInternship( "ALL", Language)%>
                                                        </select>

                                                    </div>


                                                </div>


                                                <div class="form-group row">

                                                    <label class="col-sm-3 control-label text-right">
                                                        <%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%>
                                                    </label>

                                                    <div class="col-sm-9">

                                                        <select onchange="onLevelChange()" class='form-control'  name='level' id = 'level'   tag='pb_html'>


                                                        </select>

                                                    </div>


                                                </div>

                                                <div class="form-group row" id="excelUploadChoice" >

                                                    <label class="col-sm-3 control-label text-right">
                                                        <%=LM.getText(LC.CANDIDATE_LIST_EXCEL_UPLOAD_DECISION, loginDTO)%>
                                                    </label>

                                                    <div class="col-sm-9">

                                                        <select onchange="exlUpChoiceClick(this)" class='form-control'  name='exlUpChoice' id = 'exlUpChoice'  tag='pb_html'>


                                                            <%
                                                                String exlChoiceDropDown = "";
                                                                if (Language.equals("English")) {
                                                                    exlChoiceDropDown = "<option value = ''>Select</option>";
                                                                } else {
                                                                    exlChoiceDropDown = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";
                                                                }
                                                                for (Map.Entry<Integer, String> entry : excelUploadChoice.entrySet()) {
                                                                    String text = entry.getKey()==1?LM.getText(LC.CANDIDATE_LIST_YES,loginDTO):LM.getText(LC.CANDIDATE_LIST_NO,loginDTO);
                                                                    exlChoiceDropDown=exlChoiceDropDown+"<option value="+entry.getKey()+">"+text+"</option>";
                                                                }
                                                            %>
                                                            <%=exlChoiceDropDown%>
                                                        </select>

                                                    </div>


                                                </div>


<%--                                                <div class="form-group row" id="rejectedDropDown" style="display:none;">--%>

<%--                                                    <label class="col-lg-3 control-label">--%>
<%--                                                        <%=LM.getText(LC.CANDIDATE_LIST_APPROVE_STATUS, loginDTO)%>--%>
<%--                                                    </label>--%>

<%--                                                    <div class="col-8">--%>

<%--                                                        <select onchange="" class='form-control'  name='approve' id = 'approve'   tag='pb_html'>--%>


<%--                                                            <%String[] rejectedDropDown = jobApplicantApplicationDAO.getApproveList(Language).split("</option>");--%>
<%--                                                                String dropDown="";--%>
<%--                                                                for(int k=1;k<rejectedDropDown.length;k++){--%>
<%--                                                                    dropDown = dropDown+ rejectedDropDown[k]+  "</option>";--%>
<%--                                                                }--%>
<%--                                                            %>--%>
<%--                                                            <%=dropDown%>--%>
<%--                                                        </select>--%>

<%--                                                    </div>--%>


<%--                                                </div>--%>



                                                <input type="hidden" id="job_name" name="job_name" value="" />
                                                <input type="hidden" id="level_name" name="level_name" value="" />

                                            </div>


                                        </div>


                                    </div>

                                </div>



                            </div>
                            

                        </div>

                        <div class="my-5">

                            <div class="form-actions text-center">

                                <button type="submit" class="btn btn-sm btn-success text-white shadow "
                                        id="download_excel" name="download_excel" style="border-radius: 8px;"><i class="fas fa-file-excel"></i><%=LM.getText(LC.CANDIDATE_LIST_DOWNLOAD_EXCEL, loginDTO) %></button>
                                <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>

                                <button type="button" class="btn btn-sm submit-btn text-white shadow ml-2 " style="color: white;border-radius: 8px;" id="loadFileXml"
                                        onclick="document.getElementById('testing_excelDatabase').click();" ><i class="fas fa-cloud-upload-alt"></i><%=LM.getText(LC.CANDIDATE_LIST_UPLOAD_EXCEL, loginDTO) %></button>
                                <button type="button" class="btn btn-sm btn-danger text-white shadow" style="color: white;text-align:center;display: inline-block;border-radius: 8px;"
                                         id="load_data" name="load_data" onclick="loadDataClicked()"><i class="fas fa-spinner"></i><%=LM.getText(LC.CANDIDATE_LIST_LOAD_DATA, loginDTO) %> </button>


                            </div>
                            <div class="dropzonesss" style="display: inline-block;margin-left: 40%;margin-top: 10px;">


                                <input type='file' style="display:none;" class='form-control'  name='testing_excelDatabase' id = 'testing_excelDatabase' />

                            </div>
                        </div>



                    </div>




                </form>

                <form class="form-horizontal" action="Recruitment_bulk_attendanceServlet?actionType=uploadConfirmed"
                      id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'upload')">
                    <div id='reviewDiv'>
                    </div>

                    <div class="my-5">

                        <div class="form-actions text-center">
                            <button class='btn btn-sm submit-btn text-white shadow ml-2 ' type="submit" style="display:none;margin-bottom:10px;" id="submitButton">
                                <i class="fas fa-cloud-upload-alt"></i>
                                <%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_UPLOAD_UPLOAD, loginDTO)%>
                            </button>
                        </div></div>




                </form>


            </div>

        </div>



    </div>


</div>



<%--<div class="box box-primary">--%>
<%--    <div class="box-header with-border">--%>
<%--        <h3 class="box-title"><i class="fa fa-gift"></i>--%>
<%--            <%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_UPLOAD_UPLOAD, loginDTO)%>--%>
<%--        </h3>--%>

<%--    </div>--%>

<%--    <div class="box-body">--%>
<%--        <form id = "candidate-form" name = "candidate-form" method="POST" enctype = "multipart/form-data" action="Recruitment_bulk_attendanceServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>" >--%>
<%--            <div class="form-body">--%>
<%--                <div class="form-row">--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_STATUS, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="statusChange()" class='form-control'  name='status' id = 'status'   tag='pb_html'>--%>

<%--                                    <%=jobDescriptionDAO.getStatusList(Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>


<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " id = 'job_div'>--%>
<%--                                <select onchange="onJobChange()" class='form-control'  name='job' id = 'job'   tag='pb_html'>--%>

<%--                                    <%=jobDescriptionDAO.getJobListWithoutInternship( "ALL", Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>




<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="onLevelChange()" class='form-control'  name='level' id = 'level'   tag='pb_html'>--%>


<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>

<%--                    <div class="form-group col-md-4" id="excelUploadChoice" >--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                            <%=LM.getText(LC.CANDIDATE_LIST_EXCEL_UPLOAD_DECISION, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group " >--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="exlUpChoiceClick(this)" class='form-control'  name='exlUpChoice' id = 'exlUpChoice'  tag='pb_html'>--%>


<%--                                    <%--%>
<%--                                        String exlChoiceDropDown = "";--%>
<%--                                        if (Language.equals("English")) {--%>
<%--                                            exlChoiceDropDown = "<option value = ''>Select</option>";--%>
<%--                                        } else {--%>
<%--                                            exlChoiceDropDown = "<option value = ''>অনুগ্রহ করে নির্বাচন করুন</option>";--%>
<%--                                        }--%>
<%--                                        for (Map.Entry<Integer, String> entry : excelUploadChoice.entrySet()) {--%>
<%--                                            String text = entry.getKey()==1?LM.getText(LC.CANDIDATE_LIST_YES,loginDTO):LM.getText(LC.CANDIDATE_LIST_NO,loginDTO);--%>
<%--                                            exlChoiceDropDown=exlChoiceDropDown+"<option value="+entry.getKey()+">"+text+"</option>";--%>
<%--                                        }--%>
<%--                                    %>--%>
<%--                                    <%=exlChoiceDropDown%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>

<%--                </div>--%>

<%--                <!--<div class="form-row">--%>
<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_VIEW_STATUS, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " id = ''>--%>
<%--                                <select onchange="" class='form-control'  name='view' id = 'view'   tag='pb_html'>--%>

<%--                                    <%=jobApplicantApplicationDAO.getViewList(Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>
<%--                    <div class="form-group col-md-4">--%>
<%--                        <label class="col-lg-3 control-label" style="font-size: 12px; font-weight: bold">--%>
<%--                            &lt;%&ndash;                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>&ndash;%&gt;--%>
<%--                                <%=LM.getText(LC.CANDIDATE_LIST_APPROVE_STATUS, loginDTO)%>--%>
<%--                            &lt;%&ndash;                        <span class="required"> * </span>&ndash;%&gt;--%>
<%--                        </label>--%>
<%--                        <div class="form-group ">--%>
<%--                            <div class="col-lg-9 " >--%>
<%--                                <select onchange="" class='form-control'  name='approve' id = 'approve'   tag='pb_html'>--%>

<%--                                    <%=jobApplicantApplicationDAO.getApproveList(Language)%>--%>
<%--                                </select>--%>

<%--                            </div>--%>
<%--                        </div>--%>


<%--                    </div>--%>
<%--                </div> -->--%>
<%--                </br></br>--%>
<%--                <input type="hidden" id="job_name" name="job_name" value="" />--%>
<%--                <input type="hidden" id="level_name" name="level_name" value="" />--%>

<%--                <center><input type="submit" class="btn-info" style="color: white;text-align:center;display: inline-block;float: center;" value="<%=LM.getText(LC.CANDIDATE_LIST_DOWNLOAD_EXCEL, loginDTO) %>" id="download_excel" name="download_excel">--%>

<%--                    <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>--%>
<%--                    <div class="dropzonesss" style="display: inline-block;float: center;">--%>
<%--                        <!--<input type='file' style="display:none" name='filesDropzoneFile'--%>
<%--                               id='filesDropzone_dropzone_File_<%=i%>' tag='pb_html'/> -->--%>
<%--                        <input type='file' style="display:none" class='form-control'  name='testing_excelDatabase' id = 'testing_excelDatabase' />--%>
<%--                        <!--<input type="button" class="btn-success" style="color: white;" id="loadFileXml" value="<%=LM.getText(LC.CANDIDATE_LIST_UPLOAD_EXCEL, loginDTO) %>"  onclick="document.getElementById('excelUploadChoice').style.display='block';" />-->--%>
<%--                        <input type="button" class="btn-success" style="color: white;" id="loadFileXml" value="<%=LM.getText(LC.CANDIDATE_LIST_UPLOAD_EXCEL, loginDTO) %>"  onclick="document.getElementById('testing_excelDatabase').click();" />--%>


<%--                    </div>--%>
<%--                    <input type="button" class="btn-danger" style="color: white;text-align:center;display: inline-block;float: center;" value="<%=LM.getText(LC.CANDIDATE_LIST_LOAD_DATA, loginDTO) %>" id="load_data" name="load_data" onclick="loadDataClicked()">--%>
<%--                <!--<input type='file' class='form-control'  name='testing_excelDatabase' id = 'testing_excelDatabase' />--%>
<%--                -->--%>

<%--                <!--<a class="btn btn-success" onclick = "uploadFile();">--%>
<%--                    --%>
<%--                </a> -->   </center>--%>

<%--                    <!--<input type="submit" style="position: fixed;--%>
<%--	  top: 50%;--%>
<%--	  left: 50%;--%>
<%--	  margin-top: -50px;--%>
<%--	  margin-left: 80px;color: white;background: green;" value="Upload Excel" name="upload_excel"> -->--%>

<%--            </div>--%>
<%--        </form>--%>


<%--        <br>--%>

<%--        <div id = 'total-item-count-div' style="display: none">--%>

<%--            <%=LM.getText(LC.CANDIDATE_LIST_TOTAL, loginDTO)%>: <label id="total-count"></label>,--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_VIEWED, loginDTO)%>: <label id="view-count"></label>,--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_SHORTLISTED, loginDTO)%>: <label id="shortlisted-count"></label>,--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_REJECTED, loginDTO)%>: <label id="rejected-count"></label>--%>

<%--        </div>--%>

<%--        <div id = 'data-div-parent' style="display: none">--%>
<%--            <div id = 'data-div' >--%>

<%--            </div>--%>
<%--            <% pagination_number = 1;%>--%>
<%--            <%@include file="../common/pagination_with_go2.jsp"%>--%>

<%--        </div>--%>



<%--    </div>--%>
<%--</div>--%>

<%--<form class="form-horizontal" action="Recruitment_bulk_attendanceServlet?actionType=uploadConfirmed"--%>
<%--      id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"--%>
<%--      onsubmit="return PreprocessBeforeSubmiting(0,'upload')">--%>
<%--    <div id='reviewDiv'>--%>
<%--    </div>--%>

<%--    <button class='btn btn-success' type="submit" style="display:none" id="submitButton">--%>
<%--        <%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_UPLOAD_UPLOAD, loginDTO)%>--%>
<%--    </button>--%>


<%--</form>--%>

<style>

    .loader {
        margin: 100px auto;
        font-size: 25px;
        width: 1em;
        height: 1em;
        border-radius: 50%;
        position: relative;
        text-indent: -9999em;
        -webkit-animation: load5 1.1s infinite ease;
        animation: load5 1.1s infinite ease;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
    }
    @-webkit-keyframes load5 {
        0%,
        100% {
            box-shadow: 0em -2.6em 0em 0em #2e2929, 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.5), -1.8em -1.8em 0 0em rgba(46,41,41, 0.7);
        }
        12.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.7), 1.8em -1.8em 0 0em #2e2929, 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.5);
        }
        25% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.5), 1.8em -1.8em 0 0em rgba(46,41,41, 0.7), 2.5em 0em 0 0em #2e2929, 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        37.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.5), 2.5em 0em 0 0em rgba(46,41,41, 0.7), 1.75em 1.75em 0 0em #2e2929, 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        50% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.5), 1.75em 1.75em 0 0em rgba(46,41,41, 0.7), 0em 2.5em 0 0em #2e2929, -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        62.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.5), 0em 2.5em 0 0em rgba(46,41,41, 0.7), -1.8em 1.8em 0 0em #2e2929, -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        75% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.5), -1.8em 1.8em 0 0em rgba(46,41,41, 0.7), -2.6em 0em 0 0em #2e2929, -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        87.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.5), -2.6em 0em 0 0em rgba(46,41,41, 0.7), -1.8em -1.8em 0 0em #2e2929;
        }
    }
    @keyframes load5 {
        0%,
        100% {
            box-shadow: 0em -2.6em 0em 0em #2e2929, 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.5), -1.8em -1.8em 0 0em rgba(46,41,41, 0.7);
        }
        12.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.7), 1.8em -1.8em 0 0em #2e2929, 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.5);
        }
        25% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.5), 1.8em -1.8em 0 0em rgba(46,41,41, 0.7), 2.5em 0em 0 0em #2e2929, 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        37.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.5), 2.5em 0em 0 0em rgba(46,41,41, 0.7), 1.75em 1.75em 0 0em #2e2929, 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        50% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.5), 1.75em 1.75em 0 0em rgba(46,41,41, 0.7), 0em 2.5em 0 0em #2e2929, -1.8em 1.8em 0 0em rgba(46,41,41, 0.2), -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        62.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.5), 0em 2.5em 0 0em rgba(46,41,41, 0.7), -1.8em 1.8em 0 0em #2e2929, -2.6em 0em 0 0em rgba(46,41,41, 0.2), -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        75% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.5), -1.8em 1.8em 0 0em rgba(46,41,41, 0.7), -2.6em 0em 0 0em #2e2929, -1.8em -1.8em 0 0em rgba(46,41,41, 0.2);
        }
        87.5% {
            box-shadow: 0em -2.6em 0em 0em rgba(46,41,41, 0.2), 1.8em -1.8em 0 0em rgba(46,41,41, 0.2), 2.5em 0em 0 0em rgba(46,41,41, 0.2), 1.75em 1.75em 0 0em rgba(46,41,41, 0.2), 0em 2.5em 0 0em rgba(46,41,41, 0.2), -1.8em 1.8em 0 0em rgba(46,41,41, 0.5), -2.6em 0em 0 0em rgba(46,41,41, 0.7), -1.8em -1.8em 0 0em #2e2929;
        }
    }


</style>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">

    let language = '<%=Language%>';

    $(document).ready(function () {

        $("#loadFileXml").hide();
        $("#download_excel").hide();
        $("#load_data").hide();

        CheckRequiredFields();

    });


    $(document).ready(function () {


        $('#testing_excelDatabase').on("change", function(){ uploadFile(); });
    });

    function loadDataClicked(){
        var job = document.getElementById("job").value;
        var level = document.getElementById("level").value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                toastr.success("Success");
                document.getElementById('reviewDiv').innerHTML = this.responseText ;
                document.getElementById('submitButton').style = "display:inline" ;
                ShowExcelParsingResult(i);
            }
            else{
                //toastr.error("Failure");
            }
        };
        xhttp.open("POST", "Recruitment_bulk_attendanceServlet?actionType=excelReviewWithoutUpload&job_id="+job+"&level_id="+level, true);
        xhttp.send();

    }

    function exlUpChoiceClick(nameSelect){
        var job = document.getElementById("job").value;
        var level = document.getElementById("level").value;
        CheckRequiredFields();
        //console.log("job: "+job+" level: "+level);
        // if(nameSelect.value==1){
        //     CheckRequiredFields();
        //     //document.getElementById('testing_excelDatabase').click();
        // }
        // else if(nameSelect.value==2){
        //     var xhttp = new XMLHttpRequest();
        //     xhttp.onreadystatechange = function() {
        //         if (this.readyState == 4 && this.status == 200) {
        //             toastr.success("Success");
        //             document.getElementById('reviewDiv').innerHTML = this.responseText ;
        //             document.getElementById('submitButton').style = "display:inline" ;
        //             ShowExcelParsingResult(i);
        //         }
        //         else{
        //             //toastr.error("Failure");
        //         }
        //     };
        //     xhttp.open("POST", "Recruitment_bulk_attendanceServlet?actionType=excelReviewWithoutUpload&job_id="+job+"&level_id="+level, true);
        //     xhttp.send();
        // }
    }

    function CheckRequiredFields(){
        var status = document.getElementById("status").value;
        var job = document.getElementById("job").value;
        var level = document.getElementById("level").value;
        var excelUpChoice = document.getElementById("exlUpChoice").value;
        document.getElementById('reviewDiv').innerHTML = "" ;
        document.getElementById('submitButton').style = "display:none" ;



        //console.log("status: "+status+" job: "+job+" level: "+level);

        if(status=="" || job=="" || level=="" || excelUpChoice==""){
            //$('#loadFileXml').attr('disabled','disabled');
            //$('#download_excel').attr('disabled','disabled');
            $("#loadFileXml").hide();
            $("#download_excel").hide();
            $("#load_data").hide();


        }
        else{

            if(excelUpChoice==1||excelUpChoice==2){

                $("#loadFileXml").hide();
                $("#download_excel").hide();
                $("#load_data").hide();
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        //console.log("response from 2: "+this.responseText);

                        var response = this.responseText.split(",");
                        var download_allowed = response[0];
                        var upload_allowed = response[1];

                        //console.log("download_allowed: "+download_allowed);
                        //console.log("upload_allowed: "+upload_allowed);

                        if(download_allowed==1 && excelUpChoice==1){

                            //$('#download_excel').removeAttr('disabled','disabled');
                            $("#load_data").hide();
                            $("#download_excel").show();
                            var job_name = $("#job>option:selected").text();
                            var level_name = $("#level>option:selected").text();
                            document.getElementById("job_name").value   =  job_name;
                            document.getElementById("level_name").value   =  level_name;

                            if(upload_allowed==1 && excelUpChoice==1){
                                //$('#loadFileXml').removeAttr('disabled');
                                toastr.success("Data found");
                                $("#loadFileXml").show();
                            }
                            else{
                                //$('#loadFileXml').attr('disabled','disabled');
                                toastr.error("Already uploaded");
                                $("#loadFileXml").hide();
                                //$("#download_excel").hide();
                            }

                        }
                        else if(upload_allowed==1 && excelUpChoice==2 && download_allowed==1){
                            //$('#loadFileXml').attr('disabled','disabled');
                            //$('#download_excel').attr('disabled','disabled');
                            //document.getElementById('testing_excelDatabase').innerHTML = "" ;
                            toastr.success("Data found");
                            $("#loadFileXml").hide();
                            $("#download_excel").hide();
                            $("#load_data").show();
                        }
                        else{
                            toastr.error("No data found");
                        }
                    }
                };
                xhttp.open("POST", "Recruitment_bulk_attendanceServlet?actionType=checkJobLevelExistence&job_id="+job+"&level_id="+level, true);
                xhttp.send();

            }

            else{
                $("#loadFileXml").hide();
                $("#download_excel").hide();
                $("#load_data").hide();
                toastr.error("No data found");
            }






        }
    }

    function uploadFile()
    {
        console.log('submitAjax called');

        var formData = new FormData();
        var value;
        var job_id = document.getElementById('job').value;
        var level_id = document.getElementById('level').value;
        //console.log("job_id: "+job_id);

        console.log('uploadFile called');

        formData.append('testing_excelDatabase', document.getElementById('testing_excelDatabase').files[0]);


        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function()
        {
            document.getElementById("reviewDiv").classList.add('loader');
            if (this.readyState == 4 && this.status == 200)
            {
                if(this.responseText !='')
                {
                    document.getElementById("reviewDiv").classList.remove('loader');
                    document.getElementById('reviewDiv').innerHTML = this.responseText ;
                    document.getElementById('submitButton').style = "display:inline" ;
                    ShowExcelParsingResult(i);
                }
                else
                {
                    document.getElementById("reviewDiv").classList.remove('loader');
                    console.log("No Response");
                    document.getElementById('reviewDiv').innerHTML = this.responseText ;
                }
            }
            else if(this.readyState == 4 && this.status != 200)
            {
                document.getElementById("reviewDiv").classList.remove('loader');
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", 'Recruitment_bulk_attendanceServlet?actionType=upload&job_id='+job_id+'&level_id='+level_id, true);
        xhttp.send(formData);
    }

    function convertToBn(value){
        if(language == 'English'){
            return value;
        }

        return toBn(value);
    }

    function showItemCountDiv(total, view, select, reject){
        document.getElementById('total-count').innerText = convertToBn(total);
        document.getElementById('view-count').innerText = convertToBn(view);
        document.getElementById('shortlisted-count').innerText = convertToBn(select);
        document.getElementById('rejected-count').innerText = convertToBn(reject);
        document.getElementById('total-item-count-div').style.display = 'block';
    }

    function hideItemCountDiv(){
        document.getElementById('total-item-count-div').style.display = 'none';
    }

    function statusChange(){
        hideLoadButton();
        //CheckRequiredFields();
        let statusValue = document.getElementById('status').value;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('job').innerHTML = this.responseText;
                CheckRequiredFields();


            }
            else if (this.readyState == 4 && this.status != 200) {
                CheckRequiredFields();
            }
        };

        let params = "Recruitment_job_descriptionServlet?actionType=getJobListByStatusWithoutInternship";
        params = params + "&language=" + language + "&status=" + statusValue;
        xhttp.open("Get", params, true);
        xhttp.send();
    }

    function showLoadButton(){
        //document.getElementById('submit-button-div').style.display = 'block';
    }

    function hideLoadButton(){
        //document.getElementById('submit-button-div').style.display = 'none';
    }

    function showDataDiv(){
        //document.getElementById('data-div-parent').style.display = 'block';
    }

    function hideDataDiv(){
        //document.getElementById('data-div-parent').style.display = 'none';
    }

    function onJobChange(){
        let jobValue = document.getElementById('job').value ;
        CheckRequiredFields();
        //console.log("jobValue: "+jobValue);
        if(jobValue && jobValue.length > 0){
            showLoadButton();
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('level').innerHTML = this.responseText;
                    CheckRequiredFields();
                }
                else if (this.readyState == 4 && this.status != 200) {
                    CheckRequiredFields();
                }
            };

            let params = "Recruitment_job_descriptionServlet?actionType=getExamTypesByJobId";
            params = params + "&language=" + language + "&jobId=" + jobValue;

            //console.log("params: "+params);
            //let params = "CANDIDATE_LIST_LEVEL?actionType=getExamTypesByJobId";
            //params = params + "&language=" + language + "&jobId=" + jobValue;
            xhttp.open("Get", params, true);
            xhttp.send();

        } else {
            hideLoadButton();
            document.getElementById('level').innerHTML = '';
        }
    }

    function onLevelChange(){
        CheckRequiredFields();
        let job_id = document.getElementById('job').value ;
        let level_id = document.getElementById('level').value ;
        if(job_id && job_id.length > 0){

            // let xhttp = new XMLHttpRequest();
            // xhttp.onreadystatechange = function () {
            //     if (this.readyState == 4 && this.status == 200) {
            //          if(this.responseText==1){
            //
            //              //$('#loadFileXml').attr('disabled','disabled');
            //              //toastr.error('Upload excel button disabled');
            //          }
            //          else{
            //              //CheckRequiredFields();
            //              //$('#loadFileXml').removeAttr('disabled');
            //              //toastr.success('Upload excel button enabled');
            //          }
            //     }
            //     else if (this.readyState == 4 && this.status != 200) {
            //     }
            // };
            //
            // let params = "Recruitment_bulk_attendanceServlet?actionType=CheckExistenceJobLevel";
            // params = params + "&language=" + language + "&job_id=" + job_id;
            // params = params +  "&level_id=" + level_id;



        } else {
            hideLoadButton();
            document.getElementById('level').innerHTML = '';
        }
    }

    function getAllDataCount(){
        let jobValue = document.getElementById('job').value;
        let levelValue = document.getElementById('level').value;

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let responseCount = JSON.parse(this.responseText);
                showItemCountDiv
                (responseCount.totalCount, responseCount.viewCount, responseCount.selectedCount, responseCount.rejectedCount);

            }
            else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params =  "&jobId=" + jobValue + "&level=" + levelValue;
        let prefixParam = "RecruitmentJobSpecificCandidateListServlet?actionType=getAllDataCount" + params;

        xhttp.open("Get", prefixParam, true);
        xhttp.send();

    }

    function onSubmit(params){
        hideItemCountDiv();
        let jobValue = document.getElementById('job').value;
        let statusValue = document.getElementById('status').value;
        let levelValue = document.getElementById('level').value;
        let viewValue = document.getElementById('view').value;
        let approveValue = document.getElementById('approve').value;
        // console.log({jobValue, statusValue, levelValue, viewValue, approveValue})

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('data-div').innerHTML = this.responseText;
                setPageNo();
                showDataDiv();
                getAllDataCount();

            }
            else if (this.readyState == 4 && this.status != 200) {
            }
        };

        params = params + "&jobId=" + jobValue
                        + "&view=" + viewValue
                        + "&approve=" + approveValue
                        + "&level=" + levelValue;
        let prefixParam = "RecruitmentJobSpecificCandidateListServlet?actionType=getData" + params;
        // console.log(prefixParam)

        xhttp.open("Get", prefixParam, true);
        xhttp.send();
    }

    function allfield_changed(go, pagination_number)
    {
        // console.log('came here')


        let params =  '&search=true&ajax=true';

        // var extraParams = document.getElementsByName('extraParam');
        // extraParams.forEach((param) => {
        //     params += "&" + param.getAttribute("tag") + "=" + param.value;
        // })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if(document.getElementById('hidden_totalrecords'))
        {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if(go !== '' && searchChanged == 0)
        {
            // console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[0].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        // console.log(params)
        onSubmit(params);

    }

    function shortList(selectedId){
        event.preventDefault();
        let remarksLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REMARKS, loginDTO)%>' + ":";
        let saveLabel = '<%=LM.getText(LC.CANDIDATE_LIST_YES, loginDTO)%>';
        let cancelLabel = '<%=LM.getText(LC.CANDIDATE_LIST_NO, loginDTO)%>';
        let headingLabel = '<%=LM.getText(LC.CANDIDATE_LIST_SHOTLIST_REMARKS, loginDTO)%>';

        bootbox.dialog({
            message: "<div class='row'>" +
                "<div class='col-lg-2'>"+ remarksLabel +"</div>" +
                "<textarea class='col-lg-8' style='border: 1px solid' id = 'shortlist-remarks'></textarea>" +
                "</div>",
            title: headingLabel,
            closeButton: false,
            buttons: [
                {
                    label: saveLabel,
                    className: "btn-success",
                    callback: function() {
                        let remarks =  $('#shortlist-remarks').val();
                        makeShortlisted(selectedId, remarks);
                    }
                },
                {
                    label: cancelLabel,
                    className: "btn-danger",
                    callback: function() {
                        // console.log( $('#shortlist-remarks').val());
                        // console.log('nothing happened')
                    }
                }
            ]
        });
    }

    function reject(selectedId){
        event.preventDefault();
        let remarksLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REMARKS, loginDTO)%>' + ":";
        let saveLabel = '<%=LM.getText(LC.CANDIDATE_LIST_YES, loginDTO)%>';
        let cancelLabel = '<%=LM.getText(LC.CANDIDATE_LIST_NO, loginDTO)%>';
        let headingLabel = '<%=LM.getText(LC.CANDIDATE_LIST_REJECT_REMARKS, loginDTO)%>';

        bootbox.dialog({
            message: "<div class='row'>" +
                "<div class='col-lg-2'>"+ remarksLabel +"</div>" +
                "<textarea class='col-lg-8' style='border: 1px solid' id = 'reject-remarks'></textarea>" +
                "</div>",
            title: headingLabel,
            closeButton: false,
            buttons: [
                {
                    label: saveLabel,
                    className: "btn-success",
                    callback: function() {
                        let remarks =  $('#reject-remarks').val();
                        makeRejected(selectedId, remarks);
                    }
                },
                {
                    label: cancelLabel,
                    className: "btn-danger",
                    callback: function() {
                        // console.log( $('#shortlist-remarks').val());
                        // console.log('nothing happened')
                    }
                }
            ]
        });

    }

    function makeShortlisted(id, remarks){
        let formData = new FormData();
        formData.append('id', id);
        formData.append('remarks', remarks);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");
                toastr.success("Success");
                onSubmit('');
            }
            else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeShortlist";
        xhttp.open("POST",params, true);
        xhttp.send(formData);
    }

    function makeRejected(id, remarks){
        let formData = new FormData();
        formData.append('id', id);
        formData.append('remarks', remarks);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");
                toastr.success("Success");
                onSubmit('');
            }
            else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeRejected";
        xhttp.open("POST",params, true);
        xhttp.send(formData);
    }

    function makeViewed(id){
        let formData = new FormData();
        formData.append('id', id);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                // alert("updated");

            }
            else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=makeViewed";
        xhttp.open("POST",params, true);
        xhttp.send(formData);
    }

    let numberBangla = {
        '0' :'০',
        '1' :'১',
        '2': '২',
        '3' : '৩',
        '4' :'৪',
        '5': '৫',
        '6' : '৬',
        '7':'৭',
        '8' : '৮',
        '9' : '৯'
    };

    function toBn(retStr){
        retStr = retStr + "";
        for ( let x in numberBangla) {
            retStr = retStr.replace(new RegExp(x, 'g'),
                numberBangla[x]);
        }
        return retStr;
    }
    function PreprocessBeforeSubmiting(row, validate)
    {
        if(validate == "report")
        {
        }
        else
        {
            var empty_fields = "";
            var i = 0;


            if(empty_fields != "")
            {
                if(validate == "inplaceedit")
                {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }

</script>
