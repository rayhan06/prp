
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_bulk_attendance.Recruitment_bulk_attendanceDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.text.SimpleDateFormat"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="java.util.Map" %>
<%@ page import="static sessionmanager.SessionConstants.AttendanceStatusMap" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.*" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_LANGUAGE, loginDTO);


String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
String parentTableName = "job_applicant_application";
Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO(parentTableName);
%>

<div class="row">
	<div class="col-lg-12">
		<div class="kt-portlet shadow-none" style="margin-top: -20px">

			<div class="kt-portlet__body form-body">

<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<!--<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_JOBID, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_LEVELID, loginDTO)%></th> -->
								<th><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_IDENTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_ROLL, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_STATUS, loginDTO)%></th>
															
							</tr>
						</thead>
						<tbody>
						<%
								ArrayList data = (ArrayList) session.getAttribute("recruitment_bulk_attendanceDTOs");

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = job_applicant_applicationDAO.getIDDTOsByJobLevelID(Integer.parseInt(request.getParameter("job_id")),Integer.parseInt(request.getParameter("level_id")));
										for (int i = 0; i < size; i++) 
										{
											System.out.println("In jsp, parsed dto = " + data.get(i));
											Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO =  (Recruitment_bulk_attendanceDTO)data.get(i);
											//Job_applicant_applicationDTO job_applicant_applicationDTO =  job_applicant_applicationDAO.getDTOByJobIdAndRollNumber(Long.parseLong(request.getParameter("job_id")), String.valueOf(Math.round(recruitment_bulk_attendanceDTO.roll)));
											Job_applicant_applicationDTO job_applicant_applicationDTO =  UtilCharacter.getJobApplicantApplicationDTO(job_applicant_applicationDTOS,String.valueOf(Math.round(recruitment_bulk_attendanceDTO.roll)));
											long  ID = recruitment_bulk_attendanceDTO.iD;
											out.println("<tr id = 'tr_" + i + "'>");
						%>












































			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>

<%=("<td id = '" + i + "_jobId' style='display:none;'>")%>


	<div class="form-inline" id = 'jobId_div_<%=i%>'>
		<input type='hidden' class='form-control'  name='jobId' id = 'jobId_text_<%=i%>' value=<%=request.getParameter("job_id")%>
   tag='pb_html'/>
	</div>

						<%System.out.println("IN REV JOB_ID: "+request.getParameter("job_id"));%>
						<%System.out.println("IN REV JOB_ID: "+request.getParameter("level_id"));%>

<%=("</td>")%>

<%=("<td id = '" + i + "_levelId' style='display:none;'>")%>


	<div class="form-inline" id = 'levelId_div_<%=i%>'>
		<input type='hidden' class='form-control'  name='levelId' id = 'levelId_text_<%=i%>' value=<%=request.getParameter("level_id")%>
   tag='pb_html'/>
	</div>

<%=("</td>")%>

<%=("<td id = '" + i + "_identity'>")%>


<div class="form-inline" id = 'candidateName_div_<%=i%>'>
	<input type='text' style="width:100%;" class='form-control' id = 'candidateName_text<%=i%>' value=<%=actionName.equals("edit")?("'" +UtilCharacter.getDataByLanguage(Language,job_applicant_applicationDTO.applicant_name_bn+", "+job_applicant_applicationDTO.father_name,job_applicant_applicationDTO.applicant_name_en+", "+job_applicant_applicationDTO.father_name)  + "'"):("'" + "0" + "'")%>
			tag='pb_html'/>
</div>

<%=("</td>")%>
			
<%=("<td id = '" + i + "_roll'>")%>
			
	
	<div class="form-inline" id = 'roll_div_<%=i%>'>
<%--		<input type='text' class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" +Utils.getDigits(Math.round(recruitment_bulk_attendanceDTO.roll), Language)  + "'"):("'" + "0" + "'")%>--%>
<%--   tag='pb_html'/>--%>
	<input type='text' style="width:100%;" class='form-control'  name='roll' id = 'roll_text_<%=i%>' value=<%=actionName.equals("edit")?("'" +Math.round(recruitment_bulk_attendanceDTO.roll) + "'"):("'" + "0" + "'")%>
			tag='pb_html'/>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_status'>")%>
			
	
	<div class="form-inline" id = 'status_div_<%=i%>'>
		<select name="status" style="width:100%;" class='form-control' id='status_select2_<%=i%>' tag='pb_html'>
			<!--<option value="<%=recruitment_bulk_attendanceDTO.status%>>"><%=recruitment_bulk_attendanceDTO.status%></option>-->
			<%
				for (Map.Entry<Integer,String> entry : AttendanceStatusMap.entrySet())    {

					if(entry.getKey()==recruitment_bulk_attendanceDTO.status){
						//System.out.println("ATT STATUS: "+entry.getValue());
						out.println("<option value="+entry.getKey()+" selected>"+LM.getText(entry.getKey()==1?LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT:LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT,loginDTO)+"</option>");
					}
					else{
						out.println("<option value="+entry.getKey()+" >"+LM.getText(entry.getKey()==1?LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT:LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT,loginDTO)+"</option>");
					}

				}
			%>

		</select>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + recruitment_bulk_attendanceDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=recruitment_bulk_attendanceDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
							<% 
											out.println("</tr>");
										}
									}
								}
								catch(Exception ex)
								{
									ex.printStackTrace();
								}
						%>
						</tbody>
					</table>
</div>
			</div>
		</div>
	</div>
</div>