<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_bulk_attendance.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="static sessionmanager.SessionConstants.AttendanceStatusMap" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_RECRUITMENT_BULK_ATTENDANCE;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Recruitment_bulk_attendanceDTO recruitment_bulk_attendanceDTO = (Recruitment_bulk_attendanceDTO)request.getAttribute("recruitment_bulk_attendanceDTO");
CommonDTO commonDTO = recruitment_bulk_attendanceDTO;
String servletName = "Recruitment_bulk_attendanceServlet";
Recruitment_job_descriptionDAO recruitmentJobDescriptionDAO=new Recruitment_job_descriptionDAO("recruitment_job_description");
List<Recruitment_job_descriptionDTO> recruitmentJobDescriptionDTOS = recruitmentJobDescriptionDAO.getAllRecruitment_job_description(true);



System.out.println("recruitment_bulk_attendanceDTO = " + recruitment_bulk_attendanceDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Recruitment_bulk_attendanceDAO recruitment_bulk_attendanceDAO = (Recruitment_bulk_attendanceDAO)request.getAttribute("recruitment_bulk_attendanceDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

											
		
											
											<td id = '<%=i%>_jobId'>
											<%
//											value = recruitment_bulk_attendanceDTO.jobId + "";
											Recruitment_job_descriptionDTO jobDescriptionDTO = Recruitment_job_descriptionRepository.
													getInstance().getRecruitment_job_descriptionDTOByID(recruitment_bulk_attendanceDTO.jobId);
											value = UtilCharacter.getDataByLanguage(Language, jobDescriptionDTO.jobTitleBn, jobDescriptionDTO.jobTitleEn);
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_levelId'>
											<%
											value = recruitment_bulk_attendanceDTO.levelId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_roll'>
											<%
											value = recruitment_bulk_attendanceDTO.roll + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_status'>
											<%
											value = LM.getText(recruitment_bulk_attendanceDTO.status==1?LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_PRESENT:LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_ABSENT,loginDTO) + "";
											%>
											<%=value%>
			
											</td>
		
											
		
											
		
											
		
											
		
											
		
	

											<td>
												<a href='Recruitment_bulk_attendanceServlet?actionType=view&ID=<%=recruitment_bulk_attendanceDTO.id%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
<%--											<td id = '<%=i%>_Edit'>																																	--%>
<%--	--%>
<%--&lt;%&ndash;												<a href='Recruitment_bulk_attendanceServlet?actionType=getEditPage&ID=<%=recruitment_bulk_attendanceDTO.id%>'><%=LM.getText(LC.RECRUITMENT_BULK_ATTENDANCE_SEARCH_RECRUITMENT_BULK_ATTENDANCE_EDIT_BUTTON, loginDTO)%></a>&ndash;%&gt;--%>
<%--													<a href='Recruitment_bulk_attendanceServlet?actionType=getEditPage&ID=<%=recruitment_bulk_attendanceDTO.id%>'><i style="color: #ff6a6a" class="fa fa-edit"></i></a>--%>
<%--											</td>--%>

											<td id = '<%=i%>_Edit' class="text-center"  style="vertical-align: middle;">

												<a href='Recruitment_bulk_attendanceServlet?actionType=getEditPage&ID=<%=recruitment_bulk_attendanceDTO.id%>'><i style="color: #ff6a6a" class="fa fa-edit"></i></a>

											</td>

											
<%--											<td id='<%=i%>_checkbox'>--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=recruitment_bulk_attendanceDTO.id%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>

											<td id='<%=i%>_checkbox' class="text-center" style="vertical-align: middle; padding-right: 7px; padding-top: 13px">
												<div class='checker mr-3'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=recruitment_bulk_attendanceDTO.iD%>'/></span>
												</div>
											</td>
																						
											

