<%@ page import="util.UtilCharacter" %>
<%@ page import="pi_package_new.Pi_package_newRepository" %>
<%@ page import="pi_annual_demand.Pi_annual_demandDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="pi_annual_demand.Pi_annual_demandRepository" %>
<%@ page import="pb.Utils" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">
            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title" id="header_office_name_fiscal_year">

                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="modal-product-view-table">
                            <thead>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ", "Package")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "লট", "Lot")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "তারিখ", "Date")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "নথি নাম্বার", "Nothi No")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "বিস্তারিত দেখুন", "View Details")%>
                            </th>
                            </thead>
                            <tbody id="package-lot-body">
                            <tr class="hiddenTr" id="template-package-lot-row">
                                <input type="hidden" id="hidden_package_id" value="-1">
                                <input type="hidden" id="hidden_lot_id" value="-1">
                                <input type="hidden" id="hidden_package_name" value="-1">
                                <input type="hidden" id="hidden_lot_name" value="-1">
                                <input type="hidden" id="hidden_fiscal_year" value="-1">
                                <td id="serial_number_"></td>
                                <td id="package_"></td>
                                <td id="lot_"></td>
                                <td id="package_lot_creation_date_"></td>
                                <td id="nothi_"></td>
                                <td>
                                    <button
                                            type="button"
                                            class="btn-sm border-0 shadow bg-light btn-border-radius"
                                            style="color: #ff6b6b;"
                                            onclick="openItemModal(this)">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .hiddenTr {
        display: none;
    }
</style>