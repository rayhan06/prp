<%@page import="pi_package_item_map.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_unit.Pi_unitRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>

<%
    Pi_package_item_mapDTO pi_package_item_mapDTO = new Pi_package_item_mapDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_package_item_mapDTO = Pi_package_item_mapDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_package_item_mapDTO;
    String tableName = "pi_package_item_map";

    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
    Fiscal_yearDTO cur_fis_dto = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_PACKAGE_ITEM_MAP_ADD_PI_PACKAGE_ITEM_MAP_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_package_item_mapServlet";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigForm" name="bigForm">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                        <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                               value='<%=pi_package_item_mapDTO.iD%>' tag='pb_html'/>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নাম্বার", "Package Number")%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='piPackageFinalId'
                                                        id='piPackageFinalId'
                                                        onchange="filterLotOptionsByPackage(this)">
                                                    <%= Pi_package_finalRepository.getInstance().buildOptions(Language, pi_package_item_mapDTO.piPackageFinalId) %>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row hiddenTr" id="lot-div">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "লট নাম্বার", "Lot Number")%><span
                                                    class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='piLotFinalId' id='piLotFinalId'>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label text-right">
                                                <%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_FISCALYEAR, loginDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-8">
                                                <select class='form-control' name='fiscalYear' id='fiscalYear'
                                                        tag='pb_html'>
                                                    <%
                                                        String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                        StringBuilder option = new StringBuilder();
                                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {

                                                            if (actionName.equals("ajax_edit")) {
                                                                if (pi_package_item_mapDTO.fiscalYearId == fiscal_yearDTO.id) {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                                } else {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                                }
                                                            } else {
                                                                if (cur_fis_dto.id == fiscal_yearDTO.id) {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                                                } else {
                                                                    option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                                                }
                                                            }

                                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                                        }
                                                        fiscalYearOptions += option.toString();
                                                    %>
                                                    <%=fiscalYearOptions%>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-md-8">
                                                <button class="btn btn-primary form-control"
                                                        id="add-new-list-btn"
                                                        type="button">
                                                    <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ আইটেম ম্যাপ করুন", "Package Item Map")%>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <div class="form-row">
                                <div class="col-8">
                                    <%=UtilCharacter.getDataByLanguage(Language, "আইটেমের ", "Item List")%>
                                    <span id="merged-view-header">
                                        <%=UtilCharacter.getDataByLanguage(Language, "সমন্বিত তালিকা", " Merged View")%>
                                    </span>
                                    <span class="hiddenTr" id="split-view-header">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বিস্তারিত তালিকা", " Split View")%>
                                    </span>
                                </div>
                                <div class="col-2">
                                    <button class="btn btn-primary form-control" id="merged-view-button"
                                            onclick="showMergedViewTable()"
                                            type="button">
                                        <%=UtilCharacter.getDataByLanguage(Language, "সমন্বিত তালিকা", "Show Merged View")%>
                                    </button>
                                </div>
                                <div class="col-2">
                                    <button class="btn btn-primary form-control" id="split-view-button"
                                            onclick="showSplitViewTable()"
                                            type="button">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বিস্তারিত তালিকা", "Show Split View")%>
                                    </button>
                                </div>
                            </div>
                        </h5>
                        <div class="table-responsive" id="merged_view_table">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মালামালের বিবরন", "Item Description")%>
                                    </th>
                                    <th rowspan="1"
                                        colspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "পরিমাণ", "Quantity")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "একক দর (টাকায়)", "Unit Price (BDT)")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মোট দর (টাকায়)", "Total Price (BDT)")%>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "সংখ্যা", "Number")%>
                                    </th>
                                    <th style="text-align: center;"><%=UtilCharacter.getDataByLanguage(Language, "একক", "Unit")%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="merged-view-item-row-body">

                                </tbody>
                                <tr id="template-merged-view-item-row" class="hiddenTr">
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name="mergedView.itemGroupId"/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name="mergedView.itemTypeId"/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name="mergedView.itemId">
                                    </td>
                                    <td>
                                    <textarea class='form-control' name="mergedView.itemDescription"
                                              readonly></textarea>
                                    </td>
                                    <td>
                                        <input class='form-control' name="mergedView.itemQuantity" value=''
                                               type="number"
                                               min="0"
                                               max="99999999" readonly/>
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' name="mergedView.itemUnitText" value=''
                                               readonly/>
                                    </td>
                                    <td>
                                        <input class='form-control' name="mergedView.itemUnitPrice" value=''
                                               type="number"
                                               min="0"
                                               max="99999999"
                                               onchange="calculateTotalPriceForMergedView(this)"/>
                                    </td>
                                    <td>
                                        <input class='form-control' name="mergedView.itemTotalPrice" value=''
                                               type="number"
                                               min="0"
                                               max="99999999" readonly/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="table-responsive hiddenTr" id="split_view_table">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মালামালের বিবরন", "Item Description")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "দপ্তর", "Office")%>
                                    </th>
                                    <th rowspan="1"
                                        colspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "পরিমাণ", "Quantity")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "একক দর (টাকায়)", "Unit Price (BDT)")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মোট দর (টাকায়)", "Total Price (BDT)")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle; text-align: center"><%=LM.getText(LC.PI_ANNUAL_DEMAND_ADD_PI_ANNUAL_DEMAND_CHILD_REMOVE, loginDTO)%>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "সংখ্যা", "Number")%>
                                    </th>
                                    <th style="text-align: center;"><%=UtilCharacter.getDataByLanguage(Language, "একক", "Unit")%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="item-row-body">
                                <%
                                    if (actionName.equals("ajax_edit")) {
                                        int index = -1;
                                        for (PiPackageItemMapChildDTO model : pi_package_item_mapDTO.piPackageItemMapChildDTOList) {
                                            index++;
                                            System.out.println("index index = " + index);
                                %>

                                <tr>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               id='iD_hidden_<%=childTableStartingID%>'
                                               name='packageItemMapChild.iD' value='<%=model.iD%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               id='annual_demand_iD_hidden_<%=childTableStartingID%>'
                                               name='packageItemMapChild.annualDemandID'
                                               value='<%=model.annualDemandId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               id='annual_demand_child_iD_hidden_<%=childTableStartingID%>'
                                               name='packageItemMapChild.annualDemandChildID'
                                               value='<%=model.annualDemandChildId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               id='office_unit_iD_hidden_<%=childTableStartingID%>'
                                               name='packageItemMapChild.officeUnitId' value='<%=model.officeUnitId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               id='piItemGroupId_hidden_<%=childTableStartingID%>'
                                               name='packageItemMapChild.piItemGroupId'
                                               value='<%=model.itemGroupId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               id='piItemTypeId_hidden_<%=childTableStartingID%>'
                                               name='packageItemMapChild.piItemTypeId'
                                               value='<%=model.itemTypeId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name='packageItemMapChild.piItemId'
                                               id='piItemId_hidden_<%=childTableStartingID%>' value='<%=model.itemId%>'>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' name='packageItemMapChild.piUnitId'
                                               id='piUnitId_hidden_<%=childTableStartingID%>'
                                               value='<%=model.piUnitId%>'/>
                                    </td>
                                    <td>
                                        <input class="form-control" id="serialNumber_<%=childTableStartingID%>"
                                               type="number" min="0" max="99999999"
                                               name='packageItemMapChild.serialNumber'
                                               value='<%=model.serialNumber%>'>
                                    </td>
                                    <td>
                                    <textarea class='form-control' id='itemDescription_text_<%=childTableStartingID%>'
                                              name="packageItemMapChild.itemDescription"
                                              onchange="changeAllSameItemDescription(this)"
                                    ><%=model.description%></textarea>
                                    </td>
                                    <td>
                                        <input class='form-control'
                                               id='itemOfficeUnitNameText_<%=childTableStartingID%>'
                                               value='<%=Office_unitsRepository.getInstance().geText(Language, model.officeUnitId) + ""%>'
                                               type="text" name='packageItemMapChild.officeUnitNameText' readonly/>
                                    </td>
                                    <td>
                                        <input class='form-control' id='itemQuantity_<%=childTableStartingID%>'
                                               value='<%=model.itemQuantity%>' type="number" min="0" max="99999999"
                                               name='packageItemMapChild.itemQuantity'
                                               oninput="this.value = Math.round(this.value * 100) / 100;calculateTotalPrice(this)"/>
                                    </td>
                                    <td>
                                        <%
                                            String unit = Pi_unitRepository.getInstance().getName(Language, model.piUnitId);
                                        %>
                                        <input type='text' class='form-control'
                                               name='packageItemMapChild.itemUnitText'
                                               id='piUnitId_text_<%=childTableStartingID%>'
                                               value='<%=unit%>' readonly/>
                                    </td>
                                    <td>
                                        <input class='form-control' id='itemUnitPrice_<%=childTableStartingID%>'
                                               value='<%=model.itemUnitPrice%>' type="number" min="0" max="99999999"
                                               name='packageItemMapChild.itemUnitPrice'
                                               onchange="calculateTotalPrice(this);changeAllSameItemPrices(this)"/>
                                    </td>
                                    <td>
                                        <input class='form-control' id='itemTotalPrice_<%=childTableStartingID%>'
                                               value='<%=model.itemTotalPrice%>' type="number" min="0" max="99999999"
                                               name='packageItemMapChild.itemTotalPrice' readonly/>
                                    </td>
                                    <td>
                                        <button
                                                id="remove-item"
                                                name="removeItem"
                                                type="button"
                                                class="btn btn-sm remove-btn shadow ml-2 pl-4"
                                                onclick="removeItemRow(this)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <%
                                            childTableStartingID++;
                                        }
                                    }
                                %>

                                <%PiPackageItemMapChildDTO childModel = new PiPackageItemMapChildDTO();%>

                                <tr id="template-item-row" class="hiddenTr">
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' id='iD_hidden_' value='-1'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' id='annual_demand_iD_hidden_'
                                               value='<%=childModel.annualDemandId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' id='annual_demand_child_iD_hidden_'
                                               value='<%=childModel.annualDemandChildId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control'
                                               id='office_unit_iD_hidden_'
                                               value='<%=childModel.officeUnitId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' id='piItemGroupId_hidden_'
                                               value='<%=childModel.itemGroupId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' id='piItemTypeId_hidden_'
                                               value='<%=childModel.itemTypeId%>'/>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' id='piItemId_hidden_'
                                               value='<%=childModel.itemId%>'>
                                    </td>
                                    <td style="display: none;">
                                        <input type='hidden' class='form-control' id='piUnitId_hidden_'
                                               value='<%=childModel.piUnitId%>'/>
                                    </td>
                                    <td>
                                        <input class="form-control" id="serialNumber_" type="number" min="0"
                                               max="99999999" value='<%=childModel.serialNumber%>'>
                                    </td>
                                    <td>
                                        <textarea class='form-control' id='itemDescription_text_'
                                                  onchange="changeAllSameItemDescription(this)"></textarea>
                                    </td>
                                    <td>
                                        <input class='form-control' id='officeUnitIdText_' value='' type="text"
                                               readonly/>
                                    </td>
                                    <td>
                                        <input class='form-control' id='itemQuantity_' value='' type="number" min="0"
                                               max="99999999"
                                               oninput="this.value = Math.round(this.value * 100) / 100;calculateTotalPrice(this)"/>
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' id='piUnitId_text_'
                                               name='packageItemMapChild.itemUnitText' value='' readonly/>
                                    </td>
                                    <td>
                                        <input class='form-control' id='itemUnitPrice_' value='' type="number" min="0"
                                               max="99999999"
                                               onchange="calculateTotalPrice(this);changeAllSameItemPrices(this)"/>
                                    </td>
                                    <td>
                                        <input class='form-control' id='itemTotalPrice_' value='' type="number" min="0"
                                               max="99999999"/>
                                    </td>
                                    <td>
                                        <button
                                                id="remove-item"
                                                name="removeItem"
                                                type="button"
                                                class="btn btn-sm remove-btn shadow ml-2 pl-4"
                                                onclick="removeItemRow(this)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.PI_PACKAGE_ITEM_MAP_ADD_PI_PACKAGE_ITEM_MAP_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                            onclick="submitForm()">
                        <%=LM.getText(LC.PI_PACKAGE_ITEM_MAP_ADD_PI_PACKAGE_ITEM_MAP_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../pi_package_item_map/pi_package_item_mapOfficeModal.jsp">
    <jsp:param name="index" value="0"/>
</jsp:include>
<jsp:include page="../pi_package_item_map/package_item_mapIPackageLotModal.jsp">
    <jsp:param name="index" value="0"/>
</jsp:include>
<jsp:include page="../pi_package_item_map/package_item_mapItemModal.jsp">
    <jsp:param name="index" value="0"/>
</jsp:include>

<style>
    .hiddenTr {
        display: none;
    }
</style>

<script type="text/javascript">
    const packageItemMapForm = $("#bigForm");
    let language = '<%=Language%>';
    let child_table_extra_id = <%=childTableStartingID%>;
    let isEditPage = "<%=actionName%>" === "ajax_edit";
    let itemsForDuplicateCheck = [];
    let splitViewDivId = "split_view_table";
    let splitViewTableBodyId = "item-row-body";

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function submitForm() {
        buttonStateChange(true);
        if (true) {
            let url = "<%=servletName%>?actionType=<%=actionName%>";
            $.ajax({
                type: "POST",
                url: url,
                data: packageItemMapForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        showToastSticky("সাবমিট সফল হয়েছে", "Submit Successful");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 3000);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function init(row) {
    }

    let row = 0;
    $(document).ready(function () {
        init(row);
        filterLotOptionsByPackage(document.getElementById('piPackageFinalId'));
        generateMergedViewTable();
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $('#add-new-list-btn').on('click', function () {
            loadOfficeModal()
        });
    });

    $("#add-more-PiPackageItemMapChild").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-PiPackageItemMapChild");

            $("#field-PiPackageItemMapChild").append(t.html());
            SetCheckBoxValues("field-PiPackageItemMapChild");

            var tr = $("#field-PiPackageItemMapChild").find("tr:last-child");

            tr.attr("id", "PiPackageItemMapChild_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });


            child_table_extra_id++;

        });

    function loadOfficeModal() {
        // resetModal();
        $('#search_package_item_office_modal').modal();
    }

    function constructItemForDuplicateCheck(item) {
        return {
            itemId: item["item-id"],
            officeUnitId: item["item-office-unit-id"],
        }
    }

    function existsForSameOffice(selectedItem) {
        for (let item of itemsForDuplicateCheck) {
            if (item.itemId === selectedItem.itemId && item.officeUnitId === selectedItem.officeUnitId) {
                return true;
            }
        }
        return false;
    }

    function existsForDifferentOffice(selectedItem) {
        for (let item of itemsForDuplicateCheck) {
            if (item.itemId === selectedItem.itemId && item.officeUnitId === selectedItem.officeUnitId) {
                return false;
            } else if (item.itemId === selectedItem.itemId && item.officeUnitId !== selectedItem.officeUnitId) {
                return true;
            }
        }
        return false;
    }

    itemMapModal = {
        passModalData: function (items) {
            for (let item of items) {
                setDataToTable(item);
            }
        }
    }

    function setDataToTable(item) {
        let constructedItem = constructItemForDuplicateCheck(item);
        if (existsForSameOffice(constructedItem)) {
            swal.fire(getValueByLanguage(language, "এই আইটেম টা ইতিমধ্যে যোগ করা হয়েছে", "This item is already added"));
            return;
        } else {
            itemsForDuplicateCheck.push(constructedItem);
        }
        let tableBody = document.querySelector('#item-row-body');
        let tableRow = document.querySelector('#template-item-row').cloneNode(true);
        tableRow.classList.remove('hiddenTr');
        console.log(tableRow)
        tableRow.querySelector('#iD_hidden_').name = "packageItemMapChild.iD";
        tableRow.querySelector('#annual_demand_iD_hidden_').name = "packageItemMapChild.annualDemandID";
        tableRow.querySelector('#annual_demand_child_iD_hidden_').name = "packageItemMapChild.annualDemandChildID";
        tableRow.querySelector('#office_unit_iD_hidden_').name = "packageItemMapChild.officeUnitId";
        tableRow.querySelector('#piItemGroupId_hidden_').name = "packageItemMapChild.piItemGroupId";
        tableRow.querySelector('#piItemTypeId_hidden_').name = "packageItemMapChild.piItemTypeId";
        tableRow.querySelector('#piItemId_hidden_').name = "packageItemMapChild.piItemId";
        tableRow.querySelector('#piUnitId_hidden_').name = "packageItemMapChild.piUnitId";
        tableRow.querySelector('#serialNumber_').name = "packageItemMapChild.serialNumber";
        tableRow.querySelector('#itemDescription_text_').name = "packageItemMapChild.itemDescription";
        tableRow.querySelector('#itemQuantity_').name = "packageItemMapChild.itemQuantity";
        tableRow.querySelector('#itemUnitPrice_').name = "packageItemMapChild.itemUnitPrice";
        tableRow.querySelector('#itemTotalPrice_').name = "packageItemMapChild.itemTotalPrice";
        tableRow.querySelector('#serialNumber_').value = child_table_extra_id;
        tableRow.querySelector('#piItemGroupId_hidden_').value = item["item-group-id"];
        tableRow.querySelector('#office_unit_iD_hidden_').value = item["item-office-unit-id"];
        tableRow.querySelector('#officeUnitIdText_').value = item["item-office-unit-text"];
        tableRow.querySelector('#annual_demand_iD_hidden_').value = item["item-annual-demand-id"];
        tableRow.querySelector('#annual_demand_child_iD_hidden_').value = item["item-annual-demand-child-id"];
        tableRow.querySelector('#piItemTypeId_hidden_').value = item["item-type-id"];
        tableRow.querySelector('#piItemId_hidden_').value = item["item-id"];
        tableRow.querySelector('#itemDescription_text_').innerText = item["item-description"];
        tableRow.querySelector('#itemQuantity_').value = item["item-count"];
        tableRow.querySelector('#piUnitId_hidden_').value = item["item-unit-id"];
        tableRow.querySelector('#piUnitId_text_').value = item["item-unit-name"];
        tableRow.querySelector('#itemUnitPrice_').value = item["item-price"];
        tableRow.querySelector('#itemTotalPrice_').value = item["item-total-price"];

        tableBody.append(tableRow);
        child_table_extra_id++;

        generateMergedViewTable();
    }

    function filterLotOptionsByPackage(element) {
        let packageId = document.getElementById(element.id).value;
        let lotId;
        if (isEditPage) lotId = '<%=pi_package_item_mapDTO.piLotFinalId%>';
        else lotId = document.getElementById('piLotFinalId').value;
        let lotOptions = document.getElementById('piLotFinalId');
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let res = JSON.parse(this.response)
                lotOptions.innerHTML = res.msg;
                makeLotVisibleIfRequired();
                console.log(this.responseText)
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Pi_package_finalServlet?actionType=getLotOptionsByPackage&packageId=" + packageId + "&piLotId=" + lotId, true);
        xhttp.send();
    }

    function makeLotVisibleIfRequired() {
        let lotDiv = document.getElementById('lot-div');
        let lotSelect = document.getElementById('piLotFinalId');
        if (lotSelect.length > 1) {
            lotDiv.classList.remove('hiddenTr');
        } else {
            lotSelect.innerHTML = "";
            lotDiv.classList.add('hiddenTr');
        }
    }

    function removeItemRow(row) {
        row.parentNode.parentNode.remove();
    }

    function calculateTotalPrice(element) {
        let selectedIdIndex = element.id.split('_')[1];
        let rowEl = element.parentNode.parentNode;
        let quantityElement = rowEl.querySelector('#itemQuantity_' + selectedIdIndex);
        let unitPriceElement = rowEl.querySelector('#itemUnitPrice_' + selectedIdIndex);
        let quantity = quantityElement.value;
        let unitPrice = unitPriceElement.value;
        if (quantity && unitPrice && quantity.trim().length > 0 && unitPrice.trim().length > 0) {
            let val = (quantity / 1) * (unitPrice / 1);
            rowEl.querySelector('#itemTotalPrice_' + selectedIdIndex).value = val.toFixed(0);
        } else {
            rowEl.querySelector('#itemTotalPrice_' + selectedIdIndex).value = ""
        }
    }

    function calculateTotalPriceForMergedView(element) {
        let rowEl = element.parentNode.parentNode;
        let quantityElement = rowEl.querySelector("td>input[name='mergedView.itemQuantity']");
        let unitPriceElement = rowEl.querySelector("td>input[name='mergedView.itemUnitPrice']");
        let totalPriceElement = rowEl.querySelector("td>input[name='mergedView.itemTotalPrice']");
        let itemIdElement = rowEl.querySelector("td>input[name='mergedView.itemId']");
        let quantity = quantityElement.value;
        let unitPrice = unitPriceElement.value;
        if (quantity && unitPrice && quantity.trim().length > 0 && unitPrice.trim().length > 0) {
            let val = (quantity / 1) * (unitPrice / 1);
            totalPriceElement.value = val.toFixed(0);
            updatePriceToSplitView(unitPrice, itemIdElement);
        } else {
            totalPriceElement.value = "0"
        }
    }

    function getTrByItemId(tBody, itemId) {
        let allTr = document.getElementById(tBody).children;
        let filteredTr;
        let shouldSkip = false;
        Array.from(allTr).forEach(tr => {
            if (shouldSkip) return;
            let currItemIdElement = tr.querySelector('td>input[name="packageItemMapChild.piItemId"]');
            if (currItemIdElement) {
                let currItemId = currItemIdElement.value;
                if (currItemId === itemId) {
                    filteredTr = tr;
                    shouldSkip = true;
                }
            }
        });
        return filteredTr;
    }

    function updatePriceToSplitView(unitPrice, itemIdElement) {
        let itemId = itemIdElement.value;
        let tr = getTrByItemId(splitViewTableBodyId, itemId);

        let unitPriceElementSplitView = tr.querySelector("td>input[name='packageItemMapChild.itemUnitPrice']");
        unitPriceElementSplitView.value = unitPrice;
        changeAllSameItemPrices(unitPriceElementSplitView);
    }

    function changeAllSameItemPrices(element) {
        let tr = element.parentNode.parentNode;
        let globalItemId = tr.querySelector('td>input[name="packageItemMapChild.piItemId"]').value;
        let globalUnitPrice = tr.querySelector('td>input[name="packageItemMapChild.itemUnitPrice"]').value;

        let tBody = document.getElementById('item-row-body');
        let allTr = tBody.children;
        // REMOVE TEMPLATE ROW
        let filteredTr = Array.from(allTr).filter(tr => !tr.classList.contains('hiddenTr'));
        filteredTr.forEach((tr) => {
            let trItemId = tr.querySelector('td>input[name="packageItemMapChild.piItemId"]').value;
            if (trItemId === globalItemId) {
                let quantityElement = tr.querySelector('td>input[name="packageItemMapChild.itemQuantity"]');
                let unitPriceElement = tr.querySelector('td>input[name="packageItemMapChild.itemUnitPrice"]');
                let totalPriceElement = tr.querySelector('td>input[name="packageItemMapChild.itemTotalPrice"]');
                unitPriceElement.value = globalUnitPrice;
                totalPriceElement.value = quantityElement.value * globalUnitPrice;
            }
        });
    }

    function changeAllSameItemDescription(element) {
        let tr = element.parentNode.parentNode;
        let globalItemId = tr.querySelector('td>input[name="packageItemMapChild.piItemId"]').value;
        let globalDescription = tr.querySelector('td>textarea[name="packageItemMapChild.itemDescription"]').value;

        let tBody = document.getElementById('item-row-body');
        let allTr = tBody.children;
        // REMOVE TEMPLATE ROW
        let filteredTr = Array.from(allTr).filter(tr => !tr.classList.contains('hiddenTr'));
        filteredTr.forEach((tr) => {
            let trItemId = tr.querySelector('td>input[name="packageItemMapChild.piItemId"]').value;
            if (trItemId === globalItemId) {
                let currItemDescription = tr.querySelector('td>textarea[name="packageItemMapChild.itemDescription"]');
                currItemDescription.value = globalDescription;
                currItemDescription.textContent = globalDescription;
            }
        });
    }

    function removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }

    function cleanMergedViewTable() {
        let tBody = document.getElementById('merged-view-item-row-body');
        removeAllChildNodes(tBody);
    }

    function getAllSplitViewRows() {
        let splitViewBody = document.getElementById('item-row-body');
        let allTr = splitViewBody.children;
        // REMOVE TEMPLATE ROW
        let filteredTr = Array.from(allTr).filter(tr => !tr.classList.contains('hiddenTr'));
        return filteredTr;
    }

    function generateMergedViewTable() {
        cleanMergedViewTable();
        let rows = getAllSplitViewRows();
        let mergedViewTBody = document.getElementById('merged-view-item-row-body');
        let itemsMap = new Map();
        rows.forEach(row => {
            let currItemId = row.querySelector('td>input[name="packageItemMapChild.piItemId"]').value;
            let currQuantityElement = row.querySelector('td>input[name="packageItemMapChild.itemQuantity"]');
            let currUnitPriceElement = row.querySelector('td>input[name="packageItemMapChild.itemUnitPrice"]');
            let currTotalPriceElement = row.querySelector('td>input[name="packageItemMapChild.itemTotalPrice"]');
            let itemDescription = row.querySelector('td>textarea[name="packageItemMapChild.itemDescription"]').textContent;
            let itemQuantity = row.querySelector('td>input[name="packageItemMapChild.itemQuantity"]').value;
            let itemUnitText = row.querySelector('td>input[name="packageItemMapChild.itemUnitText"]').value;
            let itemUnitPrice = row.querySelector('td>input[name="packageItemMapChild.itemUnitPrice"]').value;
            let itemTotalPrice = row.querySelector('td>input[name="packageItemMapChild.itemTotalPrice"]').value;
            if (itemsMap.has(currItemId)) {
                let previousItem = itemsMap.get(currItemId);
                let previousQuantityElement = previousItem.querySelector('td>input[name="mergedView.itemQuantity"]');
                let previousTotalPriceElement = previousItem.querySelector('td>input[name="mergedView.itemTotalPrice"]');
                previousQuantityElement.value = parseInt(previousQuantityElement.value) + parseInt(currQuantityElement.value);
                previousTotalPriceElement.value = parseInt(previousTotalPriceElement.value) + parseInt(currTotalPriceElement.value);
            } else {
                let clonedTr = document.getElementById('template-merged-view-item-row').cloneNode(true);
                clonedTr.classList.remove('hiddenTr');
                clonedTr.querySelector('td>input[name="mergedView.itemId"]').value = currItemId;
                clonedTr.querySelector('td>textarea[name="mergedView.itemDescription"]').textContent = itemDescription;
                clonedTr.querySelector('td>input[name="mergedView.itemQuantity"]').value = itemQuantity;
                clonedTr.querySelector('td>input[name="mergedView.itemUnitText"]').value = itemUnitText;
                clonedTr.querySelector('td>input[name="mergedView.itemUnitPrice"]').value = itemUnitPrice;
                clonedTr.querySelector('td>input[name="mergedView.itemTotalPrice"]').value = itemTotalPrice;
                mergedViewTBody.append(clonedTr);

                itemsMap.set(currItemId, clonedTr);
            }
        });
    }

    function showMergedViewTable() {
        let mergedViewTable = document.getElementById('merged_view_table');
        generateMergedViewTable();
        showMergedViewHeader();
        showElement(mergedViewTable);
        hideSplitViewHeader();
        hideSplitViewTable();
    }

    function showSplitViewTable() {
        let splitViewTable = document.getElementById('split_view_table');
        showSplitViewHeader();
        showElement(splitViewTable);
        hideMergedViewHeader();
        hideMergedViewTable();
    }

    function hideMergedViewTable() {
        let mergedViewTable = document.getElementById('merged_view_table');
        hideElement(mergedViewTable);
    }

    function hideMergedViewButton() {
        let mergedViewButton = document.getElementById('merged-view-button');
        hideElement(mergedViewButton);
    }

    function showMergedViewButton() {
        let mergedViewButton = document.getElementById('merged-view-button');
        showElement(mergedViewButton);
    }

    function hideSplitViewTable() {
        let splitViewTable = document.getElementById('split_view_table');
        hideElement(splitViewTable);
    }

    function hideSplitViewButton() {
        let splitViewButton = document.getElementById('split-view-button');
        hideElement(splitViewButton);
    }

    function showSplitViewButton() {
        let splitViewButton = document.getElementById('split-view-button');
        showElement(splitViewButton);
    }

    function showMergedViewHeader() {
        let header = document.getElementById('merged-view-header');
        showElement(header);
    }

    function hideMergedViewHeader() {
        let header = document.getElementById('merged-view-header');
        hideElement(header);
    }

    function showSplitViewHeader() {
        let header = document.getElementById('split-view-header');
        showElement(header);
    }

    function hideSplitViewHeader() {
        let header = document.getElementById('split-view-header');
        hideElement(header);
    }

    function hideElement(element) {
        element.classList.add('hiddenTr');
    }

    function showElement(element) {
        element.classList.remove('hiddenTr');
    }
</script>