<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    String modalTitle = request.getParameter("modalTitle");
    if (modalTitle == null) {
        modalTitle = UtilCharacter.getDataByLanguage(Language, "বার্ষিক চাহিদার জন্য অনুরোধকৃত অফিসসমূহ", "Office List of Requested Annual Demand");
    }
%>

<div class="modal fade bd-example-modal-xl" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_package_item_office_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="pi_package_item_mapOfficeModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
                        onclick="closeModalOfficeModal()">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
            <%--------------------------------SELECTED DATA-----------------------------------------%>
            <input type="hidden" id="selected_office_unit_id" value="-1">
            <input type="hidden" id="selected_office_unit_name" value="-1">
            <input type="hidden" id="selected_fiscal_year_text" value="-1">
            <input type="hidden" id="selected_has_package_lot">
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#search_package_item_office_modal').on('show.bs.modal', function () {
        event.preventDefault();
    });

    function closeModalOfficeModal() {
        $('#search_package_item_office_modal').modal('hide');
    }

    function loadPackageLotModal(element) {
        let tableRow = element.parentNode.parentNode
        document.getElementById('selected_office_unit_id').value = tableRow.querySelector("input[id='hidden_office_unit_id']").value;
        document.getElementById('selected_office_unit_name').value = tableRow.querySelector("input[id='hidden_office_unit_name']").value;
        document.getElementById('selected_fiscal_year_text').value = tableRow.querySelector("input[id='hidden_fiscal_year_text']").value;
        document.getElementById('selected_has_package_lot').value = tableRow.querySelector("input[id='hidden_has_package_lot']").value;
        loadPackageLotList();
        let hasPackageLot = document.getElementById('selected_has_package_lot');
        if (hasPackageLot.value === "true")
            $('#search_package_item_map_package_lot_modal').modal();
        else
            $('#search_package_item_map_package_lot_modal').modal('hide');
    }

    function loadItemModal() {
        $('#search_package_item_map_item_modal').modal();
    }
</script>