<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    String modalTitle = request.getParameter("modalTitle");
    if (modalTitle == null) {
        modalTitle = UtilCharacter.getDataByLanguage(Language, "প্যাকেজ আইটেম ম্যাপ", "Package Item Map");
    }
%>

<div class="modal fade bd-example-modal-xl" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_package_item_map_item_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #0098bf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="package_item_mapItemModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn submit-btn text-white shadow btn-border-radius"
                        id="modal-submit" onclick="submitSelectedItem()">
                    <%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
                        onclick="closeModal()">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    let selectedItemList = [];
    let itemMapModal = {};

    $('#search_package_item_map_item_modal').on('show.bs.modal', function () {
        event.preventDefault();
    });

    function loadItemModalHeader() {
        let officeName = document.getElementById('selected_office_unit_name').value;
        let packageName = document.getElementById('selected_package_name').value;
        let lotNameFull = document.getElementById('selected_lot_name').value;
        let lotNumber = lotNameFull.split("- ")[0];
        let lotName = lotNameFull.split("- ").length === 2 ? "-" + lotNameFull.split("- ")[1] : "";
        let fiscalYearText = document.getElementById('selected_fiscal_year_text').value;
        document.getElementById('item_modal_office_name').innerText = officeName;
        document.getElementById('item_modal_package_lot_name').innerText = packageName + "(" + lotNumber + ")" + lotName;
        document.getElementById('item_modal_fiscal_year').innerText = getValueByLanguage(language, "অর্থবছর - ", "Fiscal Year - ") + fiscalYearText;
    }

    function hidePackageLotNameSection(isHide) {
        let packageLotNameHeader = document.getElementById('item_modal_package_lot_name');
        if (isHide) packageLotNameHeader.classList.add('hiddenTr');
        else packageLotNameHeader.classList.remove('hiddenTr');
    }

    function hidePackageLotModal() {
        $('#search_package_item_map_package_lot_modal').modal('hide');
    }

    function loadItemList(isDirectLoadWithOfficeUnit) {
        loadItemModalHeader();
        let url = '';
        let officeUnitId = document.getElementById('selected_office_unit_id').value;
        let packageId = document.getElementById('selected_package_id').value;
        let lotId = document.getElementById('selected_lot_id').value;
        if (isDirectLoadWithOfficeUnit) {
            url = "Pi_annual_demandServlet?actionType=getItemListByOfficeUnitId&officeUnitId=" + officeUnitId;
            hidePackageLotNameSection(true);
        } else {
            url = "Pi_annual_demandServlet?actionType=getItemListByPackageLot&packageId=" + packageId + "&lotId=" + lotId;
            hidePackageLotNameSection(false);
        }
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let packageLotList = JSON.parse(this.responseText);
                setDataToItemTable(packageLotList.msg);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", url, false);
        xhttp.send();
    }

    function setDataToItemTable(items) {
        let tableBody = document.getElementById('item-list-body');
        let tableRow = document.querySelector('.template-item-row-cls');
        items = JSON.parse(items);
        removeAllChildNodes(tableBody);
        for (let item of items) {
            let clonedRow = tableRow.cloneNode(true);
            clonedRow.classList.remove('hiddenTr','template-item-row-cls');
            clonedRow.querySelector("td>input[name='checkbox']").value = false;
            clonedRow.querySelector("td>input[name='checkbox']").checked = false;
            if (item['piPackageFinalId'] !== -1) {
                clonedRow.classList.add('disabledRow');
                clonedRow.querySelector("#checkbox_").disabled = true;
            } else {
                clonedRow.classList.remove('disabledRow');
                clonedRow.querySelector("#checkbox_").disabled = false;
            }
            clonedRow.querySelector('#hidden_annual_demand_id').value = item['annualDemandId'];
            clonedRow.querySelector('#hidden_annual_demand_child_id').value = item['annualDemandChildId'];
            clonedRow.querySelector('#hidden_office_unit_id').value = item['officeUnitId'];
            clonedRow.querySelector('#hidden_office_unit_text').value = item['officeUnitText'];
            clonedRow.querySelector('#hidden_item_group_id').value = item['itemGroupId'];
            clonedRow.querySelector('#hidden_item_type_id').value = item['itemTypeId'];
            clonedRow.querySelector('#hidden_item_id').value = item['itemId'];
            clonedRow.querySelector('#hidden_item_quantity').value = item['itemQuantity'];
            clonedRow.querySelector('#hidden_item_unit_id').value = item['piUnitId'];
            clonedRow.querySelector('#hidden_item_unit_price').value = item['itemUnitPrice'];
            clonedRow.querySelector('#hidden_item_total_price').value = item['itemTotalPrice'];
            clonedRow.querySelector('#modal_item_serial_').innerText = item['serialNumber'];
            clonedRow.querySelector('#modal_item_description_').innerText = getValueByLanguage(language, item['descriptionBn'], item['descriptionEn']);
            clonedRow.querySelector('#modal_item_quantity_').innerText = item['itemQuantity'];
            clonedRow.querySelector('#modal_item_unit_name').innerText = getValueByLanguage(language, item['piUnitNameBn'], item['piUnitNameEn']);
            clonedRow.querySelector('#modal_item_unit_price').innerText = item['itemUnitPrice'];
            clonedRow.querySelector('#modal_item_total_price').innerText = item['itemTotalPrice'];
            tableBody.append(clonedRow);
        }
    }

    function resetItemModal() {
        let tableBody = document.getElementById('item-list-body');
        removeAllChildNodes(tableBody);
    }

    function closeModal() {
        resetItemModal();
        $('#search_package_item_map_item_modal').modal('hide');
    }

    function constructItem(itemRow) {
        return {
            "item-annual-demand-id": itemRow.querySelector("input[id='hidden_annual_demand_id']").value,
            "item-annual-demand-child-id": itemRow.querySelector("input[id='hidden_annual_demand_child_id']").value,
            "item-office-unit-id": itemRow.querySelector("input[id='hidden_office_unit_id']").value,
            "item-office-unit-text": itemRow.querySelector("input[id='hidden_office_unit_text']").value,
            "item-group-id": itemRow.querySelector("input[id='hidden_item_group_id']").value,
            "item-type-id": itemRow.querySelector("input[id='hidden_item_type_id']").value,
            "item-id": itemRow.querySelector("input[id='hidden_item_id']").value,
            "item-count": itemRow.querySelector("input[id='hidden_item_quantity']").value,
            "item-unit-id": itemRow.querySelector("input[id='hidden_item_unit_id']").value,
            "item-price": itemRow.querySelector("input[id='hidden_item_unit_price']").value,
            "item-total-price": itemRow.querySelector("input[id='hidden_item_total_price']").value,
            "item-description": itemRow.querySelector("td[id='modal_item_description_']").innerText.trim(),
            "item-unit-name": itemRow.querySelector("td[id='modal_item_unit_name']").innerText.trim(),
        }
    }

    function loadSelectedItem() {
        let itemListTableBody = document.getElementById('item-list-body');
        selectedItemList.length = 0;
        // ITERATE OVER ITEM ROW
        Array.prototype.slice.call(itemListTableBody.children).forEach(itemRow => {
            let checkBox = itemRow.querySelector("td>input[name='checkbox']");
            if (checkBox && checkBox.value === "true") {
                let item = constructItem(itemRow);
                selectedItemList.push(item);
            }
        });
    }

    function submitSelectedItem() {
        loadSelectedItem();
        if (itemMapModal.passModalData) {
            itemMapModal.passModalData(selectedItemList);
        }
        $('#search_package_item_map_item_modal').modal('hide');
        $('#search_package_item_office_modal').modal('hide');
    }
</script>