<%@ page import="util.UtilCharacter" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.Utils" %>
<%@ page import="pi_annual_demand.PiAnnualDemandChildDTO" %>
<%@ page import="pi_annual_demand.PiAnnualDemandChildRepository" %>
<%@ page import="procurement_package.Procurement_packageDTO" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_package.ProcurementGoodsTypeDTO" %>
<%@ page import="procurement_package.ProcurementGoodsTypeRepository" %>
<%@ page import="procurement_goods.Procurement_goodsDTO" %>
<%@ page import="procurement_goods.Procurement_goodsRepository" %>
<%@ page import="pi_unit.Pi_unitRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">
            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title">
                        <h4 class="text-center" style="color: #0098bf;">
                            <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                        </h4>
                        <h5 class="text-center" id="item_modal_office_name" style="color: #0098bf;"></h5>
                        <h5 class="text-center" id="item_modal_package_lot_name" style="color: #0098bf;"></h5>
                        <h5 class="text-center" id="item_modal_fiscal_year" style="color: #0098bf;"></h5>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="modal-product-view-table">
                            <thead>
                            <tr>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                                </th>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মালামালের বিবরন", "Item Description")%>
                                </th>
                                <th rowspan="1"
                                    colspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "পরিমাণ", "Quantity")%>
                                </th>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "একক দর (টাকায়)", "Unit Price (BDT)")%>
                                </th>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মোট দর (টাকায়)", "Total Price (BDT)")%>
                                </th>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "সকল নির্বাচন", "Select All")%>
                                </th>
                            </tr>
                            <tr>
                                <th style="text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "সংখ্যা", "Number")%>
                                </th>
                                <th style="text-align: center;"><%=UtilCharacter.getDataByLanguage(Language, "একক", "Unit")%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="item-list-body">

                            </tbody>
                            <tr class="hiddenTr template-item-row-cls" id="template-item-list-row">
                                <input type="hidden" id="hidden_annual_demand_id" value="-1">
                                <input type="hidden" id="hidden_annual_demand_child_id" value="-1">
                                <input type="hidden" id="hidden_office_unit_id" value="-1">
                                <input type="hidden" id="hidden_office_unit_text" value="">
                                <input type="hidden" id="hidden_package_final_id" value="-1">
                                <input type="hidden" id="hidden_lot_final_id" value="-1">
                                <input type="hidden" id="hidden_item_group_id" value="-1">
                                <input type="hidden" id="hidden_item_type_id" value="-1">
                                <input type="hidden" id="hidden_item_id" value="-1">
                                <input type="hidden" id="hidden_item_quantity" value="-1">
                                <input type="hidden" id="hidden_item_unit_id" value="-1">
                                <input type="hidden" id="hidden_item_unit_price" value="-1">
                                <input type="hidden" id="hidden_item_total_price" value="-1">
                                <td id="modal_item_serial_">
                                </td>
                                <td id="modal_item_description_">
                                </td>
                                <td id="modal_item_quantity_">
                                </td>
                                <td id="modal_item_unit_name">
                                </td>
                                <td id="modal_item_unit_price">
                                </td>
                                <td id="modal_item_total_price">
                                </td>
                                <td>
                                    <input type="checkbox" name="checkbox" id="checkbox_"
                                           onclick="toggle(this)">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .hiddenTr {
        display: none;
    }
    .disabledRow{
        color: #475b54;
    }
</style>

<script>
    function toggle(element) {
        if (element.value) {
            if (element.value === "true") element.value = false;
            else element.value = true;
        } else {
            element.value = true;
        }
    }
</script>