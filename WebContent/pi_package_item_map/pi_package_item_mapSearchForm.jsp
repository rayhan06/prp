<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pi_package_item_map.*" %>
<%@ page import="util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pi_package_final.Pi_package_finalDTO" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="pi_package_final.PiPackageLotFinalDTO" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>


<%
    String navigator2 = "navPI_PACKAGE_ITEM_MAP";
    String servletName = "Pi_package_item_mapServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=UtilCharacter.getDataByLanguage(Language, "অর্থবছর", "Fiscal Year")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ নাম্বার", "Package Number")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "লট নাম্বার", "Lot Number")%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.PI_PACKAGE_ITEM_MAP_SEARCH_PI_PACKAGE_ITEM_MAP_EDIT_BUTTON, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ডিলিট করুন", "Delete")%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Pi_package_item_mapDTO>) rn2.list;
            try {
                if (data != null) {
                    int size = data.size();
                    for (int i = 0; i < size; i++) {
                        Pi_package_item_mapDTO model = (Pi_package_item_mapDTO) data.get(i);
                        String cell = "";
        %>
        <tr>
            <input type="hidden" id="packageItemMapId" value="<%=model.iD%>">
            <td>
                <%
                    cell = Fiscal_yearRepository.getInstance().getText(model.fiscalYearId, Language);
                %>
                <%=Utils.getDigits(cell, Language)%>
            </td>
            <td>
                <%
                    Pi_package_finalDTO packageDTO = Pi_package_finalRepository.getInstance()
                            .getPi_package_finalDTOByiD(model.piPackageFinalId);
                    cell = packageDTO == null ? "" : UtilCharacter.getDataByLanguage(Language, packageDTO.packageNumberBn, packageDTO.packageNumberEn);
                %>
                <%=cell%>
            </td>
            <td>
                <%
                    PiPackageLotFinalDTO lotDTO = PiPackageLotFinalRepository.getInstance()
                            .getPiPackageLotFinalDTOByiD(model.piLotFinalId);
                    cell = lotDTO == null ? "" : UtilCharacter.getDataByLanguage(Language, lotDTO.lotNumberBn, lotDTO.lotNumberEn);
                %>
                <%=cell%>
            </td>

            <%CommonDTO commonDTO = model; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-center">
                <button
                        type="button"
                        class="btn btn-sm remove-btn shadow ml-2 pl-4"
                        onclick="deletePackageItemMap(this)">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<script type="text/javascript">
    function deletePackageItemMap(element) {
        let tr = element.parentNode.parentNode;
        let packageItemMapId = tr.querySelector("#packageItemMapId").value;
        let url = "Pi_package_item_mapServlet?actionType=deletePackageItemMapWithValidation&packageItemMapId=" + packageItemMapId;
        $.ajax({
            type: "GET",
            url: url,
            success: function (response) {
                response = JSON.parse(response);
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    buttonStateChange(false);
                } else if (response.responseCode === 200) {
                    tr.remove();
                    showToastSticky("ডিলিট সফল হয়েছে", "Delete Successful");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }
</script>