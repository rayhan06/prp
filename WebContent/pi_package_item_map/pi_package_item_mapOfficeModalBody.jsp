<%@ page import="util.UtilCharacter" %>
<%@ page import="pi_annual_demand.Pi_annual_demandDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="pi_annual_demand.Pi_annual_demandRepository" %>
<%@ page import="pb.Utils" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">
            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title"></h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="modal-product-view-table">
                            <thead>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "অফিস", "Office")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "অর্থবছর", "Fiscal Year")%>
                            </th>
                            <th style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "বিস্তারিত দেখুন", "View Details")%>
                            </th>
                            </thead>
                            <tbody id="modalAppendTbody">
                            <%
                                List<Pi_annual_demandDTO> models = Pi_annual_demandRepository.getInstance().getPi_annual_demandList();
                                Set<Long> offices = new HashSet<>();
                                String cell = "";
                                int serial = 1;
                                for (Pi_annual_demandDTO model : models) {
                                    if (offices.contains(model.officeUnitId)) continue;
                                    else offices.add(model.officeUnitId);
                            %>
                            <tr>
                                <td style="display: none">
                                    <input type="hidden" value="<%=model.officeUnitId%>" id="hidden_office_unit_id">
                                    <input type="hidden"
                                           value="<%=Office_unitsRepository.getInstance().geText(Language, model.officeUnitId)%>"
                                           id="hidden_office_unit_name">
                                    <input type="hidden"
                                           value="<%=Fiscal_yearRepository.getInstance().getText(model.fiscalYear, Language)%>"
                                           id="hidden_fiscal_year_text">
                                    <input type="hidden" id="hidden_has_package_lot" value = "<%=model.hasPackageLot%>">
                                </td>
                                <td>
                                    <%=Utils.getDigits(serial++, Language)%>
                                </td>
                                <td>
                                    <%
                                        cell = Office_unitsRepository.getInstance().geText(Language, model.officeUnitId) + "";
                                    %>
                                    <%=cell%>
                                </td>
                                <td>
                                    <%
                                        cell = Fiscal_yearRepository.getInstance().getText(model.fiscalYear, Language);
                                    %>
                                    <%=cell%>
                                </td>
                                <td>
                                    <button
                                            type="button"
                                            class="btn-sm border-0 shadow bg-light btn-border-radius"
                                            style="color: #ff6b6b;"
                                            onclick="loadPackageLotModal(this)"
                                    >
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .hiddenTr {
        display: none;
    }
</style>