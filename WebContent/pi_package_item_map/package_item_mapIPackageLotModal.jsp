<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    String modalTitle = request.getParameter("modalTitle");
    if (modalTitle == null) {
        modalTitle = UtilCharacter.getDataByLanguage(Language, "প্যাকেজ লটের লিস্ট", "List of Package Lot");
    }
%>

<div class="modal fade bd-example-modal-xl" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_package_item_map_package_lot_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #0098bf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="package_item_mapIPackageLotModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
                        onclick="closePackageLotModal()">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
            <%--------------------------------SELECTED DATA-----------------------------------------%>
            <input type="hidden" id="selected_package_id" value="-1">
            <input type="hidden" id="selected_lot_id" value="-1">
            <input type="hidden" id="selected_package_name" value="-1">
            <input type="hidden" id="selected_lot_name" value="-1">
            <input type="hidden" id="selected_fiscal_year_text" value="-1">
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#search_package_item_map_package_lot_modal').on('show.bs.modal', function () {
        event.preventDefault();
    });

    function closePackageLotModal() {
        $('#search_package_item_map_package_lot_modal').modal('hide');
    }

    function loadItemModal() {
        $('#search_package_item_map_item_modal').modal();
    }

    function loadModalHeader() {
        let officeUnitName = document.getElementById('selected_office_unit_name').value;
        let fiscalYearText = document.getElementById('selected_fiscal_year_text').value;
        document.getElementById('header_office_name_fiscal_year').innerText = officeUnitName + ", " + fiscalYearText;
    }

    function loadPackageLotList() {
        loadModalHeader();
        let officeUnitId = document.getElementById('selected_office_unit_id').value;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let packageLotList = JSON.parse(this.responseText);
                setDataToPackageLotTable(packageLotList.msg);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "Pi_annual_demandServlet?actionType=getPackageLotListForModal&officeUnitId=" + officeUnitId, false);
        xhttp.send();
    }

    function setDataToPackageLotTable(packageLotList) {
        let modalBody = document.getElementById('package-lot-body');
        let tableRow = document.getElementById('template-package-lot-row');
        packageLotList = JSON.parse(packageLotList);
        if(packageLotList.length === 0) {
            return openItemModal(null);
        }
        removeAllChildNodes(modalBody);
        for (let row of packageLotList) {
            let clonedRow = tableRow.cloneNode(true);
            clonedRow.classList.remove('hiddenTr');
            clonedRow.querySelector('#hidden_package_id').value = row['packageId'];
            clonedRow.querySelector('#hidden_lot_id').value = row['lotId'];
            clonedRow.querySelector('#hidden_package_name').value = getValueByLanguage(language, row['packageBn'], row['packageEn']);
            clonedRow.querySelector('#hidden_lot_name').value = getValueByLanguage(language, row['lotBn'], row['lotEn']);
            clonedRow.querySelector('#hidden_fiscal_year').value = row['fiscalYear'];
            clonedRow.querySelector('#serial_number_').innerText = row['serialNum'];
            clonedRow.querySelector('#package_').innerText = getValueByLanguage(language, row['packageBn'], row['packageEn']);
            clonedRow.querySelector('#lot_').innerText = getValueByLanguage(language, row['lotBn'], row['lotEn']);
            clonedRow.querySelector('#package_lot_creation_date_').innerText = row['creationDate'];
            clonedRow.querySelector('#nothi_').innerText = row['nothiNo'];
            modalBody.append(clonedRow);
        }
    }

    function removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }

    function openItemModal(element){
        if(element === null){
            loadItemList(true);
        } else {
            let tableRow = element.parentNode.parentNode;
            let packageId = tableRow.querySelector('#hidden_package_id').value;
            let lotId = tableRow.querySelector('#hidden_lot_id').value;
            let packageName = tableRow.querySelector('#hidden_package_name').value;
            let lotName = tableRow.querySelector('#hidden_lot_name').value;
            let fiscalYear = tableRow.querySelector('#hidden_fiscal_year').value;
            document.getElementById('selected_package_id').value = packageId;
            document.getElementById('selected_lot_id').value = lotId;
            document.getElementById('selected_package_name').value = packageName;
            document.getElementById('selected_lot_name').value = lotName;
            document.getElementById('selected_fiscal_year_text').value = fiscalYear;
            loadItemList(false);
        }
        $('#search_package_item_map_item_modal').modal();
    }
</script>