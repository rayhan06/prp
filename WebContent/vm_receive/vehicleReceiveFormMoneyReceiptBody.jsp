<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vm_requisition.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="static sessionmanager.SessionConstants.VM_REQUISITION_PER_KM_FARE" %>
<%@ page import="static sessionmanager.SessionConstants.*" %>
<%@ page import="util.HttpRequestUtils" %>

<%
Vm_requisitionDTO vm_requisitionDTO=null;
Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO();
vm_requisitionDTO = vm_requisitionDAO.getDTOByID(Long.parseLong(request.getParameter("ID"))) ;
//vm_requisitionDTO = (Vm_requisitionDTO)request.getAttribute("vm_requisitionDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
if(vm_requisitionDTO == null)
{
	vm_requisitionDTO = new Vm_requisitionDTO();

}
System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "paymentStatus";
}
String formTitle = LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO);
String servletName = "Vm_requisitionServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
	boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
String vehicleReceiveBtnUrl = "Vm_requisitionServlet?actionType=vehicleReceiveEditFormPage&ID="+vm_requisitionDTO.iD;
%>




<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_INFO, loginDTO)%>
                        </h3>
                    </div>
                </div>
<%--                <form class="form-horizontal"--%>
<%--                      action="Vm_requisitionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
<%--                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"--%>
<%--                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>
                <form class="form-horizontal"  id="bigform" name="bigform" >

                    <div class="kt-portlet__body form-body">
                        <div class="row">

                            <div class="col-10 offset-1">
                                <div class="onlyborder">
                                    <div class="row">

                                        <div class="col-10">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_INFO, loginDTO)%>
                                                            </h4>
                                                        </div>
                                                     </div>



                                                    <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=vm_requisitionDTO.iD%>' tag='pb_html'/>
                                                    <input type='hidden' class='form-control'  name='paidStatus' id = 'paidStatus'  tag='pb_html'/>
                                                    <input type='hidden' class='form-control'  name='paidAmount' id = 'paidAmount'  tag='pb_html'/>
                                                    <input type='hidden' class='form-control'  name='payment_type' id = 'payment_type'  tag='pb_html'/>

													<div class="form-group row">
														<label class="col-6 col-form-label text-right">
															<%=LM.getText(LC.VM_REQUISITION_ADD_PER_KM_FARE, loginDTO)%>

														</label>
														<div class="col-6 col-form-label" style="text-align: center;">

                                                            <%
                                                                double sum = 0;
                                                                double total_km_fare = vm_requisitionDTO.totalTripDistance*VM_REQUISITION_PER_KM_FARE;
                                                            %>

                                                            <span><%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("Bangla")?
                                                                    (Utils.getDigitBanglaFromEnglish(String.format("%.2f", total_km_fare))):(String.format("%.2f", total_km_fare))%></span>

                                                            <%

                                                                sum = sum+vm_requisitionDTO.totalTripDistance*VM_REQUISITION_PER_KM_FARE;
                                                            %>

														</div>



													</div>

                                                    <div class="form-group row">
                                                        <label class="col-6 col-form-label text-right">
                                                            <%=LM.getText(LC.VM_REQUISITION_ADD_HOURLY_FARE, loginDTO)%>

                                                        </label>
                                                        <div class="col-6 col-form-label" style="text-align: center;">

                                                            <%
                                                                double total_hourly_fare = vm_requisitionDTO.totalTripTime*VEHICLE_REQUISITION_PRICE_PER_HOUR.get(vm_requisitionDTO.givenVehicleType);
                                                            %>

                                                            <span><%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("Bangla")?
                                                                    (Utils.getDigitBanglaFromEnglish(String.format("%.2f", total_hourly_fare))):(String.format("%.2f", total_hourly_fare))%></span>
                                                            <%
                                                                sum = sum+vm_requisitionDTO.totalTripTime*VEHICLE_REQUISITION_PRICE_PER_HOUR.get(vm_requisitionDTO.givenVehicleType);
                                                            %>

                                                        </div>



                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-6 col-form-label text-right">
                                                            <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_PETROL_SUPPLY, loginDTO)%>

                                                        </label>
                                                        <div class="col-6 col-form-label" style="text-align: center;">

                                                            <%

                                                                double totalPetrol=0;
                                                                totalPetrol=vm_requisitionDTO.petrolAmount!=-1?(vm_requisitionDTO.petrolAmount*VM_REQUISITION_PETROL_PER_LITER):0;
                                                                sum = sum+totalPetrol;

                                                            %>

                                                            <span><%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("Bangla")?
                                                                    (Utils.getDigitBanglaFromEnglish(String.format("%.2f", totalPetrol))):(String.format("%.2f", totalPetrol))%></span>

                                                        </div>



                                                    </div>

                                                    <div class="form-group row">

                                                        <div class="col-3 text-right">



                                                        </div>

                                                        <div class="col-9 text-right">

                                                            <hr style="height:1px;border:none;color:grey;background-color:grey;margin-top: 10px;margin-bottom: 10px;" />

                                                        </div>



                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-6 col-form-label text-right">
                                                            <%=LM.getText(LC.VM_REQUISITION_ADD_TOTAL_FARE, loginDTO)%>

                                                        </label>
                                                        <div class="col-6 col-form-label" style="text-align: center;">

                                                            <span id="totalPaidAmount"><%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language.equalsIgnoreCase("Bangla")?
                                                                    (Utils.getDigitBanglaFromEnglish(String.format("%.2f", sum))):(String.format("%.2f", sum))%></span>

                                                        </div>



                                                    </div>


														</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                          </div>
                       </div>
                       <div class="form-actions text-center mb-5">
							<button class="btn btn-success" type="button" onclick="paidClick()"  value="1" name="pay_status_btn"><%=LM.getText(LC.VM_REQUISITION_ADD_PAID, loginDTO)%></button>
                            <button class="btn btn-danger" type="button" onclick="unPaidClick()" value="2" id="pay_status_btn"><%=LM.getText(LC.VM_REQUISITION_ADD_UNPAID, loginDTO)%></button>
					   </div>
                   </form>
               </div>
          </div>
      </div>
 </div>








<script type="text/javascript">

const vehicleReceiveForm = $('#bigform');
function edit_petrol_supply(){
	$("input[name='vehicle_hour_spent']").removeAttr("readonly");
	var input = $("input[name='vehicle_hour_spent']");
	input.css("border","2px solid black");
}

function PreprocessBeforeSubmiting(row, validate)
{

	preprocessDateBeforeSubmitting('startDate', row);
	preprocessDateBeforeSubmitting('endDate', row);
	preprocessTimeBeforeSubmitting('startTime', row);
	preprocessTimeBeforeSubmitting('endTime', row);



	// let data = added_requester_map.keys().next();
	// if(!(data && data.value)){
	// 	toastr.error("Please Select Person");
	// 	return false;
	// }
	// document.getElementById('requesterEmpId_hidden_0').value = JSON.stringify(
	// 		Array.from(added_requester_map.values()));

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vm_requisitionServlet");
}

function init(row)
{

	setDateByStringAndId('startDate_js_' + row, $('#startDate_date_' + row).val());
	setDateByStringAndId('endDate_js_' + row, $('#endDate_date_' + row).val());
	setTimeById('startTime_js_' + row, $('#startTime_time_' + row).val());
	setTimeById('endTime_js_' + row, $('#endTime_time_' + row).val());


}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();





});

async function paidMethod(){
    const paymentType = await Swal.fire({
        title: '<%=LM.getText(LC.VM_REQUISITION_ADD_PAYMENT_ACCEPTANCE, loginDTO)%>',
        confirmButtonText: '<%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_EDIT_SUBMIT_BUTTON, loginDTO)%>',
        cancelButtonText: '<%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_CANCEL_BUTTON, loginDTO)%>',
        input: 'select',
        allowOutsideClick: false,
        inputOptions: {

            '<%=VM_REQUISITION_PAYMENT_TYPE_NAGAD%>'        :'<%=LM.getText(LC.VM_REQUISITION_ADD_NAGAD_CASH, loginDTO)%>',
            '<%=VM_REQUISITION_PAYMENT_TYPE_CHEQUE%>'        :'<%=LM.getText(LC.VM_REQUISITION_ADD_CHEQUE, loginDTO)%>',

        },
        inputPlaceholder: '<%=LM.getText(LC.JOB_JOB, loginDTO)%>',
        showCancelButton: true,
        inputValidator: (value) => {
            return new Promise((resolve) => {
                if (value.localeCompare("1")==0 || value.localeCompare("2")==0) {
                    resolve();
                }
                else{
                    resolve('<%=LM.getText(LC.JOB_JOB, loginDTO)%>');
                }
            })
        }
    });

    if (paymentType.value !== undefined) {

        document.getElementById("payment_type").value = paymentType.value;
        //document.forms.bigform.submit();
        submitForm();
    }
}

var numbers = {
    "\u09E6": 0,
    "\u09E7": 1,
    "\u09E8": 2,
    "\u09E9": 3,
    "\u09EA": 4,
    "\u09EB": 5,
    "\u09EC": 6,
    "\u09ED": 7,
    "\u09EE": 8,
    "\u09EF": 9
};

function convertBNToEN(input) {

    let output = [];

    for (let i = 0; i < input.length; ++i) {
        if (numbers.hasOwnProperty(input[i])) {
            output.push(numbers[input[i]]);
        } else {
            output.push(input[i]);
        }
    }

    return output.join('');
}

function paidClick(){
    //paidAmount

    document.getElementById("paidAmount").value = convertBNToEN(document.getElementById("totalPaidAmount").innerText);
    //document.getElementById("paidStatus").value = <%=VM_REQUISITION_PAYMENT_GIVEN_YES%>;
    document.getElementById("paidStatus").value = <%=CommonApprovalStatus.PAID.getValue()%>;
    paidMethod();
    //document.forms.bigform.submit();
}
function unPaidClick(){

    document.getElementById("paidAmount").value = convertBNToEN(document.getElementById("totalPaidAmount").innerText);
    //document.getElementById("paidStatus").value = <%=VM_REQUISITION_PAYMENT_GIVEN_NO%>;
    document.getElementById("paidStatus").value = <%=CommonApprovalStatus.UNPAID.getValue()%>;
    document.getElementById("payment_type").value = -1;
    //document.forms.bigform.submit();
    submitForm();
}

function submitForm(){


    if($("#bigform").valid()){
        $.ajax({
            type : "POST",
            url : "Vm_requisitionServlet?actionType=paymentStatus",
            data : vehicleReceiveForm.serialize(),
            dataType : 'JSON',
            success : function(response) {

                if(response.responseCode === 0){
                    showToastSticky(response.msg,response.msg);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {

                toastr.error('Can not update payment');
            }
        });
    }else{

    }
}

var child_table_extra_id = <%=childTableStartingID%>;





function insertAddressRow (tableId) {
	var myTable = document.getElementById(tableId);
	var currentIndex = myTable.rows.length;
	var currentRow = myTable.insertRow(-1);

	var linksBox = document.createElement("input");
	linksBox.setAttribute("name", "startAddresses");
	linksBox.setAttribute("id", "startAddress_TextField_" + currentIndex);
	linksBox.setAttribute("type", "text");
	linksBox.setAttribute("class", "form-control");
	linksBox.setAttribute("placeholder", "Road Number, House Number etc");
	linksBox.setAttribute("tag", "pb_html");

	var addRowBox = document.createElement("input");
	addRowBox.setAttribute("type", "button");
	addRowBox.setAttribute("value", "- ");
	addRowBox.setAttribute("onclick", "deleteAddressRow('" + tableId + "', this);");
	addRowBox.setAttribute("class", "button");

	var currentCell = currentRow.insertCell(-1);
	currentCell.appendChild(linksBox);

	currentCell = currentRow.insertCell(-1);
	currentCell.appendChild(addRowBox);
}


function deleteAddressRow (tableId, button) {
	var myTable = document.getElementById(tableId);
	var rowIndex = button.parentNode.parentNode.rowIndex;
	myTable.deleteRow(rowIndex);
}




// TODO: EMPLOYEE SEARCH MODAL
// select action of modal's add button

// map to store and send added employee data
// map to store and send added employee data


<%
List<EmployeeSearchIds> addedEmpIdsList = null;
if(actionName.equals("edit")) {
	addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
	(vm_requisitionDTO.requesterEmpId,vm_requisitionDTO.requesterOfficeUnitId,vm_requisitionDTO.requesterOrgId));
}
%>

<%--added_requester_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);--%>



/* IMPORTANT
 * This map is converts table name to the table's added employees map
 */
// table_name_to_collcetion_map = new Map(
// 		[
// 			['tagged_requester_table', {
// 				info_map: added_requester_map,
// 				isSingleEntry: true
// 			}]
// 		]
// );

// modal row button desatination table in the page
modal_button_dest_table = 'none';




</script>


<style>
	.required{
		color: red;
	}


</style>



