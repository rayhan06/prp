<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vm_requisition.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="static sessionmanager.SessionConstants.PetrolStatusMap" %>

<%
	Vm_requisitionDTO vm_requisitionDTO;
	vm_requisitionDTO = (Vm_requisitionDTO)request.getAttribute("vm_requisitionDTO");
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
	if(vm_requisitionDTO == null)
	{
		vm_requisitionDTO = new Vm_requisitionDTO();

	}
	System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);

	String actionName;
	System.out.println("actionType = " + request.getParameter("actionType"));
	if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
	{
		actionName = "add";
	}
	else
	{
		actionName = "receiveVehicle";
	}
	String formTitle = LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO);
	String servletName = "Vm_requisitionServlet";
	String fileColumnName = "";

	String ID = request.getParameter("ID");
	if(ID == null || ID.isEmpty())
	{
		ID = "0";
	}
	System.out.println("ID = " + ID);
	int i = 0;

	String value = "";

	int childTableStartingID = 1;

	boolean isPermanentTable = true;
	String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
	boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	CommonDAO.language = Language;
	CatDAO.language = Language;
	String vehicleReceiveBtnUrl = "Vm_requisitionServlet?actionType=vehicleReceiveEditFormPage&ID="+vm_requisitionDTO.iD;
%>

<!-- begin:: Subheader -->
<%--<div class="ml-auto mr-3 mt-4">--%>
<%--	<%--%>
<%--		if (true) {--%>
<%--	%>--%>
<%--	<button type="button" class="btn" id='printer'--%>
<%--			onclick="downloadPdf('prescription.pdf','modalbody')">--%>
<%--		&lt;%&ndash;        <%=LM.getText(LC.HM_PDF, loginDTO)%>&ndash;%&gt;--%>
<%--		<i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--	</button>--%>
<%--	<button type="button" class="btn" id='printer'--%>
<%--			onclick="printDiv('modalbody')">--%>
<%--		<i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--		&lt;%&ndash;        <%=LM.getText(LC.HM_PRINT, loginDTO)%>&ndash;%&gt;--%>
<%--	</button>--%>
<%--	<%--%>
<%--		}--%>
<%--	%>--%>
<%--</div>--%>
<%--<!-- end:: Subheader -->--%>

<%--<div class="kt-content" id="kt_content">--%>
<%--	<div class="row" id="modalbody">--%>
<%--		<div class="kt-portlet">--%>
<%--			<div class="kt-portlet__body">--%>

<%--				<div class="row">--%>
<%--					<div class="col-12 row">--%>
<%--						<div class="col-6">--%>
<%--							<%=LM.getText(LC.VM_REQUISITION_ADD_NAME_APPELLATE_OFFICER, loginDTO)%>--%>
<%--						</div>--%>
<%--						<div class="col-6">--%>
<%--							<span><%=isLanguageEnglish ? (   vm_requisitionDTO.requesterNameEn )--%>
<%--									: (  vm_requisitionDTO.requesterNameBn )%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-12 row">--%>
<%--						<div class="col-6">--%>
<%--							<%=LM.getText(LC.PROMOTION_HISTORY_SEARCH_RANKCAT, loginDTO)%>--%>
<%--						</div>--%>
<%--						<div class="col-6">--%>
<%--							<%--%>
<%--								String postName = isLanguageEnglish ? (vm_requisitionDTO.requesterOfficeUnitOrgNameEn + ", " + vm_requisitionDTO.requesterOfficeUnitNameEn)--%>
<%--										: (vm_requisitionDTO.requesterOfficeUnitOrgNameBn + ", " + vm_requisitionDTO.requesterOfficeUnitNameBn);--%>
<%--							%>--%>
<%--							<span><%=postName%> </span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-12 row">--%>
<%--						<div class="col-6">--%>
<%--							<%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>--%>
<%--						</div>--%>
<%--						<div class="col-6">--%>
<%--							<span><%=UtilCharacter.convertDataByLanguage(Language,vm_requisitionDTO.requesterPhoneNum)%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--				</div>--%>

<%--				<div class="row">--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<%--%>
<%--								String val ="";--%>
<%--								val = CatDAO.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);--%>
<%--							%>--%>
<%--							<span><%=Utils.getDigits(val, Language)%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<%--%>
<%--								value = vm_requisitionDTO.startDate + "";--%>
<%--							%>--%>
<%--							<%--%>
<%--								String formatted_startDate = dateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--							%>--%>


<%--							<span><%=Utils.getDigits(formatted_startDate, Language)%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<%--%>
<%--								val = vm_requisitionDTO.startAddress + "";--%>
<%--								val = val.replaceAll("\\$", " -> ");--%>
<%--							%>--%>

<%--							<span><%=val%> </span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<%--%>
<%--								val = vm_requisitionDTO.endAddress + "";--%>
<%--								val = val.replaceAll("\\$", " -> ");--%>
<%--							%>--%>

<%--							<span><%=val%> </span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<%--%>
<%--								val = vm_requisitionDTO.startTime + "";--%>
<%--							%>--%>
<%--							<span><%=Utils.getDigits(val, Language)%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<%--%>
<%--								val = vm_requisitionDTO.endTime + "";--%>
<%--							%>--%>
<%--							<span><%=Utils.getDigits(val, Language)%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_WHERE_WHEN_TO_REPORT, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_APPROXIMATE_TIME, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_USAGE_DETAILS, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.CANDIDATE_LIST_APPLY_DATE, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<%--%>
<%--								value = vm_requisitionDTO.insertionDate + "";--%>
<%--							%>--%>
<%--							<%--%>
<%--								formatted_startDate = dateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--							%>--%>


<%--							<span><%=Utils.getDigits(formatted_startDate, Language)%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_PRIOR_BILL, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERS_NAME, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<span><%=isLanguageEnglish ? (   vm_requisitionDTO.driverNameEn )--%>
<%--									: (  vm_requisitionDTO.driverNameBn )%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				--%>

<%--			</div>--%>
<%--		</div>--%>
<%--	</div>--%>
<%--</div>--%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
	<div class="row">
		<div class="col-lg-12">
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title prp-page-title">
							<i class="fa fa-gift"></i>&nbsp;
							<%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_INFO, loginDTO)%>
						</h3>
					</div>
				</div>
				<%--                <form class="form-horizontal"--%>
				<%--                      action="Vm_requisitionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
				<%--                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"--%>
				<%--                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>

				<form class="form-horizontal"  id="bigform" name="bigform" >
					<div class="kt-portlet__body form-body">
						<div class="row">

							<div class="col-12 ">
								<div class="onlyborder">
									<div class="row">

										<div class="col-10 offset-1">
											<div class="row">
												<div class="col-12" >
													<div class="sub_title_top">
														<div class="sub_title">
															<h4 style="background: white"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_INFO, loginDTO)%>
															</h4>
														</div>
													</div>



													<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=vm_requisitionDTO.iD%>' tag='pb_html'/>
													<input type='hidden' class='form-control'  name='trip_startDate' id = 'trip_startDate' value='<%=vm_requisitionDTO.startDate%>' tag='pb_html'/>
													<input type='hidden' class='form-control'  name='trip_startTime' id = 'trip_startTime' value='<%=vm_requisitionDTO.startTime%>' tag='pb_html'/>




													<div class="form-group row">
														<label class="col-2 col-form-label text-right">
															<%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_RECEIVE_DATE, loginDTO)%>

														</label>
														<div class="col-4">

															<jsp:include page="/date/date.jsp">
																<jsp:param name="DATE_ID" value="vehicle_receive_date"></jsp:param>
																<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
															</jsp:include>
															<input type='hidden' name='vehicle_receive_date' id='vrd' value= '<%=dateFormat.format(new Date(vm_requisitionDTO.startDate))%>' tag='pb_html'/>

														</div>

														<label class="col-2  col-form-label text-right">
															<%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_RECEIVE_TIME, loginDTO)%>

														</label>
														<div class="col-4">


															<jsp:include page="/time/time.jsp">
																<jsp:param name="TIME_ID" value="jsTimeIn"></jsp:param>
																<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																<jsp:param name="IS_AMPM" value="true"></jsp:param>
															</jsp:include>
															<input type='hidden' class='form-control' name='inTime' id='inTime' value='' tag='pb_html'/>

														</div>

													</div>

													<div class="form-group row">
														<label class="col-2 col-form-label text-right">
															<%=UtilCharacter.getDataByLanguage(Language, "যাত্রার শেষে মিটার রিডিং", "Meter Reading When Journey End")%>
														</label>
														<div class="col-4">
															<input type='text' class='form-control '  name='journey_end_meter_reading'
																   id = 'journey_end_meter_reading' value=''
																   onchange="calculateTravelledDistance()" tag='pb_html'/>
														</div>

														<label class="col-2 col-form-label text-right">
															<%=UtilCharacter.getDataByLanguage(Language, "যাত্রার শুরুতে মিটার রিডিং", "Meter Reading When Journey Start")%>
														</label>
														<div class="col-4">
															<input type='text' class='form-control '  name='journey_start_meter_reading'
																   id = 'journey_start_meter_reading' value=''
																   onchange="calculateTravelledDistance()" tag='pb_html'/>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-2 col-form-label text-right">
															<%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_TRAVELLED_DISTANCE, loginDTO)%>

														</label>
														<div class="col-4">

															<%String tripDistance = String.valueOf(vm_requisitionDTO.totalTripDistance==-1?0:vm_requisitionDTO.totalTripDistance);%>

															<input type='text' class='form-control'  name='vehicle_travelled_distance'
																   id = 'vehicle_travelled_distance' value='<%=tripDistance%>'
																   tag='pb_html'/>

														</div>

														<label class="col-2  col-form-label text-right">
															<%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_PETROL_SUPPLY, loginDTO)%>

														</label>
														<div class="col-4">

															<%--															<select name="select_petrol_supply" style="width:100%;" class='form-control' id='select_petrol_supply' tag='pb_html'>--%>

															<%--																<%--%>
															<%--																	for (Map.Entry<Integer,String> entry : PetrolStatusMap.entrySet())    {--%>

															<%--																		out.println("<option value="+entry.getKey()+" >"+LM.getText(entry.getKey()==1?LC.YES_NO_YES:LC.YES_NO_NO,loginDTO)+"</option>");--%>

															<%--																	}--%>
															<%--																%>--%>

															<%--															</select>--%>

															<input type="checkbox" class="form-control-sm" name="select_petrol_supply" id="select_petrol_supply"  tag="pb_html">



														</div>

													</div>

													<div class="form-group row">
														<label class="col-2 col-form-label text-right">
															<%=UtilCharacter.getDataByLanguage(Language, "পেট্রোল সরবরাহ(লিটার)", "Petrol Supply(Litre)")%>
														</label>
														<div class="col-4">
															<%String petrol_required = String.valueOf(vm_requisitionDTO.petrolGiven==-1?0:vm_requisitionDTO.petrolGiven);%>
															<input type='text' class='form-control '  name='vehicle_petrol_required' id = 'vehicle_petrol_required' value='<%=petrol_required%>'  tag='pb_html' />
														</div>

                                                        <label class="col-2 col-form-label text-right">
                                                            <%=UtilCharacter.getDataByLanguage(Language, "পেট্রোল খরচ(লিটার)", "Petrol Consumption(Litre)")%>
                                                        </label>
                                                        <div class="col-4">
                                                            <%String petrol_consumption = String.valueOf(vm_requisitionDTO.petrolConsumption==-1?0:vm_requisitionDTO.petrolConsumption);%>
                                                            <input type='text' class='form-control '  name='vehicle_petrol_consumption' id = 'vehicle_petrol_consumption' value='<%=petrol_consumption%>'  tag='pb_html' />
                                                        </div>
													</div>
													<div class="form-group row">
														<label class="col-2  col-form-label text-right">
															<%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_HOURS_SPENT, loginDTO)%>

														</label>
														<div class="col-3">

															<input type='text' class='form-control '  name='vehicle_hour_spent' id = 'vehicle_hour_spent'   tag='pb_html' readonly/>

														</div>

                                                        <div class="col-1 " style="">

                                                            <button type="button" style="background-color: white;border:none;color: darkred;margin: 0 auto;" onclick='edit_petrol_supply()' id='edit_petrol_supply_id'><i class="fas fa-pencil-alt" ></i></button>

                                                        </div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions text-center mb-5">
						<%--	                        <a class="btn btn-primary" href="<%=vehicleReceiveBtnUrl%>"><%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_EDIT_SUBMIT_BUTTON, loginDTO)%></a>--%>
						<button class="btn btn-success" type="button" onclick="submitForm()"><%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_EDIT_SUBMIT_BUTTON, loginDTO)%></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>








<script type="text/javascript">

	const vehicleReceiveForm = $('#bigform');

	// $('form').on('submit', () => {
	//
	// 	document.getElementById('vrd').value = getDateTimestampById('vehicle_receive_date');
	// 	document.getElementById('inTime').value = getTimeById('jsTimeIn', true);
	// 	return dateValidator('vehicle_receive_date', true) && timeNotEmptyValidator('jsTimeIn');
	// });

	function checkDateAndTimeValidator(){
		document.getElementById('vrd').value = getDateTimestampById('vehicle_receive_date');
		document.getElementById('inTime').value = getTimeById('jsTimeIn', true);
		return dateValidator('vehicle_receive_date', true) && timeNotEmptyValidator('jsTimeIn');
	}

	//action="Vm_requisitionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"

	function submitForm(){
		//buttonStateChange(true);
		//PreprocessBeforeSubmiting(0,'<%=actionName%>')
		checkDateAndTimeValidator();

		if($("#bigform").valid()){
			$.ajax({
				type : "POST",
				url : "Vm_requisitionServlet?actionType=receiveVehicle",
				data : vehicleReceiveForm.serialize(),
				dataType : 'JSON',
				success : function(response) {

					if(response.responseCode === 0){
						//$('#toast_message').css('background-color','#ff6063');
						showToastSticky(response.msg,response.msg);
						//buttonStateChange(false);
					}else if(response.responseCode === 200){
						window.location.replace(getContextPath()+response.msg);
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
					// toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
					// 		+ ", Message: " + errorThrown);
					//buttonStateChange(false);

					toastr.error('Can not receive vehicle: '+errorThrown);
				}
			});
		}else{
			//buttonStateChange(false);
		}
	}


	function edit_petrol_supply(){
		$("input[name='vehicle_hour_spent']").removeAttr("readonly");
		var input = $("input[name='vehicle_hour_spent']");
		input.css("border","2px solid black");
	}

	function PreprocessBeforeSubmiting(row, validate)
	{

		preprocessDateBeforeSubmitting('startDate', row);
		preprocessDateBeforeSubmitting('endDate', row);
		preprocessTimeBeforeSubmitting('startTime', row);
		preprocessTimeBeforeSubmitting('endTime', row);



		// let data = added_requester_map.keys().next();
		// if(!(data && data.value)){
		// 	toastr.error("Please Select Person");
		// 	return false;
		// }
		// document.getElementById('requesterEmpId_hidden_0').value = JSON.stringify(
		// 		Array.from(added_requester_map.values()));

		return true;
	}


	function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
	{
		addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vm_requisitionServlet");
	}

	function init(row)
	{

		// setDateByStringAndId('startDate_js_' + row, $('#startDate_date_' + row).val());
		// setDateByStringAndId('endDate_js_' + row, $('#endDate_date_' + row).val());
		// setTimeById('startTime_js_' + row, $('#startTime_time_' + row).val());
		// setTimeById('endTime_js_' + row, $('#endTime_time_' + row).val());

		setDateByStringAndId('vehicle_receive_date', $('#vrd').val());


	}

	var row = 0;
	$(document).ready(function(){
		init(row);
		CKEDITOR.replaceAll();



		$('#vehicle_receive_date').on('datepicker.change', (event, param) => {
			var date = getDateStringById('vehicle_receive_date','MM/DD/YYYY');   //setDateByStringAndId ,getDateTimestampById
			var time = getTimeById("jsTimeIn");
			//console.log('herer 21 time: '+time.length+" time: "+time);
			if(date.length>5 && date!=null && time.length>5 && time!=null ){


				document.getElementById('vehicle_hour_spent').value = totalTimeSpent(date,time);
			}
		});
		$('#jsTimeIn').on('timepicker.change', (event, param) => {
			var date = getDateStringById('vehicle_receive_date','MM/DD/YYYY');
			var time = getTimeById("jsTimeIn");

			if(date.length>5 && date!=null && time.length>5 && time!=null){

				document.getElementById('vehicle_hour_spent').value = totalTimeSpent(date,time);

			}
		});



		$("#bigform").validate({
			errorClass: 'error is-invalid',
			validClass: 'is-valid',
			rules: {
				inTime: "required",
				vehicle_receive_date: "required",
				// vehicle_travelled_distance: "required",
				vehicle_hour_spent : "required",
				journey_start_meter_reading : "required",
				journey_end_meter_reading : "required",


			},
			messages: {
				vehicle_receive_date: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter vehicle receive date" : "অনুগ্রহ করে যানবাহন গ্রহণের তারিখ দিন"%>',
				inTime: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter vehicle receive time" : "অনুগ্রহ করে যানবাহন গ্রহণের সময় দিন"%>',
				<%--vehicle_travelled_distance: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter vehicle receive date" : "অনুগ্রহ করে ঢোকার তারিখ দিন"%>',--%>
				vehicle_hour_spent: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter vehicle hours spent" : "অনুগ্রহ করে কত কিলোমিটার যাত্রা হয়েছে দিন"%>',
				journey_start_meter_reading: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter meter reading when journey start" : "অনুগ্রহ করে যাত্রার শুরুতে মিটার রিডিং দিন"%>',
				journey_end_meter_reading: '<%=Language.equalsIgnoreCase("English") ? "Kindly enter meter reading when journey end" : "অনুগ্রহ করে যাত্রার শেষে মিটার রিডিং দিন"%>',

			}
		});



	});


	function totalTimeSpent(date,time){
		let startDate = parseInt (document.getElementById('trip_startDate').value);
		startDate = new Date(startDate).toLocaleDateString();
		let startTime = document.getElementById('trip_startTime').value;

		let endDate = date
		endDate = new Date(endDate).toLocaleDateString();
		let endTime = time;

		let startDateTime = formatDate(startDate)+" "+startTime;
		let endDateTime = formatDate(endDate)+" "+endTime;

		let diff = (new Date(endDateTime) - new Date(startDateTime));
		let hours = (diff / (1000*60*60)).toFixed(2);

		return hours;


	}

	function formatDate(date){

		date = date.split("/");
		let month = date[0];
		let day = date[1];
		let year = date[2];
		if(month<10){
			month = "0"+month;
		}
		if(day<10){
			day = "0"+day;
		}
		let formattedDate  = month+"/"+day+"/"+year;
		return formattedDate;

	}

	var child_table_extra_id = <%=childTableStartingID%>;


	function calculateTravelledDistance(){
		let meterReadingInitial = document.getElementById('journey_start_meter_reading').value;
		let meterReadingEnd = document.getElementById('journey_end_meter_reading').value;
		let distanceTravelledNode = document.getElementById('vehicle_travelled_distance');

		if(!meterReadingInitial || !meterReadingEnd) distanceTravelledNode.value = '0';
		else distanceTravelledNode.value = meterReadingEnd - meterReadingInitial;
	}


	function insertAddressRow (tableId) {
		var myTable = document.getElementById(tableId);
		var currentIndex = myTable.rows.length;
		var currentRow = myTable.insertRow(-1);

		var linksBox = document.createElement("input");
		linksBox.setAttribute("name", "startAddresses");
		linksBox.setAttribute("id", "startAddress_TextField_" + currentIndex);
		linksBox.setAttribute("type", "text");
		linksBox.setAttribute("class", "form-control");
		linksBox.setAttribute("placeholder", "Road Number, House Number etc");
		linksBox.setAttribute("tag", "pb_html");

		var addRowBox = document.createElement("input");
		addRowBox.setAttribute("type", "button");
		addRowBox.setAttribute("value", "- ");
		addRowBox.setAttribute("onclick", "deleteAddressRow('" + tableId + "', this);");
		addRowBox.setAttribute("class", "button");

		var currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(linksBox);

		currentCell = currentRow.insertCell(-1);
		currentCell.appendChild(addRowBox);
	}


	function deleteAddressRow (tableId, button) {
		var myTable = document.getElementById(tableId);
		var rowIndex = button.parentNode.parentNode.rowIndex;
		myTable.deleteRow(rowIndex);
	}




	// TODO: EMPLOYEE SEARCH MODAL
	// select action of modal's add button

	// map to store and send added employee data
	// map to store and send added employee data


	<%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("edit")) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (vm_requisitionDTO.requesterEmpId,vm_requisitionDTO.requesterOfficeUnitId,vm_requisitionDTO.requesterOrgId));
    }
    %>

	<%--added_requester_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);--%>



	/* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
	// table_name_to_collcetion_map = new Map(
	// 		[
	// 			['tagged_requester_table', {
	// 				info_map: added_requester_map,
	// 				isSingleEntry: true
	// 			}]
	// 		]
	// );

	// modal row button desatination table in the page
	modal_button_dest_table = 'none';




</script>


<style>
	.required{
		color: red;
	}


</style>



