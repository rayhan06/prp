<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_requisition.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>

<%
    Vm_requisitionDTO vm_requisitionDTO;
    vm_requisitionDTO = (Vm_requisitionDTO) request.getAttribute("vm_requisitionDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (vm_requisitionDTO == null) {
        vm_requisitionDTO = new Vm_requisitionDTO();

    }
    System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_requisitionServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String vehicleReceiveBtnUrl = "Vm_requisitionServlet?actionType=vehicleReceiveEditFormPage&ID=" + vm_requisitionDTO.iD;

    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_requisitionDTO.givenVehicleId);

    String context = request.getContextPath() + "/";

    //System.out.println("vm_requisitionDTO.givenVehicleId in jsp: "+vm_requisitionDTO.givenVehicleId);

%>

<!-- begin:: Subheader -->
<%--<div class="ml-auto mr-3 mt-4">--%>
<%--	<%--%>
<%--		if (true) {--%>
<%--	%>--%>
<%--	<button type="button" class="btn" id='printer'--%>
<%--			onclick="downloadPdf('prescription.pdf','modalbody')">--%>
<%--		           <%=LM.getText(LC.HM_PDF, loginDTO)%> % --%>
<%--		<i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--	</button>--%>
<%--	<button type="button" class="btn" id='printer'--%>
<%--			onclick="printDiv('modalbody')">--%>
<%--		<i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>--%>
<%--		           <%=LM.getText(LC.HM_PRINT, loginDTO)%> % --%>
<%--	</button>--%>
<%--	<%--%>
<%--		}--%>
<%--	%>--%>
<%--</div>--%>
<%--<!-- end:: Subheader -->--%>

<%--<div class="kt-content" id="kt_content">--%>
<%--	<div class="row" id="modalbody2">--%>
<%--		<div class="kt-portlet">--%>
<%--			<div class="kt-portlet__body">--%>

<%--				<div class="row">--%>
<%--					<div class="col-12 row">--%>
<%--						<div class="col-6">--%>
<%--							<%=LM.getText(LC.VM_REQUISITION_ADD_NAME_APPELLATE_OFFICER, loginDTO)%>--%>
<%--						</div>--%>
<%--						<div class="col-6">--%>
<%--							<span><%=isLanguageEnglish ? (   vm_requisitionDTO.requesterNameEn )--%>
<%--									: (  vm_requisitionDTO.requesterNameBn )%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-12 row">--%>
<%--						<div class="col-6">--%>
<%--							<%=LM.getText(LC.PROMOTION_HISTORY_SEARCH_RANKCAT, loginDTO)%>--%>
<%--						</div>--%>
<%--						<div class="col-6">--%>

<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-12 row">--%>
<%--						<div class="col-6">--%>
<%--							<%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>--%>
<%--						</div>--%>
<%--						<div class="col-6">--%>
<%--							<span><%=UtilCharacter.convertDataByLanguage(Language,vm_requisitionDTO.requesterPhoneNum)%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--				</div>--%>

<%--				<div class="row">--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>

<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>

<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>

<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>

<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>

<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>

<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_WHERE_WHEN_TO_REPORT, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_APPROXIMATE_TIME, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_USAGE_DETAILS, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.CANDIDATE_LIST_APPLY_DATE, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>


<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_PRIOR_BILL, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERS_NAME, loginDTO)%></div>--%>
<%--						<div class="col-6">--%>
<%--							<span><%=isLanguageEnglish ? (   vm_requisitionDTO.driverNameEn )--%>
<%--									: (  vm_requisitionDTO.driverNameBn )%></span>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<div class="col-6 row">--%>
<%--						<div class="col-6"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%></div>--%>
<%--						<div class="col-6"></div>--%>
<%--					</div>--%>
<%--				</div>--%>


<%--			</div>--%>
<%--		</div>--%>
<%--	</div>--%>
<%--</div>--%>

<style>
    .form-group {
        margin-bottom: 1rem !important;
    }

    .required {
        color: red;
    }
</style>


<!-- begin:: Subheader -->
<div class="ml-auto mr-3 mt-4">
    <%
        if (true) {
    %>
    <button type="button" class="btn" id='printer'
            onclick="downloadPdf('prescription.pdf','modalbody')">
        <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <button type="button" class="btn" id='printer'
            onclick="printDiv('modalbody')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
    <%
        }
    %>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="row" id="modalbody">
        <div class="kt-portlet">
            <%-- remove the class "bg-secondary" and add background color according to design bellow--%>
            <div class="kt-portlet__body m-3" style="background-color: #f6f9fb">

                <div class="row">
                    <div class="col-12 text-center">
                        <img
                                width="10%"
                                src="<%=context%>assets/static/parliament_logo.png"
                                alt="logo"
                                class="logo-default"
                        />
                        <h2 class="mt-2 text-color">
                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_BANGLADESH_NATIONAL_PARLIAMENT, loginDTO)%>
                        </h2>
                        <h4 class="text-dark">
                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_TRANSPORT_BRANCH, loginDTO)%>
                        </h4>
                        <h5 class="text-dark">
                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTED_GOVERNMENT_VEHICLE_RECEIPT, loginDTO)%>
                        </h5>
                    </div>
                </div>

                <div class="row p-5">
                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_NAME_APPELLATE_OFFICER, loginDTO)%>
                            </label>
                            <div class="col-10">
                                <%=isLanguageEnglish ? (vm_requisitionDTO.requesterNameEn) : (vm_requisitionDTO.requesterNameBn)%>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.PROMOTION_HISTORY_SEARCH_RANKCAT, loginDTO)%>
                            </label>
                            <div class="col-10">
                                <%
                                    String postName = isLanguageEnglish ? (vm_requisitionDTO.requesterOfficeUnitOrgNameEn + ", " + vm_requisitionDTO.requesterOfficeUnitNameEn)
                                            : (vm_requisitionDTO.requesterOfficeUnitOrgNameBn + ", " + vm_requisitionDTO.requesterOfficeUnitNameBn);
                                %>
                                <%=postName%>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>
                            </label>
                            <div class="col-10">
                                <%=UtilCharacter.convertDataByLanguage(Language, vm_requisitionDTO.requesterPhoneNum)%>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    String val = "";
                                    val = CatRepository.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
                                %>
                                <%=val%>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    value = vm_requisitionDTO.startDate + "";
                                %>
                                <%
                                    String formatted_startDate = dateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <%=Utils.getDigits(formatted_startDate, Language)%>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    val = vm_requisitionDTO.startAddress + "";
                                    val = val.replaceAll("\\$", " -> ");
                                %>
                                <%=val%>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    val = vm_requisitionDTO.endAddress + "";
                                    val = val.replaceAll("\\$", " -> ");
                                %>
                                <span><%=val%> </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    val = vm_requisitionDTO.startTime + "";
                                %>
                                <span><%=Utils.getDigits(val, Language)%></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    val = vm_requisitionDTO.endTime + "";
                                %>
                                <span><%=Utils.getDigits(val, Language)%></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_WHERE_WHEN_TO_REPORT, loginDTO)%>
                            </label>
                            <div class="col-8">

                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_APPROXIMATE_TIME, loginDTO)%>
                            </label>
                            <div class="col-8">

                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    val = CatRepository.getName(Language, "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat);
                                %>
                                <%=val%>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_USAGE_DETAILS, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    val = vm_requisitionDTO.tripDescription;
                                %>
                                <%=val%>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.CANDIDATE_LIST_APPLY_DATE, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    value = vm_requisitionDTO.insertionDate + "";
                                %>
                                <%
                                    formatted_startDate = dateFormat.format(new Date(Long.parseLong(value)));
                                %>
                                <span><%=Utils.getDigits(formatted_startDate, Language)%></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_PRIOR_BILL, loginDTO)%>
                            </label>
                            <div class="col-8">

                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERS_NAME, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <span>
                                    <%=isLanguageEnglish ? (vm_requisitionDTO.driverNameEn) : (vm_requisitionDTO.driverNameBn)%>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label">
                                <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%if(vm_vehicleDTO!=null){%>
                                    <%=UtilCharacter.convertDataByLanguage(Language, vm_vehicleDTO.regNo)%>
                                <%}%>


                            </div>
                        </div>
                    </div>
<%--                    <div class="col-6">--%>
<%--                        <div class="form-group row d-flex align-items-center">--%>
<%--                            <label class="col-4 col-form-label">--%>
<%--                                <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLE_NUMBER, loginDTO)%>--%>
<%--                            </label>--%>
<%--                            <div class="col-8">--%>
<%--                                <%=UtilCharacter.convertDataByLanguage(Language, vm_vehicleDTO.regNo)%>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
                </div>
                <div class="form-actions text-right">
                    <a class="btn submit-btn text-white shadow btn-border-radius" href="<%=vehicleReceiveBtnUrl%>">
                        <%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_BUTTON, loginDTO)%>
                    </a>
                    <%--<button class="btn btn-success" type="submit"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_SUBMIT_BUTTON, loginDTO)%></button>--%>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {

        preprocessDateBeforeSubmitting('startDate', row);
        preprocessDateBeforeSubmitting('endDate', row);
        preprocessTimeBeforeSubmitting('startTime', row);
        preprocessTimeBeforeSubmitting('endTime', row);


        // let data = added_requester_map.keys().next();
        // if(!(data && data.value)){
        // 	toastr.error("Please Select Person");
        // 	return false;
        // }
        // document.getElementById('requesterEmpId_hidden_0').value = JSON.stringify(
        // 		Array.from(added_requester_map.values()));

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_requisitionServlet");
    }

    function init(row) {

        setDateByStringAndId('startDate_js_' + row, $('#startDate_date_' + row).val());
        setDateByStringAndId('endDate_js_' + row, $('#endDate_date_' + row).val());
        setTimeById('startTime_js_' + row, $('#startTime_time_' + row).val());
        setTimeById('endTime_js_' + row, $('#endTime_time_' + row).val());


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    function insertAddressRow(tableId) {
        var myTable = document.getElementById(tableId);
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);

        var linksBox = document.createElement("input");
        linksBox.setAttribute("name", "startAddresses");
        linksBox.setAttribute("id", "startAddress_TextField_" + currentIndex);
        linksBox.setAttribute("type", "text");
        linksBox.setAttribute("class", "form-control");
        linksBox.setAttribute("placeholder", "Road Number, House Number etc");
        linksBox.setAttribute("tag", "pb_html");

        var addRowBox = document.createElement("input");
        addRowBox.setAttribute("type", "button");
        addRowBox.setAttribute("value", "- ");
        addRowBox.setAttribute("onclick", "deleteAddressRow('" + tableId + "', this);");
        addRowBox.setAttribute("class", "button");

        var currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(linksBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(addRowBox);
    }


    function deleteAddressRow(tableId, button) {
        var myTable = document.getElementById(tableId);
        var rowIndex = button.parentNode.parentNode.rowIndex;
        myTable.deleteRow(rowIndex);
    }


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("edit")) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (vm_requisitionDTO.requesterEmpId,vm_requisitionDTO.requesterOfficeUnitId,vm_requisitionDTO.requesterOrgId));
    }
    %>

    <%--added_requester_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);--%>



    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    // table_name_to_collcetion_map = new Map(
    // 		[
    // 			['tagged_requester_table', {
    // 				info_map: added_requester_map,
    // 				isSingleEntry: true
    // 			}]
    // 		]
    // );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';


</script>



