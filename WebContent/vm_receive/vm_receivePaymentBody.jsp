
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="static sessionmanager.SessionConstants.VM_REQUISITION_PAYMENT_GIVEN_YES" %>
<%@ page import="static sessionmanager.SessionConstants.VM_REQUISITION_PAYMENT_TYPE_CHEQUE" %>
<%@ page import="static sessionmanager.SessionConstants.*" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "Vm_requisitionServlet?actionType=search";
    String navigator = SessionConstants.NAV_VM_REQUISITION;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
    String actionName = "paymentStatusFromSearch";
%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.VM_REQUISITION_SEARCH_VM_REQUISITION_SEARCH_FORMNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="./vehicle_receive_paymentNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.VM_REQUISITION_SEARCH_VM_REQUISITION_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
<%--                    <form class="form-horizontal"--%>
<%--                          action="Vm_requisitionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"--%>
<%--                          id="bigform" name="bigform" method="POST" enctype="multipart/form-data"--%>
<%--                          onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">--%>

                    <form class="form-horizontal"  id="bigform" name="bigform" >

                        <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden'  tag='pb_html'/>
                        <input type='hidden' class='form-control'  name='paidStatus' id = 'paidStatus'  tag='pb_html'/>
                        <input type='hidden' class='form-control'  name='payment_type' id = 'payment_type'  tag='pb_html'/>

                        <jsp:include page="vm_receivePaymentSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.VM_REQUISITION_SEARCH_VM_REQUISITION_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        initDeleteCheckBoxes();
        dateTimeInit("<%=Language%>");
    });

    async function paidMethod(iD){
        const paymentType = await Swal.fire({
            title: '<%=LM.getText(LC.VM_REQUISITION_ADD_PAYMENT_ACCEPTANCE, loginDTO)%>',
            confirmButtonText: '<%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE_EDIT_SUBMIT_BUTTON, loginDTO)%>',
            cancelButtonText: '<%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_CANCEL_BUTTON, loginDTO)%>',
            input: 'select',
            inputOptions: {

                '<%=VM_REQUISITION_PAYMENT_TYPE_NAGAD%>'        :'<%=LM.getText(LC.VM_REQUISITION_ADD_NAGAD_CASH, loginDTO)%>',
                '<%=VM_REQUISITION_PAYMENT_TYPE_CHEQUE%>'        :'<%=LM.getText(LC.VM_REQUISITION_ADD_CHEQUE, loginDTO)%>',

            },
            inputPlaceholder: '<%=LM.getText(LC.JOB_JOB, loginDTO)%>',
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value.localeCompare("1")==0 || value.localeCompare("2")==0) {
                        resolve();
                    }
                    else{
                        resolve('<%=LM.getText(LC.JOB_JOB, loginDTO)%>');
                    }
                })
            }
        });

        if (paymentType) {

            //document.getElementById("payment_type").value = paymentType.value;

            if (paymentType.value.localeCompare("1")==0 || paymentType.value.localeCompare("2")==0) {
                document.getElementById("payment_type").value = paymentType.value;
                //document.getElementById("paidStatus").value = <%=VM_REQUISITION_PAYMENT_GIVEN_YES%>;
                document.getElementById("paidStatus").value = <%=CommonApprovalStatus.PAID.getValue()%>;
                document.getElementById("iD_hidden").value = iD;
                console.log('id_hidden: '+iD);
                //document.forms.bigform.submit();
                submitForm();
            }

            <%--if (paymentType.value.localeCompare("1")==0 || paymentType.value.localeCompare("2")==0) {--%>
            <%--    var xhttp = new XMLHttpRequest();--%>
            <%--    xhttp.onreadystatechange = function () {--%>
            <%--        if (this.readyState == 4 && this.status == 200) {--%>

            <%--            var xhttp2 = new XMLHttpRequest();--%>
            <%--            xhttp2.onreadystatechange = function () {--%>
            <%--                if (this.readyState == 4 && this.status == 200) {--%>

            <%--                }--%>
            <%--            };--%>
            <%--            xhttp.open("GET", this.responseText, true);--%>
            <%--            xhttp.send();--%>

            <%--        } else if (this.readyState == 4 && this.status != 200) {--%>
            <%--            alert('failed ' + this.status);--%>
            <%--        }--%>
            <%--    };--%>

            <%--    let params = "iD="+iD+"&payment_type="+paymentType.value+"&paidStatus="+<%=VM_REQUISITION_PAYMENT_GIVEN_YES%>;--%>
            <%--    xhttp.open("POST", "Vm_requisitionServlet?actionType=paymentStatusFromSearch&"+params, true);--%>
            <%--    xhttp.send();--%>
            <%--}--%>



        }
    }

    const vehicleReceiveForm = $('#bigform');
    function submitForm(){


        if($("#bigform").valid()){
            $.ajax({
                type : "POST",
                url : "Vm_requisitionServlet?actionType=paymentStatusFromSearch",
                data : vehicleReceiveForm.serialize(),
                dataType : 'JSON',
                success : function(response) {

                    if(response.responseCode === 0){
                        showToastSticky(response.msg,response.msg);
                    }else if(response.responseCode === 200){
                        window.location.replace(getContextPath()+response.msg);
                    }
                },
                error : function(jqXHR, textStatus, errorThrown) {

                    toastr.error('Can not receive payment');
                }
            });
        }else{

        }
    }

</script>


