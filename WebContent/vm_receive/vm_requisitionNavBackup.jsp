<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%
	System.out.println("Inside nav.jsp");
	String url = request.getParameter("url");
	String navigator = request.getParameter("navigator");
	String pageName = request.getParameter("pageName");
	if (pageName == null)
		pageName = "Search";
	String pageno = "";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
	pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

	System.out.println("rn " + rn);

	String action = url;
	String context = "../../.." + request.getContextPath() + "/";
	String link = context + url;
	String concat = "?";
	if (url.contains("?")) {
		concat = "&";
	}
	String[][] searchFieldInfo = rn.getSearchFieldInfo();
	String totalPage = "1";
	if (rn != null)
		totalPage = rn.getTotalPages() + "";
	int row = 0;

	String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	int pagination_number = 0;
	boolean isPermanentTable = rn.m_isPermanentTable;
	System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>


<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <%
                    out.println("<input  autocomplete='off' type='text' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.VM_REQUISITION_SEARCH_ANYFIELD, loginDTO) + "' ");
                    String value = (String) session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);

                    if (value != null) {
                        out.println("value = '" + value + "'");
                    } else {
                        out.println("value=''");
                    }

                    out.println("/><br />");
                %>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
                    <div class="form-group row">
                        <label class="col-2 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.VM_REQUISITION_ADD_INSERTIONDATE, loginDTO)%></label>					
						<div class="col-10">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="insertion_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="insertion_date_start" name="insertion_date_start">
						</div>
					</div>
				</div>			

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.VM_REQUISITION_ADD_INSERTIONDATE, loginDTO)%></label>
						<div class="col-10">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="insertion_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="insertion_date_end" name="insertion_date_end">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
                    <div class="form-group row">
                        <label class="col-2 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%></label>					
						<div class="col-10">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="start_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="start_date_start" name="start_date_start">
						</div>
					</div>
				</div>			

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%></label>
						<div class="col-10">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="start_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="start_date_end" name="start_date_end">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
                    <div class="form-group row">
                        <label class="col-2 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.VM_REQUISITION_ADD_ENDDATE, loginDTO)%></label>					
						<div class="col-10">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="end_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="end_date_start" name="end_date_start">
						</div>
					</div>
				</div>			

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.VM_REQUISITION_ADD_ENDDATE, loginDTO)%></label>
						<div class="col-10">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="end_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="end_date_end" name="end_date_end">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%></label>					
						<div class="col-10">
							<select class='form-control'  name='vehicle_requisition_purpose_cat' id = 'vehicle_requisition_purpose_cat' onSelect='setSearchChanged()'>
								<option value='-1'></option>
								<%										
									Options = CatDAO.getOptions(Language, "vehicle_requisition_purpose", -1);
								%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%></label>					
						<div class="col-10">
							<select class='form-control'  name='vehicle_type_cat' id = 'vehicle_type_cat' onSelect='setSearchChanged()'>
								<option value='-1'></option>
								<%										
									Options = CatDAO.getOptions(Language, "vehicle_type", -1);
								%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_TRIPDESCRIPTION, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="trip_description" placeholder="" name="trip_description" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONON, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_on" placeholder="" name="decision_on" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONDESCRIPTION, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_description" placeholder="" name="decision_description" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLETYPE, loginDTO)%></label>					
						<div class="col-10">
							<select class='form-control'  name='given_vehicle_type' id = 'given_vehicle_type' onSelect='setSearchChanged()'>
								<%
																	Options = CommonDAO.getOptions(Language, "select", "given_vehicle", "givenVehicleType_select_" + row, "form-control", "givenVehicleType", "any" );
																%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
                    <div class="form-group row">
                        <label class="col-2 col-form-label"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVEDATE, loginDTO)%></label>					
						<div class="col-10">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="receive_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="receive_date_start" name="receive_date_start">
						</div>
					</div>
				</div>			

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVEDATE, loginDTO)%></label>
						<div class="col-10">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="receive_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="receive_date_end" name="receive_date_end">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_PAYMENTTYPE, loginDTO)%></label>					
						<div class="col-10">
							<select class='form-control'  name='payment_type' id = 'payment_type' onSelect='setSearchChanged()'>
								<%
																	Options = CommonDAO.getOptions(Language, "select", "payment", "paymentType_select_" + row, "form-control", "paymentType", "any" );
																%>
								<%=Options%>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERPHONENUM, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_phone_num" placeholder="" name="requester_phone_num" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYPHONENUM, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_phone_num" placeholder="" name="decision_by_phone_num" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERPHONENUM, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_phone_num" placeholder="" name="driver_phone_num" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_name_en" placeholder="" name="requester_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_name_en" placeholder="" name="decision_by_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_name_en" placeholder="" name="driver_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_name_bn" placeholder="" name="requester_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_name_bn" placeholder="" name="decision_by_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_name_bn" placeholder="" name="driver_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_office_name_en" placeholder="" name="requester_office_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICENAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_office_name_en" placeholder="" name="decision_by_office_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICENAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_office_name_en" placeholder="" name="driver_office_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_office_name_bn" placeholder="" name="requester_office_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICENAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_office_name_bn" placeholder="" name="decision_by_office_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICENAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_office_name_bn" placeholder="" name="driver_office_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_office_unit_name_en" placeholder="" name="requester_office_unit_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_office_unit_name_en" placeholder="" name="decision_by_office_unit_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_office_unit_name_en" placeholder="" name="driver_office_unit_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_office_unit_name_bn" placeholder="" name="requester_office_unit_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_office_unit_name_bn" placeholder="" name="decision_by_office_unit_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_office_unit_name_bn" placeholder="" name="driver_office_unit_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_office_unit_org_name_en" placeholder="" name="requester_office_unit_org_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITORGNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_office_unit_org_name_en" placeholder="" name="decision_by_office_unit_org_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITORGNAMEEN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_office_unit_org_name_en" placeholder="" name="driver_office_unit_org_name_en" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="requester_office_unit_org_name_bn" placeholder="" name="requester_office_unit_org_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITORGNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="decision_by_office_unit_org_name_bn" placeholder="" name="decision_by_office_unit_org_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-top: 5px;">
					<div class="form-group row">
						<label class="col-2 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITORGNAMEBN, loginDTO)%></label>					
						<div class="col-10">
							<input type="text" class="form-control" id="driver_office_unit_org_name_bn" placeholder="" name="driver_office_unit_org_name_bn" onChange='setSearchChanged()'>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ml-auto">
            <input type="hidden" name="search" value="yes"/>
            <button type="button" class="btn shadow " onclick="allfield_changed('',0)"
                    style="background-color: #00a1d4; color: white; border-radius: 6px!important; border-radius: 8px; margin-right: 8px">
                <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
            </button>
        </div>
	</div>           
</div>


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>


<script type="text/javascript">

	function dosubmit(params)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		//alert(params);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) 
		    {
		    	document.getElementById('tableForm').innerHTML = this.responseText ;
				setPageNo();
				searchChanged = 0;
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };
		  
		  xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
		  xhttp.send();
		
	}

	function allfield_changed(go, pagination_number)
	{
		var params = 'AnyField=' + document.getElementById('anyfield').value;

		$("#insertion_date_start").val(getDateStringById('insertion_date_start_js', 'DD/MM/YYYY'));
		params +=  '&insertion_date_start='+ getBDFormattedDate('insertion_date_start');
		$("#insertion_date_end").val(getDateStringById('insertion_date_end_js', 'DD/MM/YYYY'));
		params +=  '&insertion_date_end='+ getBDFormattedDateWithOneDayAddition('insertion_date_end');
		$("#start_date_start").val(getDateStringById('start_date_start_js', 'DD/MM/YYYY'));
		params +=  '&start_date_start='+ getBDFormattedDate('start_date_start');
		$("#start_date_end").val(getDateStringById('start_date_end_js', 'DD/MM/YYYY'));
		params +=  '&start_date_end='+ getBDFormattedDate('start_date_end');		
		$("#end_date_start").val(getDateStringById('end_date_start_js', 'DD/MM/YYYY'));
		params +=  '&end_date_start='+ getBDFormattedDate('end_date_start');
		$("#end_date_end").val(getDateStringById('end_date_end_js', 'DD/MM/YYYY'));
		params +=  '&end_date_end='+ getBDFormattedDate('end_date_end');		
		if($("#vehicle_requisition_purpose_cat").val() != -1)
		{
			params +=  '&vehicle_requisition_purpose_cat='+ $("#vehicle_requisition_purpose_cat").val();
		}
		if($("#vehicle_type_cat").val() != -1)
		{
			params +=  '&vehicle_type_cat='+ $("#vehicle_type_cat").val();
		}
		params +=  '&trip_description='+ $('#trip_description').val();
		params +=  '&decision_on='+ $('#decision_on').val();
		params +=  '&decision_description='+ $('#decision_description').val();
		if($("#given_vehicle_type").val() != -1)
		{
			params +=  '&given_vehicle_type='+ $("#given_vehicle_type").val();
		}
		$("#receive_date_start").val(getDateStringById('receive_date_start_js', 'DD/MM/YYYY'));
		params +=  '&receive_date_start='+ getBDFormattedDate('receive_date_start');
		$("#receive_date_end").val(getDateStringById('receive_date_end_js', 'DD/MM/YYYY'));
		params +=  '&receive_date_end='+ getBDFormattedDate('receive_date_end');		
		if($("#payment_type").val() != -1)
		{
			params +=  '&payment_type='+ $("#payment_type").val();
		}
		params +=  '&requester_phone_num='+ $('#requester_phone_num').val();
		params +=  '&decision_by_phone_num='+ $('#decision_by_phone_num').val();
		params +=  '&driver_phone_num='+ $('#driver_phone_num').val();
		params +=  '&requester_name_en='+ $('#requester_name_en').val();
		params +=  '&decision_by_name_en='+ $('#decision_by_name_en').val();
		params +=  '&driver_name_en='+ $('#driver_name_en').val();
		params +=  '&requester_name_bn='+ $('#requester_name_bn').val();
		params +=  '&decision_by_name_bn='+ $('#decision_by_name_bn').val();
		params +=  '&driver_name_bn='+ $('#driver_name_bn').val();
		params +=  '&requester_office_name_en='+ $('#requester_office_name_en').val();
		params +=  '&decision_by_office_name_en='+ $('#decision_by_office_name_en').val();
		params +=  '&driver_office_name_en='+ $('#driver_office_name_en').val();
		params +=  '&requester_office_name_bn='+ $('#requester_office_name_bn').val();
		params +=  '&decision_by_office_name_bn='+ $('#decision_by_office_name_bn').val();
		params +=  '&driver_office_name_bn='+ $('#driver_office_name_bn').val();
		params +=  '&requester_office_unit_name_en='+ $('#requester_office_unit_name_en').val();
		params +=  '&decision_by_office_unit_name_en='+ $('#decision_by_office_unit_name_en').val();
		params +=  '&driver_office_unit_name_en='+ $('#driver_office_unit_name_en').val();
		params +=  '&requester_office_unit_name_bn='+ $('#requester_office_unit_name_bn').val();
		params +=  '&decision_by_office_unit_name_bn='+ $('#decision_by_office_unit_name_bn').val();
		params +=  '&driver_office_unit_name_bn='+ $('#driver_office_unit_name_bn').val();
		params +=  '&requester_office_unit_org_name_en='+ $('#requester_office_unit_org_name_en').val();
		params +=  '&decision_by_office_unit_org_name_en='+ $('#decision_by_office_unit_org_name_en').val();
		params +=  '&driver_office_unit_org_name_en='+ $('#driver_office_unit_org_name_en').val();
		params +=  '&requester_office_unit_org_name_bn='+ $('#requester_office_unit_org_name_bn').val();
		params +=  '&decision_by_office_unit_org_name_bn='+ $('#decision_by_office_unit_org_name_bn').val();
		params +=  '&driver_office_unit_org_name_bn='+ $('#driver_office_unit_org_name_bn').val();
		
		params +=  '&search=true&ajax=true';
		
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged == 0)
		{
			console.log("go found");
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		dosubmit(params);
	
	}

</script>

