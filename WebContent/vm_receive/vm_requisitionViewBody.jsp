<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_requisition.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%@ page import="geolocation.*" %>


<%
    String servletName = "Vm_requisitionServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO("vm_requisition");
    Vm_requisitionDTO vm_requisitionDTO = vm_requisitionDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO)%>
                    </h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <h3><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO)%>
                </h3>
                <table class="table table-bordered table-striped">


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEEN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.requesterNameEn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameEn;
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.requesterNameBn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameBn;
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.startDate + "";
                            %>
                            <%
                                String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_startDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_ENDDATE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.endDate + "";
                            %>
                            <%
                                String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_endDate, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.startAddress + "";
                                value = value.replaceAll("\\$", " -> ");
                            %>
                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.endAddress + "";
                                value = value.replaceAll("\\$", " -> ");
                            %>
                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.startTime + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.endTime + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.vehicleRequisitionPurposeCat + "";
                            %>
                            <%
                                value = CatDAO.getName(Language, "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.vehicleTypeCat + "";
                            %>
                            <%
                                value = CatDAO.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_TRIPDESCRIPTION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = vm_requisitionDTO.tripDescription + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>


        </div>


    </div>
</div>