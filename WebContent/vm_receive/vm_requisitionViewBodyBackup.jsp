

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="vm_requisition.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>

<%@ page import="geolocation.*"%>



<%
String servletName = "Vm_requisitionServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO("vm_requisition");
Vm_requisitionDTO vm_requisitionDTO = vm_requisitionDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO)%></h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO)%></h3>
						<table class="table table-bordered table-striped">
									
			
			
			
			
			
			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERORGID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOrgId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYORGID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOrgId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERORGID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOrgId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.startDate + "";
											%>
											<%
											String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_startDate, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_ENDDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.endDate + "";
											%>
											<%
											String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_endDate, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.startAddress + "";
											%>
											<%=GeoLocationDAO2.getAddressToShow(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.endAddress + "";
											%>
											<%=GeoLocationDAO2.getAddressToShow(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.vehicleRequisitionPurposeCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.vehicleTypeCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_TRIPDESCRIPTION, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.tripDescription + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_STATUS, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.status + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONON, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionOn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONDESCRIPTION, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionDescription + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLETYPE, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.givenVehicleType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "given_vehicle", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_GIVENVEHICLEID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.givenVehicleId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVEDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.receiveDate + "";
											%>
											<%
											String formatted_receiveDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_receiveDate, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_TOTALTRIPDISTANCE, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.totalTripDistance + "";
											%>
											<%
											value = String.format("%.1f", vm_requisitionDTO.totalTripDistance);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_TOTALTRIPTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.totalTripTime + "";
											%>
											<%
											value = String.format("%.1f", vm_requisitionDTO.totalTripTime);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_PETROLGIVEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.petrolGiven + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_PETROLAMOUNT, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.petrolAmount + "";
											%>
											<%
											value = String.format("%.1f", vm_requisitionDTO.petrolAmount);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_PAYMENTGIVEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.paymentGiven + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_PAYMENTTYPE, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.paymentType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "payment", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.startTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.endTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVETIME, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.receiveTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOfficeId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOfficeId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOfficeId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOfficeUnitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOfficeUnitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEREMPID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterEmpId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYEMPID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByEmpId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEREMPID, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverEmpId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERPHONENUM, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterPhoneNum + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYPHONENUM, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByPhoneNum + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERPHONENUM, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverPhoneNum + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVERNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOfficeNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICENAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOfficeNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICENAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOfficeNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOfficeNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICENAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOfficeNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICENAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOfficeNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOfficeUnitNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOfficeUnitNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOfficeUnitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOfficeUnitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOfficeUnitOrgNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITORGNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitOrgNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITORGNAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOfficeUnitOrgNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.requesterOfficeUnitOrgNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DECISIONBYOFFICEUNITORGNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitOrgNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.VM_REQUISITION_ADD_DRIVEROFFICEUNITORGNAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = vm_requisitionDTO.driverOfficeUnitOrgNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
		
						</table>
                    </div>
			






			</div>	

               


        </div>
	</div>