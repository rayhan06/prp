<%@page pageEncoding="UTF-8" %>

<%@page import="vm_requisition.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_REQUISITION;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_requisitionDTO vm_requisitionDTO = (Vm_requisitionDTO)request.getAttribute("vm_requisitionDTO");
CommonDTO commonDTO = vm_requisitionDTO;
String servletName = "Vm_requisitionServlet";


System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_requisitionDAO vm_requisitionDAO = (Vm_requisitionDAO)request.getAttribute("vm_requisitionDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
		
		
		
		
		
		
											<td id = '<%=i%>_requesterOrgId'>
											<%
											value = Language.equals("English") ?
													vm_requisitionDTO.requesterNameEn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameEn
													: vm_requisitionDTO.requesterNameBn + ", " + vm_requisitionDTO.requesterOfficeUnitOrgNameBn
													;
											%>

											<%=Utils.getDigits(value, Language)%>


											</td>

											<td id = '<%=i%>_requesterOrgId'>
												<%
													value = vm_requisitionDTO.requesterPhoneNum + "";
												%>

												<%=Utils.getDigits(value, Language)%>


											</td>



											<td id = '<%=i%>_startDate'>
											<%
											value = vm_requisitionDTO.startDate + "";
											%>
											<%
											String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_startDate, Language)%>


											</td>

											<td id = '<%=i%>_driverName'>
												<%
													value = Language.equals("English") ?
															vm_requisitionDTO.driverNameEn
															: vm_requisitionDTO.driverNameBn
													;
												%>

												<%=Utils.getDigits(value, Language)%>


											</td>

											<td id = '<%=i%>_startAndEndAddress'>
												<%
												String start = vm_requisitionDTO.startAddress + "";
												start = start.replaceAll("\\$", " -> ");

												String end = vm_requisitionDTO.endAddress + "";
												end = 	end.replaceAll("\\$", "-> ");

												String travelled_address = start+"-> "+end;

												%>
												<%=travelled_address%>


											</td>

											<td id = '<%=i%>_vehicleTypeCat'>
												
												<%
													value = CatRepository.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
												%>

												<%=value%>


											</td>

											<td id = '<%=i%>_vehicleRequisitionPurposeCat'>

											<%
											value = CatRepository.getName(Language, "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat);
											%>

											<%=value%>


											</td>




		
	

											<td>
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_requisitionServlet?actionType=view&ID=<%=vm_requisitionDTO.iD%>'">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_requisitionServlet?actionType=vehicleReceiveViewFormPage&ID=<%=vm_requisitionDTO.iD%>'">
											        <%=LM.getText(LC.VM_REQUISITION_ADD_RECEIVE_VEHICLE, loginDTO)%>
											    </button>
																				
											</td>											
