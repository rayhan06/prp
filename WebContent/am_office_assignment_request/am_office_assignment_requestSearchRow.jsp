<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.CommonDTO" %>
<%@ page import="util.StringUtils" %>


<%if(Language.equalsIgnoreCase("English")){%>

<td>
    <%=am_office_assignment_requestDTO.requesterNameEn %>
</td>

<td>
    <%=am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn %>
</td>

<td>
    <%=am_office_assignment_requestDTO.requesterOfficeUnitNameEn %>
</td>
<%}else{%>
<td>
    <%=am_office_assignment_requestDTO.requesterNameBn %>
</td>
<td>
    <%=am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn %>
</td>
<td>
    <%=am_office_assignment_requestDTO.requesterOfficeUnitNameBn %>
</td>
<%}%>

<td>
    <%=StringUtils.getFormattedDate(Language,am_office_assignment_requestDTO.insertionDate)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language,am_office_assignment_requestDTO.assignmentDate)%>
</td>

<td>
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=AmOfficeAssignmentRequestStatus.getColor(am_office_assignment_requestDTO.status)%>; color: white; border-radius: 8px;cursor: text">
            <%=CatRepository.getInstance().getText(
                    Language, "am_office_assignment_request_status", am_office_assignment_requestDTO.status
            )%>
    </span>
</td>

<%CommonDTO commonDTO = am_office_assignment_requestDTO; %>
<%if(isAdmin.equals("isAdmin")){%>

    <td>
        <button
                type="button"
                class="btn-sm border-0 shadow bg-light btn-border-radius"
                style="color: #ff6b6b;"
                onclick="location.href='<%=servletName%>?actionType=adminView&ID=<%=commonDTO.iD%>'"
        >
            <i class="fa fa-eye"></i>
        </button>
    </td>

    <td>
        <%
            if(commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT)
            {
        %>
        <button
                type="button"
                class="btn-sm border-0 shadow btn-border-radius text-white"
                style="background-color: #ff6b6b;"
                onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
        >
            <i class="fa fa-edit"></i>
        </button>
        <%
            }
        %>
    </td>

<%}else{%>

<%--<%@include file="../pb/searchAndViewButton.jsp"%>--%>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <%
        if(commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT && am_office_assignment_requestDTO.status==AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue())
        {
    %>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
    <%
        }
    %>
</td>

<%}%>

<%--<%CommonDTO commonDTO = (CommonDTO)am_office_assignment_requestDTO; %>--%>
<%--<%@include file="../pb/searchAndViewButton.jsp"%>--%>

<td class="text-right">


    <%
        if(am_office_assignment_requestDTO.status==AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue())
        {
    %>
    <div class='checker'>
        <span class='chkEdit' ><input type='checkbox' name='ID' value='<%=am_office_assignment_requestDTO.iD%>'/></span>
    </div>
    <%
        }
    %>


</td>