<%@ page import="util.StringUtils" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="am_other_office_unit.Am_other_office_unitDTO" %>
<%@ page import="am_other_office_unit.Am_other_office_unitRepository" %>
<%@ page import="pb.Utils" %>
<%@ page import="am_parliament_building_level.Am_parliament_building_levelRepository" %>
<%@ page import="am_parliament_building_block.Am_parliament_building_blockRepository" %>
<%@ page import="am_parliament_building_room.Am_parliament_building_roomRepository" %>
<%@ page import="am_minister_hostel_block.Am_minister_hostel_blockRepository" %>
<%@ page import="am_minister_hostel_block.AmMinisterHostelSideRepository" %>
<%@ page import="am_minister_hostel_unit.Am_minister_hostel_unitRepository" %>
<%@ page import="am_minister_hostel_level.Am_minister_hostel_levelRepository" %>
<%@ page import="am_office_assignment.AmOfficeAssignmentFilesDTO" %>
<%@ page import="am_office_assignment.AmOfficeAssignmentFilesDAO" %>
<div class="row mt-5">



    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-2 col-form-label">
                <%=LM.getText(LC.REPORT_SEARCH_NAME, loginDTO)%>

            </label>
            <div class="col-9" id="emp_name">

                <%=UtilCharacter.getDataByLanguage(Language,am_office_assignment_requestDTO.requesterNameBn,am_office_assignment_requestDTO.requesterNameEn)%>

            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-2 col-form-label">
                <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>

            </label>
            <div class="col-9" id="emp_ofc">

                <%=UtilCharacter.getDataByLanguage(Language,am_office_assignment_requestDTO.requesterOfficeUnitNameBn,am_office_assignment_requestDTO.requesterOfficeUnitNameEn)%>

            </div>

        </div>
    </div>

    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-2 col-form-label">
                <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>

            </label>
            <div class="col-9" id="emp_org">

                <%=UtilCharacter.getDataByLanguage(Language,am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn,am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn)%>

            </div>

        </div>
    </div>

    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-2 col-form-label">
                <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>

            </label>
            <div class="col-9" id="emp_phone">
                <%=UtilCharacter.convertDataByLanguage(Language, am_office_assignment_requestDTO.requesterPhoneNum)%>
            </div>

        </div>
    </div>

    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-2 col-form-label">
                <%=LM.getText(LC.CANDIDATE_LIST_APPLY_DATE, loginDTO)%>

            </label>
            <div class="col-9" >
                <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.insertionDate)%>
            </div>

        </div>
    </div>

    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-2 col-form-label">
                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ALLOCATION_REQUEST_DATE, loginDTO)%>

            </label>
            <div class="col-9" >
                <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.assignmentDate)%>
            </div>

        </div>
    </div>

    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-2 col-form-label">
                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ASSIGNMENTDATE, loginDTO)%>

            </label>
            <div class="col-9" >
                <%=StringUtils.getFormattedDate(Language, am_office_assignmentDTO.assignmentDate)%>
            </div>

        </div>
    </div>

    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-12 col-form-label mt-2 text-color">
                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ASSIGNED_IN_THE_NAME_OF_THAT_OFFICE_OR_PERSON, loginDTO)%>
            </label>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEOFFICENAME, loginDTO)%>

            </label>
            <div class="col-8">

                <%if(am_office_assignmentDTO.officeType==1){
                    Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(am_office_assignmentDTO.officeUnitId);
                %>

                <%=UtilCharacter.getDataByLanguage(Language,office_unitsDTO.unitNameBng,office_unitsDTO.unitNameEng)%>

                <%} else{
                    Am_other_office_unitDTO am_other_office_unitDTO  = Am_other_office_unitRepository.getInstance().getAm_other_office_unitDTOByID(am_office_assignmentDTO.officeUnitId);
                %>

                <%=UtilCharacter.getDataByLanguage(Language,am_other_office_unitDTO.officeNameBn,am_other_office_unitDTO.officeNameEn)%>

                <%}%>



            </div>

        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BUILDINGTYPECAT, loginDTO)%>

            </label>
            <div class="col-8">

                <%=CatRepository.getInstance().getText(Language,"building_type",am_office_assignmentDTO.buildingTypeCat)%>

            </div>

        </div>
    </div>

    <%if(am_office_assignmentDTO.buildingTypeCat==1){%>
    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%>

            </label>
            <div class="col-8">

                <%=Am_parliament_building_levelRepository.getInstance().getText(am_office_assignmentDTO.level)%>

            </div>

        </div>
    </div>
    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.ASSIGN_ASSET_ADD_BLOCKCAT, loginDTO)%>

            </label>
            <div class="col-8">

                <%=Am_parliament_building_blockRepository.getInstance().getText(am_office_assignmentDTO.block,Language)%>

            </div>

        </div>
    </div>
    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ROOMNO, loginDTO)%>

            </label>
            <div class="col-8">

                <%=Am_parliament_building_roomRepository.getInstance().getText(am_office_assignmentDTO.roomNo)%>

            </div>

        </div>
    </div>


    <%}else if(am_office_assignmentDTO.buildingTypeCat==2){%>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.ASSIGN_ASSET_ADD_BLOCKCAT, loginDTO)%>

            </label>
            <div class="col-8">

                <%=Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockDTOByID(am_office_assignmentDTO.block).blockNo%>

            </div>

        </div>
    </div>
    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_SEARCH_SIDE, loginDTO)%>

            </label>
            <div class="col-8">

                <%=CatRepository.getInstance().getText(Language,"am_minister_hostel_side", AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByID(am_office_assignmentDTO.side).ministerHostelSideCat)%>

            </div>

        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.ISSUED_TO_OFFICE_TYPE_UNIT, loginDTO)%>

            </label>
            <div class="col-8">

                <%=Am_minister_hostel_unitRepository.getInstance().getAm_minister_hostel_unitDTOByID(am_office_assignmentDTO.unit).unitNumber%>

            </div>

        </div>
    </div>

    <div class="col-6">
        <div class="form-group row d-flex align-items-center">
            <label class="col-4 col-form-label ">
                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%>

            </label>
            <div class="col-8">

                <%=CatRepository.getInstance().getText(Language,"am_minister_hostel_level", Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelDTOByID(am_office_assignmentDTO.level).amMinisterHostelLevelCat)%>

            </div>

        </div>
    </div>

    <%}%>

    <div class="col-12">
        <div class="form-group row d-flex align-items-center">
            <label class="col-12 col-form-label mt-2 text-color">
                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_NECESSARY_DOCUMENTS, loginDTO)%>
            </label>
        </div>
    </div>
    <div class="col-12">

        <%
            String fileColumnName1 = "filesDropzone";
            int ii=0;
            FilesDAO filesDAO1 = new FilesDAO();
            if(true)
            {
                List<AmOfficeAssignmentFilesDTO> amOfficeAssignmentFilesDTOS = AmOfficeAssignmentFilesDAO.getInstance().getByAmOfficeAssignmentId(am_office_assignmentDTO.iD);

                for(AmOfficeAssignmentFilesDTO amOfficeAssignmentFilesDTO : amOfficeAssignmentFilesDTOS){



                    List<FilesDTO> fileList = filesDAO1.getMiniDTOsByFileID(amOfficeAssignmentFilesDTO.filesDropzone);
        %>
        <table>

            <%
                if (fileList != null) {
                    for (int j = 0; j < fileList.size(); j++) {
                        FilesDTO filesDTO = fileList.get(j);
                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
            %>
            <tr>

                <td>

                    <%=amOfficeAssignmentFilesDTO.title%>

                </td>

                <td id='<%=fileColumnName1%>_td_<%=filesDTO.iD%>'>
                    <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                       download><%=filesDTO.fileTitle%>
                    </a>
                    <%
                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                    %>
                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
								%>' style='width:100px'/>
                    <%
                        }
                    %>


                </td>
            </tr>
            <%
                        }
                    }

                }
            %>

        </table>
        <%
            }
        %>
    </div>

    <div class="col-12">


        

    </div>


</div>