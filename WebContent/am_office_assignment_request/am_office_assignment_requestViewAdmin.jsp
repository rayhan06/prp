<%@ page import="static permission.MenuConstants.ASSET_MANAGEMENT" %>
<%@ page import="static permission.MenuConstants.AM_OFFICE_ASSIGNMENT_REQUEST" %>
<%@ page import="static permission.MenuConstants.AM_OFFICE_APPROVAL_MAPPING_SEARCH" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%
String context = "../../.."  + request.getContextPath() + "/";
	request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(ASSET_MANAGEMENT,
			AM_OFFICE_ASSIGNMENT_REQUEST,
			AM_OFFICE_APPROVAL_MAPPING_SEARCH)));
%>
<link href="<%=context%>/assets/css/pagecustom.css" rel="stylesheet" type="text/css"/>
<jsp:include page="../common/layout.jsp" flush="true">
<jsp:param name="title" value="View" /> 
	<jsp:param name="body" value="../am_office_assignment_request/am_office_assignment_requestViewBodyAdmin.jsp" />
</jsp:include> 