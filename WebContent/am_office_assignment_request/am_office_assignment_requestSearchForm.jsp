
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_office_assignment_request.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>


<%
String navigator2 = "navAM_OFFICE_ASSIGNMENT_REQUEST";
String servletName = "Am_office_assignment_requestServlet";
String isAdmin = String.valueOf(request.getAttribute("isAdmin"));
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_APPLICATIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ALLOCATION_REQUEST_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_SEARCH_AM_OFFICE_ASSIGNMENT_REQUEST_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>

						<tbody>
						<%
							List<Am_office_assignment_requestDTO> data = (List<Am_office_assignment_requestDTO>) rn2.list;
							if (data != null && data.size() > 0) {
								for (Am_office_assignment_requestDTO am_office_assignment_requestDTO : data) {
						%>
						<tr>
							<%@include file="am_office_assignment_requestSearchRow.jsp" %>
						</tr>
						<% }
						} %>

						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			