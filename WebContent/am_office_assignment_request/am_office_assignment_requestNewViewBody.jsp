<%@page import="login.LoginDTO" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDTO" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDAO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="am_office_assignment_request.AmOfficeAssignmentRequestStatus" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="am_office_assignment.Am_office_assignmentDTO" %>
<%@ page import="am_office_assignment.Am_office_assignmentDAO" %>
<%@ page import="am_office_approval_mapping.Am_office_approval_mappingDTO" %>
<%@ page import="am_office_approval_mapping.Am_office_approval_mappingDAO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="card_info.CardApprovalDTO" %>
<%@ page import="card_info.CardApprovalRepository" %>
<%@ page import="common.BaseServlet" %>

<%

    String servletName = "Am_office_assignment_requestServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    Am_office_assignment_requestDTO am_office_assignment_requestDTO = (Am_office_assignment_requestDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);

    String context = request.getContextPath() + "/";

    List<Am_office_approval_mappingDTO> amOfficeApprovalMappingDTOs =
            Am_office_approval_mappingDAO.getInstance().getAllApprovalDTOByAmOfficeRequestId(am_office_assignment_requestDTO.iD);

    List<Long> cardApprovalIds = amOfficeApprovalMappingDTOs.stream()
            .map(e -> e.cardApprovalId)
            .collect(Collectors.toList());

    List<CardApprovalDTO> cardApprovalDTOList = CardApprovalRepository.getInstance().getByIds(cardApprovalIds);

    Map<Long, CardApprovalDTO> mapCardApprovalDTOById =
            cardApprovalDTOList.stream()
                    .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));

    Map<Boolean, List<Am_office_approval_mappingDTO>> partitionByIsPending =
            amOfficeApprovalMappingDTOs.stream()
                    .collect(Collectors.partitioningBy(
                            e -> e.amOfficeAssignmentStatusCat == AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue()
                    ));

    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);

%>

<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group {
        margin-bottom: .5rem;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;">
        <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">


            <div class="row">
                <div class="col-12 text-center">
                    <img
                            width="10%"
                            src="<%=context%>assets/static/parliament_logo.png"
                            alt="logo"
                            class="logo-default"
                    />
                    <h2 class="mt-2 text-color">
                        <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_BANGLADESH_NATIONAL_PARLIAMENT, loginDTO)%>
                    </h2>
                    <h4 class="text-dark">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ASSET_MANAGEMENT_BRANCH, loginDTO)%>
                    </h4>
                    <h5 class="text-dark">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_APPLICATION_FORM_FOR_ALLOTMENT_OF_GOVERNMENT_OFFICE, loginDTO)%>
                    </h5>
                </div>
            </div>
            <form class="form-horizontal">


                <%
                    if (am_office_assignment_requestDTO.status == AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue()
                            || am_office_assignment_requestDTO.status == AmOfficeAssignmentRequestStatus.REJECTED.getValue()) {
                %>

                <div class="row mt-5">

                    <div class="col-12 col-form-label" id="previewDescriptionText">

                        <%=am_office_assignment_requestDTO.descriptionText%>

                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.REPORT_SEARCH_NAME, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_name">

                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterNameBn, am_office_assignment_requestDTO.requesterNameEn)%>

                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_ofc">


                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterOfficeUnitNameBn, am_office_assignment_requestDTO.requesterOfficeUnitNameEn)%>

                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_org">

                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn, am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn)%>

                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_phone">
                                <%=UtilCharacter.convertDataByLanguage(Language, am_office_assignment_requestDTO.requesterPhoneNum)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label mt-2 text-color">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_NECESSARY_DOCUMENTS, loginDTO)%>
                            </label>
                        </div>
                    </div>
                    <div class="col-12">

                        <%
                            String fileColumnName = "filesDropzone";
                            int i = 0;
                            FilesDAO filesDAO = new FilesDAO();
                            if (true) {
                                List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(am_office_assignment_requestDTO.filesDropzone);
                        %>
                        <table>

                            <%
                                if (fileList != null) {
                                    for (int j = 0; j < fileList.size(); j++) {
                                        FilesDTO filesDTO = fileList.get(j);
                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                            %>
                            <tr>
                                <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                    <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                       download><%=filesDTO.fileTitle%>
                                    </a>
                                    <%
                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                    %>
                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                    <%
                                        }
                                    %>


                                </td>
                            </tr>
                            <%
                                    }
                                }
                            %>

                        </table>
                        <%
                            }
                        %>
                    </div>

                    <div class="col-12">


                        <%
                            if (am_office_assignment_requestDTO.status == AmOfficeAssignmentRequestStatus.APPROVED.getValue()) {
                                Am_office_assignmentDTO am_office_assignmentDTO = Am_office_assignmentDAO.getInstance().getByAmOfficeAssignmentRequestId(am_office_assignment_requestDTO.iD);
                        %>

                        <div class="col-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-4 col-form-label ">
                                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BUILDINGTYPECAT, loginDTO)%>

                                </label>
                                <div class="col-8">

                                    <%=CatRepository.getInstance().getText(Language, "building_type", am_office_assignmentDTO.buildingTypeCat)%>

                                </div>

                            </div>
                        </div>

                        <%
                            }
                        %>

                    </div>


                </div>
                <%
                } else if (am_office_assignment_requestDTO.status == AmOfficeAssignmentRequestStatus.APPROVED.getValue()
                        || am_office_assignment_requestDTO.status == AmOfficeAssignmentRequestStatus.WITHDRAWN.getValue()) {
                %>

                <%Am_office_assignmentDTO am_office_assignmentDTO = Am_office_assignmentDAO.getInstance().getByAmOfficeAssignmentRequestId(am_office_assignment_requestDTO.iD);%>
                <%@ include file="am_office_assignmentApprovedOrWithdrawnViewBody.jsp" %>

                <%}%>


            </form>
        </div>
    </div>

    <div class="kt-portlet mt-3">
        <h3><%=LM.getText(LC.CARD_INFO_APPROVAL_INFORMATION, loginDTO)%>
        </h3>
        <div class="table-responsive">
            <table class="table table-bordered table-striped text-nowrap">
                <thead>
                <tr>
                    <th><%=LM.getText(LC.CARD_INFO_APPROVER_OFFICE_INFORMATION, loginDTO)%>
                    </th>
                    <th><%=LM.getText(LC.CARD_INFO_APPROVAL_STATUS, loginDTO)%>
                    </th>
                    <th><%=LM.getText(LC.CARD_INFO_APPROVAL_DATE_TIME, loginDTO)%>
                    </th>
                </tr>
                </thead>
                <tbody>
                <%--Pending Approvals--%>
                <%
                    for (Am_office_approval_mappingDTO approvalMappingDTO : partitionByIsPending.getOrDefault(true, new ArrayList<>())) {
                        CardApprovalDTO cardApprovalDTO = mapCardApprovalDTOById.get(approvalMappingDTO.cardApprovalId);
                %>
                <tr>
                    <td style="width: 35%">
                        <b>
                            <%=UtilCharacter.getDataByLanguage(Language, cardApprovalDTO.officeUnitBng, cardApprovalDTO.officeUnitEng)%>
                        </b></td>
                    <td style="width: 35%">
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=AmOfficeAssignmentRequestStatus.getColor(approvalMappingDTO.amOfficeAssignmentStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "am_office_assignment_request_status", approvalMappingDTO.amOfficeAssignmentStatusCat)%>
                                    </span>
                    </td>
                    <td style="width: 35%">
                        <%if (approvalMappingDTO.amOfficeAssignmentStatusCat != AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue()) {%>

                        <%=StringUtils.convertToDateAndTime(isLanguageEnglish, approvalMappingDTO.lastModificationTime)%>
                        <%}%>
                    </td>
                </tr>
                <%}%>

                <%--Not Approvals--%>
                <%
                    for (Am_office_approval_mappingDTO am_office_approval_mappingDTO : partitionByIsPending.getOrDefault(false, new ArrayList<>())) {
                        CardApprovalDTO cardApprovalDTO = mapCardApprovalDTOById.get(am_office_approval_mappingDTO.cardApprovalId);
                %>
                <tr>
                    <td style="width: 35%">
                        <%=UtilCharacter.getDataByLanguage(Language, cardApprovalDTO.officeUnitBng, cardApprovalDTO.officeUnitEng)%>
                        </b></td>
                    <td style="width: 35%">
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=AmOfficeAssignmentRequestStatus.getColor(am_office_approval_mappingDTO.amOfficeAssignmentStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "am_office_assignment_request_status", am_office_approval_mappingDTO.amOfficeAssignmentStatusCat)%>
                                    </span>
                        <%if (am_office_approval_mappingDTO.amOfficeAssignmentStatusCat == AmOfficeAssignmentRequestStatus.REJECTED.getValue()) {%>
                        <div class="container mt-3">
                            <b><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_REJECTION_REASON, loginDTO)%>
                            </b>
                            <br>
                            <%=am_office_approval_mappingDTO.comment%>
                        </div>
                        <%}%>

                        <%if (am_office_approval_mappingDTO.amOfficeAssignmentStatusCat == AmOfficeAssignmentRequestStatus.WITHDRAWN.getValue()) {%>
                        <div class="container mt-3">
                            <b><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_REASONS_FOR_REVOCATION, loginDTO)%>
                            </b>
                            <br>
                            <%=am_office_assignment_requestDTO.withdrawalReason%>
                        </div>
                        <%}%>

                    </td>
                    <td style="width: 35%">
                        <%if (am_office_approval_mappingDTO.amOfficeAssignmentStatusCat != AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue()) {%>
                        <%=StringUtils.convertToDateAndTime(isLanguageEnglish, am_office_approval_mappingDTO.lastModificationTime)%>
                        <%}%>
                    </td>
                </tr>
                <%}%>
                </tbody>
            </table>
        </div>
    </div>

    <div class="form-actions text-right mt-4">

        <button class="btn btn-sm btn-primary shadow btn-border-radius" onclick="history.back()">
            <%=LM.getText(LC.GO_BACK_BACK, loginDTO)%>
        </button>


    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        if ($(document).prop('title') === 'View') {
            $(document).prop('title', '<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_SELECT_ASSIGNMENTDETAILSEN, loginDTO)%>');
        }

    });
</script>

<style>
    .required {
        color: red;
    }
</style>



