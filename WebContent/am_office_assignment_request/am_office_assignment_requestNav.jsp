<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="pb.*"%>
<%@page contentType="text/html;charset=utf-8" %>


<%
	System.out.println("Inside nav.jsp");
	String url = "Am_office_assignment_requestServlet?actionType=search";
	String toggle = request.getParameter("toggle");
	boolean isExpanded = toggle != null && toggle.length() > 0;
%>
<%@include file="../pb/navInitializer.jsp"%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
	<div class="kt-portlet__body">
		<!-- BEGIN FORM-->
		<div class="ml-1">
			<div class="row">

				<div class="col-md-6" style="display: none;">
					<div class="form-group row">
						<label class="col-md-3 col-form-label"><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_APPLICATIONDATE, loginDTO)%></label>
						<div class="col-md-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="assignment_date_start_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="assignment_date_start" name="assignment_date_start">
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-3 col-form-label"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ALLOCATION_REQUEST_DATE, loginDTO)%></label>
						<div class="col-md-9">
							<jsp:include page="/date/date.jsp">
								<jsp:param name="DATE_ID" value="assignment_date_end_js"></jsp:param>
								<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
							</jsp:include>
							<input type="hidden" id="assignment_date_end" name="assignment_date_end">
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group row">
						<label class="col-md-3 col-form-label"><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%>
						</label>
						<div class="col-md-9">
							<select class='form-control' name='fundApplicationStatusCat'
									id='fund_application_status_cat' onSelect='setSearchChanged()'>
								<%=CatRepository.getInstance().buildOptions("am_office_assignment_request_status", Language, -1)%>
							</select>
						</div>
					</div>
				</div>


			</div>
			<div class="row">
				<div class="col-12 text-right">
					<input type="hidden" name="search" value="yes"/>
					<button type="submit"
							class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
							onclick="allfield_changed('',0)"
							style="background-color: #00a1d4;">
						<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
	<div class="search-loader-container-circle ">
		<div class="search-loader-circle"></div>
	</div>
</div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">

	const servletName = 'Am_office_assignment_requestServlet';

	const statusSelector = $('#fund_application_status_cat');
	const assignmentDateSelector = $('#assignment_date_end');
	$(document).ready(() => {

		readyInit(servletName);

		<%if(isExpanded){%>
		document.querySelector('.kt-portlet__body').style.display = 'block';
		<%}%>

	});

	function resetInputs() {
		$('select').prop('selectedIndex', 0);
		resetDateById('assignment_date_end_js');
	}

	window.addEventListener('popstate', e => {
		if (e.state) {

			let params = e.state;
			dosubmit(params, false);
			resetInputs();
			let arr = params.split('&');
			arr.forEach(e => {
				let item = e.split('=');
				if (item.length === 2) {
					switch (item[0]) {
						case 'assignment_date_end':
							setDateByTimestampAndId('assignment_date_end_js',item[1]);
							break;
						case 'amOfficeAssignmentStatusCat':
							$('#fund_application_status_cat option[value="'+item[1]+'"]').prop('selected', 'selected').change();
							break;
					}
				}
			});
		} else {
			dosubmit(null, false);
			resetInputs();
		}
	});

	function dosubmit(params, pushState = true)
	{
		document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
		//alert(params);
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) 
		    {
				if (pushState) {
					history.pushState(params, '', servletName+'?actionType=search&' + params);
				}
				setTimeout(() => {
					document.getElementById('tableForm').innerHTML = this.responseText;
					setPageNo();
					searchChanged = 0;
				}, 200);
			}
		    else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		  };

		let url = "<%=action%>&ajax=true&isPermanentTable=true";
		if (params) {
			url += "&" + params;
		}
		xhttp.open("Get", url, false);
		xhttp.send();
		
	}

	function allfield_changed(go, pagination_number)
	{
		let params = 'search=true';

		let assignmentDateStart = $("#assignment_date_start").val(getDateStringById('assignment_date_start_js', 'DD/MM/YYYY'));
		if(assignmentDateStart.val()){
			params +=  '&assignment_date_start='+ getBDFormattedDate('assignment_date_start');
		}

		let assignmentDateEnd = $("#assignment_date_end").val(getDateStringById('assignment_date_end_js', 'DD/MM/YYYY'));
		if(assignmentDateEnd.val()){
			params +=  '&assignment_date_end='+ (+getBDFormattedDate('assignment_date_end'));
		}
		if (statusSelector.val()) {
			params += '&amOfficeAssignmentStatusCat=' + $("#fund_application_status_cat").val();
		}

		let extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		let pageNo = document.getElementsByName('pageno')[0].value;
		let rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		let totalRecords = 0;
		let lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		if(go !== '' && searchChanged == 0)
		{
			console.log("go found");
			params += '&go=1';
			pageNo = document.getElementsByName('pageno')[pagination_number].value;
			rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
			setPageNoInAllFields(pageNo);
			setRPPInAllFields(rpp);
		}
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		dosubmit(params);
	
	}

</script>

