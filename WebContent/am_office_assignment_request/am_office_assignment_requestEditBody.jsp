<%@page import="login.LoginDTO" %>
<%@page import="am_office_assignment_request.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="common.BaseServlet" %>

<%
    String servletName = "Am_office_assignment_requestServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    String actionName;
    Am_office_assignment_requestDTO am_office_assignment_requestDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        am_office_assignment_requestDTO = (Am_office_assignment_requestDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        am_office_assignment_requestDTO = new Am_office_assignment_requestDTO();
    }

    String formTitle = LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_AM_OFFICE_ASSIGNMENT_REQUEST_ADD_FORMNAME, loginDTO);
    String fileColumnName = "";

    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    Gson gson = new Gson();

    EmployeeSearchModel model = (gson.fromJson
            (EmployeeSearchModalUtil.getEmployeeSearchModelJson
                            (userDTO.employee_record_id,
                                    userDTO.unitID,
                                    userDTO.organogramID),
                    EmployeeSearchModel.class)
    );


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body" id="formItems">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1 ">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=am_office_assignment_requestDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ASSIGNMENTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "assignmentDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='assignmentDate' id='assignmentDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_office_assignment_requestDTO.assignmentDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row" style="display: none;">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_DECISIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "decisionDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='decisionDate' id='decisionDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_office_assignment_requestDTO.decisionDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_DESCRIPTIONTEXT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">

                                                            <textarea type='text' class='form-control'
                                                                      name='descriptiontext' id='descriptiontext'
                                                                      tag='pb_html'/>
                                            <%=actionName.equals("edit") ? (am_office_assignment_requestDTO.descriptionText) : ""%>
                                            </textarea>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                fileColumnName = "filesDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(am_office_assignment_requestDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    am_office_assignment_requestDTO.filesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=am_office_assignment_requestDTO.filesDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=am_office_assignment_requestDTO.filesDropzone%>'/>


                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_AM_OFFICE_ASSIGNMENT_REQUEST_CANCEL_BUTTON, loginDTO)%>
                            </button>

                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                    onclick="viewPreview()">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_VIEW_PREVIEW, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="previewContents" style="display: none">

                <%@ include file="am_office_assignment_requestPreviewBody.jsp" %>

            </div>

        </form>
    </div>
</div>

<script type="text/javascript">

    var row = 0;
    var child_table_extra_id = <%=childTableStartingID%>;

    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        let lang = '<%=Language%>';
        let amRequestDateErr;
        let amRequestDescriptionTextErr;

        if (lang == 'english') {
            amRequestDateErr = 'Please provide allocation date';
            amRequestDescriptionTextErr = 'Please provide description';

        } else {
            amRequestDateErr = 'অনুগ্রহপূর্বক বরাদ্দের তারিখ প্রদান করুন';
            amRequestDescriptionTextErr = 'অনুগ্রহপূর্বক বিস্তারিত তথ্য প্রদান করুন';

        }

        $.validator.addMethod('nonEmpty', function (value, element) {
            return value.toString().trim().length > 0;
        });

        $.validator.addMethod('nonEmptyDate', function (value, element) {
            return value.toString().trim().length > 0;
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                assignmentDate: {
                    nonEmptyDate: true,
                },
                descriptiontext: {
                    nonEmpty: true,
                },

            },
            messages: {
                assignmentDate: amRequestDateErr,
                descriptiontext: amRequestDescriptionTextErr,

            }
        });

        <%
            if(actionName.equalsIgnoreCase("edit")){
        %>
        $("#descriptiontext").html("<%=am_office_assignment_requestDTO.descriptionText%>");
        <%}else{%>
        $("#descriptiontext").html("সম্মানিত,</br></br>আমি আপনার সম্মানিত প্রতিষ্ঠানের <%=model.officeUnitNameBn%> বিভাগে <%=model.organogramNameBn%> হিসাবে কাজ করি। আমার নাম <%=model.employeeNameBn%> , এমপ্লয়ি ইউজারনেম <%=Utils.getDigits(WorkflowController.getUserNameFromOrganogramId(model.organogramId),"Bangla")%>। </br> আমি অফিসের জায়গার প্রয়োজনীয়তার জন্য আপনার সদয় বিবেচনায় আনতে চাই। ব্যবস্থাটি সহজ করার জন্য আমি আপনাকে অনুরোধ করছি ");
        <%}%>


    });

    const viewPreview = () => {
        document.getElementById("formItems").style.display = "none";
        document.getElementById("previewContents").style.display = "block";
        let val;
        let lang = '<%=Language%>';
        for (instance in CKEDITOR.instances) {
            val = $(CKEDITOR.instances[instance].getData());
        }
        $("#previewDescriptionText").html(val);
    }

    const viewFormItems = () => {
        document.getElementById("previewContents").style.display = "none";
        document.getElementById("formItems").style.display = "block";
    }

    const amOfficeRequestForm = $("#bigform");

    function PreprocessBeforeSubmiting(row, validate) {
        preprocessDateBeforeSubmitting('assignmentDate', row);
        preprocessDateBeforeSubmitting('decisionDate', row);

        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        amOfficeRequestForm.validate();
        return amOfficeRequestForm.valid();
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting(0)) {
            $.ajax({
                type: "POST",
                url: "Am_office_assignment_requestServlet?actionType=ajax_<%=actionName%>",
                data: amOfficeRequestForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Am_office_assignment_requestServlet");
    }

    function init(row) {
        setDateByStringAndId('assignmentDate_js_' + row, $('#assignmentDate_date_' + row).val());
        setDateByStringAndId('decisionDate_js_' + row, $('#decisionDate_date_' + row).val());

        var today = new Date();
        var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        setMinDateById('assignmentDate_js_0', todayDate);
    }
</script>






