<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="am_office_assignment_request.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="util.StringUtils" %>

<%
    String servletName = "Vm_incidentServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    Am_office_assignment_requestDTO am_office_assignment_requestDTO = Am_office_assignment_requestDAO.getInstance().getDTOFromID(Long.parseLong(ID));
    FilesDAO filesDAO = new FilesDAO();
%>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_AM_OFFICE_ASSIGNMENT_REQUEST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_AM_OFFICE_ASSIGNMENT_REQUEST_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ASSIGNMENTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=StringUtils.getFormattedDate(Language,am_office_assignment_requestDTO.assignmentDate)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_DECISIONDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">

                                    </div>
                                </div>
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_STATUS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                    </div>
                                </div>
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>