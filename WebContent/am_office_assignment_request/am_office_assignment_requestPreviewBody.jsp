<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_requisition.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>

<%

    String context = request.getContextPath() + "/";
%>

<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group{
        margin-bottom: .5rem;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;">
        <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">




            <div class="row">
                <div class="col-12 text-center">
                    <img
                            width="10%"
                            src="<%=context%>assets/static/parliament_logo.png"
                            alt="logo"
                            class="logo-default"
                    />
                    <h2 class="mt-2 text-color">
                        <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                    </h2>
                    <h4 class="text-dark">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ASSET_MANAGEMENT_BRANCH, loginDTO)%>
                    </h4>
                    <h5 class="text-dark">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_APPLICATION_FORM_FOR_ALLOTMENT_OF_GOVERNMENT_OFFICE, loginDTO)%>
                    </h5>
                </div>
            </div>
            <form class="form-horizontal">



                <div class="row">

                    <div class="col-12 col-form-label" id="previewDescriptionText" >



                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.REPORT_SEARCH_NAME, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_name">

                                <%if(Language.equalsIgnoreCase("English")){%>
                                    <%=model.employeeNameEn%>
                                <%}else{%>
                                    <%=model.employeeNameBn%>
                                <%}%>

                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_ofc">

                                <%if(Language.equalsIgnoreCase("English")){%>
                                    <%=model.officeUnitNameEn%>
                                <%}else{%>
                                    <%=model.officeUnitNameBn%>
                                <%}%>

                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_org">
                                <%if(Language.equalsIgnoreCase("English")){%>
                                    <%=model.organogramNameEn%>
                                <%}else{%>
                                    <%=model.organogramNameBn%>
                                <%}%>

                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>

                            </label>
                            <div class="col-9" id="emp_phone">
                                <%=UtilCharacter.convertDataByLanguage(Language, model.phoneNumber)%>
                            </div>

                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
    <div class="form-actions text-right mt-4">

        <button class="btn btn-sm btn-primary shadow btn-border-radius" type="button" onclick="viewFormItems()">
            <%=LM.getText(LC.GO_BACK_BACK, loginDTO)%>
        </button>

        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitForm()">
            <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_AM_OFFICE_ASSIGNMENT_REQUEST_SUBMIT_BUTTON, loginDTO)%>
        </button>

    </div>
</div>


<script type="text/javascript">


</script>

<style>
    .required {
        color: red;
    }
</style>



