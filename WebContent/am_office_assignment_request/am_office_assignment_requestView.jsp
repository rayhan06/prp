<%@ page import="static permission.MenuConstants.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH" %>
<%@ page import="static permission.MenuConstants.AM_HOUSE_ALLOCATION" %>
<%@ page import="static permission.MenuConstants.*" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%
String context = "../../.."  + request.getContextPath() + "/";
	request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(ASSET_MANAGEMENT,
			AM_OFFICE_ASSIGNMENT_REQUEST,
			AM_OFFICE_ASSIGNMENT_REQUEST_SEARCH)));
%>
<link href="<%=context%>/assets/css/pagecustom.css" rel="stylesheet" type="text/css"/>
<jsp:include page="../common/layout.jsp" flush="true">
<jsp:param name="title" value="View" /> 
	<jsp:param name="body" value="../am_office_assignment_request/am_office_assignment_requestNewViewBody.jsp" />
</jsp:include> 