<%@page import="login.LoginDTO" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDTO" %>
<%@ page import="am_office_assignment_request.Am_office_assignment_requestDAO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="files.FilesDAO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="am_office_approval_mapping.Am_office_approval_mappingDTO" %>
<%@ page import="am_office_assignment_request.AmOfficeAssignmentRequestStatus" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="am_office_assignment.Am_office_assignmentDTO" %>
<%@ page import="am_office_assignment.Am_office_assignmentDAO" %>
<%@ page import="pb.CatRepository" %>


<%


    String servletName = "Am_office_assignment_requestServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;


    Am_office_assignment_requestDTO am_office_assignment_requestDTO = (Am_office_assignment_requestDTO) request.getAttribute("am_office_assignment_requestDTO");
    Am_office_approval_mappingDTO approverAmOfficeAssignmentRequestDTO = (Am_office_approval_mappingDTO) request.getAttribute("approverAmOfficeAssignmentRequestDTO");

    String allocateBtnUrl = "Am_office_assignmentServlet?actionType=getAddPage&amOfficeAssignmentRequestId=" + am_office_assignment_requestDTO.iD;

    String context = request.getContextPath() + "/";
%>

<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group {
        margin-bottom: .5rem;
    }
</style>

<div class="kt-content" id="kt_content">
    <div class="kt-portlet" style="background-color: #F2F2F2!important;">
        <div class="kt-portlet__body m-4" style="background-color: #f6f9fb">


            <div class="row">
                <div class="col-12 text-center">
                    <img
                            width="10%"
                            src="<%=context%>assets/static/parliament_logo.png"
                            alt="logo"
                            class="logo-default"
                    />
                    <h2 class="mt-2 text-color">
                        <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_BANGLADESH_NATIONAL_PARLIAMENT, loginDTO)%>
                    </h2>
                    <h4 class="text-dark">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ASSET_MANAGEMENT_BRANCH, loginDTO)%>
                    </h4>
                    <h5 class="text-dark">
                        <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_APPLICATION_FORM_FOR_ALLOTMENT_OF_GOVERNMENT_OFFICE, loginDTO)%>
                    </h5>
                </div>
            </div>
            <form class="form-horizontal">


                <div class="row mt-5">

                    <%
                        if (am_office_assignment_requestDTO.status == CommonApprovalStatus.SATISFIED.getValue() ||
                                am_office_assignment_requestDTO.status == CommonApprovalStatus.WITHDRAWN.getValue()) {
                            Am_office_assignmentDTO am_office_assignmentDTO = Am_office_assignmentDAO.getInstance().getByAmOfficeAssignmentRequestId(am_office_assignment_requestDTO.iD);
                    %>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.REPORT_SEARCH_NAME, loginDTO)%>

                            </label>
                            <div class="col-9">

                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterNameBn, am_office_assignment_requestDTO.requesterNameEn)%>

                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>

                            </label>
                            <div class="col-9">

                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterOfficeUnitNameBn, am_office_assignment_requestDTO.requesterOfficeUnitNameEn)%>

                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>

                            </label>
                            <div class="col-9">

                                <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn, am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn)%>

                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>

                            </label>
                            <div class="col-9">
                                <%=UtilCharacter.convertDataByLanguage(Language, am_office_assignment_requestDTO.requesterPhoneNum)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.CANDIDATE_LIST_APPLY_DATE, loginDTO)%>

                            </label>
                            <div class="col-9">
                                <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.insertionDate)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ALLOCATION_REQUEST_DATE, loginDTO)%>

                            </label>
                            <div class="col-9">
                                <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.assignmentDate)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-2 col-form-label">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ASSIGNMENTDATE, loginDTO)%>

                            </label>
                            <div class="col-9">
                                <%=StringUtils.getFormattedDate(Language, am_office_assignmentDTO.assignmentDate)%>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-12 col-form-label mt-2 text-color">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ASSIGNED_IN_THE_NAME_OF_THAT_OFFICE_OR_PERSON, loginDTO)%>
                            </label>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.VM_MAINTENANCE_ADD_VEHICLEOFFICENAME, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%
                                    if (am_office_assignmentDTO.officeType == 1) {
                                        Office_unitsDTO office_unitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(am_office_assignmentDTO.officeUnitId);
                                %>

                                <%=UtilCharacter.getDataByLanguage(Language, office_unitsDTO.unitNameBng, office_unitsDTO.unitNameEng)%>

                                <%
                                } else {
                                    Am_other_office_unitDTO am_other_office_unitDTO = Am_other_office_unitRepository.getInstance().getAm_other_office_unitDTOByID(am_office_assignmentDTO.officeUnitId);
                                %>

                                <%=UtilCharacter.getDataByLanguage(Language, am_other_office_unitDTO.officeNameBn, am_other_office_unitDTO.officeNameEn)%>

                                <%}%>


                            </div>

                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_BUILDINGTYPECAT, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=CatRepository.getInstance().getText(Language, "building_type", am_office_assignmentDTO.buildingTypeCat)%>

                            </div>

                        </div>
                    </div>

                    <%if (am_office_assignmentDTO.buildingTypeCat == 1) {%>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_parliament_building_levelRepository.getInstance().getText(am_office_assignmentDTO.level)%>

                            </div>

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.ASSIGN_ASSET_ADD_BLOCKCAT, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_parliament_building_blockRepository.getInstance().getText(am_office_assignmentDTO.block, Language)%>

                            </div>

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_ADD_ROOMNO, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_parliament_building_roomRepository.getInstance().getText(am_office_assignmentDTO.roomNo)%>

                            </div>

                        </div>
                    </div>


                    <%} else if (am_office_assignmentDTO.buildingTypeCat == 2) {%>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.ASSIGN_ASSET_ADD_BLOCKCAT, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_minister_hostel_blockRepository.getInstance().getAm_minister_hostel_blockDTOByID(am_office_assignmentDTO.block).blockNo%>

                            </div>

                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_SEARCH_SIDE, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=CatRepository.getInstance().getText(Language, "am_minister_hostel_side", AmMinisterHostelSideRepository.getInstance().getAmMinisterHostelSideDTOByID(am_office_assignmentDTO.side).ministerHostelSideCat)%>

                            </div>

                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.ISSUED_TO_OFFICE_TYPE_UNIT, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=Am_minister_hostel_unitRepository.getInstance().getAm_minister_hostel_unitDTOByID(am_office_assignmentDTO.unit).unitNumber%>

                            </div>

                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-4 col-form-label ">
                                <%=LM.getText(LC.AM_PARLIAMENT_BUILDING_ROOM_ADD_LEVELTYPE, loginDTO)%>

                            </label>
                            <div class="col-8">

                                <%=CatRepository.getInstance().getText(Language, "am_minister_hostel_level", Am_minister_hostel_levelRepository.getInstance().getAm_minister_hostel_levelDTOByID(am_office_assignmentDTO.level).amMinisterHostelLevelCat)%>

                            </div>

                        </div>
                    </div>

                    <%}%>

                    <div class="col-12">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-12 col-form-label mt-2 text-color">
                                <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_NECESSARY_DOCUMENTS, loginDTO)%>
                            </label>
                        </div>
                    </div>
                    <div class="col-12">

                        <%
                            String fileColumnName = "filesDropzone";
                            int i = 0;
                            FilesDAO filesDAO = new FilesDAO();
                            if (true) {
                                List<AmOfficeAssignmentFilesDTO> amOfficeAssignmentFilesDTOS = AmOfficeAssignmentFilesDAO.getInstance().getByAmOfficeAssignmentId(am_office_assignmentDTO.iD);

                                for (AmOfficeAssignmentFilesDTO amOfficeAssignmentFilesDTO : amOfficeAssignmentFilesDTOS) {


                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(amOfficeAssignmentFilesDTO.filesDropzone);
                        %>
                        <table>

                            <%
                                if (fileList != null) {
                                    for (int j = 0; j < fileList.size(); j++) {
                                        FilesDTO filesDTO = fileList.get(j);
                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                            %>
                            <tr>

                                <td>

                                    <%=amOfficeAssignmentFilesDTO.title%>

                                </td>

                                <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                    <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                       download><%=filesDTO.fileTitle%>
                                    </a>
                                    <%
                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                    %>
                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
								%>' style='width:100px'/>
                                    <%
                                        }
                                    %>


                                </td>
                            </tr>
                            <%
                                        }
                                    }

                                }
                            %>

                        </table>
                        <%
                            }
                        %>
                    </div>


                    <%if (am_office_assignment_requestDTO.status == CommonApprovalStatus.WITHDRAWN.getValue()) {%>

                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-4 col-form-label ">
                                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_REASONS_FOR_REVOCATION, loginDTO)%>
                                </label>
                                <div class="col-8">

                                    <%=am_office_assignment_requestDTO.withdrawalReason%>

                                </div>

                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-4 col-form-label ">
                                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_SELECT_WITHDRAWALDATE, loginDTO)%>
                                </label>
                                <div class="col-8">

                                    <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.withdrawalDate)%>

                                </div>

                            </div>
                        </div>

                    <%}%>


                    <%}%>


                    <%
                        if (am_office_assignment_requestDTO.status == CommonApprovalStatus.DISSATISFIED.getValue() ||
                                am_office_assignment_requestDTO.status == CommonApprovalStatus.PENDING.getValue()) {
                    %>

                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-2 col-form-label">
                                    <%=LM.getText(LC.REPORT_SEARCH_NAME, loginDTO)%>

                                </label>
                                <div class="col-9" id="emp_name">

                                    <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterNameBn, am_office_assignment_requestDTO.requesterNameEn)%>

                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-2 col-form-label">
                                    <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%>

                                </label>
                                <div class="col-9" id="emp_ofc">

                                    <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterOfficeUnitNameBn, am_office_assignment_requestDTO.requesterOfficeUnitNameEn)%>

                                </div>

                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-2 col-form-label">
                                    <%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%>

                                </label>
                                <div class="col-9" id="emp_org">

                                    <%=UtilCharacter.getDataByLanguage(Language, am_office_assignment_requestDTO.requesterOfficeUnitOrgNameBn, am_office_assignment_requestDTO.requesterOfficeUnitOrgNameEn)%>

                                </div>

                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-2 col-form-label">
                                    <%=LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO)%>

                                </label>
                                <div class="col-9" id="emp_phone">
                                    <%=UtilCharacter.convertDataByLanguage(Language, am_office_assignment_requestDTO.requesterPhoneNum)%>
                                </div>

                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-2 col-form-label">
                                    <%=LM.getText(LC.CANDIDATE_LIST_APPLY_DATE, loginDTO)%>

                                </label>
                                <div class="col-9">
                                    <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.insertionDate)%>
                                </div>

                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-2 col-form-label">
                                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ALLOCATION_REQUEST_DATE, loginDTO)%>

                                </label>
                                <div class="col-9">
                                    <%=StringUtils.getFormattedDate(Language, am_office_assignment_requestDTO.assignmentDate)%>
                                </div>

                            </div>
                        </div>

                        <div class="col-12 col-form-label" id="previewDescriptionText">

                            <%=am_office_assignment_requestDTO.descriptionText%>

                        </div>

                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-2 col-form-label mt-2 text-color">
                                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_NECESSARY_DOCUMENTS, loginDTO)%>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">

                            <%
                                String fileColumnName = "filesDropzone";
                                int i = 0;
                                FilesDAO filesDAO = new FilesDAO();
                                if (true) {
                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(am_office_assignment_requestDTO.filesDropzone);
                            %>
                            <table>

                                <%
                                    if (fileList != null) {
                                        for (int j = 0; j < fileList.size(); j++) {
                                            FilesDTO filesDTO = fileList.get(j);
                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                %>
                                <tr>
                                    <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                        <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                           download><%=filesDTO.fileTitle%>
                                        </a>
                                        <%
                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                        %>
                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
                                %>' style='width:100px'/>
                                        <%
                                            }
                                        %>


                                    </td>
                                </tr>
                                <%
                                        }
                                    }
                                %>

                            </table>
                            <%
                                }
                            %>
                        </div>

                    <%}%>

                    <%if (am_office_assignment_requestDTO.status == CommonApprovalStatus.DISSATISFIED.getValue()) {%>

                        <div class="col-12">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-2 col-form-label ">
                                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_REJECTION_REASON, loginDTO)%>
                                </label>
                                <div class="col-10">

                                    <%=am_office_assignment_requestDTO.comment%>

                                </div>

                            </div>
                        </div>

                    <%}%>


                    <%

                        if (am_office_assignment_requestDTO.status == CommonApprovalStatus.PENDING.getValue()) {

                            List<Am_office_assignment_requestDTO> am_office_assignment_requestDTOS = Am_office_assignment_requestDAO.getInstance().getAllByEmployeeRecordIdAndStatus(am_office_assignment_requestDTO.requesterEmpId);
                            if (am_office_assignment_requestDTOS != null && am_office_assignment_requestDTOS.size() > 0) {%>

                    <%
                                for (Am_office_assignment_requestDTO dto : am_office_assignment_requestDTOS) {
                                    Am_office_assignmentDTO am_office_assignmentDTO = Am_office_assignmentDAO.getInstance().getByAmOfficeAssignmentRequestId(dto.iD);

                                    if (am_office_assignmentDTO != null && am_office_assignmentDTO.officeType > 0) {%>

                                        <div class="col-12">
                                            <div class="form-group row d-flex align-items-center">
                                                <label class="col-2 col-form-label mt-2 text-color">
                                                    <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_PREVIOUSLY_ASSIGNED_OFFICE, loginDTO)%>
                                                </label>
                                            </div>
                                        </div>

                                         <%@ include file="am_office_assignmentPreviousOfficeAllotment.jsp" %>

                                    <%}%>
                                <%}%>
                            <%}%>

                        <%}%>


                </div>

            </form>

        </div>
    </div>
    <div class="form-actions text-right mt-4">

        <button class="btn btn-sm btn-primary shadow btn-border-radius" onclick="history.back()"
        ><%=LM.getText(LC.GO_BACK_BACK, loginDTO)%>
        </button>

        <%if (approverAmOfficeAssignmentRequestDTO.amOfficeAssignmentStatusCat == AmOfficeAssignmentRequestStatus.WAITING_FOR_APPROVAL.getValue()) {%>

        <a class="btn btn-sm btn-success shadow btn-border-radius"
           href="<%=allocateBtnUrl%>"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ALLOCATE_ROOM, loginDTO)%>
        </a>

        <button type="button" class="btn btn-danger btn-sm shadow ml-2" style="border-radius: 8px;" id="reject_btn"
                onclick="rejectApplication()">
            <%=UtilCharacter.getDataByLanguage(Language, "অননুমোদিত",
                    "Unauthorized")%>
        </button>
        <%}%>
        <%if (approverAmOfficeAssignmentRequestDTO.amOfficeAssignmentStatusCat == AmOfficeAssignmentRequestStatus.APPROVED.getValue()) {%>

        <button type="button" class="btn btn-warning btn-sm shadow ml-2" style="border-radius: 8px;color: white;"
                id="withdrawal_btn"
                onclick="withdrawAllocation()">
            <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_REVOKE_ALLOCATION, loginDTO)%>
        </button>

        <%}%>

    </div>
</div>
<div style="display: none">
    <form action="Am_office_approval_mappingServlet?actionType=rejectApplication"
          id="rejectForm" name="rejectForm" method="POST"
          enctype="multipart/form-data">
        <input type="hidden" name="amOfficeAssignmentRequestId" value="<%=am_office_assignment_requestDTO.iD%>">
        <input type="hidden" name="reject_reason" id="reject_reason">
    </form>
</div>

<div style="display: none">
    <form action="Am_office_approval_mappingServlet?actionType=withdrawAllocation"
          id="withdrawalForm" name="withdrawalForm" method="POST"
          enctype="multipart/form-data">
        <input type="hidden" name="amOfficeAssignmentRequestId" value="<%=am_office_assignment_requestDTO.iD%>">
        <input type="hidden" name="withdrawal_reason" id="withdrawal_reason">
    </form>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        if ($(document).prop('title') === 'View') {
            $(document).prop('title', '<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_HISTORY_REPORT_SELECT_ASSIGNMENTDETAILSEN, loginDTO)%>');
        }

    });

    function rejectApplication() {
        let msg = '<%=UtilCharacter.getDataByLanguage(Language, "অননুমোদিতের কারণ","Reason For Unauthorized")%>';
        let placeHolder = '<%=UtilCharacter.getDataByLanguage(Language, "অননুমোদিতের কারণ এখানে লিখুন","Enter Unauthorized Reason Here")%>';
        let confirmButtonText = '<%=UtilCharacter.getDataByLanguage(Language, "হ্যাঁ, অননুমোদিত","Yes, Unauthorized")%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        let emptyReasonText = '<%=UtilCharacter.getDataByLanguage(Language, "অননুমোদিতের কারণ এখানে লিখুন","Enter Unauthorized Reason Here")%>';
        dialogMessageWithTextBoxWithoutAnimation(msg, placeHolder, confirmButtonText, cancelButtonText, emptyReasonText, (reason) => {
            $('#reject_btn').prop("disabled", true);
            $('#reject_reason').val(reason);
            $('#rejectForm').submit();
        }, () => {
        });
    }

    function withdrawAllocation() {
        let msg = '<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_REASONS_FOR_REVOCATION_OF_APPROVAL, loginDTO)%>';
        let placeHolder = '<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_ENTER_THE_REASONS_FOR_REVOKING_THE_APPROVAL_HERE, loginDTO)%>';
        let confirmButtonText = '<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_YES__REVOKE, loginDTO)%>';
        let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';
        dialogMessageWithTextBoxWithoutAnimation(msg, placeHolder, confirmButtonText, cancelButtonText, emptyReasonText, (reason) => {
            $('#withdrawal_btn').prop("disabled", true);
            $('#withdrawal_reason').val(reason);
            $('#withdrawalForm').submit();
        }, () => {
        });
    }


</script>

<style>
    .required {
        color: red;
    }
</style>



