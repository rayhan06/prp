
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="user_organogram.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.USER_ORGANOGRAM_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


User_organogramDAO user_organogramDAO = (User_organogramDAO)request.getAttribute("user_organogramDAO");


String navigator2 = SessionConstants.NAV_USER_ORGANOGRAM;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";
%>				
				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
							<th><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_ROLEID, loginDTO)%></th>
							<th><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_USERTYPE, loginDTO)%></th>
							<th><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_ORGANOGRAMID, loginDTO)%></th>
							<th><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_DESIGNATION, loginDTO)%></th>
							<th><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_LanguageID, loginDTO)%></th>
							<th><%=LM.getText(LC.USER_ORGANOGRAM_EDIT_STATUS, loginDTO)%></th>
								
								<%
								if(isPermanentTable)
								{
									out.println("<th>" + LM.getText(LC.USER_ORGANOGRAM_SEARCH_USER_ORGANOGRAM_EDIT_BUTTON, loginDTO) + "</th>");
								}
								else if (!isPermanentTable && userDTO.approvalRole == SessionConstants.VALIDATOR)
								{
									out.println("<th><%=LM.getText(LC.HM_VALIDATE, loginDTO)%></th>");
								}
								%>
								
								
								<%
								if(!isPermanentTable && userDTO.approvalOrder > -1)
								{
									out.println("<th>Approve</th>");
									out.println("<th>Operation Type</th>");
									out.println("<th>Original Value</th>");
								}								
								%>
								<th><input type="submit" class="btn btn-xs btn-danger" value="
								<%
								if(isPermanentTable)
								{
									out.println(LM.getText(LC.USER_ORGANOGRAM_SEARCH_USER_ORGANOGRAM_DELETE_BUTTON, loginDTO));
								}
								else
								{
									out.println("REJECT");
								}
								%>
								" /></th>
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_USER_ORGANOGRAM);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											User_organogramDTO row = (User_organogramDTO) data.get(i);
											TempTableDTO tempTableDTO = null;
											if(!isPermanentTable)
											{
												tempTableDTO = user_organogramDAO.getTempTableDTOFromTableById(tableName, row.iD);
											}																						
											
											out.println("<tr id = 'tr_" + i + "'>");
											
								%>
											
		
								<%  								
								    request.setAttribute("user_organogramDTO",row);
								%>  
								
								 <jsp:include page="./user_organogramSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											out.println("</tr>");
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			