<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import="user.UserRepository"%>
<%@page import="user.UserTypeDTO"%>
<%@page import="user.UserTypeRepository"%>
<%@page import="user.UserDTO"%>
<%@page import="util.ServletConstant"%>
<%@page import="util.CommonConstant"%>
<%@page import="role.RoleDTO"%>
<%@page import="user_organogram.*"%>

<%

//request.setAttribute("ID", user_organogramDTO.iD);
//request.setAttribute("user_organogramDTO",user_organogramDTO);
//request.setAttribute("user_organogramDAO",user_organogramDAO);

String actionName = request.getParameter("actionType");


	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");
	String Options;
	LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	
	User_organogramDTO user_organogramDTO = (User_organogramDTO) request.getAttribute("user_organogramDTO");
	UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO2);
	UserDTO userDTO = (UserDTO)request.getAttribute(ServletConstant.USER_DTO);
	ArrayList<RoleDTO> roleList = (ArrayList<RoleDTO>)request.getAttribute(ServletConstant.ROLE_LIST); 
	
	if(userDTO == null)
	{
		userDTO = new UserDTO();
	}
	String Language2 = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO2);
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	
	
	
			
%>

<div class="pull-right">
	<a class="btn btn-info" href="InboxHistoryServlet?actionType=getHistory" >Back </a>
	<a class="btn btn-info" href="InboxHistoryServlet?actionType=getHistory" ><%=LM.getText(LC.HM_HISTORY, loginDTO)%></a>
</div>
<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">

				<form class="form-horizontal"  action="User_organogramServlet?actionType=add" id="bigform" name="bigform"  method="POST">
					<div class="portlet-window-body">
						<div class="row" style="margin-left: 0; margin-right: 0;">
							
	
							<div class="portlet light">
								<div class="portlet-title">
									<h3>Internal</h3>
									
								</div>
								<div class="portlet-window-body">
								
									<div class="form-group ">
										<label class="col-sm-3 control-label">
										<%=LM.getText(LC.USER_ADD_USER_TYPE, loginDTO2) %>
										</label>
					
										<div class="col-sm-6 ">
												<select class="form-control pull-right" name="userType" >
													<%
													if(actionName.equals("edit"))
													{
														for(UserTypeDTO userTypeDTO: UserTypeRepository.getInstance().getAllUserType())
														{
														%>
															<option value="<%=userTypeDTO.ID%>" <%if (userTypeDTO.ID == user_organogramDTO.userType){%> selected <%}%>><%=(loginUserDTO.languageID == CommonConstant.Language_ID_Bangla) ? userTypeDTO.name_bn : userTypeDTO.name_en%></option>
														<%}
													}
													else
													{
														for(UserTypeDTO userTypeDTO: UserTypeRepository.getInstance().getAllUserType())
														{
														%>
															<option value="<%=userTypeDTO.ID%>" <%if (userTypeDTO.ID == userDTO.userType){%> selected <%}%>><%=(loginUserDTO.languageID == CommonConstant.Language_ID_Bangla) ? userTypeDTO.name_bn : userTypeDTO.name_en%></option>
														<%}
													}
													
													%>																
													
												</select>
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-sm-3 control-label">
										<%=LM.getText(LC.USER_ADD_ROLE, loginDTO2) %>
										</label>
					
										<div class="col-sm-6 ">
												<select class="form-control pull-right" name="roleName" >
													<%
													
													if(actionName.equals("edit"))
													{
														for(RoleDTO roleDTO: roleList)
														{%>
															<option value="<%=roleDTO.ID%>" <%if (roleDTO.ID == user_organogramDTO.roleID){%> selected <%}%>><%=roleDTO.roleName%></option>
														<%}
													}
													else
													{
														for(RoleDTO roleDTO: roleList)
														{%>
															<option value="<%=roleDTO.ID%>" <%if (roleDTO.ID == userDTO.roleID){%> selected <%}%>><%=roleDTO.roleName%></option>
														<%}
													}
													
													%>																
													
												</select>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-sm-3 control-label">
										<%=LM.getText(LC.USER_ADD_LANGUAGE, loginDTO2) %>
										</label>
					
										<div class="col-sm-6 ">
												<select class="form-control pull-right" name="languageID" >
												<%
													
													if(actionName.equals("edit"))
													{
														
														%>
														
														<option value="1" <%if (1== user_organogramDTO.languageID){%> selected <%}%>>English</option>
														<option value="2" <%if (2== user_organogramDTO.languageID){%> selected <%}%>>বাংলা</option>
														
														<% 
													}
													else
													{
														%>
															<option value="1" <%if (1== userDTO.languageID){%> selected <%}%>>English</option>
														<option value="2" <%if (2== userDTO.languageID){%> selected <%}%>>বাংলা</option>
														<%
													}
													
													%>	
												
													
												</select>
										</div>
									</div>					
									
									
									<div class="clearfix"></div>

									<div id="office_unit_tree_panel"
										class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
										role="tree" state="0">
										<label style="font-weight:bold;">Select User: </label>
										<%@include file="treeNode.jsp"%>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions text-center">
							<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
								CANCEL
							</a>
							<input type="hidden" name="organogram_id" value="">
							<input type="hidden" name="designation" value="">
							<button class="btn btn-success"  onclick="myfunction()" type="submit">
								SUBMIT
							</button>
						</div>
					</div>
				</form>
			</div>


		</div>
	</div>
</div>



<script type="text/javascript">

function checkbox_toggeled(id, text, cb_id, name, parent_id)
{
	var cb = document.getElementById(cb_id);
	var parent = document.getElementById(parent_id);
	var child_cbs = parent.querySelectorAll('input[type = "checkbox"]');
	var treeCBChecked = document.getElementById("treeCBChecked_" + id);
	var i;
	if(cb.checked)
	{
		console.log('checked');
		treeCBChecked.value = id;
		for (i = 0; i < child_cbs.length; i++) 
		{
			child_cbs[i].checked = true;
			document.getElementById("treeCBChecked_" + child_cbs[i].value).value = child_cbs[i].value;
		}
	}
	else
	{
		console.log('unchecked');
		treeCBChecked.value = -1;
		for (i = 0; i < child_cbs.length; i++) 
		{
			child_cbs[i].checked = false;
			document.getElementById("treeCBChecked_" + child_cbs[i].value).value = -1;
		}
	}
}

function fillNextElementWithID(id, element)
{
	console.log("\n\nback in js, got element = " + element.getAttribute('id'));
	
}
</script>

<script>
$(function(){
	CKEDITOR.replace( 'note' );
});
	
</script>

<script>

var tempOrganogramId = "";

var designation = "";

function checkbox_toggeled(id, text, cb_id, name, parent_id)
{
	//alert("hi " + name + " : " + id + " : "+ text);
	
	tempOrganogramId = id;
	
	designation = text;
	
	var cb = document.getElementById(cb_id);
	if(cb.checked)
	{
		console.log('checked');
		addElement(id, text, cb_id, name, "addotherpermission");		
	}
	else
	{
		console.log('unchecked');
		var tr_id = "tr_selected_" + id;
		removeElementByID(tr_id);
	}
}

	$("#add-more-ProjectTrackerFiles").click(function(e) {
		e.preventDefault();
		var t = $("#template-ProjectTrackerFiles");
		$("#field-ProjectTrackerFiles").append(t.html());
		SetCheckBoxValues("field-ProjectTrackerFiles");

	});
	
    
    $("#remove-ProjectTrackerFiles").click(function(e){
	    var tablename = 'field-ProjectTrackerFiles';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });
    
    function myfunction() {
    	
    	//var tempOrganogramId = "";

    	//var designation = "";
    	
    	//var email = document.getElementById("organogram_id").value;
    	
    	//var email = document.getElementById("designation").value;
    	
    	//document.getElementById("organogram_id").innerHTML = tempOrganogramId;
    	
    	//document.getElementById("designation").innerHTML = designation;
    	
    	 document.getElementById("bigform").action = "http://119.148.4.20:8087/rootstocktest6/User_organogramServlet?actionType=add&id="+tempOrganogramId + "&designation="+ designation ; //Setting form action to "success.php" page
    	    document.getElementById("bigform").submit(); // Submitting form
    
    }
    
  
</script>


