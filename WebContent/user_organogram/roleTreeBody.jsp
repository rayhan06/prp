<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%

	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");

			
%>
<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-check-square-o"></i><%=pageTitle%>
					</div>

				</div>
				<div class="portlet-window-body">
					<div class="row">
						<%@include file="dropDownNode.jsp"%>
						<hr>
					</div>


					<div class="row">
						<div class="portlet light col-md-10">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>Select Role
								</div>
							</div>
							<div class="portlet-window-body">
							
							<form  action="Office_unit_organogramsServlet?actionType=MultiUpdate" id="bigform" name="bigform"  method="POST">
									<div
										class="jstree jstree-1 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
										id="origin_unit_tree_panel" role="tree" state="0">
										<table class="table table-bordered table-hover"
										id="tblOfficeUnitOrganograms">
										<tbody id="tbodylOfficeUnitOrganograms">
	
										</tbody>
										</table>
									</div>
									<div class="form-actions text-center">
										<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
											<%
		
												out.print(LM.getText(LC.OFFICE_UNIT_ORGANOGRAMS_EDIT_OFFICE_UNIT_ORGANOGRAMS_CANCEL_BUTTON, loginDTO));
										
											
											%>
										</a>
										<button class="btn btn-success" type="submit">
											<%
											
												out.print(LM.getText(LC.OFFICE_UNIT_ORGANOGRAMS_EDIT_OFFICE_UNIT_ORGANOGRAMS_SUBMIT_BUTTON, loginDTO));
											
											%>
										</button>
									</div>
								</form>
							</div>
						</div>						
					</div>
				</div>
			</div>


		</div>
	</div>
</div>





<script type="text/javascript">

function callGetFlatChildren()
{
	console.log("caller called");
	//getFlatChildren();
	
	var x = document.getElementsByName("tbodytoload");
	var i;
	for (i = 0; i < x.length; i++) 
	{
		var id = x[i].getAttribute("data");
		var elementid = x[i].getAttribute("id");
		getChildrenFlat(1,id, elementid, "getFlatUnits", "", id, "");
	  
	}
}

function fillNextElementWithID(id, element)
{
	console.log("\n\nback in js, got element = " + element.getAttribute('id'));
	if(element.getAttribute('id') === "node_div_offices")
	{
		var office_origins = document.getElementById("select_office_origins");
		var office_origins_id = office_origins.options[office_origins.selectedIndex].value;
		var office = document.getElementById("select_offices");
		var office_id = office.options[office.selectedIndex].value;
		console.log("got office_origins_id = " + office_origins_id + " office_id = " + office_id);
		
		document.getElementById("tbodylOfficeUnitOrganograms").innerHTML = "";

		
		global_office_origins_id = office_origins_id;
		getChildrenFlat(0, office_id, "tbodylOfficeUnitOrganograms", "getFlatUnits", callGetFlatChildren, office_id, "");		
		
	}
}
</script>
