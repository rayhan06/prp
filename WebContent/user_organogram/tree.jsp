<% 
String treeBody = "../user_organogram/" + request.getParameter("treeBody");
request.setAttribute("treeBodyPath",treeBody);
System.out.println("TreeBody = " + treeBody);
System.out.println("TreeType = " + request.getParameter("treeType"));
%>
<jsp:include page="../common/layout.jsp" flush="true">
<jsp:param name="title" value="View Tree" /> 
	<jsp:param name="body" value="${treeBodyPath}" />
</jsp:include> 
<script type="text/javascript">

		var autoExpand = false;


		function doAutoExpand(parentElementID)
		{
			$("#" + parentElementID + " ul").find( ".fa-arrows" ).each(function() {
					//console.log("found, id = " + $(this).attr('id'));
			       $(this).click();
		     });
		}
		
		function getChildrenInElement(layer, id, element, treeType, actionType, returnfunc, parentID, parentElementID, extraParams, layer_id_pairs)
		{
			var servletType = document.getElementById('servletType').value;
			if(layer_id_pairs === undefined)
			{
				layer_id_pairs = "";
			}
			console.log("layer_id_pairs = " + layer_id_pairs);
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() 
			{
				if (this.readyState == 4 && this.status == 200) 
				{
					if(this.responseText !='')
					{
						//console.log("\n\nResponse: " + this.responseText + "\n\n");
						if(treeType === "select")
						{
							element.innerHTML = this.responseText ;
						}
						else
						{
							element.innerHTML += this.responseText ;
						}
						
						if(autoExpand == true)
						{
							doAutoExpand(parentElementID);
						}
						
						if(returnfunc)
						{
							returnfunc();
						}
					}
					else
					{
						console.log("null response");
						fillNextElementWithID(id, element);
					}
				}
				else if(this.readyState == 4 && this.status != 200)
				{
					alert('failed ' + this.status);
				}
			};
			
	      var requestmsg = servletType + "?actionType=" + actionType 
	    		  + "&myLayer=" + layer 
	    		  + "&id=" + id 
	    		  + "&treeType=" + treeType 
	    		  + "&parentID=" + parentID
	    		  + "&parentElementID=" + parentElementID
	    		  + "&layer_id_pairs=" + layer_id_pairs
	    		  + extraParams;
			
		  //console.log("requestmsg = " + requestmsg);

		  xhttp.open("Get", requestmsg, true);
		  xhttp.send();	
		}
		
		function getChildrenSelect(layer, selectElement, divElement, actionType, retunfunc, parentID, extraParams, layer_id_pairs) //works for select
		{
			//console.log("getChildren called for dropdown");
			//console.log("servletType = " + document.getElementById('servletType').value);
			//console.log("actionType = " + actionType);
			var div = document.getElementById(divElement);
			var select = document.getElementById(selectElement);
			var id = select.options[select.selectedIndex].value;
			
		
			getChildrenInElement(layer, id, div, "select", actionType, retunfunc, 0, parentID, divElement, extraParams, layer_id_pairs);
						
		}

		function getChildren(layer, id, parentElement, actionType, retunfunc, parentID, extraParams, checkBoxID, layer_id_pairs)
		{
			//console.log("servletType = " + document.getElementById('servletType').value);
			//console.log("actionType = " + actionType + " layer = " + layer + " id= " + id + ' parentID = ' + parentID + ' parentElement = ' + parentElement);
			var parent = document.getElementById(parentElement);
			if(parent.getAttribute('state') == '0')
			{
				var treeCBStatus = document.getElementById("treeCBStatus_" + id);
				if(treeCBStatus !== null)
				{
					treeCBStatus.value = 1;
				}
				var xhttp = new XMLHttpRequest();
				parent.setAttribute("state", 1);
				var checkBox = document.getElementById(checkBoxID);
				var checkBoxChecked = false;
				if(checkBox !== null)
				{
					checkBoxChecked = checkBox.checked;
				}
				extraParams += "&checkBoxChecked=" + checkBoxChecked;
				getChildrenInElement(layer, id, parent, "tree", actionType, retunfunc, parentID, parentElement, extraParams, layer_id_pairs);
			}
			else if(parent.getAttribute('state') == '1')
			{
				console.log("state = " + parent.getAttribute('state'));
				parent.getElementsByTagName('ul')[0].style = "display:none";
				parent.setAttribute("state", 2);
			}
			else if(parent.getAttribute('state') == '2')
			{
				console.log("state = " + parent.getAttribute('state'));
				parent.getElementsByTagName('ul')[0].style = "display:block";
				parent.setAttribute("state", 1);
			}
			
		}
		
		
		function getChildrenFlat(layer, id, parentElement, actionType, retunfunc, parentID, extraParams,layer_id_pairs)
		{
			//console.log("servletType = " + document.getElementById('servletType').value);
			//console.log("actionType = " + actionType + " layer = " + layer + " id= " + id + ' parentID = ' + parentID);
			var parent = document.getElementById(parentElement);
			
			var xhttp = new XMLHttpRequest();
			parent.setAttribute("state", 1);
			getChildrenInElement(layer, id, parent, "flat", actionType, retunfunc, parentID, parentElement, extraParams, layer_id_pairs);
			
			
		}
		
		function fillChildren(id, text, cb_id, name, parent_id)
		{
			var cb = document.getElementById(cb_id);
			var parent = document.getElementById(parent_id);
			var child_cbs = parent.querySelectorAll('input[type = "checkbox"]');
			var treeCBChecked = document.getElementById("treeCBChecked_" + id);
			var ids_checked_or_unchecked = [];
			var i;
			if(cb.checked)
			{
				console.log('checked');
				treeCBChecked.value = id;
				ids_checked_or_unchecked[0] = id;
				for (i = 0; i < child_cbs.length; i++) 
				{
					child_cbs[i].checked = true;
					document.getElementById("treeCBChecked_" + child_cbs[i].value).value = child_cbs[i].value;
					ids_checked_or_unchecked[i + 1] = child_cbs[i].value;
				}
			}
			else
			{
				console.log('unchecked');
				treeCBChecked.value = -1;
				treeCBChecked.value = id;
				for (i = 0; i < child_cbs.length; i++) 
				{
					child_cbs[i].checked = false;
					document.getElementById("treeCBChecked_" + child_cbs[i].value).value = -1;
					ids_checked_or_unchecked[i + 1] = child_cbs[i].value;
				}
			}
			return ids_checked_or_unchecked;
		}
		
		function moveup(i)
		{
			var row = i.closest("tr");
			console.log("rowindex = " + row.rowIndex);
			
			var sibling = row.previousElementSibling;
		    var parent = row.parentNode;

		 	parent.insertBefore(row, sibling);
		}

		function movedown(i)
		{
			var row = i.closest("tr");
			console.log("rowindex = " + row.rowIndex);
			var sibling = row.nextElementSibling;
		    var parent = row.parentNode;

		 	parent.insertBefore(sibling, row);
		}

		function removeElementByID(id)
		{
			console.log("removing element " + id);
			var element = document.getElementById(id);
			element.parentNode.removeChild(element);
		}

		function removeElementByIDandUncheckCheckbox(id, cb_id)
		{
			removeElementByID(id);
			document.getElementById(cb_id).checked = false;
		}

		function addElement(id, text, cb_id, name, tbodyID)
		{
			if(document.getElementById("td_delete_" + id) == null)
			{		
				var tbody = document.getElementById(tbodyID);
				var template = document.getElementById('tr-template');
				var template_backup = template.innerHTML;
				
				
				var tr = template.rows[0];
				var tr_backup = tr;
				var tr_id = "tr_selected_" + id;
				tr.setAttribute("id", tr_id);
				var i = tr.querySelector("i");
				i.setAttribute("onclick", "removeElementByIDandUncheckCheckbox('" + tr_id + "', '" + cb_id + "')");
					
				var hiddeninput = tr.querySelector('input[type="hidden"]');
				hiddeninput.setAttribute("name", "organogram_id");
				hiddeninput.setAttribute("value", id);
				hiddeninput.setAttribute("id", "organogram_id_hidden_" + id);
				console.log("organogram_id_hidden = " + document.getElementById("organogram_id_hidden_" + id).value);
				
				var select = tr.querySelector("select");
				select.setAttribute("name", "roleType");
			
				
				var td_delete = tr.cells[0];
				td_delete.setAttribute("id", "td_delete_" + id);
				
			
				
				var td_text = tr.cells[1];
				if(name !== "")
				{
					td_text.innerHTML = text + ", " + name;
				}
				else
				{
					td_text.innerHTML = text;
				}
				
				template.innerHTML = template_backup;
			
			
				tbody.innerHTML += tr.outerHTML ;
			}
			
		}

			
</script>
