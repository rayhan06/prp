
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="user_organogram.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
User_organogramDTO user_organogramDTO;
user_organogramDTO = (User_organogramDTO)request.getAttribute("user_organogramDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(user_organogramDTO == null)
{
	user_organogramDTO = new User_organogramDTO();
	
}
System.out.println("user_organogramDTO = " + user_organogramDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.USER_ORGANOGRAM_EDIT_USER_ORGANOGRAM_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.USER_ORGANOGRAM_ADD_USER_ORGANOGRAM_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
User_organogramDTO row = user_organogramDTO;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="User_organogramServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.USER_ORGANOGRAM_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_ORGANOGRAM_EDIT_USERID, loginDTO)):(LM.getText(LC.USER_ORGANOGRAM_ADD_USERID, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'userId_div_<%=i%>'>	
		<input type='text' class='form-control'  name='userId' id = 'userId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogramDTO.userId + "'"):("'" + "0" + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_ORGANOGRAM_EDIT_ORGANOGRAMID, loginDTO)):(LM.getText(LC.USER_ORGANOGRAM_ADD_ORGANOGRAMID, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'organogramId_div_<%=i%>'>	
		<input type='text' class='form-control'  name='organogramId' id = 'organogramId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogramDTO.organogramId + "'"):("'" + "0" + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_ORGANOGRAM_EDIT_STATUS, loginDTO)):(LM.getText(LC.USER_ORGANOGRAM_ADD_STATUS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'status_div_<%=i%>'>	
		<input type='text' class='form-control'  name='status' id = 'status_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogramDTO.status + "'"):("'" + "0" + "'")%>   />					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + user_organogramDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogramDTO.lastModificationTime + "'"):("'" + "0" + "'")%>/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.USER_ORGANOGRAM_EDIT_USER_ORGANOGRAM_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.USER_ORGANOGRAM_ADD_USER_ORGANOGRAM_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.USER_ORGANOGRAM_EDIT_USER_ORGANOGRAM_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.USER_ORGANOGRAM_ADD_USER_ORGANOGRAM_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	return true;
}

function PostprocessAfterSubmiting(row)
{
}


function init(row)
{
}var row = 0;
//bkLib.onDomLoaded(function() 
//{	
//});
	
window.onload =function ()
{
	init(row);
}





</script>






