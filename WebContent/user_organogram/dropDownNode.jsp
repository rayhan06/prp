<%@ page language="java"%>
<%@ page import="java.util.*"%>
<%@ page import="pb.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="treeView.*"%>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO);
String actionType = request.getParameter("actionType");
String servletType2 = request.getParameter("servletType");

TreeDTO treeDTO = (TreeDTO)request.getAttribute("treeDTO");
int myLayer = (int)request.getAttribute("myLayer");
ArrayList<Integer> nodeIDs = (ArrayList<Integer>) request.getAttribute("nodeIDs");
int parentID = Integer.parseInt(request.getParameter("parentID"));

int maxLayer = treeDTO.parentChildMap.length;
String geoName = treeDTO.parentChildMap[myLayer];
String englishNameColumn = treeDTO.englishNameColumn[myLayer];
String banglaNameColumn = treeDTO.banglaNameColumn[myLayer];
String treeName = treeDTO.treeName;
int checkBoxType = treeDTO.checkBoxType;
int plusButtonType =  treeDTO.plusButtonType;
String showName = treeDTO.showNameMap[myLayer];
%>
<div class="col-md-4 form-group form-horizontal">
<label class="col-lg-3 control-label"><%=showName %></label>
<select class="form-control" id = "select_<%=geoName%>" onChange="getChildrenSelect('<%=myLayer + 1%>', 'select_<%=geoName%>', 'node_div_<%=geoName%>', '<%=actionType%>', null, <%=parentID%>,'')">
<option value = '-1'>Select <%=showName %></option>
<%
for(int i = 0; i < nodeIDs.size(); i ++)
{
	int id = nodeIDs.get(i);
%>
	<option value = '<%=id %>'><%=GenericTree.getName(geoName, id, Language, englishNameColumn, banglaNameColumn) %></option>									
<%
}
%>
</select>
</div>
<div id = 'node_div_<%=geoName%>'>
</div>