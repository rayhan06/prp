<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="user_organogram.User_organogramDTO"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
User_organogramDTO user_organogramDTO = (User_organogramDTO)request.getAttribute("user_organogramDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(user_organogramDTO == null)
{
	user_organogramDTO = new User_organogramDTO();
	
}
System.out.println("user_organogramDTO = " + user_organogramDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";
User_organogramDTO row = user_organogramDTO;
%>




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.USER_ORGANOGRAM_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_userId'>")%>
			
	
	<div class="form-inline" id = 'userId_div_<%=i%>'>
		<input type='text' class='form-control'  name='userId' id = 'userId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogramDTO.userId + "'"):("'" + "0" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_organogramId'>")%>
			
	
	<div class="form-inline" id = 'organogramId_div_<%=i%>'>
		<input type='text' class='form-control'  name='organogramId' id = 'organogramId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogramDTO.organogramId + "'"):("'" + "0" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_status'>")%>
			
	
	<div class="form-inline" id = 'status_div_<%=i%>'>
		<input type='text' class='form-control'  name='status' id = 'status_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogramDTO.status + "'"):("'" + "0" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + user_organogramDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=user_organogramDTO.lastModificationTime%>'/>
		
												
<%=("</td>")%>
					
		