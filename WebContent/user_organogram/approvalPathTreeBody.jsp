<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%

	String servletType = request.getParameter("servletType");
	String pageTitle = request.getParameter("pageTitle");
	String Options;
	LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language2 = LM.getText(LC.CENTRE_EDIT_LANGUAGE, loginDTO2);
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	
	String pathType = request.getParameter("pathType");
	
	int iPathType = Integer.parseInt(pathType);
			
%>
<table id='tr-template' style='display: none'>
	<tr>
		<td class="deleterow" style="width: 30px; text-align: center;"><i
			class="fa fa-times"></i>
		</td>

		<td class="text-left">&nbsp;&nbsp;শেখ হাসিনা, প্রধানমন্ত্রী,
			প্রধানমন্ত্রীর কার্যালয়, প্রধানমন্ত্রীর কার্যালয়			
		</td>
		<td>
			<span class="up-down-link"> 
				<a class="up-link">
					<span>
						<i onclick="moveup(this)" class="fa fa-arrow-circle-up"></i>
					</span>
				</a>
				<a class="down-link">
					<span> 
						<i onclick="movedown(this)" class="fa fa-arrow-circle-down"></i>
					</span>
				</a>
				<input type = "hidden" name = "organogram_id_template" value =""></input>
			</span>
		</td>
		
		<td>
			<select class='form-control'  name='roleType_template'   >
			<%
				
				Options = CommonDAO.getOptions(Language2, "select", "approval_role", "roleType", "form-control", "approval_roleType" );			
	
				out.print(Options);
			%>
			</select>
		</td>
	</tr>
</table>

<input type='hidden' id='servletType' value='<%=servletType%>' />
<div class="row" style="margin: 0px !important;">
	<div class="col-md-12" style="padding: 5px !important;">
		<div id="ajax-content">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-check-square-o"></i><%=pageTitle%>
					</div>

				</div>
				<div class="portlet-window-body">
					<div class="row">
						<%@include file="dropDownNode.jsp"%>
						<hr>
					</div>


					<div class="row">
						<div class="portlet light col-md-5">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>Add Approval Path
								</div>
							</div>
							<div class="portlet-window-body">
								<form class="form-horizontal" action="Approval_pathServlet?actionType=add&pathType=<%=pathType%>" id="bigform" name="bigform"  method="POST" onsubmit="return PreprocessBeforeSubmiting()">
									<div
										class="jstree jstree-1 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
										id="origin_unit_tree_panel" role="tree" state="0">
										
										<table class="table table-bordered">
											<tbody id="addotherpermission">
	
											
																							
											</tbody>
										</table>
										
										<label class="col-lg-3 control-label">Name in English</label>
										<div class="form-group ">					
											<div class="col-lg-6 ">	
												<input type='text' class='form-control'  name='nameEn' id = 'nameEn'/>					
											</div>
										</div>
										
										<label class="col-lg-3 control-label">Name in Bangla</label>
										<div class="form-group ">					
											<div class="col-lg-6 ">	
												<input type='text' class='form-control'  name='nameBn' id = 'nameBn'/>					
											</div>
										</div>
										
										
										
										<label class="col-lg-3 control-label">Employee Organogram ID</label>
										<div class="form-group ">					
											<div class="col-lg-6 ">	
												<input type='text' class='form-control'  name='organogramId' id = 'organogramId'/>					
											</div>
										</div>
										
										<input type='hidden' class='form-control'  name='officeOriginId' id = 'officeOriginId'/>
										
											
										<label class="col-lg-3 control-label">Creator Organogram ID</label>
										<div class="form-group ">					
											<div class="col-lg-6 ">	
												<input type='text' class='form-control'  name='creatorOrganogramId' id = 'creatorOrganogramId'/>					
											</div>
										</div>
										
										
										<input type='hidden' class='form-control'  name='createdDate' id = 'createdDate' value='<%=datestr%>'/>					
											
										
										
										<div class="form-actions text-center">
											<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
											Cancel
											</a>
											<button class="btn btn-success" type="submit">
											Submit
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="portlet light col-md-5">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-check-square-o"></i>Select Units
								</div>
							</div>
							<div class="portlet-window-body">
								<div id="office_unit_tree_panel"
									class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
									role="tree" state="0">
									<ul class="jstree-container-ul jstree-children"></ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>



<script type="text/javascript">

function PreprocessBeforeSubmiting()
{
	
		
	console.log("found date = " + document.getElementById('createdDate').value);
	document.getElementById('createdDate').value = new Date(document.getElementById('createdDate').value).getTime();
	

	var office_origins = document.getElementById("select_office_origins");
	var office_origins_id = office_origins.options[office_origins.selectedIndex].value;
	document.getElementById('officeOriginId').value = office_origins_id;

	
	return true;
}


function checkbox_toggeled(id, text, cb_id, name, parent_id)
{
	var cb = document.getElementById(cb_id);
	if(cb.checked)
	{
		console.log('checked');
		addElement(id, text, cb_id, name, "addotherpermission");		
	}
	else
	{
		console.log('unchecked');
		var tr_id = "tr_selected_" + id;
		removeElementByID(tr_id);
	}
}

function fillNextElementWithID(id, element)
{
	console.log("\n\nback in js, got element = " + element.getAttribute('id'));
<% 
	if(iPathType == 1)
	{
%>	
	if(element.getAttribute('id') === "node_div_offices")
	{
		var office_origins = document.getElementById("select_office_origins");
		var office_origins_id = office_origins.options[office_origins.selectedIndex].value;
		var office = document.getElementById("select_offices");
		var office_id = office.options[office.selectedIndex].value;
		console.log("got office_origins_id = " + office_origins_id + " office_id = " + office_id);
		
		document.getElementById("office_unit_tree_panel").innerHTML = "";
		document.getElementById("office_unit_tree_panel").setAttribute("state", 0);
		
		

		getChildren(0, 0, "office_unit_tree_panel", "getUnitWithCheckBox", null, office_id, "");		
		
	}
<%
}
	else if(iPathType == 2)
	{
%>	
		if(element.getAttribute('id') === "node_div_office_origins")
		{
			var office_origins = document.getElementById("select_office_origins");
			var office_origins_id = office_origins.options[office_origins.selectedIndex].value;

			console.log("got office_origins_id = " + office_origins_id);
			
			document.getElementById("office_unit_tree_panel").innerHTML = "";
			document.getElementById("office_unit_tree_panel").setAttribute("state", 0);
			
			
		
			getChildren(0, 0, "office_unit_tree_panel", "getUnitOriginWithCheckBox", null, office_origins_id, "");		
			
		}
<%
	}
%>
}
</script>
