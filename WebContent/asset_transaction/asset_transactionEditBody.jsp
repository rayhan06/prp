<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="asset_transaction.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Asset_transactionDTO asset_transactionDTO;
asset_transactionDTO = (Asset_transactionDTO)request.getAttribute("asset_transactionDTO");
CommonDTO commonDTO = asset_transactionDTO;
if(asset_transactionDTO == null)
{
	asset_transactionDTO = new Asset_transactionDTO();
	
}
String tableName = "asset_transaction";
%>
<%@include file="../pb/addInitializer.jsp"%>
<%
String formTitle = LM.getText(LC.ASSET_TRANSACTION_ADD_ASSET_TRANSACTION_ADD_FORMNAME, loginDTO);
String servletName = "Asset_transactionServlet";
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_transactionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=asset_transactionDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_TABLENAME, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='tableName' id = 'tableName_text_<%=i%>' value='<%=asset_transactionDTO.tableName%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_BRANDTYPE, loginDTO)%></label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='brandType' id = 'brandType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "brand", asset_transactionDTO.brandType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSETMODELTYPE, loginDTO)%></label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='assetModelType' id = 'assetModelType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "asset_model", asset_transactionDTO.assetModelId);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSETCATEGORYTYPE, loginDTO)%></label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='assetCategoryType' id = 'assetCategoryType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "asset_category", asset_transactionDTO.assetCategoryId);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_MODEL, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='model' id = 'model_text_<%=i%>' value='<%=asset_transactionDTO.model%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_SL, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='sl' id = 'sl_text_<%=i%>' value='<%=asset_transactionDTO.sl%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_TRANSACTIONCAT, loginDTO)%></label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='transactionCat' id = 'transactionCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("transaction", Language, asset_transactionDTO.transactionCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_LOT, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='lot' id = 'lot_text_<%=i%>' value='<%=asset_transactionDTO.lot%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_OFFICEUNITTYPE, loginDTO)%></label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='officeUnitType' id = 'officeUnitType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "office_unit", asset_transactionDTO.officeUnitType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_WINGTYPE, loginDTO)%></label>
                                                            <div class="col-md-8">
																<select class='form-control'  name='wingType' id = 'wingType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "wing", asset_transactionDTO.wingType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='assignedUserId' id = 'assignedUserId_hidden_<%=i%>' value='<%=asset_transactionDTO.assignedUserId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSIGNEDORGANOGRAMID, loginDTO)%></label>
                                                            <div class="col-md-8">
																<button type="button" class="btn btn-primary btn-block shadow btn-border-radius" onclick="addEmployeeWithRow(this.id)" id="assignedOrganogramId_button_<%=i%>" tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%></button>
																<table class="table table-bordered table-striped">
																	<tbody id="assignedOrganogramId_table_<%=i%>" tag='pb_html'>
																	<%
																	if(asset_transactionDTO.assignedOrganogramId != -1)
																	{
																		%>
																		<tr>
																			<td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(asset_transactionDTO.assignedOrganogramId)%></td>
																			<td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(asset_transactionDTO.assignedOrganogramId, Language)%></td>
																			<td><%=WorkflowController.getOrganogramName(asset_transactionDTO.assignedOrganogramId, Language)%>, <%=WorkflowController.getUnitNameFromOrganogramId(asset_transactionDTO.assignedOrganogramId, Language)%></td>
																		</tr>
																		<%
																	}
																	%>
																	</tbody>
																</table>
																<input type='hidden' class='form-control'  name = 'assignedOrganogramId' id = 'assignedOrganogramId_hidden_<%=i%>' value='<%=asset_transactionDTO.assignedOrganogramId%>' tag='pb_html'/>
			
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_RECEIVERNAMEEN, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='receiverNameEn' id = 'receiverNameEn_text_<%=i%>' value='<%=asset_transactionDTO.receiverNameEn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_RECEIVERNAMEBN, loginDTO)%></label>
                                                            <div class="col-md-8">
																<input type='text' class='form-control'  name='receiverNameBn' id = 'receiverNameBn_text_<%=i%>' value='<%=asset_transactionDTO.receiverNameBn%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_TRANSACTION_ADD_TRANSACTIONDATE, loginDTO)%></label>
                                                            <div class="col-md-8">
																<%value = "transactionDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='transactionDate' id = 'transactionDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(asset_transactionDTO.transactionDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=asset_transactionDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=asset_transactionDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=asset_transactionDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=asset_transactionDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='lastModifierUser' id = 'lastModifierUser_hidden_<%=i%>' value='<%=asset_transactionDTO.lastModifierUser%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=asset_transactionDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=asset_transactionDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSET_TRANSACTION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSET_TRANSACTION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{
	preprocessDateBeforeSubmitting('transactionDate', row);

	submitAddForm();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Asset_transactionServlet");	
}

function init(row)
{

	setDateByStringAndId('transactionDate_js_' + row, $('#transactionDate_date_' + row).val());

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






