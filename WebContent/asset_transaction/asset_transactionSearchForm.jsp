
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_transaction.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navASSET_TRANSACTION";
String servletName = "Asset_transactionServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.ASSET_TRANSACTION_ADD_TRANSACTIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_TRANSACTION_ADD_TRANSACTIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_TRANSACTION_ADD_BRANDTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_MODEL, loginDTO)%></th>
								<th><%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSETCATEGORYTYPE, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_SL, loginDTO)%></th>
								
								<th><%=LM.getText(LC.ASSET_MODEL_ADD_LOT, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_COST, loginDTO)%></th>

								<th><%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_DESCRIPTION, loginDTO)%></th>

								
<%-- 								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								 --%>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute("viewASSET_TRANSACTION");

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Asset_transactionDTO asset_transactionDTO = (Asset_transactionDTO) data.get(i);
																																
											
											%>
											<tr>
											
											<td>
											
											<%
											String formatted_transactionDate = simpleDateFormat.format(new Date(asset_transactionDTO.transactionDate));
											%>
											<%=Utils.getDigits(formatted_transactionDate, Language)%>
				
			
											</td>
											
											<td>
											
											<%
											value = CatRepository.getInstance().getText(Language, "assignment_status", asset_transactionDTO.transactionCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
								

		
											<td>
											<%
											value = asset_transactionDTO.brandType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "brand", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
											<td>
											<%
											value = asset_transactionDTO.model + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
		
											<td>
											<%
												value = asset_transactionDTO.assetCategoryId + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "asset_category", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
										
		
											<td>
											<%
											value = asset_transactionDTO.sl + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
		
											<td>
											<%
											value = asset_transactionDTO.lot + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>

											<td>

												<%=Utils.getDigits(String.format("%.2f", asset_transactionDTO.cost), Language)%>

											</td>

		
											<td>
											
										

												<%=WorkflowController.getNameFromEmployeeRecordID (asset_transactionDTO.employeeRecordId, Language)%>,
												<%=WorkflowController.getOrganogramName(asset_transactionDTO.assignedOrganogramId, Language)%>
				
											
											</td>
											
											<td>
											
												<%
												if(asset_transactionDTO.toOrgId != -1)
												{
												%>
				
												<%=WorkflowController.getNameFromOrganogramId (asset_transactionDTO.toOrgId, Language)%>,
												<%=WorkflowController.getOrganogramName(asset_transactionDTO.toOrgId, Language)%>
												<%
												}
												%>
				
											
											</td>
											
											<td>
											
										
												<%=asset_transactionDTO.comment%>
												
				
											
											</td>
		
										
		
										
		
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = asset_transactionDTO; %>
<!-- 											<td> -->
<!-- 											    <button -->
<!-- 											            type="button" -->
<!-- 											            class="btn-sm border-0 shadow bg-light btn-border-radius" -->
<!-- 											            style="color: #ff6b6b;" -->
<%-- 											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'" --%>
<!-- 											    > -->
<!-- 											        <i class="fa fa-eye"></i> -->
<!-- 											    </button> -->
<!-- 											</td>											 -->
																						
											
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			