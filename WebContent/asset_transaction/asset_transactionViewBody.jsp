

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_transaction.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Asset_transactionServlet";
String ID = request.getParameter("ID");
Asset_transactionDAO asset_transactionDAO = new Asset_transactionDAO("asset_transaction");
long id = Long.parseLong(ID);
Asset_transactionDTO asset_transactionDTO = (Asset_transactionDTO)asset_transactionDAO.getDTOByID(id);
CommonDTO commonDTO = asset_transactionDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSET_TRANSACTION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2">
                            <div class="col-md-10 offset-md-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSET_TRANSACTION_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_TABLENAME, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.tableName + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_BRANDTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.brandType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "brand", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSETMODELTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
												value = asset_transactionDTO.assetModelId + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "asset_model", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSETCATEGORYTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
												value = asset_transactionDTO.assetCategoryId + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "asset_category", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_MODEL, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.model + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_SL, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.sl + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_TRANSACTIONCAT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.transactionCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "transaction", asset_transactionDTO.transactionCat);
											%>	
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_LOT, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.lot + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_OFFICEUNITTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.officeUnitType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "office_unit", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_WINGTYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.wingType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "wing", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSIGNEDUSERID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.assignedUserId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_ASSIGNEDORGANOGRAMID, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.assignedOrganogramId + "";
											%>
											<%
											value = WorkflowController.getNameFromOrganogramId(asset_transactionDTO.assignedOrganogramId, Language) + ", " + WorkflowController.getOrganogramName(asset_transactionDTO.assignedOrganogramId, Language) + ", " + WorkflowController.getUnitNameFromOrganogramId(asset_transactionDTO.assignedOrganogramId, Language);
											%>											
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_RECEIVERNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.receiverNameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_RECEIVERNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.receiverNameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_TRANSACTION_ADD_TRANSACTIONDATE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8 form-control">
											<%
											value = asset_transactionDTO.transactionDate + "";
											%>
											<%
											String formatted_transactionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_transactionDate, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>