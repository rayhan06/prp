<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="new_user_task.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="java.util.List" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.NEW_USER_TASK_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    New_user_taskDAO new_user_taskDAO = (New_user_taskDAO) request.getAttribute("new_user_taskDAO");


    String navigator2 = SessionConstants.NAV_NEW_USER_TASK;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.NEW_USER_TASK_SEARCH_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.NEW_USER_TASK_SEARCH_NOTIFIEDRECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.NEW_USER_TASK_ADD_STATUS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            List<New_user_taskDTO> data = (List<New_user_taskDTO>) rn2.list;


            if (data != null && data.size() > 0) {
                int size = data.size();
                for (int i = 0; i < size; i++) {
                    New_user_taskDTO new_user_taskDTO = data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("new_user_taskDTO", new_user_taskDTO);
            %>

            <jsp:include page="./new_user_taskSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                }
            }


        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			