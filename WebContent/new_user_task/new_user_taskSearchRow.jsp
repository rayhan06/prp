<%@page pageEncoding="UTF-8" %>

<%@page import="new_user_task.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="test_lib.EmployeeRecordDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="common.BaseServlet" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.NEW_USER_TASK_EDIT_LANGUAGE, loginDTO);


    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    boolean isPermanentTable = rn2.m_isPermanentTable;

    System.out.println("isPermanentTable = " + isPermanentTable);
    New_user_taskDTO new_user_taskDTO = (New_user_taskDTO) request.getAttribute("new_user_taskDTO");


    System.out.println("new_user_taskDTO = " + new_user_taskDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));

    Employee_recordsDTO employeeRecordDTO = Employee_recordsRepository.getInstance().getById(new_user_taskDTO.employeeRecordsId);
    Employee_recordsDTO notifiedRecordDTO = Employee_recordsRepository.getInstance().getById(new_user_taskDTO.notifiedRecordsId);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);

%>

<td id='<%=i%>_employeeRecordsId'>
    <%=isLanguageEnglish ? employeeRecordDTO.nameEng : employeeRecordDTO.nameBng%>
</td>

<td id='<%=i%>_notifiedRecordsId'>
    <%=isLanguageEnglish ? notifiedRecordDTO.nameEng : notifiedRecordDTO.nameBng%>

</td>

<td id='<%=i%>_status'>
<span class="btn btn-sm border-0 shadow"
      style="background-color:<%=TaskStatusEnum.getColor(new_user_taskDTO.statusCat)%>; color: white; border-radius: 8px;cursor: text">
    <%=CatRepository.getInstance().getText(Language, "new_user_task_status", new_user_taskDTO.statusCat)%>
</span>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='New_user_taskServlet?actionType=view&ID=<%=new_user_taskDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=new_user_taskDTO.iD%>'/></span>
    </div>
</td>
																						
											

