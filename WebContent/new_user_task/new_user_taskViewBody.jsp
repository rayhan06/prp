<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="new_user_task.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="user.UserRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String Language = LM.getText(LC.NEW_USER_TASK_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    New_user_taskDAO new_user_taskDAO = New_user_taskDAO.getInstance();
    New_user_taskDTO new_user_taskDTO = new_user_taskDAO.getDTOByID(id);
    int i = 0;
    String formTitle = LM.getText(LC.NEW_USER_TASK_NEW_USER_TASK_STATUS, loginDTO);
    UserRepository userRepositoryInstance = UserRepository.getInstance();
    Employee_recordsDTO employee_recordsDTO = Employee_recordsRepository.getInstance().getById(new_user_taskDTO.employeeRecordsId);
    Employee_recordsDTO notified_recordsDTO = Employee_recordsRepository.getInstance().getById(new_user_taskDTO.notifiedRecordsId);
    String employeeUserName = Utils.getDigits(userRepositoryInstance.getUserDtoByEmployeeRecordId(new_user_taskDTO.employeeRecordsId).userName, Language);
    String notifiedUserName = Utils.getDigits(userRepositoryInstance.getUserDtoByEmployeeRecordId(new_user_taskDTO.notifiedRecordsId).userName, Language);
    EmployeeOfficeDTO officeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(new_user_taskDTO.notifiedRecordsId);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
    String officeDesignationForNotified = "";
    if (officeDTO != null) {
        long fromOrganongramId = officeDTO.officeUnitOrganogramId;
        OfficeUnitOrganograms notifiedUserOfficeDTO = OfficeUnitOrganogramsRepository.getInstance().getById(fromOrganongramId);
        if (notifiedUserOfficeDTO != null) {
            officeDesignationForNotified = isLanguageEnglish ? notifiedUserOfficeDTO.designation_eng + "<br>" + Office_unitsRepository.getInstance().geText("ENGLISH", notifiedUserOfficeDTO.office_unit_id) :
                    notifiedUserOfficeDTO.designation_bng + "<br>" + Office_unitsRepository.getInstance().geText("BANGLA", notifiedUserOfficeDTO.office_unit_id);
        }
    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <%--                <h3 class="kt-portlet__head-title prp-page-title">--%>
                <%--                    <i class="fa fa-gift"></i>&nbsp;--%>
                <%--                    <%=formTitle%>--%>
                <%--                </h3>--%>
            </div>
        </div>
        <form class="form-horizontal"
              action="New_user_taskServlet?actionType=updateTaskStatus&ID=<%=ID%>>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=new_user_taskDTO.iD%>' tag='pb_html'/>
                                    <div class="row mt-3" id="search_employee_info">
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between">
                                                <h4 class="caption-subject font-blue-madison table-title"><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_PERSONAL_INFORMATION, loginDTO))%>
                                                </h4>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th><%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_USERNAME, loginDTO))%>
                                                        </th>
                                                        <th><%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAME, loginDTO))%>
                                                        </th>
                                                        <th><%=(LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO))%>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="info_table_body">
                                                    <tr>
                                                        <td id="p_user_id"><%=employeeUserName%>
                                                        </td>
                                                        <td id="p_name"><%=isLanguageEnglish ? employee_recordsDTO.nameEng : employee_recordsDTO.nameBng%>
                                                        </td>
                                                        <td id="p_mobile"><%=Utils.getDigits(employee_recordsDTO.personalMobile, Language)%>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3" id="notified_table_div">
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between">
                                                <h4 class="caption-subject font-blue-madison table-title"><%=(LM.getText(LC.NEW_USER_TASK_ADD_NOTIFIEDRECORDSID, loginDTO))%>
                                                </h4>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped  text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 12%">
                                                            <b><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 30%">
                                                            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 53%">
                                                            <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                                            </b></th>
                                                        <th><%=(LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO))%>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody id="notified_table">
                                                    <tr>
                                                        <td><%=notifiedUserName%>
                                                        </td>
                                                        <td><%=isLanguageEnglish ? notified_recordsDTO.nameEng : notified_recordsDTO.nameBng%>
                                                        </td>
                                                        <td><%=officeDesignationForNotified%>
                                                        </td>
                                                        <td><%=Utils.getDigits(notified_recordsDTO.personalMobile, Language)%>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <tbody>
                                                    <tr>
                                                        <td><b><%=LM.getText(LC.USER_TASK_COMMENT, loginDTO)%>
                                                        </b></td>
                                                        <td><%=new_user_taskDTO.comment%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30px">
                                                            <b><%=LM.getText(LC.NEW_USER_TASK_ADD_STATUS, loginDTO)%>
                                                            </b></td>
                                                        <td>
                                                            <span class="shadow"
                                                                  style="alignment:left;background-color:<%=TaskStatusEnum.getColor(new_user_taskDTO.statusCat)%>; color: white; padding:5px 10px;border-radius:5px">
                                                                <%=CatRepository.getInstance().getText(Language, "new_user_task_status", new_user_taskDTO.statusCat)%>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row mt-3" id="submit_div" style="display: none">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                                <%=LM.getText(LC.NEW_USER_TASK_ADD_NEW_USER_TASK_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    onclick="submitItem('bigform')" type="button" id="submit_button">
                                <%=LM.getText(LC.NEW_USER_TASK_MARK_AS_DONE, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        <%
            if(new_user_taskDTO.statusCat==TaskStatusEnum.NOT_DONE.getValue()){

        %>
        $("#submit_div").show();
        <%
            }
        %>
        $("#cancel-btn").click(e => {
            location.href = "New_user_taskServlet?actionType=search";
        })
    });

    function submitItem(id) {
        const lang = '<%=Language%>'.toLowerCase();
        let msg, title;
        if (lang === 'english') {
            msg = 'Do you want to submit?';
            title = "You won't be able to revert this !";
        } else {
            msg = 'আপনি কি সাবমিট করতে চান?';
            title = 'পরবর্তিতে আর পরিবর্তন করতে পারবেন না !';
        }
        messageDialog(msg, title, 'success', true, '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_YES_SUBMIT, loginDTO))%>',
            '<%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_CANCEL, loginDTO))%>', () => {
                $('#' + id).submit();
            },
            () => {
            });
    }
</script>