<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="new_user_task.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>


<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>

<%
    String context = "../../.." + request.getContextPath() + "/";
    New_user_taskDTO new_user_taskDTO;
    new_user_taskDTO = (New_user_taskDTO) request.getAttribute("new_user_taskDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (new_user_taskDTO == null) {
        new_user_taskDTO = new New_user_taskDTO();

    }
    System.out.println("new_user_taskDTO = " + new_user_taskDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.NEW_USER_TASK_ADD_NEW_USER_TASK_ADD_FORMNAME, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;


    int childTableStartingID = 1;

    String Language = LM.getText(LC.NEW_USER_TASK_EDIT_LANGUAGE, loginDTO);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    CommonDAO.language = Language;
    CatDAO.language = Language;
    String Language2 = LM.getText(LC.OFFICE_ORIGIN_UNIT_ORGANOGRAMS_EDIT_LANGUAGE, loginDTO);
    String today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    String URL = "New_user_taskServlet?actionType=" + "ajax_" + actionName + "&isPermanentTable=true";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form" id="new_user_task_form" name="bigform"
              enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-2 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=new_user_taskDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_USERNAME, loginDTO))%>
                                        </label>
                                        <div class="col-md-7">
                                            <input type='text' class='form-control' name='userName'
                                                   id='userName'
                                                   value='' tag='pb_html'/>
                                        </div>
                                        <div class="col-md-2 text-right text-md-left">
                                            <input type="button"
                                                   value="<%=(LM.getText(LC.GLOBAL_SEARCH, loginDTO))%>"
                                                   class="btn btn-primary mt-3 mt-md-0"
                                                   onclick="getEmployeeDetails()">
                                        </div>
                                    </div>
                                    <div class="row" id="search_employee_info" style="display: none">
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between">
                                                <h4 class="caption-subject font-blue-madison">
                                                    <%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_PERSONAL_INFORMATION, loginDTO))%>
                                                </h4>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th><%=(LM.getText(LC.EMPLOYEE_RECORDS_EDIT_USERNAME, loginDTO))%>
                                                        </th>
                                                        <th><%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAME, loginDTO))%>
                                                        </th>
                                                        <th><%=(LM.getText(LC.GLOBAL_MOBILE_NUMBER, loginDTO))%>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="info_table_body">
                                                    <tr>
                                                        <td id="p_user_id"></td>
                                                        <td id="p_name"></td>
                                                        <td id="p_mobile"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <h4 class="caption-subject font-blue-madison"><%=(LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_CURRENT_DESIGNATION, loginDTO))%>
                                            </h4>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr class="uppercase">
                                                        <th><%=(LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO))%>
                                                        </th>
                                                        <th><%=(LM.getText(LC.EMPLOYEE_OFFICE_REPORT_SELECT_INCHARGELABEL, loginDTO))%>
                                                        </th>
                                                        <th><%=(LM.getText(LC.EMPLOYEE_RECORDS_ADD_JOININGDATE, loginDTO))%>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="search_employee_organogram">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="notified_in_div" style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.USER_TASK_EMPLOYEES, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-9" id="notified_div">
                                            <div>
                                                <input type='hidden' id='notified'
                                                       class='form-control'
                                                       name='notified'
                                                       tag='pb_html' value=''>

                                                <button id="notified_button" type="button"
                                                        class="btn btn-primary btn-block shadow btn-border-radius mb-3">
                                                    <%=LM.getText(LC.USER_TASK_ADD_EMPLOYEES)%>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3" id="notified_table_div" style="display: none">
                                        <div class="col-12">
                                            <div class="d-flex justify-content-between">
                                                <h4 class="caption-subject font-blue-madison"><%=(LM.getText(LC.NEW_USER_TASK_ADD_NOTIFIEDRECORDSID, loginDTO))%>
                                                </h4>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 12%">
                                                            <b><%=LM.getText(LC.USER_ADD_USER_NAME, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 30%">
                                                            <b><%=LM.getText(LC.EMPLOYEE_SERVICE_HISTORY_SEARCH_NAMEOFEMPLOYEE, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 53%">
                                                            <b><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_OFFICE_POST, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 53%">
                                                            <b><%=LM.getText(LC.USER_TASK_COMMENT, loginDTO)%>
                                                            </b></th>
                                                        <th style="width: 5%"></th>
                                                    </tr>
                                                    </thead>

                                                    <tbody id="notified_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-danger btn-block"
                                                                    onclick="remove_containing_row(this,'notified_table');">
                                                                x
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <input type='hidden' class='form-control'
                                               name='addedComments'
                                               id='addedComments' value='' tag='pb_html'/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row mt-4" id="submit_div" style="display: none">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.NEW_USER_TASK_ADD_NEW_USER_TASK_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                    type="button" onclick="submitItem('new_user_task_form')">
                                <%=LM.getText(LC.NEW_USER_TASK_ADD_NEW_USER_TASK_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script src="nicEdit.js" type="text/javascript"></script>
<script src="<%=context%>/assets/scripts/util1.js"></script>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var language = '<%=Language2%>'.toLowerCase();
    let name = '';
    const today = '<%=today%>'
    const commentMap = new Map();
    var commentClass = function () {
    };
    commentClass.prototype = {
        recordId: 0,
        comment: "",
    };

    function PreprocessBeforeSubmiting() {
        var empModels = Array.from(notified_map.values());
        for (let employee of empModels) {
            console.log(employee.employeeRecordId);
            console.log(document.getElementById(employee.employeeRecordId + "_comment").value);
            var obj = new commentClass();
            obj.recordId = employee.employeeRecordId;
            obj.comment = document.getElementById(employee.employeeRecordId + "_comment").value;
            commentMap.set(Number(employee.employeeRecordId), obj);


        }
        document.getElementById("addedComments").value = JSON.stringify(
            Array.from(commentMap.values())
        );
        document.getElementById("notified").value = JSON.stringify(
            Array.from(notified_map.values())
        );
        return true;
    }

    function isFormValid($form) {
        const jQueryValid = $form.valid();
        return PreprocessBeforeSubmiting() && jQueryValid;
    }


    let isEnglish = <%=isLanguageEnglish%>;

    function submitItem(id) {
        let submitFunction = () => {
            let formName = '#' + id;
            const form = $(formName);

            if (isFormValid(form)) {

                submitAjaxByData(form.serialize(), "<%=URL%>", 'POST')


            }
        }
        if (isEnglish) {
            messageDialog('Do you want to submit?', "You won't be able to revert this!", 'success', true, 'Submit', 'Cancel', submitFunction);
        } else {
            messageDialog('সাবমিট করতে চান?', "সাবমিটের পর পরিবর্তনযোগ্য না!", 'success', true, 'সাবমিট', 'বাতিল', submitFunction);
        }
    }

    function buttonStateChangeFunction(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "New_user_taskServlet");
    }

    function init(row) {


    }

    function getEmployeeDetails() {
        hideAndClear();
        var userName = document.getElementById('userName').value;
        var phone = '';

        if (!userName && !phone) {
            showToast('নাম অথবা ফোন যে কোন একটি তথ্য দিন', 'Please provide name or phone');
            return;
        }
        $('#search_employee_info').show();
        document.getElementById('search_employee_organogram').innerHTML = '';
        $('#info_table_body').hide();
        user_info = {}
        name = '';
        user_organogram_info = [];
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '' && this.responseText.length > 0 && this.responseText != '[]') {
                    try {
                        var list = JSON.parse(this.responseText);
                        let userName = '';
                        let mobileNumber = '';
                        $('#notified_in_div').show();
                        $('#notified_table_div').show();
                        $('#submit_div').show();
                        for (var i = 0; i < list.length; i++) {
                            var data = list[i];
                            user_info.id = data.id;
                            user_info.employee_office_id = data.employee_office_id;
                            if (language === 'english') {
                                if (data.username) {
                                    userName = data.username;
                                }
                                if (data.nameEng) {
                                    name = data.nameEng;
                                }
                                if (data.personalMobile) {
                                    mobileNumber = data.personalMobile;
                                }
                            } else {
                                if (data.username) {
                                    userName = convertNumberToBangla(data.username);
                                }
                                if (data.nameBng) {
                                    name = data.nameBng;
                                }
                                if (data.personalMobile) {
                                    mobileNumber = convertNumberToBangla(data.personalMobile);
                                }
                            }
                            user_info.nameEng = data.nameEng;
                            user_info.nameBng = data.nameBng;
                            user_info.personalEmail = data.personalEmail;
                            user_info.personalMobile = data.personalMobile;
                            user_info.designation_id = data.designation_id;
                            user_info.designation = data.designation;
                            user_info.unitNameEng = data.unitNameEng;
                            user_info.unitNameBng = data.unitNameBng;
                            user_info.officeNameEng = data.officeNameEng;
                            user_info.officeNameBng = data.officeNameBng;
                            user_info.joiningDate = data.joiningDate;
                            user_info.lastOfficeDate = data.lastOfficeDate;
                            user_organogram_info.push(data);
                            let office = '';
                            let post = '';
                            let inChargeLevel = '';
                            let start_date = '';
                            if (user_info.lastOfficeDate == <%=SessionConstants.MIN_DATE%>) {
                                if (language === 'english') {
                                    if (data.unitNameEng) {
                                        office = data.unitNameEng;
                                    }
                                    if (data.designationEng) {
                                        post = data.designationEng;
                                    }
                                    if (data.joiningDateEng) {
                                        start_date = data.joiningDateEng;
                                    }
                                    if (data.inChargeLabelEng) {
                                        inChargeLevel = data.inChargeLabelEng;
                                    }
                                } else {
                                    if (data.unitNameBng) {
                                        office = data.unitNameBng;
                                    }
                                    if (data.designationBng) {
                                        post = data.designationBng;
                                    }
                                    if (data.joiningDateBng) {
                                        start_date = data.joiningDateBng;
                                    }
                                    if (data.inChargeLabelBng) {
                                        inChargeLevel = data.inChargeLabelBng;
                                    }
                                }
                                let newRow = "<tr>" +
                                    '<td id="p_user_designation_' + i + '">' + post + '<br>' + office + '</td>' +
                                    '<td id="p_incharge_level_' + i + '">' + inChargeLevel + '</td>' +
                                    '<td id="p_joining_date_' + i + '">' + start_date + '</td>' +
                                    '</tr>';
                                $('#search_employee_organogram').append(newRow);
                            }
                        }
                        if (name || userName || mobileNumber) {
                            $('#p_name').html(name);
                            $('#p_user_id').html(userName);
                            $('#p_mobile').html(mobileNumber);
                            $('#info_table_body').show();
                        }
                    } catch (e) {
                        console.log(e);
                        hideAndClear()
                        showToast('কোন তথ্য পাওয়া যায় নি', "Don't find any data");
                    }
                } else {
                    hideAndClear()
                    showToast('কোন তথ্য পাওয়া যায় নি', "Don't find any data");
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("Get", "EmployeeAssignServlet?actionType=getEmployeeDetailsByUserName" +
            '&userName=' + userName +
            '&phone=' + phone, true);
        xhttp.send();
    }

    function hideAndClear() {
        var empModels = Array.from(notified_map.values());
        for (let employee of empModels) {
            console.log(employee.employeeRecordId);
            let row = document.getElementById(employee.employeeRecordId + "_td_button").parentNode;
            row.remove();
        }
        $('#notified_in_div').hide();
        $('#notified_table_div').hide();
        $('#submit_div').hide();
        commentMap.clear();
        notified_map.clear();
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    <%
        List<EmployeeSearchIds> addedEmpIdsList = null;
    %>
    notified_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);
    table_name_to_collcetion_map = new Map(
        [
            ['notified_table', {
                info_map: notified_map,
                isSingleEntry: false,
                callBackFunction: function (empInfo) {
                    console.log(empInfo.employeeRecordId);
                    console.log(empInfo.employeeRecordId + "_td_button");
                    let n_col = document.getElementById(empInfo.employeeRecordId + "_td_button").parentNode.cells.length;
                    document.getElementById(empInfo.employeeRecordId + "_td_button").parentNode.cells[n_col - 1].id = empInfo.employeeRecordId + "_td_button";
                    document.getElementById(empInfo.employeeRecordId + "_td_button").parentNode.cells[n_col - 2].id = empInfo.employeeRecordId + "_td_comment";
                    document.getElementById(empInfo.employeeRecordId + "_td_button").parentNode.cells[n_col - 2].innerHTML =
                        '<textarea rows="3" style="resize: none;" id=' + empInfo.employeeRecordId + '_comment value=""\n' +
                        ' tag=\'pb_html\'/>';
                    console.log('callBackFunction called with latest added emp info JSON');
                    console.log(empInfo);
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#notified_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'notified_table';
        $('#search_emp_modal').modal();
    });


</script>






