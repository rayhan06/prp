
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="user_details.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="geolocation.GeoLocationDAO2"%>

<%
User_detailsDTO user_detailsDTO;
user_detailsDTO = (User_detailsDTO)request.getAttribute("user_detailsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(user_detailsDTO == null)
{
	user_detailsDTO = new User_detailsDTO();
	
}
System.out.println("user_detailsDTO = " + user_detailsDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.USER_DETAILS_EDIT_USER_DETAILS_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.USER_DETAILS_ADD_USER_DETAILS_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
User_detailsDTO row = user_detailsDTO;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="User_detailsServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				



<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.USER_DETAILS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_PRESENTADDRESS, loginDTO)):(LM.getText(LC.USER_DETAILS_ADD_PRESENTADDRESS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'presentAddress_div_<%=i%>'>	
		<div id ='presentAddress_geoDIV_<%=i%>'>
			<select class='form-control' name='presentAddress_active' id = 'presentAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'presentAddress_geoDIV_<%=i%>', 'presentAddress_geolocation_<%=i%>')" required="required"  pattern="^((?!select division).)*$" title="presentAddress must be selected"
></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='presentAddress_text' id = 'presentAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(user_detailsDTO.presentAddress)  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc'>
		<input type='hidden' class='form-control'  name='presentAddress' id = 'presentAddress_geolocation_<%=i%>' value=<%=("'" + "1" + "'")%>>
						
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_PERMANENTADDRESS, loginDTO)):(LM.getText(LC.USER_DETAILS_ADD_PERMANENTADDRESS, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'permanentAddress_div_<%=i%>'>	
		<div id ='permanentAddress_geoDIV_<%=i%>'>
			<select class='form-control' name='permanentAddress_active' id = 'permanentAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'permanentAddress_geoDIV_<%=i%>', 'permanentAddress_geolocation_<%=i%>')" required="required"  pattern="^((?!select division).)*$" title="permanentAddress must be selected"
></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='permanentAddress_text' id = 'permanentAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(user_detailsDTO.permanentAddress)  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc'>
		<input type='hidden' class='form-control'  name='permanentAddress' id = 'permanentAddress_geolocation_<%=i%>' value=<%=("'" + "1" + "'")%>>
						
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + user_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_detailsDTO.lastModificationTime + "'"):("'" + "0" + "'")%>/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.USER_DETAILS_EDIT_USER_DETAILS_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.USER_DETAILS_ADD_USER_DETAILS_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.USER_DETAILS_EDIT_USER_DETAILS_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.USER_DETAILS_ADD_USER_DETAILS_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	document.getElementById('presentAddress_geolocation_' + row).value = document.getElementById('presentAddress_geolocation_' + row).value + ":" + document.getElementById('presentAddress_geoTextField_' + row).value;
	console.log("geo value = " + document.getElementById('presentAddress_geolocation_' + row).value);
	document.getElementById('permanentAddress_geolocation_' + row).value = document.getElementById('permanentAddress_geolocation_' + row).value + ":" + document.getElementById('permanentAddress_geoTextField_' + row).value;
	console.log("geo value = " + document.getElementById('permanentAddress_geolocation_' + row).value);
	return true;
}

function PostprocessAfterSubmiting(row)
{
	document.getElementById('presentAddress_geolocation_' + row).value = "1";
	document.getElementById('permanentAddress_geolocation_' + row).value = "1";
}

function addrselected(value, htmlID, selectedIndex, tagname, geodiv, hiddenfield)
{
	console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
	try 
	{
		var elements, ids;
		elements = document.getElementById(geodiv).children;
		
		document.getElementById(hiddenfield).value = value;
		
		ids = '';
		for(var i = elements.length - 1; i >= 0; i--) 
		{
			var elemID = elements[i].id;
			if(elemID.includes(htmlID) && elemID > htmlID)
			{
				ids += elements[i].id + ' ';
				
				for(var j = elements[i].options.length - 1; j >= 0; j--)
				{
				
					elements[i].options[j].remove();
				}
				elements[i].remove();
				
			}
		}
				

		var newid = htmlID + '_1';

		document.getElementById(geodiv).innerHTML += "<select class='form-control' name='" + tagname + "' id = '" + newid 
		+ "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '" + geodiv +"', '" + hiddenfield +"')\"></select>";
		//console.log('innerHTML= ' + document.getElementById(geodiv).innerHTML);
		document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
		document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;
		//console.log('innerHTML again = ' + document.getElementById(geodiv).innerHTML);
		
		
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() 
		{
			if (this.readyState == 4 && this.status == 200) 
			{
				if(!this.responseText.includes('option'))
				{
					document.getElementById(newid).remove();
				}
				else
				{
					document.getElementById(newid).innerHTML = this.responseText ;
				}
				
			}
			else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		};
		 
		xhttp.open("POST", "User_detailsServlet?actionType=getGeo&myID="+value, true);
		xhttp.send();
	}
	catch(err) 
	{
		alert("got error: " + err);
	}	  

}

function init(row)
{
		
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
	    	document.getElementById('presentAddress_geoSelectField_' + row).innerHTML = this.responseText ;
	    	document.getElementById('permanentAddress_geoSelectField_' + row).innerHTML = this.responseText ;
		}
	    else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	 };
	xhttp.open("POST", "User_detailsServlet?actionType=getGeo&myID=1", true);
	xhttp.send();
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}





</script>






