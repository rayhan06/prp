
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="user_details.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@page import="theme.ThemeRepository"%>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->    
<head>
<meta charset="utf-8" />
<title><%=LM.getText(LC.GLOBAL_TITLE) %></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />   

<%
String context_folder = "../../.."  + request.getContextPath() ;

%>
<link href="<%=context_folder%>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />  
<link href="<%=context_folder%>/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
<link href="<%=context_folder%>/assets/css/material-design-iconic-font.min.css" rel="stylesheet" type="text/css" />  
<link href="<%=context_folder%>/assets/css/google_font.css" rel="stylesheet" type="text/css" />
<link href="<%=context_folder%>/assets/css/login.css" rel="stylesheet" type="text/css" />  

<%
//String context_folder = "../../.."  + request.getContextPath() + "/";
String themeDescription = ThemeRepository.getInstance().getCurrentAppliedThemeDescriprion();
if(themeDescription!=null){
%>

<%} %>        
        

<style type="text/css">
.login{
background-color: #fff !important;
}
.login .copyright {
color: black !important;
}
.login .content h3{
color: black !important;
}
@media only screen and (min-width: 760px) {
.quick-links span{
font-size: 18px !important;
}
.quick-links a{
font-size: 20px !important;
margin-left: 10px;
text-decoration: underline;
}
}
</style>
</head>    		

<%
User_detailsDTO user_detailsDTO;
user_detailsDTO = (User_detailsDTO)request.getAttribute("user_detailsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(user_detailsDTO == null)
{
	user_detailsDTO = new User_detailsDTO();
	
}
System.out.println("user_detailsDTO = " + user_detailsDTO);

String actionName = request.getParameter("actionType");

if(actionName == null)
{
	actionName = "getAddPage";
}
//System.out.println("actionType = " + request.getParameter("actionType"));
if (actionName.equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;

if(loginDTO == null)
{
	loginDTO = new LoginDTO();
}

loginDTO.userID = 0;

loginDTO.isOisf = 0;


if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.USER_DETAILS_EDIT_USER_DETAILS_EDIT_FORMNAME);
}
else
{
	formTitle = "";//LM.getText(LC.USER_DETAILS_ADD_USER_DETAILS_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
User_detailsDTO row = user_detailsDTO;
%>

<body >
 <div class="limiter">
		<div class="container-login100" style="background-image: url('../assets/images/bmd_2.jpg');">

<div class="wrap-login200" style="padding: 35px 55px 30px 55px;width: 600px !important;">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	
		<form class="form-horizontal" action="../User_detailsServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
			
			<span class="login100-form-logo">
						<img  class="img-responsive login-page-logo" src="<%=request.getContextPath()%>/assets/images/company-name.png" alt="" /> </a>
					</span>
					
					<span class="login100-form-title p-b-15 p-t-15">
						Bureau of Mineral Development 
					</span>	
				
				
				



<%
String Language = "";//LM.getText(LC.USER_DETAILS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
		
		
<div class="form-group ">	

<label class="col-lg-4 control-label2" style="color:#fff">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_NAME)):(LM.getText(LC.USER_DETAILS_ADD_NAME))%>
	<span class="required"> * </span>
</label>				
	<div class="col-lg-7 " id = 'employerName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='citizen_name' id = 'employerRep_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_detailsDTO.name + "'"):("''")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"
<%
	}
%>
  tag='pb_html'
   />					
	</div>
</div>				


<div class="form-group ">	

<label class="col-lg-4 control-label2" style="color:#fff">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_PHONE)):(LM.getText(LC.USER_DETAILS_ADD_PHONE))%>
	<span class="required"> * </span>
</label>
				
	<div class="col-lg-7 " id = 'employerName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='citizen_phone' id = 'employerRep_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_detailsDTO.phone + "'"):("''")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"
<%
	}
%>
  tag='pb_html'
   />					
	</div>
</div>


<div class="form-group ">	

<label class="col-lg-4 control-label2" style="color:#fff">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_EMAIL)):(LM.getText(LC.USER_DETAILS_ADD_EMAIL))%>
	<span class="required"> * </span>
</label>				
	<div class="col-lg-7 " id = 'employerName_div_<%=i%>'>	
		<input type='email' class='form-control'  name='citizen_email' id = 'employerRep_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_detailsDTO.email + "'"):("''")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"
<%
	}
%>
  tag='pb_html'
   />					
	</div>
</div>


<div class="form-group ">	

<label class="col-lg-4 control-label2" style="color:#fff">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_USERNAME)):(LM.getText(LC.USER_DETAILS_ADD_USERNAME))%>
	<span class="required"> * </span>
</label>				
	<div class="col-lg-7 " id = 'employerName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='citizen_username' id = 'employerRep_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_detailsDTO.username + "'"):("''")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"
<%
	}
%>
  tag='pb_html'
   />					
	</div>
</div>


<div class="form-group ">	

<label class="col-lg-4 control-label2" style="color:#fff">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_PASSWORD)):(LM.getText(LC.USER_DETAILS_ADD_PASSWORD))%>
	<span class="required"> * </span>
</label>				
	<div class="col-lg-7 " id = 'employerName_div_<%=i%>'>	
		<input type='password' class='form-control'  name='citizen_password' id = 'employerRep_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_detailsDTO.password + "'"):("''")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"
<%
	}
%>
  tag='pb_html'
   />					
	</div>
</div>	



		
												
	

<div class="form-group ">	

<label class="col-lg-4 control-label2" style="color:#fff">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_PRESENTADDRESS)):(LM.getText(LC.USER_DETAILS_ADD_PRESENTADDRESS))%>
</label>				
	<div class="col-lg-7 " id = 'presentAddress_div_<%=i%>'>	
		<div id ='presentAddress_geoDIV_<%=i%>'>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='presentAddress' id = 'presentAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  user_detailsDTO.presentAddress  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc'>
	</div>
</div>			
				
	

<div class="form-group ">	

<label class="col-lg-4 control-label2" style="color:#fff">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_DETAILS_EDIT_PERMANENTADDRESS)):(LM.getText(LC.USER_DETAILS_ADD_PERMANENTADDRESS))%>
</label>				
	<div class="col-lg-7 " id = 'permanentAddress_div_<%=i%>'>	
		<div id ='permanentAddress_geoDIV_<%=i%>'>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='permanentAddress' id = 'permanentAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  user_detailsDTO.permanentAddress  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc'>
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + user_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_detailsDTO.lastModificationTime + "'"):("'" + "0" + "'")%>/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.USER_DETAILS_EDIT_USER_DETAILS_CANCEL_BUTTON));
					}
					else
					{
						out.print(LM.getText(LC.USER_DETAILS_ADD_USER_DETAILS_CANCEL_BUTTON));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.USER_DETAILS_EDIT_USER_DETAILS_SUBMIT_BUTTON));
					}
					else
					{
						out.print(LM.getText(LC.USER_DETAILS_ADD_USER_DETAILS_SUBMIT_BUTTON));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	document.getElementById('presentAddress_geolocation_' + row).value = document.getElementById('presentAddress_geolocation_' + row).value + ":" + document.getElementById('presentAddress_geoTextField_' + row).value;
	console.log("geo value = " + document.getElementById('presentAddress_geolocation_' + row).value);
	document.getElementById('permanentAddress_geolocation_' + row).value = document.getElementById('permanentAddress_geolocation_' + row).value + ":" + document.getElementById('permanentAddress_geoTextField_' + row).value;
	console.log("geo value = " + document.getElementById('permanentAddress_geolocation_' + row).value);
	return true;
}

function PostprocessAfterSubmiting(row)
{
	document.getElementById('presentAddress_geolocation_' + row).value = "1";
	document.getElementById('permanentAddress_geolocation_' + row).value = "1";
}

function addrselected(value, htmlID, selectedIndex, tagname, geodiv, hiddenfield)
{
	console.log('geodiv = ' + geodiv + ' hiddenfield = ' + hiddenfield);
	try 
	{
		var elements, ids;
		elements = document.getElementById(geodiv).children;
		
		document.getElementById(hiddenfield).value = value;
		
		ids = '';
		for(var i = elements.length - 1; i >= 0; i--) 
		{
			var elemID = elements[i].id;
			if(elemID.includes(htmlID) && elemID > htmlID)
			{
				ids += elements[i].id + ' ';
				
				for(var j = elements[i].options.length - 1; j >= 0; j--)
				{
				
					elements[i].options[j].remove();
				}
				elements[i].remove();
				
			}
		}
				

		var newid = htmlID + '_1';

		document.getElementById(geodiv).innerHTML += "<select class='form-control' name='" + tagname + "' id = '" + newid 
		+ "' onChange=\"addrselected(this.value, this.id, this.selectedIndex, this.name, '" + geodiv +"', '" + hiddenfield +"')\"></select>";
		//console.log('innerHTML= ' + document.getElementById(geodiv).innerHTML);
		document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
		document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;
		//console.log('innerHTML again = ' + document.getElementById(geodiv).innerHTML);
		
		
		
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() 
		{
			if (this.readyState == 4 && this.status == 200) 
			{
				if(!this.responseText.includes('option'))
				{
					document.getElementById(newid).remove();
				}
				else
				{
					document.getElementById(newid).innerHTML = this.responseText ;
				}
				
			}
			else if(this.readyState == 4 && this.status != 200)
			{
				alert('failed ' + this.status);
			}
		};
		 
		xhttp.open("POST", "User_detailsServlet?actionType=getGeo&myID="+value, true);
		xhttp.send();
	}
	catch(err) 
	{
		alert("got error: " + err);
	}	  

}

function init(row)
{
		
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
	    	document.getElementById('presentAddress_geoSelectField_' + row).innerHTML = this.responseText ;
	    	document.getElementById('permanentAddress_geoSelectField_' + row).innerHTML = this.responseText ;
		}
	    else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	 };
	xhttp.open("POST", "User_detailsServlet?actionType=getGeo&myID=1", true);
	xhttp.send();
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}





</script>



</div>
</div>
</body>
</html>


