<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="user_details.User_detailsDTO"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="geolocation.GeoLocationDAO2"%>

<%
User_detailsDTO user_detailsDTO = (User_detailsDTO)request.getAttribute("user_detailsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(user_detailsDTO == null)
{
	user_detailsDTO = new User_detailsDTO();
	
}
System.out.println("user_detailsDTO = " + user_detailsDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";
User_detailsDTO row = user_detailsDTO;
%>




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.USER_DETAILS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_presentAddress'>")%>
			
	
	<div class="form-inline" id = 'presentAddress_div_<%=i%>'>
		<div id ='presentAddress_geoDIV_<%=i%>'>
			<select class='form-control' name='presentAddress_active' id = 'presentAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'presentAddress_geoDIV_<%=i%>', 'presentAddress_geolocation_<%=i%>')" required="required"  pattern="^((?!select division).)*$" title="presentAddress must be selected"
></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='presentAddress_text' id = 'presentAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(user_detailsDTO.presentAddress)  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc'>
		<input type='hidden' class='form-control'  name='presentAddress' id = 'presentAddress_geolocation_<%=i%>' value=<%=("'" + "1" + "'")%>>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_permanentAddress'>")%>
			
	
	<div class="form-inline" id = 'permanentAddress_div_<%=i%>'>
		<div id ='permanentAddress_geoDIV_<%=i%>'>
			<select class='form-control' name='permanentAddress_active' id = 'permanentAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'permanentAddress_geoDIV_<%=i%>', 'permanentAddress_geolocation_<%=i%>')" required="required"  pattern="^((?!select division).)*$" title="permanentAddress must be selected"
></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)" name='permanentAddress_text' id = 'permanentAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(user_detailsDTO.permanentAddress)  + "'"):("'" + "" + "'")%> placeholder='Road Number, House Number etc'>
		<input type='hidden' class='form-control'  name='permanentAddress' id = 'permanentAddress_geolocation_<%=i%>' value=<%=("'" + "1" + "'")%>>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + user_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=user_detailsDTO.lastModificationTime%>'/>
		
												
<%=("</td>")%>
					
		