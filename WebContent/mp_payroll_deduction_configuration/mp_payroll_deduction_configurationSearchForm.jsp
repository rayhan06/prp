<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="mp_payroll_deduction_configuration.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeDTO" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>


<%
    String navigator2 = "navMP_PAYROLL_DEDUCTION_CONFIGURATION";
    String servletName = "Mp_payroll_deduction_configurationServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
            </th>
            <th>
                <%=getDataByLanguage(Language, "অর্থনৈতিক কোড বিবরণ", "Economic Code Description")%>
            </th>
            <th>
                <%=getDataByLanguage(Language, "পরিমাণ(টাকা)", "Amount(Taka)")%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Mp_payroll_deduction_configurationDTO> data = (ArrayList<Mp_payroll_deduction_configurationDTO>) rn2.list;
            Collections.sort(data);
            try {

                if (data != null) {
                    for (Mp_payroll_deduction_configurationDTO mp_payroll_deduction_configurationDTO : data) {
        %>
        <tr>
            <%
                Economic_sub_codeDTO codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(mp_payroll_deduction_configurationDTO.economicSubCodeId);
            %>
            <td>
                <%=Utils.getDigits(codeDTO.code, Language)%>
            </td>
            <td>
                <%=isLanguageEnglish ? codeDTO.descriptionEn : codeDTO.descriptionBn%>
            </td>
            <td>
                <%=Utils.getDigits(mp_payroll_deduction_configurationDTO.amount, Language)%>
            </td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			