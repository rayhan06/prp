
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_purchase.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navPI_PURCHASE";
String servletName = "Pi_purchaseServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_OFFICEUNITID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_FISCALYEARID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_PACKAGEID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_VENDORID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_SAROKNUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_PURCHASEDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_TOTALBILL, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMTYPEID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_AMOUNT, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_PURCHASE_ADD_TOTALVALUE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.PI_PURCHASE_SEARCH_PI_PURCHASE_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Pi_purchaseDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Pi_purchaseDTO pi_purchaseDTO = (Pi_purchaseDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = pi_purchaseDTO.officeUnitId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.fiscalYearId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.packageId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.vendorId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.sarokNumber + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.purchaseDate + "";
											%>
											<%
											String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_purchaseDate, Language)%>
				
			
											</td>
		
		
		
											<td>
											<%
											value = pi_purchaseDTO.totalBill + "";
											%>
											<%
											value = String.format("%.1f", pi_purchaseDTO.totalBill);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.itemTypeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.itemId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.amount + "";
											%>
											<%
											value = String.format("%.1f", pi_purchaseDTO.amount);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_purchaseDTO.totalValue + "";
											%>
											<%
											value = String.format("%.1f", pi_purchaseDTO.totalValue);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	
											<%CommonDTO commonDTO = pi_purchaseDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=pi_purchaseDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			