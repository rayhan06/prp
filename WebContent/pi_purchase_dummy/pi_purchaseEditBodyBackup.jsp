<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="pi_purchase.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>

<%
    Pi_purchaseDTO pi_purchaseDTO = new Pi_purchaseDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_purchaseDTO = Pi_purchaseDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_purchaseDTO;
    String tableName = "pi_purchase";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_purchaseServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_purchaseServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="mt-5 col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=getDataByLanguage(Language, "ক্রয় তথ্য", "Purchase Information")%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=pi_purchaseDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='officeUnitId'
                                           id='officeUnitId_hidden_<%=i%>' value='<%=pi_purchaseDTO.officeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='totalBill'
                                           id='totalBill_hidden_<%=i%>' value='<%=pi_purchaseDTO.totalBill%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_PURCHASEDATE, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <%value = "purchaseDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='purchaseDate' id='purchaseDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(pi_purchaseDTO.purchaseDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_FISCALYEARID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select class='form-control' name='fiscalYearId'
                                                        id='fiscalYearId_select_<%=i%>' tag='pb_html'>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_PACKAGEID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select class='form-control' name='packageId'
                                                        id='packageId_select_<%=i%>'
                                                        tag='pb_html'>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_VENDORID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select class='form-control' name='vendorId' id='vendorId_select_<%=i%>'
                                                        tag='pb_html'>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_SAROKNUMBER, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='sarokNumber'
                                                       id='sarokNumber_text_<%=i%>'
                                                       value='<%=pi_purchaseDTO.sarokNumber%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_REMARKS, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='remarks'
                                                       id='remarks_text_<%=i%>' value='<%=pi_purchaseDTO.remarks%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--                <div class="form-group row">--%>
                <%--                    <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMTYPEID, loginDTO)%>--%>
                <%--                    </label>--%>
                <%--                    <div class="col-8">--%>
                <%--                        <select class='form-control' name='itemTypeId' id='itemTypeId_select_<%=i%>'--%>
                <%--                                tag='pb_html'>--%>
                <%--                        </select>--%>

                <%--                    </div>--%>
                <%--                </div>--%>
                <%--                <div class="form-group row">--%>
                <%--                    <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMID, loginDTO)%>--%>
                <%--                    </label>--%>
                <%--                    <div class="col-8">--%>
                <%--                        <select class='form-control' name='itemId' id='itemId_select_<%=i%>'--%>
                <%--                                tag='pb_html'>--%>
                <%--                        </select>--%>

                <%--                    </div>--%>
                <%--                </div>--%>
                <%--                <div class="form-group row">--%>
                <%--                    <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_AMOUNT, loginDTO)%>--%>
                <%--                    </label>--%>
                <%--                    <div class="col-8">--%>
                <%--                        <input type='text' class='form-control' name='amount'--%>
                <%--                               id='amount_text_<%=i%>' value='<%=pi_purchaseDTO.amount%>'--%>
                <%--                               tag='pb_html'/>--%>
                <%--                    </div>--%>
                <%--                </div>--%>
                <%--                <div class="form-group row">--%>
                <%--                    <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_TOTALVALUE, loginDTO)%>--%>
                <%--                    </label>--%>
                <%--                    <div class="col-8">--%>
                <%--                        <input type='text' class='form-control' name='totalValue'--%>
                <%--                               id='totalValue_text_<%=i%>' value='<%=pi_purchaseDTO.totalValue%>'--%>
                <%--                               tag='pb_html'/>--%>
                <%--                    </div>--%>
                <%--                </div>--%>
                <%--                <div class="form-group row">--%>
                <%--                    <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_EXPIRYDATE, loginDTO)%>--%>
                <%--                    </label>--%>
                <%--                    <div class="col-8">--%>
                <%--                        <%value = "expiryDate_js_" + i;%>--%>
                <%--                        <jsp:include page="/date/date.jsp">--%>
                <%--                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
                <%--                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
                <%--                        </jsp:include>--%>
                <%--                        <input type='hidden' name='expiryDate' id='expiryDate_date_<%=i%>'--%>
                <%--                               value='<%=dateFormat.format(new Date(pi_purchaseDTO.expiryDate)).toString()%>'--%>
                <%--                               tag='pb_html'>--%>
                <%--                    </div>--%>
                <%--                </div>--%>
                <div class="row mt-5">
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_FILESDROPZONE, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    fileColumnName = "filesDropzone";
                                    if (actionName.equals("ajax_edit")) {
                                        List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(pi_purchaseDTO.filesDropzone);
                                %>
                                <%@include file="../pb/dropzoneEditor.jsp" %>
                                <%
                                    } else {
                                        ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                        pi_purchaseDTO.filesDropzone = ColumnID;
                                    }
                                %>

                                <div class="dropzone"
                                     action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=pi_purchaseDTO.filesDropzone%>">
                                    <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                           id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                </div>
                                <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                       id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                <input type='hidden' name='<%=fileColumnName%>'
                                       id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                       value='<%=pi_purchaseDTO.filesDropzone%>'/>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const bigForm = $('#bigform');
    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        preprocessDateBeforeSubmitting('purchaseDate', row);
        preprocessDateBeforeSubmitting('expiryDate', row);

        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_purchaseServlet");
    }

    function init(row) {
        setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());
        select2SingleSelector("#vendorId_select__0", '<%=Language%>');
        select2SingleSelector("#packageId_select__0", '<%=Language%>');
        select2SingleSelector("#fiscalYearId_select__0", '<%=Language%>');
        $.validator.addMethod('vendorSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('packageSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('fiscalYearSelection', function (value, element) {
            return value != 0;
        });
        bigForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vendorId: {
                    required: true,
                    vendorSelection: true
                },
                packageId: {
                    required: true,
                    packageSelection: true
                },
                fiscalYearId: {
                    required: true,
                    fiscalYearSelection:true
                },
            },

            messages: {
                vendorId: isLangEng ? "Please Select Vendor" : "অনুগ্রহ করে সরবরাহকারী নির্বাচন করুন",
                packageId: isLangEng ? "Please Select Package Type" : "অনুগ্রহ করে প্যাকেজ ধরণ প্রবেশ করুন",
                fiscalYearId: isLangEng ? "Please Enter Fiscal year" : "অনুগ্রহ করে অর্থবছর প্রবেশ করুন",
            }
        });
        //setDateByStringAndId('expiryDate_js_' + row, $('#expiryDate_date_' + row).val());
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






