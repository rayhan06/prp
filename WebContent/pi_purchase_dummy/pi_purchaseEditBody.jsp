<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="pi_purchase.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_package.ProcurementGoodsTypeRepository" %>

<%
    List<Pi_purchaseDTO> purchaseDTOS = new ArrayList<>();
    Pi_purchaseDTO mainPurchaseDTO = new Pi_purchaseDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        mainPurchaseDTO = Pi_purchaseDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = mainPurchaseDTO;
    String tableName = "pi_purchase";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%

    if (actionName.equals("ajax_edit")) {
        String sql = "select * from " + Pi_purchaseDAO.getInstance().getTableName() + " where sarok_number = '" + request.getParameter("sarok_number") + "' and isDeleted=0 ";
        purchaseDTOS = Pi_purchaseDAO.getInstance().getDTOs(sql);
        if (!purchaseDTOS.isEmpty())mainPurchaseDTO = purchaseDTOS.get(0);
    }

    String formTitle = LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_purchaseServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_purchaseServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="mt-5 col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=getDataByLanguage(Language, "ক্রয় তথ্য", "Purchase Information")%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=mainPurchaseDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='officeUnitId'
                                           id='officeUnitId_hidden_<%=i%>' value='<%=userDTO.unitID%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_FISCALYEARID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select class='form-control' name='fiscalYearId' onchange="loadData('packageId')"
                                                        id='fiscalYearId_select_<%=i%>' tag='pb_html'>
                                                    <%
                                                        Options = CommonDAO.getOptions(Language, "fiscal_year", mainPurchaseDTO.fiscalYearId);
                                                    %>
                                                    <%=Options%>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_PACKAGEID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select class='form-control' name='packageId' onchange="loadData('vendorId')"
                                                        id='packageId_select_<%=i%>'
                                                        tag='pb_html'>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_VENDORID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select class='form-control' name='vendorId' onchange="loadData('field-Purchase')"
                                                        id='vendorId_select_<%=i%>'
                                                        tag='pb_html'>

                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_PURCHASEDATE, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <%value = "purchaseDate_js_" + i;%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='purchaseDate' id='purchaseDate_date_<%=i%>'
                                                       value='<%=dateFormat.format(new Date(mainPurchaseDTO.purchaseDate))%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_SAROKNUMBER, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='sarokNumber'
                                                       id='sarokNumber_text_<%=i%>'
                                                       value='<%=mainPurchaseDTO.sarokNumber%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row col-6">
                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_REMARKS, loginDTO)%>
                                            </label>
                                            <div class="col-8">
                                                <input type='text' class='form-control' name='remarks'
                                                       id='remarks_text_<%=i%>' value='<%=mainPurchaseDTO.remarks%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_ADD_FORMNAME, loginDTO)%></th>
                                    <th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%></th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMID, loginDTO)%></th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_CURRENT_STOCK, loginDTO)%></th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_AMOUNT, loginDTO)%></th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_APP_ALLOCATED, loginDTO)%></th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_APP_REMAINING, loginDTO)%></th>
                                    <th><%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_UNITPRICE, loginDTO)%></th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_TOTALVALUE, loginDTO)%></th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_EXPIRYDATE, loginDTO)%></th>
                                    <th><%=LM.getText(LC.HM_SELECT, loginDTO)%></th>
                                </tr>
                                </thead>
                                <tbody id="field-Purchase">


                                <%
                                    int index = 0;
                                    if(actionName.equals("ajax_edit")){

                                        for(Pi_purchaseDTO purchaseDTO: purchaseDTOS)
                                        {
                                            index++;

                                            System.out.println("index index = "+index);

                                %>

                                <tr id = "Purchase_<%=index + 1%>">
                                    <td style="display: none;">





                                        <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=purchaseDTO.iD%>' tag='pb_html'/>

                                    </td>
                                    <td>


                                        <%=Language.equals("English")?
                                                Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(purchaseDTO.packageId).nameEn :
                                                Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(purchaseDTO.packageId).nameBn%>

                                    </td>
                                    <td>


                                        <%=Language.equals("English")?
                                                ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(purchaseDTO.itemTypeId).nameEn :
                                                ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(purchaseDTO.itemTypeId).nameBn%>

                                    </td>
                                    <td>

                                        bortoman mojud

                                    </td>
                                    <td>

                                        <input type='number' class='form-control'  name='amount' id = 'amount_number_<%=childTableStartingID%>' value='<%=purchaseDTO.amount%>'  tag='pb_html'>

                                    </td>
                                    <td>

                                        APP boraddo

                                    </td>
                                    <td>

                                        APP oboshishto

                                    </td>
                                    <td>

                                        unit price

                                    </td>
                                    <td>

                                        total price

                                    </td>
                                    <td>

                                        <%value = "expiryDate_js_" + index;%>
                                        <jsp:include page="/date/date.jsp">
                                            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        </jsp:include>
                                        <input type='hidden' name='expiryDate' id = 'expiryDate_date_<%=index%>' value= '<%=dateFormat.format(new Date(purchaseDTO.expiryDate))%>' tag='pb_html'>

                                    </td>
                                    <td class="text-right">
                                        <div class='checker'>
                                            <span class='chkEdit' ><input type='checkbox' name='ID' value='<%=purchaseDTO.iD%>'/></span>
                                        </div>
                                    </td>
                                    <td style="display: none;">





                                        <input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=childTableStartingID%>' value='<%=purchaseDTO.searchColumn%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">





                                        <input type='hidden' class='form-control'  name='insertedBy' id = 'insertedByOrganogramId_hidden_<%=childTableStartingID%>' value='<%=purchaseDTO.insertedBy%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">





                                        <input type='hidden' class='form-control'  name='insertionTime' id = 'insertionDate_hidden_<%=childTableStartingID%>' value='<%=purchaseDTO.insertionTime%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">





                                        <input type='hidden' class='form-control'  name='modifiedBy' id = 'lastModifierUser_hidden_<%=childTableStartingID%>' value='<%=purchaseDTO.modifiedBy%>' tag='pb_html'/>
                                    </td>
                                    <td style="display: none;">





                                        <input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=purchaseDTO.isDeleted%>' tag='pb_html'/>

                                    </td>
                                    <td style="display: none;">





                                        <input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=purchaseDTO.lastModificationTime%>' tag='pb_html'/>
                                    </td>
                                </tr>

                                <%
                                            childTableStartingID ++;
                                        }
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>
                        
                        
                        
                    </div>
                </div>




                <div class="row mt-5">
                    <div class="col-6">
                        <div class="form-group row">
                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_PURCHASE_ADD_FILESDROPZONE, loginDTO)%>
                            </label>
                            <div class="col-8">
                                <%
                                    fileColumnName = "filesDropzone";
                                    if (actionName.equals("ajax_edit")) {
                                        List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(mainPurchaseDTO.filesDropzone);
                                %>
                                <%@include file="../pb/dropzoneEditor.jsp" %>
                                <%
                                    } else {
                                        ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                        mainPurchaseDTO.filesDropzone = ColumnID;
                                    }
                                %>

                                <div class="dropzone"
                                     action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=mainPurchaseDTO.filesDropzone%>">
                                    <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                           id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                </div>
                                <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                       id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                <input type='hidden' name='<%=fileColumnName%>'
                                       id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                       value='<%=mainPurchaseDTO.filesDropzone%>'/>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<jsp:include page="../procurement_goods/procurementGoodsModal.jsp">
    <jsp:param name="index" value="1"/>
</jsp:include>



<script type="text/javascript">

    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const bigForm = $('#bigform');
    var child_table_extra_id = 0;


    function preprocessPurchaseCheckBoxBeforeSubmitting(row) {
        if (document.getElementById('isSelected_checkbox_' + row).checked) {
            document.getElementById('selected_hidden_' + row).value = "true";
        } else {
            document.getElementById('selected_hidden_' + row).value = "false";
        }
    }


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        preprocessDateBeforeSubmitting('purchaseDate', row);
        // for (var j=1; j<child_table_extra_id; j++) {
        //     preprocessDateBeforeSubmitting('expiryDate', row);
        // }
        for (var j=1; j<=child_table_extra_id; j++) {
            preprocessPurchaseCheckBoxBeforeSubmitting(j);
        }

        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_purchaseServlet");
    }

    function init(row) {

        for (var j=1; j<child_table_extra_id; j++) {
            // setDateByStringAndId('expiryDate_js_' + row, $('#expiryDate_date_' + row).val());
        }

        setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());
        select2SingleSelector("#vendorId_select__0", '<%=Language%>');
        select2SingleSelector("#packageId_select__0", '<%=Language%>');
        select2SingleSelector("#fiscalYearId_select__0", '<%=Language%>');
        $.validator.addMethod('vendorSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('packageSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('fiscalYearSelection', function (value, element) {
            return value != 0;
        });
        bigForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vendorId: {
                    required: true,
                    vendorSelection: true
                },
                packageId: {
                    required: true,
                    packageSelection: true
                },
                fiscalYearId: {
                    required: true,
                    fiscalYearSelection:true
                },
            },

            messages: {
                vendorId: isLangEng ? "Please Select Vendor" : "অনুগ্রহ করে সরবরাহকারী নির্বাচন করুন",
                packageId: isLangEng ? "Please Select Package Type" : "অনুগ্রহ করে প্যাকেজ ধরণ প্রবেশ করুন",
                fiscalYearId: isLangEng ? "Please Enter Fiscal year" : "অনুগ্রহ করে অর্থবছর প্রবেশ করুন",
            }
        });
    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    child_table_extra_id = <%=childTableStartingID%>;







    function loadData(name) {

        var fiscalYearId = $("#fiscalYearId_select_0").val();
        var officeUnitId = <%=userDTO.unitID%>;
        var packageId = $("#packageId_select_0").val();
        var vendorId = $("#vendorId_select_0").val();

        var url = '';

        if (name == 'packageId' && fiscalYearId != '') {
            url = 'Pi_package_vendorServlet?actionType=getPackageByFiscalYearIdAndOfficeUnitId&fiscalYearId=' + fiscalYearId + '&officeUnitId=' + officeUnitId;
            ajaxGet(url, processPackageResponse, processPackageResponse);
        }
        if (name == 'vendorId' && fiscalYearId != '' && packageId != '') {
            url = 'Pi_package_vendorServlet?actionType=getVendorByFiscalYearIdAndOfficeUnitIdAndPackageId&fiscalYearId=' + fiscalYearId + '&officeUnitId=' + officeUnitId + '&packageId=' + packageId;
            ajaxGet(url, processVendorResponse, processVendorResponse);
        }
        if (name == 'field-Purchase' && fiscalYearId != '' && packageId != '' && vendorId != '') {
            url = 'Pi_package_vendorServlet?actionType=getPriceByFiscalYearIdAndOfficeUnitIdAndPackageIdAndVendorId&fiscalYearId=' + fiscalYearId + '&officeUnitId=' + officeUnitId + '&packageId=' + packageId + '&vendorId=' + vendorId;
            ajaxGet(url, processTableResponse, processTableResponse);
        }

    }

    function processPackageResponse(data) {
        if (data.responseCode == 200) {
            $('#packageId_select_0').html(data.msg);
        }
    }

    function processVendorResponse(data) {
        if (data.responseCode == 200) {
            $('#vendorId_select_0').html(data.msg);
        }
    }

    function processTableResponse(data) {
        if (data.responseCode == 200) {
            $('#field-Purchase').html(JSON.parse(data.msg).itemsHtml);
            child_table_extra_id = JSON.parse(data.msg).itemsCount;
        }
    }

    function loadTotalValue(index, language) {
        var amounts = document.getElementsByName('amount');
        var unitPrices = document.getElementsByName('unitPrice');

        var amountEn = parseInt(amounts[index-1].value.toString().toEng());
        var unitPriceEn = parseInt(unitPrices[index-1].value.toString().toEng());
        $("#totalBill_number_" + index).val((amountEn * unitPriceEn).toString());
    }

</script>






