<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="pi_purchase.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_package.ProcurementGoodsTypeRepository" %>

<%
    String LanguagePurchaseItems = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    SimpleDateFormat dateFormatPurchaseItems = new SimpleDateFormat("dd/MM/yyyy");
    String valuePurchaseItems = "";
    int childTableStartingIDPurchaseItems = 1;
    List<Pi_purchaseDTO> items = (List<Pi_purchaseDTO>) request.getAttribute("items");
    int indexPurchaseItems = 0;

      if (items != null) {
          for (Pi_purchaseDTO purchaseDTO: items) {
              indexPurchaseItems++;
              System.out.println("index index = "+indexPurchaseItems);
%>

           <tr id = "Purchase_<%=indexPurchaseItems + 1%>">
    <td style="display: none;">





        <input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=childTableStartingIDPurchaseItems%>' value='<%=purchaseDTO.iD%>' tag='pb_html'/>

    </td>
    <td>


        <%=LanguagePurchaseItems.equals("English")?
                Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(purchaseDTO.packageId).nameEn :
                Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(purchaseDTO.packageId).nameBn%>

    </td>
    <td>


        <%=LanguagePurchaseItems.equals("English")?
                ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(purchaseDTO.itemTypeId).nameEn :
                ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(purchaseDTO.itemTypeId).nameBn%>

    </td>
    <td>

        bortoman mojud

    </td>
    <td>

        <input type='number' class='form-control'  name='amount' id = 'amount_number_<%=childTableStartingIDPurchaseItems%>' value='<%=purchaseDTO.amount%>'  tag='pb_html'>

    </td>
    <td>

        APP boraddo

    </td>
    <td>

        APP oboshishto

    </td>
    <td>

        unit price

    </td>
    <td>

        total price

    </td>
    <td>

        <%valuePurchaseItems = "expiryDate_js_" + indexPurchaseItems;%>
        <jsp:include page="/date/date.jsp">
            <jsp:param name="DATE_ID" value="<%=valuePurchaseItems%>"></jsp:param>
            <jsp:param name="LANGUAGE" value="<%=LanguagePurchaseItems%>"></jsp:param>
        </jsp:include>
        <input type='hidden' name='expiryDate' id = 'expiryDate_date_<%=indexPurchaseItems%>' value= '<%=dateFormatPurchaseItems.format(new Date(purchaseDTO.expiryDate))%>' tag='pb_html'>

    </td>
    <td class="text-right">
        <div class='checker'>
            <span class='chkEdit' ><input type='checkbox' name='ID' value='<%=purchaseDTO.iD%>'/></span>
        </div>
    </td>
    <td style="display: none;">





        <input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=childTableStartingIDPurchaseItems%>' value='<%=purchaseDTO.searchColumn%>' tag='pb_html'/>
    </td>
    <td style="display: none;">





        <input type='hidden' class='form-control'  name='insertedBy' id = 'insertedByOrganogramId_hidden_<%=childTableStartingIDPurchaseItems%>' value='<%=purchaseDTO.insertedBy%>' tag='pb_html'/>
    </td>
    <td style="display: none;">





        <input type='hidden' class='form-control'  name='insertionTime' id = 'insertionDate_hidden_<%=childTableStartingIDPurchaseItems%>' value='<%=purchaseDTO.insertionTime%>' tag='pb_html'/>
    </td>
    <td style="display: none;">





        <input type='hidden' class='form-control'  name='modifiedBy' id = 'lastModifierUser_hidden_<%=childTableStartingIDPurchaseItems%>' value='<%=purchaseDTO.modifiedBy%>' tag='pb_html'/>
    </td>
    <td style="display: none;">





        <input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=childTableStartingIDPurchaseItems%>' value= '<%=purchaseDTO.isDeleted%>' tag='pb_html'/>

    </td>
    <td style="display: none;">





        <input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingIDPurchaseItems%>' value='<%=purchaseDTO.lastModificationTime%>' tag='pb_html'/>
    </td>
</tr>

<%
            childTableStartingIDPurchaseItems ++;
        }

    }
%>




<script type="text/javascript">

    var purchaseIndex = <%=indexPurchaseItems%>;

    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        preprocessDateBeforeSubmitting('purchaseDate', row);
        for (var j=1; j<purchaseIndex; j++) {
            preprocessDateBeforeSubmitting('expiryDate', row);
        }

        submitAddForm2();
        return false;
    }

    function init(row) {

        for (var j=1; j<purchaseIndex; j++) {
            setDateByStringAndId('expiryDate_js_' + row, $('#expiryDate_date_' + row).val());
        }

        setDateByStringAndId('purchaseDate_js_' + row, $('#purchaseDate_date_' + row).val());
        select2SingleSelector("#vendorId_select__0", '<%=LanguagePurchaseItems%>');
        select2SingleSelector("#packageId_select__0", '<%=LanguagePurchaseItems%>');
        select2SingleSelector("#fiscalYearId_select__0", '<%=LanguagePurchaseItems%>');
        $.validator.addMethod('vendorSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('packageSelection', function (value, element) {
            return value != 0;
        });
        $.validator.addMethod('fiscalYearSelection', function (value, element) {
            return value != 0;
        });
        bigForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vendorId: {
                    required: true,
                    vendorSelection: true
                },
                packageId: {
                    required: true,
                    packageSelection: true
                },
                fiscalYearId: {
                    required: true,
                    fiscalYearSelection:true
                },
            },

            messages: {
                vendorId: isLangEng ? "Please Select Vendor" : "অনুগ্রহ করে সরবরাহকারী নির্বাচন করুন",
                packageId: isLangEng ? "Please Select Package Type" : "অনুগ্রহ করে প্যাকেজ ধরণ প্রবেশ করুন",
                fiscalYearId: isLangEng ? "Please Enter Fiscal year" : "অনুগ্রহ করে অর্থবছর প্রবেশ করুন",
            }
        });
    }

</script>