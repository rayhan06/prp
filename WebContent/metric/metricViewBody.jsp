

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="metric.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "MetricServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
MetricDTO metricDTO = MetricDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = metricDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.METRIC_ADD_METRIC_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.METRIC_ADD_METRIC_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.METRIC_ADD_EQUATION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=metricDTO.equation%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.METRIC_ADD_X, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(String.format("%.1f", metricDTO.x), Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.METRIC_ADD_T, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(String.format("%.1f", metricDTO.t), Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.METRIC_ADD_METRICS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=metricDTO.metrics%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.METRIC_ADD_DIFFMETRICS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=metricDTO.diffMetrics%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.METRIC_ADD_INVMETRICS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=metricDTO.invMetrics%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.METRIC_ADD_GAMMAS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=metricDTO.gammas%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.METRIC_ADD_RIEMANNS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=metricDTO.riemanns%>
                                    </div>
                                </div>
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>