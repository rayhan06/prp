<%@page import="language.LC"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="language.LM"%>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="searchform.SearchForm"%>
<%@ page import="pb.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page contentType="text/html;charset=utf-8" %>


<%
	System.out.println("Inside nav.jsp");
	String url = "MetricServlet?actionType=search";	
%>
<%@include file="../pb/navInitializer.jsp"%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
				<input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text' class='form-control border-0'
					   onKeyUp='allfield_changed(false)' id='anyfield'  name='anyfield'
				>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>        
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

			<div class="row">
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.METRIC_ADD_EQUATION, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="equation" placeholder="" name="equation" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.METRIC_ADD_X, loginDTO)%></label>
						<div class="col-9">
							<input type="number" class="form-control" id="x" placeholder="" name="x" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.METRIC_ADD_T, loginDTO)%></label>
						<div class="col-9">
							<input type="number" class="form-control" id="t" placeholder="" name="t" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.METRIC_ADD_METRICS, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="metrics" placeholder="" name="metrics" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.METRIC_ADD_DIFFMETRICS, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="diff_metrics" placeholder="" name="diff_metrics" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.METRIC_ADD_INVMETRICS, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="inv_metrics" placeholder="" name="inv_metrics" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.METRIC_ADD_GAMMAS, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="gammas" placeholder="" name="gammas" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group row">
						<label class="col-3 col-form-label"><%=LM.getText(LC.METRIC_ADD_RIEMANNS, loginDTO)%></label>
						<div class="col-9">
							<input type="text" class="form-control" id="riemanns" placeholder="" name="riemanns" onChange='setSearchChanged()'>

						</div>
					</div>
				</div>
		</div>
		<div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed(true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
<div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
</div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">

	function getParams()
	{
		var params = 'AnyField=' + document.getElementById('anyfield').value;

		params +=  '&equation='+ $('#equation').val();
		params +=  '&metrics='+ $('#metrics').val();
		params +=  '&diff_metrics='+ $('#diff_metrics').val();
		params +=  '&inv_metrics='+ $('#inv_metrics').val();
		params +=  '&gammas='+ $('#gammas').val();
		params +=  '&riemanns='+ $('#riemanns').val();
		
		params +=  '&search=true';
		
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}

		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;
		
		return params;
	}

	function allfield_changed(setPushState)
	{
		var params = getParams();
		navsubmit(params, "MetricServlet", setPushState);
	}
	
	function resetFields()
	{
		$("#anyfield").val("");
		$("#equation").val("");
		$("#x").val("");
		$("#t").val("");
		$("#metrics").val("");
		$("#diff_metrics").val("");
		$("#inv_metrics").val("");
		$("#gammas").val("");
		$("#riemanns").val("");
	}
	
	 window.addEventListener('popstate',e=>{
		if(e.state){
			let params = e.state;
			navsubmit(params, "Test_moduleServlet", false);
			resetFields();
			let arr = params.split('&');
			arr.forEach(e=>{
				let item = e.split('=');
				if(item.length === 2){
					//console.log("param " + item[0] + " = " + item[1]);
					switch (item[0]){
						case 'anyfield':	                        	
							$("#anyfield").val(item[1]);
							break;
						case 'AnyField':
							$("#anyfield").val(item[1]);
							break;
						case 'equation':
							$("#equation").val(item[1]);
							break;
						case 'x':
							$("#x").val(item[1]);
							break;
						case 't':
							$("#t").val(item[1]);
							break;
						case 'metrics':
							$("#metrics").val(item[1]);
							break;
						case 'diff_metrics':
							$("#diff_metrics").val(item[1]);
							break;
						case 'inv_metrics':
							$("#inv_metrics").val(item[1]);
							break;
						case 'gammas':
							$("#gammas").val(item[1]);
							break;
						case 'riemanns':
							$("#riemanns").val(item[1]);
							break;
					}
				}
			});
		}else{
			navsubmit(null, "Test_moduleServlet", false);
		}
	});
	
	window.onbeforeunload = function(){
		 var params = getParams();
		 history.pushState(params,'', 'MetricServlet?actionType=search&'+params);
	};
	
	 $(document).ready(() => {
        readyInit('MetricServlet');
    });

</script>

