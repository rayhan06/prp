

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="asset_category.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Asset_categoryServlet";
String ID = request.getParameter("ID");
Asset_categoryDAO asset_categoryDAO = new Asset_categoryDAO();
long id = Long.parseLong(ID);
Asset_categoryDTO asset_categoryDTO = asset_categoryDAO.getDTOByID(id);
CommonDTO commonDTO = asset_categoryDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.ASSET_CATEGORY_ADD_ASSET_CATEGORY_ADD_FORMNAME, loginDTO)%>
                </h3>
                <div class="ml-auto mr-3">
				    <button type="button" class="btn" 
				            onclick="location.href='Asset_categoryServlet?actionType=search'">
				        <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-md-8 offset-md-2">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-md-8 offset-md-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.ASSET_CATEGORY_ADD_ASSET_CATEGORY_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_CATEGORY_ADD_NAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%
											value = asset_categoryDTO.nameEn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_CATEGORY_ADD_NAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%
											value = asset_categoryDTO.nameBn + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        <%=LM.getText(LC.ASSET_CATEGORY_ADD_ASSETTYPETYPE, loginDTO)%>
                                    </label>
                                    <div class="col-md-8">
											<%
											value = asset_categoryDTO.assetTypeType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "asset_type", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			

                                
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Parents
                                    </label>
                                    <div class="col-md-8">
											<%
											String[] parents = asset_categoryDTO.parentList.split(", ");
											if(asset_categoryDTO.parentList != null && !asset_categoryDTO.parentList.equalsIgnoreCase(""))
                                        	{
												int j = 0;
                                        		for(String parent: parents)
                                        		{
                                        			Asset_categoryDTO acTO = Asset_categoryRepository.getInstance().getAsset_categoryDTOByID(Long.parseLong(parent));
                                        			if(acTO != null)
                                        			{
                                        				%>
                                        				<%=acTO.nameEn%>
                                        				<%=j < parents.length  - 1? ", ":""%>
                                        				<%
                                        				j++;
                                        			}
                                        		}
                                        	}
                                            %>
											
				
			
                                    </div>
                                </div>
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>