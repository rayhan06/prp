<%@page import="asset_type.Asset_typeDTO"%>
<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>
<%@page import="asset_category.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Asset_categoryDTO asset_categoryDTO = new Asset_categoryDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	asset_categoryDTO = Asset_categoryDAO.getInstance().getDTOFromID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = asset_categoryDTO;
String tableName = "asset_category";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.ASSET_CATEGORY_ADD_ASSET_CATEGORY_ADD_FORMNAME, loginDTO);
String servletName = "Asset_categoryServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_categoryServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=asset_categoryDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ASSET_CATEGORY_ADD_NAMEEN, loginDTO)%> *</label>
                                                            <div class="col-md-9">
																<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=asset_categoryDTO.nameEn%>'  required="required"  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ASSET_CATEGORY_ADD_NAMEBN, loginDTO)%></label>
                                                            <div class="col-md-9">
																<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=asset_categoryDTO.nameBn%>'   tag='pb_html'/>					
															</div>
                                                      </div>
                                                      																	
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.ASSET_CATEGORY_ADD_ASSETTYPETYPE, loginDTO)%></label>
                                                            <div class="col-md-9">
																<select class='form-control'  name='assetTypeType' id = 'assetTypeType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "asset_type", asset_categoryDTO.assetTypeType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>
                                                      <div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-md-right">Parents</label>
                                                            <div class="col-md-9 row mx-0">
                                                            <%
                                                            	List<Asset_categoryDTO> asset_categoryDTOs = Asset_categoryDAO.getInstance().getAssetsByType(Asset_typeDTO.HARDWARE);
                                                                String[] parents = asset_categoryDTO.parentList.split(", ");
                                                                for(Asset_categoryDTO acDTO: asset_categoryDTOs)
                                                                {
                                                                	boolean dtoInParent = false;
                                                                	if(asset_categoryDTO.parentList != null && !asset_categoryDTO.parentList.equalsIgnoreCase(""))
                                                                	{
                                                                		for(String parent: parents)
                                                                		{
                                                                			if(acDTO.iD == Long.parseLong(parent))
                                                                			{
                                                                				dtoInParent = true;
                                                                				break;
                                                                			}
                                                                		}
                                                                	}
                                                            %>
                                                            
                                                            <div class="col-md-4 d-flex align-items-start">
																<input type='checkbox' class='form-control-sm mt-1' name='parent' 
																<%=dtoInParent?"checked":""%> 
																value = "<%=acDTO.iD%>"
																>
																<label class="col-form-label ml-2"><%=acDTO.nameEn%></label>
		
															</div>
															
                                                            <%
                                                            }
                                                            %>
                                                            </div>
                                                            
                                                      </div>
                                                      
                                                      <div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-md-right">In Assignable</label>
                                                            <div class="col-md-9">
																<input type='checkbox' class='form-control-sm mt-1'  name='isAssignable' id = 'isAssignable'
																<%=asset_categoryDTO.isAssignable?"checked":"" %>
																 tag='pb_html'/>					
															</div>
                                                      </div>									
																					
																		
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.ASSET_CATEGORY_ADD_ASSET_CATEGORY_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.ASSET_CATEGORY_ADD_ASSET_CATEGORY_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Asset_categoryServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






