<form class="form-horizontal"
      action="Vm_route_travelServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
    <div class="kt-portlet__body form-body">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="onlyborder">
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-lg-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=vm_route_travelDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_route_travelDTO.lastModificationTime%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>' value='<%=vm_route_travelDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='status' id='status_hidden_<%=i%>'
                                           value='<%=CommonApprovalStatus.PENDING.getValue()%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTOPPAGEID, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' onchange="showHideOthers();"
                                                    name='requestedStoppageId' id='requestedStoppageId_type_<%=i%>'
                                                    tag='pb_html'>
                                                <%
                                                    Options = new VmRouteStoppageDAO().getOptionList(Language, vm_route_travelDTO.requestedStoppageId);
//                                                                    Options = CommonDAO.getOptions(Language, "vm_route_stoppage", "stoppage_name", "stoppage_name", String.valueOf(vm_route_travelDTO.requestedStoppageId));
//																	Options += "<option value = '-1'>" + (Language.equalsIgnoreCase("English") ? "Others" : "অন্যান্য") + "</option>";
                                                %>
                                                <%=Options%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="requestedStoppageName_div" style="display: none">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTOPPAGENAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='requestedStoppageName'
                                                   id='requestedStoppageName_text_<%=i%>'
                                                   value='<%=vm_route_travelDTO.requestedStoppageName%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTARTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "requestedStartDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='requestedStartDate'
                                                   id='requestedStartDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(vm_route_travelDTO.requestedStartDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REMARKS, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
															<textarea class='form-control' name='remarks'
                                                                      value='<%=vm_route_travelDTO.remarks%>'
                                                                      id='remarks_text_<%=i%>'>
																</textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11 form-actions text-right mt-3">
                <a
                        class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                        href="<%=request.getHeader("referer")%>">
                    <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_CANCEL_BUTTON, loginDTO)%>
                </a>
                <button
                        class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                        onclick="event.preventDefault();submitBigForm()">
                    <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_SUBMIT_BUTTON, loginDTO)%>
                </button>
            </div>
        </div>

    </div>
</form>