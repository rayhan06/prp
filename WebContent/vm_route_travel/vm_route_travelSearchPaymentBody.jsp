<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="vm_route_travel.Vm_route_travelDAO" %>
<%@ page import="vm_route_travel.Vm_route_travelDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="vm_route_travel.Vm_route_travelRepository" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "Vm_route_travelServlet?actionType=search";
    String navigator = SessionConstants.NAV_VM_ROUTE_TRAVEL;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO();
    List<Vm_route_travelDTO> vm_route_travelDTOList = Vm_route_travelRepository.getInstance().getApprovedVm_route_travelDTOByrequester_org_id(Long.parseLong((String)request.getAttribute("requester_org_id")));

    request.setAttribute("vm_route_travelDAO", vm_route_travelDAO);

    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>

<style>
    .kt-content {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }
</style>

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item border-bottom" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.VM_ROUTE_TRAVEL_SEARCH_VM_ROUTE_TRAVEL_SEARCH_FORMNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <form action="Vm_route_travelServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                  method="POST"
                  id="tableForm" enctype="multipart/form-data">
                <%
                    boolean first = true;
                    for (Vm_route_travelDTO vm_route_travelDTO : vm_route_travelDTOList) {
                        request.setAttribute("vm_route_travelDTO", vm_route_travelDTO);

                        if (first) {
                %>
                <!-- begin:: Content -->
                <div class="mb-5">
                    <jsp:include page="vm_route_travelSearchPaymentFormBodyFull.jsp" flush="true">
                        <jsp:param name="pageName"
                                   value="<%=LM.getText(LC.VM_ROUTE_TRAVEL_SEARCH_VM_ROUTE_TRAVEL_SEARCH_FORMNAME, loginDTO)%>"/>
                    </jsp:include>
                </div>
                <%
                } else {
                %>
                <div class="my-5">
                    <jsp:include page="vm_route_travelSearchPaymentFormBody.jsp" flush="true">
                        <jsp:param name="pageName"
                                   value="<%=LM.getText(LC.VM_ROUTE_TRAVEL_SEARCH_VM_ROUTE_TRAVEL_SEARCH_FORMNAME, loginDTO)%>"/>
                    </jsp:include>
                </div>
                <%
                    }
                    first = false;

                    if (vm_route_travelDTO.status != CommonApprovalStatus.PENDING.getValue()
                            && vm_route_travelDTO.status != CommonApprovalStatus.DISSATISFIED.getValue()
                            && vm_route_travelDTO.status != CommonApprovalStatus.CANCELLED.getValue()) {
                %>
                <div class="">
                    <jsp:include page="vm_route_travelSearchPaymentForm.jsp" flush="true">
                        <jsp:param name="pageName"
                                   value="<%=LM.getText(LC.VM_ROUTE_TRAVEL_SEARCH_VM_ROUTE_TRAVEL_SEARCH_FORMNAME, loginDTO)%>"/>
                    </jsp:include>
                </div>
                <%
                        }
                    }
                %>
            </form>
        </div>
    </div>
</div>
<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initDeleteCheckBoxes();
        dateTimeInit("<%=Language%>");
    });

</script>


