<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vm_route_travel.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
Vm_route_travelDTO vm_route_travelDTO;
vm_route_travelDTO = (Vm_route_travelDTO)request.getAttribute("vm_route_travelDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(vm_route_travelDTO == null)
{
	vm_route_travelDTO = new Vm_route_travelDTO();
	
}
System.out.println("vm_route_travelDTO = " + vm_route_travelDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_ADD_FORMNAME, loginDTO);
String servletName = "Vm_route_travelServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID = -1;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Vm_route_travelServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="onlyborder">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                     </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=vm_route_travelDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=vm_route_travelDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=vm_route_travelDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=vm_route_travelDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=vm_route_travelDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=vm_route_travelDTO.lastModificationTime%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=vm_route_travelDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requestedStoppageId' id = 'requestedStoppageId_hidden_<%=i%>' value='<%=vm_route_travelDTO.requestedStoppageId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTOPPAGENAME, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requestedStoppageName' id = 'requestedStoppageName_text_<%=i%>' value='<%=vm_route_travelDTO.requestedStoppageName%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REMARKS, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='remarks' id = 'remarks_text_<%=i%>' value='<%=vm_route_travelDTO.remarks%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTARTDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "requestedStartDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='requestedStartDate' id = 'requestedStartDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_route_travelDTO.requestedStartDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='requesterOrgId' id = 'requesterOrgId_hidden_<%=i%>' value='<%=vm_route_travelDTO.requesterOrgId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requesterOfficeId' id = 'requesterOfficeId_hidden_<%=i%>' value='<%=vm_route_travelDTO.requesterOfficeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requesterOfficeUnitId' id = 'requesterOfficeUnitId_hidden_<%=i%>' value='<%=vm_route_travelDTO.requesterOfficeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requesterEmpId' id = 'requesterEmpId_hidden_<%=i%>' value='<%=vm_route_travelDTO.requesterEmpId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERPHONENUM, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterPhoneNum' id = 'requesterPhoneNum_text_<%=i%>' value='<%=vm_route_travelDTO.requesterPhoneNum%>' 																<% 
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="880[0-9]{10}" title="requesterPhoneNum must start with 880, then contain 10 digits"
																<%
																	}
																%>
  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterNameEn' id = 'requesterNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.requesterNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterNameBn' id = 'requesterNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.requesterNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeNameEn' id = 'requesterOfficeNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.requesterOfficeNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeNameBn' id = 'requesterOfficeNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.requesterOfficeNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeUnitNameEn' id = 'requesterOfficeUnitNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.requesterOfficeUnitNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeUnitNameBn' id = 'requesterOfficeUnitNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.requesterOfficeUnitNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeUnitOrgNameEn' id = 'requesterOfficeUnitOrgNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.requesterOfficeUnitOrgNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='requesterOfficeUnitOrgNameBn' id = 'requesterOfficeUnitOrgNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.requesterOfficeUnitOrgNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_STATUS, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(vm_route_travelDTO.status != -1)
																	{
																	value = vm_route_travelDTO.status + "";
																	}
																%>		
																<input type='number' class='form-control'  name='status' id = 'status_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='routeId' id = 'routeId_hidden_<%=i%>' value='<%=vm_route_travelDTO.routeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='stoppageId' id = 'stoppageId_hidden_<%=i%>' value='<%=vm_route_travelDTO.stoppageId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='fiscalYearId' id = 'fiscalYearId_hidden_<%=i%>' value='<%=vm_route_travelDTO.fiscalYearId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEDSTARTDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "approvedStartDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='approvedStartDate' id = 'approvedStartDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_route_travelDTO.approvedStartDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_DECISIONREMARKS, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='decisionRemarks' id = 'decisionRemarks_text_<%=i%>' value='<%=vm_route_travelDTO.decisionRemarks%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FILESDROPZONE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																fileColumnName = "filesDropzone";
																if(actionName.equals("edit"))
																{
																	List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_route_travelDTO.filesDropzone);
																%>			
																	<%@include file="../pb/dropzoneEditor.jsp"%>
																<%
																}
																else
																{
																	ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
																	vm_route_travelDTO.filesDropzone = ColumnID;
																}
																%>
				
																<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_route_travelDTO.filesDropzone%>">
																	<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>			
																</div>								
																<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
																<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=vm_route_travelDTO.filesDropzone%>'/>		
		


															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEDDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "approvedDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='approvedDate' id = 'approvedDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_route_travelDTO.approvedDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWALDATE, loginDTO)%></label>
                                                            <div class="col-8">
																<%value = "withdrawalDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='withdrawalDate' id = 'withdrawalDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(vm_route_travelDTO.withdrawalDate))%>' tag='pb_html'>
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='approverOrgId' id = 'approverOrgId_hidden_<%=i%>' value='<%=vm_route_travelDTO.approverOrgId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approverOfficeId' id = 'approverOfficeId_hidden_<%=i%>' value='<%=vm_route_travelDTO.approverOfficeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approverOfficeUnitId' id = 'approverOfficeUnitId_hidden_<%=i%>' value='<%=vm_route_travelDTO.approverOfficeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approverEmpId' id = 'approverEmpId_hidden_<%=i%>' value='<%=vm_route_travelDTO.approverEmpId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVERPHONENUM, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverPhoneNum' id = 'approverPhoneNum_text_<%=i%>' value='<%=vm_route_travelDTO.approverPhoneNum%>' 																<% 
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="880[0-9]{10}" title="approverPhoneNum must start with 880, then contain 10 digits"
																<%
																	}
																%>
  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVERNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverNameEn' id = 'approverNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.approverNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVERNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverNameBn' id = 'approverNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.approverNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICENAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeNameEn' id = 'approverOfficeNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.approverOfficeNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICENAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeNameBn' id = 'approverOfficeNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.approverOfficeNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeUnitNameEn' id = 'approverOfficeUnitNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.approverOfficeUnitNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeUnitNameBn' id = 'approverOfficeUnitNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.approverOfficeUnitNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITORGNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeUnitOrgNameEn' id = 'approverOfficeUnitOrgNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.approverOfficeUnitOrgNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEROFFICEUNITORGNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='approverOfficeUnitOrgNameBn' id = 'approverOfficeUnitOrgNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.approverOfficeUnitOrgNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='withdrawerOrgId' id = 'withdrawerOrgId_hidden_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOrgId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='withdrawerOfficeId' id = 'withdrawerOfficeId_hidden_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOfficeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='withdrawerOfficeUnitId' id = 'withdrawerOfficeUnitId_hidden_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOfficeUnitId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='withdrawerEmpId' id = 'withdrawerEmpId_hidden_<%=i%>' value='<%=vm_route_travelDTO.withdrawerEmpId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWERPHONENUM, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerPhoneNum' id = 'withdrawerPhoneNum_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerPhoneNum%>' 																<% 
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="880[0-9]{10}" title="withdrawerPhoneNum must start with 880, then contain 10 digits"
																<%
																	}
																%>
  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWERNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerNameEn' id = 'withdrawerNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWERNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerNameBn' id = 'withdrawerNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICENAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerOfficeNameEn' id = 'withdrawerOfficeNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOfficeNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICENAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerOfficeNameBn' id = 'withdrawerOfficeNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOfficeNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerOfficeUnitNameEn' id = 'withdrawerOfficeUnitNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOfficeUnitNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerOfficeUnitNameBn' id = 'withdrawerOfficeUnitNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOfficeUnitNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITORGNAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerOfficeUnitOrgNameEn' id = 'withdrawerOfficeUnitOrgNameEn_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOfficeUnitOrgNameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_WITHDRAWEROFFICEUNITORGNAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='withdrawerOfficeUnitOrgNameBn' id = 'withdrawerOfficeUnitOrgNameBn_text_<%=i%>' value='<%=vm_route_travelDTO.withdrawerOfficeUnitOrgNameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
					
														</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>                               
                          </div>
                       </div>
                       <div class="form-actions text-center mb-5">
	                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_CANCEL_BUTTON, loginDTO)%></a>										
							<button class="btn btn-success" type="submit"><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_SUBMIT_BUTTON, loginDTO)%></button>
					   </div>
                   </form>
               </div>                      
          </div>
      </div>
 </div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{


	preprocessDateBeforeSubmitting('requestedStartDate', row);
	preprocessDateBeforeSubmitting('approvedStartDate', row);
	preprocessDateBeforeSubmitting('approvedDate', row);
	preprocessDateBeforeSubmitting('withdrawalDate', row);

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vm_route_travelServlet");	
}

function init(row)
{

	setDateByStringAndId('requestedStartDate_js_' + row, $('#requestedStartDate_date_' + row).val());
	setDateByStringAndId('approvedStartDate_js_' + row, $('#approvedStartDate_date_' + row).val());
	setDateByStringAndId('approvedDate_js_' + row, $('#approvedDate_date_' + row).val());
	setDateByStringAndId('withdrawalDate_js_' + row, $('#withdrawalDate_date_' + row).val());

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






