<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_route_travel.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="vm_route.VmRouteStoppageDAO" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawDAO" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawDTO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="vm_route_travel_withdraw.Vm_route_travel_withdrawRepository" %>

<%
    Vm_route_travelDTO vm_route_travelDTO;
    vm_route_travelDTO = (Vm_route_travelDTO) request.getAttribute("vm_route_travelDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_route_travelDTO == null) {
        vm_route_travelDTO = new Vm_route_travelDTO();

    }
    System.out.println("vm_route_travelDTO = " + vm_route_travelDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_route_travelServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    Vm_route_travel_withdrawDAO vm_route_travel_withdrawDAO = new Vm_route_travel_withdrawDAO("vm_route_travel_withdraw");
    //Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO = vm_route_travel_withdrawDAO.getDTOForRouteEnrollmentByRequesterOrgID(userDTO.organogramID);
    Vm_route_travel_withdrawDTO vm_route_travel_withdrawDTO =null;

    List<Vm_route_travel_withdrawDTO> vm_route_travel_withdrawDT0s1 = Vm_route_travel_withdrawRepository.getInstance().getPendingVm_route_travel_withdrawDTOByRequesterOrgID(userDTO.organogramID);
    if(vm_route_travel_withdrawDT0s1.size()>0) {
        vm_route_travel_withdrawDTO = vm_route_travel_withdrawDT0s1.get(0);
    }



//System.out.println("vm_route_travel_withdrawDTO dto jsp: "+userDTO.organogramID);


    boolean routeTravelEnrollment = false;
    int routeTravelE = 0;
//if(vm_route_travelDTO.status==CommonApprovalStatus.PENDING.getValue() || vm_route_travel_withdrawDTO!=null){
    Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO();
    Vm_route_travelDTO vm_route_travelDTOCheckStatus = vm_route_travelDAO.getDTOByOrganogramIDForCheckStatus(userDTO.organogramID);

    if (vm_route_travelDTOCheckStatus != null && actionName.equalsIgnoreCase("add")) {

        if (vm_route_travelDTOCheckStatus.status == CommonApprovalStatus.PRE_PENDING.getValue() || vm_route_travelDTOCheckStatus.status == CommonApprovalStatus.PENDING.getValue() || vm_route_travelDTOCheckStatus.status == CommonApprovalStatus.SATISFIED.getValue() || vm_route_travel_withdrawDTO != null) {
            routeTravelEnrollment = false;
            routeTravelE = 0;
            //System.out.println("vm_route_travelDTOCheckStatus.status: "+vm_route_travelDTOCheckStatus.status);
        } else {
            routeTravelEnrollment = true;
            routeTravelE = 1;
        }
    } else {
        routeTravelEnrollment = true;
        routeTravelE = 1;
    }

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <input type="hidden" id="withdrawVal" value="<%=routeTravelE%>">

        <%

            if (routeTravelEnrollment) {

        %>

        <%@ include file="vmRouteTravelContent.jsp" %>


        <%

        } else {%>

        <% }

        %>

    </div>
</div>

<script type="text/javascript">

    function showHideOthers() {
        var value = $("#requestedStoppageId_type_0").val();
        if (value == undefined || value.toString().trim() != '-1') {
            $("#requestedStoppageName_div").hide();
        } else {
            $("#requestedStoppageName_div").show();
        }
    }

    function processResponse(data) {

        if (data.includes('No driver')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, No driver found for this vehicle!" : "দুঃখিত, এই গাড়ির কোনো চালক পাওয়া যায়নি "%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        } else if (data.includes('Invalid Input')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, missing required field!" : "দুঃখিত, আবশ্যকীয় তথ্য প্রদান করুন "%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        } else {
            window.location = 'Vm_route_travelServlet?actionType=search';
        }
    }

    function ajaxPost(url, accept, send, postData, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: accept,
            contentType: send,
            data: postData,
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


    function submitBigForm() {

        if (PreprocessBeforeSubmiting(0, '<%=actionName%>')) {
            var form = $("#bigform");

            var actionUrl = form.attr("action");
            var postData = (form.serialize());

            ajaxPost(actionUrl, "json", "application/x-www-form-urlencoded", postData, processResponse, processResponse);
        }


    }

    function PreprocessBeforeSubmiting(row, validate) {


        preprocessDateBeforeSubmitting('requestedStartDate', row);

        let form = $("#bigform");
        form.validate();
        let valid = form.valid();

        return valid;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_route_travelServlet");
    }

    function init(row) {

        setDateByStringAndId('requestedStartDate_js_' + row, $('#requestedStartDate_date_' + row).val());

        showHideOthers();

    }

    var row = 0;
    $(document).ready(function () {

        let val = document.getElementById('withdrawVal').value;

        if (val == 0) {
            setTimeout(function () {
                swal.fire({
                    title: '<%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_ADD_FORMNAME, loginDTO)%>',
                    text: '<%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_YOU_ALREADY_APPLIED, loginDTO)%>',
                    type: "error",
                    allowOutsideClick: false,
                    confirmButtonColor: "#00B4B4"
                }, function () {
                    window.location.href = "";
                });
            }, 0);


        }

        init(row);
        // CKEDITOR.replaceAll();

        $.validator.addMethod('validStoppage', function (value, element) {
            var others = $("#requestedStoppageName_text_0").val();
            return !$('#requestedStoppageName_text_0').is(':visible') || (value && value != -1) || others.toString().trim().length > 0;
        });

        let lang = '<%=Language%>';
        let stoppageError;

        if (lang == 'English') {
            stoppageError = 'Please provide stoppage';

        } else {
            stoppageError = 'স্টপেজ প্রদান করুন';

        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                requestedStoppageName: {
                    validStoppage: true,
                },

            },
            messages: {
                requestedStoppageName: stoppageError,

            }
        });


    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






