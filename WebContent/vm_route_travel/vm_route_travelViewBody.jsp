<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_route_travel.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>


<%
    String servletName = "Vm_route_travelServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_ROUTE_TRAVEL_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_route_travelDAO vm_route_travelDAO = new Vm_route_travelDAO("vm_route_travel");
    Vm_route_travelDTO vm_route_travelDTO = Vm_route_travelRepository.getInstance().getVm_route_travelDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_ADD_FORMNAME, loginDTO)%>
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body form-body">
                    <div class="row">
                        <%--						<div class="col-md-1"></div>--%>
                        <div class="col-md-12">
                            <div class="onlyborder">
                                <div class="row">
                                    <%--										<div class="col-md-2"></div>--%>
                                    <div class="col-md-12">
                                        <div class="sub_title_top">
                                            <div class="sub_title">
                                                <h4 style="background: white">
                                                    <%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_VM_ROUTE_TRAVEL_ADD_FORMNAME, loginDTO)%>
                                                </h4>
                                            </div>

                                            <div class="col-md-12">
                                                <table class="table table-bordered table-striped">


                                                    <%
                                                        if (Language.equals("English")) {
                                                    %>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERNAMEEN, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requesterNameEn + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requesterOfficeUnitOrgNameEn + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requesterOfficeUnitNameEn + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICENAMEEN, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requesterOfficeNameEn + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <%
                                                    } else {
                                                    %>
                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTERNAMEBN, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requesterNameBn + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requesterOfficeUnitOrgNameBn + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requesterOfficeUnitNameBn + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEROFFICENAMEBN, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requesterOfficeNameBn + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <%
                                                        }
                                                    %>

                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REQUESTEDSTARTDATE, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requestedStartDate + "";
                                                            %>
                                                            <%
                                                                String formatted_requestedStartDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                            %>
                                                            <%=Utils.getDigits(formatted_requestedStartDate, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_STATUS, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = CatRepository.getName(Language, "vm_requisition_status", vm_route_travelDTO.status);
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_ROUTEID, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.routeId + "";
                                                                value = CommonDAO.getName(Language, "vm_route", vm_route_travelDTO.routeId);
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_STOPPAGEID, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.requestedStoppageId + "";
                                                                if (value.equals("-1")) {
                                                                    value = Language.equals("English") ? "Others" : "অন্যান্য";
                                                                    value += "(" + vm_route_travelDTO.requestedStoppageName + ")";
                                                                } else {
                                                                    value = CommonDAO.getName(vm_route_travelDTO.requestedStoppageId, "vm_route_stoppage", "stoppage_name", "ID");
                                                                }
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FISCALYEARID, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = "";
                                                                if (vm_route_travelDTO.fiscalYearId != -1) {
                                                                    value = CommonDAO.getName(Language, "fiscal_year", vm_route_travelDTO.fiscalYearId);
                                                                }
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_FILESDROPZONE, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.filesDropzone + "";
                                                            %>
                                                            <%
                                                                {
                                                                    List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(vm_route_travelDTO.filesDropzone);
                                                            %>
                                                            <%@include file="../pb/dropzoneViewer.jsp" %>
                                                            <%
                                                                }
                                                            %>


                                                        </td>

                                                    </tr>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_REMARKS, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.remarks + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <%
                                                        if (vm_route_travelDTO.status == CommonApprovalStatus.DISSATISFIED.getValue()) {

                                                            if (!Language.equals("English")) {
                                                    %>

                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEREMPID, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.approverNameBn + " (" + vm_route_travelDTO.approverOfficeUnitOrgNameBn + ", " + vm_route_travelDTO.approverOfficeUnitNameBn + ")";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>

                                                    <%
                                                    } else {
                                                    %>


                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_APPROVEREMPID, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.approverNameEn + " (" + vm_route_travelDTO.approverOfficeUnitOrgNameEn + ", " + vm_route_travelDTO.approverOfficeUnitNameEn + ")";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>


                                                    <%
                                                        }
                                                    %>

                                                    <tr>
                                                        <td style="width:30%">
                                                            <b><%=LM.getText(LC.VM_ROUTE_TRAVEL_ADD_DECISIONREMARKS, loginDTO)%>
                                                            </b></td>
                                                        <td>

                                                            <%
                                                                value = vm_route_travelDTO.decisionRemarks + "";
                                                            %>

                                                            <%=Utils.getDigits(value, Language)%>


                                                        </td>

                                                    </tr>

                                                    <%
                                                        }
                                                    %>


                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                    <%--									<div class="col-md-1"></div>--%>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <%--						<div class="col-md-1"></div>--%>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>