<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="ticket_issues.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="java.util.List" %>
<%@ page import="workflow.WorkflowController" %>
<%@page pageEncoding="UTF-8" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.TICKET_ISSUES_ADD_NAMEEN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TICKET_ISSUES_ADD_NAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.TICKET_ISSUES_SEARCH_TICKET_ISSUES_EDIT_BUTTON, loginDTO)%>
            </th>

            <th class="text-center" style="width: 25px">
                <span><%=isLangEng?"All":"সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Ticket_issuesDTO> data = (List<Ticket_issuesDTO>) rn2.list;

            if (data != null && data.size() > 0) {
                for (Ticket_issuesDTO ticket_issuesDTO : data) {
        %>
        <tr>
            <td>
                <%=ticket_issuesDTO.nameEn%>
            </td>

            <td>
                <%=ticket_issuesDTO.nameBn%>
            </td>

            <td>
                <%=WorkflowController.getNameFromOrganogramId(ticket_issuesDTO.adminOrganogramId, isLangEng)%>
            </td>

            <td >
                <button type="button" class="btn-sm btn-secondary border-0 shadow btn-border-radius" style="color: #ff6b6b;"
                        onclick="location.href='Ticket_issuesServlet?actionType=view&ID=<%=ticket_issuesDTO.iD%>'">
                    <i class="fa fa-eye"></i>
                </button>
            </td>

            <td >
                <button type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='Ticket_issuesServlet?actionType=getEditPage&ID=<%=ticket_issuesDTO.iD%>'">
                    <i class="fa fa-edit"></i>
                </button>
            </td>


            <td id='checkbox' class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=ticket_issuesDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <%
                }
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>