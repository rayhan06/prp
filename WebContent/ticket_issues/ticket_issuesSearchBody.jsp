<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    String url = "Ticket_issuesServlet?actionType=search";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    RecordNavigator rn = (RecordNavigator) session.getAttribute("recordNavigator");
    String context = "../../.." + request.getContextPath() + "/";
    int pagination_number = 0;
%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.TICKET_ISSUES_SEARCH_TICKET_ISSUES_SEARCH_FORMNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="./ticket_issuesNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.TICKET_ISSUES_SEARCH_TICKET_ISSUES_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <form action="Ticket_issuesServlet?isPermanentTable=true&actionType=delete"
                          method="POST" id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="ticket_issuesSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.TICKET_ISSUES_SEARCH_TICKET_ISSUES_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initDeleteCheckBoxes();
    });

</script>