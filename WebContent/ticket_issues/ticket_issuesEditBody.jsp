<%@page import="pb.CatRepository"%>
<%@page import="pb.CatDTO"%>
<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="ticket_issues.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="office_units.*" %>
<%@page import="office_unit_organograms.*" %>
<%@page import="employee_offices.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    Ticket_issuesDTO ticket_issuesDTO;
    String actionName, formTitle;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        ticket_issuesDTO = (Ticket_issuesDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
        formTitle = LM.getText(LC.TICKET_ISSUES_EDIT_TICKET_ISSUES_EDIT_FORMNAME, loginDTO);
    } else {
        actionName = "add";
        ticket_issuesDTO = new Ticket_issuesDTO();
        formTitle = LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_ADD_FORMNAME, loginDTO);
    }
    int childTableStartingID = 1;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    //Office_unitsDAO office_unitsDAO = Office_unitsDAO.getInstance();
    //Set<Long> officeIds = OfficeUnitOrganogramsRepository.getInstance().getDescentsOfficeUnitId(SessionConstants.BROADCASTING_AND_IT_WING);
    List<Office_unitsDTO> office_unitsDTOs = Office_unitsRepository.getInstance().getAllChildOfficeDTOWithParent(SessionConstants.BROADCASTING_AND_IT_WING);

    /*for (long officeId : officeIds) {
        Office_unitsDTO office_unitsDTO = office_unitsDAO.getDTOFromID(officeId);
        if (office_unitsDTO != null) {
            office_unitsDTOs.add(office_unitsDTO);
        }
    }*/
    int i = 0;
    boolean isLangEng = Language.equalsIgnoreCase("english");
    List<CatDTO> eServiceCats = CatRepository.getDTOs("eservice");
%>

<style>
    @media (max-width: 767.98px) {
        .table-section input.form-control {
            width: auto !important;
        }

        .table-section select.form-control {
            width: auto !important;
        }
    }


    @media (min-width: 768px) {
        .table-section input.form-control {
            width: 100% !important;
        }

        .table-section select.form-control {
            width: 100% !important;
        }
    }


</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Ticket_issuesServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=ticket_issuesDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TICKET_ISSUES_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>' value='<%=ticket_issuesDTO.nameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TICKET_ISSUES_ADD_NAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>' value='<%=ticket_issuesDTO.nameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.HM_ASSIGNED_TO, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='adminOrganogramId'
                                                    id='adminOrganogramId'
                                                    tag='pb_html' required>
                                                <option><%=LM.getText(LC.HM_SELECT, loginDTO)%>
                                                </option>
                                                <%
                                                    Set<Long> ticketAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.TICKET_ADMIN_ROLE);
                                                    for (Long ticketAdmin : ticketAdmins) {
                                                %>
                                                <option value="<%=ticketAdmin%>" <%=ticket_issuesDTO.adminOrganogramId == ticketAdmin ? "selected" : "" %>>
                                                    <%=WorkflowController.getNameFromOrganogramId(ticketAdmin, Language)%>
                                                </option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-section">
                    <h5 class="table-title mt-5">
                        <%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_SUBTYPE, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_SUBTYPE_NAMEEN, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_SUBTYPE_NAMEBN, loginDTO)%>
                                </th>
                                <%
                                if(ticket_issuesDTO.iD == Ticket_issuesDTO.ESERVICE)
                                {
                                	%>
                                <th><%=isLangEng?"E-Service Types":"ইসেবার ধরণ"%>
                                </th>
                                	<%
                                }
                                %>
                                <th><%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_SUBTYPE_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-TicketIssuesSubtype">
                            <%
                                if (actionName.equals("edit")) {
                                    int index = -1;
                                    ticket_issuesDTO.ticketIssuesSubtypeDTOList = TicketIssueSubTypeRepository.getInstance().getByTicketIssuesId(ticket_issuesDTO.iD);
                                    for (TicketIssuesSubtypeDTO ticketIssuesSubtypeDTO : ticket_issuesDTO.ticketIssuesSubtypeDTOList) {
                                        index++;
                            %>

                            <tr id="TicketIssuesSubtype_<%=index + 1%>">
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='ticketIssuesSubtype.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=ticketIssuesSubtypeDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='ticketIssuesSubtype.ticketIssuesId'
                                           id='ticketIssuesId_hidden_<%=childTableStartingID%>'
                                           value='<%=ticketIssuesSubtypeDTO.ticketIssuesId%>' tag='pb_html'/>
                                </td>
                                <td>


                                    <input required type='text' class='form-control'
                                           name='ticketIssuesSubtype.nameEn'
                                           id='nameEn_text_<%=childTableStartingID%>'
                                           value='<%=ticketIssuesSubtypeDTO.nameEn%>' tag='pb_html'/>
                                </td>
                                <td>


                                    <input required type='text' class='form-control'
                                           name='ticketIssuesSubtype.nameBn'
                                           id='nameBn_text_<%=childTableStartingID%>'
                                           value='<%=ticketIssuesSubtypeDTO.nameBn%>' tag='pb_html'/>
                                </td>
                                
                                 <%
                                if(ticket_issuesDTO.iD == Ticket_issuesDTO.ESERVICE)
                                {
                                	%>
                                <td>
                                	<table>
                                		<tr>
                                		<%
                                		for(CatDTO eserviceDTO: eServiceCats)
                                		{
                                			%>
                                			<td><%=isLangEng?eserviceDTO.nameEn:eserviceDTO.nameBn%></td>
                                			<td>
                                			<input type='checkbox' class='form-control'
                                           	name='es'
                                           	<%=ticketIssuesSubtypeDTO.eserviceCatsArray.contains((long)eserviceDTO.value)?"checked":""%>
                                           	value='<%=eserviceDTO.value%>' tag='pb_html'/>
                                			</td>
                                			<%
                                		}
                                		%>
                                		</tr>
                                	</table>
                                	<input  type='hidden' class='form-control'
                                           name='ticketIssuesSubtype.eserviceCats'
                                           id='eserviceCats_text_<%=childTableStartingID%>'
                                           value='<%=ticketIssuesSubtypeDTO.eserviceCats%>' tag='pb_html'/>
                                </td>
                                	<%
                                }
                                %>


                                <td>
                                    <div class='checker'>
                                        <span>
                                            <input type='checkbox' id='ticketIssuesSubtype_cb_<%=index%>' name='checkbox' value=''/>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>

                            </tbody>
                        </table>


                    </div>
                    <div class="form-group">
                        <div class="col-xs-9 text-right">
                            <button
                                    id="add-more-TicketIssuesSubtype"
                                    name="add-moreTicketIssuesSubtype"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                            </button>
                            <button
                                    id="remove-TicketIssuesSubtype"
                                    name="removeTicketIssuesSubtype"
                                    type="button"
                                    class="btn btn-sm remove-btn shadow ml-2">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <%TicketIssuesSubtypeDTO ticketIssuesSubtypeDTO = new TicketIssuesSubtypeDTO();%>

                    <template id="template-TicketIssuesSubtype">
                        <tr>
                            <td style="display: none;">
                                <input type='hidden' class='form-control' name='ticketIssuesSubtype.iD'
                                       id='iD_hidden_' value='<%=ticketIssuesSubtypeDTO.iD%>' tag='pb_html'/>

                            </td>

                            <td style="display: none;">
                                <input type='hidden' class='form-control'
                                       name='ticketIssuesSubtype.ticketIssuesId' id='ticketIssuesId_hidden_'
                                       value='<%=ticketIssuesSubtypeDTO.ticketIssuesId%>' tag='pb_html'/>
                            </td>

                            <td>
                                <input required type='text' class='form-control'
                                       name='ticketIssuesSubtype.nameEn' id='nameEn_text_'
                                       value='<%=ticketIssuesSubtypeDTO.nameEn%>' tag='pb_html'/>
                            </td>
                            <td>
                                <input required type='text' class='form-control'
                                       name='ticketIssuesSubtype.nameBn' id='nameBn_text_'
                                       value='<%=ticketIssuesSubtypeDTO.nameBn%>' tag='pb_html'/>
                            </td>
                            
                                   <%
                                if(ticket_issuesDTO.iD == Ticket_issuesDTO.ESERVICE)
                                {
                                	%>
                                <td>
                                	<table>
                                		<tr>
                                		<%
                                		for(CatDTO eserviceDTO: eServiceCats)
                                		{
                                			%>
                                			<td><%=isLangEng?eserviceDTO.nameEn:eserviceDTO.nameBn%></td>
                                			<td>
                                			<input type='checkbox' class='form-control'
                                           	name='es'
                                           	
                                           	value='<%=eserviceDTO.value%>' tag='pb_html'/>
                                			</td>
                                			<%
                                		}
                                		%>
                                		</tr>
                                	</table>
                                	<input  type='hidden' class='form-control'
                                           name='ticketIssuesSubtype.eserviceCats'
                                           id='eserviceCats_text_'
                                           value='<%=ticketIssuesSubtypeDTO.eserviceCats%>' tag='pb_html'/>
                                </td>
                                	<%
                                }
                                %>

                            <td>
                                <div><span><input type='checkbox' name='checkbox' value=''/></span>
                                </div>
                            </td>
                        </tr>

                    </template>
                </div>
                <div class="form-actions text-center mt-5">
                    <a class="btn btn-sm cancel-btn shadow text-white btn-border-radius"
                       href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_CANCEL_BUTTON, loginDTO)%>
                    </a>
                    <button class="btn  btn-sm submit-btn shadow text-white btn-border-radius ml-2"
                            type="button" onclick="submitTicketIssueForm()">
                        <%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const ticketIssueForm = $('#bigform');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';
    var child_table_extra_id = <%=childTableStartingID%>;

    var row = 0;

    function setButtonState(value) {
        cancelBtn.prop("disabled", value);
        submitBtn.prop("disabled", value);
    }

    function submitTicketIssueForm() {
        setButtonState(true);
        
        $( "[name = 'ticketIssuesSubtype.eserviceCats']" ).each(function( index ) {
        	  var parent = $( this ).parent();
        	  var es = "";
        	  parent.find('input[name="es"]').each(function( index ) {
            	  if($(this).prop("checked"))
            	  {
            		  es += $(this).val() + ", ";
            	  }
            });
        	console.log("es = " + es);
        	$(this).val(es);
        	  
        });
		//return;
        let params = ticketIssueForm.serialize().split('&');
        let msg = null;
        for (let i = 0; i < params.length; i++) {
            let param = params[i].split('=');
            switch (param[0]) {
                case 'nameBn':
                    if (!param[1]) {
                        msg = isLangEng ? "Bangla name for ticket issue is missing" : "টিকেট ইস্যুর বাংলা নাম পাওয়া যায়নি";
                    }
                    break;
                case 'nameEn':
                    if (!param[1]) {
                        msg = isLangEng ? "English name for ticket issue is missing" : "টিকেট ইস্যুর ইংরেজীর নাম পাওয়া যায়নি";
                    }
                    break;
                case 'adminOrganogramId':
                    if (!param[1]) {
                        msg = isLangEng ? "Please select assign to" : "অনুগ্রহ করে কর্মকর্তা এসাইন করুন";
                    }
                    break;
                case 'ticketIssuesSubtype.nameBn':
                    if (!param[1]) {
                        msg = isLangEng ? "Bangla name for ticket issue's subtype  is missing" : "টিকেট ইস্যুর সাবটাইপের বাংলা নাম পাওয়া যায়নি";
                    }
                    break;
                case 'ticketIssuesSubtype.nameEn':
                    if (!param[1]) {
                        msg = isLangEng ? "English name for ticket issue's subtype is missing" : "টিকেট ইস্যুর সাবটাইপের ইংরেজীর নাম পাওয়া যায়নি";
                    }
                    break;
            }
            if (msg) {
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(msg, msg);
                setButtonState(false);
                return;
            }
        }
        submitAjaxForm();
    }

    $("#add-more-TicketIssuesSubtype").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-TicketIssuesSubtype");

            $("#field-TicketIssuesSubtype").append(t.html());
            SetCheckBoxValues("field-TicketIssuesSubtype");

            var tr = $("#field-TicketIssuesSubtype").find("tr:last-child");

            tr.attr("id", "TicketIssuesSubtype_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                //console.log(index + ": " + $(this).attr('id'));
            });
            child_table_extra_id++;
        });


    $("#remove-TicketIssuesSubtype").click(function (e) {
        var tablename = 'field-TicketIssuesSubtype';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });

    function getOfficeEmployees(id) {
        var office = $("#" + id).val();
        var rowId = id.split("_")[2];
        var orgId = "assignedOrganogramId_hidden_" + rowId;
        var org = $("#" + orgId).val();
        console.log("id = " + id + " orgId = " + orgId);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById(orgId).innerHTML = this.responseText;
                console.log("html = " + $("#orgId").html());
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        console.log("selected value = " + office);

        xhttp.open("POST", "Ticket_issuesServlet?actionType=getOfficeEmployees&office="
            + office + "&language=<%=Language%>&orgId=" + org, true);
        xhttp.send();
    }


</script>






