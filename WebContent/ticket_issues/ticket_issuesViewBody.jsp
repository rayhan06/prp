<%@page import="pb.CatDAO"%>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="ticket_issues.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    Ticket_issuesDTO ticket_issuesDTO = (Ticket_issuesDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = Language.equalsIgnoreCase("english");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_ADD_FORMNAME, loginDTO)%>
                </h3>
                <div class="ml-auto mr-3">
				    <button type="button" class="btn" id='printer2'
				            onclick="location.href='Ticket_issuesServlet?actionType=search'">
				        <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
            </div>
             
        </div>
       
        <div class="kt-portlet__body form-body">
            <div class=" div_border office-div">
                <h5 class="table-title">
                    <%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_ADD_FORMNAME, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.TICKET_ISSUES_ADD_NAMEEN, loginDTO)%>
                            </b></td>
                            <td>
                                <%=ticket_issuesDTO.nameEn%>
                            </td>

                        </tr>

                        <tr>
                            <td style="width:30%"><b><%=LM.getText(LC.TICKET_ISSUES_ADD_NAMEBN, loginDTO)%>
                            </b></td>
                            <td>
                                <%=ticket_issuesDTO.nameBn%>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="div_border attachement-div">
                <h5 class="table-title mt-5">
                    <%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_SUBTYPE, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <tr>
                            <th><%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_SUBTYPE_NAMEEN, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.TICKET_ISSUES_ADD_TICKET_ISSUES_SUBTYPE_NAMEBN, loginDTO)%>
                            </th>
                            
                             <%
                                if(ticket_issuesDTO.iD == Ticket_issuesDTO.ESERVICE)
                                {
                                	%>
                                <th><%=isLangEng?"E-Service Types":"ইসেবার ধরণ"%>
                                </th>
                                	<%
                                }
                                %>

                        </tr>
                        <%
                            List<TicketIssuesSubtypeDTO> ticketIssuesSubtypeDTOs = TicketIssueSubTypeRepository.getInstance().getByTicketIssuesId(ticket_issuesDTO.iD);
                            for (TicketIssuesSubtypeDTO ticketIssuesSubtypeDTO : ticketIssuesSubtypeDTOs) {
                        %>
                        <tr>
                            <td>
                                <%=ticketIssuesSubtypeDTO.nameEn%>
                            </td>
                            <td>
                                <%=ticketIssuesSubtypeDTO.nameBn%>
                            </td>
                            
                             <%
                                if(ticket_issuesDTO.iD == Ticket_issuesDTO.ESERVICE)
                                {
                                	%>
                               <td>
                               <%
                               int i = 0;
                               for(long es: ticketIssuesSubtypeDTO.eserviceCatsArray)
                               {
                            	   %>
                            	   <%=i > 0?", ":""%>
                            	   <%=CatDAO.getName(Language, "eservice", es) %>
                            	   <%
                            	   i++;
                               }
                               %>
                               </td>
                                	<%
                                }
                                %>
                        </tr>
                        <% } %>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>