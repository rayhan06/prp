<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="login.LoginDTO" %>
<%@ page pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>
<div class="form-group">
    <label><%=(LM.getText(LC.OFFICE_ADMIN_MINISTRY, loginDTO))%>
    </label>
    <select class="form-control" id="ministry_select" onchange="onMinistryChange(this.value)"></select>
</div>
<div class="form-group">
    <label><%=(LM.getText(LC.OFFICE_ADMIN_LAYER, loginDTO))%>
    </label>
    <select class="form-control" id="layer_select" onchange="onLayerChange(this.value)"></select>
</div>
<div class="form-group">
    <label><%=(LM.getText(LC.OFFICE_ADMIN_ORIGIN, loginDTO))%>
    </label>
    <select class="form-control" id="origin_select" onchange="onOriginChange(this.value)"></select>
</div>
<div class="form-group">
    <label><%=(LM.getText(LC.OFFICE_ADMIN_OFFICE, loginDTO))%>
    </label>
    <select class="form-control" id="office_select" onchange="onOfficeChange(this.value)"></select>
</div>
<script>
    let callback;

    function clear(count) {
        if (count >= 3) document.getElementById("office_select").innerHTML = '';
        if (count >= 4) document.getElementById("origin_select").innerHTML = '';
        if (count >= 5) document.getElementById("layer_select").innerHTML = '';
        if (count >= 6) document.getElementById("ministry_select").innerHTML = '';
    }

    function getMinistry() {
        clear(5);
        getGeoData('MinistryToOfficeServlet?actionType=getMinistry', 'ministry_select', 'name_bng');
    }

    function setCallback(_callback) {
        callback = _callback;
    }

    function onMinistryChange(value) {
        clear(4);
        getGeoData('MinistryToOfficeServlet?actionType=getLayer&ministry_id=' + value, 'layer_select', 'layer_name_bng');
    }

    function onLayerChange(value) {
        clear(3);
        getGeoData('MinistryToOfficeServlet?actionType=getOrigin&layer_id=' + value, 'origin_select', 'office_name_bng');
    }

    function onOriginChange(value) {
        clear(2);
        getGeoData('MinistryToOfficeServlet?actionType=getOffice&origin_id=' + value, 'office_select', 'office_name_bng');
    }

    function onOfficeChange(value) {
        clear(1);
        callback(value);
    }

    function getGeoData(servlet_with_parameter, element_name, value_text, callback) {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200 && this.responseText !== '' && this.responseText.length > 0) {
                if (element_name !== null) {
                    const opt = document.getElementById(element_name);
                    if (opt !== null) {
                        opt.innerHTML = ('');
                        const my_language = document.getElementById('my_language').value;
                        opt.options[opt.options.length] = new Option(my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --", '-1');
                        JSON.parse(this.responseText).forEach(function (val) {
                            opt.options[opt.options.length] = new Option(val[value_text], val.id);
                        });
                    }
                } else if (callback !== null) {
                    callback(this.responseText);
                }
            }
        };
        xhttp.open("Get", servlet_with_parameter, true);
        xhttp.send();
    }
</script>