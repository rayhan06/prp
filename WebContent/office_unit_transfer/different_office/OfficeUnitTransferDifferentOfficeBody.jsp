<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
%>

<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="kt-portlet">
    <div id="ajax-content">
        <div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M8.43296491,7.17429118 L9.40782327,7.85689436 C9.49616631,7.91875282 9.56214077,8.00751728 9.5959027,8.10994332 C9.68235021,8.37220548 9.53982427,8.65489052 9.27756211,8.74133803 L5.89079566,9.85769242 C5.84469033,9.87288977 5.79661753,9.8812917 5.74809064,9.88263369 C5.4720538,9.8902674 5.24209339,9.67268366 5.23445968,9.39664682 L5.13610134,5.83998177 C5.13313425,5.73269078 5.16477113,5.62729274 5.22633424,5.53937151 C5.384723,5.31316892 5.69649589,5.25819495 5.92269848,5.4165837 L6.72910242,5.98123382 C8.16546398,4.72182424 10.0239806,4 12,4 C16.418278,4 20,7.581722 20,12 C20,16.418278 16.418278,20 12,20 C7.581722,20 4,16.418278 4,12 L6,12 C6,15.3137085 8.6862915,18 12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C10.6885336,6 9.44767246,6.42282109 8.43296491,7.17429118 Z"
                                      fill="#000000" fill-rule="nonzero"/>
                            </g>
                        </svg>
                        <%--<%=(LM.getText(LC.OFFICE_INFORMATION_EDIT_TITLE, loginDTO2))%>--%>
                        অন্যান্য অফিসে শাখা ট্র্যান্সফার
                    </h3>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <%@include file="../../treeView/dropDownNode.jsp" %>
            </div>
            <div class="row">
                <table class="table table-bordered table-striped">
                    <thead class="thead-light text-center">
                    <tr>
                        <td><input id="select_unit_btn" class="custom-button-active" type="button"
                                   value="শাখা নির্বাচন করুন"
                                   onclick="onSelectUnitClick()"></td>
                        <td><input id="select_organogram_btn" class="custom-button-inactive" type="button"
                                   value="পদবিসমূহ"
                                   onclick="onSelectOrganogramClick()"></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="selected_option_data" colspan="3">
                            <div id="from_unit_to_transfer" style="display: none">
                                <div class="col-md-6 col-sm-12 float-left">
                                    <div id="tree_view"
                                         class="jstree jstree-2 jstree-default jstree-default-large jstree-checkbox-no-clicked jstree-checkbox-selection jstree-leaf"
                                         role="tree" state="0">
                                        <ul class="jstree-container-ul jstree-children"></ul>
                                    </div>
                                </div>
                                <div id="edit_page" class="col-md-6 col-sm-12 float-right">
                                    <h4>যে অফিসে স্থানান্তর করা হবে</h4>
                                    <br>
                                    <jsp:include page="MinistryToUnit.jsp"/>
                                </div>
                            </div>

                            <div id="to_unit_to_transfer" style="display: none">
                                <table class="table table-bordered table-striped" style="width: 100%">
                                    <thead class="thead-light">
                                    <tr>
                                        <td>হস্তান্তরযোগ্য পদবি</td>
                                        <td>হস্তান্তরিত নতুন পদবি</td>
                                    </tr>
                                    </thead>
                                    <tbody id="organogran_list" colspan="3">
                                    </tbody>
                                </table>
                                <br>
                                <div>
                                    <input type="button" class="btn btn-success" onclick="onSubmitStartTransfer()"
                                           value="শাখা স্থানান্তর প্রক্রিয়া শুরু করুন"/>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    let organogram_list = [];

    function onSelectUnitClick() {
        changeButton(1);
        document.getElementById('from_unit_to_transfer').style.display = '';
        document.getElementById('to_unit_to_transfer').style.display = 'none';
    }

    function onSelectOrganogramClick() {
        const office_id = document.getElementById('office_select').value;
        if (office_id == '' || office_id == null || office_id == '-1') {
            showError('অফিস সিলেক্ট করুন', 'Select Office');
            return;
        }
        if (selected_office_unit_list.length == 0) {
            showError('শাখা সিলেক্ট করুন', 'Select Unit');
            return;
        }
        changeButton(0);
        getDesignationDetails();
    }

    let selected_office_unit_list = [];
    let to_unit_list = [];
    let to_organogram_list = [];
    let unit_list_html;

    function onChangeOfficeUnitToTransfer(employee_record_id, office_id, office_unit_id, id) {
        const unitOp = document.getElementById('select_unit_' + id).value;
        let unit_list = document.getElementById('select_organogram_' + id);
        unit_list.options.length = 0;

        //console.log('option adding: ', 'select_organogram_' + id);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    JSON.parse(this.responseText).forEach(function (val) {
                        to_organogram_list.push(val);
                        unit_list.options[unit_list.options.length] = new Option(val.designation_bng, val.organogram_id);
                    });
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("Get", "OfficeUnitTransferDifferentOfficeServlet?actionType=getOrganogram&unit_id=" + unitOp, true);
        xhttp.send();
    }

    function onChangeOrganogramToTransfer() {
    }

    function getDesignationDetails() {
        organogram_list = [];

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '' && this.responseText.length > 0) {
                    let html = '';
                    document.getElementById('from_unit_to_transfer').style.display = 'none';
                    document.getElementById('to_unit_to_transfer').style.display = '';

                    JSON.parse(this.responseText).forEach(function (val) {
                        organogram_list.push(val);
                        console.log('before insert ====> ', val.id);

                        html += '<tr>' +
                            '<td>' +
                            '<div class="d-flex flex-column">\n' +
                            '    <span id="em_name">' + getLanguageBaseText(val.name_bng, val.name_eng) + '</span>\n' +
                            '    <span id="em_current_designation">' + val.designation_bng + '</span>\n' +
                            '    <span id="em_unit_office">' + val.unit_name_bng + '</span>\n' +
                            '    <input id="em_id" type="hidden" value="' + val.employee_record_id + '"/>\n' +
                            '    <input id="office_id" type="hidden" value="' + val.office_id + '"/>\n' +
                            '    <input id="unit_id" type="hidden"value="' + val.office_unit_id + '"/>\n' +
                            '    <input id="organogram_id" type="hidden"value="' + val.id + '"/>\n' +
                            '</div>' +
                            '</td>' +
                            '<td>' +
                            '<div style="width: 250px">\n' +
                            '    <div class="form-group"> ' +
                            '       <select class="form-control"  id="select_unit_' + val.id + '" onchange="onChangeOfficeUnitToTransfer(' + val.employee_record_id + ',' + val.office_id + ',' + val.office_unit_id + ',' + val.id + ')">' +
                            '       </select>' +
                            '   </div>\n' +
                            '   <div class="form-group"> ' +
                            '       <select class="form-control"  id="select_organogram_' + val.id + '" onchange="onChangeOrganogramToTransfer()">' +
                            '       </select>' +
                            '   </div>\n' +
                            '</div>' +
                            '</td>' +
                            '</tr>';
                    });

                    document.getElementById('organogran_list').innerHTML = '';
                    document.getElementById('organogran_list').innerHTML = html;

                    organogram_list.forEach(function (val) {
                        const d = document.getElementById('select_unit_' + val.id);
                        console.log('--> select_unit_' + val.id);

                        const my_language = document.getElementById('my_language').value;

                        d.options.length = 0;
                        d.appendChild(new Option(my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --", '-1'));

                        to_unit_list.forEach(function (val0) {
                            d.appendChild(new Option(val0.unit_name_bng, val0.office_unit_id));
                        });
                    });
                } else {
                    showError('কোন তথ্য পাওয়া যায় নি', "Can't find any information.");
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        let unit_ids = ('');
        selected_office_unit_list.forEach(function (val) {
            unit_ids += '&unit_id=' + val;
        });
        xhttp.open("Get", "OfficeUnitTransferSameOfficeServlet?actionType=getDesignation" + unit_ids, true);
        xhttp.send();
    }

    // ok
    function fillNextElementWithID(id, element) {
        if (element.getAttribute('id') === 'node_div_4') {
            autoExpand = true;
            let office_id = document.getElementById("select_offices") ? document.getElementById("select_offices").value : 0;

            document.getElementById('from_unit_to_transfer').style.display = '';
            document.getElementById('to_unit_to_transfer').style.display = 'none';
            document.getElementById('tree_view').innerHTML = ('<ul class="jstree-container-ul jstree-children"></ul>');

            document.getElementById('tree_view').setAttribute("state", 0);
            getChildren(0, 0, "tree_view", "getOfficeUnitToTransfer", null, office_id, "", "");
        }
    }

    // ok
    function checkbox_toggeled(id, text, cb_id, name, parent_id) {
        let treeName = cb_id.split("_")[0];
        console.log("treeName = " + treeName);
        let res = fillChildren(id, text, cb_id, name, parent_id);
        let ids = res[0];

        for (let i = 0; i < ids.length; i++) {
            if (ids[i] === undefined) continue;
            if (res[1] === true) {
                if (selected_office_unit_list.indexOf(ids[i]) === -1) {
                    selected_office_unit_list.push(ids[i]);
                }
            }
            else {
                if (selected_office_unit_list.indexOf(ids[i]) !== -1) {
                    selected_office_unit_list.splice(selected_office_unit_list.indexOf(ids[i]), 1);
                }
            }
        }
        console.log("ids: " + selected_office_unit_list, ' ', res[1]);
    }

    getMinistry();
    setCallback(function (val) {
        console.log(val);
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    unit_list_html = document.createElement('select');
                    JSON.parse(this.responseText).forEach(function (val) {
                        to_unit_list.push(val);
                        unit_list_html.options[unit_list_html.options.length] = new Option(val.designation_bng, val.organogram_id);
                    });
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("Get", "OfficeUnitTransferDifferentOfficeServlet?actionType=getUnits&office_id=" + val, true);
        xhttp.send();
    });

    function newOriginUnitFound(id, element) {
    }
</script>

<script>
    function changeButton(ok) {
        if (ok) {
            document.getElementById('select_unit_btn').classList.remove("custom-button-inactive");
            document.getElementById('select_organogram_btn').classList.remove("custom-button-active");

            document.getElementById('select_unit_btn').classList.add("custom-button-active");
            document.getElementById('select_organogram_btn').classList.add("custom-button-inactive");
        } else {
            document.getElementById('select_organogram_btn').classList.remove("custom-button-inactive");
            document.getElementById('select_unit_btn').classList.remove("custom-button-active");

            document.getElementById('select_organogram_btn').classList.add("custom-button-active");
            document.getElementById('select_unit_btn').classList.add("custom-button-inactive");
        }
    }
</script>

<script>
    function onSubmitStartTransfer() {
        let employee_record_id = ('');
        let from_unit = ('');
        let from_designation_id = ('');
        let to_unit_id = ('');
        let to_designation_id = ('');
        let to_designation_name = ('');
        const to_office = document.getElementById('office_select').value;
        let to_designation_id_check = [];

        let isOk = true;
        organogram_list.forEach(function (parm) {
            employee_record_id += '&emp=' + (parm.employee_record_id);
            from_designation_id += '&frm_desg=' + (parm.id);
            try {
                const unit = document.getElementById('select_unit_' + parm.id);
                const designation = document.getElementById('select_organogram_' + parm.id);
                to_unit_id += '&to_unit=' + (unit.value);
                to_designation_id += '&to_desg=' + (designation.value);
                to_designation_id_check.push(designation.value);
                to_designation_name += "&to_desg_name=" + (designation.options[designation.selectedIndex].text);
            } catch (e) {
                showToast('প্রত্যেক কর্মকর্তার জন্য উপাধি সিলেক্ট করুন', "Please select designation from every employee.");
                isOk = false;
            }
        });
        if (isOk == false) {
            return;
        }

        let findDuplicates = arr => arr.filter((item, index) => arr.indexOf(item) !== index);
        const dup = findDuplicates(to_designation_id_check);
        if (dup !== null && dup.length > 0) {
            showToast('আপনি একই উপাধি অনেক কর্মকর্তার জন্য সিলেক্ট করেছেন।', "Can't set same designation for different employee.");
            return;
        }

        selected_office_unit_list.forEach(function (parm) {
            from_unit += '&from_unit=' + parm;
        });

        for (let i = 0; i < employee_record_id.length; i++) {
            console.log('data: ', employee_record_id[i], ' ', from_designation_id[i], ' ', to_unit_id[i], ' ', to_designation_id[i]);
        }

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    const res = JSON.parse(this.responseText);
                    if (res.success == true) {
                        showToast('সম্পন্ন হয়েছে', 'Success');
                        location.reload();
                    } else {
                        showToast('ব্যর্থ হয়েছে', 'Failed');
                    }
                    console.log('office unit transfer ', this.responseText);
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Post", "OfficeUnitTransferDifferentOfficeServlet?actionType=transfer" +
            employee_record_id +
            from_unit +
            from_designation_id +
            "&to_office=" + to_office +
            to_unit_id +
            to_designation_id +
            to_designation_name, true);
        xhttp.send();
    }
</script>

<style>
    .custom-button-active {
        background-color: #4CAF50;
        border: none;
        width: 100%;
        color: white;
        padding: 10px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
    }

    .custom-button-inactive {
        background-color: #eff3ef;
        border: none;
        width: 100%;
        color: #0c0c0c;
        padding: 10px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
    }
</style>