<%@ page import="language.LM" %>
<%@ page import="language.LC" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.CommonConstant" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String servletType = request.getParameter("servletType");
    String pageTitle = request.getParameter("pageTitle");

    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO006 = UserRepository.getInstance().getUserDtoByUserId(loginDTO2.userID);

    int my_language2 = LM.getLanguageIDByUserDTO(userDTO006) == CommonConstant.Language_ID_English ? 2 : 1;
%>
<input type='hidden' id='servletType' value='<%=servletType%>'/>
<div class="kt-portlet">
    <div id="ajax-content">
        <div class="kt-portlet kt-portlet--responsive-tablet-and-mobile py-3">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                             height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M8.43296491,7.17429118 L9.40782327,7.85689436 C9.49616631,7.91875282 9.56214077,8.00751728 9.5959027,8.10994332 C9.68235021,8.37220548 9.53982427,8.65489052 9.27756211,8.74133803 L5.89079566,9.85769242 C5.84469033,9.87288977 5.79661753,9.8812917 5.74809064,9.88263369 C5.4720538,9.8902674 5.24209339,9.67268366 5.23445968,9.39664682 L5.13610134,5.83998177 C5.13313425,5.73269078 5.16477113,5.62729274 5.22633424,5.53937151 C5.384723,5.31316892 5.69649589,5.25819495 5.92269848,5.4165837 L6.72910242,5.98123382 C8.16546398,4.72182424 10.0239806,4 12,4 C16.418278,4 20,7.581722 20,12 C20,16.418278 16.418278,20 12,20 C7.581722,20 4,16.418278 4,12 L6,12 C6,15.3137085 8.6862915,18 12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C10.6885336,6 9.44767246,6.42282109 8.43296491,7.17429118 Z"
                                      fill="#000000" fill-rule="nonzero"/>
                            </g>
                        </svg>
                        <%=(LM.getText(LC.OFFICE_UNIT_MANAGEMENT_TITLE, loginDTO2))%>
                    </h3>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <%@include file="../../treeView/dropDownNode.jsp" %>
            </div>
            <div class="row">
                <table class="table table-bordered table-striped">
                    <thead class="thead-light text-center">
                    <tr>
                        <td><input id="select_unit_btn" class="custom-button-active" type="button"
                                   value="শাখা নির্বাচন করুন"
                                   onclick="onViewSelectUnit()"></td>
                        <td><input id="select_organogram_btn" class="custom-button-inactive" type="button"
                                   value="পদবিসমূহ"
                                   onclick="onViewOrganogramClick()">
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td id="main_view" colspan="3">
                            <div id="select_unit_list" class="row" style="display: none">
                                <div class="col-md-6 col-sm-12">
                                    <table>
                                        <thead>
                                        <tr>
                                            <td>শাখা সমূহ</td>
                                        </tr>
                                        </thead>
                                        <tbody id="unit_list_row">
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <div class="float-right">
                                        <label for="drop_down_unit_where_transfer">যে শাখায় স্থানান্তর করা
                                            হবে</label><select
                                            id="drop_down_unit_where_transfer" style="width: 200px"
                                            onchange="onSelectWhichUnitToTransfer(this.value)">
                                    </select>
                                    </div>
                                </div>
                            </div>

                            <div id="select_organogram_list" style="display: none">
                                <table class="table table-bordered table-striped" style="width: 100%">
                                    <thead class="thead-light text-center">
                                    <tr>
                                        <td>#</td>
                                        <td>ব্যাবহারকারীর নাম</td>
                                        <td>পদের নাম</td>
                                        <td>শাখার নাম</td>
                                    </tr>
                                    </thead>
                                    <tbody id="organogran_list" colspan="3">
                                    </tbody>
                                </table>
                                <br>
                                <div>
                                    <input type="button" class="btn btn-primary btn-hover-brand btn-square"
                                           value="শাখা স্থানান্তর প্রক্রিয়া শুরু করুন"
                                           onclick="onStartTransfer()"/>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    let unit_list = [];
    let organogram_list = [];
    let selected_units_from_transfer = [];
    let unit_to_transfer;

    // ok
    function onViewSelectUnit() {
        changeButton(1);
        document.getElementById('select_unit_list').style.display = '';
        document.getElementById('select_organogram_list').style.display = 'none';
    }

    function onViewOrganogramClick() {
        if (selected_units_from_transfer != null && selected_units_from_transfer.length > 0 && unit_to_transfer != null) {
            let isOk = 1;
            selected_units_from_transfer.forEach(function (val) {
                if (val == unit_to_transfer) {
                    isOk = 0;
                    showToast('আপনি একই অফিস সিলেক্ট করেছেন', 'You select same office');
                    document.getElementById("drop_down_unit_where_transfer").selectedIndex = 0;
                }
            });
            if (isOk === 0) return;
            changeButton(0);
            getDesignationDetails();
        } else {
            showToast('অনুগ্রহ করে শাখা সিলেক্ট করুন', 'Please select unit first');
        }
    }

    function onStartTransfer() {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    //const res = JSON.parse(this.responseText);
                    if (this.responseText == 'true') {
                        showToast('সম্পন্ন হয়েছে', 'Success');
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    } else {
                        showToast('ব্যর্থ হয়েছে', 'Failed');
                    }
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        let units = '';
        selected_units_from_transfer.forEach(function (val) {
            units += '&from=' + val;
        });
        xhttp.open("Post", "OfficeUnitTransferSameOfficeServlet?actionType=transfer" + units + '&to=' + unit_to_transfer, true);
        xhttp.send();
    }

    function getOfficeUnits() {
        unit_list = [];

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    let html = '';
                    document.getElementById('select_unit_list').style.display = '';
                    document.getElementById('select_organogram_list').style.display = 'none';

                    JSON.parse(this.responseText).forEach(function (val) {
                        unit_list.push(val);
                        html += '<tr><td><label class="kt-checkbox kt-checkbox--brand"><input type="checkbox" onchange="onSelectUnitFromTransfer(' + val.id + ')"><span></span></label> <input id="unit_id_' + val.id + "\"" + 'type="hidden" value="' + val.id + '"> <label>' + val.unit_name_bng + '</label></td></tr>';
                    });

                    document.getElementById('unit_list_row').innerHTML = '';
                    document.getElementById('unit_list_row').innerHTML += html;

                    const d = document.getElementById("drop_down_unit_where_transfer");
                    d.options.length = 0;
                    const my_language = document.getElementById('my_language').value;
                    d.appendChild(new Option(my_language == '2' ? "-- Please select one --" : "-- বাছাই করুন --", '-1'));

                    unit_list.forEach(function (val) {
                        d.appendChild(new Option(val.unit_name_bng, val.id));
                    });
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        let office_id = document.getElementById('select_offices').value;
        xhttp.open("Get", "OfficeUnitTransferSameOfficeServlet?actionType=getUnits&office_id=" + office_id, true);
        xhttp.send();
    }

    function getDesignationDetails() {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (this.responseText !== '') {
                    let html = '', id = 1;
                    JSON.parse(this.responseText).forEach(function (val) {
                        organogram_list.push(val);
                        html += '<tr>' +
                            '<td><label>' + (id++) + '</label></td>' +
                            '<td><input id="office_id_' + val.id + "\"" + 'type="hidden" value="' + val.id + '"> <label>' +
                            (val.name_bng == undefined ? getLanguageBaseText("পাওয়া যায় নি", "Not found") : val.name_bng) +
                            '</label></td>' +
                            '<td><label>' + val.designation_bng + '</label></td>' +
                            '<td> <label>' + val.unit_name_bng + '</label></td>' +
                            '</tr>';
                    });

                    document.getElementById('organogran_list').innerHTML = html;
                    document.getElementById('select_unit_list').style.display = 'none';
                    document.getElementById('select_organogram_list').style.display = '';
                } else {
                    showToast(no_data_found_bng, no_data_found_eng);
                }
            }
            else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        let unit_ids = '';
        selected_units_from_transfer.forEach(function (val) {
            unit_ids += '&unit_id=' + val;
        });
        xhttp.open("Get", "OfficeUnitTransferSameOfficeServlet?actionType=getDesignation" + unit_ids, true);
        xhttp.send();
    }

    function fillNextElementWithID(id, element) {
        if (element.getAttribute('id') === 'node_div_4') {
            getOfficeUnits();
        }
    }

</script>

<script>

    function onSelectWhichUnitToTransfer(value) {
        if (selected_units_from_transfer !== null && selected_units_from_transfer.length > 0) {
            selected_units_from_transfer.forEach(function (val) {
                if (val == value) {
                    showToast('আপনি একই অফিস সিলেক্ট করেছেন', 'You select same office');
                    document.getElementById("drop_down_unit_where_transfer").selectedIndex = 0;
                }
            });
            unit_to_transfer = value;
        } else {
            showToast('অনুগ্রহ করে শাখা সিলেক্ট করুন', 'Please select unit first');
            document.getElementById("drop_down_unit_where_transfer").selectedIndex = 0;
        }
        console.log(value);
    }

    function onSelectUnitFromTransfer(unit_id) {
        if (selected_units_from_transfer.indexOf(unit_id) > -1) {
            selected_units_from_transfer.splice(selected_units_from_transfer.indexOf(unit_id), 1);
        } else {
            selected_units_from_transfer.push(unit_id);
        }
        selected_units_from_transfer.forEach(function (val) {
            if (val == unit_to_transfer) {
                showToast('আপনি একই অফিস সিলেক্ট করেছেন', 'You select same office');
                document.getElementById("drop_down_unit_where_transfer").selectedIndex = 0;
            }
        });
    }
</script>

<script>
    function changeButton(ok) {
        if (ok) {
            document.getElementById('select_unit_btn').classList.remove("custom-button-inactive");
            document.getElementById('select_organogram_btn').classList.remove("custom-button-active");

            document.getElementById('select_unit_btn').classList.add("custom-button-active");
            document.getElementById('select_organogram_btn').classList.add("custom-button-inactive");
        } else {
            document.getElementById('select_organogram_btn').classList.remove("custom-button-inactive");
            document.getElementById('select_unit_btn').classList.remove("custom-button-active");

            document.getElementById('select_organogram_btn').classList.add("custom-button-active");
            document.getElementById('select_unit_btn').classList.add("custom-button-inactive");
        }
    }
</script>

<style>
    .custom-button-active {
        background-color: #4CAF50;
        border: none;
        width: 100%;
        color: white;
        padding: 10px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        box-shadow: 0 16px 26px -10px rgba(76, 175, 80, 0.56), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(76, 175, 80, 0.2);
    }

    .custom-button-inactive {
        background-color: #eff3ef;
        border: none;
        width: 100%;
        color: #0c0c0c;
        padding: 10px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
    }
</style>