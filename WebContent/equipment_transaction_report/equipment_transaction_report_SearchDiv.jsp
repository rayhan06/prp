<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EQUIPMENT_TRANSACTION_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.TRANSACTION_REPORT_SELECT_TRANSACTIONTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='transactionType' id = 'transactionType' >		
						<option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
						<option value="0"><%=Language.equalsIgnoreCase("english")?"Stock In":"ক্রয়"%></option>
						<option value="3"><%=Language.equalsIgnoreCase("english")?"Return":"ফেরত"%></option>
						<option value="8"><%=Language.equalsIgnoreCase("english")?"Utilize":"ব্যবহার করা"%></option>
						<option value="9"><%=Language.equalsIgnoreCase("english")?"Borrow":"ধার নেওয়া"%></option>
					</select>
				</div>
			</div>
		</div>
	
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_TYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='medicalEquipmentCat' id = 'medicalEquipmentCat' onchange = 'fillEquipemnts(this.id)'>		
						<%
                        Options = CatDAO.getOptions(Language, "medical_equipment", CatDTO.CATDEFAULT);
                        out.print(Options);
                    %>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.TRANSACTION_REPORT_SELECT_ITEM, loginDTO)%>
				</label>
				<div class="col-sm-9">

					
					<select class='form-control'  name='medicalEquipmentNameId' id = 'medicalEquipmentNameId'>		
						<option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
					</select>						
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_SELECT_EMPLOYEE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<button type="button" class="btn submit-btn text-white shadow btn-border-radius btn-block"
                        onclick="addEmployee()"
                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                </button>
                <table class="table table-bordered table-striped">
                    <tbody id="employeeToSet"></tbody>
                </table>
                
                <input class='form-control' type='hidden' name='userName' id='userName' value=''/>						
				</div>
			</div>
		</div>
    </div>
    
    <div class="search-criteria-div col-md-12">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
            </label>
            <div class="col-md-9">              
                <input class='form-control' type='text' name='userNameRaw' id='userNameRaw' value='' onKeyUp="setEngUserName(this.value, 'userName')"/>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function init()
{
	dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = false;
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}
function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#userName").val(userName);
    $("#userNameRaw").val(userName);
}


function fillEquipemnts(id)
{

	var eqNameId = "medicalEquipmentNameId";
	var medicalDeptCat = -1;
	var medicalEquipmentCat = $("#" + id).val();
	
	console.log("medicalDeptCat = " + medicalDeptCat + " medicalEquipmentCat = " + medicalEquipmentCat )
	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			$("#" + eqNameId).html(this.responseText);
		} else if (this.readyState == 4 && this.status != 200) {
			alert('failed ' + this.status);
		}
	};

	xhttp.open("GET",  "Medical_equipment_nameServlet?actionType=getEquipments&medicalDeptCat=" + medicalDeptCat + "&medicalEquipmentCat=" + medicalEquipmentCat, true);
	xhttp.send();
}
</script>