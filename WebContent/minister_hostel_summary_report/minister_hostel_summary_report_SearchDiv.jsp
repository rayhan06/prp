<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@page import="java.util.Calendar" %>
<%@page import="pbReport.DateUtils" %>

<%@ page import="pb.*" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2 mx-md-0">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELBLOCKTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select onchange="loadSideOptions('')" class='form-control'  name='amMinisterHostelBlockId' id = 'amMinisterHostelBlockId_select_<%=i%>'   tag='pb_html'>
						<%--																<%--%>
						<%--																	Options = CommonDAO.getOptions(Language, "am_minister_hostel_block", am_minister_hostel_levelDTO.amMinisterHostelBlockId);--%>
						<%--																%>--%>
						<%--																<%=Options%>--%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_MINISTER_HOSTEL_UNIT_ADD_AMMINISTERHOSTELSIDEID, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select onchange="loadUnitOptions('')" class='form-control'  name='amMinisterHostelSideId' id = 'amMinisterHostelSideId_select_<%=i%>'   tag='pb_html'>

					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELUNITID, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='amMinisterHostelUnitId' id = 'amMinisterHostelUnitId_select_<%=i%>'   tag='pb_html'>

					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_MINISTER_HOSTEL_LEVEL_ADD_AMMINISTERHOSTELLEVELCAT, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='amMinisterHostelLevelCat' id = 'amMinisterHostelLevelCat_category_<%=i%>'   tag='pb_html'>
						<%
							Options = CatRepository.getInstance().buildOptions("am_minister_hostel_level", Language, -1);
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">

			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right"><%=LM.getText(LC.PI_REQUISITION_ADD_STATUS, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control' name='status'
							id='status'
							tag='pb_html'>
						<%=CommonApprovalStatus.buildOptionForAsset(Language)%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-md-right">
					<%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_EMPLOYEE_WISE_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">



	function processBlockResponse(data){
		document.getElementById('amMinisterHostelBlockId_select_0').innerHTML = data;
		loadSideOptions('');
	}

	function processSideResponse(data){
		document.getElementById('amMinisterHostelSideId_select_0').innerHTML = data;
		loadUnitOptions('');
	}

	function processUnitResponse(data){
		document.getElementById('amMinisterHostelUnitId_select_0').innerHTML = data;
	}

	function loadUnitOptions(id) {
		var blockId = document.getElementById('amMinisterHostelSideId_select_0').value;
		if (blockId && blockId != undefined && blockId.toString().trim().length > 0 && parseInt(blockId.toString()) > 0) {
			var unitOptionsGetUrl = 'Am_minister_hostel_unitServlet?actionType=getUnitOptionsBySideId&sideId=' + blockId;
			unitOptionsGetUrl += id;
			ajaxGet(unitOptionsGetUrl, processUnitResponse, processUnitResponse);
		}
	}

	function loadSideOptions(id) {
		var blockId = document.getElementById('amMinisterHostelBlockId_select_0').value;
		if (blockId && blockId != undefined && blockId.toString().trim().length > 0 && parseInt(blockId.toString()) > 0) {
			var sideOptionsGetUrl = 'Am_minister_hostel_blockServlet?actionType=getSideOptionsByBlockId&blockId=' + blockId;
			sideOptionsGetUrl += id;
			ajaxGet(sideOptionsGetUrl, processSideResponse, processSideResponse);
		}
	}

	function loadBlockOptions(id) {
		var blockOptionsGetUrl = 'Am_minister_hostel_blockServlet?actionType=getBlockOptions';
		blockOptionsGetUrl += id;
		ajaxGet(blockOptionsGetUrl, processBlockResponse, processBlockResponse);
	}


	function ajaxGet(url, onSuccess, onError) {
		$.ajax({
			type: "GET",
			url: getContextPath()+url,
			dataType: "json",
			success: onSuccess,
			error: onError,
			complete: function () {
				// $.unblockUI();
			}
		});
	}


$(document).ready(() => {
	showFooter = false;

	select2SingleSelector('#amMinisterHostelBlockId_select_0', '<%=Language%>');
	select2SingleSelector('#amMinisterHostelSideId_select_0', '<%=Language%>');
	select2SingleSelector('#amMinisterHostelUnitId_select_0', '<%=Language%>');
	select2SingleSelector('#amMinisterHostelLevelCat_category_0', '<%=Language%>');
	select2SingleSelector('#status', '<%=Language%>');
});
function init()
{
    dateTimeInit($("#Language").val());

	loadBlockOptions('');

}
function PreprocessBeforeSubmiting()
{
}

</script>