<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="disciplinary_action.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.DISCIPLINARY_ACTION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Disciplinary_actionDAO disciplinary_actionDAO = new Disciplinary_actionDAO("disciplinary_action");
    Disciplinary_actionDTO disciplinary_actionDTO = (Disciplinary_actionDTO) disciplinary_actionDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
    <div class="modal-header">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <h5 class="modal-title"><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARY_ACTION_ADD_FORMNAME, loginDTO)%>
                    </h5>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="modal-body container">

        <div class="row div_border office-div">

            <div class="col-md-12">
                <h3><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARY_ACTION_ADD_FORMNAME, loginDTO)%>
                </h3>
                <table class="table table-bordered table-striped">


                    <tr>
                        <td style="width:30%">
                            <b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARYLOGINCIDENTNUMBER, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = disciplinary_actionDTO.disciplinaryLogIncidentNumber + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_WORKSDONE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = disciplinary_actionDTO.worksDone + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_FINDINGS, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = disciplinary_actionDTO.findings + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_PREVENTIONADVICE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = disciplinary_actionDTO.preventionAdvice + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_FILESDROPZONE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = disciplinary_actionDTO.filesDropzone + "";
                            %>
                            <%
                                {
                                    List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_actionDTO.filesDropzone);
                            %>
                            <table>
                                <tr>
                                    <%
                                        if (FilesDTOList != null) {
                                            for (int j = 0; j < FilesDTOList.size(); j++) {
                                                FilesDTO filesDTO = FilesDTOList.get(j);
                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                    %>
                                    <td>
                                        <%
                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                        %>
                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                             style='width:100px'/>
                                        <%
                                            }
                                        %>
                                        <a href='Disciplinary_actionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                           download><%=filesDTO.fileTitle%>
                                        </a>
                                    </td>
                                    <%
                                            }
                                        }
                                    %>
                                </tr>
                            </table>
                            <%
                                }
                            %>


                        </td>

                    </tr>


                </table>
            </div>


        </div>


    </div>
</div>