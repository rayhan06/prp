<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="disciplinary_action.Disciplinary_actionDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>

<%
Disciplinary_actionDTO disciplinary_actionDTO = (Disciplinary_actionDTO)request.getAttribute("disciplinary_actionDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(disciplinary_actionDTO == null)
{
	disciplinary_actionDTO = new Disciplinary_actionDTO();
	
}
System.out.println("disciplinary_actionDTO = " + disciplinary_actionDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.DISCIPLINARY_ACTION_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=disciplinary_actionDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_disciplinaryLogIncidentNumber'>")%>
			

		<input type='hidden' class='form-control'  name='disciplinaryLogIncidentNumber' id = 'disciplinaryLogIncidentNumber_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + disciplinary_actionDTO.disciplinaryLogIncidentNumber + "'"):("'" + "0,1000000" + "'")%>
 tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_worksDone'>")%>
			
	
	<div class="form-inline" id = 'worksDone_div_<%=i%>'>
		<textarea class='form-control'  name='worksDone' id = 'worksDone_textarea_<%=i%>'   tag='pb_html'><%=actionName.equals("edit")?(disciplinary_actionDTO.worksDone):("")%>
</textarea>		
											
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_findings'>")%>
			
	
	<div class="form-inline" id = 'findings_div_<%=i%>'>
		<textarea class='form-control'  name='findings' id = 'findings_textarea_<%=i%>'   tag='pb_html'><%=actionName.equals("edit")?(disciplinary_actionDTO.findings):("")%>
</textarea>		
											
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_preventionAdvice'>")%>
			
	
	<div class="form-inline" id = 'preventionAdvice_div_<%=i%>'>
		<textarea class='form-control'  name='preventionAdvice' id = 'preventionAdvice_textarea_<%=i%>'   tag='pb_html'><%=actionName.equals("edit")?(disciplinary_actionDTO.preventionAdvice):("")%>
</textarea>		
											
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_filesDropzone'>")%>
			
	
	<div class="form-inline" id = 'filesDropzone_div_<%=i%>'>
		<%
		if(actionName.equals("edit"))
		{
			List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_actionDTO.filesDropzone);
			%>			
			<table>
				<tr>
			<%
			if(filesDropzoneDTOList != null)
			{
				for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
				{
					FilesDTO filesDTO = filesDropzoneDTOList.get(j);
					byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
					%>
					<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
					{
						%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64) 
						%>' style='width:100px' />
						<%
					}
					%>
					<a href = 'Disciplinary_actionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>		
					</td>
					<%
				}
			}
			%>
			</tr>
			</table>
			<%
		}
		%>
		
		<%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>			
		<div class="dropzone" action="Disciplinary_actionServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?disciplinary_actionDTO.filesDropzone:ColumnID%>">
			<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?disciplinary_actionDTO.filesDropzone:ColumnID%>'/>		


	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=disciplinary_actionDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=disciplinary_actionDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=disciplinary_actionDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + disciplinary_actionDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=disciplinary_actionDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Disciplinary_actionServlet?actionType=view&ID=<%=disciplinary_actionDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Disciplinary_actionServlet?actionType=view&modal=1&ID=<%=disciplinary_actionDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	