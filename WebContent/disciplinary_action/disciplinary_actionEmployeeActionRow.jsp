<%@ page import="complain_action.Complain_actionRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@page pageEncoding="UTF-8" %>

<form id="disciplinary_detail_form_<%=disciplinary_detailsDTO.employeeRecordsId%>" name="bigform"
      enctype="multipart/form-data">
    <input type='hidden' class='form-control' name='iD'
           id='iD_hidden_<%=i%>'
           value='<%=disciplinary_detailsDTO.iD%>'/>

    <input type='hidden' class='form-control' name='employeeRecordsId'
           id='employeeRecordsId_hidden_<%=i%>'
           value='<%=disciplinary_detailsDTO.employeeRecordsId%>'/>

    <input type='hidden' class='form-control' name='officeUnitId'
           id='officeUnitId_hidden_<%=i%>'
           value='<%=disciplinary_detailsDTO.officeUnitId%>'/>

    <input type='hidden' class='form-control' name='organogramId'
           id='organogramId_hidden_<%=i%>'
           value='<%=disciplinary_detailsDTO.organogramId%>' tag='pb_html'/>

    <div class="row">
        <div class="col-md-3 form-group">
            <label for="employeeName">
                <b>
                    <%=LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_EMPLOYEE_NAME, loginDTO)%>
                </b>
            </label>
            <input type="text" class="form-control" id="employeeName" readonly

                   value="<%=isLanguageEnglish ? disciplinary_detailsDTO.employeeNameEn:disciplinary_detailsDTO.employeeNameBn%>">
        </div>

        <div class="col-md-3 form-group">
            <label for="employeeNumber">
                <b>
                    <%=LM.getText(LC.ENTRY_PAGE_ADD_ORGANOGRAMID, loginDTO)%>
                </b>
            </label>
            <br>
            <input type="text" class="form-control disabled" id="employeeNumber" readonly

                   value="<%=Employee_recordsRepository.getInstance().getById(disciplinary_detailsDTO.employeeRecordsId).employeeNumber%>">
        </div>

        <div class="col-md-3 form-group">
            <label for="office"><b><%=LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_OFFICE, loginDTO)%>
            </b></label>
            <input type="text" class="form-control disabled" id="office" readonly

                   value="<%=isLanguageEnglish ? disciplinary_detailsDTO.officeNameEn:disciplinary_detailsDTO.officeNameBn%>">
        </div>

        <div class="col-md-3 form-group">
            <label for="designation"><b><%=LM.getText(LC.OFFICE_AND_EMPLOYEE_MANAGEMENT_DESIGNATION_NAME, loginDTO)%>
            </b></label>
            <input type="text" class="form-control disabled" id="designation" readonly

                   value="<%=isLanguageEnglish ? disciplinary_detailsDTO.organogramNameEn:disciplinary_detailsDTO.organogramNameBn%>">
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 form-group">
            <label for='complainActionType_select_<%=disciplinary_detailsDTO.employeeRecordsId%>'><b>
                <%=LM.getText(LC.DISCIPLINARY_DETAILS_ADD_COMPLAINACTIONTYPE, loginDTO)%>
            </b>
            </label>
            <select class='form-control' name='complainActionType'
                    id='complainActionType_select_<%=disciplinary_detailsDTO.employeeRecordsId%>'
                    tag='pb_html'>
                <%=Complain_actionRepository.getInstance().buildOptions(Language, disciplinary_detailsDTO.complainActionType)%>
            </select>
        </div>
        <div class="col-md-6 form-group"
             id='complianOther_div_<%=disciplinary_detailsDTO.employeeRecordsId%>'>
            <label for='complianOther<%=disciplinary_detailsDTO.employeeRecordsId%>'><b>
                <%=LM.getText(LC.DISCIPLINARY_DETAILS_ADD_COMPLIANOTHER, loginDTO)%>
            </b>
            </label>
            <input type="text" class="form-control" name='complianOther'
                   id='complianOther_<%=disciplinary_detailsDTO.employeeRecordsId%>'
                   value='<%=disciplinary_detailsDTO.complianOther == null ? "":disciplinary_detailsDTO.complianOther%>'>
        </div>
    </div>

    <div class="row" id="from_to_date_div_<%=disciplinary_detailsDTO.employeeRecordsId%>">
        <div class="col-md-6 form-group">
            <label class="control-label"
                   for='from_date_<%=disciplinary_detailsDTO.employeeRecordsId%>'><b>
                <%=LM.getText(LC.EMPLOYEE_OFFICE_HOUR_EDIT_STARTDATE, loginDTO)%>
            </b></label>
            <div id='from_date_div_<%=disciplinary_detailsDTO.employeeRecordsId%>'>
                <%
                    String dateIdFrom = "from_date_js_" + disciplinary_detailsDTO.employeeRecordsId;
                %>

                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="<%=dateIdFrom%>"/>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                </jsp:include>

                <input type='hidden' class='rounded form-control formRequired'
                       id='from_date_<%=disciplinary_detailsDTO.employeeRecordsId%>'
                       name='fromDate' value='' tag='pb_html'/>
            </div>
        </div>

        <div class="col-md-6 form-group">
            <label class="control-label"
                   for='to_date_<%=disciplinary_detailsDTO.employeeRecordsId%>'>
                <b><%=LM.getText(LC.TRAINING_CALENDER_EDIT_ENDDATE, loginDTO)%>
                </b></label>
            <div id='to_date_div_<%=disciplinary_detailsDTO.employeeRecordsId%>'>
                <%
                    String dateIdTo = "to_date_js_" + disciplinary_detailsDTO.employeeRecordsId;
                %>
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="<%=dateIdTo%>"/>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                </jsp:include>

                <input type='hidden' class='rounded form-control formRequired'
                       id='to_date_<%=disciplinary_detailsDTO.employeeRecordsId%>'
                       name='toDate' value='' tag='pb_html'/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <label for='remarks_<%=disciplinary_detailsDTO.employeeRecordsId%>'><b>
                <%=LM.getText(LC.DISCIPLINARY_DETAILS_ADD_REMARKS, loginDTO)%>
            </b>
            </label>
            <input type="text" class="form-control" name='remarks'
                   id='remarks_<%=disciplinary_detailsDTO.employeeRecordsId%>'
                   value='<%=disciplinary_detailsDTO.remarks == null ? "":disciplinary_detailsDTO.remarks%>'>
        </div>
    </div>
</form>









