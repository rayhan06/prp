<%@page pageEncoding="UTF-8" %>

<%@page import="disciplinary_action.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.DISCIPLINARY_ACTION_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_DISCIPLINARY_ACTION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Disciplinary_actionDTO disciplinary_actionDTO = (Disciplinary_actionDTO) request.getAttribute("disciplinary_actionDTO");
    CommonDTO commonDTO = disciplinary_actionDTO;
    String servletName = "Disciplinary_actionServlet";


    System.out.println("disciplinary_actionDTO = " + disciplinary_actionDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Disciplinary_actionDAO disciplinary_actionDAO = (Disciplinary_actionDAO) request.getAttribute("disciplinary_actionDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_disciplinaryLogIncidentNumber'>
    <%
        value = disciplinary_actionDTO.disciplinaryLogIncidentNumber + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_worksDone'>
    <%
        value = disciplinary_actionDTO.worksDone + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_findings'>
    <%
        value = disciplinary_actionDTO.findings + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_preventionAdvice'>
    <%
        value = disciplinary_actionDTO.preventionAdvice + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_filesDropzone'>
    <%
        value = disciplinary_actionDTO.filesDropzone + "";
    %>
    <%
        {
            List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_actionDTO.filesDropzone);
    %>
    <table>
        <tr>
            <%
                if (FilesDTOList != null) {
                    for (int j = 0; j < FilesDTOList.size(); j++) {
                        FilesDTO filesDTO = FilesDTOList.get(j);
                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
            %>
            <td>
                <%
                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                %>
                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                <%
                    }
                %>
                <a href='Disciplinary_actionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                   download><%=filesDTO.fileTitle%>
                </a>
            </td>
            <%
                    }
                }
            %>
        </tr>
    </table>
    <%
        }
    %>


</td>


<td>
    <a href='Disciplinary_actionServlet?actionType=view&ID=<%=disciplinary_actionDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
    </a>

</td>

<td id='<%=i%>_Edit'>

    <a href='Disciplinary_actionServlet?actionType=getEditPage&ID=<%=disciplinary_actionDTO.iD%>'><%=LM.getText(LC.DISCIPLINARY_ACTION_SEARCH_DISCIPLINARY_ACTION_EDIT_BUTTON, loginDTO)%>
    </a>

</td>


<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=disciplinary_actionDTO.iD%>'/></span>
    </div>
</td>
																						
											

