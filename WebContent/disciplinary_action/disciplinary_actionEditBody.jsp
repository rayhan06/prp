<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="disciplinary_action.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="employee_offices.EmployeeModel" %>
<%@ page import="disciplinary_details.Disciplinary_detailsDTO" %>

<%
    Disciplinary_actionDTO disciplinary_actionDTO;
    disciplinary_actionDTO = (Disciplinary_actionDTO) request.getAttribute("disciplinary_actionDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    if (disciplinary_actionDTO == null) {
        disciplinary_actionDTO = new Disciplinary_actionDTO();

    }
    System.out.println("disciplinary_actionDTO = " + disciplinary_actionDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage") || request.getParameter("actionType").equalsIgnoreCase("add")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARY_ACTION_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;

    String Language = LM.getText(LC.DISCIPLINARY_ACTION_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    List<Disciplinary_detailsDTO> disciplinary_detailsDTOList = (List<Disciplinary_detailsDTO>) request.getAttribute("disciplinary_detailsDTOList");

    String incidentNumber = request.getParameter("incidentNumber");

    Object complaintStatusCatObj = request.getAttribute("complaintStatusCat");
    int complaintStatusCat = complaintStatusCatObj == null ? 0 : (int) complaintStatusCatObj;
    List<Integer> complaintStatusFilter = new ArrayList<>();
    complaintStatusFilter.add(1);
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" name="disciplinaryActionform" id="disciplinaryActionform"
              action="Disciplinary_actionServlet?actionType=ajax_<%=actionName%>"
              enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                        <%--begin:: Input fields--%>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.DISCIPLINARY_LOG_ADD_COMPLAINTSTATUSCAT, userDTO)%>
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-9">
                                                <select class='form-control' name='complaintStatusCat'
                                                        id='complaintStatusCat' tag='pb_html'>
                                                    <%=CatRepository.getInstance().buildOptionsWithFiler("complaint_status", Language, complaintStatusCat, complaintStatusFilter)%>
                                                </select>
                                            </div>
                                        </div>
                                        <input type='hidden' class='form-control' name='iD'
                                               id='iD_hidden_<%=i%>'
                                               value='<%=disciplinary_actionDTO.iD%>' tag='pb_html'/>
                                        <input type='hidden' class='form-control'
                                               name='disciplinaryLogIncidentNumber'
                                               id='disciplinaryLogIncidentNumber_hidden_<%=i%>'
                                               tag='pb_html'
                                               value='<%=actionName.equals("edit") ? disciplinary_actionDTO.disciplinaryLogIncidentNumber : incidentNumber%>'/>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_WORKSDONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                            <textarea class='form-control' name='worksDone'
                                                                      id='worksDone_textarea_<%=i%>' value=""
                                                                      tag='pb_html'><%=actionName.equals("edit") ? (disciplinary_actionDTO.worksDone) : ("")%></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_FINDINGS, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                            <textarea class='form-control' name='findings'
                                                                      id='findings_textarea_<%=i%>' value=""
                                                                      tag='pb_html'><%=actionName.equals("edit") ? (disciplinary_actionDTO.findings) : ("")%></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_PREVENTIONADVICE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                            <textarea class='form-control' name='preventionAdvice'
                                                                      id='preventionAdvice_textarea_<%=i%>'
                                                                      tag='pb_html'><%=actionName.equals("edit") ? (disciplinary_actionDTO.preventionAdvice) : ("")%></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_FILESDROPZONE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9" id='filesDropzone_div_<%=i%>'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(disciplinary_actionDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (filesDropzoneDTOList != null) {
                                                                for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                 style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Disciplinary_actionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                            <a class='btn btn-danger'
                                                               onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>

                                                <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                                <div class="dropzone"
                                                     action="Disciplinary_actionServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?disciplinary_actionDTO.filesDropzone:ColumnID%>">
                                                    <input type='file' style="display:none"
                                                           name='filesDropzoneFile'
                                                           id='filesDropzone_dropzone_File_<%=i%>'
                                                           tag='pb_html'/>
                                                </div>

                                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                                       id='filesDropzoneFilesToDelete_<%=i%>'
                                                       value='' tag='pb_html'/>
                                                <input type='hidden' name='filesDropzone'
                                                       id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=actionName.equals("edit")?disciplinary_actionDTO.filesDropzone:ColumnID%>'/>
                                            </div>
                                        </div>
                                        <%--end:: Input fields--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--hidden fields--%>
            <input type='hidden' class='form-control' name='disciplinaryDetails' id='disciplinary_details_from_data' value='[]'>
            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>' value='<%=disciplinary_actionDTO.iD%>'/>
            <input type='hidden' class='form-control' name='disciplinaryLogIncidentNumber'
                   id='disciplinaryLogIncidentNumber_hidden_<%=i%>'
                   value='<%=actionName.equals("edit") ? disciplinary_actionDTO.disciplinaryLogIncidentNumber : incidentNumber%>' />
        </form>

        <%--Accused Employee Keep it outside main form--%>
        <%if (disciplinary_detailsDTOList != null) {%>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="sub_title_top mb-4">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=LM.getText(LC.DISCIPLINARY_LOG_EDIT__TAKE_ACTION, userDTO)%>
                                            </h4>
                                        </div>

                                        <%-- Accused Employee Action --%>
                                        <%for (Disciplinary_detailsDTO disciplinary_detailsDTO : disciplinary_detailsDTOList) {%>
                                            <div class="mb-5">
                                                <%@include file="disciplinary_actionEmployeeActionRow.jsp" %>
                                            </div>
                                        <%}%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <%}%>
        <div class="text-right mr-5">
            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                <%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARY_ACTION_CANCEL_BUTTON, loginDTO)%>
            </button>
            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                    onclick="submitForms()">
                <%=LM.getText(LC.DISCIPLINARY_ACTION_ADD_DISCIPLINARY_ACTION_SUBMIT_BUTTON, loginDTO)%>
            </button>
        </div>
    </div>
</div>
<!-- end:: Content -->

<script type="text/javascript">
    const language = "<%=Language%>";
    const other = language === 'English' ? 'other' : 'অন্যান্য';
    const select = language === 'English' ? 'select' : 'বাছাই করুন';
    const please_select = language === 'English' ? 'Please Select' : 'অনুগ্রহ করে নির্বাচন করুন';
    const closed = language === 'English' ? 'closed' : 'বন্ধ';

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        <%if(disciplinary_detailsDTOList != null){%>
        <%for(Disciplinary_detailsDTO disciplinary_detailsDTO : disciplinary_detailsDTOList){%>
        showOrHideOtherActionOrDates('<%=disciplinary_detailsDTO.employeeRecordsId%>', false);

        $('#complainActionType_select_<%=disciplinary_detailsDTO.employeeRecordsId%>').change(function () {
            showOrHideOtherActionOrDates('<%=disciplinary_detailsDTO.employeeRecordsId%>', true);
        });

        $("#from_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>").on('datepicker.change', () => {
            setMinDateById(
                "to_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>",
                getDateStringById("from_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>")
            );
        });

        <%
            if(actionName.equals("edit") && disciplinary_detailsDTO.fromDate > SessionConstants.MIN_DATE) {
                String fromDateStr = dateFormat.format(new Date(disciplinary_detailsDTO.fromDate));
        %>
        setDateByStringAndId('from_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>', '<%=fromDateStr%>')
        <%}%>

        <%
            if(actionName.equals("edit") && disciplinary_detailsDTO.toDate > SessionConstants.MIN_DATE) {
                String toDateStr = dateFormat.format(new Date(disciplinary_detailsDTO.toDate));
        %>

        setDateByStringAndId("to_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>", '<%=toDateStr%>')
        <%}%>

        <%}%>
        <%}%>
        dateTimeInit("<%=Language%>");
        initValidation();
    });

    function initValidation() {
        $("#disciplinaryActionform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                complaintStatusCat: {
                    required: true
                }
            },
            messages: {
                complaintStatusCat: '<%=isLanguageEnglish ? "Please select complaint status" : "অনুগ্রহ করে নালিশের অবস্থা নির্বাচন করুন"%>'
            }
        });

        $.validator.addMethod('mandatoryIfCaseClosed', function (value, element) {
            if (!isCaseClosed())
                return true;
            return value !== "";
        });

        $.validator.addMethod('mandatoryIfClosedAndNotOther', function (value, element) {
            console.log("Inside mandatory if closed and not other");
            if (!isCaseClosed())
                return true;
            // date elements has id in the from
            // from_date_employeeRecordsId
            let employeeId = element.id.split('_')[2];
            if (isOtherActionSelected(employeeId))
                return true;
            return value !== "";
        });
        $.validator.addMethod('mandatoryIfClosedAndNotOtherForNewDatePickerFrom', function (value, element) {
            if (!isCaseClosed())
                return true;

            // date elements has id in the from
            // from_date_employeeRecordsId
            let employeeId = element.id.split('_')[3];
            if (isOtherActionSelected(employeeId))
                return true;

            return dateValidator('from_date_js_' + employeeId, true);
        });
        $.validator.addMethod('mandatoryIfClosedAndNotOtherForNewDatePickerTo', function (value, element) {
            if (!isCaseClosed())
                return true;

            // date elements has id in the from
            // from_date_employeeRecordsId
            let employeeId = element.id.split('_')[3];
            if (isOtherActionSelected(employeeId))
                return true;

            return dateValidator('to_date_js_' + employeeId, true);
        });

        $.validator.addMethod('mandatoryIfClosedAndOther', function (value, element) {
            if (!isCaseClosed())
                return true;

            // date elements has id in the from
            // complianOther_employeeRecordsId
            let employeeId = element.id.split('_')[1];

            if (!isOtherActionSelected(employeeId))
                return true;

            return value !== "";
        });

        <%if(disciplinary_detailsDTOList != null){%>
        <%for(Disciplinary_detailsDTO disciplinary_detailsDTO : disciplinary_detailsDTOList){%>
        $("#disciplinary_detail_form_<%=disciplinary_detailsDTO.employeeRecordsId%>").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                complainActionType: {
                    mandatoryIfCaseClosed: true
                },
                fromDate: {
                    mandatoryIfClosedAndNotOther: true
                },
                toDate: {
                    mandatoryIfClosedAndNotOther: true
                },
                complianOther: {
                    mandatoryIfClosedAndOther: true
                },
                daySelectionfrom_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: {
                    mandatoryIfClosedAndNotOtherForNewDatePickerFrom: true
                },
                monthSelectionfrom_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: {
                    mandatoryIfClosedAndNotOtherForNewDatePickerFrom: true
                },
                yearSelectionfrom_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: {
                    mandatoryIfClosedAndNotOtherForNewDatePickerFrom: true
                },
                daySelectionto_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: {
                    mandatoryIfClosedAndNotOtherForNewDatePickerTo: true
                },
                monthSelectionto_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: {
                    mandatoryIfClosedAndNotOtherForNewDatePickerTo: true
                },
                yearSelectionto_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: {
                    mandatoryIfClosedAndNotOtherForNewDatePickerTo: true
                },
            },
            messages: {
                complainActionType: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                fromDate: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                toDate: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                complianOther: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                daySelectionfrom_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                monthSelectionfrom_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                yearSelectionfrom_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                daySelectionto_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                monthSelectionto_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>',
                yearSelectionto_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>: '<%=isLanguageEnglish ? "Mandatory for closing complain" : "নালিশ বন্ধের জন্য আবশ্যক"%>'
            }
        });
        <%}%>
        <%}%>
    }

    function isCaseClosed() {
        return $('#complaintStatusCat option:selected').text().toLowerCase() === closed;
    }

    function isOtherActionSelected(userName) {
        return $('#complainActionType_select_' + userName + ' option:selected').text().toLowerCase() === other;
    }

    function showOrHideOtherActionOrDates(userName, bool_ClearVal) {
        let selected_item = $('#complainActionType_select_' + userName + ' option:selected').text().toLowerCase();
        if (selected_item === select) {
            $('#complianOther_div_' + userName).hide();
            $('#from_to_date_div_' + userName).hide();
        } else if (selected_item === other) {
            $('#complianOther_div_' + userName).show();
            $('#from_to_date_div_' + userName).hide();
        } else {
            $('#complianOther_div_' + userName).hide();
            $('#from_to_date_div_' + userName).show();
        }

        if (bool_ClearVal) {
            $('#complianOther_' + userName).val('');
            resetDateById('from_date_js_' + userName);
            resetDateById('to_date_js_' + userName);
            $('#to_date_' + userName).val('');
            $('#from_date_' + userName).val('');
        }
    }

    function submitForms() {
        let all_valid = true;

        <%if(disciplinary_detailsDTOList != null){%>
            let disciplinary_details = [];
            let form, data, json_data, user_action_form;
            <%for(Disciplinary_detailsDTO disciplinary_detailsDTO : disciplinary_detailsDTOList){%>
                document.getElementById("from_date_<%=disciplinary_detailsDTO.employeeRecordsId%>").value = getDateStringById("from_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>")
                document.getElementById("to_date_<%=disciplinary_detailsDTO.employeeRecordsId%>").value = getDateStringById("to_date_js_<%=disciplinary_detailsDTO.employeeRecordsId%>")
                user_action_form = $('#disciplinary_detail_form_<%=disciplinary_detailsDTO.employeeRecordsId%>');
                all_valid &&= user_action_form.valid();
                user_action_form.validate();

                form = document.getElementById(
                    'disciplinary_detail_form_<%=disciplinary_detailsDTO.employeeRecordsId%>'
                );
                data = new FormData(form);
                json_data = Object.fromEntries(data.entries());
                disciplinary_details.push(json_data);
            <%}%>
            console.log(disciplinary_details);
            $('#disciplinary_details_from_data').val(
                JSON.stringify(disciplinary_details)
            );
        <%}%>

        const disciplinaryActionform = $("#disciplinaryActionform");
        all_valid &&= disciplinaryActionform.valid();
        if (all_valid) {
            submitAjaxForm('disciplinaryActionform');
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }
</script>