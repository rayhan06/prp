<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="java.util.List" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_REPORT_WHERE_STARTTIME, loginDTO)%>
				</label>
				<div class="col-md-9">
<%--					<input class='form-control'  name='startTime' id = 'startTime' value=""/>							--%>
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="startDate_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='startDate' name='startTime' value=""
							   tag='pb_html'
						/>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style="display: none;">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_REPORT_WHERE_STARTTIME_1, loginDTO)%>
				</label>
				<div class="col-md-9">
<%--					<input class='form-control'  name='startTime_1' id = 'startTime_1' value=""/>							--%>

						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="startTime_1_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='startTime_1' name='startTime_1' value=""
							   tag='pb_html'
						/>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_REPORT_WHERE_ENDTIME, loginDTO)%>
				</label>
				<div class="col-md-9">
<%--					<input class='form-control'  name='endTime' id = 'endTime' value=""/>							--%>
						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="endTime_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='endTime' name='endTime' value=""
							   tag='pb_html'
						/>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style="display: none;">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_REPORT_WHERE_ENDTIME_3, loginDTO)%>
				</label>
				<div class="col-md-9">
<%--					<input class='form-control'  name='endTime_3' id = 'endTime_3' value=""/>							--%>

						<jsp:include page="/date/date.jsp">
							<jsp:param name="DATE_ID" value="endTime_3_js"></jsp:param>
							<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
						</jsp:include>
						<input type='hidden' class='form-control formRequired datepicker' readonly="readonly"
							   data-label="Document Date" id='endTime_3' name='endTime_3' value=""
							   tag='pb_html'
						/>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_REPORT_WHERE_VEHICLETYPECAT, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control'  name='vehicleTypeCat' id = 'vehicleTypeCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "vehicle_type", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_REPORT_WHERE_OFFICEID, loginDTO)%>
				</label>
				<div class="col-md-9">
<%--					<input class='form-control'  name='officeId' id = 'officeId' value=""/>--%>
						<select class='form-control'  name='officeId' id = 'officeId'>

							<%=Office_unitsRepository.getInstance().buildOptions(Language, 0L)%>

						</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" style = "display: none;">
			<div class="form-group row">
				<label class="col-md-3 control-label text-md-right">
					<%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_REPORT_WHERE_ISDELETED, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='isDeleted' id = 'isDeleted' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(() => {
	showFooter = false;
});

function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}

</script>