<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="pi_requisition.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>


<%
    String navigator2 = "navPI_REQUISITION";
    String servletName = "Pi_requisitionServlet";
    String actionType = request.getParameter("actionType");
    System.out.println(actionType);
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=isLanguageEnglish ? "Name" : "নাম"%>
            </th>
            <th><%=isLanguageEnglish ? "Designation" : "পদবি"%>
            </th>
            <th><%=isLanguageEnglish ? "Office" : "অফিস"%>
            </th>
            <th><%=LM.getText(LC.PI_REQUISITION_ADD_APPLYDATE, loginDTO)%>
            </th>
<%--            <th><%=LM.getText(LC.PI_REQUISITION_ADD_REMARKS, loginDTO)%>--%>
<%--            </th>--%>
            <th><%=LM.getText(LC.PI_REQUISITION_ADD_STATUS, loginDTO)%>
            </th>
<%--            <th><%=isLanguageEnglish ? "First Approver's Name, Designation and Office" : "প্রথম অনুমোদনকারীর নাম, পদবি এবং অফিস"%>--%>
<%--            </th>--%>
<%--            <th><%=isLanguageEnglish ? "First Approver's Approval/Rejection Date" : "প্রথম অনুমোদনকারীর অনুমোদনের/প্রত্যাখ্যানের তারিখ"%>--%>
<%--            </th>--%>
<%--            <th><%=isLanguageEnglish ? "Second Approver's Name, Designation and Office" : "দ্বিতীয় অনুমোদনকারীর নাম, পদবি এবং অফিস"%>--%>
<%--            </th>--%>
<%--            <th><%=isLanguageEnglish ? "Second Approver's Approval/Rejection Date" : "দ্বিতীয় অনুমোদনকারীর অনুমোদনের/প্রত্যাখ্যানের তারিখ"%>--%>
<%--            </th>--%>
<%--            <th><%=isLanguageEnglish ? "Store Keeper's Name, Designation and Office" : "স্টোর কিপারের নাম, পদবি এবং অফিস"%>--%>
<%--            </th>--%>
<%--            <th><%=isLanguageEnglish ? "Store Keeper's Supply Date" : "স্টোর কিপারেরর সরবরাহের তারিখ"%>--%>
<%--            </th>--%>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <%
                if (actionType.equals("search")) {
            %>
<%--            <th><%=LM.getText(LC.PI_REQUISITION_SEARCH_PI_REQUISITION_EDIT_BUTTON, loginDTO)%>--%>
<%--            </th>--%>
<%--            <th class="">--%>
<%--                <div class="text-center">--%>
<%--                    <span>All</span>--%>
<%--                </div>--%>
<%--                <div class="d-flex align-items-center justify-content-between mt-3">--%>
<%--                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">--%>
<%--                        <i class="fa fa-trash"></i>--%>
<%--                    </button>--%>
<%--                    &lt;%&ndash;                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>&ndash;%&gt;--%>
<%--                </div>--%>
<%--            </th>--%>
            <%}%>

        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Pi_requisitionDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Pi_requisitionDTO pi_requisitionDTO = (Pi_requisitionDTO) data.get(i);


        %>
        <tr>


            <td>
                <%
                    value = isLanguageEnglish ? pi_requisitionDTO.requesterNameEng : pi_requisitionDTO.requesterNameBng + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>


            <td>
                <%
                    value = isLanguageEnglish ? pi_requisitionDTO.organogramEng : pi_requisitionDTO.organogramBng + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>

            <td>
                <%
                    value = isLanguageEnglish ? pi_requisitionDTO.officeUnitEng : pi_requisitionDTO.officeUnitBng + "";
                %>

                <%=Utils.getDigits(value, Language)%>


            </td>
            <td>
                <%
                    value = pi_requisitionDTO.applyDate + "";
                %>
                <%
                    String formatted_applyDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                %>
                <%=Utils.getDigits(formatted_applyDate, Language)%>


            </td>

<%--            <td>--%>
<%--                <%--%>
<%--                    value = pi_requisitionDTO.comment + "";--%>
<%--                %>--%>

<%--                <%=Utils.getDigits(value, Language)%>--%>


<%--            </td>--%>

            <td>

                <%=CommonApprovalStatus.getRequisitionText(pi_requisitionDTO.status, Language)%>


<%--            </td>--%>
<%--            <%--%>
<%--                OfficeUnitOrganograms approverOne = OfficeUnitOrganogramsRepository.getInstance().getById(pi_requisitionDTO.approvalOneOrganogramId);--%>
<%--                Office_unitsDTO approverOneOfficeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(approverOne.office_unit_id);--%>
<%--                EmployeeOfficeDTO approverOneOffice = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(pi_requisitionDTO.approvalOneOrganogramId);--%>
<%--                Employee_recordsDTO approverOneEmployeeRecordsDTO = Employee_recordsRepository.getInstance().getById(approverOneOffice.employeeRecordId);--%>

<%--            %>--%>

<%--            <td>--%>
<%--                <%--%>
<%--                    value = isLanguageEnglish ? approverOneEmployeeRecordsDTO.nameEng + "," + approverOne.designation_eng + "," + approverOneOfficeUnitsDTO.unitNameEng :--%>
<%--                            approverOneEmployeeRecordsDTO.nameBng + "," + approverOne.designation_bng + "," + approverOneOfficeUnitsDTO.unitNameBng;--%>
<%--                %>--%>

<%--                <%=Utils.getDigits(value, Language)%>--%>


<%--            </td>--%>


<%--            <td style="text-align: center">--%>
<%--                <%--%>

<%--                    value = pi_requisitionDTO.approvalOneDate + "";--%>
<%--                %>--%>
<%--                <%--%>
<%--                    String formatted_approvalOneDate = simpleDateFormat.format(new Date(Long.parseLong(value)));--%>
<%--                    if (pi_requisitionDTO.status < CommonApprovalStatus.REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue()) {--%>
<%--                        formatted_approvalOneDate = "-";--%>
<%--                    }--%>
<%--                %>--%>
<%--                <%=Utils.getDigits(formatted_approvalOneDate, Language)%>--%>


<%--            </td>--%>


<%--            <%--%>
<%--                OfficeUnitOrganograms approverTwo = OfficeUnitOrganogramsRepository.getInstance().getById(pi_requisitionDTO.approvalTwoOrganogramId);--%>
<%--                Office_unitsDTO approverTwoOfficeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(approverTwo.office_unit_id);--%>
<%--                EmployeeOfficeDTO approverTwoOffice = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(pi_requisitionDTO.approvalTwoOrganogramId);--%>
<%--                Employee_recordsDTO approverTwoEmployeeRecordsDTO = Employee_recordsRepository.getInstance().getById(approverTwoOffice.employeeRecordId);--%>

<%--            %>--%>

<%--            <td>--%>
<%--                <%--%>
<%--                    value = isLanguageEnglish ? approverTwoEmployeeRecordsDTO.nameEng + "," + approverTwo.designation_eng + "," + approverTwoOfficeUnitsDTO.unitNameEng :--%>
<%--                            approverTwoEmployeeRecordsDTO.nameBng + "," + approverTwo.designation_bng + "," + approverTwoOfficeUnitsDTO.unitNameBng;--%>
<%--                %>--%>

<%--                <%=Utils.getDigits(value, Language)%>--%>


<%--            </td>--%>

<%--            <td style="text-align: center">--%>
<%--                <%--%>
<%--                    value = pi_requisitionDTO.approvalTwoDate + "";--%>
<%--                %>--%>
<%--                <%--%>
<%--                    String formatted_approvalTwoDate = simpleDateFormat.format(new Date(Long.parseLong(value)));--%>
<%--                    if (pi_requisitionDTO.status < CommonApprovalStatus.REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue()) {--%>
<%--                        formatted_approvalTwoDate = "-";--%>
<%--                    }--%>
<%--                %>--%>
<%--                <%=Utils.getDigits(formatted_approvalTwoDate, Language)%>--%>


<%--            </td>--%>


<%--            <%--%>

<%--                OfficeUnitOrganograms storeKeeper  ;--%>
<%--                Office_unitsDTO storeKeeperOfficeUnitsDTO = null ;--%>
<%--                EmployeeOfficeDTO storeKeeperOffice ;--%>
<%--                Employee_recordsDTO storeKeeperEmployeeRecordsDTO ;--%>

<%--                storeKeeper = OfficeUnitOrganogramsRepository.getInstance().getById(pi_requisitionDTO.storeKeeperOrganogramId);--%>
<%--                if(storeKeeper != null){--%>
<%--                    storeKeeperOfficeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(storeKeeper.office_unit_id);--%>
<%--                }--%>
<%--                storeKeeperOffice = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(pi_requisitionDTO.storeKeeperOrganogramId);--%>
<%--                if(storeKeeperOffice != null){--%>
<%--                    storeKeeperEmployeeRecordsDTO = Employee_recordsRepository.getInstance().getById(storeKeeperOffice.employeeRecordId);--%>

<%--                    if(storeKeeperEmployeeRecordsDTO != null){--%>
<%--                        value = isLanguageEnglish ? storeKeeperEmployeeRecordsDTO.nameEng + "," + storeKeeper.designation_eng + "," + storeKeeperOfficeUnitsDTO.unitNameEng :--%>
<%--                                storeKeeperEmployeeRecordsDTO.nameBng + "," + storeKeeper.designation_bng + "," + storeKeeperOfficeUnitsDTO.unitNameBng;--%>
<%--                    }--%>
<%--                    else--%>
<%--                    {--%>
<%--                        value = "";--%>
<%--                    }--%>


<%--                }--%>
<%--                else{--%>
<%--                    value = "";--%>
<%--                }--%>


<%--            %>--%>

<%--            <td>--%>


<%--                <%=Utils.getDigits(value, Language)%>--%>


<%--            </td>--%>

<%--            <td style="text-align: center">--%>
<%--                <%--%>
<%--                    value = pi_requisitionDTO.storeKeeperDate + "";--%>
<%--                %>--%>
<%--                <%--%>
<%--                    String formatted_storeKeeperDate = simpleDateFormat.format(new Date(Long.parseLong(value)));--%>
<%--                    if (pi_requisitionDTO.status < CommonApprovalStatus.REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue()) {--%>
<%--                        formatted_storeKeeperDate = "-";--%>
<%--                    }--%>
<%--                %>--%>
<%--                <%=Utils.getDigits(formatted_storeKeeperDate, Language)%>--%>


<%--            </td>--%>


            <%
                CommonDTO commonDTO = pi_requisitionDTO;
                if (actionType.equals("getFirstApproverList")) {

            %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getFirstApproverPage&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>


            <%
            } else if (actionType.equals("getSecondApproverList")) {
            %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getSecondApproverPage&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>


            <%
            } else if (actionType.equals("getSupplyList")) {
            %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getSupplyPage&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>

            <%} else {%>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>
            <%
                }
            %>
<%--            <%--%>
<%--                if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue() && actionType.equals("search")) {--%>
<%--            %>--%>
<%--            <td class="text-right">--%>
<%--                <div class='checker'>--%>
<%--                    <span class='chkEdit'><input type='checkbox' name='ID' value='<%=pi_requisitionDTO.iD%>'/></span>--%>
<%--                </div>--%>
<%--            </td>--%>
<%--            <%} else if (actionType.equals("search")) {%>--%>

<%--            <td></td>--%>
<%--            <%}%>--%>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			