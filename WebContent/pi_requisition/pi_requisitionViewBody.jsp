<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="pi_requisition.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>


<%
    String servletName = "Pi_requisitionServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Pi_requisitionDTO pi_requisitionDTO = Pi_requisitionDAO.getInstance().getDTOByID(id);
    List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(Long.parseLong(ID));

%>
<%@include file="../pb/viewInitializer.jsp" %>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_REQUISITION_ADD_PI_REQUISITION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="mb-4">
                <div class="onlyborder">
                    <div class="row mx-2 mx-md-0">
                        <div class="col-md-10 offset-md-1">
                            <div class="sub_title_top">
                                <div class="sub_title">
                                    <h4 style="background: white">
                                        <%=LM.getText(LC.PI_REQUISITION_ADD_PI_REQUISITION_ADD_FORMNAME, loginDTO)%>
                                    </h4>
                                </div>
                            </div>


                            <div class="form-group row d-flex align-items-center">
                                <label class="col-md-4 col-form-label text-md-right">
                                    <%=isLanguageEnglish ? "Name" : "নাম"%>
                                </label>
                                <div class="col-md-8 form-control">
                                    <%
                                        value = isLanguageEnglish ? pi_requisitionDTO.requesterNameEng : pi_requisitionDTO.requesterNameBng + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </div>
                            </div>


                            <div class="form-group row d-flex align-items-center">
                                <label class="col-md-4 col-form-label text-md-right">
                                    <%=isLanguageEnglish ? "Designation" : "পদবি"%>
                                </label>
                                <div class="col-md-8 form-control">
                                    <%
                                        value = isLanguageEnglish ? pi_requisitionDTO.organogramEng : pi_requisitionDTO.organogramBng + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </div>
                            </div>


                            <div class="form-group row d-flex align-items-center">
                                <label class="col-md-4 col-form-label text-md-right">
                                    <%=isLanguageEnglish ? "Office" : "অফিস"%>
                                </label>
                                <div class="col-md-8 form-control">
                                    <%
                                        value = isLanguageEnglish ? pi_requisitionDTO.officeUnitEng : pi_requisitionDTO.officeUnitBng + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </div>
                            </div>


                            <div class="form-group row d-flex align-items-center">
                                <label class="col-md-4 col-form-label text-md-right">
                                    <%=LM.getText(LC.PI_REQUISITION_ADD_APPLYDATE, loginDTO)%>
                                </label>
                                <div class="col-md-8 form-control">
                                    <%
                                        value = pi_requisitionDTO.applyDate + "";
                                    %>
                                    <%
                                        String formatted_applyDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                    %>
                                    <%=Utils.getDigits(formatted_applyDate, Language)%>


                                </div>
                            </div>

                            <div class="form-group row d-flex align-items-center">
                                <label class="col-md-4 col-form-label text-md-right">
                                    <%=LM.getText(LC.PI_REQUISITION_ADD_REMARKS, loginDTO)%>
                                </label>
                                <div class="col-md-8 form-control">
                                    <%
                                        value = pi_requisitionDTO.comment + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-md-4 col-form-label text-md-right">
                                    <%=UtilCharacter.getDataByLanguage(Language, "বিতরণ মন্তব্য", "Disburse Comment")%>
                                </label>
                                <div class="col-md-8 form-control">
                                    <%
                                        value = pi_requisitionDTO.disburseComment + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </div>
                            </div>

                            <div class="form-group row d-flex align-items-center">
                                <label class="col-md-4 col-form-label text-md-right">
                                    <%=LM.getText(LC.PI_REQUISITION_ADD_STATUS, loginDTO)%>
                                </label>
                                <div class="col-md-8 form-control">


                                    <%=CommonApprovalStatus.getRequisitionText(pi_requisitionDTO.status, Language)%>


                                </div>
                            </div>

<%--                            <%--%>
<%--                                OfficeUnitOrganograms approverOne = OfficeUnitOrganogramsRepository.getInstance().getById(pi_requisitionDTO.approvalOneOrganogramId);--%>
<%--                                Office_unitsDTO approverOneOfficeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(approverOne.office_unit_id);--%>
<%--                                EmployeeOfficeDTO approverOneOffice = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(pi_requisitionDTO.approvalOneOrganogramId);--%>
<%--                                Employee_recordsDTO approverOneEmployeeRecordsDTO = Employee_recordsRepository.getInstance().getById(approverOneOffice.employeeRecordId);--%>

<%--                            %>--%>
<%--                            <div class="form-group row d-flex align-items-center">--%>
<%--                                <label class="col-md-4 col-form-label text-md-right">--%>
<%--                                    <%=isLanguageEnglish ? "First Approver's Name, Designation and Office" : "প্রথম অনুমোদনকারীর নাম, পদবি এবং অফিস"%>--%>
<%--                                </label>--%>
<%--                                <div class="col-md-8 form-control">--%>
<%--                                    <%--%>
<%--                                        value = isLanguageEnglish ? approverOneEmployeeRecordsDTO.nameEng + "," + approverOne.designation_eng + "," + approverOneOfficeUnitsDTO.unitNameEng :--%>
<%--                                                approverOneEmployeeRecordsDTO.nameBng + "," + approverOne.designation_bng + "," + approverOneOfficeUnitsDTO.unitNameBng;--%>
<%--                                    %>--%>

<%--                                    <%=Utils.getDigits(value, Language)%>--%>


<%--                                </div>--%>
<%--                            </div>--%>


<%--                            <div class="form-group row d-flex align-items-center">--%>
<%--                                <label class="col-md-4 col-form-label text-md-right">--%>
<%--                                    <%=isLanguageEnglish ? "First Approver's Approval/Rejection Date" : "প্রথম অনুমোদনকারীর অনুমোদনের/প্রত্যাখ্যানের তারিখ"%>--%>
<%--                                </label>--%>
<%--                                <div class="col-md-8 form-control">--%>
<%--                                    <%--%>
<%--                                        value = pi_requisitionDTO.approvalOneDate + "";--%>
<%--                                    %>--%>
<%--                                    <%--%>
<%--                                        String formatted_approvalOneDate = simpleDateFormat.format(new Date(Long.parseLong(value)));--%>
<%--                                        if (pi_requisitionDTO.status < CommonApprovalStatus.REQUISITION_REJECTED_BY_FIRST_APPROVER.getValue()) {--%>
<%--                                            formatted_approvalOneDate = "-";--%>
<%--                                        }--%>
<%--                                    %>--%>
<%--                                    <%=Utils.getDigits(formatted_approvalOneDate, Language)%>--%>


<%--                                </div>--%>
<%--                            </div>--%>

<%--                            <%--%>
<%--                                OfficeUnitOrganograms approverTwo = OfficeUnitOrganogramsRepository.getInstance().getById(pi_requisitionDTO.approvalTwoOrganogramId);--%>
<%--                                Office_unitsDTO approverTwoOfficeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(approverTwo.office_unit_id);--%>
<%--                                EmployeeOfficeDTO approverTwoOffice = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(pi_requisitionDTO.approvalTwoOrganogramId);--%>
<%--                                Employee_recordsDTO approverTwoEmployeeRecordsDTO = Employee_recordsRepository.getInstance().getById(approverTwoOffice.employeeRecordId);--%>

<%--                            %>--%>
<%--                            <div class="form-group row d-flex align-items-center">--%>
<%--                                <label class="col-md-4 col-form-label text-md-right">--%>
<%--                                    <%=isLanguageEnglish ? "Second Approver's Name, Designation and Office" : "দ্বিতীয় অনুমোদনকারীর নাম, পদবি এবং অফিস"%>--%>
<%--                                </label>--%>
<%--                                <div class="col-md-8 form-control">--%>
<%--                                    <%--%>
<%--                                        value = isLanguageEnglish ? approverTwoEmployeeRecordsDTO.nameEng + "," + approverTwo.designation_eng + "," + approverTwoOfficeUnitsDTO.unitNameEng :--%>
<%--                                                approverTwoEmployeeRecordsDTO.nameBng + "," + approverTwo.designation_bng + "," + approverTwoOfficeUnitsDTO.unitNameBng;--%>
<%--                                    %>--%>

<%--                                    <%=Utils.getDigits(value, Language)%>--%>


<%--                                </div>--%>
<%--                            </div>--%>


<%--                            <div class="form-group row d-flex align-items-center">--%>
<%--                                <label class="col-md-4 col-form-label text-md-right">--%>
<%--                                    <%=isLanguageEnglish ? "Second Approver's Approval/Rejection Date" : "দ্বিতীয় অনুমোদনকারীর অনুমোদনের/প্রত্যাখ্যানের তারিখ"%>--%>
<%--                                </label>--%>
<%--                                <div class="col-md-8 form-control">--%>
<%--                                    <%--%>
<%--                                        value = pi_requisitionDTO.approvalTwoDate + "";--%>
<%--                                    %>--%>
<%--                                    <%--%>
<%--                                        String formatted_approvalTwoDate = simpleDateFormat.format(new Date(Long.parseLong(value)));--%>
<%--                                        if (pi_requisitionDTO.status < CommonApprovalStatus.REQUISITION_REJECTED_BY_SECOND_APPROVER.getValue()) {--%>
<%--                                            formatted_approvalTwoDate = "-";--%>
<%--                                        }--%>
<%--                                    %>--%>
<%--                                    <%=Utils.getDigits(formatted_approvalTwoDate, Language)%>--%>


<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <%--%>
<%--                                OfficeUnitOrganograms storeKeeper = OfficeUnitOrganogramsRepository.getInstance().getById(pi_requisitionDTO.storeKeeperOrganogramId);--%>
<%--                                Office_unitsDTO storeKeeperOfficeUnitsDTO = Office_unitsRepository.getInstance().getOffice_unitsDTOByID(storeKeeper.office_unit_id);--%>
<%--                                EmployeeOfficeDTO storeKeeperOffice = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(pi_requisitionDTO.storeKeeperOrganogramId);--%>
<%--                                Employee_recordsDTO storeKeeperEmployeeRecordsDTO = Employee_recordsRepository.getInstance().getById(storeKeeperOffice.employeeRecordId);--%>

<%--                            %>--%>

<%--                            <div class="form-group row d-flex align-items-center">--%>
<%--                                <label class="col-md-4 col-form-label text-md-right">--%>
<%--                                    <%=isLanguageEnglish ? "Store Keeper's Name, Designation and Office" : "স্টোর কিপারের নাম, পদবি এবং অফিস"%>--%>
<%--                                </label>--%>
<%--                                <div class="col-md-8 form-control">--%>
<%--                                    <%--%>
<%--                                        value = isLanguageEnglish ? storeKeeperEmployeeRecordsDTO.nameEng + "," + storeKeeper.designation_eng + "," + storeKeeperOfficeUnitsDTO.unitNameEng :--%>
<%--                                                storeKeeperEmployeeRecordsDTO.nameBng + "," + storeKeeper.designation_bng + "," + storeKeeperOfficeUnitsDTO.unitNameBng;--%>
<%--                                    %>--%>

<%--                                    <%=Utils.getDigits(value, Language)%>--%>


<%--                                </div>--%>
<%--                            </div>--%>


<%--                            <div class="form-group row d-flex align-items-center">--%>
<%--                                <label class="col-md-4 col-form-label text-md-right">--%>
<%--                                    <%=isLanguageEnglish ? "Store Keeper's Supply Date" : "স্টোর কিপারেরর সরবরাহের তারিখ"%>--%>
<%--                                </label>--%>
<%--                                <div class="col-md-8 form-control">--%>
<%--                                    <%--%>
<%--                                        value = pi_requisitionDTO.storeKeeperDate + "";--%>
<%--                                    %>--%>
<%--                                    <%--%>
<%--                                        String formatted_storeKeeperDate = simpleDateFormat.format(new Date(Long.parseLong(value)));--%>
<%--                                        if (pi_requisitionDTO.status < CommonApprovalStatus.REQUISITION_SUPPLIED_BY_STORE_KEEPER.getValue()) {--%>
<%--                                            formatted_storeKeeperDate = "-";--%>
<%--                                        }--%>
<%--                                    %>--%>
<%--                                    <%=Utils.getDigits(formatted_storeKeeperDate, Language)%>--%>


<%--                                </div>--%>
<%--                            </div>--%>


                        </div>
                    </div>
                </div>
            </div>
            <h5 class="table-title">
                <%=isLanguageEnglish ? "Requested Items" : "অনুরোধকৃত দ্রব্যাদি"%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <thead>
                    <tr>
                        <th class="row-data-product-type-name">
                            <%=isLanguageEnglish ? "Item Type" : "দ্রব্যের ধরণ"%>
                        </th>
                        <th class="row-data-product-name">
                            <%=isLanguageEnglish ? "Item Name" : "দ্রব্যের নাম"%>
                        </th>
                        <th class="row-data-product-amount">
                            <%=isLanguageEnglish ? "Amount" : "পরিমাণ"%>
                        </th>
                        <%
                            if (pi_requisitionDTO.status >= CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()) {
                        %>
                        <th><%=isLanguageEnglish ? "Approved Supply Amount" : "অনুমোদনকৃত সরবরাহ পরিমাণ"%>
                        </th>
                        <%
                            if (pi_requisitionDTO.status >= CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue()) {
                        %>
<%--                        <th><%=isLanguageEnglish ? "Supply" : "সরবরাহ"%>--%>
<%--                        </th>--%>
                        <%
                            }
                        %>
                        <%
                            }
                        %>
                    </tr>
                    </thead>

                    <tbody>
                    <%
                        for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
                    %>
                    <tr>
                        <td>
                            <%=UtilCharacter.getDataByLanguage(Language,pi_requisition_itemDTO.itemTypeNameBn,pi_requisition_itemDTO.itemTypeNameEn)%>
                        </td>
                        <td>
                            <%=UtilCharacter.getDataByLanguage(Language,pi_requisition_itemDTO.itemNameBn,pi_requisition_itemDTO.itemNameEn)%>
                        </td>
                        <td>
                            <%=Utils.getDigits(pi_requisition_itemDTO.requestedQuantity,Language)%>
                        </td>
<%--                        <%--%>
<%--                            if (pi_requisitionDTO.status >= CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()) {--%>
<%--                        %>--%>
<%--                        <td>--%>
<%--                            <%=Utils.getDigits(pi_requisition_itemDTO.approvalOneQuantity,Language)%>--%>
<%--                        </td>--%>
<%--                        <%--%>
<%--                            if (pi_requisitionDTO.status >= CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue()) {--%>
<%--                        %>--%>
                        <td>
                            <%=Utils.getDigits(pi_requisition_itemDTO.approvalTwoQuantity,Language)%>
                        </td>
<%--                        <%--%>
<%--                            }--%>
<%--                        %>--%>
<%--                        <%}%>--%>

                    </tr>

                    <%
                        }
                    %>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>