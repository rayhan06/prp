<%@page import="pi_requisition.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.Date" %>
<%@ page import="util.*" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="java.util.List" %>
<%@ page import="sessionmanager.SessionConstants" %>

<%
    Pi_requisitionDTO pi_requisitionDTO = new Pi_requisitionDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_requisitionDTO = Pi_requisitionDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_REQUISITION_ADD_PI_REQUISITION_ADD_FORMNAME, loginDTO);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(ID);

%>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_requisitionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=pi_requisitionDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-2"><%=LM.getText(LC.TRAVEL_ALLOWANCE_VIEW_APPLICANT, loginDTO)%>
                                        </label>
                                        <div class="col-4">
                                            <%=isLanguageEnglish ? pi_requisitionDTO.requesterNameEng : pi_requisitionDTO.requesterNameBng%>
                                        </div>
                                        <label class="col-2"><%=isLanguageEnglish ? "Organogram" : "দপ্তর"%>
                                        </label>
                                        <div class="col-4">
                                            <%=isLanguageEnglish ? pi_requisitionDTO.organogramEng : pi_requisitionDTO.organogramBng%>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2"><%=isLanguageEnglish ? "Office Unit" : "অফিস ইউনিট"%>
                                        </label>
                                        <div class="col-4">
                                            <%=isLanguageEnglish ? pi_requisitionDTO.officeUnitEng : pi_requisitionDTO.organogramBng%>
                                        </div>
                                        <label class="col-2"><%=LM.getText(LC.PI_REQUISITION_ADD_APPLYDATE, loginDTO)%>
                                        </label>
                                        <div class="col-4">
                                            <%=Utils.getDigits(simpleDateFormat.format(new Date(pi_requisitionDTO.approvalTwoDate)), Language)%>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2"><%=LM.getText(LC.PI_REQUISITION_ADD_REMARKS, loginDTO)%>
                                        </label>
                                        <div class="col-4">
                                            <%=pi_requisitionDTO.comment%>
                                        </div>
                                        <label class="col-2"><%=isLanguageEnglish ? "Approved By" : "অনুমোদনকারি"%>
                                        </label>
                                        <div class="col-4">
                                            <%
                                                EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByOfficeUnitOrganogramId(pi_requisitionDTO.approvalTwoOrganogramId);
                                                Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(employeeOfficeDTO.employeeRecordId);
                                            %>
                                            <%=isLanguageEnglish ? employeeRecordsDTO.nameEng : employeeRecordsDTO.nameBng%>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2">
                                            <%=UtilCharacter.getDataByLanguage(Language, "মালামাল প্রদান মন্তব্য", "Product Disburse Comment")%>
                                        </label>
                                        <div class="col-10">
                                            <%
                                                if (pi_requisitionDTO.status ==
                                                        CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue()) {
                                            %>
                                            <input type='text' class='form-control' name='disburseComment' maxlength="2000"
                                                   id='disburseComment' value="<%=pi_requisitionDTO.disburseComment%>"/>
                                            <%} else {%>
                                            <%=pi_requisitionDTO.disburseComment%>
                                            <%}%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <h5 class="table-title">
                        <%=isLanguageEnglish ? "Requested Items" : "অনুরোধকৃত দ্রব্যাদি"%>
                    </h5>
                    <div class="row mt-5">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="product-view-table">
                                <thead>
                                <tr>
                                    <th class="row-data-product-type-name">
                                        <%=isLanguageEnglish ? "Item Type" : "দ্রব্যের ধরণ"%>
                                    </th>
                                    <th class="row-data-product-name">
                                        <%=isLanguageEnglish ? "Item Name" : "দ্রব্যের নাম"%>
                                    </th>
                                    <th class="row-data-product-amount">
                                        <%=isLanguageEnglish ? "Amount" : "পরিমাণ"%>
                                    </th>
                                    <th><%=isLanguageEnglish ? "Approved Supply Amount" : "অনুমোদনকৃত সরবরাহ পরিমাণ"%>
                                    </th>
<%--                                    <th><%=isLanguageEnglish ? "Supply" : "সরবরাহ"%>--%>
<%--                                    </th>--%>
                                    <th class="row-data-last-apply-date"><%=isLanguageEnglish ? "Last Requisition Date" : "সর্বশেষ চাহিদা প্রদানের তারিখ"%>
                                    </th>
                                    <th class="row-data-last-requested-quantity"><%=isLanguageEnglish ? "Last Requested Amount" : "সর্বশেষ চাহিদাকৃত পরিমাণ"%>
                                    </th>
                                    <th class="row-data-last-received-quantity"><%=isLanguageEnglish ? "Last Received Amount" : "সর্বশেষ প্রাপ্তির পরিমাণ"%>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <%
                                    for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
                                        Pi_requisition_itemDTO previousRequisition = Pi_requisition_itemDAO.getInstance().getDTOsByItemId(pi_requisition_itemDTO.itemId, pi_requisition_itemDTO.requesterEmployeeRecordId, pi_requisition_itemDTO.apply_date);
                                %>
                                <tr>
                                    <td>
                                        <%=UtilCharacter.getDataByLanguage(Language,
                                                pi_requisition_itemDTO.itemTypeNameBn,
                                                pi_requisition_itemDTO.itemTypeNameEn)%>
                                    </td>
                                    <td>
                                        <%=UtilCharacter.getDataByLanguage(Language,
                                            pi_requisition_itemDTO.itemNameBn, pi_requisition_itemDTO.itemNameEn)%>
                                    </td>
                                    <td><%=pi_requisition_itemDTO.requestedQuantity%>
                                    </td>
<%--                                    <td><%=pi_requisition_itemDTO.approvalOneQuantity%>--%>
<%--                                    </td>--%>
                                    <td><%=pi_requisition_itemDTO.approvalTwoQuantity%>
                                    </td>
                                    <%
                                        if (previousRequisition != null) {
                                    %>
                                    <td><%=simpleDateFormat.format(new Date(previousRequisition.apply_date))%>
                                    </td>
                                    <td><%=previousRequisition.requestedQuantity%>
                                    </td>
                                    <td><%=previousRequisition.approvalTwoQuantity%>
                                    </td>
                                    <%
                                    } else {
                                    %>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <%
                                        }
                                    %>
                                </tr>

                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <%
                    if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_SECOND_APPROVER.getValue()
                            && userDTO.roleID != SessionConstants.ADMIN_ROLE
                            && userDTO.roleID != SessionConstants.INVENTORY_ADMIN_ROLE) {
                %>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button type="button" class="btn btn-success shadow ml-2" style="border-radius: 8px;"
                                    id="approve_btn"
                                    onclick="approveRequisition()"><%=isLanguageEnglish ? "Supply" : "সরবরাহ"%>
                            </button>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    let form = $("#bigform");
    function approveRequisition() {
        let msg = '<%=isLanguageEnglish?"Do you want to supply?":"আপনি কি সরবরাহ করতে চান?"%>';
        let confirmButtonText = '<%=isLanguageEnglish?"Yes":"হ্যাঁ"%>';
        let cancelButtonText = '<%=isLanguageEnglish?"No":"না"%>';
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText, () => {
            buttonStateChange(true)
            $.ajax({
                type: "POST",
                url: "Pi_requisitionServlet?actionType=ajax_supply&ID=<%=pi_requisitionDTO.iD%>",
                dataType: 'JSON',
                data: form.serialize(),
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }, () => {
        });
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_requisitionServlet");
    }

    function init(row) {
    }

    let row = 0;
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

</script>