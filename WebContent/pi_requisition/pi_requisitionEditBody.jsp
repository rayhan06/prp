<%@page import="workflow.WorkflowController" %>
<%@page import="login.LoginDTO" %>

<%@page import="pi_requisition.*" %>

<%@page pageEncoding="UTF-8" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="java.util.Date" %>

<%@ page import="util.*" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="java.util.List" %>
<%@ page import="procurement_goods.Procurement_goodsResponseDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>

<%
    Pi_requisitionDTO pi_requisitionDTO = new Pi_requisitionDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_requisitionDTO = Pi_requisitionDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);

    List<Pi_requisition_itemDTO> pi_requisition_itemDTOList = Pi_requisition_itemDAO.getInstance().getDTOsByRequisitionId(ID);
    List<Procurement_goodsResponseDTO> procurement_goodsResponseDTOList = new ArrayList<>();
    for (Pi_requisition_itemDTO pi_requisition_itemDTO : pi_requisition_itemDTOList) {
        Procurement_goodsResponseDTO procurement_goodsResponseDTO = new Procurement_goodsResponseDTO();
        procurement_goodsResponseDTO.iD = pi_requisition_itemDTO.itemId;
        procurement_goodsResponseDTO.nameEn = pi_requisition_itemDTO.itemNameEn;
        procurement_goodsResponseDTO.nameBn = pi_requisition_itemDTO.itemNameBn;
        procurement_goodsResponseDTO.procurementGoodsTypeId = pi_requisition_itemDTO.itemTypeId;
        procurement_goodsResponseDTO.procurementGoodsTypeNameEn = pi_requisition_itemDTO.itemNameEn;
        procurement_goodsResponseDTO.procurementGoodsTypeNameBn = pi_requisition_itemDTO.itemTypeNameBn;
        procurement_goodsResponseDTO.quantity = pi_requisition_itemDTO.requestedQuantity;
        procurement_goodsResponseDTOList.add(procurement_goodsResponseDTO);
    }

    List<Office_unitsDTO> office_unitsDTOS = Office_unitsRepository.getInstance().getOfficeUnitsDTOListAsInventoryStore();

%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_REQUISITION_ADD_PI_REQUISITION_ADD_FORMNAME, loginDTO);
    String context = request.getContextPath() + "/";
    long roleId = userDTO.roleID;
    Employee_recordsDTO employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(userDTO.employee_record_id);
    if (ID != -1) {
        employeeRecordsDTO = Employee_recordsRepository.getInstance().getById(pi_requisitionDTO.employeeRecordId);
    }
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<style>
    .template-row {
        display: none;
    }
</style>
<%

    if (ID == -1) {
        EmployeeOfficeDTO employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(userDTO.employee_record_id);
        pi_requisitionDTO.organogramId = employeeOfficeDTO.officeUnitOrganogramId;
    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_requisitionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=pi_requisitionDTO.iD%>' tag='pb_html'/>
                                    <input type="hidden" name="productData" id="productData">

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right"><%=LM.getText(LC.TRAVEL_ALLOWANCE_VIEW_APPLICANT, loginDTO)%>
                                        </label>
                                        <%
                                            if (roleId == SessionConstants.INVENTORY_ADMIN_ROLE || roleId == SessionConstants.ADMIN_ROLE) {
                                        %>
                                        <div class="col-md-10">
                                            <button type="button" class="btn btn-primary form-control"
                                                    onclick="addEmployeeWithRow(this.id)"
                                                    id="organogramId_button_<%=i%>"
                                                    tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%>
                                            </button>
                                            <table class="table table-bordered table-striped">
                                                <tbody id="organogramId_table_<%=i%>" tag='pb_html'>
                                                <%
                                                    if (pi_requisitionDTO.organogramId != -1) {
                                                %>
                                                <tr>
                                                    <td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(pi_requisitionDTO.organogramId)%>
                                                    </td>
                                                    <td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(pi_requisitionDTO.organogramId, Language)%>
                                                    </td>
                                                    <td><%=WorkflowController.getOrganogramName(pi_requisitionDTO.organogramId, Language)%>
                                                        , <%=WorkflowController.getUnitNameFromOrganogramId(pi_requisitionDTO.organogramId, Language)%>
                                                    </td>
                                                </tr>
                                                <%
                                                    }
                                                %>
                                                </tbody>
                                            </table>
                                            <input type='hidden' class='form-control' name='organogramId'
                                                   id='organogramId_hidden_<%=i%>'
                                                   value='<%=pi_requisitionDTO.organogramId%>' tag='pb_html'/>

                                        </div>
                                        <%
                                        } else {
                                        %>
                                        <div class="col-md-10">

                                            <span class="form-control"><%=isLanguageEnglish
                                                    ?
                                                    employeeRecordsDTO
                                                            .
                                                            nameEng
                                                    :
                                                    employeeRecordsDTO
                                                            .
                                                            nameBng%></span>
                                            <input type='hidden' class='form-control' name='organogramId'
                                                   id='organogramId_hidden_<%=i%>'
                                                   value='<%=pi_requisitionDTO.organogramId%>' tag='pb_html'/>
                                        </div>
                                        <%}%>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right"><%=LM
                                                .
                                                getText
                                                        (
                                                                LC
                                                                        .
                                                                        PI_REQUISITION_ADD_APPLYDATE
                                                                ,
                                                                loginDTO
                                                        )%>
                                        </label>
                                        <div class="col-md-10">
                                            <%
                                                value
                                                        =
                                                        "applyDate_js_"
                                                                +
                                                                i
                                                ;
                                            %>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='applyDate' id='applyDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(pi_requisitionDTO.applyDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <%--<div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right"><%=LM
                                                .
                                                        getText
                                                                (
                                                                        LC
                                                                                .
                                                                                PI_REQUISITION_ADD_INSERTIONTIME
                                                                        ,
                                                                        loginDTO
                                                                )%>
                                        </label>
                                        <div class="col-md-10">
                                            <%
                                                value
                                                        =
                                                        "applyTime_js_"
                                                                +
                                                                i
                                                ;
                                            %>
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden'
                                                   value="<%=pi_requisitionDTO.applyTime%>"
                                                   name='applyTime' id='applyTime_time_<%=i%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>--%>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "সরবরাহের ধরন", "Requisition Type")%>
                                        </label>
                                        <div class="col-md-10" style="margin-top: 0.8rem !important;">
                                            <span class="col-md-5">
                                                <input type="radio" id="personal" name="is_personal" value="1">
                                                &nbsp;<label
                                                    for="personal"><%=LM.getText(LC.TIME_REQUEST_PERSONAL, loginDTO)%></label>
                                            </span>
                                            <span class="col-md-5">
                                                <input type="radio" id="official" name="is_personal" value="0">
                                                &nbsp;<label
                                                    for="official"><%=LM.getText(LC.TIME_REQUEST_OFFICIAL, loginDTO)%></label>
                                            </span>


                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PIAUCTIONID, loginDTO)%>
                                        </label>
                                        <div class="col-10">

                                            <select class='form-control' name='officeUnitId'
                                                    id='officeUnitId'
                                                    tag='pb_html'>
                                                <%= Office_unitsRepository.getInstance().buildOptions(Language, -1L, false, office_unitsDTOS) %>
                                            </select>

                                            <%--                                            <button type="button"--%>
                                            <%--                                                    class="btn btn-primary btn-block shadow btn-border-radius"--%>
                                            <%--                                                    id="officeUnitId_modal_button"--%>
                                            <%--                                                    onclick="officeModalButtonClicked();">--%>
                                            <%--                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>--%>
                                            <%--                                            </button>--%>

                                            <%--                                            <div class="input-group" id="officeUnitId_div" style="display: none">--%>
                                            <%--                                                <input type="hidden" name='officeUnitId'--%>
                                            <%--                                                       id='office_units_id_input' value="">--%>
                                            <%--                                                <button type="button"--%>
                                            <%--                                                        class="btn btn-secondary form-control shadow btn-border-radius"--%>
                                            <%--                                                        disabled id="office_units_id_text"></button>--%>
                                            <%--                                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>--%>
                                            <%--                                                    <button type="button" class="btn btn-outline-danger"--%>
                                            <%--                                                            onclick="crsBtnClicked('officeUnitId');"--%>
                                            <%--                                                            id='officeUnitId_crs_btn' tag='pb_html'>--%>
                                            <%--                                                        x--%>
                                            <%--                                                    </button>--%>
                                            <%--                                                </span>--%>
                                            <%--                                            </div>--%>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label text-md-right"><%=LM
                                                .
                                                getText
                                                        (
                                                                LC
                                                                        .
                                                                        PI_REQUISITION_ADD_REMARKS
                                                                ,
                                                                loginDTO
                                                        )%>
                                        </label>
                                        <div class="col-md-10">
                                            <textarea type='text' class='form-control' name='remarks'
                                                      id='remarks_text' rows="4"
                                                      placeholder="<%=isLanguageEnglish?"Enter comment":"মন্তব্য লিখুন"%>"
                                                      onkeyup="updateDescriptionLen()"
                                                      style="text-align: left;resize: none; width: 100%"
                                                      style="text-align: left;resize: none; width: 100%"><%=pi_requisitionDTO
                                                    .
                                                    comment
                                                    ==
                                                    null
                                                    ?
                                                    ""
                                                    :
                                                    pi_requisitionDTO
                                                            .
                                                            comment
                                                                    .
                                                            trim
                                                                    (
                                                                    )%></textarea>
                                            <p id="remarks_len"
                                               style="width: 100%;text-align: right;font-size: small"><%=(
                                                    pi_requisitionDTO
                                                            .
                                                            comment
                                                            ==
                                                            null
                                                            ?
                                                            ""
                                                            :
                                                            pi_requisitionDTO
                                                                    .
                                                                    comment
                                                                            .
                                                                    trim
                                                                            (
                                                                            )
                                            )
                                                    .
                                                    length
                                                            (
                                                            )%>
                                                /512</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=isLanguageEnglish
                                    ?
                                    "Requested Items"
                                    :
                                    "অনুরোধকৃত দ্রব্যাদি"%>
                        </h5>
                        <div class="">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped text-nowrap" id="product-view-table">
                                    <thead>
                                    <tr>
                                        <th class="row-data-product-type-name">
                                            <%=isLanguageEnglish
                                                    ?
                                                    "Item Type"
                                                    :
                                                    "দ্রব্যের ধরণ"%>
                                        </th>
                                        <th class="row-data-product-name">
                                            <%=isLanguageEnglish
                                                    ?
                                                    "Item Name"
                                                    :
                                                    "দ্রব্যের নাম"%>
                                        </th>
                                        <th class="row-data-product-amount">
                                            <%=isLanguageEnglish
                                                    ?
                                                    "Amount"
                                                    :
                                                    "পরিমাণ"%>
                                        </th>
                                        <%--                                        <th class="row-data-stock-amount"><%=isLanguageEnglish ? "Stock" : "মজুদ"%>--%>
                                        <%--                                        </th>--%>
                                        <th class="row-data-last-apply-date"><%=isLanguageEnglish
                                                ?
                                                "Last Requisition Date"
                                                :
                                                "সর্বশেষ চাহিদা প্রদানের তারিখ"%>
                                        </th>
                                        <th class="row-data-last-requested-quantity"><%=isLanguageEnglish
                                                ?
                                                "Last Requested Amount"
                                                :
                                                "সর্বশেষ চাহিদাকৃত পরিমাণ"%>
                                        </th>
                                        <th class="row-data-last-received-quantity"><%=isLanguageEnglish
                                                ?
                                                "Last Received Amount"
                                                :
                                                "সর্বশেষ প্রাপ্তির পরিমাণ"%>
                                        </th>
                                        <%
                                            if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue()) {
                                        %>
                                        <th></th>
                                        <%}%>

                                    </tr>
                                    </thead>

                                    <tbody></tbody>

                                    <tr class="template-row">
                                        <td class="row-data-product-type-name"></td>
                                        <td class="row-data-product-name"></td>
                                        <td class="row-data-product-amount"></td>
                                        <%--                                        <td class="row-data-stock-amount"></td>--%>

                                        <td class="row-data-last-apply-date"></td>
                                        <td class="row-data-last-requested-quantity"></td>
                                        <td class="row-data-last-received-quantity"></td>

                                        <%
                                            if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue()) {
                                        %>
                                        <td class="row-data-add-btn">
                                            <button class='btn btn-sm cancel-btn text-white shadow pl-4'
                                                    style="padding-right: 14px;" type="button"
                                                    onclick="unSelectRow(this);">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>

                                        <%}%>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <%
                            if (pi_requisitionDTO.status == CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue()) {
                        %>
                        <button class="btn-sm shadow text-white border-0 submit-btn pull-right mt-3"
                                id="add-new-list-btn"
                                type="button">
                            <%=LM
                                    .
                                    getText
                                            (
                                                    LC
                                                            .
                                                            TASK_TYPE_APPROVAL_PATH_SEARCH_TASK_TYPE_APPROVAL_PATH_SEARCH_BUTTON
                                                    ,
                                                    loginDTO
                                            )%>
                        </button>
                        <%
                            }
                        %>
                    </div>

                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="form-actions text-center">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM
                                        .
                                        getText
                                                (
                                                        LC
                                                                .
                                                                PI_REQUISITION_ADD_PI_REQUISITION_CANCEL_BUTTON
                                                        ,
                                                        loginDTO
                                                )%>
                            </button>
                            <%
                                if
                                (
                                        pi_requisitionDTO
                                                .
                                                status
                                                ==
                                                CommonApprovalStatus
                                                        .
                                                        REQUISITION_NOT_APPROVED
                                                                .
                                                        getValue
                                                                (
                                                                )
                                ) {
                            %>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM
                                        .
                                        getText
                                                (
                                                        LC
                                                                .
                                                                PI_REQUISITION_ADD_PI_REQUISITION_SUBMIT_BUTTON
                                                        ,
                                                        loginDTO
                                                )%>
                            </button>
                            <%
                                }
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<jsp:include page="../procurement_goods/procurementGoodsModal.jsp">
    <jsp:param name="index" value="1"/>
</jsp:include>
<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<script type="text/javascript">
    let itemData = new Map(); // TODO: init map with exisitng data
    function loadModal() {
        $('#search_proc_modal').modal();
    }

    $('#add-new-list-btn').on('click', function () {
        loadModal()
    });
    necessaryCollectionForProcurementModal = {
        callBackFunction: function (item) {
            if (item != undefined && item != null && item != '' && !(item instanceof Map)) {
                showSubCodesInTable(item);
            }

        }
    };

    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        const items = Array.from(itemData.values());

        for (let item of items) {
            console.log(item)
            console.log(document.getElementById('item_amount_' + item.iD))
            item.quantity = document.getElementById('item_amount_' + item.iD).value;
            itemData.set(Number(item.iD), item);
        }

        $('#productData').val(JSON.stringify(Array.from(itemData.values())));
        $('#applyTime_time_' + row).val(getTimeById('applyTime_js_' + row, true));
        preprocessDateBeforeSubmitting('applyDate', row);
        submitAddForm2();
        return false;
    }

    function updateDescriptionLen() {
        let len = $('#remarks_text').val().length;
        if (Number(len) <= 512) {
            $('#remarks_len').text(len + "/512");
            $('#remarks_len').css('color', 'black');
        } else {
            $('#remarks_len').text(len + "/512");
            $('#remarks_len').css('color', 'red');
        }

    }

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_requisitionServlet");
    }

    function init(row) {
        select2SingleSelector('#officeUnitId', '<%=Language%>');
        setDateByStringAndId('applyDate_js_' + row, $('#applyDate_date_' + row).val());
        setTimeById('applyTime_js_' + row, $('#applyTime_time_' + row).val(), true);
        <%
        for(Procurement_goodsResponseDTO procurement_goodsResponseDTO:procurement_goodsResponseDTOList){
        %>
        showSubCodesInTable(<%=new Gson().toJson(procurement_goodsResponseDTO)%>);
        <%}%>
    }

    let row = 0;
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    async function showSubCodesInTable(productDetail) {

        const productDetailId = Number(productDetail.iD);
        if (itemData.has(productDetailId)) return;
        itemData.set(productDetailId, productDetail);

        let organogram = document.getElementById("organogramId_hidden_0").value;
        if (organogram == null || organogram === '' || organogram === '-1') {
            showToastSticky("কোন কর্মকর্তা বাছাই করা হয়নি", "No employee selected");
            return;
        }


        const url = "Pi_requisitionServlet?actionType=getPreviousRequisitionItem&itemId=" + productDetail.iD + "&organogramId=" + organogram;

        const response = await fetch(url);
        const {pi_requisition_itemDTO, totalInStock} = await response.json();

        addDataInTable('product-view-table', productDetail, pi_requisition_itemDTO, totalInStock);


        const tableBody = document.querySelector('#product-view-table tbody');
        if (tableBody.innerText.trim() === '') {
            tableBody.innerHTML =
                '<tr><td colspan="7" class="text-center">'
                + '<%=LM.getText(LC.BUDGET_CODE_SELECTION_NO_UNSELECTED_ECONOMIC_CODE_FOUND, loginDTO)%>'
                + '</td></tr>';
        }
    }

    function keyDownEvent(e) {
        // const maxValue = document.getElementById(e.target.id.replace("item", "stock")).value;
        let isvalid = inputValidationForIntValue(e, $(this));
        return true == isvalid;
    }

    function getInputElement(id, value, isReadOnly, onKeyUp) {
        const inputElement = document.createElement('input');
        inputElement.classList.add('form-control');
        inputElement.type = 'text';
        inputElement.id = id;
        inputElement.value = value;
        inputElement.onkeydown = keyDownEvent;
        if (isReadOnly)
            inputElement.readOnly = isReadOnly;
        if (onKeyUp) inputElement.onkeyup = onKeyUp;
        return inputElement;
    }

    function addDataInTable(tableId, jsonData, prevData, totalInStock) {
        const table = document.getElementById(tableId);

        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.classList.remove('template-row');
        templateRow.id = 'product-details-' + jsonData.iD;

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-product-name').innerText = getValueByLanguage(language, jsonData.nameBn, jsonData.nameEn);
        templateRow.querySelector('td.row-data-product-type-name').innerText = jsonData.procurementGoodsTypeNameEn;
        // const stockAmount = templateRow.querySelector('td.row-data-stock-amount');
        // stockAmount.append(getInputElement('stock_amount_' + jsonData.iD, totalInStock, true));
        const amount = templateRow.querySelector('td.row-data-product-amount');
        <%if(pi_requisitionDTO.status==CommonApprovalStatus.REQUISITION_NOT_APPROVED.getValue()) {
                                        %>
        amount.append(getInputElement('item_amount_' + jsonData.iD, jsonData.quantity));
        <%}else{%>
        amount.innerHTML = "<span>" + jsonData.quantity + "</span>";
        <%}%>
        if (prevData.iD !== -1) {
            let applyDate = new Date(parseInt(prevData.apply_date))
            templateRow.querySelector('td.row-data-last-apply-date').innerText = applyDate.getDay().toString() + '/' + applyDate.getMonth() + '/' + applyDate.getFullYear().toString();
            templateRow.querySelector('td.row-data-last-requested-quantity').innerText = prevData.requestedQuantity;
            templateRow.querySelector('td.row-data-last-received-quantity').innerText = prevData.approvalTwoQuantity;
        }
        const tableBody = table.querySelector('tbody');
        tableBody.append(templateRow);
    }

    function getRowDataAndMoveRow(rowButtonElement, destTableId) {
        const containingRow = rowButtonElement.parentNode.parentNode;
        const rowData = JSON.parse(containingRow.dataset.rowData);
        if (destTableId) addDataInTable(destTableId, rowData);
        containingRow.remove();
        return rowData;
    }

    function unSelectRow(rowButtonElement) {
        const data = getRowDataAndMoveRow(rowButtonElement);
        itemData.delete(Number(data.iD));
    }

    /*Office unit modal start*/
    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#officeUnitId_modal_button').hide();
        $('#officeUnitId_div').show();
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id_input').val(selectedOffice.id);
        loadStockData(selectedOffice.id);
    }

    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnitId', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    function officeModalButtonClicked() {
        // console.log('Button Clicked!');
        officeSelectModalUsage = 'officeUnitId';
        $('#search_office_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    /*Office unit modal end*/

</script>