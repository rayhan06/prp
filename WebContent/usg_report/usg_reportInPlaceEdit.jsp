<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="usg_report.Usg_reportDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Usg_reportDTO usg_reportDTO = (Usg_reportDTO)request.getAttribute("usg_reportDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(usg_reportDTO == null)
{
	usg_reportDTO = new Usg_reportDTO();
	
}
System.out.println("usg_reportDTO = " + usg_reportDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

String servletName = "Usg_reportServlet";
String fileColumnName = "";
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.USG_REPORT_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=usg_reportDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_prescriptionLabTestId'>")%>
			

		<input type='hidden' class='form-control'  name='prescriptionLabTestId' id = 'prescriptionLabTestId_hidden_<%=i%>' value='<%=usg_reportDTO.prescriptionLabTestId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_prescriptionDetailsId'>")%>
			

		<input type='hidden' class='form-control'  name='prescriptionDetailsId' id = 'prescriptionDetailsId_hidden_<%=i%>' value='<%=usg_reportDTO.prescriptionDetailsId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_commonLabReportId'>")%>
			

		<input type='hidden' class='form-control'  name='commonLabReportId' id = 'commonLabReportId_hidden_<%=i%>' value='<%=usg_reportDTO.commonLabReportId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_appointmentId'>")%>
			

		<input type='hidden' class='form-control'  name='appointmentId' id = 'appointmentId_hidden_<%=i%>' value='<%=usg_reportDTO.appointmentId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_name'>")%>
			
	
	<div class="form-inline" id = 'name_div_<%=i%>'>
		<input type='text' class='form-control'  name='name' id = 'name_text_<%=i%>' value='<%=usg_reportDTO.name%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_dateOfBirth'>")%>
			
	
	<div class="form-inline" id = 'dateOfBirth_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'dateOfBirth_date_<%=i%>' name='dateOfBirth' value=	<%
	String formatted_dateOfBirth = dateFormat.format(new Date(usg_reportDTO.dateOfBirth));
	%>
	'<%=formatted_dateOfBirth%>'
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_referredByUserName'>")%>
			
	
	<div class="form-inline" id = 'referredByUserName_div_<%=i%>'>
		<input type='text' class='form-control'  name='referredByUserName' id = 'referredByUserName_text_<%=i%>' value='<%=usg_reportDTO.referredByUserName%>' <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="^[A-Za-z0-9]{5,}" title="referredByUserName must contain at least 5 letters"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_genderCat'>")%>
			
	
	<div class="form-inline" id = 'genderCat_div_<%=i%>'>
		<select class='form-control'  name='genderCat' id = 'genderCat_category_<%=i%>'   tag='pb_html'>		
<%
Options = CatDAO.getOptions(Language, "gender", usg_reportDTO.genderCat);
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_ultrasonoTypeType'>")%>
			
	
	<div class="form-inline" id = 'ultrasonoTypeType_div_<%=i%>'>
		<select class='form-control'  name='ultrasonoTypeType' id = 'ultrasonoTypeType_select_<%=i%>'   tag='pb_html'>
<%
Options = CommonDAO.getOptions(Language, "ultrasono_type", usg_reportDTO.ultrasonoTypeType);
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_comment'>")%>
			
	
	<div class="form-inline" id = 'comment_div_<%=i%>'>
		<input type='text' class='form-control'  name='comment' id = 'comment_text_<%=i%>' value='<%=usg_reportDTO.comment%>'   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_doctorId'>")%>
			

		<input type='hidden' class='form-control'  name='doctorId' id = 'doctorId_hidden_<%=i%>' value='<%=usg_reportDTO.doctorId%>' tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=usg_reportDTO.insertedByUserId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByOrganogramId" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=usg_reportDTO.insertedByOrganogramId%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=usg_reportDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_searchColumn" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=usg_reportDTO.searchColumn%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=usg_reportDTO.isDeleted%>' tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=usg_reportDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Usg_reportServlet?actionType=view&ID=<%=usg_reportDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Usg_reportServlet?actionType=view&modal=1&ID=<%=usg_reportDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	