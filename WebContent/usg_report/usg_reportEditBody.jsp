<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>

<%@page import="usg_report.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%@page import="prescription_details.*"%>
<%@page import="appointment.*"%>
<%@page import="ultrasono_type.*"%>
<%@page import="xray_report.*"%>


<%
Usg_reportDTO usg_reportDTO;
usg_reportDTO = (Usg_reportDTO)request.getAttribute("usg_reportDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
Prescription_detailsDAO prescription_detailsDAO = new Prescription_detailsDAO();
PrescriptionLabTestDAO prescriptionLabTestDAO = new PrescriptionLabTestDAO();
AppointmentDAO appointmentDAO = new AppointmentDAO();
String Language = LM.getText(LC.USG_REPORT_EDIT_LANGUAGE, loginDTO);
boolean isXray = false;
if(usg_reportDTO == null)
{
	usg_reportDTO = new Usg_reportDTO();
	if(request.getParameter("prescription_lab_test_id") != null)
	{		 
		usg_reportDTO.prescriptionLabTestId =  Long.parseLong(request.getParameter("prescription_lab_test_id"));
		PrescriptionLabTestDTO prescriptionLabTestDTO = prescriptionLabTestDAO.getDTOByID(usg_reportDTO.prescriptionLabTestId);
		Prescription_detailsDTO prescription_detailsDTO = prescription_detailsDAO.getDTOByID(prescriptionLabTestDTO.prescriptionDetailsId);
		AppointmentDTO appointmentDTO = appointmentDAO.getDTOByID(prescription_detailsDTO.appointmentId);
		
		usg_reportDTO.appointmentId = appointmentDTO.iD;
		usg_reportDTO.prescriptionDetailsId = prescription_detailsDTO.iD;
		usg_reportDTO.prescriptionLabTestId = prescriptionLabTestDTO.iD;
		
		usg_reportDTO.name = appointmentDTO.patientName;
		usg_reportDTO.referredByUserName = WorkflowController.getNameFromOrganogramId(appointmentDTO.doctorId, Language);
		usg_reportDTO.doctorId = userDTO.organogramID;
		usg_reportDTO.genderCat = appointmentDTO.genderCat;
		usg_reportDTO.dateOfBirth = appointmentDTO.dateOfBirth;
		if(prescriptionLabTestDTO.labTestType == SessionConstants.LAB_TEST_ULTRASONOGRAM)
		{
			usg_reportDTO.doctorRecommendation = "Ultrasonogram of " + prescriptionLabTestDTO.description;
		}
		else
		{
			usg_reportDTO.doctorRecommendation = "X-Ray of " + prescriptionLabTestDTO.description;
			isXray = true;
		}
		
	}
	
}
System.out.println("usg_reportDTO = " + usg_reportDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.USG_REPORT_ADD_USG_REPORT_ADD_FORMNAME, loginDTO);
if(isXray)
{
	formTitle = LM.getText(LC.XRAY_REPORT_ADD_XRAY_REPORT_ADD_FORMNAME, loginDTO);
}
String servletName = "Usg_reportServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;

String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
Ultrasono_typeDAO ultrasono_typeDAO = new Ultrasono_typeDAO();
List<Ultrasono_typeDTO> ultrasono_typeDTOs = ultrasono_typeDAO.getDTOsByGenderCat(usg_reportDTO.genderCat);

%>

<style>
.ck-editor__editable {
    min-height: 500px !important;
}
#cke_comment_text_0
{
	height:480px;
}
#cke_1_contents
{
	height:400px !important;
}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Usg_reportServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="onlyborder">
                                    <div class="row">

                                        <div class="col-md-11">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="sub_title_top">
                                                        <div class="sub_title">
                                                            <h4 style="background: white"><%=formTitle%>
                                                            </h4>
                                                        </div>
                                                     </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=usg_reportDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='prescriptionLabTestId' id = 'prescriptionLabTestId_hidden_<%=i%>' value='<%=usg_reportDTO.prescriptionLabTestId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='prescriptionDetailsId' id = 'prescriptionDetailsId_hidden_<%=i%>' value='<%=usg_reportDTO.prescriptionDetailsId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='commonLabReportId' id = 'commonLabReportId_hidden_<%=i%>' value='<%=usg_reportDTO.commonLabReportId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='appointmentId' id = 'appointmentId_hidden_<%=i%>' value='<%=usg_reportDTO.appointmentId%>' tag='pb_html'/>
														
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.PRESCRIPTION_DETAILS_ADD_ADVICE, loginDTO)%></label>
                                                            <div class="col-md-9">
																<input type='text' class='form-control' readonly  name='doctorRecommendation' id = 'doctorRecommendation_text_<%=i%>' value='<%=usg_reportDTO.doctorRecommendation%>'   tag='pb_html'/>					
															</div>
                                                      </div>
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.USG_REPORT_ADD_NAME, loginDTO)%></label>
                                                            <div class="col-md-9">
																<input type='text' class='form-control' readonly  name='name' id = 'name_text_<%=i%>' value='<%=usg_reportDTO.name%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.USG_REPORT_ADD_DATEOFBIRTH, loginDTO)%></label>
                                                            <div class="col-md-9">
																<input type='text' class='form-control' readonly data-label="Document Date" id = 'dateOfBirth_date_<%=i%>' name='dateOfBirth' value=	<%
																String formatted_dateOfBirth = dateFormat.format(new Date(usg_reportDTO.dateOfBirth));
																%>
																'<%=formatted_dateOfBirth%>'
															   tag='pb_html'>															
															 </div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.USG_REPORT_ADD_REFERREDBYUSERNAME, loginDTO)%></label>
                                                            <div class="col-md-9">
																<input type='text' class='form-control' readonly  name='referredByUserName' id = 'referredByUserName_text_<%=i%>' value='<%=usg_reportDTO.referredByUserName%>' 	tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.USG_REPORT_ADD_GENDERCAT, loginDTO)%></label>
                                                            <div class="col-md-9">
																<input type='text' class='form-control' readonly  name='gender' id = 'gender_text_<%=i%>' value='<%=CatDAO.getName(Language, "gender", usg_reportDTO.genderCat)%>'   tag='pb_html'/>		
																<input type='hidden' class='form-control' readonly  name='genderCat' id = 'gender_hidden_<%=i%>' value='<%=usg_reportDTO.genderCat%>'   tag='pb_html'/>	
	
															</div>
                                                      </div>
                                                      
                                                      <%
														if(isXray)
														{
														%>
														<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.HM_X_RAY_TYPE, loginDTO)%></label>
                                                            <div class="col-md-9">
																<select class='form-control' name='xRayType' id = 'xRayType' onchange="getXraySubtype(this.value, 'xRaySubType')"   tag='pb_html'>
																<%
					                                              List<Xray_reportDTO> xray_reportDTOs = Xray_reportRepository.getInstance().getXray_reportList();
					                                              for(Xray_reportDTO xray_reportDTO: xray_reportDTOs)
					                                              {	
					                                            	  if(!xray_reportDTO.xrayReportDetailsDTOList.isEmpty())
					                                            	  {
					                                            	  %>
					                                            	  <option value = "<%=xray_reportDTO.iD%>" ><%=xray_reportDTO.comment%></option>
					                                            	  <%
					                                            	  }
					                                              }
					                                              %>
																</select>
		
															</div>
                                                      </div>									
														<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.HM_X_RAY_SUBTYPE, loginDTO)%></label>
                                                            <div class="col-md-9">
																<select class='form-control' name='xRaySubType' id = 'xRaySubType' onchange="getXRayComment(this.value, 'comment_text_<%=i%>')"   tag='pb_html'>
																<%
																Options = CommonDAO.getOptions(Language, "xray_report_details", usg_reportDTO.xRaySubType);
																%>
																<%=Options%>
																</select>					
															</div>
                                                      </div>
                                                      
                                                      <input type="hidden" name='ultrasonoTypeType' id = 'ultrasonoTypeType_select_<%=i%>' value='-1'  tag='pb_html'/>
														<%
														}
														else
														{
														%>	
														
													<input type="hidden" name='xRayType' id = 'xRayType_select_<%=i%>' value='-1'  tag='pb_html'/>
													<input type="hidden" name='xRaySubType' id = 'xRaySubType_select_<%=i%>' value='-1'  tag='pb_html'/>								
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.USG_REPORT_ADD_ULTRASONOTYPETYPE, loginDTO)%></label>
                                                            <div class="col-md-9">
																<select class='form-control' required name='ultrasonoTypeType' id = 'ultrasonoTypeType_select_<%=i%>' onchange="getComment(this.value, 'comment_text_<%=i%>')"   tag='pb_html'>
																<option value="" ><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
																	<%
																	for(Ultrasono_typeDTO ultrasono_typeDTO: ultrasono_typeDTOs)
																	{
																		%>
																		<option value='<%=ultrasono_typeDTO.iD%>' <%=usg_reportDTO.ultrasonoTypeType ==  ultrasono_typeDTO.iD ?"selected":""%>><%=ultrasono_typeDTO.nameEn%></option>
																		<%
																	}
																	%>
																</select>
																
		
															</div>
                                                      </div>
                                                      <%
														}
                                                      %>									
													<div class="form-group row">
                                                            <label class="col-md-3 col-form-label text-right"><%=LM.getText(LC.USG_REPORT_ADD_COMMENT, loginDTO)%></label>
                                                            <div class="col-md-9">
																<textarea class='form-control'  name='comment' id = 'comment_text_<%=i%>' value='<%=usg_reportDTO.comment%>'   tag='pb_html'><%=usg_reportDTO.comment%></textarea>					
															</div>
                                                      </div>
                                                      									
														<input type='hidden' class='form-control'  name='doctorId' id = 'doctorId_hidden_<%=i%>' value='<%=usg_reportDTO.doctorId%>' tag='pb_html'/>
												

														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=usg_reportDTO.insertedByUserId%>' tag='pb_html'/>
																								
												
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=usg_reportDTO.insertedByOrganogramId%>' tag='pb_html'/>
																								
												
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=usg_reportDTO.insertionDate%>' tag='pb_html'/>
																								
												
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=usg_reportDTO.searchColumn%>' tag='pb_html'/>
																								
												
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=usg_reportDTO.isDeleted%>' tag='pb_html'/>
																							
																								
												
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=usg_reportDTO.lastModificationTime%>' tag='pb_html'/>													<div class="form-group row">
												       								
																		
														</div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>                               
                          </div>
                       </div>
                       <div class="form-actions text-center mb-5">
	                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.USG_REPORT_ADD_USG_REPORT_CANCEL_BUTTON, loginDTO)%></a>										
							<button class="btn btn-success" type="submit"><%=LM.getText(LC.USG_REPORT_ADD_USG_REPORT_SUBMIT_BUTTON, loginDTO)%></button>
					   </div>
                   </form>
               </div>                      
          </div>
      </div>
 </div>

<script type="text/javascript">

$(document).ready( function(){

    dateTimeInit("<%=Language%>");
	$("#xRayType").select2({
        dropdownAutoWidth: true,
        theme: "classic"
    });
	var value = $("#xRayType").val();
	var child = "xRaySubType";
	getXraySubtype(value, child);
	CKEDITOR.replaceAll();
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Usg_reportServlet");	
}


var row = 0;
	
window.onload =function ()
{
	
}

var child_table_extra_id = <%=childTableStartingID%>;

function getComment(value, comment)
{
	console.log( 'changed value: ' +  value );
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) 
		{
			console.log(this.responseText);
			CKEDITOR.instances[comment].setData(this.responseText)
		}
		else
		{
			console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
		}
	};

	xhttp.open("POST", "Ultrasono_typeServlet?actionType=getComment&ID=" + value, true);
	xhttp.send();
}

function getXRayComment(value, comment)
{
	console.log( 'changed value: ' +  value );
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) 
		{
			console.log(this.responseText);
			CKEDITOR.instances[comment].setData(this.responseText)
		}
		else
		{
			console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
		}
	};

	xhttp.open("POST", "Xray_reportServlet?actionType=getComment&ID=" + value, true);
	xhttp.send();
}

function getXraySubtype(value, child)
{
	console.log( 'changed value: ' +  value );
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) 
		{
			console.log(this.responseText);
			$("#" + child).html(this.responseText)
		}
		else
		{
			console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
		}
	};

	xhttp.open("POST", "Xray_reportServlet?actionType=getSubType&type=" + value + "&language=<%=Language%>", true);
	xhttp.send();
}
</script>






