<%@page pageEncoding="UTF-8" %>

<%@page import="usg_report.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.USG_REPORT_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_USG_REPORT;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Usg_reportDTO usg_reportDTO = (Usg_reportDTO)request.getAttribute("usg_reportDTO");
CommonDTO commonDTO = usg_reportDTO;
String servletName = "Usg_reportServlet";


System.out.println("usg_reportDTO = " + usg_reportDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Usg_reportDAO usg_reportDAO = (Usg_reportDAO)request.getAttribute("usg_reportDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_prescriptionLabTestId'>
											<%
											value = usg_reportDTO.prescriptionLabTestId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_prescriptionDetailsId'>
											<%
											value = usg_reportDTO.prescriptionDetailsId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_commonLabReportId'>
											<%
											value = usg_reportDTO.commonLabReportId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_appointmentId'>
											<%
											value = usg_reportDTO.appointmentId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_name'>
											<%
											value = usg_reportDTO.name + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_dateOfBirth'>
											<%
											value = usg_reportDTO.dateOfBirth + "";
											%>
											<%
											String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_dateOfBirth, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_referredByUserName'>
											<%
											value = usg_reportDTO.referredByUserName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_genderCat'>
											<%
											value = usg_reportDTO.genderCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "gender", usg_reportDTO.genderCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_ultrasonoTypeType'>
											<%
											value = usg_reportDTO.ultrasonoTypeType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "ultrasono_type", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_comment'>
											<%
											value = usg_reportDTO.comment + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_doctorId'>
											<%
											value = usg_reportDTO.doctorId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	

											<td>
												<a href='Usg_reportServlet?actionType=view&ID=<%=usg_reportDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Usg_reportServlet?actionType=getEditPage&ID=<%=usg_reportDTO.iD%>'><%=LM.getText(LC.USG_REPORT_SEARCH_USG_REPORT_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=usg_reportDTO.iD%>'/></span>
												</div>
											</td>
																						
											

