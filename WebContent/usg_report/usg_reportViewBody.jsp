

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="usg_report.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>




<%
String servletName = "Usg_reportServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.USG_REPORT_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Usg_reportDAO usg_reportDAO = new Usg_reportDAO("usg_report");
Usg_reportDTO usg_reportDTO = usg_reportDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title"><%=LM.getText(LC.USG_REPORT_ADD_USG_REPORT_ADD_FORMNAME, loginDTO)%></h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.USG_REPORT_ADD_USG_REPORT_ADD_FORMNAME, loginDTO)%></h3>
						<table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_PRESCRIPTIONLABTESTID, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.prescriptionLabTestId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_PRESCRIPTIONDETAILSID, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.prescriptionDetailsId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_COMMONLABREPORTID, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.commonLabReportId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_APPOINTMENTID, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.appointmentId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_NAME, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.name + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_DATEOFBIRTH, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.dateOfBirth + "";
											%>
											<%
											String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_dateOfBirth, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_REFERREDBYUSERNAME, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.referredByUserName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_GENDERCAT, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.genderCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "gender", usg_reportDTO.genderCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_ULTRASONOTYPETYPE, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.ultrasonoTypeType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "ultrasono_type", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_COMMENT, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.comment + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.USG_REPORT_ADD_DOCTORID, loginDTO)%></b></td>
								<td>
						
											<%
											value = usg_reportDTO.doctorId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			
			
			
			
			
			
		
						</table>
                    </div>
			






			</div>	

               


        </div>
	</div>