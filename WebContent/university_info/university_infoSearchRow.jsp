<%@page pageEncoding="UTF-8" %>

<%@page import="university_info.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.UNIVERSITY_INFO_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_UNIVERSITY_INFO;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    University_infoDTO university_infoDTO = (University_infoDTO) request.getAttribute("university_infoDTO");
    CommonDTO commonDTO = university_infoDTO;
    String servletName = "University_infoServlet";


    System.out.println("university_infoDTO = " + university_infoDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    University_infoDAO university_infoDAO = (University_infoDAO) request.getAttribute("university_infoDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_universityNameEn'>
    <%
        value = university_infoDTO.universityNameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_universityNameBn'>
    <%
        value = university_infoDTO.universityNameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <a href='University_infoServlet?actionType=view&ID=<%=university_infoDTO.id%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
    </a>

</td>

<td id='<%=i%>_Edit'>

    <a href='University_infoServlet?actionType=getEditPage&ID=<%=university_infoDTO.id%>'><%=LM.getText(LC.UNIVERSITY_INFO_SEARCH_UNIVERSITY_INFO_EDIT_BUTTON, loginDTO)%>
    </a>

</td>


<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=university_infoDTO.id%>'/></span>
    </div>
</td>
																						
											

