
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="common.NameDTO" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.UNIVERSITY_INFO_EDIT_LANGUAGE, loginDTO);


String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>	

<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.UNIVERSITY_INFO_EDIT_UNIVERSITYNAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.UNIVERSITY_INFO_EDIT_UNIVERSITYNAMEBN, loginDTO)%></th>
															
							</tr>
						</thead>
						<tbody>
						<%
								ArrayList data = (ArrayList) session.getAttribute("university_infoDTOs");

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											System.out.println("In jsp, parsed dto = " + data.get(i));
											NameDTO university_infoDTO =  (NameDTO)data.get(i);
											long  ID = university_infoDTO.iD;
											out.println("<tr id = 'tr_" + i + "'>");
						%>



























	
















			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=university_infoDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_universityNameEn'>")%>
			
	
	<div class="form-inline" id = 'universityNameEn_div_<%=i%>'>
		<input type='text' class='form-control'  name='universityNameEn' id = 'universityNameEn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + university_infoDTO.nameEn + "'"):("'" + "" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_universityNameBn'>")%>
			
	
	<div class="form-inline" id = 'universityNameBn_div_<%=i%>'>
		<input type='text' class='form-control'  name='universityNameBn' id = 'universityNameBn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + university_infoDTO.nameBn + "'"):("'" + "" + "'")%>
   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + university_infoDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=university_infoDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
							<%					
											out.println("</tr>");
										}
									}
								}
								catch(Exception ex)
								{
									ex.printStackTrace();
								}
						%>
						</tbody>
					</table>
</div>