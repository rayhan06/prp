
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="university_info.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="common.NameDTO" %>

<%
NameDTO university_infoDTO;
university_infoDTO = (NameDTO)request.getAttribute("university_infoDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(university_infoDTO == null)
{
	university_infoDTO = new NameDTO();
	
}
System.out.println("university_infoDTO = " + university_infoDTO);

String actionName = "upload";
System.out.println("actionType = " + request.getParameter("actionType"));

String formTitle = LM.getText(LC.UNIVERSITY_INFO_UPLOAD_UNIVERSITY_INFO_UPLOAD_FORMNAME, loginDTO);;
String Language = LM.getText(LC.UNIVERSITY_INFO_UPLOAD_UPLOAD, loginDTO);


String value = "";

%>


<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal"
		id="bigform1" name="bigform"  method="POST" enctype = "multipart/form-data">
			<div class="form-body">
			
					<div class="form-actions text-center">
					<label class="col-lg-3 control-label">
						<%=LM.getText(LC.UNIVERSITY_INFO_UPLOAD_CHOOSE_FILE, loginDTO)%>
					</label>
					<div class="form-group ">					
						<div class="col-lg-6 " id = 'university_infoDatabase_div'>	
							<input type='file' class='form-control'  name='university_infoDatabase' id = 'university_infoDatabase' />	
											
						</div>
						<a class="btn btn-success" style="color:white;" onclick = "uploadFile();">
							<%=LM.getText(LC.UNIVERSITY_INFO_UPLOAD_UPLOAD, loginDTO)%>
						</a>
					</div>	
					
					
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<form class="form-horizontal" action="University_infoServlet?actionType=uploadConfirmed"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
<div id='reviewDiv'>
</div>

<button class='btn btn-success' type="submit" style="display:none" id="submitButton">
	<%=LM.getText(LC.UNIVERSITY_INFO_UPLOAD_UNIVERSITY_INFO_SUBMIT_BUTTON, loginDTO)%>
</button>

</form>

<script type="text/javascript">


function uploadFile()
{
	console.log('submitAjax called');

	var formData = new FormData();
	var value;
	
	console.log('uploadFile called');

	formData.append('university_infoDatabase', document.getElementById('university_infoDatabase').files[0]);


	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				document.getElementById('reviewDiv').innerHTML = this.responseText ;
				document.getElementById('submitButton').style = "display:inline" ;
				ShowExcelParsingResult(i);
			}
			else
			{
				console.log("No Response");
				document.getElementById('reviewDiv').innerHTML = this.responseText ;
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	xhttp.open("POST", 'University_infoServlet?actionType=upload', true);
	xhttp.send(formData);
}



$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "University_infoServlet");	
}

function init(row)
{


	
}


</script>