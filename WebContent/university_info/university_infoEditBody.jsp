<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="university_info.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%
    University_infoDTO university_infoDTO;
    university_infoDTO = (University_infoDTO) request.getAttribute("university_infoDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (university_infoDTO == null) {
        university_infoDTO = new University_infoDTO();

    }
    System.out.println("university_infoDTO = " + university_infoDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.UNIVERSITY_INFO_ADD_UNIVERSITY_INFO_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal"
              action="University_infoServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="form-body">


                <%@ page import="java.text.SimpleDateFormat" %>
                <%@ page import="java.util.Date" %>

                <%@ page import="pb.*" %>

                <%
                    String Language = LM.getText(LC.UNIVERSITY_INFO_EDIT_LANGUAGE, loginDTO);
                    String Options;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = new Date();
                    String datestr = dateFormat.format(date);
                    CommonDAO.language = Language;
                    CatDAO.language = Language;
                %>


                <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>'
                       value='<%=university_infoDTO.id%>' tag='pb_html'/>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.UNIVERSITY_INFO_ADD_UNIVERSITYNAMEEN, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='universityNameEn_div_<%=i%>'>
                        <input type='text' class='form-control' name='universityNameEn'
                               id='universityNameEn_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + university_infoDTO.universityNameEn + "'"):("'" + "" + "'")%>
                                       tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=LM.getText(LC.UNIVERSITY_INFO_ADD_UNIVERSITYNAMEBN, loginDTO)%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='universityNameBn_div_<%=i%>'>
                        <input type='text' class='form-control' name='universityNameBn'
                               id='universityNameBn_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + university_infoDTO.universityNameBn + "'"):("'" + "" + "'")%>
                                       tag='pb_html'/>
                    </div>
                </div>


                <input type='hidden' class='form-control' name='insertionDate' id='insertionDate_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + university_infoDTO.insertionDate + "'"):("'" + "0" + "'")%>
                               tag='pb_html'/>


                <input type='hidden' class='form-control' name='insertedBy' id='insertedBy_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + university_infoDTO.insertedBy + "'"):("'" + "" + "'")%>
                               tag='pb_html'/>


                <input type='hidden' class='form-control' name='modifiedBy' id='modifiedBy_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + university_infoDTO.modifiedBy + "'"):("'" + "" + "'")%>
                               tag='pb_html'/>


                <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + university_infoDTO.isDeleted + "'"):("'" + "false" + "'")%>
                               tag='pb_html'/>

                <input type='hidden' class='form-control' name='lastModificationTime'
                       id='lastModificationTime_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + university_infoDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                               tag='pb_html'/>


                <div class="form-actions text-center">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                        <%=LM.getText(LC.UNIVERSITY_INFO_ADD_UNIVERSITY_INFO_CANCEL_BUTTON, loginDTO)%>
                    </a>
                    <button class="btn btn-success" type="submit">

                        <%=LM.getText(LC.UNIVERSITY_INFO_ADD_UNIVERSITY_INFO_SUBMIT_BUTTON, loginDTO)%>

                    </button>
                </div>

            </div>

        </form>

    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    $(document).ready(function () {

        dateTimeInit("<%=Language%>");
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "University_infoServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






