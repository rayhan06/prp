<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="theme.ThemeDTO" %>
<%@page import="java.util.ArrayList" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%
    ThemeDTO themeDTO;
    themeDTO = (ThemeDTO) request.getAttribute("themeDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (themeDTO == null) {
        themeDTO = new ThemeDTO();

    }
    System.out.println("themeDTO = " + themeDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.THEME_EDIT_THEME_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.THEME_ADD_THEME_ADD_FORMNAME, loginDTO);
    }
    String fieldError = "";
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
%>
<style>
    .ul-box {
        width: 65%;
        margin-left: 17%;
        margin-bottom: 1%;
        background-color: #eee;
        padding-top: 40px;
        padding-bottom: 30px;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" action="ThemeServlet?actionType=<%=actionName%>&identity=<%=ID%>"
              id="bigform" name="bigform" method="POST">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' value='<%=ID%>'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%
                                                if (actionName.equals("edit")) {
                                                    out.print(LM.getText(LC.THEME_EDIT_THEMENAME, loginDTO));
                                                } else {
                                                    out.print(LM.getText(LC.THEME_ADD_THEMENAME, loginDTO));
                                                }
                                            %>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 ">
                                            <input type='text' class='form-control' name='themeName'
                                                   id='themeName_text' value=
                                                    <%
                                                        if (actionName.equals("edit")) {
                                                            out.print("'" + themeDTO.themeName + "'");
                                                        } else {
                                                            out.print("'" + " " + "'");
                                                        }
                                                    %>/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%
                                                if (actionName.equals("edit")) {
                                                    out.print(LM.getText(LC.THEME_EDIT_DIRECTORY, loginDTO));
                                                } else {
                                                    out.print(LM.getText(LC.THEME_ADD_DIRECTORY, loginDTO));
                                                }
                                            %>
                                        </label>
                                        <div class="col-md-9 ">
                                            <input type='text' class='form-control' name='directory'
                                                   id='directory_text' value=
                                                    <%
                                                        if (actionName.equals("edit")) {
                                                            out.print("'" + themeDTO.directory + "'");
                                                        } else {
                                                            out.print("'" + " " + "'");
                                                        }
                                                    %>/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%
                                                if (actionName.equals("edit")) {
                                                    out.print(LM.getText(LC.THEME_EDIT_ISAPPLIED, loginDTO));
                                                } else {
                                                    out.print(LM.getText(LC.THEME_ADD_ISAPPLIED, loginDTO));
                                                }
                                            %>
                                        </label>
                                        <div class="col-md-9 ">
                                            <input type='checkbox' class='form-control-sm mt-1' name='isApplied'
                                                   id='isApplied_checkbox'
                                                   value='true'
                                                <%if(actionName.equals("edit") && String.valueOf(themeDTO.isApplied).equals("true")){out.print("checked");}%>
                                            ><br>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-9 ">
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   value='<%=themeDTO.isDeleted%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>">
                            <%
                                if (actionName.equals("edit")) {
                                    out.print(LM.getText(LC.THEME_EDIT_THEME_CANCEL_BUTTON, loginDTO));
                                } else {
                                    out.print(LM.getText(LC.THEME_ADD_THEME_CANCEL_BUTTON, loginDTO));
                                }

                            %>
                        </a>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2" type="submit">
                            <%
                                if (actionName.equals("edit")) {
                                    out.print(LM.getText(LC.THEME_EDIT_THEME_SUBMIT_BUTTON, loginDTO));
                                } else {
                                    out.print(LM.getText(LC.THEME_ADD_THEME_SUBMIT_BUTTON, loginDTO));
                                }
                            %>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    var geo_lastClickedValue = [];

    function DoSubmit() {
        return true;
    }

    function addrselected(value, htmlID, name, selectedIndex, tagname) {
        try {
            geo_lastClickedValue[tagname] = value;
            var elements = document.getElementsByClassName("form-control");
            var ids = '';
            for (var i = elements.length - 1; i >= 0; i--) {
                var elemID = elements[i].id;
                if (elemID.includes(tagname + "_geoSelectField") && elemID > htmlID) {
                    ids += elements[i].id + ' ';

                    for (var j = elements[i].options.length - 1; j >= 0; j--) {

                        elements[i].options[j].remove();
                    }
                    elements[i].remove();

                }
            }


            var newid = htmlID + '_1';

            document.getElementById(tagname + "_geodiv").innerHTML += "<select class='form-control' name='" + tagname + "' id = '" + newid
                + "' onChange='addrselected(this.value, this.id, this.name, this.selectedIndex, this.name)'></select>";
            document.getElementById(htmlID).options[0].innerHTML = document.getElementById(htmlID).options[selectedIndex].innerHTML;
            document.getElementById(htmlID).options[0].value = document.getElementById(htmlID).options[selectedIndex].value;

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if (!this.responseText.includes('option')) {
                        document.getElementById(newid).remove();
                    } else {
                        document.getElementById(newid).innerHTML = this.responseText;
                    }

                } else if (this.readyState == 4 && this.status != 200) {
                    alert('failed ' + this.status);
                }
            };

            var redirect = "geolocation/geoloc.jsp?myID=" + value;

            xhttp.open("GET", redirect, true);
            xhttp.send();
        } catch (err) {
            alert("got error: " + err);
        }

    }


    window.onload = function () {
        if (<%=actionName.equals("edit")%>) {
        } else {
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "geolocation/geoloc.jsp?myID=1", true);
        xhttp.send();

    }
    // bkLib.onDomLoaded(function()
    // {
    // 	// // 	// // 	// // 	// // 	// // // });
</script>





