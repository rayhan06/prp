<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="payroll_deduction_lookup.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="tax_deduction_configuration.Tax_deduction_configurationRepository" %>

<%
    String navigator2 = "navPAYROLL_DEDUCTION_LOOKUP";
    String servletName = "Payroll_deduction_lookupServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th>
                <%=LM.getText(LC.PAYROLL_DEDUCTION_LOOKUP_ADD_EMPLOYMENTCAT, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.PAYROLL_DEDUCTION_LOOKUP_ADD_ORGANOGRAMID, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.PAYROLL_DEDUCTION_LOOKUP_ADD_TAXDEDUCTIONCONFIGID, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.PAYROLL_DEDUCTION_LOOKUP_ADD_AMOUNT, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Payroll_deduction_lookupDTO> data = (List<Payroll_deduction_lookupDTO>) rn2.list;
            try {
                if (data != null) {
                    for (Payroll_deduction_lookupDTO payroll_deduction_lookupDTO : data) {
        %>
        <tr>
            <td>
                <%=CatRepository.getInstance().getText(Language, "employment", payroll_deduction_lookupDTO.employmentCat)%>
            </td>

            <td>
                <%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, payroll_deduction_lookupDTO.organogramId)%>
            </td>

            <td>
                <%=Tax_deduction_configurationRepository.getInstance().getDeductionText(
                        payroll_deduction_lookupDTO.taxDeductionConfigId,
                        Language
                )%>
            </td>

            <td>
                <%=Utils.getDigits(payroll_deduction_lookupDTO.amount, Language)%>
            </td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>