<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="daily_basis_messenger.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navDAILY_BASIS_MESSENGER";
    String servletName = "Daily_basis_messengerServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_NAME, loginDTO)%></th>
            <th><%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_FATHERNAME, loginDTO)%></th>
            <th><%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_DAILYRATE, loginDTO)%></th>
            <th><%=LM.getText(LC.HM_PHONE, loginDTO)%></th>
            <th><%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_BANKACCOUNTNO, loginDTO)%></th>
            <th><%=LM.getText(LC.DAILY_BASIS_MESSENGER_SEARCH_DAILY_BASIS_MESSENGER_EDIT_BUTTON, loginDTO)%></th>
            <th class="">
                <div class="text-center">
                    <span><%=UtilCharacter.getDataByLanguage(Language, "সব", "All")%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Daily_basis_messengerDTO> data = (List<Daily_basis_messengerDTO>) rn2.list;
            try {
                if (data != null) {
                    for (Daily_basis_messengerDTO daily_basis_messengerDTO : data) {
        %>
        <tr>
            <td>
                <%=daily_basis_messengerDTO.name%>
            </td>

            <td>
                <%=daily_basis_messengerDTO.fatherName%>
            </td>

            <td>
                <%=Utils.getDigits(daily_basis_messengerDTO.dailyRate, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(daily_basis_messengerDTO.mobileNo, Language)%>
            </td>

            <td>
                <%=Utils.getDigits(daily_basis_messengerDTO.bankAccountNo, Language)%>
            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=daily_basis_messengerDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
            </td>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=daily_basis_messengerDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>