

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="daily_basis_messenger.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Daily_basis_messengerServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Daily_basis_messengerDTO daily_basis_messengerDTO = Daily_basis_messengerDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = daily_basis_messengerDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_DAILY_BASIS_MESSENGER_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_DAILY_BASIS_MESSENGER_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_NAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=daily_basis_messengerDTO.name%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_FATHERNAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=daily_basis_messengerDTO.fatherName%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_DAILYRATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(daily_basis_messengerDTO.dailyRate, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
										<%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=daily_basis_messengerDTO.mobileNo%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_BANKACCOUNTNO, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=daily_basis_messengerDTO.bankAccountNo%>
                                    </div>
                                </div>
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>