<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="daily_basis_messenger.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Daily_basis_messengerDTO daily_basis_messengerDTO = new Daily_basis_messengerDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        daily_basis_messengerDTO = Daily_basis_messengerDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = daily_basis_messengerDTO;
    String tableName = "daily_basis_messenger";
    String context = request.getContextPath() + "/";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_DAILY_BASIS_MESSENGER_ADD_FORMNAME, loginDTO);
    String servletName = "Daily_basis_messengerServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Daily_basis_messengerServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=daily_basis_messengerDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_NAME, loginDTO)%> * </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='name' id='name_text_<%=i%>'
                                                   value='<%=daily_basis_messengerDTO.name%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_FATHERNAME, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='fatherName'
                                                   id='fatherName_text_<%=i%>'
                                                   value='<%=daily_basis_messengerDTO.fatherName%>' tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_DAILYRATE, loginDTO)%> * </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='dailyRate'
                                                   data-only-integer="true"
                                                   id='dailyRate_text_<%=i%>'
                                                   value='<%=daily_basis_messengerDTO.dailyRate%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><%=LM.getText(LC.HM_EIGHT_EIGHT, loginDTO)%>
                                                    </div>
                                                </div>
                                                <input type='text' class='form-control' name='mobileNo'
                                                       id='mobileNo_phone_<%=i%>'
                                                       value='<%=Utils.getPhoneNumberWithout88(daily_basis_messengerDTO.mobileNo, Language)%>'
                                                       tag='pb_html'>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_BANKACCOUNTNO, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='bankAccountNo'
                                                   id='bankAccountNo_text_<%=i%>'
                                                   value='<%=daily_basis_messengerDTO.bankAccountNo%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_DAILY_BASIS_MESSENGER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.DAILY_BASIS_MESSENGER_ADD_DAILY_BASIS_MESSENGER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    function typeOnlyInteger(e) {
        return true === inputValidationForPositiveValue(e, $(this), <%=Integer.MAX_VALUE%>, 0);
    }

    $(() => {
        $('body').delegate('input[data-only-integer="true"]', 'keydown', typeOnlyInteger);
    });

    function PreprocessBeforeSubmiting() {
        submitAddForm2();
        return false;
    }
</script>