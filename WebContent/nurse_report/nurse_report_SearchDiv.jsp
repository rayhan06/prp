<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_offices.*" %>
<%@ page import="pb.*" %>
<%@page import="workflow.WorkflowController"%>
<%@ page import="java.util.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.NURSE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_REFERENCE_EMPLOYEE, loginDTO)%>
				</label>
				<div class="col-md-9">
					<div class="">
		                <button type="button" class="btn submit-btn text-white shadow btn-border-radius btn-block"
		                        onclick="addEmployee()"
		                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
		                </button>
		                <table class="table table-bordered table-striped">
		                    <tbody id="employeeToSet"></tbody>
		                </table>
		                <input class='form-control' type='hidden' name='officeUnitType' id='officeUnitType' value=''/>
		                <input class='form-control' type='hidden' name='userName' id='userName' value=''/>
		            </div>						
				</div>
			</div>
		</div>
		<div class="search-criteria-div col-md-12">
	        <div class="form-group row">
	            <label class="col-md-3 col-form-label text-md-right">
	                <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
	            </label>
	            <div class="col-md-9">              
	                <input class='form-control' type='text' name='userNameRaw' id='userNameRaw' value='' onKeyUp="setEngUserName(this.value, 'userName')"/>
	            </div>
	        </div>
	    </div>

		
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_NURSE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select  class='form-control' name='nurse_user_name'
                          id='nurse_user_name' 
                          tag='pb_html' >
                          <option value=""><%=LM.getText(LC.HM_SELECT, loginDTO)%></option>
                          <%
                          Set<Long> emps = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.NURSE_ROLE);
                          
                          //emps.addAll(EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.ADMIN_ROLE));
                          for(Long em: emps)
                          {
                          %>
                          <option value = "<%=WorkflowController.getUserNameFromOrganogramId(em)%>">
                          <%=WorkflowController.getNameFromOrganogramId(em, Language)%>
                          </option>
                          <%
                          }
                          %>
                    </select>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.NURSE_REPORT_WHERE_NURSEACTIONCAT, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='nurseActionCat' id = 'nurseActionCat' >		
						<%		
						Options = CatDAO.getOptions(Language, "nurse_action", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
    </div>
</div>
<script type="text/javascript">
function init()
{
	$("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = false;
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}
function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#userName").val(userName);
    $("#userNameRaw").val(userName);
}
</script>