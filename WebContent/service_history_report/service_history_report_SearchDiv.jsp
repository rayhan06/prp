<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = LM.getText(LC.EMPLOYEE_MEDICAL_REPORT_EDIT_LANGUAGE, loginDTO);
    int year = Calendar.getInstance().get(Calendar.YEAR);
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="px-4">
    <div class="row">
        <div class="search-criteria-div col-md-6">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">
                    <%=LM.getText(LC.SERVICE_HISTORY_REPORT_WHERE_EMPLOYEERECORDSID, loginDTO)%>
                </label>
                <div class="col-md-8">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                            id="employeeRecordId_modal_button"
                            onclick="employeeRecordIdModalBtnClicked();">
                        <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                    </button>
                    <div class="input-group" id="employeeRecordId_div" style="display: none">
                        <input type="hidden" name='employeeRecordId' id='employeeRecordId_input' value="">
                        <button type="button" class="btn btn-secondary form-control" disabled
                                id="employeeRecordId_text"></button>
                        <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                            <button type="button" class="btn btn-outline-danger"
                                    onclick="crsBtnClicked('employeeRecordId');"
                                    id='employeeRecordId_crs_btn' tag='pb_html'>
                                x
                            </button>
						</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="search-criteria-div col-md-6">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">
                    <%=LM.getText(LC.SERVICE_HISTORY_REPORT_WHERE_PROMOTIONNATURECAT, loginDTO)%>
                </label>
                <div class="col-md-8">
                    <select class='form-control' name='promotionNatureCat' id='promotionNatureCat'
                    >
                        <%=CatRepository.getInstance().buildOptions("promotion_nature", Language, 0)%>
                    </select>
                </div>
            </div>
        </div>
        <div id="name_eng_div" class="search-criteria-div col-md-6 employeeNameClass">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right" for="nameEng">
                    <%=isLangEng ? "Name(English)" : "নাম(ইংরেজি)"%>
                </label>
                <div class="col-md-8">
                    <input class="englishOnly form-control" type="text" name="nameEng" id="nameEng"
                           style="width: 100%"
                           placeholder="<%=isLangEng ? "Enter Employee Name in English" : "কর্মকর্তা/কর্মচারীর নাম ইংরেজিতে লিখুন"%>"
                           value="">
                </div>
            </div>
        </div>

        <div id="name_bng_div" class="search-criteria-div col-md-6 employeeNameClass">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right" for="nameBng">
                    <%=isLangEng ? "Name(Bangla)" : "নাম(বাংলা)"%>
                </label>
                <div class="col-md-8">
                    <input class="noEnglish form-control" type="text" name="nameBng" id="nameBng"
                           style="width: 100%"
                           placeholder="<%=isLangEng ? "Enter Employee Name in Bangla" : "কর্মকর্তা/কর্মচারীর নাম বাংলাতে লিখুন"%>"
                           value="">
                </div>
            </div>
        </div>

        <div class="search-criteria-div col-md-6">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">
                    <%=LM.getText(LC.SERVICE_HISTORY_REPORT_WHERE_SERVINGTO, loginDTO)%>
                </label>
                <div class="col-md-8">
                    <jsp:include page="/date/date.jsp">
                        <jsp:param name="DATE_ID" value="dateFrom-js"/>
                        <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                        <jsp:param name="END_YEAR"
                                   value="<%=year + 50%>"></jsp:param>
                    </jsp:include>
                    <input type="hidden" name='dateFrom' id='dateFrom' value=""/>
                </div>
            </div>
        </div>
        <div class="search-criteria-div col-md-6">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right">
                    <%=LM.getText(LC.SERVICE_HISTORY_REPORT_WHERE_SERVINGTO_2, loginDTO)%>
                </label>
                <div class="col-md-8">
                    <jsp:include page="/date/date.jsp">
                        <jsp:param name="DATE_ID" value="dateTo-js"/>
                        <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                        <jsp:param name="END_YEAR"
                                   value="<%=year + 50%>"></jsp:param>
                    </jsp:include>
                    <input type="hidden" name='dateTo' id='dateTo' value=""/>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    $(document).ready(() => {
        showFooter = false;
    });

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
    }

    function employeeRecordIdInInput(empInfo) {
        console.log(empInfo);

        $('#employeeRecordId_modal_button').hide();
        $('#employeeRecordId_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.employeeNameEn + ' (' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn + ')';
        } else {
            designation = empInfo.employeeNameBn + ' (' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn + ')';
        }
        document.getElementById('employeeRecordId_text').innerHTML = designation;
        $('#employeeRecordId_input').val(empInfo.employeeRecordId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: employeeRecordIdInInput
        }]
    ]);

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    function init() {
        dateTimeInit($("#Language").val());
        select2SingleSelector('#promotionNatureCat', '<%=Language%>');
    }

    function PreprocessBeforeSubmiting() {
        $('#dateFrom').val(getDateTimestampById('dateFrom-js'));
        $('#dateTo').val(getDateTimestampById('dateTo-js'));
    }
</script>