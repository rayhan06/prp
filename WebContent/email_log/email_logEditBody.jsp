<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="email_log.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>


<%
    Email_logDTO email_logDTO;
    email_logDTO = (Email_logDTO) request.getAttribute("email_logDTO");
    String userSessionPass = (String) session.getAttribute("userPass");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (email_logDTO == null) {
        email_logDTO = new Email_logDTO();

    }
    System.out.println("email_logDTO = " + email_logDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.EMAIL_LOG_EDIT_EMAIL_LOG_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.EMAIL_LOG_ADD_EMAIL_LOG_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;

    List<Edms_documentsDTO> edms_documentsDTOList = (List<Edms_documentsDTO>) request.getAttribute( "EDMS_DOCUMENTS_LIST" );
    List<UserNameEmailDTO> listOfUser = (List<UserNameEmailDTO>) request.getAttribute( "userList" );
    Gson gson = new Gson();
    JsonElement userList = gson.toJsonTree(listOfUser, new TypeToken<List<UserNameEmailDTO>>() {}.getType());
%>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal"
              action="Email_logServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="form-body">


                <%@ page import="java.text.SimpleDateFormat" %>
                <%@ page import="java.util.Date" %>

                <%@ page import="pb.*" %>
                <%@ page import="edms_documents.Edms_documentsDTO" %>
                <%@ page import="user.UserNameEmailDTO" %>
                <%@ page import="org.json.JSONArray" %>
                <%@ page import="com.google.gson.Gson" %>
                <%@ page import="com.google.gson.JsonElement" %>
                <%@ page import="com.google.gson.reflect.TypeToken" %>

                <%
                    String Language = LM.getText(LC.EMAIL_LOG_EDIT_LANGUAGE, loginDTO);
                    String Options;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    String datestr = dateFormat.format(date);
                %>


                <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>' value='<%=email_logDTO.iD%>'
                       tag='pb_html'/>


                <%--<label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMAIL_LOG_EDIT_FROMADDR, loginDTO)) : (LM.getText(LC.EMAIL_LOG_ADD_FROMADDR, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='fromAddr_div_<%=i%>'>
                        <input type='text' class='form-control' name='fromAddr' id='fromAddr_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + email_logDTO.fromAddr + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>--%>


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMAIL_LOG_EDIT_TOADDR, loginDTO)) : (LM.getText(LC.EMAIL_LOG_ADD_TOADDR, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='toAddr_div_<%=i%>'>
                        <input type='text' class='form-control' name='toAddr' id='toAddr_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + email_logDTO.toAddr + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    Group To
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='toAddr_div_<%=i%>'>
                        <select class='form-control' name='address_book_select_to' id='address_book_select_to_<%=i%>' tag='pb_html'>
                            <option disabled selected>Select one section</option>
                            <%
                                if (actionName.equals("edit")) {
                                    String defaultVal = email_logDTO.toAddrBook == null ? "" : email_logDTO.toAddrBook + "";
                                    Options = CommonDAO.getOptions(Language, "select", "address_book", "address_book_select_to_" + i, "form-control", "address_book_select_to", defaultVal);
                                } else {
                                    Options = CommonDAO.getOptions(Language, "select", "address_book", "address_book_select_to_" + i, "form-control", "address_book_select_to");
                                }
                            %>
                            <%=Options%>
                        </select>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMAIL_LOG_EDIT_CC, loginDTO)) : (LM.getText(LC.EMAIL_LOG_ADD_CC, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='cc_div_<%=i%>'>
                        <input type='text' class='form-control' name='cc' id='cc_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + email_logDTO.cc + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    Group CC
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='ccAddr_div_<%=i%>'>
                        <select class='form-control' name='address_book_select_cc' id='address_book_select_cc_<%=i%>' tag='pb_html'>
                            <option disabled selected>Select one section</option>
                            <%
                                if (actionName.equals("edit")) {
                                    String defaultVal = email_logDTO.ccAddrBook == null ? "" : email_logDTO.ccAddrBook + "";
                                    Options = CommonDAO.getOptions(Language, "select", "address_book", "address_book_select_cc_" + i, "form-control", "address_book_select_cc", defaultVal);
                                } else {
                                    Options = CommonDAO.getOptions(Language, "select", "address_book", "address_book_select_cc_" + i, "form-control", "address_book_select_cc");
                                }
                            %>
                            <%=Options%>
                        </select>
                    </div>
                </div>

                <label class="col-lg-3 control-label">
                    BCC
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='cc_div_<%=i%>'>
                        <input type='text' class='form-control' name='bcc' id='bcc_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + email_logDTO.bcc + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMAIL_LOG_EDIT_SUBJECT, loginDTO)) : (LM.getText(LC.EMAIL_LOG_ADD_SUBJECT, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='subject_div_<%=i%>'>
                        <input type='text' class='form-control' name='subject' id='subject_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + email_logDTO.subject + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMAIL_LOG_EDIT_EMAILBODY, loginDTO)) : (LM.getText(LC.EMAIL_LOG_ADD_EMAILBODY, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='emailBody_div_<%=i%>'>
                        <textarea class='form-control' name='emailBody' id='emailBody_text_<%=i%>'
                               tag='pb_html'><%=actionName.equals("edit")?(email_logDTO.emailBody):("")%></textarea>
                    </div>
                </div>


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.EMAIL_LOG_EDIT_FILESDROPZONE, loginDTO)) : (LM.getText(LC.EMAIL_LOG_ADD_FILESDROPZONE, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='filesDropzone_div_<%=i%>'>
                        <%
                            if (actionName.equals("edit")) {
                                List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(email_logDTO.filesDropzone);
                        %>
                        <table>
                            <tr>
                                <%
                                    if (filesDropzoneDTOList != null) {
                                        for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                            FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                %>
                                <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                    <%
                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                    %>
                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                    <%
                                        }
                                    %>
                                    <a href='Email_logServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                       download><%=filesDTO.fileTitle%>
                                    </a>
                                    <a class='btn btn-danger'
                                       onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                </td>
                                <%
                                        }
                                    }
                                %>
                            </tr>
                        </table>
                        <%
                            }
                        %>

                        <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                        <div class="dropzone"
                             action="Email_logServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?email_logDTO.filesDropzone:ColumnID%>">
                            <input type='file' style="display:none" name='filesDropzoneFile'
                                   id='filesDropzone_dropzone_File_<%=i%>' tag='pb_html'/>
                        </div>
                        <input type='hidden' name='filesDropzoneFilesToDelete' id='filesDropzoneFilesToDelete_<%=i%>'
                               value='' tag='pb_html'/>
                        <input type='hidden' name='filesDropzone' id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                               value='<%=actionName.equals("edit")?email_logDTO.filesDropzone:ColumnID%>'/>


                    </div>
                </div>

                <%if( edms_documentsDTOList != null && edms_documentsDTOList.size() > 0 ){%>

                <label class="col-lg-3 control-label">

                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='emailBody_div_<%=i%>'>
                        <h3>Document Selected for Share</h3>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Document subject</th>
                                <th>Files</th>
                            </tr>
                            </thead>
                            <tbody>
                            <%for( Edms_documentsDTO edms_documentsDTO: edms_documentsDTOList ){%>

                                <input type="hidden" name="edmsDocId" value="<%=edms_documentsDTO.iD%>" />
                                <%
                                    List<FilesDTO> filesDTOList = filesDAO.getMiniDTOsByFileID( edms_documentsDTO.filesDropzone );
                                    if( filesDTOList.size() > 0 ){
                                %>

                                    <tr id="table_row_<%=edms_documentsDTO.iD%>" data-id="<%=edms_documentsDTO.iD %>" >
                                        <td rowspan="<%=filesDTOList.size() > 0? filesDTOList.size(): 1%>" ><%= edms_documentsDTO.documentSubject %></td>
                                        <td>
                                            <a onclick="window.open(this.href,'_blank');return false;" href = 'Edms_documentsServlet?actionType=previewDropzoneFile&id=<%=filesDTOList.get(0).iD%>' download> <%=filesDTOList.get(0).fileTitle%> </a>
                                        </td>
                                    </tr>

                                    <%for( int k = 1; k< filesDTOList.size(); k++ ){%>
                                        <tr>
                                            <td>
                                                <a onclick="window.open(this.href,'_blank');return false;" href = 'Edms_documentsServlet?actionType=previewDropzoneFile&id=<%=filesDTOList.get(k).iD%>' download> <%=filesDTOList.get(k).fileTitle%> </a>
                                            </td>
                                        </tr>
                                    <%}%>

                                <%}
                                else {%>

                                    <tr id="table_row_<%=edms_documentsDTO.iD%>" data-id="<%=edms_documentsDTO.iD %>" >
                                        <td><%= edms_documentsDTO.documentSubject %></td>
                                        <td>
                                            N/A
                                        </td>
                                    </tr>

                                <%}%>
                            <%}%>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%}%>

                <input type='hidden' class='form-control' name='isDraft' id='isDraft_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + email_logDTO.isDraft + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='sendingTime' id='sendingTime_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + email_logDTO.sendingTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + email_logDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>


                <input type='hidden' class='form-control' name='lastModificationTime'
                       id='lastModificationTime_hidden_<%=i%>'
                       value=<%=actionName.equals("edit")?("'" + email_logDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>


                <div class="form-actions text-center">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%="BACK"%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.EMAIL_LOG_ADD_EMAIL_LOG_CANCEL_BUTTON, loginDTO)%>
                        <%
                            }

                        %>
                    </a>
                    <%
                        if (actionName.equals("edit")) {
                            if (email_logDTO.isDraft == 1) {
                    %>
                    <button class="btn btn-success" type="submit" onclick="setDraftFalse()">
                        <%=LM.getText(LC.EMAIL_LOG_EDIT_EMAIL_LOG_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                        <%
                                }
                        } else {
                        %>
                    <button class="btn btn-success" type="submit" onclick="setDraftFalse()">
                        <%=LM.getText(LC.EMAIL_LOG_EDIT_EMAIL_LOG_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                        <%
                            }
                        %>

                    <%
                        if (actionName.equals("edit")) {
                            if (email_logDTO.isDraft == 1) {
                    %>
                    <button class="btn btn-success" type="submit" onclick="setDraftTrue()">

                        <%="Save as Draft"%>

                    </button>
                    <%
                            }
                        } else {
                    %>
                    <button class="btn btn-success" type="submit" onclick="setDraftTrue()">

                        <%="Save as Draft"%>

                    </button>
                    <%}%>
                </div>

            </div>

        </form>

    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    function setDraftTrue() {
        $('input[name=isDraft]').val('1');
    }

    function setDraftFalse() {
        $('input[name=isDraft]').val('0');
    }


    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        }
        else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Email_logServlet");
    }

    function init(row) {


    }

    function autocomplete(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function (e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) {
                return false;
            }
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                var lst = val.split(";");
                val = lst[lst.length - 1].trim();
                if (arr[i].name.toUpperCase().includes(val.toUpperCase()) || arr[i].email.toUpperCase().includes(val.toUpperCase())) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].name + ", </strong>";
                    b.innerHTML += arr[i].email ;
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i].email + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        var valList = inp.value.split(";");

                        var str = "";
                        var i;
                        for (i = 0; i < valList.length -1; i++) {
                            str += valList[i] + "; ";
                        }
                        inp.value = str + this.getElementsByTagName("input")[0].value + "; ";
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }

        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }

    var row = 0;

    var userEmails = <%=userList%>;


    window.onload = function () {
        autocomplete(document.getElementById("toAddr_text_0"), userEmails);
        autocomplete(document.getElementById("cc_text_0"), userEmails);
        autocomplete(document.getElementById("bcc_text_0"), userEmails);
        $("[name='address_book_select_to']").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });

        $("[name='address_book_select_cc']").select2({
            dropdownAutoWidth: true,
            theme: "classic"
        });
        var userSessionPassSet = <%=userSessionPass != null%>;
        if (!userSessionPassSet){
            var password = prompt("Please insert your password");
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", "Email_logServlet?actionType=setPass&password=" + password + "&redirectAction=getAddPage", true);
            xhttp.send();

        }
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






