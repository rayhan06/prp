<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="email_log.EmailInboxDTO" %>
<%@ page import="mail.EmailAttachment" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "Email_logServlet?actionType=inbox";
    String navigator = SessionConstants.NAV_EMAIL_LOG;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>


<div class="portlet box">
    <div class="portlet-body">
        <%
            EmailInboxDTO data = (EmailInboxDTO) request.getAttribute("inboxDTO");
            try {
                if (data != null) {
                    String value = "";
                    EmailInboxDTO email_logDTO = data;
        %>


        <br>
        <div> <b>From: </b></div>
        <div id='fromAddr'>
            <%
                value = email_logDTO.getFrom() + "";
            %>

            <%=value%>


        </div>

        <br>
        <div> <b>To: </b></div>
        <div id='toAddr'>
            <%
                value = email_logDTO.getTo() + "";
            %>

            <%=value%>


        </div>

        <br>
        <div> <b>CC: </b></div>
        <div id='cc'>
            <%
                value = email_logDTO.getCc() + "";
            %>

            <%=value%>


        </div>

        <br>
        <div> <b>Subject: </b></div>
        <div id='subject'>
            <%
                value = email_logDTO.getSubject() + "";
            %>

            <%=value%>
        </div>

        <br>
        <div> <b>Body: </b></div>
        <div id='emailBody'>
            <%
                value = email_logDTO.getText() + "";
            %>

            <%=value%>
        </div><br>
        <div> <b>Attachments: </b></div><br>
        <div id='attachment'>
            <%
                for (int i =0; i< email_logDTO.getAttachments().size(); i++){
                    EmailAttachment emailAttachment = email_logDTO.getAttachments().get(i);
            %>
            <div class="row" style="padding: 20px">
                <a href="Email_logServlet?actionType=downloadAttachment&emailId=<%=email_logDTO.getUid()%>&attachmentId=<%=i%>" target="_blank">
                    <%=emailAttachment.getName() == null ? "No Title" : emailAttachment.getName()%>
                </a>
            </div>
            <%}%>
        </div>

        <%
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
    </div>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">


</script>


