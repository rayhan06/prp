<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="email_log.EmailInboxDTO" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
    String url = "Email_logServlet?actionType=inbox";
    String navigator = SessionConstants.NAV_EMAIL_LOG;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String userSessionPass = (String) session.getAttribute("userPass");

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>


<div class="portlet box">
    <div class="portlet-body">
        <div class="table-responsive">
            <table id="tableData" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th><%=LM.getText(LC.EMAIL_LOG_EDIT_FROMADDR, loginDTO)%></th>
                    <th><%=LM.getText(LC.EMAIL_LOG_EDIT_TOADDR, loginDTO)%></th>
                    <th><%=LM.getText(LC.EMAIL_LOG_EDIT_CC, loginDTO)%></th>
                    <th><%=LM.getText(LC.EMAIL_LOG_EDIT_SUBJECT, loginDTO)%></th>
                    <th><%=LM.getText(LC.EMAIL_LOG_EDIT_EMAILBODY, loginDTO)%></th>
                    <%--<th><%=LM.getText(LC.EMAIL_LOG_EDIT_FILESDROPZONE, loginDTO)%></th>--%>
                    <%--<th><%=LM.getText(LC.EMAIL_LOG_EDIT_ISDRAFT, loginDTO)%></th>--%>
                    <%--<th><%=LM.getText(LC.EMAIL_LOG_EDIT_SENDINGTIME, loginDTO)%></th>--%>
                    <%--<th><%=LM.getText(LC.EMAIL_LOG_SEARCH_EMAIL_LOG_EDIT_BUTTON, loginDTO)%></th>--%>


                </tr>
                </thead>
                <tbody>
                <%
                    ArrayList data = (ArrayList) request.getAttribute("inboxDTOS");
                    try {
                        if (data != null) {
                            int size = data.size();
                            String value = "";
                            System.out.println("data not null and size = " + size + " data = " + data);
                            for (int i = 0; i < size; i++) {
                                EmailInboxDTO email_logDTO = (EmailInboxDTO) data.get(i);
                %>
                                <tr id='tr_<%=i%>' <%=email_logDTO.getSeen() ? "style=\"background-color: lavender;\"" : ""%>>

                                    <td id='<%=i%>_fromAddr'>
                                        <%
                                            value = email_logDTO.getFrom() + "";
                                        %>

                                        <%=value%>


                                    </td>


                                    <td id='<%=i%>_toAddr'>
                                        <%
                                            value = email_logDTO.getTo() + "";
                                        %>

                                        <%=value%>


                                    </td>


                                    <td id='<%=i%>_cc'>
                                        <%
                                            value = email_logDTO.getCc() + "";
                                        %>

                                        <%=value%>


                                    </td>


                                    <td id='<%=i%>_subject'>
                                        <%
                                            value = email_logDTO.getSubject() + "";
                                        %>

                                        <%=value%>
                                    </td>


                                    <td id='<%=i%>_emailBody'>
                                        <a href='Email_logServlet?actionType=getEmailViewPage&ID=<%=email_logDTO.getUid()%>'><%="VIEW"%></a>
                                    </td>

                                </tr>
                <%
                            }
                        } else {
                            System.out.println("data  null");
                        }
                    } catch (Exception e) {
                        System.out.println("JSP exception " + e);
                    }
                %>
                </tbody>

            </table>
        </div>
    </div>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var userSessionPassSet = <%=userSessionPass != null%>;
        if (!userSessionPassSet) {
            var password = prompt("Please insert your password");
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", "Email_logServlet?actionType=setPass&password=" + password + "&redirectAction=inbox", true);
            xhttp.send();

        }
    });

</script>


