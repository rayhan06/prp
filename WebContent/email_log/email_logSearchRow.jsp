<%@page pageEncoding="UTF-8" %>

<%@page import="email_log.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMAIL_LOG_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_EMAIL_LOG;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Email_logDTO email_logDTO = (Email_logDTO) request.getAttribute("email_logDTO");
    CommonDTO commonDTO = email_logDTO;
    String servletName = "Email_logServlet";


    System.out.println("email_logDTO = " + email_logDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Email_logDAO email_logDAO = (Email_logDAO) request.getAttribute("email_logDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;

%>


<%--<td id='<%=i%>_fromAddr'>
    <%
        value = email_logDTO.fromAddr + "";
    %>

    <%=value%>


</td>--%>


<td id='<%=i%>_toAddr'>
    <%
        value = email_logDTO.toAddr + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_cc'>
    <%
        value = email_logDTO.cc + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_subject'>
    <%
        value = email_logDTO.subject + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_emailBody'>
    <%
        value = email_logDTO.emailBody + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_filesDropzone'>
    <%
        value = email_logDTO.filesDropzone + "";
    %>
    <%
        {
            List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(email_logDTO.filesDropzone);
    %>
    <table>
        <tr>
            <%
                if (FilesDTOList != null) {
                    for (int j = 0; j < FilesDTOList.size(); j++) {
                        FilesDTO filesDTO = FilesDTOList.get(j);
                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
            %>
            <td>
                <%
                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                %>
                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                <%
                    }
                %>
                <a href='Email_logServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                   download><%=filesDTO.fileTitle%>
                </a>
            </td>
            <%
                    }
                }
            %>
        </tr>
    </table>
    <%
        }
    %>


</td>


<td id='<%=i%>_isDraft'>
    <%
        value = email_logDTO.isDraft == 0? "No":"Yes";
    %>

    <%=value%>


</td>


<td id='<%=i%>_sendingTime'>
    <%
        value = email_logDTO.sendingTime > 0? new SimpleDateFormat( "dd-MM-yyyy" ).format( new Date( email_logDTO.sendingTime ) ):"N/A";
    %>

    <%=value%>


</td>


<td id='<%=i%>_Edit'>

    <a href='Email_logServlet?actionType=getEditPage&ID=<%=email_logDTO.iD%>'><%="VIEW"%>
    </a>

</td>


<td id='<%=i%>_checkbox'>
    <span id='chkEdit'><input type='checkbox' name='ID' value='<%=email_logDTO.iD%>'/></span>
    <div class='checker'>
    </div>
</td>
																						
											

