<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="email_log.Email_logDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>

<%
Email_logDTO email_logDTO = (Email_logDTO)request.getAttribute("email_logDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(email_logDTO == null)
{
	email_logDTO = new Email_logDTO();
	
}
System.out.println("email_logDTO = " + email_logDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.EMAIL_LOG_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=email_logDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fromAddr'>")%>
			
	
	<div class="form-inline" id = 'fromAddr_div_<%=i%>'>
		<input type='text' class='form-control'  name='fromAddr' id = 'fromAddr_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + email_logDTO.fromAddr + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_toAddr'>")%>
			
	
	<div class="form-inline" id = 'toAddr_div_<%=i%>'>
		<input type='text' class='form-control'  name='toAddr' id = 'toAddr_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + email_logDTO.toAddr + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_cc'>")%>
			
	
	<div class="form-inline" id = 'cc_div_<%=i%>'>
		<input type='text' class='form-control'  name='cc' id = 'cc_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + email_logDTO.cc + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_subject'>")%>
			
	
	<div class="form-inline" id = 'subject_div_<%=i%>'>
		<input type='text' class='form-control'  name='subject' id = 'subject_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + email_logDTO.subject + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_emailBody'>")%>
			
	
	<div class="form-inline" id = 'emailBody_div_<%=i%>'>
		<input type='text' class='form-control'  name='emailBody' id = 'emailBody_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + email_logDTO.emailBody + "'"):("'" + "" + "'")%> <% 
	if(!actionName.equals("edit"))
	{
%>
		required="required"  pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" title="emailBody must be a of valid email address format"
<%
	}
%>
  tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_filesDropzone'>")%>
			
	
	<div class="form-inline" id = 'filesDropzone_div_<%=i%>'>
		<%
		if(actionName.equals("edit"))
		{
			List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(email_logDTO.filesDropzone);
			%>			
			<table>
				<tr>
			<%
			if(filesDropzoneDTOList != null)
			{
				for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
				{
					FilesDTO filesDTO = filesDropzoneDTOList.get(j);
					byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
					%>
					<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
					{
						%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64) 
						%>' style='width:100px' />
						<%
					}
					%>
					<a href = 'Email_logServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>		
					</td>
					<%
				}
			}
			%>
			</tr>
			</table>
			<%
		}
		%>
		
		<%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>			
		<div class="dropzone" action="Email_logServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?email_logDTO.filesDropzone:ColumnID%>">
			<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?email_logDTO.filesDropzone:ColumnID%>'/>		


	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDraft'>")%>
			

		<input type='hidden' class='form-control'  name='isDraft' id = 'isDraft_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + email_logDTO.isDraft + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_sendingTime'>")%>
			

		<input type='hidden' class='form-control'  name='sendingTime' id = 'sendingTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + email_logDTO.sendingTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + email_logDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=email_logDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
		