<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="gate_pass_item.Gate_pass_itemDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Gate_pass_itemDTO gate_pass_itemDTO = (Gate_pass_itemDTO)request.getAttribute("gate_pass_itemDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(gate_pass_itemDTO == null)
{
	gate_pass_itemDTO = new Gate_pass_itemDTO();
	
}
System.out.println("gate_pass_itemDTO = " + gate_pass_itemDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.GATE_PASS_ITEM_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_id" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='id' id = 'id_hidden_<%=i%>' value='<%=gate_pass_itemDTO.id%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_gatePassId'>")%>
			

		<input type='hidden' class='form-control'  name='gatePassId' id = 'gatePassId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.gatePassId + "'"):("'" + "0" + "'")%>
 tag='pb_html'/>
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_parliamentItemId'>")%>
			
	
	<div class="form-inline" id = 'parliamentItemId_div_<%=i%>'>
		<select class='form-control'  name='parliamentItemId' id = 'parliamentItemId_select2_<%=i%>'   tag='pb_html'>
			<option class='form-control'  value='0' <%=(actionName.equals("edit") && String.valueOf(gate_pass_itemDTO.parliamentItemId).equals("0"))?("selected"):""%>
>0<br>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_amount" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='amount' id = 'amount_number_<%=i%>' value='<%=gate_pass_itemDTO.amount%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_description" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='description' id = 'description_text_<%=i%>' value='<%=gate_pass_itemDTO.description%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=gate_pass_itemDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=gate_pass_itemDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=gate_pass_itemDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + gate_pass_itemDTO.isDeleted + "'"):("'" + "false" + "'")%>
 tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=gate_pass_itemDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Gate_pass_itemServlet?actionType=view&ID=<%=gate_pass_itemDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Gate_pass_itemServlet?actionType=view&modal=1&ID=<%=gate_pass_itemDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	