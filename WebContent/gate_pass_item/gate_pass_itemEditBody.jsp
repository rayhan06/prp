<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="gate_pass_item.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%
    Gate_pass_itemDTO gate_pass_itemDTO;
    gate_pass_itemDTO = (Gate_pass_itemDTO) request.getAttribute("gate_pass_itemDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (gate_pass_itemDTO == null) {
        gate_pass_itemDTO = new Gate_pass_itemDTO();

    }
    System.out.println("gate_pass_itemDTO = " + gate_pass_itemDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.GATE_PASS_ITEM_ADD_GATE_PASS_ITEM_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=formTitle%>
                        </h3>
                    </div>
                </div>
                <form class="form-horizontal"
                      action="Gate_pass_itemServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
                      id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
                      onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
                    <div class="kt-portlet__body form-body">
                        <%@ page import="java.text.SimpleDateFormat" %>
                        <%@ page import="java.util.Date" %>
                        <%@ page import="pb.*" %>
                        <%
                            String Language = LM.getText(LC.GATE_PASS_ITEM_EDIT_LANGUAGE, loginDTO);
                            String Options;
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            Date date = new Date();
                            String datestr = dateFormat.format(date);
                            CommonDAO.language = Language;
                            CatDAO.language = Language;
                        %>
                        <input type='hidden' class='form-control' name='id' id='id_hidden_<%=i%>'
                               value='<%=gate_pass_itemDTO.id%>' tag='pb_html'/>
                        <input type='hidden' class='form-control' name='gatePassId' id='gatePassId_hidden_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.gatePassId + "'"):("'" + "0" + "'")%>
                                       tag='pb_html'/>
                        <label class="col-lg-3 control-label">
                            <%=LM.getText(LC.GATE_PASS_ITEM_ADD_PARLIAMENTITEMID, loginDTO)%>
                        </label>
                        <div class="form-group ">
                            <div class="col-lg-6 " id='parliamentItemId_div_<%=i%>'>
                                <select class='form-control' name='parliamentItemId' id='parliamentItemId_select2_<%=i%>'
                                        tag='pb_html'>
                                    <option class='form-control'
                                            value='0' <%=(actionName.equals("edit") && String.valueOf(gate_pass_itemDTO.parliamentItemId).equals("0"))?("selected"):""%>
                                    >0<br>
                                </select>

                            </div>
                        </div>
                        <label class="col-lg-3 control-label">
                            <%=LM.getText(LC.GATE_PASS_ITEM_ADD_AMOUNT, loginDTO)%>
                        </label>
                        <div class="form-group ">
                            <div class="col-lg-6 " id='amount_div_<%=i%>'>
                                <input type='number' class='form-control' name='amount' id='amount_number_<%=i%>'
                                       value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.amount + "'"):("'" + 0 + "'")%>
                                               tag='pb_html'>

                            </div>
                        </div>
                        <label class="col-lg-3 control-label">
                            <%=LM.getText(LC.GATE_PASS_ITEM_ADD_DESCRIPTION, loginDTO)%>
                        </label>
                        <div class="form-group ">
                            <div class="col-lg-6 " id='description_div_<%=i%>'>
                                <input type='text' class='form-control' name='description' id='description_text_<%=i%>'
                                       value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.description + "'"):("'" + "" + "'")%>
                                               tag='pb_html'/>
                            </div>
                        </div>
                        <input type='hidden' class='form-control' name='insertionDate' id='insertionDate_hidden_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.insertionDate + "'"):("'" + "0" + "'")%>
                                       tag='pb_html'/>
                        <input type='hidden' class='form-control' name='insertedBy' id='insertedBy_hidden_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.insertedBy + "'"):("'" + "" + "'")%>
                                       tag='pb_html'/>
                        <input type='hidden' class='form-control' name='modifiedBy' id='modifiedBy_hidden_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.modifiedBy + "'"):("'" + "" + "'")%>
                                       tag='pb_html'/>
                        <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.isDeleted + "'"):("'" + "false" + "'")%>
                                       tag='pb_html'/>
                        <input type='hidden' class='form-control' name='lastModificationTime'
                               id='lastModificationTime_hidden_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + gate_pass_itemDTO.lastModificationTime + "'"):("'" + "0" + "'")%>
                                       tag='pb_html'/>
                        <div class="form-actions text-center">
                            <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                                <%=LM.getText(LC.GATE_PASS_ITEM_ADD_GATE_PASS_ITEM_CANCEL_BUTTON, loginDTO)%>
                            </a>
                            <button class="btn btn-success" type="submit">

                                <%=LM.getText(LC.GATE_PASS_ITEM_ADD_GATE_PASS_ITEM_SUBMIT_BUTTON, loginDTO)%>

                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    $(document).ready(function () {

        dateTimeInit("<%=Language%>");
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Gate_pass_itemServlet");
    }

    function init(row) {

        $("#parliamentItemId_select2_" + row).select2({
            dropdownAutoWidth: true
        });


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






