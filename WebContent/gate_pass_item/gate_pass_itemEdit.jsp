<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="gate_pass_item.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@ page import="pb.*" %>
<%@ page import="parliament_item.Parliament_itemRepository" %>

<%
	Gate_pass_itemDTO gate_pass_itemDTO;
	gate_pass_itemDTO = (Gate_pass_itemDTO) request.getAttribute("gate_pass_itemDTO");
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	if (gate_pass_itemDTO == null) {
		gate_pass_itemDTO = new Gate_pass_itemDTO();
	}
	System.out.println("gate_pass_itemDTO = " + gate_pass_itemDTO);

	String Language = LM.getText(LC.GATE_PASS_ITEM_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
	CatDAO.language = Language;

	int rowNumber = Integer.parseInt(request.getParameter("rowNumber"));
%>

<tr id="gate_pass_item_<%=rowNumber%>">
	<td>
		<select class='form-control' name='parliamentItemId_<%=rowNumber%>' id='parliamentItemId_<%=rowNumber%>'
				tag='pb_html'>
			<%=Parliament_itemRepository.getInstance().buildOptions(Language,0)%>
		</select>
	</td>
	<td>
		<input type='number' class='form-control' name='amount_<%=rowNumber%>'
			   id='amount_<%=rowNumber%>' value="0" tag='pb_html' min = '1'>
	</td>
	<td>
		<input type='text' class='form-control' name='description_<%=rowNumber%>'
			   id='description_<%=rowNumber%>' tag='pb_html'/>
	</td>
	<td>
		<button type="button" class="btn d-flex align-items-center" value="" style="color: #ff6a6a" 
		onclick = "$('#gate_pass_item_<%=rowNumber%>').remove()">
						<i class="fa fa-trash"></i>&nbsp;
		</button>
	</td>
</tr>








