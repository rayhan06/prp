<%@page pageEncoding="UTF-8" %>

<%@page import="gate_pass_item.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.GATE_PASS_ITEM_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_GATE_PASS_ITEM;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Gate_pass_itemDTO gate_pass_itemDTO = (Gate_pass_itemDTO) request.getAttribute("gate_pass_itemDTO");
    CommonDTO commonDTO = gate_pass_itemDTO;
    String servletName = "Gate_pass_itemServlet";


    System.out.println("gate_pass_itemDTO = " + gate_pass_itemDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Gate_pass_itemDAO gate_pass_itemDAO = (Gate_pass_itemDAO) request.getAttribute("gate_pass_itemDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_gatePassId'>
    <%
        value = gate_pass_itemDTO.gatePassId + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_parliamentItemId'>
    <%
        value = gate_pass_itemDTO.parliamentItemId + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <a href='Gate_pass_itemServlet?actionType=view&ID=<%=gate_pass_itemDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
    </a>

</td>

<td id='<%=i%>_Edit'>

    <a href='Gate_pass_itemServlet?actionType=getEditPage&ID=<%=gate_pass_itemDTO.id%>'><%=LM.getText(LC.GATE_PASS_ITEM_SEARCH_GATE_PASS_ITEM_EDIT_BUTTON, loginDTO)%>
    </a>

</td>


<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=gate_pass_itemDTO.iD%>'/></span>
    </div>
</td>
																						
											

