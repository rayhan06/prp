<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="leave_type_entitlement.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="pb.*" %>

<%
    String context = "../../.." + request.getContextPath() + "/assets/";
    Leave_type_entitlementDTO leave_type_entitlementDTO;
    leave_type_entitlementDTO = (Leave_type_entitlementDTO) request.getAttribute("leave_type_entitlementDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (leave_type_entitlementDTO == null) {
        leave_type_entitlementDTO = new Leave_type_entitlementDTO();
    }
    System.out.println("leave_type_entitlementDTO = " + leave_type_entitlementDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVE_TYPE_ENTITLEMENT_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_LEAVE_TYPE_ENTITLEMENT_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);

    int childTableStartingID = 1;


    String Language = LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LANGUAGE, loginDTO);
    String URL = "Leave_type_entitlementServlet?actionType=ajax_" + actionName + "&isPermanentTable=true";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form" id="leave_type_entitlement_form" name="bigform"
              enctype="multipart/form-data">

            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10  offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row" id='leaveTypeCat_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVETYPECAT, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_LEAVETYPECAT, loginDTO))%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <select class='form-control' name='leaveTypeCat'
                                                    id='leaveTypeCat_category' tag='pb_html'>
                                                <%=actionName.equals("edit") ? CatDAO.getOptions(Language, "leave_type", leave_type_entitlementDTO.leaveTypeCat)
                                                        : CatDAO.getOptions(Language, "leave_type", -1)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row" id='numberOfDays_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_NUMBEROFDAYS, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_NUMBEROFDAYS, loginDTO))%>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control' name='numberOfDays'
                                                   id='numberOfDays_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.numberOfDays + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row" id='frequency_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_FREQUENCY, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_FREQUENCY, loginDTO))%>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control' name='frequency'
                                                   id='frequency_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.frequency + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row" id='minimumJobDuration_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_MINIMUMJOBDURATION, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_MINIMUMJOBDURATION, loginDTO))%>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control'
                                                   name='minimumJobDuration'
                                                   id='minimumJobDuration_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.minimumJobDuration + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row" id='otherCondition1_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION1, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_OTHERCONDITION1, loginDTO))%>

                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control' maxlength="255"
                                                   name='otherCondition1' id='otherCondition1_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.otherCondition1 + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row" id='otherCondition2_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION2, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_OTHERCONDITION2, loginDTO))%>

                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control' maxlength="255"
                                                   name='otherCondition2' id='otherCondition2_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.otherCondition2 + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row" id='otherCondition3_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION3, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_OTHERCONDITION3, loginDTO))%>

                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control' maxlength="255"
                                                   name='otherCondition3' id='otherCondition3_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.otherCondition3 + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row" id='atATimeMaxEntitlementDays_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_ATATIMEMAXENTITLEMENTDAYS, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_ATATIMEMAXENTITLEMENTDAYS, loginDTO))%>

                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control'
                                                   name='atATimeMaxEntitlementDays'
                                                   id='atATimeMaxEntitlementDays_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.atATimeMaxEntitlementDays + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row" id='hasCarryForward_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_HASCARRYFORWARD, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_HASCARRYFORWARD, loginDTO))%>

                                        </label>
                                        <div class="col-1">
                                            <input type='checkbox' class='form-control-sm mt-1'
                                                   name='hasCarryForward' id='hasCarryForward_checkbox_0'
                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(leave_type_entitlementDTO.hasCarryForward).equals("true"))?("checked"):""%>
                                                   tag='pb_html'><br>
                                        </div>
                                        <div class="col-8"></div>
                                    </div>
                                    <div class="form-group row" id='maxCarryForwardInYears_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_MAXCARRYFORWARDINYEARS, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_MAXCARRYFORWARDINYEARS, loginDTO))%>

                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control'
                                                   name='maxCarryForwardInYears'
                                                   id='maxCarryForwardInYears_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.maxCarryForwardInYears + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row" id='hasLeaveEncashment_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_HASLEAVEENCASHMENT, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_HASLEAVEENCASHMENT, loginDTO))%>

                                        </label>
                                        <div class="col-1">
                                            <input type='checkbox' class='form-control-sm mt-1'
                                                   name='hasLeaveEncashment'
                                                   id='hasLeaveEncashment_checkbox_0'
                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(leave_type_entitlementDTO.hasLeaveEncashment).equals("true"))?("checked"):""%>
                                                   tag='pb_html'><br>

                                        </div>
                                        <div class="col-8"></div>
                                    </div>
                                    <div class="form-group row" id='leaveEncashmentPercentage_div'>
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVEENCASHMENTPERCENTAGE, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_LEAVEENCASHMENTPERCENTAGE, loginDTO))%>

                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control'
                                                   name='leaveEncashmentPercentage'
                                                   id='leaveEncashmentPercentage_text'
                                                   value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.leaveEncashmentPercentage + "'"):("'" + "" + "'")%>   tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_STARTDATE, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_STARTDATE, loginDTO))%>

                                        </label>
                                        <div class="col-md-8 col-xl-9" id='startDate_div'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="startDate_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden'
                                               class='form-control'
                                               id='startDate_date'
                                               name='startDate'
                                               value='' tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_ENDDATE, loginDTO)) : (LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_ENDDATE, loginDTO))%>

                                        </label>
                                        <div class="col-md-8 col-xl-9" id='endDate_div'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="endDate_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden'
                                               class='form-control'
                                               id='endDate_date'
                                               name='endDate'
                                               value='' tag='pb_html'/>
                                    </div>
                                    <!-- Hidden Fields -->
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden'
                                           value='<%=leave_type_entitlementDTO.iD%>' tag='pb_html'/>
                                    <%--TODO: remove after making sure these are handled in back end--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                            <%=actionName.equals("edit") ? LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVE_TYPE_ENTITLEMENT_CANCEL_BUTTON, loginDTO)
                                    : LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_LEAVE_TYPE_ENTITLEMENT_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="submit-btn"
                                type="button" onclick="submitItem('leave_type_entitlement_form')">
                            <%=actionName.equals("edit") ? LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVE_TYPE_ENTITLEMENT_SUBMIT_BUTTON, loginDTO)
                                    : LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_ADD_LEAVE_TYPE_ENTITLEMENT_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script src="<%=context%>scripts/util1.js"></script>
<script type="text/javascript">
    const form = $('#leave_type_entitlement_form');

    let isEnglish = <%=isLanguageEnglish%>;

    function submitItem(id) {
        let submitFunction = () => {
            let formName = '#' + id;
            const form = $(formName);

            buttonStateChange(true);
            if (isFormValid(form)) {
                $.ajax({
                    type: "POST",
                    url: "<%=URL%>",
                    data: form.serialize(),
                    dataType: 'JSON',
                    success: function (response) {
                        if (response.responseCode === 0) {
                            $('#toast_message').css('background-color', '#ff6063');
                            showToastSticky(response.msg, response.msg);
                            buttonStateChange(false);
                        } else if (response.responseCode === 200) {
                            window.location.replace(getContextPath() + response.msg);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                            + ", Message: " + errorThrown);
                        buttonStateChange(false);
                    }
                });
            } else {
                buttonStateChange(false);
            }
        }
        if (isEnglish) {
            messageDialog('Do you want to submit?', "You won't be able to revert this!", 'success', true, 'Submit', 'Cancel', submitFunction);
        } else {
            messageDialog('সাবমিট করতে চান?', "সাবমিটের পর পরিবর্তনযোগ্য না!", 'success', true, 'সাবমিট', 'বাতিল', submitFunction);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }


    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        dateTimeInit("<%=Language%>");
        <%
            long curTime=System.currentTimeMillis();
        %>
        <%if (actionName.equals("add")) {%>
        setMinDateByTimestampAndId("startDate_date_js", <%=curTime%>);
        setMinDateByTimestampAndId("endDate_date_js", <%=curTime%>);
        <%
        }
        %>
        $('#startDate_date_js').on('datepicker.change', () => {
            setMinDateById('endDate_date_js', getDateStringById('startDate_date_js'));
        });
        <%if (actionName.equals("edit")) {%>
        setDateByTimestampAndId('startDate_date_js', <%=leave_type_entitlementDTO.startDate%>);
        setDateByTimestampAndId('endDate_date_js', <%=leave_type_entitlementDTO.endDate%>);
        setMinDateById('endDate_date_js', getDateStringById('startDate_date_js'));
        <%
        }
        %>
        initUi();
        uiValidation();
    });

    function initUi() {
        $('#leaveEncashmentPercentage_text').keydown(function (e) {
            return inputValidationForFloatValue(e, $(this), 2, 100);
        });

        $('#numberOfDays_text').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 365000);
        });

        $('#frequency_text').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 365000);
        });

        $('#minimumJobDuration_text').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 365000);
        });

        $('#atATimeMaxEntitlementDays_text').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 365000);
        });

        $('#maxCarryForwardInYears_text').keydown(function (e) {
            return inputValidationForIntValue(e, $(this), 365000);
        });

    }

    function uiValidation() {
        $.validator.addMethod('leaveTypeSeection', function (value, element) {
            return value > 0;
        });
        $.validator.addMethod('startDateSelection', function (value, element) {
            if (value == "") {
                return true;
            }
            let sd = convertToDate(value);
            let today = new Date();
            today.setHours(0, 0, 0, 0);
            return sd >= today;
        });
        $.validator.addMethod('endDateSelection', function (value, element) {
            if (value == "") {
                return true;
            }
            let sd = $('#startDate_date').val();
            let today = new Date();
            today.setHours(0, 0, 0, 0);
            let ed = convertToDate(value);
            if (sd == "") {
                return ed >= today;
            } else {
                sd = convertToDate(sd);
                return sd >= today && ed >= sd;
            }
        });
        $("#leave_type_entitlement_form").validate({
            rules: {
                leaveTypeCat: {
                    required: true,
                    leaveTypeSeection: true
                },
                startDate: {
                    startDateSelection: true
                },
                endDate: {
                    endDateSelection: true
                }
            },
            messages: {
                leaveTypeCat: "Please select a leave type",
                startDate: "Start date should be after or equal today",
                endDate: "End date should be after or equla todal and start day"
            }
        });
    }

    function isFormValid($form) {
        $('#startDate_date').val(getDateStringById('startDate_date_js'));
        $('#endDate_date').val(getDateStringById('endDate_date_js'));
        const jQueryValid = $form.valid();

        preprocessCheckBoxBeforeSubmitting('hasCarryForward', row);
        preprocessCheckBoxBeforeSubmitting('hasLeaveEncashment', row);
        return jQueryValid && dateValidator("startDate_date_js", false) && dateValidator("endDate_date_js", false);
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Leave_type_entitlementServlet");
    }

    function init(row) {
    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;

    function convertToDate(dateString) {
        const dateParts = dateString.split("/");
        return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }

</script>






