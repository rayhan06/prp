<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="leave_type_entitlement.Leave_type_entitlementDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>

<%
Leave_type_entitlementDTO leave_type_entitlementDTO = (Leave_type_entitlementDTO)request.getAttribute("leave_type_entitlementDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(leave_type_entitlementDTO == null)
{
	leave_type_entitlementDTO = new Leave_type_entitlementDTO();
	
}
System.out.println("leave_type_entitlementDTO = " + leave_type_entitlementDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=leave_type_entitlementDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveTypeCat'>")%>
			
	
	<div class="form-inline" id = 'leaveTypeCat_div_<%=i%>'>
		<select class='form-control'  name='leaveTypeCat' id = 'leaveTypeCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "leave_type", leave_type_entitlementDTO.leaveTypeCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "leave_type", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_numberOfDays'>")%>
			
	
	<div class="form-inline" id = 'numberOfDays_div_<%=i%>'>
		<input type='text' class='form-control'  name='numberOfDays' id = 'numberOfDays_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.numberOfDays + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_frequency'>")%>
			
	
	<div class="form-inline" id = 'frequency_div_<%=i%>'>
		<input type='text' class='form-control'  name='frequency' id = 'frequency_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.frequency + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_minimumJobDuration'>")%>
			
	
	<div class="form-inline" id = 'minimumJobDuration_div_<%=i%>'>
		<input type='text' class='form-control'  name='minimumJobDuration' id = 'minimumJobDuration_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.minimumJobDuration + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_otherCondition1'>")%>
			
	
	<div class="form-inline" id = 'otherCondition1_div_<%=i%>'>
		<input type='text' class='form-control'  name='otherCondition1' id = 'otherCondition1_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.otherCondition1 + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_otherCondition2'>")%>
			
	
	<div class="form-inline" id = 'otherCondition2_div_<%=i%>'>
		<input type='text' class='form-control'  name='otherCondition2' id = 'otherCondition2_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.otherCondition2 + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_otherCondition3'>")%>
			
	
	<div class="form-inline" id = 'otherCondition3_div_<%=i%>'>
		<input type='text' class='form-control'  name='otherCondition3' id = 'otherCondition3_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.otherCondition3 + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_atATimeMaxEntitlementDays'>")%>
			
	
	<div class="form-inline" id = 'atATimeMaxEntitlementDays_div_<%=i%>'>
		<input type='text' class='form-control'  name='atATimeMaxEntitlementDays' id = 'atATimeMaxEntitlementDays_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.atATimeMaxEntitlementDays + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_hasCarryForward'>")%>
			
	
	<div class="form-inline" id = 'hasCarryForward_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='hasCarryForward' id = 'hasCarryForward_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(leave_type_entitlementDTO.hasCarryForward).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_maxCarryForwardInYears'>")%>
			
	
	<div class="form-inline" id = 'maxCarryForwardInYears_div_<%=i%>'>
		<input type='text' class='form-control'  name='maxCarryForwardInYears' id = 'maxCarryForwardInYears_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.maxCarryForwardInYears + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_hasLeaveEncashment'>")%>
			
	
	<div class="form-inline" id = 'hasLeaveEncashment_div_<%=i%>'>
		<input type='checkbox' class='form-control'  name='hasLeaveEncashment' id = 'hasLeaveEncashment_checkbox_<%=i%>' value='true' <%=(actionName.equals("edit") && String.valueOf(leave_type_entitlementDTO.hasLeaveEncashment).equals("true"))?("checked"):""%>   tag='pb_html'><br>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_leaveEncashmentPercentage'>")%>
			
	
	<div class="form-inline" id = 'leaveEncashmentPercentage_div_<%=i%>'>
		<input type='text' class='form-control'  name='leaveEncashmentPercentage' id = 'leaveEncashmentPercentage_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.leaveEncashmentPercentage + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_startDate'>")%>
			
	
	<div class="form-inline" id = 'startDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'startDate_date_<%=i%>' name='startDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_startDate = dateFormat.format(new Date(leave_type_entitlementDTO.startDate));
	%>
	'<%=formatted_startDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_endDate'>")%>
			
	
	<div class="form-inline" id = 'endDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'endDate_date_<%=i%>' name='endDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_endDate = dateFormat.format(new Date(leave_type_entitlementDTO.endDate));
	%>
	'<%=formatted_endDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=leave_type_entitlementDTO.insertionDate%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value='<%=leave_type_entitlementDTO.insertedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value='<%=leave_type_entitlementDTO.modifiedBy%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + leave_type_entitlementDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=leave_type_entitlementDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Leave_type_entitlementServlet?actionType=view&ID=<%=leave_type_entitlementDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Leave_type_entitlementServlet?actionType=view&modal=1&ID=<%=leave_type_entitlementDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	