<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="leave_type_entitlement.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="java.util.List" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LANGUAGE, loginDTO);
    String navigator2 = SessionConstants.NAV_LEAVE_TYPE_ENTITLEMENT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVETYPECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_NUMBEROFDAYS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_FREQUENCY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_MINIMUMJOBDURATION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION1, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION2, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION3, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_ATATIMEMAXENTITLEMENTDAYS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_HASCARRYFORWARD, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_MAXCARRYFORWARDINYEARS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_HASLEAVEENCASHMENT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVEENCASHMENTPERCENTAGE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_SEARCH_LEAVE_TYPE_ENTITLEMENT_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<Leave_type_entitlementDTO> data = (List<Leave_type_entitlementDTO>) session.getAttribute(SessionConstants.VIEW_LEAVE_TYPE_ENTITLEMENT);

            if (data != null && data.size() > 0) {
                for (Leave_type_entitlementDTO leave_type_entitlementDTO : data) {
        %>
        <tr>
            <%@include file="leave_type_entitlementSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>


    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			