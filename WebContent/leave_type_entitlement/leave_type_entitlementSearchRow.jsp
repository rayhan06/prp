<%@page pageEncoding="UTF-8" %>

<%@ page import="pb.*" %>

<td>
    <%=CatDAO.getName(Language, "leave_type", leave_type_entitlementDTO.leaveTypeCat)%>


</td>


<td>
    <%=leave_type_entitlementDTO.numberOfDays %>


</td>


<td>
    <%=leave_type_entitlementDTO.frequency%>


</td>


<td>
    <%=leave_type_entitlementDTO.minimumJobDuration%>


</td>


<td>
    <%=leave_type_entitlementDTO.otherCondition1%>


</td>


<td>
    <%=leave_type_entitlementDTO.otherCondition2%>


</td>


<td>
    <%=leave_type_entitlementDTO.otherCondition3 %>


</td>


<td>
    <%=leave_type_entitlementDTO.atATimeMaxEntitlementDays%>


</td>


<td>


    <%=leave_type_entitlementDTO.hasCarryForward %>


</td>


<td>

    <%=leave_type_entitlementDTO.maxCarryForwardInYears%>


</td>


<td>


    <%=leave_type_entitlementDTO.hasLeaveEncashment %>


</td>


<td>
    <%=leave_type_entitlementDTO.leaveEncashmentPercentage%>


</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Leave_type_entitlementServlet?actionType=view&ID=<%=leave_type_entitlementDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Leave_type_entitlementServlet?actionType=getEditPage&ID=<%=leave_type_entitlementDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=leave_type_entitlementDTO.iD%>'/></span>
    </div>
</td>
																						
											

