

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="leave_type_entitlement.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@ page import="util.StringUtils" %>

<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Leave_type_entitlementDAO leave_type_entitlementDAO = new Leave_type_entitlementDAO("leave_type_entitlement");
Leave_type_entitlementDTO leave_type_entitlementDTO = (Leave_type_entitlementDTO)leave_type_entitlementDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title prp-page-title">
					<i class="fa fa-gift"></i>&nbsp;
					Leave Type Entitlement Details
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body form-body">
			<h5 class="table-title">
				Leave Type Entitlement
			</h5>
			<div class="table-responsive">
				<table class="table table-bordered table-striped text-nowrap">
					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVETYPECAT, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.leaveTypeCat + "";
							%>
							<%
								value = CatDAO.getName(Language, "leave_type", leave_type_entitlementDTO.leaveTypeCat);
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_NUMBEROFDAYS, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.numberOfDays + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_FREQUENCY, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.frequency + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_MINIMUMJOBDURATION, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.minimumJobDuration + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION1, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.otherCondition1 + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION2, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.otherCondition2 + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_OTHERCONDITION3, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.otherCondition3 + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_ATATIMEMAXENTITLEMENTDAYS, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.atATimeMaxEntitlementDays + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_HASCARRYFORWARD, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.hasCarryForward + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_MAXCARRYFORWARDINYEARS, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.maxCarryForwardInYears + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_HASLEAVEENCASHMENT, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.hasLeaveEncashment + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_LEAVEENCASHMENTPERCENTAGE, loginDTO)%></b></td>
						<td>

							<%
								value = leave_type_entitlementDTO.leaveEncashmentPercentage + "";
							%>

							<%=value%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_STARTDATE, loginDTO)%></b></td>
						<td>

							<%
								String formatted_startDate = StringUtils.getFormattedDate(Language,leave_type_entitlementDTO.startDate);
							%>
							<%=formatted_startDate%>


						</td>

					</tr>






					<tr>
						<td style="width:30%"><b><%=LM.getText(LC.LEAVE_TYPE_ENTITLEMENT_EDIT_ENDDATE, loginDTO)%></b></td>
						<td>


							<%
								String formatted_endDate = StringUtils.getFormattedDate(Language,leave_type_entitlementDTO.endDate);
							%>
							<%=formatted_endDate%>


						</td>

					</tr>











				</table>
			</div>
		</div>
	</div>
</div>