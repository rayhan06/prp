<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="asset_supplier.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Asset_supplierDTO asset_supplierDTO;
    asset_supplierDTO = (Asset_supplierDTO) request.getAttribute("asset_supplierDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (asset_supplierDTO == null) {
        asset_supplierDTO = new Asset_supplierDTO();

    }
    System.out.println("asset_supplierDTO = " + asset_supplierDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.ASSET_SUPPLIER_ADD_ASSET_SUPPLIER_ADD_FORMNAME, loginDTO);
    String servletName = "Asset_supplierServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.ASSET_SUPPLIER_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_supplierServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='iD'
                                                   id='iD_hidden_<%=i%>' value='<%=asset_supplierDTO.iD%>'
                                                   tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_NAMEEN, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='nameEn'
                                                           id='nameEn_text_<%=i%>'
                                                           value='<%=asset_supplierDTO.nameEn%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_NAMEBN, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='nameBn'
                                                           id='nameBn_text_<%=i%>'
                                                           value='<%=asset_supplierDTO.nameBn%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_COMPANYWEBSITE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control'
                                                           name='companyWebsite' id='companyWebsite_text_<%=i%>'
                                                           value='<%=asset_supplierDTO.companyWebsite%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_WEBSITE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='website'
                                                           id='website_text_<%=i%>'
                                                           value='<%=asset_supplierDTO.website%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_CONTACTNAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='contactName'
                                                           id='contactName_text_<%=i%>'
                                                           value='<%=asset_supplierDTO.contactName%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_CONTACT1, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='contact1'
                                                           id='contact1_text_<%=i%>'
                                                           value='<%=asset_supplierDTO.contact1%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_CONTACT2, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='contact2'
                                                           id='contact2_text_<%=i%>'
                                                           value='<%=asset_supplierDTO.contact2%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_EMAIL, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='email'
                                                           id='email_text_<%=i%>'
                                                           value='<%=asset_supplierDTO.email%>' tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_COMPANYADDRESS, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <div id='companyAddress_geoDIV_<%=i%>' tag='pb_html'>
                                                        <select class='form-control'
                                                                name='companyAddress_active'
                                                                id='companyAddress_geoSelectField_<%=i%>'
                                                                onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'companyAddress', this.getAttribute('row'))"
                                                                tag='pb_html' row='<%=i%>'></select>
                                                    </div>
                                                    <input type='text' class='form-control'
                                                           name='companyAddress_text' onkeypress="return (event.charCode != 36 && event.keyCode != 36)"
                                                           id='companyAddress_geoTextField_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(asset_supplierDTO.companyAddress)  + "'"):("'" + "" + "'")%>
                                                                   placeholder='Road Number, House Number etc'
                                                    tag='pb_html'>
                                                    <input type='hidden' class='form-control'
                                                           name='companyAddress'
                                                           id='companyAddress_geolocation_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(asset_supplierDTO.companyAddress)  + "'"):("'" + "1" + "'")%>
                                                                   tag='pb_html'>
                                                    <%
                                                        if (actionName.equals("edit")) {
                                                    %>
                                                    <label class="control-label"><%=GeoLocationDAO2.parseText(asset_supplierDTO.companyAddress, Language) + "," + GeoLocationDAO2.parseDetails(asset_supplierDTO.companyAddress)%>
                                                    </label>
                                                    <%
                                                        }
                                                    %>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_IMAGEDROPZONE, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <%
                                                        fileColumnName = "imageDropzone";
                                                        if (actionName.equals("edit")) {
                                                            List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(asset_supplierDTO.imageDropzone);
                                                    %>
                                                    <%@include file="../pb/dropzoneEditor.jsp" %>
                                                    <%
                                                        } else {
                                                            ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                            asset_supplierDTO.imageDropzone = ColumnID;
                                                        }
                                                    %>

                                                    <div class="dropzone"
                                                         action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=asset_supplierDTO.imageDropzone%>">
                                                        <input type='file' style="display:none"
                                                               name='<%=fileColumnName%>File'
                                                               id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                    <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                           id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                           tag='pb_html'/>
                                                    <input type='hidden' name='<%=fileColumnName%>'
                                                           id='<%=fileColumnName%>_dropzone_<%=i%>'
                                                           tag='pb_html'
                                                           value='<%=asset_supplierDTO.imageDropzone%>'/>


                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   id='searchColumn_hidden_<%=i%>'
                                                   value='<%=asset_supplierDTO.searchColumn%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=asset_supplierDTO.insertedByUserId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control'
                                                   name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=asset_supplierDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=asset_supplierDTO.insertionDate%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='lastModifierUser'
                                                   id='lastModifierUser_hidden_<%=i%>'
                                                   value='<%=asset_supplierDTO.lastModifierUser%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>'
                                                   value='<%=asset_supplierDTO.isDeleted%>' tag='pb_html'/>

                                            <input type='hidden' class='form-control'
                                                   name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=asset_supplierDTO.lastModificationTime%>'
                                                   tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3"
                    >
                        <a class="btn cancel-btn btn-sm shadow btn-border-radius text-light"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.ASSET_SUPPLIER_ADD_ASSET_SUPPLIER_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button
                                class="btn  submit-btn btn-sm shadow btn-border-radius text-light"
                                type="submit"
                        >
                            <%=LM.getText(LC.ASSET_SUPPLIER_ADD_ASSET_SUPPLIER_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        return preprocessGeolocationBeforeSubmitting('companyAddress', row, false);

        //return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Asset_supplierServlet");
    }

    function init(row) {

        initGeoLocation('companyAddress_geoSelectField_', row, "Asset_supplierServlet");


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






