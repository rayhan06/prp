<%@page pageEncoding="UTF-8" %>

<%@page import="asset_supplier.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ASSET_SUPPLIER_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ASSET_SUPPLIER;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Asset_supplierDTO asset_supplierDTO = (Asset_supplierDTO) request.getAttribute("asset_supplierDTO");
    CommonDTO commonDTO = asset_supplierDTO;
    String servletName = "Asset_supplierServlet";


    System.out.println("asset_supplierDTO = " + asset_supplierDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Asset_supplierDAO asset_supplierDAO = (Asset_supplierDAO) request.getAttribute("asset_supplierDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id='<%=i%>_nameEn'>
    <%
        value = asset_supplierDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn'>
    <%
        value = asset_supplierDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_companyWebsite'>
    <%
        value = asset_supplierDTO.companyWebsite + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_website'>
    <%
        value = asset_supplierDTO.website + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_contactName'>
    <%
        value = asset_supplierDTO.contactName + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_contact1'>
    <%
        value = asset_supplierDTO.contact1 + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_contact2'>
    <%
        value = asset_supplierDTO.contact2 + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_email'>
    <%
        value = asset_supplierDTO.email + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_companyAddress'>
    <%
        value = asset_supplierDTO.companyAddress + "";
    %>
    <%=GeoLocationDAO2.getAddressToShow(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Asset_supplierServlet?actionType=view&ID=<%=asset_supplierDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Asset_supplierServlet?actionType=getEditPage&ID=<%=asset_supplierDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=asset_supplierDTO.iD%>'/></span>
    </div>
</td>
																						
											

