<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="supplier_institution.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Supplier_institutionDTO supplier_institutionDTO = new Supplier_institutionDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        supplier_institutionDTO = Supplier_institutionDAO.getInstance().getDTOFromID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = supplier_institutionDTO;
    String tableName = "supplier_institution";
%>

<%@include file="../pb/addInitializer2.jsp" %>

<%
    String formTitle = LM.getText(LC.SUPPLIER_INSTITUTION_ADD_SUPPLIER_INSTITUTION_ADD_FORMNAME, loginDTO);
    String servletName = "Supplier_institutionServlet";
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal"
              action="Supplier_institutionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=supplier_institutionDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_NAMEEN, loginDTO)%> *
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>' value='<%=supplier_institutionDTO.nameEn%>'
                                                   required="required" tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_NAMEBN, loginDTO)%> *
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>' value='<%=supplier_institutionDTO.nameBn%>'
                                                   required="required" tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_ADDRESSEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='addressEn'
                                                   id='addressEn_text_<%=i%>'
                                                   value='<%=supplier_institutionDTO.addressEn%>' tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_ADDRESSBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='addressBn'
                                                   id='addressBn_text_<%=i%>'
                                                   value='<%=supplier_institutionDTO.addressBn%>' tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><%=LM.getText(LC.HM_EIGHT_EIGHT, loginDTO)%>
                                                    </div>
                                                </div>
                                                <input type='text' class='form-control' name='mobileNo'
                                                       id='mobileNo_phone_<%=i%>'
                                                       value='<%=Utils.getPhoneNumberWithout88(supplier_institutionDTO.mobileNo, Language)%>'
                                                       tag='pb_html'>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_NIDNO, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='nidNo' id='nidNo_text_<%=i%>'
                                                   value='<%=supplier_institutionDTO.nidNo%>' tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_TINNO, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='tinNo' id='tinNo_text_<%=i%>'
                                                   value='<%=supplier_institutionDTO.tinNo%>' tag='pb_html'/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right">
                                            <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_FILEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "fileDropzone";
                                                if (actionName.equals("ajax_edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(supplier_institutionDTO.fileDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    supplier_institutionDTO.fileDropzone = ColumnID;
                                                }
                                            %>
                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=supplier_institutionDTO.fileDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=supplier_institutionDTO.fileDropzone%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    type="button" onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_SUPPLIER_INSTITUTION_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_SUPPLIER_INSTITUTION_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    function PreprocessBeforeSubmiting(row, action) {
        submitAddForm2();
        return false;
    }
</script>