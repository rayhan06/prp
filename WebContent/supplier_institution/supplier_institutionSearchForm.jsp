<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="supplier_institution.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="java.util.List" %>
<%@ page import="util.StringUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navSUPPLIER_INSTITUTION";
    String servletName = "Supplier_institutionServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>
                <%=UtilCharacter.getDataByLanguage(Language, "নাম", "Name")%>
            </th>
            <th>
                <%=UtilCharacter.getDataByLanguage(Language, "ঠিকানা", "Address")%>
            </th>
            <th>
                <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_TINNO, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.HM_PHONE, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.SUPPLIER_INSTITUTION_SEARCH_SUPPLIER_INSTITUTION_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span><%=UtilCharacter.getDataByLanguage(Language, "সব", "All")%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Supplier_institutionDTO> data = (List<Supplier_institutionDTO>) rn2.list;
            try {
                if (data != null) {
                    for (Supplier_institutionDTO supplier_institutionDTO : data) {
        %>
        <tr>
            <td>
                <%=UtilCharacter.getDataByLanguage(Language, supplier_institutionDTO.nameBn, supplier_institutionDTO.nameEn)%>
            </td>

            <td>
                <%=UtilCharacter.getDataByLanguage(Language, supplier_institutionDTO.addressBn, supplier_institutionDTO.addressEn)%>
            </td>
            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, supplier_institutionDTO.tinNo)%>
            </td>
            <td>
                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, supplier_institutionDTO.mobileNo)%>
            </td>

            <%CommonDTO commonDTO = supplier_institutionDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=supplier_institutionDTO.iD%>'/></span>
                </div>
            </td>
        </tr>
        <%
                    }
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>