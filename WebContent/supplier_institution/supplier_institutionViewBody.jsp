<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="supplier_institution.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="util.StringUtils" %>


<%
    String servletName = "Supplier_institutionServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Supplier_institutionDTO supplier_institutionDTO = Supplier_institutionDAO.getInstance().getDTOFromID(id);
    CommonDTO commonDTO = supplier_institutionDTO;
%>

<%@include file="../pb/viewInitializer.jsp" %>

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_SUPPLIER_INSTITUTION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_SUPPLIER_INSTITUTION_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_NAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=supplier_institutionDTO.nameEn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_NAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=supplier_institutionDTO.nameBn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_ADDRESSEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=supplier_institutionDTO.addressEn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_ADDRESSBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=supplier_institutionDTO.addressBn%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, supplier_institutionDTO.mobileNo)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_NIDNO, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, supplier_institutionDTO.nidNo)%>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_TINNO, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, supplier_institutionDTO.tinNo)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.SUPPLIER_INSTITUTION_ADD_FILEDROPZONE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(supplier_institutionDTO.fileDropzone);
                                        %>
                                        <table>
                                            <tr>
                                                <%
                                                    if (FilesDTOList != null) {
                                                        for (FilesDTO filesDTO : FilesDTOList) {
                                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                %>
                                                <td>
                                                    <%
                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                    %>
                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                         style='width:100px'/>
                                                    <%
                                                        }
                                                    %>
                                                    <a href='Medical_allowanceServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                       download><%=filesDTO.fileTitle%>
                                                    </a>
                                                </td>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>