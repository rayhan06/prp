<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_job_required_files.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_JOB_REQUIRED_FILES_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_RECRUITMENT_JOB_REQUIRED_FILES;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Recruitment_job_required_filesDTO recruitment_job_required_filesDTO = (Recruitment_job_required_filesDTO) request.getAttribute("recruitment_job_required_filesDTO");
    CommonDTO commonDTO = recruitment_job_required_filesDTO;
    String servletName = "Recruitment_job_required_filesServlet";


    System.out.println("recruitment_job_required_filesDTO = " + recruitment_job_required_filesDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Recruitment_job_required_filesDAO recruitment_job_required_filesDAO = (Recruitment_job_required_filesDAO) request.getAttribute("recruitment_job_required_filesDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_nameBn'>
    <%
        value = recruitment_job_required_filesDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_nameEn'>
    <%
        value = recruitment_job_required_filesDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Recruitment_job_required_filesServlet?actionType=view&ID=<%=recruitment_job_required_filesDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;"
            onclick="event.preventDefault(); location.href='Recruitment_job_required_filesServlet?actionType=getEditPage&ID=<%=recruitment_job_required_filesDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID'
                   value='<%=recruitment_job_required_filesDTO.iD%>'/>
        </span>
    </div>
</td>
																						
											

