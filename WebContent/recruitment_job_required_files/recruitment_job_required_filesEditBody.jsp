<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="recruitment_job_required_files.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%
    Recruitment_job_required_filesDTO recruitment_job_required_filesDTO;
    recruitment_job_required_filesDTO = (Recruitment_job_required_filesDTO) request.getAttribute("recruitment_job_required_filesDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (recruitment_job_required_filesDTO == null) {
        recruitment_job_required_filesDTO = new Recruitment_job_required_filesDTO();

    }
    System.out.println("recruitment_job_required_filesDTO = " + recruitment_job_required_filesDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.RECRUITMENT_JOB_REQUIRED_FILES_ADD_RECRUITMENT_JOB_REQUIRED_FILES_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    String Language = LM.getText(LC.RECRUITMENT_JOB_REQUIRED_FILES_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Recruitment_job_required_filesServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_REQUIRED_FILES_ADD_NAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + recruitment_job_required_filesDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_REQUIRED_FILES_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input
                                                    type='text'
                                                    class='form-control'
                                                    name='nameEn'
                                                    id='nameEn_text_<%=i%>'
                                                    value=<%=actionName.equals("edit")?("'" + recruitment_job_required_filesDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'
                                            />
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=recruitment_job_required_filesDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_job_required_filesDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_job_required_filesDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_job_required_filesDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + recruitment_job_required_filesDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.RECRUITMENT_JOB_REQUIRED_FILES_ADD_RECRUITMENT_JOB_REQUIRED_FILES_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                            <%=LM.getText(LC.RECRUITMENT_JOB_REQUIRED_FILES_ADD_RECRUITMENT_JOB_REQUIRED_FILES_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    let lang = '<%=Language%>';


    $(document).ready(function () {

        dateTimeInit("<%=Language%>");

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                nameBn: "required",
                nameEn: "required",
            },
            messages: {
                nameBn: valueByLanguage(lang,"প্রয়োজনীয়", "Required" ),
                nameEn: valueByLanguage(lang,"প্রয়োজনীয়", "Required" ),

            }
        });
    });

    function valueByLanguage(language, bnValue, enValue){
        if(language == 'english' || language == 'English'){
            return enValue;
        } else {
            return bnValue;
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function formSubmit() {
        event.preventDefault();
        buttonStateChange(true);
        let form = $("#bigform");
        form.validate();
        let valid = form.valid();
        if(valid){
            let url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }

    }




</script>






