
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="job_applicant_reference.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>

<%
	Job_applicant_referenceDAO job_applicant_referenceDAO = new Job_applicant_referenceDAO();
Job_applicant_referenceDTO job_applicant_referenceDTO = job_applicant_referenceDAO.getDTOByJobApplicantID(600).get(0);
//job_applicant_referenceDTO = (Job_applicant_referenceDTO)request.getAttribute("job_applicant_referenceDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(job_applicant_referenceDTO == null)
{
	job_applicant_referenceDTO = new Job_applicant_referenceDTO();
	
}
System.out.println("job_applicant_referenceDTO = " + job_applicant_referenceDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_JOB_APPLICANT_REFERENCE_ADD_FORMNAME, loginDTO);


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
				








<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
				<%@ page import="category.CategoryDAO" %>
				<%@ page import="category.CategoryDTO" %>

				<%
String Language = LM.getText(LC.JOB_APPLICANT_REFERENCE_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);

	CategoryDAO categoryDAO = new CategoryDAO();

	CategoryDTO categoryDTO = (CategoryDTO) categoryDAO.getDTOByDomainNameAndValue("relationship",job_applicant_referenceDTO.relationshipCat );
	String relationship = LM.getText(categoryDTO.languageId, loginDTO) ;

%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=job_applicant_referenceDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='jobApplicantId' id = 'jobApplicantId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_referenceDTO.jobApplicantId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
				<input type='hidden' class='form-control'  name='jobId' id = 'jobId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_referenceDTO.jobId + "'"):("'" + request.getParameter("jobId") + "'")%> tag='pb_html'/>









				<table style="border: 1px solid gray; width: 100%;">


					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_NAMEBNG, loginDTO)%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_referenceDTO.nameBng%>
							</div>

						</td>

					</tr>

					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_DESIGNATION, loginDTO)%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_referenceDTO.designation%>
							</div>

						</td>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_DEPARTMENT, loginDTO)%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_referenceDTO.department%>
							</div>

						</td>

					</tr>

					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_MAILADDRESS, loginDTO)%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_referenceDTO.mailAddress%>
							</div>

						</td>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_RELATIONSHIPCAT, loginDTO)%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=relationship%>
							</div>

						</td>

					</tr>

					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_PHONENO, loginDTO)%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_referenceDTO.phoneNo%>
							</div>

						</td>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_ALTERNATEPHONENO, loginDTO)%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_referenceDTO.alternatePhoneNo%>
							</div>

						</td>

					</tr>

					<tr>

						<td valign="top" class="col-lg-3 control-label-2" name="labels" style="font-size: large; width: 20%; height: auto; border: 1px solid gray;"  >

							<%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_REFADDRESS, loginDTO)%>

						</td>

						<td valign="top" class="form-group col-lg-3 font18" style="font-size: large; width: 30%; height: auto; border: 1px solid gray;">

							<div id="">
								<%=job_applicant_referenceDTO.refAddress%>
							</div>

						</td>

					</tr>

				</table>




				

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_referenceDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_referenceDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + job_applicant_referenceDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + job_applicant_referenceDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					



	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    basicInit();
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	preprocessGeolocationBeforeSubmitting('mailAddress', row, false);
	preprocessGeolocationBeforeSubmitting('refAddress', row, false);

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Job_applicant_referenceServlet");	
}

function init(row)
{

			initGeoLocation('mailAddress_geoSelectField_', row, "Job_applicant_referenceServlet");
			initGeoLocation('refAddress_geoSelectField_', row, "Job_applicant_referenceServlet");

	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






