
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_reference.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>
<%@ page import="com.google.gson.Gson" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";
}

String value = "";
String Language = LM.getText(LC.JOB_APPLICANT_REFERENCE_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Job_applicant_referenceDAO job_applicant_referenceDAO = new Job_applicant_referenceDAO();





String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements())
	{

		String paramName = parameterNames.nextElement();

		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++)
			{
				String paramValue = paramValues[i];

				%>
				
				<%

			}
		}


	}
}

					String formTitle = LM.getText(LC.JOB_APPLICANT_REFERENCE_SEARCH_ANYFIELD, loginDTO);

				%>

<%--<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>--%>

<div class="col-lg-12">
	<div class="kt-portlet shadow-none" style="margin-top: -20px">

		<div class="kt-portlet__body form-body">


				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead class="thead-light">
							<tr>
<%--								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_NAMEENG, loginDTO)%></th>--%>
								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_NAMEBNG, loginDTO)%></th>
								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_DESIGNATION, loginDTO)%></th>
								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_DEPARTMENT, loginDTO)%></th>
<%--								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_MAILADDRESS, loginDTO)%></th>--%>
								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_RELATIONSHIPCAT, loginDTO)%></th>
								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_PHONENO, loginDTO)%></th>
<%--								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_ALTERNATEPHONENO, loginDTO)%></th>--%>
<%--								<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.JOB_APPLICANT_REFERENCE_ADD_REFADDRESS, loginDTO)%></th>--%>



							</tr>
						</thead>
						<tbody>
							<%
								List<Job_applicant_referenceDTO> data = new ArrayList<>();

								if (request.getParameter("summary") != null && Boolean.parseBoolean(request.getParameter("summary"))) {

									List<Job_applicant_referenceSummaryDTO> summaryData = new ArrayList<>();

									Gson gson = new Gson();
									summaryData = (List<Job_applicant_referenceSummaryDTO>)request.getSession(true).getAttribute("job_applicant_referenceSummaryDTOList");

									if (summaryData != null) {
										for (Job_applicant_referenceSummaryDTO summary: summaryData) {

											String json = gson.toJson(summary);
											Job_applicant_referenceDTO dto = gson.fromJson(json, Job_applicant_referenceDTO.class);
											data.add(dto);
										}
									}

								}

								else {
									data = job_applicant_referenceDAO.getDTOByJobApplicantID(Long.parseLong(request.getParameter("ID")));
								}
								if (request.getParameter("summary") != null && Boolean.parseBoolean(request.getParameter("summary"))) {

									List<Job_applicant_referenceSummaryDTO> summaryData = new ArrayList<>();

									Gson gson = new Gson();
									summaryData = (List<Job_applicant_referenceSummaryDTO>)request.getSession(true).getAttribute("job_applicant_referenceSummaryDTOList");

									if (summaryData != null) {
										for (Job_applicant_referenceSummaryDTO summary: summaryData) {

											String json = gson.toJson(summary);
											Job_applicant_referenceDTO dto = gson.fromJson(json, Job_applicant_referenceDTO.class);
											data.add(dto);
										}
									}

								}

								else {
									data = job_applicant_referenceDAO.getDTOByJobApplicantID(Long.parseLong(request.getParameter("ID")));
								}
								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Job_applicant_referenceDTO job_applicant_referenceDTO = data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("job_applicant_referenceDTO",job_applicant_referenceDTO);
								%>  
								
								 <jsp:include page="./job_applicant_referenceSummarySearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

</div>
</div>
</div>




			