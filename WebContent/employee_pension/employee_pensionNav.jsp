<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Employee Pension";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.GATE_PASS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>


<!-- search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="container-fluid">
            <div class="form-group row">
                <label class="col-md-2 col-form-label">
                    <%=Language.equalsIgnoreCase("English") ? "Employee's Pension Starting Year" : "কর্মকর্তা/কর্মচারীর পেনশনের বছর"%>
                </label>
                <div class="col-md-4">
                    <select class='form-control rounded' name='fiscal_year' onchange="fetchRecords('',0)"
                            id='fiscal_year'  tag='pb_html'>
                    </select>
                </div>
            </div>

        </div>
        <!-- END FORM-->
    </div>


    <%@include file="../common/pagination_with_go2.jsp" %>


    <template id="loader">
        <div class="modal-body">
            <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
            <span>Loading...</span>
        </div>
    </template>


    <script type="text/javascript">

        $(document).ready(function () {
            const year_range = 10;
            const fiscal_year_element = $('#fiscal_year');
            const today = new Date();
            const total_year_range = 2 * year_range;
            let startYear = today.getFullYear() - year_range;
            let prevYear = startYear - 1;
            let selectedYearAtStartup;
            if(today.getMonth() > 5) {
                selectedYearAtStartup = today.getFullYear();
            } else {
                selectedYearAtStartup = today.getFullYear() - 1;
            }


            <%
                if (Language.equalsIgnoreCase("English")) {
            %>
                    fiscal_year_element.append(
                        $('<option></option>').val(-1).html("Select")
                    );
            <%
                } else {
            %>
                    fiscal_year_element.append(
                        $('<option></option>').val(-1).html("বাছাই করুন")
                    );
            <%
                }
            %>

            for (let i = 0; i < total_year_range; i++) {
                startYear = startYear + 1;
                prevYear = prevYear + 1;
                <%
                    if (Language.equalsIgnoreCase("English")) {
                %>
                    if(selectedYearAtStartup==prevYear) {
                        fiscal_year_element.append(
                            "<option value=\""+startYear+"\" selected>"+prevYear+"-"+startYear+"</option>"
                        );
                    } else {
                        fiscal_year_element.append(
                            $('<option></option>').val(prevYear).html(prevYear + "-" + startYear)
                        );
                    }
                <%
                    } else {
                %>
                    if(selectedYearAtStartup==startYear) {
                        fiscal_year_element.append(
                            "<option value=\""+startYear+"\" selected>"+convertEnglishDigitToBangla(prevYear)+"-"+convertEnglishDigitToBangla(startYear)+"</option>"
                        );
                    } else {
                        fiscal_year_element.append(
                            $('<option></option>').val(prevYear).html(convertEnglishDigitToBangla(prevYear) + "-" + convertEnglishDigitToBangla(startYear))
                        );
                    }
                <%
                    }
                %>

            }
        });

        function dosubmit(params) {
            document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
            //alert(params);
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                } else if (this.readyState == 4 && this.status != 200) {
                    alert('failed ' + this.status);
                }
            };

            xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
            xhttp.send();

        }

        function convertEnglishDigitToBangla(str){
            str = String(str);
            str = str.replaceAll('0', '০');
            str = str.replaceAll('1', '১');
            str = str.replaceAll('2', '২');
            str = str.replaceAll('3', '৩');
            str = str.replaceAll('4', '৪');
            str = str.replaceAll('5', '৫');
            str = str.replaceAll('6', '৬');
            str = str.replaceAll('7', '৭');
            str = str.replaceAll('8', '৮');
            str = str.replaceAll('9', '৯');
            return str;
        }
        function allfield_changed(go, pagination_number) {
            fetchRecords(go, pagination_number);
        }
        function fetchRecords(go, pagination_number) {
            let selected_year = $('#fiscal_year').val();
            if (selected_year==-1)
                return;

            if ($('#search-result-div').is(":hidden")) {
                $('#search-result-div').show();
                $('.my-4').show();
                $('#horizontal-line').show();
            }

            let params = 'fiscal_year=' + selected_year;

            let extraParams = document.getElementsByName('extraParam');
            extraParams.forEach((param) => {
                params += "&" + param.getAttribute("tag") + "=" + param.value;
            })

            params += '&search=true&firstReload=false';

            var pageNo = document.getElementsByName('pageno')[0].value;
            var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

            var totalRecords = 0;
            var lastSearchTime = 0;
            if (document.getElementById('hidden_totalrecords')) {
                totalRecords = document.getElementById('hidden_totalrecords').value;
                lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
            }

            if (go !== '' && searchChanged == 0) {
                console.log("go found");
                params += '&go=1';
                pageNo = document.getElementsByName('pageno')[pagination_number].value;
                rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
                setPageNoInAllFields(pageNo);
                setRPPInAllFields(rpp);
            }

            params += '&pageno=' + pageNo;
            params += '&RECORDS_PER_PAGE=' + rpp;
            params += '&TotalRecords=' + totalRecords;
            params += '&lastSearchTime=' + lastSearchTime;
            dosubmit(params);
        }

    </script>

