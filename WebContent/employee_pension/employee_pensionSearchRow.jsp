<%@page pageEncoding="UTF-8" %>

<%@page import="gate_pass.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganograms" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="office_unit_organograms.Office_unit_organogramsDTO" %>
<%@ page import="employee_offices.Employee_officesDTO" %>
<%@ page import="employee_offices.EmployeeOfficeDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_RECORDS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_EMPLOYEE_PENSION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    int i = Integer.parseInt(request.getParameter("rownum"));;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Employee_recordsDTO employee_recordsDTO = (Employee_recordsDTO) request.getAttribute("employee_recordsDTO");


    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    OfficeUnitOrganograms officeUnitOrganograms = null;
    EmployeeOfficeDTO employeeOfficeDTO = null;
    String job_grade = "";

%>

<td id='name_<%=i%>'>
    <%=Language.equalsIgnoreCase("English") ? employee_recordsDTO.nameEng : employee_recordsDTO.nameBng%>
</td>

<td id='designation_<%=i%>'>
    <%
        try {
            employeeOfficeDTO = EmployeeOfficeRepository.getInstance().getByEmployeeRecordIdIsDefault(employee_recordsDTO.iD);
            officeUnitOrganograms = OfficeUnitOrganogramsRepository.getInstance().getById(employeeOfficeDTO.officeUnitOrganogramId);
        } catch (Exception ex) {
            System.out.println("got Exception in employee office and organogram table!");
        }
    %>
    <%=officeUnitOrganograms != null ? (Language.equalsIgnoreCase("English") ? officeUnitOrganograms.designation_eng : officeUnitOrganograms.designation_bng) : ""%>
</td>

<td id='office_<%=i%>'>
    <%=employeeOfficeDTO != null ? Office_unitsRepository.getInstance().geText(Language, employeeOfficeDTO.officeUnitId) : ""%>
</td>

<td id='grade_<%=i%>'>
    <%
        if(officeUnitOrganograms != null)
            try {
                job_grade = CatRepository.getName(Language, "job_grade", officeUnitOrganograms.job_grade_type_cat);
            } catch (Exception ex) {
                System.out.println("got Exception from Cat table!");
            }
    %>
    <%=job_grade%>
</td>

<td id='misc_<%=i%>'>
    <%=""%>
</td>

<td id='startDate_<%=i%>'>
    <%=Utils.getDigits(sdf.format(new Date(employee_recordsDTO.retirementDate)), Language)%>
</td>

<td style="min-width:260px">
    <div class="input-group">
        <textarea class="form-control" name="remarks_<%=i%>" id="remarks_<%=i%>" rows="5" cols="50" maxlength="1024" style="resize: none;"><%=employee_recordsDTO.pensionRemarks != null ? employee_recordsDTO.pensionRemarks : " "%></textarea>
        <button class="btn btn-outline-info w-100 mt-2" type="button" onclick="updateRemarks('remarks_<%=i%>', '<%=employee_recordsDTO.iD%>')"><%=Language.equalsIgnoreCase("English") ? "Submit" : "সম্পাদন"%></button>
    </div>
</td>



<script>

    function updateRemarks(text_input_id, id) {
        let remarks_text = $('#'+text_input_id).val();


        // ajax call for updating the remarks and after finish show swal
        let url = "Employee_recordsServlet?actionType=updateRemarks" + "&remarks="+ remarks_text+"&id="+id;
        $.ajax({
            url: url,
            type: "POST",
            async: true,
            success: function (data) {
                console.log(data);
                Swal.fire({
                    type: 'success',
                    confirmButtonText: '<%=Language.equalsIgnoreCase("English") ? "OK": "ঠিক আছে"%>',
                    title: '<%=Language.equalsIgnoreCase("English") ? "NOTIFICATION" : "বিজ্ঞপ্তি"%>',
                    text: '<%=Language.equalsIgnoreCase("English") ? "Remarks has been updated successfully" : "মন্তব্যটি সংরক্ষন করা হয়েছে!"%>'
                });
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

</script>




