<%@page pageEncoding="UTF-8" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>

<%
    String url = "Employee_recordsServlet?actionType=pension";
    String navigator2 = SessionConstants.NAV_EMPLOYEE_PENSION;
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String pageno = "";
    String Language = LM.getText(LC.EMPLOYEE_RECORDS_EDIT_LANGUAGE, loginDTO);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator2);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    boolean isPermanentTable = rn.m_isPermanentTable;

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-money-bill fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            <%=Language.equalsIgnoreCase("English") ? "PENSION MANAGEMENT" : "পেনশন ব্যবস্থাপনা"%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="./employee_pensionNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator2%>"/>
                <jsp:param name="pageName"
                           value="PENSION MANAGEMENT" />
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <form action="Gate_passServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                          method="POST"
                          id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="employee_pensionSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="PENSION MANAGEMENT" />
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<% pagination_number = 1;%>
<%@include file="../common/pagination_with_go2.jsp" %>


<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    window.onload = function () {
        $("#fiscal_year").select2({
            dropdownAutoWidth: true
        });
    }
</script>


