<%@page pageEncoding="UTF-8" %>

<%@page import="sms_log.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.SMS_LOG_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "dd/MM/yyyy" );

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_SMS_LOG;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Sms_logDTO sms_logDTO = (Sms_logDTO)request.getAttribute("sms_logDTO");
CommonDTO commonDTO = sms_logDTO;
String servletName = "Sms_logServlet";


System.out.println("sms_logDTO = " + sms_logDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Sms_logDAO sms_logDAO = (Sms_logDAO)request.getAttribute("sms_logDAO");


String Options = "";
boolean formSubmit = false;

%>

											
		
											
											<td id = '<%=i%>_sender'>
											<%
											value = sms_logDTO.sender + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_receiver'>
											<%
											value = sms_logDTO.receiver + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_smsReceiverTypeCat'>
											<%
											value = "N/A";
											%>
											<%
											//value = CatDAO.getName(Language, "sms_receiver_type", sms_logDTO.smsReceiverTypeCat);
											%>											
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_message'>
											<%
											value = sms_logDTO.message + "";
											%>
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_smsStatusCat'>
											<%
											value = "N/A";
											%>
											<%
											//value = CatDAO.getName(Language, "sms_status", sms_logDTO.smsStatusCat);
											%>											
														
											<%=value%>
				
			
											</td>
		
											
											<td id = '<%=i%>_insertionTime'>
											<%
											value = sms_logDTO.insertionTime + "";
											%>
											<%
											String formatted_insertionTime = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_insertionTime%>
				
			
											</td>
		
											
											<td id = '<%=i%>_fetchingTime'>
											<%
											value = sms_logDTO.fetchingTime + "";
											%>
											<%
											String formatted_fetchingTime = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_fetchingTime%>
				
			
											</td>
		
											
											<td id = '<%=i%>_sendingTime'>
											<%
											value = sms_logDTO.sendingTime + "";
											%>
											<%
											String formatted_sendingTime = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_sendingTime%>
				
			
											</td>
		
											
											<td id = '<%=i%>_deliveryTime'>
											<%
											value = sms_logDTO.deliveryTime + "";
											%>
											<%
											String formatted_deliveryTime = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=formatted_deliveryTime%>
				
			
											</td>
		
	

	
											<td id = '<%=i%>_Edit'>																																	
												<a href="#"  onclick='fixedToEditable(<%=i%>,"", "<%=sms_logDTO.iD%>", <%=isPermanentTable%>, "Sms_logServlet")'><%=LM.getText(LC.SMS_LOG_SEARCH_SMS_LOG_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span id='chkEdit' ><input type='checkbox' name='ID' value='<%=sms_logDTO.iD%>'/></span>
												</div>
											</td>
																						
											

