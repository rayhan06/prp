<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="sms_log.Sms_logDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
Sms_logDTO sms_logDTO = (Sms_logDTO)request.getAttribute("sms_logDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(sms_logDTO == null)
{
	sms_logDTO = new Sms_logDTO();
	
}
System.out.println("sms_logDTO = " + sms_logDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.SMS_LOG_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=sms_logDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_sender'>")%>
			
	
	<div class="form-inline" id = 'sender_div_<%=i%>'>
		<input type='text' class='form-control'  name='sender' id = 'sender_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + sms_logDTO.sender + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_receiver'>")%>
			
	
	<div class="form-inline" id = 'receiver_div_<%=i%>'>
		<input type='text' class='form-control'  name='receiver' id = 'receiver_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + sms_logDTO.receiver + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_smsReceiverTypeCat'>")%>
			
	
	<div class="form-inline" id = 'smsReceiverTypeCat_div_<%=i%>'>
		<select class='form-control'  name='smsReceiverTypeCat' id = 'smsReceiverTypeCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "sms_receiver_type", sms_logDTO.smsReceiverTypeCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "sms_receiver_type", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_message'>")%>
			
	
	<div class="form-inline" id = 'message_div_<%=i%>'>
		<input type='text' class='form-control'  name='message' id = 'message_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + sms_logDTO.message + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_smsStatusCat'>")%>
			
	
	<div class="form-inline" id = 'smsStatusCat_div_<%=i%>'>
		<select class='form-control'  name='smsStatusCat' id = 'smsStatusCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "sms_status", sms_logDTO.smsStatusCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "sms_status", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionTime'>")%>
			
	
	<div class="form-inline" id = 'insertionTime_div_<%=i%>'>
		<input type='date' class='form-control'  name='insertionTime_Date_<%=i%>' id = 'insertionTime_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_insertionTime = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_insertionTime = format_insertionTime.format(new Date(sms_logDTO.insertionTime));
	%>
	'<%=formatted_insertionTime%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='insertionTime' id = 'insertionTime_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + sms_logDTO.insertionTime + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_fetchingTime'>")%>
			
	
	<div class="form-inline" id = 'fetchingTime_div_<%=i%>'>
		<input type='date' class='form-control'  name='fetchingTime_Date_<%=i%>' id = 'fetchingTime_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_fetchingTime = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_fetchingTime = format_fetchingTime.format(new Date(sms_logDTO.fetchingTime));
	%>
	'<%=formatted_fetchingTime%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='fetchingTime' id = 'fetchingTime_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + sms_logDTO.fetchingTime + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_sendingTime'>")%>
			
	
	<div class="form-inline" id = 'sendingTime_div_<%=i%>'>
		<input type='date' class='form-control'  name='sendingTime_Date_<%=i%>' id = 'sendingTime_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_sendingTime = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_sendingTime = format_sendingTime.format(new Date(sms_logDTO.sendingTime));
	%>
	'<%=formatted_sendingTime%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='sendingTime' id = 'sendingTime_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + sms_logDTO.sendingTime + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_deliveryTime'>")%>
			
	
	<div class="form-inline" id = 'deliveryTime_div_<%=i%>'>
		<input type='date' class='form-control'  name='deliveryTime_Date_<%=i%>' id = 'deliveryTime_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_deliveryTime = new SimpleDateFormat("yyyy-MM-dd");
	String formatted_deliveryTime = format_deliveryTime.format(new Date(sms_logDTO.deliveryTime));
	%>
	'<%=formatted_deliveryTime%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
		<input type='hidden' class='form-control'  name='deliveryTime' id = 'deliveryTime_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + sms_logDTO.deliveryTime + "'"):("'" + "0" + "'")%>  tag='pb_html'>
		
	</div>
				
<%=("</td>")%>
					
		