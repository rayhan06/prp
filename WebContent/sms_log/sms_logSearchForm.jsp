
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="sms_log.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.SMS_LOG_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Sms_logDAO sms_logDAO = (Sms_logDAO)request.getAttribute("sms_logDAO");


String navigator2 = SessionConstants.NAV_SMS_LOG;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";
%>				
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_SENDER, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_RECEIVER, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_SMSRECEIVERTYPECAT, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_MESSAGE, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_SMSSTATUSCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_INSERTIONTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_FETCHINGTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_SENDINGTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_EDIT_DELIVERYTIME, loginDTO)%></th>
								<th><%=LM.getText(LC.SMS_LOG_SEARCH_SMS_LOG_EDIT_BUTTON, loginDTO)%></th>
								<th><input type="submit" class="btn btn-xs btn-danger" value="<%=LM.getText(LC.SMS_LOG_SEARCH_SMS_LOG_DELETE_BUTTON, loginDTO)%>" /></th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_SMS_LOG);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Sms_logDTO sms_logDTO = (Sms_logDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("sms_logDTO",sms_logDTO);
								%>  
								
								 <jsp:include page="./sms_logSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">

function getOriginal(i, tempID, parentID, ServletName)
{
	console.log("getOriginal called");
	var idToSubmit;
	var isPermanentTable;
	var state = document.getElementById(i + "_original_status").value;
	if(state == 0)
	{
		idToSubmit = parentID;
		isPermanentTable = true;
	}
	else
	{
		idToSubmit = tempID;
		isPermanentTable = false;
	}
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{			
			var response = JSON.parse(this.responseText);
			document.getElementById(i + "_sender").innerHTML = response.sender;
			document.getElementById(i + "_receiver").innerHTML = response.receiver;
			document.getElementById(i + "_smsReceiverTypeCat").innerHTML = response.smsReceiverTypeCat;
			document.getElementById(i + "_message").innerHTML = response.message;
			document.getElementById(i + "_smsStatusCat").innerHTML = response.smsStatusCat;
			document.getElementById(i + "_insertionTime").innerHTML = response.insertionTime;
			document.getElementById(i + "_fetchingTime").innerHTML = response.fetchingTime;
			document.getElementById(i + "_sendingTime").innerHTML = response.sendingTime;
			document.getElementById(i + "_deliveryTime").innerHTML = response.deliveryTime;
					
			if(state == 0)
			{
				document.getElementById(i + "_getOriginal").innerHTML = "View Edited";
				state = 1;
			}
			else
			{
				document.getElementById(i + "_getOriginal").innerHTML = "View Original";
				state = 0;
			}
			
			document.getElementById(i + "_original_status").value = state;
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	};
	xhttp.open("POST", ServletName + "?actionType=getDTO&ID=" + idToSubmit + "&isPermanentTable=" + isPermanentTable, true);
	xhttp.send();
}





function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	preprocessDateBeforeSubmitting('insertionTime', row);	
	preprocessDateBeforeSubmitting('fetchingTime', row);	
	preprocessDateBeforeSubmitting('sendingTime', row);	
	preprocessDateBeforeSubmitting('deliveryTime', row);	

	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Sms_logServlet");	
}

function init(row)
{


	
}




function submitAjax(i, deletedStyle)
{
	console.log('submitAjax called');
	var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
	if(isSubmittable == false)
	{
		return;
	}
	var formData = new FormData();
	var value;
	value = document.getElementById('iD_hidden_' + i).value;
	console.log('submitAjax i = ' + i + ' id = ' + value);
	formData.append('iD', value);
	formData.append("identity", value);
	formData.append("ID", value);
	formData.append('sender', document.getElementById('sender_text_' + i).value);
	formData.append('receiver', document.getElementById('receiver_text_' + i).value);
	formData.append('smsReceiverTypeCat', document.getElementById('smsReceiverTypeCat_category_' + i).value);
	formData.append('message', document.getElementById('message_text_' + i).value);
	formData.append('smsStatusCat', document.getElementById('smsStatusCat_category_' + i).value);
	formData.append('insertionTime', document.getElementById('insertionTime_date_' + i).value);
	formData.append('fetchingTime', document.getElementById('fetchingTime_date_' + i).value);
	formData.append('sendingTime', document.getElementById('sendingTime_date_' + i).value);
	formData.append('deliveryTime', document.getElementById('deliveryTime_date_' + i).value);

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (this.readyState == 4 && this.status == 200) 
		{
			if(this.responseText !='')
			{				
				document.getElementById('tr_' + i).innerHTML = this.responseText ;
				ShowExcelParsingResult(i);
			}
			else
			{
				console.log("No Response");
				document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
			}
		}
		else if(this.readyState == 4 && this.status != 200)
		{
			alert('failed ' + this.status);
		}
	  };
	xhttp.open("POST", 'Sms_logServlet?actionType=edit&inplacesubmit=true&isPermanentTable=<%=isPermanentTable%>&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
	xhttp.send(formData);
}

window.onload =function ()
{
	ShowExcelParsingResult('general');
	}	
</script>
			