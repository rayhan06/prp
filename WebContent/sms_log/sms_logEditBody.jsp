<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="sms_log.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>


<%
    Sms_logDTO sms_logDTO;
    sms_logDTO = (Sms_logDTO) request.getAttribute("sms_logDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (sms_logDTO == null) {
        sms_logDTO = new Sms_logDTO();

    }
    System.out.println("sms_logDTO = " + sms_logDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.SMS_LOG_EDIT_SMS_LOG_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.SMS_LOG_ADD_SMS_LOG_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%>
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal"
              action="Sms_logServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="form-body">


                <%@ page import="java.text.SimpleDateFormat" %>
                <%@ page import="java.util.Date" %>

                <%@ page import="pb.*" %>

                <%
                    String Language = LM.getText(LC.SMS_LOG_EDIT_LANGUAGE, loginDTO);
                    String Options;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    String datestr = dateFormat.format(date);
                %>


                <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>' value='<%=sms_logDTO.iD%>'
                       tag='pb_html'/>


                <%--<label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.SMS_LOG_EDIT_SENDER, loginDTO)) : (LM.getText(LC.SMS_LOG_ADD_SENDER, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='sender_div_<%=i%>'>
                        <input type='text' class='form-control' name='sender' id='sender_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + sms_logDTO.sender + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>--%>
                <input type='hidden' name='sender' value="00" />


                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.SMS_LOG_EDIT_RECEIVER, loginDTO)) : (LM.getText(LC.SMS_LOG_ADD_RECEIVER, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='receiver_div_<%=i%>'>
                        <input type='text' class='form-control' name='receiver' id='receiver_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + sms_logDTO.receiver + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>


                <%--<label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.SMS_LOG_EDIT_SMSRECEIVERTYPECAT, loginDTO)) : (LM.getText(LC.SMS_LOG_ADD_SMSRECEIVERTYPECAT, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='smsReceiverTypeCat_div_<%=i%>'>
                        <select class='form-control' name='smsReceiverTypeCat' id='smsReceiverTypeCat_category_<%=i%>'
                                tag='pb_html'>
                            <%
                                if (actionName.equals("edit")) {
                                    Options = CatDAO.getOptions(Language, "sms_receiver_type", sms_logDTO.smsReceiverTypeCat);
                                } else {
                                    Options = CatDAO.getOptions(Language, "sms_receiver_type", -1);
                                }
                            %>
                            <%=Options%>
                        </select>

                    </div>
                </div>--%>
				<input type="hidden" name="smsReceiverTypeCat" value="0" />

                <label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.SMS_LOG_EDIT_MESSAGE, loginDTO)) : (LM.getText(LC.SMS_LOG_ADD_MESSAGE, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='message_div_<%=i%>'>
                        <input type='text' class='form-control' name='message' id='message_text_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + sms_logDTO.message + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                    </div>
                </div>


                <%--<label class="col-lg-3 control-label">
                    <%=(actionName.equals("edit")) ? (LM.getText(LC.SMS_LOG_EDIT_SMSSTATUSCAT, loginDTO)) : (LM.getText(LC.SMS_LOG_ADD_SMSSTATUSCAT, loginDTO))%>
                </label>
                <div class="form-group ">
                    <div class="col-lg-6 " id='smsStatusCat_div_<%=i%>'>
                        <select class='form-control' name='smsStatusCat' id='smsStatusCat_category_<%=i%>'
                                tag='pb_html'>
                            <%
                                if (actionName.equals("edit")) {
                                    Options = CatDAO.getOptions(Language, "sms_status", sms_logDTO.smsStatusCat);
                                } else {
                                    Options = CatDAO.getOptions(Language, "sms_status", -1);
                                }
                            %>
                            <%=Options%>
                        </select>

                    </div>
                </div>--%>
				<input type="hidden" name="smsStatusCat" value="0" />

                <div class="form-actions text-center">
                    <a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%=LM.getText(LC.SMS_LOG_EDIT_SMS_LOG_CANCEL_BUTTON, loginDTO)%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.SMS_LOG_ADD_SMS_LOG_CANCEL_BUTTON, loginDTO)%>
                        <%
                            }

                        %>
                    </a>
                    <button class="btn btn-success" type="submit">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%=LM.getText(LC.SMS_LOG_EDIT_SMS_LOG_SUBMIT_BUTTON, loginDTO)%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.SMS_LOG_ADD_SMS_LOG_SUBMIT_BUTTON, loginDTO)%>
                        <%
                            }
                        %>
                    </button>
                </div>

            </div>

        </form>

    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Sms_logServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






