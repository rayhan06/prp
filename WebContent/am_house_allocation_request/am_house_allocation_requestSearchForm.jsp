
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="am_house_allocation_request.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="am_house.Am_houseRepository" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryDTO" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>
<%@page pageEncoding="UTF-8" %>


<%
String navigator2 = "navAM_HOUSE_ALLOCATION_REQUEST";
String servletName = "Am_house_allocation_requestServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERORGID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITID, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEREMPID, loginDTO)%></th>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERPHONENUM, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DATEOFBIRTH, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_MARITALSTATUSCAT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALEID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALETYPEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALETYPEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_BASICSALARYEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_BASICSALARYBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASSPECIALSALARY, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SPECIALSALARYAMOUNT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_NEXTINCREMENTDATE, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEOLDNEWCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSELOCATIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSECLASSCAT, loginDTO)%></th>
								<th><%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "House Sub Class")%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEID, loginDTO)%></th>
								<th><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%></th>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTDESIGNATIONJOININGDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDESIGNATIONEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDESIGNATIONBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDESIGNATIONEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDESIGNATIONBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_ISHIGHERAVAILABILITY, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HIGHERSTEPNUMBER, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEDIVISIONCAT, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTHOUSE, loginDTO)%></th>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTHOUSEDESCRIPTION, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASHOUSEINDHAKA, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEINDHAKACONSTRUCTIONDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASRELATIVEGOVTHOUSE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTHOUSERELATIVENAME, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIONSHIPCAT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVEDESIGNATION, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REALTIVEOFFICE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVESALARY, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSENEEDEDDESCRIPTION, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_STATUS, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DECISIONDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REJECTIONREASON, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEDDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERORGID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEREMPID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERPHONENUM, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICENAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICENAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_SEARCH_AM_HOUSE_ALLOCATION_REQUEST_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Am_house_allocation_requestDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Am_house_allocation_requestDTO am_house_allocation_requestDTO = (Am_house_allocation_requestDTO) data.get(i);
																																
											
											%>
											<tr>

											<td>
											<%
												if (Language.equals("Bangla")) {
													value = am_house_allocation_requestDTO.requesterNameBn + ", " + am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn + ", " + am_house_allocation_requestDTO.requesterOfficeUnitNameBn;
												}
												else {
													value = am_house_allocation_requestDTO.requesterNameEn + ", " + am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn + ", " + am_house_allocation_requestDTO.requesterOfficeUnitNameEn;
												}
											%>
				
											<%=value%>
				
			
											</td>
		

											<td>
											<%
											value = am_house_allocation_requestDTO.amHouseOldNewCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "am_house_old_new", am_house_allocation_requestDTO.amHouseOldNewCat);
											%>	
				
											<%=value%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_requestDTO.amHouseLocationCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "am_house_location", am_house_allocation_requestDTO.amHouseLocationCat);
											%>	
				
											<%=value%>
				
			
											</td>
		
											<td>
											<%
											value = am_house_allocation_requestDTO.amHouseClassCat + "";
											%>
											<%
											value = CatRepository.getInstance().getText(Language, "am_house_class", am_house_allocation_requestDTO.amHouseClassCat);
											%>	
				
											<%=value%>
				
			
											</td>

											<td>
												<%
													Am_house_type_sub_categoryDTO dto = Am_house_type_sub_categoryRepository.getInstance()
															.getAm_house_type_sub_categoryDTOByiD(am_house_allocation_requestDTO.amHouseClassSubCat);
													value = dto == null ? "" :
															Language.equalsIgnoreCase("English") ?
																	dto.amHouseSubCatNameEn : dto.amHouseSubCatNameBn;
												%>
												<%=value%>
											</td>
		
											<td>
											<%
											value = am_house_allocation_requestDTO.amHouseId + "";
											value = Am_houseRepository.getInstance().getAm_houseDTOByID(am_house_allocation_requestDTO.amHouseId).houseNumber;
											%>
				
											<%=value%>
				
			
											</td>

											<td>
											<span class="btn btn-sm border-0 shadow"
												  style="background-color: <%=AmHouseAllocationRequestStatus.getColor(am_house_allocation_requestDTO.status)%>; color: white; border-radius: 8px;cursor: text">
													<%=CatRepository.getInstance().getText(
															Language, "am_house_allocation_status", am_house_allocation_requestDTO.status
													)%>
											</span>
											</td>
		
											<td>
											<%
											value = am_house_allocation_requestDTO.currentHouseCat + "";
											value = CatRepository.getInstance().getText(Language, "am_current_house", am_house_allocation_requestDTO.currentHouseCat);
											%>
				
											<%=value%>
				
			
											</td>


		
	
											<%CommonDTO commonDTO = am_house_allocation_requestDTO; %>


	<td>
		<button
				type="button"
				class="btn-sm border-0 shadow bg-light btn-border-radius"
				style="color: #ff6b6b;"
				onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
		>
			<i class="fa fa-eye"></i>
		</button>
	</td>


	<td>
		<%
			if(commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT && am_house_allocation_requestDTO.status == CommonApprovalStatus.PENDING.getValue())
			{
		%>
		<button
				type="button"
				class="btn-sm border-0 shadow btn-border-radius text-white"
				style="background-color: #ff6b6b;"
				onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"
		>
			<i class="fa fa-edit"></i>
		</button>
		<%
			}
		%>
	</td>

											<td class="text-right">
												<%
													if(commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT && am_house_allocation_requestDTO.status == CommonApprovalStatus.PENDING.getValue())
													{
												%>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=am_house_allocation_requestDTO.iD%>'/></span>
												</div>
												<%
													}
												%>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			