<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="am_house_allocation_request.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Am_house_allocation_requestDTO am_house_allocation_requestDTO = new Am_house_allocation_requestDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = am_house_allocation_requestDTO;
    String tableName = "am_house_allocation_request";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AM_HOUSE_ALLOCATION_REQUEST_ADD_FORMNAME, loginDTO);
    String servletName = "Am_house_allocation_requestServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Am_house_allocation_requestServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.iD%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='requesterOrgId'
                                           id='requesterOrgId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.requesterOrgId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOfficeId'
                                           id='requesterOfficeId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.requesterOfficeId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOfficeUnitId'
                                           id='requesterOfficeUnitId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.requesterOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterEmpId'
                                           id='requesterEmpId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.requesterEmpId%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERPHONENUM, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterPhoneNum'
                                                   id='requesterPhoneNum_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterPhoneNum%>'
                                                   required="required" pattern="880[0-9]{10}"
                                                   title="requesterPhoneNum must start with 880, then contain 10 digits"
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERNAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterNameEn'
                                                   id='requesterNameEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterNameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERNAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterNameBn'
                                                   id='requesterNameBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterNameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICENAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterOfficeNameEn'
                                                   id='requesterOfficeNameEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterOfficeNameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICENAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterOfficeNameBn'
                                                   id='requesterOfficeNameBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterOfficeNameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterOfficeUnitNameEn'
                                                   id='requesterOfficeUnitNameEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterOfficeUnitNameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterOfficeUnitNameBn'
                                                   id='requesterOfficeUnitNameBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterOfficeUnitNameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterOfficeUnitOrgNameEn'
                                                   id='requesterOfficeUnitOrgNameEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='requesterOfficeUnitOrgNameBn'
                                                   id='requesterOfficeUnitOrgNameBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DATEOFBIRTH, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "dateOfBirth_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='dateOfBirth' id='dateOfBirth_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.dateOfBirth))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_MARITALSTATUSCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='maritalStatusCat'
                                                    id='maritalStatusCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("marital_status", Language, am_house_allocation_requestDTO.maritalStatusCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='employeePayScaleId'
                                           id='employeePayScaleId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.employeePayScaleId%>'
                                           tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALETYPEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='employeePayScaleTypeEn'
                                                   id='employeePayScaleTypeEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.employeePayScaleTypeEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALETYPEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='employeePayScaleTypeBn'
                                                   id='employeePayScaleTypeBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.employeePayScaleTypeBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_BASICSALARYEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='basicSalaryEn'
                                                   id='basicSalaryEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.basicSalaryEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_BASICSALARYBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='basicSalaryBn'
                                                   id='basicSalaryBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.basicSalaryBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASSPECIALSALARY, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='checkbox' class='form-control-sm' name='hasSpecialSalary'
                                                   id='hasSpecialSalary_checkbox_<%=i%>'
                                                   value='true'                                                                <%=(String.valueOf(am_house_allocation_requestDTO.hasSpecialSalary).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SPECIALSALARYAMOUNT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='specialSalaryAmount'
                                                   id='specialSalaryAmount_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.specialSalaryAmount%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_NEXTINCREMENTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "nextIncrementDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='nextIncrementDate'
                                                   id='nextIncrementDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.nextIncrementDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SALARYDESCRIPTIONUPDATEFILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "salaryDescriptionUpdateFilesDropzone";
                                                if (actionName.equals("ajax_edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone%>'/>


                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='amHouseOldNewCat'
                                                    id='amHouseOldNewCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_house_old_new", Language, am_house_allocation_requestDTO.amHouseOldNewCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='amHouseLocationCat'
                                                    id='amHouseLocationCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_house_location", Language, am_house_allocation_requestDTO.amHouseLocationCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSECLASSCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='amHouseClassCat'
                                                    id='amHouseClassCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_house_class", Language, am_house_allocation_requestDTO.amHouseClassCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='amHouseId'
                                           id='amHouseId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.amHouseId%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "govtFirstAppointmentDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='govtFirstAppointmentDate'
                                                   id='govtFirstAppointmentDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.govtFirstAppointmentDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "prpFirstJoiningDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='prpFirstJoiningDate'
                                                   id='prpFirstJoiningDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.prpFirstJoiningDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTDESIGNATIONJOININGDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "currentDesignationJoiningDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='currentDesignationJoiningDate'
                                                   id='currentDesignationJoiningDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.currentDesignationJoiningDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDESIGNATIONEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   name='govtFirstAppointmentDesignationEn'
                                                   id='govtFirstAppointmentDesignationEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.govtFirstAppointmentDesignationEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDESIGNATIONBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   name='govtFirstAppointmentDesignationBn'
                                                   id='govtFirstAppointmentDesignationBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.govtFirstAppointmentDesignationBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDESIGNATIONEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='prpFirstJoiningDesignationEn'
                                                   id='prpFirstJoiningDesignationEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.prpFirstJoiningDesignationEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDESIGNATIONBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='prpFirstJoiningDesignationBn'
                                                   id='prpFirstJoiningDesignationBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.prpFirstJoiningDesignationBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   name='requestedHomeAvailablitySalaryEn'
                                                   id='requestedHomeAvailablitySalaryEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requestedHomeAvailablitySalaryEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control'
                                                   name='requestedHomeAvailablitySalaryBn'
                                                   id='requestedHomeAvailablitySalaryBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.requestedHomeAvailablitySalaryBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "requestedHomeAvailablityDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='requestedHomeAvailablityDate'
                                                   id='requestedHomeAvailablityDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.requestedHomeAvailablityDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYFILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "requestedHomeAvailablitySalaryFilesDropzone";
                                                if (actionName.equals("ajax_edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone%>'/>


                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_ISHIGHERAVAILABILITY, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='checkbox' class='form-control-sm' name='isHigherAvailability'
                                                   id='isHigherAvailability_checkbox_<%=i%>'
                                                   value='true'                                                                <%=(String.valueOf(am_house_allocation_requestDTO.isHigherAvailability).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HIGHERSTEPNUMBER, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='higherStepNumber'
                                                   id='higherStepNumber_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.higherStepNumber%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEDIVISIONCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='houseDivisionCat'
                                                    id='houseDivisionCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("house_division", Language, am_house_allocation_requestDTO.houseDivisionCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTHOUSEDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='currentHouseDescription'
                                                   id='currentHouseDescription_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.currentHouseDescription%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASHOUSEINDHAKA, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='checkbox' class='form-control-sm' name='hasHouseInDhaka'
                                                   id='hasHouseInDhaka_checkbox_<%=i%>'
                                                   value='true'                                                                <%=(String.valueOf(am_house_allocation_requestDTO.hasHouseInDhaka).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEINDHAKACONSTRUCTIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "houseInDhakaConstructionDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='houseInDhakaConstructionDate'
                                                   id='houseInDhakaConstructionDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.houseInDhakaConstructionDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASRELATIVEGOVTHOUSE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='checkbox' class='form-control-sm' name='hasRelativeGovtHouse'
                                                   id='hasRelativeGovtHouse_checkbox_<%=i%>'
                                                   value='true'                                                                <%=(String.valueOf(am_house_allocation_requestDTO.hasRelativeGovtHouse).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTHOUSERELATIVENAME, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='govtHouseRelativeName'
                                                   id='govtHouseRelativeName_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.govtHouseRelativeName%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIONSHIPCAT, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <select class='form-control' name='relationshipCat'
                                                    id='relationshipCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_relationship", Language, am_house_allocation_requestDTO.relationshipCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVEDESIGNATION, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='relativeDesignation'
                                                   id='relativeDesignation_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.relativeDesignation%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REALTIVEOFFICE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='realtiveOffice'
                                                   id='realtiveOffice_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.realtiveOffice%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVESALARY, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='relativeSalary'
                                                   id='relativeSalary_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.relativeSalary%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSENEEDEDDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='houseNeededDescription'
                                                   id='houseNeededDescription_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.houseNeededDescription%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_STATUS, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                value = "";
                                                if (am_house_allocation_requestDTO.status != -1) {
                                                    value = am_house_allocation_requestDTO.status + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='status'
                                                   id='status_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DECISIONDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "decisionDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='decisionDate' id='decisionDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.decisionDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REJECTIONREASON, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='rejectionReason'
                                                   id='rejectionReason_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.rejectionReason%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEDDATE, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <%value = "approvedDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='approvedDate' id='approvedDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.approvedDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='approverOrgId'
                                           id='approverOrgId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.approverOrgId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='approverOfficeId'
                                           id='approverOfficeId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.approverOfficeId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='approverOfficeUnitId'
                                           id='approverOfficeUnitId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.approverOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='approverEmpId'
                                           id='approverEmpId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.approverEmpId%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERPHONENUM, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverPhoneNum'
                                                   id='approverPhoneNum_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverPhoneNum%>'
                                                   required="required" pattern="880[0-9]{10}"
                                                   title="approverPhoneNum must start with 880, then contain 10 digits"
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERNAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverNameEn'
                                                   id='approverNameEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverNameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERNAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverNameBn'
                                                   id='approverNameBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverNameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICENAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverOfficeNameEn'
                                                   id='approverOfficeNameEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverOfficeNameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICENAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverOfficeNameBn'
                                                   id='approverOfficeNameBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverOfficeNameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITNAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverOfficeUnitNameEn'
                                                   id='approverOfficeUnitNameEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverOfficeUnitNameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITNAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverOfficeUnitNameBn'
                                                   id='approverOfficeUnitNameBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverOfficeUnitNameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverOfficeUnitOrgNameEn'
                                                   id='approverOfficeUnitOrgNameEn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverOfficeUnitOrgNameEn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-8">
                                            <input type='text' class='form-control' name='approverOfficeUnitOrgNameBn'
                                                   id='approverOfficeUnitOrgNameBn_text_<%=i%>'
                                                   value='<%=am_house_allocation_requestDTO.approverOfficeUnitOrgNameBn%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.insertedByUserId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.lastModifierUser%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=am_house_allocation_requestDTO.lastModificationTime%>'
                                           tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AM_HOUSE_ALLOCATION_REQUEST_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AM_HOUSE_ALLOCATION_REQUEST_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);
        preprocessDateBeforeSubmitting('dateOfBirth', row);
        preprocessCheckBoxBeforeSubmitting('hasSpecialSalary', row);
        preprocessDateBeforeSubmitting('nextIncrementDate', row);
        preprocessDateBeforeSubmitting('govtFirstAppointmentDate', row);
        preprocessDateBeforeSubmitting('prpFirstJoiningDate', row);
        preprocessDateBeforeSubmitting('currentDesignationJoiningDate', row);
        preprocessDateBeforeSubmitting('requestedHomeAvailablityDate', row);
        preprocessCheckBoxBeforeSubmitting('isHigherAvailability', row);
        preprocessCheckBoxBeforeSubmitting('hasHouseInDhaka', row);
        preprocessDateBeforeSubmitting('houseInDhakaConstructionDate', row);
        preprocessCheckBoxBeforeSubmitting('hasRelativeGovtHouse', row);
        preprocessDateBeforeSubmitting('decisionDate', row);
        preprocessDateBeforeSubmitting('approvedDate', row);

        submitAddForm2();
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Am_house_allocation_requestServlet");
    }

    function init(row) {

        setDateByStringAndId('dateOfBirth_js_' + row, $('#dateOfBirth_date_' + row).val());
        setDateByStringAndId('nextIncrementDate_js_' + row, $('#nextIncrementDate_date_' + row).val());
        setDateByStringAndId('govtFirstAppointmentDate_js_' + row, $('#govtFirstAppointmentDate_date_' + row).val());
        setDateByStringAndId('prpFirstJoiningDate_js_' + row, $('#prpFirstJoiningDate_date_' + row).val());
        setDateByStringAndId('currentDesignationJoiningDate_js_' + row, $('#currentDesignationJoiningDate_date_' + row).val());
        setDateByStringAndId('requestedHomeAvailablityDate_js_' + row, $('#requestedHomeAvailablityDate_date_' + row).val());
        setDateByStringAndId('houseInDhakaConstructionDate_js_' + row, $('#houseInDhakaConstructionDate_date_' + row).val());
        setDateByStringAndId('decisionDate_js_' + row, $('#decisionDate_date_' + row).val());
        setDateByStringAndId('approvedDate_js_' + row, $('#approvedDate_date_' + row).val());


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






