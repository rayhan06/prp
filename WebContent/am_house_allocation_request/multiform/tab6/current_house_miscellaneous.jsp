<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="am_house_allocation_request.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>


<form class="form-horizontal"
      id="current_house_miscellaneous_form" name="current_house_miscellaneous_form" method="POST"
      enctype="multipart/form-data">


    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTHOUSE, loginDTO)%>
            <span class="required"> * </span>
        </label>
        <div class="col-md-8">
            <select class='form-control' name='currentHouseCat' id='currentHouseCat_text_<%=i%>' tag='pb_html' onchange="houseDescriptionStatusHelper()">
                <%
                    Options = CatRepository.getInstance().buildOptions("am_current_house", Language, am_house_allocation_requestDTO.currentHouseCat);
                %>
                <%=Options%>
            </select>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right">
            <%=UtilCharacter.getDataByLanguage(Language, "বরাদ্দকৃত বাসার বিবরণ", "Description of Allotted House")%>
            <span class="required" id="houseDescriptionMandatorySign"> * </span>
        </label>
        <div class="col-md-8">
        <textarea class='form-control' name='currentHouseDescription'
                  id='currentHouseDescription_text_<%=i%>'><%=am_house_allocation_requestDTO.currentHouseDescription%></textarea>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASHOUSEINDHAKA, loginDTO)%>
        </label>
        <div class="col-md-8">
            <span><%=UtilCharacter.getDataByLanguage(Language, "হ্যাঁ", "Yes")%></span>
            <input type='checkbox' class='form-control-sm' name='hasHouseInDhaka' id='hasHouseInDhaka_checkbox_<%=i%>'
                   value='true' <%=(String.valueOf(am_house_allocation_requestDTO.hasHouseInDhaka).equals("true"))?("checked"):""%>
                   onchange="toggleStatus('yes')" tag='pb_html'>
            <span><%=UtilCharacter.getDataByLanguage(Language, "না", "No")%></span>
            <input type='checkbox' class='form-control-sm' name='hasNotHouseInDhaka' id='hasNotHouseInDhaka_checkbox_<%=i%>'
                   value='true' onchange="toggleStatus('no')" tag='pb_html'>
        </div>
    </div>

    <div id="houseInDhakaDetails_id" style="display: none">

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEINDHAKALOCATION, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
            <textarea class='form-control' name='houseInDhakaLocationDetails'
                      id='houseInDhakaLocationDetails_text_<%=i%>'><%=am_house_allocation_requestDTO.houseInDhakaLocationDetails%></textarea>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEINDHAKACONSTRUCTIONDATE, loginDTO)%>
            </label>
            <div class="col-md-8">
                <%value = "houseInDhakaConstructionDate_js_" + i;%>
                <jsp:include page="/date/date.jsp">
                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                </jsp:include>
                <input type='hidden' name='houseInDhakaConstructionDate' id='houseInDhakaConstructionDate_date_<%=i%>'
                       value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.houseInDhakaConstructionDate))%>'
                       tag='pb_html'>
            </div>
        </div>

    </div>


    <div class="border-bottom border-dark">
    </div>

    <div class="form-group row">
        &nbsp;
    </div>

    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASRELATIVEGOVTHOUSE, loginDTO)%>
        </label>
        <div class="col-md-8">
            <input type='checkbox' class='form-control-sm' name='hasRelativeGovtHouse'
                   id='hasRelativeGovtHouse_checkbox_<%=i%>'
                   value='true'                                                                <%=(String.valueOf(am_house_allocation_requestDTO.hasRelativeGovtHouse).equals("true"))?("checked"):""%>
                   tag='pb_html'>
        </div>
    </div>

    <div id="relativeHouseInDhakaDetails_div_id" style="display: none">

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTHOUSERELATIVENAME, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control' name='govtHouseRelativeName'
                       id='govtHouseRelativeName_text_<%=i%>'
                       value='<%=am_house_allocation_requestDTO.govtHouseRelativeName%>' tag='pb_html'/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIONSHIPCAT, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
                <select class='form-control' name='relationshipCat' id='relationshipCat_category_<%=i%>' tag='pb_html'>
                    <%
                        Options = CatRepository.getInstance().buildOptions("am_relationship", Language, am_house_allocation_requestDTO.relationshipCat);
                    %>
                    <%=Options%>
                </select>

            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVEDESIGNATION, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control' name='relativeDesignation' id='relativeDesignation_text_<%=i%>'
                       value='<%=am_house_allocation_requestDTO.relativeDesignation%>' tag='pb_html'/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REALTIVEOFFICE, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control' name='realtiveOffice' id='realtiveOffice_text_<%=i%>'
                       value='<%=am_house_allocation_requestDTO.realtiveOffice%>' tag='pb_html'/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVESALARY, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control' name='relativeSalary' id='relativeSalary_text_<%=i%>'
                       value='<%=am_house_allocation_requestDTO.relativeSalary%>' tag='pb_html'/>
            </div>
        </div>

    </div>


    <div class="border-bottom border-dark">
    </div>

    <div class="form-group row">
        &nbsp;
    </div>


    <div class="form-group row">
        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSENEEDEDDESCRIPTION, loginDTO)%>
            <span class="required"> * </span>
        </label>
        <div class="col-md-8">
        <textarea class='form-control' name='houseNeededDescription'
                  id='houseNeededDescription_text_<%=i%>'><%=am_house_allocation_requestDTO.houseNeededDescription%></textarea>
        </div>
    </div>


</form>


<script type="text/javascript">
    let flatsNotMandatoryDescription = ['5', '6'];

    $(document).ready(function () {

        $("#hasHouseInDhaka_checkbox_0").change(function () {
            if (this.checked) {
                $("#houseInDhakaDetails_id").show();
            } else {
                $("#houseInDhakaDetails_id").hide();
            }
        });

        $("#hasRelativeGovtHouse_checkbox_0").change(function () {
            if (this.checked) {
                $("#relativeHouseInDhakaDetails_div_id").show();
            } else {
                $("#relativeHouseInDhakaDetails_div_id").hide();
            }
        });

        if ($('#hasHouseInDhaka_checkbox_0').is(':checked')) $("#houseInDhakaDetails_id").show();
        if ($('#hasRelativeGovtHouse_checkbox_0').is(':checked')) $("#relativeHouseInDhakaDetails_div_id").show();


        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });


        $.validator.addMethod('houseInDhakaLocationDetailsValidation', function (value, element) {
            return !$('#hasHouseInDhaka_checkbox_0').is(':checked')
                || ($("#houseInDhakaLocationDetails_text_0").val() != undefined
                    && $("#houseInDhakaLocationDetails_text_0").val().toString().trim().length > 0);
        });

        $.validator.addMethod('govtHouseRelativeNameValidation', function (value, element) {
            return !$('#hasRelativeGovtHouse_checkbox_0').is(':checked')
                || ($("#govtHouseRelativeName_text_0").val()
                    && $("#govtHouseRelativeName_text_0").val() != -1);
        });

        $.validator.addMethod('relationshipCatValidation', function (value, element) {
            return !$('#hasRelativeGovtHouse_checkbox_0').is(':checked')
                || (value && value != -1 && value != 0);
        });

        $.validator.addMethod('relativeDesignationValidation', function (value, element) {
            return !$('#hasRelativeGovtHouse_checkbox_0').is(':checked')
                || ($("#relativeDesignation_text_0").val() != undefined
                    && $("#relativeDesignation_text_0").val().toString().trim().length > 0);
        });

        $.validator.addMethod('realtiveOfficeValidation', function (value, element) {
            return !$('#hasRelativeGovtHouse_checkbox_0').is(':checked')
                || ($("#realtiveOffice_text_0").val() != undefined
                    && $("#realtiveOffice_text_0").val().toString().trim().length > 0);
        });

        $.validator.addMethod('relativeSalaryValidation', function (value, element) {
            return !$('#hasRelativeGovtHouse_checkbox_0').is(':checked')
                || ($("#relativeSalary_text_0").val() != undefined
                    && $("#relativeSalary_text_0").val().toString().trim().length > 0);
        });

        $.validator.addMethod('changeMandatoryHouseDescriptionStatus', function (){
            return houseDescriptionStatusHelper();
        })


        let lang = '<%=Language%>';
        let currHouseTypeErr;
        let currHouseDescriptionErr;
        let familyHouseInDhakaDescriptionErr;
        let govtHouseInDhakaRelativeNameErr;
        let govtHouseInDhakaRelationshipErr;
        let govtHouseInDhakaDesignationErr;
        let govtHouseInDhakaOfficeErr;
        let govtHouseInDhakaSalaryErr;
        let houseNeededDescriptionErr;


        if (lang == 'english') {
            currHouseTypeErr = 'Please provide current house type';
            currHouseDescriptionErr = 'Please provide current house description';
            familyHouseInDhakaDescriptionErr = 'Please provide house description';
            govtHouseInDhakaRelativeNameErr = 'Please provide name';
            govtHouseInDhakaRelationshipErr = 'Please provide relationship';
            govtHouseInDhakaDesignationErr = 'Please provide designation';
            govtHouseInDhakaOfficeErr = 'Please provide office';
            govtHouseInDhakaSalaryErr = 'Please provide salary';
            houseNeededDescriptionErr = 'Please provide description';

        } else {
            currHouseTypeErr = 'বর্তমান বাসস্থানের ধরণ প্রদান করুন';
            currHouseDescriptionErr = 'বর্তমান বাসস্থান বিবরণ প্রদান করুন';
            familyHouseInDhakaDescriptionErr = 'বিস্তারিত ঠিকানা প্রদান করুন';
            govtHouseInDhakaRelativeNameErr = 'সদস্যের নাম প্রদান করুন';
            govtHouseInDhakaRelationshipErr = 'সম্পর্ক প্রদান করুন';
            govtHouseInDhakaDesignationErr = 'সদস্যের পদবি প্রদান করুন';
            govtHouseInDhakaOfficeErr = 'কর্মস্থলের নাম প্রদান করুন';
            govtHouseInDhakaSalaryErr = 'মাসিক বেতন প্রদান করুন';
            houseNeededDescriptionErr = 'বিবরণ প্রদান করুন';

        }

        $("#current_house_miscellaneous_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                currentHouseCat: {
                    validSelector: true,
                },
                currentHouseDescription: {
                    changeMandatoryHouseDescriptionStatus: true,
                },
                houseInDhakaLocationDetails: {
                    houseInDhakaLocationDetailsValidation: true,
                },
                govtHouseRelativeName: {
                    govtHouseRelativeNameValidation: true,
                },
                relationshipCat: {
                    relationshipCatValidation: true,
                },
                relativeDesignation: {
                    relativeDesignationValidation: true,
                },
                realtiveOffice: {
                    realtiveOfficeValidation: true,
                },
                relativeSalary: {
                    relativeSalaryValidation: true,
                },
                houseNeededDescription: {
                    required: true,
                },

            },
            messages: {
                currentHouseCat: currHouseTypeErr,
                currentHouseDescription: currHouseDescriptionErr,
                houseInDhakaLocationDetails: familyHouseInDhakaDescriptionErr,
                govtHouseRelativeName: govtHouseInDhakaRelativeNameErr,
                relationshipCat: govtHouseInDhakaRelationshipErr,
                relativeDesignation: govtHouseInDhakaDesignationErr,
                realtiveOffice: govtHouseInDhakaOfficeErr,
                relativeSalary: govtHouseInDhakaSalaryErr,
                houseNeededDescription: houseNeededDescriptionErr,

            }
        });


        let shouldValidate = document.getElementById('currentHouseCat_text_0').value;
        if(shouldValidate){
            houseDescriptionStatusHelper();
        }

    });

    function toggleStatus(checkBoxName){
        if(checkBoxName === 'yes') {
            document.getElementById('hasNotHouseInDhaka_checkbox_0').checked = false;
            document.getElementById('houseInDhakaDetails_id').style.display = 'block';
        }
        else if(checkBoxName === 'no') {
            document.getElementById('hasHouseInDhaka_checkbox_0').checked = false;
            document.getElementById('houseInDhakaDetails_id').style.display = 'none';
        }
    }

    function houseDescriptionStatusHelper(){
        let houseDescriptionNode = document.getElementById('currentHouseCat_text_0');
        if(flatsNotMandatoryDescription.includes(houseDescriptionNode.value)){
            document.getElementById('currentHouseDescription_text_0').required = false;
            // document.getElementsByName('currentHouseDescription').required = false;
            document.getElementById('houseDescriptionMandatorySign').style.display = 'none';
            return true;
        } else {
            document.getElementById('currentHouseDescription_text_0').required = true;
            // document.getElementsByName('currentHouseDescription').required = true;
            document.getElementById('houseDescriptionMandatorySign').style.display = 'inline';
            return true;
        }
    }

</script>













