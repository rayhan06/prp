<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDTO" %>
<%@ page import="util.CommonDTO" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDAO" %>


<%@include file="../../pb/addInitializer2.jsp"%>


<%
    Am_house_allocation_requestDTO am_house_allocation_requestDTO = new Am_house_allocation_requestDTO();
    long ID = -1;
    long requesterEmpId = -1;
    String requesterOrgName = "";
    String requesterUnitName = "";
    if(request.getParameter("ID") != null)
    {
        ID = Long.parseLong(request.getParameter("ID"));
        am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(ID);
        requesterEmpId = am_house_allocation_requestDTO.requesterEmpId;
        if (Language.equals("Bangla")) {
            requesterOrgName = am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn;
            requesterUnitName = am_house_allocation_requestDTO.requesterOfficeUnitNameBn;
        }
        else {
            requesterOrgName = am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn;
            requesterUnitName = am_house_allocation_requestDTO.requesterOfficeUnitNameEn;
        }
    }

//    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    EmployeeSearchModel own = new EmployeeSearchModel();
    own = new Gson().fromJson(EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID), EmployeeSearchModel.class);
    if(request.getParameter("ID") == null)
    {
        requesterEmpId = own.employeeRecordId;
        if (Language.equals("Bangla")) {
            requesterOrgName = own.organogramNameBn;
            requesterUnitName = own.officeUnitNameBn;
        }
        else {
            requesterOrgName = own.organogramNameEn;
            requesterUnitName = own.officeUnitNameEn;
        }
    }


    System.out.println("ID = " + ID);
    CommonDTO commonDTO = am_house_allocation_requestDTO;
    String tableName = "am_house_allocation_request";

    LoginDTO loginDTOFullBody = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String formTitle = LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AM_HOUSE_ALLOCATION_REQUEST_ADD_FORMNAME, loginDTOFullBody);
    String servletName = "Am_house_allocation_requestServlet";

    List<Employee_family_infoDTO> employee_family_infoDTOS = Employee_family_infoDAO.getInstance().getByEmployeeId(requesterEmpId);
    List<Am_reliable_family_memberDTO> am_reliable_family_memberDTOS = Am_reliable_family_memberDAO.getInstance().getByEmployeeRecordId(requesterEmpId);

    HashMap<Long, Am_reliable_family_memberDTO> familyInfoIdToReliableMap = new HashMap<>();

    am_reliable_family_memberDTOS
            .forEach(am_reliable_family_memberDTO -> {
                familyInfoIdToReliableMap.put(am_reliable_family_memberDTO.employeeFamilyInfoId, am_reliable_family_memberDTO);
            });

%>

<style>
    #aspect-content {
        margin: 50px 0 0;
        font-family: Roboto, SolaimanLipi;
    }

    #aspect-content * {
        box-sizing: border-box;
    }

    .aspect-tab {
        position: relative;
        width: 100%;
        margin: 0 auto 10px;
        border-radius: 8px;
        background-color: #fff;
        box-shadow: 0 0 0 1px #ececec;
        opacity: 1;
        transition: box-shadow .2s, opacity .4s;
    }

    .aspect-tab:hover {
        box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.11);
    }

    .aspect-input {
        display: none;
    }

    .aspect-input:checked ~ .aspect-content + .aspect-tab-content {
        max-height: 5000px;
    }

    .aspect-input:checked ~ .aspect-content:after {
        transform: rotate(0);
    }

    .aspect-label {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        max-height: 80px;
        width: 100%;
        margin: 0;
        padding: 0;
        font-size: 0;
        z-index: 1;
        cursor: pointer;
    }

    .aspect-label:hover ~ .aspect-content:after {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTI0IDI0SDBWMGgyNHoiIG9wYWNpdHk9Ii44NyIvPgogICAgICAgIDxwYXRoIGZpbGw9IiM1NTZBRUEiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE1Ljg4IDE1LjI5TDEyIDExLjQxbC0zLjg4IDMuODhhLjk5Ni45OTYgMCAxIDEtMS40MS0xLjQxbDQuNTktNC41OWEuOTk2Ljk5NiAwIDAgMSAxLjQxIDBsNC41OSA0LjU5Yy4zOS4zOS4zOSAxLjAyIDAgMS40MS0uMzkuMzgtMS4wMy4zOS0xLjQyIDB6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=");
    }

    .aspect-content {
        position: relative;
        display: block;
        height: 80px;
        margin: 0;
        padding: 0 87px 0 30px;
        font-size: 0;
        white-space: nowrap;
        cursor: pointer;
        user-select: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
    }

    .aspect-content:before, .aspect-content:after {
        content: '';
        display: inline-block;
        vertical-align: middle;
    }

    .aspect-content:before {
        height: 100%;
    }

    .aspect-content:after {
        position: absolute;
        width: 24px;
        height: 100%;
        right: 30px;
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTI0IDI0SDBWMGgyNHoiIG9wYWNpdHk9Ii44NyIvPgogICAgICAgIDxwYXRoIGZpbGw9IiNBOUFDQUYiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTE1Ljg4IDE1LjI5TDEyIDExLjQxbC0zLjg4IDMuODhhLjk5Ni45OTYgMCAxIDEtMS40MS0xLjQxbDQuNTktNC41OWEuOTk2Ljk5NiAwIDAgMSAxLjQxIDBsNC41OSA0LjU5Yy4zOS4zOS4zOSAxLjAyIDAgMS40MS0uMzkuMzgtMS4wMy4zOS0xLjQyIDB6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo=");
        background-repeat: no-repeat;
        background-position: center;
        transform: rotate(180deg);
    }

    .aspect-name {
        display: inline-block;
        width: 75%;
        margin-left: 10px;
        font-weight: 400;
        white-space: normal;
        text-align: left;
        vertical-align: middle;
    }

    .aspect-stat {
        width: 40%;
        text-align: right;
    }

    .all-opinions,
    .aspect-name {
        font-size: 1.5rem;
        line-height: 22px;
        font-family: Roboto, SolaimanLipi;
    }

    .all-opinions {
        color: #5d5d5d;
        text-align: left;
    }

    .aspect-content + .aspect-tab-content {
        max-height: 0;
        overflow: hidden;
        transition: max-height 1s ease-in-out;
    }

    .aspect-content > div,
    .aspect-stat > div {
        display: inline-block;
    }

    .aspect-content > div {
        vertical-align: middle;
    }

    .positive-count,
    .negative-count,
    .neutral-count {
        display: inline-block;
        margin: 0 0 0 20px;
        padding-left: 26px;
        background-repeat: no-repeat;
        font-size: 13px;
        line-height: 20px;
        color: #363636;
    }

    .positive-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiM3RUQzMjEiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNSAxMS40MjdhNSA1IDAgMCAwIDEwIDAgLjcxNC43MTQgMCAxIDAtMS40MjkgMCAzLjU3MSAzLjU3MSAwIDAgMS03LjE0MiAwIC43MTQuNzE0IDAgMSAwLTEuNDI5IDB6bTEuMDcxLTVhMS4wNzEgMS4wNzEgMCAxIDAgMCAyLjE0MyAxLjA3MSAxLjA3MSAwIDAgMCAwLTIuMTQzem03Ljg1OCAwYTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDMgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3oiLz4KPC9zdmc+Cg==");
    }

    .negative-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiNGRjZFMDAiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNSAxNC45OThhLjcxNC43MTQgMCAwIDAgMS40MjkgMCAzLjU3MSAzLjU3MSAwIDAgMSA3LjE0MiAwIC43MTQuNzE0IDAgMSAwIDEuNDI5IDAgNSA1IDAgMSAwLTEwIDB6bTEuMDcxLTguNTdhMS4wNzEgMS4wNzEgMCAxIDAgMCAyLjE0MiAxLjA3MSAxLjA3MSAwIDAgMCAwLTIuMTQzem03Ljg1OCAwYTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDIgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3oiLz4KPC9zdmc+Cg==");
    }

    .neutral-count {
        background-image: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KICAgIDxwYXRoIGZpbGw9IiNCQUMyRDYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTEwIDE4LjU3MWMtNC43MjYgMC04LjU3MS0zLjg0NS04LjU3MS04LjU3MSAwLTQuNzI2IDMuODQ1LTguNTcxIDguNTcxLTguNTcxIDQuNzI2IDAgOC41NzEgMy44NDUgOC41NzEgOC41NzEgMCA0LjcyNi0zLjg0NSA4LjU3MS04LjU3MSA4LjU3MXpNMjAgMTBjMCA1LjUxNC00LjQ4NiAxMC0xMCAxMFMwIDE1LjUxNCAwIDEwIDQuNDg2IDAgMTAgMHMxMCA0LjQ4NiAxMCAxMHpNNS43MTQgMTEuNDI3YS43MTQuNzE0IDAgMSAwIDAgMS40MjloOC41NzJhLjcxNC43MTQgMCAxIDAgMC0xLjQyOUg1LjcxNHptLjM1Ny01YTEuMDcxIDEuMDcxIDAgMSAwIDAgMi4xNDMgMS4wNzEgMS4wNzEgMCAwIDAgMC0yLjE0M3ptNy44NTggMGExLjA3MSAxLjA3MSAwIDEgMCAwIDIuMTQzIDEuMDcxIDEuMDcxIDAgMCAwIDAtMi4xNDN6Ii8+Cjwvc3ZnPgo=");
    }

    .aspect-info {
        width: 60%;
        white-space: nowrap;
        font-size: 0;
    }

    .aspect-info:before {
        content: '';
        display: inline-block;
        height: 44px;
        vertical-align: middle;
    }

    .chart-pie {
        position: relative;
        display: inline-block;
        height: 44px;
        width: 44px;
        border-radius: 50%;
        background-color: #e4e4e4;
        vertical-align: middle;
    }

    .chart-pie:after {
        content: '';
        display: block;
        position: absolute;
        height: 40px;
        width: 40px;
        top: 2px;
        left: 2px;
        border-radius: 50%;
        background-color: #fff;
    }

    .chart-pie-count {
        position: absolute;
        display: block;
        height: 100%;
        width: 100%;
        font-size: 14px;
        font-weight: 500;
        line-height: 44px;
        color: #242a32;
        text-align: center;
        z-index: 1;
    }

    .chart-pie > div {
        clip: rect(0, 44px, 44px, 22px);
    }

    .chart-pie > div,
    .chart-pie.over50 .first-fill {
        position: absolute;
        height: 44px;
        width: 44px;
        border-radius: 50%;
    }

    .chart-pie.over50 > div {
        clip: rect(auto, auto, auto, auto);
    }

    .chart-pie.over50 .first-fill {
        clip: rect(0, 44px, 44px, 22px);
    }

    .chart-pie:not(.over50) .first-fill {
        display: none;
    }

    .second-fill {
        position: absolute;
        clip: rect(0, 22px, 44px, 0);
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border-width: 3px;
        border-style: solid;
        box-sizing: border-box;
    }

    .chart-pie.positive .first-fill {
        background-color: #82d428;
    }

    .chart-pie.positive .second-fill {
        border-color: #82d428;
    }

    .chart-pie.negative .first-fill {
        background-color: #ff6e00;
    }

    .chart-pie.negative .second-fill {
        border-color: #ff6e00;
    }

    .aspect-tab-content {
        background-color: #f9f9f9;
        /*font-size: 0;*/
        text-align: justify;
    }

    .sentiment-wrapper {
        /*padding: 24px 30px 30px;*/
    }

    .sentiment-wrapper > div {
        display: inline-block;
        width: 100%;
    }

    .sentiment-wrapper > div > div {
        width: 100%;
        padding: 0px 16px;
    }

    .opinion-header {
        position: relative;
        width: 100%;
        margin: 0 0 24px;
        font-size: 13px;
        font-weight: 500;
        line-height: 20px;
        color: #242a32;
        text-transform: capitalize;
    }

    .opinion-header > span:nth-child(2) {
        position: absolute;
        right: 0;
    }

    .opinion-header + div > span {
        font-size: 13px;
        font-weight: 400;
        line-height: 22px;
        color: #363636;
    }

    @media screen and (max-width: 800px) {
        .aspect-label {
            max-height: 102px;
        }

        .aspect-content {
            height: auto;
            padding: 10px 87px 10px 30px;
        }

        .aspect-content:before {
            display: none;
        }

        .aspect-content:after {
            top: 0;
        }

        .aspect-content > div {
            display: block;
            width: 100%;
        }

        .aspect-stat {
            margin-top: 10px;
            text-align: left;
        }
    }

    @media screen and (max-width: 750px) {
        .sentiment-wrapper > div {
            display: block;
            width: 100%;
            max-width: 100%;
        }

        .sentiment-wrapper > div:not(:first-child) {
            margin-top: 10px;
        }
    }

    @media screen and (max-width: 500px) {
        .aspect-label {
            max-height: 140px;
        }

        .aspect-stat > div {
            display: block;
            width: 100%;
        }

        .all-opinions {
            margin-bottom: 10px;
        }

        .all-opinions + div > span:first-child {
            margin: 0;
        }
    }

    .form-control-plaintext {
        font-size: 1.2rem !important;
        font-family: Roboto, SolaimanLipi !important;
    }

    .required {
        color: red;
    }

    .hiddenInput{
        display: none;
    }

</style>


<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title" style="color: #4a95dc;">
                    <i class="fa fa-user-cog"></i>
                    <%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AM_HOUSE_ALLOCATION_REQUEST_ADD_FORMNAME, loginDTO) %>
                </h3>
            </div>
        </div>
        <div class="form-horizontal">
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-lg-12 px-0" style="margin-bottom: 15px;">
                    <jsp:include page="/am_house_allocation_request/multiform/tabs.jsp"></jsp:include>
                </div>
                <br/><br/>
                <div class="onlyborder">
                    <div class="row mx-2 mx-lg-4">
                        <div class="col-12">

                            <%@include file="common_field.jsp" %>

                            <div id="personal_info_tab_body_id">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTO) %>
                                        </h4>
                                    </div>
                                </div>

                                <%--new accordian start--%>
                                <div class="" id="personal_info_view">
                                    <%@include file="tab1/personal_info.jsp" %>
                                </div>
                                <%--new accordian end--%>
                            </div>


                            <div id="salary_description_tab_body_id" style="display: none">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SALARY_DESCRIPTION, loginDTO) %>
                                        </h4>
                                    </div>
                                </div>

                                <%--new accordian start--%>
                                <div class="" id="salary_description_view">
                                    <%@include file="tab2/salary_description.jsp" %>
                                </div>
                                <%--new accordian end--%>
                            </div>

                            <div id="choose_house_tab_body_id" style="display: none">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CHOOSE_HOUSE, loginDTO) %>
                                        </h4>
                                    </div>
                                </div>

                                <%--new accordian start--%>
                                <div class="" id="choose_house_view">
                                    <%@include file="tab3/choose_house.jsp" %>
                                </div>
                                <%--new accordian end--%>
                            </div>

                            <div id="reliable_family_member_tab_body_id" style="display: none">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELIABLE_FAMILY_MEMBERS, loginDTO) %>
                                        </h4>
                                    </div>
                                </div>

                                <%--new accordian start--%>
                                <div class="" id="reliable_family_view">
                                    <%@include file="tab4/reliable_family_member.jsp" %>
                                </div>
                                <%--new accordian end--%>
                            </div>

                            <div id="job_description_tab_body_id" style="display: none">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_JOB_DESCRIPTION, loginDTO) %>
                                        </h4>
                                    </div>
                                </div>

                                <%--new accordian start--%>
                                <div class="" id="job_description_view">
                                    <%@include file="tab5/job_description.jsp" %>
                                </div>
                                <%--new accordian end--%>
                            </div>

                            <div id="current_house_miscellaneous_tab_body_id" style="display: none">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENT_HOUSE_MISCELLANEOUS, loginDTO) %>
                                        </h4>
                                    </div>
                                </div>

                                <%--new accordian start--%>
                                <div class="" id="current_house_miscellaneous_view">
                                    <%@include file="tab6/current_house_miscellaneous.jsp" %>
                                </div>
                                <%--new accordian end--%>
                            </div>


                            <div class="row">
                                <div class="col-md-10 mt-3">
                                    <div class="form-actions text-right">

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <div class="form-actions text-right">
                        <button class="btn btn-sm border-0 btn-warning text-white shadow btn-border-radius" id="previousButton" onclick="navigateTab(-1)">
                            <%=UtilCharacter.getDataByLanguage(Language, "পূর্ববর্তী", "Previous")%>
                        </button>
                        <button class="btn btn-sm  mx-2 border-0 btn-primary shadow btn-border-radius" id="nextButton" onclick="navigateTab(1)">
                            <%=LM.getText(LC.GATE_PASS_ADD_NEXT_BUTTON, loginDTO)%>
                        </button>
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.BACK_BACK, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" id="form_buttons" onclick="onSubmitButtonClick(0,'<%=actionName%>')" style="display: none">
                            <%
                                if (am_house_allocation_requestDTO.iD == -1) {
                            %>
                            <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AM_HOUSE_ALLOCATION_REQUEST_SUBMIT_BUTTON, loginDTO)%>
                            <%
                                }
                                else {
                            %>
                            <%=LM.getText(LC.VM_REQUISITION_REPORT_OTHER_SAVE, loginDTO)%>
                            <%
                                }
                            %>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


<jsp:include page="../../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

    var currentTabName = 'personal_info';
    var currentTabIndex = 0;
    var tabNames = ['personal_info','salary_description','choose_house','reliable_family_member','job_description','current_house_miscellaneous'];
    let tabToIndexMap = {};

    tabToIndexMap['personal_info'] = 0;
    tabToIndexMap['salary_description'] = 1;
    tabToIndexMap['choose_house'] = 2;
    tabToIndexMap['reliable_family_member'] = 3;
    tabToIndexMap['job_description'] = 4;
    tabToIndexMap['current_house_miscellaneous'] = 5;



    function allReliable() {

        var checkboxes = document.getElementsByName('isReliable');

        for(var i=0;i<checkboxes.length;i++) {
            checkboxes[i].checked = $('#isAllReliable').is(':checked');
        }

    }



    function loadRequesterData(data) {

        $('#dateOfBirth_date_0').val(data.dateOfBirthString);
        $('#maritalStatusCat_category_0').val(data.maritalStatusCat);
        $('#govtFirstAppointmentDate_date_0').val(data.govtFirstAppointmentDateString);
        $('#prpFirstJoiningDate_date_0').val(data.prpFirstJoiningDateString);
        $('#currentDesignationJoiningDate_date_0').val(data.currentDesignationJoiningDateString);
        $('#prpFirstJoiningDesignationEn_text_0').val(data.prpFirstJoiningDesignationEn);
        $('#prpFirstJoiningDesignationBn_text_0').val(data.prpFirstJoiningDesignationBn);

        $('#employeePayScaleId_hidden_0').val(data.employeePayScaleId);

        $('#label_dateOfBirth_date_0').text(data.dateOfBirthString);
        $('#label_maritalStatusCat_category_0').text(data.maritalStatusCatString);
        $('#label_govtFirstAppointmentDate_date_0').text(data.govtFirstAppointmentDateString);
        $('#label_prpFirstJoiningDate_date_0').text(data.prpFirstJoiningDateString);
        $('#label_currentDesignationJoiningDate_date_0').text(data.currentDesignationJoiningDateString);
        $('#label_prpFirstJoiningDesignationEn_text_0').text(data.prpFirstJoiningDesignationEn);
        $('#label_prpFirstJoiningDesignationBn_text_0').text(data.prpFirstJoiningDesignationBn);

        $('#label_requester_org_name_0').text(data.requesterOfficeUnitOrgNameEn);
        $('#label_requester_unit_name_0').text(data.requesterOfficeUnitNameEn);

        $('#label_employeePayScaleTypeEn_text_0').text(data.employeePayScaleTypeEn);
        $('#higherEligibilityMsg_text_0').text(data.higherEligibilityMsg);

        $('#basicSalaryEn_text_0').val(data.basicSalaryEn);

        document.getElementById('family_info_tbody_id').innerHTML = data.remarks;

        $('#previousButton').hide();
    }


    function ajaxGet(url, onSuccess, onError) {
        $.ajax({
            type: "GET",
            url: getContextPath()+url,
            dataType: "json",
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }

    function navigateTab(addIndex) {
        event.preventDefault();

        let currentFormValid = true;
        if (currentTabIndex != 3 && addIndex >= 0) {
            let currentForm = $("#" + tabNames[currentTabIndex] + "_form");
            currentForm.validate();
            currentFormValid = currentForm.valid();
        }

        if (currentFormValid || currentTabIndex == 3) {
            currentTabIndex = currentTabIndex + addIndex;
            currentTabIndex = Math.max(0, currentTabIndex);
            currentTabIndex = Math.min(5, currentTabIndex);
            showHideTab(tabNames[currentTabIndex]);
        }

    }

    function clickOnTab(tabName) {
        let currentFormValid = true;
        if (currentTabIndex != 3) {
            let currentForm = $("#" + tabNames[currentTabIndex] + "_form");
            currentForm.validate();
            currentFormValid = currentForm.valid();
        }

        var requestedTabIndex = tabToIndexMap[tabName];
        var previousTabIndex = requestedTabIndex - 1;
        if (previousTabIndex >= 0 && previousTabIndex != 3) {
            let currentForm = $("#" + tabNames[previousTabIndex] + "_form");
            currentForm.validate();
            currentFormValid = currentFormValid && currentForm.valid();
        }

        if (currentFormValid || currentTabIndex == 3 || currentTabIndex > requestedTabIndex) {
            currentTabIndex = tabToIndexMap[tabName];
            showHideTab(tabName);
        }
    }

    function showHideTab(tabName) {
        document.getElementById(currentTabName + '_tab_body_id').style.display = 'none';
        $('#btn_' + currentTabName).removeClass('active').css('opacity', '0.5');
        $('#arrow-down-' + currentTabName).hide();

        document.getElementById(tabName + '_tab_body_id').style.display = 'block';
        $('#btn_' + tabName).addClass('active').css('opacity', '1');
        $('#arrow-down-' + tabName).show();

        if (tabName == 'current_house_miscellaneous') {
            $('#form_buttons').show();
            $('#previousButton').show();
            $('#nextButton').hide();
        }
        else if (tabName == 'personal_info') {
            $('#form_buttons').hide();
            $('#previousButton').hide();
            $('#nextButton').show();
        }
        else {
            $('#form_buttons').hide();
            $('#previousButton').show();
            $('#nextButton').show();
        }

        currentTabName = tabName;
    }


    function preprocessAllCheckBoxBeforeSubmitting(fieldname) {

        var checkboxes = document.getElementsByName(fieldname);

        for(var i=0;i<checkboxes.length;i++) {
            document.getElementById(fieldname + 'Values_hidden_' + i).value = checkboxes[i].checked == true? 1:0;
        }
    }


    function onSubmitButtonClick(row, action)
    {
        event.preventDefault();
        console.log("action = " + action);
        // preprocessDateBeforeSubmitting('dateOfBirth', row);
        preprocessCheckBoxBeforeSubmitting('hasSpecialSalary', row);
        // preprocessDateBeforeSubmitting('nextIncrementDate', row);
        // preprocessDateBeforeSubmitting('govtFirstAppointmentDate', row);
        // preprocessDateBeforeSubmitting('prpFirstJoiningDate', row);
        // preprocessDateBeforeSubmitting('currentDesignationJoiningDate', row);
        preprocessDateBeforeSubmitting('requestedHomeAvailablityDate', row);
        preprocessCheckBoxBeforeSubmitting('isHigherAvailability', row);
        preprocessCheckBoxBeforeSubmitting('hasHouseInDhaka', row);
        preprocessDateBeforeSubmitting('houseInDhakaConstructionDate', row);
        preprocessCheckBoxBeforeSubmitting('hasRelativeGovtHouse', row);
        preprocessAllCheckBoxBeforeSubmitting('isReliable');
        preprocessDateBeforeSubmitting('decisionDate', row);
        preprocessDateBeforeSubmitting('approvedDate', row);


        let data = added_requester_map.keys().next();
        if (!(data && data.value && data.value != undefined)) {
            toastr.error("Please Select Person");
            return false;
        }
        document.getElementById('requesterEmpId_hidden_0').value = JSON.stringify(
            Array.from(added_requester_map.values()));


        var errTab = '';

        let form = $("#current_house_miscellaneous_form");
        form.validate();
        let valid1 = form.valid();

        if (!valid1) errTab = ('current_house_miscellaneous');

         form = $("#job_description_form");
        form.validate();
        let valid2 = form.valid();

        if (!valid2) errTab = ('job_description');

         form = $("#choose_house_form");
        form.validate();
        let valid3 = form.valid();

        if (!valid3) errTab = ('choose_house');

         form = $("#salary_description_form");
        form.validate();
        let valid4 = form.valid();

        if (!valid4) errTab = ('salary_description');


        let valid = valid1 && valid2 && valid3 && valid4;
        if (valid) {
            submitHouseAllocationRequestAddForm2();
        }
        else {
            showHideTab(errTab);
        }
        return false;
    }


    function submitHouseAllocationRequestAddForm2()
    {
        console.log("submitting");

        var dataString = $("#common_field_form ,#personal_info_form, #salary_description_form, #choose_house_form, #family_info_form, #job_description_form, #current_house_miscellaneous_form").serialize();

        $.ajax({
            type : "POST",
            url : "Am_house_allocation_requestServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>",
            data : dataString,
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    console.log("Failed");
                    toastr.error(response.msg);
                }else if(response.responseCode === 200){
                    console.log("Successfully added");
                    window.location.replace(getContextPath()+response.msg);
                }
                else
                {
                    console.log("Error: " + response.responseCode);
                }
            }
            ,
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }


    function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
    {
        addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Am_house_allocation_requestServlet");
    }

    function convertToSelect2(){
        // TAB 3 - CHOOSE HOUSE
        select2SingleSelector("#amHouseOldNewCat_category_<%=i%>", '<%=Language%>');
        select2SingleSelector("#amHouseLocationCat_category_<%=i%>", '<%=Language%>');
        select2SingleSelector("#amHouseClassCat_category_<%=i%>", '<%=Language%>');
        select2SingleSelector("#amHouseClassSubCat_<%=i%>", '<%=Language%>');
        select2SingleSelector("#amHouseId_select_<%=i%>", '<%=Language%>');
        // TAB 6 - CURRENT HOUSE
        select2SingleSelector("#currentHouseCat_text_<%=i%>", '<%=Language%>');
    }

    function init(row)
    {

        // setDateByStringAndId('dateOfBirth_js_' + row, $('#dateOfBirth_date_' + row).val());
        // setDateByStringAndId('nextIncrementDate_js_' + row, $('#nextIncrementDate_date_' + row).val());
        // setDateByStringAndId('govtFirstAppointmentDate_js_' + row, $('#govtFirstAppointmentDate_date_' + row).val());
        // setDateByStringAndId('prpFirstJoiningDate_js_' + row, $('#prpFirstJoiningDate_date_' + row).val());
        // setDateByStringAndId('currentDesignationJoiningDate_js_' + row, $('#currentDesignationJoiningDate_date_' + row).val());
        setDateByStringAndId('requestedHomeAvailablityDate_js_' + row, $('#requestedHomeAvailablityDate_date_' + row).val());
        setDateByStringAndId('houseInDhakaConstructionDate_js_' + row, $('#houseInDhakaConstructionDate_date_' + row).val());
        // setDateByStringAndId('decisionDate_js_' + row, $('#decisionDate_date_' + row).val());
        // setDateByStringAndId('approvedDate_js_' + row, $('#approvedDate_date_' + row).val());

        $('#btn_salary_description').css('opacity', '0.5');
        $('#btn_choose_house').css('opacity', '0.5');
        $('#btn_reliable_family_member').css('opacity', '0.5');
        $('#btn_job_description').css('opacity', '0.5');
        $('#btn_current_house_miscellaneous').css('opacity', '0.5');


        var getRequesterUrl = 'Am_house_allocation_requestServlet?' +
            'actionType=getRequesterDetails' +
            '&id='+'<%=requesterEmpId%>';
        ajaxGet(getRequesterUrl, loadRequesterData, loadRequesterData);

        convertToSelect2();
    }

    var row = 0;
    $(document).ready(function(){
        init(row);
        // CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

    });

    var child_table_extra_id = <%=childTableStartingID%>;



</script>
