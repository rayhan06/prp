<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="am_house_allocation_request.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>


<%
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.YEAR, i);
    Date today = c.getTime();
    SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
    String todayString = sf.format(today);
    int currentYear = Integer.parseInt(todayString.substring(6));
    int nextYear = currentYear+1;
    String incrementYear = "";
    int month = Integer.parseInt(todayString.substring(3,5));
    incrementYear = month > 6 ? String.valueOf(nextYear) : String.valueOf(currentYear);
    String incrementDate = "01/07/" + incrementYear;
    long incrementDateLong = dateFormat.parse(incrementDate).getTime();
%>


<form class="form-horizontal"
      id="salary_description_form" name="salary_description_form" method="POST" enctype="multipart/form-data">

<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALETYPEEN, loginDTO)%>
    </label>
    <div class="col-md-9">
        <input type='hidden' class='form-control' name='employeePayScaleId' id = 'employeePayScaleId_hidden_<%=i%>' value='<%=am_house_allocation_requestDTO.employeePayScaleId%>' tag='pb_html'/>
        <label class="col-form-label" id = 'label_employeePayScaleTypeEn_text_<%=i%>'></label>
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_BASICSALARYEN, loginDTO)%>
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <input type='text' class='form-control'  name='basicSalaryEn' id = 'basicSalaryEn_text_<%=i%>' value='<%=Utils.getDigits(am_house_allocation_requestDTO.basicSalaryEn, Language)%>'   tag='pb_html'/>
    </div>
</div>



<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASSPECIALSALARY, loginDTO)%></label>
    <div class="col-md-9">
        <input type='checkbox' class='form-control-sm' name='hasSpecialSalary' id = 'hasSpecialSalary_checkbox_<%=i%>' value='true' <%=(String.valueOf(am_house_allocation_requestDTO.hasSpecialSalary).equals("true"))?("checked"):""%>
               tag='pb_html'>
    </div>
</div>
<div class="form-group row" id="special_salary_amount_div_id" style="display: none">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SPECIALSALARYAMOUNT, loginDTO)%>
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <input type='text' class='form-control'  name='specialSalaryAmount' id = 'specialSalaryAmount_text_<%=i%>' value='<%=am_house_allocation_requestDTO.specialSalaryAmount%>'   tag='pb_html'/>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_NEXTINCREMENTDATE, loginDTO)%></label>
    <div class="col-md-9">


        <input type='hidden' class='form-control'  name='nextIncrementDate' id = 'nextIncrementDate_date_<%=i%>' value='<%=incrementDate%>'   tag='pb_html'/>
        <%
            value = incrementDateLong + "";
        %>
        <%
            String formatted_nextIncrementDate = dateFormat.format(new Date(Long.parseLong(value)));
        %>
    <label class="form-control">
        <%=Utils.getDigits(formatted_nextIncrementDate, Language)%>
    </label>


    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SALARYDESCRIPTIONUPDATEFILESDROPZONE, loginDTO)%></label>
    <div class="col-md-9">
        <%
            fileColumnName = "salaryDescriptionUpdateFilesDropzone";
            if(actionName.equals("ajax_edit"))
            {
                List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone);
        %>
        <%@include file="../../../pb/dropzoneEditor.jsp"%>
        <%
            }
            else
            {
                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone = ColumnID;
            }
        %>

        <div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone%>">
            <input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>
        </div>
        <input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
        <input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone%>'/>



    </div>
</div>


</form>



<script type="text/javascript">

    $(document).ready(function(){

        $("#hasSpecialSalary_checkbox_0").change(function() {
            if(this.checked) {
                $("#special_salary_amount_div_id").show();
            }
            else {
                $("#special_salary_amount_div_id").hide();
            }
        });

        if ($('#hasSpecialSalary_checkbox_0').is(':checked')) $("#special_salary_amount_div_id").show();




        $.validator.addMethod('specialSalaryValidation', function (value, element) {
            return !$('#hasSpecialSalary_checkbox_0').is(':checked')
                || ($("#specialSalaryAmount_text_0").val() != undefined
                    && $("#specialSalaryAmount_text_0").val().toString().trim().length > 0);
        });


        let lang = '<%=Language%>';
        let currentPayScaleErr;
        let currentBasicSalaryErr;
        let specialSalaryErr;


        if (lang == 'english') {
            currentPayScaleErr = 'Please provide current payscale';
            currentBasicSalaryErr = 'Please provide current basic salary';
            specialSalaryErr = 'Please provide special/personal salary';

        } else {
            currentPayScaleErr = 'বর্তমান বেতন স্কেল প্রদান করুন';
            currentBasicSalaryErr = 'বর্তমান মূল বেতন প্রদান করুন';
            specialSalaryErr = 'বিশেষ বেতন/ব্যক্তিগত বেতন প্রদান করুন';

        }

        $("#salary_description_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                employeePayScaleTypeEn: {
                    required: true,
                },
                basicSalaryEn: {
                    required: true,
                },
                specialSalaryAmount: {
                    specialSalaryValidation: true,
                },

            },
            messages: {
                employeePayScaleTypeEn: currentPayScaleErr,
                basicSalaryEn: currentBasicSalaryErr,
                specialSalaryAmount: specialSalaryErr,

            }
        });


    });

</script>









