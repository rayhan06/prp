<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="am_house_allocation_request.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>


<form class="form-horizontal"
      id="personal_info_form" name="personal_info_form" method="POST" enctype="multipart/form-data">

    <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right">
            <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERNAMEEN, loginDTO)%>
            <span class="required"> * </span>
        </label>
        <div class="col-md-9 px-0">
            <input type="hidden" class='form-control'
                   name='employeeRecordsId' id='employeeRecordsId'
                   value=''>
<%--            <button type="button"--%>
<%--                    class="btn btn-primary btn-block shadow btn-border-radius mb-3"--%>
<%--                    id="tagRequester_modal_button">--%>
<%--                <%=LM.getText(LC.HM_SELECT, userDTO)%>--%>
<%--            </button>--%>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <thead></thead>

                    <tbody id="tagged_requester_table">
                    <tr style="display: none;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <button type="button"
                                    class="btn btn-sm delete-trash-btn"
                                    onclick="remove_containing_row(this,'tagged_requester_table');">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>

                    <% if (actionName.equals("ajax_edit")) { %>
                    <tr>
                        <%--                        <td><%=Employee_recordsRepository.getInstance()--%>
                        <%--                                .getById(am_house_allocation_requestDTO.requesterEmpId).employeeNumber%>--%>
                        <td><%=Utils.getDigits(am_house_allocation_requestDTO.requesterEmpId,Language)%>
                        </td>
                        <td>
                            <%=isLanguageEnglish ? (am_house_allocation_requestDTO.requesterNameEn)
                                    : (am_house_allocation_requestDTO.requesterNameBn)%>
                        </td>

                        <%
                            String postName = isLanguageEnglish ? (am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn + ", " + am_house_allocation_requestDTO.requesterOfficeUnitNameEn)
                                    : (am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn + ", " + am_house_allocation_requestDTO.requesterOfficeUnitNameBn);
                        %>

                        <td><%=postName%>
                        </td>
<%--                        <td id='<%=am_house_allocation_requestDTO.requesterEmpId%>_td_button'>--%>
<%--                            <button type="button"--%>
<%--                                    class="btn btn-sm delete-trash-btn"--%>
<%--                                    onclick="remove_containing_row(this,'tagged_requester_table');">--%>
<%--                                <i class="fa fa-trash"></i>--%>
<%--                            </button>--%>

<%--                        </td>--%>
                    </tr>
                    <%} else {%>
                    <tr>
                        <%--                        <td><%=Utils.getDigits(own.employeeRecordId,Language)%>--%>
                        <td><%=Utils.getDigits(WorkflowController.getUserNameFromOrganogramId(own.organogramId),Language)%>
                        </td>
                        <td>
                            <%=isLanguageEnglish ? (own.employeeNameEn)
                                    : (own.employeeNameBn)%>
                        </td>

                        <%
                            String postName = isLanguageEnglish ? (own.organogramNameEn + ", " + own.officeUnitNameEn)
                                    : (own.organogramNameBn + ", " + own.officeUnitNameBn);
                        %>

                        <td><%=postName%>
                        </td>
<%--                        <td id='<%=own.employeeRecordId%>_td_button'>--%>
<%--                            <button type="button"--%>
<%--                                    class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"--%>
<%--                                    style="padding-right: 14px"--%>
<%--                                    onclick="remove_containing_row(this,'tagged_requester_table');">--%>
<%--                                <i class="fa fa-trash"></i>--%>
<%--                            </button>--%>

<%--                        </td>--%>
                    </tr>
                    <%
                        }
                    %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%>
        </label>
        <div class="col-md-9 form-control">
            <%--		<%value = "dateOfBirth_js_" + i;%>--%>
            <%--		<jsp:include page="/date/date.jsp">--%>
            <%--			<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
            <%--			<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
            <%--		</jsp:include>--%>
            <%--		<input type='hidden' name='dateOfBirth' id = 'dateOfBirth_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.dateOfBirth)).toString()%>' tag='pb_html'>--%>
            <label class="" id='label_requester_org_name_<%=i%>'>
                <%=requesterOrgName%>
            </label>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%>
        </label>
        <div class="col-md-9 form-control">
            <%--		<%value = "dateOfBirth_js_" + i;%>--%>
            <%--		<jsp:include page="/date/date.jsp">--%>
            <%--			<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
            <%--			<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
            <%--		</jsp:include>--%>
            <%--		<input type='hidden' name='dateOfBirth' id = 'dateOfBirth_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.dateOfBirth)).toString()%>' tag='pb_html'>--%>
            <label class="" id='label_requester_unit_name_<%=i%>'>
                <%=requesterUnitName%>
            </label>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DATEOFBIRTH, loginDTO)%>
        </label>
        <div class="col-md-9 form-control">
            <%--		<%value = "dateOfBirth_js_" + i;%>--%>
            <%--		<jsp:include page="/date/date.jsp">--%>
            <%--			<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
            <%--			<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
            <%--		</jsp:include>--%>
            <%--		<input type='hidden' name='dateOfBirth' id = 'dateOfBirth_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.dateOfBirth)).toString()%>' tag='pb_html'>--%>
            <input type='hidden' name='dateOfBirth' id='dateOfBirth_date_<%=i%>'
                   value='<%=dateFormat.format(new Date(am_house_allocation_requestDTO.dateOfBirth))%>'>
            <label class="" id='label_dateOfBirth_date_<%=i%>'>
                <%=dateFormat.format(new Date(am_house_allocation_requestDTO.dateOfBirth))%>
            </label>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_MARITALSTATUSCAT, loginDTO)%>
        </label>
        <div class="col-md-9 form-control">
            <input type='hidden' name='maritalStatusCat' id='maritalStatusCat_category_<%=i%>'
                   value='<%=am_house_allocation_requestDTO.maritalStatusCat%>'>
            <label class="" id='label_maritalStatusCat_category_<%=i%>'>
                <%=CatRepository.getInstance().getText(Language, "marital_status", am_house_allocation_requestDTO.maritalStatusCat)%>
            </label>

        </div>
    </div>


</form>


<script type="text/javascript">


    $(document).ready(function () {

        $.validator.addMethod('requesterEmpValidation', function (value, element) {
            let data = added_requester_map.keys().next();
            if (!(data && data.value && data.value != undefined)) {
                return false;
            }
            return true;
        });

        let lang = '<%=Language%>';
        let employeeErr;


        if (lang == 'english') {
            employeeErr = 'Please Select Person';

        } else {
            employeeErr = 'এমপ্লয়ী সিলেক্ট করুন';

        }

        $("#personal_info_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                employeeRecordsId: {
                    requesterEmpValidation: true,
                },

            },
            messages: {
                employeeRecordsId: employeeErr,

            }
        });


    });


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("ajax_edit")) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (am_house_allocation_requestDTO.requesterEmpId,am_house_allocation_requestDTO.requesterOfficeUnitId,am_house_allocation_requestDTO.requesterOrgId));
    }
    else {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (own.employeeRecordId,own.officeUnitId,own.organogramId));
    }
    %>

    added_requester_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_requester_table', {
                info_map: added_requester_map,
                isSingleEntry: true,
                callBackFunction: function (empInfo) {
                    var getRequesterUrl = 'Am_house_allocation_requestServlet?' +
                        'actionType=getRequesterDetails' +
                        '&id=' + empInfo.employeeRecordId;
                    ajaxGet(getRequesterUrl, loadRequesterData, loadRequesterData);
                }
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagRequester_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_requester_table';
        $('#search_emp_modal').modal();
    });

</script>



