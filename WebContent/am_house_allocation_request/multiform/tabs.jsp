<%@page contentType="text/html;charset=utf-8" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>

<style>
    .double_blank_row {
        height: 40px !important;
        background-color: transparent;
    }

    .form-control-plaintext {
        padding: 0;
        font-size: 1.2em;
    }

    .form-group label {
        font-size: 1em;
        margin: 0;
    }

    .edit-tools {
        display: inline-block;
        float: right;
    }

    .edit-tools .btn {
        background-color: #ffffff;
        font-size: 12px;
        font-weight: 600;
        color: #808080;
        border: none;
        padding: 0 5px;
        font-family: 'Montserrat', sans-serif;
    }

    .edit-tools .btn {
        color: #0060DB;
        padding: 6px 10px;
        box-shadow: none;
        text-transform: uppercase;
        font-weight: bold;
    }

    .edit-tools .delete-btn {
        color: #E71313;
    }

    .edit-tools .delete-btn:hover {
        color: #CF1111;
    }

    .edit-tools .edit-btn:hover {
        color: #0055C2;
    }

    .edit-tools .btn:hover {
        background-color: #eceff1;
        box-shadow: none;
    }

    .edit-tools .view-btn {
        color: #44cf50;
    }


    .edit-tools .download-btn {
        color: greenyellow;
    }

    .btn-gray,
    .btn-gray:focus {
        background-color: #ffffff;
        color: #008020;
        margin-bottom: 10px;
        border: 2px solid #e0e0e0;
        padding: 10px 15px;
        font-weight: 600;
    }

    .btn-gray:hover {
        background-color: #ffffff;
        color: #008020;
        border: 2px solid #008020;
        box-shadow: none !important;
    }

</style>
<%
    Integer tab = (Integer) request.getAttribute("tab");
    String actionType = request.getParameter("actionType");
    LoginDTO loginDTOTabs = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

%>

<div class="btn-group tab-group row" role="group" style="margin: 0px 0px 5px 0px !important;">
    <div class="col-md-2">
        <button type="button" id="btn_personal_info"
                onclick="clickOnTab('personal_info')"
                class="btn tab-btn <%=tab == 1?"active":""%> btn-tab-personal_info">
            <i class="fa fa-user-circle"></i>&nbsp;
            <small><%= LM.getText(LC.PERSONAL_INFORMATION_HR_MANAGEMENT_PERSONAL_INFO, loginDTOTabs) %>
            </small>
        </button>

        <div class="arrow-down" id="arrow-down-personal_info"></div>

    </div>

    <div class="col-md-2">
        <button type="button" id="btn_salary_description"
                onclick="clickOnTab('salary_description')"
                class="btn tab-btn <%=tab == 2?"active":""%> btn-tab-salary_description">
            <i class="fa fa-briefcase "></i>
            <small><%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SALARY_DESCRIPTION, loginDTOTabs) %>
            </small>
        </button>

        <div class="arrow-down" id="arrow-down-salary_description" style="display: none"></div>

    </div>

    <div class="col-md-2">
        <button type="button" id="btn_choose_house"
                onclick="clickOnTab('choose_house')"
                class="btn tab-btn <%=tab == 3?"active":""%> btn-tab-choose_house">
            <i class="fa fa-home "></i>&nbsp;<small><%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CHOOSE_HOUSE, loginDTOTabs) %>
        </small>
        </button>

        <div class="arrow-down" id="arrow-down-choose_house" style="display: none"></div>

    </div>

    <div class="col-md-2">
        <button type="button" id="btn_reliable_family_member"
                onclick="clickOnTab('reliable_family_member')"
                class="btn tab-btn <%=tab == 4?"active":""%> btn-tab-reliable_family_member">
            <i class="fa fa-user-friends"></i>&nbsp;<small><%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELIABLE_FAMILY_MEMBERS, loginDTOTabs) %>
        </small>
        </button>

        <div class="arrow-down" id="arrow-down-reliable_family_member" style="display: none"></div>

    </div>

    <div class="col-md-2">
        <button type="button" id="btn_job_description"
                onclick="clickOnTab('job_description')"
                class="btn tab-btn <%=tab == 5?"active":""%> btn-tab-job_description">
            <i class="fa fa-building"></i>&nbsp;<small><%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_JOB_DESCRIPTION, loginDTOTabs) %>
        </small>
        </button>

        <div class="arrow-down" id="arrow-down-job_description" style="display: none"></div>

    </div>

    <div class="col-md-2">
        <button type="button" id="btn_current_house_miscellaneous"
                onclick="clickOnTab('current_house_miscellaneous')"
                class="btn tab-btn <%=tab == 6?"active":""%> btn-tab-current_house_miscellaneous">
            <i class="fa fa-address-card"></i>&nbsp;<small><%= LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENT_HOUSE_MISCELLANEOUS, loginDTOTabs) %>
        </small>
        </button>

        <div class="arrow-down" id="arrow-down-current_house_miscellaneous" style="display: none"></div>

    </div>

</div>

<br/>
<script src="<%=request.getContextPath()%>/assets/scripts/util1.js"></script>
<script>
    function deleteItem(id, index) {
        deleteDialog(null, () => {
            let formName = '#' + id + index;
            console.log($(formName));
            $(formName).submit();
        });
    }

</script>