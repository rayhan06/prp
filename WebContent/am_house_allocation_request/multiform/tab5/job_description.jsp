<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="am_house_allocation_request.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="java.time.ZoneId" %>
<%@ page import="java.time.LocalDate" %>

<%
    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    int endYear  = localDate.getYear() + 100;
%>

<form class="form-horizontal"
      id="job_description_form" name="job_description_form" method="POST" enctype="multipart/form-data">


<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDATE, loginDTO)%></label>
    <div class="col-md-9">
<%--        <%value = "govtFirstAppointmentDate_js_" + i;%>--%>
<%--        <jsp:include page="/date/date.jsp">--%>
<%--            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
<%--            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--        </jsp:include>--%>
<%--        <input type='hidden' name='govtFirstAppointmentDate' id = 'govtFirstAppointmentDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.govtFirstAppointmentDate)).toString()%>' tag='pb_html'>--%>
<%--       --%>
        <input type='hidden' readonly name='govtFirstAppointmentDate' id = 'govtFirstAppointmentDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.govtFirstAppointmentDate))%>' tag='pb_html'>
    <label class="form-control" id = 'label_govtFirstAppointmentDate_date_<%=i%>'>
        <%=dateFormat.format(new Date(am_house_allocation_requestDTO.govtFirstAppointmentDate))%>
    </label>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDATE, loginDTO)%></label>
    <div class="col-md-9">
<%--        <%value = "prpFirstJoiningDate_js_" + i;%>--%>
<%--        <jsp:include page="/date/date.jsp">--%>
<%--            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
<%--            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--        </jsp:include>--%>
<%--        <input type='hidden' name='prpFirstJoiningDate' id = 'prpFirstJoiningDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.prpFirstJoiningDate)).toString()%>' tag='pb_html'>--%>

        <input type='hidden' readonly name='prpFirstJoiningDate' id = 'prpFirstJoiningDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.prpFirstJoiningDate))%>' tag='pb_html'>
    <label class="form-control" id = 'label_prpFirstJoiningDate_date_<%=i%>'>
        <%=dateFormat.format(new Date(am_house_allocation_requestDTO.prpFirstJoiningDate))%>
    </label>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTDESIGNATIONJOININGDATE, loginDTO)%></label>
    <div class="col-md-9">
<%--        <%value = "currentDesignationJoiningDate_js_" + i;%>--%>
<%--        <jsp:include page="/date/date.jsp">--%>
<%--            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
<%--            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--        </jsp:include>--%>
<%--        <input type='hidden' name='currentDesignationJoiningDate' id = 'currentDesignationJoiningDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.currentDesignationJoiningDate)).toString()%>' tag='pb_html'>--%>
<%--        --%>
        <input type='hidden' readonly name='currentDesignationJoiningDate' id = 'currentDesignationJoiningDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.currentDesignationJoiningDate))%>' tag='pb_html'>
    <label class="form-control" id = 'label_currentDesignationJoiningDate_date_<%=i%>'>
        <%=dateFormat.format(new Date(am_house_allocation_requestDTO.currentDesignationJoiningDate))%>
    </label>
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTSALARY, loginDTO)%>
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <input type='text' class='form-control'  name='govtFirstSalary' id = 'govtFirstSalary_text_<%=i%>' value='<%=am_house_allocation_requestDTO.govtFirstSalary%>'  tag='pb_html'/>
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDESIGNATIONEN, loginDTO)%></label>
    <div class="col-md-9">
        <input type='hidden' readonly class='form-control'  name='prpFirstJoiningDesignationEn' id = 'prpFirstJoiningDesignationEn_text_<%=i%>' value='<%=am_house_allocation_requestDTO.prpFirstJoiningDesignationEn%>'   tag='pb_html'/>
        <label class="form-control" id = 'label_prpFirstJoiningDesignationEn_text_<%=i%>'>
            <%=am_house_allocation_requestDTO.prpFirstJoiningDesignationEn%>
        </label>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDESIGNATIONBN, loginDTO)%></label>
    <div class="col-md-9">
        <input type='hidden' readonly class='form-control'  name='prpFirstJoiningDesignationBn' id = 'prpFirstJoiningDesignationBn_text_<%=i%>' value='<%=am_house_allocation_requestDTO.prpFirstJoiningDesignationBn%>'   tag='pb_html'/>
        <label class="form-control" id = 'label_prpFirstJoiningDesignationBn_text_<%=i%>'>
            <%=am_house_allocation_requestDTO.prpFirstJoiningDesignationBn%>
        </label>
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYDATE, loginDTO)%></label>
    <div class="col-md-9">
        <%value = "requestedHomeAvailablityDate_js_" + i;%>
        <jsp:include page="/date/date.jsp">
            <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
            <jsp:param name="END_YEAR" value="<%=endYear%>"></jsp:param>
        </jsp:include>
        <input type='hidden' name='requestedHomeAvailablityDate' id = 'requestedHomeAvailablityDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(am_house_allocation_requestDTO.requestedHomeAvailablityDate))%>' tag='pb_html'>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYEN, loginDTO)%>
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <input type='text' class='form-control'  name='requestedHomeAvailablitySalaryEn' id = 'requestedHomeAvailablitySalaryEn_text_<%=i%>' value='<%=am_house_allocation_requestDTO.requestedHomeAvailablitySalaryEn%>'   tag='pb_html'/>
    </div>
</div>
<%--<div class="form-group row">--%>
<%--    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYBN, loginDTO)%></label>--%>
<%--    <div class="col-md-9">--%>
<%--        <input type='text' class='form-control'  name='requestedHomeAvailablitySalaryBn' id = 'requestedHomeAvailablitySalaryBn_text_<%=i%>' value='<%=am_house_allocation_requestDTO.requestedHomeAvailablitySalaryBn%>'   tag='pb_html'/>--%>
<%--    </div>--%>
<%--</div>--%>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYFILESDROPZONE, loginDTO)%></label>
    <div class="col-md-9">
        <%
            fileColumnName = "requestedHomeAvailablitySalaryFilesDropzone";
            if(actionName.equals("ajax_edit"))
            {
                List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone);
        %>
        <%@include file="../../../pb/dropzoneEditor.jsp"%>
        <%
            }
            else
            {
                ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone = ColumnID;
            }
        %>

        <div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone%>">
            <input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>
        </div>
        <input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
        <input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone%>'/>



    </div>
</div>


<div class="form-group row hiddenInput" id="isHigherAvailabilityDivId">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_ISHIGHERAVAILABILITY, loginDTO)%></label>
    <div class="col-md-9">
        <input type='checkbox' onclick="return false;" class='form-control-sm mt-1' name='isHigherAvailability' id = 'isHigherAvailability_checkbox_<%=i%>' value='true' 																<%=(String.valueOf(am_house_allocation_requestDTO.isHigherAvailability).equals("true"))?("checked"):""%>
               tag='pb_html'>
    </div>
</div>
<div class="form-group row hiddenInput" id="higherStepNumber_div_id" >
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HIGHERSTEPNUMBER, loginDTO)%></label>
    <div class="col-md-9">
        <input type='text' class='form-control' readonly="readonly"  name='higherStepNumber' id = 'higherStepNumber_text_<%=i%>' value='<%=am_house_allocation_requestDTO.higherStepNumber%>'   tag='pb_html'/>
    </div>
</div>

</form>
<script type="text/javascript">

    $(document).ready(function(){

        $("#isHigherAvailability_checkbox_0").change(function() {
            if(this.checked) {
                $("#higherStepNumber_div_id").show();
            }
            else {
                $("#higherStepNumber_div_id").hide();
            }
        });

        if ($('#isHigherAvailability_checkbox_0').is(':checked')) $("#higherStepNumber_div_id").show();




        $.validator.addMethod('higherStepNumberValidation', function (value, element) {
            return !$('#isHigherAvailability_checkbox_0').is(':checked')
                || ($("#higherStepNumber_text_0").val() != undefined
                    && $("#higherStepNumber_text_0").val().toString().trim().length > 0);
        });


        let lang = '<%=Language%>';
        let govtFirstJoinSalaryErr;
        let availabilityBasicSalaryErr;
        let higherStepNumberErr;


        if (lang == 'english') {
            govtFirstJoinSalaryErr = 'Please provide salary of first government job';
            availabilityBasicSalaryErr = 'Please provide basic salary on house availability date';
            higherStepNumberErr = 'Please provide higher step number';

        } else {
            govtFirstJoinSalaryErr = 'সরকারি চাকরিতে প্রথম নিয়োগের বেতন প্রদান করুন';
            availabilityBasicSalaryErr = 'আবেদিত বাসার প্রাপ্যতা অর্জনের তারিখে মূল বেতন প্রদান করুন';
            higherStepNumberErr = 'কয় ধাপ ঊর্ধ্বে আবেদন করেছেন প্রদান করুন';

        }

        $("#job_description_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                govtFirstSalary: {
                    required: true,
                },
                requestedHomeAvailablitySalaryEn: {
                    required: true,
                },
                higherStepNumber: {
                    higherStepNumberValidation: true,
                },

            },
            messages: {
                govtFirstSalary: govtFirstJoinSalaryErr,
                requestedHomeAvailablitySalaryEn: availabilityBasicSalaryErr,
                higherStepNumber: higherStepNumberErr,

            }
        });


    });

</script>