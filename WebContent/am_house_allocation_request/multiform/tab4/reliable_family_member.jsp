<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="am_house_allocation_request.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="am_reliable_family_member.Am_reliable_family_memberDTO" %>
<%@ page import="employee_family_info.Employee_family_infoDTO" %>
<%@ page import="employee_family_info.Employee_family_infoDAO" %>
<%@ page import="am_reliable_family_member.Am_reliable_family_memberDAO" %>
<%@ page import="relation.RelationRepository" %>



<form class="form-horizontal"
      id="family_info_form" name="family_info_form" method="POST" enctype="multipart/form-data">


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_ADD_EMPLOYEERECORDSID, loginDTO)%></th>
            <th><%=LM.getText(LC.APPLICANT_PROFILE_ADD_AGE, loginDTO)%></th>
            <th><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_RELATIONTYPE, loginDTO)%></th>
            <th><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_ADD_REMARKS, loginDTO)%></th>
            <th class="d-flex justify-content-between align-items-baseline">
                <div>
                    <span><%=LM.getText(LC.AM_RELIABLE_FAMILY_MEMBER_SEARCH_ANYFIELD, loginDTO)%></span>
                </div>
                <div>
                    <input type="checkbox" class="ml-3" id="isAllReliable" onclick="allReliable()"/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody id="family_info_tbody_id">
        <%
            ArrayList data = (ArrayList<Employee_family_infoDTO>) employee_family_infoDTOS;

            try
            {

                if (data != null)
                {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int iReliable = 0; iReliable < size; iReliable++)
                    {
                        Employee_family_infoDTO employee_family_infoDTO = (Employee_family_infoDTO) data.get(iReliable);


        %>
        <tr>


            <td>
                <%
                    if (Language.equals("Bangla")) {
                        value = employee_family_infoDTO.firstNameBn;
                    }
                    else {
                        value = employee_family_infoDTO.firstNameEn;
                    }
                %>

                <%=value%>


                <input type='hidden' class='form-control'  name='familyMemberId' id = 'familyMemberId_hidden_<%=iReliable%>'
                       value='<%=employee_family_infoDTO.iD%>' tag='pb_html'/>

                
                <input type='hidden' class='form-control'  name='isReliableValues' id = 'isReliableValues_hidden_<%=iReliable%>'
                       tag='pb_html'/>

            </td>

            <td>
                <%
                    value = Utils.calculateCompleteAgeInFullFormat(employee_family_infoDTO.dob, Language) + "";
                %>

                <%=value%>


            </td>

            <td>
                <%
                    value = RelationRepository.getInstance().getText(Language, employee_family_infoDTO.relationType) + "";
                %>

                <%=value%>


            </td>

            <td>
                <%
                    value = familyInfoIdToReliableMap.getOrDefault(employee_family_infoDTO.iD, new Am_reliable_family_memberDTO()).remarks + "";
                %>

                <textarea class='form-control w-auto' name='reliableFamilyMemberRemarks'
                          id='reliableFamilyMemberRemarks_text_<%=iReliable%>'><%=value%>
                </textarea>


            </td>










            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit' ><input type='checkbox' <%=(familyInfoIdToReliableMap.containsKey(employee_family_infoDTO.iD)
                            && familyInfoIdToReliableMap.get(employee_family_infoDTO.iD).isReliable == 1)?"checked":""%>
                                                  name='isReliable'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                }
                else
                {
                    System.out.println("data  null");
                }
            }
            catch(Exception e)
            {
                System.out.println("JSP exception " + e);
            }
        %>



        </tbody>

    </table>
</div>


</form>
