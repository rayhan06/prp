<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="am_house_allocation_request.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>


<form class="form-horizontal"
      id="choose_house_form" name="choose_house_form" method="POST" enctype="multipart/form-data">

    <div class="form-group row">
                                        <span id="higherEligibilityMsg_text_0" class="col-12 text-center font-weight-bold" style="color: red">
                                        </span>
    </div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <select class='form-control' onchange="loadHouse()" name='amHouseOldNewCat' id = 'amHouseOldNewCat_category_<%=i%>'   tag='pb_html'>
            <%
                Options = CatRepository.getInstance().buildOptions("am_house_old_new", Language, am_house_allocation_requestDTO.amHouseOldNewCat);
            %>
            <%=Options%>
        </select>

    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <select class='form-control' onchange="loadHouse()" name='amHouseLocationCat' id = 'amHouseLocationCat_category_<%=i%>'   tag='pb_html'>
            <%
                Options = CatRepository.getInstance().buildOptions("am_house_location", Language, am_house_allocation_requestDTO.amHouseLocationCat);
            %>
            <%=Options%>
        </select>

    </div>
</div>
<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSECLASSCAT, loginDTO)%>
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <select class='form-control' onchange="loadHouseSubClass()" name='amHouseClassCat' id = 'amHouseClassCat_category_<%=i%>'   tag='pb_html'>
            <%
                Options = CatRepository.getInstance().buildOptions("am_house_class", Language, am_house_allocation_requestDTO.amHouseClassCat);
            %>
            <%=Options%>
        </select>

    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=UtilCharacter.getDataByLanguage(Language, "আবেদনাধীন বাসার উপশ্রেণী(প্রাপ্যতা অনুযায়ী)", "House Sub Class(According to Availability)")%>
    </label>
    <div class="col-md-9">
        <select class='form-control' onchange="loadHouse()" name='amHouseClassSubCat' id = 'amHouseClassSubCat_<%=i%>'   tag='pb_html'>
            <%
                Options = Am_house_type_sub_categoryRepository.getInstance().buildOptionsByHouseClass(Language,
                        am_house_allocation_requestDTO.amHouseClassCat, am_house_allocation_requestDTO.amHouseClassSubCat);
            %>
            <%=Options%>
        </select>

    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEID, loginDTO)%>
        <span class="required"> * </span>
    </label>
    <div class="col-md-9">
        <select class='form-control' name='amHouseId' id = 'amHouseId_select_<%=i%>'   tag='pb_html'>

        </select>
    </div>
</div>


</form>


<script type="text/javascript">
    function loadHouse() {
        var oldNew = $('#amHouseOldNewCat_category_0').val();
        var location = $('#amHouseLocationCat_category_0').val();
        var houseClass = $('#amHouseClassCat_category_0').val();

        if (
            houseClass != undefined && houseClass.toString().trim().length > 0 && houseClass != '-1'
        ){
            let url = "Am_grade_house_mappingServlet?actionType=getApplyForHigherClass&classCat="+ houseClass+
            '&id='+'<%=requesterEmpId%>';
            ajaxGet(url, processHouseGradeResponse, processHouseGradeResponse);
        }

        if (
            oldNew != undefined && oldNew.toString().trim().length > 0 && oldNew != '-1' &&
            location != undefined && location.toString().trim().length > 0 && location != '-1' &&
            houseClass != undefined && houseClass.toString().trim().length > 0 && houseClass != '-1'
        )
        {
            let url = "Am_houseServlet?actionType=getActiveHouse&newCat=" + oldNew
                + "&locCat="+ location
                + "&classCat="+ houseClass
                + "&defaultValue=" + '<%=am_house_allocation_requestDTO.amHouseId%>';
            ajaxGet(url, processHouseResponse, processHouseResponse);
        }
    }

    function processHouseResponse(data){
        document.getElementById('amHouseId_select_0').innerHTML = data.msg;
    }

    function processHouseGradeResponse(data){
        let upperGrade = data.msg.split("_");
        let isHigherAvailability = document.querySelector('#isHigherAvailabilityDivId');
        let isHigherAvailabilityCheckBox = document.querySelector('#isHigherAvailability_checkbox_0');
        let higherStepNumber_div_id = document.querySelector('#higherStepNumber_div_id');
        let higherStepNumber_text = document.querySelector('#higherStepNumber_text_0');


        if(+upperGrade[1] > 0){
            showToastSticky("আপনি "+convertEnglishDigitToBangla(upperGrade[1])+" ধাপ উপরের গ্রেডের বাসার জন্য আবেদন করছেন",
                "You are applying for a "+upperGrade[1]+" step upper grade home");
            isHigherAvailability.classList.remove("hiddenInput");
            higherStepNumber_div_id.classList.remove("hiddenInput");
            isHigherAvailabilityCheckBox.checked = true;
            higherStepNumber_text.value =  upperGrade[1];
        }
        else{
            isHigherAvailabilityCheckBox.checked = false;
            isHigherAvailability.classList.add("hiddenInput");
            higherStepNumber_div_id.classList.add("hiddenInput");
            higherStepNumber_text.value = '';
        }


    }

    function loadHouseSubClass(){
        let houseClass = $('#amHouseClassCat_category_0').val();
        if ( houseClass !== undefined && houseClass.toString().trim().length > 0 && houseClass !== '-1' ){
            let url = "Am_house_type_sub_categoryServlet?actionType=getHouseSubClass&houseClass="+ houseClass;
            $.ajax({
                type: "GET",
                url: getContextPath()+url,
                dataType: "json",
                success: function (data){
                    document.getElementById("amHouseClassSubCat_0").innerHTML = data.msg;
                    loadHouse();
                },
                error: function (){
                    showError("বাসার উপশ্রেণী লোড হয়নি!", "House sub class not loaded!")
                },
                complete: function () {
                }
            });
        }
    }

    function convertEnglishDigitToBangla(str){
        str = String(str);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    $(document).ready(function(){

        loadHouse();




        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });


        let lang = '<%=Language%>';
        let oldNewErr;
        let locationErr;
        let classErr;
        let houseNumErr;


        if (lang == 'english') {
            oldNewErr = 'Please provide if old or new';
            locationErr = 'Please provide location';
            classErr = 'Please provide class';
            houseNumErr = 'Please provide house number';

        } else {
            oldNewErr = 'নতুন বাসা/পুরাতন বাসা সিলেক্ট করুন';
            locationErr = 'বাসার অবস্থান প্রদান করুন';
            classErr = 'আবেদনাধীন বাসার শ্রেণী(প্রাপ্যতা অনুযায়ী) প্রদান করুন';
            houseNumErr = 'আবেদনাধীন বাসার নম্বর প্রদান করুন';

        }

        $("#choose_house_form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                amHouseOldNewCat: {
                    validSelector: true,
                },
                amHouseLocationCat: {
                    validSelector: true,
                },
                amHouseClassCat: {
                    validSelector: true,
                },
                amHouseId: {
                    validSelector: true,
                },

            },
            messages: {
                amHouseOldNewCat: oldNewErr,
                amHouseLocationCat: locationErr,
                amHouseClassCat: classErr,
                amHouseId: houseNumErr,

            }
        });



    });

</script>









