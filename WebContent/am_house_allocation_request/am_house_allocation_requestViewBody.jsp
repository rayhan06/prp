<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="am_house_allocation_request.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>


<%
    String servletName = "Am_house_allocation_requestServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Am_house_allocation_requestDTO am_house_allocation_requestDTO = Am_house_allocation_requestDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = am_house_allocation_requestDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AM_HOUSE_ALLOCATION_REQUEST_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AM_HOUSE_ALLOCATION_REQUEST_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERORGID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOrgId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOfficeId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOfficeUnitId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEREMPID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterEmpId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERPHONENUM, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterPhoneNum + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTERNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICENAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOfficeNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICENAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOfficeNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOfficeUnitNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOfficeUnitNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DATEOFBIRTH, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.dateOfBirth + "";
                                        %>
                                        <%
                                            String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_dateOfBirth, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_MARITALSTATUSCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.maritalStatusCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "marital_status", am_house_allocation_requestDTO.maritalStatusCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.employeePayScaleId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALETYPEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.employeePayScaleTypeEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_EMPLOYEEPAYSCALETYPEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.employeePayScaleTypeBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_BASICSALARYEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.basicSalaryEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_BASICSALARYBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.basicSalaryBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASSPECIALSALARY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.hasSpecialSalary + "";
                                        %>

                                        <%=Utils.getYesNo(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SPECIALSALARYAMOUNT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.specialSalaryAmount + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_NEXTINCREMENTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.nextIncrementDate + "";
                                        %>
                                        <%
                                            String formatted_nextIncrementDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_nextIncrementDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_SALARYDESCRIPTIONUPDATEFILESDROPZONE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone + "";
                                        %>
                                        <%
                                            {
                                                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(am_house_allocation_requestDTO.salaryDescriptionUpdateFilesDropzone);
                                        %>
                                        <%@include file="../pb/dropzoneViewer.jsp" %>
                                        <%
                                            }
                                        %>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.amHouseOldNewCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "am_house_old_new", am_house_allocation_requestDTO.amHouseOldNewCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.amHouseLocationCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "am_house_location", am_house_allocation_requestDTO.amHouseLocationCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSECLASSCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.amHouseClassCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "am_house_class", am_house_allocation_requestDTO.amHouseClassCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.amHouseId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.govtFirstAppointmentDate + "";
                                        %>
                                        <%
                                            String formatted_govtFirstAppointmentDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_govtFirstAppointmentDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.prpFirstJoiningDate + "";
                                        %>
                                        <%
                                            String formatted_prpFirstJoiningDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_prpFirstJoiningDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTDESIGNATIONJOININGDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.currentDesignationJoiningDate + "";
                                        %>
                                        <%
                                            String formatted_currentDesignationJoiningDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_currentDesignationJoiningDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDESIGNATIONEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.govtFirstAppointmentDesignationEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTFIRSTAPPOINTMENTDESIGNATIONBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.govtFirstAppointmentDesignationBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDESIGNATIONEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.prpFirstJoiningDesignationEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_PRPFIRSTJOININGDESIGNATIONBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.prpFirstJoiningDesignationBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requestedHomeAvailablitySalaryEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requestedHomeAvailablitySalaryBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requestedHomeAvailablityDate + "";
                                        %>
                                        <%
                                            String formatted_requestedHomeAvailablityDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_requestedHomeAvailablityDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REQUESTEDHOMEAVAILABLITYSALARYFILESDROPZONE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone + "";
                                        %>
                                        <%
                                            {
                                                List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(am_house_allocation_requestDTO.requestedHomeAvailablitySalaryFilesDropzone);
                                        %>
                                        <%@include file="../pb/dropzoneViewer.jsp" %>
                                        <%
                                            }
                                        %>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_ISHIGHERAVAILABILITY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.isHigherAvailability + "";
                                        %>

                                        <%=Utils.getYesNo(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HIGHERSTEPNUMBER, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.higherStepNumber + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEDIVISIONCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.houseDivisionCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "house_division", am_house_allocation_requestDTO.houseDivisionCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTHOUSEDESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.currentHouseDescription + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASHOUSEINDHAKA, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.hasHouseInDhaka + "";
                                        %>

                                        <%=Utils.getYesNo(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSEINDHAKACONSTRUCTIONDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.houseInDhakaConstructionDate + "";
                                        %>
                                        <%
                                            String formatted_houseInDhakaConstructionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_houseInDhakaConstructionDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HASRELATIVEGOVTHOUSE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.hasRelativeGovtHouse + "";
                                        %>

                                        <%=Utils.getYesNo(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_GOVTHOUSERELATIVENAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.govtHouseRelativeName + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIONSHIPCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.relationshipCat + "";
                                        %>
                                        <%
                                            value = CatRepository.getInstance().getText(Language, "am_relationship", am_house_allocation_requestDTO.relationshipCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVEDESIGNATION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.relativeDesignation + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REALTIVEOFFICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.realtiveOffice + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_RELATIVESALARY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.relativeSalary + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_HOUSENEEDEDDESCRIPTION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.houseNeededDescription + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_STATUS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.status + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_DECISIONDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.decisionDate + "";
                                        %>
                                        <%
                                            String formatted_decisionDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_decisionDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_REJECTIONREASON, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.rejectionReason + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEDDATE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approvedDate + "";
                                        %>
                                        <%
                                            String formatted_approvedDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                        %>
                                        <%=Utils.getDigits(formatted_approvedDate, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERORGID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOrgId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOfficeId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOfficeUnitId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEREMPID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverEmpId + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERPHONENUM, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverPhoneNum + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVERNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICENAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOfficeNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICENAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOfficeNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOfficeUnitNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOfficeUnitNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEEN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOfficeUnitOrgNameEn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_APPROVEROFFICEUNITORGNAMEBN, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%
                                            value = am_house_allocation_requestDTO.approverOfficeUnitOrgNameBn + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>