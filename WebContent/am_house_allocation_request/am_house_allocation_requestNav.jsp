<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Am_house_allocation_requestServlet?actionType=search";
    String toggle = request.getParameter("toggle");
    boolean isExpanded = toggle != null && toggle.length() > 0 && toggle.equalsIgnoreCase("true");
%>

<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0,false)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='am_house_old_new_cat' id='am_house_old_new_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("am_house_old_new", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='am_house_location_cat' id='am_house_location_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("am_house_location", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSECLASSCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='am_house_class_cat' id='am_house_class_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("am_house_class", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_CURRENTHOUSE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='current_house_cat' id='current_house_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getInstance().buildOptions("am_current_house", Language, -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0,true)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const oldNewSelector = $('#am_house_old_new_cat');
    const locationSelector = $('#am_house_location_cat');
    const classSelector = $('#am_house_class_cat');
    const currentHouseSelector = $('#current_house_cat');
    const anyFieldSelector = $('#anyfield');
    const servletName = "Am_house_allocation_requestServlet";

    $(document).ready(() => {

        readyInit(servletName);
        <%if(isExpanded){%>
        document.querySelector('.kt-portlet__body').style.display = 'block';
        document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse');
        <%}%>

    });

    function resetInputs() {
        $('select').prop('selectedIndex', 0);
        anyFieldSelector.val('');
    }


    window.addEventListener('popstate', e => {
        if (e.state) {

            let params = e.state;
            dosubmit(params, false);
            resetInputs();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    switch (item[0]) {

                        case 'am_house_old_new_cat':
                            $('#am_house_old_new_cat option[value="' + item[1] + '"]').prop('selected', 'selected').change();
                            break;
                        case 'am_house_location_cat':
                            $('#am_house_location_cat option[value="' + item[1] + '"]').prop('selected', 'selected').change();
                            break;
                        case 'am_house_class_cat':
                            $('#am_house_class_cat option[value="' + item[1] + '"]').prop('selected', 'selected').change();
                            break;
                        case 'current_house_cat':
                            $('#current_house_cat option[value="' + item[1] + '"]').prop('selected', 'selected').change();
                            break;
                        case 'AnyField':
                            anyFieldSelector.val(item[1]);
                            break;
                        case 'toggle':
                            if(item[1]==='true'){
                                document.querySelector('.kt-portlet__body').style.display = 'block';
                                document.querySelector('#kt_portlet_tools_1').classList.remove('kt-portlet--collapse');
                            }
                            else{
                                document.querySelector('.kt-portlet__body').style.display = 'none';
                                document.querySelector('#kt_portlet_tools_1').classList.add('kt-portlet--collapse');
                            }
                            break;
                        default:
                            setPaginationFields(item);
                    }
                }
            });
        } else {
            dosubmit(null, false);
            resetInputs();
            resetPaginationFields();
        }
    });

    function dosubmit(params, pushState = true) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (pushState) {
                    history.pushState(params, '', servletName+'?actionType=search&' + params);
                }
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        let url = "<%=action%>&ajax=true&isPermanentTable=true";
        if (params) {
            url += "&" + params;
        }
        xhttp.open("Get", url, false);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number, pushState) {
        let params = 'search=true';
        let toggleFlag = 0;
        if (anyFieldSelector.val()) {
            params += '&AnyField=' + anyFieldSelector.val();
        }

        if (oldNewSelector.val()) {
            params += '&am_house_old_new_cat=' + $("#am_house_old_new_cat").val();
            toggleFlag = 1;
        }
        if (locationSelector.val()) {
            params += '&am_house_location_cat=' + $("#am_house_location_cat").val();
            toggleFlag = 1;
        }
        if (classSelector.val()) {
            params += '&am_house_class_cat=' + $("#am_house_class_cat").val();
            toggleFlag = 1;
        }
        if (currentHouseSelector.val()) {
            params += '&current_house_cat=' + $("#current_house_cat").val();
            toggleFlag = 1;
        }

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        let toggleVal = toggleFlag===1?true:false;
        params += '&toggle='+toggleVal;
        dosubmit(params, pushState);

    }

</script>

