<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="user_organogram_history.User_organogram_historyDTO"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
User_organogram_historyDTO user_organogram_historyDTO = (User_organogram_historyDTO)request.getAttribute("user_organogram_historyDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(user_organogram_historyDTO == null)
{
	user_organogram_historyDTO = new User_organogram_historyDTO();
	
}
System.out.println("user_organogram_historyDTO = " + user_organogram_historyDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";
User_organogram_historyDTO row = user_organogram_historyDTO;
%>




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_userOrganogramId'>")%>
			
	
	<div class="form-inline" id = 'userOrganogramId_div_<%=i%>'>
		<input type='text' class='form-control'  name='userOrganogramId' id = 'userOrganogramId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogram_historyDTO.userOrganogramId + "'"):("'" + "0" + "'")%>   />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_username'>")%>
			
	
	<div class="form-inline" id = 'username_div_<%=i%>'>
		<input type='text' class='form-control'  name='username' id = 'username_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogram_historyDTO.username + "'"):("'" + " " + "'")%> required="required"  pattern="^[A-Za-z0-9]{5,}" title="username must contain at least 5 letters"
  />					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_accessDate'>")%>
			
	
	<div class="form-inline" id = 'accessDate_div_<%=i%>'>
		<input type='date' class='form-control'  name='accessDate_Date_<%=i%>' id = 'accessDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_accessDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_accessDate = format_accessDate.format(new Date(user_organogram_historyDTO.accessDate));
	out.println("'" + formatted_accessDate + "'");
}
else
{
	out.println("'" + datestr + "'");
}
%>
  >
		<input type='hidden' class='form-control'  name='accessDate' id = 'accessDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogram_historyDTO.accessDate + "'"):("'" + "0" + "'")%> >
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + user_organogram_historyDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=user_organogram_historyDTO.lastModificationTime%>'/>
		
												
<%=("</td>")%>
					
		