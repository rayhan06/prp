<%@page pageEncoding="UTF-8" %>

<%@page import="user_organogram_history.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_LANGUAGE, loginDTO);

User_organogram_historyDTO row = (User_organogram_historyDTO)request.getAttribute("user_organogram_historyDTO");
if(row == null)
{
	row = new User_organogram_historyDTO();
	
}
System.out.println("row = " + row);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
String navigator2 = SessionConstants.NAV_USER_ORGANOGRAM_HISTORY;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

User_organogram_historyDAO user_organogram_historyDAO = (User_organogram_historyDAO)request.getAttribute("user_organogram_historyDAO");
TempTableDTO tempTableDTO = null;
if(!isPermanentTable)
{
	tempTableDTO = user_organogram_historyDAO.getTempTableDTOFromTableById(tableName, row.iD);
}



											
		
											
											out.println("<td id = '" + i + "_userOrganogramId'>");
											value = row.userOrganogramId + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_username'>");
											value = row.username + "";
														
											out.println(value);
				
			
											out.println("</td>");
		
											
											out.println("<td id = '" + i + "_accessDate'>");
											value = row.accessDate + "";
											SimpleDateFormat format_accessDate = new SimpleDateFormat("dd-MM-yyyy");
											String formatted_accessDate = format_accessDate.format(new Date(Long.parseLong(value)));
											out.println(formatted_accessDate);
				
			
											out.println("</td>");
		
											
		
											
		
	

	
											out.println("<td id = '" + i + "_Edit'>");
											if(isPermanentTable || (!isPermanentTable && userDTO.approvalRole == SessionConstants.VALIDATOR && userDTO.approvalOrder == tempTableDTO.approval_order))
											{
																							
	
												out.println("<a href='User_organogram_historyServlet?actionType=getEditPage&ID=" +row.iD + "'>" + LM.getText(LC.USER_ORGANOGRAM_HISTORY_SEARCH_USER_ORGANOGRAM_HISTORY_EDIT_BUTTON, loginDTO) + "</a>");
																						
											}
											out.println("</td>");
											if(!isPermanentTable && userDTO.approvalOrder > -1)
											{
												String Successmessage = "";
												if(userDTO.approvalOrder < userDTO.maxApprovalOrder)
												{
													Successmessage = successMessageForwarded;
												}
												else
												{
													Successmessage= successMessageApproved;
												}
												out.println("<td id = 'tdapprove_" + row.iD + "'>");
												if(userDTO.approvalOrder == tempTableDTO.approval_order)
												{
													String onclickFunc = "\"approve('" + row.iD + "' , '" + Successmessage + "' , " + i + ", 'User_organogram_historyServlet')\"";	
													out.println("<a id = 'approve_" + row.iD + "' onclick=" + onclickFunc + "><%=LM.getText(LC.HM_APPROVE, loginDTO)%></a>");
												}
												else if(userDTO.approvalOrder > tempTableDTO.approval_order)
												{
													out.println("You cannot approve it yet");
												}
												else
												{
													if(userDTO.approvalOrder < userDTO.maxApprovalOrder)
													{
														out.println(Successmessage);
													}
													
												}
												out.println("</td>");
												
												
												out.println("<td id = 'tdoperation_" + row.iD + "'>");
												out.println(SessionConstants.operation_names[tempTableDTO.operation_type]);
												out.println("</td>");
												
												out.println("<td id = 'original_" + row.iD + "'>");
												if(tempTableDTO.operation_type == SessionConstants.UPDATE)
												{
													String onclickFunc = "\"getOriginal(" + i + ", " + row.iD + " , " + tempTableDTO.permanent_table_id + ", " + " 'User_organogram_historyServlet')\"";
													out.println("<a id = '" + i + "_getOriginal' onclick=" + onclickFunc + ">View Original</a>");
													out.println("<input type='hidden' id='" + i + "_original_status' value='0'/>");
												}
												out.println("</td>");
											}		
											
											
											out.println("<td id='" + i + "_checkbox'>");
											if(isPermanentTable || (!isPermanentTable && userDTO.approvalOrder == tempTableDTO.approval_order))
											{
												out.println("<div class='checker'>");
												out.println("<span id='chkEdit' ><input type='checkbox' name='ID' value='" + row.iD + "'/></span>");
												out.println("</div");
											}
											out.println("</td>");%>

