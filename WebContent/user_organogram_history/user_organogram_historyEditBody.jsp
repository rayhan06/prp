
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="user_organogram_history.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%
User_organogram_historyDTO user_organogram_historyDTO;
user_organogram_historyDTO = (User_organogram_historyDTO)request.getAttribute("user_organogram_historyDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(user_organogram_historyDTO == null)
{
	user_organogram_historyDTO = new User_organogram_historyDTO();
	
}
System.out.println("user_organogram_historyDTO = " + user_organogram_historyDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_USER_ORGANOGRAM_HISTORY_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.USER_ORGANOGRAM_HISTORY_ADD_USER_ORGANOGRAM_HISTORY_ADD_FORMNAME, loginDTO);
}

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
User_organogram_historyDTO row = user_organogram_historyDTO;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="User_organogram_historyServlet?actionType=<%=actionName%>&identity=<%=ID%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	














<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=ID%>'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_USERORGANOGRAMID, loginDTO)):(LM.getText(LC.USER_ORGANOGRAM_HISTORY_ADD_USERORGANOGRAMID, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'userOrganogramId_div_<%=i%>'>	
		<input type='text' class='form-control'  name='userOrganogramId' id = 'userOrganogramId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogram_historyDTO.userOrganogramId + "'"):("'" + "0" + "'")%>   />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_USERNAME, loginDTO)):(LM.getText(LC.USER_ORGANOGRAM_HISTORY_ADD_USERNAME, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'username_div_<%=i%>'>	
		<input type='text' class='form-control'  name='username' id = 'username_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogram_historyDTO.username + "'"):("'" + " " + "'")%> required="required"  pattern="^[A-Za-z0-9]{5,}" title="username must contain at least 5 letters"
  />					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=(actionName.equals("edit"))?(LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_ACCESSDATE, loginDTO)):(LM.getText(LC.USER_ORGANOGRAM_HISTORY_ADD_ACCESSDATE, loginDTO))%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'accessDate_div_<%=i%>'>	
		<input type='date' class='form-control'  name='accessDate_Date_<%=i%>' id = 'accessDate_date_Date_<%=i%>' value=<%
if(actionName.equals("edit"))
{
	SimpleDateFormat format_accessDate = new SimpleDateFormat("dd-MM-yyyy");
	String formatted_accessDate = format_accessDate.format(new Date(user_organogram_historyDTO.accessDate));
	out.println("'" + formatted_accessDate + "'");
}
else
{
	out.println("'" + datestr + "'");
}
%>
  >
		<input type='hidden' class='form-control'  name='accessDate' id = 'accessDate_date_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogram_historyDTO.accessDate + "'"):("'" + "0" + "'")%> >
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + user_organogram_historyDTO.isDeleted + "'"):("'" + "false" + "'")%>/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + user_organogram_historyDTO.lastModificationTime + "'"):("'" + "0" + "'")%>/>
												
					
	







				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_USER_ORGANOGRAM_HISTORY_CANCEL_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.USER_ORGANOGRAM_HISTORY_ADD_USER_ORGANOGRAM_HISTORY_CANCEL_BUTTON, loginDTO));
					}
					
					%>
					</a>
					<button class="btn btn-success" type="submit">
					<%
					if(actionName.equals("edit"))
					{
						out.print(LM.getText(LC.USER_ORGANOGRAM_HISTORY_EDIT_USER_ORGANOGRAM_HISTORY_SUBMIT_BUTTON, loginDTO));
					}
					else
					{
						out.print(LM.getText(LC.USER_ORGANOGRAM_HISTORY_ADD_USER_ORGANOGRAM_HISTORY_SUBMIT_BUTTON, loginDTO));
					}
					%>
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	console.log("found date = " + document.getElementById('accessDate_date_Date_' + row).value);
	document.getElementById('accessDate_date_' + row).value = new Date(document.getElementById('accessDate_date_Date_' + row).value).getTime();
	return true;
}

function PostprocessAfterSubmiting(row)
{
}


function init(row)
{
}var row = 0;
bkLib.onDomLoaded(function() 
{	
});
	
window.onload =function ()
{
	init(row);
}





</script>






