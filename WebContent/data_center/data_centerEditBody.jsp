<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="data_center.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Data_centerDTO data_centerDTO = new Data_centerDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	data_centerDTO = Data_centerDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = data_centerDTO;
String tableName = "data_center";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_ADD_FORMNAME, loginDTO);
String servletName = "Data_centerServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Data_centerServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row mb-4">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-11">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=data_centerDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_DATACENTERTYPECAT, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='dataCenterTypeCat' id = 'dataCenterTypeCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("data_center_type", Language, data_centerDTO.dataCenterTypeCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_CLUSTERNAMECAT, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='clusterNameCat' id = 'clusterNameCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("cluster_name", Language, data_centerDTO.clusterNameCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_SERVERNAME, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='serverName' id = 'serverName_text_<%=i%>' value='<%=data_centerDTO.serverName%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_SERVERIP, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='serverIp' id = 'serverIp_text_<%=i%>' value='<%=data_centerDTO.serverIp%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_VMNAME, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='vmName' id = 'vmName_text_<%=i%>' value='<%=data_centerDTO.vmName%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_SERVERDETAILS, loginDTO)%>															</label>
                                                            <div class="col-8">
																<textaea class='form-control'  name='serverDetails' id = 'serverDetails_text_<%=i%>'  tag='pb_html'><%=data_centerDTO.serverDetails%></textaea>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_HOSTNAME, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='hostName' id = 'hostName_text_<%=i%>' value='<%=data_centerDTO.hostName%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_OSVERSIONCAT, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='osVersionCat' id = 'osVersionCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("operating_system_version", Language, data_centerDTO.osVersionCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_CPU, loginDTO)%>															</label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(data_centerDTO.cpu != -1)
																	{
																	value = data_centerDTO.cpu + "";
																	}
																%>		
																<input type='number' class='form-control'  name='cpu' id = 'cpu_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.ASSET_MODEL_ADD_PROCESSOR, loginDTO)%>															</label>
                                                            <div class="col-4">
																<select class='form-control'  name='processorGenCat' id = 'processorGenCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("processor_gen", Language, data_centerDTO.processorGenCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                     														
                                                            <div class="col-4">
																<select class='form-control'  name='processorCat' id = 'processorCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("processor", Language, data_centerDTO.processorCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
																						
																					
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_DATASTORECAT, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='dataStoreCat' id = 'dataStoreCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("data_store", Language, data_centerDTO.dataStoreCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_NETWORKADAPTERCAT, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='networkAdapterCat' id = 'networkAdapterCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("network_adapter", Language, data_centerDTO.networkAdapterCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_NATUSERIP, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='natUserIp' id = 'natUserIp_text_<%=i%>' value='<%=data_centerDTO.natUserIp%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_REALIP, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='realIp' id = 'realIp_text_<%=i%>' value='<%=data_centerDTO.realIp%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.DATA_CENTER_ADD_ACTIVITY, loginDTO)%>															</label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='activity' id = 'activity_text_<%=i%>' value='<%=data_centerDTO.activity%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='assetModelId' id = 'assetModelId_hidden_<%=i%>' value='<%=data_centerDTO.assetModelId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='assetAssigneeId' id = 'assetAssigneeId_hidden_<%=i%>' value='<%=data_centerDTO.assetAssigneeId%>' tag='pb_html'/>
					
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_RAM, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_RAM_RAMCAT, loginDTO)%></th>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_RAM_RAMSIZECAT, loginDTO)%></th>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_RAM_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-DataCenterRam">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(DataCenterRamDTO dataCenterRamDTO: data_centerDTO.dataCenterRamDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "DataCenterRam_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterRam.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=dataCenterRamDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterRam.dataCenterId' id = 'dataCenterId_hidden_<%=childTableStartingID%>' value='<%=dataCenterRamDTO.dataCenterId%>' tag='pb_html'/>
									</td>
									<td>										





																<select class='form-control'  name='dataCenterRam.ramCat' id = 'ramCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("ram", Language, dataCenterRamDTO.ramCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>										





																<select class='form-control'  name='dataCenterRam.ramSizeCat' id = 'ramSizeCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("ram_size", Language, dataCenterRamDTO.ramSizeCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-DataCenterRam"
											name="add-moreDataCenterRam"
											type="button"
											onclick="childAdded(event, 'DataCenterRam')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-DataCenterRam"
											name="removeDataCenterRam"
											type="button"
											onclick="childRemoved(event, 'DataCenterRam')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%DataCenterRamDTO dataCenterRamDTO = new DataCenterRamDTO();%>
					
							<template id="template-DataCenterRam" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterRam.iD' id = 'iD_hidden_' value='<%=dataCenterRamDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterRam.dataCenterId' id = 'dataCenterId_hidden_' value='<%=dataCenterRamDTO.dataCenterId%>' tag='pb_html'/>
									</td>
									<td>





																<select class='form-control'  name='dataCenterRam.ramCat' id = 'ramCat_category_'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("ram", Language, dataCenterRamDTO.ramCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>





																<select class='form-control'  name='dataCenterRam.ramSizeCat' id = 'ramSizeCat_category_'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("ram_size", Language, dataCenterRamDTO.ramSizeCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_HARD_DISK, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_HARD_DISK_HDCAT, loginDTO)%></th>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_HARD_DISK_HDSIZECAT, loginDTO)%></th>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_HARD_DISK_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-DataCenterHardDisk">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(DataCenterHardDiskDTO dataCenterHardDiskDTO: data_centerDTO.dataCenterHardDiskDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "DataCenterHardDisk_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterHardDisk.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=dataCenterHardDiskDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterHardDisk.dataCenterId' id = 'dataCenterId_hidden_<%=childTableStartingID%>' value='<%=dataCenterHardDiskDTO.dataCenterId%>' tag='pb_html'/>
									</td>
									<td>										





																<select class='form-control'  name='dataCenterHardDisk.hdCat' id = 'hdCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("hd", Language, dataCenterHardDiskDTO.hdCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>										





																<select class='form-control'  name='dataCenterHardDisk.hdSizeCat' id = 'hdSizeCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("hd_size", Language, dataCenterHardDiskDTO.hdSizeCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-DataCenterHardDisk"
											name="add-moreDataCenterHardDisk"
											type="button"
											onclick="childAdded(event, 'DataCenterHardDisk')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-DataCenterHardDisk"
											name="removeDataCenterHardDisk"
											type="button"
											onclick="childRemoved(event, 'DataCenterHardDisk')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%DataCenterHardDiskDTO dataCenterHardDiskDTO = new DataCenterHardDiskDTO();%>
					
							<template id="template-DataCenterHardDisk" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterHardDisk.iD' id = 'iD_hidden_' value='<%=dataCenterHardDiskDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterHardDisk.dataCenterId' id = 'dataCenterId_hidden_' value='<%=dataCenterHardDiskDTO.dataCenterId%>' tag='pb_html'/>
									</td>
									<td>





																<select class='form-control'  name='dataCenterHardDisk.hdCat' id = 'hdCat_category_'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("hd", Language, dataCenterHardDiskDTO.hdCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>





																<select class='form-control'  name='dataCenterHardDisk.hdSizeCat' id = 'hdSizeCat_category_'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("hd_size", Language, dataCenterHardDiskDTO.hdSizeCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>
               <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
									<tr>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER_USERTYPECAT, loginDTO)%></th>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER_USERNAME, loginDTO)%></th>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER_PASSWORD, loginDTO)%></th>
										<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
							<tbody id="field-DataCenterUser">
						
						
								<%
									if(actionName.equals("ajax_edit")){
										int index = -1;
										
										
										for(DataCenterUserDTO dataCenterUserDTO: data_centerDTO.dataCenterUserDTOList)
										{
											index++;
											
											System.out.println("index index = "+index);

								%>	
							
								<tr id = "DataCenterUser_<%=index + 1%>">
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterUser.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=dataCenterUserDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterUser.dataCenterId' id = 'dataCenterId_hidden_<%=childTableStartingID%>' value='<%=dataCenterUserDTO.dataCenterId%>' tag='pb_html'/>
									</td>
									<td>										





																<select class='form-control'  name='dataCenterUser.userTypeCat' id = 'userTypeCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("user_type", Language, dataCenterUserDTO.userTypeCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>										





																<input type='text' class='form-control'  name='dataCenterUser.userName' id = 'userName_text_<%=childTableStartingID%>' value='<%=dataCenterUserDTO.userName%>'  
																required="required"     tag='pb_html'/>					
									</td>
									<td>										





																<input type='text' class='form-control'  name='dataCenterUser.password' id = 'password_password_<%=childTableStartingID%>' value='<%=dataCenterUserDTO.password%>'   tag='pb_html'/>					
									</td>
									<td>
										<span id='chkEdit'>
											<input type='checkbox' name='checkbox' value='' deletecb='true'
												   class="form-control-sm"/>
										</span>
									</td>
								</tr>								
								<%	
											childTableStartingID ++;
										}
									}
								%>						
						
								</tbody>
							</table>
						</div>
						<div class="form-group">
								<div class="col-xs-9 text-right">
									<button
											id="add-more-DataCenterUser"
											name="add-moreDataCenterUser"
											type="button"
											onclick="childAdded(event, 'DataCenterUser')"
											class="btn btn-sm text-white add-btn shadow">
										<i class="fa fa-plus"></i>
										<%=LM.getText(LC.HM_ADD, loginDTO)%>
									</button>
									<button
											id="remove-DataCenterUser"
											name="removeDataCenterUser"
											type="button"
											onclick="childRemoved(event, 'DataCenterUser')"
											class="btn btn-sm remove-btn shadow ml-2 pl-4">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
					
							<%DataCenterUserDTO dataCenterUserDTO = new DataCenterUserDTO();%>
					
							<template id="template-DataCenterUser" >						
								<tr>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterUser.iD' id = 'iD_hidden_' value='<%=dataCenterUserDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">





														<input type='hidden' class='form-control'  name='dataCenterUser.dataCenterId' id = 'dataCenterId_hidden_' value='<%=dataCenterUserDTO.dataCenterId%>' tag='pb_html'/>
									</td>
									<td>





																<select class='form-control'  name='dataCenterUser.userTypeCat' id = 'userTypeCat_category_'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("user_type", Language, dataCenterUserDTO.userTypeCat);
																%>
																<%=Options%>
																</select>
	
									</td>
									<td>





																<input type='text' class='form-control'  name='dataCenterUser.userName'
																 id = 'userName_text_' value='<%=dataCenterUserDTO.userName%>'  
																 required="required"  
																  
																 tag='pb_html'/>					
									</td>
									<td>





																<input type='text' class='form-control'  name='dataCenterUser.password' id = 'password_password_' value='<%=dataCenterUserDTO.password%>'   tag='pb_html'/>					
									</td>
									<td>
											<span id='chkEdit'>
												<input type='checkbox' name='checkbox' value='' deletecb='true'
													   class="form-control-sm"/>
											</span>
									</td>
								</tr>								
						
							</template>
                        </div>
                    </div>
                <div class="form-actions text-right mb-2 mt-4">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                        <%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>				

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Data_centerServlet");	
}

function init(row)
{


	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;

function processRowsWhileAdding(childName)
{
	if(childName == "DataCenterRam")
	{			
	}
	if(childName == "DataCenterHardDisk")
	{			
	}
	if(childName == "DataCenterUser")
	{			
	}
}

</script>






