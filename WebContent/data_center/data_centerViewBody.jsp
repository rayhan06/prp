

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="data_center.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Data_centerServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Data_centerDTO data_centerDTO = Data_centerDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = data_centerDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_ADD_FORMNAME, loginDTO)%>
                </h3>
                <div class="ml-auto mr-3">
				    <button type="button" class="btn" id='printer2'
				            onclick="location.href='Data_centerServlet?actionType=search'">
				        <i class="fa fa-search fa-2x" style="color: gray" aria-hidden="true"></i>
				    </button>			    			   
				</div>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_DATACENTERTYPECAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "data_center_type", data_centerDTO.dataCenterTypeCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_CLUSTERNAMECAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "cluster_name", data_centerDTO.clusterNameCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_SERVERNAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=data_centerDTO.serverName%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_SERVERIP, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=data_centerDTO.serverIp%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_VMNAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=data_centerDTO.vmName%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_SERVERDETAILS, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=data_centerDTO.serverDetails%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_HOSTNAME, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=data_centerDTO.hostName%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_OSVERSIONCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "operating_system_version", data_centerDTO.osVersionCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_CPU, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=Utils.getDigits(data_centerDTO.cpu, Language)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_PROCESSORGENCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "processor_gen", data_centerDTO.processorGenCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_PROCESSORCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "processor", data_centerDTO.processorCat)%>
                                    </div>
                                </div>
			
								
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_DATASTORECAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "data_store", data_centerDTO.dataStoreCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_NETWORKADAPTERCAT, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=CatRepository.getInstance().getText(Language, "network_adapter", data_centerDTO.networkAdapterCat)%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_NATUSERIP, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=data_centerDTO.natUserIp%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_REALIP, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=data_centerDTO.realIp%>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.DATA_CENTER_ADD_ACTIVITY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%=data_centerDTO.activity%>
                                    </div>
                                </div>
					
							</div>
                        </div>
                    </div>
                </div>
            </div>			

             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_RAM, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_RAM_RAMCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_RAM_RAMSIZECAT, loginDTO)%></th>
							</tr>
							<%
                        	DataCenterRamDAO dataCenterRamDAO = DataCenterRamDAO.getInstance();
                         	List<DataCenterRamDTO> dataCenterRamDTOs = (List<DataCenterRamDTO>)dataCenterRamDAO.getDTOsByParent("data_center_id", data_centerDTO.iD);
                         	
                         	for(DataCenterRamDTO dataCenterRamDTO: dataCenterRamDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=CatRepository.getInstance().getText(Language, "ram", dataCenterRamDTO.ramCat)%>
										</td>
										<td>
											<%=CatRepository.getInstance().getText(Language, "ram_size", dataCenterRamDTO.ramSizeCat)%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_HARD_DISK, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_HARD_DISK_HDCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_HARD_DISK_HDSIZECAT, loginDTO)%></th>
							</tr>
							<%
                        	DataCenterHardDiskDAO dataCenterHardDiskDAO = DataCenterHardDiskDAO.getInstance();
                         	List<DataCenterHardDiskDTO> dataCenterHardDiskDTOs = (List<DataCenterHardDiskDTO>)dataCenterHardDiskDAO.getDTOsByParent("data_center_id", data_centerDTO.iD);
                         	
                         	for(DataCenterHardDiskDTO dataCenterHardDiskDTO: dataCenterHardDiskDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=CatRepository.getInstance().getText(Language, "hd", dataCenterHardDiskDTO.hdCat)%>
										</td>
										<td>
											<%=CatRepository.getInstance().getText(Language, "hd_size", dataCenterHardDiskDTO.hdSizeCat)%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
             <div class="mt-5">
                <div class=" div_border attachement-div">
                        <h5><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER_USERTYPECAT, loginDTO)%></th>
								<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER_USERNAME, loginDTO)%></th>
								<th><%=LM.getText(LC.DATA_CENTER_ADD_DATA_CENTER_USER_PASSWORD, loginDTO)%></th>
							</tr>
							<%
                        	DataCenterUserDAO dataCenterUserDAO = DataCenterUserDAO.getInstance();
                         	List<DataCenterUserDTO> dataCenterUserDTOs = (List<DataCenterUserDTO>)dataCenterUserDAO.getDTOsByParent("data_center_id", data_centerDTO.iD);
                         	
                         	for(DataCenterUserDTO dataCenterUserDTO: dataCenterUserDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%=CatRepository.getInstance().getText(Language, "user_type", dataCenterUserDTO.userTypeCat)%>
										</td>
										<td>
											<%=dataCenterUserDTO.userName%>
										</td>
										<td>
											<%=dataCenterUserDTO.password%>
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
        </div>
    </div>
</div>