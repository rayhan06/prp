<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDAO" %>
<%@ page import="util.StringUtils" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String formTitle = LM.getText(LC.BUDGET_CODE_SELECTION, loginDTO);

    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String context = request.getContextPath() + "/";

    String currentYear = BudgetUtils.getEconomicYearForCodeSelection(System.currentTimeMillis());
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(currentYear);
    boolean isBudgetSubmitted = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
%>

<style>
    /* class added dynamically: don't remove if shows unused */
    .fly-in-from-down {
        animation: flyFromDown 1s ease-out;
    }

    @keyframes flyFromDown {
        0% {
            transform: translateY(200%);
        }
        100% {
            transform: translateY(0%);
        }
    }

    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .template-row {
        display: none;
    }

    .row-data-code {
        width: 20%;
    }

    .row-data-name {
        width: 60%;
    }
</style>

<!-- begin:: Subheader -->
<div class="mt-4">
    <div class="kt-subheader__main ml-4">
        <h2 class="kt-subheader__title" style="color: #00a1d4;">
            <i class="far fa-address-card"></i>
            <%=formTitle%>
            (<%=LM.getText(LC.BUDGET_ECONOMIC_YEAR, loginDTO)%>
            <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, currentYear)%>)
        </h2>
    </div>

    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="row mt-3 mx-0">
                <div class="col-md-6 col-xl-3 form-group">
                    <h5 class="">
                        <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                    </h5>
                    <select
                            id="budgetInstitutionalGroup"
                            class='form-control rounded shadow-sm w-100'
                            onchange="institutionalGroupChanged(this);"
                    >
                        <%=Budget_institutional_groupRepository.getInstance().buildOptions(Language, 0L, false)%>
                    </select>
                </div>
                <div class="col-md-6 col-xl-3 form-group">
                    <h5 class="">
                        <%=LM.getText(LC.BUDGET_BUDGET_TYPE, loginDTO)%>
                    </h5>
                    <select id="budgetCat"
                            class='form-control rounded shadow-sm w-100'
                            onchange="budgetCatChanged(this);"
                    >
                        <%--Dynamically Added with AJAX--%>
                    </select>
                </div>
                <div class="col-md-6 col-xl-3 form-group">
                    <h5 class="">
                        <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                    </h5>
                    <select
                            id="budgetOffice"
                            class='form-control rounded shadow-sm w-100'
                            onchange="budgetOfficeChanged(this);">
                        <%--Dynamically Added with AJAX--%>
                    </select>
                </div>
                <div class="col-md-6 col-xl-3 form-group">
                    <h5 class="">
                        <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                    </h5>
                    <select
                            id="budgetOperation"
                            class='form-control rounded shadow-sm w-100'
                            onchange="budgetOperationChanged(this);"
                    >
                        <%--Dynamically Added with AJAX--%>
                    </select>
                </div>
            </div>
            <%if (isBudgetSubmitted) {%>
            <div class="row">
                <div class="col-12 mt-5 text-center">
                    <button
                            class="btn btn-sm shadow text-white btn-border-radius"
                            type="button"
                            style="background: #4db2d1;"
                            onclick="location.href='BudgetCodeSelectionServlet?actionType=budgetCodeSelectionPreview'">
                        <%=LM.getText(LC.BUDGET_CODE_SELECTION_PREVIEW, loginDTO)%>
                    </button>
                </div>
            </div>
            <%}%>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content pt-0" id="kt_content" style="display: none">
    <div class="kt-portlet">
        <div class="kt-portlet__body page-bg">
            <div style="background-color: #ffffff" class="p-5">
                <div class="row">
                    <div class="col-12 row">
                        <div class="offset-3 col-6 text-center">
                            <img width="20%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                    <div class="col-12 row mt-5 mx-0 py-3 rounded" style="border: 1px solid #00ACD8;">
                        <div class="col-md-9 text-left">
                            <h5 class="text-left mt-3 top-section-font" id="budgetCatHeading">
                                <%--Dynamically Added with AJAX--%>
                            </h5>
                            <h5 class="text-left mt-3 top-section-font" id="operationalCodeHeading">
                                <%--Dynamically Added with AJAX--%>
                            </h5>
                        </div>
                        <div class="col-md-3">
                            <h5 class="text-md-right mt-3 top-section-font" id="economicYearHeading">
                                <%--Dynamically Added with AJAX--%>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="table-responsive">
                        <table class="table" id="budget-view-table">
                            <thead>
                            <tr>
                                <th class="row-data-code">
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </th>
                                <th class="row-data-name">
                                    <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                </th>
                                <%if (!isBudgetSubmitted) {%>
                                <th></th>
                                <%}%>
                            </tr>
                            </thead>

                            <tbody></tbody>

                            <%--Template Row-> to be cloned to add new row.
                                CONVENTIONS:
                                    * one tr with template-row class
                                    * td with class -> row-data-filedNameInJson
                            --%>
                            <tr class="template-row">
                                <td class="row-data-code"></td>
                                <td class="row-data-name"></td>
                                <%if (!isBudgetSubmitted) {%>
                                <td class="row-data-add-btn">
                                    <button class='btn btn-sm cancel-btn text-white shadow pl-4'
                                            style="padding-right: 14px;" type="button"
                                            onclick="unSelectRow(this);">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                                <%}%>
                            </tr>
                        </table>
                    </div>
                    <%if (!isBudgetSubmitted) {%>
                    <div>
                        <button class="btn btn-sm btn-success shadow btn-border-radius"
                                id="add-economic-code-btn"
                                type="button">
                            <%=LM.getText(LC.BUDGET_ADD_ECONOMIC_CODE, loginDTO)%>
                        </button>
                    </div>
                    <%}%>
                </div>
                <div class="row my-5">
                    <div class="col-12">
                        <form class="form-horizontal text-right"
                              action="BudgetCodeSelectionServlet?actionType=saveEconomicSubCodes"
                              id="save-economic-code-form" method="POST"
                              enctype="multipart/form-data">

                            <input type="hidden" name="budgetMappingId" id="budgetMappingId-input">
                            <input type="hidden" name="economicSubCodes" id="economicSubCodes-input">
                            <%if (!isBudgetSubmitted) {%>
                            <button class="btn btn-sm shadow text-white btn-border-radius" type="button"
                                    style="background: #4db2d1;"
                                    onclick="location.href='BudgetCodeSelectionServlet?actionType=budgetCodeSelectionPreview'">
                                <%=LM.getText(LC.BUDGET_CODE_SELECTION_PREVIEW, loginDTO)%>
                            </button>
                            <button id="cancel-btn" class="btn btn-sm shadow text-white btn-border-radius cancel-btn mx-2" type="button">
                                <%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn btn-sm shadow text-white submit-btn" type="submit">
                                <%=LM.getText(LC.PERFORMANCE_LOG_REPORT_SAVE, loginDTO)%>
                            </button>
                            <%}%>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="codeSelectModal.jsp"/>

<script>
    function resetPage() {
        document.getElementById('budgetInstitutionalGroup').value = '';
        clearSelects(0);
        clearBudgetPage();
    }

    $(() => {
        $('#cancel-btn').click(resetPage);

        $('#save-economic-code-form').submit(async (event) => {
            event.preventDefault();

            const economicSubCodes = Array.from(economicSubCodeMap.values());
            $('#economicSubCodes-input').val(JSON.stringify(economicSubCodes));

            const formElement = event.target;
            const formData = new FormData(formElement);
            const url = formElement.action;
            const searchParam = new URLSearchParams(formData);

            try {
                const response = await fetch(url, {
                    method: 'post',
                    body: searchParam
                });
                const json = await response.json();
                if (!json.success) throw new Error(json);

                $('#toast_message').css('background-color', '#04c73c');
                showToast(
                    '<%=LM.getText(LC.BUDGET_CODE_SELECTION_ECONOMIC_CODE_SAVED,"Bangla")%>',
                    '<%=LM.getText(LC.BUDGET_CODE_SELECTION_ECONOMIC_CODE_SAVED,"English")%>'
                );
            } catch (error) {
                console.error(error);
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(
                    '<%=LM.getText(LC.BUDGET_CODE_SELECTION_ECONOMIC_CODE_SAVE_FAILED,"Bangla")%>',
                    '<%=LM.getText(LC.BUDGET_CODE_SELECTION_ECONOMIC_CODE_SAVE_FAILED,"English")%>'
                );
            }
        });
    });

    $('#add-economic-code-btn').on('click', function () {
        $('#economic-code-modal').modal();
    });

    function clearSelects(startIndex) {
        const selectIds = ['budgetCat', 'budgetOffice', 'budgetOperation'];
        for (let i = startIndex; i < selectIds.length; i++) {
            const selectElement = document.getElementById(selectIds[i]);
            selectElement.innerHTML = '';
        }
    }

    async function institutionalGroupChanged(selectElement) {
        $('#kt_content').hide();
        const selectedInstitutionalGroupId = selectElement.value;
        clearSelects(0);
        if (selectedInstitutionalGroupId === '') return;

        const url = 'Budget_mappingServlet?actionType=getBudgetCatList&budget_instituitional_group_id='
            + selectedInstitutionalGroupId;
        const response = await fetch(url);
        document.getElementById('budgetCat').innerHTML = await response.text();
    }

    async function budgetCatChanged(selectElement) {
        $('#kt_content').hide();
        const selectedBudgetCat = selectElement.value;
        clearSelects(1);
        if (selectedBudgetCat === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&budget_instituitional_group_id='
            + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat;
        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
    }

    async function budgetOfficeChanged(selectElement) {
        $('#kt_content').hide();
        const selectedBudgetOffice = selectElement.value;
        clearSelects(2);
        if (selectedBudgetOffice === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;
        const selectedBudgetCat = document.getElementById('budgetCat').value;

        const url = 'Budget_mappingServlet?actionType=getOperationCodeList&budget_instituitional_group_id='
            + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat
            + '&budget_office_id=' + selectedBudgetOffice;
        const response = await fetch(url);
        document.getElementById('budgetOperation').innerHTML = await response.text();
    }

    async function budgetOperationChanged(selectElement) {
        const selectedBudgetOperationId = selectElement.value;
        if (selectedBudgetOperationId === '') {
            $('#kt_content').hide();
            return;
        }

        const url = 'Budget_mappingServlet?actionType=getBudgetOperationModel&id=' + selectedBudgetOperationId
                    + "&economicYear=" + "<%=currentYear%>";

        const response = await fetch(url);
        const budgetOperationJson = await response.json();
        loadBudgetPage(budgetOperationJson);
        await buildEconomicGroup(selectedBudgetOperationId);
    }

    function clearBudgetPage() {
        const mainContent = $('#kt_content');
        mainContent.hide();
        mainContent.removeClass('fly-in-from-down');

        document.querySelector('#budget-view-table tbody').innerHTML = '';
        document.getElementById('budgetCatHeading').innerText = '';
        document.getElementById('operationalCodeHeading').innerText = '';
        document.getElementById('economicYearHeading').innerText = '';

        economicSubCodeMap.clear();
        document.getElementById('economicSubCodes-input').value = '';
        document.getElementById('budgetMappingId-input').value = '';
    }

    function loadBudgetPage(budgetOperationJson) {
        clearBudgetPage();

        document.getElementById('budgetCatHeading').innerText = budgetOperationJson.budgetCatName
            + ' - ' + budgetOperationJson.officeCode;
        document.getElementById('operationalCodeHeading').innerText = budgetOperationJson.description
            + ' - ' + budgetOperationJson.operationCode;
        document.getElementById('economicYearHeading').innerText = budgetOperationJson.economicYear;
        document.getElementById('budgetMappingId-input').value = budgetOperationJson.budgetMappingId;

        const economicSubCodeModels = budgetOperationJson.economicSubCodeModels;
        if (Array.isArray(economicSubCodeModels)) {
            economicSubCodeModels.forEach(economicSubCodeModel => {
                addDataInTable('budget-view-table', economicSubCodeModel, true);
                economicSubCodeMap.set(
                    Number(economicSubCodeModel.id),
                    economicSubCodeModel
                );
            });
            sortBudgetViewTable();
        }

        const mainContent = $('#kt_content');
        mainContent.show();
        mainContent.addClass('fly-in-from-down');
    }

    function getSortIndex(economicCodeId) {
        let code = economicCodeId.replaceAll(economicCodeIdPrefix, '');
        code = code.padEnd(7, '0'); // make all 7 digit number
        return Number(code);
    }

    function sortBudgetViewTable() {
        const tableBody = document.getElementById('budget-view-table').querySelector('tbody');

        const rowsObj = Array.from(tableBody.rows).map(row => {
            return {
                data: getSortIndex(row.id),
                rowElement: row
            };
        });

        rowsObj.sort((a, b) => {
            if (a.data === b.data) return 0;
            return a.data < b.data ? -1 : 1;
        });

        tableBody.innerHTML = '';
        rowsObj.forEach(rowObj => tableBody.append(rowObj.rowElement));
    }

    // modal on close event
    $('#economic-code-modal').on("hidden.bs.modal", sortBudgetViewTable);
</script>





