<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>

<%
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.CODE_SELECTION_SEARCH_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
    Long currentSelectionId = BudgetSelectionInfoRepository.getInstance().getRunningYearSelectionId();
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.CODE_SELECTION_SEARCH_ECONOMIC_YEAR, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='budget_selection_info_id' id='budget_selection_info_id'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=BudgetSelectionInfoRepository.getInstance().buildEconomicYears(Language, currentSelectionId)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='budget_institional_group_id' id='budget_institional_group_id'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=Budget_institutional_groupRepository.getInstance().buildOptions(Language,null,true)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.BUDGET_BUDGET_TYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='budget_cat' id='budget_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("budget",Language,null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='budget_office_id' id='budget_office_id'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=Budget_officeRepository.getInstance().buildOptions(Language,null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.CODE_SELECTION_SEARCH_BUDGET_OPEATION_CODE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='budget_operation_id' id='budget_operation_id'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=Budget_operationRepository.getInstance().buildOperationCodes(Language,null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit" class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%--<%@include file="../common/pagination_with_go2.jsp" %>--%>
<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>
<script type="text/javascript">
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    $(document).ready(()=>{
        select2SingleSelector('#budget_selection_info_id','<%=Language%>');
        select2SingleSelector('#budget_operation_id','<%=Language%>');
        select2SingleSelector('#budget_office_id','<%=Language%>');
        select2SingleSelector('#budget_institional_group_id','<%=Language%>');
        select2SingleSelector('#budget_cat','<%=Language%>');
    });
    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = '';
        params += '&budget_selection_info_id=' + document.getElementById('budget_selection_info_id').value;
        params += '&budget_operation_id=' + document.getElementById('budget_operation_id').value;
        params += '&budget_office_id=' + document.getElementById('budget_office_id').value;
        params += '&budget_institional_group_id=' + document.getElementById('budget_institional_group_id').value;
        params += '&budget_cat=' + document.getElementById('budget_cat').value;
        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })
        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        if(document.getElementById("budget_selection_info_id").value==""){
            var msg="";
            if(isLangEng){
                msg="Please Select an Economic Year";
            }else{
                msg="অর্থবছর নির্বাচন করুন"
            }
            toastr.error(msg);
        }else {
            dosubmit(params);
        }

    }

</script>