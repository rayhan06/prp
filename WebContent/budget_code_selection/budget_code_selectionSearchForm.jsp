<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="budget_code_selection.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.List" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="budget.BudgetDTO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String Language = LM.getText(LC.CODE_SELECTION_SEARCH_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String navigator2 = SessionConstants.NAV_BUDGET_CODE_SELECTION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<div class="table-responsive">
    <%
        List<BudgetDTO> budgetDTOS = (List<BudgetDTO>) session.getAttribute(SessionConstants.VIEW_BUDGET_CODE_SELECTION);
    %>
    <%@include file="budget_code_selectionSearchTable.jsp"%>

</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>