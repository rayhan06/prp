<%@ page import="budget.BudgetUtils" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDAO" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="budget.BudgetDTO" %>
<%@ page import="budget.BudgetDAO" %>
<%@ page import="budget_mapping.Budget_mappingDTO" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="budget_operation.BudgetOperationModel" %>
<%@ page import="economic_code.Economic_codeDTO" %>
<%@ page import="common.NameDTO" %>
<%@ page import="economic_group.EconomicGroupRepository" %>
<%@ page import="economic_code.Economic_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeDTO" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="java.util.*" %>
<%@ page import="util.StringUtils" %>

<%

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    String context = request.getContextPath() + "/";


    String currentYear = BudgetUtils.getEconomicYearForCodeSelection(System.currentTimeMillis());
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(currentYear);
    String noDataThisYearMessage = null;
    List<BudgetDTO> budgetDTOS = new ArrayList<>();
    List<Budget_mappingDTO> budgetMappingDTOS = new ArrayList<>();
    if (budgetSelectionInfoDTO != null) {
        budgetDTOS = BudgetDAO.getInstance().getDTOsBySelectionInfo(budgetSelectionInfoDTO.iD);
        budgetMappingDTOS = Budget_mappingRepository.getInstance().getBudgetMappingList();
    }else {
        noDataThisYearMessage = UtilCharacter.getDataByLanguage(
                Language,
                "চলমান ইকোনোমিক বছরের জন্যে কোনো ডাটা দেওয়া হয় নি।",
                "No Data entried for this economic year."
        );
    }

    boolean isBudgetSubmitted = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
%>

<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .row-data-code {
        width: 20%;
    }

    .row-data-name {
        width: 60%;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-auto mt-4">
    <button type="button" class="btn" id='printer' onclick="printDiv('kt_content')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body page-bg">
                <div style="background-color: #ffffff" class="p-5">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <img width="10%"
                                     src="<%=context%>assets/static/parliament_logo.png"
                                     alt="logo"
                                     class="logo-default"
                                />
                            </div>
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>

                    <%if(noDataThisYearMessage != null){%>
                        <div class="container text-center mt-5">
                            <h4 class="text-danger">
                                <%=noDataThisYearMessage%>
                            </h4>
                        </div>
                    <%}%>

                    <%
                        for (Budget_mappingDTO budgetMappingDTO : budgetMappingDTOS) {
                            List<BudgetDTO> runningBudgetDTOs = budgetDTOS.stream()
                                    .filter(i -> i.budgetMappingId == budgetMappingDTO.iD)
                                    .collect(Collectors.toList());
                            if (runningBudgetDTOs.size() == 0) continue;

                            BudgetOperationModel model = Budget_mappingRepository.getInstance()
                                    .getBudgetOperationModelWithoutSubCode(budgetMappingDTO.budgetOperationId, currentYear, Language);
                            if (model == null) {
                                model = new BudgetOperationModel();
                            }
                    %>
                    <div class="row mt-5 rounded mx-1" style="border: 1px solid #00ACD8;">
                        <div class="col-md-9 text-left p-3 px-4">
                            <h5 class="text-left mt-3 top-section-font" id="budgetCatHeading">
                                <%=model.budgetCatName + " - " + model.officeCode%>
                            </h5>
                            <h5 class="text-left mt-3 top-section-font" id="operationalCodeHeading">
                                <%=model.description + " - " + model.operationCode%>
                            </h5>
                        </div>
                        <div class="col-md-3 p-3 px-4">
                            <h5 class="text-md-right mt-3 top-section-font" id="economicYearHeading">
                                <%=model.economicYear%>
                            </h5>
                        </div>
                    </div>

                    <div class="mt-5 page-break">
                        <div class="table-responsive">
                            <table class="table" id="budget-view-table">
                                <thead>
                                <tr>
                                    <th class="row-data-code">
                                        <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                    </th>
                                    <th class="row-data-name">
                                        <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                <%
                                    Set<Long> distinctEconomicGroups = runningBudgetDTOs.stream()
                                            .map(i -> i.economicGroupId)
                                            .sorted()
                                            .collect(Collectors.toCollection(LinkedHashSet::new));
                                    for (long economicGroup : distinctEconomicGroups) {
                                        NameDTO groupDTO = EconomicGroupRepository.getInstance().getDTOByID(economicGroup);
                                        if (groupDTO == null) {
                                            groupDTO = new NameDTO();
                                        }
                                %>

                                <tr>
                                    <td style=" font-weight: bold" colspan="2">
                                        <%String groupID = StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(groupDTO.iD));%>
                                        <%=groupID + " - " + UtilCharacter.getDataByLanguage(Language, groupDTO.nameBn, groupDTO.nameEn)%>
                                    </td>
                                </tr>

                                <%
                                    List<BudgetDTO> budgetDTOListByEcoGroup = runningBudgetDTOs.stream()
                                            .filter(i -> i.economicGroupId == economicGroup)
                                            .collect(Collectors.toList());

                                    Set<Long> distinctEconomicCodes = budgetDTOListByEcoGroup.stream()
                                            .map(i -> i.economicCodeId)
                                            .sorted()
                                            .collect(Collectors.toCollection(LinkedHashSet::new));

                                    List<Economic_codeDTO> economic_codeDTOS = new ArrayList<>();

                                    for (long economicCode : distinctEconomicCodes) {

                                        Economic_codeDTO economicCodeDTO = Economic_codeRepository.getInstance()
                                                .getById(economicCode);
                                        if (economicCodeDTO != null) {
                                            economic_codeDTOS.add(economicCodeDTO);
                                        }
                                    }

                                    economic_codeDTOS = economic_codeDTOS.stream()
                                            .sorted(Comparator.comparingInt(i -> Integer.parseInt(i.code)))
                                            .collect(Collectors.toList());
                                    for (Economic_codeDTO economicCodeDTO : economic_codeDTOS) {
                                %>

                                <tr>
                                    <td style=" font-weight: bold" colspan="2">
                                        <%=economicCodeDTO.getFormattedCodeAndName(Language)%>
                                    </td>
                                </tr>

                                <%

                                    Set<Long> economicSubCodes = budgetDTOListByEcoGroup.stream()
                                            .filter(i -> i.economicCodeId == economicCodeDTO.iD)
                                            .map(i -> i.economicSubCodeId)
                                            .collect(Collectors.toSet());

                                    List<Economic_sub_codeDTO> economicSubCodeDTOS = new ArrayList<>();

                                    for (Long subCode : economicSubCodes) {
                                        Economic_sub_codeDTO economicSubCodeDTO = Economic_sub_codeRepository.
                                                getInstance().getDTOByID(subCode);
                                        if (economicSubCodeDTO != null) {
                                            economicSubCodeDTOS.add(economicSubCodeDTO);
                                        }
                                    }

                                    economicSubCodeDTOS = economicSubCodeDTOS.stream()
                                            .sorted(Comparator.comparingInt(i -> Integer.parseInt(i.code)))
                                            .collect(Collectors.toList());
                                    for (Economic_sub_codeDTO economicSubCodeDTO : economicSubCodeDTOS) {
                                %>
                                        <tr>
                                            <td class="row-data-code">
                                                <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, economicSubCodeDTO.code)%>
                                            </td>
                                            <td class="row-data-name">
                                                <%=UtilCharacter.getDataByLanguage(Language, economicSubCodeDTO.descriptionBn,
                                                        economicSubCodeDTO.descriptionEn)%>
                                            </td>
                                        </tr>
                                <%
                                            }
                                        }
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <%}%>

                    <%if (noDataThisYearMessage == null && !isBudgetSubmitted) {%>
                        <div id='button-div' class="row my-5">
                            <div class="col-12 text-right">
                                <button onclick="submit()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                        type="button">
                                    <%=UtilCharacter.getDataByLanguage(Language, "দাখিল", "Submit")%>
                                </button>
                            </div>
                        </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let lang = '<%=Language%>'.toLowerCase();

    function submit() {
        let errMsg1 = "Code Selection will be closed."
        let errMsg1Bn = "দাখিলের পর কোড বাছাইকরণ বন্ধ হয়ে যাবে";
        let errMsg2 = "Are you sure?";
        let errMsg2Bn = "আপনি কি নিশ্চিত?";
        messageDialog(valueByLanguage(lang, errMsg1Bn, errMsg1),
            valueByLanguage(lang, errMsg2Bn, errMsg2), 'warning', true, valueByLanguage(lang, "হ্যাঁ", "Yes")
            , valueByLanguage(lang, "না", "No"), finalSubmit, function () {}
        );
    }

    function finalSubmit() {
        let url = "BudgetCodeSelectionServlet?actionType=economicCodeFinalSubmission";
        $.ajax({
            type: "POST",
            url: url,
            success: function(data) {
                if(data && data === 'true'){
                    messageDialog(valueByLanguage(lang, "সফলভাবে দাখিল হয়েছে", "Successfully submitted." ),
                    '','success',false,valueByLanguage(lang, "ঠিক আছে", "Ok"));
                    $("#button-div").hide();
                }
            },
            error: function (req, err) {
                console.error(err);
            }
        });
    }

    function valueByLanguage(lang, bnValue, enValue){
        return lang === 'english' ? enValue : bnValue;
    }

    function printDiv(divName) {
        let button = $("#button-div");
        if (button.length) {
            button.hide();
        }
        let printContents = document.getElementById(divName).innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        if (button.length) button.show();
    }
</script>