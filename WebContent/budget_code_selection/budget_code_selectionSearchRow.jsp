<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeDTO" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_operation.Budget_operationRepository" %>
<%@ page import="util.StringUtils" %>
<%
    Economic_sub_codeDTO sub_codeDTO = Economic_sub_codeRepository.getInstance().getDTOByID(budgetDTO.economicSubCodeId);
%>
<td>
    <%=BudgetSelectionInfoRepository.getInstance().getEconomicYearById(Language, budgetDTO.budgetSelectionInfoId)%>
</td>


<td>
    <%=Budget_operationRepository.getInstance().getTextById(Language, budgetDTO.budgetOperationId)%>
</td>


<td>
    <%
        String subCodeId = sub_codeDTO.code;
        if (!Language.equalsIgnoreCase("ENGLISH")) {
            subCodeId = StringUtils.convertToBanNumber(subCodeId);
        }
    %>
    <%=subCodeId%>
</td>


<td>
    <%
        String subCodeDescription = sub_codeDTO.descriptionEn;
        if (!Language.equalsIgnoreCase("ENGLISH")) {
            subCodeDescription = sub_codeDTO.descriptionBn;
        }
    %>
    <%=subCodeDescription%>
</td>

