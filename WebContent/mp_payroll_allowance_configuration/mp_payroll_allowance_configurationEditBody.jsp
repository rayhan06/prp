<%@page import="com.google.gson.Gson" %>
<%@page import="economic_sub_code.EconomicSubCodeModel" %>
<%@page import="language.LC" %>
<%@ page import="java.util.Map" %>

<%@page import="language.LM" %>
<%@page import="mp_payroll_allowance_configuration.Mp_payroll_allowance_configurationRepository" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="java.util.List" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.google.gson.JsonObject" %>
<%@ page import="org.json.JSONObject" %>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String context = request.getContextPath() + "/";
    String formTitle = LM.getText(LC.MP_PAYROLL_ALLOWANCE_CONFIGURATION_ADD_MP_PAYROLL_ALLOWANCE_CONFIGURATION_ADD_FORMNAME, loginDTO);
    List<EconomicSubCodeModel> subCodeModels =
            Mp_payroll_allowance_configurationRepository.getInstance()
                    .getEconomicSubCodeModels(Language);
    Map<Long, Integer> amountMap = Mp_payroll_allowance_configurationRepository.getInstance().getAmountBySubCodeId();
    JSONObject amountMapJson = new JSONObject(amountMap);

%>

<style>
    .page-bg {
        background-color: #f9f9fb;
    }

    .template-row {
        display: none;
    }

    .row-data-code {
        width: 20%;
    }

    .row-data-name {
        width: 60%;
    }

    input[readonly],
    textarea[readonly] {
        font-weight: bold;
        background-color: rgba(231, 231, 231, .5) !important;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body">
                <div style="background-color: #ffffff" class="">
                    <div class="">
                        <div class="table-responsive">
                            <input type="hidden" name="economicSubCodes" id="economicSubCodes-input">
                            <input type="hidden" name="amountValues" id="amountValues">
                            <table class="table table-bordered table-striped text-nowrap" id="budget-view-table">
                                <thead>
                                <tr>
                                    <th class="row-data-code">
                                        <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                    </th>
                                    <th class="row-data-name">
                                        <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                    </th>
                                    <th class="row-data-amount">
                                        <%=getDataByLanguage(Language, "পরিমাণ(টাকা)", "Amount(Taka)")%>
                                    </th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                                <%--Template Row-> to be cloned to add new row.
                                    CONVENTIONS:
                                        * one tr with template-row class
                                        * td with class -> row-data-filedNameInJson
                                --%>
                                <tr class="template-row">
                                    <td class="row-data-code"></td>
                                    <td class="row-data-name" ></td>
                                    <td class="row-data-amount" ></td>
                                    <td class="row-data-add-btn">
                                        <button class='btn btn-sm  delete-trash-btn'
                                                style="padding-right: 14px;" type="button"
                                                onclick="unSelectRow(this);">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="text-right mt-3">
                            <button class="btn btn-sm btn-success shadow btn-border-radius"
                                    id="add-economic-code-btn"
                                    type="button">
                                <%=LM.getText(LC.BUDGET_ADD_ECONOMIC_CODE, loginDTO)%>
                            </button>
                        </div>
                    </div>
                    <div class="row my-5">
                        <div class="col-12 text-center">
                            <button id="cancel-btn" type="button"
                                    class="btn btn-sm shadow text-white btn-border-radius cancel-btn mx-2"
                                    onclick="location.href='<%=request.getHeader("referer")%>'">
                                <%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn btn-sm shadow text-white submit-btn" type="button"
                                    onclick="submitForm();">
                                <%=LM.getText(LC.PERFORMANCE_LOG_REPORT_SAVE, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="codeSelectModal.jsp"/>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const form = $('#bigform');
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const amountMap =<%=amountMapJson%>;
    var sendAmountMap = new Map();
    var amountClass = function () {
    };
    amountClass.prototype = {
        economicSubCodeId: 0,
        amount: 0,
    };

    function submitForm() {
        setButtonDisableState(true);
        const economicSubCodes = Array.from(economicSubCodeMap.keys());
        $('#economicSubCodes-input').val(JSON.stringify(economicSubCodes));
        let msg = null;
        for (let economicSubcode of economicSubCodes) {
            var obj = new amountClass();
            obj.economicSubCodeId = economicSubcode;
            obj.amount = document.getElementById("amount_" + economicSubcode).value;
            if (obj.amount == "") {
                msg = isLangEng ? "Please Inset Allowance Amount" : "ভাতার পরিমাণ প্রবেশ করুন";
            }
            sendAmountMap.set(Number(economicSubcode), obj);
        }
        if (msg) {
            $('#toast_message').css('background-color', '#ff6063');
            showToastSticky(msg, msg);
            setButtonDisableState(false);
            return;
        }
        document.getElementById("amountValues").value = JSON.stringify(
            Array.from(sendAmountMap.values())
        );
        $.ajax({
            type: "POST",
            url: "Mp_payroll_allowance_configurationServlet?actionType=ajax_add",
            data: form.serialize(),
            dataType: 'JSON',
            success: function (response) {
                if (response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                    setButtonDisableState(false);
                } else if (response.responseCode === 200) {
                    window.location.replace(getContextPath() + response.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                setButtonDisableState(false);
            }
        });
    }

    function setButtonDisableState(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function getSortIndex(economicCodeId) {
        let code = economicCodeId.replaceAll(economicCodeIdPrefix, '');
        code = code.padEnd(7, '0'); // make all 7 digit number
        return Number(code);
    }

    function sortBudgetViewTable() {
        const tableBody = document.getElementById('budget-view-table').querySelector('tbody');
        const rowsObj = Array.from(tableBody.rows).map(row => {
            return {
                data: getSortIndex(row.id),
                rowElement: row
            };
        });
        rowsObj.sort((a, b) => {
            if (a.data === b.data) return 0;
            return a.data < b.data ? -1 : 1;
        });
        tableBody.innerHTML = '';
        rowsObj.forEach(rowObj => tableBody.append(rowObj.rowElement));
    }

    function keyDownEvent(e) {
        console.log(e);
        let isvalid = inputValidationForIntValue(e, $(this), 10000000);
        return true == isvalid;
    }

    function getAmountInputElement(id) {
        const inputElement = document.createElement('input');
        inputElement.classList.add('form-control');
        inputElement.type = 'text';
        inputElement.id = id;
        inputElement.onkeydown = keyDownEvent;
        inputElement.placeholder = '<%=getDataByLanguage(Language,"পরিমাণ প্রবেশ করুন","Enter Amount")%>'
        return inputElement;
    }

    $(document).ready(function () {
        console.log("amountMap", amountMap);
        $('#add-economic-code-btn').on('click', function () {
            $('#economic-code-modal').modal();
        });
        // modal on close event
        $('#economic-code-modal').on("hidden.bs.modal", sortBudgetViewTable);

        const economicSubCodeModels = JSON.parse('<%=new Gson().toJson(subCodeModels)%>');
        if (Array.isArray(economicSubCodeModels)) {
            economicSubCodeModels.forEach(economicSubCodeModel => {
                addDataInTable('budget-view-table', economicSubCodeModel, true);
                document.getElementById("amount_" + economicSubCodeModel.id).value = amountMap[economicSubCodeModel.id];
                economicSubCodeMap.set(
                    Number(economicSubCodeModel.id),
                    economicSubCodeModel, amountMap[economicSubCodeModel.id]
                );
            });
            sortBudgetViewTable();
        }
    });
</script>