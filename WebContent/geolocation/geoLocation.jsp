<%@ page import="pb.OptionDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pb.Utils" %>
<%@ page import="geolocation.*" %>
<%--
  Created by IntelliJ IDEA.
  User: Jahin
  Date: 6/20/2021
  Time: 4:32 PM
--%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%
    OptionDTO optionDTO;
    String language = "Bangla";
    if (request.getParameter(JSLocationConfig.LANGUAGE.toString()) != null) {
        language = request.getParameter(JSLocationConfig.LANGUAGE.toString());
    }

    String geoLocationId = "jsGeoLocation";
    if (request.getParameter(JSLocationConfig.GEOLOCATION_ID.toString()) != null) {
        geoLocationId = request.getParameter(JSLocationConfig.GEOLOCATION_ID.toString());
    }

    List<OptionDTO> divisionOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("Select Division","বিভাগ নির্বাচন করুন","0");
    divisionOptionDTOList.add(optionDTO);
    List<GeoLocationDTO> geoDivisionDTOList = GeoLocationRepository.getInstance().getByParentId(1);
    for(GeoLocationDTO dto : geoDivisionDTOList){
        optionDTO = new OptionDTO(dto.name_en,dto.name_bn,String.valueOf(dto.id));
        divisionOptionDTOList.add(optionDTO);
    }


    List<OptionDTO> districtOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("Select District","জেলা নির্বাচন করুন","0");
    districtOptionDTOList.add(optionDTO);


    List<OptionDTO> upozilaOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("Select Upozila","উপজেলা নির্বাচন করুন","0");
    upozilaOptionDTOList.add(optionDTO);


    List<OptionDTO> municipalityOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("Select Municipality","পৌরসভা নির্বাচন করুন","0");
    municipalityOptionDTOList.add(optionDTO);
%>

<div class="geolocation-container" id="<%=geoLocationId%>" tag='pb_html'>
    <div class="row">
        <div class="col-lg-12" id='divisionSelect_div_<%=geoLocationId%>'tag='pb_html'>
            <select class='form-control divisionSelection rounded reve-geolocation' name='divisionSelection<%=geoLocationId%>' id='divisionSelection<%=geoLocationId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(divisionOptionDTOList,language,"0")%>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" id='districtSelect_div_<%=geoLocationId%>' tag='pb_html'>
            <select style="display: none" class='form-control districtSelection rounded reve-geolocation' name='districtSelection<%=geoLocationId%>' id='districtSelection<%=geoLocationId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(districtOptionDTOList,language,"0")%>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" id='upozilaSelect_div_<%=geoLocationId%>' tag='pb_html'>
            <select style="display: none" class='form-control upozilaSelection rounded reve-geolocation' name='upozilaSelection<%=geoLocationId%>' id='upozilaSelection<%=geoLocationId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(upozilaOptionDTOList,language,"0")%>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" id='municipalitySelect_div_<%=geoLocationId%>' tag='pb_html'>
            <select  style="display: none" class='form-control municipalitySelection rounded reve-geolocation' name='municipalitySelection<%=geoLocationId%>' id='municipalitySelection<%=geoLocationId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(municipalityOptionDTOList,language,"0")%>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" id='addressText_div_<%=geoLocationId%>' tag='pb_html'>
            <input type="text" class='form-control addressText rounded reve-geolocation <%=language.equals("English") ? "englishOnly" : "noEnglish"%>'
                   name='addressText<%=geoLocationId%>' id='addressText<%=geoLocationId%>' tag='pb_html'
            placeholder="<%=language.equals("English") ? "Enter Address (English)" : "ঠিকানা (বাংলা) লিখুন"%>">
        </div>
    </div>
    <span id='error<%=geoLocationId%>' style="color:#fd397a; text-align:center; margin-left:12px; margin-top: 1%; font-size: smaller"></span>
    <input type='hidden' class='form-control geolocation-language' name='language_<%=geoLocationId%>' id='language_<%=geoLocationId%>' value ='<%=language%>' tag='pb_html'/>
</div>