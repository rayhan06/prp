<%--
  Created by IntelliJ IDEA.
  User: Jahin
  Date: 6/20/2021
  Time: 4:33 PM
--%>
<%@page pageEncoding="UTF-8" %>

<script type="text/javascript">

    function getDivisionText(id){
        let val = '';
        if ($("#divisionSelection"+ id).val() != 0)
            val = $("#divisionSelection"+ id + " option:selected").html();
        return val;
    }

    function getDistrictText(id){
        let val = '';
        if ($("#districtSelection"+ id).val() != 0)
            val = $("#districtSelection"+ id + " option:selected").html();
        return val;
    }

    function getUpozilaText(id){
        let val = '';
        if ($("#upozilaSelection"+ id).val() != 0)
            val = $("#upozilaSelection"+ id + " option:selected").html();
        return val;
    }

    function getMunicipalityText(id){
        let val = '';
        if ($("#municipalitySelection"+ id).val() != 0)
            val = $("#municipalitySelection"+ id + " option:selected").html();
        return val;
    }

    function getAddressText(id){
        return $("#addressText"+ id).val();
    }

    function geoLocationValidator(id, allowEmpty = false){
        const language = $('#language_'+id).val();
        let errorEng = '';
        let errorBng = '';
        let noError = true;
        const division = $('#divisionSelection'+id).val();
        const district = $('#districtSelection'+id).val();
        const upozila = $('#upozilaSelection'+id).val();
        const municipality = $('#municipalitySelection'+id).val();
        const address = $('#addressText'+id).val();

        if(division == 0 && !(allowEmpty == true && address == '')){
            noError = false;
            errorEng = 'Select Division!';
            errorBng = 'বিভাগ নির্বাচন করুন!';
            $('#divisionSelection'+id).focus();
        }
        else if(district == 0 && $('#districtSelection'+id).is(':visible')){
            noError = false;
            errorEng = 'Select District!';
            errorBng = 'জেলা নির্বাচন করুন!';
            $('#districtSelection'+id).focus();
        }
        else if(upozila == 0 && $('#upozilaSelection'+id).is(':visible')){
            noError = false;
            errorEng = 'Select Upozila!';
            errorBng = 'উপজেলা নির্বাচন করুন!';
            $('#upozilaSelection'+id).focus();
        }
        // else if(municipality == 0 && $('#municipalitySelection'+id).is(':visible')){
        //     noError = false;
        //     errorEng = 'Select Municipality!';
        //     errorBng = 'পৌরসভা নির্বাচন করুন!';
        // }
        else if(address == '' && !(allowEmpty == true && division == 0) ){
            noError = false;
            errorEng = 'Enter Address!';
            errorBng = 'ঠিকানা প্রদান করুন!';
            $('#addressText'+id).focus();
        }
        if(!noError){
            if (language == 'English')
                $('#error'+id).text(errorEng);
            else
                $('#error'+id).text(errorBng);
        }
        else
            $('#error'+id).empty();
        return noError;
    }

    // function convertGeoLocationToObject(location){
    //     const geoloc = location.split('$%');
    //     let url = 'geo_Servlet?actionType=getGeoLocationObject&divisionId='+geoloc[0]+'&districtId='+geoloc[1]
    //                     +'&upozilaId='+geoloc[2]+'&municipalityId='+geoloc[3]+'&address='+geoloc[4];
    //     $.ajax({
    //         url: url,
    //         type: "GET",
    //         async: true,
    //         success: function (data) {
    //             const obj = JSON.parse(data);
    //             console.log(obj);
    //             // $('#districtSelection'+id).empty();
    //             // $('#districtSelection'+id).show();
    //             // const districtList = JSON.parse(data);
    //             // for(let i = 0; i < districtList.length; i++) {
    //             //     console.log(districtList[i]['banglaText']);
    //             //     $('#districtSelection' + id).append($('<option>', {text:(language == 'English' ? districtList[i]['englishText'] : districtList[i]['banglaText']), value:districtList[i]['value']}));  //"<option value = '"+i.toString()+"'>" + (language == 'English' ? i.toString() : convDateStrToBn(i)) + "</option>";
    //             // }
    //         }
    //         // complete: callbackFunction,
    //         // error: function (error) {
    //         //     console.log(error);
    //         // }
    //     });
    // }

    function getGeoLocation(id) {
        const division = $('#divisionSelection'+id).val();
        const district = $('#districtSelection'+id).val();
        const upozila = $('#upozilaSelection'+id).val();
        const municipality = $('#municipalitySelection'+id).val();
        const address = $('#addressText'+id).val();
        return division+'@@'+district+'@@'+upozila+'@@'+municipality+'@@'+address;
    }

    function setGeoLocation(location, id) {
        const geoloc = location.split('@@');

        if(geoloc.length < 5) return;

        $('#error'+id).empty();

        $('#districtSelection'+id).hide();
        $('#upozilaSelection'+id).hide();
        $('#municipalitySelection'+id).hide();
        $('#districtSelection'+id).val('0');
        $('#upozilaSelection'+id).val('0');
        $('#municipalitySelection'+id).val('0');

        if (geoloc[0] != 0 && isNaN(geoloc[0]) == false) {
            $('#divisionSelection'+id).val(geoloc[0]);
            loadDistricts(geoloc[0], id, geoloc[1]);
            if(geoloc[1] != 0 && isNaN(geoloc[1]) == false) {
                loadUpozilas(geoloc[1], id, geoloc[2]);
                if(geoloc[2] != 0 && isNaN(geoloc[2]) == false) {
                    loadMunicipalities(geoloc[2], id, geoloc[3]);
                }
            }
        }
        if(geoloc[4] != '')
            $('#addressText' + id).val(geoloc[4]);
    }

    function loadDistricts(divisionId, id, districtId){
        const language = $('#language_'+id).val();
        //let url = 'geo_Servlet?actionType=getDistricts&language='+language+'&divisionId='+divisionId;
        let url = 'geo_Servlet?actionType=geo_location&parent_id='+divisionId+"&type=2";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (data) {
                $('#districtSelection'+id).empty();
                $('#districtSelection'+id).show();
                const districtList = JSON.parse(data);
                for(let i = 0; i < districtList.length; i++) {
                    $('#districtSelection' + id).append($('<option>', {text:(language == 'English' ? districtList[i]['englishText'] : districtList[i]['banglaText']), value:districtList[i]['value']}));  //"<option value = '"+i.toString()+"'>" + (language == 'English' ? i.toString() : convDateStrToBn(i)) + "</option>";
                }
                $('#districtSelection'+id).val('0');
            },
            complete: function () {
                if(districtId != undefined)
                    $('#districtSelection'+id).val(districtId);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function loadUpozilas(districtId, id,upozilaId){
        const language = $('#language_'+id).val();
        //let url = 'geo_Servlet?actionType=getUpozilas&language='+language+'&districtId='+districtId;
        let url = 'geo_Servlet?actionType=geo_location&parent_id='+districtId+"&type=3";
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                $('#upozilaSelection'+id).empty();
                $('#upozilaSelection'+id).show();
                const upozilaList = JSON.parse(data);
                for(let i = 0; i < upozilaList.length; i++) {
                    $('#upozilaSelection' + id).append($('<option>', {text:(language == 'English' ? upozilaList[i]['englishText'] : upozilaList[i]['banglaText']), value:upozilaList[i]['value']}));  //"<option value = '"+i.toString()+"'>" + (language == 'English' ? i.toString() : convDateStrToBn(i)) + "</option>";
                }
                $('#upozilaSelection'+id).val('0');
            },
            complete: function () {
                if(upozilaId != undefined)
                    $('#upozilaSelection'+id).val(upozilaId);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function loadMunicipalities(upozilaId, id, municipalityId){
        const language = $('#language_'+id).val();
        //let url = 'geo_Servlet?actionType=getMunicipalities&language='+language+'&upozilaId='+upozilaId;
        let url = 'geo_Servlet?actionType=geo_location&parent_id='+upozilaId+"&type=4";
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                $('#municipalitySelection' + id).empty();
                const municipalityList = JSON.parse(data);
                for (let i = 0; i < municipalityList.length; i++) {
                    $('#municipalitySelection' + id).append($('<option>', {
                        text: (language == 'English' ? municipalityList[i]['englishText'] : municipalityList[i]['banglaText']),
                        value: municipalityList[i]['value']
                    }));  //"<option value = '"+i.toString()+"'>" + (language == 'English' ? i.toString() : convDateStrToBn(i)) + "</option>";
                }
                if (municipalityList.length == 1) {
                    $('#municipalitySelection' + id).hide();
                }
                else {
                    $('#municipalitySelection' + id).val('0');
                    $('#municipalitySelection' + id).show();
                }

            },
            complete: function () {
                if(municipalityId != undefined)
                    $('#municipalitySelection'+id).val(municipalityId);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    document.addEventListener('DOMContentLoaded', () => {

        $('.divisionSelection').val('0');

        $(document).on('input', '.addressText', function () {
            let geoLocationDiv = $(this).closest("div.geolocation-container");

            if ($('#error'+$(geoLocationDiv).prop('id')).text() != '')
                $('#error'+$(geoLocationDiv).prop('id')).empty();
        });

        $(document).on('change', '.municipalitySelection', function () {
            let geoLocationDiv = $(this).closest("div.geolocation-container");

            const id = $(this).prop('id');
            const division = $(geoLocationDiv).find('.divisionSelection').val();
            const district = $(geoLocationDiv).find('.districtSelection').val();
            const upozila = $(geoLocationDiv).find('.upozilaSelection').val();
            const municipality = $(geoLocationDiv).find('.municipalitySelection').val();

            $('#' + id).trigger('geolocation.change', [{
                'changed': id.split('Selection')[0],
                'data': {
                    'division': division,
                    'district': district,
                    'upozila': upozila,
                    'municipality' : municipality
                }
            }]);
        });

        $(document).on('change', '.upozilaSelection', function () {

            // $(this).css('border-color', '#0abb87');
            let geoLocationDiv = $(this).closest("div.geolocation-container");

            const id = $(this).prop('id');
            $(geoLocationDiv).find('.municipalitySelection').val('0');
            const division = $(geoLocationDiv).find('.divisionSelection').val();
            const district = $(geoLocationDiv).find('.districtSelection').val();
            const upozila = $(geoLocationDiv).find('.upozilaSelection').val();
            const municipality = '0'; // $(geoLocationDiv).find('.municipalitySelection').val();

            if(upozila == '0')
                $(geoLocationDiv).find('.municipalitySelection').hide();
            else {
                loadMunicipalities(upozila, $(geoLocationDiv).prop('id'), 0);
            }

            $('#' + id).trigger('geolocation.change', [{
                'changed': id.split('Selection')[0],
                'data': {
                    'division': division,
                    'district': district,
                    'upozila': upozila,
                    'municipality' : municipality
                }
            }]);

            if ($('#error'+$(geoLocationDiv).prop('id')).text() != '')
                $('#error'+$(geoLocationDiv).prop('id')).empty();

        });

        $(document).on('change', '.districtSelection', function () {

            // $(this).css('border-color', '#0abb87');
            let geoLocationDiv = $(this).closest("div.geolocation-container");

            const id = $(this).prop('id');
            $(geoLocationDiv).find('.upozilaSelection').val('0');
            $(geoLocationDiv).find('.municipalitySelection').val('0');
            const division = $(geoLocationDiv).find('.divisionSelection').val();
            const district = $(geoLocationDiv).find('.districtSelection').val();
            const upozila = '0'; // $(geoLocationDiv).find('.upozilaSelection').val();
            const municipality = '0'; // $(geoLocationDiv).find('.municipalitySelection').val();

            $(geoLocationDiv).find('.upozilaSelection').hide();
            $(geoLocationDiv).find('.municipalitySelection').hide();

            if(district != '0') {
                loadUpozilas(district, $(geoLocationDiv).prop('id'), 0);
            }

            $('#' + id).trigger('geolocation.change', [{
                'changed': id.split('Selection')[0],
                'data': {
                    'division': division,
                    'district': district,
                    'upozila': upozila,
                    'municipality' : municipality
                }
            }]);

            if ($('#error'+$(geoLocationDiv).prop('id')).text() != '')
                $('#error'+$(geoLocationDiv).prop('id')).empty();

        });

        $(document).on('change', '.divisionSelection', function () {

            // $(this).css('border-color', '#0abb87');
            let geoLocationDiv = $(this).closest("div.geolocation-container");

            const id = $(this).prop('id');
            $(geoLocationDiv).find('.districtSelection').val('0');
            $(geoLocationDiv).find('.upozilaSelection').val('0');
            $(geoLocationDiv).find('.municipalitySelection').val('0');
            const division = $(geoLocationDiv).find('.divisionSelection').val();
            const district = '0'; //$(geoLocationDiv).find('.districtSelection').val();
            const upozila = '0'; // $(geoLocationDiv).find('.upozilaSelection').val();
            const municipality = '0'; // $(geoLocationDiv).find('.municipalitySelection').val();

            $(geoLocationDiv).find('.districtSelection').hide();
            $(geoLocationDiv).find('.upozilaSelection').hide();
            $(geoLocationDiv).find('.municipalitySelection').hide();

            if(division != '0') {
                loadDistricts(division, $(geoLocationDiv).prop('id'), 0);
            }

            $('#' + id).trigger('geolocation.change', [{
                'changed': id.split('Selection')[0],
                'data': {
                    'division': division,
                    'district': district,
                    'upozila': upozila,
                    'municipality' : municipality
                }
            }]);

            if ($('#error'+$(geoLocationDiv).prop('id')).text() != '')
                $('#error'+$(geoLocationDiv).prop('id')).empty();

        });

    });
</script>
