<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="vm_route_travel_withdraw_approval.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO;
vm_route_travel_withdraw_approvalDTO = (Vm_route_travel_withdraw_approvalDTO)request.getAttribute("vm_route_travel_withdraw_approvalDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(vm_route_travel_withdraw_approvalDTO == null)
{
	vm_route_travel_withdraw_approvalDTO = new Vm_route_travel_withdraw_approvalDTO();
	
}
System.out.println("vm_route_travel_withdraw_approvalDTO = " + vm_route_travel_withdraw_approvalDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_FORMNAME, loginDTO);
String servletName = "Vm_route_travel_withdraw_approvalServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
String Language = LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp" >
<jsp:param name="isHierarchyNeeded" value="false" />
</jsp:include>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_route_travel_withdraw_approvalServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='employeeRecordId' id = 'employeeRecordId_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.employeeRecordId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_NAMEBN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.nameBn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_NAMEEN, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.nameEn%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_USERNAME, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='userName' id = 'userName_text_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.userName%>' 																<% 
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="^[A-Za-z0-9]{5,}" title="userName must contain at least 5 letters"
																<%
																	}
																%>
  tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='officeUnitId' id = 'officeUnitId_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.officeUnitId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_OFFICEUNITENG, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='officeUnitEng' id = 'officeUnitEng_text_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.officeUnitEng%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_OFFICEUNITBNG, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='officeUnitBng' id = 'officeUnitBng_text_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.officeUnitBng%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_ORGANOGRAMID, loginDTO)%></label>
                                                            <div class="col-8">
																<button type="button" class="btn btn-primary form-control" onclick="addEmployeeWithRow(this.id)" id="organogramId_button_<%=i%>" tag='pb_html'><%=LM.getText(LC.HM_ADD_EMPLOYEE, loginDTO)%></button>
																<table class="table table-bordered table-striped">
																	<tbody id="organogramId_table_<%=i%>" tag='pb_html'>
																	<%
																	if(vm_route_travel_withdraw_approvalDTO.organogramId != -1)
																	{
																		%>
																		<tr>
																			<td style="width:20%"><%=WorkflowController.getUserNameFromOrganogramId(vm_route_travel_withdraw_approvalDTO.organogramId)%></td>
																			<td style="width:40%"><%=WorkflowController.getNameFromOrganogramId(vm_route_travel_withdraw_approvalDTO.organogramId, Language)%></td>
																			<td><%=WorkflowController.getOrganogramName(vm_route_travel_withdraw_approvalDTO.organogramId, Language)%>, <%=WorkflowController.getUnitNameFromOrganogramId(vm_route_travel_withdraw_approvalDTO.organogramId, Language)%></td>
																		</tr>
																		<%
																	}
																	%>
																	</tbody>
																</table>
																<input type='hidden' class='form-control'  name = 'organogramId' id = 'organogramId_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.organogramId%>' tag='pb_html'/>
			
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_ORGANOGRAMENG, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='organogramEng' id = 'organogramEng_text_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.organogramEng%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_ORGANOGRAMBNG, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='organogramBng' id = 'organogramBng_text_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.organogramBng%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=vm_route_travel_withdraw_approvalDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=vm_route_travel_withdraw_approvalDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_ADD_VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{



	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Vm_route_travel_withdraw_approvalServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






