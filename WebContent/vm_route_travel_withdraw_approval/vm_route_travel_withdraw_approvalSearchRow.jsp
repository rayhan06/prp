<%@page pageEncoding="UTF-8" %>

<%@page import="vm_route_travel_withdraw_approval.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_route_travel_withdraw_approvalDTO vm_route_travel_withdraw_approvalDTO = (Vm_route_travel_withdraw_approvalDTO)request.getAttribute("vm_route_travel_withdraw_approvalDTO");
CommonDTO commonDTO = vm_route_travel_withdraw_approvalDTO;
String servletName = "Vm_route_travel_withdraw_approvalServlet";


System.out.println("vm_route_travel_withdraw_approvalDTO = " + vm_route_travel_withdraw_approvalDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_route_travel_withdraw_approvalDAO vm_route_travel_withdraw_approvalDAO = (Vm_route_travel_withdraw_approvalDAO)request.getAttribute("vm_route_travel_withdraw_approvalDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	

											<td>
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_route_travel_withdraw_approvalServlet?actionType=view&ID=<%=vm_route_travel_withdraw_approvalDTO.iD%>'">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_route_travel_withdraw_approvalServlet?actionType=getEditPage&ID=<%=vm_route_travel_withdraw_approvalDTO.iD%>'">
											        <%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_SEARCH_VM_ROUTE_TRAVEL_WITHDRAW_APPROVAL_EDIT_BUTTON, loginDTO)%>
											    </button>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_route_travel_withdraw_approvalDTO.iD%>'/></span>
												</div>
											</td>
																						
											

