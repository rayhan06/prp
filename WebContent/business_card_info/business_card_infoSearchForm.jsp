<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="business_card_info.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@ page import="card_info.CardEmployeeInfoDTO" %>
<%@ page import="card_info.CardEmployeeInfoRepository" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="card_info.CardEmployeeOfficeInfoDTO" %>
<%@ page import="card_info.CardEmployeeOfficeInfoRepository" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value = "";
    String Language = LM.getText(LC.BUSINESS_CARD_INFO_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String navigator2 = SessionConstants.NAV_BUSINESS_CARD_INFO;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>



<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th style="text-align: center"><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_CARDSTATUSCAT, loginDTO)%></th>
            <th style="text-align: center"><%=LM.getText(LC.CARD_APPROVAL_MAPPING_CARDHOLDER_INFORMATION, loginDTO)%></th>
            <th style="text-align: center"><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_TOTALCOUNT, loginDTO)%></th>
            <th style="text-align: center"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Business_card_infoDTO> data = (List<Business_card_infoDTO>) rn2.list;

            if (data != null && data.size() > 0) {
                List<Long> employeeInfoIds = new ArrayList<>();
                List<Long> employeeOfficeIds = new ArrayList<>();
                for (Business_card_infoDTO dto : data) {
                    employeeInfoIds.add(dto.cardEmployeeInfoId);
                    employeeOfficeIds.add(dto.cardEmployeeOfficeInfoId);
                }
                Map<Long, CardEmployeeInfoDTO> cardEmployeeInfoDTOMap = CardEmployeeInfoRepository.getInstance()
                        .getByIds(employeeInfoIds)
                        .stream()
                        .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
                Map<Long, CardEmployeeOfficeInfoDTO> cardEmployeeOfficeInfoDTOMap = CardEmployeeOfficeInfoRepository.getInstance()
                        .getByIds(employeeOfficeIds)
                        .stream()
                        .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
                for (int i = 0; i < data.size(); i++) {
                    Business_card_infoDTO business_card_infoDTO = data.get(i);
        %>
                    <tr id='tr_<%=i%>'>
                        <%@include file="business_card_infoSearchRow.jsp"%>
                    </tr>
        <%
                }
            }
        %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			