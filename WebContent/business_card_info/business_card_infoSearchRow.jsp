<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.StringUtils" %>
<%
    CardEmployeeInfoDTO cardEmployeeInfoDTO = cardEmployeeInfoDTOMap.get(business_card_infoDTO.cardEmployeeInfoId);
    CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = cardEmployeeOfficeInfoDTOMap.get(business_card_infoDTO.cardEmployeeOfficeInfoId);
%>
<td style='display:none;'>
    <input type='hidden' id='failureMessage_<%=i%>' value=''/>
</td>

<td id='<%=i%>_cardStatusCat' style="text-align: center">
    <span class="btn btn-sm border-0 shadow" style="background-color:<%=BusinessCardStatusEnum.getColor(business_card_infoDTO.cardStatusCat)%>; color: white; border-radius: 8px;cursor: text">
        <%=CatRepository.getInstance().getText(Language,"business_card_status", business_card_infoDTO.cardStatusCat)%>
    </span>
</td>

<td id='<%=i%>_cardEmployeeInfoId' style="text-align: center">
    <%=isLanguageEnglish?cardEmployeeInfoDTO.nameEn :cardEmployeeInfoDTO.nameBn%>
    <br>
    <%=isLanguageEnglish?"<b>"+cardEmployeeOfficeInfoDTO.organogramEng+"</b><br>"+cardEmployeeOfficeInfoDTO.officeUnitEng
            : "<b>"+cardEmployeeOfficeInfoDTO.organogramBng+"</b><br>"+cardEmployeeOfficeInfoDTO.officeUnitBng%>
</td>

<td id='<%=i%>_totalCount' style="text-align: center">
    <%=isLanguageEnglish?business_card_infoDTO.totalCount: StringUtils.convertToBanNumber(String.valueOf(business_card_infoDTO.totalCount))%>
</td>

<td style="text-align: center">
    <span class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
          onclick="location.href='Business_card_infoServlet?actionType=view&ID=<%=business_card_infoDTO.iD%>'">
        <a href='Business_card_infoServlet?actionType=view&ID=<%=business_card_infoDTO.iD%>'
           style="color: #FFFFFF"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
    </span>
</td>