<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="business_card_info.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="business_card_approval_mapping.Business_card_approval_mappingDTO" %>
<%@ page import="business_card_approval_mapping.Business_card_approval_mappingDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="card_info.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="util.HttpRequestUtils" %>


<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    Business_card_infoDAO business_card_infoDAO = Business_card_infoDAO.getInstance();
    Business_card_infoDTO business_card_infoDTO = business_card_infoDAO.getDTOFromID(id);
    CardEmployeeInfoDTO cardEmployeeInfoDTO = CardEmployeeInfoRepository.getInstance().getById(business_card_infoDTO.cardEmployeeInfoId);
    CardEmployeeOfficeInfoDTO cardEmployeeOfficeInfoDTO = CardEmployeeOfficeInfoRepository.getInstance().getById(business_card_infoDTO.cardEmployeeOfficeInfoId);

    List<Business_card_approval_mappingDTO> cardApprovalMappingDTOList = Business_card_approval_mappingDAO.getInstance().getAllApprovalDTOByCardInfoId(id);
    List<Long> cardApprovalIds = cardApprovalMappingDTOList.stream()
            .map(e -> e.cardApprovalId)
            .collect(Collectors.toList());
    List<CardApprovalDTO> cardApprovalDTOList = CardApprovalRepository.getInstance().getByIds(cardApprovalIds);
    Map<Long, CardApprovalDTO> mapCardApprovalDTOById = cardApprovalDTOList.stream()
            .collect(Collectors.toMap(e -> e.iD, e -> e, (e1, e2) -> e1));
    Map<Boolean, List<Business_card_approval_mappingDTO>> groupByPendingStatus = cardApprovalMappingDTOList.stream()
            .collect(Collectors.partitioningBy(e -> e.cardApprovalStatusCat == ApprovalStatus.PENDING.getValue()));
    List<CardApprovalDTO> pendingCardApprovalDTOList = new ArrayList<>(groupByPendingStatus.get(true)
            .stream()
            .map(e -> mapCardApprovalDTOById.get(e.cardApprovalId))
            .collect(Collectors.toMap(e -> e.officeUnitId, e -> e, (e1, e2) -> e1))
            .values());
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_BUSINESS_CARD_INFO_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div id="employee_info">
                <div class="col-12">
                    <h3 class="table-title"><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_BUSINESS_CARD_INFO_ADD_FORMNAME, loginDTO)%>
                    </h3>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <%
                                if (business_card_infoDTO.cardStatusCat == BusinessCardStatusEnum.REJECTED.getValue()) {
                            %>
                            <tr>
                                <td style="width:15%">
                                    <b><%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>
                                    </b>
                                </td>
                                <td style="color: red" colspan="3"><%=business_card_infoDTO.comment%>
                                </td>
                            </tr>
                            <%
                                }
                            %>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.CARD_INFO_ADD_CARDSTATUSCAT, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
								<span class="btn btn-sm border-0 shadow"
                                      style="background-color: <%=BusinessCardStatusEnum.getColor(business_card_infoDTO.cardStatusCat)%>; color: white; border-radius: 8px;cursor: text">
											<%=CatRepository.getInstance().getText(Language, "business_card_status", business_card_infoDTO.cardStatusCat)%>
								</span>
                                </td>
                                <td style="width:15%"><b><%=LM.getText(LC.BUSINESS_CARD_INFO_ADD_TOTALCOUNT, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? business_card_infoDTO.totalCount : StringUtils.convertToBanNumber(String.valueOf(business_card_infoDTO.totalCount))%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.nameEn : cardEmployeeInfoDTO.nameBn%>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.GLOBAL_EMAIL, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=cardEmployeeInfoDTO.email%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHER_NAME, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.fatherNameEn : cardEmployeeInfoDTO.fatherNameBn%>
                                </td>

                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHER_NAME, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.motherNameEn : cardEmployeeInfoDTO.motherNameBn%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%"><b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                                </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeOfficeInfoDTO.organogramEng : cardEmployeeOfficeInfoDTO.organogramBng%>
                                </td>

                                <td style="width:15%">
                                    <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                                    </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeOfficeInfoDTO.officeUnitEng : cardEmployeeOfficeInfoDTO.officeUnitBng%>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:15%">
                                    <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PRESENTADDRESS, loginDTO)%>
                                    </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.presentAddressEn : cardEmployeeInfoDTO.presentAddressBn%>
                                </td>

                                <td style="width:15%">
                                    <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_PERMANENTADDRESS, loginDTO)%>
                                    </b></td>
                                <td style="width: 35%">
                                    <%=isLanguageEnglish ? cardEmployeeInfoDTO.permanentAddressEn : cardEmployeeInfoDTO.permanentAddressBn%>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <div id="card_requester_info">
                <div class="col-12">
                    <h3 class="table-title"><%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION, loginDTO)%>
                    </h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><%=LM.getText(LC.CARD_INFO_NAME, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_DESIGNATION_OFFICE, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_REQUEST_CREATE_DATE_TIME, loginDTO)%>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="width: 35%"><%=isLanguageEnglish ? business_card_infoDTO.insertByNameEng : business_card_infoDTO.insertByNameBng%>
                            </td>
                            <td style="width: 35%">
                                <b><%=isLanguageEnglish ? business_card_infoDTO.insertByOfficeUnitOrganogramEng : business_card_infoDTO.insertByOfficeUnitOrganogramBng%>
                                </b>
                                <br><%=isLanguageEnglish ? business_card_infoDTO.insertByOfficeUnitEng : business_card_infoDTO.insertByOfficeUnitBng%>
                            </td>
                            <td style="width: 35%"><%=StringUtils.convertToDateAndTime(isLanguageEnglish, business_card_infoDTO.insertionTime)%>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="approval_info_div">
                <div class="col-12">
                    <h3 class="table-title"><%=LM.getText(LC.CARD_INFO_APPROVAL_INFORMATION, loginDTO)%>
                    </h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVER_OFFICE_INFORMATION, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVAL_STATUS, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVAL_DATE_TIME, loginDTO)%>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            if (business_card_infoDTO.cardStatusCat == BusinessCardStatusEnum.WAITING_FOR_APPROVAL.getValue()) {
                                for (CardApprovalDTO cardApprovalDTO : pendingCardApprovalDTOList) {
                        %>
                        <tr>
                            <td style="width: 35%">
                                <b><%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                </b></td>
                            <td style="width: 35%">
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=ApprovalStatus.getColor(ApprovalStatus.PENDING.getValue())%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "card_approval_status", ApprovalStatus.PENDING.getValue())%>
                                    </span>
                            </td>
                            <td style="width: 35%"></td>
                        </tr>
                        <%
                                }
                            }
                        %>
                        <%
                            for (Business_card_approval_mappingDTO cardApprovalMappingDTO : groupByPendingStatus.get(false)) {
                                CardApprovalDTO cardApprovalDTO = mapCardApprovalDTOById.get(cardApprovalMappingDTO.cardApprovalId);
                        %>
                        <tr>
                            <td style="width: 35%">
                                <b><%=isLanguageEnglish ? cardApprovalDTO.officeUnitEng : cardApprovalDTO.officeUnitBng%>
                                </b></td>
                            <td style="width: 35%">
                                    <span class="btn btn-sm border-0 shadow"
                                          style="background-color: <%=ApprovalStatus.getColor(cardApprovalMappingDTO.cardApprovalStatusCat)%>; color: white; border-radius: 8px;cursor: text">
                                        <%=CatRepository.getInstance().getText(Language, "card_approval_status", cardApprovalMappingDTO.cardApprovalStatusCat)%>
                                    </span>
                            </td>
                            <td style="width: 35%">
                                <%=StringUtils.convertToDateAndTime(isLanguageEnglish, cardApprovalMappingDTO.lastModificationTime)%>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>