<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String formTitle = LM.getText(LC.BUSINESS_CARD_ADD_FORM_NAME, loginDTO);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String context = request.getContextPath() + "/";
    Long employeeRecordId = (Long) request.getAttribute("employeeRecordId");
    boolean isAdmin = false;
    if (request.getAttribute("isAdmin") != null) {
        isAdmin = (boolean) request.getAttribute("isAdmin");
    }
    String paramEmployeeRecordId = request.getParameter("employeeRecordId");
%>
<style>
    .fly-in-from-down {
        animation: flyFromDown 1s ease-out;
    }

    @keyframes flyFromDown {
        0% {
            transform: translateY(200%);
        }
        100% {
            transform: translateY(0%);
        }
    }

    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-4 mt-4">
    <div class="kt-subheader__main">
        <h2 class="kt-subheader__title" style="color: #00a1d4;">
            <i class="far fa-address-card"></i> <%=formTitle%>
        </h2>
    </div>
    <div class="text-center ml-4 mt-4" <%=isAdmin ? "" : "style='display: none'"%> >
        <button type="button" class="btn btn-primary shadow btn-border-radius" id="self-info-btn"
                <%=employeeRecordId == null ? "style='display: none'" : ""%> >
            <%=LM.getText(LC.CARD_INFO_ADD_SELF_INFORMATION, loginDTO)%>
        </button>

        <button type="button" class="btn btn-info shadow btn-border-radius" id="select-employee-btn">
            <%=LM.getText(LC.CARD_INFO_ADD_SELECT_EMPLOYEE, loginDTO)%>
        </button>
        <div class="mt-4 mr-4" id="employee_record_id_div" style="display: none">
            <button type="button" class="btn btn-secondary form-control" disabled
                    id="employee_record_id_text"></button>
        </div>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content fly-in-from-down" id="kt_content" style="display: none">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body m-4 page-bg">
                <div class="row">
                    <div class="col-12 row">
                        <div class="col-3"></div>
                        <div class="col-6 text-center">
                            <img width="13%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h5 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h5>
                            <h5 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h5>
                        </div>
                        <div class="col-3"></div>
                    </div>
                </div>

                <div class="row" id="missing_info_div" style="display: none">
                    <div class="col-12 my-4">
                        <h3 style="color: red;text-align: center;width: 100%">
                            <%=LM.getText(LC.ERROR_MESSAGE_PLEASE_FILL_PROFILE_MISSING_INFO, loginDTO)%>
                        </h3>
                    </div>
                </div>

                <div class="row my-5">
                    <div class="col-6 row my-4 my-3">
                        <div class="col-12" id="name_label">
                            <label class="h5"><%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="name"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="email_label">
                                <%=LM.getText(LC.GLOBAL_EMAIL, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="email"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="fatherName_label">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_FATHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="fatherName"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="motherName_label">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_MOTHERNAMEENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="motherName"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="post_label">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_POST, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="post"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="office_label">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_CURRENT_OFFICE, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label id="office" class="h5"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="presentAddress_label">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PRESENTADDRESS_ENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="presentAddress"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="permanentAddress_label">
                                <%=LM.getText(LC.EMPLOYEE_RECORDS_EDIT_PERMANENTADDRESS_ENG, loginDTO).replaceAll("\\(.+\\)", "")%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="permanentAddress"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="mobileNumber_label">
                                <%=LM.getText(LC.HR_MANAGEMENT_OTHERS_OFFICE_PHONE_NUMBER, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <label class="h5" id="mobileNumber"></label>
                        </div>
                    </div>

                    <div class="col-6 row my-4 my-3">
                        <div class="col-12">
                            <label class="h5" id="totalCount_label">
                                <%=LM.getText(LC.BUSINESS_CARD_ADD_NUMBER_OF_BUSINESS_CARD, loginDTO)%>
                            </label>
                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control digitOnly" name="totalCount" id="totalCount_input" maxlength="4"
                                   onkeydown="$('#totalCount_error').hide();">
                        </div>
                        <div class="col-12 mt-3" id="totalCount_error"
                             style="display: none;color: red;font-weight: bold;">
                            <%=LM.getText(LC.BUSINESS_CARD_ADD_ENTER_CORRECT_NUMBER, loginDTO)%>
                        </div>
                    </div>
                </div>
                <div class="row my-5" id="action_button">
                    <div class="col-12">
                        <form class="form-horizontal text-right" action="Business_card_infoServlet?actionType=ajax_addBusinessCard"
                              id="businessCardForm" enctype="multipart/form-data">
                            <input type="hidden" name="employeeRecordId" id="employee-record-id-form-input">
                            <input type="hidden" name="totalCount" id="total-count-form-input">
                            <a class="btn btn-danger shadow" style="border-radius: 8px;" id="cancel_btn"
                               href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_CANCEL_BUTTON, loginDTO)%>
                            </a>
                            <button type="button" id="submit_btn" class="btn btn-success shadow ml-2"
                                    style="border-radius: 8px;" onclick="submitForm()">
                                <%=LM.getText(LC.CARD_INFO_ADD_CARD_INFO_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%if (isAdmin) {%>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>
<%}%>
<script src="<%=context%>assets/scripts/input_validation.js"></script>
<script>
    let dataMissing = false;

    function submitForm() {
        const totalCount = $('#totalCount_input').val().trim();
        if (totalCount === '' || totalCount <= 0) {
            $('#totalCount_error').show();
            buttonStateChange(false);
            return false;
        }

        $('#total-count-form-input').val(totalCount);

        let message = '<%=LM.getText(LC.CARD_INFO_ADD_DO_YOU_WANT_TO_SUBMIT_THIS_CARD, loginDTO)%>';
        let confirmBtnText = '<%=LM.getText(LC.CARD_INFO_ADD_YES_SUBMIT, loginDTO)%>';
        let cancelBtnText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
        messageDialog(
            '', message, 'success', true, confirmBtnText, cancelBtnText,
            ()=>{
                submitAjaxForm('businessCardForm');
            }, () => false
        );
    }


    function buttonStateChange(value) {
        $('#cancel_btn').prop("disabled", value);
        $('#submit_btn').prop("disabled", value);
    }

    async function showCardInfo(employeeRecordId) {
        const mainContent = $('#kt_content');
        mainContent.hide();
        mainContent.removeClass('fly-in-from-down');

        const url = 'Card_infoServlet?actionType=ajax_getCardInfoModel&employeeRecordId=' + employeeRecordId + '&language=' + '<%=Language%>';
        const res = await fetch(url);
        const cardInfoModel = await res.json();

        document.getElementById('name').textContent = cardInfoModel.name;
        document.getElementById('post').textContent = cardInfoModel.post;
        document.getElementById('fatherName').textContent = cardInfoModel.fatherName;
        document.getElementById('motherName').textContent = cardInfoModel.motherName;
        document.getElementById('presentAddress').textContent = cardInfoModel.presentAddress;
        document.getElementById('permanentAddress').textContent = cardInfoModel.permanentAddress;
        document.getElementById('office').textContent = cardInfoModel.office;
        document.getElementById('mobileNumber').textContent = cardInfoModel.mobileNumber;
        document.getElementById('email').textContent = cardInfoModel.email;

        document.getElementById('employee-record-id-form-input').value = employeeRecordId;

        let dataMissing = false;
        if (cardInfoModel.name === "") {
            $('#name_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#name_label').css('color', 'black');
        }

        if (cardInfoModel.post === "") {
            $('#post_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#post_label').css('color', 'black');
        }

        if (cardInfoModel.fatherName === "") {
            $('#fatherName_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#fatherName_label').css('color', 'black');
        }

        if (cardInfoModel.motherName === "") {
            $('#motherName_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#motherName_label').css('color', 'black');
        }

        if (cardInfoModel.presentAddress === "") {
            $('#presentAddress_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#presentAddress_label').css('color', 'black');
        }

        if (cardInfoModel.permanentAddress === "") {
            $('#permanentAddress_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#permanentAddress_label').css('color', 'black');
        }

        if (cardInfoModel.office === "") {
            $('#office_label').css('color', 'red');
            dataMissing = true;
        } else {
            $('#office_label').css('color', 'black');
        }

        console.log("cardInfoModel : ", cardInfoModel);
        console.log("dataMissing : ", dataMissing);

        if (dataMissing) {
            $('#missing_info_div').show();
            $("#action_button").hide();
        } else {
            $('#missing_info_div').hide();
            $("#action_button").show();
        }

        const empInfo = {
            employeeNameEn: cardInfoModel.name,
            employeeNameBn: cardInfoModel.name,
            organogramNameEn: cardInfoModel.post,
            organogramNameBn: cardInfoModel.post,
            officeUnitNameEn: cardInfoModel.office,
            officeUnitNameBn: cardInfoModel.office
        };
        viewEmployeeRecordIdInInput(empInfo);

        mainContent.show();
        <%if(isAdmin){%>
        mainContent.addClass('fly-in-from-down');
        <%}%>
    }

    $('#self-info-btn').click(async () => {
        await showCardInfo(<%=employeeRecordId%>);
    });

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.employeeNameEn + ', ' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + ', ' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: empInfo => showCardInfo(empInfo.employeeRecordId)
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';
    // modal trigger button
    $('#select-employee-btn').click(() => {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    });

    $(async () => {
        <%if(!isAdmin){%>
        await showCardInfo(<%=employeeRecordId%>);
        <%}else if(paramEmployeeRecordId != null){%>
        await showCardInfo(<%=paramEmployeeRecordId%>);
        <%}%>
    });
</script>