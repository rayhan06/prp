<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.*"%>
<%@ page import="pb.*" %>
<%@page import="workflow.WorkflowController"%>
<%@ page import="employee_offices.*"%>
<%@ page import="pb.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.SOLVED_TICKET_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.SOLVED_TICKET_REPORT_WHERE_TICKETISSUESTYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='ticketIssuesType' id = 'ticketIssuesType' >		
						<%		
						Options = CommonDAO.getOptions(Language, "ticket_issues", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=Language.equalsIgnoreCase("english")?"Support Engineer":"সাপোর্ট ইঞ্জিনিয়ার"%>
				</label>
				<div class="col-sm-9">
					
					<select name='forwardFromOrganogramId' id = 'forwardFromOrganogramId'	class='form-control'>
						<option value = ""></option>
						 <%
						     Set<Long> engineersAndAdmins = EmployeeOfficeRepository.getInstance().getByRole(SessionConstants.SUPPORT_ENGINEER_ROLE);

						
						 
                            for(Long employee: engineersAndAdmins)
                            {
                            %>
                            <option value = "<%=employee%>" >
                             <%=WorkflowController.getNameFromOrganogramId(employee, Language)%>
                            </option>
                            <%
                            }
                            %>
						</select>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.HM_ISSUE_RAISER, loginDTO)%>
				</label>
				<div class="col-sm-9">
					
					<button type="button" class="btn btn-block submit-btn btn-border-radius text-white"
	                        onclick="addEmployee()"
	                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
	                </button>
	                <table class="table table-bordered table-striped">
	                    <tbody id="employeeToSet"></tbody>
	                </table>
                	<input type='hidden'  name='issueRaiserOrganogramId' id = 'issueRaiserOrganogramId' value=""/>						
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
    </div>
</div>
<script type="text/javascript">
function init()
{
	dateTimeInit($("#Language").val());
    $("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = true;
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}
function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#issueRaiserOrganogramId").val(orgId);
}

function getLink(list)
{
	var id = convertToEnglishNumber(list[1]);

	return "Support_ticketServlet?actionType=view&ID=" + id;
}
</script>