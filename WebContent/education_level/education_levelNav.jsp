<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="common.BaseServlet" %>
<%@page pageEncoding="UTF-8" %>


<%
    String action = request.getParameter("url");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String context = "../../.." + request.getContextPath() + "/";
    String Language = LM.getText(LC.EDUCATION_LEVEL_EDIT_LANGUAGE, loginDTO);
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0' onKeyUp='allfield_changed("",0)' id='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="educationId"><%=LM.getText(LC.HM_NAME, loginDTO)%></label>
                        <div class="col-md-9">
                            <select class='form-control' name='nameId' id='educationId' style="width: 100%" onchange="allfield_changed()">
                                <%=Education_levelRepository.getInstance().buildOptions(Language,null)%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed()"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>

<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script>
    $(document).ready(()=>{
        select2SingleSelector('#educationId','<%=Language%>')
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed() {
        let params = 'AnyField=' + document.getElementById('anyfield').value;
        params += '&search=true&ajax=true';
        if($('#educationId').val()){
            params += '&nameId='+$('#educationId').val();
        }
        params += '&pageno=' + document.getElementsByName('pageno')[0].value;
        params += '&RECORDS_PER_PAGE=' + document.getElementsByName('RECORDS_PER_PAGE')[0].value;
        dosubmit(params);
    }
</script>

