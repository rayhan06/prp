<%@page pageEncoding="UTF-8" %>
<%@ page import="common.NameDTO" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>


<td id='<%=i%>_nameBn'>
    <%=StringEscapeUtils.escapeHtml4(education_levelDTO.nameBn)%>
</td>


<td id='<%=i%>_nameEn'>
    <%=StringEscapeUtils.escapeHtml4(education_levelDTO.nameEn)%>
</td>

<td id='<%=i%>_Edit'>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Education_levelServlet?actionType=getEditPage&ID=<%=education_levelDTO.iD%>'"
    >
        <i class="fa fa-edit"></i>
    </button>
</td>

<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=education_levelDTO.iD%>'/></span>
    </div>
</td>