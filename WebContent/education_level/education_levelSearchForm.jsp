<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="common.NameDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="user.UserRepository" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    RecordNavigator rn2 = (RecordNavigator)request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EDUCATION_LEVEL_EDIT_NAMEBN, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EDUCATION_LEVEL_EDIT_NAMEEN, loginDTO)%>
            </th>
            <th style="width: 100px"><%=LM.getText(LC.EDUCATION_LEVEL_SEARCH_EDUCATION_LEVEL_EDIT_BUTTON, loginDTO)%>
            </th>
			<th class="text-center" style="width: 25px">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
				<div class="d-flex align-items-center justify-content-between">
					<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
						<i class="fa fa-trash"></i>&nbsp;
					</button>
					<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
				</div>
			</th>
        </tr>
        </thead>
        <tbody>
        <%
            List<NameDTO> data = (List<NameDTO>) rn2.list;
                if (data != null && data.size()>0) {
                    for (int i = 0; i < data.size(); i++) {
                        NameDTO education_levelDTO = data.get(i);
        %>
                        <tr id='tr_<%=i%>'>
                            <%@include file="education_levelSearchRow.jsp"%>
                        </tr>
        <% } } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


<script src="nicEdit.js" type="text/javascript"></script>

<script type="text/javascript">

    function getOriginal(i, tempID, parentID, ServletName) {
        console.log("getOriginal called");
        var idToSubmit;
        var isPermanentTable;
        var state = document.getElementById(i + "_original_status").value;
        if (state == 0) {
            idToSubmit = parentID;
            isPermanentTable = true;
        } else {
            idToSubmit = tempID;
            isPermanentTable = false;
        }
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var response = JSON.parse(this.responseText);
                document.getElementById(i + "_nameBn").innerHTML = response.nameBn;
                document.getElementById(i + "_nameEn").innerHTML = response.nameEn;

                if (state == 0) {
                    document.getElementById(i + "_getOriginal").innerHTML = "View Edited";
                    state = 1;
                } else {
                    document.getElementById(i + "_getOriginal").innerHTML = "View Original";
                    state = 0;
                }

                document.getElementById(i + "_original_status").value = state;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", ServletName + "?actionType=getDTO&ID=" + idToSubmit + "&isPermanentTable=" + isPermanentTable, true);
        xhttp.send();
    }

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;
            if (!document.getElementById('nameBn_text_' + row).checkValidity()) {
                empty_fields += "nameBn";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }
            if (!document.getElementById('nameEn_text_' + row).checkValidity()) {
                empty_fields += "nameEn";
                if (i > 0) {
                    empty_fields += ", ";
                }
                i++;
            }


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Education_levelServlet");
    }

    function init(row) {

    }


    function submitAjax(i, deletedStyle) {
        console.log('submitAjax called');
        var isSubmittable = PreprocessBeforeSubmiting(i, "inplaceedit");
        if (isSubmittable == false) {
            return;
        }
        var formData = new FormData();
        var value;
        value = document.getElementById('iD_hidden_' + i).value;
        console.log('submitAjax i = ' + i + ' id = ' + value);
        formData.append('iD', value);
        formData.append("identity", value);
        formData.append("ID", value);
        formData.append('nameBn', document.getElementById('nameBn_text_' + i).value);
        formData.append('nameEn', document.getElementById('nameEn_text_' + i).value);
        formData.append('isDeleted', document.getElementById('isDeleted_hidden_' + i).value);
        formData.append('lastModificationTime', document.getElementById('lastModificationTime_hidden_' + i).value);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != '') {
                    document.getElementById('tr_' + i).innerHTML = this.responseText;
                    ShowExcelParsingResult(i);
                } else {
                    console.log("No Response");
                    document.getElementById('tr_' + i).innerHTML = 'NULL RESPONSE';
                }
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("POST", 'Education_levelServlet?actionType=edit&inplacesubmit=true&isPermanentTable=true&deletedStyle=' + deletedStyle + '&rownum=' + i, true);
        xhttp.send(formData);
    }

    window.onload = function () {
        ShowExcelParsingResult('general');
    }
</script>
			