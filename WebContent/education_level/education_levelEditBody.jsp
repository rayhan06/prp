<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="education_level.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="common.NameDTO" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="java.util.List" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    NameDTO education_levelDTO;
    String actionName,formTitle;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        education_levelDTO = Education_levelRepository.getInstance().getDTOByID(Long.parseLong(ID));
        formTitle = LM.getText(LC.EDUCATION_LEVEL_EDIT_EDUCATION_LEVEL_EDIT_FORMNAME, loginDTO);
    } else {
        actionName = "add";
        education_levelDTO = new NameDTO();
        formTitle = LM.getText(LC.EDUCATION_LEVEL_ADD_EDUCATION_LEVEL_ADD_FORMNAME, loginDTO);
    }
    int i = 0;
    int childTableStartingID = 1;
    String Language = LM.getText(LC.EDUCATION_LEVEL_EDIT_LANGUAGE, loginDTO);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal kt-form" id="educationLevelForm" name="bigform"
              action="Education_levelServlet?actionType=ajax_<%=actionName%>&isPermanentTable=true&id=<%=ID%>"
              enctype="multipart/form-data">

            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">
                                            <%=LM.getText(LC.EDUCATION_LEVEL_ADD_NAMEBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='nameBn_div_<%=i%>'>
                                            <input type='text' class='form-control' name='nameBn'
                                                   id='nameBn_text_<%=i%>' required="required"
                                                   value='<%=education_levelDTO.nameBn == null ? "":education_levelDTO.nameBn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">
                                            <%=LM.getText(LC.EDUCATION_LEVEL_ADD_NAMEEN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9" id='nameEn_div_<%=i%>'>
                                            <input type='text' class='form-control' name='nameEn'
                                                   id='nameEn_text_<%=i%>' required="required"
                                                   value='<%=education_levelDTO.nameEn == null ? "" : education_levelDTO.nameEn%>' tag='pb_html'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.EDUCATION_LEVEL_ADD_DEGREE_EXAM, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.EDUCATION_LEVEL_ADD_DEGREE_EXAM_NAMEBN, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.EDUCATION_LEVEL_ADD_DEGREE_EXAM_NAMEEN, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.EDUCATION_LEVEL_ADD_DEGREE_EXAM_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-DegreeExam">
                            <%
                                List<DegreeExamDTO> list = DegreeExamDAO.getInstance().getDegreeExamDTOListByEducationLevelID(education_levelDTO.iD);
                                if (actionName.equals("edit")) {
                                    int index = -1;
                                    for (DegreeExamDTO degreeExamDTO : list) {
                                        index++;
                            %>

                            <tr id="DegreeExam_<%=index + 1%>">
                                <td style="display: none;">
                                    <input type='hidden' class='form-control'
                                           name='degreeExam.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=degreeExamDTO.iD%>'
                                           tag='pb_html'/>
                                </td>
                                <td>
                                    <input type='text' class='form-control'
                                           name='degreeExam.nameBn' required
                                           value='<%=degreeExamDTO.nameBn%>' tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='text' class='form-control'
                                           name='degreeExam.nameEn' required
                                           value='<%=degreeExamDTO.nameEn%>' tag='pb_html'/>
                                </td>
                                <td>
                                    <div class='checker'>
                                        <input type='checkbox' id='degreeExam_cb_<%=index%>'
                                               name='checkbox' value=''/>
                                    </div>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <button
                                id="add-more-DegreeExam"
                                name="add-moreDegreeExam"
                                type="button"
                                class="btn btn-sm text-white add-btn shadow">
                            <i class="fa fa-plus"></i>
                            <%=LM.getText(LC.EDUCATION_LEVEL_ADD_DEGREE_EXAM_ADD_MORE, loginDTO)%>
                        </button>
                        <button
                                id="remove-DegreeExam"
                                name="removeDegreeExam"
                                type="button"
                                class="btn btn-sm remove-btn shadow ml-2 pl-4">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                    <template id="template-DegreeExam">
                        <tr>
                            <td style="display: none;">
                                <input type='hidden' class='form-control' name='degreeExam.iD'
                                       value='0' tag='pb_html'/>
                            </td>
                            <td>
                                <input type='text' class='form-control' name='degreeExam.nameBn'
                                       value='' required tag='pb_html'/>
                            </td>
                            <td>
                                <input type='text' class='form-control' name='degreeExam.nameEn'
                                       value='' required tag='pb_html'/>
                            </td>
                            <td>
                                <div>
                                    <input type='checkbox' name='checkbox' value=''/>
                                </div>
                            </td>
                        </tr>
                    </template>
                </div>
                <div class="text-center mt-5">
                    <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                        <%=LM.getText(LC.EDUCATION_LEVEL_ADD_EDUCATION_LEVEL_CANCEL_BUTTON, loginDTO)%>
                    </button>
                    <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitEducationLevelForm()">
                        <%=LM.getText(LC.EDUCATION_LEVEL_ADD_EDUCATION_LEVEL_SUBMIT_BUTTON, loginDTO)%>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const educationLevelForm = $('#educationLevelForm');
    const cancelBtn = $('#cancel-btn');
    const submitBtn = $('#submit-btn');
    const isLangEng = '<%=HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng%>';

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    function submitEducationLevelForm(){
        setButtonState(true);
        let params = educationLevelForm.serialize().split('&');
        let msg = null;
        for(let i = 0;i<params.length ;i++){
            let param = params[i].split('=');
            switch (param[0]){
                case 'nameBn':
                    if(!param[1]){
                        msg = isLangEng ? "Bangla name for education level is missing": "শিক্ষাস্তরের বাংলা নাম পাওয়া যায়নি";
                    }
                    break;
                case 'nameEn':
                    if(!param[1]){
                        msg = isLangEng ? "English name for education level is missing" : "শিক্ষাস্তরের ইংরেজীর নাম পাওয়া যায়নি";
                    }
                    break;
                case 'degreeExam.nameBn':
                    if(!param[1]){
                        msg = isLangEng ? "Bangla name for degree exam is missing" : "ডিগ্রী পরীক্ষার বাংলা নাম পাওয়া যায়নি";
                    }
                    break;
                case 'degreeExam.nameEn':
                    if(!param[1]){
                        msg = isLangEng ? "English name for degree exam is missing" : "ডিগ্রী পরীক্ষার ইংরেজীর নাম পাওয়া যায়নি";
                    }
                    break;
            }
            if(msg){
                $('#toast_message').css('background-color','#ff6063');
                showToastSticky(msg,msg);
                setButtonState(false);
                return;
            }
        }
        submitAjaxForm('educationLevelForm');
    }

    function setButtonState(value){
        cancelBtn.prop("disabled",value);
        submitBtn.prop("disabled",value);
    }

    var row = 0;

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-DegreeExam").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-DegreeExam");

            $("#field-DegreeExam").append(t.html());
            SetCheckBoxValues("field-DegreeExam");
            var tr = $("#field-DegreeExam").find("tr:last-child");
            tr.attr("id", "DegreeExam_" + child_table_extra_id);
            child_table_extra_id++;
        });


    $("#remove-DegreeExam").click(function (e) {
        var tablename = 'field-DegreeExam';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }
        }
    });


</script>