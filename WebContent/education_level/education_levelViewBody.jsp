<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="education_level.*" %>
<%@ page import="common.NameDTO" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EDUCATION_LEVEL_EDIT_LANGUAGE, loginDTO);
    String ID = request.getParameter("ID");
    NameDTO education_levelDTO = Education_levelDAO.getInstance().getDTOFromID(Long.parseLong(ID));
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    Education Level Details
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                Education Level
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EDUCATION_LEVEL_EDIT_NAMEBN, loginDTO)%>
                        </b></td>
                        <td>
                            <%=education_levelDTO.nameBn%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EDUCATION_LEVEL_EDIT_NAMEEN, loginDTO)%>
                        </b></td>
                        <td>
                            <%=education_levelDTO.nameEn%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>