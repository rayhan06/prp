<%@page import="inbox.TicketInboxDTO" %>
<%@page import="category.CategoryDAO" %>
<%@page import="config.GlobalConfigConstants" %>

<%@page import="config.GlobalConfigurationRepository" %>
<%@page import="inbox.InboxDAO" %>
<%@page import="inbox.InboxDTO" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="inbox.*" %>

<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="office_unit_organograms.Office_unit_organogramsDAO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserDTO" %>
<%@ page import="pb.*" %>
<%@ page import="approval_execution_table.*" %>


<%@page import="user.UserRepository" %>

<%@ page import="user_organogram.User_organogramDAO" %>

<%@page import="util.RecordNavigator" %>

<%@page import="java.util.ArrayList" %>
<%@page import="java.util.List" %>

<%@page import="holidays.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="workflow.WorkflowController" %>


<%

    Office_unit_organogramsDAO office_unit_organogramsDAO = new Office_unit_organogramsDAO();

    InboxDTO inboxDTO;
    inboxDTO = (InboxDTO) request.getAttribute("inboxDTO");

    if (inboxDTO == null) {
        inboxDTO = new InboxDTO();

    }

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    InboxDAO inboxDAO = (InboxDAO) request.getAttribute("inboxDAO");

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

//tab_data

    List<InboxTabData> inboxTabDataList = (List<InboxTabData>) request.getAttribute("tab_data");

    if (inboxTabDataList == null) {
        System.out.println("In jsp form , inboxTabDataList is null");
    } else {
        System.out.println("In jsp form, inboxTabDataList size = " + inboxTabDataList.size());
        if (inboxTabDataList.get(0).tabDTOList == null) {
            System.out.println("In jsp form inboxTabDataList.get(0).tabDTOList == null");
        } else {
            System.out.println("In jsp form inboxTabDataList.get(0).tabDTOList size = " + inboxTabDataList.get(0).tabDTOList.size());
        }
    }


    int tempAjaxFlag = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.INBOX_DEFAULT_AJAX).value);//SessionConstants.DEFAULT_INBOX_ORGANOGRAM_ID;


    int tempTabIDInt = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.INBOX_DEFAULT_TAB_ID).value);//SessionConstants.DEFAULT_INBOX_ORGANOGRAM_ID;

    if (request.getAttribute("tab_id") != null) {
        tempTabIDInt = (int) request.getAttribute("tab_id");
    }


    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);

    String url = request.getParameter("url");
    String action = "InboxServlet?actionType=getInboxMain";////url;
    String navigator = "navINBOX";//SessionConstants.NAV_INBOX;//request.getParameter("navINBOX");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";

    RecordNavigator rn = (RecordNavigator) request.getSession(true).getAttribute("navigator");

    int totalpage2 = 0, temp_total_records = 0;

    long temp_search_time = 0;

    if (rn == null) {
        rn = new RecordNavigator();
    }

    if (rn != null) {
        totalpage2 = rn.getTotalPages();

        temp_total_records = rn.getTotalRecords();

        temp_search_time = rn.getSearchTime();
    }

    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    int pagination_number = 0;

    User_organogramDAO tempUser_organogramDAO = new User_organogramDAO();

    CategoryDAO tempCategory = new CategoryDAO();


    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    System.out.println("Total pages = " + rn.getTotalPages());


%>


<div style="font-family: Roboto, SolaimanLipi">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs border-bottom-0" role="tablist">
        <li role="presentation" class="border p-3 rounded px-4">
            <a
                    class="font-weight-bold" style="color: #00a1d4"
                    href="#tab_0"
                    aria-controls="tab_0"
                    role="tab"
                    data-toggle="tab"
                    id="tab_button_0"
                    onclick="hilite('tab_button_0', 'tab_button_1')"
                    
            >
                <%=LM.getText(LC.HM_INCOMING, loginDTO)%>
                (<%=Utils.getDigits(inboxTabDataList.get(0).count, Language)%>)
            </a>

        </li>
        <li role="presentation" class="border  p-3 rounded ml-3 px-4">
            <a
                    class="font-weight-bold" style="color: #0bd398"
                    href="#tab_1"
                    aria-controls="tab_1"
                    role="tab"
                    data-toggle="tab"
                    id="tab_button_1"
                    onclick="hilite('tab_button_1', 'tab_button_0')"
            >
                <%=LM.getText(LC.HM_OUTGOING, loginDTO)%>
                (<%=Utils.getDigits(inboxTabDataList.get(1).count, Language)%>)
            </a>

        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <%
            for (int tabIndex = 0; tabIndex < inboxTabDataList.size(); tabIndex++) {
                int tempTabCountUnseen = 0;
                System.out.println("tabIndex = " + tabIndex);

                long fromDateUnformatted = -1;
                Date fromDate = null;
                Date dueDate = null;

                if (inboxTabDataList.get(tabIndex).tabDTOList == null) {
                    System.out.println("loop inboxTabDataList.get(tabIndex).tabDTOList is null ");
                } else {
                    System.out.println("loop inboxTabDataList Row count in jsp = " + inboxTabDataList.get(tabIndex).tabDTOList.size());
                }
        %>
        <div role="tabpanel" class="tab-pane <% if(tabIndex==0) out.print("active"); %>" id="tab_<%= tabIndex %>">
            <div class="table-responsive">
                <table class="table table-bordered text-nowrap">

                    <%
                        int effectiveTabIndex = tabIndex;
                        if (effectiveTabIndex == InboxTabData.OUTGOIUNG_TICKET_TAB || effectiveTabIndex == InboxTabData.INCOMING_TICKET_TAB) {
                    %>
                    <thead>
                    <tr style="color: #0d7fcc">
                        <th><%=LM.getText(LC.SUPPORT_TICKET_ADD_ID, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.HM_ARRIVAL_DATE, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.SUPPORT_TICKET_ADD_DUEDATE, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.HM_ISSUE_RAISER, loginDTO)%></th>
                        <th><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESSUBTYPETYPE, loginDTO)%>
                        </th>

                        <th><%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETSTATUSCAT, loginDTO)%>
                        </th>
                        <th><%=LM.getText(LC.SUPPORT_TICKET_ADD_PRIORITYCAT, loginDTO)%>
                        </th>

                    </tr>
                    </thead>
                    <%
                        }
                    %>
                    <tbody>
                    <%
                        if (inboxTabDataList.get(tabIndex).tabDTOList == null) {
                            System.out.println("inboxTabDataList.get(" + tabIndex + ").tabDTOList is null ");
                        } else {
                            System.out.println("inboxTabDataList.get(" + tabIndex + ").tabDTOList Row count in jsp = " + inboxTabDataList.get(tabIndex).tabDTOList.size());
                            for (int rowIndex = 0; rowIndex < inboxTabDataList.get(tabIndex).tabDTOList.size(); rowIndex++) {
                                String color = "black";
                                if (effectiveTabIndex == InboxTabData.OUTGOIUNG_TICKET_TAB || effectiveTabIndex == InboxTabData.INCOMING_TICKET_TAB) {
                                    TicketInboxDTO ticketInboxDTO = (TicketInboxDTO) inboxTabDataList.get(tabIndex).tabDTOList.get(rowIndex);
                                    if (ticketInboxDTO.isSeen) {
                                        color = "blue";
                                    }
                                    if (new Date().getTime() > ticketInboxDTO.dueDate && ticketInboxDTO.isSeen == false) {
                                        color = "red";
                                    }
                                    String clickLink = "Support_ticketServlet?actionType=view&ID=" + ticketInboxDTO.inboxOriginatorId;
                                    String value = "";
                    %>
                    <tr style="cursor: pointer; color: <%=color%>" onclick="window.location = '<%=clickLink%>';"
                        data-link="<%=clickLink%>">
                        <td>
                            <%=Utils.getDigits(ticketInboxDTO.supportTicketId + "", Language)%>
                        </td>
                        <td>
                            <%
                                String formatted_arrivalDate = simpleDateFormat.format(new Date(ticketInboxDTO.arrivalDate));
                            %>
                            <%=Utils.getDigits(formatted_arrivalDate, Language)%>

                        </td>
                        <td>
                            <%
                                String formatted_dueDate = simpleDateFormat.format(new Date(ticketInboxDTO.dueDate));
                            %>
                            <%=Utils.getDigits(formatted_dueDate, Language)%>

                        </td>
                        
                        <td>
                            <%
                            value = WorkflowController.getNameFromOrganogramId(ticketInboxDTO.issueRaiser, Language) + "";
                            %>
                            <%=Utils.getDigits(value, Language)%>

                        </td>
                        <td>
                            <%
                                value = CommonDAO.getName(ticketInboxDTO.ticketIssuesType, "ticket_issues", Language.equals("English") ? "name_en" : "name_bn", "id");
                            %>

                            <%=Utils.getDigits(value, Language)%>

                        </td>
                        <td>

                            <%
                                value = CommonDAO.getName(ticketInboxDTO.ticketIssuesSubtypeType, "ticket_issues_subtype", Language.equals("English") ? "name_en" : "name_bn", "id");
                                if (value.equalsIgnoreCase("others") || value.equalsIgnoreCase("অন্যান্য")) {
                                    value = ticketInboxDTO.issueText;
                                }
                            %>

                            <%=Utils.getDigits(value, Language)%>
                        </td>
                        <td>
                            <%
                                value = CatDAO.getName(Language, "ticket_status", ticketInboxDTO.ticketStatusCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>
                        </td>
                        <td>
                            <%
                                value = CatDAO.getName(Language, "priority", ticketInboxDTO.priorityCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>
                        </td>

                    </tr>
                    <%
                                }
                            }

                        }
                    %>
                    </tbody>
                </table>
            </div>
        </div>
        <%

            }
        %>


        <input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>

        <input type="hidden" id="hidden_totalrecords" value="<%=temp_total_records%>"/>

        <input type="hidden" id="hidden_lastSearchTime" value="<%=temp_search_time%>"/>

        <input type="hidden" id="hidden_pageno" value="<%=pageno%>"/>

        <input type="hidden" id="isPermanentTable" value="true"/>
    </div><!-- END TAB PAN -->


</div>
<!-- END TAB PAN WRAPER -->
		
		