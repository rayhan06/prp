
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="inbox_tab_column.*"%>
<%@page import="java.util.*"%>
<%@page import="inbox.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="inbox_movements.*"%>


<%

InboxDTO inboxDTO;
List<Inbox_movementsDTO> tempDTOList = (List<Inbox_movementsDTO>)request.getAttribute("data");

//if(inboxDTO == null)
//{
//	inboxDTO = new InboxDTO();
	
//}

/*
Inbox_tab_columnDTO inbox_tab_columnDTO;
inbox_tab_columnDTO = (Inbox_tab_columnDTO)request.getAttribute("inbox_tab_columnDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

InboxDAO inboxDAO = (InboxDAO)request.getAttribute("inboxDAO");

List<InboxTabData> tempTabData = inboxDAO.getTabData();

if(inbox_tab_columnDTO == null)
{
	inbox_tab_columnDTO = new Inbox_tab_columnDTO();
	
}
System.out.println("inbox_tab_columnDTO = " + inbox_tab_columnDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle;
if(actionName.equals("edit"))
{
	formTitle = LM.getText(LC.INBOX_TAB_COLUMN_EDIT_INBOX_TAB_COLUMN_EDIT_FORMNAME, loginDTO);
}
else
{
	formTitle = LM.getText(LC.INBOX_TAB_COLUMN_ADD_INBOX_TAB_COLUMN_ADD_FORMNAME, loginDTO);
}

formTitle = LM.getText(LC.INBOX_SEARCH_INBOX_SEARCH_FORMNAME, loginDTO);



String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";
InboxDTO row = inboxDTO;
String Language = LM.getText(LC.INBOX_EDIT_LANGUAGE, loginDTO);

*/
%>

<div class="portlet light">

	<form  action="Office_unit_organogramsServlet?actionType=MultiAssignApprovalPathToOrganograms" id="bigform" name="bigform"  method="POST">
		<div class="portlet-window-body">
			<div class="row" style="margin-left: 0; margin-right: 0;">
				

				<div class="portlet light">
					<div class="portlet-title">
						<h3>Inbox History</h3>
						
					</div>
					<div class="portlet-window-body">
					
						<div class="form-group" style="border:1px solid #659be0">

							<!-- Start Accrodian -->
							<div class="panel-group" id="accordion" role="tablist">
								<% for(int k=0; k < tempDTOList.size(); k++){ %>
								<div class="panel panel-default">
									<div class="panel-heading" id="headingOne">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_<%=k%>">
												&nbsp;
											</a>
										</h4>
									</div>
									<div id="collapse_<%=k%>" class="panel-collapse collapse in">
										<div class="panel-body">
											<div>
												<label class="control-label label-center"> Message Details </label>
							
												<div>
													<%=tempDTOList.get(k).note %>
												</div>
												
												<label class="control-label label-center">Existing Files</label>
												<table class="table table-bordered" >
													<thead>
														<tr>
															<th>Name</th>
															<th>File</th>
															<th>Title</th>
															<th>Tag</th>
														</tr>
													</thead>
													<tbody>
														<% 
														for(int j =0; j < tempDTOList.get(k).files.size(); j++){ 
															String encode = Base64.getEncoder().encodeToString(tempDTOList.get(k).files.get(j).fileData);
														%>
														<tr>
															<td><%=tempDTOList.get(k).files.get(j).fileTitle %></td>
															<td>
																<a href="Project_trackerServlet?actionType=getFile&id=<%=tempDTOList.get(k).files.get(j).iD%>" class="download_file">Download</a>
																<a href="#" data-toggle="modal" data-target="#dataFileModal_1_<%=j %>">View</a>
																<div class="modal fade" id="dataFileModal_1_<%=j %>">
																	<div class="modal-dialog modal-lg" role="document">
																		<div class="modal-content">
																			<div class="modal-body">
																				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">×</span>
																				</button>
																				<object type="<%=tempDTOList.get(k).files.get(j).fileType %>" data="data:<%=tempDTOList.get(k).files.get(j).fileType %>;base64,<% out.write(encode) ;%>" width="100%" height="500" style="height: 85vh;">No Support</object>
																			</div>
																		</div>
																	</div>
																</div>
					
															</td>
															<td><%=tempDTOList.get(k).files.get(j).fileTitle %></td>
															<td><%=tempDTOList.get(k).files.get(j).fileTag %></td>
														</tr>
														<% } %>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<% } %>
							
							</div><!-- END ACORDIAN CONTAINER -->
						</div>

					</div>
				</div>
			</div>
			
		</div>
	</form>
</div>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">




function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	return true;
}

function PostprocessAfterSubmiting(row)
{
}


function init(row)
{
}var row = 0;

	
window.onload =function ()
{
	init(row);
}





</script>

<script>
$(function(){
	$('.tab-pane table tbody tr').click(function(){
		
		//var link_data = $(this).data('link');
		
		//window.location = "InboxServlet?actionType=getInboxMainInternal&id="+link_data;
		
		//window.location = link_data;
	});
		
});
</script>





