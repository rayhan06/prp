<%@page import="category.CategoryDAO" %>
<%@page import="config.GlobalConfigConstants" %>

<%@page import="config.GlobalConfigurationRepository" %>
<%@page import="inbox.InboxDAO" %>
<%@page import="inbox.InboxDTO" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="inbox.InboxTabData" %>
<%@page import="inbox_status.Inbox_statusDAO" %>
<%@page import="inbox_tab_action.Inbox_tab_actionDAO" %>
<%@page import="inbox_tab_column.Inbox_tab_columnDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="oisf.OisfDAO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserDTO" %>
<%@page import="user.UserRepository" %>


<%@page import="user_organogram.User_organogramDAO" %>

<%@ page import="util.RecordNavigator" %>

<%@page import="java.util.ArrayList" %>

<%@page import="java.util.Hashtable" %>
<%@page import="java.util.List" %>


<%

    InboxDTO inboxDTO;
    inboxDTO = (InboxDTO) request.getAttribute("inboxDTO");

    if (inboxDTO == null) {
        inboxDTO = new InboxDTO();

    }

    Inbox_tab_actionDAO tempInbox_tab_actionDAO = new Inbox_tab_actionDAO();

    Inbox_tab_columnDTO inbox_tab_columnDTO;
    inbox_tab_columnDTO = (Inbox_tab_columnDTO) request.getAttribute("inbox_tab_columnDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    InboxDAO inboxDAO = (InboxDAO) request.getAttribute("inboxDAO");

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

//tab_data

    List<InboxTabData> tabDataList = (List<InboxTabData>) request.getAttribute("tab_data");


    if (tabDataList == null) {
        System.out.println("In jsp, tabDataList is null");
    } else {
        System.out.println("In jsp, tabDataList size = " + tabDataList.size());
        if (tabDataList.get(0).tabDTOList == null) {
            System.out.println("tabDataList.get(0).tabDTOList == null");
        } else {
            System.out.println("tabDataList.get(0).tabDTOList size = " + tabDataList.get(0).tabDTOList.size());
        }
    }

    int tempAjaxFlag = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.INBOX_DEFAULT_AJAX).value);//SessionConstants.DEFAULT_INBOX_ORGANOGRAM_ID;


    int tempTabIDInt = Integer.parseInt(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.INBOX_DEFAULT_TAB_ID).value);//SessionConstants.DEFAULT_INBOX_ORGANOGRAM_ID;

    if (request.getAttribute("tab_id") != null) {
        tempTabIDInt = (int) request.getAttribute("tab_id");
    }


    List<String> listSearch = new ArrayList();

    Hashtable listSearchType = new Hashtable();


//String[][] searchFieldInfo = new String[listSearch.size()-1][1];

    request.getSession(true).setAttribute("listSearch", listSearch);

    request.getSession(true).setAttribute("listSearchType", listSearchType);

//request.getSession(true).setAttribute("searchFieldInfo", searchFieldInfo);

//List<InboxTabData> tabDataList = inboxDAO.getTabData(loginDTO.userID, userDTO.userType);

    if (inbox_tab_columnDTO == null) {
        inbox_tab_columnDTO = new Inbox_tab_columnDTO();

    }
    System.out.println("inbox_tab_columnDTO = " + tempTabIDInt);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = "";//LM.getText(LC.INBOX_TAB_COLUMN_EDIT_INBOX_TAB_COLUMN_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = "";//LM.getText(LC.INBOX_TAB_COLUMN_ADD_INBOX_TAB_COLUMN_ADD_FORMNAME, loginDTO);
    }

    formTitle = LM.getText(LC.HM_INBOX, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";
    InboxDTO row = inboxDTO;
    String Language = "";//LM.getText(LC.INBOX_EDIT_LANGUAGE, loginDTO);

    String url = request.getParameter("url");
    String action = "InboxServlet?actionType=getInboxMain";////url;
    String navigator = "navigator";//SessionConstants.NAV_INBOX;//request.getParameter("navINBOX");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";

    RecordNavigator rn = (RecordNavigator) request.getSession(true).getAttribute("navigator");

    int totalpage2 = 0, temp_total_records = 0;

    long temp_search_time = 0;

    System.out.println("############################ rn 1 = ");

    if (rn == null) {
        System.out.println("############################ rn null = ");
        rn = new RecordNavigator();
    }

    if (rn != null) {
        System.out.println("############################ rn not null = ");
        totalpage2 = rn.getTotalPages();

        temp_total_records = rn.getTotalRecords();

        temp_search_time = rn.getSearchTime();
    }

    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    int pagination_number = 0;

    int page_size = rn.getPageSize();


    User_organogramDAO tempUser_organogramDAO = new User_organogramDAO();

    CategoryDAO tempCategory = new CategoryDAO();

    Inbox_statusDAO tempInbox_statusDAO = new Inbox_statusDAO();

    OisfDAO tempOisfDAO = new OisfDAO();

    request.getSession(true).setAttribute("navigator", rn);

%>

<!-- begin:: Subheader -->
<%--<div class="kt-subheader  kt-grid__item" id="kt_subheader">--%>
<%--    <div class="kt-subheader__main">--%>
<%--        <h3 class="kt-subheader__title">--%>
<%--            <i class="fa fa-gift"></i>--%>
<%--            <%=formTitle%>--%>
<%--        </h3>--%>
<%--    </div>--%>
<%--</div>--%>
<!-- end:: Subheader -->

<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.APPOINTMENT_SEARCH_APPOINTMENT_SEARCH_FORMNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="shadow-none border-0">
        <jsp:include page="./inboxMainNav.jsp" flush="true">
            <jsp:param name="url" value="<%=url%>"/>
            <jsp:param name="navigator" value="<%=navigator%>"/>
            <jsp:param name="pageName" value="<%=LM.getText(LC.HM_INBOX_SEARCH, loginDTO)%>"/>
            <jsp:param name="pagination_number" value="<%=pagination_number%>"/>
        </jsp:include>
        <div style="height: 1px; background: #ecf0f5"></div>
        <div class="kt-portlet shadow-none">
            <div class="kt-portlet__body">
                <form action="" method="POST" id="tableForm" enctype="multipart/form-data">
                    <jsp:include page="inboxMainForm.jsp" flush="true">
                        <jsp:param
                                name="pageName"
                                value="<%=LM.getText(LC.CATEGORY_SEARCH_CATEGORY_SEARCH_FORMNAME, loginDTO)%>"
                        />
                    </jsp:include>
                </form>
            </div>
        </div>
    </div>
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>


<script>
    $(function () {
        $('.tab-pane table tbody tr').click(function () {

            var link_data = $(this).data('link');

            //window.location = "InboxServlet?actionType=getInboxMainInternal&id="+link_data;

            window.location = link_data;
        });

    });

    $(document).ready(function () {
        dateTimeInit("<%=Language%>");
    });
    
    function hilite(tabId, otherTabId)
    {
    	$("#" + tabId).css("color", "#00a1d4");
    	$("#" + otherTabId).css("color", "#0bd398");
    }


</script>



