<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>


<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");

    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) request.getSession(true).getAttribute("navigator");
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.SUPPORT_TICKET_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>

<!-- search control -->
<div class="kt-content kt-portlet  kt-portlet--collapse"
     data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0 px-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px; margin-top: 1px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <%
                    out.println("<input placeholder='অনুসন্ধান করুন' autocomplete='off' type='text' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ANYFIELD, loginDTO) + "' ");
                    String value = "";

                    if (value != null) {
                        out.println("value = '" + value + "'");
                    }

                    out.println("/><br />");
                %>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
<%--        <div class="kt-portlet__head-toolbar">--%>
<%--            <div class="kt-portlet__head-group">--%>
<%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
<%--                     aria-hidden="true" x-placement="top"--%>
<%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
<%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
<%--                    <div class="tooltip-inner">Collapse</div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
    </div>
    <div class="kt-portlet__body px-0" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label control-label">
                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='ticket_issues_type'
                                    onchange='getSubType("ticket_issues_subtype_type", this.value)'
                                    id='ticket_issues_type'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CommonDAO.getOptions(Language, "select", "ticket_issues", "ticketIssuesType_select_" + row, "form-control", "ticketIssuesType", "any");
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label control-label">
                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESSUBTYPETYPE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='ticket_issues_subtype_type'
                                    id='ticket_issues_subtype_type'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CommonDAO.getOptions(Language, "select", "ticket_issues_subtype", "ticketIssuesSubtypeType_select_" + row, "form-control", "ticketIssuesSubtypeType", "any");
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label control-label">
                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETSTATUSCAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='ticket_status_cat' id='ticket_status_cat'
                                    onSelect='setSearchChanged()'>
                                <option value='-1'></option>
                                <%

                                    Options = CatDAO.getOptions(Language, "ticket_status", -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label control-label">
                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_PRIORITYCAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='priority_cat' id='priority_cat'
                                    onSelect='setSearchChanged()'>
                                <option value='-1'></option>
                                <%

                                    Options = CatDAO.getOptions(Language, "priority", -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label control-label">
                            <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.HM_ARRIVAL_DATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="arrival_date_start_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input
                                    type="hidden"
                                    class="form-control"
                                    id="arrival_date_start"
                                    placeholder=""
                                    name="arrival_date_start"
                                    onChange='setSearchChanged()'
                            >
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label control-label">
                            <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.HM_ARRIVAL_DATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="arrival_date_end_js"></jsp:param>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                            </jsp:include>
                            <input
                                    type="hidden"
                                    class="form-control formRequired datepicker"
                                    id="arrival_date_end"
                                    placeholder="" name="arrival_date_end"
                                    onChange='setSearchChanged()'
                            >
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-right">
                <input type="hidden" name="search" value="yes"/>
                <button type="submit"
                        class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                        onclick="allfield_changed('',0)"
                        style="background-color: #00a1d4;">
                    <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                </button>
            </div>
        </div>
    </div>
</div>

<%@include file="../common/pagination_with_go2.jsp" %>
<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script type="text/javascript">

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                //setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        console.log(params);

        xhttp.open("Get", "InboxServlet?actionType=getInboxMain&ajax=1&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#ticket_issues_type").val() != -1) {
            params += '&ticket_issues_type=' + $("#ticket_issues_type").val();
        }
        if ($("#ticket_issues_subtype_type").val() != -1) {
            params += '&ticket_issues_subtype_type=' + $("#ticket_issues_subtype_type").val();
        }

        if ($("#ticket_status_cat").val() != -1) {
            params += '&ticket_status_cat=' + $("#ticket_status_cat").val();
        }

        if ($("#priority_cat").val() != -1) {
            params += '&priority_cat=' + $("#priority_cat").val();
        }
        $("#arrival_date_start").val(getDateStringById('arrival_date_start_js', 'DD/MM/YYYY'));
        params += '&arrival_date_start=' + getBDFormattedDate('arrival_date_start');
        $("#arrival_date_end").val(getDateStringById('arrival_date_end_js', 'DD/MM/YYYY'));
        params += '&arrival_date_end=' + getBDFormattedDate('arrival_date_end');

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

    function getSubType(childElement, value) {
        console.log("getting Element " + value);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById(childElement).innerHTML = this.responseText;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        console.log("selected value = " + value);

        xhttp.open("POST", "Support_ticketServlet?actionType=getSubType&type="
            + value + "&language=<%=Language%>", true);
        xhttp.send();


    }

</script>

<!-- search control -->
<%--<div class="portlet box portlet-btcl">--%>
<%--    <div class="portlet-title">--%>
<%--        &lt;%&ndash;        <div class="caption" style="margin-top: 5px;">&ndash;%&gt;--%>
<%--        &lt;%&ndash;            <i class="fa fa-search-plus" style="margin-top:-3px"></i>&ndash;%&gt;--%>
<%--        &lt;%&ndash;            <%=pageName%>&ndash;%&gt;--%>
<%--        &lt;%&ndash;        </div>&ndash;%&gt;--%>
<%--        <p class="desktop-only" style="float:right; margin:10px 5px !important;">Advanced Search</p>--%>
<%--        <div class="tools">--%>
<%--            <a class="expand" href="javascript:;" data-original-title="" title=""></a>--%>
<%--        </div>--%>
<%--        <div class="col-xs-12 col-sm-5 col-md-4" style="margin-top:10px">--%>
<%--            &lt;%&ndash;            &lt;%&ndash;%>--%>

<%--            &lt;%&ndash;                out.println("<input type='text' class='form-control' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.SUPPORT_TICKET_SEARCH_ANYFIELD, loginDTO) + "' ");&ndash;%&gt;--%>
<%--            &lt;%&ndash;                String value = "";&ndash;%&gt;--%>

<%--            &lt;%&ndash;                if (value != null) {&ndash;%&gt;--%>
<%--            &lt;%&ndash;                    out.println("value = '" + value + "'");&ndash;%&gt;--%>
<%--            &lt;%&ndash;                }&ndash;%&gt;--%>

<%--            &lt;%&ndash;                out.println("/><br />");&ndash;%&gt;--%>
<%--            &lt;%&ndash;            %>&ndash;%&gt;--%>
<%--        </div>--%>
<%--    </div>--%>
<%--    <div class="portlet-body form collapse">--%>
<%--        <!-- BEGIN FORM-->--%>
<%--        <div class="container-fluid">--%>
<%--            <div class="row col-lg-offset-1">--%>
<%--                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--                    <div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--                        &lt;%&ndash;                        <label for="" class="control-label pull-right">&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESTYPE, loginDTO)%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </label>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                    <div class="col-xs-10 col-sm-8 col-md-8">--%>
<%--                        &lt;%&ndash;                        <select class='form-control' name='ticket_issues_type'&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                                onchange='getSubType("ticket_issues_subtype_type", this.value)' id='ticket_issues_type'&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                                onSelect='setSearchChanged()'>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            &lt;%&ndash;%>--%>
<%--                        &lt;%&ndash;                                Options = CommonDAO.getOptions(Language, "select", "ticket_issues", "ticketIssuesType_select_" + row, "form-control", "ticketIssuesType", "any");&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            %>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=Options%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </select>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--                    <div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--                        &lt;%&ndash;                        <label for=""&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                               class="control-label pull-right">&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETISSUESSUBTYPETYPE, loginDTO)%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </label>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                    <div class="col-xs-10 col-sm-8 col-md-8">--%>
<%--                        &lt;%&ndash;                        <select class='form-control' name='ticket_issues_subtype_type' id='ticket_issues_subtype_type'&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                                onSelect='setSearchChanged()'>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            &lt;%&ndash;%>--%>
<%--                        &lt;%&ndash;                                Options = CommonDAO.getOptions(Language, "select", "ticket_issues_subtype", "ticketIssuesSubtypeType_select_" + row, "form-control", "ticketIssuesSubtypeType", "any");&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            %>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=Options%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </select>&ndash;%&gt;--%>

<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--                    <div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--                        &lt;%&ndash;                        <label for=""&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                               class="control-label pull-right">&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_TICKETSTATUSCAT, loginDTO)%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </label>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                    <div class="col-xs-10 col-sm-8 col-md-8">--%>
<%--                        &lt;%&ndash;                        <select class='form-control' name='ticket_status_cat' id='ticket_status_cat'&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                                onSelect='setSearchChanged()'>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <option value='-1'></option>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            &lt;%&ndash;%>--%>

<%--                        &lt;%&ndash;                                Options = CatDAO.getOptions(Language, "ticket_status", -1);&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            %>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=Options%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </select>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--                    <div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--                        &lt;%&ndash;                        <label for=""&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                               class="control-label pull-right">&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=LM.getText(LC.SUPPORT_TICKET_ADD_PRIORITYCAT, loginDTO)%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </label>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                    <div class="col-xs-10 col-sm-8 col-md-8">--%>
<%--                        &lt;%&ndash;                        <select class='form-control' name='priority_cat' id='priority_cat'&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                                onSelect='setSearchChanged()'>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <option value='-1'></option>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            &lt;%&ndash;%>--%>

<%--                        &lt;%&ndash;                                Options = CatDAO.getOptions(Language, "priority", -1);&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            %>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=Options%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </select>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--                    <div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--                        &lt;%&ndash;                        <label for=""&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                               class="control-label pull-right">&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.HM_ARRIVAL_DATE, loginDTO)%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </label>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                    <div class="col-xs-10 col-sm-8 col-md-8">--%>
<%--                        &lt;%&ndash;                        <jsp:include page="/date/date.jsp">&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <jsp:param name="DATE_ID" value="arrival_date_start_js"></jsp:param>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </jsp:include>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        <input type="hidden" class="form-control" id="arrival_date_start" placeholder=""&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                               name="arrival_date_start" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-top: 5px;">--%>
<%--                    <div class="col-xs-2 col-sm-4 col-md-4">--%>
<%--                        &lt;%&ndash;                        <label for=""&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                               class="control-label pull-right">&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.HM_ARRIVAL_DATE, loginDTO)%>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </label>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                    <div class="col-xs-10 col-sm-8 col-md-8">--%>
<%--                        &lt;%&ndash;                        <jsp:include page="/date/date.jsp">&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <jsp:param name="DATE_ID" value="arrival_date_end_js"></jsp:param>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                            <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        </jsp:include>&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                        <input type="hidden" class="form-control formRequired datepicker" id="arrival_date_end"&ndash;%&gt;--%>
<%--                        &lt;%&ndash;                               placeholder="" name="arrival_date_end" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <div class=clearfix></div>--%>
<%--            <div class="form-actions fluid" style="margin-top:10px">--%>
<%--                <div class="container-fluid">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-lg-offset-3 col-xs-12 col-md-12  col-md-12 col-lg-9">--%>
<%--                            <div class="col-xs-4  col-sm-4  col-md-6">--%>
<%--                                <input type="hidden" name="search" value="yes"/>--%>
<%--                                <!-- 				          	<input type="reset" class="btn  btn-sm btn btn-circle  grey-mint btn-outline sbold uppercase" value="Reset" > -->--%>
<%--                                <input--%>
<%--                                        type="submit"--%>
<%--                                        onclick="allfield_changed('',0)"--%>
<%--                                        class="btn  btn-sm btn btn-circle btn-sm green-meadow btn-outline sbold uppercase advanceseach"--%>
<%--                                        value="<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>"--%>
<%--                                >--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>

<%--        </div>--%>
<%--        <!-- END FORM-->--%>
<%--    </div>--%>
<%--</div>--%>

