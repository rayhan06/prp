<%@page import="user.UserDTO" %>
<%@page import="user.UserRepository" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="permission.MenuConstants" %>
<%@page import="role.PermissionRepository" %>
<%@page import="util.ActionTypeConstant" %>
<html:base/>


<%


    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO loginUserDTO = UserRepository.getUserDTOByUserID(loginDTO);

    boolean hasTemplateUpdatePermission = PermissionRepository.checkPermissionByRoleIDAndMenuID(loginUserDTO.roleID, MenuConstants.REPORT_PERFORMANCE_LOG_TEMPLATE_UPDATE);


%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    Report Generator
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <form class="form-horizontal" id="ReportForm"
                  action="<%=request.getContextPath()%>/Report/performanceLogServlet?actionType=<%=ActionTypeConstant.REPORT_RESULT%>">
                <div class="portlet box portlet-btcl">
                    <div class="portlet-body form">

                        <input type="hidden" id="countURL" name="countURL"
                               value="Report/performanceLogServlet?actionType=<%=ActionTypeConstant.REPORT_COUNT%>">
                        <div class="form-body">
                            <div class="row" <%=(hasTemplateUpdatePermission ? "" : "style=\"display:none\"")%>>
                                <div class="col-md-6">
                                    <div class="col-md-4">
                                        <button id="save-template-button" type="button" class="btn blue">
                                            <i class="fa fa-save"></i> Save
                                        </button>
                                    </div>

                                    <div class="col-md-5">
                                        <label class="checkbox">
								<span>
									<input id="isSinglePageReport" type="checkbox" class="input-checkbox-display"
                                           name="isSinglePageReport" value="1">
								</span>Single Page Report
                                        </label>
                                    </div>
                                </div>


                            </div>
                            <div class="row" <%=(hasTemplateUpdatePermission ? "" : "style=\"display:none\"")%>>
                                <hr>
                            </div>
                            <div class="row" <%=(hasTemplateUpdatePermission ? "" : "style=\"display:none\"")%> >
                                <div class="col-md-4" id="criteria"></div>
                                <div class="col-md-4" id="display">
                                    <%@include file="reportPerformanceLogDisplayDiv.jsp" %>
                                </div>
                                <div class="col-md-4" id="orderby"></div>
                            </div>
                            <div id="searchCriteria"></div>

                            <div class="custom-form-action">
                                <div class="mt-5">
                                    <div class="text-center">
                                        <button type="reset" class="btn btn-sm text-white cancel-btn shadow btn-border-radius">Reset</button>
                                        <button id="defaultLoad" type="submit" class="btn  btn-sm text-white submit-btn shadow btn-border-radius ml-2">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="portlet box portlet-btcl light navigator">
                    <div class="portlet-body">
                        <div class="row text-center">
                            <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0px;">
                                    <li style="float:left;"><i class="hidden-xs">Record Per Page</i>&nbsp;&nbsp;
                                        <input type="text" class="custom-form-control  black-text"
                                               name="RECORDS_PER_PAGE"
                                               style="height: 34px;" placeholder="" value="100"/>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </li>
                                    <li class="page-item"><a class="page-link" href="" id="firstLoad"
                                                             aria-label="First" title="Left"> <i
                                            class="fa fa-angle-double-left" aria-hidden="true"></i> <span
                                            class="sr-only">First</span>
                                    </a></li>
                                    <li class="page-item"><a class="page-link" id="previousLoad"
                                                             href="" aria-label="Previous" title="Previous"> <i
                                            class="fa fa-angle-left" aria-hidden="true"></i> <span
                                            class="sr-only">Previous</span>
                                    </a></li>

                                    <li class="page-item"><a class="page-link" href="" id="nextLoad"
                                                             aria-label="Next" title="Next"> <i
                                            class="fa fa-angle-right"
                                            aria-hidden="true"></i> <span
                                            class="sr-only">Next</span>
                                    </a></li>
                                    <li class="page-item"><a class="page-link" href="" id="lastLoad"
                                                             aria-label="Last" title="Last"> <i
                                            class="fa fa-angle-double-right" aria-hidden="true"></i> <span
                                            class="sr-only">Last</span>
                                    </a></li>
                                    <li>&nbsp;&nbsp;<i class="hidden-xs">Page </i><input
                                            type="text" class="custom-form-control  black-text" name="pageno" value=''
                                            style="height: 34px;"
                                            size="15"> <i class="hidden-xs">of</i>&nbsp;&nbsp;<input
                                            type="text" class="custom-form-control  black-text" name="tatalPageNo"
                                            value=''
                                            style="height: 34px;"
                                            size="15" disabled> <input type="hidden" name="totalRecord"><input
                                            type="submit"
                                            id="forceLoad"
                                            class="btn btn-success"
                                            value="GO"/>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </form>
            <div>
                <h5 class="table-title mt-5">Report</h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="reportTable"
                           style="width: 100%">

                        <thead>
                        <tr>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




