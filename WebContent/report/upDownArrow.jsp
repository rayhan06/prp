<span class="up-down-link d-flex align-items-center">
	<a class="up-link"><span><i class="fa fa-arrow-circle-up"></i></span></a>
	<a class="down-link ml-2"><span><i class="fa fa-arrow-circle-down"></i></span></a>
</span>