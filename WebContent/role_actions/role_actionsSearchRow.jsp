<%@page pageEncoding="UTF-8" %>

<%@page import="role_actions.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ROLE_ACTIONS_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ROLE_ACTIONS;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Role_actionsDTO role_actionsDTO = (Role_actionsDTO) request.getAttribute("role_actionsDTO");
    CommonDTO commonDTO = role_actionsDTO;
    String servletName = "Role_actionsServlet";


    System.out.println("role_actionsDTO = " + role_actionsDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Role_actionsDAO role_actionsDAO = (Role_actionsDAO) request.getAttribute("role_actionsDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_roleType'>
    <%
        value = role_actionsDTO.roleType + "";
    %>
    <%
        value = CommonDAO.getName(Integer.parseInt(value), "role", "roleName", "id");
    %>

    <%=value%>


</td>


<td>

    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;" onclick="location.href='Role_actionsServlet?actionType=view&ID=<%=role_actionsDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>

    <button type="button" class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;" onclick="location.href='Role_actionsServlet?actionType=getEditPage&ID=<%=role_actionsDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span id='chkEdit'><input type='checkbox' name='ID' value='<%=role_actionsDTO.iD%>'/></span>
    </div>
</td>
																						
											

