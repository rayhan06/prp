<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="role_actions.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.ROLE_ACTIONS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Role_actionsDAO role_actionsDAO = new Role_actionsDAO("role_actions");
    Role_actionsDTO role_actionsDTO = (Role_actionsDTO) role_actionsDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    Role Actions Details
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-success app_register"
                               data-id="419637"> Register </a>
                        </div>
                        <div class="col-md-6">
                            <a href="javascript:" style="display: none" class="btn btn-danger app_reject"
                               data-id="419637"> Reject </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body mt-5">
                <div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5>Role Actions</h5>
                    </div>


                    <div class="col-md-6">
                        <label class="col-md-5"
                               style="padding-right: 0px;"><b><span><%=LM.getText(LC.ROLE_ACTIONS_EDIT_ROLETYPE, loginDTO)%></span><span
                                style="float:right;">:</span></b></label>
                        <label id="roleType">

                            <%
                                value = role_actionsDTO.roleType + "";
                            %>
                            <%
                                value = CommonDAO.getName(Integer.parseInt(value), "role", Language.equals("English") ? "name_en" : "name_bn", "id");
                            %>

                            <%=value%>


                        </label>
                    </div>


                </div>
                <div class="row div_border attachement-div mt-5">
                    <div class="col-md-12">
                        <h5 class="table-title">Action Description</h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th><%=(actionName.equals("edit")) ? (LM.getText(LC.ROLE_ACTIONS_EDIT_ACTION_DESCRIPTION_NAMEEN, loginDTO)) : (LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION_NAMEEN, loginDTO))%>
                                    </th>
                                    <th><%=(actionName.equals("edit")) ? (LM.getText(LC.ROLE_ACTIONS_EDIT_ACTION_DESCRIPTION_NAMEBN, loginDTO)) : (LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION_NAMEBN, loginDTO))%>
                                    </th>
                                    <th><%=(actionName.equals("edit")) ? (LM.getText(LC.ROLE_ACTIONS_EDIT_ACTION_DESCRIPTION_URL, loginDTO)) : (LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION_URL, loginDTO))%>
                                    </th>
                                </tr>
                                <%
                                    ActionDescriptionDAO actionDescriptionDAO = new ActionDescriptionDAO();
                                    List<ActionDescriptionDTO> actionDescriptionDTOs = actionDescriptionDAO.getActionDescriptionDTOListByRoleActionsID(role_actionsDTO.iD);

                                    for (ActionDescriptionDTO actionDescriptionDTO : actionDescriptionDTOs) {
                                %>
                                <tr>
                                    <td>
                                        <%
                                            value = actionDescriptionDTO.nameEn + "";
                                        %>

                                        <%=value%>


                                    </td>
                                    <td>
                                        <%
                                            value = actionDescriptionDTO.nameBn + "";
                                        %>

                                        <%=value%>


                                    </td>
                                    <td>
                                        <%
                                            value = actionDescriptionDTO.url + "";
                                        %>
                                        <%=value%>


                                    </td>
                                </tr>
                                <%

                                    }

                                %>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

