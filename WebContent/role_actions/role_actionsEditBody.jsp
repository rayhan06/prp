<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="role_actions.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>


<%
    Role_actionsDTO role_actionsDTO;
    role_actionsDTO = (Role_actionsDTO) request.getAttribute("role_actionsDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (role_actionsDTO == null) {
        role_actionsDTO = new Role_actionsDTO();

    }
    System.out.println("role_actionsDTO = " + role_actionsDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.ROLE_ACTIONS_EDIT_ROLE_ACTIONS_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.ROLE_ACTIONS_ADD_ROLE_ACTIONS_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    String Language = LM.getText(LC.ROLE_ACTIONS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Role_actionsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=role_actionsDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.ROLE_ACTIONS_EDIT_ROLETYPE, loginDTO)) : (LM.getText(LC.ROLE_ACTIONS_ADD_ROLETYPE, loginDTO))%>
                                        </label>
                                        <div class="col-md-8" id='roleType_div_<%=i%>'>
                                            <select class='form-control' name='roleType' id='roleType_select_<%=i%>' tag='pb_html'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        Options = CommonDAO.getOptionsWithDefault("role", "roleName", role_actionsDTO.roleType);
                                                    } else {
                                                        Options = CommonDAO.getOptionsWithTableName("role", "roleName");
                                                    }
                                                %>
                                                <%=Options%>
                                            </select>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + role_actionsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>


                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + role_actionsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><%=(actionName.equals("edit")) ? (LM.getText(LC.ROLE_ACTIONS_EDIT_ACTION_DESCRIPTION_NAMEEN, loginDTO)) : (LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION_NAMEEN, loginDTO))%>
                                </th>
                                <th><%=(actionName.equals("edit")) ? (LM.getText(LC.ROLE_ACTIONS_EDIT_ACTION_DESCRIPTION_NAMEBN, loginDTO)) : (LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION_NAMEBN, loginDTO))%>
                                </th>
                                <th><%=(actionName.equals("edit")) ? (LM.getText(LC.ROLE_ACTIONS_EDIT_ACTION_DESCRIPTION_URL, loginDTO)) : (LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION_URL, loginDTO))%>
                                </th>
                                <th><%=LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-ActionDescription">


                            <%
                                if (actionName.equals("edit")) {
                                    int index = -1;


                                    for (ActionDescriptionDTO actionDescriptionDTO : role_actionsDTO.actionDescriptionDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>

                            <tr id="ActionDescription_<%=index + 1%>">
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='actionDescription.iD'
                                           id='iD_hidden_<%=childTableStartingID%>' value='<%=actionDescriptionDTO.iD%>'
                                           tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='actionDescription.roleActionsId'
                                           id='roleActionsId_hidden_<%=childTableStartingID%>'
                                           value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.roleActionsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='text' class='form-control' name='actionDescription.nameEn'
                                           id='nameEn_text_<%=childTableStartingID%>'
                                           value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='text' class='form-control' name='actionDescription.nameBn'
                                           id='nameBn_text_<%=childTableStartingID%>'
                                           value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='text' class='form-control' name='actionDescription.url'
                                           id='url_url_<%=childTableStartingID%>'
                                           value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.url + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='actionDescription.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='actionDescription.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                </td>
                                <td>
                                    <div class='checker'><span id='chkEdit'><input type='checkbox'
                                                                                   id='actionDescription_cb_<%=index%>'
                                                                                   name='checkbox' value=''/></span>
                                    </div>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>

                            </tbody>
                        </table>


                    </div>
                    <div class="form-group">
                        <div class="text-right">

                            <button id="add-more-ActionDescription" name="add-moreActionDescription" type="button" class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.ROLE_ACTIONS_ADD_ACTION_DESCRIPTION_ADD_MORE, loginDTO)%>
                            </button>

                            <button id="remove-ActionDescription" name="removeActionDescription" type="button" class="btn btn-sm remove-btn shadow ml-2 pl-4 remove-me1">
                                <i class="fa fa-trash"></i>
                            </button>

                        </div>
                    </div>
                    <%ActionDescriptionDTO actionDescriptionDTO = new ActionDescriptionDTO();%>
                    <template id="template-ActionDescription">
                        <tr>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='actionDescription.iD' id='iD_hidden_'
                                       value='<%=actionDescriptionDTO.iD%>' tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='actionDescription.roleActionsId'
                                       id='roleActionsId_hidden_'
                                       value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.roleActionsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                            </td>
                            <td>


                                <input type='text' class='form-control' name='actionDescription.nameEn'
                                       id='nameEn_text_'
                                       value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.nameEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                            </td>
                            <td>


                                <input type='text' class='form-control' name='actionDescription.nameBn'
                                       id='nameBn_text_'
                                       value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.nameBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                            </td>
                            <td>


                                <input type='text' class='form-control' name='actionDescription.url' id='url_url_'
                                       value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.url + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='actionDescription.isDeleted'
                                       id='isDeleted_hidden_'
                                       value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='actionDescription.lastModificationTime'
                                       id='lastModificationTime_hidden_'
                                       value=<%=actionName.equals("edit")?("'" + actionDescriptionDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                            </td>
                            <td>
                                <div><span id='chkEdit'><input type='checkbox' name='checkbox' value=''/></span></div>
                            </td>
                        </tr>

                    </template>
                </div>
                <div class="form-actions text-center">
                    <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius" href="<%=request.getHeader("referer")%>">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%=LM.getText(LC.ROLE_ACTIONS_EDIT_ROLE_ACTIONS_CANCEL_BUTTON, loginDTO)%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.ROLE_ACTIONS_ADD_ROLE_ACTIONS_CANCEL_BUTTON, loginDTO)%>
                        <%
                            }

                        %>
                    </a>
                    <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2" type="submit">
                        <%
                            if (actionName.equals("edit")) {
                        %>
                        <%=LM.getText(LC.ROLE_ACTIONS_EDIT_ROLE_ACTIONS_SUBMIT_BUTTON, loginDTO)%>
                        <%
                        } else {
                        %>
                        <%=LM.getText(LC.ROLE_ACTIONS_ADD_ROLE_ACTIONS_SUBMIT_BUTTON, loginDTO)%>
                        <%
                            }
                        %>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>



<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


    $(document).ready(function () {

        $('.datepicker').datepicker({

            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            todayHighlight: true,
            showButtonPanel: true
        });
    });

    function PreprocessBeforeSubmiting(row, validate) {
        if (validate == "report") {
        } else {
            var empty_fields = "";
            var i = 0;


            if (empty_fields != "") {
                if (validate == "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        for (i = 1; i < child_table_extra_id; i++) {
        }
        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Role_actionsServlet");
    }

    function init(row) {


        for (i = 1; i < child_table_extra_id; i++) {
        }

    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-ActionDescription").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-ActionDescription");

            $("#field-ActionDescription").append(t.html());
            SetCheckBoxValues("field-ActionDescription");

            var tr = $("#field-ActionDescription").find("tr:last-child");

            tr.attr("id", "ActionDescription_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });


            child_table_extra_id++;

        });


    $("#remove-ActionDescription").click(function (e) {
        var tablename = 'field-ActionDescription';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


</script>






