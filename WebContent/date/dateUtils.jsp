
<%--
  Created by IntelliJ IDEA.
  User: Jahin
  Date: 3/8/2021
  Time: 11:55 AM
--%>
<%@page pageEncoding="UTF-8" %>

<script type="text/javascript">

    const dateBn = ['তারিখ', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '১০', '১১', '১২', '১৩', '১৪', '১৫', '১৬', '১৭', '১৮', '১৯', '২০', '২১', '২২', '২৩', '২৪', '২৫', '২৬', '২৭', '২৮', '২৯', '৩০', '৩১']
    const shortMonthEn = {
        '1': 'Jan',
        '2': 'Feb',
        '3': 'Mar',
        '4': 'Apr',
        '5': 'May',
        '6': 'Jun',
        '7': 'Jul',
        '8': 'Aug',
        '9': 'Sep',
        '10': 'Oct',
        '11': 'Nov',
        '12': 'Dec'
    };
    const monthEn = {
        '1': 'January',
        '2': 'February',
        '3': 'March',
        '4': 'April',
        '5': 'May',
        '6': 'June',
        '7': 'July',
        '8': 'August',
        '9': 'September',
        '10': 'October',
        '11': 'November',
        '12': 'December'
    };
    const monthBn = {
        '১': "জানুয়ারী",
        '২': "ফেব্রুয়ারী",
        '৩': "মার্চ",
        '৪': "এপ্রিল",
        '৫': "মে",
        '৬': "জুন",
        '৭': "জুলাই",
        '৮': "আগস্ট",
        '৯': "সেপ্টেম্বর",
        '১০': "অক্টোবর",
        '১১': "নভেম্বর",
        '১২': "ডিসেম্বর"
    };

    //strictValidation inforces validation on each field selection
    //pass error message in obj as 'errorEn' and 'errorBn'
    function convDateStrToBn(s) {
        let str = s.toString();
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    function dateValidator(dateId, strictValidation = false, obj = undefined) {
        const language = $('#language_' + dateId).prop('value');
        const hideDay = $('#hide_day_' + dateId).prop('value').localeCompare('true') == 0;
        let error = '';
        let errorEn = 'Invalid Date!', errorBn = 'অবৈধ তারিখ!';
        if (obj !== undefined) {
            errorEn = obj.errorEn;
            errorBn = obj.errorBn;
        }
        let day = 1;
        if (!hideDay) day = parseInt($('#' + dateId).find('.daySelection').prop('value'));
        const month = parseInt($('#' + dateId).find('.monthSelection').prop('value'));
        const year = parseInt($('#' + dateId).find('.yearSelection').prop('value'));
        const curdate = new Date(year, month - 1, day);
        if (strictValidation && (day == 0 || month == 0 || year == 0)) {
            error = language.localeCompare('English') == 0 ? errorEn : errorBn;
            $('#error' + dateId).text(error);
            return false;
        }
        if (day != curdate.getDate() && ((day > 0 && month > 0 && (month != 2 || year > 0)))) {
            error = language.localeCompare('English') == 0 ? errorEn : errorBn;
            $('#error' + dateId).text(error);
            return false;
        } else {
            $('#error' + dateId).empty();
            return true;
        }
    }

    function refreshDateOnMinMax(id) {
        var hideDay = false;
        if ($('#hide_day_' + id).length) {
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        }
        let maxDate; // = new Date(parseInt($('#max_date_'+id).prop('value')));
        if ($('#max_date_' + id).prop('value') == "") {
            maxDate = new Date();
            maxDate.setFullYear((parseInt($('#end_year_' + id).prop('value')) + 1));
        } else {
            maxDate = new Date(parseInt($('#max_date_' + id).prop('value')));
        }

        let minDate; // = new Date(parseInt($('#min_date_'+id).prop('value')));
        if ($('#min_date_' + id).prop('value') == "") {
            minDate = new Date();
            minDate.setFullYear((parseInt($('#start_year_' + id).prop('value')) - 1));
        } else {
            minDate = new Date(parseInt($('#min_date_' + id).prop('value')));
        }

        // for(let x = maxDate.getFullYear()+1; x <= parseInt($('#end_year_'+id).val()); x++){
        //     $("#yearSelection"+id+" option[value*='"+x.toString()+"']").prop('disabled',true);
        //     if($("#yearSelection"+id).prop('value') == x.toString()){
        //         document.getElementById('yearSelection'+id).value = 0;
        //         $('#yearSelection'+id).css('border-color', 'red');
        //     }
        // }
        // for(let x = maxDate.getFullYear(); x >= minDate.getFullYear(); x--){
        //     $("#yearSelection"+id+" option[value*='"+x.toString()+"']").prop('disabled',false);
        // }
        // for(let x = minDate.getFullYear()-1; x >= parseInt($('#start_year_'+id).val()); x--){
        //     $("#yearSelection"+id+" option[value*='"+x.toString()+"']").prop('disabled',true);
        //     if($("#yearSelection"+id).prop('value') == x.toString()){
        //         document.getElementById('yearSelection'+id).value = 0;
        //         $('#yearSelection'+id).css('border-color', 'red');
        //     }
        // }
        for (let x = 0; x <= 12; x++) {
            // $($("#monthSelection" + id + " option")[x]).prop('disabled', false);

            $('#monthSelection' + id + ' option[value="' + x + '"]').removeAttr('disabled');
        }
        for (let x = 0; x <= 31; x++) {
            // $($("#daySelection" + id + " option")[x]).prop('disabled', false);
            $('#daySelection' + id + ' option[value="' + x + '"]').removeAttr('disabled');

        }
        if (maxDate.getFullYear() == parseInt($('#yearSelection' + id).val())) {
            for (let x = maxDate.getMonth() + 2; x <= 12; x++) {
                // $($("#monthSelection" + id + " option")[x]).prop('disabled', true);
                $('#monthSelection' + id + ' option[value="' + x + '"]').attr('disabled', 'disabled');

                if ($("#monthSelection" + id).prop('value') == x.toString()) {
                    // document.getElementById('monthSelection' + id).value = 0;
                    $('#monthSelection' + id).val(0).trigger('change.select2');
                    // $('#monthSelection' + id).select2("val", 0);
                    $('#monthSelection' + id).css('border-color', 'red');
                }
            }
            if (!hideDay && maxDate.getMonth() + 1 == parseInt($('#monthSelection' + id).val())) {
                for (let x = maxDate.getDate() + 1; x <= 31; x++) {
                    // $($("#daySelection" + id + " option")[x]).prop('disabled', true);
                    $('#daySelection' + id + ' option[value="' + x + '"]').attr('disabled', 'disabled');

                    if ($("#daySelection" + id).prop('value') == x.toString()) {
                        // document.getElementById('daySelection' + id).value = 0;
                        $('#daySelection' + id).val(0).trigger('change.select2');
                        // $('#daySelection' + id).select2("val", 0);
                        $('#daySelection' + id).css('border-color', 'red');
                    }
                }
            }
        }
        if (minDate.getFullYear() == parseInt($('#yearSelection' + id).val())) {
            for (let x = minDate.getMonth(); x > 0; x--) {
                // $($("#monthSelection" + id + " option")[x]).prop('disabled', true);
                $('#monthSelection' + id + ' option[value="' + x + '"]').attr('disabled', 'disabled');

                if ($("#monthSelection" + id).prop('value') == x.toString()) {
                    document.getElementById('monthSelection' + id).value = 0;
                    $('#monthSelection' + id).val(0).trigger('change.select2');
                    // $('#monthSelection' + id).select2("val", 0);
                    $('#monthSelection' + id).css('border-color', 'red');
                }
            }


            if (!hideDay && minDate.getMonth() + 1 == parseInt($('#monthSelection' + id).val())) {
                for (let x = minDate.getDate() - 1; x > 0; x--) {
                    // $($("#daySelection" + id + " option")[x]).prop('disabled', true);
                    $('#daySelection' + id + ' option[value="' + x + '"]').attr('disabled', 'disabled');

                    if ($("#daySelection" + id).prop('value') == x.toString()) {
                        // document.getElementById('daySelection' + id).value = 0;
                        $('#daySelection' + id).val(0).trigger('change.select2');
                        // $('#daySelection' + id).select2("val", 0);
                        $('#daySelection' + id).css('border-color', 'red');
                    }
                }


            }
        }

        reDrawDatePickerSelect2(id);
    }

    function reDrawDatePickerSelect2(id) {
        const language = $('#language_' + id).prop('value');
        select2SingleSelector('#yearSelection' + id, language);
        select2SingleSelector('#monthSelection' + id, language);
        select2SingleSelector('#daySelection' + id, language);
    }

    function getDateStringById(id, format) {
        var hideDay = false;
        if ($('#hide_day_' + id).length) {
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        }
        if (format === undefined || format.length == 0) format = 'DD/MM/YYYY';
        let day = 1;
        if (!hideDay) day = $('#daySelection' + id).prop('value');
        let month = $('#monthSelection' + id).prop('value');
        const year = $('#yearSelection' + id).prop('value');
        const curdate = new Date(year, parseInt(month) - 1, day);
        let date = '';
        if (parseInt(day) > 0 && parseInt(month) > 0 && parseInt(year) > 0) {
            date = format.replace('DD', curdate.getDate() < 10 ? ('0' + day) : day);
            date = date.replace('YYYY', year);
            date = date.replace('YY', year.substring(year.length - 2));
            date = date.replace('MMMM', monthEn[month]);
            date = date.replace('MMM', shortMonthEn[month]);
            date = date.replace('MM', curdate.getMonth() + 1 < 10 ? ('0' + month) : month);
        }
        return date;
    }

    function setDateByStringAndId(id, date, delim = '/') {
    	console.log("setDateByStringAndId with date = " + date + " id = " + id);
        var hideDay = false;
        if ($('#hide_day_' + id).length) {
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        }
        const day = (hideDay ? 1 : date.split(delim)[0]);
        const month = date.split(delim)[1];
        const year = date.split(delim)[2];
        console.log("year = " + year);
        const curdate = new Date(year, parseInt(month) - 1, day);
        if ($("#yearSelection" + id + " option:contains('" + year + "')").length == 0) {
        	console.log("selecting year");
            $("#yearSelection" + id).append(new Option(year, year)).trigger('change.select2');
        }

        $('#monthSelection' + id).val((curdate.getMonth() + 1).toString()).trigger('change.select2');
        // $('#monthSelection' + id).select2("val", (curdate.getMonth() + 1).toString());
        $('#yearSelection' + id).val(year).trigger('change.select2');
        // $('#yearSelection' + id).select2("val", curdate.getFullYear().toString());
        if (!hideDay) {
            $('#daySelection' + id).val(curdate.getDate().toString()).trigger('change.select2');
            // $('#daySelection' + id).select2("val", curdate.getDate().toString());
        }
    }

    function getDateTimestampById(id) {
        var hideDay = false;
        if ($('#hide_day_' + id).length) {
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        }
        let day = 1;
        if (!hideDay) day = $('#daySelection' + id).prop('value');
        const month = $('#monthSelection' + id).prop('value');
        const year = $('#yearSelection' + id).prop('value');
        const curdate = new Date(year, parseInt(month) - 1, day);
        let date = curdate.getTime();
        if (parseInt(day) > 0 && parseInt(month) > 0 && parseInt(year) > 0) return date;
        return '';
    }

    function setDisableStatusById(id, disableStatus) {
        let hideDay = false;
        if ($('#hide_day_' + id).length)
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        if (!hideDay)
            $('#daySelection' + id).prop('disabled', disableStatus);
        $('#monthSelection' + id).prop('disabled', disableStatus);
        $('#yearSelection' + id).prop('disabled', disableStatus);
    }

    function setDateByTimestampAndId(id, date) {
        var hideDay = false;
        if ($('#hide_day_' + id).length) {
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        }
        if (date == -62135791200000) return;    //value set by session constrains
        const curdate = new Date(parseInt(date));
        // document.getElementById('monthSelection' + id).value = (curdate.getMonth() + 1).toString();
        // document.getElementById('yearSelection' + id).value = curdate.getFullYear().toString();
        // $('#monthSelection' + id).select2("val", (curdate.getMonth() + 1).toString());
        $('#monthSelection' + id).val((curdate.getMonth() + 1).toString()).trigger('change.select2');
        // $('#yearSelection' + id).select2("val", curdate.getFullYear().toString());
        $('#yearSelection' + id).val(curdate.getFullYear().toString()).trigger('change.select2');
        if (!hideDay) {
            // $('#daySelection' + id).select2("val", curdate.getDate().toString());
            $('#daySelection' + id).val(curdate.getDate().toString()).trigger('change.select2');
            // document.getElementById('daySelection' + id).value = curdate.getDate().toString();
        }
    }

    function setMaxDateById(id, maxdate) {  //maxdate should be in DD/MM/YYYY
        var hideDay = false;
        if ($('#hide_day_' + id).length) {
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        }
        const day = (hideDay ? 1 : maxdate.split('/')[0]);
        const month = maxdate.split('/')[1];
        const year = maxdate.split('/')[2];
        const maxDate = new Date(year, parseInt(month) - 1, day);
        document.getElementById('max_date_' + id).value = maxDate.getTime().toString();
        document.getElementById('end_year_' + id).value = year;
        resetMaxMinYearById(id, $('#end_year_' + id).prop('value'), $('#start_year_' + id).prop('value'));
        refreshDateOnMinMax(id);
    }

    function setMaxDateByTimestampAndId(id, maxdate) {
        const maxDate = new Date(parseInt(maxdate));
        document.getElementById('max_date_' + id).value = maxDate.getTime().toString();
        if (maxDate) document.getElementById('end_year_' + id).value = maxDate.getFullYear().toString();
        resetMaxMinYearById(id, $('#end_year_' + id).prop('value'), $('#start_year_' + id).prop('value'));
        refreshDateOnMinMax(id);
    }

    function setMinDateById(id, mindate) {
        var hideDay = false;
        if ($('#hide_day_' + id).length) {
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        }
        const day = (hideDay ? 1 : mindate.split('/')[0]);
        const month = mindate.split('/')[1];
        const year = mindate.split('/')[2];
        const minDate = new Date(year, parseInt(month) - 1, day);
        document.getElementById('min_date_' + id).value = minDate.getTime().toString();
        document.getElementById('start_year_' + id).value = year;
        resetMaxMinYearById(id, $('#end_year_' + id).prop('value'), $('#start_year_' + id).prop('value'));
        // console.log(id, $('#end_year_'+id).prop('value'), $('#start_year_'+id).prop('value'), minDate.getFullYear());
        refreshDateOnMinMax(id);
    }

    function setMinDateByTimestampAndId(id, mindate) {
        const minDate = new Date(parseInt(mindate));
        document.getElementById('min_date_' + id).value = minDate.getTime().toString();
        if (minDate) document.getElementById('start_year_' + id).value = minDate.getFullYear().toString();
        resetMaxMinYearById(id, $('#end_year_' + id).prop('value'), $('#start_year_' + id).prop('value'));
        refreshDateOnMinMax(id);
    }

    function resetMaxMinYearById(id, maxyear, minyear) {
        const language = $('#language_' + id).prop('value');
        const lastval = $('#yearSelection' + id).prop('value');
        $('#yearSelection' + id).empty();
        $('#yearSelection' + id).append($('<option>', {
            text: (language == 'English' ? 'Year' : 'বছর'),
            value: '0'
        })).trigger('change.select2'); //"<option value = '0'>" + (language == 'English' ? 'Year' : 'বছর') + "</option>";
        for (let i = maxyear; i >= minyear; i--) {
            $('#yearSelection' + id).append($('<option>', {
                text: (language == 'English' ? i.toString() : convDateStrToBn(i)),
                value: i.toString()
            })).trigger('change.select2');  //"<option value = '"+i.toString()+"'>" + (language == 'English' ? i.toString() : convDateStrToBn(i)) + "</option>";
        }
        if (lastval <= maxyear && lastval >= minyear) {
            $('#yearSelection' + id).val(lastval).trigger('change.select2');
            // $('#yearSelection' + id).select2("val", lastval);
        }

        // $('#yearSelection' + id).val(lastval);
        $('#start_year_' + id).val(minyear.toString());
        $('#end_year_' + id).val(maxyear.toString());
        // refreshDateOnMinMax(id);
    }

    function resetDateById(id) {
        var hideDay = false;
        if ($('#hide_day_' + id).length) {
            hideDay = $('#hide_day_' + id).prop('value').localeCompare('true') == 0;
        }
        if (!hideDay) {
            $('#daySelection' + id).val(0).trigger('change.select2');
            // $('#daySelection' + id).select2("val", 0);
        }
        // document.getElementById('daySelection' + id).value = '0';
        // document.getElementById('monthSelection' + id).value = '0';
        $('#monthSelection' + id).val(0).trigger('change.select2');
        // $('#monthSelection' + id).select2("val", 0);
        // document.getElementById('yearSelection' + id).value = '0';
        $('#yearSelection' + id).val(0).trigger('change.select2');
        // $('#yearSelection' + id).select2("val", 0);
        refreshDateOnMinMax(id);
    }

    function createDatePicker(divId, paramObject) {

        createDatePicker(divId, paramObject, function () {
        });
    }

    function createDatePicker(divId, paramObject, completeCallback) {
        let url = "DateServlet?";
        for (let key in paramObject) {
            url += key + "=" + paramObject[key] + "&";
        }
        // console.log("office_unit ajax url : " + url);
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                $('#' + divId).append(data);
            },
            error: function (error) {
                console.log(error);
            },
            complete: completeCallback
        });
    }

    document.addEventListener('DOMContentLoaded', () => {

        $(document).on('change', '.yearSelection', function () {

            $(this).css('border-color', '#0abb87');
            let datePickerDiv = $(this).closest("div.datepicker-container");

            const id = $(this).prop('id');
            const hideDay = $(datePickerDiv).find('.datepicker-hide-day').prop('value').localeCompare('true') == 0;
            const day = (hideDay ? 1 : $(datePickerDiv).find('.daySelection').prop('value'));
            const month = $(datePickerDiv).find('.monthSelection').prop('value');
            const year = $(datePickerDiv).find('.yearSelection').prop('value');
            $('#' + id).trigger('datepicker.change', [{
                'changed': id.split('Selection')[0],
                'data': {
                    'day': day,
                    'month': month,
                    'year': year
                }
            }]);

            refreshDateOnMinMax($(datePickerDiv).prop('id'));

            dateValidator($(datePickerDiv).prop('id'));
        });

        $(document).on('change', '.monthSelection', function () {

            $(this).css('border-color', '#0abb87');
            let datePickerDiv = $(this).closest("div.datepicker-container");

            const id = $(this).prop('id');
            const hideDay = $(datePickerDiv).find('.datepicker-hide-day').prop('value').localeCompare('true') == 0;
            const day = (hideDay ? 1 : $(datePickerDiv).find('.daySelection').prop('value'));
            const month = $(datePickerDiv).find('.monthSelection').prop('value');
            const year = $(datePickerDiv).find('.yearSelection').prop('value');
            $('#' + id).trigger('datepicker.change', [{
                'changed': id.split('Selection')[0],
                'data': {
                    'day': day,
                    'month': month,
                    'year': year
                }
            }]);

            refreshDateOnMinMax($(datePickerDiv).prop('id'));

            dateValidator($(datePickerDiv).prop('id'));

        });

        $(document).on('change', '.daySelection', function () {
            $(this).css('border-color', '#0abb87');
            const datePickerDiv = $(this).closest("div.datepicker-container");
            dateValidator($(datePickerDiv).prop('id'));

            const id = $(this).prop('id');
            const day = $(datePickerDiv).find('.daySelection').prop('value');
            const month = $(datePickerDiv).find('.monthSelection').prop('value');
            const year = $(datePickerDiv).find('.yearSelection').prop('value');
            $('#' + id).trigger('datepicker.change', [{
                'changed': id.split('Selection')[0],
                'data': {
                    'day': day,
                    'month': month,
                    'year': year
                }
            }]);
            refreshDateOnMinMax($(datePickerDiv).prop('id'));

            dateValidator($(datePickerDiv).prop('id'));
        });
    });
</script>