<%--
  Created by IntelliJ IDEA.
  User: Jahin
  Date: 3/8/2021
  Time: 11:55 AM
--%>
<%@page import="date.JSDateConfig" %>
<%@ page import="pb.OptionDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="pb.Utils" %>
<%@ page import="util.StringUtils" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Arrays" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    OptionDTO optionDTO;
    Date curDate = new Date();
    String language = "Bangla";
    if (request.getParameter(JSDateConfig.LANGUAGE.toString()) != null) {
        language = request.getParameter(JSDateConfig.LANGUAGE.toString());
    }

    String dateId = "jsDate";
    if (request.getParameter(JSDateConfig.DATE_ID.toString()) != null) {
        dateId = request.getParameter(JSDateConfig.DATE_ID.toString());
    }

    boolean isDisabled = false;
    if (request.getParameter(JSDateConfig.IS_DISABLED.toString()) != null) {
        isDisabled = Boolean.parseBoolean(request.getParameter(JSDateConfig.IS_DISABLED.toString()));
    }

    int startYear = 1900;
    if (request.getParameter(JSDateConfig.START_YEAR.toString()) != null) {
        startYear = Integer.parseInt(request.getParameter(JSDateConfig.START_YEAR.toString()));
    }

    int endYear = curDate.getYear() + 1900;
    if (request.getParameter(JSDateConfig.END_YEAR.toString()) != null) {
        endYear = Integer.parseInt(request.getParameter(JSDateConfig.END_YEAR.toString()));
    }

    Boolean hideDay = false;
    if (request.getParameter(JSDateConfig.HIDE_DAY.toString()) != null) {
        hideDay = request.getParameter(JSDateConfig.HIDE_DAY.toString()).equalsIgnoreCase("true");
    }

    int len = 4;
    if (hideDay)
        len = 6;
//
//    Boolean hideMonth = false;
//    if (request.getParameter(JSDateConfig.HIDE_MONTH.toString()) != null) {
//        hideMonth = request.getParameter(JSDateConfig.HIDE_MONTH.toString()).equalsIgnoreCase("true");
//    }
//
//    Boolean hideYear = false;
//    if (request.getParameter(JSDateConfig.HIDE_YEAR.toString()) != null) {
//        hideYear = request.getParameter(JSDateConfig.HIDE_YEAR.toString()).equalsIgnoreCase("true");
//    }

    List<OptionDTO> dayOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("Date", "দিন", "0");
    dayOptionDTOList.add(optionDTO);
    for (int x = 1; x <= 31; x++) {
        optionDTO = new OptionDTO(String.valueOf(x), StringUtils.convertToBanNumber(String.valueOf(x)), String.valueOf(x));
        dayOptionDTOList.add(optionDTO);
    }

    List<String> monthsEn = Arrays.asList("Month", "January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December");
    List<String> monthsBn = Arrays.asList("মাস", "জানুয়ারী", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর",
            "নভেম্বর", "ডিসেম্বর");
    List<OptionDTO> monthOptionDTOList = new ArrayList<>();
    for (int x = 0; x <= 12; x++) {
        optionDTO = new OptionDTO(monthsEn.get(x), monthsBn.get(x), String.valueOf(x));
        monthOptionDTOList.add(optionDTO);
    }

    List<OptionDTO> yearOptionDTOList = new ArrayList<>();
    optionDTO = new OptionDTO("Year", "বছর", "0");
    yearOptionDTOList.add(optionDTO);
    for (int x = endYear; x >= startYear; x--) {
        optionDTO = new OptionDTO(String.valueOf(x), StringUtils.convertToBanNumber(String.valueOf(x)), String.valueOf(x));
        yearOptionDTOList.add(optionDTO);
    }
%>


<div class=" datepicker-container" id="<%=dateId%>" tag='pb_html'>
    <div class="row">
        <%if (!hideDay) {%>
        <div class="col-lg-<%=len%> " id='daySelect_div_<%=dateId%>' tag='pb_html'>
            <select class='form-control daySelection rounded reve-datepicker' name='daySelection<%=dateId%>'
                    id='daySelection<%=dateId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(dayOptionDTOList, language, "0")%>
            </select>
        </div>
        <%}%>
        <div class="col-lg-<%=len%> " id='monthSelect_div_<%=dateId%>' tag='pb_html'>
            <select class='form-control monthSelection rounded reve-datepicker' name='monthSelection<%=dateId%>'
                    id='monthSelection<%=dateId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(monthOptionDTOList, language, "0")%>
            </select>
        </div>

        <div class="col-lg-<%=len%> " id='yearSelect_div_<%=dateId%>' tag='pb_html'>
            <select class='form-control yearSelection rounded reve-datepicker' name='yearSelection<%=dateId%>'
                    id='yearSelection<%=dateId%>' tag='pb_html'>
                <%=Utils.buildOptionsWithoutSelectWithSelectId(yearOptionDTOList, language, "0")%>
            </select>
        </div>

        <span id='error<%=dateId%>'
              style="color:#fd397a; text-align:center; margin-left:12px; margin-top: 1%; font-size: smaller"></span>

    </div>

    <input type='hidden' class='form-control datepicker-language' name='language_<%=dateId%>' id='language_<%=dateId%>'
           value='<%=language%>' tag='pb_html'/>
    <input type='hidden' class='form-control datepicker-start-year' name='start_year_<%=dateId%>'
           id='start_year_<%=dateId%>' value='<%=startYear%>' tag='pb_html'/>
    <input type='hidden' class='form-control datepicker-end-year' name='end_year_<%=dateId%>' id='end_year_<%=dateId%>'
           value='<%=endYear%>' tag='pb_html'/>
    <input type='hidden' class='form-control datepicker-max-date' name='max_date_<%=dateId%>' id='max_date_<%=dateId%>'
           value='' tag='pb_html'/>
    <input type='hidden' class='form-control datepicker-min-date' name='min_date_<%=dateId%>' id='min_date_<%=dateId%>'
           value='' tag='pb_html'/>
    <input type='hidden' class='form-control datepicker-hide-day' name='hide_day_<%=dateId%>' id='hide_day_<%=dateId%>'
           value='<%=hideDay%>' tag='pb_html'/>

</div>

<script type="text/javascript">

    $(document).ready(function () {
        select2SingleSelector('#daySelection<%=dateId%>','<%=language%>');
        select2SingleSelector('#monthSelection<%=dateId%>','<%=language%>');
        select2SingleSelector('#yearSelection<%=dateId%>','<%=language%>');
        let date = new Date();
        <%if(isDisabled) {%>
        setDisableStatusById('<%=dateId%>', true);
        <%}%>
        <%--date.setFullYear('<%=endYear+1%>');--%>
        <%--document.getElementById('max_date_<%=dateId%>').value = date.getTime();--%>
        <%--date.setFullYear('<%=startYear-1%>');--%>
        <%--document.getElementById('min_date_<%=dateId%>').value = date.getTime();--%>
    });

    <%--function init<%=dateId%>(row) {--%>
    <%--    $("#daySelection<%=dateId%>").select2({--%>
    <%--        dropdownAutoWidth: true--%>
    <%--    });--%>
    <%--    $("#monthSelection<%=dateId%>").select2({--%>
    <%--        dropdownAutoWidth: true--%>
    <%--    });--%>
    <%--    $("#yearSelection<%=dateId%>").select2({--%>
    <%--        dropdownAutoWidth: true--%>
    <%--    });--%>
    <%--}--%>


    <%--var row<%=dateId%> = 0;--%>

    <%--window.onload = function () {--%>
    <%--    init<%=dateId%>(row<%=dateId%>);--%>
    <%--}--%>
</script>

