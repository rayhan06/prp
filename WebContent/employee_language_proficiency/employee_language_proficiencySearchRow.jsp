<%@page import="employee_records.Employee_recordsRepository" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>

<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_language_proficiencyDTO.employeeRecordsId, Language)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language,"language", employee_language_proficiencyDTO.languageCat)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language,"read_skill", employee_language_proficiencyDTO.readSkillCat)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language,"write_skill", employee_language_proficiencyDTO.writeSkillCat)%>
</td>

<td>
    <%=CatRepository.getInstance().getText(Language,"speak_skill", employee_language_proficiencyDTO.speakSkillCat)%>
</td>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_language_proficiencyServlet?actionType=getEditPage&ID=<%=employee_language_proficiencyDTO.iD%>&empId=<%=employee_language_proficiencyDTO.employeeRecordsId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_language_proficiencyDTO.iD%>'/></span>
    </div>
</td>