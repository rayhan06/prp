<%@page import="employee_language_proficiency.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="common.BaseServlet" %>

<%@include file="../pb/addInitializer2.jsp" %>
<%
    Employee_language_proficiencyDTO employee_language_proficiencyDTO;
    String empId = request.getParameter("empId");
    if (empId == null) {
        empId = String.valueOf(request.getAttribute("empId"));
    }
    String formTitle;
    if ("ajax_edit".equals(actionName)) {
        formTitle = LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_FORMNAME, loginDTO);
        employee_language_proficiencyDTO = (Employee_language_proficiencyDTO)request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        formTitle = LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_FORMNAME, loginDTO);
        employee_language_proficiencyDTO = new Employee_language_proficiencyDTO();
    }

    String url = "Employee_language_proficiencyServlet?actionType=" + actionName + "&isPermanentTable=true&empId="
            + empId+"&iD="+employee_language_proficiencyDTO.iD;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform" enctype="multipart/form-data" action="<%=url%>">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="languageCat_category">
                                            <%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_LANGUAGECAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='languageCat_div'>
                                            <select class='form-control' name='languageCat' required
                                                    id='languageCat_category' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("language", Language, employee_language_proficiencyDTO.languageCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for='readSkillCat_category'>
                                            <%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_READSKILLCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='readSkillCat_div'>
                                            <select class='form-control' name='readSkillCat' required
                                                    id='readSkillCat_category'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("read_skill", Language, employee_language_proficiencyDTO.readSkillCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for="writeSkillCat_category">
                                            <%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_WRITESKILLCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='writeSkillCat_div'>
                                            <select class='form-control' name='writeSkillCat' required
                                                    id='writeSkillCat_category'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("write_skill", Language, employee_language_proficiencyDTO.writeSkillCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"
                                               for='speakSkillCat_category'>
                                            <%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_SPEAKSKILLCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9 " id='speakSkillCat_div'>
                                            <select class='form-control' name='speakSkillCat' required
                                                    id='speakSkillCat_category'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("speak_skill", Language, employee_language_proficiencyDTO.speakSkillCat)%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_EMPLOYEE_LANGUAGE_PROFICIENCY_CANCEL_BUTTON, userDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_EMPLOYEE_LANGUAGE_PROFICIENCY_SUBMIT_BUTTON, userDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    const form = $("#bigform");

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        select2SingleSelector("#languageCat_category", "<%=Language%>");
        select2SingleSelector("#readSkillCat_category", "<%=Language%>");
        select2SingleSelector("#writeSkillCat_category", "<%=Language%>");
        select2SingleSelector("#speakSkillCat_category", "<%=Language%>");
        form.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            messages: {
                languageCat: "<%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_PLEASE_SELECT_LANGUAGE, userDTO)%>",
                readSkillCat: "<%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_PLEASE_SELECT_READ_SKILL, userDTO)%>",
                writeSkillCat: "<%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_PLEASE_SELECT_WRITE_SKILL, userDTO)%>",
                speakSkillCat: "<%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_ADD_PLEASE_SELECT_SPEAK_SKILL, userDTO)%>"
            }
        });
    });

    function submitForm() {
        submitAjaxForm('bigform');
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>