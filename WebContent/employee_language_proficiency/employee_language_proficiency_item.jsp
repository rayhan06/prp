<td style="vertical-align: middle;"><%=isLanguageEnglish ? languageItem.languageEng : languageItem.languageBan%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? languageItem.writeSkillEng : languageItem.writeSkillBan%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? languageItem.readSkillEng : languageItem.readSkillBan%></td>
<td style="vertical-align: middle;"><%=isLanguageEnglish ? languageItem.speakSkillEng : languageItem.speakSkillBan%></td>
<td style="text-align: center; vertical-align: middle;">
    <form action="Employee_language_proficiencyServlet?isPermanentTable=true&actionType=delete&tab=2&ID=<%=languageItem.dto.iD%>&userId=<%=request.getParameter("userId")%>" method="POST" id="tableForm_language<%=langIndex%>" enctype = "multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn-success" title="Edit" onclick="location.href='<%=request.getContextPath()%>/Employee_language_proficiencyServlet?actionType=getEditPage&tab=2&iD=<%=languageItem.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'"><i class="fa fa-edit"></i></button>
            &nbsp;
            <button type="button" class="btn-danger" title="Delete" onclick="deleteItem('tableForm_language',<%=langIndex%>)"><i class="fa fa-trash"></i></button>
        </div>
    </form>
</td>