<%@ page import="employee_language_proficiency.EmployeeLanguageProficiencyModel" %>
<%@ page import="java.util.List" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th><b><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_LANGUAGECAT, loginDTO)%>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_WRITESKILLCAT, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_READSKILLCAT, loginDTO) %>
        </b></th>
        <th><b><%= LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_SPEAKSKILLCAT, loginDTO) %>
        </b></th>
        <th><b></b></th>
    </tr>
    </thead>
    <tbody>
    <%
        List<EmployeeLanguageProficiencyModel> savedLanguageProficiencyList = (List<EmployeeLanguageProficiencyModel>) request.getAttribute("savedLanguageProficiencyList");
        int langIndex = 0;
        if (savedLanguageProficiencyList != null && savedLanguageProficiencyList.size() > 0) {
            for (EmployeeLanguageProficiencyModel languageItem : savedLanguageProficiencyList) {
                langIndex++;
    %>
    <tr>
        <%@include file="/employee_language_proficiency/employee_language_proficiency_item.jsp" %>
    </tr>
    <%
            }
        }
    %>
    </tbody>
</table>


<div class="row mt-3">
    <div class="col-12 text-right">
        <button
                class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='<%=request.getContextPath()%>/Employee_language_proficiencyServlet?actionType=getAddPage&tab=2&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'"
        >
            <i class="fa fa-plus"></i>&nbsp;
            <%= LM.getText(LC.HR_MANAGEMENT_EDUCATION_AND_LINGO_ADD_LANGUAGE, loginDTO) %>
        </button>
    </div>
</div>