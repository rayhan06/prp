<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="employee_language_proficiency.*" %>
<%@ page import="java.util.List" %>

<%
    String navigator2 = "";
    String servletName = "Employee_language_proficiencyServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>

<%
    List<Employee_language_proficiencyDTO> data = (List<Employee_language_proficiencyDTO>) rn2.list;
    if (data != null && data.size() > 0) {
%>
<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_LANGUAGECAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_READSKILLCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_WRITESKILLCAT, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_SPEAKSKILLCAT, loginDTO)%>
            </th>
            <th style="width: 100px"><%=LM.getText(LC.EMPLOYEE_LANGUAGE_PROFICIENCY_SEARCH_EMPLOYEE_LANGUAGE_PROFICIENCY_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center" style="width: 25px">
                <span><%="English".equalsIgnoreCase(Language) ? "All" : "সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
                for (Employee_language_proficiencyDTO employee_language_proficiencyDTO : data) {
        %>
        <tr>
            <%@include file="employee_language_proficiencySearchRow.jsp" %>
        </tr>
        <% }%>
        </tbody>
    </table>
</div>
<%
} else {
%>
<label style="width: 100%;text-align: center;font-size: larger;font-weight: bold;color: red">
    <%=isLanguageEnglish ? "No information is found" : "কোন তথ্য পাওয়া যায় নি"%>
</label>
<%
    }
%>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>