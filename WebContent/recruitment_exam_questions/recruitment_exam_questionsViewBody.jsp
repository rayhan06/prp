

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_exam_questions.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>

<%@include file="../pb/viewInitializer.jsp"%>
<%
String servletName = "Recruitment_exam_questionsServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Recruitment_exam_questionsDTO recruitment_exam_questionsDTO = Recruitment_exam_questionsDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = recruitment_exam_questionsDTO;

    String recruitmentTestName = "";
    String recruitmentJob = "";
    String recruitmentJobSpecificExam = "";

    Recruitment_test_nameDTO recruitment_test_nameDTO = Recruitment_test_nameRepository.getInstance()
            .getRecruitment_test_nameDTOByiD(recruitment_exam_questionsDTO.recruitmentTestNameId);
    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance()
            .getRecruitment_job_descriptionDTOByID(recruitment_exam_questionsDTO.recruitmentJobDescriptionId);
    if (recruitment_job_descriptionDTO != null) {
        List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
                .getRecruitmentJobSpecificExamTypeDTOByjob_id(recruitment_job_descriptionDTO.iD);
        RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO = recruitmentJobSpecificExamTypeDTOS.stream()
                .filter(e -> e.order == recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId && e.isSelected)
                .findFirst()
                .orElse(null);
        if(jobSpecificExamTypeDTO != null){
            recruitmentJobSpecificExam = CatRepository.getName(Language, "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);
        }
        else{
            recruitmentJobSpecificExam = "";
        }
    }

    recruitmentTestName = recruitment_test_nameDTO != null ? isLanguageEnglish ? recruitment_test_nameDTO.nameEn : recruitment_test_nameDTO.nameBn : "";
    recruitmentJob = recruitment_job_descriptionDTO != null ? isLanguageEnglish ? recruitment_job_descriptionDTO.jobTitleEn
            : recruitment_job_descriptionDTO.jobTitleBn : "";

%>



<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENT_EXAM_QUESTIONS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENT_EXAM_QUESTIONS_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENTTESTNAMEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=recruitmentTestName%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENTJOBDESCRIPTIONID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=recruitmentJob%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENTJOBSPECIFICEXAMTYPEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
                                        <%=recruitmentJobSpecificExam%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>