<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="recruitment_exam_questions.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>

<%
    Recruitment_exam_questionsDTO recruitment_exam_questionsDTO = new Recruitment_exam_questionsDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        recruitment_exam_questionsDTO = Recruitment_exam_questionsDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = recruitment_exam_questionsDTO;
    String tableName = "recruitment_exam_questions";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%@ include file="../recruitment_seat_plan/manualLoader.jsp" %>
<%
    String formTitle = LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENT_EXAM_QUESTIONS_ADD_FORMNAME, loginDTO);
    String servletName = "Recruitment_exam_questionsServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=recruitment_exam_questionsDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='recruitmentTestNameId'
                                                    id='recruitmentTestNameId'
                                                    onchange="onRecruitmentTestNameChange(this)" tag='pb_html'>
                                                <%= Recruitment_test_nameRepository.getInstance().buildOptions(Language, recruitment_exam_questionsDTO.recruitmentTestNameId) %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBTITLE, loginDTO)%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='recruitmentJobDescriptionId'
                                                    id='recruitmentJobDescriptionId'
                                                    onchange="onRecruitmentJobDescriptionChange(this)"
                                                    tag='pb_html'>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%><span
                                                class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='recruitmentJobSpecificExamTypeId'
                                                    id='recruitmentJobSpecificExamTypeId'
                                                    onchange="onRecruitmentJobSpecificExamTypeIdChange(this)"
                                                    tag='pb_html'>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-4 col-form-label text-right"><%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_FILESDROPZONE, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-8">
                                            <%
                                                fileColumnName = "filesDropzone";
                                                if (actionName.equals("ajax_edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(recruitment_exam_questionsDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    recruitment_exam_questionsDTO.filesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=recruitment_exam_questionsDTO.filesDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=recruitment_exam_questionsDTO.filesDropzone%>'/>


                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENT_EXAM_QUESTIONS_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitForm()">
                                <%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENT_EXAM_QUESTIONS_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    let language = '<%=Language%>';
    const fullPageLoader = $('#full-page-loader');
    const recruitmentQuestionPaperForm = $("#bigform");
    let selectorArr = ['recruitmentTestNameId', 'recruitmentJobDescriptionId', 'recruitmentJobSpecificExamTypeId'];
    function PreprocessBeforeSubmiting() {

        return true;
    }

    function resetSelectors(startIndex) {
        selectorArr.filter((item, index) => index >= startIndex).map(element => $("#" + element).empty());
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting()) {
            fullPageLoader.show();
            let url = "<%=servletName%>?actionType=<%=actionName%>";
            $.ajax({
                type: "POST",
                url: url,
                data: recruitmentQuestionPaperForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        fullPageLoader.hide();
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        fullPageLoader.hide();
                        window.location.replace(getContextPath() + response.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    fullPageLoader.hide();
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
            fullPageLoader.hide();
        }
    }
    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function onRecruitmentTestNameChange(selectedVal) {
        resetSelectors(1);
        let recruitmentTestNameId = selectedVal.value;

        if (recruitmentTestNameId !== null && recruitmentTestNameId > 0 && recruitmentTestNameId !== "") {
            const url = 'Recruitment_job_descriptionServlet?actionType=getJobDescriptionByRecTestName&recruitmentTestNameId=' + recruitmentTestNameId+
                '&selectedId='+<%=recruitment_exam_questionsDTO.recruitmentJobDescriptionId%>;

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (response) {
                    document.getElementById('recruitmentJobDescriptionId').innerHTML = response;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function onRecruitmentJobDescriptionChange(selectedVal) {
        //resetSelectors(2);
        let recruitmentTestNameId = document.getElementById('recruitmentTestNameId').value;
        let jobValue = selectedVal.value;

        if (recruitmentTestNameId !== null && recruitmentTestNameId > 0 && recruitmentTestNameId !== "") {
            const url = 'Recruitment_job_descriptionServlet?actionType=getExamTypesByTestNameAndJobId&recruitmentTestNameId=' + recruitmentTestNameId+
                '&jobId='+jobValue+ '&selectedId='+<%=recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId%>;

            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (response) {
                    document.getElementById('recruitmentJobSpecificExamTypeId').innerHTML = response;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function init(row) {
        select2SingleSelector('#recruitmentTestNameId', '<%=Language%>');
        select2SingleSelector('#recruitmentJobDescriptionId', '<%=Language%>');
        select2SingleSelector('#recruitmentJobSpecificExamTypeId', '<%=Language%>');


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        }) ;
        let actionName = '<%=actionName%>';
        if (actionName === 'ajax_edit') {
            let recruitmentTestNameElement = document.querySelector('#recruitmentTestNameId');
            onRecruitmentTestNameChange(recruitmentTestNameElement);
            let recruitmentJobDescriptionElement = document.querySelector('#recruitmentJobDescriptionId');
            onRecruitmentJobDescriptionChange(recruitmentJobDescriptionElement);
        }

        fullPageLoader.hide();
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    function onRecruitmentJobSpecificExamTypeIdChange(selectedVal) {

    }


</script>






