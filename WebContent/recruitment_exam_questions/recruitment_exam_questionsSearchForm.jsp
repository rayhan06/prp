<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_exam_questions.*" %>
<%@ page import="util.*" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionDTO" %>
<%@ page import="recruitment_job_description.Recruitment_job_descriptionRepository" %>
<%@ page import="recruitment_job_specific_exam_type.RecruitmentJobSpecificExamTypeDTO" %>
<%@ page import="recruitment_job_specific_exam_type.JobSpecificExamTypeRepository" %>
<%@ page import="java.util.List" %>


<%
    String navigator2 = "navRECRUITMENT_EXAM_QUESTIONS";
    String servletName = "Recruitment_exam_questionsServlet";
%>
<%@include file="../pb/searchInitializer.jsp" %>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th><%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENTTESTNAMEID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_ADD_RECRUITMENTJOBDESCRIPTIONID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_SEAT_PLAN_ADD_RECRUITMENTJOBSPECIFICEXAMTYPEID, loginDTO)%>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_EXAM_QUESTIONS_SEARCH_RECRUITMENT_EXAM_QUESTIONS_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="">
                <div class="text-center">
                    <span>All</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList<Recruitment_exam_questionsDTO>) rn2.list;

            try {

                if (data != null) {
                    int size = data.size();
                    String recruitmentTestName = "";
                    String recruitmentJob = "";
                    String recruitmentJobSpecificExam = "";
                    for (int i = 0; i < size; i++) {
                        Recruitment_exam_questionsDTO recruitment_exam_questionsDTO = (Recruitment_exam_questionsDTO) data.get(i);

                        Recruitment_test_nameDTO recruitment_test_nameDTO = Recruitment_test_nameRepository.getInstance()
                                .getRecruitment_test_nameDTOByiD(recruitment_exam_questionsDTO.recruitmentTestNameId);
                        Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.getInstance()
                                .getRecruitment_job_descriptionDTOByID(recruitment_exam_questionsDTO.recruitmentJobDescriptionId);
                        if (recruitment_job_descriptionDTO != null) {
                            List<RecruitmentJobSpecificExamTypeDTO> recruitmentJobSpecificExamTypeDTOS = JobSpecificExamTypeRepository.getInstance()
                                    .getRecruitmentJobSpecificExamTypeDTOByjob_id(recruitment_job_descriptionDTO.iD);
                            RecruitmentJobSpecificExamTypeDTO jobSpecificExamTypeDTO = recruitmentJobSpecificExamTypeDTOS.stream()
                                    .filter(e -> e.order == recruitment_exam_questionsDTO.recruitmentJobSpecificExamTypeId)
                                    .findFirst()
                                    .orElse(null);
                            if (jobSpecificExamTypeDTO != null) {
                                recruitmentJobSpecificExam = CatRepository.getName(Language, "job_exam_type", jobSpecificExamTypeDTO.jobExamTypeCat);
                            } else {
                                recruitmentJobSpecificExam = "";
                            }
                        }
                        recruitmentTestName = recruitment_test_nameDTO != null ? isLanguageEnglish ? recruitment_test_nameDTO.nameEn : recruitment_test_nameDTO.nameBn : "";
                        recruitmentJob = recruitment_job_descriptionDTO != null ? isLanguageEnglish ? recruitment_job_descriptionDTO.jobTitleEn
                                : recruitment_job_descriptionDTO.jobTitleBn : "";


        %>
        <tr>


            <td>

                <%=recruitmentTestName%>

            </td>

            <td>

                <%=recruitmentJob%>

            </td>

            <td>

                <%=recruitmentJobSpecificExam%>

            </td>


            <%CommonDTO commonDTO = recruitment_exam_questionsDTO; %>
            <%@include file="../pb/searchAndViewButton.jsp" %>

            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'><input type='checkbox' name='ID'
                                                 value='<%=recruitment_exam_questionsDTO.iD%>'/></span>
                </div>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			