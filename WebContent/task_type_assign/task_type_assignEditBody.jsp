<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="task_type_assign.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="role.PermissionRepository" %>
<%@ page import="task_type.Task_typeRepository" %>
<%@ page import="task_type.Task_typeDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    Task_type_assignDTO task_type_assignDTO;
    String actionName;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        task_type_assignDTO = (Task_type_assignDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        task_type_assignDTO = new Task_type_assignDTO();
    }
    String formTitle = LM.getText(LC.TASK_TYPE_ASSIGN_ADD_TASK_TYPE_ASSIGN_ADD_FORMNAME, loginDTO);
    long taskTypeId = Long.parseLong(request.getParameter("taskTypeId"));
    Task_typeDTO taskTypeDTO = Task_typeRepository.getInstance().getDTOById(taskTypeId);
    int i = 0;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Task_type_assignServlet?actionType=<%=actionName%>&isPermanentTable=true"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='iD'
                                                   id='iD_hidden_<%=i%>' value='<%=task_type_assignDTO.iD%>'
                                                   tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TASK_TYPE_ASSIGN_ADD_TASKTYPEID, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="hidden" name="taskTypeID" value="<%=taskTypeId%>">
                                                    <button type="button" class="btn btn-secondary form-control"
                                                            disabled id="office_units_id_text">
                                                        <%=taskTypeDTO.getFormattedName(Language)%>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.TASK_TYPE_ASSIGN_ADD_ROLEID, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <select multiple="multiple" class='form-control rounded'
                                                            name='roleID'
                                                            id='roleID_text_<%=i%>'>
                                                        <%=PermissionRepository.getInstance().buildOptions(Language, task_type_assignDTO.roleID)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10 form-actions text-right mb-5">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius "
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.TASK_TYPE_ASSIGN_ADD_TASK_TYPE_ASSIGN_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.TASK_TYPE_ASSIGN_ADD_TASK_TYPE_ASSIGN_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    function PreprocessBeforeSubmiting(row, validate) {
        processMultipleSelectBoxBeforeSubmit("roleID");
        return true;
    }

    $(document).ready(function () {
        select2MultiSelector("#roleID_text_0", '<%=Language%>');
    });
</script>






