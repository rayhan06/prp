

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="fast_search_test.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>

<%@ page import="geolocation.*"%>



<%
String servletName = "Fast_search_testServlet";
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.FAST_SEARCH_TEST_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Fast_search_testDAO fast_search_testDAO = new Fast_search_testDAO("fast_search_test");
Fast_search_testDTO fast_search_testDTO = fast_search_testDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title"><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_ADD_FORMNAME, loginDTO)%></h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_ADD_FORMNAME, loginDTO)%></h3>
						<table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_RELIGIONCAT, loginDTO)%></b></td>
								<td>
						
											<%
											value = fast_search_testDTO.religionCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "religion", fast_search_testDTO.religionCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_MEDICALEQUIPMENTNAMETYPE, loginDTO)%></b></td>
								<td>
						
											<%
											value = fast_search_testDTO.medicalEquipmentNameType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "medical_equipment_name", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_NAMEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = fast_search_testDTO.nameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_NAMEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = fast_search_testDTO.nameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			
			
			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_HOMEADDRESS, loginDTO)%></b></td>
								<td>
						
											<%
											value = fast_search_testDTO.homeAddress + "";
											%>
											<%=GeoLocationDAO2.getAddressToShow(value, Language)%>
				
			
								</td>
						
							</tr>

				


			
			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FILESDROPZONE, loginDTO)%></b></td>
								<td>
						
											<%
											value = fast_search_testDTO.filesDropzone + "";
											%>
											<%
											{
												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(fast_search_testDTO.filesDropzone);
											%>
												<%@include file="../pb/dropzoneViewer.jsp"%>
											<%												
											}
											%>
				
			
								</td>
						
							</tr>

				


			
			
			
			
		
						</table>
                    </div>
			






			</div>	

                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_RELIGIONCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_MEDICALEQUIPMENTNAMETYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_NAMEBN, loginDTO)%></th>
							</tr>
							<%
                        	FastSearchTestChildDAO fastSearchTestChildDAO = new FastSearchTestChildDAO();
                         	List<FastSearchTestChildDTO> fastSearchTestChildDTOs = fastSearchTestChildDAO.getFastSearchTestChildDTOListByFastSearchTestID(fast_search_testDTO.iD);
                         	
                         	for(FastSearchTestChildDTO fastSearchTestChildDTO: fastSearchTestChildDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = fastSearchTestChildDTO.religionCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "religion", fastSearchTestChildDTO.religionCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = fastSearchTestChildDTO.medicalEquipmentNameType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "medical_equipment_name", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = fastSearchTestChildDTO.nameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = fastSearchTestChildDTO.nameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
               


        </div>
	</div>