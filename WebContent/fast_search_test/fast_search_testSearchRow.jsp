<%@page pageEncoding="UTF-8" %>

<%@page import="fast_search_test.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.FAST_SEARCH_TEST_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_FAST_SEARCH_TEST;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Fast_search_testDTO fast_search_testDTO = (Fast_search_testDTO)request.getAttribute("fast_search_testDTO");
CommonDTO commonDTO = fast_search_testDTO;
String servletName = "Fast_search_testServlet";

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
String Message = "Done";
approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("fast_search_test", fast_search_testDTO.iD);

System.out.println("fast_search_testDTO = " + fast_search_testDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Fast_search_testDAO fast_search_testDAO = (Fast_search_testDAO)request.getAttribute("fast_search_testDAO");

FilesDAO filesDAO = new FilesDAO();

String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_religionCat'>
											<%
											value = fast_search_testDTO.religionCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "religion", fast_search_testDTO.religionCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_medicalEquipmentNameType'>
											<%
											value = fast_search_testDTO.medicalEquipmentNameType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "medical_equipment_name", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_nameEn'>
											<%
											value = fast_search_testDTO.nameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_nameBn'>
											<%
											value = fast_search_testDTO.nameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
											<td id = '<%=i%>_homeAddress'>
											<%
											value = fast_search_testDTO.homeAddress + "";
											%>
											<%=GeoLocationDAO2.getAddressToShow(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
	

											<td>
												<a href='Fast_search_testServlet?actionType=view&ID=<%=fast_search_testDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Fast_search_testServlet?actionType=getEditPage&ID=<%=fast_search_testDTO.iD%>'><%=LM.getText(LC.FAST_SEARCH_TEST_SEARCH_FAST_SEARCH_TEST_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td>
											<%
												if(fast_search_testDTO.isDeleted == 0 && approval_execution_tableDTO != null)
												{
											%>
												<a href="Approval_execution_tableServlet?actionType=search&tableName=fast_search_test&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"  ><%=LM.getText(LC.HM_HISTORY, loginDTO)%></a>
											<%
												}
												else
												{
											%>
												<%=LM.getText(LC.HM_NO_HISTORY_IS_AVAILABLE, loginDTO)%>
											<%
												}
											%>
											</td>
											
											<td>
											<%
											if(fast_search_testDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT)
											{
											%>
												<button type="button" class="btn btn-success" data-toggle="modal" data-target="#sendToApprovalPathModal" >
													<%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>
												</button>
												<%@include file="../inbox_internal/sendToApprovalPathModal.jsp"%>
											<%
											}
											else
											{
											%>
											<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
											<%
											}
											%>
											</td>
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=fast_search_testDTO.iD%>'/></span>
												</div>
											</td>
																						
											
<script>
window.onload =function ()
{
    console.log("using ckEditor");
    CKEDITOR.replaceAll();
}
	
</script>	

