
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="fast_search_test.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="user.*"%>

<%@page import="workflow.*"%>
<%@page import="util.TimeFormat"%>

<%
Fast_search_testDTO fast_search_testDTO;
fast_search_testDTO = (Fast_search_testDTO)request.getAttribute("fast_search_testDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
if(fast_search_testDTO == null)
{
	fast_search_testDTO = new Fast_search_testDTO();
	
}
System.out.println("fast_search_testDTO = " + fast_search_testDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_ADD_FORMNAME, loginDTO);
String servletName = "Fast_search_testServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID = -1;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
if(request.getParameter("isPermanentTable") != null)
{
	isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
}

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

String tableName = "fast_search_test";

boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

if(!isPermanentTable)
{
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("fast_search_test", fast_search_testDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("fast_search_test", fast_search_testDTO.iD);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
	
	isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
	
	canTerminate = isInitiator && fast_search_testDTO.isDeleted == 2;
}
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Fast_search_testServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.FAST_SEARCH_TEST_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=fast_search_testDTO.iD%>' tag='pb_html'/>
	
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.FAST_SEARCH_TEST_ADD_RELIGIONCAT, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'religionCat_div_<%=i%>'>	
		<select class='form-control'  name='religionCat' id = 'religionCat_category_<%=i%>'   tag='pb_html'>		
<%
Options = CatDAO.getOptions(Language, "religion", fast_search_testDTO.religionCat);
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.FAST_SEARCH_TEST_ADD_MEDICALEQUIPMENTNAMETYPE, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'medicalEquipmentNameType_div_<%=i%>'>	
		<select class='form-control'  name='medicalEquipmentNameType' id = 'medicalEquipmentNameType_select_<%=i%>'   tag='pb_html'>
<%
Options = CommonDAO.getOptions(Language, "medical_equipment_name", fast_search_testDTO.medicalEquipmentNameType);
%>
<%=Options%>
		</select>
		
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.FAST_SEARCH_TEST_ADD_NAMEEN, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'nameEn_div_<%=i%>'>	
		<input type='text' class='form-control'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=fast_search_testDTO.nameEn%>'   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.FAST_SEARCH_TEST_ADD_NAMEBN, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'nameBn_div_<%=i%>'>	
		<input type='text' class='form-control'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=fast_search_testDTO.nameBn%>'   tag='pb_html'/>					
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=fast_search_testDTO.insertedByUserId%>' tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=fast_search_testDTO.insertedByOrganogramId%>' tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=fast_search_testDTO.searchColumn%>' tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.FAST_SEARCH_TEST_ADD_HOMEADDRESS, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'homeAddress_div_<%=i%>'>	
		<div id ='homeAddress_geoDIV_<%=i%>' tag='pb_html'>
			<select class='form-control' name='homeAddress_active' id = 'homeAddress_geoSelectField_<%=i%>' onChange="addrselected(this.value, this.id, this.selectedIndex, this.name, 'homeAddress', this.getAttribute('row'))"  tag='pb_html' row = '<%=i%>'></select>
		</div>
		<input type='text' class='form-control' onkeypress="return (event.charCode != 36 && event.keyCode != 36)"  name='homeAddress_text' id = 'homeAddress_geoTextField_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseDetails(fast_search_testDTO.homeAddress)  + "'"):("'" + "" + "'")%>
 placeholder='Road Number, House Number etc' tag='pb_html'>
		<input type='hidden' class='form-control'  name='homeAddress' id = 'homeAddress_geolocation_<%=i%>' value=<%=actionName.equals("edit")?("'" +  GeoLocationDAO2.parseID(fast_search_testDTO.homeAddress)  + "'"):("'" + "1" + "'")%>
 tag='pb_html'>
		<%
		if(actionName.equals("edit"))
		{
		%>
		<label class="control-label"><%=GeoLocationDAO2.parseText(fast_search_testDTO.homeAddress, Language) + "," + GeoLocationDAO2.parseDetails(fast_search_testDTO.homeAddress)%></label>
		<%
		}
		%>
			
						
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value='<%=fast_search_testDTO.jobCat%>' tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FILESDROPZONE, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'filesDropzone_div_<%=i%>'>	
		<%
		fileColumnName = "filesDropzone";
		if(actionName.equals("edit"))
		{
			List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(fast_search_testDTO.filesDropzone);
			%>			
			<%@include file="../pb/dropzoneEditor.jsp"%>
			<%
		}
		else
		{
			ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
			fast_search_testDTO.filesDropzone = ColumnID;
		}
		%>
				
		<div class="dropzone" action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=fast_search_testDTO.filesDropzone%>">
			<input type='file' style="display:none"  name='<%=fileColumnName%>File' id = '<%=fileColumnName%>_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='<%=fileColumnName%>FilesToDelete' id = '<%=fileColumnName%>FilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='<%=fileColumnName%>' id = '<%=fileColumnName%>_dropzone_<%=i%>'  tag='pb_html' value='<%=fast_search_testDTO.filesDropzone%>'/>		
		


	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=fast_search_testDTO.insertionDate%>' tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=fast_search_testDTO.isDeleted%>' tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=fast_search_testDTO.lastModificationTime%>' tag='pb_html'/>
												
					
	





				<div class="col-md-12" style="padding-top: 20px;">
					<legend class="text-left content_legend"><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD, loginDTO)%></legend>
				</div>

				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_RELIGIONCAT, loginDTO)%></th>
										<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_MEDICALEQUIPMENTNAMETYPE, loginDTO)%></th>
										<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_NAMEEN, loginDTO)%></th>
										<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_NAMEBN, loginDTO)%></th>
										<th><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_REMOVE, loginDTO)%></th>
									</tr>
								</thead>
								<tbody id="field-FastSearchTestChild">
						
						
<%
	if(actionName.equals("edit")){
		int index = -1;
		
		
		for(FastSearchTestChildDTO fastSearchTestChildDTO: fast_search_testDTO.fastSearchTestChildDTOList)
		{
			index++;
			
			System.out.println("index index = "+index);

%>	
							
									<tr id = "FastSearchTestChild_<%=index + 1%>">
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='fastSearchTestChild.iD' id = 'iD_hidden_<%=childTableStartingID%>' value='<%=fastSearchTestChildDTO.iD%>' tag='pb_html'/>
	
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='fastSearchTestChild.fastSearchTestId' id = 'fastSearchTestId_hidden_<%=childTableStartingID%>' value='<%=fastSearchTestChildDTO.fastSearchTestId%>' tag='pb_html'/>
										</td>
										<td>										











		<select class='form-control'  name='fastSearchTestChild.religionCat' id = 'religionCat_category_<%=childTableStartingID%>'   tag='pb_html'>		
<%
Options = CatDAO.getOptions(Language, "religion", fastSearchTestChildDTO.religionCat);
%>
<%=Options%>
		</select>
		
										</td>
										<td>										











		<select class='form-control'  name='fastSearchTestChild.medicalEquipmentNameType' id = 'medicalEquipmentNameType_select_<%=childTableStartingID%>'   tag='pb_html'>
<%
Options = CommonDAO.getOptions(Language, "medical_equipment_name", fastSearchTestChildDTO.medicalEquipmentNameType);
%>
<%=Options%>
		</select>
		
										</td>
										<td>										











		<input type='text' class='form-control'  name='fastSearchTestChild.nameEn' id = 'nameEn_text_<%=childTableStartingID%>' value='<%=fastSearchTestChildDTO.nameEn%>'   tag='pb_html'/>					
										</td>
										<td>										











		<input type='text' class='form-control'  name='fastSearchTestChild.nameBn' id = 'nameBn_text_<%=childTableStartingID%>' value='<%=fastSearchTestChildDTO.nameBn%>'   tag='pb_html'/>					
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='fastSearchTestChild.isDeleted' id = 'isDeleted_hidden_<%=childTableStartingID%>' value= '<%=fastSearchTestChildDTO.isDeleted%>' tag='pb_html'/>
											
										</td>
										<td style="display: none;">











		<input type='hidden' class='form-control'  name='fastSearchTestChild.lastModificationTime' id = 'lastModificationTime_hidden_<%=childTableStartingID%>' value='<%=fastSearchTestChildDTO.lastModificationTime%>' tag='pb_html'/>
										</td>
										<td><div class='checker'><span id='chkEdit' ><input type='checkbox' id='fastSearchTestChild_cb_<%=index%>' name='checkbox' value=''/></span></div></td>
									</tr>								
<%	
			childTableStartingID ++;
		}
	}
%>						
						
								</tbody>
							</table>
						
						
						
					</div>
					<div class="form-group">
						<div class="col-xs-9 text-right">

							<button id="remove-FastSearchTestChild" name="removeFastSearchTestChild" type="button"
									class="btn btn-danger remove-me1"><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_REMOVE, loginDTO)%></button>

							<button id="add-more-FastSearchTestChild" name="add-moreFastSearchTestChild" type="button"
									class="btn btn-primary"><%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CHILD_ADD_MORE, loginDTO)%></button>

						</div>
					</div>
					
					<%FastSearchTestChildDTO fastSearchTestChildDTO = new FastSearchTestChildDTO();%>
					
					<template id="template-FastSearchTestChild" >						
								<tr>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='fastSearchTestChild.iD' id = 'iD_hidden_' value='<%=fastSearchTestChildDTO.iD%>' tag='pb_html'/>
	
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='fastSearchTestChild.fastSearchTestId' id = 'fastSearchTestId_hidden_' value='<%=fastSearchTestChildDTO.fastSearchTestId%>' tag='pb_html'/>
									</td>
									<td>











		<select class='form-control'  name='fastSearchTestChild.religionCat' id = 'religionCat_category_'   tag='pb_html'>		
<%
Options = CatDAO.getOptions(Language, "religion", fastSearchTestChildDTO.religionCat);
%>
<%=Options%>
		</select>
		
									</td>
									<td>











		<select class='form-control'  name='fastSearchTestChild.medicalEquipmentNameType' id = 'medicalEquipmentNameType_select_'   tag='pb_html'>
<%
Options = CommonDAO.getOptions(Language, "medical_equipment_name", fastSearchTestChildDTO.medicalEquipmentNameType);
%>
<%=Options%>
		</select>
		
									</td>
									<td>











		<input type='text' class='form-control'  name='fastSearchTestChild.nameEn' id = 'nameEn_text_' value='<%=fastSearchTestChildDTO.nameEn%>'   tag='pb_html'/>					
									</td>
									<td>











		<input type='text' class='form-control'  name='fastSearchTestChild.nameBn' id = 'nameBn_text_' value='<%=fastSearchTestChildDTO.nameBn%>'   tag='pb_html'/>					
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='fastSearchTestChild.isDeleted' id = 'isDeleted_hidden_' value= '<%=fastSearchTestChildDTO.isDeleted%>' tag='pb_html'/>
											
									</td>
									<td style="display: none;">











		<input type='hidden' class='form-control'  name='fastSearchTestChild.lastModificationTime' id = 'lastModificationTime_hidden_' value='<%=fastSearchTestChildDTO.lastModificationTime%>' tag='pb_html'/>
									</td>
									<td><div><span id='chkEdit' ><input type='checkbox' name='checkbox' value=''/></span></div></td>
								</tr>								
						
					</template>
				</div>		

				<%if(canValidate)
				{
				%>	
				<%@include file="../approval_path/validateDiv.jsp"%>
				<%
				}
				%>
				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">					
						<%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_CANCEL_BUTTON, loginDTO)%>						
					</a>
					<button class="btn btn-success" type="submit">
					
						<%=LM.getText(LC.FAST_SEARCH_TEST_ADD_FAST_SEARCH_TEST_SUBMIT_BUTTON, loginDTO)%>						
					
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>

<script type="text/javascript">


$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}

	return preprocessGeolocationBeforeSubmitting('homeAddress', row, false);

	// for(i = 1; i < child_table_extra_id; i ++)
	// {
	// }
	// return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Fast_search_testServlet");	
}

function init(row)
{

			initGeoLocation('homeAddress_geoSelectField_', row, "Fast_search_testServlet");

	for(i = 1; i < child_table_extra_id; i ++)
	{
	}
	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-FastSearchTestChild").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-FastSearchTestChild");

            $("#field-FastSearchTestChild").append(t.html());
			SetCheckBoxValues("field-FastSearchTestChild");
			
			var tr = $("#field-FastSearchTestChild").find("tr:last-child");
			
			tr.attr("id","FastSearchTestChild_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			dateTimeInit("<%=Language%>");
			child_table_extra_id ++;

        });

    
      $("#remove-FastSearchTestChild").click(function(e){    	
	    var tablename = 'field-FastSearchTestChild';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');				
				if(checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


</script>






