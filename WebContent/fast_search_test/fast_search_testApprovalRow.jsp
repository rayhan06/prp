<%@page pageEncoding="UTF-8" %>

<%@page import="fast_search_test.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="workflow.*"%>
<%@page import="dbm.*" %>
<%@page import="holidays.*" %>
<%@page import="approval_summary.*" %>

<%@page import="geolocation.GeoLocationDAO2"%>

<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.FAST_SEARCH_TEST_EDIT_LANGUAGE, loginDTO);

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_FAST_SEARCH_TEST;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Fast_search_testDTO fast_search_testDTO = (Fast_search_testDTO)request.getAttribute("fast_search_testDTO");

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;
boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
boolean isInPreviousOffice = false;
String Message = "Done";

approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("fast_search_test", fast_search_testDTO.iD);
Approval_summaryDTO approval_summaryDTO = approval_summaryDAO.getDTOByTableNameAndTableID("fast_search_test", approval_execution_tableDTO.previousRowId);
System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
{
	canApprove = true;
	if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
	{
		canValidate = true;
	}
}

isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

canTerminate = isInitiator && fast_search_testDTO.isDeleted == 2;

Approval_pathDAO approval_pathDAO = new Approval_pathDAO();
Approval_pathDTO approval_pathDTO = approval_pathDAO.getDTOByID(approval_execution_tableDTO.approvalPathId);
	

System.out.println("fast_search_testDTO = " + fast_search_testDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Fast_search_testDAO fast_search_testDAO = (Fast_search_testDAO)request.getAttribute("fast_search_testDAO");

FilesDAO filesDAO = new FilesDAO();

SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

boolean isOverDue = false;

Date fromDate = new Date(approval_execution_tableDTO.lastModificationTime);
Date dueDate = null;
if(fromDate != null && approvalPathDetailsDTO!= null)
{
	dueDate = CalenderUtil.getDateAfter(fromDate, approvalPathDetailsDTO.daysRequired);
	long today = System.currentTimeMillis();
	boolean timeOver = today > dueDate.getTime();
	isOverDue  =  (approval_execution_tableDTO.approvalStatusCat == Approval_execution_tableDTO.PENDING) && timeOver;
	
	System.out.println("time dif = " + (today - dueDate.getTime()));
}
String formatted_dueDate;

System.out.println("i = " + i  + " OVERDUE = " + isOverDue);

if(isOverDue)
{
%>
<style>
#tr_<%=i%> {
  color: red;
}
</style>
<%
}
%>

											
		
											
											<td id = '<%=i%>_religionCat'>
											<%
											value = fast_search_testDTO.religionCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "religion", fast_search_testDTO.religionCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_medicalEquipmentNameType'>
											<%
											value = fast_search_testDTO.medicalEquipmentNameType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "medical_equipment_name", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_nameEn'>
											<%
											value = fast_search_testDTO.nameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_nameBn'>
											<%
											value = fast_search_testDTO.nameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
		
											
		
											
		
											
											<td id = '<%=i%>_homeAddress'>
											<%
											value = fast_search_testDTO.homeAddress + "";
											%>
											<%=GeoLocationDAO2.getAddressToShow(value, Language)%>
				
			
											</td>
		
											
		
											
											<td id = '<%=i%>_filesDropzone'>
											<%
											value = fast_search_testDTO.filesDropzone + "";
											%>
											<%
											{
												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(fast_search_testDTO.filesDropzone);
											%>
												<%@include file="../pb/dropzoneViewer.jsp"%>
											<%												
											}
											%>
				
			
											</td>
		
											
		
											
		
											
		
	

<td>
											<a href='Fast_search_testServlet?actionType=view&ID=<%=fast_search_testDTO.iD%>'&isPermanentTable=false><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
											
											
											</td>
											
											<td>
												<%
												value = approval_pathDTO.nameEn;
												%>											
															
												<%=value%>
											</td>
											
											<td>
												<%
												value = WorkflowController.getNameFromUserId(approval_summaryDTO.initiator, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td id = '<%=i%>_dateOfInitiation'>
												<%
												value = approval_summaryDTO.dateOfInitiation + "";
												%>
												<%

												String formatted_dateOfInitiation = simpleDateFormat.format(new Date(Long.parseLong(value)));
												%>
												<%=Utils.getDigits(formatted_dateOfInitiation, Language)%>
				
			
											</td>
											
											
											<td>
												<%
												value = WorkflowController.getNameFromOrganogramId(approval_summaryDTO.assignedTo, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td >
												<%
												String formatted_dateOfAssignment = simpleDateFormat.format(new Date(approval_execution_tableDTO.lastModificationTime));
												%>
												<%=Utils.getDigits(formatted_dateOfAssignment, Language)%>
				
			
											</td>
											
											<td>
											
												<%
												
												
												if(dueDate!= null)
												{
													formatted_dueDate = simpleDateFormat.format(dueDate);
												}
												else
												{
													formatted_dueDate = "";
												}
												%>
												<%=Utils.getDigits(formatted_dueDate, Language)%>
				
			
											</td>
		
											
											
											<td>
												<%
												if(isOverDue)
												{
													value = LM.getText(LC.HM_OVERDUE, loginDTO);
												}
												else
												{
													value = CatDAO.getName(Language, "approval_status", approval_summaryDTO.approvalStatusCat);
												}
												
												%>											
															
												<%=value%>
											</td>
	
											<td>
											<%
											if(canApprove || canTerminate)
											{
												%>
												<a href='Fast_search_testServlet?actionType=view&ID=<%=fast_search_testDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'>View</a>
												
												<%
											}
											else
											{
											
											 	%>
											 	<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
											 	<%
											}
											 %>																						
											</td>
											
											<td>
											<%
											if(canValidate)
											{
												%>
												<a href='Fast_search_testServlet?actionType=getEditPage&ID=<%=fast_search_testDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
												
												<%
											}
											else
											{
											
											 	%>
											 	<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>
											 	<%
											}
											 %>																						
											</td>
											
											
											<td>
												<a href="Approval_execution_tableServlet?actionType=search&tableName=fast_search_test&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"  ><%=LM.getText(LC.HM_HISTORY, loginDTO)%></a>
											</td>
											

