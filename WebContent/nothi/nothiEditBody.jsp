<%@page import="nothi_configuration.Nothi_configurationDAO"%>
<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="nothi.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
NothiDTO nothiDTO = new NothiDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	nothiDTO = NothiDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = nothiDTO;
String tableName = "nothi";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.NOTHI_ADD_NOTHI_ADD_FORMNAME, loginDTO);
String servletName = "NothiServlet";
boolean isLangEng = Language.equalsIgnoreCase("english");
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="NothiServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=nothiDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.NOTHI_ADD_NOTHICONFIGURATIONTYPE, loginDTO)%>															</label>
                                                            <div class="col-8">
																<select class='form-control'  name='nothiConfigurationType' 
																id = 'nothiConfigurationType_select_<%=i%>'   tag='pb_html'
																onchange = "getDetails(this.value)"
																>
																<%
																	Options = Nothi_configurationDAO.getInstance().getUnusedOptions(isLangEng, nothiDTO.iD);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
																						
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.NOTHI_ADD_DESCRIPTION, loginDTO)%>															
															</label>
                                                            <div class="col-8" id = "descriptionDiv">
																
															</div>
                                                      </div>

                                                      <div class="form-group row">
                                                            <label class="col-4 col-form-label text-right">
															<%=LM.getText(LC.HM_DATE, loginDTO)%>															</label>
                                                            <div class="col-8">
																<%value = "nothiDate_js_" + i;%>
																<jsp:include page="/date/date.jsp">
																	<jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
																	<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
																</jsp:include>
																<input type='hidden' name='nothiDate' id = 'nothiDate_date_<%=i%>' value= '<%=dateFormat.format(new Date(nothiDTO.nothiDate))%>' tag='pb_html'>
															</div>
                                                      </div>
																	
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.NOTHI_ADD_NOTHI_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.NOTHI_ADD_NOTHI_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	preprocessDateBeforeSubmitting('nothiDate', row);

	submitAddForm2();
	return false;
}

function init(row)
{

	setDateByStringAndId('nothiDate_js_' + row, $('#nothiDate_date_' + row).val());

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

function getDetails(configId)
{
	console.log('configId: ' + configId);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
        	$("#descriptionDiv").html(this.responseText);
        } else {
            //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
        }
    };

    xhttp.open("GET", "Nothi_configurationServlet?actionType=getDetails&id=" + configId , true);
    xhttp.send();
}


</script>






