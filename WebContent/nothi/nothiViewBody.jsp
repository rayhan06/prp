

<%@page import="nothi_history.Nothi_historyDAO"%>
<%@page import="nothi_history.Nothi_historyDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="nothi.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "NothiServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
NothiDTO nothiDTO = NothiDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = nothiDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>
<%
boolean isLangEng = Language.equalsIgnoreCase("english");
%>	

<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.NOTHI_ADD_NOTHI_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="ml-auto mr-5 mt-4">
		   
		    <button type="button" class="btn" id='printer2'
		            onclick="printAnyDiv('modalbody')">
		        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
		    </button>
		    
		</div>
        <div class="kt-portlet__body form-body" id = modalbody>                               
                                <div class="mt-5">
									<div class=" div_border attachement-div">
										<table class="table table-bordered table-striped">
											<tr>
												<td><b><%=LM.getText(LC.NOTHI_ADD_NOTHINUMBER, loginDTO)%></b></td>
												<td><%=Utils.getDigits(nothiDTO.nothiNumber, Language)%></td>
												
												<td><b><%=LM.getText(LC.NOTHI_ADD_DESCRIPTION, loginDTO)%></b></td>
												<td><%=nothiDTO.description%></td>
											
											</tr>
											<tr>
												<td><b><%=LM.getText(LC.NOTHI_ADD_ASSIGNEDOFFICEID, loginDTO)%></b></td>
												<td><%=WorkflowController.getOfficeUnitName(nothiDTO.assignedOfficeId, Language)%></td>
											
												<td><b><%=isLangEng?"Current Wing":"বর্তমান উইং"%></b></td>
												<td><%=WorkflowController.getOfficeUnitName(nothiDTO.assignedOfficeId, Language)%></td>
											</tr>
											
											<tr>
												<td><b><%=LM.getText(LC.HM_INSERTED_BY, loginDTO)%></b></td>
												<td><%=WorkflowController.getNameFromEmployeeRecordID(nothiDTO.insertedByErId, Language)%>
												<br>
												<%=WorkflowController.getOrganogramName(nothiDTO.insertedByOrganogramId, isLangEng)%>
												<br>
												<%=WorkflowController.getOfficeUnitName(nothiDTO.insertedByOfficeId, Language)%></td>
												
												<td><b><%=isLangEng?"Entry DAte":"এন্ট্রির তারিখ"%></b></td>
												<td><%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, nothiDTO.nothiDate), Language)%></td>
											</tr>
											
										</table>
									</div>
								</div>

								<div class="mt-5">
									<div class=" div_border attachement-div">
										<h5><%=LM.getText(LC.HM_HISTORY, loginDTO)%></h5>
										<table class="table table-bordered table-striped">
											<tr>
												<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
												<th><%=LM.getText(LC.NOTHI_HISTORY_ADD_FROMOFFICEID, loginDTO)%></th>
												<th><%=isLangEng?"From Wing":"প্রেরক উইং"%></th>
												<th><%=LM.getText(LC.NOTHI_HISTORY_ADD_TOOFFICEID, loginDTO)%></th>
												<th><%=isLangEng?"To Wing":"প্রাপক উইং"%></th>
												<th><%=LM.getText(LC.NOTHI_HISTORY_ADD_INSERTEDBYERID, loginDTO)%></th>
												
											</tr>
											<%
											List<Nothi_historyDTO> nothi_historyDTOs = Nothi_historyDAO.getInstance().getAllByNothiId(nothiDTO.iD);
											for(Nothi_historyDTO nothi_historyDTO: nothi_historyDTOs)
											{
												%>
												<tr>
													<td>
													<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, nothi_historyDTO.transferDate), Language)%>
													</td>
													<td>
													<%=WorkflowController.getOfficeUnitName(nothi_historyDTO.fromOfficeId, Language)%>
													</td>
													
													<td>
													<%=WorkflowController.getWingNameFromOfficeUnitId(nothi_historyDTO.fromOfficeId, isLangEng)%>
													</td>
				
													<td>
													<%=WorkflowController.getOfficeUnitName(nothi_historyDTO.toOfficeId, Language)%>
													</td>
													
													<td>
													<%=WorkflowController.getWingNameFromOfficeUnitId(nothi_historyDTO.toOfficeId, isLangEng)%>
													</td>
				
													<td>
													<%=WorkflowController.getNameFromEmployeeRecordID(nothi_historyDTO.insertedByErId, Language)%>
													<br>
													<%=WorkflowController.getOrganogramName(nothi_historyDTO.insertedByOrganogramId, isLangEng)%>
													<br>
													<%=WorkflowController.getOfficeNameFromOrganogramId(nothi_historyDTO.insertedByOrganogramId, isLangEng)%>
													</td>
													
												</tr>
												<%

											}
											%>
										</table>
									</div>
								</div>

           
        </div>
    </div>
</div>