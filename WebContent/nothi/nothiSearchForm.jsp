
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="nothi.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>

<%@ page import="workflow.*"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navNOTHI";
String servletName = "NothiServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>	
<%
boolean isLangEng = Language.equalsIgnoreCase("english");
%>			
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.HM_SL, loginDTO)%></th>
								<th><%=LM.getText(LC.NOTHI_ADD_NOTHINUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.NOTHI_ADD_DESCRIPTION, loginDTO)%></th>
								<th><%=LM.getText(LC.NOTHI_ADD_ASSIGNEDOFFICEID, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_INSERTED_BY, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_DATE, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<NothiDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											NothiDTO nothiDTO = (NothiDTO) data.get(i);
																																
											
											%>
											<tr>
											
											<td>
												<%=Utils.getDigits(i + 1 + ((rn2.getCurrentPageNo() - 1) * rn2.getPageSize()), Language)%>.
											</td>

											<td>
											<%=Utils.getDigits(nothiDTO.nothiNumber, Language)%>
											</td>
		
											<td>
											<%=nothiDTO.description%>
											</td>
		
		
											<td>
											<%=WorkflowController.getOfficeUnitName(nothiDTO.assignedOfficeId, Language)%>
											</td>
		
											
											<td>
											<%=WorkflowController.getNameFromEmployeeRecordID(nothiDTO.insertedByErId, Language)%>
											<br>
											<%=WorkflowController.getOrganogramName(nothiDTO.insertedByOrganogramId, isLangEng)%>
											<br>
											<%=WorkflowController.getOfficeUnitName(nothiDTO.insertedByOfficeId, Language)%>
											</td>
		
		
		
		
		
											<td>
											<%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, nothiDTO.nothiDate), Language)%>
											</td>
		
		
		
	
											<%CommonDTO commonDTO = nothiDTO; %>
											<td>
											    <button
											            type="button"
											            class="btn-sm border-0 shadow bg-light btn-border-radius"
											            style="color: #ff6b6b;"
											            onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
											    >
											        <i class="fa fa-eye"></i>
											    </button>
											</td>
											
																				
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=nothiDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			