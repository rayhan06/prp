<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="pi_unit.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>

<%
    Pi_unitDTO pi_unitDTO = new Pi_unitDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        pi_unitDTO = Pi_unitDAO.getInstance().getDTOByID(ID);
    }
    System.out.println("ID = " + ID);
    CommonDTO commonDTO = pi_unitDTO;
    String tableName = "pi_unit";
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = LM.getText(LC.PI_UNIT_ADD_PI_UNIT_ADD_FORMNAME, loginDTO);
    String servletName = "Pi_unitServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_unitServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=pi_unitDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PI_UNIT_ADD_NAMEEN, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' autocomplete="off" list="unit_suggestion_en" name='nameEn'
                                                   id='nameEn_text_<%=i%>' value='<%=pi_unitDTO.nameEn%>'
                                                   tag='pb_html'/>

                                            <datalist id="unit_suggestion_en">
                                                <%=Pi_unitRepository.getInstance().getOptionsForSuggestion("English", -1)%>
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PI_UNIT_ADD_NAMEBN, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' autocomplete="off" list="unit_suggestion_bn" name='nameBn'
                                                   id='nameBn_text_<%=i%>' value='<%=pi_unitDTO.nameBn%>'
                                                   tag='pb_html'/>
                                            <datalist id="unit_suggestion_bn">
                                                <%=Pi_unitRepository.getInstance().getOptionsForSuggestion("Bangla", -1)%>
                                            </datalist>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>' value='<%=pi_unitDTO.searchColumn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>' value='<%=pi_unitDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=pi_unitDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>' value='<%=pi_unitDTO.insertionDate%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='lastModifierUser'
                                           id='lastModifierUser_hidden_<%=i%>' value='<%=pi_unitDTO.lastModifierUser%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>' value='<%=pi_unitDTO.isDeleted%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=pi_unitDTO.lastModificationTime%>' tag='pb_html'/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-11">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_UNIT_ADD_PI_UNIT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PI_UNIT_ADD_PI_UNIT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    let duplicatePackageErr;
    let requiredMsg, duplicateMsg;

    function PreprocessBeforeSubmiting(row, action) {
        console.log("action = " + action);

        let form = $("#bigform");
        form.validate();
        let valid = form.valid();
        if (valid) {
            submitAddForm2();
        }
        return false;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Pi_unitServlet");
    }

    function init(row) {

        let lang = '<%=Language%>';

        if (lang == 'English') {
            requiredMsg = 'This is required';
        } else {
            requiredMsg = '<%="এই তথ্যটি আবশ্যক"%>';
        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                nameEn: {
                    required: true,
                },
                nameBn: {
                    required: true,
                },
            },
            messages: {
                nameEn: requiredMsg,
                nameBn: requiredMsg,
            }
        });

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






