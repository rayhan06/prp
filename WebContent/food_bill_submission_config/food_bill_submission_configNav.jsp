<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="budget_office.Budget_officeRepository" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "Food_bill_submission_configServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__body">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="electionDetailsId">
                            <%=isLanguageEnglishNav ? "Parliament Number" : "সংসদ নম্বর"%>
                        </label>
                        <div class="col-md-9">
                            <select id="electionDetailsId"
                                    name='electionDetailsId'
                                    class='form-control rounded shadow-sm'
                                    onchange="electionDetailsChanged(this);"
                            >
                                <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="parliamentSessionId">
                            <%=isLanguageEnglishNav ? "Parliament Session" : "সংসদ অধিবেশন"%>
                        </label>
                        <div class="col-md-9">
                            <select id="parliamentSessionId" name='parliamentSessionId'
                                    class='form-control rounded shadow-sm'>
                                <%--Dynamically Added with AJAX--%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="foodBillTypeCat">
                            <%=isLanguageEnglishNav ? "Bill Type" : "বিলের ধরণ"%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='foodBillTypeCat'
                                    id='foodBillTypeCat'
                                    style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions(
                                        SessionConstants.FOOD_BILL_TYPE_CAT_DOMAIN_NAME,
                                        Language,
                                        null
                                )%>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="location.href='Food_bill_submission_configServlet?actionType=getAddPage'"
                            style="background-color: #00a1d4;">
                        <%=isLanguageEnglishNav ? "Add New" : "নতুন যোগ করুন"%>
                    </button>
                </div>
                <div class="col-6 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<template id="loader">
    <div class="modal-body">
        <div class="search-loader-container-circle ">
            <div class="search-loader-circle"></div>
        </div>
    </div>
</template>


<script type="text/javascript">
    const parliamentSessionIdSelect = document.getElementById('parliamentSessionId');
    const electionDetailsIdSelect = document.getElementById('electionDetailsId');
    const foodBillTypeCatSelect = document.getElementById('foodBillTypeCat');

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                setTimeout(() => {
                    document.getElementById('tableForm').innerHTML = this.responseText;
                    setPageNo();
                    searchChanged = 0;
                }, 200);
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "<%=url%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        let params = 'electionDetailsId=' + electionDetailsIdSelect.value;
        params += '&parliamentSessionId=' + parliamentSessionIdSelect.value;
        params += '&foodBillTypeCat=' + foodBillTypeCatSelect.value;

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);
    }

    async function electionDetailsChanged(selectElement) {
        parliamentSessionIdSelect.innerHTML = '';
        let electionDetailsId = selectElement.value;
        if (electionDetailsId === -1 || electionDetailsId === "") {
            return;
        }
        const url = 'OT_type_CalendarServlet?actionType=ajax_getParliamentSession'
                    + '&electionDetailsId=' + electionDetailsId;
        const response = await fetch(url);
        parliamentSessionIdSelect.innerHTML = await response.text();
    }
</script>