<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.List" %>
<%@ page import="util.StringUtils" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="food_bill_submission_config.Food_bill_submission_configDTO" %>
<%@ page import="parliament_session.Parliament_sessionRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navOVERTIME_ALLOWANCE";
    String servletName = "Food_bill_submission_configServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead class="text-center">
        <tr>
            <th>
                <%=isLanguageEnglish ? "Bill Type" : "বিলের ধরণ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Submission Start Date" : "জমা শুরুর তারিখ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Submission End Date" : "জমা শেষের তারিখ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Edit" : "পরিবর্তন"%>
            </th>
            <th class="">
                <div class="text-center">
                    <span><%=UtilCharacter.getDataByLanguage(Language, "সব", "All")%></span>
                </div>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<Food_bill_submission_configDTO> data = (List<Food_bill_submission_configDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (Food_bill_submission_configDTO submissionConfigDTO : data) {
        %>
        <tr>
            <td>
                <%=submissionConfigDTO.getText(isLanguageEnglish)%>
            </td>
            <td>
                <%=StringUtils.getFormattedDate(isLanguageEnglish, submissionConfigDTO.startDate)%>
            </td>
            <td>
                <%=StringUtils.getFormattedDate(isLanguageEnglish, submissionConfigDTO.endDate)%>
            </td>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow btn-border-radius text-white"
                        style="background-color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=submissionConfigDTO.iD%>'"
                >
                    <i class="fa fa-edit"></i>
                </button>
            </td>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'>
                        <input type='checkbox' name='ID' value='<%=submissionConfigDTO.iD%>'/>
                    </span>
                </div>
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>