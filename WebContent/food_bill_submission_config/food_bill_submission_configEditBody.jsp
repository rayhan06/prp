<%@page pageEncoding="UTF-8" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="pbReport.DateUtils" %>
<%@ page import="election_details.Election_detailsRepository" %>
<%@ page import="food_bill_submission_config.Food_bill_submission_configDTO" %>
<%@ page import="parliament_session.Parliament_sessionRepository" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>

<%
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String formTitle = isLanguageEnglish ? "Food Bill Submission Time Configuration" : "খাবার ভাতা জমাদানের সময় নির্ধারণ";

    String actionName = "ajax_edit";
    boolean isEditPage = true;
    Food_bill_submission_configDTO submissionConfigDTO = (Food_bill_submission_configDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    if (submissionConfigDTO == null) {
        submissionConfigDTO = new Food_bill_submission_configDTO();
        isEditPage = false;
        actionName = "ajax_add";
    }

    long defaultStartTime = DateUtils.get12amTime(System.currentTimeMillis());
    long defaultEndTime = defaultStartTime + (DateUtils.ONE_DAY_IN_MILLIS * 7) - 1;
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal" id="ot-bill-submission-config-form">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <%--for data goes here--%>

                                    <input type="hidden" name="iD" value="<%=submissionConfigDTO.iD%>">

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="electionDetailsId">
                                            <%=isLanguageEnglish ? "Parliament Number" : "সংসদ নম্বর"%>
                                        </label>
                                        <div class="col-md-9">
                                            <%if (isEditPage) {%>
                                            <div class='form-control rounded shadow-sm'>
                                                <%=Election_detailsRepository.getInstance().getText(submissionConfigDTO.electionDetailsId, Language)%>
                                                <input type="hidden"
                                                       id="electionDetailsId"
                                                       name='electionDetailsId'
                                                       value="<%=submissionConfigDTO.electionDetailsId%>">
                                            </div>
                                            <%} else {%>
                                            <select id="electionDetailsId"
                                                    name='electionDetailsId'
                                                    class='form-control rounded shadow-sm'
                                                    onchange="electionDetailsChanged(this);"
                                            >
                                                <%=Election_detailsRepository.getInstance().buildOptions(Language, null)%>
                                            </select>
                                            <%}%>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label" for="parliamentSessionId">
                                            <%=isLanguageEnglish ? "Parliament Session" : "সংসদ অধিবেশন"%>
                                        </label>
                                        <div class="col-md-9">
                                            <%if (isEditPage) {%>
                                            <div class='form-control rounded shadow-sm'>
                                                <%=Parliament_sessionRepository.getInstance().getText(submissionConfigDTO.parliamentSessionId, isLanguageEnglish)%>
                                                <input type="hidden"
                                                       id="parliamentSessionId"
                                                       name='parliamentSessionId'
                                                       value="<%=submissionConfigDTO.parliamentSessionId%>">
                                            </div>
                                            <%} else {%>
                                            <select id="parliamentSessionId"
                                                    name='parliamentSessionId'
                                                    class='form-control rounded shadow-sm'>
                                                <%--Dynamically added with ajax--%>
                                            </select>
                                            <%}%>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right" for="foodBillTypeCat">
                                            <%=isLanguageEnglish ? "Bill Type" : "বিলের ধরণ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='foodBillTypeCat'
                                                    id="foodBillTypeCat"
                                                    onchange="foodBillTypeCatChanged(this)"
                                                    style="width: 100%">
                                                <%=CatRepository.getInstance().buildOptionsWithoutSelectOption(
                                                        SessionConstants.FOOD_BILL_TYPE_CAT_DOMAIN_NAME,
                                                        Language,
                                                        submissionConfigDTO.foodBillTypeCat
                                                )%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Submission Start Date" : "জমা শুরুর তারিখ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="startDate_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='startDate' id='startDate' value=''>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Submission  End Date" : "জমা শেষের তারিখ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="endDate_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='endDate' value=''>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Applicable for Offices" : "যেসব অফিসের জন্য প্রযোজ্য"%>
                                        </label>
                                        <div class="col-md-9" style="display: flex; align-items: center;">
                                            <div>
                                                <label for="allOfficesCheckbox">
                                                    <%=isLanguageEnglish ? "All Offices" : "সকল অফিস"%>
                                                </label>
                                            </div>
                                            <div class="ml-3">
                                                <input type="checkbox"
                                                       id="allOfficesCheckbox"
                                                       name="allOfficesCheckbox"
                                                       <%=submissionConfigDTO.allowedOfficeIds == null ? "checked" : ""%>
                                                       onchange="allOfficesCheckboxChanged(this)"
                                                >
                                            </div>
                                        </div>
                                        <div class="offset-md-3 col-md-9" id="allowedOfficeIds-div">
                                            <select class='form-control rounded shadow-sm'
                                                    id="allowedOfficeIds"
                                                    name="allowedOfficeIds"
                                                    multiple="multiple"
                                            >
                                                <%=Office_unitsRepository.getInstance().buildOptionsMultiSelect(
                                                        Language,
                                                        submissionConfigDTO.getAllowedOfficeIdsStr()
                                                )%>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button type="button" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'">
                                <%=isLanguageEnglish ? "Cancel" : "বাতিল করুন"%>
                            </button>
                            <button type="button" id="submit-btn"
                                    class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    onclick="submitOtBillSubmissionConfigForm()">
                                <%=isLanguageEnglish ? "Submit" : "জমাদিন"%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    const form = $('#ot-bill-submission-config-form');
    const monthInput = $('#month');
    const yearInput = $('#year');
    const startDateInput = $('#startDate');
    const endDateInput = $('#endDate');
    const parliamentSessionIdSelect = document.getElementById('parliamentSessionId');
    const electionDetailsIdSelect = document.getElementById('electionDetailsId');
    const allOfficesCheckbox = document.getElementById('allOfficesCheckbox');
    const $allowedOfficeIds = $('#allowedOfficeIds');
    const $allowedOfficeIdsDiv = $('#allowedOfficeIds-div');

    $(() => {
        select2MultiSelector("#allowedOfficeIds", '<%=Language%>');
        allOfficesCheckboxChanged(allOfficesCheckbox);

        $('#startDate_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('startDate_js', true);
            const dateStr = isValidDate ? getDateStringById('startDate_js') : '';
            startDateInput.val(dateStr);
        });

        $('#endDate_js').on('datepicker.change', (event, param) => {
            const isValidDate = dateValidator('endDate_js', true);
            const dateStr = isValidDate ? getDateStringById('endDate_js') : '';
            endDateInput.val(dateStr);
        });

        <%if(isEditPage){%>
        setDateByTimestampAndId('startDate_js', <%=submissionConfigDTO.startDate%>);
        startDateInput.val(getDateStringById('startDate_js'));

        setDateByTimestampAndId('endDate_js', <%=submissionConfigDTO.endDate%>);
        endDateInput.val(getDateStringById('endDate_js'));
        <%} else {%>
        setDateByTimestampAndId('startDate_js', <%=defaultStartTime%>);
        startDateInput.val(getDateStringById('startDate_js'));

        setDateByTimestampAndId('endDate_js', <%=defaultEndTime%>);
        endDateInput.val(getDateStringById('endDate_js'));
        <%}%>
    });

    function submitOtBillSubmissionConfigForm() {
        submitAjaxByData(form.serialize(), "Food_bill_submission_configServlet?actionType=<%=actionName%>");
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    async function electionDetailsChanged(selectElement) {
        parliamentSessionIdSelect.innerHTML = '';
        let electionDetailsId = selectElement.value;
        if (electionDetailsId === -1 || electionDetailsId === "") {
            return;
        }
        const url = 'OT_type_CalendarServlet?actionType=ajax_getParliamentSession'
                    + '&electionDetailsId=' + electionDetailsId;
        const response = await fetch(url);
        parliamentSessionIdSelect.innerHTML = await response.text();
    }

    function allOfficesCheckboxChanged(allOfficesCheckbox) {
        if(allOfficesCheckbox.checked) {
            $allowedOfficeIdsDiv.hide();
            $allowedOfficeIds.val([]);
        } else {
            $allowedOfficeIdsDiv.show();
        }
        $allowedOfficeIds.change();
    }

    const ARREARS_BILL = '2';

    function foodBillTypeCatChanged(selectElem) {
        const overtimeBillType = selectElem.value;
        allOfficesCheckbox.checked = overtimeBillType !== ARREARS_BILL;
        allOfficesCheckboxChanged(allOfficesCheckbox);
    }
</script>