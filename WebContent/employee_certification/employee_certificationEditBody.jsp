<%@page import="dbm.DBMW" %>
<%@page import="employee_certification.Employee_certificationDTO" %>
<%@page import="files.FilesDAO" %>
<%@page import="files.FilesDTO" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="pb.CatRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.List" %>

<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String actionName = request.getParameter("actionType").equals("edit") ? "edit" : "add";
    long empId;
    if(request.getParameter("empId")!=null){
        empId = Long.parseLong(request.getParameter("empId"));
    }else{
        empId = userDTO.employee_record_id;
    }
    String formTitle;
    Employee_certificationDTO employee_certificationDTO;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEE_CERTIFICATION_EDIT_FORMNAME, loginDTO);
        employee_certificationDTO = (Employee_certificationDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        formTitle = LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_EMPLOYEE_CERTIFICATION_ADD_FORMNAME, loginDTO);
        employee_certificationDTO=new Employee_certificationDTO();
    }
    int i = 0;
    long ColumnID;
    FilesDAO filesDAO = new FilesDAO();

    String url = "Employee_certificationServlet?actionType=ajax_" + actionName + "&isPermanentTable=true&empId=" + empId+"&iD="+employee_certificationDTO.iD;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }

    int year = Calendar.getInstance().get(Calendar.YEAR);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form" action="<%=url%>"
              id="employee-certification" name="employee-certification"
              enctype="multipart/form-data">
            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CERTIFICATIONNAME, loginDTO)) : (LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_CERTIFICATIONNAME, loginDTO))%><span
                                                style="color: red">&nbsp;*</span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='certificationName_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='certificationName'
                                                   id='certificationName_text_<%=i%>'
                                                   value='<%=employee_certificationDTO.certificationName == null ? "" :employee_certificationDTO.certificationName%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGORGANIZATION, loginDTO)) : (LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_ISSUINGORGANIZATION, loginDTO))%><span
                                                style="color: red">&nbsp;*</span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='issuingOrganization_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='issuingOrganization'
                                                   id='issuingOrganization_text_<%=i%>'
                                                   value='<%=employee_certificationDTO.issuingOrganization==null?"":employee_certificationDTO.issuingOrganization%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGINSTITUTEADDRESS, loginDTO)) : (LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_ISSUINGINSTITUTEADDRESS, loginDTO))%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='issuingInstituteAddress_div_<%=i%>'>
                                            <input type='text' class='form-control'
                                                   name='issuingInstituteAddress'
                                                   id='issuingInstituteAddress_text_<%=i%>'
                                                   value='<%=employee_certificationDTO.issuingInstituteAddress == null ? "" : employee_certificationDTO.issuingInstituteAddress%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_VALIDFROM, loginDTO)%><span
                                                style="color: red">&nbsp;*</span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='validFrom_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="validFrom_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden'
                                               class='form-control'
                                               id='validFrom_text'
                                               name='validFrom'
                                               value='' tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_HASEXPIRYCAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='hasExpiryCat_div_<%=i%>'>
                                            <select class='form-control hasExpiry' name='hasExpiryCat'
                                                    id='hasExpiryCat_category_<%=i%>'
                                                    tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("has_expiry", Language, employee_certificationDTO.hasExpiryCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               id='validUpto_label_<%=i%>'>
                                            <%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDUPTO, loginDTO)%><span
                                                style="color: red">&nbsp;*</span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='validUpto_div_<%=i%>'>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="validUpto_date_js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR"
                                                           value="<%=year + 60%>"></jsp:param>
                                            </jsp:include>
                                        </div>
                                        <input type='hidden'
                                               class='form-control'
                                               id='validUpto_text'
                                               name='validUpto'
                                               value='' tag='pb_html'/>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CREDENTIALID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='credentialId_div_<%=i%>'>
                                            <input type='text' class='form-control' name='credentialId'
                                                   id='credentialId_text_<%=i%>'
                                                   value='<%=employee_certificationDTO.credentialId == null ?"":employee_certificationDTO.credentialId%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=(actionName.equals("edit")) ? (LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_FILESDROPZONE, loginDTO)) : (LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_FILESDROPZONE, loginDTO))%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='filesDropzone_div_<%=i%>'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(employee_certificationDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                             style='width:100px'/>
                                                        <%
                                                            }
                                                        %>
                                                        <a href='Employee_certificationServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                           download><%=filesDTO.fileTitle%>
                                                        </a>
                                                        <a class='btn btn-danger'
                                                           onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                            <div class="dropzone"
                                                 action="Employee_certificationServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_certificationDTO.filesDropzone:ColumnID%>">
                                                <input type='file' style="display:none"
                                                       name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File_<%=i%>'
                                                       tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete_<%=i%>'
                                                   value='' tag='pb_html'/>
                                            <input type='hidden' name='filesDropzone'
                                                   id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=actionName.equals("edit")?employee_certificationDTO.filesDropzone:ColumnID%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%
                                if (actionName.equals("edit")) {
                            %>
                            <%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEE_CERTIFICATION_CANCEL_BUTTON, loginDTO)%>
                            <%
                            } else {
                            %>
                            <%=LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_EMPLOYEE_CERTIFICATION_CANCEL_BUTTON, loginDTO)%>
                            <%
                                }

                            %>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                onclick="submitForm()">
                            <%
                                if (actionName.equals("edit")) {
                            %>
                            <%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEE_CERTIFICATION_SUBMIT_BUTTON, loginDTO)%>
                            <%
                            } else {
                            %>
                            <%=LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_EMPLOYEE_CERTIFICATION_SUBMIT_BUTTON, loginDTO)%>
                            <%
                                }
                            %>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">
    const form = $("#employee-certification");

    $(document).ready(function () {
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        dateTimeInit("<%=Language%>");
        select2SingleSelector("#hasExpiryCat_category_0", '<%=Language%>');
        $('#validFrom_date_js').on('datepicker.change', () => {
            setMinDateById('validUpto_date_js', getDateStringById('validFrom_date_js'));
        });
        <%if (actionName.equals("edit")) {%>
        setDateByStringAndId('validFrom_date_js', '<%=employee_certificationDTO.validFrom%>');
        setDateByStringAndId('validUpto_date_js', '<%=employee_certificationDTO.validUpto%>');
        setMinDateById('validUpto_date_js', getDateStringById('validFrom_date_js'));
        <%
        }
        %>
        showValidUptoInput();
        $(".hasExpiry").change(function () {
            showValidUptoInput();
        }).change();
        form.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                certificationName: "required",
                issuingOrganization: "required"
            },
            messages: {
                certificationName: "<%=LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_EMPLOYEE_CERTIFICATION_PLEASE_ENTER_CERTIFICATION_NAME, userDTO)%>",
                issuingOrganization: "<%=LM.getText(LC.EMPLOYEE_CERTIFICATION_ADD_EMPLOYEE_CERTIFICATION_PLEASE_ENTER_ISSUING_ORGANIZATION, userDTO)%>"
            }
        });
    });

    function showValidUptoInput() {
        let optionValue = $('.hasExpiry option:selected').text()
        if (optionValue === 'Yes' || optionValue === 'হ্যা') {
            $("#validUpto_div_0").show();
            $("#validUpto_label_0").show();
        } else {
            $("#validUpto_div_0").hide();
            $("#validUpto_label_0").hide();
            $("#validUpto_text").val('');
            resetDateById('validUpto_date_js');
        }
    }


    function isFormValid() {
        $('#validFrom_text').val(getDateStringById('validFrom_date_js'));
        $('#validUpto_text').val(getDateStringById('validUpto_date_js'));

        const jQueryValid = $('#employee-certification').valid();
        const validUptoDateValid = dateValidator('validUpto_date_js', true);
        const validFromDateValid = dateValidator('validFrom_date_js', true);

        let optionValue = $('.hasExpiry option:selected').text();
        if (optionValue === 'Yes' || optionValue === 'হ্যা') {
            return jQueryValid && validUptoDateValid && validFromDateValid;
        } else
            return jQueryValid && validFromDateValid;
    }

    function submitForm() {
        buttonStateChange(true);
        if (isFormValid()) {
            submitAjaxForm('employee-certification');
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }
</script>
