<%@page import="employee_records.Employee_recordsRepository" %>

<td>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(employee_certificationDTO.employeeRecordsId, Language)%>
</td>


<td>
    <%=employee_certificationDTO.certificationName%>
</td>


<td>
    <%=employee_certificationDTO.issuingOrganization%>
</td>

<td>
    <%=employee_certificationDTO.validFrom%>
</td>


<td>
    <%=employee_certificationDTO.credentialId%>
</td>

<td>
	<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
			onclick="location.href='Employee_certificationServlet?actionType=view&ID=<%=employee_certificationDTO.iD%>&empId=<%=employee_certificationDTO.employeeRecordsId%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<td>
	<button
			type="button"
			class="btn-sm border-0 shadow btn-border-radius text-white"
			style="background-color: #ff6b6b;"
			onclick="location.href='Employee_certificationServlet?actionType=getEditPage&ID=<%=employee_certificationDTO.iD%>&empId=<%=employee_certificationDTO.employeeRecordsId%>'">
		<i class="fa fa-edit"></i>
	</button>
</td>


<td class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_certificationDTO.iD%>'/></span>
    </div>
</td>