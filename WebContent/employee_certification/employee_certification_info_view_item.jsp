
<td style="vertical-align: middle;">
    <%=employee_certificationDTO.certificationName%>
</td>

<td style="vertical-align: middle;">
    <%=employee_certificationDTO.issuingOrganization%>
</td>

<%
    if (!actionName.equals("viewSummary")) {
%>
<td style="text-align: center; vertical-align: middle;">
    <div class="btn-group" role="group" aria-label="Basic example">
        <form action="Employee_certificationServlet?isPermanentTable=true&actionType=delete&tab=5&ID=<%=employee_certificationDTO.iD%>&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>"
              method="POST" id="empCertificationTableForm<%=certificationIndex%>" enctype = "multipart/form-data">
            <button type="button" class="btn-primary" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                    onclick="location.href='Employee_certificationServlet?actionType=view&ID=<%=employee_certificationDTO.iD%>&empId=<%=ID%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-eye"></i>&nbsp;
            </button>
            <button type="button" class="btn-success" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO) %>"
                    onclick="location.href='Employee_certificationServlet?actionType=getEditPage&iD=<%=employee_certificationDTO.iD%>&empId=<%=ID%>&tab=5&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-edit"></i>&nbsp;
            </button>
            <button type="button" class="btn-danger" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                    onclick="deleteItem('empCertificationTableForm',<%=certificationIndex%>)"><i
                    class="fa fa-trash"></i>&nbsp;
            </button>
        </form>
    </div>
</td>
<%
    }
%>