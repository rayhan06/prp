<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_certification.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="employee_records.Employee_recordsRepository" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    String value = "";
    String Language = LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    Employee_certificationDTO employee_certificationDTO = Employee_certificationDAO.getInstance().getDTOFromID(id);
    FilesDAO filesDAO = new FilesDAO();
%>
<%!
    private String convertToBangla(String str, boolean isLangEnglish) {
        if (str == null) return "";
        if (isLangEnglish) return str;
        String[] tokens = str.split(" ");
        String result = "";
        if (tokens.length > 0) {
            result = StringUtils.convertToBanglaMonth(tokens[0]);
        }
        if (tokens.length > 1) {
            result = result.concat(" ").concat(StringUtils.convertToBanNumber(tokens[1]));
        }
        if (result.length() == 0) {
            return str;
        }
        return result;
    }
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=isLanguageEnglish?"Employee's Certification Details":"কর্মকর্তার সার্টিফিকেটের বিশদ বিবরন"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title"><%=isLanguageEnglish?"Employee's Certification Details":"কর্মকর্তার সার্টিফিকেটের বিশদ বিবরন"%></h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEERECORDSID, loginDTO)%></b></td>
                        <td><%=Employee_recordsRepository.getInstance().getEmployeeName(employee_certificationDTO.employeeRecordsId,Language)%></td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CERTIFICATIONNAME, loginDTO)%></b></td>
                        <td><%=employee_certificationDTO.certificationName%></td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGORGANIZATION, loginDTO)%></b></td>
                        <td><%=employee_certificationDTO.issuingOrganization%></td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGINSTITUTEADDRESS, loginDTO)%></b></td>
                        <td><%=employee_certificationDTO.issuingInstituteAddress%></td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDFROM, loginDTO)%></b></td>
                        <td><%=convertToBangla(employee_certificationDTO.validFrom, isLanguageEnglish)%></td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_HASEXPIRYCAT, loginDTO)%></b></td>
                        <td><%=CatRepository.getInstance().getText(Language,"has_expiry", employee_certificationDTO.hasExpiryCat)%></td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDUPTO, loginDTO)%></b></td>
                        <td><%=convertToBangla(employee_certificationDTO.validUpto, isLanguageEnglish)%></td>
                    </tr>

                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CREDENTIALID, loginDTO)%></b></td>
                        <td><%=employee_certificationDTO.credentialId%></td>
                    </tr>


                    <tr>
                        <td style="width:30%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, loginDTO)%>
                        </b></td>
                        <td>
                            <%
                                {
                                    List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(employee_certificationDTO.filesDropzone);
                            %>
                            <table>
                                <tr>
                                    <%
                                        if (FilesDTOList != null) {
                                            for (int j = 0; j < FilesDTOList.size(); j++) {
                                                FilesDTO filesDTO = FilesDTOList.get(j);
                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                    %>
                                    <td>
                                        <%
                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                        %>
                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                             style='width:100px'/>
                                        <%
                                            }
                                        %>
                                        <a href='Employee_certificationServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                           download><%=filesDTO.fileTitle%>
                                        </a>
                                    </td>
                                    <%
                                            }
                                        }
                                    %>
                                </tr>
                            </table>
                            <%
                                }
                            %>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>