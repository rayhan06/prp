<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="employee_certification.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="java.util.List" %>
<%@ page import="common.BaseServlet" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_LANGUAGE, loginDTO);
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_EMPLOYEERECORDSID, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CERTIFICATIONNAME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGORGANIZATION, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_VALIDFROM, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CREDENTIALID, loginDTO)%>
            </th>
            <th>View Details</th>
            <th><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_SEARCH_EMPLOYEE_CERTIFICATION_EDIT_BUTTON, loginDTO)%>
            </th>
			<th class="text-center">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
				<div class="d-flex align-items-center justify-content-between">
					<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
						<i class="fa fa-trash"></i>&nbsp;
					</button>
					<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
				</div>
			</th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Employee_certificationDTO> data = (List<Employee_certificationDTO>) rn2.list;
                if (data != null && data.size()>0) {
                    for (Employee_certificationDTO employee_certificationDTO : data) {
        %>
                <tr><%@include file="employee_certificationSearchRow.jsp"%></tr>
        <% } } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>