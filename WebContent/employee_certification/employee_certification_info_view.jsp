<%@ page import="employee_certification.Employee_certificationDTO" %>
<%@ page import="java.util.List" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
	<thead>
	<tr>
		<th><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_CERTIFICATIONNAME, loginDTO)%></b></th>
		<th><b><%=LM.getText(LC.EMPLOYEE_CERTIFICATION_EDIT_ISSUINGORGANIZATION, loginDTO)%></b></th>
		<%
			if (!actionType.equals("viewSummary")) {
		%>
		<th><b></b></th>
		<%
			}
		%>
	</tr>
	</thead>
	<tbody>
	<%
		List<Employee_certificationDTO> employee_certificationDtos = (List<Employee_certificationDTO>) request.getAttribute("employee_certificationDtos");
		int certificationIndex = 0;
		if (employee_certificationDtos != null && employee_certificationDtos.size() > 0) {
			for (Employee_certificationDTO employee_certificationDTO : employee_certificationDtos) {
				++certificationIndex;
	%>
	<tr>
	<%@include file="/employee_certification/employee_certification_info_view_item.jsp"%>
	</tr>
	<%
			}
		}
	%>
	</tbody>
</table>


<div class="row">
	<div class="col-12 text-right mt-3">
		<button class="btn btn-gray m-t-10 rounded-pill"
			onclick="location.href='Employee_certificationServlet?actionType=getAddPage&empId=<%=ID%>&tab=5&userId=<%=request.getParameter("userId")%>'">
			<i class="fa fa-plus"></i>&nbsp; <%= LM.getText(LC.HR_MANAGEMENT_TRAVEL_AND_CERTIFICATION_ADD_CRETIFICATE, loginDTO) %>
		</button>
	</div>
</div>
