<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%@page import="java.util.*" %>

<%@page import="office_units.*" %>
<%@page import="office_unit_organograms.*" %>
<%@page import="employee_offices.*" %>
<%@page import="workflow.WorkflowController" %>
<%@page contentType="text/html;charset=utf-8" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PRESCRIPTION_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row mx-2 mx-md-3">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=LM.getText(LC.PRESCRIPTION_REPORT_WHERE_DOCTORID, loginDTO)%>
				</label>
				<div class="col-md-9">
					<select class='form-control' name='dr_user_name' id='dr_user_name'
                        tag='pb_html'>
                    <%
                        Options = CommonDAO.getDoctorsByUserName();
                        out.print(Options);
                    %>
                </select>						
				</div>
			</div>
		</div>
		<div  class="search-criteria-div" >
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=LM.getText(LC.HM_EMPLOYEE_ID, loginDTO)%>
				</label>
				<div class="col-md-9">
					<button type="button" class="btn btn-block submit-btn btn-border-radius text-white"
                        onclick="addEmployee()"
                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
	                </button>
	                <table class="table table-bordered table-striped">
	                    <tbody id="employeeToSet"></tbody>
	                </table>
	                <input class='form-control' type='hidden' name='employeeUnit' id='officeUnitType' value=''/>
	                <input class='form-control' type='hidden' name='employeeId' id='organogramType' value=''/>
	                <input class='form-control' type='hidden' name='employee_user_name' id='employee_user_name' value=''/>							
				</div>
			</div>
		</div>
		<div class="search-criteria-div col-md-12">
	        <div class="form-group row">
	            <label class="col-md-3 col-form-label text-md-right">
	                <%=LM.getText(LC.HM_USER_NAME, loginDTO)%>
	            </label>
	            <div class="col-md-9">              
	                <input class='form-control' type='text' name='userNameRaw' id='userNameRaw' value='' onKeyUp="setEngUserName(this.value, 'employee_user_name')"/>
	            </div>
	        </div>
	    </div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=LM.getText(LC.HM_NAME, loginDTO)%>
				</label>
				<div class="col-md-9">
					<input class='form-control'  name='name' id = 'name' value=""/>							
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-md-3 col-form-label text-md-right">
					<%=LM.getText(LC.HM_PHONE, loginDTO)%>
				</label>
				<div class="col-md-9">
					<div class="input-group mb-2">
	                           <div class="input-group-prepend">
	                               <div class="input-group-text"><%=Language.equalsIgnoreCase("english")? "+88" : "+৮৮"%></div>
	                           </div>
	                           <input type="text" class="form-control" id="phone_text"  name="phone_text"
	                                value="" onchange = "setPhone(this.value)">
	                            <input type="hidden" class="form-control" id="phone"  name="phone"
	                                value="">
	                 </div>								
				</div>
			</div>
		</div>
        <%@include file="../pbreport/yearmonth.jsp"%>
        <%@include file="../pbreport/calendar.jsp"%>
    </div>
</div>
<script type="text/javascript">
var isVisible = [true, true, true, true, true, true, true, false];
function init()
{
	addables = [0, 0, 0, 0, 0, 0, 0];
	$("#search_by_date").prop('checked', true);
    $("#search_by_date").trigger("change");
    setDateByStringAndId('startDate_js', '<%=datestr%>');
    setDateByStringAndId('endDate_js', '<%=datestr%>');
    add1WithEnd = false;
    
    $("#dr_user_name").select2({
    	dropdownAutoWidth: true,
        theme: "classic"
    });
    processNewCalendarDateAndSubmit();
}
function PreprocessBeforeSubmiting()
{
}
function patient_inputted(userName, orgId) {
    console.log("patient_inputted " + userName);
    $("#organogramType").val(orgId);
    $("#employee_user_name").val(userName);
    $("#userNameRaw").val(userName);
}
function setPhone(value)
{
	var convertedPhone = phoneNumberAdd88ConvertLanguage(value, '<%=Language%>');
	$("#phone").val(convertedPhone);
}

function getLink(list)
{
	var id = convertToEnglishNumber(list[7]);

	return "Prescription_detailsServlet?actionType=view&ID=" + id;
}
</script>