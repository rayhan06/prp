﻿


<%--<div class="kt-portlet__head ">--%>
<%--    <div class="kt-portlet__head-label">--%>
<%--        <h3 class="kt-portlet__head-title">--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>--%>
<%--        </h3>--%>
<%--        &nbsp;&nbsp;--%>
<%--&lt;%&ndash;        <select id="job_applicant_status"  class='form-control' tag='pb_html' onchange="changeJobForApplicantStatusCount()">&ndash;%&gt;--%>
<%--&lt;%&ndash;            <%=new Recruitment_job_descriptionDAO().getJobList(language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;        </select>&ndash;%&gt;--%>
<%--    </div>--%>
<%--</div>--%>
<div class="kt-portlet__body" style="background-color: white">
    <div id="barchart_material" style="height: 300px;"></div>
</div>







<%--<div>--%>
<%--    <label for="job_applicant_status" class="control-label">Jobs</label>--%>
<%--    <select id="job_applicant_status"  class='form-control' tag='pb_html' onchange="changeJobForApplicantStatusCount()">--%>
<%--        <%=new Recruitment_job_descriptionDAO().getJobList(language)%>--%>
<%--    </select>--%>
<%--    <div id="barchart_material" style="height: 300px;"></div>--%>
<%--</div>--%>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    let applicantStatusCountFetchedData;
    let receivedData = [];

    $(document).ready(()=>{
        changeJobForApplicantStatusCount();
    })
    function changeJobForApplicantStatusCount(){
        const jobId = $("#job_gender").val();
        if(jobId){
            let url = "RecruitmentDashboardServlet?actionType=status_count&jobId=" + jobId + "&language=<%=language%>";
            // console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    applicantStatusCountFetchedData = fetchedData;
                    receivedData = [];
                    receivedData.push(['<%=LM.getText(LC.CANDIDATE_LIST_LEVEL, loginDTO)%>',
                        '<%=LM.getText(LC.CANDIDATE_LIST_TOTAL, loginDTO)%>',
                        '<%=LM.getText(LC.CANDIDATE_LIST_SHORTLISTED, loginDTO)%>',
                        '<%=LM.getText(LC.CANDIDATE_LIST_REJECTED, loginDTO)%>']);
                    for(let i = 0; i < applicantStatusCountFetchedData.data.length; i++){
                        let rowData = applicantStatusCountFetchedData.data[i];
                        if(rowData){
                            receivedData.push([rowData.level, rowData.total, rowData.shortListed, rowData.rejected]);
                        }
                    }

                    // console.log(receivedData)

                    google.charts.load('current', {'packages':['bar']});
                    google.charts.setOnLoadCallback(drawChart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function drawChart() {
        var data = google.visualization.arrayToDataTable(receivedData);
        //     [
        //     ['Level', 'Total', 'ShortListed', 'Rejected'],
        //     ['Primary', 1000, 400, 200],
        //     ['Written', 1170, 460, 250],
        //     ['Viva', 660, 1120, 300],
        //     ['Health', 660, 1120, 300],
        // ]);

        var options = {
            chart: {
                title: '<%=LM.getText(LC.RECRUITMENT_DASHBOARD_LEVEL, loginDTO)%>',

                // subtitle: 'Sales, Expenses, and Profit: 2014-2017',
            },
            bars: 'vertical' // Required for Material Bar Charts.
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
</script>