﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="util.UtilCharacter" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String language = LM.getText(LC.TRAINING_CALENDER_EDIT_LANGUAGE, loginDTO);

//    String runningJobCount = UtilCharacter.convertDataByLanguage(language,
//            new Recruitment_job_descriptionDAO().getRunningJobCount() + "");
//    String totalApplicantCount = UtilCharacter.convertDataByLanguage(language,
//            new Job_applicant_applicationDAO().getTotalApplicationCount() + "");

%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.RECRUITMENT_DASHBOARD_DASHBOARD, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body" style="background-color: #f2f2f2">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="d-flex justify-content-center align-items-center">
                                <div class="shadow"
                                     style="width: 100%; height: 90%; background: linear-gradient(to right, #9861C2, #6132B2); border-radius: 12px;">
                                    <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-3 px-lg-4 px-xl-5">
                                        <div class="">
                                            <span class="h3 text-white">
                                                <%=LM.getText(LC.RECRUITMENT_DASHBOARD_RUNNING_JOB_COUNT, loginDTO)%>
                                            </span>
                                        </div>
                                        <div class="ml-5">
                                            <span id = 'jobCountSpan' class="patient_count h1 text-white">
<%--                                                <%=runningJobCount%>--%>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-3 mt-md-0">
                            <div class="d-flex justify-content-center align-items-center">
                                <div class="shadow"
                                     style="width: 100%; height: 90%; background: linear-gradient(to right, #EE9048, #EE9048); border-radius: 12px;">
                                    <div class="d-flex justify-content-between align-items-center mx-2 my-4 px-3 px-lg-4 px-xl-5">
                                        <div class="">
                                            <span class="h3 text-white">
                                                <%=LM.getText(LC.RECRUITMENT_DASHBOARD_APPLICATION_COUNT, loginDTO)%>
                                            </span>
                                        </div>
                                        <div class="ml-5">
                                            <span id="applicantCountSpan" class="patient_count h1 text-white"
                                                  style="background-color: #FCB42E;">
<%--                                                <%=totalApplicantCount%>--%>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-column flex-md-row justify-content-center align-items-center my-5">
                        <h5 class="mt-1">
                            <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>
                        </h5>
                        <div class="ml-3">
                            <select id="job_gender" class='form-control rounded shadow-sm' tag='pb_html'
                                    onchange="jobChange()">
                                <%=new Recruitment_job_descriptionDAO().getJobList(language)%>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="applicant_count_by_gender.jsp" %>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="position_wise_applicant_status.jsp" %>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="applicant_count_by_highest_edu_level.jsp" %>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="applicant_count_final.jsp" %>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="applicant_count_by_division.jsp" %>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="shadow dashboard-card-border-radius overflow-hidden">
                                <div class="">
                                    <%@include file="applicant_count_by_district.jsp" %>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>




