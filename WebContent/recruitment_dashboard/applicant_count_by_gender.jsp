
<div class="kt-portlet__body" style="background-color: white">
    <div id="pie_chart_emp_gender_count_by_job_id" style="height: 300px;"></div>
</div>

<script type="text/javascript">
    $(document).ready(()=>{
        changeJobForApplicantGenderCount();
        getJobCount();
    })
    let applicantGenderCountFetchedData;

    function jobChange(){
        changeJobForApplicantGenderCount();
        changeJobForApplicantHighestEduLevelCount();
        changeJobForApplicantFinalCount();
        changeJobForApplicantStatusCount();
        changeJobForApplicantDivisionCount();
        changeJobForApplicantDistrictCount();

    }

    function getJobCount(){

        let url = "RecruitmentDashboardServlet?actionType=job_count";
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if(fetchedData){
                    $('#jobCountSpan').text(fetchedData.jobCount);
                    $('#applicantCountSpan').text(fetchedData.applicantCount);
                }
                // console.log(fetchedData);

            },
            error: function (error) {
                console.log(error);
            }
        });

    }


    function changeJobForApplicantGenderCount(){
        const jobId = $("#job_gender").val();
        if(jobId){
            let url = "RecruitmentDashboardServlet?actionType=gender_count&jobId=" + jobId + "&language=<%=language%>";
            // console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    applicantGenderCountFetchedData = fetchedData;
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawApplicantGenderCountChart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function drawApplicantGenderCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Designation');
        data.addColumn('number', 'Count');
        if(applicantGenderCountFetchedData){
            for(let i in applicantGenderCountFetchedData){
                data.addRows([[String(i),applicantGenderCountFetchedData[i]]]);
            }
        }

        const view = new google.visualization.DataView(data);
        // view.setColumns([0, 1,
        //     { calc: "stringify",
        //         sourceColumn: 1,
        //         type: "string",
        //         role: "annotation" }
        // ]);
        const options = {
            title: '<%=LM.getText(LC.RECRUITMENT_DASHBOARD_GENDER, loginDTO)%>',
            // is3D: true,
            // legend: { position: "none" }
        };
        const chart = new google.visualization.PieChart(document.getElementById('pie_chart_emp_gender_count_by_job_id'));
        chart.draw(view, options);
    }
</script>