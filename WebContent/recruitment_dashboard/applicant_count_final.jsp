<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>

<%--<div class="kt-portlet__head ">--%>
<%--    <div class="kt-portlet__head-label">--%>
<%--        <h3 class="kt-portlet__head-title">--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>--%>
<%--        </h3>--%>
<%--        &nbsp;&nbsp;--%>
<%--&lt;%&ndash;        <select id="job_final_count"  class='form-control' tag='pb_html' onchange="changeJobForApplicantFinalCount()">&ndash;%&gt;--%>
<%--&lt;%&ndash;            <%=new Recruitment_job_descriptionDAO().getJobList(language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;        </select>&ndash;%&gt;--%>
<%--    </div>--%>
<%--</div>--%>


<div class="kt-portlet__body" style="background-color: white">
    <div id="pie_chart_emp_final_count_by_job_id" style="height: 300px;"></div>
</div>




<%--<div>--%>
<%--    <label for="job_final_count" class="control-label">Jobs</label>--%>
<%--    <select id="job_final_count"  class='form-control' tag='pb_html' onchange="changeJobForApplicantFinalCount()">--%>
<%--        <%=new Recruitment_job_descriptionDAO().getJobList(language)%>--%>
<%--    </select>--%>
<%--    <div id="pie_chart_emp_final_count_by_job_id" style="height: 300px;"></div>--%>
<%--</div>--%>

<script type="text/javascript">
    $(document).ready(()=>{
        changeJobForApplicantFinalCount();
    })
    let applicantFinalCountFetchedData;
    function changeJobForApplicantFinalCount(){
        const jobId = $("#job_gender").val();
        if(jobId){
            let url = "RecruitmentDashboardServlet?actionType=all_status_count&jobId=" + jobId ;
            // console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    applicantFinalCountFetchedData = fetchedData;
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawApplicantFinalCountChart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }


    function drawApplicantFinalCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Type');
        data.addColumn('number', 'Count');
        // console.log(applicantFinalCountFetchedData)
        if(applicantFinalCountFetchedData){
            for(let i in applicantFinalCountFetchedData){
                data.addRows([[String(i),applicantFinalCountFetchedData[i]]]);
            }
        }
        const view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            { calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation" }
        ]);
        const options = {
            title: '<%=LM.getText(LC.RECRUITMENT_DASHBOARD_FINAL, loginDTO)%>',
            legend: { position: "none" }
        };
        const chart = new google.visualization.ColumnChart(document.getElementById('pie_chart_emp_final_count_by_job_id'));
        chart.draw(view, options);
    }

    // function drawApplicantFinalCountChart() {
    //     const data = new google.visualization.DataTable();
    //     data.addColumn('string', 'Designation');
    //     data.addColumn('number', 'Count');
    //     console.log(applicantFinalCountFetchedData)
    //     if(applicantFinalCountFetchedData){
    //         for(let i in applicantFinalCountFetchedData){
    //             data.addRows([[String(i),applicantFinalCountFetchedData[i]]]);
    //         }
    //     }
    //
    //     const view = new google.visualization.DataView(data);
    //     // view.setColumns([0, 1,
    //     //     { calc: "stringify",
    //     //         sourceColumn: 1,
    //     //         type: "string",
    //     //         role: "annotation" }
    //     // ]);
    //     const options = {
    //         title: 'Applicant Final Count',
    //         // is3D: true,
    //         // legend: { position: "none" }
    //     };
    //     const chart = new google.visualization.PieChart(document.getElementById('pie_chart_emp_final_count_by_job_id'));
    //     chart.draw(view, options);
    // }


</script>