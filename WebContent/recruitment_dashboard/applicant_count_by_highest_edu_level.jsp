<%@ page import="recruitment_job_description.Recruitment_job_descriptionDAO" %>


<%--<div class="kt-portlet__head ">--%>
<%--    <div class="kt-portlet__head-label">--%>
<%--        <h3 class="kt-portlet__head-title">--%>
<%--            <%=LM.getText(LC.CANDIDATE_LIST_POST_NAME, loginDTO)%>--%>
<%--        </h3>--%>
<%--        &nbsp;&nbsp;--%>
<%--&lt;%&ndash;        <select id="job_edu_level"  class='form-control' tag='pb_html' onchange="changeJobForApplicantHighestEduLevelCount()">&ndash;%&gt;--%>
<%--&lt;%&ndash;            <%=new Recruitment_job_descriptionDAO().getJobList(language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;        </select>&ndash;%&gt;--%>
<%--    </div>--%>
<%--</div>--%>
<div class="kt-portlet__body" style="background-color: white">
    <div id="pie_chart_emp_edu_level_count_by_job_id" style="height: 300px;"></div>
</div>

<%--<div>--%>
<%--    <label for="job_edu_level" class="control-label">Jobs</label>--%>
<%--    <select id="job_edu_level"  class='form-control' tag='pb_html' onchange="changeJobForApplicantHighestEduLevelCount()">--%>
<%--        <%=new Recruitment_job_descriptionDAO().getJobList(language)%>--%>
<%--    </select>--%>
<%--    <div id="pie_chart_emp_edu_level_count_by_job_id" style="height: 300px;"></div>--%>
<%--</div>--%>

<script type="text/javascript">
    $(document).ready(()=>{
        changeJobForApplicantHighestEduLevelCount();
    })
    let applicantHighestEduLevelCountFetchedData;
    function changeJobForApplicantHighestEduLevelCount(){
        const jobId = $("#job_gender").val();
        if(jobId){
            let url = "RecruitmentDashboardServlet?actionType=highest_edu_level_count&jobId=" + jobId + "&language=<%=language%>";
            // console.log("url : " + url);
            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    applicantHighestEduLevelCountFetchedData = fetchedData;
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawApplicantHighestEduLevelCountChart);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function drawApplicantHighestEduLevelCountChart() {
        const data = new google.visualization.DataTable();
        data.addColumn('string', 'Designation');
        data.addColumn('number', 'Count');
        if(applicantHighestEduLevelCountFetchedData){
            for(let i in applicantHighestEduLevelCountFetchedData){
                data.addRows([[String(i),applicantHighestEduLevelCountFetchedData[i]]]);
            }
        }

        const view = new google.visualization.DataView(data);
        // view.setColumns([0, 1,
        //     { calc: "stringify",
        //         sourceColumn: 1,
        //         type: "string",
        //         role: "annotation" }
        // ]);
        const options = {
            title: '<%=LM.getText(LC.RECRUITMENT_DASHBOARD_EDU, loginDTO)%>',
            // is3D: true,
            pieHole: 0.4,
            // legend: { position: "none" }
        };
        const chart = new google.visualization.PieChart(document.getElementById('pie_chart_emp_edu_level_count_by_job_id'));
        chart.draw(view, options);
    }
</script>