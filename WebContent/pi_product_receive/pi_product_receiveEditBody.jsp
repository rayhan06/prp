<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>
<%@ page import="pi_product_receive.Pi_product_receiveDTO" %>
<%@ page import="pi_product_receive.Pi_product_receiveDAO" %>

<%@include file="../pb/addInitializer2.jsp" %>
<%
    List<Pi_product_receiveDTO> productReceiveDTOs = new ArrayList<>();
    Pi_product_receiveDTO productReceiveDTO = new Pi_product_receiveDTO();
    long ID = -1;
    if (request.getParameter("ID") != null) {
        ID = Long.parseLong(request.getParameter("ID"));
        productReceiveDTO = Pi_product_receiveDAO.getInstance().getDTOByID(ID);
    }
    CommonDTO commonDTO = productReceiveDTO;
    String tableName = "pi_product_receive";

    long officeUnitId = userDTO.unitID;

    if (actionName.equals("ajax_edit")) {
//        productReceiveDTOs = (List<Pi_product_receiveDTO>) Pi_product_receiveDAO.getInstance().getDTOsByParent("pi_purchase_parent_id", productReceiveDTO.iD);
    }

    String servletName = "Pi_product_receiveServlet";
    String context = request.getContextPath() + "/";

    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
    c.add(Calendar.YEAR, i);
    Date today = c.getTime();
    SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
    String todayString = sf.format(today);
    int currentYear = Integer.parseInt(todayString.substring(6));
    int nextYear = currentYear + 1;
    String endYear = "2041";

    List<Office_unitsDTO> office_unitsDTOS = Office_unitsRepository.getInstance().getOfficeUnitsDTOListAsInventoryStore();
%>

<style>
    .custom-modal {
        width: 300px !important; /* Use !important in case you want to override another val*/
        height: 200px;
        position: absolute; /*You can use fixed too*/
        top: 50%;
        left: 50%;
        margin-top: 30px;
        margin-left: 20px;
        display: none; /* You want it to be hidden, and show it using jquery*/
    }

    .hiddenTr {
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(Language, "মালামাল গ্রহণ", "Product Receive")%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="mt-5 col-12">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=getDataByLanguage(Language, "মালামাল গ্রহণ", "Product Receive")%>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_FISCALYEARID, loginDTO)%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='fiscalYearSelect'
                                                        onchange="searchPurchaseList()"
                                                        id='fiscalYearSelect'>
                                                    <%
                                                        Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
                                                        if (productReceiveDTO.fiscalYearId == -1) {
                                                            Fiscal_yearDTO fiscalYearDTO = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());
                                                            if (fiscalYearDTO != null)
                                                                productReceiveDTO.fiscalYearId = fiscalYearDTO.id;
                                                        }
                                                        Options = CommonDAO.getOptions(Language, "fiscal_year", productReceiveDTO.fiscalYearId);
                                                    %>
                                                    <%=Options%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6 ">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ", "Package")%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='packageSelect'
                                                        onchange="searchPurchaseList()"
                                                        id='packageSelect'>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6 hiddenTr" id="lotSelectDiv">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "লট", "Lot")%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='lotSelect'
                                                        onchange="searchPurchaseList()"
                                                        id='lotSelect'>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-md-6 ">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "ক্রয়ের আদেশ নম্বর", "Purchase Order Number")%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <%--HERE VALUE IS THE PURCHASE PARENT ID--%>
                                                <select class='form-control' name='purchaseOrderNumberSelect'
                                                        onchange="loadItemList(this)"
                                                        id='purchaseOrderNumberSelect'>
                                                </select>
                                                <input type="hidden" id="purchaseOrderNumberText"
                                                       name='purchaseOrderNumberText' value="">
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "স্টোর", "Store")%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='storeOfficeName'
                                                       id='storeOfficeName' readonly
                                                       value=''/>
                                                <input type='hidden' class='form-control' name='storeOfficeId'
                                                       id='storeOfficeId'
                                                       value=''/>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "সরবরাহকারীর নাম", "Vendor")%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='vendorName'
                                                       id='vendorName' readonly
                                                       value=''/>
                                                <input type='hidden' class='form-control' name='vendorId'
                                                       id='vendorId'
                                                       value=''/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "মালামাল গ্রহণের তারিখ", "Product Receive Date")%>
                                                <span class="required" style="color: red;"> * </span>
                                            </label>
                                            <div class="col-md-8">
                                                <%value = "receiveDate_js";%>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="END_YEAR"
                                                               value="<%=endYear%>"></jsp:param>
                                                    <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' name='receiveDate' id='receiveDate_date'
                                                       value='<%=dateFormat.format(new Date(System.currentTimeMillis()))%>'>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "মালামাল গ্রহণের নম্বর", "Good Receiving Number")%>
                                                <span class="required">*</span>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='goodReceivingNumber'
                                                       id='goodReceivingNumber_text_<%=i%>'
                                                       value='<%=productReceiveDTO.goodReceivingNumber%>'/>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "স্মারক নম্বর", "Sarok Number")%>
                                            </label>
                                            <div class="col-md-8">
                                                <input type='text' class='form-control' name='sarokNumber'
                                                       id='sarokNumber_text_<%=i%>'
                                                       value='<%=productReceiveDTO.sarokNumber%>'/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "মন্তব্য", "Remarks")%>
                                            </label>
                                            <div class="col-md-8">
                                                <textarea class='form-control' name='globalRemark' rows="4"
                                                          style="resize: none"
                                                          id='remarks_text_<%=i%>'><%=productReceiveDTO.remarks%></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-6">
                                            <label class="col-md-4 col-form-label text-md-right">
                                                <%=LM.getText(LC.FILES_SEARCH_ANYFIELD, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <%
                                                    fileColumnName = "filesDropzone";
                                                    if (actionName.equals("ajax_edit")) {
                                                        List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(productReceiveDTO.filesDropZone);
                                                %>
                                                <%@include file="../pb/dropzoneEditor.jsp" %>
                                                <%
                                                    } else {
                                                        ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                        productReceiveDTO.filesDropZone = ColumnID;
                                                    }
                                                %>

                                                <div class="dropzone"
                                                     action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=productReceiveDTO.filesDropZone%>">
                                                    <input type='file' style="display:none"
                                                           name='<%=fileColumnName%>File'
                                                           id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                       id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                       tag='pb_html'/>
                                                <input type='hidden' name='<%=fileColumnName%>'
                                                       id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                       value='<%=productReceiveDTO.filesDropZone%>'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <div class="form-body">
                        <h5 class="table-title">
                            <%=LM.getText(LC.PI_PURCHASE_ADD_ID, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table id="purchase-table" class="table table-bordered table-striped text-nowrap">
                                <thead>
                                <tr>
                                    <th><%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PI_PACKAGE_NEW_ADD_FORMNAME, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                                    </th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMID, loginDTO)%>
                                    </th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "মালামাল গ্রহণ সংখ্যা", "Item Received Count")%>
                                    </th>
                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_AMOUNT, loginDTO)%>
                                    </th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "ক্রয়কৃত সংখ্যা", "Purchase Count")%>
                                    </th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "অবশিষ্ট", "Remaining")%>
                                    </th>
<%--                                    <th><%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_UNITPRICE, loginDTO)%>--%>
<%--                                    </th>--%>
<%--                                    <th><%=LM.getText(LC.PI_PURCHASE_ADD_TOTALVALUE, loginDTO)%>--%>
<%--                                    </th>--%>
                                    <th style="min-width: 25rem"><%=LM.getText(LC.APPROVAL_EXECUTION_TABLE_ADD_REMARKS, loginDTO)%>
                                    </th>
<%--                                    <th><%=LM.getText(LC.HM_SELECT, loginDTO)%>--%>
<%--                                    </th>--%>
                                </tr>
                                </thead>
                                <tbody id="item-list-for-receive">
                                </tbody>
                                <tr class="loading-gif" style="display: none;">
                                    <td class="text-center" colspan="100%">
                                        <img alt="" class="loading"
                                             src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                        <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
                                    </td>
                                </tr>
                                <tr id="template-item-row" class="hiddenTr">
                                    <input type="hidden" id="isActive" value="false">
                                    <input type="hidden" id="productReceiveChildId">
                                    <input type="hidden" id="piPurchaseId">
                                    <input type="hidden" id="packageId">
                                    <input type="hidden" id="itemGroupId">
                                    <input type="hidden" id="itemTypeId">
                                    <input type="hidden" id="itemId">
                                    <td id="packageName"></td>
                                    <td id="itemTypeName"></td>
                                    <td id="itemName"></td>
                                    <td id="itemReceivedCount"></td>
                                    <td>
                                        <input id="quantity" class='form-control' type="number" min="0"
                                               value="0" onchange="validateQuantityCount(this)">
                                    </td>
                                    <td id="purchaseCount"></td>
                                    <td id="remainingPurchaseCount"></td>
<%--                                    <td id="unitPrice"></td>--%>
<%--                                    <td id="totalPrice"></td>--%>
                                    <td>
                                        <input id="remark" class='form-control' type="text" value="">
                                    </td>
<%--                                    <td id="select"></td>--%>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" onclick="submitForm()">
                                <%=LM.getText(LC.PI_PURCHASE_ADD_PI_PURCHASE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="" style="align-content: center">
                    <div class="custom-modal">
                        <div class="spinner-grow text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-secondary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-success" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-danger" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-warning" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-info" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-light" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="spinner-grow text-dark" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<jsp:include page="../procurement_goods/procurementGoodsModal.jsp">
    <jsp:param name="index" value="1"/>
</jsp:include>

<script type="text/javascript">
    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';
    const language = '<%=Language%>';
    const bigForm = $('#bigform');
    let child_table_extra_id = 0;
    let row = 0;

    function isValueValid(str) {
        return str !== '';
    }

    function searchPurchaseList() {
        let fiscalYearId = document.getElementById('fiscalYearSelect').value;
        let isFiscalYearSelected = isValueValid(fiscalYearId);
        let packageId = document.getElementById('packageSelect').value;
        let isPackageSelected = isValueValid(packageId);
        let lotId = document.getElementById('lotSelect').value;
        let isLotSelected = isValueValid(lotId);

        if (isFiscalYearSelected && isPackageSelected && isLotSelected) {
            loadPurchaseOrderNumberList(fiscalYearId, packageId, lotId);
        } else if (isFiscalYearSelected && isPackageSelected) {
            loadLotList(fiscalYearId, packageId, lotId);
        } else if (isFiscalYearSelected) {
            loadPackageList(fiscalYearId);
        }
    }

    function loadPurchaseOrderNumberList(fiscalYearId, packageId, lotId) {
        let url = 'Pi_purchase_parentServlet?actionType=getPurchaseOrderNumbersByFiscalYearAndPackageAndLot' +
            '&fiscalYearId=' + fiscalYearId +
            '&packageId=' + packageId +
            '&lotId=' + lotId;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let response = JSON.parse(this.responseText);
                let purchaseSarokNumberOptions = JSON.parse(response.msg);
                let purchaseSarokNumberElement = document.getElementById('purchaseOrderNumberSelect');
                purchaseSarokNumberElement.innerHTML = purchaseSarokNumberOptions;
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", url, false);
        xhttp.send();
    }

    function clearSarokNumberList() {
        let sarokNumberElement = document.getElementById('purchaseOrderNumberSelect');
        sarokNumberElement.innerHTML = '';
    }

    function loadPackageList(fiscalYearId) {
        let url = 'Pi_purchase_parentServlet?actionType=getPackageListByFiscalYear&fiscalYearId=' + fiscalYearId;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let response = JSON.parse(this.responseText);
                let packageOptions = JSON.parse(response.msg);
                let packageElement = document.getElementById('packageSelect');
                packageElement.innerHTML = packageOptions;
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", url, false);
        xhttp.send();
    }

    function clearPackageList() {
        let packageElement = document.getElementById('packageSelect');
        packageElement.innerHTML = "";
    }

    function loadLotList(fiscalYearId, packageId, lotId) {
        let url = 'Pi_purchase_parentServlet?actionType=getLotListByFiscalYear&fiscalYearId=' + fiscalYearId;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let response = JSON.parse(this.responseText);
                let lotOptions = JSON.parse(response.msg);
                let lotElement = document.getElementById('lotSelect');
                lotElement.innerHTML = lotOptions;
                showLotIfRequired(fiscalYearId, packageId, lotId);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", url, false);
        xhttp.send();
    }

    function showLotIfRequired(fiscalYearId, packageId, lotId) {
        let lotSelect = document.getElementById('lotSelect');
        let lotSelectDiv = document.getElementById('lotSelectDiv');
        if (lotSelect.length > 0) {
            showElement(lotSelectDiv);
        } else {
            loadPurchaseOrderNumberList(fiscalYearId, packageId, lotId);
            hideElement(lotSelectDiv);
        }
    }

    function showElement(element) {
        element.classList.remove('hiddenTr');
    }

    function hideElement(element) {
        element.classList.add('hiddenTr');
    }

    function cleanLotList() {
        let lotElement = document.getElementById('lotSelect');
        lotElement.innerHTML = '';
    }

    function setPurchaseOrderNumberText(purchaseSarokSelect) {
        let purchaseOrderNumberTextElement = document.getElementById('purchaseOrderNumberText');
        purchaseOrderNumberTextElement.value = purchaseSarokSelect.options[purchaseSarokSelect.selectedIndex].text;
    }

    function loadItemList(purchaseOrderSelect) {
        setPurchaseOrderNumberText(purchaseOrderSelect);
        let piPurchaseParentId = document.getElementById('purchaseOrderNumberSelect').value;
        let url = 'Pi_purchase_parentServlet?actionType=getItemsByPurchaseOrderNumber&piPurchaseParentId=' + piPurchaseParentId;
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let response = JSON.parse(this.responseText);
                let items = JSON.parse(response.msg);
                setDataToItemTable(items);
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", url, false);
        xhttp.send();
    }

    function preprocess(tr) {
        tr.classList.remove('hiddenTr');
    }

    function setName(clonedTr) {
        clonedTr.querySelector('input[id="productReceiveChildId"]').name = "productReceiveChildId";
        clonedTr.querySelector('input[id="piPurchaseId"]').name = "piPurchaseId";
        clonedTr.querySelector('input[id="packageId"]').name = "packageId";
        clonedTr.querySelector('input[id="itemGroupId"]').name = "itemGroupId";
        clonedTr.querySelector('input[id="itemTypeId"]').name = "itemTypeId";
        clonedTr.querySelector('input[id="itemId"]').name = "itemId";
        clonedTr.querySelector('td>input[id="quantity"]').name = "quantity";
        clonedTr.querySelector('td>input[id="remark"]').name = "remark";
    }

    function setValue(clonedTr, itemData) {
        clonedTr.querySelector('input[id="piPurchaseId"]').value = itemData["piPurchaseId"];
        clonedTr.querySelector('input[id="packageId"]').value = itemData["packageId"];
        clonedTr.querySelector('input[id="itemTypeId"]').value = itemData["itemTypeId"];
        clonedTr.querySelector('input[id="itemId"]').value = itemData["itemId"];
        clonedTr.querySelector('td[id="packageName"]').innerText = itemData["packageName"];
        clonedTr.querySelector('td[id="itemTypeName"]').innerText = itemData["itemTypeName"];
        clonedTr.querySelector('td[id="itemName"]').innerText = itemData["itemName"];
        clonedTr.querySelector('td[id="itemReceivedCount"]').innerText = itemData["currentReceivedCount"];
        clonedTr.querySelector('td>input[id="quantity"]').value = itemData["quantity"];
        clonedTr.querySelector('td>input[id="quantity"]').min = 0;
        clonedTr.querySelector('td>input[id="quantity"]').max = itemData["remaining"];
        clonedTr.querySelector('td[id="purchaseCount"]').innerText = itemData["purchaseCount"];
        clonedTr.querySelector('td[id="remainingPurchaseCount"]').innerText = itemData["remainingPurchaseCount"];
        // clonedTr.querySelector('td[id="unitPrice"]').innerText = itemData["unitPrice"];
        clonedTr.querySelector('td>input[id="remark"]').value = itemData["remark"];
    }

    function setStoreData(itemData) {
        document.getElementById('storeOfficeId').value = itemData["storeOfficeUnitId"];
        document.getElementById('storeOfficeName').value = itemData["storeOfficeUnitName"];
    }

    function setVendorData(itemData) {
        document.getElementById('vendorId').value = itemData["vendorId"];
        document.getElementById('vendorName').value = itemData["vendorName"];
    }

    function cleanTableBody() {
        let tableBody = document.getElementById('item-list-for-receive');
        tableBody.innerHTML = '';
    }

    function setDataToItemTable(items) {
        cleanTableBody();
        let tableBody = document.getElementById('item-list-for-receive');
        for (let item of items) {
            let clonedTr = document.getElementById('template-item-row').cloneNode(true);
            preprocess(clonedTr);
            setName(clonedTr);
            setValue(clonedTr, item);
            setStoreData(item);
            setVendorData(item);
            tableBody.append(clonedTr);
        }
    }

    function validateQuantityCount(element){
        try{
            let tr = element.parentElement.parentElement;
            let quantityElement = tr.querySelector('td>input[id="quantity"]');
            let quantity = quantityElement.value;
            let remaining = tr.querySelector('td[id="remainingPurchaseCount"]').innerText.trim();
            if(parseInt(quantity) > parseInt(remaining)){
                quantityElement.value = parseInt(remaining);
                swal.fire(getValueByLanguage(language, "আপনি অবশিষ্ট পরিমাণের চেয়ে বেশি মালামাল গ্রহণ করতে পারবেন না!",
                    "You can not receive more than remaining count!"));
            }
        }catch (e){
            console.debug(e);
        }
    }

    function isFormValid() {
        return true;
    }

    function submitForm() {
        if (!isFormValid()) return;
        event.preventDefault();
        let msg = '<%=isLanguageEnglish?"Do you want to receive the items?":"মালামাল গ্রহণ করতে চান?"%>';
        let confirmButtonText = '<%=isLanguageEnglish?"Yes":"হ্যাঁ"%>';
        let cancelButtonText = '<%=isLanguageEnglish?"No":"না"%>';
        messageDialog('', msg, 'success', true, confirmButtonText, cancelButtonText, () => {
            let $myModal = $('.custom-modal');
            let url = 'Pi_product_receiveServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>';
            $('.form-actions').hide();
            $myModal.fadeIn();
            buttonStateChange(true);
            console.log("submitting");
            let form = $("#bigform");
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        console.log("Failed");
                        showError(response.msg, response.msg);
                        buttonStateChange(false);
                        $myModal.fadeOut();
                        $('.form-actions').show();
                    } else if (response.responseCode === 200) {
                        console.log("Successfully added");
                        $myModal.fadeOut();
                        window.location.replace(getContextPath() + response.msg);
                    } else {
                        console.log("Error: " + response.responseCode);
                        $myModal.fadeOut();
                        $('.form-actions').show();
                        buttonStateChange(false);
                    }
                }
                ,
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        }, () => {
        });
    }

    function setCurrentDate() {
        setDateByStringAndId('receiveDate_js', $('#receiveDate_date').val());
    }

    $(document).ready(function () {
        searchPurchaseList();
        setCurrentDate();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });
</script>