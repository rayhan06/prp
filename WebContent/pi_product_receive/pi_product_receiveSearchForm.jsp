<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@ page import="pb.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="java.util.*" %>
<%@ page import="pi_package_final.Pi_package_finalDTO" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_product_receive.Pi_product_receiveDTO" %>
<%@ page import="pi_package_final.PiPackageLotFinalDTO" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    String navigator2 = "navPI_PACKAGE_VENDOR";
    String servletName = "Pi_product_receiveServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_FISCALYEARID, loginDTO)%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "মালামাল গ্রহণের নম্বর", "Good Receiving Number")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "ক্রয় আদেশ নম্বর", "Purchase Order Number")%>
            </th>
            <th><%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ", "Package")%>
            </th>
                        <th><%=UtilCharacter.getDataByLanguage(Language, "লট", "Lot")%>
                        </th>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            try {
                List<Pi_product_receiveDTO> data = (List<Pi_product_receiveDTO>) rn2.list;
                if (data != null && data.size() > 0) {
                    for (Pi_product_receiveDTO model : data) {
                        Pi_package_finalDTO packageDTO = Pi_package_finalRepository.getInstance()
                                .getPi_package_finalDTOByiD(model.packageId);
                        PiPackageLotFinalDTO lotDTO = PiPackageLotFinalRepository.getInstance()
                                .getPiPackageLotFinalDTOByiD(model.lotId);
        %>
        <tr>
            <td>
                <%
                    value = Fiscal_yearRepository.getInstance().getText(model.fiscalYearId, Language) + "";
                %>
                <%=Utils.getDigits(value, Language)%>
            </td>
            <td>
                <%
                    value = model.goodReceivingNumber;
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = model.purchaseOrderNumber;
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = packageDTO == null ? "" : UtilCharacter.getDataByLanguage
                            (Language, packageDTO.packageNumberBn, packageDTO.packageNumberEn);
                %>
                <%=value%>
            </td>
            <td>
                <%
                    value = lotDTO == null ? "" : UtilCharacter.getDataByLanguage
                            (Language, lotDTO.lotNumberBn, lotDTO.lotNumberEn);
                %>
                <%=value%>
            </td>

            <%CommonDTO commonDTO = model; %>
            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=view&ID=<%=commonDTO.iD%>'"
                >
                    <i class="fa fa-eye"></i>
                </button>
            </td>
        </tr>
        <%
                    }
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>