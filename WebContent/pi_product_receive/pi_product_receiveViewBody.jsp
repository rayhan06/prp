<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.*" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="pi_product_receive.Pi_product_receiveDTO" %>
<%@ page import="pi_product_receive.Pi_product_receiveDAO" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_purchase_parent.Pi_purchase_parentRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="pb.Utils" %>
<%@ page import="pi_product_receive.Pi_product_receive_childDTO" %>
<%@ page import="pi_product_receive.Pi_product_receive_childDAO" %>
<%@ page import="procurement_package.ProcurementGoodsTypeRepository" %>
<%@ page import="procurement_goods.Procurement_goodsRepository" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>
<%@ page import="pi_vendor_auctioneer_details.Pi_vendor_auctioneer_detailsRepository" %>

<%
    String servletName = "Pi_package_vendorServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Pi_product_receiveDTO model = Pi_product_receiveDAO.getInstance().getDTOByID(id);
    CommonDTO commonDTO = model;
%>
<%@include file="../pb/viewInitializer.jsp" %>

<style>
    input[type=checkbox] {
        opacity: 0.8;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_PACKAGE_VENDOR_ADD_PI_PACKAGE_VENDOR_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="mt-5 col-12">
                    <div class="onlyborder">
                        <div class="row mx-2 mx-md-0">
                            <div class="col-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=UtilCharacter.getDataByLanguage(Language, "মালামাল গ্রহণ", "Product Receive")%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PI_PURCHASE_ADD_FISCALYEARID, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='fiscalYear'
                                                   id='FiscalYear' readonly
                                                   value='<%=Fiscal_yearRepository.getInstance().getText(model.fiscalYearId, Language)%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6 ">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "প্যাকেজ", "Package")%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='package'
                                                   id='package' readonly
                                                   value='<%=Pi_package_finalRepository.getInstance().getName(model.packageId, Language)%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6 ">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "লট", "Lot")%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='lot'
                                                   id='lot' readonly
                                                   value='<%=PiPackageLotFinalRepository.getInstance().getName(model.lotId, Language)%>'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group row col-md-6 ">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "ক্রয়ের আদেশ নম্বর", "Purchase Order Number")%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='purchaseOrderNumberText'
                                                   id='purchaseOrderNumberText' readonly
                                                   value='<%=Pi_purchase_parentRepository.getInstance()
                                                   .getPurchaseOrderNumber(model.piPurchaseParentId)%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "স্টোর", "Store")%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='storeOfficeName'
                                                   id='storeOfficeName' readonly
                                                   value='<%=Office_unitsRepository.getInstance().geText(Language, model.officeUnitId)%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "সরবরাহকারীর নাম", "Vendor")%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='vendorName'
                                                   id='vendorName' readonly
                                                   value='<%=Pi_vendor_auctioneer_detailsRepository.getInstance()
                                                   .getText(model.vendorId, Language)%>'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "মালামাল গ্রহণের তারিখ", "Product Receive Date")%>
                                        </label>
                                        <div class="col-md-8">
                                            <%
                                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                                String date = simpleDateFormat.format(new Date(model.receiveDate));
                                                date = Utils.getDigits(date, Language);
                                            %>
                                            <input type='text' class='form-control' name='receiveDate'
                                                   id='receiveDate' readonly
                                                   value='<%=date%>'/>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "মালামাল গ্রহণের নম্বর", "Good Receiving Number")%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='goodReceivingNumber'
                                                   id='goodReceivingNumber_text_<%=i%>'
                                                   value='<%=model.goodReceivingNumber%>' readonly/>
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "স্মারক নম্বর", "Sarok Number")%>
                                        </label>
                                        <div class="col-md-8">
                                            <input type='text' class='form-control' name='sarokNumber'
                                                   id='sarokNumber_text_<%=i%>' readonly
                                                   value='<%=model.sarokNumber%>'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="form-group row col-md-6">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "মন্তব্য", "Remarks")%>
                                        </label>
                                        <div class="col-md-8">
                                                <textarea class='form-control' name='globalRemark' rows="4"
                                                          style="resize: none"
                                                          id='remarks_text_<%=i%>'
                                                          readonly>
                                                    <%=model.remarks%>
                                                </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title">
                        <%=LM.getText(LC.PI_PURCHASE_ADD_ID, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table id="purchase-table" class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.PI_PACKAGE_NEW_ADD_PI_PACKAGE_NEW_ADD_FORMNAME, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_ITEMID, loginDTO)%>
                                </th>
<%--                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_CURRENT_STOCK, loginDTO)%>--%>
<%--                                </th>--%>
                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_AMOUNT, loginDTO)%>
                                </th>
<%--                                <th><%=UtilCharacter.getDataByLanguage(Language, "বরাদ্দ", "Allocated")%>--%>
<%--                                </th>--%>
<%--                                <th><%=UtilCharacter.getDataByLanguage(Language, "অবশিষ্ট", "Remaining")%>--%>
<%--                                </th>--%>
<%--                                <th><%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_UNITPRICE, loginDTO)%>--%>
<%--                                </th>--%>
<%--                                <th><%=LM.getText(LC.PI_PURCHASE_ADD_TOTALVALUE, loginDTO)%>--%>
<%--                                </th>--%>
                                <th style="min-width: 25rem"><%=LM.getText(LC.APPROVAL_EXECUTION_TABLE_ADD_REMARKS, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="item-list-for-receive">
                            <%
                                List<Pi_product_receive_childDTO> childModels = (List<Pi_product_receive_childDTO>)
                                        Pi_product_receive_childDAO.getInstance()
                                                .getDTOsByParent("pi_product_receive_id", model.iD);
                                for (Pi_product_receive_childDTO childModel : childModels) {
                            %>
                            <tr id="template-item-row">
                                <td id="packageName">
                                    <%=Pi_package_finalRepository.getInstance().getName(childModel.packageId, Language)%>
                                </td>
                                <td id="itemTypeName">
                                    <%=ProcurementGoodsTypeRepository.getInstance().getName(Language, childModel.itemTypeId)%>
                                </td>
                                <td id="itemName">
                                    <%=Procurement_goodsRepository.getInstance().getName(Language, childModel.itemId)%>
                                </td>
<%--                                <td id="currentStock">--%>

<%--                                </td>--%>
                                <td>
                                    <%=(long)childModel.quantity%>
                                </td>
<%--                                <td id="allocatedStock"></td>--%>
<%--                                <td id="remainingStock"></td>--%>
<%--                                <td id="unitPrice"></td>--%>
<%--                                <td id="totalPrice"></td>--%>
                                <td>
                                    <%=childModel.remarks%>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>