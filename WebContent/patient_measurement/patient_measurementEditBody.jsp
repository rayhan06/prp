<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="patient_measurement.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="appointment.*" %>

<%
    Patient_measurementDTO patient_measurementDTO;
    patient_measurementDTO = (Patient_measurementDTO) request.getAttribute("patient_measurementDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (patient_measurementDTO == null) {
        patient_measurementDTO = new Patient_measurementDTO();

    }
    System.out.println("patient_measurementDTO = " + patient_measurementDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.PATIENT_MEASUREMENT_ADD_PATIENT_MEASUREMENT_ADD_FORMNAME, loginDTO);
    String servletName = "Patient_measurementServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.PATIENT_MEASUREMENT_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    long appointmentId = -1;
    AppointmentDAO appointmentDAO = new AppointmentDAO();
    AppointmentDTO appointmentDTO = null;

    if (request.getParameter("appointmentId") != null) {
        appointmentId = Long.parseLong(request.getParameter("appointmentId"));
        appointmentDTO = appointmentDAO.getDTOByID(appointmentId);
    }
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Patient_measurementServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8  offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1 ">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=patient_measurementDTO.iD%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='appointmentId'
                                           id='appointmentId_hidden_<%=i%>'
                                           value=<%=actionName.equals("edit")?("'" + patient_measurementDTO.appointmentId + "'"):("'" + appointmentId + "'")%> tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_NAME, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' readonly name='name'
                                                   id='name_text_<%=i%>'
                                                   value=<%=actionName.equals("edit")?("'" + patient_measurementDTO.name + "'"):("'" + appointmentDTO.patientName + "'")%>' 																  tag='
                                                   pb_html'/>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' readonly="readonly"
                                           data-label="Document Date" id='dateOfBirth_date_<%=i%>'
                                           name='dateOfBirth' value=<%
													if(actionName.equals("edit"))
													{
														String formatted_dateOfBirth = dateFormat.format(new Date(patient_measurementDTO.dateOfBirth));
														%>
                                                   '<%=formatted_dateOfBirth%>'
                                    <%
                                    } else {
                                        String formatted_Date = dateFormat.format(new Date(appointmentDTO.dateOfBirth));
                                    %>
                                    '<%=formatted_Date%>'
                                    <%
                                        }
                                    %>
                                    tag='pb_html'>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_HEIGHT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                value = "";
                                                if (patient_measurementDTO.height != -1) {
                                                    value = patient_measurementDTO.height + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='height'
                                                   id='height_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_WEIGHT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                value = "";
                                                if (patient_measurementDTO.weight != -1) {
                                                    value = patient_measurementDTO.weight + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='weight'
                                                   id='weight_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.HM_BLOOD_PRESSURE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <div class="row d-flex justify-content-between" style="font-family: Roboto,SolaimanLipi!important; font-weight: 400;">
                                                <div class="col-6 row d-flex align-items-center">
                                                    <div class="col-md-3">
                                                        <%=LM.getText(LC.HM_SYSTOLE, loginDTO)%>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <input type='number' class='form-control'
                                                               name='bloodPressureSystole'
                                                               id='bloodPressureSystole_text_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" + patient_measurementDTO.bloodPressureSystole + "'"):("''")%>
                                                                       tag='pb_html'
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-6 row d-flex align-items-center">
                                                    <div class="col-md-4">
                                                        <%=LM.getText(LC.HM_DIASTOLE, loginDTO)%>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input
                                                                type='number' class='form-control'
                                                                name='bloodPressureDiastole'
                                                                id='bloodPressureDiastole_text_<%=i%>'
                                                                value=<%=actionName.equals("edit")?("'" + patient_measurementDTO.bloodPressureDiastole + "'"):("''")%>
                                                                        tag='pb_html'
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PULSE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                value = "";
                                                if (patient_measurementDTO.pulse != -1) {
                                                    value = patient_measurementDTO.pulse + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='pulse'
                                                   id='pulse_number_<%=i%>' value='<%=value%>' tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_TEMPERATURE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                value = "";
                                                if (patient_measurementDTO.temperature != -1) {
                                                    value = patient_measurementDTO.temperature + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='temperature'
                                                   id='temperature_number_<%=i%>' value='<%=value%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("english")?"Blood Sugar":"রক্তের শর্করা"%>
                                        </label>
                                        <div class="col-md-9">
                                            
                                            <input type='number' class='form-control' name='sugar'
                                                   id='sugar' value='<%=patient_measurementDTO.sugar%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_OXYGENSATURATION, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                value = "";
                                                if (patient_measurementDTO.oxygenSaturation != -1) {
                                                    value = patient_measurementDTO.oxygenSaturation + "";
                                                }
                                            %>
                                            <input type='number' class='form-control' name='oxygenSaturation'
                                                   id='oxygenSaturation_number_<%=i%>' value='<%=value%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=patient_measurementDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=patient_measurementDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=patient_measurementDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-actions text-right mt-3">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PATIENT_MEASUREMENT_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PATIENT_MEASUREMENT_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">


    function PreprocessBeforeSubmiting(row, validate) {


        preprocessDateBeforeSubmitting('dateOfBirth', row);

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Patient_measurementServlet");
    }

    function init(row) {

        setDateByStringAndId('dateOfBirth_js_' + row, $('#dateOfBirth_date_' + row).val());


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>






