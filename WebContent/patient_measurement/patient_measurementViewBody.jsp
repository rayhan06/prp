<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="patient_measurement.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>

<%
    String servletName = "Patient_measurementServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.PATIENT_MEASUREMENT_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Patient_measurementDAO patient_measurementDAO = new Patient_measurementDAO("patient_measurement");
    Patient_measurementDTO patient_measurementDTO = patient_measurementDAO.getDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PATIENT_MEASUREMENT_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PATIENT_MEASUREMENT_ADD_FORMNAME, loginDTO)%>
            </h5>
            <div>
                <table class="table table-bordered table-striped">
                    <tr>
                        <td ><b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_NAME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = patient_measurementDTO.name + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td ><b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_DATEOFBIRTH, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = patient_measurementDTO.dateOfBirth + "";
                            %>
                            <%
                                String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
                            %>
                            <%=Utils.getDigits(formatted_dateOfBirth, Language)%>


                        </td>

                    </tr>





                    <tr>
                        <td ><b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_HEIGHT, loginDTO)%>
                        </b></td>
                        <td>


                            <%
                                value = String.format("%.1f", patient_measurementDTO.height);
                            %>

                            <%=patient_measurementDTO.height > 0 ? Utils.getDigits(value, Language) : ""%>


                        </td>

                    </tr>


                    <tr>
                        <td ><b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_WEIGHT, loginDTO)%>
                        </b></td>
                        <td>

                            
                            <%
                                value = String.format("%.1f", patient_measurementDTO.weight);
                            %>

                            <%=patient_measurementDTO.weight > 0 ? Utils.getDigits(value, Language) : ""%>


                        </td>

                    </tr>


                    <tr>
                        <td >
                            <b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_BLOODPRESSUREDIASTOLE, loginDTO)%>
                            </b></td>
                        <td>

                           
                            <%
                                value = String.format("%.1f", patient_measurementDTO.bloodPressureDiastole);
                            %>

                            <%=patient_measurementDTO.bloodPressureDiastole > 0 ? Utils.getDigits(value, Language) : ""%>


                        </td>

                    </tr>


                    <tr>
                        <td >
                            <b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_BLOODPRESSURESYSTOLE, loginDTO)%>
                            </b></td>
                        <td>

                            <%
                                value = String.format("%.1f", patient_measurementDTO.bloodPressureSystole);
                            %>

                            <%=patient_measurementDTO.bloodPressureSystole > 0 ? Utils.getDigits(value, Language) : ""%>


                        </td>

                    </tr>


                    <tr>
                        <td ><b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_PULSE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = String.format("%.1f", patient_measurementDTO.pulse);
                            %>

                            <%=patient_measurementDTO.pulse > 0 ? Utils.getDigits(value, Language) : ""%>


                        </td>

                    </tr>
                    
                    <tr>
                        <td ><b><%=Language.equalsIgnoreCase("english")?"Blood Sugar":"রক্তের শর্করা"%>

                        </b></td>
                        <td>

                            <%=Utils.getDigits(patient_measurementDTO.sugar, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td ><b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_TEMPERATURE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = String.format("%.1f", patient_measurementDTO.temperature);
                            %>

                            <%=patient_measurementDTO.temperature > 0 ? Utils.getDigits(value, Language) : ""%>


                        </td>

                    </tr>


                    <tr>
                        <td ><b><%=LM.getText(LC.PATIENT_MEASUREMENT_ADD_OXYGENSATURATION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = String.format("%.1f", patient_measurementDTO.oxygenSaturation);
                            %>

                            <%=patient_measurementDTO.oxygenSaturation > 0 ? Utils.getDigits(value, Language) : ""%>


                        </td>

                    </tr>


                   

                </table>
            </div>
        </div>
    </div>
</div>