<%@page pageEncoding="UTF-8" %>

<%@page import="patient_measurement.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PATIENT_MEASUREMENT_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_PATIENT_MEASUREMENT;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Patient_measurementDTO patient_measurementDTO = (Patient_measurementDTO) request.getAttribute("patient_measurementDTO");
    CommonDTO commonDTO = patient_measurementDTO;
    String servletName = "Patient_measurementServlet";


    System.out.println("patient_measurementDTO = " + patient_measurementDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Patient_measurementDAO patient_measurementDAO = (Patient_measurementDAO) request.getAttribute("patient_measurementDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_name' class="text-nowrap">
    <%
        value = patient_measurementDTO.name + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_dateOfBirth' class="text-nowrap">
    <%
        value = patient_measurementDTO.dateOfBirth + "";
    %>
    <%
        String formatted_dateOfBirth = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_dateOfBirth, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Patient_measurementServlet?actionType=view&ID=<%=patient_measurementDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Patient_measurementServlet?actionType=getEditPage&ID=<%=patient_measurementDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td class="text-right" id='<%=i%>_checkbox'>
	<div class='checker'>
        <span class='chkEdit'>
            <input type='checkbox' name='ID' value='<%=patient_measurementDTO.iD%>'/>
        </span>
	</div>
</td>
