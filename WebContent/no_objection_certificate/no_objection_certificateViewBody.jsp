<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="file_tracker.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@page import="util.*" %>
<%@ page import="no_objection_certificate.No_objection_certificateDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>


<%
    String servletName = "No_objection_certificateServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    No_objection_certificateDTO nocDTO = (No_objection_certificateDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    CommonDTO commonDTO = nocDTO;
%>
<%@include file="../pb/viewInitializer.jsp" %>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=isLanguageEnglish ? "NOC" : "এন.ও.সি"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-10 offset-1">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=isLanguageEnglish ? "NOC" : "এন.ও.সি"%>
                                        </h4>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Subject" : "বিষয়"%>
                                    </label>
                                    <div class="col-9">
                                        <%=nocDTO.subject%>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Recipient" : "প্রাপক"%>
                                    </label>
                                    <div class="col-9">
                                        <%=Employee_recordsRepository.getInstance().getEmployeeName(nocDTO.employeeRecordId, isLanguageEnglish)%>
                                        <br>
                                        <strong>
                                            <%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, nocDTO.organogramId)%>
                                        </strong>
                                        <br>
                                        <%=Office_unitsRepository.getInstance().geText(Language, nocDTO.officeUnitId)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Issue Date" : "প্রদানের তারিখ"%>
                                    </label>
                                    <div class="col-9">
                                        <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, nocDTO.issueDate), Language)%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=isLanguageEnglish ? "Issuing authority" : "প্রদানকারী কর্তৃপক্ষ"%>
                                    </label>
                                    <div class="col-9">
                                        <%=nocDTO.issueAuthority%>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-3 col-form-label text-right">
                                        <%=LM.getText(LC.FILE_TRACKER_ADD_FILEDROPZONE, loginDTO)%>
                                    </label>
                                    <div class="col-9">
                                        <%
                                            String fileColumnName = "";
                                            List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(nocDTO.fileDropzone);
                                        %>
                                        <table>
                                            <tr>
                                                <%
                                                    if (fileList != null) {
                                                        for (FilesDTO filesDTO : fileList) {
                                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                %>
                                                <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                                    <%
                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                    %>
                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px'/>
                                                    <%
                                                        }
                                                    %>
                                                    <a href='<%=servletName%>?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                       download><%=filesDTO.fileTitle%>
                                                    </a>
                                                </td>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>