<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="file_tracker.*" %>
<%@ page import="util.*" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%@ page import="no_objection_certificate.No_objection_certificateDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="office_unit_organograms.OfficeUnitOrganogramsRepository" %>


<%
    String navigator2 = "navFILE_TRACKER";
    String servletName = "No_objection_certificateServlet";
%>

<%@include file="../pb/searchInitializer.jsp" %>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>
                <%=isLanguageEnglish ? "Office" : "দপ্তর"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Recipient" : "প্রাপক"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Issue Date" : "প্রদানের তারিখ"%>
            </th>
            <th>
                <%=isLanguageEnglish ? "Issuing authority" : "প্রদানকারী কর্তৃপক্ষ"%>
            </th>
            <th>
                <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th>
                <%=LM.getText(LC.FILE_TRACKER_SEARCH_FILE_TRACKER_EDIT_BUTTON, loginDTO)%>
            </th>
            <th>
                <div class="d-flex align-items-center justify-content-between mt-3">
                    <span><%=isLanguageEnglish? "All" : "সব"%></span>
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            RecordNavigator recordNavigator = (RecordNavigator) request.getAttribute("recordNavigator");
            List<No_objection_certificateDTO> data = (List<No_objection_certificateDTO>) recordNavigator.list;
            try {
                if (data != null) {
                    for (No_objection_certificateDTO noObjectionCertificateDTO : data) {
        %>
        <tr>
            <td>
                <%=Office_unitsRepository.getInstance().geText(Language, noObjectionCertificateDTO.officeUnitId)%>
            </td>
            <td>
                <%=Employee_recordsRepository.getInstance().getEmployeeName(noObjectionCertificateDTO.employeeRecordId, isLanguageEnglish)%>
                <br>
                <strong>
                    <%=OfficeUnitOrganogramsRepository.getInstance().getDesignation(Language, noObjectionCertificateDTO.organogramId)%>
                </strong>
            </td>
            <td>
                <%=Utils.getDigits(util.StringUtils.getFormattedDate(Language, noObjectionCertificateDTO.issueDate), Language)%>
            </td>
            <td>
                <%=noObjectionCertificateDTO.issueAuthority%>
            </td>

            <%
                CommonDTO commonDTO = noObjectionCertificateDTO;
            %>
            <%@include file="../pb/searchAndViewButton.jsp" %>
            <td class="text-right">
                <div class='checker'>
                    <span class='chkEdit'>
                        <input type='checkbox' name='ID' value='<%=noObjectionCertificateDTO.iD%>'/>
                    </span>
                </div>
            </td>
        </tr>
        <%
                    }
                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>
        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			