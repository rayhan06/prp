<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    System.out.println("Inside nav.jsp");
    String url = "No_objection_certificateServlet?actionType=search";
%>
<%@include file="../pb/navInitializer.jsp" %>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed(false)' id='anyfield' name='anyfield'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"
                     aria-hidden="true" x-placement="top"
                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">
                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>
                    <div class="tooltip-inner">Collapse</div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label">
                            <%=isLanguageEnglishNav ? "Issue Date (From)" : "প্রদানের তারিখ (হতে)"%>
                        </label>
                        <div class="col-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="issueDateFrom_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type="hidden" id="issueDateFrom" name="issueDateFrom">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label">
                            <%=isLanguageEnglishNav ? "Issue Date (To)" : "প্রদানের তারিখ (পর্যন্ত)"%>
                        </label>
                        <div class="col-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="issueDateTo_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type="hidden" id="issueDateTo" name="issueDateTo">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-xl-3 col-form-label">
                            <%=isLanguageEnglishNav ? "Office" : "অফিস"%>
                        </label>
                        <div class="col-md-8 col-xl-9 col-form-label">
                            <%--Office Select Modal Button--%>
                            <button type="button" class="btn btn-primary btn-block shadow btn-border-radius"
                                    id="officeUnit_modal_button">
                                <%=isLanguageEnglishNav ? "Select" : "বাছাই করুন"%>
                            </button>
                            <div class="input-group mr-1" id="office_units_id_div" style="display: none">
                                <input type="hidden" id='office_units_id' name="office_units_id" value='' tag='pb_html'>
                                <button type="button" class="btn btn-secondary btn-block shadow-sm btn-border-radius form-control"
                                        id="office_units_id_text" onclick="officeModalEditButtonClicked()">
                                </button>
                                <span class="input-group-btn" tag='pb_html'>
                                <button type="button" class="btn btn-outline-danger"
                                        id='office_units_id_crs_btn' tag='pb_html'>
									x
								</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-3 col-form-label">
                            <%=isLanguageEnglishNav ? "Issuing authority" : "প্রদানকারী কর্তৃপক্ষ"%>
                        </label>
                        <div class="col-9">
                            <input type="text" class="form-control"
                                   id="issueAuthority"
                                   name="issueAuthority"
                            >
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-right">
                <input type="hidden" name="search" value="yes"/>
                <button type="submit"
                        class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                        onclick="allfield_changed(true)"
                        style="background-color: #00a1d4;">
                    <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>

<jsp:include page="../employee_assign/officeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>

<script src="<%=context%>/assets/scripts/search_param_state.js" type="text/javascript"></script>
<script type="text/javascript">
    const $anyField = $('#anyfield');
    const $issueDateFrom = $('#issueDateFrom');
    const $issueDateTo = $('#issueDateTo');
    const $issueAuthority = $('#issueAuthority');
    const $officeUnitsId = $('#office_units_id');

    function getParams() {
        var params = 'AnyField=' + document.getElementById('anyfield').value;
        $issueDateFrom.val(getDateStringById('issueDateFrom_js'));
        params += '&issueDateFrom=' + getFormattedDate('issueDateFrom');

        $issueDateTo.val(getDateStringById('issueDateTo_js'));
        params += '&issueDateTo=' + getBDFormattedDate('issueDateTo');
        params += '&issueAuthority=' + $issueAuthority.val();
        params += '&officeUnitsId=' + $officeUnitsId.val();

        params += '&search=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }

        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        return params;
    }

    function allfield_changed(setPushState) {
        var params = getParams();
        navsubmit(params, "No_objection_certificateServlet", setPushState);
    }

    function resetFields() {
        $anyField.val("");
        $issueAuthority.val("");
        resetDateById('issueDateFrom_js');
        $issueDateFrom.val('');
        resetDateById('issueDateTo_js');
        $issueDateTo.val('');
    }

    window.addEventListener('popstate', e => {
        if (e.state) {
            let params = e.state;
            navsubmit(params, "No_objection_certificateServlet", false);
            resetFields();
            let arr = params.split('&');
            arr.forEach(e => {
                let item = e.split('=');
                if (item.length === 2) {
                    //console.log("param " + item[0] + " = " + item[1]);
                    switch (item[0]) {
                        case 'anyfield':
                        case 'AnyField':
                            $anyField.val(item[1]);
                            break;
                        case 'issueDateFrom':
                            setDateByTimestampAndId('issueDateFrom_js', item[1]);
                            break;
                        case 'issueDateTo':
                            setDateByTimestampAndId('issueDateTo_js', item[1]);
                            break;
                        case 'issueAuthority':
                            $issueAuthority.val(item[1]);
                            break;
                    }
                }
            });
        } else {
            navsubmit(null, "No_objection_certificateServlet", false);
        }
    });

    window.onbeforeunload = function () {
        history.pushState(getParams(), '', '<%=url%>&' + params);
    };

    $(document).ready(() => {
        readyInit('No_objection_certificateServlet');
    });

    function viewOfficeIdInInput(selectedOffice) {
        if (selectedOffice.id === '') {
            return;
        }
        $('#officeUnit_modal_button').hide();
        $('#office_units_id_div').show();
        document.getElementById('office_units_id_text').innerHTML = selectedOffice.name;
        $('#office_units_id').val(selectedOffice.id);
    }

    $('#office_units_id_crs_btn').on('click', function () {
        $('#officeUnit_modal_button').show();
        $('#office_units_id_div').hide();

        $('#office_units_id').val('');
        document.getElementById('office_units_id_text').innerHTML = '';
        document.getElementById('rank_cat').innerHTML = '';
    });

    // Office Select Modal
    // modal trigger button
    // this part is needed because if one may have more than one place to select office
    officeSelectModalUsage = 'none';
    officeSelectModalOptionsMap = new Map([
        ['officeUnit', {
            officeSelectedCallback: viewOfficeIdInInput
        }]
    ]);

    $('#officeUnit_modal_button').on('click', function () {
        officeSelectModalUsage = 'officeUnit';
        $('#search_office_modal').modal();
    });

    function officeModalEditButtonClicked() {
        officeSelectModalUsage = 'officeUnit';
        officeSearchSetSelectedOfficeLayers($('#office_units_id').val());
        $('#search_office_modal').modal();
    }
</script>

