<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="file_tracker.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.OfficeUnitTypeEnum" %>
<%@ page import="employee_records.EmployeeFlatInfoDTO" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="no_objection_certificate.No_objection_certificateDTO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="no_objection_certificate.No_objection_certificateDAO" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="pbReport.DateUtils" %>


<%
    No_objection_certificateDTO no_objection_certificateDTO = (No_objection_certificateDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    CommonDTO commonDTO = no_objection_certificateDTO;
    if (no_objection_certificateDTO == null) {
        no_objection_certificateDTO = new No_objection_certificateDTO();
    }
    String tableName = No_objection_certificateDAO.getInstance().getTableName();
    long defaultIssueDate = DateUtils.get12amTime(System.currentTimeMillis());
%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = isLanguageEnglish ? "NOC" : "এন.ও.সি";
    String servletName = "No_objection_certificateServlet";
    boolean isEditPage = "ajax_edit".equalsIgnoreCase(actionName);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bigform" name="bigform" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=no_objection_certificateDTO.iD%>' tag='pb_html'/>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="subject">
                                            <%=isLanguageEnglish ? "Subject" : "বিষয়"%>
                                        </label>
                                        <div class="col-9">
                                            <textarea class='form-control' name='subject' style="resize: none"
                                                      rows="3" id='subject'
                                                      maxlength="2048"><%=no_objection_certificateDTO.subject%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=isLanguageEnglish ? "Recipient" : "প্রাপক"%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-md-9">
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="employee_record_id_modal_button"
                                                    onclick="employeeRecordIdModalBtnClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, userDTO)%>
                                            </button>
                                            <div class="input-group" id="employee_record_id_div" style="display: none">
                                                <input type="hidden" name='officeUnitId' id='officeUnitId_input'
                                                       value="">
                                                <input type="hidden" name='organogramId' id='organogramId_input'
                                                       value="">
                                                <input type="hidden" name='employeeRecordId' id='employeeRecordId_input'
                                                       value="">
                                                <button type="button" class="btn btn-secondary form-control" disabled
                                                        id="employee_record_id_text"></button>
                                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
                                                    <button type="button" class="btn btn-outline-danger modalCross"
                                                            onclick="crsBtnClicked('employee_record_id');"
                                                            id='employee_record_id_crs_btn' tag='pb_html'>
                                                        x
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right">
                                            <%=isLanguageEnglish ? "Issue Date" : "প্রদানের তারিখ"%>
                                            <span>*</span>
                                        </label>
                                        <div class="col-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID" value="issueDate_js"/>
                                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                                            </jsp:include>
                                            <input type='hidden' name='issueDate' id='issueDate'
                                                   value='<%=dateFormat.format(new Date(no_objection_certificateDTO.issueDate))%>'
                                            >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right" for="issueAuthority">
                                            <%=isLanguageEnglish ? "Issuing authority" : "প্রদানকারী কর্তৃপক্ষ"%>
                                        </label>
                                        <div class="col-9">
                                            <input type='text' class='form-control' name='issueAuthority'
                                                   id='issueAuthority'
                                                   value='<%=no_objection_certificateDTO.issueAuthority%>'/>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right">
                                            <%=LM.getText(LC.FILE_TRACKER_ADD_FILEDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-9">
                                            <%
                                                fileColumnName = "fileDropzone";
                                                if (actionName.equals("ajax_edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(no_objection_certificateDTO.fileDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    no_objection_certificateDTO.fileDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=no_objection_certificateDTO.fileDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=no_objection_certificateDTO.fileDropzone%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn"
                                    onclick="location.href = '<%=request.getHeader("referer")%>'"
                            >
                                <%=LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button" onclick="submitForm()">
                                <%=LM.getText(LC.FILE_TRACKER_ADD_FILE_TRACKER_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">
    const form = $('#bigform');
    const $officeUnitId = $('#officeUnitId_input');
    const $organogramId = $('#organogramId_input');
    const $employeeRecordId = $('#employeeRecordId_input');
    const $issueDate = $('#issueDate');

    $(document).ready(function () {
        <%if(isEditPage){
            String employeeSearchModelJson = EmployeeSearchModalUtil.getEmployeeSearchModelJson(
                    no_objection_certificateDTO.employeeRecordId,
                    no_objection_certificateDTO.officeUnitId,
                    no_objection_certificateDTO.organogramId
            );
        %>
        setDateByStringAndId('issueDate_js', $issueDate.val());
        const employeeSearchModel = JSON.parse('<%=employeeSearchModelJson%>');
        viewEmployeeRecordIdInInput(employeeSearchModel);
        <%} else {%>
        setDateByTimestampAndId('issueDate_js', <%=defaultIssueDate%>);
        $issueDate.val(getDateStringById('issueDate_js'));
        <%}%>
    });

    function submitForm() {
        $issueDate.val(getDateStringById('issueDate_js'));
        submitAjaxByData(form.serialize(), "<%=servletName%>?actionType=<%=actionName%>");
    }

    function buttonStateChange(value) {
        $(':button').prop('disabled', value);
    }

    function crsBtnClicked(fieldName) {
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        document.getElementById(fieldName + '_text').innerHTML = '';
        $officeUnitId.val('');
        $organogramId.val('');
        $employeeRecordId.val('');
    }

    function viewEmployeeRecordIdInInput(empInfo) {
        $('#employee_record_id_modal_button').hide();
        $('#employee_record_id_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let employeeView;
        if (language === 'english') {
            employeeView = empInfo.employeeNameEn + ', ' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn;
        } else {
            employeeView = empInfo.employeeNameBn + ', ' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn;
        }
        document.getElementById('employee_record_id_text').innerHTML = employeeView;
        $officeUnitId.val(empInfo.officeUnitId);
        $organogramId.val(empInfo.organogramId);
        $employeeRecordId.val(empInfo.employeeRecordId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: viewEmployeeRecordIdInInput
        }]
    ]);
    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }
</script>






