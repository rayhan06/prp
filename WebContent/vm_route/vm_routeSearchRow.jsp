<%@page pageEncoding="UTF-8" %>

<%@page import="vm_route.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle_office_assignment.Vm_vehicle_office_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_ROUTE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_VM_ROUTE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Vm_routeDTO vm_routeDTO = (Vm_routeDTO) request.getAttribute("vm_routeDTO");
    CommonDTO commonDTO = vm_routeDTO;
    String servletName = "Vm_routeServlet";


    System.out.println("vm_routeDTO = " + vm_routeDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Vm_routeDAO vm_routeDAO = (Vm_routeDAO) request.getAttribute("vm_routeDAO");

    FilesDAO filesDAO = new FilesDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
<td id='<%=i%>_nameEn'>
    <%
        value = vm_routeDTO.nameEn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_nameBn'>
    <%
        value = vm_routeDTO.nameBn + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_vehicleTypeCat'>
    <%
        value = vm_routeDTO.vehicleTypeCat + "";
    %>
    <%
        value = CatRepository.getInstance().getText(Language, "vehicle_type", vm_routeDTO.vehicleTypeCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_vehicleId'>
    <%
        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_routeDTO.vehicleId);
        value = "";
        if (vm_vehicleDTO != null) {
            value = vm_vehicleDTO.regNo;
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>

<td id='<%=i%>_vehicleDriverAssignmentId'>
    <%
        Vm_vehicle_driver_assignmentDTO assignmentDTO = Vm_vehicle_driver_assignmentRepository.getInstance()
                .getVm_vehicle_driver_assignmentDTOByID(vm_routeDTO.vehicleDriverAssignmentId);
        value = "";
        if (assignmentDTO != null) {
            value = UtilCharacter.getDataByLanguage(Language,
                    assignmentDTO.employeeRecordNameBn, assignmentDTO.employeeRecordName);
        }
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<%--											<td id = '<%=i%>_remarks'>--%>
<%--											<%--%>
<%--											value = vm_routeDTO.remarks + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>

<td id='<%=i%>_status'>
    <%
        value = Vm_routeDAO.getStatus(vm_routeDTO.status, Language);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='Vm_routeServlet?actionType=view&ID=<%=vm_routeDTO.iD%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button
            type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Vm_routeServlet?actionType=getEditPage&ID=<%=vm_routeDTO.iD%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=vm_routeDTO.iD%>'/></span>
    </div>
</td>
																						
											

