<%@ page import="util.UtilCharacter" %>
<%@page pageEncoding="UTF-8" %>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_routeServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="row mx-2 mx-md-0">
                                        <div class="col-lg-12">
                                            <div class="sub_title_top">
                                                <div class="sub_title">
                                                    <h4 style="background: white"><%=formTitle%>
                                                    </h4>
                                                </div>
                                            </div>


                                            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                                   value='<%=vm_routeDTO.iD%>' tag='pb_html'/>

                                            <%--                                                    <input type='hidden' class='form-control'  name='vehicleId' id = 'vehicleId_hidden_<%=i%>' value='<%=vm_routeDTO.vehicleId%>' tag='pb_html'/>--%>
                                            <input type='hidden' class='form-control' name='vehicleDriverAssignmentId'
                                                   id='vehicleDriverAssignmentId_hidden_<%=i%>'
                                                   value='<%=vm_routeDTO.vehicleDriverAssignmentId%>' tag='pb_html'/>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.VM_ROUTE_ADD_NAMEEN, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='nameEn'
                                                           id='nameEn_text_<%=i%>' value='<%=vm_routeDTO.nameEn%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.VM_ROUTE_ADD_NAMEBN, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' class='form-control' name='nameBn'
                                                           id='nameBn_text_<%=i%>' value='<%=vm_routeDTO.nameBn%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.VM_ROUTE_ADD_VEHICLETYPECAT, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='vehicleTypeCat'
                                                            id='vehicleTypeCat_category_<%=i%>' tag='pb_html'
                                                            onchange="loadVehicleList()">
                                                        <%
                                                            Options = CatRepository.getInstance().buildOptions("vehicle_type", Language, vm_routeDTO.vehicleTypeCat);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group row" id="vehicleIdDiv">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_VEHICLE_OFFICE_ASSIGNMENT_ADD_VEHICLEID, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='vehicleId'
                                                            id='givenVehicleId_select_<%=i%>' tag='pb_html'>
                                                    </select>

                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.VM_ROUTE_ADD_STATUS, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-9">
                                                    <select class='form-control' name='status' id='status_select_<%=i%>'
                                                            tag='pb_html'>
                                                        <%
                                                            Options = Vm_routeDAO.getStatusList(Language, vm_routeDTO.status);
                                                        %>
                                                        <%=Options%>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_ROUTE_ADD_REMARKS, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
																<textarea class='form-control' name='remarks'
                                                                          id='remarks_text_<%=i%>'><%=vm_routeDTO.remarks%></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_ROUTE_ADD_FILESDROPZONE, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <%
                                                        fileColumnName = "filesDropzone";
                                                        if (actionName.equals("edit")) {
                                                            List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_routeDTO.filesDropzone);
                                                    %>
                                                    <%@include file="../pb/dropzoneEditor.jsp" %>
                                                    <%
                                                        } else {
                                                            ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                            vm_routeDTO.filesDropzone = ColumnID;
                                                        }
                                                    %>

                                                    <div class="dropzone"
                                                         action="<%=servletName%>?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_routeDTO.filesDropzone%>">
                                                        <input type='file' style="display:none"
                                                               name='<%=fileColumnName%>File'
                                                               id='<%=fileColumnName%>_dropzone_File_<%=i%>'
                                                               tag='pb_html'/>
                                                    </div>
                                                    <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                           id='<%=fileColumnName%>FilesToDelete_<%=i%>' value=''
                                                           tag='pb_html'/>
                                                    <input type='hidden' name='<%=fileColumnName%>'
                                                           id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                           value='<%=vm_routeDTO.filesDropzone%>'/>


                                                </div>
                                            </div>
                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   id='insertedByUserId_hidden_<%=i%>'
                                                   value='<%=vm_routeDTO.insertedByUserId%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                                   id='insertedByOrganogramId_hidden_<%=i%>'
                                                   value='<%=vm_routeDTO.insertedByOrganogramId%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   id='insertionDate_hidden_<%=i%>'
                                                   value='<%=vm_routeDTO.insertionDate%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   id='isDeleted_hidden_<%=i%>' value='<%=vm_routeDTO.isDeleted%>'
                                                   tag='pb_html'/>

                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   id='lastModificationTime_hidden_<%=i%>'
                                                   value='<%=vm_routeDTO.lastModificationTime%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   id='searchColumn_hidden_<%=i%>' value='<%=vm_routeDTO.searchColumn%>'
                                                   tag='pb_html'/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5">
                    <h5 class="table-title">
                        <%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped text-nowrap">
                            <thead>
                            <tr>
                                <th><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_STOPPAGENAME, loginDTO)%>
                                </th>
                                <th><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_RENT, loginDTO)%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "শুরুর তারিখ ", "Start Date")%>
                                </th>
                                <%--                                                <th><%=UtilCharacter.getDataByLanguage(Language, "শেষের তারিখ ", "End Date")%></th>--%>
                                <th><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_REMOVE, loginDTO)%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="field-VmRouteStoppage">


                            <%
                                if (actionName.equals("edit")) {
                                    int index = -1;

//                                    System.out.println("###");
//                                    System.out.println(vm_routeDTO.vmRouteStoppageDTOList);
//                                    System.out.println("###");


                                    for (VmRouteStoppageDTO vmRouteStoppageDTO : vm_routeDTO.vmRouteStoppageDTOList) {
                                        index++;

                                        System.out.println("index index = " + index);

                            %>

                            <tr id="VmRouteStoppage_<%=index + 1%>">
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='vmRouteStoppage.iD'
                                           id='iD_hidden_<%=childTableStartingID%>'
                                           value='<%=vmRouteStoppageDTO.iD%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='vmRouteStoppage.vmRouteId'
                                           id='vmRouteId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmRouteStoppageDTO.vmRouteId%>' tag='pb_html'/>
                                </td>
                                <td>


                                    <input type='text' class='form-control' name='vmRouteStoppage.stoppageName'
                                           id='stoppageName_text_<%=childTableStartingID%>'
                                           value='<%=vmRouteStoppageDTO.stoppageName%>' tag='pb_html'/>
                                </td>
                                <td>


                                    <%
                                        value = "";
                                        if (vmRouteStoppageDTO.rent != -1) {
                                            value = vmRouteStoppageDTO.rent + "";
                                        }
                                    %>
                                    <input type='number' class='form-control' name='vmRouteStoppage.rent'
                                           id='rent_number_<%=childTableStartingID%>' value='<%=value%>'
                                           tag='pb_html'>
                                </td>


                                <td>
                                    <%value = "startDate_js_" + childTableStartingID;%>
                                    <jsp:include page="/date/date.jsp">
                                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>
                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                        <jsp:param name="END_YEAR" value="<%=year + 5%>"></jsp:param>
                                    </jsp:include>
                                    <input type='hidden' name='vmRouteStoppage.startDate'
                                           id='startDate_date_<%=childTableStartingID%>'
                                           value='<%=dateFormat.format(new Date(vmRouteStoppageDTO.start_date))%>'
                                           tag='pb_html'>
                                </td>

                                <%--                                                <td>--%>
                                <%--                                                    <%value = "endDate_js_" + childTableStartingID;%>--%>
                                <%--                                                    <jsp:include page="/date/date.jsp">--%>
                                <%--                                                        <jsp:param name="DATE_ID" value="<%=value%>"></jsp:param>--%>
                                <%--                                                        <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
                                <%--                                                    </jsp:include>--%>
                                <%--                                                    <input type='hidden' name='vmRouteStoppage.endDate' id = 'endDate_date_<%=childTableStartingID%>' value= '<%=dateFormat.format(new Date(vmRouteStoppageDTO.end_date)).toString()%>' tag='pb_html'>--%>
                                <%--                                                </td>--%>


                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='vmRouteStoppage.insertedByUserId'
                                           id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmRouteStoppageDTO.insertedByUserId%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='vmRouteStoppage.insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=childTableStartingID%>'
                                           value='<%=vmRouteStoppageDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='vmRouteStoppage.insertionDate'
                                           id='insertionDate_hidden_<%=childTableStartingID%>'
                                           value='<%=vmRouteStoppageDTO.insertionDate%>' tag='pb_html'/>
                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control' name='vmRouteStoppage.isDeleted'
                                           id='isDeleted_hidden_<%=childTableStartingID%>'
                                           value='<%=vmRouteStoppageDTO.isDeleted%>' tag='pb_html'/>

                                </td>
                                <td style="display: none;">


                                    <input type='hidden' class='form-control'
                                           name='vmRouteStoppage.lastModificationTime'
                                           id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                           value='<%=vmRouteStoppageDTO.lastModificationTime%>' tag='pb_html'/>
                                </td>
                                <td>
                                    <div class='checker'><span id='chkEdit'><input type='checkbox'
                                                                                   id='vmRouteStoppage_cb_<%=index%>'
                                                                                   name='checkbox'
                                                                                   value=''/></span></div>
                                </td>
                            </tr>
                            <%
                                        childTableStartingID++;
                                    }
                                }
                            %>

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right mt-3">
                            <button
                                    id="add-more-VmRouteStoppage"
                                    name="add-moreVmRouteStoppage"
                                    type="button"
                                    class="btn btn-sm text-white add-btn shadow">
                                <i class="fa fa-plus"></i>
                                <%=LM.getText(LC.HM_ADD, loginDTO)%>
                            </button>
                            <button
                                    id="remove-VmRouteStoppage"
                                    name="removeVmRouteStoppage"
                                    type="button"
                                    class="btn btn-sm remove-btn shadow ml-2 pl-4">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    <%VmRouteStoppageDTO vmRouteStoppageDTO = new VmRouteStoppageDTO();%>
                    <template id="template-VmRouteStoppage">
                        <tr>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='vmRouteStoppage.iD'
                                       id='iD_hidden_' value='<%=vmRouteStoppageDTO.iD%>' tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='vmRouteStoppage.vmRouteId'
                                       id='vmRouteId_hidden_' value='<%=vmRouteStoppageDTO.vmRouteId%>'
                                       tag='pb_html'/>
                            </td>
                            <td>


                                <input type='text' class='form-control' name='vmRouteStoppage.stoppageName'
                                       id='stoppageName_text_' value='<%=vmRouteStoppageDTO.stoppageName%>'
                                       tag='pb_html'/>
                            </td>
                            <td>


                                <%
                                    value = "";
                                    if (vmRouteStoppageDTO.rent != -1) {
                                        value = vmRouteStoppageDTO.rent + "";
                                    }
                                %>
                                <input type='number' class='form-control' name='vmRouteStoppage.rent'
                                       id='rent_number_' value='<%=value%>' tag='pb_html'>
                            </td>

                            <td>
                                <div id="startDate_div_" tag='pb_html'></div>

                                <input type='hidden' name='vmRouteStoppage.startDate' id='startDate_date_'
                                       value='<%=dateFormat.format(new Date())%>' tag='pb_html'>

                            </td>

                            <%--                                            <td>--%>
                            <%--                                                <div id="endDate_div_" tag='pb_html'></div>--%>

                            <%--                                                <input type='hidden' name='vmRouteStoppage.endDate' id = 'endDate_date_' value= '<%=dateFormat.format(new Date()).toString()%>' tag='pb_html'>--%>

                            <%--                                            </td>--%>


                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='vmRouteStoppage.insertedByUserId' id='insertedByUserId_hidden_'
                                       value='<%=vmRouteStoppageDTO.insertedByUserId%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='vmRouteStoppage.insertedByOrganogramId'
                                       id='insertedByOrganogramId_hidden_'
                                       value='<%=vmRouteStoppageDTO.insertedByOrganogramId%>' tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='vmRouteStoppage.insertionDate'
                                       id='insertionDate_hidden_' value='<%=vmRouteStoppageDTO.insertionDate%>'
                                       tag='pb_html'/>
                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control' name='vmRouteStoppage.isDeleted'
                                       id='isDeleted_hidden_' value='<%=vmRouteStoppageDTO.isDeleted%>'
                                       tag='pb_html'/>

                            </td>
                            <td style="display: none;">


                                <input type='hidden' class='form-control'
                                       name='vmRouteStoppage.lastModificationTime'
                                       id='lastModificationTime_hidden_'
                                       value='<%=vmRouteStoppageDTO.lastModificationTime%>' tag='pb_html'/>
                            </td>
                            <td>
                                <div><span id='chkEdit'><input type='checkbox' name='checkbox' value=''/></span>
                                </div>
                            </td>
                        </tr>

                    </template>
                </div>
                <div class="row mt-5">
                    <div class="col-12 form-actions text-center">
                        <a id = "cancel-btn" class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button id = "submit-btn" class="btn  btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                type="submit"><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<style>
    .required {
        color: red;
    }
</style>
