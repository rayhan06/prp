<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_route.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentDTO" %>
<%@ page import="vm_vehicle_driver_assignment.Vm_vehicle_driver_assignmentRepository" %>
<%@ page import="util.UtilCharacter" %>


<%
    String servletName = "Vm_routeServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_ROUTE_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_routeDTO vm_routeDTO = Vm_routeRepository.getInstance().getVm_routeDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title prp-page-title">
                            <i class="fa fa-gift"></i>&nbsp;
                            <%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_ADD_FORMNAME, loginDTO)%>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body form-body">
                    <h5 class="table-title">
                        <%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_ADD_FORMNAME, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">


                            <tr>
                                <td style="width:30%">
                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_VEHICLEID, loginDTO)%>
                                    </b></td>
                                <td>

                                    <%
                                        Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().getVm_vehicleDTOByID(vm_routeDTO.vehicleId);
                                        value = "";
                                        if (vm_vehicleDTO != null) {
                                            value = vm_vehicleDTO.regNo;
                                        }
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>

                            </tr>


                            <tr>
                                <td style="width:30%">
                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_VEHICLEDRIVERASSIGNMENTID, loginDTO)%>
                                    </b></td>
                                <td>

                                    <%
                                        Vm_vehicle_driver_assignmentDTO assignmentDTO = Vm_vehicle_driver_assignmentRepository.getInstance()
                                                .getVm_vehicle_driver_assignmentDTOByID(vm_routeDTO.vehicleDriverAssignmentId);
                                        value = "";
                                        if (assignmentDTO != null) {
                                            value = UtilCharacter.getDataByLanguage(Language,
                                                    assignmentDTO.employeeRecordNameBn, assignmentDTO.employeeRecordName);
                                        }
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>

                            </tr>


                            <tr>
                                <td style="width:30%">
                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_VEHICLETYPECAT, loginDTO)%>
                                    </b></td>
                                <td>

                                    <%
                                        value = vm_routeDTO.vehicleTypeCat + "";
                                    %>
                                    <%
                                        value = CatRepository.getInstance().getText(Language, "vehicle_type", vm_routeDTO.vehicleTypeCat);
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>

                            </tr>


                            <tr>
                                <td style="width:30%">
                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_NAMEEN, loginDTO)%>
                                    </b></td>
                                <td>

                                    <%
                                        value = vm_routeDTO.nameEn + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>

                            </tr>


                            <tr>
                                <td style="width:30%">
                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_NAMEBN, loginDTO)%>
                                    </b></td>
                                <td>

                                    <%
                                        value = vm_routeDTO.nameBn + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>

                            </tr>


                            <tr>
                                <td style="width:30%">
                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_REMARKS, loginDTO)%>
                                    </b></td>
                                <td>

                                    <%
                                        value = vm_routeDTO.remarks + "";
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>

                            </tr>


                            <tr>
                                <td style="width:30%">
                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_STATUS, loginDTO)%>
                                    </b></td>
                                <td>

                                    <%
                                        value = Vm_routeDAO.getStatus(vm_routeDTO.status, Language);
                                    %>

                                    <%=Utils.getDigits(value, Language)%>


                                </td>

                            </tr>


                            <tr>
                                <td style="width:30%">
                                    <b><%=LM.getText(LC.VM_ROUTE_ADD_FILESDROPZONE, loginDTO)%>
                                    </b></td>
                                <td>

                                    <%
                                        value = vm_routeDTO.filesDropzone + "";
                                    %>
                                    <%
                                        {
                                            List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(vm_routeDTO.filesDropzone);
                                    %>
                                    <%@include file="../pb/dropzoneViewer.jsp" %>
                                    <%
                                        }
                                    %>


                                </td>

                            </tr>


                        </table>
                    </div>
                    <div>
                        <h5 class="table-title mt-5">
                            <%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE, loginDTO)%>
                        </h5>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_STOPPAGENAME, loginDTO)%>
                                    </th>
                                    <%--														<th><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_RENT, loginDTO)%></th>--%>
                                    <%--														<th><%=UtilCharacter.getDataByLanguage(Language, "শুরুর তারিখ ", "Start Date")%></th>--%>
                                    <%--														<th><%=UtilCharacter.getDataByLanguage(Language, "শেষের তারিখ ", "End Date")%></th>--%>
                                    <th></th>

                                </tr>
                                <%
//                                    VmRouteStoppageDAO vmRouteStoppageDAO = new VmRouteStoppageDAO();


                                    List<VmRouteStoppageDTO> vmRouteStoppageDTOs =
                                            VmStoppageRepository.getInstance().
                                            getVmRouteStoppageDTOByRouteId(vm_routeDTO.iD);
//                                            vmRouteStoppageDAO.getVmRouteStoppageDTOListByVmRouteID(vm_routeDTO.iD);

                                    for (VmRouteStoppageDTO vmRouteStoppageDTO : vmRouteStoppageDTOs) {
                                %>
                                <tr>
                                    <td>
                                        <%
                                            value = vmRouteStoppageDTO.stoppageName + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>


                                    </td>


                                    <td>
                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <%--																<th><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_STOPPAGENAME, loginDTO)%></th>--%>
                                                <th><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_STOPPAGE_RENT, loginDTO)%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "শুরুর তারিখ ", "Start Date")%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "শেষের তারিখ ", "End Date")%>
                                                </th>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <%
                                                        value = vmRouteStoppageDTO.rent + "";
                                                    %>
                                                    <%
                                                        value = String.format("%.1f", vmRouteStoppageDTO.rent);
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>


                                                </td>
                                                <td>
                                                    <%
                                                        value = vmRouteStoppageDTO.start_date + "";
                                                    %>
                                                    <%
                                                        String formatted_purchaseDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(formatted_purchaseDate, Language)%>

                                                </td>

                                                <td></td>

                                            </tr>

                                            <%
                                                List<VmRouteStoppageDTO> stoppageDTOS = VmStoppageRepository.getInstance().
                                                        getVmRouteStoppageDTOBySelfId(vmRouteStoppageDTO.iD);
//                                                        vmRouteStoppageDAO.getVmRouteStoppageDTOListBySelfId(vmRouteStoppageDTO.iD);
                                                for (VmRouteStoppageDTO sDTO : stoppageDTOS) {
                                            %>


                                            <tr>

                                                <td>
                                                    <%
                                                        //															value = sDTO.rent + "";
                                                    %>
                                                    <%
                                                        value = String.format("%.1f", sDTO.rent);
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>


                                                </td>
                                                <td>
                                                    <%
                                                        value = sDTO.start_date + "";
                                                    %>
                                                    <%
                                                        String f1 = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(f1, Language)%>

                                                </td>

                                                <td>
                                                    <%
                                                        value = sDTO.end_date + "";
                                                    %>
                                                    <%
                                                        String f2 = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(f2, Language)%>

                                                </td>

                                            </tr>

                                            <%

                                                }
                                            %>
                                        </table>

                                    </td>


                                    <%--														<td>--%>
                                    <%--															<%--%>
                                    <%--																value = vmRouteStoppageDTO.end_date + "";--%>
                                    <%--															%>--%>
                                    <%--															<%--%>
                                    <%--																String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
                                    <%--															%>--%>
                                    <%--															<%=Utils.getDigits(formatted_endDate, Language)%>--%>

                                    <%--														</td>--%>


                                        <%

														}

													%>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- <div class="modal-content viewmodal"> -->
<%--<div class="menubottom">--%>
<%--            <div class="modal-header">--%>
<%--                <div class="col-md-12">--%>
<%--                    <div class="row">--%>
<%--                        <div class="col-md-9 col-sm-12">--%>
<%--                            <h5 class="modal-title"><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_ADD_FORMNAME, loginDTO)%></h5>--%>
<%--                        </div>--%>
<%--                        <div class="col-md-3 col-sm-12">--%>
<%--                            <div class="row">--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>--%>
<%--                                </div>--%>
<%--                                <div class="col-md-6">--%>
<%--                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>


<%--            </div>--%>

<%--            <div class="modal-body container">--%>

<%--			<div class="row div_border office-div">--%>

<%--                    <div class="col-md-12">--%>
<%--                        <h3><%=LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_ADD_FORMNAME, loginDTO)%></h3>--%>

<%--                    </div>--%>


<%--			</div>--%>

<%--                <div class="row div_border attachement-div">--%>
<%--                    --%>
<%--                </div>--%>


<%--        </div>--%>
<%--	</div>--%>