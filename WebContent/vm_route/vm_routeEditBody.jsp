<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_route.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>

<%
    Vm_routeDTO vm_routeDTO;
    vm_routeDTO = (Vm_routeDTO) request.getAttribute("vm_routeDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (vm_routeDTO == null) {
        vm_routeDTO = new Vm_routeDTO();

    }
    System.out.println("vm_routeDTO = " + vm_routeDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_ROUTE_ADD_VM_ROUTE_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_routeServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    long ColumnID = -1;
    FilesDAO filesDAO = new FilesDAO();
    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_ROUTE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    int year = Calendar.getInstance().get(Calendar.YEAR);
%>
<%@include file="vm_routeEditForm.jsp" %>

<script type="text/javascript">


    function PreprocessBeforeSubmiting() {
        for (i = 1; i < child_table_extra_id; i++) {
            if (document.getElementById("startDate_date_" + i)) {
                preprocessDateBeforeSubmitting('startDate', i);
                // if (document.getElementById("startDate_date_" + i).getAttribute("processed") == null) {
                //
                //     document.getElementById("startDate_date_" + i).setAttribute("processed", "1");
                // }
            }

            // if(document.getElementById("endDate_date_" + i))
            // {
            // 	if(document.getElementById("endDate_date_" + i).getAttribute("processed") == null)
            // 	{
            // 		preprocessDateBeforeSubmitting('endDate', i);
            // 		document.getElementById("endDate_date_" + i).setAttribute("processed","1");
            // 	}
            // }
        }
        // return true;
    }


    function init(row) {
        if (actionName == 'edit') {
            loadVehicleList();
        }

        for (i = 1; i < child_table_extra_id; i++) {
            setDateByStringAndId('startDate_js_' + i, $('#startDate_date_' + i).val());
            // setDateByStringAndId('endDate_js_' + i, $('#endDate_date_' + i).val());
        }


    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }

    function formSubmitAfterValidation() {
        let form = $("#bigform");
        PreprocessBeforeSubmiting();
        <%--PreprocessBeforeSubmiting(0,'<%=actionName%>');--%>
        let url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            dataType: 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    buttonStateChange(false);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });
    }


    function vehicleAssignValidation() {
        let vehicleId = $("#givenVehicleId_select_0").val();
        let url = "Vm_routeServlet?actionType=vehicleValidation&vehicleId=" +
            vehicleId + "&type=" + actionName + "&iD=" + dtoId;

        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                if (fetchedData && fetchedData.flag == true) {
                    formSubmitAfterValidation();
                } else if (fetchedData && fetchedData.flag == false) {
                    let errMsg1 = "Selected vehicle already assigned to another route."
                    let errMsg1Bn = "নির্বাচিত গাড়ি ইতিমধ্যে অন্য রুটে নির্বাচিত রয়েছে";
                    let errMsg2 = "Do you want to assign it for this route?";
                    let errMsg2Bn = "আপনি কি এই রুটের জন্যে নির্বাচিত করতে চান?";
                    messageDialog(valueByLanguage(language, errMsg1Bn, errMsg1),
                        valueByLanguage(language, errMsg2Bn, errMsg2), 'warning', true, "Yes", "No",
                        formSubmitAfterValidation, h2);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function valueByLanguage(language, bnValue, enValue) {
        if (language == 'english' || language == 'English') {
            return enValue;
        } else {
            return bnValue;
        }
    }

    function h2() {
    }


    function formSubmit() {
        event.preventDefault();

        let form = $("#bigform");
        form.validate();
        let valid = form.valid();

        if (valid) {
            let vehicleId = $("#givenVehicleId_select_0").val();
            let url = "Vm_routeServlet?actionType=driverValidation&vehicleId=" + vehicleId;


            $.ajax({
                url: url,
                type: "GET",
                async: false,
                success: function (fetchedData) {
                    if (fetchedData && fetchedData.flag == true) {
                        vehicleAssignValidation();
                        // formSubmitAfterValidation();
                    } else if (fetchedData && fetchedData.flag == false) {
                        toastr.error(fetchedData.errMsg);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });


            // formSubmitAfterValidation();
        }


    }

    let row = 0;
    let actionName = '<%=actionName%>';
    let vehicleId = '<%=vm_routeDTO.vehicleId%>';
    let dtoId = '<%=vm_routeDTO.iD%>';
    let language = '<%=Language%>';
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });


        let errVehicleTypeCat;
        let errVehicle;
        let errBn;
        let errEn;
        let errStatus;

        if (language == 'english') {
            errVehicleTypeCat = 'Please select vehicle type';
            errVehicle = 'Please select vehicle';
            errBn = "Bangla name required";
            errEn = "English name required";
            errStatus = "Please select status";


        } else {
            errVehicleTypeCat = 'যানবাহনের ধরণ নির্বাচন করুন ';
            errVehicle = 'যানবাহন নির্বাচন করুন ';
            errBn = "বাংলা নাম লিখুন ";
            errEn = "ইংরেজি নাম লিখুন ";
            errStatus = "অবস্থা নির্বাচন করুন";

        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                vehicleTypeCat: {
                    required: true,
                    validSelector: true,
                },
                vehicleId: {
                    required: true,
                    validSelector: true,
                },
                status: {
                    required: true,
                    validSelector: true,
                },
                nameEn: {
                    required: true,
                },
                nameBn: {
                    required: true,
                },
            },
            messages: {
                vehicleTypeCat: errVehicleTypeCat,
                vehicleId: errVehicle,
                status: errStatus,
                nameBn: errBn,
                nameEn: errEn,

            }
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;

    $("#add-more-VmRouteStoppage").click(
        function (e) {
            e.preventDefault();
            var t = $("#template-VmRouteStoppage");

            $("#field-VmRouteStoppage").append(t.html());
            SetCheckBoxValues("field-VmRouteStoppage");

            var tr = $("#field-VmRouteStoppage").find("tr:last-child");

            tr.attr("id", "VmRouteStoppage_" + child_table_extra_id);

            tr.find("[tag='pb_html']").each(function (index) {
                var prev_id = $(this).attr('id');
                $(this).attr('id', prev_id + child_table_extra_id);
                console.log(index + ": " + $(this).attr('id'));
            });


            // child_table_extra_id ++;
            datesetter();

        });


    function datesetter() {
        console.log("hi");
        createDatePicker("startDate_div_" + child_table_extra_id, {
            'DATE_ID': 'startDate_js_' + child_table_extra_id,
            'LANGUAGE': '<%=Language%>',
            'END_YEAR': '<%=year + 5%>'
        }, childIncrementer());

    }

    function endDateSetter() {
        createDatePicker("endDate_div_" + child_table_extra_id, {
            'DATE_ID': 'endDate_js_' + child_table_extra_id,
            'LANGUAGE': '<%=Language%>',
            'END_YEAR': '2100'
        }, childIncrementer());
    }

    function childIncrementer() {
        console.log("incrementing child");
        child_table_extra_id++;
    }


    $("#remove-VmRouteStoppage").click(function (e) {
        var tablename = 'field-VmRouteStoppage';
        var i = 0;
        console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
        var element = document.getElementById(tablename);

        var j = 0;
        for (i = document.getElementById(tablename).childNodes.length - 1; i >= 0; i--) {
            var tr = document.getElementById(tablename).childNodes[i];
            if (tr.nodeType === Node.ELEMENT_NODE) {
                console.log("tr.childNodes.length= " + tr.childNodes.length);
                var checkbox = tr.querySelector('input[type="checkbox"]');
                if (checkbox.checked == true) {
                    tr.remove();
                }
                j++;
            }

        }
    });


    function loadVehicleList() {

        let typeValue = document.getElementById('vehicleTypeCat_category_0').value;
        let url = "Vm_vehicleServlet?actionType=getByVehicleTypeForAssignment&type=" + typeValue;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                // const response = JSON.parse(fetchedData);
                $("#givenVehicleId_select_0").html(fetchedData);
                if (actionName == 'edit') {
                    $("#givenVehicleId_select_0").val(vehicleId);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });

    }


</script>






