
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="util.ActionTypeConstant" %>
<%@ page import="employee_office_report.InChargeLevelEnum" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="pb.Utils" %>
<%@ page import="java.util.Calendar" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%
String url = "Procurement_goodsServlet?actionType=report";
String navigator = SessionConstants.NAV_PROCUREMENT_GOODS;
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

String pageno = "";

RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
boolean isPermanentTable = rn.m_isPermanentTable;

System.out.println("rn " + rn);

String action = url;
String context = "../../.." + request.getContextPath() + "/";
String link = context + url;
String concat = "?";
if (url.contains("?")) {
	concat = "&";
}
int pagination_number = 0;


    String dropDown = "<select class='form-control' onchange='goodsSearch()' name='procurementYear' id='procurementYear' tag='pb_html'>x</select>";
    String option = "<option value='xe'>xb</option>";
    String options = "";
    for (int i =-4; i<5; i++) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, i);
        Date today = c.getTime();
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
        String todayString = sf.format(today);
        int currentYear = Integer.parseInt(todayString.substring(6));
        int previousYear = currentYear-1;
        int nextYear = currentYear+1;

        String firstYear = "";
        String secondYear = "";

        int month = Integer.parseInt(todayString.substring(3,5));
        firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
        secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);

        String reportYear = firstYear + "-" + secondYear;
        String reportYearByLanguage = Utils.getDigits(reportYear, Language);

        String optionReplaced =  option.replaceAll("xe", reportYear);
        optionReplaced =  optionReplaced.replaceAll("xb", reportYearByLanguage);
        if (i==0)optionReplaced = optionReplaced.replaceAll("<option", "<option selected");
        options += optionReplaced;
    }

    dropDown = dropDown.replaceAll("x", options);

%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->




<!--- begin search -->



<div class="kt-portlet shadow-none">
    <div class="kt-portlet__body">
        <form>
            <table class="" style="width: 100%">
                <tr>
                    <td style="width:8%"><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_REPORT_NAMEEN, loginDTO)%></b></td>
                    <td style="width:20%">
                        <input type='text' class='form-control goodsSearch'  name='reportName' id = 'reportName' value=''   tag='pb_html'/>
                    </td>

                    <td style="width:20%"></td>
                    <td style="width:5%"><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_REPORT_YEAR, loginDTO)%></b></td>
                    <td style="width:15%">
                        <%=dropDown%>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>


<!--- end search -->




<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">


            <jsp:include page="./procurement_goodsNavReport.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <div class="caption">
                        <i class="icon-settings"></i> <span id = "report-name"
                                                            class="caption-subject bold uppercase">
								<%=LM.getText(LC.PROCUREMENT_REPORT_PROCUREMENT_REPORT, loginDTO)%>
								</span>
                    </div>
                    <form action="Procurement_goodsServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                          method="POST"
                          id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="procurement_goodsReportForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="dummy"/>

<%--<% pagination_number = 1;%>--%>
<%--<%@include file="../common/pagination_with_go2.jsp"%>--%>
<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	initDeleteCheckBoxes();
	dateTimeInit("<%=Language%>");
});

</script>





<script src="<%=context%>/assets/report/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/dataTables.buttons.min.js" type="text/javascript"></script>


<script src="<%=context%>/assets/report/dataTables.buttons.min.js" type="text/javascript"></script>


<script src="<%=context%>/assets/report/jszip.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/pdfmake.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/vfs_fonts.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/buttons.html5.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/buttons.print.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/dataTables.bootstrap.min.js" type="text/javascript"></script>


<script src="<%=context%>/assets/report/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<%=context%>/assets/pages/scripts/table-datatables-scroller.min.js" type="text/javascript"></script>

<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>--%>
<%--<script src="<%=context%>/assets/report/pdfmake.min.js" type="text/javascript"></script>--%>
<%--<script src="<%=context%>/assets/report/vfs_fonts.js" type="text/javascript"></script>--%>
<%--<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>--%>
<%--<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>--%>
<%--<script src="<%=context%>/assets/report/dataTables.bootstrap.min.js" type="text/javascript"></script>--%>


<script type="text/javascript">



    $('.goodsSearch').keyup(function ()
        {
            var reportNameValue = $('#reportName').val();
            var procurementYearValue = $('#procurementYear').val();

            // var filter = 'report_name = \'' + reportNameValue + '\' AND ' + 'procurement_year = \'' + procurementYearValue + '\'';

            var formData = new FormData();
            formData.append('report_name_ajax', reportNameValue);
            formData.append('procurement_year_ajax', procurementYearValue);

            var params = "Procurement_goodsServlet?actionType=getReport";

            params +=  '&filter=1&search=true&ajax=true';
            var extraParams = document.getElementsByName('extraParam');
            extraParams.forEach((param) => {
                params += "&" + param.getAttribute("tag") + "=" + param.value;
            })

            // var pageNo = document.getElementsByName('pageno')[0].value;
            // var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

            var pageNo = 0;
            var rpp = 10000;

            var totalRecords = 0;
            var lastSearchTime = 0;
            if(document.getElementById('hidden_totalrecords'))
            {
                totalRecords = document.getElementById('hidden_totalrecords').value;
                lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
            }


            params += '&pageno=' + pageNo;
            params += '&RECORDS_PER_PAGE=' + rpp;
            params += '&TotalRecords=' + totalRecords;
            params += '&lastSearchTime=' + lastSearchTime;

            if (reportNameValue.toString().trim().length > 0 || procurementYearValue.toString().trim().length > 0) {

                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {

                        // setPageNo();
                        // searchChanged = 0;

                        //
                        $("#dummy").html(this.responseText);
                        $("#tbodyData").html($("#tbodyDataDummy").html());



                    } else if (this.readyState === 4 && this.status !== 200) {
                        alert('failed ' + this.status);
                    }
                };
                xhttp.open("POST", params, true);
                xhttp.send(formData);
            }

        }
    );

    function goodsSearch()
    {
        var reportNameValue = $('#reportName').val();
        var procurementYearValue = $('#procurementYear').val();

        // var filter = 'report_name = \'' + reportNameValue + '\' AND ' + 'procurement_year = \'' + procurementYearValue + '\'';

        var formData = new FormData();
        formData.append('report_name_ajax', reportNameValue);
        formData.append('procurement_year_ajax', procurementYearValue);

        var params = "Procurement_goodsServlet?actionType=getReport";

        params +=  '&filter=1&search=true&ajax=true';
        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        // var pageNo = document.getElementsByName('pageno')[0].value;
        // var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var pageNo = 0;
        var rpp = 10000;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if(document.getElementById('hidden_totalrecords'))
        {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        if (reportNameValue.toString().trim().length > 0 || procurementYearValue.toString().trim().length > 0) {

            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {

                    $('#tableForm').html(this.responseText);

                } else if (this.readyState === 4 && this.status !== 200) {
                    alert('failed ' + this.status);
                }
            };
            xhttp.open("POST", params, true);
            xhttp.send(formData);
        }

    }




    var columnDataFromater = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    /*data.replace( /[$,.]/g, '' )*/
                    return data.replace(/(&nbsp;|<([^>]+)>)/ig, "");/*remove html tag from data when exporting*/
                }
            }
        }
    };

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;

    var reportTable = $('#tableData');

    var reportDataTable = reportTable.DataTable(
        {
            'searching': false,
            'destroy': true,
            'ordering': false,
            'scrollX': true,
            'scrollY': true,
            'paging': false,
            'pagingType': 'simple',
            'dom': 'Bfrtip',
            'buttons': [
                $.extend(true, {}, columnDataFromater,
                    {
                        className: 'export-btn',
                        extend: 'copyHtml5',
                        footer: true,
                    }),
                $.extend(true, {}, columnDataFromater,
                    {
                        className: 'export-btn',
                        extend: 'excelHtml5',
                        footer: true,
                        customize: function( xlsx ) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            $('c[r^="A"]', sheet).attr( 's', '2' );
                            $('c[r^="B"]', sheet).attr( 's', '2' );
                            $('c[r^="C"]', sheet).attr( 's', '2' );
                            // $('c[r^="A1"]', sheet).text('');

                            // var row = 3;
                            // var num_columns = 19;
                            //
                            // $('row', sheet).each(function(x) {
                            //
                            // 		// console.log("");
                            // 		// console.log("---- row x: " + x);
                            //
                            // 		for(var i=0; i<num_columns; i++) {
                            //
                            // 			// console.log(output_table.row(':eq('+x+')').data());
                            //
                            // 			if ($(output_table.cell(':eq('+x+')', i).node()).hasClass('boldType')) {
                            // 				// console.log('YES - sig-w - row ' + x + ', column ' + i);
                            // 				$('row:nth-child('+(row)+') c', sheet).eq(i).attr('s', '2');
                            // 				break;
                            // 			}
                            //
                            // 		}
                            //
                            // 		row++;
                            //
                            // });


                            $('row:nth-child(2) c', sheet).attr('s', '7');

                        }
                    }),
                $.extend(true, {}, columnDataFromater,
                    {
                        className: 'export-btn',
                        extend: 'csvHtml5',
                        footer: true,
                        charset: 'UTF-8',
                        bom: true
                    }),
                $.extend(true, {}, columnDataFromater,
                    {
                        className: 'export-btn',
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'A4',
                        footer: true,
                        charset: 'UTF-8',
                        bom: true,
                        // customize: function (doc) {
                        // 	doc.defaultStyle.font = 'SolaimanLipi';
                        // }
                    }),
                {
                    className: 'export-btn',
                    extend: 'print',
                    text: 'Print',
                    orientation: 'landscape',
                    pageSize: 'A0',
                    title: $("#report-name").html() + ', ' + today,
                    autoPrint: true,
                    footer: true,
                    header: true,
                }
            ],
            columnDefs: [
                {
                    targets: [0,1,2],
                    className: 'bolded'
                }
            ]

        });





</script>
