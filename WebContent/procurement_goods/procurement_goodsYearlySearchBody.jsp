
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="pb.Utils" %>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%

    String url = "Procurement_goodsServlet?actionType=search";
String navigator = SessionConstants.NAV_PROCUREMENT_GOODS;
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

String pageno = "";

RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
boolean isPermanentTable = rn.m_isPermanentTable;

System.out.println("rn " + rn);

String action = url;
String context = "../../.." + request.getContextPath() + "/";
String link = context + url;
String concat = "?";
if (url.contains("?")) {
	concat = "&";
}
int pagination_number = 0;


String dropDown = "<select class='form-control' onchange='goodsSearch()' name='procurementYear' id='procurementYear' tag='pb_html'>x</select>";
String option = "<option value='xe'>xb</option>";
String options = "";

    Date today = new Date();
    SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
    String todayString = sf.format(today);
    int currentYear = Integer.parseInt(todayString.substring(6));
    int previousYear = currentYear-1;
    int nextYear = currentYear+1;

    String firstYear = "";
    String secondYear = "";

    int month = Integer.parseInt(todayString.substring(3,5));
    firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
    secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);

    String currentReportYear = firstYear + "-" + secondYear;


    for (int i =-4; i<5; i++) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, i);
        today = c.getTime();
        sf = new SimpleDateFormat("dd/MM/yyyy");
        todayString = sf.format(today);
        currentYear = Integer.parseInt(todayString.substring(6));
        previousYear = currentYear-1;
        nextYear = currentYear+1;

        firstYear = "";
        secondYear = "";

        month = Integer.parseInt(todayString.substring(3,5));
        firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
        secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);

        String reportYear = firstYear + "-" + secondYear;
        String reportYearByLanguage = Utils.getDigits(reportYear, Language);

        String optionReplaced =  option.replaceAll("xe", reportYear);
        optionReplaced =  optionReplaced.replaceAll("xb", reportYearByLanguage);
        if (reportYear.equals(currentReportYear))optionReplaced = optionReplaced.replaceAll("<option", "<option selected");
        options += optionReplaced;
    }

    dropDown = dropDown.replaceAll("x", options);




%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="./procurement_goodsNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">

                    <table class="mb-3" style="width: 100%">
                        <tr>
                            <td style="width:8%"><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_REPORT_NAMEEN, loginDTO)%></b></td>
                            <td style="width:20%">
                                <input type='text' class='form-control goodsSearch'  name='reportName' id = 'reportName' value=''   tag='pb_html'/>
                            </td>

                            <td style="width:20%"></td>
                            <td style="width:5%"><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_REPORT_YEAR, loginDTO)%></b></td>
                            <td style="width:15%">
                                    <%=dropDown%>
                        </tr>
                    </table>

                    <form action="Procurement_goodsServlet?isPermanentTable=<%=isPermanentTable%>&actionType=addAll"
                          method="POST"
                          id="tableForm" enctype="multipart/form-data">

                        <jsp:include page="procurement_goodsYearlySearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
                    <div class="form-actions text-center mb-5">
                        <%--                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_CANCEL_BUTTON, loginDTO)%></a>--%>
                        <button class="btn btn-sm shadow btn-border-radius btn-success" onclick="goodsSearchByButton('')"><%=LM.getText(LC.HM_ADD, loginDTO)%></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<% pagination_number = 1;%>
<%@include file="../common/pagination_with_go2.jsp"%>
<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	initDeleteCheckBoxesForProcurementGoods();
	dateTimeInit("<%=Language%>");

    goodsSearch();
});

function submitGoods() {
    event.preventDefault();

    $('#tableForm').append('<input type=\'text\' style=\'display: none\' name=\'reportName\' id="reportNameHidden" value=\'\'/>');
    $('#tableForm').append('<input type=\'text\' style=\'display: none\' name=\'procurementYear\' id="procurementYearHidden" value=\'\'/>');

    var procurementYearHidden = $('#procurementYear').val();
    var reportNameHidden = $('#reportName').val();
    $('#procurementYearHidden').val(procurementYearHidden);
    $('#reportNameHidden').val(reportNameHidden);

    $('#tableForm').submit();
}

function initDeleteCheckBoxesForProcurementGoods()
{
    $('#tableForm').submit(function(e) {
        var currentForm = this;
        var selected=false;
        e.preventDefault();
        var set = $('#tableData').find('input[name="ID"]');
        $(set).each(function() {
            if($(this).prop('checked')){
                selected=true;
            }
        });
        // if(!selected){
        //     bootbox.confirm("Select rows to delete!", function(result) { });
        // }else{
        //     bootbox.confirm("Are you sure you want to delete the record(s)?", function(result) {
        //         if (result) {
        //             currentForm.submit();
        //         }
        //     });
        // }
        currentForm.submit();
    });


    $(document).on( "click",'.chkEdit',function(){

        $(this).toggleClass("checked");
    });
}


</script>


