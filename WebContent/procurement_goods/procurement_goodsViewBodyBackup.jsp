<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="procurement_goods.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="procurement_package.*" %>


<%
    String servletName = "Procurement_goodsServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
    Procurement_goodsDTO procurement_goodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PROCUREMENT_GOODS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%>
            </h5>
            <div>
                <table class="table table-bordered table-striped">

                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_ADD_FORMNAME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                Procurement_packageDAO procurement_packageDAO = Procurement_packageDAO.getInstance();
                                ProcurementGoodsTypeDAO procurementGoodsTypeDAO = ProcurementGoodsTypeDAO.getInstance();
                                ProcurementGoodsTypeDTO procurementGoodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(procurement_goodsDTO.procurementGoodsTypeId);
                                Procurement_packageDTO procurement_packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(procurementGoodsTypeDTO.procurementPackageId);
                                value = Procurement_packageRepository.getInstance().getName(Language, procurement_packageDTO.iD) + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = ProcurementGoodsTypeRepository.getInstance().getName(Language, procurementGoodsTypeDTO.iD) + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <%--							<tr>--%>
                    <%--								<td ><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEEN, loginDTO)%></b></td>--%>
                    <%--								<td>--%>
                    <%--						--%>
                    <%--											<%--%>
                    <%--											value = procurement_goodsDTO.nameEn + "";--%>
                    <%--											%>--%>
                    <%--														--%>
                    <%--											<%=Utils.getDigits(value, Language)%>--%>
                    <%--				--%>
                    <%--			--%>
                    <%--								</td>--%>
                    <%--						--%>
                    <%--							</tr>--%>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEBN, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.nameBn + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <%--							<tr>--%>
                    <%--								<td ><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PROCUREMENTGOODSTYPEID, loginDTO)%></b></td>--%>
                    <%--								<td>--%>
                    <%--						--%>
                    <%--											<%--%>
                    <%--											value = procurement_goodsDTO.procurementGoodsTypeId + "";--%>
                    <%--											%>--%>
                    <%--														--%>
                    <%--											<%=Utils.getDigits(value, Language)%>--%>
                    <%--				--%>
                    <%--			--%>
                    <%--								</td>--%>
                    <%--						--%>
                    <%--							</tr>--%>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_QUANTITY, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.quantity + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_UNITPRICE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.unitPrice + "";
                            %>
                            <%
                                value = String.format("%.1f", procurement_goodsDTO.unitPrice);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_ESTCOST, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.estCost + "";
                            %>
                            <%
                                value = String.format("%.1f", procurement_goodsDTO.estCost);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_METHODANDTYPECAT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.methodAndTypeCat + "";
                            %>
                            <%
                                value = CatRepository.getName(Language, "method_and_type", procurement_goodsDTO.methodAndTypeCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_APPROVINGAUTHORITY, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.approvingAuthority + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_SOURCEOFFUNDCAT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.sourceOfFundCat + "";
                            %>
                            <%
                                value = CatRepository.getName(Language, "source_of_fund", procurement_goodsDTO.sourceOfFundCat);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TIMECODE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.timeCode + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDER, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.tender + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDEROPENING, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.tenderOpening + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDEREVALUATION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.tenderEvaluation + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_APPROVALTOWARD, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.approvalToward + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_AWARDNOTIFICATION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.awardNotification + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_SIGININGOFCONTRACT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.siginingOfContract + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_CONTRACTSIGNATURETIME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.contractSignatureTime + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_CONTRACTCOMPLETIONTIME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.contractCompletionTime + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>
        </div>
    </div>
</div>