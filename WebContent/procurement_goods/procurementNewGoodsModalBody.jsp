<%@ page import="util.UtilCharacter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<style>
    .hiddenTr {
        display: none;
    }
</style>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%>
                </label>
                <div class="col-md-8">
                    <select class='form-control' name='procurementPackageId'
                            id='procurementPackageId_<%=i%>' tag='pb_html'>
                        <%=packageOptions%>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                </label>
                <div class="col-md-8">
                    <select class='form-control' name='procurementGoodsTypeId'
                            id='procurementGoodsTypeId_<%=i%>' tag='pb_html'>
                    </select>
                </div>
            </div>
            <%--            <div class="form-group row">--%>
            <%--                <label class="col-md-4 col-form-label text-md-right">--%>
            <%--                    <%=UtilCharacter.getDataByLanguage(Language, "অথবা", "Or")%>--%>
            <%--                </label>--%>
            <%--            </div>--%>
            <%--            <div class="form-group row">--%>
            <%--                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>--%>
            <%--                </label>--%>
            <%--                <div class="col-md-8">--%>
            <%--                    <select class='form-control' name='procurementGoodsId'--%>
            <%--                            id='procurementGoodsId'--%>
            <%--                            onchange="onProcurementGoodsChange(this)" tag='pb_html'>--%>
            <%--                        <%= Procurement_goodsRepository.getInstance().buildOptions(Language, -1L) %>--%>
            <%--                    </select>--%>
            <%--                    <input type="hidden" id="parent_id_hidden" value="">--%>
            <%--                </div>--%>
            <%--            </div>--%>
            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title">
                        <%=UtilCharacter.getDataByLanguage(Language, "আইটেম লিস্ট ", "Item List")%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="modal-product-view-table">
                            <thead>
                            <tr>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "আইটেম", "Item")%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "সাব টাইপ", "Sub Types")%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "ইউনিট", "Unit")%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "বিবরণ", "Description")%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "সরান", "Remove")%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="modalAppendTbody">

                            </tbody>
                            <tr class="hiddenTr template-item-list-of-group-type-modal">
                                <input type='hidden' class='form-control groupTypeIdCls'/>
                                <input type='hidden' class='form-control itemTypeIdCls'/>
                                <input type='hidden' class='form-control itemIdCls'/>
                                <input type='hidden' class='form-control unitIdCls'/>
                                <td class="itemTdCls">
                                </td>
                                <td class="subTypeTdCls">
                                </td>
                                <td class="unitTdCls">
                                </td>
                                <td class="descriptionTdCls">
                                </td>
                                <td class="isSelectedTdCls">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>