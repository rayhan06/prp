
<%@page import="procurement_goods.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="procurement_package.ProcurementGoodsTypeRepository" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>

<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_PROCUREMENT_GOODS;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Procurement_goodsDTO procurement_goodsDTO = (Procurement_goodsDTO)request.getAttribute("procurement_goodsDTO");
CommonDTO commonDTO = procurement_goodsDTO;
String servletName = "Procurement_goodsServlet";


System.out.println("procurement_goodsDTO = " + procurement_goodsDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Procurement_goodsDAO procurement_goodsDAO = (Procurement_goodsDAO)request.getAttribute("procurement_goodsDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


											<td>

													<%=Utils.getDigits(i + 1, Language)%>


												</td>


											<td id = '<%=i%>_procurementPackageId'>
													<%
														Options = Procurement_packageRepository.getInstance().getName(Language, procurement_goodsDTO.procurementPackageId);
													%>
													<%=Options%>


											</td>

											<td id = '<%=i%>_procurementGoodsTypeId'>
													<%
														Options = ProcurementGoodsTypeRepository.getInstance().getName(Language, procurement_goodsDTO.procurementGoodsTypeId);
													%>
													<%=Options%>


											</td>


											<td id = '<%=i%>_subType'>

												<%=request.getParameter("subType")%>


											</td>


											<td id = '<%=i%>_nameEn'>
												<%=procurement_goodsDTO.nameEn%>
											</td>

											<td id = '<%=i%>_nameBn'>
												<%=procurement_goodsDTO.nameBn%>
											</td>
		
		
		
		
		
		
		
	

											<td>
												<button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
														onclick="event.preventDefault();location.href='Procurement_goodsServlet?actionType=view&ID=<%=procurement_goodsDTO.iD%>'">
													<i class="fa fa-eye"></i>
												</button>
											</td>
	
											<td id = '<%=i%>_Edit'>																																	

												<button
														class="btn-sm border-0 shadow btn-border-radius text-white"
														style="background-color: #ff6b6b;"
														onclick="event.preventDefault();location.href='Procurement_goodsServlet?actionType=getEditPage&ID=<%=procurement_goodsDTO.iD%>'">
													<i class="fa fa-edit"></i>
												</button>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=procurement_goodsDTO.iD%>'/></span>
												</div>
											</td>



