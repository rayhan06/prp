<%@ page import="procurement_package.*" %>
<%@ page import="pb.CommonDAO" %>
<%@ page import="procurement_goods.Procurement_goodsRepository" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">

            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%>
                </label>
                <div class="col-md-4">
                    <select class='form-control' name='procurementPackageId'
                            id='procurementPackageId_<%=i%>' tag='pb_html'>
                        <%=packageOptions%>
                    </select>

                </div>
<%--                <div class="col-md-4">--%>

<%--                    <select class='form-control' name='procurementGoodsId'--%>
<%--                            id='procurementGoodsId'--%>
<%--                            onchange="onProcurementGoodsChange(this)" tag='pb_html'>--%>
<%--                        <%= Procurement_goodsRepository.getInstance().buildOptions(Language, -1L) %>--%>
<%--                    </select>--%>
<%--                    <input type="hidden" id="parent_id_hidden" value="">--%>

<%--                </div>--%>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                </label>
                <div class="col-md-8">
                    <select class='form-control' name='procurementGoodsTypeId'
                            id='procurementGoodsTypeId_<%=i%>' tag='pb_html'>
                    </select>

                </div>
            </div>



            <div id="subType">
            </div>


        </div>
    </div>
</div>