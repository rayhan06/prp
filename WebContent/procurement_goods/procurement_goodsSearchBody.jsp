
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="pb.Utils" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%-- <%@ page errorPage="failure.jsp"%> --%>
<%

    String url = "Procurement_goodsServlet?actionType=search";
String navigator = SessionConstants.NAV_PROCUREMENT_GOODS;
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.APPOINTMENT_EDIT_LANGUAGE, loginDTO);

String pageno = "";

String reportName = (String) request.getSession(true).getAttribute("report_name");
RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
boolean isPermanentTable = rn.m_isPermanentTable;

System.out.println("rn " + rn);

String action = url;
String context = "../../.." + request.getContextPath() + "/";
String link = context + url;
String concat = "?";
if (url.contains("?")) {
	concat = "&";
}
int pagination_number = 0;


String dropDown = "<select class='form-control' onchange='goodsSearch()' name='procurementYear' id='procurementYear' tag='pb_html'>x</select>";
String option = "<option value='xe'>xb</option>";
String options = "";



    Date today = new Date();
    SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
    String todayString = sf.format(today);
    int currentYear = Integer.parseInt(todayString.substring(6));
    int previousYear = currentYear-1;
    int nextYear = currentYear+1;

    String firstYear = "";
    String secondYear = "";

    int month = Integer.parseInt(todayString.substring(3,5));
    firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
    secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);

    String currentReportYear = firstYear + "-" + secondYear;


    for (int i =-4; i<5; i++) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, i);
        today = c.getTime();
        sf = new SimpleDateFormat("dd/MM/yyyy");
        todayString = sf.format(today);
        currentYear = Integer.parseInt(todayString.substring(6));
        previousYear = currentYear-1;
        nextYear = currentYear+1;

        firstYear = "";
        secondYear = "";

        month = Integer.parseInt(todayString.substring(3,5));
        firstYear = month > 6 ? String.valueOf(currentYear) : String.valueOf(previousYear);
        secondYear = month > 6 ? String.valueOf(nextYear).substring(2) : String.valueOf(currentYear).substring(2);

        String reportYear = firstYear + "-" + secondYear;
        String reportYearByLanguage = Utils.getDigits(reportYear, Language);

        String optionReplaced =  option.replaceAll("xe", reportYear);
        optionReplaced =  optionReplaced.replaceAll("xb", reportYearByLanguage);
        if (reportYear.equals(currentReportYear))optionReplaced = optionReplaced.replaceAll("<option", "<option selected");
        options += optionReplaced;
    }

    dropDown = dropDown.replaceAll("x", options);




%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title">
            &nbsp; <%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->


<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="./procurement_goodsNav.jsp" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value="<%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>"/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">

<%--                    <table class="" style="width: 100%">--%>
<%--                        <tr>--%>
<%--                            <td style="width:8%"><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_REPORT_NAMEEN, loginDTO)%></b></td>--%>
<%--                            <td style="width:20%">--%>
<%--                                <input type='text' class='form-control goodsSearch noDollar'  name='reportName' id = 'reportName' value='<%=reportName%>'   tag='pb_html'/>--%>
<%--                                <label class="error-msg" id="reportNameErr" style="color: red;display: none"></label>--%>
<%--                            </td>--%>

<%--                            <td style="width:20%"></td>--%>
<%--                            <td style="width:5%"><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_REPORT_YEAR, loginDTO)%></b></td>--%>
<%--                            <td style="width:15%">--%>
<%--                                    <%=dropDown%>--%>
<%--                        </tr>--%>
<%--                    </table>--%>

                    <form action="Procurement_goodsServlet?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                    method="POST"
                          id="tableForm" enctype="multipart/form-data">

                        <jsp:include page="procurement_goodsSearchForm.jsp" flush="true">
                            <jsp:param name="pageName"
                                       value="<%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, loginDTO)%>"/>
                        </jsp:include>
                    </form>
<%--                    <div class="form-actions text-center mb-5">--%>
<%--&lt;%&ndash;                        <a class="btn btn-danger" href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_CANCEL_BUTTON, loginDTO)%></a>&ndash;%&gt;--%>
<%--                        <button class="btn btn-sm shadow btn-border-radius btn-success" onclick="submitGoods()"><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_SUBMIT_BUTTON, loginDTO)%></button>--%>
<%--                    </div>--%>
                </div>
            </div>
        </div>
    </div>
</div>

<% pagination_number = 1;%>
<%@include file="../common/pagination_with_go2.jsp"%>
<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">

    let requiredMsg, duplicateMsg;

$(document).ready(function(){
    select2SingleSelector('#package_id', '<%=Language%>');
    select2SingleSelector('#type_id', '<%=Language%>');

    initDeleteCheckBoxes();
	dateTimeInit("<%=Language%>");

    let lang = '<%=Language%>';

    if(lang == 'English'){
        requiredMsg = 'This is required';
        duplicateMsg = 'This is duplicated';

    }
    else {
        requiredMsg = '<%="এই তথ্যটি আবশ্যক"%>';
        duplicateMsg = '<%="এই নামটি পূর্বে ব্যবহৃত হয়েছে"%>';

    }

    // goodsSearch();

});


    function processResponse(data){

        if (data.includes('Invalid Input')) {
            var alertMsg = "<%=Language.equals("English") ? "Sorry, missing required field!" : "দুঃখিত, আবশ্যকীয় তথ্য প্রদান করুন "%>";
            bootbox.alert(alertMsg
                , function (result) {
                });
        }
        else {
            window.location = 'Procurement_goodsServlet?actionType=search&filter=1&report_name_ajax=' + data;
        }
    }

    function ajaxPost(url, accept, send, postData, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: accept,
            contentType: send,
            data: postData,
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


// function submitGoods() {
//     event.preventDefault();
//
//     $('#tableForm').append('<input type=\'text\' style=\'display: none\' name=\'reportName\' id="reportNameHidden" value=\'\'/>');
//     $('#tableForm').append('<input type=\'text\' style=\'display: none\' name=\'procurementYear\' id="procurementYearHidden" value=\'\'/>');
//
//     var procurementYearHidden = $('#procurementYear').val();
//     var reportNameHidden = $('#reportName').val();
//     $('#procurementYearHidden').val(procurementYearHidden);
//     $('#reportNameHidden').val(reportNameHidden);
//
//     // $('#tableForm').submit();
//     resetErrorMsg();
//     let valid = validateSelector() && validateString() && noDuplicate();
//
//     if (valid) {
//         var form=$("#tableForm");
//
//         var actionUrl =  form.attr("action");
//         var postData =  (form.serialize());
//
//         ajaxPost(actionUrl, "json", "application/x-www-form-urlencoded", postData, processResponse, processResponse);
//     }
//
// }

// function initDeleteCheckBoxesForProcurementGoods()
// {
//     $('#tableForm').submit(function(e) {
//         var currentForm = this;
//         var selected=false;
//         e.preventDefault();
//         var set = $('#tableData').find('input[name="ID"]');
//         $(set).each(function() {
//             if($(this).prop('checked')){
//                 selected=true;
//             }
//         });
//         // if(!selected){
//         //     bootbox.confirm("Select rows to delete!", function(result) { });
//         // }else{
//         //     bootbox.confirm("Are you sure you want to delete the record(s)?", function(result) {
//         //         if (result) {
//         //             currentForm.submit();
//         //         }
//         //     });
//         // }
//         currentForm.submit();
//     });
//
//
//     $(document).on( "click",'.chkEdit',function(){
//
//         $(this).toggleClass("checked");
//     });
// }


    $(".noDollar").keypress(function (evt) {
        let keycode = evt.charCode || evt.keyCode;
        if (keycode  == 36) {
            return false;
        }
    });

</script>


