<%@page pageEncoding="UTF-8" %>

<%@page import="procurement_goods.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="com.sun.org.apache.xpath.internal.operations.Bool" %>
<%@ page import="procurement_package.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_PROCUREMENT_GOODS;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Procurement_goodsDTO procurement_goodsDTO = (Procurement_goodsDTO)request.getAttribute("procurement_goodsDTO");
CommonDTO commonDTO = procurement_goodsDTO;
String servletName = "Procurement_goodsServlet";


System.out.println("procurement_goodsDTO = " + procurement_goodsDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
//out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Procurement_goodsDAO procurement_goodsDAO = (Procurement_goodsDAO)request.getAttribute("procurement_goodsDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

int packageIdCount = Integer.parseInt(request.getParameter("packageIdCount"));
int packageNum = Integer.parseInt(request.getParameter("packageNum"));
int itemNum = Integer.parseInt(request.getParameter("itemNum"));
Boolean newPackage = Boolean.parseBoolean(request.getParameter("newPackage"));
Boolean newGoodsType = Boolean.parseBoolean(request.getParameter("newGoodsType"));


%>

					<%
						if (newPackage) {
					%>

					<td rowspan="<%=packageIdCount%>"><b><%=packageNum%></b></td>

											<td id = '<%=i%>_procurementPackageId' rowspan="<%=packageIdCount%>">
	<%
//		Procurement_packageDAO procurement_packageDAO = Procurement_packageDAO.getInstance();
//		Procurement_packageDTO procurement_packageDTO = procurement_packageDAO.getInstance().getProcurement_goodsDTOByID(procurement_goodsDTO.procurementPackageId);
		value = Procurement_packageRepository.getInstance().getName(Language, procurement_goodsDTO.procurementPackageId) + "";
	%>

	<%=Utils.getDigits(value, Language)%>


</td>

					<%
						}
						else {
					%>

					<td style="display: none"></td>
					<td style="display: none"></td>

					<%
						}

						if (newGoodsType) {

					%>


	<td colspan="5" class="boldType"><b>
		<%
			//		ProcurementGoodsTypeDAO procurementGoodsTypeDAO = ProcurementGoodsTypeDAO.getInstance();
//		ProcurementGoodsTypeDTO procurementGoodsTypeDTO = procurementGoodsTypeDAO.getInstance().getProcurement_goodsDTOByID(procurement_goodsDTO.procurementGoodsTypeId);
			value = ProcurementGoodsTypeRepository.getInstance().getName(Language, procurement_goodsDTO.procurementGoodsTypeId) + "";
		%>

		<%=Utils.getDigits(value, Language)%>

	</b>
	</td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
<%--	<td></td>--%>
	<td style="display: none"></td>
	<td style="display: none"></td>
	<td style="display: none"></td>
	<td style="display: none"></td>

<%
	}

						else {
%>





<%--											<td id = '<%=i%>_procurementGoodsTypeId'>--%>
<%--	<%--%>
<%--//		ProcurementGoodsTypeDAO procurementGoodsTypeDAO = ProcurementGoodsTypeDAO.getInstance();--%>
<%--//		ProcurementGoodsTypeDTO procurementGoodsTypeDTO = procurementGoodsTypeDAO.getInstance().getProcurement_goodsDTOByID(procurement_goodsDTO.procurementGoodsTypeId);--%>
<%--		value = ProcurementGoodsTypeRepository.getInstance().getName(Language, procurement_goodsDTO.procurementGoodsTypeId) + "";--%>
<%--	%>--%>

<%--	<%=Utils.getDigits(value, Language)%>--%>


<%--</td>--%>
		
<%--											<td id = '<%=i%>_nameEn'>--%>
<%--											<%--%>
<%--											value = procurement_goodsDTO.nameEn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
		
											<td>
											<%
											value = itemNum + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>

											<td id = '<%=i%>_nameBn'>
											<%
											value = procurement_goodsDTO.nameBn + "";
											%>

											<%=Utils.getDigits(value, Language)%>


											</td>

                                            <td id = '<%=i%>_quantity' style="text-align: right">
                                                <%
                                                    value = procurement_goodsDTO.quantity + "";
                                                %>

                                                <%=Utils.getDigits(value, Language)%>


                                            </td>

											<td id = '<%=i%>_unitPrice' style="text-align: right">
											<%
											value = procurement_goodsDTO.unitPrice + "";
											%>
											<%
											value = String.format("%.1f", procurement_goodsDTO.unitPrice);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_estCost' style="text-align: right">
											<%
											value = procurement_goodsDTO.estCost + "";
											%>
											<%
											value = String.format("%.1f", procurement_goodsDTO.estCost);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_methodAndTypeCat'>
											<%
											value = procurement_goodsDTO.methodAndTypeCat + "";
											%>
											<%
											value = CatRepository.getName(Language, "method_and_type", procurement_goodsDTO.methodAndTypeCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_approvingAuthority'>
											<%
											value = procurement_goodsDTO.approvingAuthority + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_sourceOfFundCat'>
											<%
											value = procurement_goodsDTO.sourceOfFundCat + "";
											%>
											<%
											value = CatRepository.getName(Language, "source_of_fund", procurement_goodsDTO.sourceOfFundCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_timeCode'>
											<%
											value = procurement_goodsDTO.timeCode + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_tender'>
											<%
											value = procurement_goodsDTO.tender + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_tenderOpening'>
											<%
											value = procurement_goodsDTO.tenderOpening + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_tenderEvaluation'>
											<%
											value = procurement_goodsDTO.tenderEvaluation + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_approvalToward'>
											<%
											value = procurement_goodsDTO.approvalToward + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_awardNotification'>
											<%
											value = procurement_goodsDTO.awardNotification + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_siginingOfContract'>
											<%
											value = procurement_goodsDTO.siginingOfContract + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_contractSignatureTime'>
											<%
											value = procurement_goodsDTO.contractSignatureTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_contractCompletionTime'>
											<%
											value = procurement_goodsDTO.contractCompletionTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	

<%--											<td>--%>
<%--												<button class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"--%>
<%--											            onclick="location.href='Procurement_goodsServlet?actionType=view&ID=<%=procurement_goodsDTO.iD%>'">--%>
<%--											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>--%>
<%--											    </button>												--%>
<%--											</td>--%>
<%--	--%>
<%--											<td id = '<%=i%>_Edit'>																																	--%>
<%--	--%>
<%--												<button class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"--%>
<%--											            onclick="location.href='Procurement_goodsServlet?actionType=getEditPage&ID=<%=procurement_goodsDTO.iD%>'">--%>
<%--											        <%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_EDIT_BUTTON, loginDTO)%>--%>
<%--											    </button>--%>
<%--																				--%>
<%--											</td>											--%>
<%--											--%>
<%--											--%>
<%--											<td id='<%=i%>_checkbox'>--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=procurement_goodsDTO.iD%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>
																						
									<%
										}
									%>

