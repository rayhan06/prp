
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="procurement_goods.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.text.SimpleDateFormat"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.*" %>


<%
	String context = "../../.." + request.getContextPath() + "/";

	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Procurement_goodsDAO procurement_goodsDAO = (Procurement_goodsDAO)request.getAttribute("procurement_goodsDAO");


String navigator2 = SessionConstants.NAV_PROCUREMENT_GOODS;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>


<style>

	.bolded {
		font-weight: bold;
	}

</style>


<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th><%=LM.getText(LC.VISITOR_PASS_REQUEST_SERIAL_NUMBER, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%></th>
							<%--								<th><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_GOODS_TYPE, loginDTO)%></th>--%>
<%--							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEEN, loginDTO)%></th>--%>
							<th><%=LM.getText(LC.MEETING_MINUTES_ADDING_ADD_MEETING_MINUTES_ITEMNO, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEBN, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_QUANTITY, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_UNITPRICE, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_ESTCOST, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_METHODANDTYPECAT, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_APPROVINGAUTHORITY, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_SOURCEOFFUNDCAT, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TIMECODE, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDER, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDEROPENING, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDEREVALUATION, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_APPROVALTOWARD, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_AWARDNOTIFICATION, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_SIGININGOFCONTRACT, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_CONTRACTSIGNATURETIME, loginDTO)%></th>
							<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_CONTRACTCOMPLETIONTIME, loginDTO)%></th>
							<%--								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>--%>
							<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_EDIT_BUTTON, loginDTO)%></th>--%>
							<%--								<th class="">--%>
							<%--									<span class="ml-4">All</span>--%>
							<%--									<div class="d-flex align-items-center mr-3">--%>
							<%--										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">--%>
							<%--											<i class="fa fa-trash"></i>&nbsp;--%>
							<%--										</button>--%>
							<%--										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>--%>
							<%--									</div>--%>
							<%--								</th>--%>


						</tr>
						</thead>
						<tbody id="tbodyDataDummy">
						<%
							List<Procurement_goodsDTO> data = (List<Procurement_goodsDTO>) session.getAttribute(SessionConstants.VIEW_PROCUREMENT_GOODS);

							try
							{

								if (data != null)
								{
									int size = data.size();
									System.out.println("data not null and size = " + size + " data = " + data);

									HashMap<Long, Integer> packageIdToCount = new HashMap<>();
									HashMap<Long, HashSet<Long>> packageIdToGoodsType = new HashMap<>();

									data
											.forEach(dto -> {
												if (packageIdToCount.containsKey(dto.procurementPackageId)) {
													int count = packageIdToCount.get(dto.procurementPackageId);
													count++;
													packageIdToCount.put(dto.procurementPackageId, count);
												}
												else {
													packageIdToCount.put(dto.procurementPackageId, 1);
												}

												HashSet<Long> goodsType;
												if (packageIdToGoodsType.containsKey(dto.procurementPackageId)) {
													goodsType = packageIdToGoodsType.get(dto.procurementPackageId);
												}
												else {
													goodsType = new HashSet<>();
												}
												goodsType.add(dto.procurementGoodsTypeId);
												packageIdToGoodsType.put(dto.procurementPackageId, goodsType);
											});

									long previousProcurementPackageId = -1;
									long previousProcurementGoodsTypeId = -1;

									long packageNum = 0;
									long itemNum = 0;

									for (int i = 0; i < size; i++)
									{
										Procurement_goodsDTO procurement_goodsDTO = data.get(i);

										int rowSpan = (packageIdToCount.get(procurement_goodsDTO.procurementPackageId) + packageIdToGoodsType.get(procurement_goodsDTO.procurementPackageId).size()) ;

						%>

						<%

							boolean newPackage = procurement_goodsDTO.procurementPackageId != previousProcurementPackageId;
							if (newPackage) {
								previousProcurementPackageId = procurement_goodsDTO.procurementPackageId;
								packageNum++;
							}

							boolean newGoodsType = procurement_goodsDTO.procurementGoodsTypeId != previousProcurementGoodsTypeId;
							if (newGoodsType) {
								previousProcurementGoodsTypeId = procurement_goodsDTO.procurementGoodsTypeId;
							}

							request.setAttribute("procurement_goodsDTO",procurement_goodsDTO);

							if (newGoodsType) {

								itemNum = 1;

						%>

						<tr>

							<jsp:include page="./procurement_goodsReportRow.jsp">
								<jsp:param name="pageName" value="searchrow" />
								<jsp:param name="rownum" value="<%=i%>" />
								<jsp:param name="packageNum" value="<%=packageNum%>" />
								<jsp:param name="itemNum" value="<%=itemNum%>" />
								<jsp:param name="newGoodsType" value="<%=newGoodsType%>" />
								<jsp:param name="newPackage" value="<%=newPackage%>" />
								<jsp:param name="packageIdCount" value="<%=rowSpan%>" />
							</jsp:include>

						</tr>

						<%
							}
						%>

						<tr id = 'tr_<%=i%>'>

							<jsp:include page="./procurement_goodsReportRow.jsp">
								<jsp:param name="pageName" value="searchrow" />
								<jsp:param name="rownum" value="<%=i%>" />
								<jsp:param name="packageNum" value="<%=packageNum%>" />
								<jsp:param name="itemNum" value="<%=itemNum++%>" />
								<jsp:param name="newGoodsType" value="<%=false%>" />
								<jsp:param name="newPackage" value="<%=false%>" />
								<jsp:param name="packageIdCount" value="<%=rowSpan%>" />
							</jsp:include>

						</tr>
						<%
									}

									System.out.println("printing done");
								}
								else
								{
									System.out.println("data  null");
								}
							}
							catch(Exception e)
							{
								System.out.println("JSP exception " + e);
							}
						%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />






