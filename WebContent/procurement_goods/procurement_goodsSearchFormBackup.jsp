
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="procurement_goods.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
	String context = "../../.." + request.getContextPath() + "/";

	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Procurement_goodsDAO procurement_goodsDAO = (Procurement_goodsDAO)request.getAttribute("procurement_goodsDAO");


String navigator2 = SessionConstants.NAV_PROCUREMENT_GOODS;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>

				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th style="display: none"></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_QUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_UNITPRICE, loginDTO)%></th>
								<th><%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_ESTCOST, loginDTO)%></th>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_METHODANDTYPECAT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_APPROVINGAUTHORITY, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_SOURCEOFFUNDCAT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TIMECODE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDER, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDEROPENING, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDEREVALUATION, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_APPROVALTOWARD, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_AWARDNOTIFICATION, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_SIGININGOFCONTRACT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_CONTRACTSIGNATURETIME, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_CONTRACTCOMPLETIONTIME, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<span class="ml-4">All</span>
									<div class="d-flex align-items-center mr-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>&nbsp;
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody id="field-ProcurementGoods">
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_PROCUREMENT_GOODS);
								int childTableStartingID = 0;

								try
								{

									if (data != null) 
									{
										int size = data.size();
										childTableStartingID = size;
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Procurement_goodsDTO procurement_goodsDTO = (Procurement_goodsDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("procurement_goodsDTO",procurement_goodsDTO);
								%>  
								
								 <jsp:include page="./procurement_goodsSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
										request.setAttribute("procurement_goodsDTO",null);
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type='hidden' class='form-control'  name='idsForDelete' id = 'idsForDelete' value='' tag='pb_html'/>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


<div class="form-group">
	<div class="col-xs-9 text-right">
		<button
				id="add-more"
				name="add-more-ProcurementGoods"
				onclick="addMore()"
				class="btn btn-sm text-white add-btn shadow">
			<i class="fa fa-plus"></i>
			<%=LM.getText(LC.LAB_TEST_ADD_LAB_TEST_LIST_ADD_MORE, loginDTO)%>
		</button>
		<button
				id="remove-goods"
				name="remove-ProcurementGoods"
				onclick="removeGoods()"
				class="btn btn-sm remove-btn shadow ml-2">
			<i class="fa fa-trash"></i>
		</button>
	</div>
</div>


<%
	Procurement_goodsDTO procurement_goodsDTOTemplate = new Procurement_goodsDTO();
	procurement_goodsDTOTemplate.iD = -1;
%>

<template id="template-ProcurementGoods" >
	<tr>
		<td>
			<select onchange="showOrHideProcurementGoodsType(this)" class='form-control'  name='procurementPackageId' id = 'procurementPackageId_'   tag='pb_html'>
				<%
					String OptionsTemplate = CommonDAO.getOptions(Language, "procurement_package", procurement_goodsDTOTemplate.procurementPackageId);
				%>
				<%=OptionsTemplate%>
			</select>

			<label class="error-msg" style="color: red;display: none"></label>


		</td>

		<td>
			<select class='form-control'  name='procurementGoodsTypeId' id = 'procurementGoodsTypeId_'   tag='pb_html'>
<%--				<%--%>
<%--					OptionsTemplate = CommonDAO.getOptions(Language, "procurement_goods_type", procurement_goodsDTOTemplate.procurementGoodsTypeId);--%>
<%--				%>--%>
<%--				<%=OptionsTemplate%>--%>
			</select>

			<label class="error-msg" style="color: red;display: none"></label>

		</td>

		<td>
			<input type='text' class='form-control noDollar'  name='nameEn' id = 'nameEn_text_' value='<%=procurement_goodsDTOTemplate.nameEn%>'   tag='pb_html'/>

			<label class="error-msg" style="color: red;display: none"></label>

		</td>

		<td>
			<input type='text' class='form-control noDollar'  name='nameBn' id = 'nameBn_text_' value='<%=procurement_goodsDTOTemplate.nameBn%>'   tag='pb_html'/>

			<label class="error-msg" style="color: red;display: none"></label>

		</td>

		<td>
			<input type='number' class='form-control estCostCalc'  name='quantity' id = 'quantity_number_' value='<%=value%>'  tag='pb_html'>

			<label class="error-msg" style="color: red;display: none"></label>

		</td>

		<td>
			<input type='number' class='form-control estCostCalc'  name='unitPrice' id = 'unitPrice_number_' value='<%=value%>'  tag='pb_html'>

			<label class="error-msg" style="color: red;display: none"></label>

		</td>

		<td>
			<input readonly="readonly" type='number' class='form-control'  name='estCost' id = 'estCost_number_' value='<%=value%>'  tag='pb_html'>


		</td>

<%--		<td>--%>
<%--			<select class='form-control'  name='methodAndTypeCat' id = 'methodAndTypeCat_category_'   tag='pb_html'>--%>
<%--				<%--%>
<%--					OptionsTemplate = CatRepository.getInstance().getOptions(Language, "method_and_type", procurement_goodsDTOTemplate.methodAndTypeCat);--%>
<%--				%>--%>
<%--				<%=OptionsTemplate%>--%>
<%--			</select>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<input type='text' class='form-control'  name='approvingAuthority' id = 'approvingAuthority_text_' value='<%=procurement_goodsDTOTemplate.approvingAuthority%>'   tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<select class='form-control'  name='sourceOfFundCat' id = 'sourceOfFundCat_category_'   tag='pb_html'>--%>
<%--				<%--%>
<%--					OptionsTemplate = CatRepository.getInstance().getOptions(Language, "source_of_fund", procurement_goodsDTOTemplate.sourceOfFundCat);--%>
<%--				%>--%>
<%--				<%=OptionsTemplate%>--%>
<%--			</select>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<%value = "timeCode_js_" ;%>--%>
<%--			<jsp:include page="/time/time.jsp">--%>
<%--				<jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>--%>
<%--				<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--				<jsp:param name="IS_AMPM" value="true"></jsp:param>--%>
<%--			</jsp:include>--%>
<%--			<input type='hidden' value="<%=procurement_goodsDTOTemplate.timeCode%>" name='timeCode' id = 'timeCode_time_'  tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<input type='text' class='form-control'  name='tender' id = 'tender_text_' value='<%=procurement_goodsDTOTemplate.tender%>'   tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<input type='text' class='form-control'  name='tenderOpening' id = 'tenderOpening_text_' value='<%=procurement_goodsDTOTemplate.tenderOpening%>'   tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<input type='text' class='form-control'  name='tenderEvaluation' id = 'tenderEvaluation_text_' value='<%=procurement_goodsDTOTemplate.tenderEvaluation%>'   tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<input type='text' class='form-control'  name='approvalToward' id = 'approvalToward_text_' value='<%=procurement_goodsDTOTemplate.approvalToward%>'   tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<input type='text' class='form-control'  name='awardNotification' id = 'awardNotification_text_' value='<%=procurement_goodsDTOTemplate.awardNotification%>'   tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<input type='text' class='form-control'  name='siginingOfContract' id = 'siginingOfContract_text_' value='<%=procurement_goodsDTOTemplate.siginingOfContract%>'   tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<%value = "contractSignatureTime_js_" ;%>--%>
<%--			<jsp:include page="/time/time.jsp">--%>
<%--				<jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>--%>
<%--				<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--				<jsp:param name="IS_AMPM" value="true"></jsp:param>--%>
<%--			</jsp:include>--%>
<%--			<input type='hidden' value="<%=procurement_goodsDTOTemplate.contractSignatureTime%>" name='contractSignatureTime' id = 'contractSignatureTime_time_'  tag='pb_html'/>--%>


<%--		</td>--%>

<%--		<td>--%>
<%--			<%value = "contractCompletionTime_js_" ;%>--%>
<%--			<jsp:include page="/time/time.jsp">--%>
<%--				<jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>--%>
<%--				<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--				<jsp:param name="IS_AMPM" value="true"></jsp:param>--%>
<%--			</jsp:include>--%>
<%--			<input type='hidden' value="<%=procurement_goodsDTOTemplate.contractCompletionTime%>" name='contractCompletionTime' id = 'contractCompletionTime_time_'  tag='pb_html'/>--%>


<%--		--%>
<%--		</td>--%>









		<td>
<%--			<button class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"--%>
<%--					onclick="location.href='Procurement_goodsServlet?actionType=view&ID=<%=procurement_goodsDTOTemplate.iD%>'">--%>
<%--				<%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>--%>
<%--			</button>--%>
		</td>

		<td>

<%--			<button class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"--%>
<%--					onclick="location.href='Procurement_goodsServlet?actionType=getEditPage&ID=<%=procurement_goodsDTOTemplate.iD%>'">--%>
<%--				<%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_EDIT_BUTTON, loginDTO)%>--%>
<%--			</button>--%>

		</td>


		<td>
			<div class='checker'>
				<span class='chkEdit' ><input type='checkbox' value='<%=procurement_goodsDTOTemplate.iD%>'/></span>
			</div>
		</td>





		<input type='hidden' class='form-control'  name='ID' id = 'id_hidden_' value='<%=procurement_goodsDTOTemplate.iD%>' tag='pb_html'/>
		<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_' value='<%=procurement_goodsDTOTemplate.insertedByUserId%>' tag='pb_html'/>
		<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_' value='<%=procurement_goodsDTOTemplate.insertedByOrganogramId%>' tag='pb_html'/>
		<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_' value='<%=procurement_goodsDTOTemplate.insertionDate%>' tag='pb_html'/>
		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_' value= '<%=procurement_goodsDTOTemplate.isDeleted%>' tag='pb_html'/>

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_' value='<%=procurement_goodsDTOTemplate.lastModificationTime%>' tag='pb_html'/>
		<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_' value='<%=procurement_goodsDTOTemplate.searchColumn%>' tag='pb_html'/>







	</tr>

</template>



<script src="<%=context%>/assets/report/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/jszip.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/pdfmake.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/vfs_fonts.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/buttons.html5.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/buttons.print.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">


	$('.goodsSearch').keyup(function () {
		var reportNameValue = $('#reportName').val();
		var procurementYearValue = $('#procurementYear').val();

		// var filter = 'report_name = \'' + reportNameValue + '\' AND ' + 'procurement_year = \'' + procurementYearValue + '\'';

		var formData = new FormData();
		formData.append('report_name_ajax', reportNameValue);
		formData.append('procurement_year_ajax', procurementYearValue);

		var params = "Procurement_goodsServlet?actionType=search";

		params +=  '&filter=1&search=true&ajax=true';
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
		})

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;

		if (reportNameValue.toString().trim().length > 0 || procurementYearValue.toString().trim().length > 0) {
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {

					document.getElementById('tableForm').innerHTML = this.responseText ;
					setPageNo();
					searchChanged = 0;

				} else if (this.readyState === 4 && this.status !== 200) {
					alert('failed ' + this.status);
				}
			};
			xhttp.open("POST", params, true);
			xhttp.send(formData);
		}

	});

	function goodsSearch() {
		var reportNameValue = $('#reportName').val();
		var procurementYearValue = $('#procurementYear').val();

		// var filter = 'report_name = \'' + reportNameValue + '\' AND ' + 'procurement_year = \'' + procurementYearValue + '\'';

		var formData = new FormData();
		formData.append('report_name_ajax', reportNameValue);
		formData.append('procurement_year_ajax', procurementYearValue);

		var params = "Procurement_goodsServlet?actionType=search";

		params +=  '&filter=1&search=true&ajax=true';
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
		})

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;

		if (reportNameValue.toString().trim().length > 0 || procurementYearValue.toString().trim().length > 0) {
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {

					document.getElementById('tableForm').innerHTML = this.responseText ;
					setPageNo();
					searchChanged = 0;

				} else if (this.readyState === 4 && this.status !== 200) {
					alert('failed ' + this.status);
				}
			};
			xhttp.open("POST", params, true);
			xhttp.send(formData);
		}

	}

	$('body').on('blur','.estCostCalc', function (event) {
		var id = event.target.id;
		id = id.toString().trim();
		var index = id.substring(id.lastIndexOf('_') + 1);
		var quantity = $("#quantity_number_" + index).val();
		var unitPrice = $("#unitPrice_number_" + index).val();

		if (quantity.trim().length > 0 && unitPrice.trim().length > 0) {
			var estCost = quantity * unitPrice;
			$("#estCost_number_" + index).val(estCost);
			$("#estCost_number_" + index).html(estCost);
			$("#estCost_number_" + index).text(estCost);
		}
	});

	function showOrHideProcurementGoodsType(e) {
		if (e.value && e.value >= 0) {
			var id = e.id;
			var index = id.toString().trim().substring(21);
			loadProcurementGoodsType($('#procurementPackageId_' + index).val(), index);

		} else {
			$('#procurementGoodsTypeId_' + index).val(null);

		}

	}

	function loadProcurementGoodsType(packageId, index) {

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if (!this.responseText.includes('option')) {
					$('#procurementGoodsTypeId_' + index).html(null);
				} else {
					$('#procurementGoodsTypeId_' + index).html(this.responseText);
				}

			} else if (this.readyState == 4 && this.status != 200) {
				alert('failed ' + this.status);
			}
		};

		xhttp.open("GET", "Procurement_packageServlet?actionType=getByPackageId&ID=" + packageId, true);
		xhttp.send();

	}



	var child_table_extra_id = <%=childTableStartingID%>;



			function addMore()
			{
				event.preventDefault();
				var t = $("#template-ProcurementGoods");

				$("#field-ProcurementGoods").append(t.html());
				SetCheckBoxValues("field-ProcurementGoods");

				var tr = $("#field-ProcurementGoods").find("tr:last-child");

				tr.attr("id","ProcurementGoods_" + child_table_extra_id);

				tr.find("[tag='pb_html']").each(function( index )
				{
					var prev_id = $( this ).attr('id');
					$( this ).attr('id', prev_id + child_table_extra_id);
					console.log( index + ": " + $( this ).attr('id') );
				});


				child_table_extra_id ++;

				$(".noDollar").keypress(function (evt) {
					let keycode = evt.charCode || evt.keyCode;
					if (keycode  == 36) {
						return false;
					}
				});

			}


	function removeGoods(){

		event.preventDefault();

		var tablename = 'field-ProcurementGoods';
		var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelector('input[type="checkbox"]');
				if(checkbox.checked == true)
				{
					tr.remove();
					var idsForDelete = document.getElementById('idsForDelete').value;
					idsForDelete += ',' + checkbox.value ;
					document.getElementById('idsForDelete').value = idsForDelete;
				}
				j ++;
			}

		}
	}


</script>