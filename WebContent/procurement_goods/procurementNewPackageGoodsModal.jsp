<!-- Modal -->
<%--
  Author: Moaz Mahmud
  USAGE: see disciplinary_log/disciplinary_logEditBody.jsp
--%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@ page import="procurement_goods.Procurement_goodsDTO" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@ page import="pi_package_new.Pi_package_newRepository" %>
<%@page pageEncoding="UTF-8" %>
<%--<%@page contentType="text/html;charset=utf-8" %>--%>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String isHierarchyNeededStr = request.getParameter("isHierarchyNeeded");
    boolean isHierarchyNeeded = isHierarchyNeededStr != null && Boolean.parseBoolean(isHierarchyNeededStr);
    String modalTitle = request.getParameter("modalTitle");
    if (modalTitle == null) {
        modalTitle = LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, userDTO);
    }

    Procurement_goodsDTO procurement_goodsDTO = new Procurement_goodsDTO();
    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
//    String ID = request.getParameter("ID");
//    if (ID != null) {
//        long id = Long.parseLong(ID);
//        procurement_goodsDTO = procurement_goodsDAO.getDTOByID(id);
//    }

    String index = request.getParameter("index");
    int i = 0;
    if (index != null) {
        i = Integer.parseInt(index);
    }

%>

<div class="modal fade bd-example-modal-xl" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_proc_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="procurementPackageNewGoodsModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn submit-btn text-white shadow btn-border-radius"
                        onclick="updateSelectedItem(1)">
                    <%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
                        onclick="updateSelectedItem(0)">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


    var subTypeCount = <%=index%>;
    var itemSelected = false;
    var selectedItemFromProcurementModal = new Map;
    var lastItemFromProcurementModal = new Map;
    var necessaryCollectionForProcurementModal = {};
    let modalItemData = new Map();

    $('#search_proc_modal').on('show.bs.modal', function () {
        resetModal();
        event.preventDefault();
        itemSelected = false;
    });

    function loadTableFromManualSearch(parentIdHidden){
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    lastItemFromProcurementModal = totalResponse.procurementGoodsResponseDTO;

                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByParent&ID=" + parentIdHidden + "&index=" + 0, false);
        xhttp.send();
    }

    function updateSelectedItem(updateSelected) {

        // let manualSearch = document.getElementById('procurementGoodsId').value;
        // let parentIdHidden = document.getElementById('parent_id_hidden').value;
        // if(manualSearch !== null && manualSearch !== '' && updateSelected == 1){
        if(updateSelected === 1){

            for (const [key, value] of modalItemData.entries()) {
                loadProcurementGoodsByParent(value);
                selectedItemFromProcurementModal = lastItemFromProcurementModal;
                if (necessaryCollectionForProcurementModal.callBackFunction) {
                    necessaryCollectionForProcurementModal.callBackFunction(selectedItemFromProcurementModal);
                }
            }

            $('#search_proc_modal').modal('hide');
        }


    }

    $(document).ready(function () {
        select2SingleSelector('#piPackageNewId', '<%=Language%>');

    });

    function resetModal() {
        modalItemData.clear();
        $('#piPackageNewId').val('');
        $('#modalAppendTbody').empty();
    }

    function piNewPackageChanged(selectedElement) {
        let piPackageNewId = selectedElement.value;
        let piPackageItemMapId = -1;
        let procurementPackageId = -1;
        let procurementGoodsTypeId = -1;

        let actionType = '';
        let modalActionType = 'dataByPiPackageNewId';
        actionType = 'getItemWiseRowEdit';


        let param = 'actionType=' + actionType;
        param += '&piPackageNewId=' + piPackageNewId;
        param += '&piPackageItemMapId=' + piPackageItemMapId;
        param += '&procurementPackageId=' + procurementPackageId;
        param += '&procurementGoodsTypeId=' + procurementGoodsTypeId;
        param += '&modalActionType=' + modalActionType;

        if (piPackageNewId !== '') {
            //fullPageLoader.show();
            const url = 'Procurement_goodsServlet?' + param;
            let response = getItemResponse(url);
            setModalTableData(response);
        }
    }

    function getItemResponse(url){
        let res=[];
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (response) {
                if (response !== null) {
                    res =  response;
                }
                //fullPageLoader.hide();
            },
            error: function (error) {
                //fullPageLoader.hide();
            }
        });
        return res;
    }

    function setModalTableData(data) {

        data.forEach((item, index) => {
            if (!modalItemData.has(+item.itemId)) {
                modalItemData.set(+item.itemId, item);
                setEachItemInTr('modalAppendTbody',item, index);
            }

        });

    }

    function setEachItemInTr(tbodyId,item,index){

        let {
            itemId,
            piPackageItemMapId,
            piPackageItemMapChildId,
            itemName,
            subTypes,
            unit,
            unitId,
            description,
            procurementPackageId,
            procurementGoodsTypeId,
            isSelected,
        } = item;

        let trContent = document.querySelector('.hiddenTr').cloneNode(true);
        trContent.classList.replace('hiddenTr', 'appendedTr');

        trContent.querySelector('.piPackageItemMapIdCls').value = piPackageItemMapId;
        trContent.querySelector('.piPackageItemMapIdCls').name = 'piPackageItemMapChild.piPackageItemMapId';

        trContent.querySelector('.piPackageItemMapChildIdCls').value = piPackageItemMapChildId;
        trContent.querySelector('.piPackageItemMapChildIdCls').name = 'piPackageItemMapChild.piPackageItemMapChildId';

        trContent.querySelector('.groupTypeIdCls').value = procurementPackageId;
        trContent.querySelector('.groupTypeIdCls').name = 'piPackageItemMapChild.itemGroupId';

        trContent.querySelector('.itemTypeIdCls').value = procurementGoodsTypeId;
        trContent.querySelector('.itemTypeIdCls').name = 'piPackageItemMapChild.itemTypeId';

        trContent.querySelector('.itemIdCls').value = itemId;
        trContent.querySelector('.itemIdCls').name = 'piPackageItemMapChild.itemId';

        trContent.querySelector('.unitIdCls').value = unitId;
        trContent.querySelector('.unitIdCls').name = 'piPackageItemMapChild.piUnitId';

        trContent.querySelector('.itemTdCls').innerText = itemName;
        trContent.querySelector('.subTypeTdCls').innerText = subTypes;
        trContent.querySelector('.unitTdCls').innerText = unit;
        trContent.querySelector('.descriptionTdCls').innerText = description;
        trContent.querySelector('.isSelectedTdCls').innerHTML = '<button type="button" onclick="removeItemFromModal(this)" class="btn d-flex align-items-center" value="" style="color: #ff6a6a"><i class="fa fa-trash"></i></button>';

        $("#"+tbodyId).append(trContent);

    }


    function loadProcurementGoodsByParent(element) {

        itemSelected = true;
        let searchId = element.itemId;

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (!this.responseText.includes('option')) {
                } else {
                    var totalResponse = JSON.parse(this.responseText);
                    lastItemFromProcurementModal = totalResponse.procurementGoodsResponseDTO;
                }

            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByParent&ID=" + searchId + "&index=-1", false);

        if (searchId !== undefined && searchId !== null && searchId !== '') xhttp.send();

    }

    function removeItemFromModal(selectedElement){
        let parentTr = selectedElement.parentNode.parentNode;
        modalItemData.delete(+(parentTr.querySelector('.itemIdCls').value));
        //itemData.delete(+(parentTr.querySelector('.itemIdCls').value));
        $(parentTr).remove();
    }


</script>