<%@ page import="util.UtilCharacter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">

            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%>
                </label>
                <div class="col-md-4">
                    <select class='form-control' name='piPackageNewId'
                            id='piPackageNewId' tag='pb_html' onchange="piNewPackageChanged(this)">
                        <%= Pi_package_newRepository.getInstance().buildOptions(Language,null) %>
                    </select>

                </div>
            </div>

            <div class="mt-4">
                <div class="form-body">
                    <h5 class="table-title">
                        <%=LM.getText(LC.PI_PACKAGE_ITEM_MAP_ADD_PI_PACKAGE_ITEM_MAP_CHILD, loginDTO)%>
                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="modal-product-view-table">
                            <thead>
                            <tr>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "আইটেম", "Item")%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "সাব টাইপ", "Sub Types")%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "ইউনিট", "Unit")%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "বিবরণ", "Description")%>
                                </th>
                                <th><%=UtilCharacter.getDataByLanguage(Language, "সরান", "Remove")%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="modalAppendTbody">


                            </tbody>

                            <tr class="hiddenTr">


                                <input type='hidden' class='form-control piPackageItemMapIdCls'
                                       tag='pb_html'/>

                                <input type='hidden' class='form-control piPackageItemMapChildIdCls'
                                       tag='pb_html'/>

                                <input type='hidden' class='form-control groupTypeIdCls'
                                       tag='pb_html'/>

                                <input type='hidden' class='form-control itemTypeIdCls'
                                       tag='pb_html'/>

                                <input type='hidden' class='form-control itemIdCls'
                                       tag='pb_html'/>

                                <input type='hidden' class='form-control unitIdCls'
                                       tag='pb_html'/>


                                <td class="itemTdCls">

                                </td>
                                <td class="subTypeTdCls">

                                </td>
                                <td class="unitTdCls">

                                </td>

                                <td class="descriptionTdCls">

                                </td>
                                <td class="isSelectedTdCls">

                                </td>

                            </tr>


                        </table>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

<style>
    .hiddenTr{
        display: none;
    }
</style>