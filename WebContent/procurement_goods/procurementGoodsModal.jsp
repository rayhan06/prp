<!-- Modal -->
<%--
  Author: Moaz Mahmud
  USAGE: see disciplinary_log/disciplinary_logEditBody.jsp
--%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@ page import="procurement_goods.Procurement_goodsDTO" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String isHierarchyNeededStr = request.getParameter("isHierarchyNeeded");
    boolean isHierarchyNeeded = isHierarchyNeededStr != null && Boolean.parseBoolean(isHierarchyNeededStr);
    String modalTitle = request.getParameter("modalTitle");
    if (modalTitle == null) {
        modalTitle = LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, userDTO);
    }

    Procurement_goodsDTO procurement_goodsDTO = new Procurement_goodsDTO();
    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
//    String ID = request.getParameter("ID");
//    if (ID != null) {
//        long id = Long.parseLong(ID);
//        procurement_goodsDTO = procurement_goodsDAO.getDTOByID(id);
//    }

    String index = request.getParameter("index");
    int i = 0;
    if (index != null) {
        i = Integer.parseInt(index);
    }

    String packageOptions = Procurement_packageRepository.getInstance().getOptions(Language, procurement_goodsDTO.procurementPackageId);

%>

<div class="modal fade bd-example-modal-xl"  aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_proc_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="procurementGoodsModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn submit-btn text-white shadow btn-border-radius"
                        id="modal-submit" onclick="updateSelectedItem(1)">
                    <%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius" onclick="updateSelectedItem(0)">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        hideSubmitButton(submitButtonId);
        initProcurementGoodsModal('0');
    })



    var subTypeCount = <%=index%>;
    var itemSelected = false;
    let submitButtonId = "modal-submit";

    function showSubmitButton(buttonId){
        document.getElementById(buttonId).style.visibility = 'visible';
    }
    function hideSubmitButton(buttonId){
        document.getElementById(buttonId).style.visibility = 'hidden';
    }


    $('#search_proc_modal').on('show.bs.modal', function () {
        event.preventDefault();

        $("#procurementPackageId_<%=index%>").val(-1);
        $("#procurementGoodsTypeId_<%=index%>").html('');
        $("#subType").html('');

        // if($('#procurementGoodsId').val() !== ''){
        //     //$('#procurementGoodsId').html('');
        //     $('#parent_id_hidden').val('');
        // }

        itemSelected = false;

    });

    function loadTableFromManualSearch(parentIdHidden){
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    lastItemFromProcurementModal = totalResponse.procurementGoodsResponseDTO;

                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByParent&ID=" + parentIdHidden + "&index=" + 0, false);
        xhttp.send();
    }


    function updateSelectedItem(updateSelected) {

        // let manualSearch = document.getElementById('procurementGoodsId').value;
        // let parentIdHidden = document.getElementById('parent_id_hidden').value;
        // if(manualSearch !== null && manualSearch !== '' && updateSelected == 1){
        if(updateSelected == 1){

            // loadTableFromManualSearch(parentIdHidden);

            selectedItemFromProcurementModal = lastItemFromProcurementModal;
            if (necessaryCollectionForProcurementModal.callBackFunction) {
                necessaryCollectionForProcurementModal.callBackFunction(selectedItemFromProcurementModal);
            }

            $('#search_proc_modal').modal('hide');
        }

        else if (updateSelected == 1 && !itemSelected) {
            var language = '<%=Language%>';
            var reqMSg = language == 'Bangla' ? "কোনো আইটেম বাছাই করা হয়নি" : "No item selected";
            bootbox.alert(reqMSg, function (result) {
            });
        }
        else {
            if (updateSelected == 1) {

                selectedItemFromProcurementModal = lastItemFromProcurementModal;
                if (necessaryCollectionForProcurementModal.callBackFunction) {
                    necessaryCollectionForProcurementModal.callBackFunction(selectedItemFromProcurementModal);
                }
            }
            $('#search_proc_modal').modal('hide');
        }
    }

    function initProcurementGoodsModal(row) {

        $('#procurementPackageId_<%=i%>').change(function () {
            showOrHideProcurementGoodsType();
        });

        $('#procurementGoodsTypeId_<%=i%>').change(function () {
            loadProcurementGoodsByType(this.value);
        });

    }

    function showOrHideProcurementGoodsType() {
        if ($('#procurementPackageId_<%=i%>').val() && $('#procurementPackageId_<%=i%>').val() > 0) {
            loadProcurementGoodsType($('#procurementPackageId_<%=i%>').val());

        } else {
            $('#procurementGoodsTypeId_<%=i%>').val(null);

        }

    }

    function loadProcurementGoodsType(packageId) {

        $("#subType").html('');
        hideSubmitButton(submitButtonId);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {
                    $('#procurementGoodsTypeId_<%=i%>').html(this.responseText);
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_packageServlet?actionType=getByPackageId&ID=" + packageId, false);

        if (packageId != undefined && packageId != null && packageId != '') xhttp.send();

    }


    function loadProcurementGoodsByType(goodsTypeId) {

        $("#subType").html('');
        hideSubmitButton(submitButtonId);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    var options = totalResponse.options;

                    $("#subType").append(options);
                    updateLabels();

                    $('#subType_select_' + subTypeCount).change(function () {
                        loadProcurementGoodsByParent(this);
                        if(document.getElementsByName("subType").item(0).length > 1)
                            showSubmitButton(submitButtonId);
                    });

                    subTypeCount++;
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByGoodsType&ID=" + goodsTypeId + "&index=" + subTypeCount, true);

        if (goodsTypeId != undefined && goodsTypeId != null && goodsTypeId != '') xhttp.send();

    }


    function loadProcurementGoodsByParent(element) {

        var nextIndexes = parseInt(element.id.toString().substring(15)) + 1;
        subTypeCount = nextIndexes;
        itemSelected = true;

        while ($("#subType_select_" + nextIndexes).val() != undefined && $("#subType_select_" + nextIndexes).val() != null) {
            $("#div_id_" + nextIndexes).remove();
            nextIndexes++;
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    var options = totalResponse.options;
                    lastItemFromProcurementModal = totalResponse.procurementGoodsResponseDTO;

                    $("#subType").append(options);
                    updateLabels();

                    $('#subType_select_' + subTypeCount).change(function () {
                        loadProcurementGoodsByParent(this);
                    });

                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByParent&ID=" + element.value + "&index=" + subTypeCount, true);

        if (element.value != undefined && element.value != null && element.value != '') xhttp.send();

    }

    var selectedItemFromProcurementModal = new Map;
    var lastItemFromProcurementModal = new Map;
    var necessaryCollectionForProcurementModal = {};


    function onProcurementGoodsChange(selected){

        let procurementGoodsId = selected.value;
        const url = 'Procurement_goodsServlet?actionType=getGoodsDtoResponseById&procurementGoodsId=' + procurementGoodsId;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (response) {
                let {packageId,procurementGoodsTypeId,procurementGoodsParentId} =  response;
                document.getElementById('procurementPackageId_1').value=packageId;
                loadProcurementGoodsType(packageId);
                document.getElementById('procurementGoodsTypeId_1').value=procurementGoodsTypeId;
                //document.getElementById('parent_id_hidden').value=procurementGoodsParentId;
                document.getElementById('parent_id_hidden').value=procurementGoodsId;
            },
            error: function (error) {
                console.log(error);
            }
        });

    }

    function updateLabels() {
        var goodsLabels = document.getElementById('subType').getElementsByClassName('col-form-label');

        var totalLabels = goodsLabels.length;
        for (var i=0; i< totalLabels - 1; i++) {
            goodsLabels[i].innerHTML = "<%=LM.getText(LC.PROCUREMENT_GOODS_ADD_ID, loginDTO)%>";
        }
        goodsLabels[totalLabels-1].innerHTML = "<%=LM.getText(LC.GATE_PASS_VIEW_ITEM_NAME, loginDTO)%>";
    }

</script>