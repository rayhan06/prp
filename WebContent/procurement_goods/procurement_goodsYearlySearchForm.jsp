
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="procurement_goods.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
	String context = "../../.." + request.getContextPath() + "/";

	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Procurement_goodsDAO procurement_goodsDAO = (Procurement_goodsDAO)request.getAttribute("procurement_goodsDAO");


String navigator2 = SessionConstants.NAV_PROCUREMENT_GOODS;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>

				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="display: none"></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_REPORT_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								
							</tr>
						</thead>
						<tbody id="field-ProcurementGoods">
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_PROCUREMENT_GOODS);
								int childTableStartingID = 0;

								try
								{

									if (data != null) 
									{
										int size = data.size();
										childTableStartingID = size;
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Procurement_goodsDTO procurement_goodsDTO = (Procurement_goodsDTO) data.get(i);
											
								%>
											
		
								<%  								
								    request.setAttribute("procurement_goodsDTO",procurement_goodsDTO);
								%>  
								
								 <jsp:include page="./procurement_goodsYearlySearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
										request.setAttribute("procurement_goodsDTO",null);
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type='hidden' class='form-control'  name='idsForDelete' id = 'idsForDelete' value='' tag='pb_html'/>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />





<script src="<%=context%>/assets/report/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/jszip.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/pdfmake.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/vfs_fonts.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/buttons.html5.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/buttons.print.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">


	$('.goodsSearch').keyup(function () {
		var reportNameValue = $('#reportName').val();
		var procurementYearValue = $('#procurementYear').val();

		// var filter = 'report_name = \'' + reportNameValue + '\' AND ' + 'procurement_year = \'' + procurementYearValue + '\'';

		var formData = new FormData();
		formData.append('report_name_ajax', reportNameValue);
		formData.append('procurement_year_ajax', procurementYearValue);

		var params = "Procurement_goodsServlet?actionType=yearlySearch";

		params +=  '&filter=1&search=true&ajax=true';
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
		})

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;

		if (reportNameValue.toString().trim().length > 0 || procurementYearValue.toString().trim().length > 0) {
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {

					document.getElementById('tableForm').innerHTML = this.responseText ;
					setPageNo();
					searchChanged = 0;

				} else if (this.readyState === 4 && this.status !== 200) {
					alert('failed ' + this.status);
				}
			};
			xhttp.open("POST", params, true);
			xhttp.send(formData);
		}

	});

	function goodsSearch() {
		var reportNameValue = $('#reportName').val();
		var procurementYearValue = $('#procurementYear').val();

		// var filter = 'report_name = \'' + reportNameValue + '\' AND ' + 'procurement_year = \'' + procurementYearValue + '\'';

		var formData = new FormData();
		formData.append('report_name_ajax', reportNameValue);
		formData.append('procurement_year_ajax', procurementYearValue);

		var params = "Procurement_goodsServlet?actionType=yearlySearch";

		params +=  '&filter=1&search=true&ajax=true';
		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
		})

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;

		if (reportNameValue.toString().trim().length > 0 || procurementYearValue.toString().trim().length > 0) {
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {

					document.getElementById('tableForm').innerHTML = this.responseText ;
					setPageNo();
					searchChanged = 0;

				} else if (this.readyState === 4 && this.status !== 200) {
					alert('failed ' + this.status);
				}
			};
			xhttp.open("POST", params, true);
			xhttp.send(formData);
		}

	}

	function goodsSearchByButton(reportName) {
		var procurementYearValue = $('#procurementYear').val();

		var params = "Procurement_goodsServlet?actionType=search";

		var extraParams = document.getElementsByName('extraParam');
		extraParams.forEach((param) => {
			params += "&" + param.getAttribute("tag") + "=" + param.value;
		})

		var pageNo = document.getElementsByName('pageno')[0].value;
		var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

		var totalRecords = 0;
		var lastSearchTime = 0;
		if(document.getElementById('hidden_totalrecords'))
		{
			totalRecords = document.getElementById('hidden_totalrecords').value;
			lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
		}


		params += '&filter=1' ;
		params += '&report_name_ajax=' + reportName ;
		params += '&procurement_year_ajax=' + procurementYearValue ;
		params += '&pageno=' + pageNo;
		params += '&RECORDS_PER_PAGE=' + rpp;
		params += '&TotalRecords=' + totalRecords;
		params += '&lastSearchTime=' + lastSearchTime;

		// if (reportNameValue.toString().trim().length > 0 || procurementYearValue.toString().trim().length > 0) {
		// 	let xhttp = new XMLHttpRequest();
		// 	xhttp.onreadystatechange = function () {
		// 		if (this.readyState === 4 && this.status === 200) {
		//
		// 			document.getElementById('tableForm').innerHTML = this.responseText ;
		// 			setPageNo();
		// 			searchChanged = 0;
		//
		// 		} else if (this.readyState === 4 && this.status !== 200) {
		// 			alert('failed ' + this.status);
		// 		}
		// 	};
		// 	xhttp.open("POST", params, true);
		// 	xhttp.send(formData);
		// }

		location.href=params;

	}

</script>