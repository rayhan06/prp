<%@page pageEncoding="UTF-8" %>

<%@page import="procurement_goods.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
	String Language2 = Language;

	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


	String navigator2 = SessionConstants.NAV_PROCUREMENT_GOODS;
	RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
	boolean isPermanentTable = rn2.m_isPermanentTable;
	String tableName = rn2.m_tableName;

	System.out.println("isPermanentTable = " + isPermanentTable);
	Procurement_goodsDTO procurement_goodsDTO = (Procurement_goodsDTO)request.getAttribute("procurement_goodsDTO");
	CommonDTO commonDTO = procurement_goodsDTO;
	String servletName = "Procurement_goodsServlet";


	System.out.println("procurement_goodsDTO = " + procurement_goodsDTO);


	int i = Integer.parseInt(request.getParameter("rownum"));
	out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

	String value = "";


	Procurement_goodsDAO procurement_goodsDAO = (Procurement_goodsDAO)request.getAttribute("procurement_goodsDAO");


	String Options = "";
	boolean formSubmit = false;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<td id = '<%=i%>_procurementPackageId'>
	<select onchange="showOrHideProcurementGoodsType(this)" class='form-control'  name='procurementPackageId' id = 'procurementPackageId_<%=i%>'   tag='pb_html'>
		<%
			Options = CommonDAO.getOptions(Language, "procurement_package", procurement_goodsDTO.procurementPackageId);
		%>
		<%=Options%>
	</select>

	<label class="error-msg" style="color: red;display: none"></label>

</td>

<td id = '<%=i%>_procurementGoodsTypeId'>
	<select class='form-control'  name='procurementGoodsTypeId' id = 'procurementGoodsTypeId_<%=i%>'   tag='pb_html'>
		<%
			Options = CommonDAO.getOptions(Language, "procurement_goods_type", procurement_goodsDTO.procurementGoodsTypeId);
		%>
		<%=Options%>
	</select>

	<label class="error-msg" style="color: red;display: none"></label>

</td>

<td id = '<%=i%>_nameEn'>
	<input type='text' class='form-control noDollar'  name='nameEn' id = 'nameEn_text_<%=i%>' value='<%=procurement_goodsDTO.nameEn%>'   tag='pb_html'/>

	<label class="error-msg" style="color: red;display: none"></label>

</td>

<td id = '<%=i%>_nameBn'>
	<input type='text' class='form-control noDollar'  name='nameBn' id = 'nameBn_text_<%=i%>' value='<%=procurement_goodsDTO.nameBn%>'   tag='pb_html'/>

	<label class="error-msg" style="color: red;display: none"></label>

</td>

<td id = '<%=i%>_quantity' style="text-align: right">
	<input type='number' class='form-control estCostCalc'  name='quantity' id = 'quantity_number_<%=i%>' value='<%=procurement_goodsDTO.quantity%>'  tag='pb_html'>

	<label class="error-msg" style="color: red;display: none"></label>

</td>

<td id = '<%=i%>_unitPrice' style="text-align: right">
	<input type='number' class='form-control estCostCalc'  name='unitPrice' id = 'unitPrice_number_<%=i%>' value='<%=procurement_goodsDTO.unitPrice%>'  tag='pb_html'>

	<label class="error-msg" style="color: red;display: none"></label>

</td>

<td id = '<%=i%>_estCost' style="text-align: right">
	<input readonly="readonly" type='number' class='form-control'  name='estCost' id = 'estCost_number_<%=i%>' value='<%=procurement_goodsDTO.estCost%>'  tag='pb_html'>


</td>

<%--											<td id = '<%=i%>_methodAndTypeCat'>--%>
<%--												<select class='form-control'  name='methodAndTypeCat' id = 'methodAndTypeCat_category_<%=i%>'   tag='pb_html'>--%>
<%--													<%--%>
<%--														Options = CatRepository.getInstance().getOptions(Language, "method_and_type", procurement_goodsDTO.methodAndTypeCat);--%>
<%--													%>--%>
<%--													<%=Options%>--%>
<%--												</select>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approvingAuthority'>--%>
<%--												<input type='text' class='form-control'  name='approvingAuthority' id = 'approvingAuthority_text_<%=i%>' value='<%=procurement_goodsDTO.approvingAuthority%>'   tag='pb_html'/>--%>


<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_sourceOfFundCat'>--%>
<%--												<select class='form-control'  name='sourceOfFundCat' id = 'sourceOfFundCat_category_<%=i%>'   tag='pb_html'>--%>
<%--													<%--%>
<%--														Options = CatRepository.getInstance().getOptions(Language, "source_of_fund", procurement_goodsDTO.sourceOfFundCat);--%>
<%--													%>--%>
<%--													<%=Options%>--%>
<%--												</select>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_timeCode'>--%>
<%--												<%value = "timeCode_js_" + i;%>--%>
<%--												<jsp:include page="/time/time.jsp">--%>
<%--													<jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>--%>
<%--													<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--													<jsp:param name="IS_AMPM" value="true"></jsp:param>--%>
<%--												</jsp:include>--%>
<%--												<input type='hidden' value="<%=procurement_goodsDTO.timeCode%>" name='timeCode' id = 'timeCode_time_<%=i%>'  tag='pb_html'/>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_tender'>--%>
<%--												<input type='text' class='form-control'  name='tender' id = 'tender_text_<%=i%>' value='<%=procurement_goodsDTO.tender%>'   tag='pb_html'/>--%>


<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_tenderOpening'>--%>
<%--												<input type='text' class='form-control'  name='tenderOpening' id = 'tenderOpening_text_<%=i%>' value='<%=procurement_goodsDTO.tenderOpening%>'   tag='pb_html'/>--%>


<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_tenderEvaluation'>--%>
<%--												<input type='text' class='form-control'  name='tenderEvaluation' id = 'tenderEvaluation_text_<%=i%>' value='<%=procurement_goodsDTO.tenderEvaluation%>'   tag='pb_html'/>--%>


<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_approvalToward'>--%>
<%--												<input type='text' class='form-control'  name='approvalToward' id = 'approvalToward_text_<%=i%>' value='<%=procurement_goodsDTO.approvalToward%>'   tag='pb_html'/>--%>


<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_awardNotification'>--%>
<%--												<input type='text' class='form-control'  name='awardNotification' id = 'awardNotification_text_<%=i%>' value='<%=procurement_goodsDTO.awardNotification%>'   tag='pb_html'/>--%>


<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_siginingOfContract'>--%>
<%--												<input type='text' class='form-control'  name='siginingOfContract' id = 'siginingOfContract_text_<%=i%>' value='<%=procurement_goodsDTO.siginingOfContract%>'   tag='pb_html'/>--%>


<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_contractSignatureTime'>--%>
<%--												<%value = "contractSignatureTime_js_" + i;%>--%>
<%--												<jsp:include page="/time/time.jsp">--%>
<%--													<jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>--%>
<%--													<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--													<jsp:param name="IS_AMPM" value="true"></jsp:param>--%>
<%--												</jsp:include>--%>
<%--												<input type='hidden' value="<%=procurement_goodsDTO.contractSignatureTime%>" name='contractSignatureTime' id = 'contractSignatureTime_time_<%=i%>'  tag='pb_html'/>--%>


<%--											</td>--%>
<%--		--%>
<%--											<td id = '<%=i%>_contractCompletionTime'>--%>
<%--												<%value = "contractCompletionTime_js_" + i;%>--%>
<%--												<jsp:include page="/time/time.jsp">--%>
<%--													<jsp:param name="TIME_ID" value="<%=value%>"></jsp:param>--%>
<%--													<jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>--%>
<%--													<jsp:param name="IS_AMPM" value="true"></jsp:param>--%>
<%--												</jsp:include>--%>
<%--												<input type='hidden' value="<%=procurement_goodsDTO.contractCompletionTime%>" name='contractCompletionTime' id = 'contractCompletionTime_time_<%=i%>'  tag='pb_html'/>--%>
<%--												--%>

<%--											</td>--%>









<td>
	<button class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
			onclick="event.preventDefault();location.href='Procurement_goodsServlet?actionType=view&ID=<%=procurement_goodsDTO.iD%>'">
		<%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
	</button>
</td>

<td id = '<%=i%>_Edit'>

	<button class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
			onclick="event.preventDefault();location.href='Procurement_goodsServlet?actionType=getEditPage&ID=<%=procurement_goodsDTO.iD%>'">
		<%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_EDIT_BUTTON, loginDTO)%>
	</button>

</td>


<td id='<%=i%>_checkbox'>
	<div class='checker'>
		<span class='chkEdit' ><input type='checkbox' value='<%=procurement_goodsDTO.iD%>'/></span>
	</div>
</td>



<input type='hidden' class='form-control'  name='ID' id = 'id_hidden_<%=i%>' value='<%=procurement_goodsDTO.iD%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=procurement_goodsDTO.insertedByUserId%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=procurement_goodsDTO.insertedByOrganogramId%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=procurement_goodsDTO.insertionDate%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=procurement_goodsDTO.isDeleted%>' tag='pb_html'/>

<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=procurement_goodsDTO.lastModificationTime%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=procurement_goodsDTO.searchColumn%>' tag='pb_html'/>
