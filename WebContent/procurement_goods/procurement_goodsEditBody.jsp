<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="procurement_goods.*" %>
<%@page import="java.util.*" %>

<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="procurement_package.*" %>
<%@ page import="org.apache.commons.codec.language.bm.Lang" %>
<%@ page import="pi_unit.Pi_unitRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
    Procurement_goodsDTO procurement_goodsDTO;
    procurement_goodsDTO = (Procurement_goodsDTO) request.getAttribute("procurement_goodsDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (procurement_goodsDTO == null) {
        procurement_goodsDTO = new Procurement_goodsDTO();

    }
    System.out.println("procurement_goodsDTO = " + procurement_goodsDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.HM_ITEM, loginDTO);
    String servletName = "Procurement_goodsServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

//    boolean childExists = !(procurement_goodsDAO.getDTOByParentId(procurement_goodsDTO.iD).isEmpty() || actionName.equals("add"));

    List<Procurement_goodsDTO> allSubTypes = new ArrayList<>();


    if (StringUtils.isValidString(procurement_goodsDTO.parentIdsCommaSeparated)) {

        allSubTypes = Procurement_goodsRepository.getInstance().getDTOsByIds(
                Arrays.stream(procurement_goodsDTO.parentIdsCommaSeparated.split(","))
                        .map(parentId -> Long.parseLong(parentId))
                        .collect(Collectors.toList()));

    }


//    long parentId = procurement_goodsDTO.parentId;
//    while (parentId != -1) {
//        Procurement_goodsDTO parent = procurement_goodsDAO.getDTOByID(parentId);
//        allSubTypes.add(parent);
//        parentId = parent.parentId;
//    }
//    Collections.reverse(allSubTypes);
    String displayStyleForMinusSubTypeButton = allSubTypes.isEmpty() ? "display: none" : "";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Procurement_goodsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>
                                    <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                                           value='<%=procurement_goodsDTO.iD%>' tag='pb_html'/>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='procurementPackageId'
                                                    id='procurementPackageId_<%=i%>' tag='pb_html'>
                                                <%
                                                    Procurement_packageDAO procurement_packageDAO = Procurement_packageDAO.getInstance();
                                                    ProcurementGoodsTypeDAO procurementGoodsTypeDAO = ProcurementGoodsTypeDAO.getInstance();
                                                    ProcurementGoodsTypeDTO procurementGoodsTypeDTO = actionName.equals("edit") ? procurementGoodsTypeDAO.getDTOByID(procurement_goodsDTO.procurementGoodsTypeId) : new ProcurementGoodsTypeDTO();
                                                    Procurement_packageDTO procurement_packageDTO = actionName.equals("edit") ? procurement_packageDAO.getDTOByID(procurementGoodsTypeDTO.procurementPackageId) : new Procurement_packageDTO();
                                                    long defaultValue = actionName.equals("edit") ? procurement_packageDTO.iD : -1;
                                                    Options = CommonDAO.getOptions(Language, "procurement_package", procurement_packageDTO.iD);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class='form-control' name='procurementGoodsTypeId'
                                                    id='procurementGoodsTypeId_<%=i%>' tag='pb_html'>
                                                <%
                                                    defaultValue = actionName.equals("edit") ? procurementGoodsTypeDTO.iD : -1;
                                                    if (defaultValue != -1) {
                                                        Options = ProcurementGoodsTypeRepository.getInstance().getOptions(Language, defaultValue);
                                                    }
                                                    else {
                                                        Options = "";
                                                    }
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>



                                    <div class="singleItemFieldsDiv_container">
                                        <div class="singleItemFieldsDiv">
                                            <div class="form-group row">
                                                <div class="col-12 text-right">
                                                    <button
                                                            type="button"
                                                            onclick="deleteSubType(this);"
                                                            class="minusButton btn btn-sm btn-warning shadow btn-border-radius text-white pl-4"
                                                            style="<%=displayStyleForMinusSubTypeButton%>">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                    <button
                                                            type="button"
                                                            onclick="addMore(this)"
                                                            class="add-more btn btn-sm text-white add-btn shadow btn-border-radius">
                                                        <i class="fa fa-plus"></i>
                                                        <%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_ID, loginDTO)%>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="subType_container">
                                                <%
                                                    for (Procurement_goodsDTO parent: allSubTypes) {
                                                %>
                                                <div class="subType">


                                                    <div class="form-group row">

                                                        <label class="col-md-4 col-form-label subType-label text-md-right">
                                                            <%=LM.getText(LC.PROCUREMENT_GOODS_EDIT_NAMEEN, loginDTO)%>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-5">
                                                            <input type='text' class='form-control subTypeEn' name='subTypeEn' value="<%=parent.nameEn%>"
                                                                   tag='pb_html'/>
                                                            <label class="error-msg" style="color: red;display: none"></label>
                                                        </div>
                                                        <div class="col-md-3">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-4 col-form-label subType-label text-md-right">
                                                            <%=LM.getText(LC.PROCUREMENT_GOODS_EDIT_NAMEBN, loginDTO)%>
                                                            <span class="required"> * </span>
                                                        </label>
                                                        <div class="col-md-5">
                                                            <input type='text' class='form-control subTypeBn' name='subTypeBn' value="<%=parent.nameBn%>"
                                                                   tag='pb_html'/>
                                                            <label class="error-msg" style="color: red;display: none"></label>
                                                        </div>
                                                        <label class="col-md-2 col-form-label text-md-right">
                                                        </label>
                                                        <div class="col-md-1">
                                                        </div>
                                                    </div>

<%--                                                    <div class="form-group row">--%>

<%--                                                        <label class="col-md-4 col-form-label text-md-right">--%>
<%--                                                            <%=LM.getText(LC.PROCUREMENT_GOODS_EDIT_NAMEEN, loginDTO)%>--%>
<%--                                                        </label>--%>
<%--                                                        <div class="col-md-8">--%>
<%--                                                            <label class="col-md-4 col-form-label text-md-left"><%=parent.nameEn%></label>--%>
<%--                                                        </div>--%>

<%--                                                    </div>--%>
<%--                                                    <div class="form-group row">--%>

<%--                                                        <label class="col-md-4 col-form-label text-md-right">--%>
<%--                                                            <%=LM.getText(LC.PROCUREMENT_GOODS_EDIT_NAMEBN, loginDTO)%>--%>
<%--                                                        </label>--%>
<%--                                                        <div class="col-md-8">--%>
<%--                                                            <label class="col-md-4 col-form-label text-md-left"><%=parent.nameBn%></label>--%>
<%--                                                        </div>--%>

<%--                                                    </div>--%>

                                                </div>

                                                <%
                                                    }
                                                %>

                                            </div>


                                            <div class="item_container">

                                                <div class="form-group row">

                                                    <label class="col-md-4 col-form-label subType-label text-md-right">
                                                        <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEEN, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-5">
                                                        <input type='text' class='form-control subTypeEn' name='nameEn'
                                                               value='<%=procurement_goodsDTO.nameEn%>' tag='pb_html'/>
                                                        <label class="error-msg" style="color: red;display: none"></label>
                                                    </div>
                                                    <div class="col-md-3">
                                                    </div>

                                                </div>
                                                <div class="form-group row">

                                                    <label class="col-md-4 col-form-label subType-label text-md-right">
                                                        <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEBN, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-5">
                                                        <input type='text' class='form-control subTypeBn' name='nameBn'
                                                               value='<%=procurement_goodsDTO.nameBn%>' tag='pb_html'/>
                                                        <label class="error-msg" style="color: red;display: none"></label>
                                                    </div>
                                                    <label class="col-md-2 col-form-label text-md-right">
                                                        <%=UtilCharacter.getDataByLanguage(Language, "ফেরতযোগ্য", "returnable")%>
                                                    </label>
                                                    <div class="col-md-1">
                                                        <input type='checkbox' class='form-control-sm' <%=procurement_goodsDTO.returnable == 1 ? "checked" : ""%> name='returnableCheckbox' value="<%=procurement_goodsDTO.returnable == 1%>" tag='pb_html'/>
                                                        <input type='hidden' class='form-control' name='returnable' value="<%=procurement_goodsDTO.returnable == 1%>"/>
                                                    </div>

                                                </div>

                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_UNIT, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='piUnitId'
                                                            tag='pb_html'>
                                                        <%
                                                            Options = Pi_unitRepository.getInstance().getOptions(Language, procurement_goodsDTO.piUnitId);
                                                        %>
                                                        <%=Options%>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CENTRE_ADD_DESCRIPTION, loginDTO)%>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control' name='description'
                                                           value='<%=procurement_goodsDTO.description%>'
                                                           tag='pb_html'/>
                                                </div>
                                            </div>

                                            <input type='hidden' class='form-control' name='insertedByUserId'
                                                   value='<%=procurement_goodsDTO.insertedByUserId%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertedByOrganogramId'
                                                   value='<%=procurement_goodsDTO.insertedByOrganogramId%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='insertionDate'
                                                   value='<%=procurement_goodsDTO.insertionDate%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='isDeleted'
                                                   value='<%=procurement_goodsDTO.isDeleted%>' tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='lastModificationTime'
                                                   value='<%=procurement_goodsDTO.lastModificationTime%>'
                                                   tag='pb_html'/>
                                            <input type='hidden' class='form-control' name='searchColumn'
                                                   value='<%=procurement_goodsDTO.searchColumn%>' tag='pb_html'/>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 text-right">
                                            <button
                                                    type="button"
                                                    onclick="deleteItem(this);"
                                                    class="minusButtonItem btn btn-sm btn-warning shadow btn-border-radius text-white pl-4"
                                                    style="display: none">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <button
                                                    type="button"
                                                    onclick="addMoreItem(this)"
                                                    class="add-more-item btn btn-sm text-white add-btn shadow btn-border-radius">
                                                <i class="fa fa-plus"></i>
                                                <%=Language.equals("English") ? "Add more item" : "আইটেম যোগ করুন"%>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10 text-right">
                        <button type="button" id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                            <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PROCUREMENT_GOODS_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button class="btn-sm shadow text-white border-0 submit-btn ml-2" onclick="submitGoods()">
                            <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PROCUREMENT_GOODS_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>




<div id="template_subType" style="display: none">


    <div class="subType">

        <div class="form-group row">

            <label class="col-md-4 col-form-label subType-label text-md-right">
                <%=LM.getText(LC.PROCUREMENT_GOODS_EDIT_NAMEEN, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-5">
                <input type='text' class='form-control subTypeEn' name='subTypeEn'
                       tag='pb_html'/>
                <label class="error-msg" style="color: red;display: none"></label>
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-form-label subType-label text-md-right">
                <%=LM.getText(LC.PROCUREMENT_GOODS_EDIT_NAMEBN, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-5">
                <input type='text' class='form-control subTypeBn' name='subTypeBn'
                       tag='pb_html'/>
                <label class="error-msg" style="color: red;display: none"></label>
            </div>
            <label class="col-md-2 col-form-label text-md-right">
            </label>
            <div class="col-md-1">
            </div>
        </div>

    </div>


</div>

<div id="template_singleItemFieldsDiv" style="display: none">
    <div class="singleItemFieldsDiv">
        <div class="form-group row">
            <div class="col-12 text-right">
                <button
                        type="button"
                        onclick="deleteSubType(this);"
                        class="minusButton btn btn-sm btn-warning shadow btn-border-radius text-white pl-4"
                        style="display: none">
                    <i class="fa fa-minus"></i>
                </button>
                <button
                        type="button"
                        onclick="addMore(this)"
                        class="add-more btn btn-sm text-white add-btn shadow btn-border-radius">
                    <i class="fa fa-plus"></i>
                    <%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_ID, loginDTO)%>
                </button>
            </div>
        </div>
        <div class="subType_container">


        </div>
        <div class="item_container">
            <div class="form-group row">
                <label class="col-md-4 col-form-label subType-label text-md-right">
                    <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEEN, loginDTO)%>
                    <span class="required"> * </span>
                </label>
                <div class="col-md-5">
                    <input type='text' class='form-control subTypeEn' name='nameEn'
                           tag='pb_html'/>
                    <label class="error-msg" style="color: red;display: none"></label>
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 col-form-label subType-label text-md-right">
                    <%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEBN, loginDTO)%>
                    <span class="required"> * </span>
                </label>
                <div class="col-md-5">
                    <input type='text' class='form-control subTypeBn' name='nameBn'
                           tag='pb_html'/>
                    <label class="error-msg" style="color: red;display: none"></label>
                </div>
                <label class="col-md-2 col-form-label text-md-right">
                    <%=UtilCharacter.getDataByLanguage(Language, "ফেরতযোগ্য", "returnable")%>
                </label>
                <div class="col-md-1">
                    <input type='checkbox' class='form-control-sm' name='returnableCheckbox' value="false" tag='pb_html'/>
                    <input type='hidden' class='form-control' name='returnable' value="true"/>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_UNIT, loginDTO)%>
                <span class="required"> * </span>
            </label>
            <div class="col-md-8">
                <select class='form-control' name='piUnitId'
                        tag='pb_html'>
                    <%
                        Options = Pi_unitRepository.getInstance().getOptions(Language, procurement_goodsDTO.piUnitId);
                    %>
                    <%=Options%>
                </select>

            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.CENTRE_ADD_DESCRIPTION, loginDTO)%>
            </label>
            <div class="col-md-8">
                <input type='text' class='form-control' name='description'
                       tag='pb_html'/>
            </div>
        </div>

        <input type='hidden' class='form-control' name='insertedByUserId'
               tag='pb_html'/>
        <input type='hidden' class='form-control' name='insertedByOrganogramId'
               tag='pb_html'/>
        <input type='hidden' class='form-control' name='insertionDate'
               tag='pb_html'/>
        <input type='hidden' class='form-control' name='isDeleted'
               tag='pb_html'/>
        <input type='hidden' class='form-control' name='lastModificationTime'
               tag='pb_html'/>
        <input type='hidden' class='form-control' name='searchColumn'
               tag='pb_html'/>

        <div class="form-group row">
            <div class="col-12 text-right">
                <button
                        type="button"
                        onclick="deleteItem(this);"
                        class="minusButtonItem btn btn-sm btn-warning shadow btn-border-radius text-white pl-4"
                        >
                    <i class="fa fa-minus"></i>
                </button>
<%--                <button--%>
<%--                        type="button"--%>
<%--                        onclick="addMoreItem(this)"--%>
<%--                        class="add-more-item btn btn-sm text-white add-btn shadow btn-border-radius">--%>
<%--                    <i class="fa fa-plus"></i>--%>
<%--                    <%=Language.equals("English") ? "Add more item" : "আইটেম যোগ করুন"%>--%>
<%--                </button>--%>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let requiredMsg;

    function resetErrorMsg(){
        var inp = document.getElementsByClassName("error-msg");
        for ( var i = 0; i < inp.length; ++i )    {
            inp[i].style.display = 'none';
        }
    }

    function preprocessGoodsCheckBoxBeforeSubmitting(fieldname) {
        var checkboxes = document.getElementsByName(fieldname + 'Checkbox');
        var hiddens = document.getElementsByName(fieldname);
        for (var i=0;i<checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                hiddens[i].value = "1";
            } else {
                hiddens[i].value = "0";
            }
        }
    }

    function validateString(){
        var foundErr = false;
        var fields = ['subTypeEn', 'subTypeBn'];

        for ( var field = 0; field < fields.length; ++field )    {
            var inp = document.getElementById('bigform').getElementsByClassName(fields[field]);
            for ( var i = 0; i < inp.length; ++i )    {
                if (!(inp[i].value && inp[i].value != undefined && inp[i].value.toString().trim().length > 0)) {
                    inp[i].parentNode.childNodes.item(3).style.display = 'block';
                    inp[i].parentNode.childNodes.item(3).innerText = requiredMsg;
                    foundErr = true;
                }
            }
        }

        return !foundErr;
    }

    function addMore(addMoreButton)
    {
        const templateSubType = document.getElementById('template_subType').cloneNode(true);
        $(addMoreButton).closest('.singleItemFieldsDiv').find('.subType_container').append($(templateSubType).html());
        $(addMoreButton).prev(".minusButton").show();
    }

    function deleteSubType(minusButton) {
        $(minusButton).closest('.singleItemFieldsDiv').find('.subType').last().remove();
    }

    function addMoreItem(addMoreButton)
    {
        const templateSingleItemFieldsDiv = document.getElementById('template_singleItemFieldsDiv').cloneNode(true);
        // $(addMoreButton).closest('.singleItemFieldsDiv_container').append($(templateSingleItemFieldsDiv).html());
        $('.singleItemFieldsDiv_container').append($(templateSingleItemFieldsDiv).html());
    }

    function deleteItem(minusButton) {
        $(minusButton).closest('.singleItemFieldsDiv').remove();
    }

    function processResponse(data){

        let alertMsg;
        if (data.includes('Invalid Input')) {
            alertMsg = "<%=Language.equals("English") ? "Sorry, missing required field!" : "দুঃখিত, আবশ্যকীয় তথ্য প্রদান করুন "%>";
            bootbox.alert(alertMsg, function (result) {});
        }
        else if (data.includes('Not English')) {
            alertMsg = "<%=Language.equals("English") ? "Please give english name" : "ইংরেজি নাম প্রদান করুন "%>";
            bootbox.alert(alertMsg, function (result) {});
        }
        else if (data.includes('Not Bangla')) {
            alertMsg = "<%=Language.equals("English") ? "Please give bangla name" : "বাংলা নাম প্রদান করুন "%>";
            bootbox.alert(alertMsg, function (result) {});
        }
        else if (data.includes('Child Exists')) {
            alertMsg = "<%=Language.equals("English") ? "Child exists, parent name not updated" : "চাইল্ড বিদ্যমান থাকায় প্যারেন্ট এর নাম অপরিবর্তিত রাখা হলো "%>";
            bootbox.alert(alertMsg
                , function (result) {
                    window.location = 'Procurement_goodsServlet?actionType=search';
                });
        }
        else if (data.includes('App exists')) {
            alertMsg = "<%=Language.equals("English") ? "App exists with this item, can not be updated" : "বার্ষিক ক্রয় পরিকল্পনা বিদ্যমান থাকায় আইটেমটি হালনাগাদ করা যাচ্ছে না "%>";
            bootbox.alert(alertMsg
                , function (result) {
                    window.location = 'Procurement_goodsServlet?actionType=search';
                });
        }
        else {
            // window.location = 'Procurement_goodsServlet?actionType=search&filter=1&report_name_ajax=' + data;
            showToastSticky("সাবমিট সফল হয়েছে","Submit Successful");
            setTimeout(() => {
                window.location = 'Procurement_goodsServlet?actionType=search';
            }, 3000);
        }
    }

    function ajaxPost(url, accept, send, postData, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: accept,
            contentType: false,
            processData: false,
            data: postData,
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


    function submitGoods() {
        event.preventDefault();

        // $('#tableForm').submit();
        resetErrorMsg();
        let form = $("#bigform");
        form.validate();
        let valid = form.valid();
         valid = valid && validateString();

        if (valid) {
            const form = $("#bigform");

            preprocessGoodsCheckBoxBeforeSubmitting('returnable');
            var actionUrl =  form.attr("action");
            // var postData =  (form.serialize());

            let singleItemList = document.getElementById('bigform').getElementsByClassName('singleItemFieldsDiv');

            let singleItemObjectList = [];

            $.each( singleItemList, function( index, singleItem ) {
                let elementList = $(singleItem).find('input, select, textarea');
                let singleItemObject = {};
                let subTypeEn = [];
                let subTypeBn = [];
                $.each(elementList, function( elementIndex, element ) {
                    if (element.name.includes('subTypeEn')) {
                        subTypeEn.push($(element).val());
                        // singleItemObject[element.name + '[' + subTypeIndex + ']'] = $(element).val();
                        // $(element).attr('name', element.name + '[' + subTypeIndex + ']');
                    }
                    else if (element.name.includes('subTypeBn')) {
                        subTypeBn.push($(element).val());
                        // singleItemObject[element.name + '[' + subTypeIndex + ']'] = $(element).val();
                        // $(element).attr('name', element.name + '[' + subTypeIndex++ + ']');
                    }
                    else {
                        singleItemObject[element.name] = $(element).val();
                    }
                });

                singleItemObject['subTypeEn'] = subTypeEn;
                singleItemObject['subTypeBn'] = subTypeBn;
                singleItemObject['procurementPackageId'] = $('#procurementPackageId_0').val();
                singleItemObject['procurementGoodsTypeId'] = $('#procurementGoodsTypeId_0').val();
                if (index == 0) singleItemObject['iD'] = $('#iD_hidden_0').val();

                singleItemObjectList.push(singleItemObject);
            });



            // const postData = new FormData(form[0]);

            let formData = new FormData();
            formData.append('itemList', JSON.stringify(singleItemObjectList));

            ajaxPost(actionUrl, "json", "application/x-www-form-urlencoded", formData, processResponse, processResponse);
        }

    }

    function PreprocessBeforeSubmiting(row, validate) {

        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Procurement_goodsServlet");
    }

    function init(row) {

        $('#procurementPackageId_0').change(function () {
            showOrHideProcurementGoodsType();
        });

        $.validator.addMethod('procurementPackage', function (value, element) {
            value = parseInt(value);
            return value > 0;
        });
        $.validator.addMethod('procurementGoodsType', function (value, element) {
            value = parseInt(value);
            return value > 0;
        });
        $.validator.addMethod('validSelector', function (value, element) {
            value = parseInt(value);
            return value > -1;
        });

        let lang = '<%=Language%>';

        if(lang == 'English'){
            requiredMsg = 'This is required';

        }
        else {
            requiredMsg = '<%="এই তথ্যটি আবশ্যক"%>';

        }


        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                procurementPackageId: {
                    procurementPackage: true,
                },
                procurementGoodsTypeId: {
                    procurementGoodsType: true,
                },
                piUnitId: {
                    validSelector: true,
                },
            },

            messages: {
                procurementPackageId: requiredMsg,
                procurementGoodsTypeId: requiredMsg,
                piUnitId: requiredMsg,
            }
        });

    }

    function showOrHideProcurementGoodsType() {
        if ($('#procurementPackageId_0').val() && $('#procurementPackageId_0').val() > 0) {
            loadProcurementGoodsType($('#procurementPackageId_0').val());

        } else {
            $('#procurementGoodsTypeId_0').val(null);

        }

    }

    function loadProcurementGoodsType(packageId) {

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {
                    $('#procurementGoodsTypeId_0').html(this.responseText);
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_packageServlet?actionType=getByPackageId&ID=" + packageId, true);

        xhttp.send();

    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        CKEDITOR.replaceAll();

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });

    var child_table_extra_id = <%=childTableStartingID%>;


</script>





<style>
    .required {
        color: red;
    }
</style>
