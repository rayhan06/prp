<!-- Modal -->
<%--
  Author: Moaz Mahmud
  USAGE: see disciplinary_log/disciplinary_logEditBody.jsp
--%>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="user.UserRepository" %>
<%@page import="user.UserDTO" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="util.CommonConstant" %>
<%@ page import="procurement_goods.Procurement_goodsDTO" %>
<%@ page import="procurement_goods.Procurement_goodsDAO" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    String Language = userDTO.languageID == CommonConstant.Language_ID_English ? "English" : "Bangla";
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String isHierarchyNeededStr = request.getParameter("isHierarchyNeeded");
    boolean isHierarchyNeeded = isHierarchyNeededStr != null && Boolean.parseBoolean(isHierarchyNeededStr);
    String modalTitle = request.getParameter("modalTitle");
    if (modalTitle == null) {
        modalTitle = LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_SEARCH_FORMNAME, userDTO);
    }

    Procurement_goodsDTO procurement_goodsDTO = new Procurement_goodsDTO();
    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
//    String ID = request.getParameter("ID");
//    if (ID != null) {
//        long id = Long.parseLong(ID);
//        procurement_goodsDTO = procurement_goodsDAO.getDTOByID(id);
//    }

    String index = request.getParameter("index");
    int i = 0;
    if (index != null) {
        i = Integer.parseInt(index);
    }

    String packageOptions = Procurement_packageRepository.getInstance().getOptions(Language, procurement_goodsDTO.procurementPackageId);
%>

<div class="modal fade bd-example-modal-xl" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true" id="search_proc_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <%--------------------------------HEADER----------------------------------------------%>
            <div class="modal-header">
                <h4 class="caption" style="color: #56b2cf;">
                    <%=modalTitle%>
                </h4>
            </div>

            <%--------------------------------BODY----------------------------------------------%>
            <div class="modal-body modal-lg">
                <%@include file="procurementNewGoodsModalBody.jsp" %>
            </div>

            <%--------------------------------FOOTER----------------------------------------------%>
            <div class="modal-footer border-0">
                <button type="button" class="btn submit-btn text-white shadow btn-border-radius"
                        onclick="updateSelectedItem(1)">
                    <%=LM.getText(LC.GLOBAL_SUBMIT, loginDTO)%>
                </button>
                <button type="button" class="btn cancel-btn text-white shadow btn-border-radius"
                        onclick="updateSelectedItem(0)">
                    <%=LM.getText(LC.CLOSE_CLOSE, loginDTO)%>
                </button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var subTypeCount = <%=index%>;
    let itemSelected = false;
    let itemData = new Map();
    let modalItemData = new Map();


    $('#search_proc_modal').on('show.bs.modal', function () {
        event.preventDefault();

        $("#procurementPackageId_<%=index%>").val(-1);
        $("#procurementGoodsTypeId_<%=index%>").html('');
        $("#subType").html('');

        if ($('#procurementGoodsId').val() !== '') {
            //$('#procurementGoodsId').html('');
            $('#parent_id_hidden').val('');
        }

        itemSelected = false;
    });

    function loadTableFromManualSearch(parentIdHidden) {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    lastItemFromProcurementModal = totalResponse.procurementGoodsResponseDTO;

                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByParent&ID=" + parentIdHidden + "&index=" + 0, false);
        xhttp.send();
    }

    function updateSelectedItem(updateSelected) {
        // let manualSearch = document.getElementById('procurementGoodsId').value;
        // let parentIdHidden = document.getElementById('parent_id_hidden').value;
        // if(manualSearch !== null && manualSearch !== '' && updateSelected === 1){
        //
        //     loadTableFromManualSearch(parentIdHidden);
        //
        //     selectedItemFromProcurementModal = lastItemFromProcurementModal;
        //     if (necessaryCollectionForProcurementModal.callBackFunction) {
        //         necessaryCollectionForProcurementModal.callBackFunction(modalItemData);
        //     }
        //
        //     $('#search_proc_modal').modal('hide');
        // }

        if (updateSelected === 1 && !itemSelected) {
            var language = '<%=Language%>';
            var reqMSg = language == 'Bangla' ? "কোনো আইটেম বাছাই করা হয়নি" : "No item selected";
            bootbox.alert(reqMSg, function (result) {
            });
        } else {
            if (updateSelected === 1) {
                selectedItemFromProcurementModal = lastItemFromProcurementModal;
                if (necessaryCollectionForProcurementModal.callBackFunction) {
                    necessaryCollectionForProcurementModal.callBackFunction(modalItemData);
                }
            }
            $('#search_proc_modal').modal('hide');
        }
    }

    function setModalTableData(data) {
        itemSelected = true;
        data.forEach((item, index) => {
            if (!modalItemData.has(+item.itemId)) {
                modalItemData.set(+item.itemId, item);
                setEachItemInTr('modalAppendTbody', item, index);
            }
        });
    }

    function setEachItemInTr(tbodyId, item, index) {
        let {
            itemId,
            itemName,
            subTypes,
            unit,
            unitId,
            description,
            procurementPackageId,
            procurementGoodsTypeId,
            isSelected,
        } = item;

        let trContent = document.querySelector('.template-item-list-of-group-type-modal').cloneNode(true);
        trContent.classList.remove('hiddenTr');
        trContent.classList.replace('template-item-list-of-group-type-modal', 'appendedTr');

        trContent.querySelector('.groupTypeIdCls').value = procurementPackageId;
        trContent.querySelector('.groupTypeIdCls').name = 'piPackageItemMapChild.itemGroupId';

        trContent.querySelector('.itemTypeIdCls').value = procurementGoodsTypeId;
        trContent.querySelector('.itemTypeIdCls').name = 'piPackageItemMapChild.itemTypeId';

        trContent.querySelector('.itemIdCls').value = itemId;
        trContent.querySelector('.itemIdCls').name = 'piPackageItemMapChild.itemId';

        trContent.querySelector('.unitIdCls').value = unitId;
        trContent.querySelector('.unitIdCls').name = 'piPackageItemMapChild.piUnitId';

        trContent.querySelector('.itemTdCls').innerText = itemName;
        trContent.querySelector('.subTypeTdCls').innerText = subTypes;
        trContent.querySelector('.unitTdCls').innerText = unit;
        trContent.querySelector('.descriptionTdCls').innerText = description;
        trContent.querySelector('.isSelectedTdCls').innerHTML = '<button type="button" onclick="removeItemFromModal(this)" class="btn d-flex align-items-center" value="" style="color: #ff6a6a"><i class="fa fa-trash"></i></button>';

        $("#" + tbodyId).append(trContent);
    }

    function removeItemFromModal(selectedElement) {
        let parentTr = selectedElement.parentNode.parentNode;
        modalItemData.delete(+(parentTr.querySelector('.itemIdCls').value));
        itemData.delete(+(parentTr.querySelector('.itemIdCls').value));
        $(parentTr).remove();
    }

    function initProcurementGoodsModal(row) {
        $('#procurementPackageId_<%=i%>').change(function () {
            showOrHideProcurementGoodsType();
        });

        $('#procurementGoodsTypeId_<%=i%>').change(function () {
            //loadProcurementGoodsByType(this.value);
            loadAllItems($('#procurementPackageId_<%=i%>').val(), this.value);
        });
    }

    function loadAllItems(itemGroupId, itemTypeId) {
        itemSelected = false;
        let procurementPackageId = itemGroupId;
        let procurementGoodsTypeId = itemTypeId;
        let actionType = 'getItemWiseRow';

        let param = 'actionType=' + actionType;
        param += '&procurementPackageId=' + procurementPackageId;
        param += '&procurementGoodsTypeId=' + procurementGoodsTypeId;

        if (procurementPackageId !== null && procurementGoodsTypeId !== '') {
            fullPageLoader.show();
            const url = 'Procurement_goodsServlet?' + param;
            let response = getItemResponse(url);
            setModalTableData(response);
        }
    }

    function getItemResponse(url) {
        let res = [];
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (response) {
                if (response !== null) {
                    res = response;
                }
                fullPageLoader.hide();
            },
            error: function (error) {
                fullPageLoader.hide();
            }
        });
        return res;
    }

    function showOrHideProcurementGoodsType() {
        if ($('#procurementPackageId_<%=i%>').val() && $('#procurementPackageId_<%=i%>').val() > 0) {
            loadProcurementGoodsType($('#procurementPackageId_<%=i%>').val());

        } else {
            $('#procurementGoodsTypeId_<%=i%>').val(null);

        }

    }

    function loadProcurementGoodsType(packageId) {

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {
                    $('#procurementGoodsTypeId_<%=i%>').html(this.responseText);
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_packageServlet?actionType=getByPackageId&ID=" + packageId, false);

        if (packageId != undefined && packageId != null && packageId != '') xhttp.send();

    }


    function loadProcurementGoodsByType(goodsTypeId) {

        $("#subType").html('');

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    var options = totalResponse.options;

                    $("#subType").append(options);
                    updateLabels();

                    $('#subType_select_' + subTypeCount).change(function () {
                        loadProcurementGoodsByParent(this);
                    });

                    subTypeCount++;
                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByGoodsType&ID=" + goodsTypeId + "&index=" + subTypeCount, true);

        if (goodsTypeId != undefined && goodsTypeId != null && goodsTypeId != '') xhttp.send();

    }


    function loadProcurementGoodsByParent(element) {
        var nextIndexes = parseInt(element.id.toString().substring(15)) + 1;
        subTypeCount = nextIndexes;
        itemSelected = true;

        while ($("#subType_select_" + nextIndexes).val() != undefined && $("#subType_select_" + nextIndexes).val() != null) {
            $("#div_id_" + nextIndexes).remove();
            nextIndexes++;
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    var options = totalResponse.options;
                    lastItemFromProcurementModal = totalResponse.procurementGoodsResponseDTO;

                    $("#subType").append(options);
                    updateLabels();

                    $('#subType_select_' + subTypeCount).change(function () {
                        loadProcurementGoodsByParent(this);
                    });

                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByParent&ID=" + element.value + "&index=" + subTypeCount, true);

        if (element.value != undefined && element.value != null && element.value != '') xhttp.send();
    }

    var selectedItemFromProcurementModal = new Map;
    var lastItemFromProcurementModal = new Map;
    var necessaryCollectionForProcurementModal = {};

    $(document).ready(function () {
        initProcurementGoodsModal('0');
        select2SingleSelector('#procurementGoodsId', '<%=Language%>');

    });

    function onProcurementGoodsChange(selected) {
        itemSelected = true;
        let procurementGoodsId = selected.value;
        const url = 'Procurement_goodsServlet?actionType=getGoodsDtoResponseById&procurementGoodsId=' + procurementGoodsId;
        let res = [];
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (response) {
                res = response;
                document.getElementById('procurementPackageId_0').value = response.packageId;
                //loadProcurementGoodsType(packageId);
                document.getElementById('procurementGoodsTypeId_0').value = response.procurementGoodsTypeId;
                document.getElementById('parent_id_hidden').value = procurementGoodsId;
            },
            error: function (error) {
                console.log(error);
            }
        });

        if (res.length !== 0) {
            let {packageId, procurementGoodsTypeId, procurementGoodsParentId} = res;
            getSingleItemWiseResponse(packageId, procurementGoodsTypeId, procurementGoodsId);

        }
    }

    function getSingleItemWiseResponse(itemGroupId, itemTypeId, itemId) {
        // let piPackageNewId = document.querySelector('#piPackageNewId').value;
        let procurementPackageId = itemGroupId;
        let procurementGoodsTypeId = itemTypeId;

        let actionType = 'getItemWiseRow';

        let param = 'actionType=' + actionType;
        // param += '&piPackageNewId=' + piPackageNewId;
        param += '&procurementPackageId=' + procurementPackageId;
        param += '&procurementGoodsTypeId=' + procurementGoodsTypeId;

        const url = 'Procurement_goodsServlet?' + param;
        let response = getItemResponse(url);
        response = response.filter((res) => +res.itemId === +itemId);
        if (response.length > 0) {
            setModalTableData(response);
        }
    }

    function updateLabels() {
        let goodsLabels = document.getElementById('subType').getElementsByClassName('col-form-label');

        let totalLabels = goodsLabels.length;
        for (let i = 0; i < totalLabels - 1; i++) {
            goodsLabels[i].innerHTML = "<%=LM.getText(LC.PROCUREMENT_GOODS_ADD_ID, loginDTO)%>";
        }
        goodsLabels[totalLabels - 1].innerHTML = "<%=LM.getText(LC.GATE_PASS_VIEW_ITEM_NAME, loginDTO)%>";
    }


    function resetModal() {
        modalItemData.clear();
        $('#procurementPackageId_0').val('');
        $('#procurementGoodsId').val('');
        $('#procurementGoodsTypeId_0').val('');
        $('#modalAppendTbody').empty();
    }


</script>