<%@page pageEncoding="UTF-8" %>

<%@page import="procurement_goods.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_PROCUREMENT_GOODS;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Procurement_goodsDTO procurement_goodsDTO = (Procurement_goodsDTO)request.getAttribute("procurement_goodsDTO");
CommonDTO commonDTO = procurement_goodsDTO;
String servletName = "Procurement_goodsServlet";


System.out.println("procurement_goodsDTO = " + procurement_goodsDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Procurement_goodsDAO procurement_goodsDAO = (Procurement_goodsDAO)request.getAttribute("procurement_goodsDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

											<td id = '<%=i%>_procurementPackageId'>

													<%=procurement_goodsDTO.reportName%>

											</td>

											<td>
												<button class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="event.preventDefault();goodsSearchByButton('<%=procurement_goodsDTO.reportName%>')">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>




<input type='hidden' class='form-control'  name='ID' id = 'id_hidden_<%=i%>' value='<%=procurement_goodsDTO.iD%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=procurement_goodsDTO.insertedByUserId%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=procurement_goodsDTO.insertedByOrganogramId%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=procurement_goodsDTO.insertionDate%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=procurement_goodsDTO.isDeleted%>' tag='pb_html'/>

<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=procurement_goodsDTO.lastModificationTime%>' tag='pb_html'/>
<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=procurement_goodsDTO.searchColumn%>' tag='pb_html'/>
