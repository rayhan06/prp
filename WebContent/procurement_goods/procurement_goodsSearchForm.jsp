
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="procurement_goods.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.text.SimpleDateFormat"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="util.StringUtils" %>
<%@ page import="java.util.*" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<%
	String context = "../../.." + request.getContextPath() + "/";

	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Procurement_goodsDAO procurement_goodsDAO = (Procurement_goodsDAO)request.getAttribute("procurement_goodsDAO");


String navigator2 = SessionConstants.NAV_PROCUREMENT_GOODS;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";

String ajax = request.getParameter("ajax");
boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>	

<%

if(hasAjax == false)
{
	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) 
	{

		String paramName = parameterNames.nextElement();
	   
		if(!paramName.equalsIgnoreCase("actionType"))
		{
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) 
			{
				String paramValue = paramValues[i];
				
				%>
				
				<%
				
			}
		}
	   

	}
}

%>

				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
<%--								<th style="display: none"></th>--%>
								<th><%=LM.getText(LC.REGISTER_LEASE_BMD_SEARCH_SERIALNUMBER, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%></th>
								<th><%=Language.equals("English") ? "Sub-type" : "সাব-টাইপ"%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEBN, loginDTO)%></th>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_QUANTITY, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_UNITPRICE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.YEARLY_PROCUREMENT_REPORT_SELECT_ESTCOST, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_METHODANDTYPECAT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_APPROVINGAUTHORITY, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_SOURCEOFFUNDCAT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TIMECODE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDER, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDEROPENING, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_TENDEREVALUATION, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_APPROVALTOWARD, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_AWARDNOTIFICATION, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_SIGININGOFCONTRACT, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_CONTRACTSIGNATURETIME, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_CONTRACTCOMPLETIONTIME, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th><%=LM.getText(LC.PROCUREMENT_GOODS_SEARCH_PROCUREMENT_GOODS_EDIT_BUTTON, loginDTO)%></th>
								<th class="text-center">
									<span class="">All</span>
									<div class="d-flex align-items-center justify-content-between">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>&nbsp;
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_PROCUREMENT_GOODS);
								int childTableStartingID = 0;

								try
								{

									if (data != null) 
									{

										List<Procurement_goodsDTO> procurement_goodsDTOList = (List<Procurement_goodsDTO>) data;

										String parentIdListCommaSeparated =
												procurement_goodsDTOList
												.stream()
												.filter(procurement_goodsDTO -> StringUtils.isValidString(procurement_goodsDTO.parentIdsCommaSeparated))
												.map(procurement_goodsDTO -> procurement_goodsDTO.parentIdsCommaSeparated)
												.collect(Collectors.joining(","));

										HashMap<Long, String> parentIdToName = new HashMap<>();

										if (StringUtils.isValidString(parentIdListCommaSeparated)) {

											List<Procurement_goodsDTO> parentDTOs = Procurement_goodsRepository.getInstance().getDTOsByIds(
													Arrays.stream(parentIdListCommaSeparated.split(","))
													.map(parentId -> Long.parseLong(parentId))
													.collect(Collectors.toList())
											);


											parentDTOs
													.forEach(parent -> {
														String name = Language.equals("English") ? parent.nameEn : parent.nameBn;
														parentIdToName.put(parent.iD, name);
													});

										}

										int size = data.size();
										childTableStartingID = size;
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Procurement_goodsDTO procurement_goodsDTO = (Procurement_goodsDTO) data.get(i);

											String subType = Arrays.stream(procurement_goodsDTO.parentIdsCommaSeparated.split(","))
													.filter(parentId -> StringUtils.isValidString(parentId))
													.map(parentId -> parentIdToName.getOrDefault(Long.parseLong(parentId), ""))
													.collect(Collectors.joining(" -> "));

											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("procurement_goodsDTO",procurement_goodsDTO);
								%>  
								
								 <jsp:include page="./procurement_goodsSearchRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 		<jsp:param name="subType" value="<%=subType%>" />
								 </jsp:include>

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
										request.setAttribute("procurement_goodsDTO",null);
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type='hidden' class='form-control'  name='idsForDelete' id = 'idsForDelete' value='' tag='pb_html'/>
<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


<jsp:include page="procurementGoodsModal.jsp">
	<jsp:param name="index" value="1"/>
</jsp:include>


<script src="<%=context%>/assets/report/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/jszip.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/pdfmake.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/vfs_fonts.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/buttons.html5.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/buttons.print.min.js" type="text/javascript"></script>
<script src="<%=context%>/assets/report/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript">





	var child_table_extra_id = <%=childTableStartingID%>;

function loadModal() {
	$('#search_proc_modal').modal();
}

	necessaryCollectionForProcurementModal = {
		callBackFunction: function (item) {
			console.log(item);
			console.log(selectedItemFromProcurementModal);
			console.log(lastItemFromProcurementModal);
		}
	};

</script>