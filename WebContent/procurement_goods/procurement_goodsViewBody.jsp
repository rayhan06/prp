<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="procurement_goods.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="procurement_package.*" %>
<%@ page import="pi_unit.Pi_unitRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="util.StringUtils" %>


<%
    String servletName = "Procurement_goodsServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.PROCUREMENT_GOODS_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Procurement_goodsDAO procurement_goodsDAO = Procurement_goodsDAO.getInstance();
    Procurement_goodsDTO procurement_goodsDTO = Procurement_goodsRepository.getInstance().getProcurement_goodsDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    String returnableString = Language.equals("English") ? "Returnable" : "ফেরতযোগ্য";
    String nonReturnableString = Language.equals("English") ? "Not Returnable" : "ফেরতযোগ্য নয়";

    List<Procurement_goodsDTO> allSubTypes = new ArrayList<>();


    if (StringUtils.isValidString(procurement_goodsDTO.parentIdsCommaSeparated)) {

        allSubTypes = Procurement_goodsRepository.getInstance().getDTOsByIds(
                Arrays.stream(procurement_goodsDTO.parentIdsCommaSeparated.split(","))
                        .map(parentId -> Long.parseLong(parentId))
                        .collect(Collectors.toList()));

    }


//    long parentId = procurement_goodsDTO.parentId;
//    while (parentId != -1) {
//        Procurement_goodsDTO parent = procurement_goodsDAO.getDTOByID(parentId);
//        allSubTypes.add(parent);
//        parentId = parent.parentId;
//    }
//    Collections.reverse(allSubTypes);

    List<String> subTypeList = new ArrayList<>(1);

    int totalSubTypeSize = allSubTypes.size();
    Procurement_goodsDTO actualItem = procurement_goodsDTO;
    for (int subTypeIndex =0; subTypeIndex < totalSubTypeSize ; subTypeIndex++) {

        if (Language.equals("English")) {
            subTypeList.add(allSubTypes.get(subTypeIndex).nameEn);
        }
        else {
            subTypeList.add(allSubTypes.get(subTypeIndex).nameBn);
        }

    }

    String subTypeString = subTypeList.isEmpty() ? "" : String.join(" -> ", subTypeList);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.HM_ITEM, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title">
                <%=LM.getText(LC.HM_ITEM, loginDTO)%>
            </h5>
            <div>
                <table class="table table-bordered table-striped">

                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_PACKAGE_ADD_PROCUREMENT_PACKAGE_ADD_FORMNAME, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                Procurement_packageDAO procurement_packageDAO = Procurement_packageDAO.getInstance();
                                ProcurementGoodsTypeDAO procurementGoodsTypeDAO = ProcurementGoodsTypeDAO.getInstance();
                                ProcurementGoodsTypeDTO procurementGoodsTypeDTO = ProcurementGoodsTypeRepository.getInstance().getProcurementGoodsTypeDTOByID(procurement_goodsDTO.procurementGoodsTypeId);
                                Procurement_packageDTO procurement_packageDTO = Procurement_packageRepository.getInstance().getProcurement_packageDTOByID(procurementGoodsTypeDTO.procurementPackageId);
                                value = Procurement_packageRepository.getInstance().getName(Language, procurement_packageDTO.iD) + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                    <tr>
                        <td><b><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = ProcurementGoodsTypeRepository.getInstance().getName(Language, procurementGoodsTypeDTO.iD) + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>

                    <%

                        String label = LM.getText(LC.PROCUREMENT_GOODS_ADD_ID, loginDTO);


                        if (!subTypeString.isEmpty()) {
                    %>


                    <tr>
                        <td ><b><%=label%></b></td>
                        <td>

                            <%
                                value = subTypeString;

                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>



                    <%
                        }

                        label = Language.equals("English") ? LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEEN, loginDTO) : LM.getText(LC.PROCUREMENT_GOODS_ADD_NAMEBN, loginDTO);
                    %>



                    <tr>
                    								<td ><b><%=label%></b></td>
                    								<td>

                    											<%
                                                                    value = Language.equals("English") ? actualItem.nameEn : actualItem.nameBn;
                                                                    String returnableFlag = actualItem.returnable == 1 ? returnableString : nonReturnableString;
                                                                    value += "(" + returnableFlag + ")";

                    											%>

                    											<%=Utils.getDigits(value, Language)%>


                    								</td>

                    							</tr>







                    <tr>
                        <td><b><%=LM.getText(LC.VM_FUEL_VENDOR_ADD_VM_FUEL_VENDOR_ITEM_UNIT, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = Pi_unitRepository.getInstance().getName(Language, procurement_goodsDTO.piUnitId);
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>

                    <tr>
                        <td><b><%=LM.getText(LC.CENTRE_ADD_DESCRIPTION, loginDTO)%>
                        </b></td>
                        <td>

                            <%
                                value = procurement_goodsDTO.description + "";
                            %>

                            <%=Utils.getDigits(value, Language)%>


                        </td>

                    </tr>


                </table>
            </div>
        </div>
    </div>
</div>