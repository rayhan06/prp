<%@page import="login.LoginDTO" %>
<%@page import="am_house.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.*" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDTO" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>

<%
    String servletName = "Am_houseServlet";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    String actionName;
    Am_houseDTO am_houseDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        am_houseDTO = (Am_houseDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        am_houseDTO = new Am_houseDTO();
    }
    actionName = "bulk_house_allocation";
    String formTitle = LM.getText(LC.AM_HOUSE_ADD_AM_HOUSE_ADD_FORMNAME, loginDTO);
    int i = 0;
    Am_house_allocation_requestDTO am_house_allocation_requestDTO = new Am_house_allocation_requestDTO();

%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              id="bigform" name="bigform">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-8 offset-md-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_EMPLOYEERECORDSID, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <!-- Button trigger modal -->
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius"
                                                    id="employeeRecordId_modal_button"
                                                    onclick="employeeRecordIdModalBtnClicked();">
                                                <%=LM.getText(LC.LANGUAGE_SELECT, loginDTO)%>
                                            </button>

                                            <div class="input-group" id="employeeRecordId_div"
                                                 style="display: none">
                                                <input type="hidden" name='employeeRecordId'
                                                       id='employeeRecordId_input'
                                                       value="">
                                                <button type="button" class="btn btn-secondary form-control"
                                                        disabled
                                                        id="employeeRecordId_text"></button>
                                                <span class="input-group-btn" style="width: 5%" tag='pb_html'>
														<button type="button" class="btn btn-outline-danger"
                                                                onclick="crsBtnClicked('employeeRecordId');"
                                                                id='employeeRecordId_crs_btn' tag='pb_html'>
															x
														</button>
													</span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEOLDNEWCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' onchange="loadHouse()" name='amHouseOldNewCat' id = 'amHouseOldNewCat_category_<%=i%>'   tag='pb_html'>
                                                <%
                                                    String Options = CatRepository.getInstance().buildOptions("am_house_old_new", Language, am_house_allocation_requestDTO.amHouseOldNewCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSELOCATIONCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' onchange="loadHouse()" name='amHouseLocationCat' id = 'amHouseLocationCat_category_<%=i%>'   tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_house_location", Language, am_house_allocation_requestDTO.amHouseLocationCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSECLASSCAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' onchange="loadHouse();loadHouseSubClass()" name='amHouseClassCat' id = 'amHouseClassCat_category_<%=i%>'   tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getInstance().buildOptions("am_house_class", Language, am_house_allocation_requestDTO.amHouseClassCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী(প্রাপ্যতা অনুযায়ী)", "House Sub Class(According to Availability) ")%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='amHouseClassSubCat'
                                                    id='amHouseClassSubCat_category_<%=i%>' tag='pb_html'>
                                                <%
                                                    int defaultValue = -1;
                                                    if ("edit".equals(request.getParameter("actionType"))) {
                                                        defaultValue = am_house_allocation_requestDTO.amHouseClassSubCat;
                                                    }
                                                    Options = Am_house_type_sub_categoryRepository.getInstance().buildOptionsByHouseClass(Language,
                                                            am_house_allocation_requestDTO.amHouseClassCat, defaultValue);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_REQUEST_ADD_AMHOUSEID, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='amHouseId' id = 'amHouseId_select_<%=i%>'   tag='pb_html'>

                                            </select>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_HOUSE_ADD_AM_HOUSE_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="submit" onclick="submitForm()">
                                <%=LM.getText(LC.AM_HOUSE_ADD_AM_HOUSE_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>

<script type="text/javascript">

    const houseForm = $("#bigform");

    function loadHouse() {
        var oldNew = $('#amHouseOldNewCat_category_0').val();
        var location = $('#amHouseLocationCat_category_0').val();
        var houseClass = $('#amHouseClassCat_category_0').val();

        if (
            houseClass != undefined && houseClass.toString().trim().length > 0 && houseClass != '-1'
        ){
            <%--let url = "Am_grade_house_mappingServlet?actionType=getApplyForHigherClass&classCat="+ houseClass+--%>
            <%--    '&id='+'<%=requesterEmpId%>';--%>
            <%--ajaxGet(url, processHouseGradeResponse, processHouseGradeResponse);--%>
        }

        if (
            oldNew != undefined && oldNew.toString().trim().length > 0 && oldNew != '-1' &&
            location != undefined && location.toString().trim().length > 0 && location != '-1' &&
            houseClass != undefined && houseClass.toString().trim().length > 0 && houseClass != '-1'
        )
        {
            let url = "Am_houseServlet?actionType=getActiveHouse&newCat=" + oldNew
                + "&locCat="+ location
                + "&classCat="+ houseClass
                + "&defaultValue=" + '<%=am_house_allocation_requestDTO.amHouseId%>';
            ajaxGet(url, processHouseResponse, processHouseResponse);
        }
    }


    function loadHouseSubClass(){
        let $house = $("#HouseId_select_<%=i%>");
        let $houseSubClass = $("#amHouseClassSubCat_category_<%=i%>")
        let houseClass = document.getElementById('amHouseClassCat_category_0').value;
        if (houseClass === '') {
            $house.html("");
            return;
        }

        let url = "Am_house_type_sub_categoryServlet?actionType=getHouseSubClass&houseClass=" + houseClass;
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            success: function (fetchedData) {
                const response = JSON.parse(fetchedData);
                if (response && response.responseCode === 0) {
                    $('#toast_message').css('background-color', '#ff6063');
                    showToastSticky(response.msg, response.msg);
                } else if (response && response.responseCode === 200) {
                    $houseSubClass.html(response.msg);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
            }
        });
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function PreprocessBeforeSubmiting() {
        houseForm.validate();
        return houseForm.valid();
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting()) {
            $.ajax({
                type: "POST",
                url: "Am_house_allocation_approval_mappingServlet?actionType=<%=actionName%>",
                data: houseForm.serialize(),
                dataType: 'JSON',
                success: function (response) {
                    if (response.responseCode === 0) {
                        $('#toast_message').css('background-color', '#ff6063');
                        showToastSticky(response.msg, response.msg);
                        buttonStateChange(false);
                    } else if (response.responseCode === 200) {
                        showToastSticky("বাসা সেভ করা হয়েছে", "House saved");
                        setTimeout(() => {
                            window.location.replace(getContextPath() + response.msg);
                        }, 1500);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                        + ", Message: " + errorThrown);
                    buttonStateChange(false);
                }
            });
        } else {
            buttonStateChange(false);
        }
    }
    function processHouseResponse(data){
        document.getElementById('amHouseId_select_0').innerHTML = data.msg;
    }


    $(document).ready(function () {
        loadHouse();
        convertToSelect2();
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value !== -1 && value.toString().trim().length > 0;
        });

        let lang = '<%=Language%>';
        let divErr, locErr, classErr, houseNumberErr;
        if (lang.toUpperCase() === 'ENGLISH') {
            houseNumberErr = 'House Number Required';
            divErr = 'New House/ Old House Required';
            locErr = 'House Location Required';
            classErr = 'House Class Required';
        } else {
            houseNumberErr = 'বাসার নম্বর প্রয়োজনীয়';
            divErr = 'নতুন বাসা/পুরাতন বাসা প্রয়োজনীয়';
            locErr = 'বাসার অবস্থান প্রয়োজনীয়';
            classErr = 'বাসার শ্রেণী প্রয়োজনীয়';
        }

        houseForm.validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                houseNumber: {
                    required: true,
                },
                amHouseOldNewCat: {
                    validSelector: true,
                },
                amHouseLocationCat: {
                    validSelector: true,
                },
                amHouseClassCat: {
                    validSelector: true,
                },
            },
            messages: {
                houseNumber: houseNumberErr,
                amHouseOldNewCat: divErr,
                amHouseLocationCat: locErr,
                amHouseClassCat: classErr,
            }
        });
    });

    function convertToSelect2(){
        select2SingleSelector('#amHouseOldNewCat_category_<%=i%>','<%=Language%>');
        select2SingleSelector('#amHouseLocationCat_category_<%=i%>','<%=Language%>');
        select2SingleSelector('#amHouseClassCat_category_<%=i%>','<%=Language%>');
        select2SingleSelector('#amHouseClassSubCat_category_<%=i%>','<%=Language%>');
        select2SingleSelector('#amHouseId_select_<%=i%>','<%=Language%>');
    }

    modal_button_dest_table = 'none';

    function employeeRecordIdModalBtnClicked() {
        modal_button_dest_table = 'employeeRecordId';
        $('#search_emp_modal').modal();
    }

    function crsBtnClicked(fieldName) {
        displayClass('none');
        $('#' + fieldName + '_modal_button').show();
        $('#' + fieldName + '_div').hide();
        $('#' + fieldName + '_input').val('');
        document.getElementById(fieldName + '_text').innerHTML = '';
        let accordions = document.querySelectorAll('.accordion-populated');
        [...accordions].forEach(accordion => {
            accordion.innerHTML = '';
        });
    }

    function employeeRecordIdInInput(empInfo) {


        $('#employeeRecordId_modal_button').hide();
        $('#employeeRecordId_div').show();

        let language = '<%=Language.toLowerCase()%>'
        let designation;
        if (language === 'english') {
            designation = empInfo.employeeNameEn + ' (' + empInfo.organogramNameEn + ', ' + empInfo.officeUnitNameEn + ')';
        } else {
            designation = empInfo.employeeNameBn + ' (' + empInfo.organogramNameBn + ', ' + empInfo.officeUnitNameBn + ')';
        }
        document.getElementById('employeeRecordId_text').innerHTML = designation;
        //$('#employeeRecordId_input').val(empInfo.employeeRecordId);
        $('#employeeRecordId_input').val(empInfo.employeeRecordId);
    }

    table_name_to_collcetion_map = new Map([
        ['employeeRecordId', {
            isSingleEntry: true,
            callBackFunction: employeeRecordIdInInput
        }]
    ]);


</script>






