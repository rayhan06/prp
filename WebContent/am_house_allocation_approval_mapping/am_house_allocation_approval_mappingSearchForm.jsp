
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="am_house_allocation_approval_mapping.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="java.util.List" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDTO" %>
<%@ page import="am_house_allocation_request.Am_house_allocation_requestDAO" %>


<%
String navigator2 = "navAM_HOUSE_ALLOCATION_APPROVAL_MAPPING";
String servletName = "Am_house_allocation_approval_mappingServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped text-nowrap">
						<thead>
							<tr>
								<th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_REQUESTEREMPID, loginDTO)%></th>
								<th><%=LM.getText(LC.EMPLOYEE_ACR_REPORT_SELECT_OFFICEUNITORGANOGRAMID, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_ROUTE_TRAVEL_WITHDRAW_ADD_OFFICE, loginDTO)%></th>
								<th><%=LM.getText(LC.VM_FUEL_REQUEST_REPORT_WHERE_APPLICATIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_CLASS_OF_HOUSING_UNDER_APPLICATION, loginDTO)%></th>
<%--								<th><%=UtilCharacter.getDataByLanguage(Language, "বাসার উপশ্রেণী", "House Sub Class")%></th>--%>
								<th><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
								<th colspan="3"><%=LM.getText(LC.HM_UPDATE, loginDTO)%></th>

							</tr>
						</thead>

						<tbody>

						<%
							List<Am_house_allocation_approval_mappingDTO> data = (List<Am_house_allocation_approval_mappingDTO>) rn2.list;
							int i = -1;
							if (data != null && data.size() > 0) {
								i++;
								for (Am_house_allocation_approval_mappingDTO am_house_allocation_approval_mappingDTO : data) {

									Am_house_allocation_requestDTO am_house_allocation_requestDTO =  Am_house_allocation_requestDAO.getInstance().getDTOFromID(am_house_allocation_approval_mappingDTO.amHouseAllocationRequestId);
						%>

							<%if(am_house_allocation_requestDTO!=null){%>
								<tr>
									<%@include file="am_house_allocation_approval_mappingSearchRow.jsp" %>
								</tr>
							<%}%>

						<% }
						} %>



						</tbody>


					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />

<script>


	function rejectApplication(amHouseAllocationRequestId){


		let msg = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_REASON_FOR_DISSATISFACTION, loginDTO)%>';
		let placeHolder = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_TYPE_DISSATISFACTION_REASON_HERE, loginDTO)%>';
		let confirmButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_YES_DISSATISFIED, loginDTO)%>';
		let cancelButtonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_CARD_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>';
		let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';

		dialogMessageWithTextBoxWithoutAnimation(msg,placeHolder,confirmButtonText,cancelButtonText,emptyReasonText,(reason)=>{

			submitRejectForm(reason,amHouseAllocationRequestId);
		},()=>{});
	}

	function submitRejectForm(reason,amHouseAllocationRequestId){

		console.log('here amOfficeAssignmentRequestId: '+amHouseAllocationRequestId+" reason: "+reason);

		$.ajax({
			type : "POST",
			url : "Am_house_allocation_approval_mappingServlet?actionType=rejectApplicationSearchRow",
			data:{"amHouseAllocationRequestId":amHouseAllocationRequestId,"reject_reason":reason},
			dataType : 'JSON',
			success : function(response) {
				if(response.responseCode === 0){
					$('#toast_message').css('background-color','#ff6063');
					showToastSticky(response.msg,response.msg);
					//buttonStateChange(false);
				}else if(response.responseCode === 200){
					window.location.replace(getContextPath()+response.msg);
					//updateApprovalMappingTable();
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
						+ ", Message: " + errorThrown);
				buttonStateChange(false);
			}
		});

	}


</script>


			