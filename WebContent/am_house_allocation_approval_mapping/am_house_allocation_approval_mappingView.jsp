<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="static permission.MenuConstants.ASSET_MANAGEMENT" %>
<%@ page import="static permission.MenuConstants.AM_HOUSE_ALLOCATION" %>
<%@ page import="static permission.MenuConstants.*" %>
<%
String context = "../../.."  + request.getContextPath() + "/";
	request.setAttribute("menuIDPath", new ArrayList<>(Arrays.asList(ASSET_MANAGEMENT,
			AM_HOUSE_ALLOCATION,
			AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SEARCH)));

%>
<link href="<%=context%>/assets/css/pagecustom.css" rel="stylesheet" type="text/css"/>
<jsp:include page="../common/layout.jsp" flush="true">
<jsp:param name="title" value="View" />
	<jsp:param name="body" value="../am_house_allocation_approval_mapping/am_house_allocation_approval_mappingViewBody.jsp" />
</jsp:include>

