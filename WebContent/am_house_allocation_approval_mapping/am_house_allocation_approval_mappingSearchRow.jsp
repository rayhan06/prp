<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="util.CommonDTO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="am_house_allocation_request.AmHouseAllocationRequestStatus" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="am_house.Am_houseRepository" %>
<%@ page import="am_house.Am_houseDTO" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryRepository" %>
<%@ page import="am_house_type_sub_category.Am_house_type_sub_categoryDTO" %>


<td>
    <%=UtilCharacter.getDataByLanguage(Language, am_house_allocation_requestDTO.requesterNameBn, am_house_allocation_requestDTO.requesterNameEn)%>
</td>

<td>
    <%=UtilCharacter.getDataByLanguage(Language, am_house_allocation_requestDTO.requesterOfficeUnitOrgNameBn, am_house_allocation_requestDTO.requesterOfficeUnitOrgNameEn)%>
</td>

<td>
    <%=UtilCharacter.getDataByLanguage(Language, am_house_allocation_requestDTO.requesterOfficeUnitNameBn, am_house_allocation_requestDTO.requesterOfficeUnitNameEn)%>
</td>

<td>
    <%=StringUtils.getFormattedDate(Language, am_house_allocation_requestDTO.insertionDate)%>
</td>

<td>
    <%
        String houseType = CatRepository.getInstance().getText(Language, "am_house_class", am_house_allocation_requestDTO.amHouseClassCat);
        Am_houseDTO am_houseDTO = Am_houseRepository.getInstance().getAm_houseDTOByID(am_house_allocation_requestDTO.amHouseId);
        Am_house_type_sub_categoryDTO houseSubTypeDTO = Am_house_type_sub_categoryRepository.getInstance()
                .getAm_house_type_sub_categoryDTOByiD(am_house_allocation_requestDTO.amHouseClassSubCat);
        String houseSubType = houseSubTypeDTO == null ? "null" :
                Language == "English" ? houseSubTypeDTO.amHouseSubCatNameEn : houseSubTypeDTO.amHouseSubCatNameBn;
        if (am_houseDTO != null) {
            String houseNumber = am_houseDTO.houseNumber;
            if(houseSubType.equals("null")){%>
            <%=houseType%>-<%= houseNumber%>
        <%} else{%>
            <%=houseType%>-<%=houseSubType%> Sub Type-<%= houseNumber%>
    <%
            }
        }
    %>

</td>

<td>
    <%
        int status = am_house_allocation_approval_mappingDTO.amHouseAllocationStatusCat;
    %>
    <span class="btn btn-sm border-0 shadow"
          style="background-color: <%=AmHouseAllocationRequestStatus.getColor(status)%>; color: white; border-radius: 8px;cursor: text">
            <%=CatRepository.getInstance().getText(
                    Language, "am_house_allocation_status", status
            )%>
    </span>
</td>

<%CommonDTO commonDTO = am_house_allocation_approval_mappingDTO; %>


<td>
    <button
            type="button"
            class="btn-sm border-0 shadow bg-light btn-border-radius"
            style="color: #ff6b6b;"
            onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=am_house_allocation_requestDTO.iD%>'"
    >
        <i class="fa fa-eye"></i>
    </button>


</td>

<%if (am_house_allocation_requestDTO.status == CommonApprovalStatus.REQUISITION_APPROVED_BY_FIRST_APPROVER.getValue()) {%>

<td>
    <button
            type="button"
            class="btn-sm border-0 shadow "
            style="background-color: #228B22;color: white;border-radius: 8px;cursor: pointer;"
            onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=am_house_allocation_requestDTO.iD%>'"
    >
        <%=LM.getText(LC.EMPLOYEE_ATTENDANCE_SEARCH_APPROVE, loginDTO)%>
    </button>
</td>
<td>
    <button
            type="button"
            class="btn-sm border-0 shadow "
            style="background-color: #1E90FF;color: white;border-radius: 8px;cursor: pointer;"
            onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=am_house_allocation_requestDTO.iD%>'"
    >
        <%=LM.getText(LC.HM_PDF, loginDTO)%>
    </button>
</td>
<td>
    <button
            type="button"
            class="btn-sm border-0 shadow "
            style="background-color: #B22222;color: white;border-radius: 8px;cursor: pointer;"
            onclick="rejectApplication(<%=am_house_allocation_requestDTO.iD%>);"
            id="reject_btn_<%=i%>"
    >
        <%=LM.getText(LC.USER_ADD_CANCEL, loginDTO)%>
    </button>
</td>

<%} else if (am_house_allocation_requestDTO.status == CommonApprovalStatus.SATISFIED.getValue()) {%>

<td>
</td>
<td>
    <button
            type="button"
            class="btn-sm border-0 shadow "
            style="background-color: #1E90FF;color: white;border-radius: 8px;cursor: pointer;"
            onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=am_house_allocation_requestDTO.iD%>'"
    >
        <%=LM.getText(LC.HM_PDF, loginDTO)%>
    </button>
</td>
<td>

</td>

<%
} else if (am_house_allocation_requestDTO.status == CommonApprovalStatus.DISSATISFIED.getValue() ||
        am_house_allocation_requestDTO.status == CommonApprovalStatus.WITHDRAWN.getValue() ||
        am_house_allocation_requestDTO.status == CommonApprovalStatus.PENDING.getValue() ) {
%>
<td>
</td>
<td>
    <button
            type="button"
            class="btn-sm border-0 shadow "
            style="background-color: #1E90FF;color: white;border-radius: 8px;cursor: pointer;"
            onclick="location.href='<%=servletName%>?actionType=getApprovalPage&amHouseAllocationRequestId=<%=am_house_allocation_requestDTO.iD%>'"
    >
        <%=LM.getText(LC.HM_PDF, loginDTO)%>
    </button>
</td>

<td>
</td>

<%}%>








