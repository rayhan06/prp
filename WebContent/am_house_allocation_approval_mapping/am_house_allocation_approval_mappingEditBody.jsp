<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="am_house_allocation_approval_mapping.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Am_house_allocation_approval_mappingDTO am_house_allocation_approval_mappingDTO = new Am_house_allocation_approval_mappingDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	am_house_allocation_approval_mappingDTO = Am_house_allocation_approval_mappingDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = am_house_allocation_approval_mappingDTO;
String tableName = "am_house_allocation_approval_mapping";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_FORMNAME, loginDTO);
String servletName = "Am_house_allocation_approval_mappingServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Am_house_allocation_approval_mappingServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='amHouseAllocationRequestId' id = 'amHouseAllocationRequestId_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.amHouseAllocationRequestId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='cardApprovalId' id = 'cardApprovalId_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.cardApprovalId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_SEQUENCE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(am_house_allocation_approval_mappingDTO.sequence != -1)
																	{
																	value = am_house_allocation_approval_mappingDTO.sequence + "";
																	}
																%>		
																<input type='number' class='form-control'  name='sequence' id = 'sequence_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_AMHOUSEALLOCATIONSTATUSCAT, loginDTO)%></label>
                                                            <div class="col-8">
																<select class='form-control'  name='amHouseAllocationStatusCat' id = 'amHouseAllocationStatusCat_category_<%=i%>'   tag='pb_html'>		
																<%
																	Options = CatRepository.getInstance().buildOptions("am_house_allocation_status", Language, am_house_allocation_approval_mappingDTO.amHouseAllocationStatusCat);
																%>
																<%=Options%>
																</select>
	
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='taskTypeId' id = 'taskTypeId_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.taskTypeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.employeeRecordsId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approverEmployeeRecordsId' id = 'approverEmployeeRecordsId_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.approverEmployeeRecordsId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_REJECTIONREASON, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='rejectionReason' id = 'rejectionReason_text_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.rejectionReason%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.insertionDate%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_INSERTEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='insertedBy' id = 'insertedBy_text_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.insertedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_MODIFIEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.modifiedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=am_house_allocation_approval_mappingDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.lastModificationTime%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=am_house_allocation_approval_mappingDTO.searchColumn%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_ADD_AM_HOUSE_ALLOCATION_APPROVAL_MAPPING_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Am_house_allocation_approval_mappingServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






