<script>

    function getCheckedNodesFromJsTree( treeId ){

        return $( "#" + treeId ).jstree( "get_checked", null, true );
    }

</script>