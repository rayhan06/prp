<%@ page import="tree.JSTreeConfig" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.HashSet" %>

<%
    String lang = "Bangla";
    if (request.getParameter(JSTreeConfig.LANGUAGE.toString()) != null) {

        lang = request.getParameter(JSTreeConfig.LANGUAGE.toString());
    }

    String treeId = "jsTree";
    if (request.getParameter(JSTreeConfig.TREE_ID.toString()) != null) {

        treeId = request.getParameter(JSTreeConfig.TREE_ID.toString());
    }

    Set<String> plugins = new HashSet<>(Arrays.asList("changed", "checkbox", "massload", "search", "state", "wholerow"));

    if (request.getParameterValues(JSTreeConfig.ADD_PLUGIN.toString()) != null) {

        plugins = new HashSet<>(Arrays.asList(request.getParameterValues(JSTreeConfig.ADD_PLUGIN.toString())));
    }

    if (request.getParameterValues(JSTreeConfig.REMOVE_PLUGIN.toString()) != null) {

        for (String plugin : request.getParameterValues(JSTreeConfig.REMOVE_PLUGIN.toString())) {

            plugins.remove(plugin.toLowerCase());
        }
    }

    StringBuilder sb = new StringBuilder();
    for (String plugin : plugins) {
        sb.append("'");
        sb.append(plugin.toLowerCase());
        sb.append("'");
        sb.append(",");
    }

    if (sb.length() > 0) {

        sb.setLength(sb.length() - 1);
    }

    Boolean showCheckBoxAtLeafOnly = false;

    if (request.getParameter(JSTreeConfig.SHOW_CHECKBOX_AT_LEAF_ONLY.toString()) != null) {

        showCheckBoxAtLeafOnly = Boolean.parseBoolean(request.getParameter(JSTreeConfig.SHOW_CHECKBOX_AT_LEAF_ONLY.toString()));
    }

    Boolean ajaxSearch = true;

    if (request.getParameter(JSTreeConfig.AJAX_SEARCH.toString()) != null) {

        ajaxSearch = Boolean.parseBoolean(request.getParameter(JSTreeConfig.AJAX_SEARCH.toString()));
    }
%>

<%if (showCheckBoxAtLeafOnly) {%>

<style>
    .no_checkbox > i.jstree-checkbox {
        display: none;
    }
</style>

<%}%>

<div class="">

    <div class="">

        <%if (ajaxSearch == true) {%>
        <div class="">

            <div class="mt-4">

                <label> Search: </label> &nbsp;

                <input type="text" id="<%=treeId%>_search" name="<%=treeId%>_search"/>

            </div>

        </div>
        <%}%>

        <div class="mt-3">

            <div class="">

                <div id="<%=treeId%>"></div>

            </div>

        </div>

    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $("#<%=treeId%>").jstree(
            {
                plugins: [
                    <%=sb.toString()%>
                ],
                search: {
                    show_only_matches: true,
                    show_only_matches_children: true,
                    search_leaves_only: true,
                    ajax: {
                        url: '<%=request.getContextPath()%>/treeServlet?actionType=search'
                    }
                },
                core: {
                    data: {
                        url: '<%=request.getContextPath()%>/treeServlet?actionType=getNodes',
                        data: function (node) {

                            nodeType = typeof node.a_attr != 'undefined' ? node.a_attr.nodeType : "OFFICE_UNIT";
                            return {
                                id: node.id,
                                nodeType: nodeType,
                                lang: '<%=lang%>',
                                showCheckboxAtLeaf: <%=showCheckBoxAtLeafOnly%>
                            };
                        }
                    },
                    themes: {
                        dots: true
                    }
                }
            }
        );

        let <%=treeId%>_to = false;
        $("#<%=treeId%>_search").on("keyup", function () {

            if (<%=treeId%>_to) {
                clearTimeout(<%=treeId%>_to);
            }

            <%=treeId%>_to = setTimeout(function () {

                $('#<%=treeId%>').jstree(true).search($("#<%=treeId%>_search").val())
            }, 250);
        });
    });


</script>