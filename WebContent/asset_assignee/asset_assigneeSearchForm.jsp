<%@page import="sessionmanager.SessionConstants"%>
<%@page contentType="text/html;charset=utf-8" pageEncoding="UTF-8"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="asset_model.*"%>
<%@page import="asset_category.*"%>
<%@ page import="util.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Date"%>
<%@ page import="pb.*"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="employee_offices.EmployeeOfficeRepository" %>
<%@ page import="employee_records.Employee_recordsRepository" %>


<%
    String navigator2 = "navASSET_ASSIGNEE";
    String servletName = "Asset_modelServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, loginDTO)%></th>
            <th><%=LM.getText(LC.HM_MODEL, loginDTO)%></th>
            <th><%=isLanguageEnglish ? "Assignee" : "অধিকারপ্রাপ্ত কর্মচারী/কর্মকর্তা"%></th>
            <th><%=LM.getText(LC.ASSET_MODEL_ADD_BRANDTYPE, loginDTO)%></th>
            <th><%=LM.getText(LC.ASSET_MODEL_ADD_SL, loginDTO)%></th>
            <th><%=LM.getText(LC.HM_MAC, loginDTO)%></th>
         
            <th><%=LM.getText(LC.HM_STATUS, loginDTO)%></th>
            <th><%=LM.getText(LC.HM_ACTION, loginDTO)%></th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute("viewASSET_ASSIGNEE");

            try
            {

                if (data != null)
                {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++)
                    {
                        AssetAssigneeDTO assetAssigneeDTO = (AssetAssigneeDTO) data.get(i);
                        Asset_categoryDTO asset_categoryDTO = Asset_categoryRepository.getInstance().getAsset_categoryDTOByID(assetAssigneeDTO.assetCategoryType);


        %>
        <tr>


            <td>
                <%=isLanguageEnglish ? asset_categoryDTO.nameEn : asset_categoryDTO.nameBn%>
            </td>

            <td>
                <%=assetAssigneeDTO.model%>
            </td>

            <td>
                <%
                    if (assetAssigneeDTO.assignmentStatus == AssetAssigneeDTO.NOT_ASSIGNED) {
                %>

                <%=isLanguageEnglish ? "Not Assigned" : "অবন্টনকৃত সম্পদ"%>
                <%
                } else {
                %>
                <%=WorkflowController.getNameFromOrganogramId(assetAssigneeDTO.assignedOrganogramId, Language)%>
                <%
                    }
                %>
            </td>

            <td>
                <%=CommonDAO.getName(assetAssigneeDTO.brandType, "brand", Language.equals("English")?"name_en":"name_bn", "id")%>
            </td>

            <td>
                <%=Utils.getDigits(assetAssigneeDTO.sl, Language)%>
            </td>
            
            <td>
                <%=Utils.getDigits(assetAssigneeDTO.mac, Language)%>
            </td>



            <td>
                <%=CatDAO.getName(Language, "assignment_status", assetAssigneeDTO.assignmentStatus)%>
            </td>

            <td>
            <%
            if(userDTO.roleID == SessionConstants.TICKET_ADMIN_ROLE || userDTO.roleID == SessionConstants.ADMIN_ROLE)
            {
            %>

                <%@include file="../asset_model/assignmentFunctionalityButton.jsp"%>
                <%
            }
                %>
            </td>

            <td>
                <button
                        type="button"
                        class="btn-sm border-0 shadow bg-light btn-border-radius"
                        style="color: #ff6b6b;"
                        onclick="location.href='<%=servletName%>?actionType=viewAssetAssignee&id=<%=assetAssigneeDTO.iD%>'">
                    <i class="fa fa-eye"></i>
                </button>
            </td>

        </tr>
        <%
                    }

                    System.out.println("printing done");
                }
                else
                {
                    System.out.println("data  null");
                }
            }
            catch(Exception e)
            {
                System.out.println("JSP exception " + e);
            }
        %>



        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />

<script type="text/javascript">
    <%@include file="../asset_model/showHideDivs.jsp"%>
    <%@include file="../asset_model/assignmentStatusFunctionality.jsp"%>
</script>
			