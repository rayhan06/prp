<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@ page import="pb.*"%>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page pageEncoding="UTF-8" %>
<%@include file="../employee_assign/employeeSearchModal.jsp"%>

<%
    System.out.println("Inside nav.jsp");
    String url = "Asset_modelServlet?actionType=searchAssetAssignee";
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    UserDTO navUserDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    if (rn == null) {
        rn = (RecordNavigator) request.getAttribute("recordNavigator");
    }
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    boolean isLanguageEnglishNav = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text' class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield'  name='anyfield'
                       value = '<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSETCATEGORYTYPE, loginDTO)%></label>
                        <div class="col-md-8">
                            <select class='form-control'  name='asset_category_type' id = 'asset_category_type' onSelect='setSearchChanged()'>
                                <%
                                    Options = CommonDAO.getOptions(Language, "select", "asset_category", "assetCategoryType_select_" + row, "form-control", "assetCategoryType", "any" );
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_BRANDTYPE, loginDTO)%></label>
                        <div class="col-md-8">
                            <select class='form-control'  name='brand_type' id = 'brand_type' onSelect='setSearchChanged()'>
                                <%
                                    Options = CommonDAO.getOptions(Language, "select", "brand", "brandType_select_" + row, "form-control", "brandType", "any" );
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_NAMEEN, loginDTO)%></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="name_en" placeholder="" name="name_en" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_SL, loginDTO)%></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="sl" placeholder="" name="sl" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.ASSET_MODEL_ADD_ASSET_ASSIGNEE_MAC, loginDTO)%></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="mac" placeholder="" name="mac" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=isLanguageEnglishNav ? "Status" : "অবস্থা"%></label>
                        <div class="col-md-8">
                            <select class='form-control' name='assignment_status'
                                    id='assignment_status' onChange='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("assignment_status", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=isLanguageEnglishNav ? "Employee/Officer" : "কর্মচারী/কর্মকর্তা"%></label>
                        <div class="col-md-8">
                            <div>
                                <button type="button" class="btn btn-block submit-btn btn-border-radius text-white"
                                        onclick="addEmployee()"
                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                                </button>
                                <table class="table table-bordered table-striped">
                                    <tbody id="employeeToSet"></tbody>
                                </table>
                                <input class='form-control' type='hidden' name='employeeUnit' id='officeUnitType' value=''/>
                                <input class='form-control' type='hidden' name='assignedOrganogramId' id='assignedOrganogramId' value=''/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=isLanguageEnglishNav ? "TOE Status" : "টিওই অবস্থা"%></label>
                        <div class="col-md-8">
                            <select class='form-control' name='toe_status'
                                    id='toe_status' onChange='setSearchChanged()'>
                                <option value="" selected><%=isLanguageEnglishNav ? "Select" : "বাছাই করুন"%></option>
                                <option value="1"><%=isLanguageEnglishNav ? "Has TOE" : "টিওই আছে"%></option>
                                <option value="0"><%=isLanguageEnglishNav ? "No TOE" : "টিওই নেই"%></option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=isLanguageEnglishNav ? "Warranty Status" : "ওয়ারেন্টি অবস্থা"%></label>
                        <div class="col-md-8">
                            <select class='form-control' name='warranty_status'
                                    id='warranty_status' onChange='setSearchChanged()'>
                                <option value="" selected><%=isLanguageEnglishNav ? "Select" : "বাছাই করুন"%></option>
                                <option value="1"><%=isLanguageEnglishNav ? "Has Warranty" : "ওয়ারেন্টি আছে"%></option>
                                <option value="0"><%=isLanguageEnglishNav ? "No Warranty" : "ওয়ারেন্টি নেই"%></option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=isLanguageEnglishNav ? "Warranty From" : "ওয়ারেন্টি শুরু"%></label>
                            <div class="col-md-8">
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID"
                                               value="warranty_from_date_js"></jsp:param>
                                    <jsp:param name="LANGUAGE"
                                               value="<%=Language%>"></jsp:param>
                                </jsp:include>
                            </div>
                        <input type='hidden' id="warranty_from" name="warranty_from" value='' tag='pb_html'/>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=isLanguageEnglishNav ? "Warranty To" : "ওয়ারেন্টি শেষ"%></label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="warranty_to_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden' id="warranty_to" name="warranty_to" value='' tag='pb_html'/>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <label class="col-md-4 col-form-label"><%=isLanguageEnglishNav ? "Assignment Status" : "বন্টন অবস্থা"%></label>
                        <div class="col-md-8">
                            <select class='form-control' name='is_assigned'
                                    id='is_assigned' onChange='setSearchChanged()'>
                                <option value="" selected><%=isLanguageEnglishNav ? "Select" : "বাছাই করুন"%></option>
                                <option value="1"><%=isLanguageEnglishNav ? "Assigned" : "বন্টনকৃত"%></option>
                                <option value="0"><%=isLanguageEnglishNav ? "Not Assigned" : "অবন্টনকৃত"%></option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.HM_COST, loginDTO)%></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="cost" placeholder="" name="cost" onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0, 0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>

                </div>
            </div>
        </div>

    </div>

</div>

<div style="padding-right: 20px; padding-bottom: 20px">
	<button type="button"
	        class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
	        onclick="allfield_changed('',0, 1)"
	        style="background-color: #a8b194; float: right;">
	    <%=LM.getText(LC.HM_EXCEL, loginDTO)%>
	</button>
</div>

<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp"%>


<template id = "loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    function dosubmit(params)
    {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200)
            {
                document.getElementById('tableForm').innerHTML = this.responseText ;
                setPageNo();
                searchChanged = 0;
            }
            else if(this.readyState == 4 && this.status != 200)
            {
                alert('failed ' + this.status);
            }
        };


        xhttp.open("GET", "Asset_modelServlet?actionType=searchAssetAssignee&isPermanentTable=<%=isPermanentTable%>&" + params, true);


        xhttp.send();


        // Clear the table
        $("#employeeToSet tr").remove();
    }

    function allfield_changed(go, pagination_number, type)
    {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if($("#asset_category_type").val() != -1 && $("#asset_category_type").val() != "")
        {
            params +=  '&asset_category_type='+ $("#asset_category_type").val();
        }

        if($("#brand_type").val() != -1 && $("#brand_type").val() != "")
        {
            params +=  '&brand_type='+ $("#brand_type").val();
        }
        params +=  '&name_en='+ $('#name_en').val();

        params +=  '&sl='+ $('#sl').val();
        
        params +=  '&mac='+ $('#mac').val().replaceAll("-", "mmmm");

        params +=  '&cost='+ $('#cost').val();

        params +=  '&assignment_status='+ $('#assignment_status').val();

        params +=  '&assignedOrganogramId='+ $('#assignedOrganogramId').val();

        if($("#toe_status").val() != "")
        {
            params +=  '&toe_status='+ $("#toe_status").val();
        }

        if($("#warranty_status").val() != "")
        {
            params +=  '&warranty_status='+ $("#warranty_status").val();
        }

        if($("#is_assigned").val() != "")
        {
            params +=  '&is_assigned='+ $("#is_assigned").val();
        }

        $("#warranty_from").val(getDateTimestampById("warranty_from_date_js"));
        if($('#warranty_from').val()) {
            params += '&warranty_from=' + $('#warranty_from').val();
        }

        $("#warranty_to").val(getDateTimestampById("warranty_to_date_js"));
        if($('#warranty_to').val()) {
            params += '&warranty_to=' + $('#warranty_to').val();
        }

        params +=  '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if(document.getElementById('hidden_totalrecords'))
        {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if(go !== '' && searchChanged == 0)
        {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;

        if(type == 0 || typeof(type) == "undefined")
		{
			dosubmit(params);
		}
		else
		{
			var url =  "Asset_modelServlet?actionType=xlAssignee&isPermanentTable=<%=isPermanentTable%>&" + params;
			window.location.href = url;
		}
    }

    function patient_inputted(userName, orgId) {
        console.log("patient_inputted " + userName);
        $("#assignedOrganogramId").val(orgId);
    }

</script>

