<%@page contentType="text/html;charset=utf-8" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="util.HttpRequestUtils" %>
<%
    String navigator = (String) request.getAttribute("navigator");
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;

    String pageno = "";

    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    if (rn == null) {
        rn = (RecordNavigator) request.getAttribute("recordNavigator");
    }
    String tableName = rn.m_tableName;
    String servletName = "Asset_modelServlet";

    if(request.getAttribute("navName") == null){
        request.setAttribute("navName", "../asset_assignee/asset_assigneeNav.jsp");
    }
    if(request.getAttribute("formName") == null){
        request.setAttribute("formName", "../asset_assignee/asset_assigneeSearchForm.jsp");
    }
    String url = servletName + "?actionType=searchAssetAssignee";
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    boolean isPermanentTable = rn.m_isPermanentTable;

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    int pagination_number = 0;
    String pageNamePrefix = tableName.toUpperCase() + "_SEARCH";
    String pageNameConstant = tableName.toUpperCase() + "_SEARCH_FORMNAME";
%>
<!-- begin:: Subheader -->
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <i class="fa fa-search fa-2x" style="color:#0098bf !important;"></i>&nbsp;
        <h3 class="kt-subheader__title" id="subHeader">
            &nbsp; <%=Language.equalsIgnoreCase("English") ? "Search Asset Assignee": "বন্টনকৃত সম্পদ খুজুন"%>
        </h3>
    </div>
</div>
<!-- end:: Subheader -->

<input type = "hidden" value = "<%=Language%>" name = "Language" id = "Language" />

<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content" style="background: white">
    <div class="row shadow-none border-0">
        <div class="col-lg-12">
            <jsp:include page="${navName}" flush="true">
                <jsp:param name="url" value="<%=url%>"/>
                <jsp:param name="navigator" value="<%=navigator%>"/>
                <jsp:param name="pageName"
                           value='<%=Language.equalsIgnoreCase("English") ? "Search Asset Assignee": "বন্টনকৃত সম্পদ খুজুন"%>'/>
            </jsp:include>
            <div style="height: 1px; background: #ecf0f5"></div>
            <div class="kt-portlet shadow-none">
                <div class="kt-portlet__body">
                    <form action="<%=servletName%>?isPermanentTable=<%=isPermanentTable%>&actionType=delete"
                          method="POST"
                          id="tableForm" enctype="multipart/form-data">
                        <jsp:include page="${formName}" flush="true">
                            <jsp:param name="pageName"
                                       value='<%=Language.equalsIgnoreCase("English") ? "Search Asset Assignee": "বন্টনকৃত সম্পদ খুজুন"%>'/>
                        </jsp:include>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <% pagination_number = 1;%>
    <%@include file="../common/pagination_with_go2.jsp" %>
</div>

<link href="<%=context%>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initDeleteCheckBoxes();
        dateTimeInit("<%=Language%>");

        select2SingleSelector("#asset_category_type", "<%=Language%>");
        select2SingleSelector("#brand_type", "<%=Language%>");
        select2SingleSelector("#toe_status", "<%=Language%>");
        select2SingleSelector("#warranty_status", "<%=Language%>");
        select2SingleSelector("#is_assigned", "<%=Language%>");
    });

</script>


