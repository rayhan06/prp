<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="brand.BrandRepository" %>
<%@ page import="asset_category.Asset_categoryRepository" %>
<%@ page import="asset_type.Asset_typeRepository" %>
<%@ page import="asset_model.*" %>
<%@ page import="asset_supplier.Asset_supplierRepository" %>

<%

    long assetAssigneeId = Long.parseLong(request.getParameter("asset_assignee_id"));
    AssetAssigneeDAO assetAssigneeDAO = new AssetAssigneeDAO();
    AssetAssigneeDTO assetAssigneeDTO = (AssetAssigneeDTO) assetAssigneeDAO.getDTOByID(assetAssigneeId);
    Asset_modelDAO asset_modelDAO = new Asset_modelDAO("asset_model");
    Asset_modelDTO asset_modelDTO = asset_modelDAO.getDTOByID(assetAssigneeDTO.assetModelId, false);

    Date today = new Date();

%>
<%@include file="../pb/addInitializer2.jsp" %>
<%
    String formTitle = isLanguageEnglish ? "RETURN ASSET FROM REPAIR" : "মেরামতের পর সম্পদ ফেরত";
    String servletName = "Asset_modelServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Asset_modelServlet?actionType=addReturnFromMaintenance&asset_assignee_id=<%=assetAssigneeDTO.iD%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Asset Name" : "সম্পদের নাম"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='modelName' id='modelName'
                                                   tag='pb_html' value='<%=isLanguageEnglish ? asset_modelDTO.nameEn : asset_modelDTO.nameBn%>' readonly/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Type" : "ধরণ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='assetType' id='assetType'
                                                   tag='pb_html' value='<%=Asset_typeRepository.getInstance().getAssetTypeText(asset_modelDTO.assetTypeId, isLanguageEnglish)%>' readonly/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Category" : "বিভাগ"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='category' id='category'
                                                   tag='pb_html' value='<%=Asset_categoryRepository.getInstance().getCategoryText(asset_modelDTO.assetCategoryType, isLanguageEnglish)%>' readonly/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Brand" : "ব্র্যান্ড"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='brand' id='brand'
                                                   tag='pb_html' value='<%=BrandRepository.getInstance().getBrandText(asset_modelDTO.brandType, isLanguageEnglish)%>' readonly/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Assignee" : "স্বত্ব-প্রাপ্ত"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='assignee' id='assignee'
                                                   tag='pb_html' value='<%=AssetAssigneeRepository.getInstance().getAssigneeText(assetAssigneeDTO.iD, isLanguageEnglish)%>' readonly/>
                                        </div>
                                    </div>

                                    <%
                                        if(assetAssigneeDTO.hasWarranty && today.getTime()<assetAssigneeDTO.expiryDate) {
                                    %>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Warranty" : "ওয়ারেন্টি"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='warranty' id='warranty'
                                                   tag='pb_html' value='<%=isLanguageEnglish ? "having warranty" : "আছে"%>' readonly/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Vendor" : "বিক্রেতা"%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vendor'
                                                    id='vendor_warranty' disabled>
                                                <%=Asset_supplierRepository.getInstance().getBuildOptions(Language, String.valueOf(asset_modelDTO.assetSupplierType))%>
                                            </select>
                                        </div>
                                    </div>

                                    <input type='hidden' class='form-control' name='isWarranty' id='isWarranty'
                                           value='1' tag='pb_html'/>
                                           
                                   <input type='hidden' class='form-control' name='cost' id='cost'
                                                   tag='pb_html' value='0'/>
                                           

                                    <%
                                    } else {
                                    %>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Warranty" : "ওয়ারেন্টি"%>
                                        </label>
                                        <div class="col-md-9">
                                            <input type='text' class='form-control' name='warranty' id='warranty_vendor'
                                                   tag='pb_html' value='<%=isLanguageEnglish ? "Warranty is not available" : "নেই"%>' readonly/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Vendor" : "বিক্রেতা"%>
                                            <span class="required"> * </span></label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vendor'
                                                    id='vendor' disabled>
                                                <%=Asset_supplierRepository.getInstance().getBuildOptions(Language, null)%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=Language.equalsIgnoreCase("English") ? "Cost" : "মেন্টেনেন্স বাবদ খরচ"%>
                                            <span class="required"> * </span></label>
                                        <div class="col-md-9">
                                            <input type='number' class='form-control' name='cost' id='cost'
                                                   tag='pb_html' value='0'/>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='isWarranty' id='isWarranty_vendor'
                                           value='0' >
                                    <%
                                        }
                                    %>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=Language.equalsIgnoreCase("English") ? "Comment" : "মন্তব্য"%>
                                        </label>
                                        <div class="col-md-9">
                                            <textarea class="form-control" name="comment" id="comment"></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.MINISTRY_OFFICE_MAPPING_ADD_MINISTRY_OFFICE_MAPPING_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=isLanguageEnglish ? "RECEIVE FROM REPAIR SHOP" : "মেরামতে পাঠানো সম্পদ গ্রহণ করুন"%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        select2SingleSelector("#vendor", "<%=Language%>");

        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
    });
</script>