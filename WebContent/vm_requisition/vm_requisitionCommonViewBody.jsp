<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_requisition.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>

<%@page import="files.*" %>

<%@ page import="geolocation.*" %>
<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleDAO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="employee_records.Employee_recordsDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="util.StringUtils" %>

<%@ page import="static util.UtilCharacter.*" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="vm_requisition_approver.Vm_requisition_approverDAO" %>
<%@ page import="vm_requisition_approver.Vm_requisition_approverDTO" %>

<%

    String pdfFileName = "Vehicle_requisition_form_"
            + simpleDateFormat.format(new Date(vm_requisitionDTO.startDate));

    Employee_recordsDTO requesterEmployeeRecordsDTO = Employee_recordsRepository.getInstance().
            getById(vm_requisitionDTO.requesterEmpId);

    Employee_recordsDTO decisionEmployeeRecordsDTO = Employee_recordsRepository.getInstance().
            getById(vm_requisitionDTO.firstApproverEmpId);

    String defaultPhoto = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERG" +
            "CEYGh0dHx8fExciJCIeJBweHx7/wAALCABkASwBAREA/8QAFQABAQAAAAAAAAAAAAAAAAAAAAj/xAAUEAEAAAAAAAAAAAAAAAAAAAAA/9oACAEBAAA" +
            "/ALLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
            "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/9k=";

    List<Vm_requisitionDTO> vm_requisitionDTOS = Vm_requisitionRepository.getInstance().
            getUnpaidPersonalVm_requisitionDTOByRequesterEmpId(vm_requisitionDTO.requesterEmpId);
    boolean isUnpaidRequisition = !vm_requisitionDTOS.isEmpty();
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
%>


<style>
    .form-group label {
        font-size: 1.1rem !important;
        font-weight: 600 !important;
    }
</style>

<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group {
        margin-bottom: .5rem;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape-width"] {
        width: 297mm;
        padding: .30in .40in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    #to-print-div *,
    .printable-section * {
        font-size: 12px;
    }

    #to-print-div h1,
    .printable-section h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2,
    .printable-section h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3,
    .printable-section h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }

    .signature-div {
        color: #a406dc;
        font-size: 10px !important;
    }

    .signature-div * {
        font-size: 10px !important;
    }

    .signature-image {
        border-bottom: 2px solid #a406dc !important;
        width: 200px !important;
        height: 53px !important;
    }
</style>

<div class="kt-content p-0" id="kt_content">

    <div class="kt-portlet">

        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=UtilCharacter.getDataByLanguage(Language, "সরকারী গাড়ী ব্যবহারের জন্য রিকুইজিশন", "Request for use of official car")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body" id="bill-div">

            <div class="ml-auto m-5">
                <button type="button" class="btn" id='download-pdf'
                        onclick="downloadTemplateAsPdf('to-print-div', '<%=pdfFileName%>','landscape');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>
            <div style="margin: auto;">
                <div class="container" id="to-print-div">
                    <section class="page shadow" data-size="A4-landscape">
                        <div>
                            <div class="row align-items-center">
                                <div class="col-4"></div>
                                <div class="col-4">

                                    <h3 class="font-weight-bold text-center mt-3">
                                        <%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                    </h3>
                                    <h3 class="font-weight-bold text-center">
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.requesterOfficeUnitNameBn,
                                                vm_requisitionDTO.requesterOfficeUnitNameEn)%>
                                    </h3>

                                    <h1 class="font-weight-bold text-center mt-5">
                                        <%=UtilCharacter.getDataByLanguage(Language, "সরকারী গাড়ী ব্যবহারের জন্য রিকুইজিশন", "Request for use of official car")%>
                                    </h1>
                                </div>

                            </div>
                        </div>

                        <div class=" mt-3 mx-2" style="text-indent: 3rem;">

                            <span><%=UtilCharacter.getDataByLanguage(Language, "রিকুইজিশন কর্মকর্তার নাম", "Name of the Requisition Officer")%>
                            </span>:&nbsp;&nbsp;<span><%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.requesterNameBn,
                                vm_requisitionDTO.requesterNameEn)%></span>

                        </div>
                        <div class=" mt-3 mx-2" style="text-indent: 3rem;">

                            <span><%=UtilCharacter.getDataByLanguage(Language, "পদবী", "Position")%>
                            </span>:&nbsp;&nbsp;<span><%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.requesterOfficeUnitOrgNameBn,
                                vm_requisitionDTO.requesterOfficeUnitOrgNameEn)%></span>

                        </div>
                        <div class="mt-3 mx-5">
                            <table class="table-bordered w-100">


                                <tr>
                                    <th rowspan="2"
                                        style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "কি ধরনের গাড়ী প্রয়োজন", "What kind of car is needed")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "কোথায় থেকে কোথায় যাবে তার বিবরণ", "Details of where to go from where")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারের তারিখ ও সময়", "Date and time of use")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "কোন সময় কোথায় রিপোর্ট করতে হবে", "When and where to report")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "আনুমানিক কত সময় লাগবে", "Approximate how long it will take")%>
                                    </th>
                                    <th rowspan="2"
                                        style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "পূর্বজমা", "Previous Amount")%>
                                    </th>

                                    <th rowspan="1" colspan="2"
                                        style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "ব্যবহারের ধরন", "Type of use")%>
                                    </th>

                                    <th rowspan="2"
                                        style="vertical-align: middle;"><%=UtilCharacter.getDataByLanguage(Language, "সরকারী হলে তার বিবরণ", "If official, its details")%>
                                    </th>

                                </tr>
                                <tr>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "সরকারী", "Official")%>
                                    </th>
                                    <th><%=UtilCharacter.getDataByLanguage(Language, "ব্যক্তিগত", "Personal")%>
                                    </th>
                                </tr>

                                <tr>
                                    <td class="text-center" rowspan="2">
                                        <%
                                            value = CatRepository.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
                                        %>

                                        <%=Utils.getDigits(value, Language)%>
                                    </td>
                                    <td class="text-center">
                                        <%
                                            String startAddress = vm_requisitionDTO.startAddress;
                                            String[] addresses = vm_requisitionDTO.endAddress.split("\\$");
                                            String addressCombine = Arrays.stream(addresses).reduce(startAddress, (a1, a2) -> a1 + UtilCharacter.getDataByLanguage(Language, " -> ", " -> ") + a2);
                                        %>

                                        <%=Utils.getDigits(addressCombine, Language)%>

                                    </td>
                                    <td class="text-center">
                                        <%
                                            value = vm_requisitionDTO.startDate + "";
                                            String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            value = formatted_startDate + "<br>" + UtilCharacter.getDataByLanguage(Language, "সময়", "Time")
                                                    + " : " + vm_requisitionDTO.startTime + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>

                                    </td>

                                    <td class="text-center">
                                        <%
                                            value = vm_requisitionDTO.endDate + "";
                                            String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                            value = formatted_endDate + "<br>" + UtilCharacter.getDataByLanguage(Language, "সময়", "Time")
                                                    + " : " + vm_requisitionDTO.endTime + "";
                                        %>

                                        <%=Utils.getDigits(value, Language)%>

                                    </td>

                                    <td class="text-center">

                                        <%
                                            long diffMillies = (vm_requisitionDAO.diffBetweenTime(vm_requisitionDTO.startDate, vm_requisitionDTO.startTime,
                                                    vm_requisitionDTO.endDate, vm_requisitionDTO.endTime));
                                            long days = diffMillies / (1000 * 60 * 60 * 24);
                                            diffMillies %= (1000 * 60 * 60 * 24);
                                            long hours = diffMillies / (1000 * 60 * 60);
                                            diffMillies %= (1000 * 60 * 60);
                                            long minutes = diffMillies / (1000 * 60);

                                            value = hours + " " + LM.getText(LC.VM_REQUISITION_ADD_HOURS, loginDTO) + " "
                                                    + minutes + " " + LM.getText(LC.VM_REQUISITION_ADD_MINUTES, loginDTO);

                                            if (days > 0)
                                                value = days + " " + LM.getText(LC.VM_REQUISITION_ADD_DAYS, loginDTO) + " " + value;
                                        %>
                                        <%=Utils.getDigits(value, Language)%>


                                    </td>
                                    <%

                                    %>

                                    <td class="text-center">
                                        <%
                                            value = String.valueOf(Vm_requisitionRepository.getInstance()
                                                    .getVm_requisitionDTOByrequester_emp_id(vm_requisitionDTO.requesterEmpId)
                                                    .stream()
                                                    .filter(dto->dto.paidAmount > 0)
                                                    .filter(dto->dto.status == CommonApprovalStatus.RECEIVED.getValue())
                                                    .filter(dto->dto.vehicleRequisitionPurposeCat == 2)
                                                    .mapToDouble(dto -> dto.paidAmount)
                                                    .sum());
                                        %>

                                        <%=Utils.getDigits(value, Language)%>

                                    </td>

                                    <td class="text-center"><%=vm_requisitionDTO.vehicleRequisitionPurposeCat == 1 ? "&#10004;" : ""%>
                                    </td>
                                    <td class="text-center"><%=vm_requisitionDTO.vehicleRequisitionPurposeCat == 2 ? "&#10004;" : ""%>
                                    </td>
                                    <td class="text-center">
                                        <%
                                            String tripDescription = vm_requisitionDTO.vehicleRequisitionPurposeCat == 1 ?
                                                    vm_requisitionDTO.tripDescription : "";
                                        %>
                                        <%=Utils.getDigits(tripDescription, Language)%>
                                    </td>


                                </tr>


                            </table>
                        </div>

                        <div class="row mt-3 mx-5">
                            <%
                                if (vm_requisitionDTO.driverEmpId != -1) {
                                    String driverNameEn = vm_requisitionDTO.driverNameEn;
                                    String driverNameBn = vm_requisitionDTO.driverNameBn;
                                    String vehicleNumber = "";
                                    Vm_vehicleDTO vm_vehicleDTO = Vm_vehicleRepository.getInstance().
                                            getVm_vehicleDTOByID(vm_requisitionDTO.givenVehicleId);
                                    if (vm_vehicleDTO != null) {
                                        vehicleNumber = vm_vehicleDTO.regNo;
                                    }
                            %>

                            <div class="col-4 text-left">
                                <span><%=UtilCharacter.getDataByLanguage(Language, "ড্রাইভারের নামঃ", "Driver's Name:")%>&nbsp;
                                    <%=UtilCharacter.getDataByLanguage(Language, driverNameBn, driverNameEn)%></span>
                                <br>
                                <span><%=UtilCharacter.getDataByLanguage(Language, "ফোন নম্বর", "Phone No.:")%>&nbsp;
                                    <%=Utils.getDigits(vm_requisitionDTO.driverPhoneNum, Language)%></span>
                                <br>
                                <span><%=UtilCharacter.getDataByLanguage(Language, "গাড়ির নম্বরঃ", "Vehicle's No.:")%>&nbsp;
                                    <%=Utils.getDigits(vm_vehicleDTO.regNo, Language)%></span>

                            </div>

                            <%}%>
                        </div>

                        <div class="row col-12 mt-5">


                            <div class="col-4 text-center">

                                <div class="signature-div">
                                    <div>

                                        <img
                                                class="signature-image"
                                                alt="Signature Missing"
                                                src='<%=StringUtils.getBase64EncodedImageStr(decisionEmployeeRecordsDTO != null &&
                                                  decisionEmployeeRecordsDTO.signature != null ?decisionEmployeeRecordsDTO.signature:defaultPhoto.getBytes())%>'/>
                                        <br>
                                        <%=UtilCharacter.getDataByLanguage(Language, "অনুমোদন নিয়ন্ত্রণকারীর স্বাক্ষর", "Approver Signature")%>
                                    </div>
                                    <div>
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.firstApproverNameBn, vm_requisitionDTO.firstApproverNameEn)%>
                                        <br>
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.firstApproverOfficeUnitOrgNameBn,
                                                vm_requisitionDTO.firstApproverOfficeUnitOrgNameEn)%><br>
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.firstApproverOfficeUnitNameBn,
                                                vm_requisitionDTO.firstApproverOfficeUnitNameEn)%><br>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 text-center">

                                <div class="font-weight-bold">
                                    <span><%=UtilCharacter.getDataByLanguage(Language, "অধিযাচনকারীর ব্যক্তিগত গাড়ী ব্যবহারের জন্য",
                                            "For the use of the appellant's personal car")%></span><br>
                                    <span><%=UtilCharacter.getDataByLanguage(Language, "পূর্বের বিল বকেয়া",
                                            "Previous bill ")%></span>

                                    <%if (isUnpaidRequisition) { %>

                                    <span>
                                        <sup>&#10003;</sup>
                                        <%=UtilCharacter.getDataByLanguage(Language, "আছে / নাই", "is / is not due")%>
                                    </span>

                                    <%} else {%>
                                    <span>
                                        <%=UtilCharacter.getDataByLanguage(Language, "আছে / নাই", "is / is not due")%>
                                        <sup>&#10003;</sup>
                                    </span>
                                    <%}%>


                                </div>
                            </div>


                            <div class="col-4 text-center">

                                <div class="signature-div">
                                    <div>

                                        <img
                                                class="signature-image"
                                                alt="Signature Missing"
                                                src='<%=StringUtils.getBase64EncodedImageStr(requesterEmployeeRecordsDTO != null
                                                 && requesterEmployeeRecordsDTO.signature != null ?requesterEmployeeRecordsDTO.signature:defaultPhoto.getBytes())%>'/>
                                        <br>
                                        <%=UtilCharacter.getDataByLanguage(Language, "আবেদনকারীর স্বাক্ষর", "Applicant Signature")%>
                                    </div>
                                    <div>
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.requesterNameBn, vm_requisitionDTO.requesterNameEn)%>
                                        <br>
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.requesterOfficeUnitOrgNameBn,
                                                vm_requisitionDTO.requesterOfficeUnitOrgNameEn)%><br>
                                        <%=UtilCharacter.getDataByLanguage(Language, vm_requisitionDTO.requesterOfficeUnitNameBn,
                                                vm_requisitionDTO.requesterOfficeUnitNameEn)%><br>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </section>
                </div>
            </div>

            <%if (vm_requisitionDTO.vehicleRequisitionPurposeCat == 1) {%>

            <div>

                    <div class="container">
                        <label class=" mt-2 text-color">
                            <%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_NECESSARY_DOCUMENTS, loginDTO)%>
                        </label>
                        <%
                            String fileColumnName = "filesDropzone";
                            if (true) {

                                List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_requisitionDTO.filesDropzone);
                        %>
                        <table>

                            <%
                                if (fileList != null) {
                                    for (int j = 0; j < fileList.size(); j++) {
                                        FilesDTO filesDTO = fileList.get(j);
                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                            %>
                            <tr>

                                <td>

                                </td>

                                <td id='<%=fileColumnName%>_td_<%=filesDTO.iD%>'>
                                    <a href='Vm_incidentServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                       download><%=filesDTO.fileTitle%>
                                    </a>
                                    <%
                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                    %>
                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
								%>' style='width:100px'/>
                                    <%
                                        }
                                    %>


                                </td>
                            </tr>
                            <%
                                    }
                                }


                            %>

                        </table>
                        <%
                            }
                        %>
                    </div>


            </div>
            <%}%>

            <%--Applicant Information start--%>

            <div class="mt-5 container">
                <h5 class="table-title">
                    <%=LM.getText(LC.CARD_INFO_REQUESTER_INFORMATION, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th>
                                <%=LM.getText(LC.CARD_INFO_ADD_APPLICANT_NAME, loginDTO)%>
                            </th>
                            <th>
                                <%=LM.getText(LC.CARD_INFO_DESIGNATION_OFFICE, loginDTO)%>
                            </th>
                            <th>
                                <%=LM.getText(LC.CARD_INFO_REQUEST_CREATE_DATE_TIME, loginDTO)%>
                            </th>
                            <%if(vm_requisitionDTO.status == CommonApprovalStatus.CANCELLED.getValue()){%>
                            <th><%=UtilCharacter.getDataByLanguage(Language,"রিকুইজিশন বাতিলের তারিখ","Date of Cancellation of Requisition")%></th>
                            <th><%=UtilCharacter.getDataByLanguage(Language,"রিকুইজিশন বাতিলের কারণ","Requisition Cancellation Reason")%></th>
                            <%}%>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">
                                <%=getDataByLanguage(Language, vm_requisitionDTO.requesterNameBn, vm_requisitionDTO.requesterNameEn)%>
                            </td>
                            <td class="text-center">
                                <b>
                                    <%=getDataByLanguage(Language, vm_requisitionDTO.requesterOfficeUnitOrgNameBn, vm_requisitionDTO.requesterOfficeUnitOrgNameEn)%>
                                </b>
                                <br>
                                <%=getDataByLanguage(Language, vm_requisitionDTO.requesterOfficeUnitNameBn, vm_requisitionDTO.requesterOfficeUnitNameEn)%>
                            </td>
                            <td class="text-center">
                                <%=StringUtils.convertToDateAndTime(isLanguageEnglish, vm_requisitionDTO.insertionDate)%>
                            </td>
                            <%if(vm_requisitionDTO.status == CommonApprovalStatus.CANCELLED.getValue()){%>
                            <%
                                formatted_endDate = simpleDateFormat.format(new Date(vm_requisitionDTO.cancellationDate));
                            %>
                                <td class="text-center"><%=Utils.getDigits(formatted_endDate, Language)%></td>
                                <td class="text-center"><%=vm_requisitionDTO.cancellationReason%></td>
                            <%}%>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <%--Applicant Information end--%>

            <%--Approver Information start--%>
            <div class="mt-5 container">
                <h5 class="table-title">
                    <%=LM.getText(LC.CARD_INFO_APPROVAL_INFORMATION, loginDTO)%>
                </h5>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVER_OFFICE_INFORMATION, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVAL_STATUS, loginDTO)%>
                            </th>
                            <th><%=LM.getText(LC.CARD_INFO_APPROVAL_DATE_TIME, loginDTO)%>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <%--1st Approver start--%>
                        <tr>

                            <td class="text-center">
                                
                                <%=getDataByLanguage(Language, vm_requisitionDTO.firstApproverOfficeUnitNameBn,
                                        vm_requisitionDTO.firstApproverOfficeUnitNameEn)%>
                            </td>


                            <td class="text-center">

                                <%
                                    int firstApproverStatus = vm_requisitionDTO.firstApproverStatus>0?vm_requisitionDTO.firstApproverStatus:vm_requisitionDTO.status;
                                    int secondApproverStatus = vm_requisitionDTO.secondApproverStatus>0?vm_requisitionDTO.secondApproverStatus:vm_requisitionDTO.status;
                                %>

                                <span class="btn btn-sm border-0 shadow"
                                      style="background-color: <%=CommonApprovalStatus.getColor(firstApproverStatus)%>; color: white; border-radius: 8px;cursor: text">
                                                                    <%=CatRepository.getName(Language, "vm_requisition_status", firstApproverStatus)%>
                                </span>

                            </td>

                            <td class="text-center">
                                <%if(vm_requisitionDTO.firstApproverApproveOrRejectionDate>0){%>
                                    <%=StringUtils.convertToDateAndTime(isLanguageEnglish, vm_requisitionDTO.firstApproverApproveOrRejectionDate)%>
                                <%}%>

                            </td>

                        </tr>
                        <%--1st Approver end--%>


                        <%--Other Approvers--%>
                        <%
                            if(vm_requisitionDTO.firstApproverStatus>0 && vm_requisitionDTO.firstApproverStatus==CommonApprovalStatus.SATISFIED.getValue()){
                                Vm_requisition_approverDAO vm_requisition_approverDAO = new Vm_requisition_approverDAO();
                                List<Vm_requisition_approverDTO> vm_requisition_approverDTOS = vm_requisition_approverDAO.getAllVm_requisition_approver(true);
                                for (Vm_requisition_approverDTO dto : vm_requisition_approverDTOS) {
                        %>
                                <tr>

                                    <td class="text-center">

                                        <%=getDataByLanguage(Language, dto.approverOfficeUnitNameBn,
                                                dto.approverOfficeUnitNameEn)%>
                                    </td>


                                    <td class="text-center">

                                        <span class="btn btn-sm border-0 shadow"
                                              style="background-color: <%=CommonApprovalStatus.getColor(secondApproverStatus)%>; color: white; border-radius: 8px;cursor: text">
                                                                            <%=CatRepository.getName(Language, "vm_requisition_status", secondApproverStatus)%>
                                        </span>

                                    </td>

                                    <td class="text-center">
                                        <%if(vm_requisitionDTO.secondApproverApproveOrRejectionDate>0){%>
                                        <%=StringUtils.convertToDateAndTime(isLanguageEnglish, vm_requisitionDTO.secondApproverApproveOrRejectionDate)%>
                                        <%}%>

                                    </td>

                                </tr>
                        <%}%>
                        <%}%>


                        </tbody>
                    </table>
                </div>
            </div>
            <%--Approver Information end--%>

        </div>
    </div>
</div>


<script type="text/javascript">

    function downloadTemplateAsPdf(divId, fileName, orientation = 'landscape') {
        let content = document.getElementById(divId);
        const opt = {
            margin: 0.5,
            filename: fileName,
            image: {type: 'jpeg', quality: 1},
            html2canvas: {scale: 5},
            jsPDF: {format: 'A4', orientation: orientation}
        };
        html2pdf().from(content).set(opt).save();
    }


</script>



