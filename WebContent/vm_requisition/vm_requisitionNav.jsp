<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>


<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <%
                    out.println("<input placeholder='অনুসন্ধান করুন' autocomplete='off' type='text' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ANYFIELD, loginDTO) + "' ");
                    String value = (String) session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);

                    if (value != null) {
                        out.println("value = '" + value + "'");
                    } else {
                        out.println("value=''");
                    }

                    out.println("/><br />");
                %>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>

    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label pr-0">
                            <%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='vehicle_requisition_purpose_cat'
                                    id='vehicle_requisition_purpose_cat' onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getOptions(Language, "vehicle_requisition_purpose", -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='vehicle_type_cat' id='vehicle_type_cat'
                                    onSelect='setSearchChanged()'>
                                <%
                                    Options = CatRepository.getOptions(Language, "vehicle_type", -1);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label"><%=LM.getText(LC.FUND_APPROVAL_MAPPING_ADD_FUNDAPPLICATIONSTATUSCAT, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <select class='form-control' name='fundApplicationStatusCat'
                                    id='requisition_application_status_cat' onSelect='setSearchChanged()'>
                                <%=CatRepository.getInstance().buildOptions("vm_requisition_status", Language, -1)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_STARTDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="start_date_start_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type="hidden" id="start_date_start" name="start_date_start">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_STARTDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="start_date_end_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type="hidden" id="start_date_end" name="start_date_end">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_ENDDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="end_date_start_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type="hidden" id="end_date_start" name="end_date_start">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label">
                            <%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.EMPLOYEE_ATTACHMENT_ADD_ENDDATE, loginDTO)%>
                        </label>
                        <div class="col-md-8">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID" value="end_date_end_js"/>
                                <jsp:param name="LANGUAGE" value="<%=Language%>"/>
                            </jsp:include>
                            <input type="hidden" id="end_date_end" name="end_date_end">
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=<%=isPermanentTable%>&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        // $("#insertion_date_start").val(getDateStringById('insertion_date_start_js', 'DD/MM/YYYY'));
        // params +=  '&insertion_date_start='+ getBDFormattedDate('insertion_date_start');
        // $("#insertion_date_end").val(getDateStringById('insertion_date_end_js', 'DD/MM/YYYY'));
        // params +=  '&insertion_date_end='+ getBDFormattedDate('insertion_date_end');
        $("#start_date_start").val(getDateStringById('start_date_start_js', 'DD/MM/YYYY'));
        params +=  '&start_date_start='+ getBDFormattedDate('start_date_start');
        $("#start_date_end").val(getDateStringById('start_date_end_js', 'DD/MM/YYYY'));
        params +=  '&start_date_end='+ getBDFormattedDate('start_date_end');
        $("#end_date_start").val(getDateStringById('end_date_start_js', 'DD/MM/YYYY'));
        params +=  '&end_date_start='+ getBDFormattedDate('end_date_start');
        $("#end_date_end").val(getDateStringById('end_date_end_js', 'DD/MM/YYYY'));
        params +=  '&end_date_end='+ getBDFormattedDate('end_date_end');
        if ($("#vehicle_requisition_purpose_cat").val() != -1) {
            params += '&vehicle_requisition_purpose_cat=' + $("#vehicle_requisition_purpose_cat").val();
        }
        if ($("#vehicle_type_cat").val() != -1) {
            params += '&vehicle_type_cat=' + $("#vehicle_type_cat").val();
            params += '&given_vehicle_type=' + $("#vehicle_type_cat").val();
        }

        if ($('#requisition_application_status_cat').val()) {
            params += '&status=' + $("#requisition_application_status_cat").val();
        }

        // $("#receive_date_start").val(getDateStringById('receive_date_start_js', 'DD/MM/YYYY'));
        // params +=  '&receive_date_start='+ getBDFormattedDate('receive_date_start');
        // $("#receive_date_end").val(getDateStringById('receive_date_end_js', 'DD/MM/YYYY'));
        // params +=  '&receive_date_end='+ getBDFormattedDate('receive_date_end');

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

