
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_requisition.*" %>
<%@page import="geolocation.GeoLocationDAO2" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.Enumeration" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.UtilCharacter" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Vm_requisitionDAO vm_requisitionDAO = (Vm_requisitionDAO) request.getAttribute("vm_requisitionDAO");


    String navigator2 = SessionConstants.NAV_VM_REQUISITION;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th style="width: 12%"><%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERORGID, loginDTO)%>
            </th>
            <th style="width: 5%"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%>
            </th>
            <th style="width: 5%"><%=LM.getText(LC.VM_REQUISITION_ADD_ENDDATE, loginDTO)%>
            </th>
            <th style="width: 10%"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%>
            </th>
            <th style="width: 13%"><%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%>
            </th>
            <th style="width: 7%"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%>
            </th>
            <th style="width: 4%"><%=LM.getText(LC.VM_REQUISITION_ADD_STATUS, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%>
            </th>
            <th style="width: 7%"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <th style="width: 22%"><%=LM.getText(LC.VM_REQUISITION_APPROVER_SEARCH_VM_REQUISITION_APPROVER_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span class="">All</span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_VM_REQUISITION);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Vm_requisitionDTO vm_requisitionDTO = (Vm_requisitionDTO) data.get(i);


        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("vm_requisitionDTO", vm_requisitionDTO);
            %>

            <jsp:include page="./vm_requisitionSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>

<script>

    function cancelApplication(vmRequisitionId){


        let msg = '<%=UtilCharacter.getDataByLanguage(Language,"বাতিলের কারণ","Reason for cancellation")%>';
        let placeHolder = '<%=UtilCharacter.getDataByLanguage(Language,"বাতিলের কারণ উল্লেখ করুন","Reason for cancellation")%>';
        let confirmButtonText = '<%=UtilCharacter.getDataByLanguage(Language,"হ্যাঁ, বাতিল","Yes, Cancel")%>';
        let cancelButtonText = '<%=UtilCharacter.getDataByLanguage(Language,"বন্ধ করুন","Close")%>';
        let emptyReasonText = '<%=LM.getText(LC.CARD_APPROVAL_MAPPING_ADD_PLEASE_WRITE_DISSATISFACTION_REASON, loginDTO)%>';

        dialogMessageWithTextBoxWithoutAnimation(msg,placeHolder,confirmButtonText,cancelButtonText,emptyReasonText,(reason)=>{

            submitRejectForm(reason,vmRequisitionId);
        },()=>{});
    }

    function submitRejectForm(reason,vmRequisitionId){
        buttonStateChange(true);
        $.ajax({
            type : "GET",
            url : "Vm_requisitionServlet?actionType=cancel",
            data:{"vmRequisitionId":vmRequisitionId,"cancellationReason":reason},
            dataType : 'JSON',
            success : function(response) {
                if(response.responseCode === 0){
                    $('#toast_message').css('background-color','#ff6063');
                    showToastSticky(response.msg,response.msg);
                    //buttonStateChange(false);
                }else if(response.responseCode === 200){
                    window.location.replace(getContextPath()+response.msg);
                    //updateApprovalMappingTable();
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
                    + ", Message: " + errorThrown);
                buttonStateChange(false);
            }
        });

    }

    function buttonStateChange(value){
        //submitBtn.prop('disabled',value);
        //cancelBtn.prop('disabled',value);
    }

</script>


			