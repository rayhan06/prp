<%@page pageEncoding="UTF-8" %>

<%@page import="vm_requisition.*"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VM_REQUISITION;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Vm_requisitionDTO vm_requisitionDTO = (Vm_requisitionDTO)request.getAttribute("vm_requisitionDTO");
CommonDTO commonDTO = vm_requisitionDTO;
String servletName = "Vm_requisitionServlet";


System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Vm_requisitionDAO vm_requisitionDAO = (Vm_requisitionDAO)request.getAttribute("vm_requisitionDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
		
		
		
		
		
		
											<td id = '<%=i%>_requesterOrgId'>
											<%
											value = vm_requisitionDTO.requesterOrgId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOrgId'>
											<%
											value = vm_requisitionDTO.decisionByOrgId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOrgId'>
											<%
											value = vm_requisitionDTO.driverOrgId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_startDate'>
											<%
											value = vm_requisitionDTO.startDate + "";
											%>
											<%
											String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_startDate, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_endDate'>
											<%
											value = vm_requisitionDTO.endDate + "";
											%>
											<%
											String formatted_endDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_endDate, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_startAddress'>
											<%
											value = vm_requisitionDTO.startAddress + "";
											%>
											<%=GeoLocationDAO2.getAddressToShow(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_endAddress'>
											<%
											value = vm_requisitionDTO.endAddress + "";
											%>
											<%=GeoLocationDAO2.getAddressToShow(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_vehicleRequisitionPurposeCat'>
											<%
											value = vm_requisitionDTO.vehicleRequisitionPurposeCat + "";
											%>
											<%
											value = CatRepository.getName(Language, "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_vehicleTypeCat'>
											<%
											value = vm_requisitionDTO.vehicleTypeCat + "";
											%>
											<%
											value = CatRepository.getName(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_tripDescription'>
											<%
											value = vm_requisitionDTO.tripDescription + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_status'>
											<%
											value = vm_requisitionDTO.status + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionOn'>
											<%
											value = vm_requisitionDTO.decisionOn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionDescription'>
											<%
											value = vm_requisitionDTO.decisionDescription + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_givenVehicleType'>
											<%
											value = vm_requisitionDTO.givenVehicleType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "given_vehicle", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_givenVehicleId'>
											<%
											value = vm_requisitionDTO.givenVehicleId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_receiveDate'>
											<%
											value = vm_requisitionDTO.receiveDate + "";
											%>
											<%
											String formatted_receiveDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_receiveDate, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_totalTripDistance'>
											<%
											value = vm_requisitionDTO.totalTripDistance + "";
											%>
											<%
											value = String.format("%.1f", vm_requisitionDTO.totalTripDistance);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_totalTripTime'>
											<%
											value = vm_requisitionDTO.totalTripTime + "";
											%>
											<%
											value = String.format("%.1f", vm_requisitionDTO.totalTripTime);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_petrolGiven'>
											<%
											value = vm_requisitionDTO.petrolGiven + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_petrolAmount'>
											<%
											value = vm_requisitionDTO.petrolAmount + "";
											%>
											<%
											value = String.format("%.1f", vm_requisitionDTO.petrolAmount);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_paymentGiven'>
											<%
											value = vm_requisitionDTO.paymentGiven + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_paymentType'>
											<%
											value = vm_requisitionDTO.paymentType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "payment", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_startTime'>
											<%
											value = vm_requisitionDTO.startTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_endTime'>
											<%
											value = vm_requisitionDTO.endTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_receiveTime'>
											<%
											value = vm_requisitionDTO.receiveTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterOfficeId'>
											<%
											value = vm_requisitionDTO.requesterOfficeId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOfficeId'>
											<%
											value = vm_requisitionDTO.decisionByOfficeId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOfficeId'>
											<%
											value = vm_requisitionDTO.driverOfficeId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterOfficeUnitId'>
											<%
											value = vm_requisitionDTO.requesterOfficeUnitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOfficeUnitId'>
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOfficeUnitId'>
											<%
											value = vm_requisitionDTO.driverOfficeUnitId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterEmpId'>
											<%
											value = vm_requisitionDTO.requesterEmpId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByEmpId'>
											<%
											value = vm_requisitionDTO.decisionByEmpId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverEmpId'>
											<%
											value = vm_requisitionDTO.driverEmpId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterPhoneNum'>
											<%
											value = vm_requisitionDTO.requesterPhoneNum + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByPhoneNum'>
											<%
											value = vm_requisitionDTO.decisionByPhoneNum + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverPhoneNum'>
											<%
											value = vm_requisitionDTO.driverPhoneNum + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterNameEn'>
											<%
											value = vm_requisitionDTO.requesterNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByNameEn'>
											<%
											value = vm_requisitionDTO.decisionByNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverNameEn'>
											<%
											value = vm_requisitionDTO.driverNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterNameBn'>
											<%
											value = vm_requisitionDTO.requesterNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByNameBn'>
											<%
											value = vm_requisitionDTO.decisionByNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverNameBn'>
											<%
											value = vm_requisitionDTO.driverNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterOfficeNameEn'>
											<%
											value = vm_requisitionDTO.requesterOfficeNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOfficeNameEn'>
											<%
											value = vm_requisitionDTO.decisionByOfficeNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOfficeNameEn'>
											<%
											value = vm_requisitionDTO.driverOfficeNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterOfficeNameBn'>
											<%
											value = vm_requisitionDTO.requesterOfficeNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOfficeNameBn'>
											<%
											value = vm_requisitionDTO.decisionByOfficeNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOfficeNameBn'>
											<%
											value = vm_requisitionDTO.driverOfficeNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterOfficeUnitNameEn'>
											<%
											value = vm_requisitionDTO.requesterOfficeUnitNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOfficeUnitNameEn'>
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOfficeUnitNameEn'>
											<%
											value = vm_requisitionDTO.driverOfficeUnitNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterOfficeUnitNameBn'>
											<%
											value = vm_requisitionDTO.requesterOfficeUnitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOfficeUnitNameBn'>
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOfficeUnitNameBn'>
											<%
											value = vm_requisitionDTO.driverOfficeUnitNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterOfficeUnitOrgNameEn'>
											<%
											value = vm_requisitionDTO.requesterOfficeUnitOrgNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOfficeUnitOrgNameEn'>
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitOrgNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOfficeUnitOrgNameEn'>
											<%
											value = vm_requisitionDTO.driverOfficeUnitOrgNameEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_requesterOfficeUnitOrgNameBn'>
											<%
											value = vm_requisitionDTO.requesterOfficeUnitOrgNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_decisionByOfficeUnitOrgNameBn'>
											<%
											value = vm_requisitionDTO.decisionByOfficeUnitOrgNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_driverOfficeUnitOrgNameBn'>
											<%
											value = vm_requisitionDTO.driverOfficeUnitOrgNameBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
	

											<td>
												<button class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_requisitionServlet?actionType=view&ID=<%=vm_requisitionDTO.iD%>'">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<button class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
											            onclick="location.href='Vm_requisitionServlet?actionType=getEditPage&ID=<%=vm_requisitionDTO.iD%>'">
											        <%=LM.getText(LC.VM_REQUISITION_SEARCH_VM_REQUISITION_EDIT_BUTTON, loginDTO)%>
											    </button>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=vm_requisitionDTO.iD%>'/></span>
												</div>
											</td>
																						
											

