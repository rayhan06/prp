<%@page import="workflow.WorkflowController" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="vm_requisition.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="util.TimeFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="employee_assign.EmployeeSearchModalUtil" %>
<%@ page import="employee_assign.EmployeeSearchIds" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="employee_assign.EmployeeSearchModel" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="files.FilesDTO" %>
<%@ page import="dbm.DBMW" %>
<%@ page import="files.FilesDAO" %>

<%
    Vm_requisitionDTO vm_requisitionDTO;
    vm_requisitionDTO = (Vm_requisitionDTO) request.getAttribute("vm_requisitionDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    EmployeeSearchModel own = new EmployeeSearchModel();
//	if (userDTO.ID != -1) {
//		own = new Gson().fromJson(EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID), EmployeeSearchModel.class);
//	}
//	else {
//		own = EmployeeSearchModalUtil.getTestModel(-1);
//	}
    own = new Gson().fromJson(EmployeeSearchModalUtil.getEmployeeSearchModelJson(userDTO.employee_record_id, userDTO.unitID, userDTO.organogramID), EmployeeSearchModel.class);

    if (vm_requisitionDTO == null) {
        vm_requisitionDTO = new Vm_requisitionDTO();

    }
    System.out.println("vm_requisitionDTO = " + vm_requisitionDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_requisitionServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    FilesDAO filesDAO = new FilesDAO();
    long ColumnID = -1;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Vm_requisitionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row mx-2 mx-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='iD'
                                           id='iD_hidden_<%=i%>' value='<%=vm_requisitionDTO.iD%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='insertedByUserId'
                                           id='insertedByUserId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.insertedByUserId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='insertedByOrganogramId'
                                           id='insertedByOrganogramId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.insertedByOrganogramId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='insertionDate'
                                           id='insertionDate_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.insertionDate%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='isDeleted'
                                           id='isDeleted_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.isDeleted%>' tag='pb_html'/>

                                    <input type='hidden' class='form-control'
                                           name='lastModificationTime'
                                           id='lastModificationTime_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.lastModificationTime%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='searchColumn'
                                           id='searchColumn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.searchColumn%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterOrgId'
                                           id='requesterOrgId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOrgId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByOrgId'
                                           id='decisionByOrgId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOrgId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOrgId'
                                           id='driverOrgId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOrgId%>' tag='pb_html'/>


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_REQUESTERNAMEBN, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-9">
                                            <input type="hidden" class='form-control'
                                                   name='employeeRecordsId' id='employeeRecordsId'
                                                   value=''>
                                            <button type="button"
                                                    class="btn btn-primary btn-block shadow btn-border-radius mb-3"
                                                    id="tagRequester_modal_button">
                                                <%=LM.getText(LC.HM_SELECT, userDTO)%>
                                            </button>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped text-nowrap">
                                                    <thead></thead>

                                                    <tbody id="tagged_requester_table">
                                                    <tr style="display: none;">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button"
                                                                    class="btn btn-sm- delete-trash-btn"
                                                                    onclick="remove_containing_row(this,'tagged_requester_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>

                                                    <% if (actionName.equals("edit")) { %>
                                                    <tr>
                                                        <td><%=Employee_recordsRepository.getInstance()
                                                                .getById(vm_requisitionDTO.requesterEmpId).employeeNumber%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (vm_requisitionDTO.requesterNameEn)
                                                                    : (vm_requisitionDTO.requesterNameBn)%>
                                                        </td>

                                                        <%
                                                            String postName = isLanguageEnglish ? (vm_requisitionDTO.requesterOfficeUnitOrgNameEn + ", " + vm_requisitionDTO.requesterOfficeUnitNameEn)
                                                                    : (vm_requisitionDTO.requesterOfficeUnitOrgNameBn + ", " + vm_requisitionDTO.requesterOfficeUnitNameBn);
                                                        %>

                                                        <td><%=postName%>
                                                        </td>
                                                        <td id='<%=vm_requisitionDTO.requesterEmpId%>_td_button'>
                                                            <button type="button"
                                                                    class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                    style="padding-right: 14px"
                                                                    onclick="remove_containing_row(this,'tagged_requester_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>

                                                        </td>
                                                    </tr>
                                                    <%} else {%>
                                                    <tr>
                                                        <td><%=own.employeeRecordId%>
                                                        </td>
                                                        <td>
                                                            <%=isLanguageEnglish ? (own.employeeNameEn)
                                                                    : (own.employeeNameBn)%>
                                                        </td>

                                                        <%
                                                            String postName = isLanguageEnglish ? (own.organogramNameEn + ", " + own.officeUnitNameEn)
                                                                    : (own.organogramNameBn + ", " + own.officeUnitNameBn);
                                                        %>

                                                        <td><%=postName%>
                                                        </td>
                                                        <td id='<%=own.employeeRecordId%>_td_button'>
                                                            <button type="button"
                                                                    class="btn btn-sm cancel-btn text-white shadow btn-border-radius pl-4"
                                                                    style="padding-right: 14px"
                                                                    onclick="remove_containing_row(this,'tagged_requester_table');">
                                                                <i class="fa fa-trash"></i>
                                                            </button>

                                                        </td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "startDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='startDate'
                                                   id='startDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(vm_requisitionDTO.startDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_REQUISITION_ADD_ENDDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "endDate_js_" + i;%>
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' name='endDate' id='endDate_date_<%=i%>'
                                                   value='<%=dateFormat.format(new Date(vm_requisitionDTO.endDate))%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=UtilCharacter.getDataByLanguage(Language,"অধিযাচনের স্থান","Requisition Location")%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleRequisitionLocationCat'
                                                    id='vehicleRequisitionLocationCat_<%=i%>' tag='pb_html'>
                                                <%
                                                    Options = CatRepository.getOptions(Language, "vm_requisition_location", vm_requisitionDTO.vehicleRequisitionLocationCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTADDRESS, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <div id='startAddress_DIV_<%=i%>' tag='pb_html'>
                                                <table id="startAddressTable" style="width: 100%">
                                                    <tr>
                                                        <td><input type='text'
                                                                   class='form-control noDollar'
                                                                   name='startAddresses'
                                                                   id='startAddress_TextField_<%=i%>'
                                                                   value="<%=actionName.equals("edit")?("" +  vm_requisitionDTO.startAddress  + ""):("" + "" + "")%>"
                                                                   placeholder='Road Number, House Number etc'
                                                                   tag='pb_html'></td>
                                                        <%--																		<td><input type="button" class="button" value="+" onclick="insertAddressRow('startAddressTable');"></td>--%>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <input type='hidden' class='form-control'
                                                   name='startAddress' id='startAddress_hidden_<%=i%>'
                                                   value='<%=actionName.equals("edit")?("'" +  vm_requisitionDTO.startAddress  + "'"):("'" + "" + "'")%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.VM_REQUISITION_ADD_ENDADDRESS, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <div id='endAddress_DIV_<%=i%>' tag='pb_html'>
                                                <table id="endAddressTable" style="width: 100%">
                                                    <%
                                                        if (actionName.equals("edit")) {
                                                            String[] addresses = vm_requisitionDTO.endAddress.split("\\$");
                                                            for (int addressIndex = 0; addressIndex < addresses.length; addressIndex++) {
                                                                String address = addresses[addressIndex];
                                                                String onClick = addressIndex > 0 ? "deleteAddressRow('endAddressTable', this);" : "insertAddressRow('endAddressTable');";
                                                                String tdValue = addressIndex > 0 ? "- " : "+";
                                                    %>
                                                    <tr>
                                                        <td><input type='text'
                                                                   class='form-control noDollar'
                                                                   name='endAddresses'
                                                                   id='endAddress_TextField_<%=addressIndex%>'
                                                                   value="<%=actionName.equals("edit")?("" +  address  + ""):("'" + "" + "'")%>"
                                                                   placeholder='Road Number, House Number etc'
                                                                   tag='pb_html'></td>
                                                        <td><input type="button" class="button"
                                                                   value="<%=tdValue%>"
                                                                   onclick="<%=onClick%>"></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    } else {
                                                    %>
                                                    <tr>
                                                        <td><input type='text'
                                                                   class='form-control noDollar'
                                                                   name='endAddresses'
                                                                   id='endAddress_TextField_<%=i%>'
                                                                   value='<%=actionName.equals("edit")?("'" +  vm_requisitionDTO.endAddress  + "'"):("'" + "" + "'")%>'
                                                                   placeholder='Road Number, House Number etc'
                                                                   tag='pb_html'></td>
                                                        <td>
                                                            <button
                                                                    type="button"
                                                                    class="btn btn-success btn-block shadow btn-border-radius pl-4"
                                                                    onclick="insertAddressRow('endAddressTable');"
                                                            >
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                </table>
                                            </div>

                                            <input type='hidden' class='form-control' name='endAddress'
                                                   id='endAddress_hidden_<%=i%>'
                                                   value='<%=actionName.equals("edit")?("'" +  vm_requisitionDTO.endAddress  + "'"):("'" + "" + "'")%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_REQUISITION_ADD_STARTTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "startTime_js_" + i;%>
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden'
                                                   value="<%=vm_requisitionDTO.startTime%>"
                                                   name='startTime' id='startTime_time_<%=i%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_REQUISITION_ADD_ENDTIME, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%value = "endTime_js_" + i;%>
                                            <jsp:include page="/time/time.jsp">
                                                <jsp:param name="TIME_ID"
                                                           value="<%=value%>"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="IS_AMPM" value="true"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' value="<%=vm_requisitionDTO.endTime%>"
                                                   name='endTime' id='endTime_time_<%=i%>'
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLEREQUISITIONPURPOSECAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control'
                                                    name='vehicleRequisitionPurposeCat'
                                                    id='vehicleRequisitionPurposeCat_category_<%=i%>'
                                                    tag='pb_html'
                                                    onchange="vehicleTypeChanged(this)">
                                                <%
                                                    Options = CatRepository.getOptions(Language, "vehicle_requisition_purpose", vm_requisitionDTO.vehicleRequisitionPurposeCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_REQUISITION_ADD_VEHICLETYPECAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <select class='form-control' name='vehicleTypeCat'
                                                    id='vehicleTypeCat_category_<%=i%>' tag='pb_html' >
                                                <%
                                                    Options = CatRepository.getOptions(Language, "vehicle_type", vm_requisitionDTO.vehicleTypeCat);
                                                %>
                                                <%=Options%>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.VM_REQUISITION_ADD_TRIPDESCRIPTION, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
																<textarea class='form-control' name='tripDescription'
                                                                          id='tripDescription_text_<%=i%>'>
																	<%=vm_requisitionDTO.tripDescription%>
																</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row hiddenDropzone fileDropzoneCls">
                                        <label class="col-md-3 col-form-label text-md-right"><%=LM.getText(LC.AM_OFFICE_ASSIGNMENT_REQUEST_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-9">
                                            <%
                                                fileColumnName = "filesDropzone";
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> fileList = filesDAO.getMiniDTOsByFileID(vm_requisitionDTO.filesDropzone);
                                            %>
                                            <%@include file="../pb/dropzoneEditor.jsp" %>
                                            <%
                                                } else {
                                                    ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
                                                    vm_requisitionDTO.filesDropzone = ColumnID;
                                                }
                                            %>

                                            <div class="dropzone"
                                                 action="Vm_incidentServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=<%=fileColumnName%>&ColumnID=<%=vm_requisitionDTO.filesDropzone%>">
                                                <input type='file' style="display:none" name='<%=fileColumnName%>File'
                                                       id='<%=fileColumnName%>_dropzone_File_<%=i%>' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='<%=fileColumnName%>FilesToDelete'
                                                   id='<%=fileColumnName%>FilesToDelete_<%=i%>' value='' tag='pb_html'/>
                                            <input type='hidden' name='<%=fileColumnName%>'
                                                   id='<%=fileColumnName%>_dropzone_<%=i%>' tag='pb_html'
                                                   value='<%=vm_requisitionDTO.filesDropzone%>'/>


                                        </div>
                                    </div>


                                    <input type='hidden' class='form-control' name='requesterOfficeId'
                                           id='requesterOfficeId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByOfficeId'
                                           id='decisionByOfficeId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeId'
                                           id='driverOfficeId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitId'
                                           id='requesterOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitId'
                                           id='decisionByOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeUnitId'
                                           id='driverOfficeUnitId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitId%>'
                                           tag='pb_html'/>
                                    <input type='text' style='display:none' class='form-control'
                                           name='requesterEmpId' id='requesterEmpId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterEmpId%>' tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByEmpId'
                                           id='decisionByEmpId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByEmpId%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverEmpId'
                                           id='driverEmpId_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverEmpId%>' tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeNameEn'
                                           id='requesterOfficeNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeNameEn'
                                           id='decisionByOfficeNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeNameEn'
                                           id='driverOfficeNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitNameEn'
                                           id='requesterOfficeUnitNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitNameEn'
                                           id='decisionByOfficeUnitNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='driverOfficeUnitNameEn'
                                           id='driverOfficeUnitNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterNameEn'
                                           id='requesterNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByNameEn'
                                           id='decisionByNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverNameEn'
                                           id='driverNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverNameEn%>' tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeNameBn'
                                           id='requesterOfficeNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeNameBn'
                                           id='decisionByOfficeNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverOfficeNameBn'
                                           id='driverOfficeNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitNameBn'
                                           id='requesterOfficeUnitNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitNameBn'
                                           id='decisionByOfficeUnitNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='driverOfficeUnitNameBn'
                                           id='driverOfficeUnitNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='requesterNameBn'
                                           id='requesterNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByNameBn'
                                           id='decisionByNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverNameBn'
                                           id='driverNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverNameBn%>' tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitOrgNameEn'
                                           id='requesterOfficeUnitOrgNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitOrgNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitOrgNameEn'
                                           id='decisionByOfficeUnitOrgNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitOrgNameEn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='driverOfficeUnitOrgNameEn'
                                           id='driverOfficeUnitOrgNameEn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitOrgNameEn%>'
                                           tag='pb_html'/>


                                    <input type='hidden' class='form-control'
                                           name='requesterOfficeUnitOrgNameBn'
                                           id='requesterOfficeUnitOrgNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterOfficeUnitOrgNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='decisionByOfficeUnitOrgNameBn'
                                           id='decisionByOfficeUnitOrgNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByOfficeUnitOrgNameBn%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control'
                                           name='driverOfficeUnitOrgNameBn'
                                           id='driverOfficeUnitOrgNameBn_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverOfficeUnitOrgNameBn%>'
                                           tag='pb_html'/>

                                    <input type='hidden' class='form-control' name='requesterPhoneNum'
                                           id='requesterPhoneNum_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.requesterPhoneNum%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='decisionByPhoneNum'
                                           id='decisionByPhoneNum_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.decisionByPhoneNum%>'
                                           tag='pb_html'/>
                                    <input type='hidden' class='form-control' name='driverPhoneNum'
                                           id='driverPhoneNum_hidden_<%=i%>'
                                           value='<%=vm_requisitionDTO.driverPhoneNum%>' tag='pb_html'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 form-actions text-right mt-3">
                        <a class="btn btn-sm cancel-btn text-white shadow btn-border-radius"
                           href="<%=request.getHeader("referer")%>"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button class="btn btn-sm submit-btn text-white shadow btn-border-radius ml-2"
                                onclick="event.preventDefault();submitBigForm()"><%=LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<jsp:include page="../employee_assign/employeeSearchModal.jsp">
    <jsp:param name="isHierarchyNeeded" value="false"/>
</jsp:include>


<script type="text/javascript">


    function processResponse(data) {

        var lineManagerErr, invalidInputErr;
        var language = '<%=Language%>';
        lineManagerErr = language == 'English' ? 'No line manager found' : 'লাইন ম্যানেজার খুঁজে পাওয়া যায়নি';
        invalidInputErr = language == 'English' ? 'Invalid Input!!' : 'প্রদত্ত ইনপুট বৈধ নয়';

        if (data.includes('Employee already has vehicle')) {
            //let hasVehicle = language == 'English' ? 'Employee already has government vehicle' : 'প্রদত্ত কর্মকর্তার সরকারি যানবাহন আছে';
            let hasVehicle = language == 'English' ? 'You are not eligible to apply vehicle requisition' :
                'আপনি যানবাহন অধিযাচনের জন্য আবেদন করতে পারবেন না';
            bootbox.alert(hasVehicle, function (result) {
            });
        }

        else if (data.includes('Requisition exists')) {
            bootbox.alert("<%=LM.getText(LC.VM_REQUISITION_ADD_ANOTHEREXISTS, loginDTO)%>", function (result) {
            });
        } else if (data.includes('No line manager')) {
            bootbox.alert(lineManagerErr, function (result) {
            });
        } else if (data.includes('Invalid Input')) {
            bootbox.alert(invalidInputErr, function (result) {
            });
        } else {
            window.location = 'Vm_requisitionServlet?actionType=search';
        }
    }

    function ajaxPost(url, accept, send, postData, onSuccess, onError) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: accept,
            contentType: send,
            data: postData,
            success: onSuccess,
            error: onError,
            complete: function () {
                // $.unblockUI();
            }
        });
    }


    function submitBigForm() {

        if (PreprocessBeforeSubmiting(0, '<%=actionName%>')) {
            var form = $("#bigform");

            var actionUrl = form.attr("action");
            var postData = (form.serialize());

            ajaxPost(actionUrl, "json", "application/x-www-form-urlencoded", postData, processResponse, processResponse);
        }


    }

    function PreprocessBeforeSubmiting(row, validate) {

        preprocessDateBeforeSubmitting('startDate', row);
        preprocessDateBeforeSubmitting('endDate', row);
        preprocessTimeBeforeSubmitting('startTime', row);
        preprocessTimeBeforeSubmitting('endTime', row);


        let data = added_requester_map.keys().next();
        if (!(data && data.value)) {
            toastr.error("Please Select Person");
            return false;
        }
        document.getElementById('requesterEmpId_hidden_0').value = JSON.stringify(
            Array.from(added_requester_map.values()));

        let form = $("#bigform");
        form.validate();
        let valid = form.valid();

        return valid;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Vm_requisitionServlet");
    }

    function init(row) {

        setDateByStringAndId('startDate_js_' + row, $('#startDate_date_' + row).val());
        setDateByStringAndId('endDate_js_' + row, $('#endDate_date_' + row).val());
        setTimeById('startTime_js_' + row, $('#startTime_time_' + row).val());
        setTimeById('endTime_js_' + row, $('#endTime_time_' + row).val());

        select2SingleSelector("#vehicleRequisitionLocationCat_0", '<%=Language%>');
        select2SingleSelector("#vehicleRequisitionPurposeCat_category_0", '<%=Language%>');
        select2SingleSelector("#vehicleTypeCat_category_0", '<%=Language%>');


    }

    var row = 0;
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();

        $.validator.addMethod('validSelector', function (value, element) {
            return value && value != -1;
        });

        $.validator.addMethod('validEndDate', function (value, element) {
            var startDate = $("#startDate_date_0").val();
            var endDate = $("#endDate_date_0").val();

            var day = parseInt(startDate.split('/')[0]);
            var month = parseInt(startDate.split('/')[1]);
            var year = parseInt(startDate.split('/')[2]);
            var start = new Date(year, parseInt(month) - 1, day);

            day = parseInt(endDate.split('/')[0]);
            month = parseInt(endDate.split('/')[1]);
            year = parseInt(endDate.split('/')[2]);
            var end = new Date(year, parseInt(month) - 1, day);

            return start <= end;
        });

        $.validator.addMethod('validStartAddresses', function (value, element) {
            return $("#startAddress_TextField_0").val().toString().trim().length > 0;
        });

        $.validator.addMethod('validEndAddresses', function (value, element) {
            return $("#endAddress_TextField_0").val().toString().trim().length > 0;
        });

        // $.validator.addMethod('validStartTime',function(value,element){
        // 	return timeNotEmptyValidator('startTime_time_0', {
        // 	});
        // });
        //
        // $.validator.addMethod('validEndTime',function(value,element){
        // 	return timeNotEmptyValidator('endTime_time_0', {
        // 	});
        // });

        $.validator.addMethod('nonEmpty', function (value, element) {
            return value.toString().trim().length > 0;
        });

        let lang = '<%=Language%>';
        let startAddressErr;
        let endAddressErr;
        let startTimeErr;
        let endTimeErr;
        let purposeCatErr;
        let endDateValidityErr;
        let vehicleTypeCatErr;

        if (lang == 'english') {
            startAddressErr = 'Please provide address';
            endAddressErr = 'Please provide address';
            startTimeErr = 'Please provide start time';
            endTimeErr = 'Please provide end time';
            purposeCatErr = 'Please select purpose';
            endDateValidityErr = 'End Date cannot be before start date';
            vehicleTypeCatErr = 'Please provide vehicle type';

        } else {
            startAddressErr = 'ঠিকানা প্রদান করুন';
            endAddressErr = 'ঠিকানা প্রদান করুন';
            startTimeErr = 'শুরুর সময় প্রদান করুন';
            endTimeErr = 'শেষ সময় প্রদান করুন';
            purposeCatErr = 'উদ্দেশ্য সিলেক্ট করুন';
            endDateValidityErr = 'শেষ তারিখ শুরুর তারিখের পূর্বে হতে পারেনা';
            vehicleTypeCatErr = 'গাড়ির ধরণ প্রদান করুন';

        }

        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                startAddresses: {
                    validStartAddresses: true,
                },
                endAddresses: {
                    validEndAddresses: true,
                },
                startTime: {
                    nonEmpty: true,
                },
                endTime: {
                    nonEmpty: true,
                },
                vehicleRequisitionPurposeCat: {
                    validSelector: true,
                },
                endDate: {
                    validEndDate: true,
                },
                vehicleTypeCat: {
                    validSelector: true,
                },

            },
            messages: {
                startAddresses: startAddressErr,
                endAddresses: endAddressErr,
                startTime: startTimeErr,
                endTime: endTimeErr,
                vehicleRequisitionPurposeCat: purposeCatErr,
                endDate: endDateValidityErr,
                vehicleTypeCat: vehicleTypeCatErr,

            }
        });

        vehicleTypeChanged(document.getElementById("vehicleRequisitionPurposeCat_category_0"));
    });

    var child_table_extra_id = <%=childTableStartingID%>;


    $(".noDollar").keypress(function (evt) {
        let keycode = evt.charCode || evt.keyCode;
        if (keycode == 36) {
            return false;
        }
    });

    function insertAddressRow(tableId) {
        var type = tableId.toString().substring(0, tableId.toString().lastIndexOf('Table'));

        var myTable = document.getElementById(tableId);
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);

        var linksBox = document.createElement("input");
        linksBox.setAttribute("name", type + "es");
        linksBox.setAttribute("id", type + "_TextField_" + currentIndex);
        linksBox.setAttribute("type", "text");
        linksBox.setAttribute("class", "form-control noDollar");
        linksBox.setAttribute("placeholder", "Road Number, House Number etc");
        linksBox.setAttribute("tag", "pb_html");

        var addRowBox = document.createElement("button");
        addRowBox.setAttribute("type", "button");
        // addRowBox.setAttribute("value", "-");
        addRowBox.innerHTML="<i class='fa fa-minus'></i>"
        addRowBox.setAttribute("onclick", "deleteAddressRow('" + tableId + "', this);");
        addRowBox.setAttribute("class", "btn form-control btn-warning shadow btn-border-radius text-white pl-4");

        var currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(linksBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(addRowBox);

        $(".noDollar").keypress(function (evt) {
            let keycode = evt.charCode || evt.keyCode;
            if (keycode == 36) {
                return false;
            }
        });
    }


    function deleteAddressRow(tableId, button) {
        var myTable = document.getElementById(tableId);
        var rowIndex = button.parentNode.parentNode.rowIndex;
        myTable.deleteRow(rowIndex);
    }


    // TODO: EMPLOYEE SEARCH MODAL
    // select action of modal's add button

    // map to store and send added employee data
    // map to store and send added employee data


    <%
    List<EmployeeSearchIds> addedEmpIdsList = null;
    if(actionName.equals("edit")) {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (vm_requisitionDTO.requesterEmpId,vm_requisitionDTO.requesterOfficeUnitId,vm_requisitionDTO.requesterOrgId));
    }
    else {
        addedEmpIdsList = Arrays.asList(new EmployeeSearchIds
        (own.employeeRecordId,own.officeUnitId,own.organogramId));
    }
    %>

    added_requester_map = new Map(<%=EmployeeSearchModalUtil.initJsMap(addedEmpIdsList)%>);


    /* IMPORTANT
     * This map is converts table name to the table's added employees map
     */
    table_name_to_collcetion_map = new Map(
        [
            ['tagged_requester_table', {
                info_map: added_requester_map,
                isSingleEntry: true
            }]
        ]
    );

    // modal row button desatination table in the page
    modal_button_dest_table = 'none';

    // modal trigger button
    $('#tagRequester_modal_button').on('click', function () {
        // alert('CLICKED');
        modal_button_dest_table = 'tagged_requester_table';
        $('#search_emp_modal').modal();
    });

    function vehicleTypeChanged(selected){
        let vehicleType = selected.value;
        if(+vehicleType == 2){
               let element = document.querySelector('.fileDropzoneCls');
               element.classList.add('hiddenDropzone');
        }
        else{
            let element = document.querySelector('.fileDropzoneCls');
            element.classList.remove('hiddenDropzone');
        }
    }


</script>


<style>
    .required {
        color: red;
    }
    .hiddenDropzone{
        display: none;
    }
</style>



