<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="vm_requisition.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="files.*" %>

<%@ page import="vm_vehicle.Vm_vehicleDTO" %>
<%@ page import="vm_vehicle.Vm_vehicleRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="util.StringUtils" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="employee_records.Employee_recordsDTO" %>


<%
    String servletName = "Vm_requisitionServlet";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Vm_requisitionDAO vm_requisitionDAO = new Vm_requisitionDAO("vm_requisition");
    Vm_requisitionDTO vm_requisitionDTO = Vm_requisitionRepository.getInstance().getVm_requisitionDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    String context = request.getContextPath() + "/";

%>


<%@ include file="vm_requisitionCommonViewBody.jsp" %>
<%if(vm_requisitionDTO.status == CommonApprovalStatus.PRE_PENDING.getValue()){%>
<div class="form-actions text-center mb-5">
    <button class="btn btn-danger"
            onclick="event.preventDefault();location.href='Vm_requisitionServlet?actionType=decidePrepending&iD=<%=vm_requisitionDTO.iD%>&decision=Reject'">
        <%=LM.getText(LC.HM_REJECT, loginDTO)%>
    </button>
    <button class="btn btn-success"
            onclick="event.preventDefault();location.href='Vm_requisitionServlet?actionType=decidePrepending&iD=<%=vm_requisitionDTO.iD%>&decision=Approve'">
        <%=LM.getText(LC.HM_APPROVE, loginDTO)%>
    </button>
</div>
<%}%>




