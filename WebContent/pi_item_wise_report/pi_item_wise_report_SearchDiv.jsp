<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>

<%@ page import="pb.*" %>
<%
    
    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.PI_ITEM_WISE_REPORT_EDIT_LANGUAGE, loginDTO);
	CommonDAO.language = Language;
    CatDAO.language = Language;
%>

<input type='hidden'  name='Language' id = 'Language' value='<%=Language%>' />
<div class="row">
    <div class="col-12">
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.PI_ITEM_WISE_REPORT_WHERE_PROCUREMENTPACKAGETYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='procurementPackageType' id = 'procurementPackageType' >		
						<%		
						Options = CommonDAO.getOptions(Language, "procurement_package", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.PI_ITEM_WISE_REPORT_WHERE_PROCUREMENTGOODSTYPETYPE, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<select class='form-control'  name='procurementGoodsTypeType' id = 'procurementGoodsTypeType' >		
						<%		
						Options = CommonDAO.getOptions(Language, "procurement_goods_type", CatDTO.CATDEFAULT);								
						%>
						<%=Options%>
					</select>
				</div>
			</div>
		</div>
		<div  class="search-criteria-div">
			<div class="form-group row">
				<label class="col-sm-3 control-label text-right">
					<%=LM.getText(LC.PI_ITEM_WISE_REPORT_WHERE_NAMEBN, loginDTO)%>
				</label>
				<div class="col-sm-9">
					<input class='form-control'  name='nameBn' id = 'nameBn' value=""/>							
				</div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
function init()
{
    dateTimeInit($("#Language").val());
}
function PreprocessBeforeSubmiting()
{
}

$(document).ready(function () {
	select2SingleSelector('#procurementPackageType','<%=Language%>');
	select2SingleSelector('#procurementGoodsTypeType','<%=Language%>');
});
</script>