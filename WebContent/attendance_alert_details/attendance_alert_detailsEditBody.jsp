
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="attendance_alert_details.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>

<%
Attendance_alert_detailsDTO attendance_alert_detailsDTO;
attendance_alert_detailsDTO = (Attendance_alert_detailsDTO)request.getAttribute("attendance_alert_detailsDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(attendance_alert_detailsDTO == null)
{
	attendance_alert_detailsDTO = new Attendance_alert_detailsDTO();
	
}
System.out.println("attendance_alert_detailsDTO = " + attendance_alert_detailsDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.ATTENDANCE_ALERT_DETAILS_ADD_ATTENDANCE_ALERT_DETAILS_ADD_FORMNAME, loginDTO);


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Attendance_alert_detailsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
		id="bigform" name="bigform"  method="POST" enctype = "multipart/form-data"
		onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">
				
				
				




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ATTENDANCE_ALERT_DETAILS_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>


		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=attendance_alert_detailsDTO.iD%>' tag='pb_html'/>
	
												

		<input type='hidden' class='form-control'  name='employeeRecordsId' id = 'employeeRecordsId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.employeeRecordsId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='supervisorOrganogramId' id = 'supervisorOrganogramId_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.supervisorOrganogramId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.ATTENDANCE_ALERT_DETAILS_ADD_SUPERVISORNAME, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'supervisorName_div_<%=i%>'>	
		<input type='text' class='form-control'  name='supervisorName' id = 'supervisorName_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.supervisorName + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.ATTENDANCE_ALERT_DETAILS_ADD_SUPERVISORDESIGNATION, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'supervisorDesignation_div_<%=i%>'>	
		<input type='text' class='form-control'  name='supervisorDesignation' id = 'supervisorDesignation_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.supervisorDesignation + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
</div>			
				
	
<label class="col-lg-3 control-label">
	<%=LM.getText(LC.ATTENDANCE_ALERT_DETAILS_ADD_ALERTSENDTIME, loginDTO)%>
</label>
<div class="form-group ">					
	<div class="col-lg-6 " id = 'alertSendTime_div_<%=i%>'>	
			<%
                 value = "";
                 if(actionName.equals("edit")) {
                	 value = TimeFormat.getInAmPmFormat(attendance_alert_detailsDTO.alertSendTime);
                 }
             %>	
			<div class='input-group date edms-datetimepicker'>
                <input type='text' class="form-control" value="<%=value%>" id = 'alertSendTime_time_<%=i%>' name='alertSendTime'  tag='pb_html'/>
                <span class="input-group-addon" style="width:45px;">
                   <span><i class="fa fa-clock-o"></i></span>
                </span>
            </div>	
		
	</div>
</div>			
				

		<input type='hidden' class='form-control'  name='insertedBy' id = 'insertedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.insertedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='modifiedBy' id = 'modifiedBy_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.modifiedBy + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.searchColumn + "'"):("'" + "" + "'")%> tag='pb_html'/>
												

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value=<%=actionName.equals("edit")?("'" + attendance_alert_detailsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
												
					
	






				<div class="form-actions text-center">
					<a class="btn btn-danger" href="<%=request.getHeader("referer")%>">					
						<%=LM.getText(LC.ATTENDANCE_ALERT_DETAILS_ADD_ATTENDANCE_ALERT_DETAILS_CANCEL_BUTTON, loginDTO)%>						
					</a>
					<button class="btn btn-success" type="submit">
					
						<%=LM.getText(LC.ATTENDANCE_ALERT_DETAILS_ADD_ATTENDANCE_ALERT_DETAILS_SUBMIT_BUTTON, loginDTO)%>						
					
					</button>
				</div>
							
			</div>
		
		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">


$(document).ready( function(){

    dateTimeInit("<%=Language%>");
});

function PreprocessBeforeSubmiting(row, validate)
{
	if(validate == "report")
	{
	}
	else
	{
		var empty_fields = "";
		var i = 0;


		if(empty_fields != "")
		{
			if(validate == "inplaceedit")
			{
				$('<input type="submit">').hide().appendTo($('#tableForm')).click().remove(); 
				return false;
			}
		}

	}


	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Attendance_alert_detailsServlet");	
}

function init(row)
{


	
}

var row = 0;
	
window.onload =function ()
{
	init(row);
	CKEDITOR.replaceAll();
}

var child_table_extra_id = <%=childTableStartingID%>;



</script>






