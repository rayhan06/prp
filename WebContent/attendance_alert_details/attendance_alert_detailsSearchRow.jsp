<%@page pageEncoding="UTF-8" %>

<%@page import="attendance_alert_details.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.ATTENDANCE_ALERT_DETAILS_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_ATTENDANCE_ALERT_DETAILS;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Attendance_alert_detailsDTO attendance_alert_detailsDTO = (Attendance_alert_detailsDTO)request.getAttribute("attendance_alert_detailsDTO");
CommonDTO commonDTO = attendance_alert_detailsDTO;
String servletName = "Attendance_alert_detailsServlet";


System.out.println("attendance_alert_detailsDTO = " + attendance_alert_detailsDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Attendance_alert_detailsDAO attendance_alert_detailsDAO = (Attendance_alert_detailsDAO)request.getAttribute("attendance_alert_detailsDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

											
		
											
											<td id = '<%=i%>_employeeRecordsId'>
											<%
											value = attendance_alert_detailsDTO.employeeRecordsId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_supervisorOrganogramId'>
											<%
											value = attendance_alert_detailsDTO.supervisorOrganogramId + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_supervisorName'>
											<%
											value = attendance_alert_detailsDTO.supervisorName + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_supervisorDesignation'>
											<%
											value = attendance_alert_detailsDTO.supervisorDesignation + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_alertSendTime'>
											<%
											value = attendance_alert_detailsDTO.alertSendTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
											
											<td id = '<%=i%>_alertSendTime'>
											    <%
        value = attendance_alert_detailsDTO.attendanceDate + "";
    %>
    <%
        String formatted_startDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_startDate, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_alertSendTime'>
											<%
											value = attendance_alert_detailsDTO.inTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
											
											<td id = '<%=i%>_alertSendTime'>
											<%
											value = attendance_alert_detailsDTO.outTime + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
		
											
		
											
		
											
		
	

											<td>
												<a href='Attendance_alert_detailsServlet?actionType=view&ID=<%=attendance_alert_detailsDTO.iD%>'><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></a>
										
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<a href='Attendance_alert_detailsServlet?actionType=getEditPage&ID=<%=attendance_alert_detailsDTO.iD%>'><%=LM.getText(LC.ATTENDANCE_ALERT_DETAILS_SEARCH_ATTENDANCE_ALERT_DETAILS_EDIT_BUTTON, loginDTO)%></a>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=attendance_alert_detailsDTO.iD%>'/></span>
												</div>
											</td>
																						
											

