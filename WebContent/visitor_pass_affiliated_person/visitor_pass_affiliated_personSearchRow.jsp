<%@page pageEncoding="UTF-8" %>

<%@page import="visitor_pass_affiliated_person.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_EDIT_LANGUAGE, loginDTO);
String Language2 = Language;

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_VISITOR_PASS_AFFILIATED_PERSON;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO = (Visitor_pass_affiliated_personDTO)request.getAttribute("visitor_pass_affiliated_personDTO");
CommonDTO commonDTO = visitor_pass_affiliated_personDTO;
String servletName = "Visitor_pass_affiliated_personServlet";


System.out.println("visitor_pass_affiliated_personDTO = " + visitor_pass_affiliated_personDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Visitor_pass_affiliated_personDAO visitor_pass_affiliated_personDAO = (Visitor_pass_affiliated_personDAO)request.getAttribute("visitor_pass_affiliated_personDAO");


String Options = "";
boolean formSubmit = false;
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>
		
											<td id = '<%=i%>_name'>
											<%
											value = visitor_pass_affiliated_personDTO.name + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
											<td id = '<%=i%>_credentialNo'>
											<%
											value = visitor_pass_affiliated_personDTO.credentialNo + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_mobileNumber'>
											<%
											value = visitor_pass_affiliated_personDTO.mobileNumber + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td id = '<%=i%>_nationalityType'>
											<%
											value = visitor_pass_affiliated_personDTO.nationalityType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "nationality", Language.equals("English")?"name_en":"name_bn", "id");
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
		
		
	

											<td>
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #22ccc1; color: white; border-radius: 8px"
											            onclick="location.href='Visitor_pass_affiliated_personServlet?actionType=view&ID=<%=visitor_pass_affiliated_personDTO.iD%>'">
											        <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
											    </button>												
											</td>
	
											<td id = '<%=i%>_Edit'>																																	
	
												<button type="button" class="btn btn-sm border-0 shadow" style="background-color: #cc22c1; color: white; border-radius: 8px"
											            onclick="location.href='Visitor_pass_affiliated_personServlet?actionType=getEditPage&ID=<%=visitor_pass_affiliated_personDTO.iD%>'">
											        <%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_SEARCH_VISITOR_PASS_AFFILIATED_PERSON_EDIT_BUTTON, loginDTO)%>
											    </button>
																				
											</td>											
											
											
											<td id='<%=i%>_checkbox'>
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=visitor_pass_affiliated_personDTO.iD%>'/></span>
												</div>
											</td>
																						
											

