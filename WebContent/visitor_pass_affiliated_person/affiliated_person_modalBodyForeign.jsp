<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="gate_pass_affiliated_person.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="geolocation.GeoLocationDAO2" %>
<%@page import="util.TimeFormat" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<%@ page import="pb.*" %>
<%@ page import="visitor_pass_affiliated_person.Visitor_pass_affiliated_personDTO" %>

<%
    Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO;
    visitor_pass_affiliated_personDTO = (Visitor_pass_affiliated_personDTO) request.getAttribute("visitor_pass_affiliated_personDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    if (visitor_pass_affiliated_personDTO == null) {
        visitor_pass_affiliated_personDTO = new Visitor_pass_affiliated_personDTO();

    }
    System.out.println("gate_pass_affiliated_personDTO = " + visitor_pass_affiliated_personDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_VISITOR_PASS_AFFILIATED_PERSON_ADD_FORMNAME, loginDTO);


    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;

    String Language = LM.getText(LC.GATE_PASS_AFFILIATED_PERSON_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    int personCount = 0;
    if (request.getParameter("personCount") != "")
        personCount = Integer.parseInt(request.getParameter("personCount"));
    int startVal = Integer.parseInt(request.getParameter("startVal"));
    for (int x = startVal; x < personCount; x++) {
%>

<div class="affliatedPerson" id="affliatedPerson_div_<%=x%>">
    <div class="form-group row" id="person_row_div">
        <div class="col-4 " id='name_div_<%=x%>'>
            <input type='text' class='form-control name' name='name_<%=x%>'
                   id='name_<%=x%>'
                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Enter Name" : "ব্যক্তির নাম লিখুন"%>'
                   tag='pb_html'/>
            <span class="name-error"
                  style="color:#ff0000; text-align:left; font-size:x-small; margin-top: auto; display: none"></span>
        </div>
        <div class="col-4 englishDigitOrCharOnly<%=x%>" id='credentialNo_div_<%=x%>'>
            <input type='text' class='form-control credential-no' name='credentialNo_<%=x%>'
                   id='credentialNo_<%=x%>'
                   placeholder='<%=Language.equalsIgnoreCase("English") ? "passport number" : "পাসপোর্ট নাম্বার"%>'
                   tag='pb_html'/>
            <span class="affiliated-id-error"
                  style="color:#ff0000; text-align:left; font-size:x-small; margin-top: auto; display: none"></span>
        </div>
        <div class="col-4 " id='nationality_div_<%=x%>'>   
            <input type='text' class='form-control nationality' name='nationality_<%=x%>'
                   id='nationality_<%=x%>'
                   placeholder='<%=Language.equalsIgnoreCase("English") ? "Nationality" : "জাতীয়তা"%>'
                   tag='pb_html'/>
            <span class="nationality-error"
                  style="color:#ff0000; text-align:left; font-size:x-small; margin-top: auto; display: none"></span>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $("#credentialNo_<%=x%>").on("input", function () {
            const element = $(this).closest('.affliatedPerson').find('.affiliated-id-error');
            if ($(this).val() != "") element.hide();
        });

        $("#name_<%=x%>").on("input", function () {
            const element = $(this).closest('.affliatedPerson').find('.name-error');
            if ($(this).val() != "") element.hide();
        });

        $("#nationality_<%=x%>").on("input", function () {
            const element = $(this).closest('.affliatedPerson').find('.nationality-error');
            if ($(this).val() != "") element.hide();
        });

    });

    function populateInputFields<%=x%>(event) {
        let english = /^[A-Za-z0-9]*$/;
        let mobileNo = $('#mobileNumber_<%=x%>').val();

        if (mobileNo.length === 11) {
            if (!english.test(mobileNo)) {
                mobileNo = convertToEnglishNumber(mobileNo);
                $('#mobileNumber_<%=x%>').val(mobileNo);
            }
            mobileNo = '88' + mobileNo;
            console.log(mobileNo);
            // ajax call for stored data
            let url = "Gate_pass_affiliated_personServlet?actionType=getStoredDataByMobileNumber" + "&mobileNumber=" + mobileNo;

            $.ajax({
                url: url,
                type: "GET",
                async: true,
                success: function (data) {

                    if (data.localeCompare('noString') === -1) {

                        let dto = JSON.parse(data);

                        // Now populate data with stored data
                        $('#credentialType_<%=x%>').val(dto.credentialType);
                        $('#credentialType_<%=x%>').prop("disabled", true);

                        $('#credentialNo_<%=x%>').val(dto.credentialNo);
                        $('#credentialNo_<%=x%>').prop("readonly", true);

                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    $(".englishDigitOrCharOnly<%=x%>").keypress(function (event) {
        var ew = event.which;
        if (ew == 32)
            return true;
        var charStr = String.fromCharCode(ew);

        if (charStr == "0") {
            return true;
        } else if (charStr == "1") {
            return true;
        } else if (charStr == "2") {
            return true;
        } else if (charStr == "3") {
            return true;
        } else if (charStr == "4") {
            return true;
        } else if (charStr == "5") {
            return true;
        } else if (charStr == "6") {
            return true;
        } else if (charStr == "7") {
            return true;
        } else if (charStr == "8") {
            return true;
        } else if (charStr == "9") {
            return true;
        } else if (charStr >= 'A' && charStr <= 'Z'){
            return true;
        } else if (charStr >= 'a' && charStr <= 'z'){
            return true;
        }
        return false;
    });

</script>

<%
    }
%>