<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="visitor_pass_affiliated_person.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
Visitor_pass_affiliated_personDTO visitor_pass_affiliated_personDTO;
visitor_pass_affiliated_personDTO = (Visitor_pass_affiliated_personDTO)request.getAttribute("visitor_pass_affiliated_personDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
if(visitor_pass_affiliated_personDTO == null)
{
	visitor_pass_affiliated_personDTO = new Visitor_pass_affiliated_personDTO();
	
}
System.out.println("visitor_pass_affiliated_personDTO = " + visitor_pass_affiliated_personDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_VISITOR_PASS_AFFILIATED_PERSON_ADD_FORMNAME, loginDTO);
String servletName = "Visitor_pass_affiliated_personServlet";
String fileColumnName = "";

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

boolean isPermanentTable = true;
String Language = LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Visitor_pass_affiliated_personServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.iD%>' tag='pb_html'/>
	
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_NAME, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='name' id = 'name_text_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.name%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='visitorPassId' id = 'visitorPassId_hidden_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.visitorPassId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_CREDENTIALCAT, loginDTO)%></label>
                                                            <div class="col-8">
																<select class='form-control'  name='credentialCat' id = 'credentialCat_select2_<%=i%>'   tag='pb_html'>
                                                                    <%=CatRepository.getInstance().buildOptions("credential", Language, null)%>
                                                                </select>
		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_CREDENTIALNO, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='credentialNo' id = 'credentialNo_text_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.credentialNo%>' 																  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_MOBILENUMBER, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='mobileNumber' id = 'mobileNumber_text_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.mobileNumber%>' 																<% 
																	if(!actionName.equals("edit"))
																	{
																%>
																		required="required"  pattern="880[0-9]{10}" title="mobileNumber must start with 880, then contain 10 digits"
																<%
																	}
																%>
  tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_NATIONALITYTYPE, loginDTO)%></label>
                                                            <div class="col-8">
                                                            	
																<select class='form-control'  name='nationalityType' id = 'nationalityType_select_<%=i%>'   tag='pb_html'>
																<%
																	Options = CommonDAO.getOptions(Language, "nationality", visitor_pass_affiliated_personDTO.nationalityType);
																%>
																<%=Options%>
																</select>
		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.insertedByOrganogramId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='lastModifierUser' id = 'lastModifierUser_hidden_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.lastModifierUser%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=visitor_pass_affiliated_personDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=visitor_pass_affiliated_personDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_VISITOR_PASS_AFFILIATED_PERSON_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.VISITOR_PASS_AFFILIATED_PERSON_ADD_VISITOR_PASS_AFFILIATED_PERSON_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, validate)
{



	return true;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Visitor_pass_affiliated_personServlet");	
}

function init(row)
{

	$("#credentialCat_select2_" + row).select2({
		dropdownAutoWidth: true
	});

	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






