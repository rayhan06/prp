<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="budget.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoRepository" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDTO" %>
<%@ page import="budget_submission_info.Budget_submission_infoDTO" %>
<%@ page import="budget_submission_info.Budget_submission_infoDAO" %>
<%@ page import="util.StringUtils" %>
<%@ page import="budget_institutional_group.Budget_institutional_groupRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="budget_preparation.Budget_preparationServlet" %>
<%@ page import="budget_selection_info.BudgetSelectionInfoDAO" %>

<%
    String context = request.getContextPath() + "/";

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

    String formTitle = LM.getText(LC.BUDGET_PREPARATION_FORM_NAME, loginDTO);

    String Language = LM.getText(LC.BUDGET_EDIT_LANGUAGE, loginDTO);
    CommonDAO.language = Language;
    CatDAO.language = Language;
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");

    BudgetInfo budgetInfo = BudgetUtils.getBudgetInfoForBudgetPreparation(System.currentTimeMillis());
    BudgetSelectionInfoDTO budgetSelectionInfoDTO = BudgetSelectionInfoDAO.getInstance().getDTOByEconomicYear(budgetInfo.economicYear);

    Budget_submission_infoDTO budgetSubmissionInfoDTO = null;
    BudgetTypeEnum budgetType = budgetInfo.budgetTypeEnum;
    String errorMessage = null;

    boolean isCodeSelectionDone = budgetSelectionInfoDTO != null && budgetSelectionInfoDTO.isSubmitted;
    if(!isCodeSelectionDone){
        budgetSelectionInfoDTO = new BudgetSelectionInfoDTO();
        errorMessage = UtilCharacter.getDataByLanguage(
                Language,
                "এই অর্থ বছরের বাজেটের কোড বাছাইকরণ প্রক্রিয়া এখনো সম্পন্ন হয়নি!",
                "Budget code selection process has not yet been completed for this year!"
        );
    }else {
        budgetSubmissionInfoDTO = Budget_submission_infoDAO.getInstance().getByBudgetTypeAndBudgetOffice(
                budgetSelectionInfoDTO.iD, budgetType.getValue(), BudgetUtils.FINANCE_SECTION_OFFICE_ID
        );
        if(!Budget_preparationServlet.hasFinanceSubmittedBudget(budgetInfo.budgetTypeEnum.getValue(), budgetSubmissionInfoDTO)){
            errorMessage = UtilCharacter.getDataByLanguage(
                    Language,
                    "অর্থ শাখা এখনো এই অর্থবছরের বাজেট জমা দেয়নি!",
                    "Finance has not yet submitted Budget for this Economic Year!"
            );
        }
    }

    boolean isBudgetPreparationSubmitted = budgetSubmissionInfoDTO != null
                                           && budgetSubmissionInfoDTO.isPreparationSubmit == SubmissionStatusEnum.SUBMITTED.getValue();

    String economicYearText = StringUtils.convertBanglaIfLanguageIsBangla(Language, budgetInfo.economicYear);
    String prevEconomicYearText = StringUtils.convertBanglaIfLanguageIsBangla(
            Language,
            BudgetInfo.getEconomicYear(budgetInfo.beginYear - 2)
    );
    String prevPrevEconomicYearText = StringUtils.convertBanglaIfLanguageIsBangla(
            Language,
            BudgetInfo.getEconomicYear(budgetInfo.beginYear - 3)
    );
%>

<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }

    .template-row {
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Budget_preparationServlet?actionType=insertBudgetForFinance"
              id="budget-form" name="budgetform" method="POST" enctype="multipart/form-data">
            <%if (errorMessage != null) {%>
            <div class="my-4">
                <h5 class="text-center" style="color: red">
                    <%=errorMessage%>
                </h5>
            </div>
            <%} else {%>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row">
                        <div class="offset-md-3 col-md-6 text-center">
                            <img width="15%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="budgetType" id="budgetType" value="<%=budgetType.getValue()%>">
                <input type='hidden' class='form-control' name='budgetSelectionInfoId' id='budgetSelectionInfoId' value=<%=budgetSelectionInfoDTO.iD%>>
                <input type="hidden" name="budgetModels" id="budgetModels">

                <div class="row mt-5">
                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="budgetInstitutionalGroup">
                            <%=LM.getText(LC.BUDGET_INSTITUTIONAL_GROUP, loginDTO)%>
                        </label>
                        <select id="budgetInstitutionalGroup" class='form-control rounded' onchange="institutionalGroupChanged(this);">
                            <%=Budget_institutional_groupRepository.getInstance().buildOptions(Language, 0L, true)%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="budgetCat">
                            <%=LM.getText(LC.BUDGET_BUDGET_TYPE, loginDTO)%>
                        </label>
                        <select id="budgetCat" class='form-control rounded' onchange="budgetCatChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="budgetOffice">
                            <%=LM.getText(LC.BUDGET_OFFICE, loginDTO)%>
                        </label>
                        <select id="budgetOffice" class='form-control rounded' onchange="budgetOfficeChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="budgetOperationId">
                            <%=LM.getText(LC.BUDGET_OPERATION_CODE, loginDTO)%>
                        </label>
                        <select id="budgetOperationId" class='form-control rounded'
                                onchange="budgetOperationChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="economicGroup">
                            <%=LM.getText(LC.BUDGET_ECONOMIC_GROUP, loginDTO)%>
                        </label>
                        <select id="economicGroup" class='form-control rounded' onchange="economicGroupChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>

                    <div class="col-md-6 col-lg-4 my-3">
                        <label class="h5" for="economicCodes">
                            <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                        </label>
                        <select id="economicCodes" class='form-control rounded' onchange="economicCodesChanged(this);">
                            <%--Dynamically Added with AJAX--%>
                        </select>
                    </div>
                </div>

                <div class="mt-5">
                    <h5 class="table-title text-right">
                        (<%=LM.getText(LC.BUDGET_MONEY_INPUT_UNIT_HEADER, loginDTO)%>)
                    </h5>
                    <div class="w-100">
                        <table class="table table-striped table-bordered text-nowrap" id="budget-view-table">
                            <thead>
                            <tr class="text-center">
                                <th class="row-data-code" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </th>
                                <th class="row-data-name" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                </th>
                                <th colspan="2">
                                    <%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE, loginDTO)%>
                                </th>
                                <th colspan="2">
                                    <%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE, loginDTO)%>
                                    <br>
                                    (<%=LM.getText(LC.BUDGET_FIRST_SIX_MONTHS, loginDTO)%>)
                                </th>
                                <th class="row-data-budget" rowspan="2">
                                    <%=CatRepository.getInstance().getText(Language, "budget_type", BudgetTypeEnum.REVISED_BUDGET.getValue())%>
                                    <br>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                            Language,
                                            BudgetInfo.getEconomicYear(budgetInfo.beginYear - 1)
                                    )%>
                                </th>
                                <th class="row-data-budget" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_ESTIMATES, loginDTO)%>
                                    <br>
                                    <%=economicYearText%>
                                </th>

                                <th class="row-data-budget" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_PREPARATION_CHANGE_RATE_, loginDTO)%>
                                </th>

                                <th class="row-data-projection_0" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_PROJECTION, loginDTO)%>
                                    <br>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                            Language,
                                            BudgetInfo.getEconomicYear(budgetInfo.beginYear + 1)
                                    )%>
                                </th>
                                <th class="row-data-projection_1" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_PROJECTION, loginDTO)%>
                                    <br>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                            Language,
                                            BudgetInfo.getEconomicYear(budgetInfo.beginYear + 2)
                                    )%>
                                </th>
                                <th class="row-data-preparation-comment" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_COMMENT, loginDTO)%>
                                </th>
                            </tr>
                            <tr>
                                <th class="row-data-revised-expenditure_0">
                                    <%=prevPrevEconomicYearText%>
                                </th>
                                <th class="row-data-revised-expenditure_1">
                                    <%=prevEconomicYearText%>
                                </th>
                                <th class="row-data-expenditure_0">
                                    <%=prevPrevEconomicYearText%>
                                </th>
                                <th class="row-data-expenditure_1">
                                    <%=prevEconomicYearText%>
                                </th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                            <tr class="template-row">
                                <td class="row-data-code"></td>
                                <td class="row-data-name"></td>
                                <td class="row-data-revised-expenditure_0"></td>
                                <td class="row-data-revised-expenditure_1"></td>
                                <td class="row-data-expenditure_0"></td>
                                <td class="row-data-expenditure_1"></td>
                                <td class="row-data-revised-budget_2"></td>
                                <td class="row-data-final-budget"></td>
                                <td class="row-data-change-rate"></td>
                                <td class="row-data-projection_0"></td>
                                <td class="row-data-projection_1"></td>
                                <td class="row-data-preparation-comment"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mt-3 text-right">
                        <button class="btn btn-sm shadow text-white border-0 btn-border-radius " type="button"
                                style="background: #4db2d1;"
                                onclick="location.href='Budget_preparationServlet?actionType=getPreviewPage'">
                            <%=LM.getText(LC.BUDGET_PREVIEW, loginDTO)%>
                        </button>

                        <button id="cancel-btn" class="btn btn-sm shadow text-white border-0 btn-border-radius cancel-btn mx-2"
                                type="button"
                                onclick="resetPage();">
                            <%=LM.getText(LC.BUDGET_ADD_BUDGET_CANCEL_BUTTON, loginDTO)%>
                        </button>

                        <%if (!isBudgetPreparationSubmitted) {%>
                        <button class="btn btn-sm shadow text-white border-0 btn-border-radius submit-btn" type="submit">
                            <%=LM.getText(LC.BUDGET_ADD_BUDGET_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                        <%}%>
                    </div>
                </div>
            </div>
            <%}%>
        </form>
    </div>
</div>

<%if (errorMessage == null) {%>
<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>

<script type="text/javascript">
    const budgetModelMap = new Map();
    const isBudgetSubmitted = <%=isBudgetPreparationSubmitted%>;
    const REVISED_BUDGET = '<%=BudgetTypeEnum.REVISED_BUDGET.getValue()%>';

    function resetPage() {
        document.getElementById('budgetInstitutionalGroup').value = '';
        clearSelects(0);
        clearTable();
    }

    $(() => {
        clearTable();

        $('#budget-form').submit(async (event) => {
            event.preventDefault();

            if (isBudgetSubmitted) return;

            const budgetModels = Array.from(budgetModelMap.values());

            for (let budgetModel of budgetModels) {

                budgetModel.projection1 = document.getElementById('projection_0_' + budgetModel.id).value;
                budgetModel.projection2 = document.getElementById('projection_1_' + budgetModel.id).value;
                budgetModel.preparationComment = document.getElementById('comment_' + budgetModel.id).value;

                budgetModelMap.set(Number(budgetModel.id), budgetModel);
            }

            $('#budgetModels').val(JSON.stringify(Array.from(budgetModelMap.values())));

            const formElement = event.target;
            const formData = new FormData(formElement);
            const url = formElement.action;
            const searchParam = new URLSearchParams(formData);

            try {
                const response = await fetch(url, {
                    method: 'post',
                    body: searchParam
                });
                const json = await response.json();
                if (!json.success) throw new Error(json);

                $('#toast_message').css('background-color', '#04c73c');
                showToast(
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVED,"Bangla")%>',
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVED,"English")%>'
                );
            } catch (error) {
                console.error(error);
                $('#toast_message').css('background-color', '#ff6063');
                showToastSticky(
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVING_FAILED,"Bangla")%>',
                    '<%=LM.getText(LC.BUDGET_SUBMISSION_BUDGET_SAVING_FAILED,"English")%>'
                );
            }
        });
    });

    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "BudgetServlet");
    }

    function clearSelects(startIndex) {
        const selectIds = ['budgetCat', 'budgetOffice', 'budgetOperationId', 'economicGroup', 'economicCodes'];
        for (let i = startIndex; i < selectIds.length; i++) {
            const selectElement = document.getElementById(selectIds[i]);
            selectElement.innerHTML = '';
        }
    }

    function getBudgetInputElement(id, value, onKeyUp) {
        const inputElement = document.createElement('input');
        inputElement.classList.add('form-control');
        inputElement.type = 'text';
        inputElement.id = id;
        inputElement.value = value;
        inputElement.onkeydown = keyDownEvent;
        if (onKeyUp) inputElement.onkeyup = onKeyUp;
        return inputElement;
    }

    function getBudgetCommentElement(id, value) {
        const textAreaElement = document.createElement('textarea');
        textAreaElement.classList.add('form-control');
        textAreaElement.id = id;
        textAreaElement.rows = 4;
        textAreaElement.cols = 30;
        textAreaElement.style.resize = 'none';
        textAreaElement.style.width = 'auto';
        textAreaElement.append(value);
        return textAreaElement;
    }

    function addDataInTable(tableId, jsonData) {
        const table = document.getElementById(tableId);


        const templateRow = table.querySelector('.template-row').cloneNode(true);
        templateRow.id = jsonData.economicSubCode;
        templateRow.classList.remove('template-row');

        templateRow.dataset.rowData = JSON.stringify(jsonData);

        templateRow.querySelector('td.row-data-code').innerText = jsonData.code;
        templateRow.querySelector('td.row-data-name').innerText = jsonData.name;


        const revisedExpenditurePrev0 = templateRow.querySelector('td.row-data-revised-expenditure_0');
        const revisedExpenditurePrev1 = templateRow.querySelector('td.row-data-revised-expenditure_1');

        const expenditurePrev0 = templateRow.querySelector('td.row-data-expenditure_0');
        const expenditurePrev1 = templateRow.querySelector('td.row-data-expenditure_1');

        const revisedBudget = templateRow.querySelector('td.row-data-revised-budget_2');
        const changeRate = templateRow.querySelector('td.row-data-change-rate');

        const finalBudget = templateRow.querySelector('td.row-data-final-budget');
        const projection0 = templateRow.querySelector('td.row-data-projection_0');
        const projection1 = templateRow.querySelector('td.row-data-projection_1');
        const preparationComment = templateRow.querySelector('td.row-data-preparation-comment');


        if (!isBudgetSubmitted) {
            projection0.append(
                getBudgetInputElement(
                    'projection_0_' + jsonData.id,
                    jsonData.projection1,
                    function () {sumInputElementColumn(this, 2);}
                )
            );

            projection1.append(
                getBudgetInputElement(
                    'projection_1_' + jsonData.id,
                    jsonData.projection2,
                    function () {sumInputElementColumn(this, 2);}
                )
            );

            preparationComment.append(
                getBudgetCommentElement('comment_' + jsonData.id, jsonData.preparationComment)
            );

        } else {
            projection0.innerText = jsonData.projection1;
            projection1.innerText = jsonData.projection2;
            preparationComment.innerText = jsonData.preparationComment;
        }
        finalBudget.innerText = jsonData.finalBudgetAmount;

        if (jsonData.previousAmounts.length != 0) {
            revisedExpenditurePrev0.innerText = Number(jsonData.previousAmounts[2].expenditure) + Number(jsonData.previousAmounts[2].revisedExpenditure);
            revisedExpenditurePrev1.innerText = Number(jsonData.previousAmounts[1].expenditure) + Number(jsonData.previousAmounts[1].revisedExpenditure);

            revisedBudget.innerText = jsonData.previousAmounts[0].revisedBudget;
            if (Number(jsonData.finalBudgetAmount) == 0.0) {
                changeRate.innerText = "0.0";
            } else {
                changeRate.innerText = (100 * (Number(jsonData.finalBudgetAmount) - Number(jsonData.previousAmounts[0].revisedBudget))) / Number(jsonData.finalBudgetAmount);

            }


            expenditurePrev0.innerText = jsonData.previousAmounts[2].expenditure;
            expenditurePrev1.innerText = jsonData.previousAmounts[1].expenditure;
        } else {
            revisedExpenditurePrev0.innerText = "0.0";
            revisedExpenditurePrev1.innerText = "0.0";
            revisedBudget.innerText = "0.0";
            expenditurePrev0.innerText = "0.0";
            expenditurePrev1.innerText = "0.0";
        }

        const tableBody = table.querySelector('tbody');
        tableBody.append(templateRow);
    }

    function clearTable() {
        budgetModelMap.clear();
        const noDataText = '<%=isLanguageEnglish? "No data found!" : "কোনো তথ্য পাওয়া যায় নি!"%>';
        const table = document.querySelector('#budget-view-table');
        table.querySelector('tbody').innerHTML = '<tr><td colspan="100%" class="text-center">' + noDataText + '</td></tr>';
        table.querySelectorAll('tfoot').forEach(tfoot => tfoot.remove());
    }

    async function institutionalGroupChanged(selectElement) {
        const selectedInstitutionalGroupId = selectElement.value;
        clearSelects(0);
        clearTable();
        if (selectedInstitutionalGroupId === '') return;

        const url = 'Budget_mappingServlet?actionType=getBudgetCatList&budget_instituitional_group_id='
            + selectedInstitutionalGroupId;
        const response = await fetch(url);
        document.getElementById('budgetCat').innerHTML = await response.text();
    }

    async function budgetCatChanged(selectElement) {
        const selectedBudgetCat = selectElement.value;
        clearSelects(1);
        clearTable();
        if (selectedBudgetCat === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;
        const url = 'Budget_mappingServlet?actionType=getBudgetOfficeList&withCode=true&budget_instituitional_group_id='
            + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat;
        const response = await fetch(url);
        document.getElementById('budgetOffice').innerHTML = await response.text();
    }

    async function budgetOfficeChanged(selectElement) {
        const selectedBudgetOffice = selectElement.value;
        clearSelects(2);
        clearTable();
        if (selectedBudgetOffice === '') return;

        const selectedInstitutionalGroup = document.getElementById('budgetInstitutionalGroup').value;
        const selectedBudgetCat = document.getElementById('budgetCat').value;

        const url = 'Budget_mappingServlet?actionType=getOperationCodeList&withCode=true&budget_instituitional_group_id='
            + selectedInstitutionalGroup + '&budget_cat=' + selectedBudgetCat
            + '&budget_office_id=' + selectedBudgetOffice;
        const response = await fetch(url);
        document.getElementById('budgetOperationId').innerHTML = await response.text();
    }

    async function fetchAndShowBudgetModels(url) {
        const response = await fetch(url);
        const budgetModels = await response.json();

        const tbody = document.getElementById('budget-view-table').querySelector('tbody');
        tbody.innerHTML = '';
        if (Array.isArray(budgetModels)) {
            for (const budgetModel of budgetModels) {
                addDataInTable('budget-view-table', budgetModel);
                budgetModelMap.set(
                    Number(budgetModel.id),
                    budgetModel
                );
            }

            const colIndicesToSum = [];
            for (let colIndex = 2; colIndex <= 10; colIndex++) {
                if (colIndex != 8)
                    colIndicesToSum.push(colIndex);
            }

            const totalTitleColSpan = 2;
            const totalTitle = '<%=LM.getText(LC.BUDGET_SUBTOTAL,loginDTO)%>';
            setupTotalRow('budget-view-table', totalTitle, totalTitleColSpan, colIndicesToSum, undefined, undefined, 2);
        }
    }

    async function budgetOperationChanged() {
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOffice').value;
        clearSelects(3);
        clearTable();
        if (selectedOperationGroup !== '') {
            const url = 'BudgetServlet?actionType=buildEconomicGroups&budgetSelectionInfoId='
                        + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup;

            const response = await fetch(url);
            document.getElementById('economicGroup').innerHTML = await response.text();
        }
        const budgetModelUrl =
            'Budget_preparationServlet?actionType=getBudgetModels&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup;
        await fetchAndShowBudgetModels(budgetModelUrl);
    }

    async function economicGroupChanged(selectElement) {
        const selectedEconomicGroup = selectElement.value;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOffice').value;
        clearSelects(4);
        clearTable();
        if (selectedEconomicGroup !== '') {
            const url = 'BudgetServlet?actionType=buildEconomicCodes&budgetSelectionInfoId='
                        + selectedSelectionCat + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
                        + '&economicGroupId=' + selectedEconomicGroup;
            const response = await fetch(url);
            document.getElementById('economicCodes').innerHTML = await response.text();
        }
        const budgetModelUrl =
            'Budget_preparationServlet?actionType=getBudgetModels&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
            + '&economicGroupId=' + selectedEconomicGroup;
        await fetchAndShowBudgetModels(budgetModelUrl);
    }

    async function economicCodesChanged(selectElement) {
        const selectedEconomicCode = selectElement.value;
        const selectedEconomicGroup = document.getElementById('economicGroup').value;
        const selectedSelectionCat = document.getElementById('budgetSelectionInfoId').value;
        const selectedOperationGroup = document.getElementById('budgetOperationId').value;
        const selectedBudgetOffice = document.getElementById('budgetOffice').value;
        clearTable();
        const budgetModelUrl =
            'Budget_preparationServlet?actionType=getBudgetModels&budgetSelectionInfoId=' + selectedSelectionCat
            + '&budgetOfficeId=' + selectedBudgetOffice + '&budgetOperationId=' + selectedOperationGroup
            + '&economicGroupId=' + selectedEconomicGroup + '&economicCodeId=' + selectedEconomicCode;
        await fetchAndShowBudgetModels(budgetModelUrl);
    }

    function keyDownEvent(e) {
        return true == inputValidationForFloatValue(e, $(this), 3, null);
    }
</script>
<%}%>