<%@ page import="login.LoginDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="budget_mapping.Budget_mappingRepository" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="budget_operation.BudgetOperationModel" %>
<%@ page import="economic_code.Economic_codeDTO" %>
<%@ page import="common.NameDTO" %>
<%@ page import="economic_group.EconomicGroupRepository" %>
<%@ page import="economic_code.Economic_codeRepository" %>
<%@ page import="economic_sub_code.Economic_sub_codeDTO" %>
<%@ page import="economic_sub_code.Economic_sub_codeRepository" %>
<%@ page import="java.util.*" %>
<%@ page import="util.StringUtils" %>
<%@ page import="static java.util.stream.Collectors.toList" %>
<%@ page import="budget_submission_info.Budget_submission_infoDTO" %>
<%@ page import="budget.*" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.CARD_INFO_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = "English".equalsIgnoreCase(Language);

    String context = request.getContextPath() + "/";

    Long thisYearId = (Long) request.getAttribute("thisYearId");

    Long prevYearId = (Long) request.getAttribute("prevYearId");
    if (prevYearId == null) prevYearId = -1L;

    Long prevPrevYearId = (Long) request.getAttribute("prevPrevYearId");
    if (prevPrevYearId == null) prevPrevYearId = -1L;

    Map<Long, Map<Long, List<BudgetDTO>>> yearsBudgetDTOsByMappingId =
            (Map<Long, Map<Long, List<BudgetDTO>>>) request.getAttribute("yearsBudgetDTOsByMappingId");

    Map<Long, List<BudgetDTO>> mapByBudgetMappingId = new HashMap<>();

    String errorMessage = (String) request.getAttribute("errorMessage");
    if (errorMessage == null) {
        mapByBudgetMappingId = yearsBudgetDTOsByMappingId.get(thisYearId);
        if (mapByBudgetMappingId == null || mapByBudgetMappingId.isEmpty()) {
            mapByBudgetMappingId = new HashMap<>();
            errorMessage = UtilCharacter.getDataByLanguage(
                    Language,
                    "চলমান অর্থ বছরের জন্যে কোনো ডাটা দেওয়া হয় নি!",
                    "No Data provided for this economic year!"
            );
        }
    } else {
        yearsBudgetDTOsByMappingId = new HashMap<>();
    }

    BudgetInfo budgetInfo = (BudgetInfo) request.getAttribute("thisYearBudgetInfo");
    if (budgetInfo == null) budgetInfo = new BudgetInfo();

    Budget_submission_infoDTO budgetSubmissionInfoDTO = (Budget_submission_infoDTO) request.getAttribute("budgetSubmissionInfoDTO");
    boolean isBudgetPreparationSubmitted = budgetSubmissionInfoDTO != null
                                           && budgetSubmissionInfoDTO.isPreparationSubmit == SubmissionStatusEnum.SUBMITTED.getValue();

    String economicYearEn = budgetInfo.economicYear;
    String economicYear = StringUtils.convertBanglaIfLanguageIsBangla(
            Language,
            economicYearEn
    );
    String prevEconomicYear = StringUtils.convertBanglaIfLanguageIsBangla(
            Language,
            BudgetInfo.getEconomicYear(budgetInfo.beginYear - 2)
    );
    String prevPrevEconomicYear = StringUtils.convertBanglaIfLanguageIsBangla(
            Language,
            BudgetInfo.getEconomicYear(budgetInfo.beginYear - 3)
    );
%>

<style>
    .top-section-font {
        color: #00a1d4;
        font-weight: 500;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    @media print {
        .page-break {
            page-break-after: always;
        }
    }
</style>

<!-- begin:: Subheader -->
<div class="ml-auto mr-3 mt-4">
    <button type="button" class="btn" id='printer'
            onclick="printDiv('kt_content')">
        <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
    </button>
</div>
<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content" id="kt_content">
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__body m-4 page-bg">
                <div class="row">
                    <div class="col-12 row">
                        <div class="offset-3 col-6 text-center">
                            <img width="20%"
                                 src="<%=context%>assets/static/parliament_logo.png"
                                 alt="logo"
                                 class="logo-default"
                            />
                            <h2 class="text-center mt-3 top-section-font">
                                <%=LM.getText(LC.HM_BANGLADESH_PARLIAMENT_SECRETARIAT, loginDTO)%>
                            </h2>
                            <h4 class="text-center mt-2 top-section-font">
                                <%=LM.getText(LC.HM_PARLIAMENT_ADDRESS, loginDTO)%>
                            </h4>
                        </div>
                    </div>
                </div>

                <%if (errorMessage != null) {%>
                <div class="container text-center m-4 text-danger">
                    <span class="h4">
                        <%=errorMessage%>
                    </span>
                </div>
                <%}%>
                <%--If error message is not null, mapByBudgetMappingId is set as empty. No need for else for if (errorMessage != null)--%>
                <%
                    for (Map.Entry<Long, List<BudgetDTO>> entry : mapByBudgetMappingId.entrySet()) {
                        Long budgetMappingId = entry.getKey();
                        List<BudgetDTO> budgetDTOsByMapping = entry.getValue();
                        BudgetOperationModel model = Budget_mappingRepository.getInstance()
                                                                             .getBudgetOperationModelWithoutSubCode(budgetMappingId, economicYearEn, Language);

                        Map<Long, BudgetDTO> prevYearBudgetDTOsBySubCodeId =
                                yearsBudgetDTOsByMappingId.getOrDefault(prevYearId, new HashMap<>())
                                                          .getOrDefault(budgetMappingId, new ArrayList<>())
                                                          .stream()
                                                          .collect(Collectors.toMap(dto -> dto.economicSubCodeId, dto -> dto));

                        Map<Long, BudgetDTO> prevPrevYearBudgetDTOsBySubCodeId =
                                yearsBudgetDTOsByMappingId.getOrDefault(prevPrevYearId, new HashMap<>())
                                                          .getOrDefault(budgetMappingId, new ArrayList<>())
                                                          .stream()
                                                          .collect(Collectors.toMap(dto -> dto.economicSubCodeId, dto -> dto));
                %>
                <div class="row mt-5 mx-0 py-3 rounded" style="border: 1px solid #00ACD8;">
                    <div class="col-9 text-left">
                        <h5 class="text-left mt-3 top-section-font" id="budgetCatHeading">
                            <%=model.budgetCatName + " - " + model.officeCode%>
                        </h5>
                        <h5 class="text-left mt-3 top-section-font" id="operationalCodeHeading">
                            <%=model.description + " - " + model.operationCode%>
                        </h5>
                    </div>
                    <div class="col-3">
                        <h5 class="text-right mt-3 top-section-font" id="economicYearHeading">
                            <%=model.economicYear%>
                        </h5>
                    </div>
                </div>

                <div class="row mt-5 page-break">
                    <div class="w-100">
                        <h5 class="text-right">(<%=LM.getText(LC.BUDGET_MONEY_INPUT_UNIT_HEADER, loginDTO)%>)</h5>
                        <table class="table" id="budget-view-table">
                            <thead class="text-center">
                            <tr>
                                <th class="row-data-code" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_ECONOMIC_CODE, loginDTO)%>
                                </th>
                                <th class="row-data-name" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_DESCRIPTION, loginDTO)%>
                                </th>
                                <th colspan="2">
                                    <%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE, loginDTO)%>
                                </th>
                                <th colspan="2">
                                    <%=LM.getText(LC.BUDGET_ACTUAL_EXPENDITURE, loginDTO)%>
                                    <br>
                                    (<%=LM.getText(LC.BUDGET_FIRST_SIX_MONTHS, loginDTO)%>)
                                </th>

                                <th class="row-data-budget" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_ESTIMATES, loginDTO)%>
                                    <br>
                                    <%=economicYear%>
                                </th>

                                <th class="row-data-projection_0" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_PROJECTION, loginDTO)%>
                                    <br>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                            Language,
                                            BudgetInfo.getEconomicYear(budgetInfo.beginYear + 1)
                                    )%>
                                </th>
                                <th class="row-data-projection_1" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_PROJECTION, loginDTO)%>
                                    <br>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(
                                            Language,
                                            BudgetInfo.getEconomicYear(budgetInfo.beginYear + 2)
                                    )%>
                                </th>
                                <th class="row-data-budget" rowspan="2">
                                    <%=LM.getText(LC.BUDGET_COMMENT, loginDTO)%>
                                </th>
                            </tr>

                            <tr>
                                <th class="row-data-revised-expenditure_0">
                                    <%=prevPrevEconomicYear%>
                                </th>
                                <th class="row-data-revised-expenditure_1">
                                    <%=prevEconomicYear%>
                                </th>
                                <th class="row-data-expenditure_0">
                                    <%=prevPrevEconomicYear%>
                                </th>
                                <th class="row-data-expenditure_1">
                                    <%=prevEconomicYear%>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <%--Economic Groups--%>
                            <%
                                TreeMap<Long, List<BudgetDTO>> sortedMapByEconomicGroup =
                                        budgetDTOsByMapping.stream()
                                                           .collect(Collectors.groupingBy(
                                                                   dto -> dto.economicGroupId,
                                                                   TreeMap::new,
                                                                   toList()
                                                           ));

                                for (Map.Entry<Long, List<BudgetDTO>> groupBudgedPair : sortedMapByEconomicGroup.entrySet()) {
                                    Long economicGroupId = groupBudgedPair.getKey();
                                    List<BudgetDTO> budgetDTOsByGroup = groupBudgedPair.getValue();
                                    NameDTO groupDTO = EconomicGroupRepository.getInstance().getDTOByID(economicGroupId);
                                    if (groupDTO == null) {
                                        groupDTO = new NameDTO();
                                    }
                            %>
                            <tr>
                                <td style=" font-weight: bold" colspan="100%">
                                    <%
                                        String groupID = StringUtils.convertBanglaIfLanguageIsBangla(Language, String.valueOf(groupDTO.iD));%>
                                    <%=groupID + " - " + UtilCharacter.getDataByLanguage(Language, groupDTO.nameBn, groupDTO.nameEn)%>
                                </td>
                            </tr>

                            <%--Economic Code--%>
                            <%
                                Map<Long, List<BudgetDTO>> mapByEconomicCode = budgetDTOsByGroup.stream()
                                                                                                .collect(Collectors.groupingBy(dto -> dto.economicCodeId));

                                List<Long> economicCodeIdSortedByCode = mapByEconomicCode.keySet()
                                                                                         .stream().sorted(BudgetUtils::compareEconomicCodeId)
                                                                                         .collect(toList());

                                for (Long economicCodeId : economicCodeIdSortedByCode) {
                                    List<BudgetDTO> budgetDTOsByCode = mapByEconomicCode.get(economicCodeId);
                                    Economic_codeDTO codeDTO = Economic_codeRepository.getInstance().getById(economicCodeId);
                                    if (codeDTO == null) {
                                        codeDTO = new Economic_codeDTO();
                                    }
                            %>
                            <tr>
                                <td style="font-weight: bold" colspan="100%">
                                    <%
                                        String economicCode = StringUtils.convertBanglaIfLanguageIsBangla(Language, codeDTO.code);%>
                                    <%= economicCode + " - "
                                        + UtilCharacter.getDataByLanguage(Language, codeDTO.descriptionBn, codeDTO.descriptionEn)%>
                                </td>
                            </tr>

                            <%--Economic Sub Code--%>
                            <%
                                budgetDTOsByCode.sort(BudgetUtils::compareBudgetDTOonSubCode);

                                for (BudgetDTO budgetDTOofSubCode : budgetDTOsByCode) {
                                    BudgetDTO prevYearBudgetDTOofSubCode = prevYearBudgetDTOsBySubCodeId.getOrDefault(
                                            budgetDTOofSubCode.economicSubCodeId,
                                            new BudgetDTO()
                                    );
                                    BudgetDTO prevPrevYearBudgetDTOofSubCode = prevPrevYearBudgetDTOsBySubCodeId.getOrDefault(
                                            budgetDTOofSubCode.economicSubCodeId,
                                            new BudgetDTO()
                                    );

                                    Economic_sub_codeDTO subCodeDTO = Economic_sub_codeRepository.getInstance()
                                                                                                 .getDTOByID(budgetDTOofSubCode.economicSubCodeId);
                                    if (subCodeDTO == null) {
                                        subCodeDTO = new Economic_sub_codeDTO();
                                    }
                            %>
                            <tr>
                                <td>
                                    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, subCodeDTO.code)%>
                                </td>
                                <td>
                                    <%=UtilCharacter.getDataByLanguage(Language, subCodeDTO.descriptionBn,
                                            subCodeDTO.descriptionEn)%>
                                </td>

                                <%--Budget Info Goes here--%>
                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevPrevYearBudgetDTOofSubCode.expenditureAmount
                                            + prevPrevYearBudgetDTOofSubCode.revisedExpenditureAmount
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevYearBudgetDTOofSubCode.expenditureAmount
                                            + prevYearBudgetDTOofSubCode.revisedExpenditureAmount
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevPrevYearBudgetDTOofSubCode.expenditureAmount
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevYearBudgetDTOofSubCode.expenditureAmount
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            budgetDTOofSubCode.finalAmount
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            budgetDTOofSubCode.nextYear1Projection
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            budgetDTOofSubCode.nextYear2Projection
                                    )%>
                                </td>

                                <td>
                                    <%=budgetDTOofSubCode.preparationComment == null ? "" : budgetDTOofSubCode.preparationComment%>
                                </td>
                            </tr>
                            <%}%> <%--end for economic sub group--%>
                            <%--Sub Total of Economic Codes--%>
                            <tr>
                                <td colspan="2" style="font-weight: bold" class="text-right">
                                    <%=LM.getText(LC.BUDGET_SUBTOTAL, loginDTO)%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevPrevExpenditureSumByCode =
                                                budgetDTOsByCode.stream()
                                                                .map(dto -> dto.economicSubCodeId)
                                                                .map(prevPrevYearBudgetDTOsBySubCodeId::get)
                                                                .mapToDouble(
                                                                        dto -> dto == null ? .0
                                                                                           : dto.expenditureAmount + dto.revisedExpenditureAmount
                                                                )
                                                                .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevPrevExpenditureSumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevExpenditureSumByCode =
                                                budgetDTOsByCode.stream()
                                                                .map(dto -> dto.economicSubCodeId)
                                                                .map(prevYearBudgetDTOsBySubCodeId::get)
                                                                .mapToDouble(
                                                                        dto -> dto == null ? .0
                                                                                           : dto.expenditureAmount + dto.revisedExpenditureAmount
                                                                )
                                                                .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevExpenditureSumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevPrevExpenditureSumByCode1st6Month =
                                                budgetDTOsByCode.stream()
                                                                .map(dto -> dto.economicSubCodeId)
                                                                .map(prevPrevYearBudgetDTOsBySubCodeId::get)
                                                                .mapToDouble(
                                                                        dto -> dto == null ? .0
                                                                                           : dto.expenditureAmount
                                                                )
                                                                .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevPrevExpenditureSumByCode1st6Month
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevExpenditureSumByCode1st6Month =
                                                budgetDTOsByCode.stream()
                                                                .map(dto -> dto.economicSubCodeId)
                                                                .map(prevYearBudgetDTOsBySubCodeId::get)
                                                                .mapToDouble(
                                                                        dto -> dto == null ? .0
                                                                                           : dto.expenditureAmount
                                                                )
                                                                .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevExpenditureSumByCode1st6Month
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double amountSumByCode =
                                                budgetDTOsByCode.stream()
                                                                .mapToDouble(dto -> dto.finalAmount)
                                                                .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            amountSumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double projection1SumByCode =
                                                budgetDTOsByCode.stream()
                                                                .mapToDouble(dto -> dto.nextYear1Projection)
                                                                .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            projection1SumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double projection2SumByCode =
                                                budgetDTOsByCode.stream()
                                                                .mapToDouble(dto -> dto.nextYear2Projection)
                                                                .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            projection2SumByCode
                                    )%>
                                </td>


                                <td>
                                    <%--Place horder on comment--%>
                                </td>
                            </tr>
                            <%}%> <%--end for economic code--%>
                            <%}%> <%--end for economic group--%>
                            <tr>
                                <td colspan="2" style="font-weight: bold" class="text-right">
                                    <%=LM.getText(LC.BUDGET_TOTAL, loginDTO)%> - <%=model.description%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevPrevExpenditureSumByCode =
                                                budgetDTOsByMapping.stream()
                                                                   .map(dto -> dto.economicSubCodeId)
                                                                   .map(prevPrevYearBudgetDTOsBySubCodeId::get)
                                                                   .mapToDouble(
                                                                           dto -> dto == null ? .0
                                                                                              : dto.expenditureAmount + dto.revisedExpenditureAmount
                                                                   )
                                                                   .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevPrevExpenditureSumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevExpenditureSumByCode =
                                                budgetDTOsByMapping.stream()
                                                                   .map(dto -> dto.economicSubCodeId)
                                                                   .map(prevYearBudgetDTOsBySubCodeId::get)
                                                                   .mapToDouble(
                                                                           dto -> dto == null ? .0
                                                                                              : dto.expenditureAmount + dto.revisedExpenditureAmount
                                                                   )
                                                                   .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevExpenditureSumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevPrevExpenditureSumByCode1st6Month =
                                                budgetDTOsByMapping.stream()
                                                                   .map(dto -> dto.economicSubCodeId)
                                                                   .map(prevPrevYearBudgetDTOsBySubCodeId::get)
                                                                   .mapToDouble(
                                                                           dto -> dto == null ? .0
                                                                                              : dto.expenditureAmount
                                                                   )
                                                                   .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevPrevExpenditureSumByCode1st6Month
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double prevExpenditureSumByCode1st6Month =
                                                budgetDTOsByMapping.stream()
                                                                   .map(dto -> dto.economicSubCodeId)
                                                                   .map(prevYearBudgetDTOsBySubCodeId::get)
                                                                   .mapToDouble(
                                                                           dto -> dto == null ? .0
                                                                                              : dto.expenditureAmount
                                                                   )
                                                                   .sum();%>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            prevExpenditureSumByCode1st6Month
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double amountSumByCode =
                                                budgetDTOsByMapping.stream()
                                                                   .mapToDouble(dto -> dto.finalAmount)
                                                                   .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            amountSumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double projection1SumByCode =
                                                budgetDTOsByMapping.stream()
                                                                   .mapToDouble(dto -> dto.nextYear1Projection)
                                                                   .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            projection1SumByCode
                                    )%>
                                </td>

                                <td class="text-right">
                                    <%
                                        Double projection2SumByCode =
                                                budgetDTOsByMapping.stream()
                                                                   .mapToDouble(dto -> dto.nextYear2Projection)
                                                                   .sum();
                                    %>
                                    <%=BudgetUtils.getFormattedAmount(
                                            Language,
                                            projection2SumByCode
                                    )%>
                                </td>
                                <td>
                                    <%--Place horder on comment--%>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <%}%> <%--end for office--%>

                <%--Submit Button On Condition--%>
                <%if (!isBudgetPreparationSubmitted) {%>
                    <div id='button-div' class="row my-5">
                        <div class="col-12 text-right">
                            <button onclick="submitBudget();" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                    type="button">
                                <%=UtilCharacter.getDataByLanguage(Language, "জমা দিন", "Submit")%>
                            </button>
                        </div>
                    </div>
                <%}%>
            </div>
        </div>
    </div>
</div>

<script>
    function submitBudget() {
        const warning = '<%=isLanguageEnglish ? "After Submission Budget Preparation will be closed." : "দাখিলের পর বাজেট প্রস্তুতকরণ বন্ধ হয়ে যাবে।"%>';
        const confirmation = '<%=isLanguageEnglish ? "Are you sure?" : "আপনি কি নিশ্চিত?"%>';
        messageDialog(
            warning, confirmation, 'warning', true,
            '<%=StringUtils.getYesNo(Language,true)%>', '<%=StringUtils.getYesNo(Language,false)%>',
            confirmSubmit, () => {}
        );
    }

    async function confirmSubmit() {
        try {
            const url = 'Budget_preparationServlet?actionType=submitBudgetForFinance';
            const response = await fetch(url, {method: 'post'});
            const json = await response.json();
            if (!json.success) throw new Error(JSON.stringify(json));
            messageDialog(
                '<%=isLanguageEnglish ? "Successfully submitted." : "সফলভাবে জমা দেয়া হয়েছে"%>', '', 'success', false,
                '<%=LM.getText(LC.BUDGET_OKAY, loginDTO)%>', '', () => {}, () => {}
            );
            $("#button-div").hide();
        } catch (error) {
            console.error(error);
            messageDialog(
                '<%=isLanguageEnglish ? "Submission Failed!" : "জমা দেয়া ব্যর্থ হয়েছে!"%>', '', 'error', false,
                '<%=LM.getText(LC.BUDGET_OKAY, loginDTO)%>', '', () => {}, () => {}
            );
        }
    }

    function printDiv(divName) {
        let button = $("#button-div");
        if (button.length) button.hide();
        let printContents = document.getElementById(divName).innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        if (button.length) button.show();
    }
</script>