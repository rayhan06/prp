<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%

    String Options;
    int i = 0;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.CURRENT_STOCK_REPORT_EDIT_LANGUAGE, loginDTO);
%>

<input type='hidden' name='Language' id='Language' value='<%=Language%>'/>
<div class="row mx-2">
    <div class="search-criteria-div col-md-6">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.CURRENT_STOCK_REPORT_WHERE_MEDICALAGENTTYPE, loginDTO)%>
            </label>
            <div class="col-md-9">
                <select class='form-control' name='medicalAgentType' id='medicalAgentType'
                        tag='pb_html' onchange = "activateDiv(this.value)">
                    <option value="">Select</option>
                    <option value="drug">Drug</option>
<!--                     <option value="reagent">Reagent</option> -->
                    <option value="xray or equipment">XRay Films or other equipments</option>
                </select>
            </div>
        </div>
    </div>
    <div class="search-criteria-div col-md-6" id = "drugDiv" style = "display:none">
        <div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=LM.getText(LC.HM_DRUGS, loginDTO)%>
            </label>
            <div class="col-md-9">
                <div class="input-group">
			        <input
			          class="form-control py-2  border searchBox w-auto"
			          type="search"
			          placeholder="Search"
			          
                                              id='suggested_drug_0'
                                              tag='pb_html'
                                              onkeyup="getDrugs(this.id)"
                                              autocomplete="off"
			        />
			        
			      </div>
			      <div
			        id="searchSgtnSection_"
			        class="search-sgtn-section shadow-sm bg-white"
					tag='pb_html'
			      >
			        <div class="pt-3">
			          <ul class="px-0" style="list-style: none" id="drug_ul_0" tag='pb_html'>
			          </ul>
			        </div>
			      </div>
			      
			      <input name='formStr'
                                           id='formStr_0' type="hidden"
                                           tag='pb_html'
                                           value=''></input>
                                    
                                           
                   <input name='drug_information_id'
                             id='drugInformationType_select_0' type="hidden"
                             tag='pb_html'
                             value=''></input>
            </div>
        </div>
    </div>
    
    <div class="search-criteria-div col-md-6" id = "reagentDiv" style = "display:none">
    	<div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=Language.equalsIgnoreCase("english")?"Reagent":"রিয়েজেন্ট"%>
            </label>
            <div class="col-md-9">
				<select class='form-control'
                         name='reagent'
                         id='reagent'
                         tag='pb_html' onchange = "setItemId(this.value)">
                     <%
                         
                         Options = CommonDAO.getReagents(-1);
                         
                     %>
                     <%=Options%>
                 </select>
            </div>
        </div>
    </div>
    
    
      <div class="search-criteria-div col-md-6" id = "equipmentDiv" style = "display:none">
    	<div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=Language.equalsIgnoreCase("english")?"XRay Films and other Equipments":"এক্স রে ফিল্ম আর অন্যান্য উপকরণ"%>
            </label>
            <div class="col-md-9">
				<select class='form-control'
                         name='reagent'
                         id='reagent'
                         tag='pb_html' onchange = "setItemId(this.value)">
                     <%
                         
                         Options = CommonDAO.getOptions(Language, "medical_equipment_name", -1);
                         
                     %>
                     <%=Options%>
                 </select>
            </div>
        </div>
    </div>
    
    <div class="search-criteria-div col-md-6" id = "formDiv" style = "display:none">
    	<div class="form-group row">
            <label class="col-md-3 col-form-label text-md-right">
                <%=Language.equalsIgnoreCase("english")?"Form":"ধরণ"%>
            </label>
            <div class="col-md-9">
				<select class='form-control'
                         name='drug_form_cat'
                         id='drug_form_cat'
                         tag='pb_html' >
                     <%
                         
                         Options = CatDAO.getOptions(Language, "drug_form", -2);
                         
                     %>
                     <%=Options%>
                 </select>
            </div>
        </div>
    </div>
    
    <input name='item_id' id='item_id' type="hidden" value='' />
  
</div>
<script type="text/javascript">
    function init() {
        dateTimeInit($("#Language").val());
        addables = [0, 0, 0, 0, 1, 0];
    }

    function PreprocessBeforeSubmiting() {
    	
    	if($("#suggested_drug_0").val() == "" && ($("#medicalAgentType").val() == "drug" || $("#medicalAgentType").val() == ""))
   		{
    		console.log("resetting");
   			$("#item_id").val("");
   		}
    }
    
    function getStyle(list)
    {
    	var currentStock = parseInt(convertToEnglishNumber(list[3]));
    	var alertLevel = parseInt(convertToEnglishNumber(list[4]));
    	if(currentStock <= alertLevel)
   		{
   			return "color: red;";
   		}
    	else
    	{
    		return "";
    	}
    	
    }
    function drugClicked(rowId, drugText, drugId, form, stock)
    {
    	console.log("drugClicked with " + rowId + ", drugId = " + drugId + ", form = " + form);
    	$("#suggested_drug_" + rowId).val(drugText);
    	$("#suggested_drug_" + rowId).attr("style", "width: " + ($("#suggested_drug_" + rowId).val().length + 1)*8 + "px !important");
    	
    	 $("#drug_modal_textdiv_" + rowId).html(stock);
         $("#totalCurrentStock_text_" + rowId).val(stock);

    	$("#drugInformationType_select_" + rowId).val(drugId);
    	$("#quantity_number_" + rowId).attr("max", stock);
    	
    	$("#drug_ul_" + rowId).html("");
    	$("#item_id").val(drugId);
    	//$("#drug_ul_" + rowId).css("display", "none");
    }
    
    function activateDiv(value)
    {
    	console.log("activateDiv, value = " + value);
    	if(value == "drug")
   		{
   			$("#drugDiv").removeAttr("style");
   			$("#reagentDiv").css("display", "none");
   			$("#equipmentDiv").css("display", "none");
   			$("#formDiv").show();
   		}
    	else if(value == "reagent")
   		{
    		$("#drugDiv").css("display", "none");
   			$("#reagentDiv").removeAttr("style");
   			$("#equipmentDiv").css("display", "none");
   			$("#formDiv").hide();
   		}
    	else if(value == "xray or equipment")
   		{
    		$("#drugDiv").css("display", "none");
   			$("#reagentDiv").css("display", "none");
   			$("#equipmentDiv").removeAttr("style");
   			$("#formDiv").hide();
   		}
    	else
   		{
    		$("#drugDiv").css("display", "none");
   			$("#reagentDiv").css("display", "none");
   			$("#equipmentDiv").css("display", "none");
   			$("#formDiv").hide();
   		}
    }
    
    function setItemId(value)
    {
    	if(value == "-1")
   		{
    		$("#item_id").val("");
   		}
    	else
   		{
    		$("#item_id").val(value);
   		}
    }
</script>