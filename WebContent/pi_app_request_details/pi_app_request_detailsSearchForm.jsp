
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_app_request_details.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
String navigator2 = "navPI_APP_REQUEST_DETAILS";
String servletName = "Pi_app_request_detailsServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_PACKAGEID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ITEMTYPEID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ITEMID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_CONDITION, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ISMAINTENANCE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ISSELECTED, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDQUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDUNITPRICE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDESTIMATEDCOST, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDPROCUREMENTMETHODID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDCONTRACTAPPAUTHID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDSOURCEFUNDID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEQUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEUNITPRICE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEESTIMATEDCOST, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEPROCUREMENTMETHODID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONECONTRACTAPPAUTHID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONESOURCEFUNDID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOQUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOUNITPRICE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOESTIMATEDCOST, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOPROCUREMENTMETHODID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOCONTRACTAPPAUTHID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOSOURCEFUNDID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEQUANTITY, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEUNITPRICE, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEESTIMATEDCOST, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEPROCUREMENTMETHODID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREECONTRACTAPPAUTHID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREESOURCEFUNDID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_MODIFIEDBY, loginDTO)%></th>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
								<th><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_SEARCH_PI_APP_REQUEST_DETAILS_EDIT_BUTTON, loginDTO)%></th>
								<th class="">
									<div class="text-center">
										<span>All</span>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-3">
										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
											<i class="fa fa-trash"></i>
										</button>
										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>
									</div>
								</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Pi_app_request_detailsDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Pi_app_request_detailsDTO pi_app_request_detailsDTO = (Pi_app_request_detailsDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
											value = pi_app_request_detailsDTO.packageId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.itemTypeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.itemId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.condition + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.isMaintenance + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.isSelected + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.requestedQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.requestedUnitPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.requestedUnitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.requestedEstimatedCost + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.requestedEstimatedCost);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.requestedProcurementMethodId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.requestedContractAppAuthId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.requestedSourceFundId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveOneQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveOneUnitPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveOneUnitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveOneEstimatedCost + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveOneEstimatedCost);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveOneProcurementMethodId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveOneContractAppAuthId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveOneSourceFundId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveTwoQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveTwoUnitPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveTwoUnitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveTwoEstimatedCost + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveTwoEstimatedCost);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveTwoProcurementMethodId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveTwoContractAppAuthId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveTwoSourceFundId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveThreeQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveThreeUnitPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveThreeUnitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveThreeEstimatedCost + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveThreeEstimatedCost);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveThreeProcurementMethodId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveThreeContractAppAuthId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											<td>
											<%
											value = pi_app_request_detailsDTO.approveThreeSourceFundId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
											<td>
											<%
											value = pi_app_request_detailsDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
		
		
		
		
	
											<%CommonDTO commonDTO = pi_app_request_detailsDTO; %>
											<%@include file="../pb/searchAndViewButton.jsp"%>											
																						
											<td class="text-right">
												<div class='checker'>
													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=pi_app_request_detailsDTO.iD%>'/></span>
												</div>
											</td>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			