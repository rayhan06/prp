<%@page import="workflow.WorkflowController"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="pi_app_request_details.*"%>
<%@page import="java.util.*"%>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@ page import="user.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>
<%@ page import="util.*"%>

<%
Pi_app_request_detailsDTO pi_app_request_detailsDTO = new Pi_app_request_detailsDTO();
long ID = -1;
if(request.getParameter("ID") != null)
{
	ID = Long.parseLong(request.getParameter("ID"));
	pi_app_request_detailsDTO = Pi_app_request_detailsDAO.getInstance().getDTOByID(ID);
}
System.out.println("ID = " + ID);
CommonDTO commonDTO = pi_app_request_detailsDTO;
String tableName = "pi_app_request_details";
%>
<%@include file="../pb/addInitializer2.jsp"%>
<%
String formTitle = LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_PI_APP_REQUEST_DETAILS_ADD_FORMNAME, loginDTO);
String servletName = "Pi_app_request_detailsServlet";
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Pi_app_request_detailsServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-8 offset-2">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
									</div>



														<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.iD%>' tag='pb_html'/>
	
														<input type='hidden' class='form-control'  name='packageId' id = 'packageId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.packageId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='itemTypeId' id = 'itemTypeId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.itemTypeId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='itemId' id = 'itemId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.itemId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_CONDITION, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='condition' id = 'condition_text_<%=i%>' value='<%=pi_app_request_detailsDTO.condition%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ISMAINTENANCE, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='checkbox' class='form-control-sm' name='isMaintenance' id = 'isMaintenance_checkbox_<%=i%>' value='true' 																<%=(String.valueOf(pi_app_request_detailsDTO.isMaintenance).equals("true"))?("checked"):""%>
   tag='pb_html'>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ISSELECTED, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='checkbox' class='form-control-sm' name='isSelected' id = 'isSelected_checkbox_<%=i%>' value='true' 																<%=(String.valueOf(pi_app_request_detailsDTO.isSelected).equals("true"))?("checked"):""%>
   tag='pb_html'>
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDQUANTITY, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.requestedQuantity != -1)
																	{
																	value = pi_app_request_detailsDTO.requestedQuantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='requestedQuantity' id = 'requestedQuantity_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDUNITPRICE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.requestedUnitPrice != -1)
																	{
																	value = pi_app_request_detailsDTO.requestedUnitPrice + "";
																	}
																%>		
																<input type='number' class='form-control'  name='requestedUnitPrice' id = 'requestedUnitPrice_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDESTIMATEDCOST, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.requestedEstimatedCost != -1)
																	{
																	value = pi_app_request_detailsDTO.requestedEstimatedCost + "";
																	}
																%>		
																<input type='number' class='form-control'  name='requestedEstimatedCost' id = 'requestedEstimatedCost_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='requestedProcurementMethodId' id = 'requestedProcurementMethodId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.requestedProcurementMethodId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requestedContractAppAuthId' id = 'requestedContractAppAuthId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.requestedContractAppAuthId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='requestedSourceFundId' id = 'requestedSourceFundId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.requestedSourceFundId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEQUANTITY, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveOneQuantity != -1)
																	{
																	value = pi_app_request_detailsDTO.approveOneQuantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveOneQuantity' id = 'approveOneQuantity_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEUNITPRICE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveOneUnitPrice != -1)
																	{
																	value = pi_app_request_detailsDTO.approveOneUnitPrice + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveOneUnitPrice' id = 'approveOneUnitPrice_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEESTIMATEDCOST, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveOneEstimatedCost != -1)
																	{
																	value = pi_app_request_detailsDTO.approveOneEstimatedCost + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveOneEstimatedCost' id = 'approveOneEstimatedCost_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='approveOneProcurementMethodId' id = 'approveOneProcurementMethodId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveOneProcurementMethodId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approveOneContractAppAuthId' id = 'approveOneContractAppAuthId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveOneContractAppAuthId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approveOneSourceFundId' id = 'approveOneSourceFundId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveOneSourceFundId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOQUANTITY, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveTwoQuantity != -1)
																	{
																	value = pi_app_request_detailsDTO.approveTwoQuantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveTwoQuantity' id = 'approveTwoQuantity_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOUNITPRICE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveTwoUnitPrice != -1)
																	{
																	value = pi_app_request_detailsDTO.approveTwoUnitPrice + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveTwoUnitPrice' id = 'approveTwoUnitPrice_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOESTIMATEDCOST, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveTwoEstimatedCost != -1)
																	{
																	value = pi_app_request_detailsDTO.approveTwoEstimatedCost + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveTwoEstimatedCost' id = 'approveTwoEstimatedCost_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='approveTwoProcurementMethodId' id = 'approveTwoProcurementMethodId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveTwoProcurementMethodId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approveTwoContractAppAuthId' id = 'approveTwoContractAppAuthId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveTwoContractAppAuthId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approveTwoSourceFundId' id = 'approveTwoSourceFundId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveTwoSourceFundId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEQUANTITY, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveThreeQuantity != -1)
																	{
																	value = pi_app_request_detailsDTO.approveThreeQuantity + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveThreeQuantity' id = 'approveThreeQuantity_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEUNITPRICE, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveThreeUnitPrice != -1)
																	{
																	value = pi_app_request_detailsDTO.approveThreeUnitPrice + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveThreeUnitPrice' id = 'approveThreeUnitPrice_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEESTIMATEDCOST, loginDTO)%></label>
                                                            <div class="col-8">
																<%
																	value = "";
																	if(pi_app_request_detailsDTO.approveThreeEstimatedCost != -1)
																	{
																	value = pi_app_request_detailsDTO.approveThreeEstimatedCost + "";
																	}
																%>		
																<input type='number' class='form-control'  name='approveThreeEstimatedCost' id = 'approveThreeEstimatedCost_number_<%=i%>' value='<%=value%>'  tag='pb_html'>		
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='approveThreeProcurementMethodId' id = 'approveThreeProcurementMethodId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveThreeProcurementMethodId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approveThreeContractAppAuthId' id = 'approveThreeContractAppAuthId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveThreeContractAppAuthId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='approveThreeSourceFundId' id = 'approveThreeSourceFundId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.approveThreeSourceFundId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByUserId' id = 'insertedByUserId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.insertedByUserId%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='insertedByOrganogramId' id = 'insertedByOrganogramId_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.insertedByOrganogramId%>' tag='pb_html'/>
													<div class="form-group row">
                                                            <label class="col-4 col-form-label text-right"><%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_MODIFIEDBY, loginDTO)%></label>
                                                            <div class="col-8">
																<input type='text' class='form-control'  name='modifiedBy' id = 'modifiedBy_text_<%=i%>' value='<%=pi_app_request_detailsDTO.modifiedBy%>'   tag='pb_html'/>					
															</div>
                                                      </div>									
														<input type='hidden' class='form-control'  name='insertionDate' id = 'insertionDate_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.insertionDate%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='searchColumn' id = 'searchColumn_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.searchColumn%>' tag='pb_html'/>
														<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= '<%=pi_app_request_detailsDTO.isDeleted%>' tag='pb_html'/>
											
														<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=pi_app_request_detailsDTO.lastModificationTime%>' tag='pb_html'/>
					
									</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-10">
                        <div class="form-actions text-right">
                            <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn">
                                <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_PI_APP_REQUEST_DETAILS_CANCEL_BUTTON, loginDTO)%>
                            </button>
                            <button class="btn-sm shadow text-white border-0 submit-btn ml-2" type="submit">
                                <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_PI_APP_REQUEST_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">



function PreprocessBeforeSubmiting(row, action)
{
	console.log("action = " + action);
	preprocessCheckBoxBeforeSubmitting('isMaintenance', row);
	preprocessCheckBoxBeforeSubmitting('isSelected', row);

	submitAddForm2();
	return false;
}


function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row)
{	
	addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Pi_app_request_detailsServlet");	
}

function init(row)
{


	
}

var row = 0;
$(document).ready(function(){
	init(row);
	CKEDITOR.replaceAll();
	$("#cancel-btn").click(e => {
		e.preventDefault();
		location.href = "<%=request.getHeader("referer")%>";
	})
});	

var child_table_extra_id = <%=childTableStartingID%>;



</script>






