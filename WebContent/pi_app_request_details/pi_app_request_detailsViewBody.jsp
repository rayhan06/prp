

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_app_request_details.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="util.*"%>




<%
String servletName = "Pi_app_request_detailsServlet";
String ID = request.getParameter("ID");
long id = Long.parseLong(ID);
Pi_app_request_detailsDTO pi_app_request_detailsDTO = Pi_app_request_detailsDAO.getInstance().getDTOByID(id);
CommonDTO commonDTO = pi_app_request_detailsDTO;
%>
<%@include file="../pb/viewInitializer.jsp"%>


<style>
    .form-group {
        margin-bottom: 1rem;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_PI_APP_REQUEST_DETAILS_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row mb-4">
                <div class="col-8 offset-2">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_PI_APP_REQUEST_DETAILS_ADD_FORMNAME, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_PACKAGEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.packageId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ITEMTYPEID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.itemTypeId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ITEMID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.itemId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_CONDITION, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.condition + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ISMAINTENANCE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.isMaintenance + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_ISSELECTED, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.isSelected + "";
											%>
				
											<%=Utils.getYesNo(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDQUANTITY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.requestedQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDUNITPRICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.requestedUnitPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.requestedUnitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDESTIMATEDCOST, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.requestedEstimatedCost + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.requestedEstimatedCost);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDPROCUREMENTMETHODID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.requestedProcurementMethodId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDCONTRACTAPPAUTHID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.requestedContractAppAuthId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_REQUESTEDSOURCEFUNDID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.requestedSourceFundId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEQUANTITY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveOneQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEUNITPRICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveOneUnitPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveOneUnitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEESTIMATEDCOST, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveOneEstimatedCost + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveOneEstimatedCost);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONEPROCUREMENTMETHODID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveOneProcurementMethodId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONECONTRACTAPPAUTHID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveOneContractAppAuthId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVEONESOURCEFUNDID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveOneSourceFundId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOQUANTITY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveTwoQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOUNITPRICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveTwoUnitPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveTwoUnitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOESTIMATEDCOST, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveTwoEstimatedCost + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveTwoEstimatedCost);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOPROCUREMENTMETHODID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveTwoProcurementMethodId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOCONTRACTAPPAUTHID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveTwoContractAppAuthId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETWOSOURCEFUNDID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveTwoSourceFundId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEQUANTITY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveThreeQuantity + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEUNITPRICE, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveThreeUnitPrice + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveThreeUnitPrice);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEESTIMATEDCOST, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveThreeEstimatedCost + "";
											%>
											<%
											value = String.format("%.1f", pi_app_request_detailsDTO.approveThreeEstimatedCost);
											%>												
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREEPROCUREMENTMETHODID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveThreeProcurementMethodId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREECONTRACTAPPAUTHID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveThreeContractAppAuthId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_APPROVETHREESOURCEFUNDID, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.approveThreeSourceFundId + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
								<div class="form-group row d-flex align-items-center">
                                    <label class="col-4 col-form-label text-right">
                                        <%=LM.getText(LC.PI_APP_REQUEST_DETAILS_ADD_MODIFIEDBY, loginDTO)%>
                                    </label>
                                    <div class="col-8">
											<%
											value = pi_app_request_detailsDTO.modifiedBy + "";
											%>
				
											<%=Utils.getDigits(value, Language)%>
				
			
                                    </div>
                                </div>
			
			
			
			
			
		
							</div>
                        </div>
                    </div>
                </div>
            </div>			

        </div>
    </div>
</div>