<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="util.*" %>
<%@ page import="static util.UtilCharacter.getDataByLanguage" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="pb.Utils" %>
<%@ page import="fiscal_year.Fiscal_yearDAO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ALLOWANCE_CONFIGURE_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;

    String formTitle = getDataByLanguage(Language, "স্টক দেখুন", "VIEW STOCK");
    String context = request.getContextPath() + "/";

    List<Office_unitsDTO> office_unitsDTOS = Office_unitsRepository.getInstance().getOfficeUnitsDTOListAsInventoryStore();
    Fiscal_yearDAO fiscal_yearDAO = new Fiscal_yearDAO();
    List<Fiscal_yearDTO> fiscal_yearDTOS = Fiscal_yearRepository.getInstance().getFiscal_yearList();
%>

<style>
    .fly-in-from-down {
        animation: flyFromDown 1s ease-out;
    }

    @keyframes flyFromDown {
        0% {
            transform: translateY(200%);
        }
        100% {
            transform: translateY(0%);
        }
    }

    .template-row {
        display: none;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        width: 450px;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border-radius: 1px;
        -moz-box-shadow: 0px 0px 5px 0px rgba(212, 182, 212, 1)
    }

    .card-header {
        background-color: #fff
    }

    .card-body h4 {
        color: #545b62
    }

    .hiddenTr{
        display: none;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" id="bill-form" enctype="multipart/form-data">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12 row mt-2">
                        <div class="form-group row col-md-6">
                            <label class="col-form-label col-md-3 text-md-right">
                                <%=getDataByLanguage(Language, "অর্থবছর", "Economic Year")%>
                            </label>
                            <div class="col-md-9">
                                <select id="economicYear"
                                        name='economicYear'
                                        class='form-control rounded shadow-sm'
                                        onchange="clearOther()"
                                >
                                    <%
                                        Fiscal_yearDTO cur_fis_dto = fiscal_yearDAO.getFiscalYearBYDateLong(System.currentTimeMillis());

                                        String fiscalYearOptions = Utils.buildSelectOption(isLanguageEnglish);
                                        StringBuilder option = new StringBuilder();
                                        for (Fiscal_yearDTO fiscal_yearDTO : fiscal_yearDTOS) {
                                            if (cur_fis_dto.id == fiscal_yearDTO.id) {
                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("' selected>");
                                            } else {
                                                option.append("<option value = '").append(fiscal_yearDTO.id).append("'>");
                                            }

                                            option.append(isLanguageEnglish ? fiscal_yearDTO.nameEn : fiscal_yearDTO.nameBn).append("</option>");
                                        }
                                        fiscalYearOptions += option.toString();
                                    %>
                                    <%=fiscalYearOptions%>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-form-label col-md-3 text-md-right">
                                <%=UtilCharacter.getDataByLanguage(Language, "তারিখ হতে", "Date From")%>
                            </label>
                            <div class="col-md-9">
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID" value="requested_date_start_js"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                </jsp:include>
                                <input type="hidden" id="startDate" name="startDate">
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-form-label col-md-3 text-md-right">
                                <%=UtilCharacter.getDataByLanguage(Language, "তারিখ পর্যন্ত", "Data Upto")%>
                            </label>
                            <div class="col-md-9">
                                <jsp:include page="/date/date.jsp">
                                    <jsp:param name="DATE_ID" value="requested_date_end_js"></jsp:param>
                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                </jsp:include>
                                <input type="hidden" id="endDate" name="endDate">
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-form-label col-md-3 text-md-right">
                                <%=LM.getText(LC.PI_AUCTION_ITEMS_ADD_PIAUCTIONID, loginDTO)%>
                            </label>
                            <div class="col-md-9">
                                <select class='form-control' name='storeOfficeUnitId'
                                        id='storeOfficeUnitId'
                                        tag='pb_html'>
                                    <%= Office_unitsRepository.getInstance().buildOptions(Language, -1L, false, office_unitsDTOS) %>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row col-md-6">
                            <label class="col-form-label col-md-3 text-md-right">
                                <%=getDataByLanguage(Language, "আইটেম বাছাই করুন", "Select Item")%>
                            </label>
                            <div class="col-md-9">
                                <button id="itemSelectBtn" type="button"
                                        class="btn btn-primary btn-block shadow btn-border-radius mb-3 ">
                                    <%=getDataByLanguage(Language, "বাছাই করুন", "Select")%>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2" id="itemInfoDiv" style="display: none">
                    <div class="row onlyborder">
                        <div class="col-md-10 offset-md-1">
                            <div class="sub_title_top">
                                <div class="sub_title">
                                    <h4><%=getDataByLanguage(Language, "আইটেম তথ্য", "Item Information")%>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-12 row mt-2">
                                <div class="form-group row col-md-6">
                                    <label class="col-form-label col-md-3 text-md-right">
                                        <%=getDataByLanguage(Language, "আইটেম ধরণ", "Item Type")%>
                                    </label>
                                    <label class="col-form-label text-center col-md-9" id="item_type"
                                    >
                                    </label>
                                </div>

                                <div class="form-group row col-md-6">
                                    <label class="col-form-label col-md-3 text-md-right">
                                        <%=getDataByLanguage(Language, "আইটেম নাম", "Item Name")%>
                                    </label>
                                    <label class="col-form-label text-center col-md-9" id="item_name"
                                    >
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row" style="display: none" id="progress_div">
                                <label class="col-form-label col-md-3 text-md-left pt-4">
                                    <%=getDataByLanguage(Language, "খরচের পরিমাণ", "Expense Percentage")%>
                                </label>
                                <div class="col-md-9 pt-1">
                                    <div class="text-center" id="percentage_text_div">
                                        <strong>80%</strong>
                                    </div>
                                    <div class="progress">
                                        <div
                                                class="progress-bar progress-bar-striped bg-danger"
                                                role="progressbar"
                                                style="width: 80%"
                                                aria-valuenow="100"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                id="progress_id"
                                        ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="display: none" id="search_div">
                    <div class="col-12 mt-3 text-right">
                        <button id="search-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button" onclick="onSearchClicked()">
                            <%=getDataByLanguage(Language, "খুঁজুন", "Search")%>
                        </button>
                    </div>
                </div>
                <div class="mt-4 table-responsive">
                    <table id="allowance-table" class="table table-bordered table-striped text-nowrap">
                        <thead>
                        <tr>
                            <th>
                                <%=getDataByLanguage(Language, "মাস", "Month")%>
                            </th>
                            <th>
                                <%=getDataByLanguage(Language, "প্রারম্ভিক মজুদ", "Initial Stock")%>
                            </th>
                            <%--                            <th>--%>
                            <%--                                <%=getDataByLanguage(--%>
                            <%--                                        Language,--%>
                            <%--                                        "সরবরাহকারী প্রতিষ্ঠানের নাম",--%>
                            <%--                                        "Vendor Name"--%>
                            <%--                                )%>--%>
                            <%--                            </th>--%>
                            <%--                            <th>--%>
                            <%--                                <%=getDataByLanguage(--%>
                            <%--                                        Language,--%>
                            <%--                                        "আইটেম প্রাপ্তি তারিখ",--%>
                            <%--                                        "Item Requisition Date"--%>
                            <%--                                )%>--%>
                            <%--                            </th>--%>
                            <th>
                                <%=getDataByLanguage(
                                        Language,
                                        "ক্রয় পরিমাণ",
                                        "Purchase Amount"
                                )%>
                            </th>
                            <th>
                                <%=getDataByLanguage(Language, "মোট বিতরণ", "Total Distribution")%>
                            </th>
                            <th>
                                <%=getDataByLanguage(
                                        Language,
                                        "অবশিষ্ট",
                                        "Remaining"
                                )%>
                            </th>
                            <%--                            <th>--%>
                            <%--                                <%=getDataByLanguage(--%>
                            <%--                                        Language,--%>
                            <%--                                        "মেয়াদ উত্তীর্ণের তারিখ",--%>
                            <%--                                        "Expiry Date"--%>
                            <%--                                )%>--%>
                            <%--                            </th>--%>
                        </tr>
                        </thead>
                        <tbody class="main-tbody">
                        </tbody>
                        <tfoot>
                        <tr class="hiddenTr" id="stockTotal">
                            <td><%=UtilCharacter.getDataByLanguage(Language, "মোট", "Total")%>
                            </td>
                            <td id="totalInitialStock"></td>
                            <td id="totalPurchaseAmount"></td>
                            <td id="totalDistributionSum"></td>
                            <td id="totalRemaining"></td>
                        </tr>
                        </tfoot>
                        <%--don't put these tr inside tbody--%>
                        <tr class="loading-gif" style="display: none;">
                            <td class="text-center" colspan="100%">
                                <img alt="" class="loading"
                                     src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
                                <span><%=getDataByLanguage(Language, "লোড হচ্ছে...", "Loading...")%></span>
                            </td>
                        </tr>

                        <tr class="template-row">
                            <td class="row-data-monthName"></td>
                            <td class="row-data-initialStock"></td>
                            <%--                            <td class="row-data-vendorName"></td>--%>
                            <%--                            <td class="row-data-purchaseDate"></td>--%>
                            <td class="row-data-purchaseAmount"></td>
                            <td class="row-data-distributedAmount"></td>
                            <td class="row-data-remainingAmount"></td>
                            <%--                            <td class="row-data-expiryDate"></td>--%>
                        </tr>
                    </table>
                </div>

                <div class="row">
                    <div class="col-12 mt-3 text-right">
                        <button id="back-btn"
                                class="btn-sm shadow text-white border-0 submit-btn ml-2 btn-border-radius"
                                type="button">
                            <%=getDataByLanguage(Language, "পিছনে যান", "Go Back")%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../procurement_goods/procurementGoodsModal.jsp">
    <jsp:param name="index" value="1"/>
</jsp:include>
<%@include file="../common/table-sum-utils.jsp" %>
<script src="<%=context%>/assets/scripts/util1.js"></script>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    let totalInitialStock = 0;
    let totalPurchaseAmount = 0;
    let totalDistributionSum = 0;
    let totalRemaining = 0;
    let language = '<%=Language%>';

    const isLangEng = '<%=Language%>'.toLowerCase() === 'english';

    function loadModal() {
        $('#search_proc_modal').modal();
    }

    $('#itemSelectBtn').on('click', function () {
        loadModal();
    });
    necessaryCollectionForProcurementModal = {
        callBackFunction: function (item) {
            console.log(selectedItemFromProcurementModal);
            if (!(selectedItemFromProcurementModal instanceof Map)) {
                setSearchParamView(selectedItemFromProcurementModal);
            }
        }
    };

    function clearTable() {
        const tableBody = document.querySelector('#allowance-table tbody.main-tbody');
        tableBody.innerHTML = '';
        // document.querySelectorAll('#allowance-table tfoot')
        //     .forEach(tfoot => tfoot.remove());
    }

    function showNoDataFound() {
        const tableBody = document.querySelector('#allowance-table tbody.main-tbody');
        const emptyTableText = '<%=getDataByLanguage(Language, "কোন তথ্য পাওয়া যায়নি", "No data found")%>';
        tableBody.innerHTML = '<tr class="text-center"><td colspan="100%">'
            + emptyTableText + '</td></tr>';
    }

    function setSearchParamView(selectedObject) {
        $("#itemInfoDiv").show();
        $("#search_div").show();
        clearOther();
        document.getElementById("item_type").innerHTML = isLangEng ? selectedObject.procurementGoodsTypeNameEn : selectedObject.procurementGoodsTypeNameBn;
        document.getElementById("item_name").innerHTML = isLangEng ? selectedObject.nameEn : selectedObject.nameBn;

    }

    function onSearchClicked() {
        const fiscalYearId = document.getElementById("economicYear").value;
        let startDateElement = document.getElementById("startDate");
        let endDateElement = document.getElementById("endDate");
        startDateElement.value = getDateTimestampById('requested_date_start_js');
        endDateElement.value = getDateTimestampById('requested_date_end_js');
        let startDate = startDateElement.value;
        let endDate = endDateElement.value;

        let storeOfficeUnitId = document.getElementById("storeOfficeUnitId").value;
        if ($("#bill-form").valid() && selectedItemFromProcurementModal.iD) {
            sendAjaxCall(fiscalYearId, startDate, endDate, storeOfficeUnitId, selectedItemFromProcurementModal.iD);
        }
        // else {
        //     $('#toast_message').css('background-color', '#ff6063');
        //     showToastSticky("অনুগ্রহ করে অর্থবছর নির্বাচন করুন","Please Select Economic Year");
        // }
        hideStockTotalRow();
    }

    function showOrHideLoadingGif(tableId, toShow) {
        const loadingGif = $('#' + tableId + ' tr.loading-gif');
        if (toShow) {
            document.querySelector('#' + tableId + ' tbody.main-tbody').innerHTML = '';
            // document.querySelectorAll('#' + tableId + ' tfoot').forEach(tfoot => tfoot.remove());
            loadingGif.show();
        } else loadingGif.hide();
    }

    async function sendAjaxCall(fiscalYearId, startDate, endDate, storeOfficeUnitId, itemId) {
        showOrHideLoadingGif('allowance-table', true);
        const url = 'Pi_stockServlet?actionType=ajax_getStockData'
            + '&fiscalYearId=' + fiscalYearId
            + '&startDate=' + startDate
            + '&endDate=' + endDate
            + '&storeOfficeUnitId=' + storeOfficeUnitId
            + '&itemId=' + itemId;
        const response = await fetch(url);

        const {stockModels, totalStock, totalDistribution} = await response.json();
        console.log("totalStock: ", totalStock);
        console.log("totalDistribution: ", totalDistribution);
        const tableBody = document.querySelector('#allowance-table tbody');
        const templateRow = document.querySelector('#allowance-table tr.template-row');

        showOrHideLoadingGif('allowance-table', false);
        if (stockModels.length === 0) {
            showNoDataFound();
        } else {
            stockModels.forEach(model => showModelInTable(tableBody, templateRow, model));
        }
        setSummationInFooter(stockModels);
        setProgressBarData(totalStock, totalDistribution);
    }

    function hideStockTotalRow(){
        let tr = document.getElementById('stockTotal');
        hideElement(tr);
    }

    function showStockTotalRow(){
        let tr = document.getElementById('stockTotal');
        showElement(tr);
    }

    function hideElement(element){
        element.classList.add('hiddenTr');
    }

    function showElement(element){
        element.classList.remove('hiddenTr');
    }

    function setTotalPurchaseAmount(stockModels){
        let totalPurchaseAmount = calculateTotalPurchaseAmount(stockModels);
        document.getElementById('totalPurchaseAmount').innerHTML =
            getDigitByLanguage(language, totalPurchaseAmount.toString());
    }

    function setTotalDistributionAmount(stockModels){
        let totalDistributionSum = calculateTotalDistributionAmount(stockModels);
        document.getElementById('totalDistributionSum').innerHTML =
            getDigitByLanguage(language, totalDistributionSum.toString());
    }

    function setSummationInFooter(stockModels) {
        setTotalPurchaseAmount(stockModels);
        setTotalDistributionAmount(stockModels);
        showStockTotalRow();
    }

    function calculateTotalPurchaseAmount(stockModels){
        let totalPurchaseAmount = 0;
        stockModels.forEach(model => totalPurchaseAmount += model.purchaseAmountLong);
        return totalPurchaseAmount;
    }

    function calculateTotalDistributionAmount(stockModels){
        let totalDistributionSum = 0;
        stockModels.forEach(model => totalDistributionSum += model.distributedAmountLong);
        return totalDistributionSum;
    }


    function convertNumberToBangla2(engValue) {
        let banglaValue = "";
        for (let i = 0; i < engValue.length; i++) {
            const x = engValue[i];
            switch (x) {
                case '0':
                    banglaValue = banglaValue + '০';
                    break;
                case '1':
                    banglaValue = banglaValue + '১';
                    break;
                case '2':
                    banglaValue = banglaValue + '২';
                    break;
                case '3':
                    banglaValue = banglaValue + '৩';
                    break;
                case '4':
                    banglaValue = banglaValue + '৪';
                    break;
                case '5':
                    banglaValue = banglaValue + '৫';
                    break;
                case '6':
                    banglaValue = banglaValue + '৬';
                    break;
                case '7':
                    banglaValue = banglaValue + '৭';
                    break;
                case '8':
                    banglaValue = banglaValue + '৮';
                    break;
                case '9':
                    banglaValue = banglaValue + '৯';
                    break;
                default:
                    banglaValue = banglaValue + x;
            }
        }
        return banglaValue;
    }


    function setProgressBarData(totalStock, totalDistribution) {
        $("#progress_div").show();
        if (totalStock == 0) {
            percentage = 0;
        } else {
            percentage = Math.round((totalDistribution / totalStock) * 100);
        }
        document.getElementById("progress_id").style.width = percentage + "%";
        if (!isLangEng)
            percentage = convertNumberToBangla2(String(percentage));
        console.log("percentage ", percentage);
        document.getElementById("percentage_text_div").innerHTML = "<strong>" + percentage + "%" + "</strong>";
    }

    function setRowData(templateRow, stockModel) {
        const rowDataPrefix = 'row-data-';
        for (const key in stockModel) {
            const td = templateRow.querySelector('.' + rowDataPrefix + key);
            if (!td) continue;
            td.innerText = stockModel[key];
        }
    }

    function showModelInTable(tableBody, templateRow, stockModel) {
        const modelRow = templateRow.cloneNode(true);
        modelRow.classList.remove('template-row');
        setRowData(modelRow, stockModel);
        tableBody.append(modelRow);
    }

    function init(row) {
        select2SingleSelector("#economicYear", '<%=Language%>');
        select2SingleSelector("#storeOfficeUnitId", '<%=Language%>');
        $.validator.addMethod('economicYearSelection', function (value, element) {
            return value != 0;
        });
        $("#bill-form").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                economicYear: {
                    required: true,
                    economicYearSelection: true
                },
            },

            messages: {
                economicYear: isLangEng ? "Please Select Economic Year" : "অনুগ্রহ করে অর্থবছর নির্বাচন করুন",
            }
        });
    }

    function clearOther() {
        clearTable();
        $("#progress_div").hide();
        //$("#itemInfoDiv").hide();
        //selectedItemFromProcurementModal = {};
    }

    let row = 0;
    $(document).ready(function () {
        init(row);
        // CKEDITOR.replaceAll();
        $("#back-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        })
    });


    function convertNumberToBangla(engValue) {
        let banglaValue = "";
        for (let i = 0; i < engValue.length; i++) {
            const x = engValue[i];
            switch (x) {
                case '0':
                    banglaValue = banglaValue + '০';
                    break;
                case '1':
                    banglaValue = banglaValue + '১';
                    break;
                case '2':
                    banglaValue = banglaValue + '২';
                    break;
                case '3':
                    banglaValue = banglaValue + '৩';
                    break;
                case '4':
                    banglaValue = banglaValue + '৪';
                    break;
                case '5':
                    banglaValue = banglaValue + '৫';
                    break;
                case '6':
                    banglaValue = banglaValue + '৬';
                    break;
                case '7':
                    banglaValue = banglaValue + '৭';
                    break;
                case '8':
                    banglaValue = banglaValue + '৮';
                    break;
                case '9':
                    banglaValue = banglaValue + '৯';
                    break;
                default:
                    banglaValue = banglaValue + x;
            }
        }
        return banglaValue;
    }

</script>