<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_EDIT_LANGUAGE, loginDTO);
    String Options;
    CommonDAO.language = Language;
    CatDAO.language = Language;
    int i = 0;
    String index = "1";
    String packageOptions = Procurement_packageRepository.getInstance().getOptions(Language, -1);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(Language, "স্টোর লেজার", "STORE LEDGER")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <form class="form-horizontal" id="bigForm" name="bigForm">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 ">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=UtilCharacter.getDataByLanguage(Language, "আইটেম নির্বাচন করুন", "Select ITEM")%>
                                            </h4>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_PACKAGE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='procurementPackageId'
                                                        onchange="loadProcurementGoodsType(this)"
                                                        id='procurementPackageId_<%=i%>'>
                                                    <%=packageOptions%>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label text-md-right"><%=LM.getText(LC.PROCUREMENT_GOODS_ADD_GOODS_TYPE, loginDTO)%>
                                            </label>
                                            <div class="col-md-8">
                                                <select class='form-control' name='procurementGoodsTypeId'
                                                        onchange="loadProcurementGoodsByType(this)"
                                                        id='procurementGoodsTypeId_<%=i%>'>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="subType">
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-md-4 text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "তারিখ হতে", "Date From")%>
                                            </label>
                                            <div class="col-md-8">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="requested_date_start_js"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type="hidden" id="startDate" name="startDate">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-md-4 text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "তারিখ পর্যন্ত", "Data Upto")%>
                                            </label>
                                            <div class="col-md-8">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="requested_date_end_js"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type="hidden" id="endDate" name="endDate">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <button onclick="loadAndShowData()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button">
                            <%=UtilCharacter.getDataByLanguage(Language, "লোড", "Load")%>
                        </button>
                    </div>
                </div>
            </form>
            <div class="mt-2 w-100" id="store-ledger" style="display:none;">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let language = '<%=Language%>';
    let subTypeCount = <%=index%>;
    let storeLedger = $('#store-ledger');

    function loadProcurementGoodsType(packageElement) {
        let packageId = packageElement.value;
        $("#subType").html('');
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (!this.responseText.includes('option')) {
                } else {
                    $('#procurementGoodsTypeId_<%=i%>').html(this.responseText);
                }

            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        };
        xhttp.open("GET", "Procurement_packageServlet?actionType=getByPackageId&ID=" + packageId, true);
        if (packageId !== undefined && packageId !== null && packageId !== '') xhttp.send();
    }

    function loadProcurementGoodsByType(goodsTypeElement) {
        let goodsTypeId = goodsTypeElement.value;
        $("#subType").html('');
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {
                    let totalResponse = JSON.parse(this.responseText);
                    let options = totalResponse.options;
                    $("#subType").append(options);
                    updateLabels();
                    $('#subType_select_' + subTypeCount).change(function () {
                        loadProcurementGoodsByParent(this);
                    });
                    subTypeCount++;
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                alert('failed ' + this.status);
            }
        }
        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByGoodsType&ID=" + goodsTypeId + "&index=" + subTypeCount, true);
        if (goodsTypeId !== undefined && goodsTypeId !== null && goodsTypeId !== '') xhttp.send();
    }

    function loadProcurementGoodsByParent(element) {

        var nextIndexes = parseInt(element.id.toString().substring(15)) + 1;
        subTypeCount = nextIndexes;

        while ($("#subType_select_" + nextIndexes).val() != undefined && $("#subType_select_" + nextIndexes).val() != null) {
            $("#div_id_" + nextIndexes).remove();
            nextIndexes++;
        }

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (!this.responseText.includes('option')) {
                } else {

                    var totalResponse = JSON.parse(this.responseText);
                    var options = totalResponse.options;
                    lastItemFromProcurementModal = totalResponse.procurementGoodsResponseDTO;

                    $("#subType").append(options);
                    updateLabels();

                    $('#subType_select_' + subTypeCount).change(function () {
                        loadProcurementGoodsByParent(this);
                    });

                }

            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("GET", "Procurement_goodsServlet?actionType=getByParent&ID=" + element.value + "&index=" + subTypeCount, true);

        if (element.value != undefined && element.value != null && element.value != '') xhttp.send();

    }

    function updateLabels() {
        var goodsLabels = document.getElementById('subType').getElementsByClassName('col-form-label');

        var totalLabels = goodsLabels.length;
        for (var i = 0; i < totalLabels - 1; i++) {
            goodsLabels[i].innerHTML = "<%=LM.getText(LC.PROCUREMENT_GOODS_ADD_ID, loginDTO)%>";
        }
        goodsLabels[totalLabels - 1].innerHTML = "<%=LM.getText(LC.GATE_PASS_VIEW_ITEM_NAME, loginDTO)%>";
    }

    async function loadAndShowData() {
        if (!isInputValid()) {
            return;
        }

        let itemId = document.getElementById('subType_select_1').value;
        let startDateElement = document.getElementById("startDate");
        let endDateElement = document.getElementById("endDate");
        startDateElement.value = getDateTimestampById('requested_date_start_js');
        endDateElement.value = getDateTimestampById('requested_date_end_js');
        let startDate = startDateElement.value;
        let endDate = endDateElement.value;

        let url = "Pi_stockServlet?actionType=getStoreLedgerViewBodyTable" +
            "&itemId=" + itemId +
            "&startDate=" + startDate +
            "&endDate=" + endDate;
        const res = await fetch(url);
        const htmlText = await res.text();
        if (!htmlText.includes("to-print-div")) {
            toastr.error(getValueByLanguage(language, "কোন তথ্য পাওয়া যায়নি!", "No Data Found!"));
        }
        storeLedger.html(htmlText);
        storeLedger.show();
    }

    function isInputValid(){
        let itemGroup = document.getElementById('procurementPackageId_0').value;
        let isItemGroupSelected = itemGroup !== null && itemGroup !== undefined && itemGroup !== "";
        if(!isItemGroupSelected) swal.fire(getValueByLanguage(language, "আইটেম গ্রুপ নির্বাচন করুন", "Select Item Group"));

        let itemType = document.getElementById('procurementGoodsTypeId_0').value;
        let isItemTypeSelected = itemType !== null && itemType !== undefined && itemType !== "";
        if(!isItemTypeSelected) swal.fire(getValueByLanguage(language, "আইটেম টাইপ নির্বাচন করুন", "Select Item Type"));

        let item = document.getElementById('subType_select_1').value;
        let isItemSelected = item !== null && item !== undefined && item !== "";
        if(!isItemSelected) swal.fire(getValueByLanguage(language, "আইটেম গির্বাচন করুন", "Select Item"));

        if(isItemGroupSelected && isItemTypeSelected && isItemSelected) return true;
        else return false;
    }
</script>