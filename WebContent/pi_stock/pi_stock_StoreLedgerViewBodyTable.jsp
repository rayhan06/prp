<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="vm_requisition.*" %>
<%@page import="java.util.*" %>
<%@page pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>
<%@ page import="user.UserDTO" %>
<%@ page import="user.UserRepository" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="util.TimeFormat" %>
<%@ page import="pi_stock.Pi_store_ledgerModel" %>

<%
    List<Pi_store_ledgerModel> combineList = (List<Pi_store_ledgerModel>) request.getAttribute("storeLedgerModels");

    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle = LM.getText(LC.VM_REQUISITION_ADD_VM_REQUISITION_ADD_FORMNAME, loginDTO);
    String servletName = "Vm_requisitionServlet";
    String fileColumnName = "";

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
    String Language = LM.getText(LC.VM_REQUISITION_EDIT_LANGUAGE, loginDTO);
    boolean isLanguageEnglish = Language.equalsIgnoreCase("English");
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    CommonDAO.language = Language;
    CatDAO.language = Language;

    String context = request.getContextPath() + "/";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    String pdfFileName = "store_ledger";
%>

<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group {
        margin-bottom: .5rem;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 4px;
    }

    .page {
        background: rgba(255, 255, 255, 0.85);
        padding: .5in;
        margin-bottom: 5px;
        page-break-after: always;
        box-shadow: rgba(131, 131, 109, 0.85);
    }

    @media print {
        @page {
            size: landscape;
            margin: .25in;
        }

        .shadow {
            box-shadow: none !important;
        }
    }
</style>

<div class="kt-content p-0" id="kt_content">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">

                </h3>
            </div>
        </div>
        <div class="kt-portlet__body" id="bill-div">
            <%--            DOWNLOAD BUTTON--%>
            <div class="ml-auto m-1">
                <button type="button" class="btn" id='download-pdf'
                        onclick="printDivWithJqueryPrint('to-print-div');">
                    <i class="fa fa-print fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>

            <div style="margin: auto;">
                <div class="container shadow p-4">
                    <div class="" id="to-print-div">
                        <section class="page" data-size="A4-landscape">
                            <div class="">
                                <%------------------------------------------------------------------------------------------------------------------%>
                                <%--DYNAIMICALLY GENERATE TABLE WHICH FITS DATA IN MULTIPLE PAGE--%>
                                <%--TABLE CONFIG--%>
                                <%
                                    boolean isLastPage = false;
                                    final int rowsPerPage = 50;
                                    int index = 0;
                                    while (index < combineList.size()) {
                                        boolean isFirstPage = (index == 0);
                                %>
                                <%if (isFirstPage) {%>
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-4"></div>
                                        <div class="col-4">
                                            <h1 class="font-weight-bold text-center">
                                                <%=UtilCharacter.getDataByLanguage(Language, "স্টোর লেজার", "Store Ledger")%>
                                            </h1>
                                        </div>
                                        <div class="col-4">
                                            <div class="w-100 d-flex justify-content-end mt-3">
                                                <div class="m-2 mr-4">
                                                    <p class="mb-1">
                                                        <%--                                                <%=UtilCharacter.getDataByLanguage(Language, "গাড়ির নংঃ", "Vehicle No.:")%>&nbsp;--%>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%}%>

                                <div>
                                    <div>
                                        <table class="table table-bordered">
                                            <thead style="background-color:lightgrey">
                                            <tr>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "তারিখ", "Date")%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "সরবরাহকারী/কর্মকর্তা", "Supplier/Employee")%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "জি আর এন/রিকুজিশন নাম্বার", "GRN/Requisition Number")%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "ধরন", "Type")%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "রিসিভড", "Received")%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "ইস্যুড", "Issued")%>
                                                </th>
                                                <th><%=UtilCharacter.getDataByLanguage(Language, "ব্যালেন্স", "Balance")%>
                                                </th>
                                            </tr>
                                            <tr>
                                                <%
                                                    for (i = 0; i < 8; i++) {
                                                %>
                                                <td class="text-center"><%=Utils.getDigits(i, Language)%>
                                                </td>
                                                <%}%>
                                            </tr>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <%if (!isFirstPage) {%>
                                            <%}%>

                                            <%
                                                int rowsInThisPage = 0;
                                                int serialNo = 0;
                                                while (index < combineList.size() && rowsInThisPage < rowsPerPage) {
                                                    isLastPage = (index == (combineList.size() - 1));
                                                    rowsInThisPage++;
                                                    Pi_store_ledgerModel model = combineList.get(index++);
                                            %>
                                            <tr>
                                                <td>
                                                    <%=Utils.getDigits(++serialNo, Language)%>
                                                </td>
                                                <td>
                                                    <%=model.date%>
                                                </td>
                                                <td>
                                                    <%=model.user%>
                                                </td>
                                                <td>
                                                    <%=model.identificationNumber%>
                                                </td>
                                                <td>
                                                    <%=model.transactionType%>
                                                </td>
                                                <td>
                                                    <%=model.received%>
                                                </td>
                                                <td>
                                                    <%=model.issued%>
                                                </td>
                                                <td>
                                                    <%=model.balance%>
                                                </td>
                                            </tr>
                                            </tr>
                                            <%
                                                }
                                            %>
                                            </tbody>
                                            <tfoot>
                                            <%if (isLastPage) {%>
                                            <%}%>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <%if (isLastPage) {%>
                                <div class="mt-3">
                                </div>
                                <%}%>
                                <%
                                    }
                                %>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../utility/jquery_print.jsp"/>


<script type="text/javascript">
</script>