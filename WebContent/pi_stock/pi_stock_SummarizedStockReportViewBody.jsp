<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@ page import="pb.*" %>
<%@ page import="util.UtilCharacter" %>
<%@ page import="procurement_package.Procurement_packageRepository" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.VM_VEHICLE_EDIT_LANGUAGE, loginDTO);
    String Options;
    CommonDAO.language = Language;
    CatDAO.language = Language;
    int i = 0;
    String index = "1";
    String packageOptions = Procurement_packageRepository.getInstance().getOptions(Language, -1);
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=UtilCharacter.getDataByLanguage(Language, "স্টক সারমর্ম রিপোর্ট", "STOCK SUMMARY REPORT")%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <form class="form-horizontal" id="bigForm" name="bigForm">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 ">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=UtilCharacter.getDataByLanguage(Language, "স্টক সারমর্ম রিপোর্ট", "STOCK SUMMARY REPORT")%>
                                            </h4>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-md-4 text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "তারিখ হতে", "Date From")%>
                                            </label>
                                            <div class="col-md-8">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="requested_date_start_js"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type="hidden" id="startDate" name="startDate">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-md-4 text-md-right">
                                                <%=UtilCharacter.getDataByLanguage(Language, "তারিখ পর্যন্ত", "Data Upto")%>
                                            </label>
                                            <div class="col-md-8">
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="requested_date_end_js"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type="hidden" id="endDate" name="endDate">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 form-actions text-right mt-3">
                        <button onclick="loadAndShowData()" class="btn-sm shadow text-white border-0 submit-btn ml-2"
                                type="button">
                            <%=UtilCharacter.getDataByLanguage(Language, "লোড", "Load")%>
                        </button>
                    </div>
                </div>
            </form>
            <div class="mt-2 w-100" id="stock-summary" style="display:none;">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let language = '<%=Language%>';
    let subTypeCount = <%=index%>;
    let stockSummary = $('#stock-summary');

    async function loadAndShowData() {
        let startDateElement = document.getElementById("startDate");
        let endDateElement = document.getElementById("endDate");
        startDateElement.value = getDateTimestampById('requested_date_start_js');
        endDateElement.value = getDateTimestampById('requested_date_end_js');
        let startDate = startDateElement.value;
        let endDate = endDateElement.value;

        let url = "Pi_stockServlet?actionType=getSummarizedStockReportViewBodyTable" +
            "&startDate=" + startDate +
            "&endDate=" + endDate;
        const res = await fetch(url);
        const htmlText = await res.text();
        if (!htmlText.includes("to-print-div")) {
            toastr.error(getValueByLanguage(language, "কোন তথ্য পাওয়া যায়নি!", "No Data Found!"));
        }
        stockSummary.html(htmlText);
        stockSummary.show();
    }
</script>