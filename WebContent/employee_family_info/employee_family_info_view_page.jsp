<%@ page import="employee_family_info.Employee_family_infoDtoWithValue" %>
<%@ page import="java.util.List" %>
<%@ page import="login.LoginDTO" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="util.HttpRequestUtils" %>
<%
	String context = request.getContextPath();
	LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
	String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
	String ID = request.getParameter("ID");
	List<Employee_family_infoDtoWithValue> employee_family_infos = (List<Employee_family_infoDtoWithValue>) request.getAttribute("employee_family_info");
	boolean isLanguageEnglish = Language.equals("English");
%>
<script src="<%=context%>/assets/js/bootbox/bootbox.min.js" type="text/javascript"></script>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
	<thead>
		<tr>
			<th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.HM_NAME, loginDTO) %></b></th>
			<th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_RELATIONTYPE, loginDTO) %></b></th>
			<th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_PROFESSIONTYPE, loginDTO) %></b></th>
			<th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_MOBILENUMBER1, loginDTO) %></b></th>
			<th style="vertical-align: middle;text-align: left"><b><%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_EMAIL, loginDTO) %></b></th>
			<th style="vertical-align: middle;text-align: left"><b></b></th>
		</tr>
	</thead>
	<tbody>
		<%
			if (employee_family_infos != null && employee_family_infos.size() > 0) {
				int familyInfoIndex = 0;
				for (Employee_family_infoDtoWithValue employee_family_info : employee_family_infos) {
					++familyInfoIndex;
		%>
			<tr><%@include file="/employee_family_info/employee_family_info_item.jsp"%></tr>
		<%
				}
			}
		%>
	</tbody>
</table>

<div class="row">
	<div class="col-12 mt-3 text-right">
		<button class="btn btn-gray m-t-10 rounded-pill"
			onclick="location.href='Employee_family_infoServlet?actionType=getAddPage&empId=<%=ID%>&tab=1&userId=<%=request.getParameter("userId")%>'">
			<i class="fa fa-plus"></i>&nbsp; <%= LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_NEW_MEMBER, loginDTO) %>
		</button>
	</div>
</div>