<%@page import="employee_records.Employee_recordsDAO" %>
<%@page pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@ page import="relation.RelationRepository" %>
<%@ page import="profession.ProfessionRepository" %>
<%@ page import="religion.ReligionRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>

<td style="vertical-align: middle;text-align: left">
    <%=Employee_recordsDAO.getEmployeeName(employee_family_infoDTO.employeeRecordsId, Language)%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=isLangEng ? StringEscapeUtils.escapeHtml4(employee_family_infoDTO.firstNameEn) : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.firstNameBn)%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=RelationRepository.getInstance().getText(Language, employee_family_infoDTO.relationType)%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=ProfessionRepository.getInstance().getText(Language, employee_family_infoDTO.professionType)%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=CatRepository.getInstance().getText(Language, "gender", employee_family_infoDTO.genderCat)%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=ReligionRepository.getInstance().getText(Language, employee_family_infoDTO.religionType)%>
</td>

<td style="vertical-align: middle;text-align: left">
    <%=StringUtils.convertBanglaIfLanguageIsBangla(Language, StringEscapeUtils.escapeHtml4(employee_family_infoDTO.mobileNumber1))%>
</td>

<td style="vertical-align: middle;text-align: left">
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Employee_family_infoServlet?actionType=view&iD=<%=employee_family_infoDTO.iD%>&empId=<%=employee_family_infoDTO.employeeRecordsId%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td style="vertical-align: middle;text-align: left">
    <button type="button"
            class="btn-sm border-0 shadow btn-border-radius text-white"
            style="background-color: #ff6b6b;"
            onclick="location.href='Employee_family_infoServlet?actionType=getEditPage&iD=<%=employee_family_infoDTO.iD%>&empId=<%=employee_family_infoDTO.employeeRecordsId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>


<td class="text-right" style="vertical-align: middle">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=employee_family_infoDTO.iD%>'/></span>
    </div>
</td>