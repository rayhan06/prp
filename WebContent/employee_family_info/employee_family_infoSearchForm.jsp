<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_family_info.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@page pageEncoding="UTF-8" %>

<%

    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();

%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_EMPLOYEERECORDSID, loginDTO)%></th>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(isLangEng?LC.EMPLOYEE_FAMILY_INFO_EDIT_FIRSTNAMEEN:LC.EMPLOYEE_FAMILY_INFO_EDIT_FIRSTNAMEBN, loginDTO)%></th>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_RELATIONTYPE, loginDTO)%></th>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_PROFESSIONTYPE, loginDTO)%></th>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_GENDERCAT, loginDTO)%></th>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_RELIGIONTYPE, loginDTO)%></th>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_MOBILENUMBER1, loginDTO)%></th>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>
            <th style="vertical-align: middle;text-align: left"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_SEARCH_EMPLOYEE_FAMILY_INFO_EDIT_BUTTON, loginDTO)%>
            </th>
            <th class="text-center">
                <span><%=isLangEng?"All":"সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody class="text-nowrap">
        <%
            List<Employee_family_infoDTO> data = (List<Employee_family_infoDTO>) rn2.list;

            if (data != null && data.size() > 0) {
                for (Employee_family_infoDTO employee_family_infoDTO : data) {
        %>
        <tr>
            <%@include file="employee_family_infoSearchRow.jsp" %>
        </tr>
        <% }
        } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>


			