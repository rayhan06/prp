<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="relation.RelationRepository" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();
    String action = request.getParameter("url");
    String context = "../../.." + request.getContextPath() + "/";
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";

    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    int pagination_number = 0;
%>
<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <%--        <div class="kt-portlet__head-toolbar">--%>
        <%--            <div class="kt-portlet__head-group">--%>
        <%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
        <%--                     aria-hidden="true" x-placement="top"--%>
        <%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
        <%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
        <%--                    <div class="tooltip-inner">Collapse</div>--%>
        <%--                </div>--%>
        <%--            </div>--%>
        <%--        </div>--%>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_SEARCH_FIRSTNAMEEN, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="first_name_en" placeholder=""
                                   name="first_name_en"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_SEARCH_LASTNAMEEN, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="last_name_en" placeholder="" name="last_name_en"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_SEARCH_RELATIONTYPE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='relation_type' id='relation_type'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=RelationRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label"><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_MOBILENUMBER1, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <input type="number" class="form-control" id="mobile_number_1" name="mobile_number_1"
                                   onChange='setSearchChanged()'
                                   placeholder="<%=LM.getText(LC.PLACEHOLDER_ENTER_PERSONAL_MOBILE_NUMBER, loginDTO)%>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">
    $(document).ready(() => {
        select2SingleSelector('#relation_type', '<%=Language%>');
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        if ($("#first_name_en").val()) {
            params += '&first_name_en=' + $("#first_name_en").val();
        }
        if ($("#last_name_en").val()) {
            params += '&last_name_en=' + $("#last_name_en").val();
        }
        if ($("#relation_type").val()) {
            params += '&relation_type=' + $("#relation_type").val();
        }
        if ($("#national_id_no").val()) {
            params += '&national_id_no=' + $("#national_id_no").val();
        }
        if ($("#mobile_number_1").val()) {
            params += '&mobile_number_1=' + $("#mobile_number_1").val();
        }

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

