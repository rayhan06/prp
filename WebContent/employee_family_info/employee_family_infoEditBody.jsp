<%@page import="employee_family_info.Employee_family_infoDTO" %>
<%@page import="login.LoginDTO" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="user.UserDTO" %>
<%@page pageEncoding="UTF-8" %>

<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    boolean isLangEng = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Employee_family_infoDTO employee_family_infoDTO;
    String actionName,formTitle;
    long empId;
    if(request.getParameter("empId")!=null){
        empId = Long.parseLong(request.getParameter("empId"));
    }else{
        empId = userDTO.employee_record_id;
    }

    if("edit".equals(request.getParameter("actionType"))){
        actionName = "edit";
        formTitle = LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_EMPLOYEE_FAMILY_INFO_EDIT_FORMNAME, loginDTO);
        employee_family_infoDTO = (Employee_family_infoDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    }else{
        actionName = "add";
        formTitle = LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_EMPLOYEE_FAMILY_INFO_ADD_FORMNAME, loginDTO);
        employee_family_infoDTO = new Employee_family_infoDTO();
    }
    String url = "Employee_family_infoServlet?isPermanentTable=true&empId="+empId+"&iD="+employee_family_infoDTO.iD+"&actionType=ajax_"+actionName;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab")+"&userId="+request.getParameter("userId");
    }
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal" action="<%=url%>"
              id="employee-family-info" name="employee-family">
            <div class="kt-portlet__body form-body">
                <div>
                    <%@include file="addFormBody.jsp" %>
                </div>
                <div class="row">
                    <div class="col-12 text-right pr-4">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button"
                                onclick='location.href="<%=request.getHeader("referer")%>"'>
                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_EMPLOYEE_FAMILY_INFO_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button" onclick="submitForm()">
                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_EMPLOYEE_FAMILY_INFO_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var row = 0;
    const familyForm = $('#employee-family-info');
    $(document).ready(function () {
        init(row);
        basicInit();
        dateTimeInit("<%=Language%>");
        $("#employee-family-info").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            rules: {
                nationalIdNo: {
                    digits: true
                },
                firstNameEn: "required",
                firstNameBn: "required",
                relationType: "required",
                genderCat: "required",
            },
            messages: {
                nationalIdNo: {
                    digits: '<%=isLangEng ? "Enter only digit, please!" : "অনুগ্রহ করে শুধুমাত্র ডিজিট প্রবেশ করান!"%>'
                },
                firstNameEn: '<%=isLangEng ? "Please enter name in English!" : "অনুগ্রহ করে ইংরেজিতে নাম লিখুন!"%>',
                firstNameBn: '<%=isLangEng ? "Please enter name in Bangla!" : "অনুগ্রহ করে বাংলায় নাম লিখুন!"%>',
                relationType: '<%=isLangEng ? "Please select relation!" : "অনুগ্রহ করে সম্পর্ক বাছাই করুন!"%>',
                genderCat: '<%=isLangEng ? "Please select gender!" : "অনুগ্রহ করে লিঙ্গ বাছাই করুন!"%>',
                email: '<%=isLangEng ? "Please enter a valid email!" : "অনুগ্রহ করে বৈধ ইমেইল লিখুন!"%>',
            }
        });
    });

    function PreprocessBeforeSubmiting(row, validate) {
        $('#permanentAddress').val(getGeoLocation('permanentAddress_js'));
        $('#presentAddress').val(getGeoLocation('presentAddress_js'));

        let valid = true;

        valid = geoLocationValidator('permanentAddress_js', true) && valid;
        valid = geoLocationValidator('presentAddress_js', true) && valid;

        $("#employee-family-info").validate();
        if (validate === "report") {
        } else {
            var empty_fields = "";


            if (empty_fields !== "") {
                if (validate === "inplaceedit") {
                    $('<input type="submit">').hide().appendTo($('#tableForm')).click().remove();
                    return false;
                }
            }

        }


        $('#dob-date').val(getDateStringById('dob-date-js'));
        const jQueryValid = familyForm.valid();
        const dobValid = dateValidator('dob-date-js', false, {
            'errorEn': 'Enter valid date!',
            'errorBn': 'যথার্থ তারিখ প্রবেশ করান!'
        });
        return valid && dobValid && jQueryValid;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Employee_family_infoServlet");
        if (htmlID === 'permanentAddress_geoSelectField_0_1') {
            var dist = $('#permanentAddress_geoSelectField_0_1 option:selected').text();
            $('#homeDistrict_text_0').val(dist);
        }
    }

    function init(row) {
        initGeoLocation('presentAddress_geoSelectField_', row, "Employee_family_infoServlet");
        initGeoLocation('permanentAddress_geoSelectField_', row, "Employee_family_infoServlet");
        select2SingleSelector("#relationType_select_0", '<%=Language%>')
        select2SingleSelector("#professionType_select_0", '<%=Language%>')
        select2SingleSelector("#genderCat_category_0", '<%=Language%>')
        select2SingleSelector("#religionType_select_0", '<%=Language%>')
        select2SingleSelector("#nationalityType_select_0", '<%=Language%>')
        select2SingleSelector("#bloodGroupCat_category_0", '<%=Language%>')
        select2SingleSelector("#occupationType_select_0", '<%=Language%>')
        <%
        if (actionName.equals("edit")) {
        %>
        setGeoLocation('<%=employee_family_infoDTO.permanentAddress%>','permanentAddress_js');
        setGeoLocation('<%=employee_family_infoDTO.presentAddress%>','presentAddress_js');
        <%
        }
        %>
    }
    function submitForm(){
        buttonStateChange(true);
        if(PreprocessBeforeSubmiting(0,'<%=actionName%>')){
            submitAjaxForm('employee-family-info');
        }else{
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value){
        $('#submit-btn').prop('disabled',value);
        $('#cancel-btn').prop('disabled',value);
    }

    $('#permanentAddress_js').on('geolocation.change', (event, param) => {
        $('#homeDistrict_text_0').val(getDistrictText('permanentAddress_js'));
    });

</script>