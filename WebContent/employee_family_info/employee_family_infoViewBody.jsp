<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="employee_family_info.*" %>
<%@ page import="java.util.*" %>
<%@ page import="pb.*" %>
<%@page import="files.*" %>
<%@ page import="geolocation.*" %>
<%@ page import="relation.RelationRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="profession.ProfessionRepository" %>
<%@ page import="religion.ReligionRepository" %>
<%@ page import="nationality.NationalityRepository" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLangEng = "English".equalsIgnoreCase(Language);
    Employee_family_infoDTO employee_family_infoDTO = (Employee_family_infoDTO)request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=LM.getText(LC.HR_MANAGEMENT_FAMILY_INFORMATION_DETAILS, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title"><%=LM.getText(LC.HR_MANAGEMENT_FAMILY_INFORMATION_DETAILS, loginDTO)%></h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-nowrap">
                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=employee_family_infoDTO.firstNameEn == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.firstNameEn)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=employee_family_infoDTO.firstNameBn == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.firstNameBn)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_RELATIONTYPE, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=RelationRepository.getInstance().getText(Language, employee_family_infoDTO.relationType)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_DOB, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=StringUtils.getFormattedDate(Language, employee_family_infoDTO.dob)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_PROFESSIONTYPE, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=ProfessionRepository.getInstance().getText(Language, employee_family_infoDTO.professionType)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_GENDERCAT, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=CatRepository.getInstance().getText(Language, "gender", employee_family_infoDTO.genderCat)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_RELIGIONTYPE, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=ReligionRepository.getInstance().getText(Language, employee_family_infoDTO.religionType)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_NATIONALIDNO, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=StringEscapeUtils.escapeHtml4(isLangEng ? employee_family_infoDTO.nationalIdNo : StringUtils.convertToBanNumber(employee_family_infoDTO.nationalIdNo))%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_UBRN, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=employee_family_infoDTO.uBRN == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.uBRN)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_TIN, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=employee_family_infoDTO.tIN == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.tIN)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_NATIONALITYTYPE, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=NationalityRepository.getInstance().getText(Language, employee_family_infoDTO.nationalityType)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_PASSPORTNO, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=employee_family_infoDTO.passportNo == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.passportNo)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_PASSPORTISSUEDATE, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=StringUtils.getFormattedDate(Language, employee_family_infoDTO.passportIssueDate)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_PASSPORTEXPIRYDATE, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=StringUtils.getFormattedDate(Language, employee_family_infoDTO.passportExpiryDate)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_BLOODGROUPCAT, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=CatRepository.getInstance().getText(Language, "blood_group", employee_family_infoDTO.bloodGroupCat)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_EMAIL, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=employee_family_infoDTO.email == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.email)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_ORGANIZATIONNAME, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=employee_family_infoDTO.organizationName == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.organizationName)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_WORKPLACE, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=employee_family_infoDTO.workPlace == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.workPlace)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_PRESENTADDRESS, loginDTO)%>
                            </b></td>
                        <td colspan="3"><%=GeoLocationUtils.getGeoLocationString(employee_family_infoDTO.presentAddress, Language)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_PERMANENTADDRESS, loginDTO)%>
                            </b></td>
                        <td colspan="3"><%=GeoLocationUtils.getGeoLocationString(employee_family_infoDTO.permanentAddress, Language)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_HOMEDISTRICT, loginDTO)%>
                            </b></td>
                        <td colspan="3"><%=employee_family_infoDTO.homeDistrict == null ? "" : StringEscapeUtils.escapeHtml4(employee_family_infoDTO.homeDistrict)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_MOBILENUMBER1, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%= employee_family_infoDTO.mobileNumber1 == null ? "" : StringEscapeUtils.escapeHtml4((
                                isLangEng ? employee_family_infoDTO.mobileNumber1 : util.StringUtils.convertToBanNumber(employee_family_infoDTO.mobileNumber1)))%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_EDIT_MOBILENUMBER2, loginDTO)%>
                            </b></td>
                        <td style="width: 35%"><%= employee_family_infoDTO.mobileNumber2 == null ? "" : StringEscapeUtils.escapeHtml4((
                                isLangEng ? employee_family_infoDTO.mobileNumber2 : util.StringUtils.convertToBanNumber(employee_family_infoDTO.mobileNumber2)))%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, loginDTO)%>
                            </b></td>
                        <td colspan="3">
                            <%
                                {
                                    List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(employee_family_infoDTO.filesDropzone);
                            %>
                            <table>
                                <tr>
                                    <%
                                        if (FilesDTOList != null) {
                                            for (int j = 0; j < FilesDTOList.size(); j++) {
                                                FilesDTO filesDTO = FilesDTOList.get(j);
                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                    %>
                                    <td>
                                        <%
                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                        %> <img
                                            src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                            style='width: 100px'/>
                                        <%
                                            }
                                        %>
                                        <a href='Employee_family_infoServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                           download><%=filesDTO.fileTitle%>
                                        </a>
                                    </td>
                                    <%
                                            }
                                        }
                                    %>
                                </tr>
                            </table>
                            <% } %>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>