<%@ page import="util.StringUtils" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@page contentType="text/html;charset=utf-8" %>

<td style="vertical-align: middle;text-align: left"><%=StringEscapeUtils.escapeHtml4(isLanguageEnglish ? employee_family_info.dto.firstNameEn : employee_family_info.dto.firstNameBn)%>
</td>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? employee_family_info.relationEng : employee_family_info.relationBng%>
</td>
<td style="vertical-align: middle;text-align: left"><%=isLanguageEnglish ? employee_family_info.professionEng : employee_family_info.professionBng%>
</td>
<td style="vertical-align: middle;text-align: left"><%=StringEscapeUtils.escapeHtml4(isLanguageEnglish ? employee_family_info.dto.mobileNumber1 : StringUtils.convertToBanNumber(employee_family_info.dto.mobileNumber1)) %>
</td>
<td style="vertical-align: middle;text-align: left"><%=StringEscapeUtils.escapeHtml4(employee_family_info.dto.email) %>
</td>
<td style="vertical-align: middle;text-align: center">
    <form action="Employee_family_infoServlet?isPermanentTable=true&actionType=delete&tab=1&ID=<%=employee_family_info.dto.iD%>&empId=<%=ID%>&tab=1&userId=<%=request.getParameter("userId")%>"
          method="POST" id="familyMemberTableForm<%=familyInfoIndex%>" enctype="multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn-primary" style="max-height: 30px"
                    title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                    onclick="location.href='Employee_family_infoServlet?actionType=view&iD=<%=employee_family_info.dto.iD%>&empId=<%=ID%>&tab=1&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-eye"></i></button>&nbsp;
            <button type="button" class="btn-success" style="max-height: 30px"
                    title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO) %>"
                    onclick="location.href='Employee_family_infoServlet?actionType=getEditPage&iD=<%=employee_family_info.dto.iD%>&empId=<%=ID%>&tab=1&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-edit"></i></button>&nbsp;
            <button type="button" class="btn-danger" style="max-height: 30px"
                    title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                    onclick="deleteItem('familyMemberTableForm',<%=familyInfoIndex%>)"><i class="fa fa-trash"></i>
            </button>
        </div>
    </form>
</td>