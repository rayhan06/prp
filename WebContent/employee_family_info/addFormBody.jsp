<%@page pageEncoding="UTF-8" %>
<%@page import="dbm.DBMW" %>
<%@page import="files.FilesDAO" %>
<%@page import="files.FilesDTO" %>
<%@page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@page import="nationality.NationalityRepository" %>
<%@page import="profession.ProfessionRepository" %>
<%@page import="relation.RelationRepository" %>
<%@ page import="pb.CatRepository" %>
<%@ page import="religion.ReligionRepository" %>

<%
    long ColumnID;
    int year = Calendar.getInstance().get(Calendar.YEAR);
%>
<div class="kt-portlet__body form-body">
    <div class="row">
        <div class="col-md-12">
            <div class="onlyborder">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="sub_title_top">
                            <div class="sub_title">
                                <h4 style="background-color: white">
                                    <%=formTitle%>
                                </h4>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="firstNameEn_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEENG, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='firstNameEn_div_0'>
                                                <input type='text' class='englishOnly form-control rounded'
                                                       required="required" name='firstNameEn'
                                                       id='firstNameEn_text_0'
                                                       placeholder='<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_NAME_ENGLISH, loginDTO)%>'
                                                       value='<%=employee_family_infoDTO.firstNameEn == null?"":employee_family_infoDTO.firstNameEn%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="firstNameBn_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_RECORDS_ADD_NAMEBNG, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='firstNameBn_div_0'>
                                                <input type='text' class='noEnglish form-control rounded'
                                                       required="required" name='firstNameBn' id='firstNameBn_text_0'
                                                       placeholder='<%=LM.getText(LC.CATEGORY_ADD_PLACE_HOLDER_DOMAIN_NAME_BANGLA, loginDTO)%>'
                                                       value='<%= employee_family_infoDTO.firstNameBn!=null?employee_family_infoDTO.firstNameBn:""%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="relationType_select_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_RELATIONTYPE, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='relationType_div_0'>
                                                <select class='form-control' name='relationType' required="required"
                                                        id='relationType_select_0' tag='pb_html'>
                                                    <%=RelationRepository.getInstance().buildOptions(Language, employee_family_infoDTO.relationType)%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_DOB, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='dob_div_0'>
                                                <jsp:include page="/date/date.jsp">
                                                    <jsp:param name="DATE_ID" value="dob-date-js"></jsp:param>
                                                    <jsp:param name="LANGUAGE" value="<%=Language%>"></jsp:param>
                                                </jsp:include>
                                                <input type='hidden' class='form-control' id='dob-date' name='dob'
                                                       value='' tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="professionType_select_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_PROFESSIONTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='professionType_div_0'>
                                                <select class='form-control' name='professionType'
                                                        id='professionType_select_0' tag='pb_html'>
                                                    <%=ProfessionRepository.getInstance().buildOptions(Language, employee_family_infoDTO.professionType)%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="genderCat_category_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_GENDERCAT, loginDTO)%>
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='genderCat_div_0'>
                                                <select class='form-control' name='genderCat'
                                                        id='genderCat_category_0'
                                                        tag='pb_html'>
                                                    <%=CatRepository.getInstance().buildOptions("gender", Language, employee_family_infoDTO.genderCat)%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="religionType_select_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_RELIGIONTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='religionType_div_0'>
                                                <select class='form-control' name='religionType'
                                                        id='religionType_select_0' tag='pb_html'>
                                                    <%= ReligionRepository.getInstance().buildOptions(Language, employee_family_infoDTO.religionType)%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="nationalIdNo_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_NATIONALIDNO, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='nationalIdNo_div_0'>
                                                <input type='text' class='form-control rounded' name='nationalIdNo'
                                                       id='nationalIdNo_text_0'
                                                       placeholder='<%=LM.getText(LC.PLACEHOLDER_ENTER_NID_NUMBER, loginDTO)%>'
                                                       value='<%=employee_family_infoDTO.nationalIdNo!=null?employee_family_infoDTO.nationalIdNo:""%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="uBRN_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_UBRN, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='uBRN_div_0'>
                                                <input type='text' class='form-control rounded' name='uBRN'
                                                       id='uBRN_text_0'
                                                       placeholder='<%=LM.getText(LC.PLACEHOLDER_ENTER_BIRTH_CERTIFICATE_NO, loginDTO)%>'
                                                       tag='pb_html'
                                                       value='<%=employee_family_infoDTO.uBRN!=null?employee_family_infoDTO.uBRN:""%>'>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="passportNo_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_PASSPORTNO, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='passportNo_div_0'>
                                                <input type='text' class='form-control rounded' name='passportNo'
                                                       id='passportNo_text_0'
                                                       placeholder='<%=LM.getText(LC.PLACEHOLDER_ENTER_PASSPORT_NUMBER, loginDTO)%>'
                                                       tag='pb_html'
                                                       value='<%=employee_family_infoDTO.passportNo!=null?employee_family_infoDTO.passportNo:""%>'>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="nationalityType_select_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_NATIONALITYTYPE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='nationalityType_div_0'>
                                                <select class='form-control' name='nationalityType'
                                                        id='nationalityType_select_0' tag='pb_html'>
                                                    <%=NationalityRepository.getInstance().buildOptions(Language, employee_family_infoDTO.nationalityType)%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right"
                                               for="bloodGroupCat_category_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_BLOODGROUPCAT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='bloodGroup_div_0'>
                                                <div id='bloodGroupCat_div_0'>
                                                    <select class='form-control' name='bloodGroupCat'
                                                            id='bloodGroupCat_category_0' tag='pb_html'>
                                                        <%=CatRepository.getInstance().buildOptions("blood_group", Language, employee_family_infoDTO.bloodGroupCat)%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right" for="mobileNumber1_text_0">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_MOBILENUMBER1, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">+88</div>
                                                </div>
                                                <input type='text' class='digitOnly form-control rounded'
                                                       name='mobileNumber1' maxlength="11"
                                                       id='mobileNumber1_text_0'
                                                       pattern="^(01[3-9]{1}[0-9]{8})"
                                                       value='<%=employee_family_infoDTO.mobileNumber1 ==null || employee_family_infoDTO.mobileNumber1.length() == 0?"":
                                                                   (employee_family_infoDTO.mobileNumber1.startsWith("88") ? employee_family_infoDTO.mobileNumber1.substring(2) : employee_family_infoDTO.mobileNumber1)%>'
                                                       placeholder='<%=LM.getText(LC.GLOBAL_MOBILE_PLACE_HOLDER, loginDTO)%>'
                                                       title='<%=isLangEng?"personal mobile number must start with 01, then contain 9 digits"
                                                               :"ব্যক্তিগত মোবাইল নাম্বার 01 দিয়ে শুরু হবে, তারপর ৯টি সংখ্যা হবে"%>'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMPLOYEE_FAMILY_INFO_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8">
                                            <div id='filesDropzone_div_0'>
                                                <%
                                                    if (actionName.equals("edit")) {
                                                        List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(employee_family_infoDTO.filesDropzone);
                                                %>
                                                <table>
                                                    <tr>
                                                        <%
                                                            if (filesDropzoneDTOList != null) {
                                                                for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                    FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                    byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                        %>
                                                        <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                            <%
                                                                if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                            %>
                                                            <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
						%>' style='width:100px'/>
                                                            <%
                                                                }
                                                            %>
                                                            <a href='Employee_family_infoServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                               download><%=filesDTO.fileTitle%>
                                                            </a>
                                                            <a class='btn btn-danger'
                                                               onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_0")'>x</a>
                                                        </td>
                                                        <%
                                                                }
                                                            }
                                                        %>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>

                                                <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                                <div class="dropzone"
                                                     action="Employee_family_infoServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?employee_family_infoDTO.filesDropzone:ColumnID%>">
                                                    <input type='file' style="display:none" class="rounded"
                                                           name='filesDropzoneFile'
                                                           id='filesDropzone_dropzone_File_0'
                                                           tag='pb_html'/>
                                                </div>
                                                <input type='hidden' name='filesDropzoneFilesToDelete'
                                                       id='filesDropzoneFilesToDelete_0' value=''
                                                       tag='pb_html'/>
                                                <input type='hidden' name='filesDropzone'
                                                       id='filesDropzone_dropzone_0'
                                                       tag='pb_html'
                                                       value='<%=actionName.equals("edit")?employee_family_infoDTO.filesDropzone:ColumnID%>'/>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

        $('#passport-issue-date-js').on('datepicker.change', () => {
            setMinDateById('passport-expiry-date-js', getDateStringById('passport-issue-date-js'));
        });

        <%if(actionName.equals("edit")) {%>
        setDateByTimestampAndId('dob-date-js', <%=employee_family_infoDTO.dob%>);
        <%}%>
    });
</script>