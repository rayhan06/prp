<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_job_description.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="workflow.WorkflowController" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@page import="files.*" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDAO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDTO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleRepository" %>
<%@ page import="geolocation.GeoDistrictDAO" %>
<%@ page import="geolocation.GeoDistrictDTO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="recruitment_job_required_files.Recruitment_job_required_filesRepository" %>
<%@ page import="recruitment_job_req_trains_and_certs.Recruitment_job_req_trains_and_certsRepository" %>
<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>

<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String actionName = "edit";
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }
    out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
    String value = "";
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    long id = Long.parseLong(ID);
    System.out.println("ID = " + ID);
    Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO("recruitment_job_description");
    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = Recruitment_job_descriptionRepository.
            getInstance().getRecruitment_job_descriptionDTOByID(id);
    String Value = "";
    int i = 0;
    FilesDAO filesDAO = new FilesDAO();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>

<style>
    .form-group {
        margin-bottom: .5rem !important;
    }
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title table-title">
                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_ADD_FORMNAME, loginDTO)%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <div class="row">
                <div class="col-md-10 offset-md-1 text-md-right">
                    <%
                        if (!recruitment_job_descriptionDTO.roll_created &&
                                new Job_applicant_applicationDAO().decisionNotStarted(recruitment_job_descriptionDTO.iD)) {

                    %>
                    <button style="float: right" class="btn btn-sm submit-btn text-white shadow ml-2"
                            onclick="generateRoll('<%=recruitment_job_descriptionDTO.iD%>')">
                        <%=Language.equalsIgnoreCase("English") ? "Generate Roll" : "রোল তৈরী"%>
                    </button>

                    <%
                        }
                    %>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_BASIC_INFO, loginDTO)%>
                                        </h4>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group row">

                                        <div class="form-group col-md-12 row">
                                            <span class="col-md-3 col-form-label font-weight-bold text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%>
                                            </span>
                                            <span class="col-md-7 col-form-label">
                                                <%

                                                    Recruitment_test_nameDTO recruitment_test_nameDTO = Recruitment_test_nameRepository.getInstance().getRecruitment_test_nameDTOByiD(recruitment_job_descriptionDTO.recruitmentTestName);
                                                    if (recruitment_job_descriptionDTO.recruitmentTestName != 0 && recruitment_test_nameDTO != null && Language.equalsIgnoreCase("english")) {
                                                        value = recruitment_test_nameDTO.nameEn + "";
                                                    } else if (recruitment_job_descriptionDTO.recruitmentTestName != 0 && recruitment_test_nameDTO != null && Language.equalsIgnoreCase("bangla")) {
                                                        value = recruitment_test_nameDTO.nameBn + "";
                                                    }else{
                                                        value = "";
                                                    } %>
                                                <%=Utils.getDigits(value, Language)%>
                                            </span>
                                        </div>

                                        <div class="form-group col-md-6 row">
                                            <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEEN, loginDTO)%>
                                            </span>
                                            <span class="col-md-6 col-form-label">
                                                <%=recruitment_job_descriptionDTO.jobTitleEn%>
                                            </span>
                                        </div>

                                        <div class="form-group col-md-6 row">
                                            <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEBN, loginDTO)%>
                                            </span>

                                            <span class="col-md-6 col-form-label">
                                                <%=recruitment_job_descriptionDTO.jobTitleBn%>
                                            </span>

                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group col-md-6 row">
                                            <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>

                                            </span>

                                            <span class="col-md-6 col-form-label">
                                                <%
                                                    value = CatRepository.getName(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);
                                                %>

                                                <%=Utils.getDigits(value, Language)%>
                                            </span>


                                        </div>

                                        <div class="form-group col-md-6 row">

                                            <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%>
                                            </span>

                                            <span class="col-md-6 col-form-label">
                                                <%
                                                    value = CatRepository.getName(Language, "employment_status", recruitment_job_descriptionDTO.employmentStatusCat);
                                                %>

                                                <%=Utils.getDigits(value, Language)%>
                                            </span>


                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group col-md-6 row ">
                                            <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYEEPAYSCALETYPE, loginDTO)%>

                                            </span>

                                            <span class="col-md-6 col-form-label">
                                                <%
                                                    //									long payScaleTypeId = recruitment_job_descriptionDTO.employeePayScaleType;
//										Employee_pay_scaleDAO employee_pay_scaleDAO = new Employee_pay_scaleDAO();
//										Employee_pay_scaleDTO employee_pay_scaleDTO =
//												employee_pay_scaleDAO.getDTOByID(payScaleTypeId);
//										String payScaleName = "";
//										if(employee_pay_scaleDTO != null){
//											payScaleName = employee_pay_scaleDTO.payScaleBrief;
//										}

                                                    value = "";
                                                    Employee_pay_scaleDTO employee_pay_scaleDTO = Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleDTOByID(recruitment_job_descriptionDTO.employeePayScaleType);
                                                    if (employee_pay_scaleDTO != null) {
                                                        value = CatRepository.getName(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);

                                                    }
                                                %>

                                                <%=value%>
                                            </span>


                                        </div>

                                        <div class="form-group col-md-6 row">
                                            <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_NUMBEROFVACANCY, loginDTO)%>

                                            </span>

                                            <span class="col-md-6 col-form-label">
                                                <%
                                                    value = recruitment_job_descriptionDTO.numberOfVacancy + "";
                                                %>

                                                <%=Utils.getDigits(value, Language)%>

                                            </span>


                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_REQUIREMENTS, loginDTO)%>
                                        </h4>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group row">

                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EDUCATIONLEVELTYPE, loginDTO)%>
                                                </span>

                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = Education_levelRepository.getInstance().getText
                                                                (Language, recruitment_job_descriptionDTO.educationLevelType) ;
                                                    %>
                                                    <%
//                                                        value = CommonDAO.getName(Integer.parseInt(value), "education_level", Language.equals("English") ? "name_en" : "name_bn", "id");
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>


                                            </div>

                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EDUCATIONLEVELTYPE, loginDTO)%>--%>
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_EDU_LEVEL_YEARLY, loginDTO)%>
                                                </span>

                                                <span class="col-md-6 col-form-label">
                                                    <%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EDUCATIONLEVELTYPE, loginDTO)%>--%>
                                                    <%
                                                        value = Education_levelRepository.getInstance().getText
                                                                (Language, recruitment_job_descriptionDTO.yearly_education_level_type) ;
                                                    %>
                                                    <%
//                                                        value = CommonDAO.getName(Integer.parseInt(value), "education_level", Language.equals("English") ? "name_en" : "name_bn", "id");
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>


                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINEXPERIENCE, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = String.format("%.1f", recruitment_job_descriptionDTO.minExperience);
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>

                                            </div>

                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%>--%>
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MINIMUM_AGE, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%>--%>
                                                    <%
                                                        value = recruitment_job_descriptionDTO.min_age + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>

                                            </div>


                                        </div>

                                        <div class="form-group row">

                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEREGULAR, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>

                                            </div>


                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>

                                            </div>


                                        </div>

                                        <div class="form-group row">

                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%>--%>
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MAX_AGE_FF_GRANDCHILDREN, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%>--%>
                                                    <%
                                                        value = recruitment_job_descriptionDTO.max_age_for_grandchildren_ff + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>

                                            </div>

                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%>--%>
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_DISABLED, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%--				<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%>--%>
                                                    <%
                                                        value = recruitment_job_descriptionDTO.max_age_disable + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>

                                            </div>


                                        </div>

                                        <div class="form-group row">
                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_CANCELED_DISTRICTS, loginDTO)%>
                                                    <%--						Districts--%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%


                                                        value = "";
                                                        value = recruitment_job_descriptionDTO.districts;
                                                        StringTokenizer tokenizer = new StringTokenizer(value, ",");
                                                        GeoDistrictDAO geoDistrictDAO = new GeoDistrictDAO();
                                                        String separator = "";
                                                        value = "";
                                                        while (tokenizer.hasMoreTokens()) {
                                                            Long distNum = Long.valueOf(tokenizer.nextToken().trim());
                                                            GeoDistrictDTO dto = geoDistrictDAO.getById(distNum);
                                                            value = value + separator + (Language.equalsIgnoreCase("English") ? dto.nameEng : dto.nameBan);
                                                            separator = ",";
                                                        }
                                                    %>

                                                    <%=value%>                                            <%--						Districts--%>
                                                </span>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <%--									<div class="col-md-1"></div>--%>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <%--						<div class="col-md-1"></div>--%>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_DATES, loginDTO)%>
                                        </h4>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group row">

                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAGECALCULATIONDATE, loginDTO)%>
                                                </span>

                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.lastAgeCalculationDate + "";
                                                    %>
                                                    <%
                                                        String f1 = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(f1, Language)%>
                                                </span>


                                            </div>

                                            <div class="form-group col-md-6 row">


                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_ONLINEJOBPOSTINGDATE, loginDTO)%>

                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.onlineJobPostingDate + "";
                                                    %>
                                                    <%
                                                        f1 = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(f1, Language)%>
                                                </span>

                                            </div>


                                        </div>
                                        <div class="form-group row">


                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FIRST_APPLICATION_DATE, loginDTO)%>

                                                    <%--							Start Date Application--%>

                                                </span>

                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.firstApplicationDate + "";
                                                    %>
                                                    <%
                                                        f1 = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(f1, Language)%>
                                                    <%--							Start Date Application--%>

                                                </span>


                                            </div>


                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAPPLICATIONDATE, loginDTO)%>
                                                </span>

                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.lastApplicationDate + "";
                                                    %>
                                                    <%
                                                        f1 = simpleDateFormat.format(new Date(Long.parseLong(value)));
                                                    %>
                                                    <%=Utils.getDigits(f1, Language)%>
                                                </span>


                                            </div>


                                        </div>
                                    </div>

                                </div>
                            </div>
                            <%--									<div class="col-md-1"></div>--%>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <%--						<div class="col-md-1"></div>--%>
            </div>
            <div class="row my-4">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_DESCRIPTION, loginDTO)%>
                                        </h4>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBPURPOSE, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.jobPurpose + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>
                                            </div>
                                            <div class="form-group col-md-6 row">

                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBRESPONSIBILITIES, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.jobResponsibilities + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>
                                            </div>
                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_PERSONALCHARACTERISTICS, loginDTO)%>
                                                </span>
                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.personalCharacteristics + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>

                                            </div>
                                            <div class="form-group col-md-6 row">

                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCOMPETENCE, loginDTO)%>
                                                </span>

                                                <span class="col-md-6 col-form-label">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.requiredCompetence + "";
                                                    %>

                                                    <%=Utils.getDigits(value, Language)%>
                                                </span>
                                            </div>
                                            <div class="form-group col-md-6 row">
                                                <span class="col-md-6 col-form-label font-weight-bold text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FILESDROPZONE, loginDTO)%>
                                                </span>

                                                <div class="col-md-6">
                                                    <%
                                                        value = recruitment_job_descriptionDTO.filesDropzone + "";
                                                    %>
                                                    <%
                                                        {
                                                            List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(recruitment_job_descriptionDTO.filesDropzone);
                                                    %>
                                                    <table>
                                                        <tr>
                                                            <%
                                                                if (FilesDTOList != null) {
                                                                    for (int j = 0; j < FilesDTOList.size(); j++) {
                                                                        FilesDTO filesDTO = FilesDTOList.get(j);
                                                                        byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                            %>
                                                            <td>
                                                                <%
                                                                    if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                                %>
                                                                <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                                     style='width:100px'/>
                                                                <%
                                                                    }
                                                                %>
                                                                <a href='Recruitment_job_descriptionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                                   download><%=filesDTO.fileTitle%>
                                                                </a>
                                                            </td>
                                                            <%
                                                                    }
                                                                }
                                                            %>
                                                        </tr>
                                                    </table>
                                                    <%
                                                        }
                                                    %>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="onlyborder">
                        <div class="row">
                            <div class="col-12">
                                <div class="sub_title_top">
                                    <div class="sub_title">
                                        <h4 style="background: white">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_REQUIRED_FILES_AND_QUALIFICATIONS, loginDTO)%>
                                        </h4>
                                    </div>

                                    <div class="mt-3">
                                        <h5 class="table-title">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES, loginDTO)%>
                                        </h5>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped text-nowrap">
                                                <tr>
                                                    <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_RECRUITMENTJOBREQUIREDFILESTYPE, loginDTO)%>
                                                    </th>
                                                    <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_ISMANDATORYFILES, loginDTO)%>
                                                    </th>
                                                    <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_MODIFIEDBY, loginDTO)%></th>--%>
                                                </tr>
                                                <%
//                                                    RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO();
                                                    List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOs = RecruitmentJobSpecificFilesRepository.getInstance().getRecruitmentJobSpecificFilesDTOByJobDescriptionId(recruitment_job_descriptionDTO.iD);

                                                    for (RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO : recruitmentJobSpecificFilesDTOs) {
                                                %>
                                                <tr>
                                                    <td>
                                                        <%
                                                            value = Recruitment_job_required_filesRepository.getInstance().
                                                                    getText(Language, recruitmentJobSpecificFilesDTO.recruitmentJobRequiredFilesType);
                                                        %>
                                                        <%

//                                                            value = CommonDAO.getName(Integer.parseInt(value), "recruitment_job_required_files", Language.equals("English") ? "name_en" : "name_bn", "id");
                                                        %>

                                                        <%=Utils.getDigits(value, Language)%>


                                                    </td>
                                                    <td>
                                                        <%
                                                            String text = "";
                                                            if (recruitmentJobSpecificFilesDTO.isMandatoryFiles) {
                                                                text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_YES, loginDTO);
                                                            } else {
                                                                text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NO, loginDTO);
                                                            }
                                                        %>
                                                        <%= text%>


                                                    </td>
                                                    <%--										<td>--%>
                                                    <%--											<%--%>
                                                    <%--											value = recruitmentJobSpecificFilesDTO.modifiedBy + "";--%>
                                                    <%--											%>--%>
                                                    <%--														--%>
                                                    <%--											<%=Utils.getDigits(value, Language)%>--%>
                                                    <%--				--%>
                                                    <%--			--%>
                                                    <%--										</td>--%>
                                                </tr>
                                                <%

                                                    }

                                                %>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="mt-5 mb-3">
                                        <h5 class="table-title">
                                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS, loginDTO)%>
                                        </h5>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped text-nowrap">
                                                <tr>
                                                    <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_RECRUITMENTJOBREQTRAINSANDCERTSTYPE, loginDTO)%>
                                                    </th>
                                                    <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_ISMANDATORYCERTS, loginDTO)%>
                                                    </th>
                                                    <th><%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_FILE_REQUIRED, loginDTO)%>
                                                    </th>
                                                </tr>
                                                <%
//                                                    RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO();
                                                    List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOs = RecruitmentJobSpecificCertsRepository.getInstance()
                                                            .getRecruitmentJobSpecificCertsDTOByJobDescriptionId(recruitment_job_descriptionDTO.iD);

                                                    for (RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO : recruitmentJobSpecificCertsDTOs) {
                                                %>
                                                <tr>
                                                    <td>
                                                        <%
                                                            value = Recruitment_job_req_trains_and_certsRepository.getInstance().getText
                                                                    (Language, recruitmentJobSpecificCertsDTO.recruitmentJobReqTrainsAndCertsType);
                                                            
                                                        %>
                                                        <%
//                                                            value = CommonDAO.getName(Integer.parseInt(value), "recruitment_job_req_trains_and_certs", Language.equals("English") ? "name_en" : "name_bn", "id");
                                                        %>

                                                        <%=Utils.getDigits(value, Language)%>


                                                    </td>
                                                    <td>


                                                        <%
                                                            String text = "";
                                                            if (recruitmentJobSpecificCertsDTO.isMandatoryCerts) {
                                                                text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_YES, loginDTO);
                                                            } else {
                                                                text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NO, loginDTO);
                                                            }
                                                        %>
                                                        <%= text%>


                                                    </td>


                                                    <td>
                                                        <%
                                                            text = "";
                                                            if (recruitmentJobSpecificCertsDTO.file_required) {
                                                                text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_YES, loginDTO);
                                                            } else {
                                                                text = LM.getText(LC.JOB_APPLICANT_DOCUMENTS_NO, loginDTO);
                                                            }
                                                        %>
                                                        <%= text%>
                                                    </td>
                                                </tr>
                                                <%

                                                    }

                                                %>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function generateRoll(jobId) {
        event.preventDefault();
        // console.log(jobId);

        let formData = new FormData();
        formData.append('jobId', jobId);

        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                setTimeout(function () {
                    location.reload();
                }, 3000);
                toastr.success('<%=Language.equalsIgnoreCase("English")?"Successfully Roll Numbers Generated"
				: "রোল নম্বরসমূহ তৈরী হয়েছে "%>');


            } else if (this.readyState == 4 && this.status != 200) {
            }
        };

        let params = "Job_applicant_applicationServlet?actionType=generateRoll";
        xhttp.open("POST", params, true);
        xhttp.send(formData);

    }
</script>