
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_job_description.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}

String value = "";
String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = (Recruitment_job_descriptionDAO)request.getAttribute("recruitment_job_descriptionDAO");
String ViewAll;
if(request.getParameter("ViewAll") != null)
{
	ViewAll= request.getParameter("ViewAll");
}
else
{
	ViewAll= "0";
}

String navigator2 = SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION;
System.out.println("navigator2 = " + navigator2);
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
System.out.println("rn2 = " + rn2);
String pageno2 = ( rn2 == null ) ? "1" : "" + rn2.getCurrentPageNo();
String totalpage2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalPages();
String totalRecords2 = ( rn2 == null ) ? "1" : "" + rn2.getTotalRecords();
String lastSearchTime = ( rn2 == null ) ? "0" : "" + rn2.getSearchTime();
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

String successMessageForwarded = "Forwarded to your Senior Office";
String successMessageApproved = "Approval Done";
%>				
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBTITLEEN, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBTITLEBN, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBGRADECAT, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_EMPLOYMENTSTATUSCAT, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_EMPLOYEEPAYSCALETYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBPURPOSE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBRESPONSIBILITIES, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_MINIMUMACADEMICQUALIFICATION, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_MINIMUMEXPERIENCEREQUIRED, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_REQUIREDCERTIFICATIONANDTRANING, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_PERSONALCHARACTERISTICS, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_REQUIREDCOMPETENCE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_EDUCATIONLEVELTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_MINEXPERIENCE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_MAXAGEONBOUNDARYDATEREGULAR, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_MAXAGEONBOUNDARYDATEFF, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_JOBLOCATION, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_NUMBEROFVACANCY, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LASTAPPLICATIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LASTAGECALCULATIONDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_ONLINEJOBPOSTINGDATE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_FILESDROPZONE, loginDTO)%></th>

								<th>View Details</th>
								
								<th>Approval Path</th>
								
								<th>Initiated By</th>
								
								<th>Date of Initiation</th>
								
								<th>Assigned To</th>
								
								<th>Assignment Date</th>
								
								<th>Due Date</th>
								
								<th>Status</th>

								<th>Workflow Action</th>
								
								<th>Validate</th>
								

								<th>History</th>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_RECRUITMENT_JOB_DESCRIPTION);

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO) data.get(i);
																																
											
											%>
											<tr id = 'tr_<%=i%>'>
											<%
											
								%>
											
		
								<%  								
								    request.setAttribute("recruitment_job_descriptionDTO",recruitment_job_descriptionDTO);
								%>  
								
								 <jsp:include page="./recruitment_job_descriptionApprovalRow.jsp">
								 		<jsp:param name="pageName" value="searchrow" />
								 		<jsp:param name="rownum" value="<%=i%>" />
								 </jsp:include>			

								
								<%

											%>
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />
<input type="hidden" id="ViewAll" value="<%=ViewAll%>" />
