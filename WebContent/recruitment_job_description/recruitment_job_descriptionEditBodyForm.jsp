﻿<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="recruitment_job_required_files.Recruitment_job_required_filesRepository" %>
<%@ page import="recruitment_job_req_trains_and_certs.Recruitment_job_req_trains_and_certsRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameDTO" %>
<%@ page import="recruitment_test_name.Recruitment_test_nameRepository" %>
<%
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    List<Recruitment_test_nameDTO> recruitment_test_nameDTOS = Recruitment_test_nameRepository.getInstance().getRecruitment_test_nameList();
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Recruitment_job_descriptionServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="job-form" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="formSubmit()">
            <input type='hidden' class='form-control' name='iD' id='iD_hidden_<%=i%>'
                   value='<%=recruitment_job_descriptionDTO.iD%>' tag='pb_html'/>
            <input type='hidden' class='form-control' name='jobCat' id='jobCat_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.jobCat + "'"):("'" + "-1" + "'")%> tag='pb_html'/>
            <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
            <input type='hidden' class='form-control' name='lastModificationTime'
                   id='lastModificationTime_hidden_<%=i%>'
                   value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-12">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_BASIC_INFO, loginDTO)%>

                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row mx-2">

                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-right ">
                                                    <%=LM.getText(LC.RECRUITMENT_TEST_NAME_ADD_RECRUITMENT_TEST_NAME_ADD_FORMNAME, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-7">
                                                    <select
                                                            class='form-control ' name='recruitmentTestName'
                                                            id='recruitmentTestName_<%=i%>' tag='pb_html'>
                                                        <%


                                                            String recruitmentTestNameOptions = Utils.buildSelectOption(isLanguageEnglish);
                                                            StringBuilder option = new StringBuilder();
                                                            for(Recruitment_test_nameDTO recruitment_test_nameDTO:recruitment_test_nameDTOS){

                                                                if(actionName.equals("edit")){
                                                                    if(recruitment_test_nameDTO.iD == recruitment_job_descriptionDTO.recruitmentTestName){
                                                                        option.append("<option value = '").append(recruitment_test_nameDTO.iD).append("' selected>");
                                                                    }
                                                                    else{
                                                                        option.append("<option value = '").append(recruitment_test_nameDTO.iD).append("'>");
                                                                    }
                                                                }
                                                                else{

                                                                    option.append("<option value = '").append(recruitment_test_nameDTO.iD).append("'>");

                                                                }

                                                                option.append(isLanguageEnglish ? recruitment_test_nameDTO.nameEn : recruitment_test_nameDTO.nameBn).append("</option>");
                                                            }
                                                            recruitmentTestNameOptions+=option.toString();
                                                        %>
                                                        <%=recruitmentTestNameOptions%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-left text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEEN, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control englishOnly noPaste' name='jobTitleEn'
                                                           id='jobTitleEn_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.jobTitleEn + "'"):("'" + "" + "'")%>  tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-left text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEBN, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='text' class='form-control noEnglish noPaste' name='jobTitleBn'
                                                           id='jobTitleBn_text_<%=i%>'
                                                           value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.jobTitleBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-left text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select onchange="onGradeChange()"
                                                            class='form-control jobGradeClass' name='jobGradeCat'
                                                            id='jobGradeCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            if (actionName.equals("edit")) {
                                                                Options = CatRepository.getOptions(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);
                                                            } else {
                                                                Options = CatRepository.getOptions(Language, "job_grade", -2);
                                                            }
                                                        %>
                                                        <%=Options%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-left text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='employmentStatusCat'
                                                            id='employmentStatusCat_category_<%=i%>' tag='pb_html'>
                                                        <%
                                                            if (actionName.equals("edit")) {
                                                                Options = CatRepository.getOptions(Language, "employment_status", recruitment_job_descriptionDTO.employmentStatusCat);
                                                            } else {
                                                                Options = CatRepository.getOptions(Language, "employment_status", -2);
                                                            }
                                                        %>
                                                        <%=Options%>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-left text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYEEPAYSCALETYPE, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class='form-control' name='employeePayScaleType'
                                                            id='employeePayScaleType_select_<%=i%>' tag='pb_html'>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-md-4 col-form-label text-left text-md-right">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_NUMBEROFVACANCY, loginDTO)%>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type='number' class='form-control' name='numberOfVacancy'
                                                           id='numberOfVacancy_number_<%=i%>' min='0' max='1000000'
                                                           value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.numberOfVacancy + "'"):("'" + 0 + "'")%>  tag='pb_html'>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-5">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_REQUIREMENTS, loginDTO)%>

                                            </h4>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EDUCATIONLEVELTYPE, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select class='form-control' name='educationLevelType'
                                                                id='educationLevelType_select_<%=i%>' tag='pb_html'>
                                                            <%
                                                                Options = Education_levelRepository.getInstance().buildOptions(Language, recruitment_job_descriptionDTO.educationLevelType);

//                                                                if (actionName.equals("edit")) {
//                                                                    Options = CommonDAO.getOptions(Language, "select", "education_level", "educationLevelType_select_" + i, "form-control", "educationLevelType", recruitment_job_descriptionDTO.educationLevelType + "");
//                                                                } else {
//                                                                    Options = CommonDAO.getOptions(Language, "select", "education_level", "educationLevelType_select_" + i, "form-control", "educationLevelType");
//                                                                }
                                                            %>
                                                            <%=Options%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_EDU_LEVEL_YEARLY, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select class='form-control' name='yearlyEducationLevelType'
                                                                id='yearlyEducationLevelType_select_<%=i%>'
                                                                tag='pb_html'>
                                                            <%
                                                                Options = Education_levelRepository.getInstance().buildOptions(Language, recruitment_job_descriptionDTO.yearly_education_level_type);

//                                                                if (actionName.equals("edit")) {
//                                                                     CommonDAO.getOptions(Language, "select", "education_level", "yearlyEducationLevelType_select_" + i, "form-control", "yearlyEducationLevelType",  + "");
//                                                                } else {
//                                                                    Options = CommonDAO.getOptions(Language, "select", "education_level", "yearlyEducationLevelType_select_" + i, "form-control", "yearlyEducationLevelType");
//                                                                }
                                                            %>
                                                            <%=Options%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINEXPERIENCE, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='number' class='form-control' name='minExperience'
                                                               id='minExperience_text_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.minExperience + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MINIMUM_AGE, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='number' class='form-control' name='minAge'
                                                               id='minAge_text_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.min_age + "'"):("'" + "18" + "'")%>   tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEREGULAR, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='number' class='form-control'
                                                               name='maxAgeOnBoundaryDateRegular'
                                                               id='maxAgeOnBoundaryDateRegular_text_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular + "'"):("'" + "0" + "'")%>   tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='number' class='form-control'
                                                               name='maxAgeOnBoundaryDateFf'
                                                               id='maxAgeOnBoundaryDateFf_text_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf + "'"):("'" + "32" + "'")%>   tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_MAX_AGE_FF_GRANDCHILDREN, loginDTO)%>
                                                        <span class="required"> * </span>

                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='number' class='form-control'
                                                               name='maxAgeOnBoundaryDateGrandChildrenFf'
                                                               id='maxAgeOnBoundaryDateGrandChildrenFf_text_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.max_age_for_grandchildren_ff + "'"):("'" + "30" + "'")%>   tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%--                                                                Disable--%>
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_DISABLED, loginDTO)%>
                                                        <span class="required"> * </span>

                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='number' class='form-control' name='maxAgeDisable'
                                                               id='maxAgeDisable_text_<%=i%>'
                                                               value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.max_age_disable + "'"):("'" + "32" + "'")%>   tag='pb_html'/>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_CANCELED_DISTRICTS, loginDTO)%>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <select multiple="multiple" class='form-control' name='district'
                                                                id='district_select' tag='pb_html'>
                                                            <%--								<option value='-1'><%=Language.equals("English")?"Select":"বাছাই করুন"%> </option>--%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-5">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_DATES, loginDTO)%>

                                            </h4>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAGECALCULATIONDATE, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='hidden' class='form-control ' readonly="readonly"
                                                               data-label="Document Date"
                                                               id='lastAgeCalculationDate_date_<%=i%>'
                                                               name='lastAgeCalculationDate'
                                                               value='' tag='pb_html'>

                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="date-of-last-age-js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                        </jsp:include>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_ONLINEJOBPOSTINGDATE, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='hidden' class='form-control' readonly="readonly"
                                                               data-label="Document Date"
                                                               id='onlineJobPostingDate_date_<%=i%>'
                                                               name='onlineJobPostingDate'
                                                               value='' tag='pb_html'>

                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="date-of-online-posting-js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                        </jsp:include>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FIRST_APPLICATION_DATE, loginDTO)%>
                                                        <span class="required"> * </span>


                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='hidden' class='form-control' readonly="readonly"
                                                               data-label="Document Date"
                                                               id='startApplicationDate_date_<%=i%>'
                                                               name='startApplicationDate' value='' tag='pb_html'>

                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="date-of-start-app-js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                            <jsp:param name="END_YEAR" value="<%=year + 1%>"/>
                                                        </jsp:include>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAPPLICATIONDATE, loginDTO)%>
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type='hidden' class='form-control' readonly="readonly"
                                                               data-label="Document Date"
                                                               id='lastApplicationDate_date_<%=i%>'
                                                               name='lastApplicationDate' value='' tag='pb_html'>

                                                        <jsp:include page="/date/date.jsp">
                                                            <jsp:param name="DATE_ID"
                                                                       value="date-of-end-app-js"></jsp:param>
                                                            <jsp:param name="LANGUAGE"
                                                                       value="<%=Language%>"></jsp:param>
                                                            <jsp:param name="END_YEAR" value="<%=year + 1%>"/>
                                                        </jsp:include>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-5">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_DESCRIPTION, loginDTO)%>

                                            </h4>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBPURPOSE, loginDTO)%>

                                                    </label>
                                                    <div class="col-md-8">
                                                                    <textarea class='form-control' name='jobPurpose'
                                                                              id='jobPurpose_text_<%=i%>'
                                                                              tag='pb_html'><%=actionName.equals("edit") ? (recruitment_job_descriptionDTO.jobPurpose) : ("")%>
    </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBRESPONSIBILITIES, loginDTO)%>

                                                    </label>
                                                    <div class="col-md-8">
                                                                    <textarea class='form-control'
                                                                              name='jobResponsibilities'
                                                                              id='jobResponsibilities_text_<%=i%>'
                                                                              tag='pb_html'><%=actionName.equals("edit") ? (recruitment_job_descriptionDTO.jobResponsibilities) : ("")%>
    </textarea>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_PERSONALCHARACTERISTICS, loginDTO)%>

                                                    </label>
                                                    <div class="col-md-8">
                                                                <textarea class='form-control'
                                                                          name='personalCharacteristics'
                                                                          id='personalCharacteristics_text_<%=i%>'
                                                                          tag='pb_html'><%=actionName.equals("edit") ? (recruitment_job_descriptionDTO.personalCharacteristics) : ("")%></textarea>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCOMPETENCE, loginDTO)%>

                                                    </label>
                                                    <div class="col-md-8">
                                                                <textarea class='form-control' name='requiredCompetence'
                                                                          id='requiredCompetence_text_<%=i%>'
                                                                          tag='pb_html'><%=actionName.equals("edit") ? (recruitment_job_descriptionDTO.requiredCompetence) : ("")%></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-md-4 col-form-label text-left text-md-right">
                                                        <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FILESDROPZONE, loginDTO)%>

                                                    </label>
                                                    <div class="col-md-8">
                                                        <%
                                                            if (actionName.equals("edit")) {
                                                                List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(recruitment_job_descriptionDTO.filesDropzone);
                                                        %>
                                                        <table>
                                                            <tr>
                                                                <%
                                                                    if (filesDropzoneDTOList != null) {
                                                                        for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                            FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                            byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                                %>
                                                                <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                                    <%
                                                                        if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                                    %>
                                                                    <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)
                    %>' style='width:100px'/>
                                                                    <%
                                                                        }
                                                                    %>
                                                                    <a href='Recruitment_job_descriptionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                                       download><%=filesDTO.fileTitle%>
                                                                    </a>
                                                                    <a class='btn btn-danger'
                                                                       onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>
                                                                </td>
                                                                <%
                                                                        }
                                                                    }
                                                                %>
                                                            </tr>
                                                        </table>
                                                        <%
                                                            }
                                                        %>

                                                        <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                                        <div class="dropzone"
                                                             action="Recruitment_job_descriptionServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?recruitment_job_descriptionDTO.filesDropzone:ColumnID%>">
                                                            <input type='file' style="display:none"
                                                                   name='filesDropzoneFile'
                                                                   id='filesDropzone_dropzone_File_<%=i%>'
                                                                   tag='pb_html'/>
                                                        </div>
                                                        <input type='hidden' name='filesDropzoneFilesToDelete'
                                                               id='filesDropzoneFilesToDelete_<%=i%>' value=''
                                                               tag='pb_html'/>
                                                        <input type='hidden' name='filesDropzone'
                                                               id='filesDropzone_dropzone_<%=i%>' tag='pb_html'
                                                               value='<%=actionName.equals("edit")?recruitment_job_descriptionDTO.filesDropzone:ColumnID%>'/>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-5">
                        <div class="onlyborder">
                            <div class="row mx-2">
                                <div class="col-md-12">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white">
                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_REQUIRED_FILES_AND_QUALIFICATIONS, loginDTO)%>

                                            </h4>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <h5 class="text-left content_legend table-title">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES, loginDTO)%>
                                                </h5>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped text-nowrap">
                                                        <thead class="thead-light">
                                                        <tr>
                                                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_RECRUITMENTJOBREQUIREDFILESTYPE, loginDTO)%>
                                                            </th>
                                                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_ISMANDATORYFILES, loginDTO)%>
                                                            </th>
                                                            <%--										<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_MODIFIEDBY, loginDTO)%></th>--%>
                                                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_REMOVE, loginDTO)%>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="field-RecruitmentJobSpecificFiles">


                                                        <%
                                                            if (actionName.equals("edit")) {
                                                                int index = -1;
                                                                List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOs = RecruitmentJobSpecificFilesRepository.getInstance().
                                                                        getRecruitmentJobSpecificFilesDTOByJobDescriptionId(recruitment_job_descriptionDTO.iD);


                                                                for (RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO : recruitmentJobSpecificFilesDTOs) {
                                                                    index++;

                                                                    System.out.println("index index = " + index);

                                                        %>

                                                        <tr id="RecruitmentJobSpecificFiles_<%=index + 1%>">
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificFiles.iD'
                                                                       id='iD_hidden_<%=childTableStartingID%>'
                                                                       value='<%=recruitmentJobSpecificFilesDTO.iD%>'
                                                                       tag='pb_html'/>

                                                            </td>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificFiles.recruitmentJobDescriptionId'
                                                                       id='recruitmentJobDescriptionId_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                            </td>
                                                            <td style="min-width: 20rem">


                                                                <select class='form-control'
                                                                        name='recruitmentJobSpecificFiles.recruitmentJobRequiredFilesType'
                                                                        id='recruitmentJobRequiredFilesType_select_<%=childTableStartingID%>'
                                                                        tag='pb_html'>
                                                                    <%
                                                                        Options = Recruitment_job_required_filesRepository.getInstance().
                                                                                getBuildOptions(Language, recruitmentJobSpecificFilesDTO.recruitmentJobRequiredFilesType + "");
//                                                                        if (actionName.equals("edit")) {
//                                                                            Options = CommonDAO.getOptions(Language, "select", "recruitment_job_required_files", "recruitmentJobRequiredFilesType_select_" + i, "form-control", "recruitmentJobRequiredFilesType", recruitmentJobSpecificFilesDTO.recruitmentJobRequiredFilesType + "");
//                                                                        } else {
//                                                                            Options = CommonDAO.getOptions(Language, "select", "recruitment_job_required_files", "recruitmentJobRequiredFilesType_select_" + i, "form-control", "recruitmentJobRequiredFilesType");
//                                                                        }
                                                                    %>
                                                                    <%=Options%>
                                                                </select>

                                                            </td>
                                                            <td>


                                                                <input type='checkbox' class=''
                                                                       name='recruitmentJobSpecificFiles.isMandatoryFilesCheckbox'
                                                                       id='isMandatoryFiles_checkbox_<%=childTableStartingID%>'
                                                                       value='true' <%=(actionName.equals("edit") && String.valueOf(recruitmentJobSpecificFilesDTO.isMandatoryFiles).equals("true"))?("checked"):""%>
                                                                       tag='pb_html'><br>
                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificFiles.isMandatoryFiles'
                                                                       id='isMandatoryFiles_checkbox_hidden_<%=childTableStartingID%>'
                                                                       value='<%=String.valueOf(recruitmentJobSpecificFilesDTO.isMandatoryFiles)%>'
                                                                       tag='pb_html'><br>

                                                            </td>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificFiles.insertionDate'
                                                                       id='insertionDate_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                            </td>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificFiles.insertedByUserId'
                                                                       id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                            </td>
                                                            <%--										<td>										--%>


                                                            <%--		<input type='text' class='form-control'  name='recruitmentJobSpecificFiles.modifiedBy' id = 'modifiedBy_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.modifiedBy + "'"):("'" + "" + "'")%>   tag='pb_html'/>					--%>
                                                            <%--										</td>--%>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificFiles.isDeleted'
                                                                       id='isDeleted_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                                            </td>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificFiles.lastModificationTime'
                                                                       id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                            </td>
                                                            <td>
                                                                <div class='checker'><span id='chkEdit'><input
                                                                        type='checkbox'
                                                                        id='recruitmentJobSpecificFiles_cb_<%=index%>'
                                                                        name='checkbox' value=''/></span></div>
                                                            </td>
                                                        </tr>
                                                        <%
                                                                    childTableStartingID++;
                                                                }
                                                            }
                                                        %>

                                                        </tbody>
                                                    </table>


                                                </div>
                                                <div class="form-group">
                                                    <div class="row mt-3">
                                                        <div class="col-md-6"></div>

                                                        <div align="center" class="col-md-6 text-right">


                                                            <button id="add-more-RecruitmentJobSpecificFiles"
                                                                    name="add-moreRecruitmentJobSpecificFiles"
                                                                    type="button"
                                                                    class="btn btn-sm text-white add-btn shadow">
                                                                <i class="fa fa-plus"></i>

                                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_ADD_MORE, loginDTO)%>
                                                            </button>

                                                            <button id="remove-RecruitmentJobSpecificFiles"
                                                                    name="removeRecruitmentJobSpecificFiles"
                                                                    type="button"
                                                                    class="btn btn-sm remove-btn shadow ml-2">
                                                                <i class="fa fa-trash"></i>
                                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_REMOVE, loginDTO)%>
                                                            </button>

                                                        </div>

                                                    </div>

                                                </div>

                                                <%RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO = new RecruitmentJobSpecificFilesDTO();%>

                                                <template id="template-RecruitmentJobSpecificFiles">
                                                    <tr>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificFiles.iD' id='iD_hidden_'
                                                                   value='<%=recruitmentJobSpecificFilesDTO.iD%>'
                                                                   tag='pb_html'/>

                                                        </td>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificFiles.recruitmentJobDescriptionId'
                                                                   id='recruitmentJobDescriptionId_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.recruitmentJobDescriptionId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                        </td>
                                                        <td style="min-width: 20rem">


                                                            <select class='form-control'
                                                                    name='recruitmentJobSpecificFiles.recruitmentJobRequiredFilesType'
                                                                    id='recruitmentJobRequiredFilesType_select_'
                                                                    tag='pb_html'>
                                                                <%
                                                                    Options = Recruitment_job_required_filesRepository.getInstance().
                                                                            getBuildOptions(Language, recruitmentJobSpecificFilesDTO.recruitmentJobRequiredFilesType + "");
//                                                                    if (actionName.equals("edit")) {
//                                                                        Options = CommonDAO.getOptions(Language, "select", "recruitment_job_required_files", "recruitmentJobRequiredFilesType_select_" + i, "form-control", "recruitmentJobRequiredFilesType", recruitmentJobSpecificFilesDTO.recruitmentJobRequiredFilesType + "");
//                                                                    } else {
//                                                                        Options = CommonDAO.getOptions(Language, "select", "recruitment_job_required_files", "recruitmentJobRequiredFilesType_select_" + i, "form-control", "recruitmentJobRequiredFilesType");
//                                                                    }
                                                                %>
                                                                <%=Options%>
                                                            </select>

                                                        </td>
                                                        <td>


                                                            <input type='checkbox' class=''
                                                                   name='recruitmentJobSpecificFiles.isMandatoryFilesCheckbox'
                                                                   id='isMandatoryFiles_checkbox_' value='true'
                                                                   tag='pb_html'><br>
                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificFiles.isMandatoryFiles'
                                                                   id='isMandatoryFiles_checkbox_hidden_' value='true'
                                                                   tag='pb_html'><br>


                                                        </td>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificFiles.insertionDate'
                                                                   id='insertionDate_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                        </td>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificFiles.insertedByUserId'
                                                                   id='insertedByUserId_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                        </td>
                                                        <%--									<td>--%>


                                                        <%--		<input type='text' class='form-control'  name='recruitmentJobSpecificFiles.modifiedBy' id = 'modifiedBy_text_' value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.modifiedBy + "'"):("'" + "" + "'")%>   tag='pb_html'/>					--%>
                                                        <%--									</td>--%>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificFiles.isDeleted'
                                                                   id='isDeleted_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                                        </td>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificFiles.lastModificationTime'
                                                                   id='lastModificationTime_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificFilesDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                        </td>
                                                        <td>
                                                            <div><span id='chkEdit'><input type='checkbox'
                                                                                           name='checkbox'
                                                                                           value=''/></span></div>
                                                        </td>
                                                    </tr>

                                                </template>
                                            </div>


                                            <div class="col-md-12">
                                                <h5 class="text-left content_legend table-title">
                                                    <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS, loginDTO)%>
                                                </h5>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped text-nowrap">
                                                        <thead class="thead-light">
                                                        <tr>
                                                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_RECRUITMENTJOBREQTRAINSANDCERTSTYPE, loginDTO)%>
                                                            </th>
                                                            <th>
                                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_ISMANDATORYCERTS, loginDTO)%>
                                                            </th>
                                                            <th>
                                                                <%=LM.getText(LC.JOB_APPLICANT_DOCUMENTS_FILE_REQUIRED, loginDTO)%>
                                                            </th>
                                                            <%--										<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_MODIFIEDBY, loginDTO)%></th>--%>
                                                            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_REMOVE, loginDTO)%>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="field-RecruitmentJobSpecificCerts">


                                                        <%
                                                            if (actionName.equals("edit")) {
                                                                int index = -1;
                                                                List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOs = RecruitmentJobSpecificCertsRepository.getInstance()
                                                                        .getRecruitmentJobSpecificCertsDTOByJobDescriptionId(recruitment_job_descriptionDTO.iD);

                                                                for (RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO : recruitmentJobSpecificCertsDTOs) {
                                                                    index++;

                                                                    System.out.println("index index = " + index);

                                                        %>

                                                        <tr id="RecruitmentJobSpecificCerts_<%=index + 1%>">
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificCerts.iD'
                                                                       id='iD_hidden_<%=childTableStartingID%>'
                                                                       value='<%=recruitmentJobSpecificCertsDTO.iD%>'
                                                                       tag='pb_html'/>

                                                            </td>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificCerts.recruitmentJobDescriptionId'
                                                                       id='recruitmentJobDescriptionId_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                            </td>
                                                            <td style="min-width: 25rem">


                                                                <select class='form-control'
                                                                        name='recruitmentJobSpecificCerts.recruitmentJobReqTrainsAndCertsType'
                                                                        id='recruitmentJobReqTrainsAndCertsType_select_<%=childTableStartingID%>'
                                                                        tag='pb_html'>
                                                                    <%
                                                                        Options = Recruitment_job_req_trains_and_certsRepository.getInstance().getBuildOptions
                                                                                (Language, recruitmentJobSpecificCertsDTO.recruitmentJobReqTrainsAndCertsType + "");
//                                                                        if (actionName.equals("edit")) {
//                                                                            Options = CommonDAO.getOptions(Language, "select", "recruitment_job_req_trains_and_certs", "recruitmentJobReqTrainsAndCertsType_select_" + i, "form-control", "recruitmentJobReqTrainsAndCertsType", recruitmentJobSpecificCertsDTO.recruitmentJobReqTrainsAndCertsType + "");
//                                                                        } else {
//                                                                            Options = CommonDAO.getOptions(Language, "select", "recruitment_job_req_trains_and_certs", "recruitmentJobReqTrainsAndCertsType_select_" + i, "form-control", "recruitmentJobReqTrainsAndCertsType");
//                                                                        }
                                                                    %>
                                                                    <%=Options%>
                                                                </select>

                                                            </td>
                                                            <td>

                                                                <input type='checkbox' class=''
                                                                       name='recruitmentJobSpecificCerts.isMandatoryCertsCheckbox'
                                                                       id='isMandatoryCerts_checkbox_<%=childTableStartingID%>'
                                                                       value='true' <%=(actionName.equals("edit") &&
            String.valueOf(recruitmentJobSpecificCertsDTO.isMandatoryCerts).equals("true"))?("checked"):""%>
                                                                ><br>
                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificCerts.isMandatoryCerts'
                                                                       id='isMandatoryCerts_checkbox_hidden_<%=childTableStartingID%>'
                                                                       value='<%=String.valueOf(recruitmentJobSpecificCertsDTO.isMandatoryCerts)%>'
                                                                       tag='pb_html'>
                                                                <br>

                                                            </td>

                                                            <td>

                                                                <input type='checkbox' class=''
                                                                       name='recruitmentJobSpecificCerts.file_requiredCheckbox'
                                                                       id='file_required_checkbox_<%=childTableStartingID%>'
                                                                       value='true' <%=(actionName.equals("edit") &&
            String.valueOf(recruitmentJobSpecificCertsDTO.file_required).equals("true"))?("checked"):""%>
                                                                ><br>
                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificCerts.file_required'
                                                                       id='file_required_checkbox_hidden_<%=childTableStartingID%>'
                                                                       value='<%=String.valueOf(recruitmentJobSpecificCertsDTO.file_required)%>'
                                                                       tag='pb_html'>
                                                                <br>

                                                            </td>


                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificCerts.insertionDate'
                                                                       id='insertionDate_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                            </td>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificCerts.insertedByUserId'
                                                                       id='insertedByUserId_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                            </td>
                                                            <%--										<td>										--%>


                                                            <%--		<input type='text' class='form-control'  name='recruitmentJobSpecificCerts.modifiedBy' id = 'modifiedBy_text_<%=childTableStartingID%>' value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.modifiedBy + "'"):("'" + "" + "'")%>   tag='pb_html'/>					--%>
                                                            <%--										</td>--%>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificCerts.isDeleted'
                                                                       id='isDeleted_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                                            </td>
                                                            <td style="display: none;">


                                                                <input type='hidden' class='form-control'
                                                                       name='recruitmentJobSpecificCerts.lastModificationTime'
                                                                       id='lastModificationTime_hidden_<%=childTableStartingID%>'
                                                                       value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                            </td>
                                                            <td>
                                                                <div class='checker'><span id='chkEdit'><input
                                                                        type='checkbox'
                                                                        id='recruitmentJobSpecificCerts_cb_<%=index%>'
                                                                        name='checkbox' value=''/></span></div>
                                                            </td>
                                                        </tr>
                                                        <%
                                                                    childTableStartingID++;
                                                                }
                                                            }
                                                        %>

                                                        </tbody>
                                                    </table>


                                                </div>
                                                <div class="form-group">

                                                    <div class="row mt-3">
                                                        <div class="col-md-6"></div>
                                                        <div class="col-md-6 text-right">


                                                            <button id="add-more-RecruitmentJobSpecificCerts"
                                                                    name="add-moreRecruitmentJobSpecificCerts"
                                                                    type="button"
                                                                    class="btn btn-sm text-white add-btn shadow">
                                                                <i class="fa fa-plus"></i>
                                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_ADD_MORE, loginDTO)%>
                                                            </button>

                                                            <button id="remove-RecruitmentJobSpecificCerts"
                                                                    name="removeRecruitmentJobSpecificCerts"
                                                                    type="button"
                                                                    class="btn btn-sm remove-btn shadow ml-2">
                                                                <i class="fa fa-trash"></i>
                                                                <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_REMOVE, loginDTO)%>
                                                            </button>

                                                        </div>
                                                    </div>

                                                </div>

                                                <%RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO = new RecruitmentJobSpecificCertsDTO();%>

                                                <template id="template-RecruitmentJobSpecificCerts">
                                                    <tr>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificCerts.iD' id='iD_hidden_'
                                                                   value='<%=recruitmentJobSpecificCertsDTO.iD%>'
                                                                   tag='pb_html'/>

                                                        </td>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificCerts.recruitmentJobDescriptionId'
                                                                   id='recruitmentJobDescriptionId_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.recruitmentJobDescriptionId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                        </td>
                                                        <td style="min-width: 25rem">


                                                            <select class='form-control'
                                                                    name='recruitmentJobSpecificCerts.recruitmentJobReqTrainsAndCertsType'
                                                                    id='recruitmentJobReqTrainsAndCertsType_select_'
                                                                    tag='pb_html'>
                                                                <%
                                                                    Options = Recruitment_job_req_trains_and_certsRepository.getInstance().getBuildOptions
                                                                            (Language, recruitmentJobSpecificCertsDTO.recruitmentJobReqTrainsAndCertsType + "");
//                                                                    if (actionName.equals("edit")) {
//                                                                        Options = CommonDAO.getOptions(Language, "select", "recruitment_job_req_trains_and_certs", "recruitmentJobReqTrainsAndCertsType_select_" + i, "form-control", "recruitmentJobReqTrainsAndCertsType", recruitmentJobSpecificCertsDTO.recruitmentJobReqTrainsAndCertsType + "");
//                                                                    } else {
//                                                                        Options = CommonDAO.getOptions(Language, "select", "recruitment_job_req_trains_and_certs", "recruitmentJobReqTrainsAndCertsType_select_" + i, "form-control", "recruitmentJobReqTrainsAndCertsType");
//                                                                    }
                                                                %>
                                                                <%=Options%>
                                                            </select>

                                                        </td>
                                                        <td>

                                                            <input type='checkbox' class=''
                                                                   name='recruitmentJobSpecificCerts.isMandatoryCertsCheckbox'
                                                                   id='isMandatoryCerts_checkbox_' value='true' <%=(actionName.equals("edit")
           && String.valueOf(recruitmentJobSpecificCertsDTO.isMandatoryCerts).equals("true"))?("checked"):""%>
                                                                   tag='pb_html'
                                                            ><br>
                                                            <input type='hidden' class=''
                                                                   name='recruitmentJobSpecificCerts.isMandatoryCerts'
                                                                   id='isMandatoryCerts_checkbox_hidden_'
                                                                   tag='pb_html'
                                                                   value='true'><br>

                                                        </td>


                                                        <td>

                                                            <input type='checkbox' class=''
                                                                   name='recruitmentJobSpecificCerts.file_requiredCheckbox'
                                                                   id='file_required_checkbox_' value='true' <%=(actionName.equals("edit")
            && String.valueOf(recruitmentJobSpecificCertsDTO.file_required).equals("true"))?("checked"):""%>
                                                                   tag='pb_html'><br>
                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificCerts.file_required'
                                                                   id='file_required_checkbox_hidden_' value='true'
                                                                   tag='pb_html'><br>

                                                        </td>


                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificCerts.insertionDate'
                                                                   id='insertionDate_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.insertionDate + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                        </td>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificCerts.insertedByUserId'
                                                                   id='insertedByUserId_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.insertedByUserId + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                        </td>
                                                        <%--									<td>--%>


                                                        <%--		<input type='text' class='form-control'  name='recruitmentJobSpecificCerts.modifiedBy' id = 'modifiedBy_text_' value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.modifiedBy + "'"):("'" + "" + "'")%>   tag='pb_html'/>					--%>
                                                        <%--									</td>--%>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificCerts.isDeleted'
                                                                   id='isDeleted_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>

                                                        </td>
                                                        <td style="display: none;">


                                                            <input type='hidden' class='form-control'
                                                                   name='recruitmentJobSpecificCerts.lastModificationTime'
                                                                   id='lastModificationTime_hidden_'
                                                                   value=<%=actionName.equals("edit")?("'" + recruitmentJobSpecificCertsDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                                                        </td>
                                                        <td>
                                                            <div><span id='chkEdit'><input type='checkbox'
                                                                                           name='checkbox'
                                                                                           value=''/></span></div>
                                                        </td>
                                                    </tr>

                                                </template>
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 form-actions mt-3 text-right">
                        <a id="cancel-btn" class="btn btn-sm cancel-btn text-white shadow"
                           href="<%=request.getHeader("referer")%>">
                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_CANCEL_BUTTON, loginDTO)%>
                        </a>
                        <button id="submit-btn" class="btn btn-sm submit-btn text-white shadow ml-2" type="submit">
                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


