
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>

<%@page import="recruitment_job_description.*"%>
<%@page import="java.util.*"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>

<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="user.*"%>

<%@page import="workflow.*"%>
<%@page import="util.TimeFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
Recruitment_job_descriptionDTO recruitment_job_descriptionDTO;
recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO)request.getAttribute("recruitment_job_descriptionDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
if(recruitment_job_descriptionDTO == null)
{
	recruitment_job_descriptionDTO = new Recruitment_job_descriptionDTO();
	
}
System.out.println("recruitment_job_descriptionDTO = " + recruitment_job_descriptionDTO);

String actionName;
System.out.println("actionType = " + request.getParameter("actionType"));
if (request.getParameter("actionType").equalsIgnoreCase("getAddPage"))
{
	actionName = "add";
}
else
{
	actionName = "edit";
}
String formTitle = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_ADD_FORMNAME, loginDTO);


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = 0;

String value = "";

int childTableStartingID = 1;

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
boolean isPermanentTable = true;
if(request.getParameter("isPermanentTable") != null)
{
	isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));
}

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

String tableName = "recruitment_job_description";
long currentTime = new Date().getTime();

boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;

if(!isPermanentTable)
{
	approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("recruitment_job_description", recruitment_job_descriptionDTO.iD);
	System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
	approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
	approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByUpdatedRowId("recruitment_job_description", recruitment_job_descriptionDTO.iD);
	if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
	{
		canApprove = true;
		if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
		{
			canValidate = true;
		}
	}
	
	isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
	
	canTerminate = isInitiator && recruitment_job_descriptionDTO.isDeleted == 2;



}

	String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
	String Options;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	Date date = new Date();
	String datestr = dateFormat.format(date);
	CommonDAO.language = Language;
	CatDAO.language = Language;
	int year = Calendar.getInstance().get(Calendar.YEAR);
%>


<%@include file="recruitment_job_descriptionEditBodyForm.jsp" %>

<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

	let selectedDistrict;
	let selectedDistrictArr;

function buttonStateChange(value) {
	$('#submit-btn').prop('disabled', value);
	$('#cancel-btn').prop('disabled', value);
}

function formSubmit(){
	event.preventDefault();
	let form = $("#job-form");
	form.validate();
	let valid = form.valid() && PreprocessBeforeSubmiting();
	if(valid){
		buttonStateChange(true);
		let url = form.attr('action');
		$.ajax({
			type: "POST",
			url: url,
			data: form.serialize(),
			dataType: 'JSON',
			success: function (response) {
				if (response.responseCode === 0) {
					$('#toast_message').css('background-color', '#ff6063');
					showToastSticky(response.msg, response.msg);
					buttonStateChange(false);
				} else if (response.responseCode === 200) {
					window.location.replace(getContextPath() + response.msg);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				toastr.error("Error Code: " + jqXHR.status + ", Type:" + textStatus
						+ ", Message: " + errorThrown);
				buttonStateChange(false);
			}
		});
	}
	else{
		$("#job-form").find(":input.is-invalid:first").focus();
	}
}

var allowedKeys = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
	"ARROWRIGHT", "ARROWLEFT", "TAB", "BACKSPACE", "DELETE", "HOME", "END", "০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯"
];
$(document).ready( function(){

    dateTimeInit("<%=Language%>");
	$.validator.addMethod('educationLevelSelector',function(value,element){
		return value!=0;
	});

	$.validator.addMethod('payScaleSelector',function(value,element){
		return value!=0;
	});

	$.validator.addMethod('greaterThanZero',function(value,element){
		return value > 0;
	});


	$.validator.addMethod('greaterThanMinAge',function(value,element){
		let el = $('#minAge_text_0');
		if(el.val().length > 0){
			let minAge = el.val() / 1;
			if(value.length > 0){
				value = value /1;
				return value >= minAge;
			}
		}
		return false;
	});

	let errMsgJobTitleEn = '<%=Language.equalsIgnoreCase("english")? "Please enter jobTitle in English" : " পদের নাম ইংরেজিতে লিখুন "%>';
	let errMsgJobTitleBn = '<%=Language.equalsIgnoreCase("english")? "Please enter jobTitle in Bangla" : " পদের নাম বাংলায় লিখুন  "%>';
	let errMsgGrade = '<%=Language.equalsIgnoreCase("english")? "Please select Grade" : " গ্রেড নির্বাচন করুন "%>';
	let errMsgEmploymentStatusCat = '<%=Language.equalsIgnoreCase("english")? "Please select employment Status" : " ধরণ নির্বাচন করুন  "%>';
	let errMsgEmployeePayScaleType = '<%=Language.equalsIgnoreCase("english")? "Please select PayScale Type" : " বেতন স্কেল নির্বাচন করুন  "%>';
	let errMsgNumberOfVacancy = '<%=Language.equalsIgnoreCase("english")? "Please enter number of Vacancy Greater Than Zero" : " পদের সংখ্যা লিখুন ( শূন্য থেকে বড় )  "%>';
	let errMsgMinAge = '<%=Language.equalsIgnoreCase("english")? "Please enter Min Age on Boundary Date, Greater Than Zero" : " নূন্যতম বয়স লিখুন ( শূন্য থেকে বড় )  "%>';
	let errMsgMaxAgeOnBoundaryDateRegular = '<%=Language.equalsIgnoreCase("english")? "Please enter Max Age on Boundary Date For Regular, Greater Than Zero, Greater than Min age" :
	 "সর্বোচ্চ বয়স লিখুন ( শূন্য থেকে বড়, নূন্যতম বয়স থেকে বড়  )  "%>';
	let errMsgMaxAgeOnBoundaryDateFf = '<%=Language.equalsIgnoreCase("english")? "Please enter Max Age on Boundary Date For Children of Freedom Fighter, Greater Than Zero, Greater than Min age" :
	 " সর্বোচ্চ বয়স ( মুক্তিযোদ্ধার সন্তান) লিখুন ( শূন্য থেকে বড়, নূন্যতম বয়স থেকে বড়  ) "%>';
	<%--let errMsgLastApplicationDate = '<%=Language.equalsIgnoreCase("english")? "Please select last Application date" : " আবেদনের শেষ তারিখ  নির্বাচন করুন  "%>';--%>
	let errMsgEducationLevelType = '<%=Language.equalsIgnoreCase("english")? "Please select Education Level" : " নূন্যতম শিক্ষাগত যোগ্যতা নির্বাচন করুন  "%>';
	let errMsgMaxAgeOnBoundaryDateGrandChildrenFf = '<%=Language.equalsIgnoreCase("english")? "Please enter Max Age on Boundary Date For Grand Children of Freedom Fighter, Greater Than Zero, Greater than Min age":
	" সর্বোচ্চ বয়স (মুক্তিযোদ্ধার নাতিনাতনি) লিখুন ( শূন্য থেকে বড়, নূন্যতম বয়স থেকে বড়  ) "%>';

	let errMsgDisable = '<%=Language.equalsIgnoreCase("english")? "Please enter Max Age on Boundary Date For Disabled, Greater Than Zero, Greater than Min age":
	" সর্বোচ্চ বয়স (প্রতিবন্ধী) লিখুন ( শূন্য থেকে বড়, নূন্যতম বয়স থেকে বড়  ) "%>';

	let errMsgEducationLevelTypeYearly = '<%=Language.equalsIgnoreCase("english")? "Please select Education Level For Yearly staff" : " নূন্যতম শিক্ষাগত যোগ্যতা ( সাংবাৎসরিক কর্মকর্তা ) নির্বাচন করুন  "%>';


	$("#job-form").validate({
		errorClass: 'error is-invalid',
		validClass: 'is-valid',
		rules: {
			jobTitleEn: "required",
			jobTitleBn: "required",
			jobGradeCat: "required",
			employmentStatusCat: "required",
			employeePayScaleType: {
				required: true,
				payScaleSelector: true,
			},
			numberOfVacancy: {
				required: true,
				greaterThanZero: true,
			},
			maxAgeOnBoundaryDateRegular: {
				required: true,
				greaterThanZero: true,
				greaterThanMinAge: true,
			},
			maxAgeOnBoundaryDateFf: {
				required: true,
				greaterThanZero: true,
				greaterThanMinAge: true,
			},
			maxAgeOnBoundaryDateGrandChildrenFf: {
				required: true,
				greaterThanZero: true,
				greaterThanMinAge: true,
			},
			minAge: {
				required: true,
				greaterThanZero: true,
			},
			maxAgeDisable: {
				required: true,
				greaterThanZero: true,
			},

			// lastApplicationDate: "required",
			educationLevelType: {
				required: true,
				educationLevelSelector: true,
			},

			yearlyEducationLevelType: {
				required: true,
				educationLevelSelector: true,
			}

			//

		},
		messages: {
			jobTitleEn: errMsgJobTitleEn,
			jobTitleBn: errMsgJobTitleBn,
			jobGradeCat: errMsgGrade,
			employmentStatusCat: errMsgEmploymentStatusCat,
			employeePayScaleType: errMsgEmployeePayScaleType,
			numberOfVacancy: errMsgNumberOfVacancy,
			minAge: errMsgMinAge,
			maxAgeOnBoundaryDateRegular: errMsgMaxAgeOnBoundaryDateRegular,
			maxAgeOnBoundaryDateFf: errMsgMaxAgeOnBoundaryDateFf,
			// lastApplicationDate: errMsgLastApplicationDate,
			educationLevelType: errMsgEducationLevelType,
			maxAgeOnBoundaryDateGrandChildrenFf: errMsgMaxAgeOnBoundaryDateGrandChildrenFf,
			maxAgeDisable: errMsgDisable,

			yearlyEducationLevelType: errMsgEducationLevelTypeYearly
		}
	});

	initialization();

	$(".englishOnly").keypress(function(event){
		var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
		if (allowed.includes(event.key))return true;
		var ew = event.which;
		if(ew == 32)
			return true;
		if(48 <= ew && ew <= 57)
			return true;
		if(65 <= ew && ew <= 90)
			return true;
		if(97 <= ew && ew <= 122)
			return true;
		return false;
	});
	$(".noEnglish").keypress(function(event){
		var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
		if (allowed.includes(event.key))return true;
		var ew = event.which;

		if(48 <= ew && ew <= 57)
			return false;
		if(65 <= ew && ew <= 90)
			return false;
		if(97 <= ew && ew <= 122)
			return false;
		return true;
	});
	function onInputPasteNoEnglish(event) {
		var clipboardData = event.clipboardData || window.clipboardData;
		var data = clipboardData.getData("Text");
		var valid = data.toString().split('').every(char => {
			var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
			if (allowed.includes(char)) return true;
			if ('0' <= char && char <= '9')
				return false;
			if ('a' <= char && char <= 'z')
				return false;
			if ('A' <= char && char <= 'Z')
				return false;
			return true;
		});
		if (valid) return;
		event.stopPropagation();
		event.preventDefault();
	}

	function onInputPasteEnglishOnly(event) {
		var clipboardData = event.clipboardData || window.clipboardData;
		var data = clipboardData.getData("Text");
		var valid = data.toString().split('').every(char => {
			var allowed = "~`!@#$%^&*()_-+=|\'\"\\|:?/;<>,.}]{[";
			if (allowed.includes(char)) return true;
			if (char.toString().trim() == '')
				return true;
			if ('0' <= char && char <= '9')
				return true;
			if ('a' <= char && char <= 'z')
				return true;
			if ('A' <= char && char <= 'Z')
				return true;
			return false;
		});
		if (valid) return;
		event.stopPropagation();
		event.preventDefault();
	}
	document.querySelectorAll("input.noPaste").forEach(function (input) {
		//input.addEventListener("keydown", onInputKeyDown);
		input.addEventListener("paste", onInputPaste);
	});

	var elements = document.getElementsByClassName("englishOnly");

	for (var i = 0; i < elements.length; i++) {
		elements[i].addEventListener('paste', onInputPasteEnglishOnly);
	}

	elements = document.getElementsByClassName("noEnglish");

	for (var i = 0; i < elements.length; i++) {
		elements[i].addEventListener('paste', onInputPasteNoEnglish);
	}
	function onInputPaste(event) {
		var clipboardData = event.clipboardData || window.clipboardData;
		if (/^[0-9০-৯]+$/g.test(clipboardData.getData("Text"))) return;
		event.stopPropagation();
		event.preventDefault();
	}
	function onInputKeyDown(event) {
		if (!allowedKeys.includes(event.key.toUpperCase()) && !event.ctrlKey) {
			event.preventDefault();
		}
	}

	$('.jobGradeClass option').each(function() {
		if ( +($(this).val()) >= 1 && +($(this).val()) <= 10) {
			$(this).remove();
		}
	});
});

function PreprocessBeforeSubmiting()
{
	let form = $("#job-form");
	form.validate();
	processMultipleSelectBoxBeforeSubmit("district");

	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	}


	for(i = 1; i < child_table_extra_id; i ++)
	{
		if(document.getElementById("isMandatoryFiles_checkbox_" + i))
		{
			if(document.getElementById("isMandatoryFiles_checkbox_" + i).getAttribute("processed") == null)
			{
				preprocessCheckBoxBeforeSubmitting('isMandatoryFiles', i);
				document.getElementById("isMandatoryFiles_checkbox_hidden_" + i).value = document.getElementById("isMandatoryFiles_checkbox_" + i).value;
				document.getElementById("isMandatoryFiles_checkbox_" + i).setAttribute("processed","1");
			}
		}
	}
	for(i = 1; i < child_table_extra_id; i ++)
	{
		if(document.getElementById("isMandatoryCerts_checkbox_" + i))
		{
			if(document.getElementById("isMandatoryCerts_checkbox_" + i).getAttribute("processed") == null)
			{
				preprocessCheckBoxBeforeSubmitting('isMandatoryCerts', i);
				document.getElementById("isMandatoryCerts_checkbox_hidden_" + i).value  = document.getElementById("isMandatoryCerts_checkbox_" + i).value;
				document.getElementById("isMandatoryCerts_checkbox_" + i).setAttribute("processed","1");
			}
		}

		if(document.getElementById("file_required_checkbox_" + i))
		{
			if(document.getElementById("file_required_checkbox_" + i).getAttribute("processed") == null)
			{
				preprocessCheckBoxBeforeSubmitting('file_required', i);
				document.getElementById("file_required_checkbox_hidden_" + i).value  = document.getElementById("file_required_checkbox_" + i).value;
				document.getElementById("file_required_checkbox_" + i).setAttribute("processed","1");
			}
		}
	}

	$('#lastAgeCalculationDate_date_0').val(getDateStringById('date-of-last-age-js'));
	$('#onlineJobPostingDate_date_0').val(getDateStringById('date-of-online-posting-js'));
	$('#startApplicationDate_date_0').val(getDateStringById('date-of-start-app-js'));
	$('#lastApplicationDate_date_0').val(getDateStringById('date-of-end-app-js'));

	let errorMsgForLastCalEn = "Please Select Last Age Calculation Date";
	let errorMsgForLastCalBn = "Please Select Last Age Calculation Date";

	let errorMsgForOnlinePostEn = "Please Select Online Posting Date";
	let errorMsgForOnlinePostBn = "Please Select Online Posting Date";

	let errorMsgForStartAppEn = "Please Select First Application Date";
	let errorMsgForStartAppBn = "Please Select First Application Date";

	let errorMsgForEndAppEn = "Please Select Last Application Date";
	let errorMsgForEndAppBn = "Please Select Last Application Date";

	return dateValidator('date-of-last-age-js', true, { errorEn : errorMsgForLastCalEn, errorBn : errorMsgForLastCalBn})
			&& dateValidator('date-of-online-posting-js', true, { errorEn : errorMsgForOnlinePostEn, errorBn : errorMsgForOnlinePostBn})
			&& dateValidator('date-of-start-app-js', true, { errorEn : errorMsgForStartAppEn, errorBn : errorMsgForStartAppBn})
			&& dateValidator('date-of-end-app-js', true, { errorEn : errorMsgForEndAppEn, errorBn : errorMsgForEndAppBn});
}

function init(row)
{
	<% if(actionName.equalsIgnoreCase("add")){ %>
		setDateByTimestampAndId('date-of-last-age-js', <%=currentTime%>);
		setDateByTimestampAndId('date-of-online-posting-js', <%=currentTime%>);
		setDateByTimestampAndId('date-of-start-app-js', <%=currentTime%>);
		setDateByTimestampAndId('date-of-end-app-js', <%=currentTime%>);
	<% } else { %>
		setDateByTimestampAndId('date-of-last-age-js', <%=recruitment_job_descriptionDTO.lastAgeCalculationDate%>);
		setDateByTimestampAndId('date-of-online-posting-js', <%=recruitment_job_descriptionDTO.onlineJobPostingDate%>);
		setDateByTimestampAndId('date-of-start-app-js', <%=recruitment_job_descriptionDTO.firstApplicationDate%>);
		setDateByTimestampAndId('date-of-end-app-js', <%=recruitment_job_descriptionDTO.lastApplicationDate%>);

	<% } %>
	
}

var row = 0;
let language = "<%=Language%>";
	
function initialization()
{
	init(row);

	select2MultiSelector("#district_select",'<%=Language%>');

	<% if(actionName.equalsIgnoreCase("edit")){ %>
		onGradeChange();
		selectedDistrict = '<%=recruitment_job_descriptionDTO.districts%>';
		selectedDistrictArr = selectedDistrict.split(',').map(function(item) {
			return item.trim();
		});
	<% }%>

	fetchDistricts(selectedDistrictArr);

	CKEDITOR.replace('jobPurpose_text_' + row, {
		toolbar: [
			['Cut', 'Copy', 'Paste', 'Undo', 'Redo'],			// Defines toolbar group without name.
			{name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']},
			{name: 'morestyles', items: ['Strike', 'Subscript', 'Superscript']},
			{
				name: 'paragraph',
				items: ['NumberedList', 'BulletedList']
			},
			{name: 'justify', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
			{name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
			{name: 'colors', items: ['TextColor', 'BGColor']}
		],
		height: 100,
		// width: 600
	});

	CKEDITOR.replace('jobResponsibilities_text_' + row, {
		toolbar: [
			['Cut', 'Copy', 'Paste', 'Undo', 'Redo'],			// Defines toolbar group without name.
			{name: 'basicstyles', items: ['Bold', 'Italic', 'Underline']},
			{name: 'morestyles', items: ['Strike', 'Subscript', 'Superscript']},
			{
				name: 'paragraph',
				items: ['NumberedList', 'BulletedList']
			},
			{name: 'justify', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
			{name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
			{name: 'colors', items: ['TextColor', 'BGColor']}
		],
		height: 100,
		// width: 600
	});
}



var child_table_extra_id = <%=childTableStartingID%>;

	$("#add-more-RecruitmentJobSpecificFiles").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-RecruitmentJobSpecificFiles");

            $("#field-RecruitmentJobSpecificFiles").append(t.html());
			SetCheckBoxValues("field-RecruitmentJobSpecificFiles");
			
			var tr = $("#field-RecruitmentJobSpecificFiles").find("tr:last-child");
			
			tr.attr("id","RecruitmentJobSpecificFiles_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			dateTimeInit("<%=Language%>");
			child_table_extra_id ++;

        });

    
	$("#remove-RecruitmentJobSpecificFiles").click(function(e){
	    var tablename = 'field-RecruitmentJobSpecificFiles';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelectorAll('input[type="checkbox"]')[1];
				if(checkbox && checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });
	$("#add-more-RecruitmentJobSpecificCerts").click(
        function(e) 
		{
            e.preventDefault();
            var t = $("#template-RecruitmentJobSpecificCerts");

            $("#field-RecruitmentJobSpecificCerts").append(t.html());
			SetCheckBoxValues("field-RecruitmentJobSpecificCerts");
			
			var tr = $("#field-RecruitmentJobSpecificCerts").find("tr:last-child");
			
			tr.attr("id","RecruitmentJobSpecificCerts_" + child_table_extra_id);
			
			tr.find("[tag='pb_html']").each(function( index ) 
			{
				var prev_id = $( this ).attr('id');
				$( this ).attr('id', prev_id + child_table_extra_id);
				console.log( index + ": " + $( this ).attr('id') );
			});
			
			dateTimeInit("<%=Language%>");
			child_table_extra_id ++;

        });

    
	$("#remove-RecruitmentJobSpecificCerts").click(function(e){
	    var tablename = 'field-RecruitmentJobSpecificCerts';
	    var i = 0;
		console.log("document.getElementById(tablename).childNodes.length = " + document.getElementById(tablename).childNodes.length);
		var element = document.getElementById(tablename);

		var j = 0;
		for(i = document.getElementById(tablename).childNodes.length - 1; i >= 0 ; i --)
		{
			var tr = document.getElementById(tablename).childNodes[i];
			if(tr.nodeType === Node.ELEMENT_NODE)
			{
				console.log("tr.childNodes.length= " + tr.childNodes.length);
				var checkbox = tr.querySelectorAll('input[type="checkbox"]')[2];
				if(checkbox && checkbox.checked == true)
				{
					tr.remove();
				}
				j ++;
			}
			
		}    	
    });


	function onGradeChange() {
		var defaultValue = '-2';
		if('<%=actionName %>' == 'edit'){
			defaultValue = '<%=recruitment_job_descriptionDTO.employeePayScaleType%>';
		}

		var language = '<%=Language%>';
		var jobGradeCat = $("#jobGradeCat_category_0").val();
		if(jobGradeCat.length == 0){
			jobGradeCat = '0';
		}

		// console.log({defaultValue, language, jobGradeCat});


		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById('employeePayScaleType_select_0').innerHTML = this.responseText;
				// console.log(this.responseText);
			}
			else if (this.readyState == 4 && this.status != 200) {
				//alert('failed ' + this.status);
			}
		};



		var params = "Employee_pay_scaleServlet?actionType=getOptionByJobGradeCat";
		params = params + "&defaultValue=" + defaultValue + "&language=" + language + "&jobGradeCat=" + jobGradeCat;
		xhttp.open("Get", params, true);
		xhttp.send();


	}

	function fetchDistricts(selectedDistrictArr) {

		let url = "geo_Servlet?actionType=getAllDistricts";
		// console.log("url : " + url);
		$.ajax({
			url: url,
			type: "GET",
			async: false,
			success: function (fetchedData) {
				// console.log(fetchedData);


				$('#district_select').html("");
				let o;
				let str;
				if (language === 'English') {
					str = 'Select';
					o = new Option('Select', '-1');
				} else {
					o = new Option('বাছাই করুন', '-1');
					str = 'বাছাই করুন';
				}
				$(o).html(str);
				$('#district_select').append(o);
				// console.log(fetchedData)
				const response = JSON.parse(fetchedData).payload;
				if (response && response.length > 0) {
					for (let x in response) {
						if (language === 'English') {
							str = response[x].nameEng;
						} else {
							str = response[x].nameBan;
						}

						if (selectedDistrictArr && selectedDistrictArr.length > 0) {
							if (selectedDistrictArr.includes(response[x].id + '')) {
								o = new Option(str, response[x].id, false, true);
							} else {
								o = new Option(str, response[x].id);
							}
						} else {
							o = new Option(str, response[x].id);
						}
						$(o).html(str);
						$('#district_select').append(o);
					}
				}


			},
			error: function (error) {
				console.log(error);
			}
		});
	}







</script>

<style>
	label {
		/*font-weight:bold !important;*/
		font-size: 12px !important;
		text-align: right;
	}
	.required{
		color: red;
	}
</style>






