

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_job_description.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="workflow.WorkflowController"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@ page import="employee_pay_scale.Employee_pay_scaleDAO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDTO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleRepository" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String actionName = "edit";
String failureMessage = (String)request.getAttribute("failureMessage");
if(failureMessage == null || failureMessage.isEmpty())
{
	failureMessage = "";	
}
out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
String value = "";
String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);

String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
long id = Long.parseLong(ID);
System.out.println("ID = " + ID);
Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO("recruitment_job_description");
Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = recruitment_job_descriptionDAO.getDTOByID(id);
String Value = "";
int i = 0;
FilesDAO filesDAO = new FilesDAO();
SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
%>


<!-- <div class="modal-content viewmodal"> -->
<div class="menubottom">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 col-sm-12">
                            <h5 class="modal-title"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_ADD_FORMNAME, loginDTO)%></h5>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-body container">
			
			<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h3><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_ADD_FORMNAME, loginDTO)%></h3>
						<table class="table table-bordered table-striped">
									

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEEN, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.jobTitleEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEBN, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.jobTitleBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.jobGradeCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.employmentStatusCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "employment_status", recruitment_job_descriptionDTO.employmentStatusCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYEEPAYSCALETYPE, loginDTO)%></b></td>
								<td>
						
									<%
//									long payScaleTypeId = recruitment_job_descriptionDTO.employeePayScaleType;
//										Employee_pay_scaleDAO employee_pay_scaleDAO = new Employee_pay_scaleDAO();
//										Employee_pay_scaleDTO employee_pay_scaleDTO =
//												employee_pay_scaleDAO.getDTOByID(payScaleTypeId);
//										String payScaleName = "";
//										if(employee_pay_scaleDTO != null){
//											payScaleName = employee_pay_scaleDTO.payScaleBrief;
//										}

										value = "";
										Employee_pay_scaleDTO employee_pay_scaleDTO = Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleDTOByID(recruitment_job_descriptionDTO.employeePayScaleType);
										if(employee_pay_scaleDTO != null){
											value = CatDAO.getName(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);

										}
									%>
														
									<%=value%>
				
			
								</td>
						
							</tr>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBPURPOSE, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.jobPurpose + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBRESPONSIBILITIES, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.jobResponsibilities + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINIMUMACADEMICQUALIFICATION, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.minimumAcademicQualification + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINIMUMEXPERIENCEREQUIRED, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.minimumExperienceRequired + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCERTIFICATIONANDTRANING, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.requiredCertificationAndTraning + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_PERSONALCHARACTERISTICS, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.personalCharacteristics + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCOMPETENCE, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.requiredCompetence + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EDUCATIONLEVELTYPE, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.educationLevelType + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CommonDAO.getName(Integer.parseInt(value), "education_level", Language.equals("English")?"name_en":"name_bn", "id");--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINEXPERIENCE, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.minExperience + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = String.format("%.1f", recruitment_job_descriptionDTO.minExperience);--%>
<%--											%>												--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEREGULAR, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBLOCATION, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.jobLocation + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_NUMBEROFVACANCY, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.numberOfVacancy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
								</td>
						
							</tr>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAPPLICATIONDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.lastApplicationDate + "";
											%>
											<%
											String formatted_lastApplicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_lastApplicationDate, Language)%>
				
			
								</td>
						
							</tr>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAGECALCULATIONDATE, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.lastAgeCalculationDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_lastAgeCalculationDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_lastAgeCalculationDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			

							<tr>
								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_ONLINEJOBPOSTINGDATE, loginDTO)%></b></td>
								<td>
						
											<%
											value = recruitment_job_descriptionDTO.onlineJobPostingDate + "";
											%>
											<%
											String formatted_onlineJobPostingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_onlineJobPostingDate, Language)%>
				
			
								</td>
						
							</tr>

				


			

<%--							<tr>--%>
<%--								<td style="width:30%"><b><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FILESDROPZONE, loginDTO)%></b></td>--%>
<%--								<td>--%>
<%--						--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.filesDropzone + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											{--%>
<%--												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(recruitment_job_descriptionDTO.filesDropzone);--%>
<%--												%>--%>
<%--												<table>--%>
<%--												<tr>--%>
<%--												<%--%>
<%--												if(FilesDTOList != null)--%>
<%--												{--%>
<%--													for(int j = 0; j < FilesDTOList.size(); j ++)--%>
<%--													{--%>
<%--														FilesDTO filesDTO = FilesDTOList.get(j);--%>
<%--														byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);--%>
<%--														%>--%>
<%--														<td>--%>
<%--														<%--%>
<%--														if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)--%>
<%--														{--%>
<%--															%>--%>
<%--															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />--%>
<%--															<%--%>
<%--														}--%>
<%--														%>--%>
<%--														<a href = 'Recruitment_job_descriptionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>--%>
<%--														</td>--%>
<%--													<%--%>
<%--													}--%>
<%--												}--%>
<%--												%>--%>
<%--												</tr>--%>
<%--												</table>--%>
<%--											<%												--%>
<%--											}--%>
<%--											%>--%>
<%--				--%>
<%--			--%>
<%--								</td>--%>
<%--						--%>
<%--							</tr>--%>

				


			
			
			
			
			
			
			
		
						</table>
                    </div>
			






			</div>	

<%--                <div class="row div_border attachement-div">--%>
<%--                    <div class="col-md-12">--%>
<%--                        <h5><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES, loginDTO)%></h5>--%>
<%--						<table class="table table-bordered table-striped">--%>
<%--							<tr>--%>
<%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_RECRUITMENTJOBREQUIREDFILESTYPE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_ISMANDATORYFILES, loginDTO)%></th>--%>
<%--&lt;%&ndash;								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_MODIFIEDBY, loginDTO)%></th>&ndash;%&gt;--%>
<%--							</tr>--%>
<%--							<%--%>
<%--                        	RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO();--%>
<%--                         	List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOs = recruitmentJobSpecificFilesDAO.getRecruitmentJobSpecificFilesDTOListByRecruitmentJobDescriptionID(recruitment_job_descriptionDTO.iD);--%>
<%--                         	--%>
<%--                         	for(RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO: recruitmentJobSpecificFilesDTOs)--%>
<%--                         	{--%>
<%--                         		%>--%>
<%--                         			<tr>--%>
<%--										<td>--%>
<%--											<%--%>
<%--											value = recruitmentJobSpecificFilesDTO.recruitmentJobRequiredFilesType + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CommonDAO.getName(Integer.parseInt(value), "recruitment_job_required_files", Language.equals("English")?"name_en":"name_bn", "id");--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--										</td>--%>
<%--										<td>--%>
<%--											<%--%>
<%--											value = recruitmentJobSpecificFilesDTO.isMandatoryFiles + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--										</td>--%>
<%--&lt;%&ndash;										<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = recruitmentJobSpecificFilesDTO.modifiedBy + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;										</td>&ndash;%&gt;--%>
<%--                         			</tr>--%>
<%--                         		<%--%>
<%--                         		--%>
<%--                         	}--%>
<%--                         	--%>
<%--                        %>--%>
<%--						</table>--%>
<%--                    </div>                    --%>
<%--                </div>--%>
<%--                <div class="row div_border attachement-div">--%>
<%--                    <div class="col-md-12">--%>
<%--                        <h5><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS, loginDTO)%></h5>--%>
<%--						<table class="table table-bordered table-striped">--%>
<%--							<tr>--%>
<%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_RECRUITMENTJOBREQTRAINSANDCERTSTYPE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_ISMANDATORYCERTS, loginDTO)%></th>--%>
<%--&lt;%&ndash;								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_MODIFIEDBY, loginDTO)%></th>&ndash;%&gt;--%>
<%--							</tr>--%>
<%--							<%--%>
<%--                        	RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO();--%>
<%--                         	List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOs = recruitmentJobSpecificCertsDAO.getRecruitmentJobSpecificCertsDTOListByRecruitmentJobDescriptionID(recruitment_job_descriptionDTO.iD);--%>
<%--                         	--%>
<%--                         	for(RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO: recruitmentJobSpecificCertsDTOs)--%>
<%--                         	{--%>
<%--                         		%>--%>
<%--                         			<tr>--%>
<%--										<td>--%>
<%--											<%--%>
<%--											value = recruitmentJobSpecificCertsDTO.recruitmentJobReqTrainsAndCertsType + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CommonDAO.getName(Integer.parseInt(value), "recruitment_job_req_trains_and_certs", Language.equals("English")?"name_en":"name_bn", "id");--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--										</td>--%>
<%--										<td>--%>
<%--											<%--%>
<%--											value = recruitmentJobSpecificCertsDTO.isMandatoryCerts + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--										</td>--%>
<%--&lt;%&ndash;										<td>&ndash;%&gt;--%>
<%--&lt;%&ndash;											&lt;%&ndash;%>--%>
<%--&lt;%&ndash;											value = recruitmentJobSpecificCertsDTO.modifiedBy + "";&ndash;%&gt;--%>
<%--&lt;%&ndash;											%>&ndash;%&gt;--%>
<%--&lt;%&ndash;														&ndash;%&gt;--%>
<%--&lt;%&ndash;											<%=Utils.getDigits(value, Language)%>&ndash;%&gt;--%>
<%--&lt;%&ndash;				&ndash;%&gt;--%>
<%--&lt;%&ndash;			&ndash;%&gt;--%>
<%--&lt;%&ndash;										</td>&ndash;%&gt;--%>
<%--                         			</tr>--%>
<%--                         		<%--%>
<%--                         		--%>
<%--                         	}--%>
<%--                         	--%>
<%--                        %>--%>
<%--						</table>--%>
<%--                    </div>                    --%>
   </div>


</div>