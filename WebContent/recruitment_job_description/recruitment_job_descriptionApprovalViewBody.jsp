

<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_job_description.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@page import="user.*"%>
<%@page import="workflow.*"%>
<%@ page import="util.CommonConstant" %>


<%
	String servletName = "Recruitment_job_descriptionServlet";
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);

	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);

	String actionName = "edit";
	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty())
	{
		failureMessage = "";
	}
	out.println("<input type='hidden' id='failureMessage_general' value='" + failureMessage + "'/>");
	String value = "";


	String ID = request.getParameter("ID");
	boolean isPermanentTable = Boolean.parseBoolean(request.getParameter("isPermanentTable"));

	if(ID == null || ID.isEmpty())
	{
		ID = "0";
	}
	long id = Long.parseLong(ID);
	System.out.println("ID = " + ID + " isPermanentTable = " + isPermanentTable);
	Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = new Recruitment_job_descriptionDAO("recruitment_job_description");
	Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = recruitment_job_descriptionDAO.getDTOByID(id);
	String Value = "";
	int i = 0;
	FilesDAO filesDAO = new FilesDAO();
	
	String tableName = "recruitment_job_description";

	Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
	ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
	Approval_execution_tableDTO approval_execution_tableDTO = null;
	Approval_execution_tableDTO approval_execution_table_initiationDTO = null;
	ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;

	boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
	boolean isInPreviousOffice = false;
	String Message = "Done";

	if(!isPermanentTable)
	{
		approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("recruitment_job_description", recruitment_job_descriptionDTO.iD);
		System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
		approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
		approval_execution_table_initiationDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getInitiationDTOByPreviousRowId("recruitment_job_description", approval_execution_tableDTO.previousRowId);
		if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID 
				&& approval_execution_tableDTO.iD == approval_execution_tableDAO.getMostRecentExecutionIDByPreviousRowID("recruitment_job_description", approval_execution_tableDTO.previousRowId))
		{
			canApprove = true;
			if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
			{
				canValidate = true;
			}
		}
		
		isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);
		
		canTerminate = isInitiator && recruitment_job_descriptionDTO.isDeleted == 2;
	}	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>

<div class="modal-content viewmodal">
            <div class="modal-header">
                <div class="col-md-12">
                    <div class="row">
                        
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-success app_register" data-id="419637"> Register </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:" style="display: none" class="btn btn-danger app_reject" data-id="419637"> Reject </a>
                                </div>
                            </div>
                        </div>
                    </div>
					<button type="button" class="btn btn-info" id = 'printer' onclick="printAnyDiv('modalbody')" >Print</button>
                </div>


            </div>

            <div class="modal-body container" id="modalbody">
			
				<div class="row div_border office-div">

                    <div class="col-md-12">
                        <h5><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_DESCRIPTION_ADD_FORMNAME, loginDTO)%></h5>
                    </div>
			



			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEEN, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="jobTitleEn">
						
											<%
											value = recruitment_job_descriptionDTO.jobTitleEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEBN, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="jobTitleBn">
						
											<%
											value = recruitment_job_descriptionDTO.jobTitleBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="jobGradeCat">
						
											<%
											value = recruitment_job_descriptionDTO.jobGradeCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="employmentStatusCat">
						
											<%
											value = recruitment_job_descriptionDTO.employmentStatusCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "employment_status", recruitment_job_descriptionDTO.employmentStatusCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYEEPAYSCALETYPE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="employeePayScaleType">
						
											<%
											value = recruitment_job_descriptionDTO.employeePayScaleType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "employee_pay_scale", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBPURPOSE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="jobPurpose">
						
											<%
											value = recruitment_job_descriptionDTO.jobPurpose + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBRESPONSIBILITIES, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="jobResponsibilities">
						
											<%
											value = recruitment_job_descriptionDTO.jobResponsibilities + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINIMUMACADEMICQUALIFICATION, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="minimumAcademicQualification">
						
											<%
											value = recruitment_job_descriptionDTO.minimumAcademicQualification + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINIMUMEXPERIENCEREQUIRED, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="minimumExperienceRequired">
						
											<%
											value = recruitment_job_descriptionDTO.minimumExperienceRequired + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCERTIFICATIONANDTRANING, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="requiredCertificationAndTraning">
						
											<%
											value = recruitment_job_descriptionDTO.requiredCertificationAndTraning + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_PERSONALCHARACTERISTICS, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="personalCharacteristics">
						
											<%
											value = recruitment_job_descriptionDTO.personalCharacteristics + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCOMPETENCE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="requiredCompetence">
						
											<%
											value = recruitment_job_descriptionDTO.requiredCompetence + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EDUCATIONLEVELTYPE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="educationLevelType">
						
											<%
											value = recruitment_job_descriptionDTO.educationLevelType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "education_level", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINEXPERIENCE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="minExperience">
						
											<%
											value = recruitment_job_descriptionDTO.minExperience + "";
											%>
											<%
											value = String.format("%.1f", recruitment_job_descriptionDTO.minExperience);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEREGULAR, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="maxAgeOnBoundaryDateRegular">
						
											<%
											value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="maxAgeOnBoundaryDateFf">
						
											<%
											value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBLOCATION, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="jobLocation">
						
											<%
											value = recruitment_job_descriptionDTO.jobLocation + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_NUMBEROFVACANCY, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="numberOfVacancy">
						
											<%
											value = recruitment_job_descriptionDTO.numberOfVacancy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAPPLICATIONDATE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="lastApplicationDate">
						
											<%
											value = recruitment_job_descriptionDTO.lastApplicationDate + "";
											%>
											<%
											String formatted_lastApplicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_lastApplicationDate, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAGECALCULATIONDATE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="lastAgeCalculationDate">
						
											<%
											value = recruitment_job_descriptionDTO.lastAgeCalculationDate + "";
											%>
											<%
											String formatted_lastAgeCalculationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_lastAgeCalculationDate, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_ONLINEJOBPOSTINGDATE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="onlineJobPostingDate">
						
											<%
											value = recruitment_job_descriptionDTO.onlineJobPostingDate + "";
											%>
											<%
											String formatted_onlineJobPostingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_onlineJobPostingDate, Language)%>
				
			
						
                        </label>
                    </div>

				


			

                    <div class="col-md-6">
                        <label class="col-md-5" style="padding-right: 0px;"><b><span><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FILESDROPZONE, loginDTO)%></span><span style="float:right;">:</span></b></label>
                        <label id="filesDropzone">
						
											<%
											value = recruitment_job_descriptionDTO.filesDropzone + "";
											%>
											<%
											{
												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(recruitment_job_descriptionDTO.filesDropzone);
												%>
												<table>
												<tr>
												<%
												if(FilesDTOList != null)
												{
													for(int j = 0; j < FilesDTOList.size(); j ++)
													{
														FilesDTO filesDTO = FilesDTOList.get(j);
														byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
														%>
														<td>
														<%
														if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
														{
															%>
															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
															<%
														}
														%>
														<a href = 'Recruitment_job_descriptionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
														</td>
													<%
													}
												}
												%>
												</tr>
												</table>
											<%												
											}
											%>
				
			
						
                        </label>
                    </div>

				


			
			
			
			
			
			
			
		

				<div class="col-md-6">
				<label class="col-md-5" style="padding-right: 0px;"><b><span>Initiator Instructions</span><span style="float:right;">:</span></b></label>
				<label id="referenceNo">

					<%
						value = approval_execution_table_initiationDTO.remarks + "";
					%>

					<%=value%>



				</label>
			</div>
			
			<div class="col-md-6">
				<label class="col-md-5" style="padding-right: 0px;"><b><span>Initiator Files</span><span style="float:right;">:</span></b></label>
				<label id="filesDropzone"></label> <br/>

				
				<%
					{
					List<FilesDTO>  FilesDTOList = filesDAO.getMiniDTOsByFileID(approval_execution_table_initiationDTO.fileDropzone);
						%>
						<table>
						<tr>
						<%
						if(FilesDTOList != null)
						{
							for(int j = 0; j < FilesDTOList.size(); j ++)
							{
								FilesDTO filesDTO = FilesDTOList.get(j);
								byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
								%>
								<td>
								<%
								if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
								{
									%>
									<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
									<%
								}
								%>
								<a href = 'Approval_execution_tableServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
								</td>
							<%
							}
						}
						%>
						</tr>
						</table>
					<%												
					}
					%>
			</div>

			</div>
				

                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_RECRUITMENTJOBREQUIREDFILESTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_ISMANDATORYFILES, loginDTO)%></th>
<%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_FILES_MODIFIEDBY, loginDTO)%></th>--%>
							</tr>
							<%
                        	RecruitmentJobSpecificFilesDAO recruitmentJobSpecificFilesDAO = new RecruitmentJobSpecificFilesDAO();
                         	List<RecruitmentJobSpecificFilesDTO> recruitmentJobSpecificFilesDTOs = recruitmentJobSpecificFilesDAO.getRecruitmentJobSpecificFilesDTOListByRecruitmentJobDescriptionID(recruitment_job_descriptionDTO.iD);
                         	
                         	for(RecruitmentJobSpecificFilesDTO recruitmentJobSpecificFilesDTO: recruitmentJobSpecificFilesDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = recruitmentJobSpecificFilesDTO.recruitmentJobRequiredFilesType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "recruitment_job_required_files", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentJobSpecificFilesDTO.isMandatoryFiles + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
<%--										<td>--%>
<%--											<%--%>
<%--											value = recruitmentJobSpecificFilesDTO.modifiedBy + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--										</td>--%>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
                <div class="row div_border attachement-div">
                    <div class="col-md-12">
                        <h5><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS, loginDTO)%></h5>
						<table class="table table-bordered table-striped">
							<tr>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_RECRUITMENTJOBREQTRAINSANDCERTSTYPE, loginDTO)%></th>
								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_ISMANDATORYCERTS, loginDTO)%></th>
<%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_RECRUITMENT_JOB_SPECIFIC_CERTS_MODIFIEDBY, loginDTO)%></th>--%>
							</tr>
							<%
                        	RecruitmentJobSpecificCertsDAO recruitmentJobSpecificCertsDAO = new RecruitmentJobSpecificCertsDAO();
                         	List<RecruitmentJobSpecificCertsDTO> recruitmentJobSpecificCertsDTOs = recruitmentJobSpecificCertsDAO.getRecruitmentJobSpecificCertsDTOListByRecruitmentJobDescriptionID(recruitment_job_descriptionDTO.iD);
                         	
                         	for(RecruitmentJobSpecificCertsDTO recruitmentJobSpecificCertsDTO: recruitmentJobSpecificCertsDTOs)
                         	{
                         		%>
                         			<tr>
										<td>
											<%
											value = recruitmentJobSpecificCertsDTO.recruitmentJobReqTrainsAndCertsType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "recruitment_job_req_trains_and_certs", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
										<td>
											<%
											value = recruitmentJobSpecificCertsDTO.isMandatoryCerts + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
										</td>
<%--										<td>--%>
<%--											<%--%>
<%--											value = recruitmentJobSpecificCertsDTO.modifiedBy + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--										</td>--%>
                         			</tr>
                         		<%
                         		
                         	}
                         	
                        %>
						</table>
                    </div>                    
                </div>
               


        

					<%if(!isPermanentTable && (canApprove || canTerminate)) {%>
				
						<div class="row div_border attachement-div">
							<div class="col-md-12">
								<h5><%=LM.getText(LC.HM_ATTACHMENTS, loginDTO )%></h5>
								<%
									long ColumnID = DBMW.getInstance().getNextSequenceId("fileid");
								%>
								<div class="dropzone"
									 action="Approval_execution_tableServlet?pageType=add&actionType=UploadFilesFromDropZone&columnName=approval_attached_fileDropzone&ColumnID=<%=ColumnID%>">
									<input type='file' style="display: none"
										   name='approval_attached_fileDropzoneFile'
										   id='approval_attached_fileDropzone_dropzone_File_<%=i%>'
										   tag='pb_html' />
								</div>
								<input type='hidden'
									   name='approval_attached_fileDropzoneFilesToDelete'
									   id='approval_attached_fileDropzoneFilesToDelete_<%=i%>' value=''
									   tag='pb_html' /> <input type='hidden'
															   name='approval_attached_fileDropzone'
															   id='approval_attached_fileDropzone_dropzone_<%=i%>' tag='pb_html'
															   value='<%=ColumnID%>' />
							</div>
				
							<div class="col-md-12">
								<h5><%=LM.getText(LC.HM_REMARKS, loginDTO )%></h5>
				
								<textarea class='form-control'  name='remarks' id = '<%=i%>_remarks'   tag='pb_html'></textarea>
							</div>
						</div>
						<%}%>
						<div class="form-actions text-center">
						<%
							if(canApprove)
							{
							%>
							<button type="submit"  class="btn btn-success" id = 'approve_<%=id%>' data-toggle="modal" data-target="#approvalModal" ><%=LM.getText(LC.HM_APPROVE, loginDTO)%></button>
							<%@include file="../approval_path/approvalModal.jsp"%>
							<button type="submit" class="btn btn-danger" id = 'reject_<%=id%>' data-toggle="modal" data-target="#rejectionModal"><%=LM.getText(LC.HM_REJECT, loginDTO)%></button>
							<%@include file="../approval_path/rejectionModal.jsp"%>
							<%
							}
							if(canTerminate)
							{
							%>
							<button type="submit"  class="btn btn-success" id = 'approve_<%=id%>' onclick='approve("<%=approval_execution_tableDTO.updatedRowId%>", "<%=Message%>", <%=i%>, "Recruitment_job_descriptionServlet", 2, true, true)'><%=LM.getText(LC.HM_TERMINATE, loginDTO)%></button>
							<%
							}							
							if(canValidate)
							{
							%>
							<a type="submit"  class="btn btn-success" id = 'validate_<%=id%>' href='Recruitment_job_descriptionServlet?actionType=getEditPage&ID=<%=id%>&isPermanentTable=<%=isPermanentTable%>'><%=LM.getText(LC.HM_VALIDATE, loginDTO)%></a>
							<%
							}
							%>
							<button type="submit"  class="btn btn-success" id = 'history_<%=id%>' data-toggle="modal" data-target="#historyModal"><%=LM.getText(LC.HM_HISTORY, loginDTO)%></button>
							<%@include file="../approval_path/historyModal.jsp"%>	
						</div>
					
           

        </div>
        
        
 <script type="text/javascript">
 window.onload =function ()
 {
     console.log("using ckEditor");
     CKEDITOR.replaceAll();
 }
 </script>