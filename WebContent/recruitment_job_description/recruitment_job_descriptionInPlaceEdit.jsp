<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="recruitment_job_description.Recruitment_job_descriptionDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="util.TimeFormat"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>

<%
Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO)request.getAttribute("recruitment_job_descriptionDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(recruitment_job_descriptionDTO == null)
{
	recruitment_job_descriptionDTO = new Recruitment_job_descriptionDTO();
	
}
System.out.println("recruitment_job_descriptionDTO = " + recruitment_job_descriptionDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

long ColumnID;
FilesDAO filesDAO = new FilesDAO();
%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
CommonDAO.language = Language;
CatDAO.language = Language;
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=recruitment_job_descriptionDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobTitleEn'>")%>
			
	
	<div class="form-inline" id = 'jobTitleEn_div_<%=i%>'>
		<input type='text' class='form-control'  name='jobTitleEn' id = 'jobTitleEn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.jobTitleEn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobTitleBn'>")%>
			
	
	<div class="form-inline" id = 'jobTitleBn_div_<%=i%>'>
		<input type='text' class='form-control'  name='jobTitleBn' id = 'jobTitleBn_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.jobTitleBn + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobGradeCat'>")%>
			
	
	<div class="form-inline" id = 'jobGradeCat_div_<%=i%>'>
		<select class='form-control'  name='jobGradeCat' id = 'jobGradeCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "job_grade", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employmentStatusCat'>")%>
			
	
	<div class="form-inline" id = 'employmentStatusCat_div_<%=i%>'>
		<select class='form-control'  name='employmentStatusCat' id = 'employmentStatusCat_category_<%=i%>'   tag='pb_html'>		
<%
if(actionName.equals("edit"))
{
			Options = CatDAO.getOptions(Language, "employment_status", recruitment_job_descriptionDTO.employmentStatusCat);
}
else
{			
			Options = CatDAO.getOptions(Language, "employment_status", -1);			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_employeePayScaleType'>")%>
			
	
	<div class="form-inline" id = 'employeePayScaleType_div_<%=i%>'>
		<select class='form-control'  name='employeePayScaleType' id = 'employeePayScaleType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "employee_pay_scale", "employeePayScaleType_select_" + i, "form-control", "employeePayScaleType", recruitment_job_descriptionDTO.employeePayScaleType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "employee_pay_scale", "employeePayScaleType_select_" + i, "form-control", "employeePayScaleType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobPurpose'>")%>
			
	
	<div class="form-inline" id = 'jobPurpose_div_<%=i%>'>
		<input type='text' class='form-control'  name='jobPurpose' id = 'jobPurpose_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.jobPurpose + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobResponsibilities'>")%>
			
	
	<div class="form-inline" id = 'jobResponsibilities_div_<%=i%>'>
		<input type='text' class='form-control'  name='jobResponsibilities' id = 'jobResponsibilities_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.jobResponsibilities + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_minimumAcademicQualification'>")%>
			
	
	<div class="form-inline" id = 'minimumAcademicQualification_div_<%=i%>'>
		<input type='text' class='form-control'  name='minimumAcademicQualification' id = 'minimumAcademicQualification_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.minimumAcademicQualification + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_minimumExperienceRequired'>")%>
			
	
	<div class="form-inline" id = 'minimumExperienceRequired_div_<%=i%>'>
		<input type='text' class='form-control'  name='minimumExperienceRequired' id = 'minimumExperienceRequired_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.minimumExperienceRequired + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_requiredCertificationAndTraning'>")%>
			
	
	<div class="form-inline" id = 'requiredCertificationAndTraning_div_<%=i%>'>
		<input type='text' class='form-control'  name='requiredCertificationAndTraning' id = 'requiredCertificationAndTraning_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.requiredCertificationAndTraning + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_personalCharacteristics'>")%>
			
	
	<div class="form-inline" id = 'personalCharacteristics_div_<%=i%>'>
		<input type='text' class='form-control'  name='personalCharacteristics' id = 'personalCharacteristics_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.personalCharacteristics + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_requiredCompetence'>")%>
			
	
	<div class="form-inline" id = 'requiredCompetence_div_<%=i%>'>
		<input type='text' class='form-control'  name='requiredCompetence' id = 'requiredCompetence_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.requiredCompetence + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_educationLevelType'>")%>
			
	
	<div class="form-inline" id = 'educationLevelType_div_<%=i%>'>
		<select class='form-control'  name='educationLevelType' id = 'educationLevelType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "education_level", "educationLevelType_select_" + i, "form-control", "educationLevelType", recruitment_job_descriptionDTO.educationLevelType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "education_level", "educationLevelType_select_" + i, "form-control", "educationLevelType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_minExperience'>")%>
			
	
	<div class="form-inline" id = 'minExperience_div_<%=i%>'>
		<input type='text' class='form-control'  name='minExperience' id = 'minExperience_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.minExperience + "'"):("'" + "0.0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_maxAgeOnBoundaryDateRegular'>")%>
			
	
	<div class="form-inline" id = 'maxAgeOnBoundaryDateRegular_div_<%=i%>'>
		<input type='text' class='form-control'  name='maxAgeOnBoundaryDateRegular' id = 'maxAgeOnBoundaryDateRegular_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_maxAgeOnBoundaryDateFf'>")%>
			
	
	<div class="form-inline" id = 'maxAgeOnBoundaryDateFf_div_<%=i%>'>
		<input type='text' class='form-control'  name='maxAgeOnBoundaryDateFf' id = 'maxAgeOnBoundaryDateFf_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf + "'"):("'" + "0" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobLocation'>")%>
			
	
	<div class="form-inline" id = 'jobLocation_div_<%=i%>'>
		<input type='text' class='form-control'  name='jobLocation' id = 'jobLocation_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.jobLocation + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_numberOfVacancy'>")%>
			
	
	<div class="form-inline" id = 'numberOfVacancy_div_<%=i%>'>
		<input type='number' class='form-control'  name='numberOfVacancy' id = 'numberOfVacancy_number_<%=i%>' min='0' max='1000000' value=<%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.numberOfVacancy + "'"):("'" + 0 + "'")%>  tag='pb_html'>
						
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastApplicationDate'>")%>
			
	
	<div class="form-inline" id = 'lastApplicationDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'lastApplicationDate_date_<%=i%>' name='lastApplicationDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_lastApplicationDate = dateFormat.format(new Date(recruitment_job_descriptionDTO.lastApplicationDate));
	%>
	'<%=formatted_lastApplicationDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastAgeCalculationDate'>")%>
			
	
	<div class="form-inline" id = 'lastAgeCalculationDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'lastAgeCalculationDate_date_<%=i%>' name='lastAgeCalculationDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_lastAgeCalculationDate = dateFormat.format(new Date(recruitment_job_descriptionDTO.lastAgeCalculationDate));
	%>
	'<%=formatted_lastAgeCalculationDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_onlineJobPostingDate'>")%>
			
	
	<div class="form-inline" id = 'onlineJobPostingDate_div_<%=i%>'>
		<input type='text' class='form-control formRequired datepicker' readonly="readonly" data-label="Document Date" id = 'onlineJobPostingDate_date_<%=i%>' name='onlineJobPostingDate' value=<%
if(actionName.equals("edit"))
{
	String formatted_onlineJobPostingDate = dateFormat.format(new Date(recruitment_job_descriptionDTO.onlineJobPostingDate));
	%>
	'<%=formatted_onlineJobPostingDate%>'
	<%
}
else
{
	%>
	'<%=datestr%>'
	<%
}
%>
   tag='pb_html'>
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_filesDropzone'>")%>
			
	
	<div class="form-inline" id = 'filesDropzone_div_<%=i%>'>
		<%
		if(actionName.equals("edit"))
		{
			List<FilesDTO> filesDropzoneDTOList = filesDAO.getMiniDTOsByFileID(recruitment_job_descriptionDTO.filesDropzone);
			%>			
			<table>
				<tr>
			<%
			if(filesDropzoneDTOList != null)
			{
				for(int j = 0; j < filesDropzoneDTOList.size(); j ++)
				{
					FilesDTO filesDTO = filesDropzoneDTOList.get(j);
					byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
					%>
					<td id = 'filesDropzone_td_<%=filesDTO.iD%>'>
					<%
					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
					{
						%>
						<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64) 
						%>' style='width:100px' />
						<%
					}
					%>
					<a href = 'Recruitment_job_descriptionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
					<a class='btn btn-danger' onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete_<%=i%>")'>x</a>		
					</td>
					<%
				}
			}
			%>
			</tr>
			</table>
			<%
		}
		%>
		
		<%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>			
		<div class="dropzone" action="Recruitment_job_descriptionServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?recruitment_job_descriptionDTO.filesDropzone:ColumnID%>">
			<input type='file' style="display:none"  name='filesDropzoneFile' id = 'filesDropzone_dropzone_File_<%=i%>'  tag='pb_html'/>			
		</div>								
		<input type='hidden'  name='filesDropzoneFilesToDelete' id = 'filesDropzoneFilesToDelete_<%=i%>' value=''  tag='pb_html'/>
		<input type='hidden' name='filesDropzone' id = 'filesDropzone_dropzone_<%=i%>'  tag='pb_html' value='<%=actionName.equals("edit")?recruitment_job_descriptionDTO.filesDropzone:ColumnID%>'/>		


	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_jobCat" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='jobCat' id = 'jobCat_hidden_<%=i%>' value='<%=recruitment_job_descriptionDTO.jobCat%>' tag='pb_html'/>
		
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertedByUserId" +  "' style='display:none;'>")%>
			
<%=("</td>")%>
			
<%=("<td id = '" + i + "_insertionDate" +  "' style='display:none;'>")%>
			
<%=("</td>")%>
			
<%=("<td id = '" + i + "_modifiedBy" +  "' style='display:none;'>")%>
			
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + recruitment_job_descriptionDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=recruitment_job_descriptionDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
	
											<td>
												<a href='Recruitment_job_descriptionServlet?actionType=view&ID=<%=recruitment_job_descriptionDTO.iD%>'>View</a>
												
												<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
												
												<div class='modal fade' id='viedFileModal_<%=i%>'>
												  <div class='modal-dialog modal-lg' role='document'>
													<div class='modal-content'>
													  <div class='modal-body'>
														<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
														  <span aria-hidden='true'>&times;</span>
														</button>											        
														
														<object type='text/html' data='Recruitment_job_descriptionServlet?actionType=view&modal=1&ID=<%=recruitment_job_descriptionDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
														
													  </div>
													</div>
												  </div>
												</div>
											</td>

	