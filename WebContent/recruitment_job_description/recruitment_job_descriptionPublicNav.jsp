<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="language.LM" %>
<%@ page language="java" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="searchform.SearchForm" %>
<%@ page import="pb.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    System.out.println("Inside nav.jsp");
    String url = request.getParameter("url");
    String navigator = request.getParameter("navigator");
    String pageName = request.getParameter("pageName");
    if (pageName == null)
        pageName = "Search";
    String pageno = "";
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    RecordNavigator rn = (RecordNavigator) session.getAttribute(navigator);
    pageno = (rn == null) ? "1" : "" + rn.getCurrentPageNo();

    System.out.println("rn " + rn);

    String action = url;
    String context = "../../.." + request.getContextPath() + "/";
    String link = context + url;
    String concat = "?";
    if (url.contains("?")) {
        concat = "&";
    }
    String[][] searchFieldInfo = rn.getSearchFieldInfo();
    String totalPage = "1";
    if (rn != null)
        totalPage = rn.getTotalPages() + "";
    int row = 0;

    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
    int pagination_number = 0;
    boolean isPermanentTable = rn.m_isPermanentTable;
    System.out.println("In nav::: isPermanentTable = " + isPermanentTable);
%>

<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <%
                    out.println("<input placeholder='অনুসন্ধান করুন' autocomplete='off' type='text' class='form-control border-0' onKeyUp='allfield_changed(\"\",0)' id='anyfield'  name='" + LM.getText(LC.ASSET_MANUFACTURER_SEARCH_ANYFIELD, loginDTO) + "' ");
                    String value = (String) session.getAttribute(searchFieldInfo[searchFieldInfo.length - 1][1]);

                    if (value != null) {
                        out.println("value = '" + value + "'");
                    } else {
                        out.println("value=''");
                    }

                    out.println("/><br />");
                %>
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <%--        <div class="kt-portlet__head-toolbar">--%>
        <%--            <div class="kt-portlet__head-group">--%>
        <%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
        <%--                     aria-hidden="true" x-placement="top"--%>
        <%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0px; left: 0px; transform: translate3d(631px, -39px, 0px);">--%>
        <%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
        <%--                    <div class="tooltip-inner">Collapse</div>--%>
        <%--                </div>--%>
        <%--            </div>--%>
        <%--        </div>--%>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4  col-form-label">
                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEEN, loginDTO)%>
                        </label>

                        </label>
                        <div class="col-md-8 ">
                            <input type="text" class="form-control" id="job_title_en" placeholder="" name="job_title_en"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4  col-form-label">
                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEBN, loginDTO)%>
                        </label>

                        </label>
                        <div class="col-md-8 ">
                            <input type="text" class="form-control" id="job_title_bn" placeholder="" name="job_title_bn"
                                   onChange='setSearchChanged()'>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4  col-form-label">
                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>
                        </label>

                        </label>
                        <div class="col-md-8 ">
                            <select class='form-control' name='job_grade_cat' id='job_grade_cat'
                                    onSelect='setSearchChanged()'>
                                <%--			<option value='-1'></option>--%>
                                <%

                                    Options = CatDAO.getOptions(Language, "job_grade", -2);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4  col-form-label">
                            <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%>
                        </label>

                        </label>
                        <div class="col-md-8 ">
                            <select class='form-control' name='employment_status_cat' id='employment_status_cat'
                                    onSelect='setSearchChanged()'>
                                <%--			<option value='-1'></option>--%>
                                <%

                                    // Options = CatDAO.getOptions(Language, "employment_status", -1);
                                    Options = CatDAO.getOptions(Language, "employment_status", -2);
                                %>
                                <%=Options%>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->


<!-- search control -->
<%--<div class="portlet box portlet-btcl">--%>
<%--	<div class="portlet-title">--%>
<%--		<div class="caption" ><i class="fa fa-search-plus"  style="margin-top:-3px"></i><%=pageName%></div>--%>
<%--		<p class="desktop-only" style="float:right; margin:10px 5px !important;">Advanced Search</p>--%>
<%--		<div class="tools">--%>
<%--			<a class="expand" href="javascript:;" data-original-title="" title=""></a>	--%>
<%--		</div>--%>
<%--		--%>
<%--		<div class="col-xs-12 col-sm-5 col-md-4" style="margin-top:10px">--%>

<%--		</div>--%>
<%--		--%>
<%--		--%>
<%--		--%>
<%--	</div>--%>
<%--	<div --%>
<%--	class="portlet-body form collapse"--%>
<%--	>--%>
<%--		<!-- BEGIN FORM-->--%>
<%--		<div class="container-fluid">--%>
<%--			<div class="row col-lg-offset-1">--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEEN, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<input type="text" class="form-control" id="job_title_en" placeholder="" name="job_title_en" onChange='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEBN, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<input type="text" class="form-control" id="job_title_bn" placeholder="" name="job_title_bn" onChange='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<select class='form-control'  name='job_grade_cat' id = 'job_grade_cat' onSelect='setSearchChanged()'>--%>
<%--&lt;%&ndash;			<option value='-1'></option>&ndash;%&gt;--%>
<%--<%--%>
<%--			--%>
<%--			Options = CatDAO.getOptions(Language, "job_grade", -2);--%>
<%--%>--%>
<%--			<%=Options%>--%>
<%--</select>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<select class='form-control'  name='employment_status_cat' id = 'employment_status_cat' onSelect='setSearchChanged()'>--%>
<%--&lt;%&ndash;			<option value='-1'></option>&ndash;%&gt;--%>
<%--<%--%>
<%--			--%>
<%--			// Options = CatDAO.getOptions(Language, "employment_status", -1);--%>
<%--	        Options = CatDAO.getOptions(Language, "employment_status", -2);--%>
<%--%>--%>
<%--			<%=Options%>--%>
<%--</select>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYEEPAYSCALETYPE, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<select class='form-control'  name='employee_pay_scale_type' id = 'employee_pay_scale_type' onSelect='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;&lt;%&ndash;%>--%>
<%--&lt;%&ndash;			Options = CommonDAO.getEmpPayScale(-1, Language);&ndash;%&gt;--%>
<%--&lt;%&ndash;%>&ndash;%&gt;--%>
<%--&lt;%&ndash;			<%=Options%>&ndash;%&gt;--%>
<%--&lt;%&ndash;</select>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBPURPOSE, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="job_purpose" placeholder="" name="job_purpose" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBRESPONSIBILITIES, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="job_responsibilities" placeholder="" name="job_responsibilities" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINIMUMACADEMICQUALIFICATION, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="minimum_academic_qualification" placeholder="" name="minimum_academic_qualification" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINIMUMEXPERIENCEREQUIRED, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="minimum_experience_required" placeholder="" name="minimum_experience_required" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCERTIFICATIONANDTRANING, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="required_certification_and_traning" placeholder="" name="required_certification_and_traning" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_PERSONALCHARACTERISTICS, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="personal_characteristics" placeholder="" name="personal_characteristics" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCOMPETENCE, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="required_competence" placeholder="" name="required_competence" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EDUCATIONLEVELTYPE, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<select class='form-control'  name='education_level_type' id = 'education_level_type' onSelect='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;&lt;%&ndash;%>--%>
<%--&lt;%&ndash;//			 Options = CommonDAO.getOptions(Language, "select", "education_level", "educationLevelType_select_" + row, "form-control", "educationLevelType", "any" );&ndash;%&gt;--%>
<%--&lt;%&ndash;	Options = CommonDAO.getOptions(Language, "select", "education_level", "educationLevelType_select_" + row, "form-control", "educationLevelType" );&ndash;%&gt;--%>
<%--&lt;%&ndash;%>&ndash;%&gt;--%>
<%--&lt;%&ndash;			<%=Options%>&ndash;%&gt;--%>
<%--&lt;%&ndash;</select>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINEXPERIENCE, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="min_experience" placeholder="" name="min_experience" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEREGULAR, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="max_age_on_boundary_date_regular" placeholder="" name="max_age_on_boundary_date_regular" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="max_age_on_boundary_date_ff" placeholder="" name="max_age_on_boundary_date_ff" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBLOCATION, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control" id="job_location" placeholder="" name="job_location" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAPPLICATIONDATE, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<input type="text" class="form-control formRequired datepicker" id="last_application_date_start" placeholder="" name="last_application_date_start" onChange='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAPPLICATIONDATE, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<input type="text" class="form-control formRequired datepicker" id="last_application_date_end" placeholder="" name="last_application_date_end" onChange='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAGECALCULATIONDATE, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control formRequired datepicker" id="last_age_calculation_date_start" placeholder="" name="last_age_calculation_date_start" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-2 col-md-4 col-md-4">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<label for="" class="control-label pull-right"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAGECALCULATIONDATE, loginDTO)%></label>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;					<div class="col-xs-10 col-md-8 col-md-8">&ndash;%&gt;--%>
<%--&lt;%&ndash;						<input type="text" class="form-control formRequired datepicker" id="last_age_calculation_date_end" placeholder="" name="last_age_calculation_date_end" onChange='setSearchChanged()'>&ndash;%&gt;--%>
<%--&lt;%&ndash;					</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;				</div>&ndash;%&gt;--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_ONLINEJOBPOSTINGDATE, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<input type="text" class="form-control formRequired datepicker" id="online_job_posting_date_start" placeholder="" name="online_job_posting_date_start" onChange='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_ONLINEJOBPOSTINGDATE, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<input type="text" class="form-control formRequired datepicker" id="online_job_posting_date_end" placeholder="" name="online_job_posting_date_end" onChange='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>

<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.HM_FROM, loginDTO)%> <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_INSERTIONDATE, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<input type="text" class="form-control formRequired datepicker" id="insertion_date_start" placeholder="" name="insertion_date_start" onChange='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" >--%>
<%--					<div class="col-xs-2 col-md-4 col-md-4">--%>
<%--						<label for="" class="control-label pull-right"><%=LM.getText(LC.HM_TO, loginDTO)%> <%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_INSERTIONDATE, loginDTO)%></label>--%>
<%--					</div>--%>
<%--					<div class="col-xs-10 col-md-8 col-md-8">--%>
<%--						<input type="text" class="form-control formRequired datepicker" id="insertion_date_end" placeholder="" name="insertion_date_end" onChange='setSearchChanged()'>--%>
<%--					</div>--%>
<%--				</div>--%>

<%--			</div>	--%>


<%--			<div class=clearfix></div>--%>

<%--			<div class="form-actions fluid" style="margin-top:10px">--%>
<%--				<div class="container-fluid">--%>
<%--					<div class="row">--%>
<%--						<div class="col-lg-offset-3 col-xs-12 col-md-12  col-md-12 col-lg-9">							--%>
<%--							<div class="col-xs-4  col-md-4  col-md-6">--%>
<%--								<input type="hidden" name="search" value="yes" />--%>
<%--								<!-- 				          	<input type="reset" class="btn  btn-sm btn btn-circle  grey-mint btn-outline sbold uppercase" value="Reset" > -->--%>
<%--								<input type="submit" onclick="allfield_changed('',0)"--%>
<%--									class="btn  btn-sm btn btn-circle btn-sm green-meadow btn-outline sbold uppercase advanceseach"--%>
<%--									value="<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>">--%>
<%--							</div>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--			</div>--%>
<%--			--%>
<%--		</div>--%>
<%--		<!-- END FORM-->--%>
<%--	</div>--%>


<%@include file="../common/pagination_with_go2.jsp" %>


<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        //alert(params);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "PublicServlet?actionType=public_search&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;

        params += '&job_title_en=' + $('#job_title_en').val();
        params += '&job_title_bn=' + $('#job_title_bn').val();
        var jobGradeCat = $("#job_grade_cat").val();
        if (jobGradeCat) {
            params += '&job_grade_cat=' + $("#job_grade_cat").val();
        }
        var empStatusCat = $("#employment_status_cat").val();
        if (empStatusCat) {
            params += '&employment_status_cat=' + $("#employment_status_cat").val();
        }
        // if($("#employee_pay_scale_type").val() != -1)
        // {
        // 	params +=  '&employee_pay_scale_type='+ $("#employee_pay_scale_type").val();
        // }
        // params +=  '&job_purpose='+ $('#job_purpose').val();
        // params +=  '&job_responsibilities='+ $('#job_responsibilities').val();
        // params +=  '&minimum_academic_qualification='+ $('#minimum_academic_qualification').val();
        // params +=  '&minimum_experience_required='+ $('#minimum_experience_required').val();
        // params +=  '&required_certification_and_traning='+ $('#required_certification_and_traning').val();
        // params +=  '&personal_characteristics='+ $('#personal_characteristics').val();
        // params +=  '&required_competence='+ $('#required_competence').val();
        // if($("#education_level_type").val() != 0)
        // {
        // 	params +=  '&education_level_type='+ $("#education_level_type").val();
        // }
        // params +=  '&min_experience='+ $('#min_experience').val();
        // params +=  '&max_age_on_boundary_date_regular='+ $('#max_age_on_boundary_date_regular').val();
        // params +=  '&max_age_on_boundary_date_ff='+ $('#max_age_on_boundary_date_ff').val();
        // params +=  '&job_location='+ $('#job_location').val();
        // params +=  '&last_application_date_start='+ getBDFormattedDate('last_application_date_start');
        // params +=  '&last_application_date_end='+ getBDFormattedDate('last_application_date_end');
        // params +=  '&last_age_calculation_date_start='+ getBDFormattedDate('last_age_calculation_date_start');
        // params +=  '&last_age_calculation_date_end='+ getBDFormattedDate('last_age_calculation_date_end');
        // params +=  '&online_job_posting_date_start='+ getBDFormattedDate('online_job_posting_date_start');
        // params +=  '&online_job_posting_date_end='+ getBDFormattedDate('online_job_posting_date_end');
        // params +=  '&insertion_date_start='+ getBDFormattedDate('insertion_date_start');
        // params +=  '&insertion_date_end='+ getBDFormattedDateWithOneDayAddition('insertion_date_end');

        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
            params += "&" + param.getAttribute("tag") + "=" + param.value;
        })

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }

</script>

