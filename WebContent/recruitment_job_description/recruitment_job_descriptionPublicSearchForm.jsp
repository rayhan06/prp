<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="recruitment_job_description.*" %>
<%@ page import="util.RecordNavigator" %>

<%@ page language="java" %>
<%@ page import="java.text.SimpleDateFormat" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="parliament_job_applicant.Parliament_job_applicantDTO" %>
<%@ page import="parliament_job_applicant.Parliament_job_applicantDAO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDAO" %>
<%@ page import="job_applicant_application.Job_applicant_applicationDTO" %>
<%@ page import="job_applicant_documents.Job_applicant_documentsDTO" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="config.GlobalConfigurationRepository" %>
<%@ page import="config.GlobalConfigConstants" %>


<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String failureMessage = (String) request.getAttribute("failureMessage");
    if (failureMessage == null || failureMessage.isEmpty()) {
        failureMessage = "";
    }

    String value = "";
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = (Recruitment_job_descriptionDAO) request.getAttribute("recruitment_job_descriptionDAO");


// getting job applicant data
    Parliament_job_applicantDAO parliament_job_applicantDAO = new Parliament_job_applicantDAO();
    List<Parliament_job_applicantDTO> job_applicantDTOList = parliament_job_applicantDAO.
            getAllParliament_job_applicantByUserId(loginDTO.userID);
    System.out.println(loginDTO.userID);
    long jobApplicantId = -1;
    if (job_applicantDTOList.size() > 0) {
        jobApplicantId = job_applicantDTOList.get(0).iD;
    }


// get job app app data

    Job_applicant_applicationDAO job_applicant_applicationDAO = new Job_applicant_applicationDAO();
    List<Job_applicant_applicationDTO> job_applicant_applicationDTOS = job_applicant_applicationDAO.
            getDTOByJobApplicantID(jobApplicantId);

    Map<Long, Job_applicant_applicationDTO> job_applicant_applicationDTOMap = new HashMap(
            job_applicant_applicationDTOS.stream().collect(Collectors.toMap(s -> s.jobId, s -> s)));


    String navigator2 = SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION;
    System.out.println("navigator2 = " + navigator2);
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    System.out.println("rn2 = " + rn2);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    String successMessageForwarded = "Forwarded to your Senior Office";
    String successMessageApproved = "Approval Done";

    String ajax = request.getParameter("ajax");
    boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

    if (hasAjax == false) {
        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            if (!paramName.equalsIgnoreCase("actionType")) {
                String[] paramValues = request.getParameterValues(paramName);
                for (int i = 0; i < paramValues.length; i++) {
                    String paramValue = paramValues[i];

%>

<%

                }
            }


        }
    }

%>


<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_JOBTITLE, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBTITLEBN, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBGRADECAT, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYMENTSTATUSCAT, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EMPLOYEEPAYSCALETYPE, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBPURPOSE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBRESPONSIBILITIES, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINIMUMACADEMICQUALIFICATION, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINIMUMEXPERIENCEREQUIRED, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCERTIFICATIONANDTRANING, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_PERSONALCHARACTERISTICS, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_REQUIREDCOMPETENCE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_EDUCATIONLEVELTYPE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MINEXPERIENCE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEREGULAR, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_MAXAGEONBOUNDARYDATEFF, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_JOBLOCATION, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_NUMBEROFVACANCY, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FIRST_APPLICATION_DATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAPPLICATIONDATE, loginDTO)%>
            </th>
            <th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_LASTAGECALCULATIONDATE, loginDTO)%>
            </th>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_ONLINEJOBPOSTINGDATE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_ADD_FILESDROPZONE, loginDTO)%></th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_VIEW_CIRCULAR, loginDTO)%></th>--%>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
            </th>
            <%--								<th>Action</th>--%>
            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_APPLY, loginDTO)%></th>--%>

            <%--								<th><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_RECRUITMENT_JOB_DESCRIPTION_EDIT_BUTTON, loginDTO)%></th>--%>
            <%--								<th>History</th>--%>
            <%--								<th>Send to Approval Path</th>--%>
            <%--								<th><input type="submit" class="btn btn-xs btn-danger" value="<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_RECRUITMENT_JOB_DESCRIPTION_DELETE_BUTTON, loginDTO)%>" /></th>--%>


        </tr>
        </thead>
        <tbody>
        <%
            ArrayList data = (ArrayList) session.getAttribute(SessionConstants.VIEW_RECRUITMENT_JOB_DESCRIPTION);


            long internShipId = Long.parseLong(GlobalConfigurationRepository.
                    getGlobalConfigDTOByID(GlobalConfigConstants.INTERNSHIP_ID).value);

            try {

                if (data != null) {
                    int size = data.size();
                    System.out.println("data not null and size = " + size + " data = " + data);
                    for (int i = 0; i < size; i++) {
                        Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO) data.get(i);
                        if (recruitment_job_descriptionDTO.iD == internShipId) {
                            continue;
                        }

        %>
        <tr id='tr_<%=i%>'>
            <%

            %>


            <%
                request.setAttribute("recruitment_job_descriptionDTO", recruitment_job_descriptionDTO);
                request.setAttribute("jobApplicantId", jobApplicantId);
                Boolean alreadyApplied = false;
                if (job_applicant_applicationDTOMap.containsKey(recruitment_job_descriptionDTO.iD)) {
                    alreadyApplied = true;
                }
                request.setAttribute("alreadyApplied", alreadyApplied);
            %>

            <jsp:include page="./recruitment_job_descriptionPublicSearchRow.jsp">
                <jsp:param name="pageName" value="searchrow"/>
                <jsp:param name="rownum" value="<%=i%>"/>
            </jsp:include>


            <%

            %>
        </tr>
        <%
                    }

                    System.out.println("printing done");
                } else {
                    System.out.println("data  null");
                }
            } catch (Exception e) {
                System.out.println("JSP exception " + e);
            }
        %>


        </tbody>

    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>"/>


			