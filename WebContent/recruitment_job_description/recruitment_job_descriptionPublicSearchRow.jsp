<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_job_description.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@page import="workflow.WorkflowController" %>

<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="files.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="approval_execution_table.*" %>
<%@ page import="approval_path.*" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDAO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleDTO" %>
<%@ page import="employee_pay_scale.Employee_pay_scaleRepository" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO) request.getAttribute("recruitment_job_descriptionDTO");
    CommonDTO commonDTO = recruitment_job_descriptionDTO;
    String servletName = "Recruitment_job_descriptionServlet";

    Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
    ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
    Approval_execution_tableDTO approval_execution_tableDTO = null;
    String Message = "Done";
    approval_execution_tableDTO = (Approval_execution_tableDTO) approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("recruitment_job_description", recruitment_job_descriptionDTO.iD);

    System.out.println("recruitment_job_descriptionDTO = " + recruitment_job_descriptionDTO);

    long jobApplicantId = (Long) (request.getAttribute("jobApplicantId"));
    Boolean alreadyApplied = (Boolean) (request.getAttribute("alreadyApplied"));
    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = (Recruitment_job_descriptionDAO) request.getAttribute("recruitment_job_descriptionDAO");

    FilesDAO filesDAO = new FilesDAO();
    Employee_pay_scaleDAO employee_pay_scaleDAO = new Employee_pay_scaleDAO();

    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");


%>


<td id='<%=i%>_jobTitleEn'>
    <%
        if (Language.equalsIgnoreCase("english")) {
            value = recruitment_job_descriptionDTO.jobTitleEn + "";
        } else {
            value = recruitment_job_descriptionDTO.jobTitleBn + "";
        }

//												System.out.println("##########");
//												System.out.println(Language);
//												System.out.println("##########");
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<%--											<td id = '<%=i%>_jobTitleBn'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.jobTitleBn + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td id='<%=i%>_jobGradeCat'>
    <%
        value = recruitment_job_descriptionDTO.jobGradeCat + "";
    %>
    <%
        value = CatDAO.getName(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<%--											<td id = '<%=i%>_employmentStatusCat'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.employmentStatusCat + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = CatDAO.getName(Language, "employment_status", recruitment_job_descriptionDTO.employmentStatusCat);--%>
<%--											%>	--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td id='<%=i%>_employeePayScaleType'>
    <%
        value = "";
        Employee_pay_scaleDTO employee_pay_scaleDTO = Employee_pay_scaleRepository.getInstance().getEmployee_pay_scaleDTOByID(recruitment_job_descriptionDTO.employeePayScaleType);

//												System.out.println("#####");
//												System.out.println(employee_pay_scaleDTO);
//												System.out.println("#####");

        if (employee_pay_scaleDTO != null) {
            value = CatDAO.getName(Language, "national_pay_scale_type", employee_pay_scaleDTO.nationalPayScaleCat);

        }
        //												long payScaleTypeId = recruitment_job_descriptionDTO.employeePayScaleType;
//												Employee_pay_scaleDTO employee_pay_scaleDTO =
//								.0						employee_pay_scaleDAO.getDTOByID(payScaleTypeId);
//												String payScaleName = "";
//												if(employee_pay_scaleDTO != null){
//													payScaleName = employee_pay_scaleDTO.payScaleBrief;
//												}
    %>


    <%=value%>


</td>


<%--											<td id = '<%=i%>_jobPurpose'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.jobPurpose + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_jobResponsibilities'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.jobResponsibilities + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_minimumAcademicQualification'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.minimumAcademicQualification + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_minimumExperienceRequired'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.minimumExperienceRequired + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_requiredCertificationAndTraning'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.requiredCertificationAndTraning + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_personalCharacteristics'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.personalCharacteristics + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_requiredCompetence'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.requiredCompetence + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%--											<td id = '<%=i%>_educationLevelType'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.educationLevelType + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--												if(recruitment_job_descriptionDTO.educationLevelType == 100){--%>
<%--													value = "";--%>
<%--												} else {--%>
<%--													value = CommonDAO.getName(Integer.parseInt(value), "education_level", Language.equals("English")?"name_en":"name_bn", "id");--%>
<%--												}--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%--											<td id = '<%=i%>_minExperience'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.minExperience + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											value = String.format("%.1f", recruitment_job_descriptionDTO.minExperience);--%>
<%--											%>												--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_maxAgeOnBoundaryDateRegular'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_maxAgeOnBoundaryDateFf'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											--%>
<%--											<td id = '<%=i%>_jobLocation'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.jobLocation + "";--%>
<%--											%>--%>
<%--														--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<td id='<%=i%>_numberOfVacancy'>
    <%
        value = recruitment_job_descriptionDTO.numberOfVacancy + "";
    %>

    <%=Utils.getDigits(value, Language)%>


</td>


<td id='<%=i%>_firstApplicationDate'>
    <%
        value = recruitment_job_descriptionDTO.firstApplicationDate + "";
    %>
    <%
        String formatted_firstApplicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_firstApplicationDate, Language)%>


</td>


<td id='<%=i%>_lastApplicationDate'>
    <%
        value = recruitment_job_descriptionDTO.lastApplicationDate + "";
    %>
    <%
        String formatted_lastApplicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_lastApplicationDate, Language)%>


</td>


<td id='<%=i%>_lastAgeCalculationDate'>
    <%
        value = recruitment_job_descriptionDTO.lastAgeCalculationDate + "";
    %>
    <%
        String formatted_lastAgeCalculationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
    %>
    <%=Utils.getDigits(formatted_lastAgeCalculationDate, Language)%>


</td>


<%--											<td id = '<%=i%>_onlineJobPostingDate'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.onlineJobPostingDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_onlineJobPostingDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_onlineJobPostingDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%--											<td id = '<%=i%>_filesDropzone'>--%>
<%--											<%--%>
<%--											value = recruitment_job_descriptionDTO.filesDropzone + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											{--%>
<%--												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(recruitment_job_descriptionDTO.filesDropzone);--%>
<%--												%>--%>
<%--												<table>--%>
<%--												<tr>--%>
<%--												<%--%>
<%--												if(FilesDTOList != null)--%>
<%--												{--%>
<%--													for(int j = 0; j < FilesDTOList.size(); j ++)--%>
<%--													{--%>
<%--														FilesDTO filesDTO = FilesDTOList.get(j);--%>
<%--														byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);--%>
<%--														%>--%>
<%--														<td>--%>
<%--														<%--%>
<%--														if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)--%>
<%--														{--%>
<%--															%>--%>
<%--															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />--%>
<%--															<%--%>
<%--														}--%>
<%--														%>--%>
<%--														<a href = 'Recruitment_job_descriptionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>--%>
<%--														</td>--%>
<%--													<%--%>
<%--													}--%>
<%--												}--%>
<%--												%>--%>
<%--												</tr>--%>
<%--												</table>--%>
<%--											<%												--%>
<%--											}--%>
<%--											%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>


<%--												<td id='<%=i%>_view_circular'>--%>
<%--													<%--%>
<%--														value = recruitment_job_descriptionDTO.filesDropzone + "";--%>
<%--													%>--%>
<%--													<%--%>
<%--														{--%>
<%--															List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(recruitment_job_descriptionDTO.filesDropzone);--%>
<%--													%>--%>
<%--													<ul>--%>
<%--														<%--%>
<%--															if (FilesDTOList != null) {--%>
<%--																for (int j = 0; j < FilesDTOList.size(); j++) {--%>
<%--																	FilesDTO filesDTO = FilesDTOList.get(j); %>--%>
<%--														<li>--%>
<%--															<a onclick="window.open(this.href,'_blank');return false;"--%>
<%--															   href='Recruitment_job_descriptionServlet?actionType=previewDropzoneFile&id=<%=filesDTO.iD%>'--%>
<%--															><%=filesDTO.fileTitle%>--%>
<%--															</a>--%>
<%--															&lt;%&ndash;									<a href='Edms_documentsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download>&ndash;%&gt;--%>
<%--															&lt;%&ndash;										<ul>(<%=LM.getText(LC.EDMS_DOCUMENTS_DOWNLOAD, loginDTO)%>)</ul>&ndash;%&gt;--%>
<%--															&lt;%&ndash;									</a>&ndash;%&gt;--%>
<%--														</li>--%>
<%--														<%--%>
<%--																}--%>
<%--															}--%>
<%--														%>--%>
<%--													</ul>--%>
<%--													<%}%>--%>
<%--												</td>--%>


<td>
	<button
			type="button"
			class="btn-sm border-0 shadow bg-light btn-border-radius"
			style="color: #ff6b6b;"
			onclick="location.href='Recruitment_job_descriptionServlet?actionType=view&ID=<%=recruitment_job_descriptionDTO.iD%>'">
		<i class="fa fa-eye"></i>
	</button>
</td>

<%--                                            <td>--%>
<%--												<% if(alreadyApplied){ %>--%>
<%--												<a  href= ''>--%>
<%--&lt;%&ndash;													Payment&ndash;%&gt;--%>
<%--													<%=LM.getText(LC.PUBLIC_VIEW_PAYMENT, loginDTO)%>--%>
<%--												</a>--%>

<%--												<% } else { %>--%>
<%--												<a  href= 'Parliament_job_applicantServlet?actionType=editMultiForm&tab=1&ID=<%=jobApplicantId%>&jobId=<%=recruitment_job_descriptionDTO.iD%>'>--%>
<%--													<%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_APPLY, loginDTO)%>--%>
<%--												</a>--%>

<%--												<% } %>--%>


<%--                                            </td>--%>
<%--	--%>
<%--											<td id = '<%=i%>_Edit'>																																	--%>
<%--	--%>
<%--												<a href='Recruitment_job_descriptionServlet?actionType=getEditPage&ID=<%=recruitment_job_descriptionDTO.iD%>'><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_RECRUITMENT_JOB_DESCRIPTION_EDIT_BUTTON, loginDTO)%></a>--%>
<%--																				--%>
<%--											</td>											--%>


<%--											<td>--%>
<%--											<%--%>
<%--												if(recruitment_job_descriptionDTO.isDeleted == 0 && approval_execution_tableDTO != null)--%>
<%--												{--%>
<%--											%>--%>
<%--												<a href="Approval_execution_tableServlet?actionType=search&tableName=recruitment_job_description&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"  ><%=LM.getText(LC.HM_HISTORY, loginDTO)%></a>--%>
<%--											<%--%>
<%--												}--%>
<%--												else--%>
<%--												{--%>
<%--											%>--%>
<%--												<%=LM.getText(LC.HM_NO_HISTORY_IS_AVAILABLE, loginDTO)%>--%>
<%--											<%--%>
<%--												}--%>
<%--											%>--%>
<%--											</td>--%>

<%--											<td>--%>
<%--											<%--%>
<%--											if(recruitment_job_descriptionDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT)--%>
<%--											{--%>
<%--											%>--%>
<%--												<button type="button" class="btn btn-success" data-toggle="modal" data-target="#sendToApprovalPathModal" >--%>
<%--													<%=LM.getText(LC.HM_SEND_TO_APPROVAL_PATH, loginDTO)%>--%>
<%--												</button>--%>
<%--												<%@include file="../inbox_internal/sendToApprovalPathModal.jsp"%>--%>
<%--											<%--%>
<%--											}--%>
<%--											else--%>
<%--											{--%>
<%--											%>--%>
<%--											<%=LM.getText(LC.HM_NO_ACTION_IS_REQUIRED, loginDTO)%>--%>
<%--											<%--%>
<%--											}--%>
<%--											%>--%>
<%--											</td>--%>
<%--											<td id='<%=i%>_checkbox'>--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=recruitment_job_descriptionDTO.iD%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>


<script>
    window.onload = function () {
        console.log("using ckEditor");
        CKEDITOR.replaceAll();
    }

</script>	

