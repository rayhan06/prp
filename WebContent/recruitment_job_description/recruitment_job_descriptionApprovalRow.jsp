<%@page pageEncoding="UTF-8" %>

<%@page import="recruitment_job_description.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>


<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@ page import="approval_execution_table.*"%>
<%@ page import="approval_path.*"%>
<%@ page import="workflow.*"%>
<%@page import="dbm.*" %>
<%@page import="holidays.*" %>
<%@page import="approval_summary.*" %>


<%
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
String Language = LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_EDIT_LANGUAGE, loginDTO);

UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


String navigator2 = SessionConstants.NAV_RECRUITMENT_JOB_DESCRIPTION;
RecordNavigator rn2 = (RecordNavigator)session.getAttribute(navigator2);
boolean isPermanentTable = rn2.m_isPermanentTable;
String tableName = rn2.m_tableName;

System.out.println("isPermanentTable = " + isPermanentTable);
Recruitment_job_descriptionDTO recruitment_job_descriptionDTO = (Recruitment_job_descriptionDTO)request.getAttribute("recruitment_job_descriptionDTO");

Approval_execution_tableDAO approval_execution_tableDAO = new Approval_execution_tableDAO();
Approval_summaryDAO approval_summaryDAO = new Approval_summaryDAO();
ApprovalPathDetailsDAO approvalPathDetailsDAO = new ApprovalPathDetailsDAO();
Approval_execution_tableDTO approval_execution_tableDTO = null;
ApprovalPathDetailsDTO approvalPathDetailsDTO =  null;
boolean canApprove = false, canValidate = false, isInitiator = false, canTerminate = false;
boolean isInPreviousOffice = false;
String Message = "Done";

approval_execution_tableDTO = (Approval_execution_tableDTO)approval_execution_tableDAO.getMostRecentDTOByUpdatedRowId("recruitment_job_description", recruitment_job_descriptionDTO.iD);
Approval_summaryDTO approval_summaryDTO = approval_summaryDAO.getDTOByTableNameAndTableID("recruitment_job_description", approval_execution_tableDTO.previousRowId);
System.out.println("approval_execution_tableDTO = " + approval_execution_tableDTO);
approvalPathDetailsDTO = approvalPathDetailsDAO.getApprovalPathDetailsDTOListByApprovalPathIDandApprovalOrder(approval_execution_tableDTO.approvalPathId, approval_execution_tableDTO.approvalPathOrder);
if(approvalPathDetailsDTO!= null && approvalPathDetailsDTO.organogramId == userDTO.organogramID)
{
	canApprove = true;
	if(approvalPathDetailsDTO.approvalRoleCat == SessionConstants.VALIDATOR)
	{
		canValidate = true;
	}
}

isInitiator = WorkflowController.isInitiator(tableName, approval_execution_tableDTO.previousRowId, userDTO.organogramID);

canTerminate = isInitiator && recruitment_job_descriptionDTO.isDeleted == 2;

Approval_pathDAO approval_pathDAO = new Approval_pathDAO();
Approval_pathDTO approval_pathDTO = approval_pathDAO.getDTOByID(approval_execution_tableDTO.approvalPathId);
	

System.out.println("recruitment_job_descriptionDTO = " + recruitment_job_descriptionDTO);


int i = Integer.parseInt(request.getParameter("rownum"));
out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

String value = "";


Recruitment_job_descriptionDAO recruitment_job_descriptionDAO = (Recruitment_job_descriptionDAO)request.getAttribute("recruitment_job_descriptionDAO");

FilesDAO filesDAO = new FilesDAO();

SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

boolean isOverDue = false;

Date fromDate = new Date(approval_execution_tableDTO.lastModificationTime);
Date dueDate = null;
if(fromDate != null && approvalPathDetailsDTO!= null)
{
	dueDate = CalenderUtil.getDateAfter(fromDate, approvalPathDetailsDTO.daysRequired);
	long today = System.currentTimeMillis();
	boolean timeOver = today > dueDate.getTime();
	isOverDue  =  (approval_execution_tableDTO.approvalStatusCat == Approval_execution_tableDTO.PENDING) && timeOver;
	
	System.out.println("time dif = " + (today - dueDate.getTime()));
}
String formatted_dueDate;

System.out.println("i = " + i  + " OVERDUE = " + isOverDue);

if(isOverDue)
{
%>
<style>
#tr_<%=i%> {
  color: red;
}
</style>
<%
}
%>

											
		
											
											<td id = '<%=i%>_jobTitleEn'>
											<%
											value = recruitment_job_descriptionDTO.jobTitleEn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_jobTitleBn'>
											<%
											value = recruitment_job_descriptionDTO.jobTitleBn + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_jobGradeCat'>
											<%
											value = recruitment_job_descriptionDTO.jobGradeCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "job_grade", recruitment_job_descriptionDTO.jobGradeCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_employmentStatusCat'>
											<%
											value = recruitment_job_descriptionDTO.employmentStatusCat + "";
											%>
											<%
											value = CatDAO.getName(Language, "employment_status", recruitment_job_descriptionDTO.employmentStatusCat);
											%>	
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_employeePayScaleType'>
											<%
											value = recruitment_job_descriptionDTO.employeePayScaleType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "employee_pay_scale", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_jobPurpose'>
											<%
											value = recruitment_job_descriptionDTO.jobPurpose + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_jobResponsibilities'>
											<%
											value = recruitment_job_descriptionDTO.jobResponsibilities + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_minimumAcademicQualification'>
											<%
											value = recruitment_job_descriptionDTO.minimumAcademicQualification + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_minimumExperienceRequired'>
											<%
											value = recruitment_job_descriptionDTO.minimumExperienceRequired + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_requiredCertificationAndTraning'>
											<%
											value = recruitment_job_descriptionDTO.requiredCertificationAndTraning + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_personalCharacteristics'>
											<%
											value = recruitment_job_descriptionDTO.personalCharacteristics + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_requiredCompetence'>
											<%
											value = recruitment_job_descriptionDTO.requiredCompetence + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_educationLevelType'>
											<%
											value = recruitment_job_descriptionDTO.educationLevelType + "";
											%>
											<%
											value = CommonDAO.getName(Integer.parseInt(value), "education_level", Language.equals("English")?"name_en":"name_bn", "id");
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_minExperience'>
											<%
											value = recruitment_job_descriptionDTO.minExperience + "";
											%>
											<%
											value = String.format("%.1f", recruitment_job_descriptionDTO.minExperience);
											%>												
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_maxAgeOnBoundaryDateRegular'>
											<%
											value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateRegular + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_maxAgeOnBoundaryDateFf'>
											<%
											value = recruitment_job_descriptionDTO.maxAgeOnBoundaryDateFf + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_jobLocation'>
											<%
											value = recruitment_job_descriptionDTO.jobLocation + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_numberOfVacancy'>
											<%
											value = recruitment_job_descriptionDTO.numberOfVacancy + "";
											%>
														
											<%=Utils.getDigits(value, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_lastApplicationDate'>
											<%
											value = recruitment_job_descriptionDTO.lastApplicationDate + "";
											%>
											<%
											String formatted_lastApplicationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_lastApplicationDate, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_lastAgeCalculationDate'>
											<%
											value = recruitment_job_descriptionDTO.lastAgeCalculationDate + "";
											%>
											<%
											String formatted_lastAgeCalculationDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_lastAgeCalculationDate, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_onlineJobPostingDate'>
											<%
											value = recruitment_job_descriptionDTO.onlineJobPostingDate + "";
											%>
											<%
											String formatted_onlineJobPostingDate = simpleDateFormat.format(new Date(Long.parseLong(value)));
											%>
											<%=Utils.getDigits(formatted_onlineJobPostingDate, Language)%>
				
			
											</td>
		
											
											<td id = '<%=i%>_filesDropzone'>
											<%
											value = recruitment_job_descriptionDTO.filesDropzone + "";
											%>
											<%
											{
												List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(recruitment_job_descriptionDTO.filesDropzone);
												%>
												<table>
												<tr>
												<%
												if(FilesDTOList != null)
												{
													for(int j = 0; j < FilesDTOList.size(); j ++)
													{
														FilesDTO filesDTO = FilesDTOList.get(j);
														byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
														%>
														<td>
														<%
														if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)
														{
															%>
															<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />
															<%
														}
														%>
														<a href = 'Recruitment_job_descriptionServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>
														</td>
													<%
													}
												}
												%>
												</tr>
												</table>
											<%												
											}
											%>
				
			
											</td>
		
											
		
											
		
											
		
											
		
											
		
											
		
	

<td>
											<a href='Recruitment_job_descriptionServlet?actionType=view&ID=<%=recruitment_job_descriptionDTO.iD%>'&isPermanentTable=false>View</a>
											
											<a href='#' data-toggle='modal' data-target='#viedFileModal_<%=i%>'>Modal</a>
											
											<div class='modal fade' id='viedFileModal_<%=i%>'>
											  <div class='modal-dialog modal-lg' role='document'>
											    <div class='modal-content'>
											      <div class='modal-body'>
											        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
											          <span aria-hidden='true'>&times;</span>
											        </button>											        
											        
											        <object type='text/html' data='Recruitment_job_descriptionServlet?actionType=view&modal=1&ID=<%=recruitment_job_descriptionDTO.iD%>' width='100%' height='500' style='height: 85vh;'>No Support</object>
											        
											      </div>
											    </div>
											  </div>
											</div>
											</td>
											
											<td>
												<%
												value = approval_pathDTO.nameEn;
												%>											
															
												<%=value%>
											</td>
											
											<td>
												<%
												value = WorkflowController.getNameFromUserId(approval_summaryDTO.initiator, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td id = '<%=i%>_dateOfInitiation'>
												<%
												value = approval_summaryDTO.dateOfInitiation + "";
												%>
												<%

												String formatted_dateOfInitiation = simpleDateFormat.format(new Date(Long.parseLong(value)));
												%>
												<%=formatted_dateOfInitiation%>
				
			
											</td>
											
											
											<td>
												<%
												value = WorkflowController.getNameFromOrganogramId(approval_summaryDTO.assignedTo, Language);
												%>											
															
												<%=value%>
											</td>
											
											<td >
												<%
												String formatted_dateOfAssignment = simpleDateFormat.format(new Date(approval_execution_tableDTO.lastModificationTime));
												%>
												<%=formatted_dateOfAssignment%>
				
			
											</td>
											
											<td>
											
												<%
												
												
												if(dueDate!= null)
												{
													formatted_dueDate = simpleDateFormat.format(dueDate);
												}
												else
												{
													formatted_dueDate = "";
												}
												%>
												<%=formatted_dueDate%>
				
			
											</td>
		
											
											
											<td>
												<%
												if(isOverDue)
												{
													value = "OVERDUE";
												}
												else
												{
													value = CatDAO.getName(Language, "approval_status", approval_summaryDTO.approvalStatusCat);
												}
												
												%>											
															
												<%=value%>
											</td>
	
											<td>
											<%
											if(canApprove || canTerminate)
											{
												%>
												<a href='Recruitment_job_descriptionServlet?actionType=view&ID=<%=recruitment_job_descriptionDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'>View</a>
												
												<%
											}
											else
											{
											
											 	%>
											 	No Action is required.
											 	<%
											}
											 %>																						
											</td>
											
											<td>
											<%
											if(canValidate)
											{
												%>
												<a href='Recruitment_job_descriptionServlet?actionType=getEditPage&ID=<%=recruitment_job_descriptionDTO.iD%>&isPermanentTable=<%=isPermanentTable%>'>View</a>
												
												<%
											}
											else
											{
											
											 	%>
											 	No Action is required.
											 	<%
											}
											 %>																						
											</td>
											
											
											<td>
												<a href="Approval_execution_tableServlet?actionType=search&tableName=recruitment_job_description&previousRowId=<%=approval_execution_tableDTO.previousRowId%>"  >History</a>
											</td>
											

