<%@page pageEncoding="UTF-8" %>

<%@page import="job_applicant_education_info.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<%@page import="workflow.WorkflowController"%>

<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="files.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="util.RecordNavigator"%>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%@ page import="university_info.University_infoDTO" %>
<%@ page import="university_info.University_infoDAO" %>
<%@ page import="education_level.Education_levelRepository" %>
<%@ page import="result_exam.Result_examRepository" %>
<%@ page import="board.BoardRepository" %>
<%@ page import="education_level.DegreeExamRepository" %>
<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
	String Language2 = Language;

	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);



	Job_applicant_education_infoDTO job_applicant_education_infoDTO = (Job_applicant_education_infoDTO)request.getAttribute("job_applicant_education_infoDTO");
	CommonDTO commonDTO = job_applicant_education_infoDTO;
	String servletName = "Job_applicant_education_infoServlet";
	System.out.println("job_applicant_education_infoDTO = " + job_applicant_education_infoDTO);


	int i = Integer.parseInt(request.getParameter("rownum"));
	out.println("<td style=\"  width: 20%; height: auto; border: 1px solid gray;display:none;\"><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

	String value = "";


	Job_applicant_education_infoDAO job_applicant_education_infoDAO = (Job_applicant_education_infoDAO)request.getAttribute("job_applicant_education_infoDAO");

	FilesDAO filesDAO = new FilesDAO();

	String Options = "";
	boolean formSubmit = false;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

	String lastResultEn = "";
	String lastResultTotalEn = "";
	String nameType = Language.equals("English")?"name_en":"name_bn";

	String lastEduInstituteName = job_applicant_education_infoDTO.institutionName;
//	String lastEduNameEn = CommonDAO.getName(job_applicant_education_infoDTO.educationLevelType, "education_level", nameType, "id");
	String lastEduNameEn =  Education_levelRepository.getInstance().
			getText(Language, job_applicant_education_infoDTO.educationLevelType);

	String passingYearEn = UtilCharacter.convertNumberBnToEn(String.valueOf(job_applicant_education_infoDTO.yearOfPassingNumber));
	if (!Language.equals("English")) {
		passingYearEn = UtilCharacter.convertNumberEnToBn(String.valueOf(job_applicant_education_infoDTO.yearOfPassingNumber));
	}

	if (job_applicant_education_infoDTO.resultExamType != 4) {

		lastResultEn = Result_examRepository.getInstance().getText(Language, job_applicant_education_infoDTO.resultExamType);
//				CommonDAO.getName(job_applicant_education_infoDTO.resultExamType, "result_exam", nameType, "id");
	}
	else {
		lastResultTotalEn = CatRepository.getName(Language, "grade_point", job_applicant_education_infoDTO.gradePointCat);

		if (Language.equals("English")) {
			lastResultEn = UtilCharacter.convertNumberBnToEn(String.valueOf(job_applicant_education_infoDTO.cgpaNumber)) + " out of " + UtilCharacter.convertNumberBnToEn(String.valueOf(lastResultTotalEn));
		}
		else {
			lastResultEn = UtilCharacter.convertNumberEnToBn(String.valueOf(job_applicant_education_infoDTO.cgpaNumber)) + " (" + UtilCharacter.convertNumberEnToBn(String.valueOf(lastResultTotalEn)) + " এর মধ্যে)";
		}
	}

	String resultString = "";
	if (!(lastEduInstituteName.isEmpty() && lastEduNameEn.isEmpty() && lastResultEn.isEmpty())) {
		resultString = lastResultEn;
	}

%>
<td style="  width: 10%; height: auto; border: 1px solid gray;"  id = '<%=i%>_educationLevelType'>
<%--	<% value = job_applicant_education_infoDTO.educationLevelType + ""; %>--%>
	<% value = Education_levelRepository.getInstance().getText(Language, job_applicant_education_infoDTO.educationLevelType); %>
<%--			CommonDAO.getName(Integer.parseInt(value), "education_level", Language.equals("English")?"name_en":"name_bn", "id"); %>--%>
	<%=value%>
</td>

<td style="  width: 15%; height: auto; border: 1px solid gray;"  id = '<%=i%>_degreeExamType'>
<%--	<% value = job_applicant_education_infoDTO.degreeExamType + ""; %>--%>
	<% value = DegreeExamRepository.getInstance().getText(Language, job_applicant_education_infoDTO.degreeExamType);
	%>
<%--			CommonDAO.getName(Integer.parseInt(value), "degree_exam", Language.equals("English")?"name_en":"name_bn", "id"); %>--%>
	<%=value%>
</td>


<%--<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_otherDegreeExam'>--%>
<%--	<% value = job_applicant_education_infoDTO.otherDegreeExam + ""; %>--%>
<%--	<%=value%>--%>
<%--</td>--%>


<td style="  width: 15%; height: auto; border: 1px solid gray;"  id = '<%=i%>_major'>
	<% value = job_applicant_education_infoDTO.major + ""; %>
	<%=value%>
</td>


<td style="  width: 15%; height: auto; border: 1px solid gray;"  id = '<%=i%>_resultExamType'>
<%--	<% value = job_applicant_education_infoDTO.resultExamType + ""; %>--%>
<%--	<% value = CommonDAO.getName(Integer.parseInt(value), "result_exam", Language.equals("English")?"name_en":"name_bn", "id"); %>--%>
	<%=resultString%>
</td>


<%--<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_marksPercentage'>--%>
<%--	<% value = job_applicant_education_infoDTO.marksPercentage + ""; %>--%>
<%--	<%=value%>--%>
<%--</td>--%>

<%--<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_gradePointCat'>--%>
<%--	<%--%>
<%--		if(job_applicant_education_infoDTO.gradePointCat== null){--%>
<%--			value = "";--%>
<%--		}else{--%>
<%--			value = job_applicant_education_infoDTO.gradePointCat + "";--%>
<%--			value = CatDAO.getName(Language, "grade_point", job_applicant_education_infoDTO.gradePointCat);--%>
<%--		}--%>
<%--	%>--%>
<%--	<%=value%>--%>
<%--</td>--%>


<%--<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_cgpaNumber'>--%>
<%--	<%--%>
<%--		if(job_applicant_education_infoDTO.cgpaNumber == null){--%>
<%--			value = "";--%>
<%--		}else{--%>
<%--			value = job_applicant_education_infoDTO.cgpaNumber + "";--%>
<%--		}--%>
<%--	%>--%>
<%--	<%=value%>--%>
<%--</td>--%>


<td style="  width: 10%; height: auto; border: 1px solid gray;"  id = '<%=i%>_yearOfPassingNumber'>
	<% value = job_applicant_education_infoDTO.yearOfPassingNumber + ""; %>
	<%=UtilCharacter.convertDataByLanguage(Language, value)%>
</td>


<td style="  width: 10%; height: auto; border: 1px solid gray;"  id = '<%=i%>_boardType'>
<%--	<% value = job_applicant_education_infoDTO.boardType + ""; %>--%>
	<% value = BoardRepository.getInstance().getText(Language, job_applicant_education_infoDTO.boardType); %>
<%--			CommonDAO.getName(Integer.parseInt(value), "board", Language.equals("English")?"name_en":"name_bn", "id"); %>--%>
	<%=value%>
</td>


<td style="  width: 25%; height: auto; border: 1px solid gray;"  id = '<%=i%>_institutionName'>
	<%


		if(job_applicant_education_infoDTO.institution_id != 0 && job_applicant_education_infoDTO.institution_id != -100){
//			value = "";
			University_infoDTO university_infoDTO = new University_infoDAO().getDTOByID(job_applicant_education_infoDTO.institution_id);
			value = UtilCharacter.getDataByLanguage(Language, university_infoDTO.universityNameBn, university_infoDTO.universityNameEn);
		} else {
			value = job_applicant_education_infoDTO.institutionName + "";
		}
	%>

	<%=value%>
</td>


<%--<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_isForeign'>--%>
<%--	<% value = job_applicant_education_infoDTO.isForeign + ""; %>--%>
<%--	<%=value%>--%>
<%--</td>--%>


<%--<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_durationYears'>--%>
<%--	<% value = job_applicant_education_infoDTO.durationYears + ""; %>--%>
<%--	<%=value%>--%>
<%--</td>--%>


<%--<td style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_achievement'>--%>
<%--	<% value = job_applicant_education_infoDTO.achievement + ""; %>--%>
<%--	<%=value%>--%>
<%--</td>--%>


<%--<td class="filesColumn" style="  width: 20%; height: auto; border: 1px solid gray;"  id = '<%=i%>_filesDropzone'>--%>
<%--	<% value = job_applicant_education_infoDTO.filesDropzone + ""; %>--%>
<%--	<%--%>
<%--		{--%>
<%--			List<FilesDTO> FilesDTOList = filesDAO.getMiniDTOsByFileID(job_applicant_education_infoDTO.filesDropzone);--%>
<%--	%>--%>
<%--	<table>--%>
<%--		<tr>--%>
<%--			<%--%>
<%--				if(FilesDTOList != null)--%>
<%--				{--%>
<%--					for(int j = 0; j < FilesDTOList.size(); j ++)--%>
<%--					{--%>
<%--						FilesDTO filesDTO = FilesDTOList.get(j);--%>
<%--						byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);--%>
<%--			%>--%>
<%--			<td>--%>
<%--				<%--%>
<%--					if(filesDTO.fileTypes.contains("image") && encodeBase64!= null)--%>
<%--					{--%>
<%--				%>--%>
<%--&lt;%&ndash;				<img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>' style='width:100px' />&ndash;%&gt;--%>
<%--				<%--%>
<%--					}--%>
<%--				%>--%>
<%--				<a href = 'Job_applicant_education_infoServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>' download><%=filesDTO.fileTitle%></a>--%>
<%--			</td>--%>
<%--			<%--%>
<%--					}--%>
<%--				}--%>
<%--			%>--%>
<%--		</tr>--%>
<%--	</table>--%>
<%--	<%--%>
<%--		}--%>
<%--	%>--%>

<%--</td>--%>





