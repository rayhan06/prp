
<%@page import="result_exam.Result_examRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="job_applicant_education_info.*"%>
<%@ page import="util.RecordNavigator"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="java.util.List" %>
<%@ page import="common.NameDTO" %>
<%@ page import="job_applicant_application.SummaryDTO" %>
<%@ page import="java.lang.reflect.Type" %>
<%@ page import="com.google.gson.reflect.TypeToken" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="com.fasterxml.jackson.databind.JsonNode" %>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper" %>
<%@ page import="com.fasterxml.jackson.databind.node.ObjectNode" %>


<%
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	String failureMessage = (String)request.getAttribute("failureMessage");
	if(failureMessage == null || failureMessage.isEmpty())
	{
		failureMessage = "";
	}

	String value = "";
	String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
	UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


	Job_applicant_education_infoDAO job_applicant_education_infoDAO = new Job_applicant_education_infoDAO();

	String successMessageForwarded = "Forwarded to your Senior Office";
	String successMessageApproved = "Approval Done";

	String ajax = request.getParameter("ajax");
	boolean hasAjax = ajax != null && !ajax.equalsIgnoreCase("");
%>
<input type='hidden' id='failureMessage_general' value='<%=failureMessage%>'/>

<%

	if(hasAjax == false)
	{
		Enumeration<String> parameterNames = request.getParameterNames();

		while (parameterNames.hasMoreElements())
		{

			String paramName = parameterNames.nextElement();

			if(!paramName.equalsIgnoreCase("actionType"))
			{
				String[] paramValues = request.getParameterValues(paramName);
				for (int i = 0; i < paramValues.length; i++)
				{
					String paramValue = paramValues[i];

%>

<%

				}
			}


		}
	}

	String formTitle = LM.getText(LC.JOB_APPLICANT_EDUCATION_INFO_ADD_JOB_APPLICANT_EDUCATION_INFO_ADD_FORMNAME, loginDTO);

%>

<%--<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>--%>

<div class="col-lg-12">
	<div class="kt-portlet shadow-none" style="margin-top: -20px">

		<div class="kt-portlet__body form-body">


<div class="table-responsive">
	<table id="tableData" class="table table-bordered table-striped">
		<thead class="thead-light">
		<tr>
			<th style="  width: 10%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EDUCATIONLEVELTYPE, loginDTO)%></th>
			<th style="  width: 15%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO)%></th>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_OTHERDEGREEEXAM, loginDTO)%></th>--%>
			<th style="  width: 15%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MAJOR, loginDTO)%></th>
			<th style="  width: 15%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_RESULTEXAMTYPE, loginDTO)%></th>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_MARKSPERCENTAGE, loginDTO)%></th>--%>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_GRADEPOINTCAT, loginDTO)%></th>--%>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_CGPANUMBER, loginDTO)%></th>--%>
			<th style="  width: 10%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_YEAROFPASSINGNUMBER, loginDTO)%></th>
			<th style="  width: 10%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_BOARDTYPE, loginDTO)%></th>
			<th style="  width: 25%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_INSTITUTIONNAME, loginDTO)%></th>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_ISFOREIGN, loginDTO)%></th>--%>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DURATIONYEARS, loginDTO)%></th>--%>
<%--			<th style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_ACHIEVEMENT, loginDTO)%></th>--%>
<%--			<th class="filesColumn" style="  width: 20%; height: auto; border: 1px solid gray;"><%=LM.getText(LC.RECRUITMENT_JOB_DESCRIPTION_SEARCH_FILESDROPZONE, loginDTO)%></th>--%>


		</tr>
		</thead>
		<tbody>
		<%
			List<Job_applicant_education_infoDTO> data = new ArrayList<>();

			if (request.getParameter("summary") != null && Boolean.parseBoolean(request.getParameter("summary"))) {

				List<Job_applicant_education_infoSummaryDTO> summaryData = new ArrayList<>();

				Gson gson = new Gson();
				summaryData = (List<Job_applicant_education_infoSummaryDTO>)request.getSession(true).getAttribute("job_applicant_education_infoSummaryDTOList");

				if (summaryData != null) {
					for (Job_applicant_education_infoSummaryDTO summary: summaryData) {

						String json = gson.toJson(summary);
						Job_applicant_education_infoDTO dto = gson.fromJson(json, Job_applicant_education_infoDTO.class);
						data.add(dto);
					}
				}

			}

			else {
				data = job_applicant_education_infoDAO.getDTOByJobApplicantID(Long.parseLong(request.getParameter("ID")));
			}

			try
			{

				if (data != null)
				{
					int size = data.size();
					System.out.println("data not null and size = " + size + " data = " + data);
					for (int i = 0; i < size; i++)
					{
						Job_applicant_education_infoDTO job_applicant_education_infoDTO = data.get(i);
						NameDTO dto = Result_examRepository.getInstance().getDTOByID(job_applicant_education_infoDTO.resultExamType);
						if(!dto.nameEn.equals("Grade")){
							job_applicant_education_infoDTO.gradePointCat = null;
							job_applicant_education_infoDTO.cgpaNumber = null;
						}
		%>
		<tr id = 'tr_<%=i%>'>
			<%

			%>


			<%
				request.setAttribute("job_applicant_education_infoDTO",job_applicant_education_infoDTO);
			%>

			<jsp:include page="./job_applicant_education_infoSummarySearchRow.jsp">
				<jsp:param name="pageName" value="searchrow" />
				<jsp:param name="rownum" value="<%=i%>" />
			</jsp:include>


			<%

			%>
		</tr>
		<%
					}

					System.out.println("printing done");
				}
				else
				{
					System.out.println("data  null");
				}
			}
			catch(Exception e)
			{
				System.out.println("JSP exception " + e);
			}
		%>



		</tbody>

	</table>
</div>

		</div>
	</div>
</div>



			