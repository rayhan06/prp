<%@page import="sessionmanager.SessionConstants"%>
<%@page import="login.LoginDTO"%>
<%@page import="job_applicant_education_info.*"%>
<%@page import="java.util.*"%>
<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.UUID"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="geolocation.GeoLocationDAO2"%>
<%@page import="files.*"%>
<%@page import="dbm.*" %>
<%@page import="util.TimeFormat"%>
<%@page import="java.time.LocalDate"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="pb.*"%>
<%@ page import="result_exam.*"%>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper"%>

<%
	String context = "../../.." + request.getContextPath() + "/assets/";
	String currentYear = String.valueOf(LocalDate.now().getYear());
	Long degreeExamType = null;

	Job_applicant_education_infoDTO job_applicant_education_infoDTO;
	job_applicant_education_infoDTO = (Job_applicant_education_infoDTO)request.getAttribute("job_applicant_education_infoDTO");
	LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
	if(job_applicant_education_infoDTO == null){
		job_applicant_education_infoDTO = new Job_applicant_education_infoDTO();
	}else{
		degreeExamType = job_applicant_education_infoDTO.degreeExamType;
	}
	System.out.println("job_applicant_education_infoDTO = " + job_applicant_education_infoDTO);

	String actionName;
	System.out.println("actionType = " + request.getParameter("actionType"));
	if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")){
		actionName = "add";
	}else{
		actionName = "edit";
	}
	String formTitle;
	if(actionName.equals("edit")){
		formTitle = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_EMPLOYEE_EDUCATION_INFO_EDIT_FORMNAME, loginDTO);
	}else{
		formTitle = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_EMPLOYEE_EDUCATION_INFO_ADD_FORMNAME, loginDTO);
	}
	List<CatDTO> catDtos = CatDAO.getDTOs("grade_point");
	ObjectMapper objectMapper = new ObjectMapper();
	String gradePointCatDtosJson = objectMapper.writeValueAsString(catDtos);
	String ID = request.getParameter("ID");
	if(ID == null || ID.isEmpty()){
		ID = "0";
	}
	System.out.println("ID = " + ID);
	int i = 0;

	String value = "";

	int childTableStartingID = 1;

	long ColumnID;
	FilesDAO filesDAO = new FilesDAO();
	boolean isPermanentTable = true;
%>



<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title"><i class="fa fa-gift"></i><%=formTitle%></h3>
	</div>
	<div class="box-body">
		<form class="form-horizontal" action="Job_applicant_education_infoServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
			  id="emp_edu_form" name="bigform"  method="POST" enctype = "multipart/form-data"
			  onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
			<div class="form-body">

				<%
					String Language = LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_LANGUAGE, loginDTO);
					String Options;
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					Date date = new Date();
					String datestr = dateFormat.format(date);
				%>

				<%@include file="job_applicant_education_info_summary_add_form_body.jsp" %>

			</div>

		</form>

	</div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script src="<%=context%>scripts/util1.js"></script>
<script type="text/javascript">
	const language ="<%=Language%>";
	var curYear="<%=currentYear%>";
	let selectedGradeScale;
	const scale_cal_list =<%=gradePointCatDtosJson%>
			let degreeExam = <%=degreeExamType%>;
	const psc_jsc_pattern =language=='English'? /psc|jsc|Please Select/i : /পিএসসি|জেএসসি|অনুগ্রহ করে নির্বাচন করুন/
	const grade = language=='English'?'Grade':'গ্রেড'
	const other = language=='English'?'Other':'অন্যান্য'
	const please_select = language=='English'?'Please Select':'অনুগ্রহ করে নির্বাচন করুন'
	$(document).ready( function(){
		basicInit();
		initUi();
		$.validator.addMethod('passingYearValidDuration',function(value,element){
			value = parseInt(value);
			return value>=1900 && value<=parseInt(curYear);
		});
		$.validator.addMethod('educationLevelSelection',function(value,element){
			return value!=0;
		});
		$.validator.addMethod('degreeTypeSelection',function(value,element){
			return value!=0;
		});
		$.validator.addMethod('resultTypeSelection',function(value,element){
			return value!=0;
		});
		$.validator.addMethod('gpaSelection',function(value,element){
			return $('#resultExamType_select option:selected').text() == grade && value!=0;
		});
		$.validator.addMethod('boardSelection',function(value,element){
			return value!=0;
		});

		$("#emp_edu_form").validate({
			errorClass: 'error is-invalid',
			validClass: 'is-valid',
			rules: {
				institutionName: "required",
				boardType: {
					required :true,
					boardSelection : true
				},
				major : {
					required : function(){
						if($('#major_div').is(":visible")){
							return true;
						}
						return false;
					}
				},
				marksPercentage : {
					required : {
						function(){
							if($('#resultExamType_select option:selected').text() != grade){
								let marks = $('#marksPercentage_text').val();
								console.log("marks : "+marks);
								if(marks){
									return false;
								}
								return true;
							}
							return false;
						}
					}
				},
				cgpaNumber : {
					required : {
						function(){
							if($('#resultExamType_select option:selected').text() != grade){
								let cgpa = $('#cgpaNumber_text').val();
								if(cgpa){
									return false;
								}
								return true;
							}
							return false;
						}
					}
				},
				gradePointCat : {
					required : function(){
						return $('#resultExamType_select option:selected').text() == grade;
					},
					gpaSelection : true
				},
				yearOfPassingNumber : {
					required :true,
					passingYearValidDuration : true
				},
				durationYears : {
					required : {
						function(){
							if($('#durationYears_number').val()){
								return false;
							}
							return true;
						}
					}
				},
				educationLevelType : {
					required : true,
					educationLevelSelection : true
				},
				degreeExamType : {
					required : true,
					degreeTypeSelection : true
				},
				resultExamType : {
					required : true,
					resultTypeSelection : true
				},
			},

			messages: {
				institutionName: "Please enter institute name",
				major:"Please enter major subject",
				cgpaNumber : "Please enter GPA/CGPA",
				marksPercentage : "Please enter marks percentage",
				yearOfPassingNumber : "Please enter valid passing year between 1900 to "+curYear,
				durationYears : "Please enter duration in years",
				educationLevelType : "Please select a education level",
				degreeExamType : "Please select a degree type of exam",
				resultExamType : "Please select a result type of exam",
				gradePointCat : "Please select a grade point scale",
				boardType : "Please select a board"
			}
		});
	});

	function initUi(){
		showHideElement('degreeExamType_select',other,'otherDegreeExam_div');
		showHideElement('resultExamType_select',grade,'grade_div','marksPercentage_div');
		if($('#educationLevelType_select').val()){
			fetchDegree($('#educationLevelType_select').val(),degreeExam);
		}
		$('#educationLevelType_select').change(function(){
			fetchDegree($(this).val(),null);
			showOrHideMajor();
		});
		$('#degreeExamType_select').change(function(){
			showHideElement('degreeExamType_select',other,'otherDegreeExam_div');
		});

		$('#resultExamType_select').change(function(){
			showHideElement('resultExamType_select',grade,'grade_div','marksPercentage_div');
			$('#marksPercentage_text').val("");
			$('#cgpaNumber_text').val("");
			$("#gradePointCat_category").val($("#gradePointCat_category option:first").val());
		});

		$('#marksPercentage_text').keydown(function(e){
			return inputValidationForFloatValue(e,$(this),2,100);
		});

		$('#yearOfPassingNumber_number').keydown(function(e){
			return inputValidationForIntValue(e,$(this),curYear);
		});

		$('#durationYears_number').keydown(function(e){
			return inputValidationForIntValue(e,$(this),9);
		});

		$('#gradePointCat_category').change(function(){
			$('#cgpaNumber_text').val("");
			selectedGradeScale = getSelectedGradeScaleEngText($("#gradePointCat_category option:selected").val());
			console.log("selectedGradeScale : "+selectedGradeScale);
		});

		$('#cgpaNumber_text').keydown(function(e){
			return inputValidationForFloatValue(e,$(this),2,selectedGradeScale);
		});
		showOrHideMajor();
	}

	function showOrHideMajor(){
		if(psc_jsc_pattern.test($('#educationLevelType_select option:selected').text())){
			$('#major_div').hide();
			$('#major_text').val("");
		}else{
			$('#major_div').show();
		}
	}

	function showHideElement(selecterId,selecterVal,showId,hideId){
		let x = '#'+selecterId+' option:selected';
		if($(x).text() == selecterVal){
			$('#'+showId).show();
			if(hideId){
				$('#'+hideId).hide();
			}
		}else{
			$('#'+showId).hide();
			if(hideId){
				$('#'+hideId).show();
			}
		}
	}

	function PreprocessBeforeSubmiting(row, validate){
		$("#emp_edu_form").validate();
		preprocessCheckBoxBeforeSubmitting('isForeign', row);
		return true;
	}


	function addrselected(value, htmlID, selectedIndex, tagname,  fieldName, row){
		addrselectedFunc(value, htmlID, selectedIndex, tagname,  fieldName, row, false, "Job_applicant_education_infoServlet");
	}

	function init(row){
	}

	var row = 0;

	window.onload =function (){
		init(row);
		CKEDITOR.replaceAll();
	}

	var child_table_extra_id = <%=childTableStartingID%>;

	function fetchDegree(educationLevelId,selectedDegreeValue){
		let url = "employee_education_Servlet?a=getDegrees&education_level_id="+educationLevelId;
		console.log("url : "+url);
		$.ajax({
			url: url,
			type: "GET",
			async: false,
			success: function(fetchedData) {
				$('#degreeExamType_select').html("");
				const response = JSON.parse(fetchedData).payload;
				if(response && response.length>0){
					$('#degreeExamType_div').show();
					$('#otherDegreeExam_div').hide();
					let label = "<%=(actionName.equals("edit"))
					? (LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_OTHERDEGREEEXAM, loginDTO))
					: (LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_OTHERDEGREEEXAM, loginDTO))%>";
					$('#otherDegreeExam_label').text(label);
					$('#otherDegreeExam_label').append('<span class="required"> *</span>');
					for(let x in response){
						let str;
						if(language == 'English'){
							str = response[x].nameEn;
						}else{
							str = response[x].nameBn;
						}
						let o;
						if(selectedDegreeValue!=null){
							if(selectedDegreeValue == response[x].iD){
								o = new Option(str, response[x].iD,false,true);
							}else{
								o = new Option(str, response[x].iD);
							}
						}else{
							o = new Option(str, response[x].iD);
						}
						$(o).html(str);
						$('#degreeExamType_select').append(o);
					}
				}else{
					$('#degreeExamType_div').hide();
					$('#otherDegreeExam_div').show();
					let label = "<%=(actionName.equals("edit"))
				? (LM.getText(LC.EMPLOYEE_EDUCATION_INFO_EDIT_DEGREEEXAMTYPE, loginDTO))
						: (LM.getText(LC.EMPLOYEE_EDUCATION_INFO_ADD_DEGREEEXAMTYPE, loginDTO))%>";
					$('#otherDegreeExam_label').text(label);
					$('#otherDegreeExam_label').append('<span class="required"> *</span>');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	}

	function getSelectedGradeScaleEngText(value){
		for(let i in scale_cal_list){
			if(scale_cal_list[i].value == value){
				return scale_cal_list[i].languageTextEnglish;
			}
		}
		return null;
	}


</script>