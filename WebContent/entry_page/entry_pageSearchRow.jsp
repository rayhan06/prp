<%@page pageEncoding="UTF-8" %>

<%@page import="entry_page.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>


<%@ page import="pb.*" %>
<%@page import="sessionmanager.SessionConstants" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="login.LoginDTO" %>
<%@page import="user.*" %>
<%@page import="org.apache.commons.codec.binary.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="dbm.*" %>
<%@page import="util.*" %>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    String Language = LM.getText(LC.ENTRY_PAGE_EDIT_LANGUAGE, loginDTO);
    String Language2 = Language;

    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);


    String navigator2 = SessionConstants.NAV_ENTRY_PAGE;
    RecordNavigator rn2 = (RecordNavigator) session.getAttribute(navigator2);
    boolean isPermanentTable = rn2.m_isPermanentTable;
    String tableName = rn2.m_tableName;

    System.out.println("isPermanentTable = " + isPermanentTable);
    Entry_pageDTO entry_pageDTO = (Entry_pageDTO) request.getAttribute("entry_pageDTO");
    CommonDTO commonDTO = entry_pageDTO;
    String servletName = "Entry_pageServlet";


    System.out.println("entry_pageDTO = " + entry_pageDTO);


    int i = Integer.parseInt(request.getParameter("rownum"));
    out.println("<td style='display:none;'><input type='hidden' id='failureMessage_" + i + "' value=''/></td>");

    String value = "";


    Entry_pageDAO entry_pageDAO = (Entry_pageDAO) request.getAttribute("entry_pageDAO");


    String Options = "";
    boolean formSubmit = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

%>


<td id='<%=i%>_organogramId'>
    <%
        value = entry_pageDTO.organogramId + "";
    %>

    <%=value%>


</td>


<td id='<%=i%>_actionDescriptionType'>
    <%
        value = entry_pageDTO.actionDescriptionType + "";
    %>
    <%
        value = CommonDAO.getName(Integer.parseInt(value), "action_description", Language.equals("English") ? "name_en" : "name_bn", "id");
    %>

    <%=value%>


</td>


<td id='<%=i%>_Edit'>
    <a href="#"
       onclick='fixedToEditable(<%=i%>,"", "<%=entry_pageDTO.iD%>", <%=isPermanentTable%>, "Entry_pageServlet")'><%=LM.getText(LC.ENTRY_PAGE_SEARCH_ENTRY_PAGE_EDIT_BUTTON, loginDTO)%>
    </a>

</td>


<td id='<%=i%>_checkbox'>
    <div class='checker'>
        <span id='chkEdit'><input type='checkbox' name='ID' value='<%=entry_pageDTO.iD%>'/></span>
    </div>
</td>
																						
											

