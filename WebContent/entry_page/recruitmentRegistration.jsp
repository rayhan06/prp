<%@page import="util.*"%><%@page import="theme.ThemeRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="config.GlobalConfigConstants"%>
<%@page import="config.GlobalConfigurationRepository"%>
<%@page import="java.util.Calendar"%>
<%@ page import="login.LoginDTO" %>
<%@ page import="user.UserDTO" %>
<%@ page import="sessionmanager.SessionConstants" %>
<%@ page import="user.UserRepository" %>
<%@page language="java"%><%
String loginURL=request.getRequestURL().toString();	
//String context = "../../.."  + request.getContextPath() + "/";
String context_folder = request.getContextPath();


LM.getInstance();

String context = "../../.."  + request.getContextPath() + "/";

    LoginDTO loginDTO = new LoginDTO();
    loginDTO.isOisf = 0;
    loginDTO.userID = Long.parseLong(GlobalConfigurationRepository.getGlobalConfigDTOByID(GlobalConfigConstants.DEFAULT_USER_ID).value);
    request.getSession().setAttribute(SessionConstants.USER_LOGIN, loginDTO);
    UserDTO currentUserDTO = new UserDTO();
    currentUserDTO.ID = loginDTO.userID;

    int my_language = LM.getLanguageIDByUserDTO(currentUserDTO) == CommonConstant.Language_ID_English ? 2 : 1;

%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<input type="hidden" id="my_language" value="<%=my_language%>"/>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->    
<head>
<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Parliament Login</title>
    <link rel="stylesheet" href="<%=context_folder%>/assets/css/login/all.min.css">
    <link rel="stylesheet" href="<%=context_folder%>/assets/css/login/bootstrap.min.css">
    <link rel="stylesheet" href="<%=context_folder%>/assets/css/login/style_home2.css">
</head>

<style>
    .select2-container--bootstrap .select2-selection--single .select2-selection__rendered {
        padding-top: 7px;
    }

    .custom-toast {
        visibility: hidden;
        width: 300px;
        min-height: 60px;
        color: #fff;
        text-align: center;
        padding: 20px 30px;
        position: fixed;
        z-index: 1;
        right: 23px;
        bottom: 60px;
        font-size: 13pt;
    }

    #toast_message {
        background-color: #008aa6;
    }

    #error_message {
        background-color: #ca5e59;
    }

    #toast_message.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    #error_message.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    @-webkit-keyframes fadein {
        from {
            right: -38%;
            opacity: 0;
        }
        to {
            right: 23px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            right: -38%;
            opacity: 0;
        }
        to {
            right: 23px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            right: 23px;
            opacity: 1;
        }
        to {
            right: -38%;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            right: 23px;
            opacity: 1;
        }
        to {
            right: -38%;
            opacity: 0;
        }
    }

    .modal {
    // position: absolute;
        float: left;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        width:100%;
        height:100%;
    }

</style>

<div id="toast_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;"><i class="fa fa-times" onclick="onClickHideToast()"></i></div>
    <div id="toast_message_div"></div>
</div>
<div id="error_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;"><i class="fa fa-times" onclick="onClickHideToast()"></i></div>
    <div id="error_message_div"></div>
</div>

<body class="plogin">
    <div class="parliamentlogin">

        <div style="text-align: center;">
            <h2><%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_HEADER, loginDTO)%></h2>
        </div>
        <h3><%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_HEADER_FORM, loginDTO)%></h3>
        <form id='registrationform' name="registrationform" method="post" action='<%=request.getContextPath()%>/UserServlet?actionType=addFromRecruitmentRegistration'
<%--              onsubmit="return PreprocessRecruitmentRegistrationBeforeSubmiting(0)">--%>
              >
            <div class="form-group">
                <input type="text" class="form-control noEnglish" id="fullName" placeholder="<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_NAME_BANGLA, loginDTO)%>" name="fullName">
            </div>

            <div class="form-group">
                <input type="text" class="form-control englishOnly" id="nameEn" placeholder="<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_NAME_ENGLISH, loginDTO)%>" name="nameEn">
            </div>

            <div class="form-group">
              <input type="text" class="form-control" id="mailAddress" placeholder="<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_EMAIL, loginDTO)%>" name="mailAddress"
                     pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" title="emailo must be a of valid email address format"

                     tag='pb_html'/>
            </div>

            <div class="form-group row">
                <div class="col-lg-3">
                    <input type="number" disabled="" value="880" class="form-control">
                </div>
                <div class="col-lg-9">
                    <input type="number" class="form-control" id="phoneNo" placeholder="1xxxxxxxxx" name="phoneNo" maxlength="10" minlength="10" max="1999999999">
                </div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="nid" placeholder="<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_NID, loginDTO)%>" name="nid">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="birth_registration_no" placeholder="<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_NID_REGISTRATION_NO, loginDTO)%>" name="birth_registration_no">
            </div>
            <div class="form-group">
                <input type="date" class="form-control" id="date_of_birth" placeholder="<%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_BIRTH_DATE, loginDTO)%>" name="date_of_birth">
            </div>

            <input type="hidden" class="form-control" id="userType" name="userType" value="1">
            <input type="hidden" class="form-control" id="roleName" name="roleName" value="10007">
            <input type="hidden" class="form-control" id="CentreType" name="CentreType" value="3">
            <input type="hidden" class="form-control" id="password" name="password" value="">
<%--            <input type="hidden" class="form-control" id="date_of_birth" name="date_of_birth" value="1566016148203">--%>


            <div class="checkbox">
                <input type="checkbox" id="confirmed" name="confirmed" onchange="toggleSubmit()">
                <label><%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_ACCEPT_TERM, loginDTO)%></label>
            </div>
            <!-- <button type="submit" class="btn btn-default">Submit</button> -->

            <div style="background-color: #FFFFFF;color: #000000;height: 70px;border: solid #000000">

                <input style="margin-left: 30px; margin-top: 25px;" type="checkbox" id="notRobot" name="checkboxCaptcha" onchange="toggleSubmit()">
                <div style="margin-left: 25px" class="checkboxtext">I'm not a robot</div>

                <img style="cursor: pointer;position: absolute;right: 20px" src="<%=request.getContextPath()%>/images/captcha.png" height="40px" width="75px">
                <div style="cursor: pointer;position: absolute;right: 33px;font-size:8px">reCAPTCHA</div>

                <div style="position: absolute;bottom: 60px;right: 20px">

                    <span onclick="popUpGuideLines()" style="cursor: pointer;position: absolute;right: 35px;font-size:5px">Privacy .</span>
                    <span onclick="popUpGuideLines()" style="cursor: pointer;position: absolute;right: 16px;font-size:5px">Terms</span>

                </div>

            </div>

            <div style="height: 20px;">




            </div>

            <div class="submitbutton">
                <a> <button onclick="submitRecruitmentRegistration()" disabled id="submit" style="width:411px; background-color:#72F9E7"><%=LM.getText(LC.REGISTER_FOR_ONLINE_JOB_LABEL_SIGN_UP, loginDTO)%></button></a>
            </div>

            
          </form>
    </div>

    <script src="<%=context_folder%>/assets/scripts/login/jquery-3.5.1.slim.min.js"></script>
    <script src="<%=context_folder%>/assets/global/plugins/jquery-validation/js/jquery.validate.js"></script>
    <script src="<%=context_folder%>/assets/scripts/login/bootstrap.min.js"></script>
    <script src="<%=context_folder%>/assets/scripts/login/popper.min.js"></script>
    <script src="<%=context_folder%>/assets/scripts/login/main.js"></script>
    <script src="<%=context_folder%>/assets/scripts/util1.js"></script>
    <script src="<%=context_folder%>/assets/scripts/pb.js"></script>

    <script type="text/javascript">

        $(document).ready( function(){

            $('input').keyup(function(){

                toggleSubmit();
            });

            $(".englishOnly").keypress(function(event){
                var ew = event.which;
                if(ew == 32)
                    return true;
                if(48 <= ew && ew <= 57)
                    return true;
                if(65 <= ew && ew <= 90)
                    return true;
                if(97 <= ew && ew <= 122)
                    return true;
                return false;
            });
            $(".noEnglish").keypress(function(event){
                var ew = event.which;
                if(ew == 32)
                    return false;
                if(48 <= ew && ew <= 57)
                    return false;
                if(65 <= ew && ew <= 90)
                    return false;
                if(97 <= ew && ew <= 122)
                    return false;
                return true;
            });

            var success_message_bng = 'সম্পন্ন হয়েছে';
            var success_message_eng = 'Success';
            var language = <%=LM.getLanguageIDByUserDTO(currentUserDTO)%>;
            $.validator.addMethod('mailAddressPattern',function(value,element){
                var lengthOfValue = value.toString().length - 1;
                var lastDotIndex = value.toString().lastIndexOf('.');
                var lastAtTheRateIndex = value.toString().lastIndexOf('@');
                if (value.toString().charAt(0) == '@' || value.toString().charAt(0) == '.')return false;
                if (lastDotIndex == -1 || lastAtTheRateIndex == -1)return false;
                return (lastAtTheRateIndex < lastDotIndex) && lengthOfValue > (lastDotIndex + 1);
            });
            if (language == 1) {
                $("#registrationform").validate({
                    errorClass: 'error is-invalid',
                    validClass: 'is-valid',
                    rules: {
                        // nationalIdNo: {
                        // 	digits: true
                        // },
                        fullName: "required",
                        nid: "required",
                        birth_registration_no: "required",
                        mailAddress: {
                            required: true,
                            mailAddressPattern: true,
                        },
                        phoneNo: "required",
                    },
                    messages: {
                        // nationalIdNo: {
                        // 	digits: "Please enter digits only"
                        // },
                        fullName: "Please enter name",
                        nid: "Please enter NID",
                        mailAddress: "Please enter valid email",
                        phoneNo: "Please enter contact number",
                    }
                });
            } else {
                $("#registrationform").validate({
                    errorClass: 'error is-invalid',
                    validClass: 'is-valid',
                    rules: {
                        // nationalIdNo: {
                        // 	digits: true
                        // },
                        fullName: "required",
                        nid: "required",
                        birth_registration_no: "required",
                        mailAddress: {
                            required: true,
                            mailAddressPattern: true,
                        },
                        phoneNo: "required",
                    },
                    messages: {
                        // nationalIdNo: {
                        // 	digits: "Please enter digits only"
                        // },
                        fullName: "অনুগ্রহপূর্বক নাম প্রদান করুন",
                        nid: "অনুগ্রহপূর্বক জাতীয় পরিচয়পত্র নম্বর প্রদান করুন",
                        mailAddress: "অনুগ্রহপূর্বক যথাযথ ইমেইল প্রদান করুন",
                        phoneNo: "অনুগ্রহপূর্বক যোগাযোগের নম্বর প্রদান করুন",
                    }
                });
            }


        });

        var requiredIDs = ['fullName','mailAddress','phoneNo','nid']
        var requiredFieldNameBn = ['নাম','ইমেইল','ফোন নম্বর','জাতীয় পরিচয়পত্র নম্বর']

        function popUpGuideLines() {
            if (document.getElementById('notRobot').checked) {
                alert('Some guidelines..')
            }
        }

        function toggleSubmit() {
            document.getElementById('submit').disabled = !(document.getElementById('confirmed').checked && document.getElementById('notRobot').checked && $("#registrationform").valid());
        }

        var success_message_bng = 'সম্পন্ন হয়েছে';
        var success_message_eng = 'Success';

        function PreprocessRecruitmentRegistrationBeforeSubmiting(row, e)
        {

            var valid = $("#registrationform").valid();

            if (valid) {
                <%--showToast(success_message_bng, success_message_eng);--%>
                <%--setTimeout(function(){--%>
                <%--    $("#registrationform").action ="<%=request.getContextPath()%>/UserServlet?actionType=addFromRecruitmentRegistration";--%>
                <%--    $("#registrationform").submit();--%>
                <%--    }, 3000);--%>
            }

            return true;

        }

        function showToast(bn, en) {
            const x = document.getElementById("toast_message_div");
            x.innerHTML = '';
            const my_language = document.getElementById('my_language').value;

            x.innerHTML = parseInt(my_language) === 1 ? bn : en;
            document.getElementById('toast_message').classList.add("show");

            setTimeout(function () {
                document.getElementById('toast_message').classList.remove("show");
            }, 3000);
        }



        function submitRecruitmentRegistration() {

            // event.preventDefault();
            // $("#registrationform").validate();
            var validated = $("#registrationform").valid();
            console.log(validated);

            if (true) {
                showToast("নিবন্ধন সফলভাবে সম্পন্ন", "Registration Successful!");
                // submitForRegistration();

                setTimeout(function(){

                    submitForRegistration();

                }, 3000);
            }

            // document.getElementById('registrationform').submit();

            // var formDataValid = true;
            // var formDataMissing = false;
            // var missingMessage = 'অনুগ্রহপূর্বক নিচের তথ্যগুলো পূরণ করুন: ';
            //
            // for (var i=0; i<requiredIDs.length; i++) {
            //     var requiredID = requiredIDs[i];
            //     var value = document.getElementById(requiredID).value;
            //     if (value == undefined || value.trim() == '') {
            //         document.getElementById(requiredID).style.border = 'solid #FF0000';
            //         formDataMissing = true;
            //         missingMessage = missingMessage + requiredFieldNameBn[i] + ', ';
            //     } else document.getElementById(requiredID).style.border = 'none';
            // }
            //
            // if (formDataMissing) {
            //     alert(missingMessage);
            // } else if (formDataValid) {
            //     document.getElementById('registrationform').submit();
            // }
        }

        function getLanguageBaseText(en, bn) {
            const my_language = document.getElementById('my_language').value;
            return parseInt(my_language) === 1 ? (bn == undefined || bn == null ? "পাওয়া যায় নি" : bn) : (en == undefined || en == null ? "Not found" : en);
        }

        function onClickHideToast() {
            document.getElementById('toast_message').classList.remove("show");
            document.getElementById('error_message').classList.remove("show");
        }

        function submitForRegistration() {

            $( "#registrationform" ).submit();

            var form = $( "#registrationform" );

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function()
            {
                if (this.readyState == 4 && this.status == 200)
                {
                    if (this.responseText == 'null') {
                        showToast("নিবন্ধন সফলভাবে সম্পন্ন", "Registration Successful!");

                        setTimeout(function(){

                            form.submit();

                        }, 3000);
                    } else {
                        showToast("প্রদত্ত ফোন নম্বর দিয়ে ইউজার বিদ্যমান", "User with this phone number exists!");
                    }
                }
                else if(this.readyState == 4 && this.status != 200)
                {
                    alert('failed ' + this.status);
                }
            };
            xhttp.open("GET", '<%=request.getContextPath()%>/UserServlet?actionType=getUserDTOByUsername&phoneNo=' + document.getElementById('phoneNo').value, true);
            // xhttp.send();

            <%--var formData = new FormData();--%>
            <%--formData.append('userType', '1');--%>
            <%--formData.append('roleName', '100007');--%>
            <%--formData.append('CentreType', '3');--%>
            <%--formData.append('password', '');--%>

            <%--var elements = document.forms['registrationform'].elements;--%>
            <%--for (var i=0; i<elements.length; i++){--%>
            <%--    var name = elements[i].name;--%>
            <%--    if (document.getElementsByName(name).item(0) != null) {--%>
            <%--        let data = document.getElementsByName(name).item(0).value;--%>
            <%--        formData.append(name, data);--%>
            <%--    }--%>
            <%--}--%>

            <%--let xhttp = new XMLHttpRequest();--%>
            <%--xhttp.onreadystatechange = function () {--%>
            <%--    if (this.readyState === 4 && this.status === 200) {--%>
            <%--        console.log(this.responseText)--%>
            <%--    }--%>
            <%--};--%>
            <%--xhttp.open("Post", '<%=request.getContextPath()%>/UserServlet?actionType=addFromRecruitmentRegistration', true);--%>
            <%--xhttp.send(formData);--%>

        }

    </script>

<style>


    input[name=checkboxCaptcha]
    {
        /* Double-sized Checkboxes */
        -ms-transform: scale(3); /* IE */
        -moz-transform: scale(3); /* FF */
        -webkit-transform: scale(3); /* Safari and Chrome */
        -o-transform: scale(3); /* Opera */
        transform: scale(3);
        padding: 10px;
    }

    /* Might want to wrap a span around your checkbox text */
    .checkboxtext
    {
        /* Checkbox text */
        font-size: 110%;
        display: inline;
    }

</style>


</body>
</html>

<script type='text/javascript'>
    $(function () {
        // $.bdatepicker.setDefaults(bn);
        // $('.datepicker').bdatepicker();
    });
</script>