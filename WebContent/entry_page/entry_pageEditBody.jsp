<%@page import="sessionmanager.SessionConstants" %>
<%@page import="login.LoginDTO" %>

<%@page import="entry_page.*" %>
<%@page import="java.util.*" %>

<%@page pageEncoding="UTF-8" %>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="java.util.UUID" %>
<%@page import="language.LanguageTextDTO" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>

<%@page import="user.UserDTO" %>
<%@page import="user.UserRepository" %>
<%@page import="family.*" %>

<%@page import="geolocation.GeoLocationDAO2" %>


<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="pb.*" %>


<%
    Entry_pageDTO entry_pageDTO;
    entry_pageDTO = (Entry_pageDTO) request.getAttribute("entry_pageDTO");
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);
    UserDTO userDTO = UserRepository.getUserDTOByUserID(loginDTO);
    if (entry_pageDTO == null) {
        entry_pageDTO = new Entry_pageDTO();

    }
    System.out.println("entry_pageDTO = " + entry_pageDTO);

    long error = -1;
    String sError = "";
    if (request.getParameter("error") != null) {
        error = Long.parseLong(request.getParameter("error"));
        sError = LM.getText(error, loginDTO);
    }
    System.out.println("error = " + error);

    String actionName;
    System.out.println("actionType = " + request.getParameter("actionType"));
    if (request.getParameter("actionType").equalsIgnoreCase("getAddPage")) {
        actionName = "add";
    } else {
        actionName = "edit";
    }
    String formTitle;
    if (actionName.equals("edit")) {
        formTitle = LM.getText(LC.ENTRY_PAGE_EDIT_ENTRY_PAGE_EDIT_FORMNAME, loginDTO);
    } else {
        formTitle = LM.getText(LC.ENTRY_PAGE_ADD_ENTRY_PAGE_ADD_FORMNAME, loginDTO);
    }

    String ID = request.getParameter("ID");
    if (ID == null || ID.isEmpty()) {
        ID = "0";
    }
    System.out.println("ID = " + ID);
    int i = 0;

    String value = "";

    int childTableStartingID = 1;

    boolean isPermanentTable = true;
%>

<%
    String Language = LM.getText(LC.ENTRY_PAGE_EDIT_LANGUAGE, loginDTO);
    String Options;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date = new Date();
    String datestr = dateFormat.format(date);
%>

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content"
     style="padding: 0px !important; margin-bottom: -18px">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i>&nbsp;
                    <%=formTitle%>
                </h3>
            </div>
        </div>
        <form class="form-horizontal"
              action="Entry_pageServlet?actionType=<%=actionName%>&isPermanentTable=<%=isPermanentTable%>"
              id="bigform" name="bigform" method="POST" enctype="multipart/form-data"
              onsubmit="return PreprocessBeforeSubmiting(0,'<%=actionName%>')">
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background: white"><%=formTitle%>
                                            </h4>
                                        </div>
                                        <input type='hidden' class='form-control' name='iD'
                                               id='iD_hidden_<%=i%>' value='<%=entry_pageDTO.iD%>'
                                               tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='error' id='error'
                                               value='<%=sError%>' tag='pb_html'/>
                                        <%
                                            if (userDTO.roleID != SessionConstants.INVENTORY_MANGER_ROLE) {
                                        %>
                                        <jsp:include page="../employee_assign/employeeSearchModal.jsp">
                                            <jsp:param name="isHierarchyNeeded" value="false"/>
                                        </jsp:include>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.APPOINTMENT_ADD_PATIENTID, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <input type='text' class='form-control'
                                                               onfocusout="employee_inputted(this.value)"
                                                               name='employeeUserName' id='employeeUserName'
                                                               tag='pb_html'/>
                                                    </div>
                                                    <div class="col-4 text-md-right">
                                                        <button type="button"
                                                                class="btn btn-border-radius text-white shadow"
                                                                style="background-color: #4a87e2;"
                                                                onclick="employee_inputted($('#employeeUserName').val())">
                                                            <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.HM_OR, loginDTO)%> <%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
	                                                <div class="row">
	                                                    <div class="col-8">
	                                                    	<div class="input-group mb-2">
									                            <div class="input-group-prepend">
									                                <div class="input-group-text"><%=Language.equalsIgnoreCase("english")? "+88" : "+৮৮"%></div>
									                            </div>
									                            <input type="text" class="form-control" name="employeePhoneNumber" id="employeePhoneNumber">
									                        </div>	                                                        
	                                                    </div>
	                                                    <div class="col-4 text-md-right">
	                                                        <button type="button"
	                                                                class="btn btn-border-radius text-white shadow"
	                                                                style="background-color: #4a87e2;"
	                                                                onclick="phone_inputted($('#employeePhoneNumber').val())">
	                                                            <%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%>
	                                                        </button>
	                                                    </div>
	                                                </div>
                                              </div>
                                         </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.HM_OR, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <button type="button"
                                                        class="btn text-white shadow form-control btn-border-radius"
                                                        style="background-color: #4a87e2"
                                                        onclick="addEmployee()"
                                                        id="addToTrainee_modal_button"><%=LM.getText(LC.HM_SEARCH_EMPLOYEE, loginDTO)%>
                                                </button>
                                                <table class="table table-bordered table-striped"
                                                       style="display:none">
                                                    <tbody id="employeeToSet"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.HM_NAME, loginDTO)%>
                                            </label>
                                            <div class="col-md-9" id='empName_div'>
                                                <input type='text' readonly class='form-control' id='ename'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.HM_DESIGNATION, loginDTO)%>
                                            </label>
                                            <div class="col-md-9" id='designation_div'>
                                                <input type='text' readonly class='form-control' id='edes'
                                                       tag='pb_html'/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=Language.equalsIgnoreCase("english")?"Last Appointments":"বিগত অ্যাপয়েন্টমেন্টসমূহ"%>
                                            </label>
                                            <div class="col-md-9" id='designation_div'>
                                                <table id ='last' class="table table-bordered table-striped">
                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.SELECT_PATIENT_HM, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <select class='form-control' name='whoIsThePatientCat'
                                                        onchange="getPatientInfo(this.value)"
                                                        id='whoIsThePatientCat_category_<%=i%>' tag='pb_html'>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="name_phone_div">
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.HM_PATIENT_NAME, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                    <input type='text' readonly class='form-control'
                                                           name='patientName' id='patientName_text_<%=i%>'
                                                           tag='pb_html'/>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label text-md-right">
                                                    <%=LM.getText(LC.HM_PHONE, loginDTO)%>
                                                </label>
                                                <div class="col-md-9">
                                                	<div class="input-group mb-2">
								                            <div class="input-group-prepend">
								                                <div class="input-group-text"><%=Language.equalsIgnoreCase("english")? "+88" : "+৮৮"%></div>
								                            </div>
								                            <input type='text' readonly class='form-control'
                                                           name='phoneNumber' id='phoneNumber_number_<%=i%>'
                                                           tag='pb_html'>
									                 </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <%
                                        } else {
                                        %>
                                        <input type='hidden' class='form-control' name='employeeUserName'
                                               value='-1' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='whoIsThePatientCat'
                                               value='-1' tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='patientName' value=''
                                               tag='pb_html'/>
                                        <input type='hidden' class='form-control' name='phoneNumber' value=''
                                               tag='pb_html'/>
                                        <%

                                            }
                                        %>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label text-md-right">
                                                <%=LM.getText(LC.ENTRY_PAGE_ADD_ACTIONDESCRIPTIONTYPE, loginDTO)%>
                                            </label>
                                            <div class="col-md-9">
                                                <select class='form-control' name='actionDescriptionType'
                                                        id='actionDescriptionType_select_<%=i%>' tag='pb_html'>
                                                    <%
                                                        Options = CommonDAO.getPagesForRole(Language, userDTO.roleID);
                                                    %>
                                                    <%=Options%>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-3 row">
                    <div class="col-md-11">
                        <input type='hidden' class='form-control' name='isDeleted' id='isDeleted_hidden_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + entry_pageDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
                        <input type='hidden' class='form-control' name='lastModificationTime'
                               id='lastModificationTime_hidden_<%=i%>'
                               value=<%=actionName.equals("edit")?("'" + entry_pageDTO.lastModificationTime + "'"):("'" + "0" + "'")%> tag='pb_html'/>
                        <div class="form-actions text-right">
                            <a class="btn btn-sm cancel-btn text-white shadow"
                               href="<%=request.getHeader("referer")%>">
                                <%
                                    if(actionName.equals("edit")) 
                                    {
                                %>
                                <%=LM.getText(LC.ENTRY_PAGE_EDIT_ENTRY_PAGE_CANCEL_BUTTON, loginDTO)%>
                                <%
                                	}
                                  	else 
                                	{
                                %>
                                <%=LM.getText(LC.ENTRY_PAGE_ADD_ENTRY_PAGE_CANCEL_BUTTON, loginDTO)%>
                                <%
                                    }
                                %>
                            </a>
                            <button class="btn btn-sm submit-btn text-white shadow ml-2" type="submit">
                                <%
                                    if(actionName.equals("edit")) 
                                    {
                                %>
                                <%=LM.getText(LC.ENTRY_PAGE_EDIT_ENTRY_PAGE_SUBMIT_BUTTON, loginDTO)%>
                                <%
                                	}
                                    else 
                                    {
                                %>
                                <%=LM.getText(LC.ENTRY_PAGE_ADD_ENTRY_PAGE_SUBMIT_BUTTON, loginDTO)%>
                                <%
                                    }
                                %>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

    function get_patient(value) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var options = this.responseText;
                $("#whoIsThePatientCat_category_<%=i%>").html(options);
                getAppointments(value);
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getFamily&userName=" + value + "&language=<%=Language%>", true);
        xhttp.send();
    }
    
    function fillUserInfo(responseText)
    {
    	$("#empdiv").css("display", "block");
        var userNameAndDetails = JSON.parse(responseText);
        console.log("name = " + userNameAndDetails.name);
        $("#ename").val(userNameAndDetails.name);
        if(userNameAndDetails.designation != "")
       	{
        	$("#edes").val(userNameAndDetails.designation + ", " + userNameAndDetails.deptName);
       	}
        else
       	{
        	$("#edes").val("");
       	}
        
        $("#employeeUserName").val(convertNumberToLanguage(userNameAndDetails.userName, '<%=Language%>'));
        $("#employeePhoneNumber").val(phoneNumberRemove88ConvertLanguage(userNameAndDetails.phone, '<%=Language%>'));
        $('#patientName_text_<%=i%>').val('');
        $("#phoneNumber_number_<%=i%>").val('');
        get_patient(userNameAndDetails.userName);
        
        
    }
    
    function getAppointments(userName) {
        console.log('getAppointments: ' + userName);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	$("#last").html(this.responseText);
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("GET", "AppointmentServlet?actionType=getLast&userName=" + userName + "&Language=<%=Language%>" , true);
        xhttp.send();
    }

    function employee_inputted(value) {
        console.log('changed value: ' + value);
        $("#last").html("");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	fillUserInfo(this.responseText);
            	
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getEmployeeInfo&userName=" + value + "&language=<%=Language%>", true);
        xhttp.send();
    }
    
    function phone_inputted(value) {
    	value = phoneNumberAdd88ConvertLanguage(value, '<%=Language%>');
        console.log('changed value: ' + value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
            	fillUserInfo(this.responseText)
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getEmployeeInfo&phone=" + value + "&language=<%=Language%>", true);
        xhttp.send();
    }

    function getPatientInfo(value) {
        console.log('changed value: ' + value);

        var familyMemberId = value;
        var userName = $("#employeeUserName").val();

        if (familyMemberId == <%=FamilyDTO.OTHER%>) {
            $("#name_phone_div").css("display", "none");
            return;
        } else {
            $("#name_phone_div").css("display", "block");
        }
        
        $('#patientName_text_<%=i%>').val('');
        $("#phoneNumber_number_<%=i%>").val('');


        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var familyMemberDTO = JSON.parse(this.responseText);
                <%
                if(Language.equalsIgnoreCase("english"))
                {
                %>
                $('#patientName_text_<%=i%>').val(familyMemberDTO.nameEn);
                <%
            }
            else
            {
                %>
                $('#patientName_text_<%=i%>').val(familyMemberDTO.nameBn);
                <%
            }
            %>
                $("#phoneNumber_number_<%=i%>").val(phoneNumberRemove88ConvertLanguage(familyMemberDTO.mobile, '<%=Language%>'));
            } else {
                //console.log('failed status = ' + this.status + " this.readyState = " + this.readyState);
            }
        };

        xhttp.open("POST", "FamilyServlet?actionType=getPatientDetails&familyMemberId=" + familyMemberId + "&userName=" + userName, true);
        xhttp.send();

    }


    $(document).ready(function () {

        if ($("#error").val() != "") {
            toastr.error($("#error").val());
        }

        $('.datepicker').datepicker({

            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            todayHighlight: true,
            showButtonPanel: true
        });
    });

    function PreprocessBeforeSubmiting(row, validate) {    	
        if ($("#whoIsThePatientCat_category_0").val() == -2) {
            toastr.error("Select an employee");
            return false;
        }
        var convertedPhoneNumber = phoneNumberAdd88ConvertLanguage($('#phoneNumber_number_<%=i%>').val(), '<%=Language%>');
        $("#phoneNumber_number_<%=i%>").val(convertedPhoneNumber);


        return true;
    }


    function addrselected(value, htmlID, selectedIndex, tagname, fieldName, row) {
        addrselectedFunc(value, htmlID, selectedIndex, tagname, fieldName, row, false, "Entry_pageServlet");
    }

    function init(row) {


    }

    var row = 0;

    window.onload = function () {
        init(row);
        CKEDITOR.replaceAll();
    }

    var child_table_extra_id = <%=childTableStartingID%>;


    function patient_inputted(userName, orgId) {
        console.log("patient_inputted " + userName);
        $("#employeeUserName").val(userName);
        employee_inputted(userName);
    }


</script>






