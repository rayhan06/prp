<%@page import="util.*"%><%@page import="theme.ThemeRepository"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="config.GlobalConfigConstants"%>
<%@page import="config.GlobalConfigurationRepository"%>
<%@page import="java.util.Calendar"%><%@page language="java"%><%
String loginURL=request.getRequestURL().toString();	
//String context = "../../.."  + request.getContextPath() + "/";
String context_folder = request.getContextPath();


LM.getInstance();

String context = "../../.."  + request.getContextPath() + "/";


%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->    
<head>
<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Parliament Login</title>
    <link rel="stylesheet" href="<%=context_folder%>/assets/css/login/all.min.css">
    <link rel="stylesheet" href="<%=context_folder%>/assets/css/login/bootstrap.min.css">
    <link rel="stylesheet" href="<%=context_folder%>/assets/css/login/style.css">
</head>
<body class="plogin">
    <div class="parliamentlogin">

        <h3>প্রবেশ করুন</h3>
        Recruitment
        <form id='loginform' action="<%=request.getContextPath()%>/RecruitmentLoginServlet" method="post">
            <div class="form-group">
              <input type="text" class="form-control" id="email" placeholder="ব্যাবহার কারীর নাম" name="username">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" id="pwd" placeholder="পাসওয়ার্ড" name="password">
            </div>
            <div class="checkbox">
              <label> <a href="#">পাসওয়ার্ড ভুলে গেছেন ? </a></label>
            </div>
            <!-- <button type="submit" class="btn btn-default">Submit</button> -->
            
            <div class="submitbutton">
<%--                <img src="<%=context_folder%>/assets/img/login/home1/E-nothy_normal.png" alt="E-lothi">--%>

                <a href="Entry_pageServlet?actionType=recruitmentRegistration"> রেজিস্ট্রেশন  </a>
                <a href="#" onclick="document.getElementById('loginform').submit();"> <img src="<%=context_folder%>/assets/img/login/home1/Entry_normal.png" alt="E-lothi"> </a>
            </div>

            
          </form>
    </div>


    <script src="<%=context_folder%>/assets/scripts/login/jquery-3.5.1.slim.min.js"></script>
    <script src="<%=context_folder%>/assets/scripts/login/bootstrap.min.js"></script>
    <script src="<%=context_folder%>/assets/scripts/login/popper.min.js"></script>
    <script src="<%=context_folder%>/assets/scripts/login/main.js"></script>
</body>
</html>