<%@page pageEncoding="UTF-8" %>

<%@page import="sessionmanager.SessionConstants"%>
<%@page import="entry_page.Entry_pageDTO"%>
<%@page import="java.util.*"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>

<%
Entry_pageDTO entry_pageDTO = (Entry_pageDTO)request.getAttribute("entry_pageDTO");
LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(SessionConstants.USER_LOGIN);

if(entry_pageDTO == null)
{
	entry_pageDTO = new Entry_pageDTO();
	
}
System.out.println("entry_pageDTO = " + entry_pageDTO);

String actionName = "edit";


String ID = request.getParameter("ID");
if(ID == null || ID.isEmpty())
{
	ID = "0";
}
System.out.println("ID = " + ID);
int i = Integer.parseInt(request.getParameter("rownum"));
String deletedStyle = request.getParameter("deletedstyle");

String value = "";

%>




























	















<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>

<%@ page import="pb.*"%>

<%
String Language = LM.getText(LC.ENTRY_PAGE_EDIT_LANGUAGE, loginDTO);
String Options;
SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Date date = new Date();
String datestr = dateFormat.format(date);
%>

			
<%=("<td id = '" + i + "_iD" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='iD' id = 'iD_hidden_<%=i%>' value='<%=entry_pageDTO.iD%>' tag='pb_html'/>
	
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_organogramId'>")%>
			
	
	<div class="form-inline" id = 'organogramId_div_<%=i%>'>
		<input type='text' class='form-control'  name='organogramId' id = 'organogramId_text_<%=i%>' value=<%=actionName.equals("edit")?("'" + entry_pageDTO.organogramId + "'"):("'" + "" + "'")%>   tag='pb_html'/>					
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_actionDescriptionType'>")%>
			
	
	<div class="form-inline" id = 'actionDescriptionType_div_<%=i%>'>
		<select class='form-control'  name='actionDescriptionType' id = 'actionDescriptionType_select_<%=i%>'   tag='pb_html'>
<%
if(actionName.equals("edit"))
{
			Options = CommonDAO.getOptions(Language, "select", "action_description", "actionDescriptionType_select_" + i, "form-control", "actionDescriptionType", entry_pageDTO.actionDescriptionType + "");
}
else
{			
			Options = CommonDAO.getOptions(Language, "select", "action_description", "actionDescriptionType_select_" + i, "form-control", "actionDescriptionType" );			
}
%>
<%=Options%>
		</select>
		
	</div>
				
<%=("</td>")%>
			
<%=("<td id = '" + i + "_isDeleted" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='isDeleted' id = 'isDeleted_hidden_<%=i%>' value= <%=actionName.equals("edit")?("'" + entry_pageDTO.isDeleted + "'"):("'" + "false" + "'")%> tag='pb_html'/>
											
												
<%=("</td>")%>
			
<%=("<td id = '" + i + "_lastModificationTime" +  "' style='display:none;'>")%>
			

		<input type='hidden' class='form-control'  name='lastModificationTime' id = 'lastModificationTime_hidden_<%=i%>' value='<%=entry_pageDTO.lastModificationTime%>' tag='pb_html'/>
		
												
<%=("</td>")%>
					
		