<%@page import="util.*" %>
<%@ page import="user.UserServlet" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%
    String context_folder = request.getContextPath();
    String Language = "Bangla";
    int my_language = 1;
%>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.maateen.me/solaiman-lipi/font.css">
    <title>Forgot Password</title>
    <style>

        .custom-toast {
            visibility: hidden;
            width: 300px;
            min-height: 60px;
            color: #fff;
            text-align: center;
            padding: 20px 30px;
            position: fixed;
            z-index: 1;
            right: 23px;
            bottom: 60px;
            font-size: 13pt;
        }

        #toast_message {
            background-color: #008aa6;
        }

        #error_message {
            background-color: #ca5e59;
        }

        #toast_message.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        #error_message.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        @-webkit-keyframes fadein {
            from {
                right: -38%;
                opacity: 0;
            }
            to {
                right: 23px;
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                right: -38%;
                opacity: 0;
            }
            to {
                right: 23px;
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeout {
            from {
                right: 23px;
                opacity: 1;
            }
            to {
                right: -38%;
                opacity: 0;
            }
        }

        @keyframes fadeout {
            from {
                right: 23px;
                opacity: 1;
            }
            to {
                right: -38%;
                opacity: 0;
            }
        }


        /* ==================== General Style =========== */

        * {
            margin: 0;
            padding: 0;
            outline: none;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            text-decoration: none;
            list-style: none;
        }

        /* ================== Body =================== */
        body {
        <%--background: url(<%=context_folder%>/images/pin_retrieve/BG.png) no-repeat;--%> background: rgb(0, 159, 201);
            background: radial-gradient(circle, rgba(0, 159, 201, 1) 6%, rgba(0, 54, 115, 1) 60%, rgba(0, 27, 93, 1) 91%);
            max-width: 100vw;
            height: 100vh;
            background-position: center center;
            font-family: 'SolaimanLipi', sans-serif;
            -webkit-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
            background-size: cover;
            background-attachment: fixed;
        }

        /* ======================== Top Logo area ================== */
        .leftlogo img {
            width: 159px;
            height: 113px;
            margin-top: 30px;
            margin-left: 30px;
        }

        .rightlogo img {
            width: 175px;
            height: 83px;
            margin-top: 60px;
            float: right;
            margin-right: 60px;
        }

        .leftnews {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-pack: end;
            -ms-flex-pack: end;
            justify-content: flex-end;
            justify-items: flex-end;
            float: right;
            padding-top: 35%;
        }


        .rightnews {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            -webkit-box-pack: start;
            -ms-flex-pack: start;
            justify-content: flex-start;
            justify-items: flex-start;
            float: left;
            padding-top: 35%;
        }

        .formrelative {
            position: relative;
        }

        .bottomrightcontent {
            position: relative;
        }

        .formarea {
            display: block;
            float: left;
            width: 90%;
            margin: 0 5%;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            height: auto;
            background-color: rgba(255, 255, 255, 0.15);
            border-radius: 20px;
            color: #fff;
            /*padding: 26px 47px 37px 47px;*/
        }

        .formarea h2 {
            margin-bottom: 24px;
        }

        .rightallbutton {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: end;
            -ms-flex-pack: end;
            justify-content: flex-end;
            color: #fff;
        }

        .rightallbutton button {
            margin: 0px 0px 0px 10px;
        }

        .rightallbutton .btnleft {
            color: #000;
            background-color: #FFE600;
            border: none;
            border-radius: 8px;
        }

        .rightallbutton .btnright {
            color: #fff;
            background-color: #00D4FF;
            border: none;
            border-radius: 8px;
        }

        .maintitle h2 {
            font-size: 2.25rem;
            margin-bottom: 25px;
        }


        input[type='text']::-webkit-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::-moz-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']:-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='text']::placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-webkit-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-moz-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']:-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::-ms-input-placeholder {
            color: #B1CADB;
            font-size: 14px;
        }

        input[type='password']::placeholder {
            color: #B1CADB;
            font-size: 14px;
        }


        .leftnews img {
            width: 256px;
            height: 154px;
            padding-bottom: 10px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 15px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 15px;
        }

        .rightnews img {
            width: 256px;
            height: 154px;
            padding-bottom: 10px;
        }

        .maintitle h2 {
            font-size: 1.5rem;
            margin-bottom: 0px;
            padding: 12px 0px;
        }

        .leftlogo img {
            width: 130px;
            height: 90px;
            margin-top: 20px;
            margin-left: 30px;
        }

        .rightlogo img {
            width: 120px;
            height: 57px;
            margin-top: 50px;
            float: right;
            margin-right: 60px;
        }

        .mainlogo.text-center img {
            width: 90px;
            height: 90px;
            line-height: 90px;
        }

        .leftnews {
            padding-top: 140px;
        }

        .rightnews {
            padding-top: 140px;
        }

        h2.title {
            font-size: 25px;
        }

        .formarea h2 {
            margin-bottom: 10px;
        }

        /*.formarea {*/
        /*    padding: 24px 47px 25px 47px;*/
        /*}*/

        .leftnews img {
            width: 220px;
            height: 132px;
        }

        .rightnews img {
            width: 220px;
            height: 132px;
        }

        img.img-fluid.circularimage {
            margin-bottom: 20px;
        }

        img.img-fluid.admincardimage {
            margin-bottom: 20px;
        }

        .songshodimage img {
            width: 100%;
            height: 100%;
            margin-top: 30px;
        }

        .btn-border-radius {
            border-radius: 6px !important;
        }

        /* ======================== Responsive Area ================== */

        @media screen and (max-width: 1537px) {
            .songshodimage img {
                width: 78%;
                height: 100%;
                margin-top: 15px;
            }

            .leftnews {
                padding-top: 30%;
            }

            .rightnews {
                padding-top: 30%;
            }

        }

        @media screen and (max-width: 1480px) {

            .songshodimage img {
                width: 78%;
                height: 100%;
                margin-top: 15px;
            }

            .leftnews {
                padding-top: 140px;
            }

            .rightnews {
                padding-top: 140px;
            }


            .maintitle h2 {
                font-size: 1.25rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
        }


        @media screen and (max-width: 1366px) {

            .songshodimage img {
                width: 70%;
                height: 100%;
                margin-top: 0px;
            }

            .leftnews {
                padding-top: 140px;
            }

            .rightnews {
                padding-top: 140px;
            }


            .maintitle h2 {
                font-size: 1.25rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
            .leftlogo img {

                margin-top: 10px;

            }

            .rightlogo img {
                margin-top: 40px;

            }

        }


        @media screen and (max-width: 1100px) {
            .maintitle h2 {
                font-size: 1.15rem;
                margin-bottom: 0px;
                padding: 12px 0px;
            }

            /*.formarea {*/
            /*    padding: 24px 47px 26px 47px;*/
            /*}*/
            .rightallbutton .btnleft {
                font-size: 12px;
            }

            .rightallbutton .btnright {
                font-size: 12px;
            }

            /*.formarea {*/
            /*    padding: 24px 20px 26px 20px;*/
            /*}*/
            .formarea {
                display: block;
                float: left;
                width: 100%;
                margin: 0;
            }

            img.img-fluid.circularimage {
                margin-bottom: 10px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 10px;
            }

            .songshodimage img {
                width: 100%;
            }

        }

        @media screen and (max-width: 980px) {
            .maintitle h2 {
                font-size: 1rem;

            }

            .leftnews {
                padding-top: 135px;
            }

            .rightnews {
                padding-top: 135px;
            }

            img.img-fluid.circularimage {
                margin-bottom: 11px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 11px;
            }

        }

        @media screen and (max-width: 790px) {
            .maintitle h2 {
                font-size: 0.90rem;

            }

            .leftnews {
                padding-top: 135px;
            }

            .rightnews {
                padding-top: 135px;
            }

            img.img-fluid.circularimage {
                margin-bottom: 11px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 11px;
            }

        }

        @media screen and (max-width: 767px) {

            .leftnews {
                padding-top: 30px;
                padding-bottom: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftlogo img {
                margin-left: 6px;
            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

        }

        @media screen and (max-width: 464px) {

            .leftnews {
                padding-top: 30px;
                padding-bottom: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 20px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 195px;
                height: 132px;
            }

            .rightnews img {
                width: 195px;
                height: 132px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

        }


        @media screen and (max-width: 420px) {

            .leftnews {
                padding-top: 10px;
                padding-bottom: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 170px;
                height: 132px;
            }

            .rightnews img {
                width: 170px;
                height: 132px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 0px;
            }

        }

        @media screen and (max-width: 370px) {

            .leftnews {
                padding-top: 10px;
                padding-bottom: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .rightnews {
                padding-top: 10px;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: horizontal;
                -webkit-box-direction: normal;
                -ms-flex-direction: row;
                flex-direction: row;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                width: 100%;
            }

            .leftnews img {
                width: 105px;
                height: 70px;
            }

            .rightnews img {
                width: 105px;
                height: 70px;
            }

            .leftlogo img {
                margin-left: 6px;

            }

            .rightlogo img {
                margin-right: 8px;
            }

            .mainlogo.text-center img {
                width: 25%;
                height: auto;
            }

            img.img-fluid.circularimage {
                margin-bottom: 0px;
            }

            .leftnews img {
                padding-bottom: 0px;
            }

            .songshodimage img {
                margin-top: 0px;
            }

            img.img-fluid.admincardimage {
                margin-bottom: 0px;
            }

        }

    </style>
</head>
<body>

<div id="toast_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;"><i class="fa fa-times" onclick="onClickHideToast()"></i></div>
    <div id="toast_message_div"></div>
</div>

<div id="error_message" class="custom-toast">
    <div style="float: right;margin-left: 4px;"><i class="fa fa-times" onclick="onClickHideToast()"></i></div>
    <div id="error_message_div"></div>
</div>

<input type="hidden" id="my_language" value="<%=my_language%>"/>

<div class="container-fluid">
    <div class="row pt-3 mx-0">
        <div class="col-6">
            <img width="120" src="<%=context_folder%>/images/pin_retrieve/mujib_borsho_logo.png" alt="mujib borsho">
        </div>
        <div class="col-6 text-right">
            <img width="130" src="<%=context_folder%>/images/pin_retrieve/PRP_Logo.png" alt="prp">
        </div>
    </div>
    <div class="row mx-0">
        <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 forlaptopview">
            <div>
                <div class="mainlogo text-center">
                    <a href="#"><img class="img-fluid" src="<%=context_folder%>/images/pin_retrieve/Sangsad_Logo.png"
                                     alt="mainlogo"></a>
                </div>
                <div class="maintitle text-center text-white">
                    <h2>বাংলাদেশ জাতীয় সংসদ সচিবালয়</h2>
                </div>
                <div class="formarea p-4">
                    <form id="pinCodeRetrieveForm" name="pinCodeRetrieveForm"
                          action="<%=request.getContextPath()%>/UserServlet?actionType=pinCodeRetrieve" method="post">
                        <div class="form-group d-flex align-items-center" id="methodDiv">
                            <div class="radio-inline">
                                <input class="" type="radio" name="methodRadio" value="mobile"
                                       onchange="handleMethodRadioClick(this);" checked>
                                মোবাইল নাম্বার
                            </div>
                            <div class="radio-inline ml-3">
                                <input class="" type="radio" name="methodRadio"
                                       onchange="handleMethodRadioClick(this);" value="email">
                                ইমেইল
                            </div>
                        </div>
                        <p id="p_message">পাসওয়ার্ড রিসেটের জন্য ওটিপি পেতে আপনার মোবাইল নম্বর (বাংলায়/ইংরেজিতে)টি
                            দিন</p>

                        <div class="form-group" id="mobileNoDiv">
                            <div class="input-icon">
                                <input autocomplete="nope" class="form-control placeholder-no-fix" id="phoneNo"
                                       name="phoneNo" type="text" value=""
                                       maxlength="11"
                                       placeholder="<%=UtilCharacter.convertDataByLanguage(Language, "01XXXXXXXXX")%>">
                            </div>
                        </div>

                        <div class="form-group" style="display: none" id="emailDiv">
                            <div class="input-icon">
                                <input type='text' class='form-control rounded'
                                       name='email'
                                       id='email'
                                       value=''
                                       placeholder='ইমেইল আইডি টাইপ করুন'
                                       pattern="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
                                       title="ভ্যালিড ইমেইল প্রদান করুন"
                                />
                            </div>
                        </div>
                        <div id="otp_password_div" style="display: none">
                            <div class="form-group mt-2">
                                <div class="input-icon">
                                    <input autocomplete="nope" class="form-control placeholder-no-fix" id="otp_input"
                                           name="otp" type="text" value=""
                                           maxlength="<%=UserServlet.OTP_LENGTH%>"
                                           placeholder="ওটিপি টাইপ করুন(ইংরেজি অংকে)">
                                </div>
                            </div>

                            <div class="form-group mt-2">
                                <div class="input-icon">
                                    <input autocomplete="new-password" type='password'
                                           class='form-control rounded'
                                           placeholder='নতুন পাসওয়ার্ড ইনপুট দিন'
                                           name='password'
                                           id='password'
                                    />
                                </div>
                            </div>

                            <div class="form-group mt-2">
                                <div class="input-icon">
                                    <input type='password'
                                           class='form-control rounded'
                                           placeholder='পাসওয়ার্ড নিশ্চিত করুন'
                                           name='confirmPassword'
                                           id='confirmPassword'
                                    />
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <div class="col-md-12 d-flex align-items-center justify-content-end">
                                <div style="display: none" id="password_submit_div">
                                    <button class="btn btn-success uppercase pull-right btn-border-radius shadow-sm ml-2"
                                            type="button"
                                            id="password_submit"
                                            onclick="submitPassword();">
                                        সাবমিট
                                    </button>
                                </div>
                                <div style="display: none" id="resend_otp_div">
                                    <button class="btn btn-success uppercase pull-right btn-border-radius shadow-sm ml-2"
                                            type="button"
                                            id="resend_otp"
                                            onclick="sendOTP(true)">
                                        নতুন ওটিপি
                                    </button>
                                </div>
                                <div class=" d-flex justify-content-md-end" id="mobile_submit_div">
                                    <button class="btn btn-success uppercase btn-border-radius shadow-sm ml-2"
                                            type="button"
                                            id="mobile_submit"
                                            onclick="sendOTP(false);">
                                        সাবমিট
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0">
        <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 pt-3  px-0">
            <div class="songshodimage text-center mt-0">
                <img class="img-fluid" src="<%=context_folder%>/images/pin_retrieve/shongshod_bg.png" alt="">
            </div>
        </div>
    </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="<%=context_folder%>/assets/backup/js/jquery-3.5.1.slim.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/bootstrap.min.js"></script>
<script src="<%=context_folder%>/assets/backup/js/popper.min.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/login/main.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/util1.js"></script>
<script src="<%=context_folder%>/assets/backup/scripts/pb.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery.min.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-validation/js/jquery.validate.js"></script>
<script src="<%=context_folder%>/assets/backup/global/plugins/jquery-ui/jquery-ui.min.js"></script>

<script>
    let downloadTimer;

    $(document).ready(function () {
        $("#password_submit").prop("disabled", true);
        $("#resend_otp").prop("disabled", true);

    });

    const phoneNoInput = document.getElementById("phoneNo");
    const emailInput = document.getElementById("email");
    const validChars = new Set([
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯',
    ]);

    const validOTPChars = new Set([
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    ]);

    phoneNoInput.addEventListener("keypress", function (event) {
        if (!validChars.has(event.key)) {
            event.preventDefault();
        }
    });

    document.getElementById('otp_input').addEventListener("keypress", function (event) {
        if (!validOTPChars.has(event.key)) {
            event.preventDefault();
        }
    });

    phoneNoInput.addEventListener("keyup", function (event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            resetCitizenPinCode();
        }
    });

    function sendOTP(isResend) {
        const pinCodeRetrieveForm = $("#pinCodeRetrieveForm");
        const url = "UserServlet?actionType=sendOTP";
        $.ajax({
            type: "POST",
            url: url,
            data: pinCodeRetrieveForm.serialize(),
            dataType: 'text',
            success: function (data) {
                const response = JSON.parse(data);
                if (response.success) {
                    showToast(response.messageBn, response.messageEn);
                    if (!isResend) {
                        $("#mobile_submit").prop("disabled", true);
                        $("#mobile_submit_div").removeClass('d-flex');
                        $("#mobile_submit_div").hide();
                        document.getElementById("phoneNo").readOnly = true;
                        document.getElementById("email").readOnly = true;
                        $("#methodDiv").removeClass('d-flex');
                        $("#methodDiv").hide();
                        $("#otp_password_div").show();
                    } else {
                        $("#resend_otp").prop("disabled", true);
                        $("#resend_otp_div").hide();
                        clearOTPPasswords();
                        clearInterval(downloadTimer);
                    }
                    $("#password_submit").prop("disabled", false);
                    $("#password_submit_div").show();
                    timer(response.TimeRemaining);
                } else {
                    showError(response.messageBn, response.messageEn);
                }
            },
            error: function (req, err) {
                showError("ওটিপি প্রদান করা সম্ভব হয়নি", "Could not send OTP");
            }
        });
    }

    function clearOTPPasswords() {
        $("#otp_input").val("");
        $("#password").val("");
        $("#confirmPassword").val("");
    }

    function submitPassword() {
        const pinCodeRetrieveForm = $("#pinCodeRetrieveForm");
        if ($("#otp_input").val() === "") {
            showToast("ওটিপি ইনপুট দিন", "Please Insert OTP");
            return;
        }

        const passwordVal = $("#password").val();
        if (passwordVal === "") {
            showToast("পাসওয়ার্ড ইনপুট দিন", "Please Insert Password");
            return;
        }

        const confirmPasswordVal = $("#confirmPassword").val();
        if (confirmPasswordVal === "") {
            showToast("পাসওয়ার্ড কনফার্ম করুন", "Please Confirm Password");
            return;
        }

        if (passwordVal.length < <%=UserServlet.MIN_PASSWORD_LENGTH%>) {
            showToast("পাসওয়ার্ড ন্যূনতম দৈর্ঘ্যের চেয়ে ছোট হয়েছে", "Password is too short");
            return;
        }

        if (confirmPasswordVal !== passwordVal) {
            showToast("একই পাসওয়ার্ড দিয়ে নিশ্চিত করুন", "Confirm with same password");
            return;
        }

        const url = "UserServlet?actionType=validatePasswordOTP";
        $.ajax({
            type: "POST",
            url: url,
            data: pinCodeRetrieveForm.serialize(),
            dataType: 'text',
            success: function (data) {
                console.log("data", data);
                const response = JSON.parse(data);
                if (response.success) {
                    showToast(response.messageBn, response.messageEn);
                    setTimeout(
                        () => window.location.replace("<%=request.getContextPath()%>" + response.redirectPath),
                        3000
                    );
                } else {
                    showError(response.messageBn, response.messageEn);
                }
            },
            error: function (req, err) {
                showError("পাসওয়ার্ড পরিবর্তন ব্যর্থ হয়েছে!", "Password change failed!");
            }
        });
    }

    function timer(timeLeft) {
        downloadTimer = setInterval(function () {
            if (timeLeft <= 0) {
                clearInterval(downloadTimer);
                document.getElementById("p_message").innerHTML = "ওটিপি মেয়াদোত্তীর্ণ হয়েছে";
                timerOut();
            } else {
                let minutes = Math.floor(timeLeft / 60);
                let seconds = Math.floor(timeLeft % 60);
                document.getElementById("p_message").innerHTML = "ওটিপি মেয়াদোত্তীর্ণ হবে "
                    + convertToBangla(minutes) + " মিনিট "
                    + convertToBangla(seconds) + " সেকেন্ড";
            }
            timeLeft -= 1;
        }, 1000);
    }

    function timerOut() {
        $("#password_submit").prop("disabled", true);
        $("#password_submit_div").hide();
        $("#resend_otp").prop("disabled", false);
        $("#resend_otp_div").show();
        clearOTPPasswords();
    }

    function showToast(bn, en) {
        const toastMessageDiv = document.getElementById("toast_message_div");
        toastMessageDiv.innerHTML = '';
        const my_language = document.getElementById('my_language').value;
        toastMessageDiv.innerHTML = parseInt(my_language) === 1 ? bn : en;

        document.getElementById('toast_message').classList.add("show");

        setTimeout(() => document.getElementById('toast_message').classList.remove("show"), 3000);
    }

    function showError(bn, en) {
        const toastMessageDiv = document.getElementById("error_message_div");
        toastMessageDiv.innerHTML = '';
        const my_language = document.getElementById('my_language').value;
        toastMessageDiv.innerHTML = parseInt(my_language) === 1 ? bn : en;

        document.getElementById('error_message').classList.add("show");

        setTimeout(() => document.getElementById('error_message').classList.remove("show"), 3000);
    }

    function convertToBangla(str) {
        str = String(str);
        str = str.replaceAll('0', '০');
        str = str.replaceAll('1', '১');
        str = str.replaceAll('2', '২');
        str = str.replaceAll('3', '৩');
        str = str.replaceAll('4', '৪');
        str = str.replaceAll('5', '৫');
        str = str.replaceAll('6', '৬');
        str = str.replaceAll('7', '৭');
        str = str.replaceAll('8', '৮');
        str = str.replaceAll('9', '৯');
        return str;
    }

    let currentValue = 'mobile';

    function handleMethodRadioClick(methodRadio) {
        currentValue = methodRadio.value;
        if (currentValue == 'mobile') {
            document.getElementById('p_message').innerText = 'পাসওয়ার্ড রিসেটের জন্য ওটিপি পেতে আপনার মোবাইল নম্বর (বাংলায়/ইংরেজিতে)টি দিন';
            $("#mobileNoDiv").show();
            $("#emailDiv").hide();
        } else {
            document.getElementById('p_message').innerText = 'পাসওয়ার্ড রিসেটের জন্য ওটিপি পেতে আপনার ইমেইল আইডিটি দিন';
            $("#mobileNoDiv").hide();
            $("#emailDiv").show();
        }

    }
</script>
</body>
</html>