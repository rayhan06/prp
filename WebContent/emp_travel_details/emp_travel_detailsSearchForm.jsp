<%@page import="emp_travel_details.*" %>
<%@ page import="util.RecordNavigator" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="user.*" %>
<%@ page import="java.util.List" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>

<%
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    RecordNavigator rn2 = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String pageno2 = (rn2 == null) ? "1" : "" + rn2.getCurrentPageNo();
    String totalpage2 = (rn2 == null) ? "1" : "" + rn2.getTotalPages();
    String totalRecords2 = (rn2 == null) ? "1" : "" + rn2.getTotalRecords();
    String lastSearchTime = (rn2 == null) ? "0" : "" + rn2.getSearchTime();
%>

<div class="table-responsive">
    <table id="tableData" class="table table-bordered table-striped text-nowrap">
        <thead>
        <tr>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_EMPLOYEERECORDSID, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELPLANNUMBER, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELTYPECAT, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_MODELOFTRAVELCAT, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELPURPOSECAT, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELDESTINATION, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRANSPORTFACILITYCAT, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_DEPARTUREDATE, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_RETURNDATE, userDTO)%>
            </th>
            <th><%=LM.getText(LC.HM_VIEW_DETAILS, userDTO)%>
            </th>
            <th><%=LM.getText(LC.EMP_TRAVEL_DETAILS_SEARCH_EMP_TRAVEL_DETAILS_EDIT_BUTTON, userDTO)%>
            </th>
            <th class="text-center">
                <span><%="English".equalsIgnoreCase(Language)?"All":"সকল"%></span>
                <div class="d-flex align-items-center justify-content-between">
                    <button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">
                        <i class="fa fa-trash"></i>&nbsp;
                    </button>
                    <input type="checkbox" name="delete" id="deleteAll" onclick=""/>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <%
            List<Emp_travel_detailsDTO> data = (List<Emp_travel_detailsDTO>) rn2.list;

                if (data != null && data.size() > 0) {
                    for (int i = 0; i < data.size(); i++) {
                        Emp_travel_detailsDTO emp_travel_detailsDTO = data.get(i);
        %>
        <tr id='tr_<%=i%>'>
            <%@include file="emp_travel_detailsSearchRow.jsp" %>
        </tr>
        <% } } %>
        </tbody>
    </table>
</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>"/>
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>"/>
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>"/>
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>"/>
<input type="hidden" id="isPermanentTable" value="true"/>