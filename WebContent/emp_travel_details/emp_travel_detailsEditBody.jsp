<%@page import="login.LoginDTO" %>
<%@page import="emp_travel_details.*" %>
<%@page import="java.util.*" %>
<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="files.*" %>
<%@page import="dbm.*" %>
<%@ page import="user.*" %>
<%@ page import="pb.*" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="geolocation.GeoDistrictRepository" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="util.HttpRequestUtils" %>
<%@page pageEncoding="UTF-8" %>
<%
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    String actionName, formTitle;
    Emp_travel_detailsDTO emp_travel_detailsDTO;
    if ("edit".equals(request.getParameter("actionType"))) {
        actionName = "edit";
        formTitle = LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_EMP_TRAVEL_DETAILS_EDIT_FORMNAME, loginDTO);
        emp_travel_detailsDTO = (Emp_travel_detailsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    } else {
        actionName = "add";
        formTitle = LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_EMP_TRAVEL_DETAILS_ADD_FORMNAME, loginDTO);
        emp_travel_detailsDTO = new Emp_travel_detailsDTO();
    }
    long empId;
    if (request.getParameter("empId") != null) {
        empId = Long.parseLong(request.getParameter("empId"));
    } else {
        empId = userDTO.employee_record_id;
    }
    long ColumnID;
    String select2PlaceHolder = LM.getText(LC.GLOBAL_TYPE_TO_SELECT, Language);
    String url = "Emp_travel_detailsServlet?actionType=ajax_" + actionName + "&isPermanentTable=true&empId=" + empId + "&iD=" + emp_travel_detailsDTO.iD;
    if (request.getParameter("tab") != null) {
        url += "&tab=" + request.getParameter("tab") + "&userId=" + request.getParameter("userId");
    }
    String context = request.getContextPath() + "/";
    int year = Calendar.getInstance().get(Calendar.YEAR);
    boolean isLangEng = Language.equalsIgnoreCase("English");
%>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=formTitle%>
                </h3>
            </div>
        </div>

        <form class="form-horizontal kt-form" action="<%=url%>"
              id="bigform" name="bigform">
            <!-- FORM BODY SKULL -->
            <div class="kt-portlet__body form-body">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="onlyborder">
                            <div class="row px-4 px-md-0">
                                <div class="col-md-10 offset-md-1">
                                    <div class="sub_title_top">
                                        <div class="sub_title">
                                            <h4 style="background-color: #FFFFFF">
                                                <%=formTitle%>
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="travelTypeCat_category">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELTYPECAT, loginDTO)%>
                                            <span class="required"> *</span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='travelTypeCat_div'>
                                            <select class='form-control' name='travelTypeCat'
                                                    required="required" onchange="travelTypeChange()"
                                                    id='travelTypeCat_category' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("travel_type_cat", Language, emp_travel_detailsDTO.travelTypeCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="travelPurposeCat_category">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_TRAVELPURPOSECAT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='travelPurposeCat_div'>
                                            <select class='form-control' name='travelPurposeCat'
                                                    required="required" onchange="openCalendar()"
                                                    id='travelPurposeCat_category' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("travel_purpose", Language, emp_travel_detailsDTO.travelPurposeCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="other_purpose_div" style="display:none;">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="travelPurposeOther_text">
                                            <%=Language.equalsIgnoreCase("English") ? "Other Purpose Description" : "অন্যান্য উদ্দেশ্য বিবরণ"%>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <textarea rows="3"
                                                      type='text'
                                                      class='form-control '
                                                      name='travelPurposeOther'
                                                      maxlength="256"
                                                      placeholder="<%=!isLangEng?"বিবরণ লিখুন":"Enter Purpose"%>"
                                                      id='travelPurposeOther_text'
                                                      onkeyup="updateDescriptionLen()"
                                                      style="text-align: left;resize: none; width: 100%"
                                                      tag='pb_html'><%=emp_travel_detailsDTO.travelPurposeOther%></textarea>
                                            <%--                                            <p id="description_len"--%>
                                            <%--                                               style="width: 100%;text-align: right;font-size: small"><%=emp_travel_detailsDTO.travelPurposeOther.length()%>--%>
                                            <%--                                                /1024</p>--%>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="modelOfTravelCat_category">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_MODELOFTRAVELCAT, loginDTO)%>

                                        </label>
                                        <div class="col-md-8 col-xl-9" id='modelOfTravelCat_div'>
                                            <select class='form-control'
                                                    name='modelOfTravelCat'
                                                    id='modelOfTravelCat_category' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("model_of_travel", Language, emp_travel_detailsDTO.modelOfTravelCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="transportFacilityCat_category">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_TRANSPORTFACILITYCAT, loginDTO)%>

                                        </label>
                                        <div class="col-md-8 col-xl-9" id='transportFacilityCat_div'>
                                            <select class='form-control'
                                                    name='transportFacilityCat'
                                                    id='transportFacilityCat_category' tag='pb_html'>
                                                <%=CatRepository.getInstance().buildOptions("transport_facility", Language, emp_travel_detailsDTO.transportFacilityCat)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="country_id">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="country_select">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_TRAVELDESTINATION, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='travelDestination_div'>
                                            <select multiple="multiple" class='form-control' name='travelCountry'
                                                    id='country_select' tag='pb_html'>
                                                <%=GeoCountryRepository.getInstance().buildOptions(Language, emp_travel_detailsDTO.travelDestination, OrderByEnum.ASC, false, true)%>
                                            </select>
                                            <input type="hidden" name="country" id="country"
                                                   value="<%=emp_travel_detailsDTO.travelDestination%>">
                                        </div>
                                    </div>
                                    <div class="form-group row" id="district_id">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="district_select">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_TRAVELDESTINATION, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <select class='form-control' multiple="multiple" name='travelDistrict'
                                                    id='district_select' tag='pb_html'>
                                                <%=GeoDistrictRepository.getInstance().buildOptions(Language, emp_travel_detailsDTO.travelDestination, OrderByEnum.ASC, false, true)%>
                                            </select>
                                            <input type="hidden" name="district" id="district"
                                                   value="<%=emp_travel_detailsDTO.travelDestination%>">
                                        </div>
                                    </div>
                                    <div class="form-group row" id="cities_id">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right" for="cities_text">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_PLANNEDCITIES, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='plannedCities_div'>
                                                            <textarea type='text' class='form-control' rows="4"
                                                                      style="text-align:left;resize: none"
                                                                      placeholder="<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_PLACE_HOLDER_PLANNED_CITIES, loginDTO)%>"
                                                                      name='plannedCities'
                                                                      id='cities_text'
                                                                      tag='pb_html'><%=emp_travel_detailsDTO.plannedCities != null ? emp_travel_detailsDTO.plannedCities : ""%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_DEPARTUREDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="departure-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR"
                                                           value="<%=year + 10%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' class='form-control'
                                                   id='departure-date' name='departureDate' value=''
                                                   tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_RETURNDATE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <jsp:include page="/date/date.jsp">
                                                <jsp:param name="DATE_ID"
                                                           value="return-date-js"></jsp:param>
                                                <jsp:param name="LANGUAGE"
                                                           value="<%=Language%>"></jsp:param>
                                                <jsp:param name="END_YEAR"
                                                           value="<%=year + 10%>"></jsp:param>
                                            </jsp:include>
                                            <input type='hidden' class='form-control' id='return-date'
                                                   name='returnDate' value='' tag='pb_html'/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="plannedCostBdt_text">
                                            <%=!isLangEng ? "আয়োজনে" : "Arranged By"%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='arrangedBy_div'>
                                            <input type='text' class='form-control'
                                                   name='arrangedBy'
                                                   placeholder="<%=!isLangEng?"আয়োজনে থাকা সংস্থার নাম লিখুন":"Enter Arranged By Department"%>"
                                                   id='arrangedBy_text'
                                                   value='<%=emp_travel_detailsDTO.arrangedBy!=null?emp_travel_detailsDTO.arrangedBy:""%>'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="plannedCostBdt_text">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_PLANNEDCOSTBDT, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='plannedCostBdt_div'>
                                            <input type='text' class='form-control'
                                                   name='plannedCostBdt'
                                                   placeholder="<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_PLACE_HOLDER_PLANNED_COST, loginDTO)%>"
                                                   id='plannedCostBdt_text'
                                                   value='<%=emp_travel_detailsDTO.plannedCostBdt>0?emp_travel_detailsDTO.plannedCostBdt:""%>'>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="isAdvanceRequired_checkbox_0">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_ISADVANCEREQUIRED, loginDTO)%>
                                        </label>
                                        <div class="col-1" id='isAdvanceRequired_div'>
                                            <input onchange="openAmount()" type='checkbox'
                                                   class='form-control-sm mt-1'
                                                   name='isAdvanceRequired'
                                                   id='isAdvanceRequired_checkbox_0'
                                                   value='true' <%=(actionName.equals("edit") && String.valueOf(emp_travel_detailsDTO.isAdvanceRequired).equals("true"))?("checked"):""%>
                                                   tag='pb_html'>
                                        </div>
                                        <div class="col-8"></div>
                                    </div>
                                    <div class="form-group row" id="requestedAdvanceAmount"
                                         style="display: none">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="requestedAdvanceAmount_number">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_REQUESTEDADVANCEAMOUNT, loginDTO)%>
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8 col-xl-9">
                                            <input type='text' class='form-control'
                                                   name='requestedAdvanceAmount'
                                                   placeholder="<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_PLACE_HOLDER_ENTER_ADVANCE_AMOUNT, loginDTO)%>"
                                                   id='requestedAdvanceAmount_number'
                                                   value='<%=emp_travel_detailsDTO.requestedAdvanceAmount>0?emp_travel_detailsDTO.requestedAdvanceAmount:""%>'
                                                   tag='pb_html'>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="numberOfPassengers_number">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_NUMBEROFPASSENGERS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='numberOfPassengers_div'>
                                            <input onchange="openPassengerDetails()" type='text'
                                                   placeholder="<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_PLACE_HOLDER_NUMBER_OF_PASSENGER, loginDTO)%>"
                                                   class='form-control' name='numberOfPassengers'
                                                   id='numberOfPassengers_number' tag='pb_html'
                                                   value='<%=emp_travel_detailsDTO.numberOfPassengers > 0 ? emp_travel_detailsDTO.numberOfPassengers : ""%>'>
                                        </div>
                                    </div>

                                    <div class="form-group row" id="detailsOfOtherPassengers"
                                         style="display:none">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="detailsOfOtherPassengers_textarea">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_DETAILSOFOTHERPASSENGERS, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='detailsOfOtherPassengers_div'>
                                               <textarea class='form-control' name='detailsOfOtherPassengers' rows="4"
                                                         style="text-align:left;resize: none"
                                                         id='detailsOfOtherPassengers_textarea'
                                                         placeholder="<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_PLACE_HOLDER_PASSENGER_DETAIL, loginDTO)%>"
                                                         tag='pb_html'><%=emp_travel_detailsDTO.detailsOfOtherPassengers == null ? "" : emp_travel_detailsDTO.detailsOfOtherPassengers%></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right"
                                               for="detailedPurpose_textarea">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_DETAILEDPURPOSE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='detailedPurpose_div'>
                                               <textarea class='form-control' name='detailedPurpose' rows="4"
                                                         style="text-align:left;resize: none"
                                                         id='detailedPurpose_textarea'
                                                         placeholder="<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_PLACE_HOLDER_DETAILS_PURPOSE, loginDTO)%>"
                                                         tag='pb_html'><%=emp_travel_detailsDTO.detailedPurpose == null ? "" : emp_travel_detailsDTO.detailedPurpose%></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-xl-3 col-form-label text-md-right">
                                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_FILESDROPZONE, loginDTO)%>
                                        </label>
                                        <div class="col-md-8 col-xl-9" id='filesDropzone_div'>
                                            <%
                                                if (actionName.equals("edit")) {
                                                    List<FilesDTO> filesDropzoneDTOList = new FilesDAO().getMiniDTOsByFileID(emp_travel_detailsDTO.filesDropzone);
                                            %>
                                            <table>
                                                <tr>
                                                    <%
                                                        if (filesDropzoneDTOList != null) {
                                                            for (int j = 0; j < filesDropzoneDTOList.size(); j++) {
                                                                FilesDTO filesDTO = filesDropzoneDTOList.get(j);
                                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                                    %>
                                                    <td id='filesDropzone_td_<%=filesDTO.iD%>'>
                                                        <%
                                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                                        %>
                                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                                             style='width:100px'/>
                                                        <%
                                                            }
                                                        %>
                                                        <a href='Emp_travel_detailsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                                           download><%=filesDTO.fileTitle%>
                                                        </a>
                                                        <a class='btn btn-danger'
                                                           onclick='deletefile(<%=filesDTO.iD%>, "filesDropzone_td_<%=filesDTO.iD%>", "filesDropzoneFilesToDelete")'>x</a>
                                                    </td>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tr>
                                            </table>
                                            <%
                                                }
                                            %>

                                            <%ColumnID = DBMW.getInstance().getNextSequenceId("fileid"); %>
                                            <div class="dropzone"
                                                 action="Emp_travel_detailsServlet?pageType=<%=actionName%>&actionType=UploadFilesFromDropZone&columnName=filesDropzone&ColumnID=<%=actionName.equals("edit")?emp_travel_detailsDTO.filesDropzone:ColumnID%>">
                                                <input type='file' style="display:none"
                                                       name='filesDropzoneFile'
                                                       id='filesDropzone_dropzone_File' tag='pb_html'/>
                                            </div>
                                            <input type='hidden' name='filesDropzoneFilesToDelete'
                                                   id='filesDropzoneFilesToDelete'
                                                   value='' tag='pb_html'/>
                                            <input type='hidden' name='filesDropzone'
                                                   id='filesDropzone_dropzone' tag='pb_html'
                                                   value='<%=actionName.equals("edit")?emp_travel_detailsDTO.filesDropzone:ColumnID%>'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 mt-3 text-right">
                        <button id="cancel-btn" class="btn-sm shadow text-white border-0 cancel-btn" type="button">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_EMP_TRAVEL_DETAILS_CANCEL_BUTTON, loginDTO)%>
                        </button>
                        <button id="submit-btn" class="btn-sm shadow text-white border-0 submit-btn ml-2" type="button"
                                onclick="submitForm()">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_EMP_TRAVEL_DETAILS_SUBMIT_BUTTON, loginDTO)%>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<%=context%>assets/scripts/number_validation_1.js" type="text/javascript"></script>
<script type="text/javascript">
    const travelForm = $('#bigform');
    const requestedAdvanceAmountSelector = $('#requestedAdvanceAmount_number');
    const plannedCostBdt_textSelector = $('#plannedCostBdt_text');

    function travelTypeChange() {
        let travelType = $('#travelTypeCat_category').val();
        if (travelType) {
            if (travelType == 1) {
                $('#country_id').hide();
                $('#district_id').show();
                $('#district_select').prop('required', true);
                $('#country_select').prop('required', false);
                $('#cities_id').show();
                if ($('#country_select').val()) {
                    $('#country_select').val(-1).trigger('change');
                    $('#country').val("");
                    $('#select2-country_select-container').text('<%=select2PlaceHolder%>');
                    $('#select2-country_select-container').css('color', '#999999');
                }
            } else if (travelType == 2) {
                $('#country_id').show();
                $('#country_select').prop('required', true);
                $('#district_select').prop('required', false);
                $('#district_id').hide();
                $('#cities_id').show();
                if ($('#district_select').val()) {
                    $('#district_select').val(-1).trigger('change');
                    $('#district').val("");
                    $('#select2-district_select-container').text('<%=select2PlaceHolder%>');
                    $('#select2-district_select-container').css('color', '#999999');
                }
            } else {
                resetDestination();
            }
        } else {
            resetDestination();
        }
    }

    function resetDestination() {
        $('#country_id').hide();
        $('#district_id').hide();
        $('#cities_id').hide();
        $('#district_select').val('');
        $('#district').val('');
        $('#country_select').val('');
        $('#country').val('');
        $('#cities_text').val('');
    }

    $(document).ready(function () {
        $("#bigform").validate({
            errorClass: 'error is-invalid',
            validClass: 'is-valid',
            messages: {
                travelTypeCat: "<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_SELECT_TRAVEL_TYPE,userDTO)%>",
                travelPurposeCat: "<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_SELECT_TRAVEL_PURPOSE,userDTO)%>",
                modelOfTravelCat: "<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_SELECT_TRAVEL_MODE,userDTO)%>",
                transportFacilityCat: "<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_SELECT_TRANSPORT_FACILITY,userDTO)%>",
                travelCountry: "<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_TRAVEL_DESTINATION,userDTO)%>",
                travelDistrict: "<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_TRAVEL_DESTINATION,userDTO)%>",
                requestedAdvanceAmount: "<%=LM.getText(LC.EMP_TRAVEL_DETAILS_ADD_ENTER_ADVANCE_AMOUNT,userDTO)%>",
                travelPurposeOther: "<%=isLangEng?"অনুগ্রহ করে উদ্দেশ্য লিখুন":"Please Enter Other Purpose"%>"
            }
        });
        requestedAdvanceAmountSelector.keydown(e => {
            return inputValidationForIntValue(e, requestedAdvanceAmountSelector, 100000000);
        });
        plannedCostBdt_textSelector.keydown(e => {
            return inputValidationForIntValue(e, plannedCostBdt_textSelector, 100000000);
        });
        $('#numberOfPassengers_number').keydown(e => {
            return inputValidationForIntValue(e, $('#numberOfPassengers_number'), 100000000);
        });
        $("#cancel-btn").click(e => {
            e.preventDefault();
            location.href = "<%=request.getHeader("referer")%>";
        });
        select2SingleSelector("#travelTypeCat_category", '<%=Language%>');
        select2SingleSelector("#travelPurposeCat_category", '<%=Language%>');
        select2SingleSelector("#modelOfTravelCat_category", '<%=Language%>');
        select2SingleSelector("#transportFacilityCat_category", '<%=Language%>');
        select2MultiSelector("#country_select", '<%=Language%>');
        select2MultiSelector("#district_select", '<%=Language%>');
        dateTimeInit("<%=Language%>");
        var passenger2 = document.getElementById("numberOfPassengers_number").value;
        if (passenger2 > 0) {
            $("#detailsOfOtherPassengers").show();
        } else {
            $("#detailsOfOtherPassengers").hide();
            $('#detailsOfOtherPassengers_textarea').val('');
        }
        var purposeInit = $('#travelPurposeCat_category option:selected').text();
        if (purposeInit == 'Training' || purposeInit == 'প্রশিক্ষণ') {
            $(".trainingCalendar").show();
        } else {
            $(".trainingCalendar").hide();
        }
        travelTypeChange();
        openAmount();
        if ($('#travelPurposeCat_category').val() ==<%=Emp_travel_detailsServlet.travelPurposeOtherCategory%>) {
            $("#other_purpose_div").show();
            $('#travelPurposeOther_text').prop('required', true);
        }
    });


    function PreprocessBeforeSubmiting(row, validate) {
        $('#departure-date').val(getDateStringById('departure-date-js'));
        $('#return-date').val(getDateStringById('return-date-js'));
        ProcessMultipleSelectBoxBeforeSubmit("travelCountry", "country");
        ProcessMultipleSelectBoxBeforeSubmit("travelDistrict", "district");
        const jQueryValid = travelForm.valid();
        preprocessCheckBoxBeforeSubmitting('isAdvanceRequired', row);
        return jQueryValid ;
    }

    function submitForm() {
        buttonStateChange(true);
        if (PreprocessBeforeSubmiting(0, '<%=actionName%>')) {
            submitAjaxForm();
        } else {
            buttonStateChange(false);
        }
    }

    function buttonStateChange(value) {
        $('#submit-btn').prop('disabled', value);
        $('#cancel-btn').prop('disabled', value);
    }

    function updateDescriptionLen() {
        $('#description_len').text($('#travelPurposeOther_text').val().length + "/1024");
    }

    function openCalendar() {
        var purpose = $('#travelPurposeCat_category option:selected').text();
        if (purpose == 'Training' || purpose == 'প্রশিক্ষণ') {
            $(".trainingCalendar").show();
        } else {
            $(".trainingCalendar").hide();
        }
        if ($('#travelPurposeCat_category').val() ==<%=Emp_travel_detailsServlet.travelPurposeOtherCategory%>) {
            $("#other_purpose_div").show();
            $('#travelPurposeOther_text').prop('required', true);
        } else {
            $("#other_purpose_div").hide();
            $('#travelPurposeOther_text').prop('required', false);
        }
    }

    function openAmount() {
        var isChecked = document.getElementById("isAdvanceRequired_checkbox_0").checked;
        if (isChecked) {
            $("#requestedAdvanceAmount").show();
            requestedAdvanceAmountSelector.prop('required', true);
        } else {
            $("#requestedAdvanceAmount").hide();
            requestedAdvanceAmountSelector.prop('required', false);
            requestedAdvanceAmountSelector.val('');
        }
    }

    function openPassengerDetails() {
        var passenger = document.getElementById("numberOfPassengers_number").value;
        if (passenger > 0) {
            $("#detailsOfOtherPassengers").show();
        } else {
            $("#detailsOfOtherPassengers").hide();
        }
    }

    function ProcessMultipleSelectBoxBeforeSubmit(name, hiddenFieldName) {
        $("[name='" + name + "']").each(function (i) {
            var selectedInputs = $(this).val();
            var temp = "";
            if (selectedInputs != null) {
                selectedInputs.forEach(function (value, index, array) {
                    if (index > 0) {
                        temp += ", ";
                    }
                    temp += value;
                });
            }
            document.getElementById(hiddenFieldName).value = temp;
        });
    }

    $(function () {

        $('#departure-date-js').on('datepicker.change', () => {
            setMinDateById('return-date-js', getDateStringById('departure-date-js'));
        });

        <%
      if(actionName.equals("edit")) {%>
        setDateByTimestampAndId('departure-date-js', <%=emp_travel_detailsDTO.departureDate%>);
        setDateByTimestampAndId('return-date-js', <%=emp_travel_detailsDTO.returnDate%>);
        <%}%>
    });
</script>

<style>
    .button-cross-picker {
        height: 34px;
        width: 37px;
        border: 1px solid #c2cad8;
    }

    .input-width89 {
        width: 89%;
        float: left;
    }
</style>