<%@page import="employee_records.Employee_recordsRepository" %>
<%@ page import="pb.*" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="geolocation.GeoDistrictRepository" %>


<td id='<%=i%>_employeeRecordsId'>
    <%=Employee_recordsRepository.getInstance().getEmployeeName(emp_travel_detailsDTO.employeeRecordsId, Language) %>
</td>


<td id='<%=i%>_travelPlanNumber'>
    <%=emp_travel_detailsDTO.travelPlanNumber%>
</td>


<td id='<%=i%>_travelTypeCat'>
    <%=CatRepository.getInstance().getText(Language, "travel_type_cat", emp_travel_detailsDTO.travelTypeCat)%>
</td>


<td id='<%=i%>_modelOfTravelCat'>
    <%=CatRepository.getInstance().getText(Language, "model_of_travel", emp_travel_detailsDTO.modelOfTravelCat)%>
</td>


<td id='<%=i%>_travelPurposeCat'>
    <%=CatRepository.getInstance().getText(Language, "travel_purpose", emp_travel_detailsDTO.travelPurposeCat)%>
</td>

<%
    if (emp_travel_detailsDTO.travelTypeCat == 1) {
%>
<td id='<%=i%>_travelDestination'><%=GeoDistrictRepository.getInstance().getTexts(Language, emp_travel_detailsDTO.travelDestination)%>
</td>
<%
} else {
%>
<td id='<%=i%>_travelDestination'><%=GeoCountryRepository.getInstance().getTexts(Language, emp_travel_detailsDTO.travelDestination)%>
</td>
<%
    }
%>


<td id='<%=i%>_transportFacilityCat'>
    <%=CatRepository.getInstance().getText(Language, "transport_facility", emp_travel_detailsDTO.transportFacilityCat)%>
</td>


<td id='<%=i%>_departureDate'>
    <%=StringUtils.getFormattedDate(Language, emp_travel_detailsDTO.departureDate)%>
</td>


<td id='<%=i%>_returnDate'>
    <%=StringUtils.getFormattedDate(Language, emp_travel_detailsDTO.returnDate)%>
</td>


<td>
    <button type="button" class="btn-sm border-0 shadow bg-light btn-border-radius" style="color: #ff6b6b;"
            onclick="location.href='Emp_travel_detailsServlet?actionType=view&iD=<%=emp_travel_detailsDTO.iD%>&empId=<%=emp_travel_detailsDTO.employeeRecordsId%>'">
        <i class="fa fa-eye"></i>
    </button>
</td>

<td id='<%=i%>_Edit'>
    <button type="button" class="btn-sm border-0 shadow btn-border-radius text-white" style="background-color: #ff6b6b;"
            onclick="location.href='Emp_travel_detailsServlet?actionType=getEditPage&iD=<%=emp_travel_detailsDTO.iD%>&empId=<%=emp_travel_detailsDTO.employeeRecordsId%>'">
        <i class="fa fa-edit"></i>
    </button>
</td>

<td id='<%=i%>_checkbox' class="text-right">
    <div class='checker'>
        <span class='chkEdit'><input type='checkbox' name='ID' value='<%=emp_travel_detailsDTO.iD%>'/></span>
    </div>
</td>