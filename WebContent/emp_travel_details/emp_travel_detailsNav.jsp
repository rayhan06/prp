<%@page import="language.LC" %>
<%@page import="login.LoginDTO" %>
<%@page import="language.LM" %>
<%@ page import="util.RecordNavigator" %>
<%@ page import="pb.*" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@page contentType="text/html;charset=utf-8" %>

<%
    String action = request.getParameter("url");
    LoginDTO loginDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().loginDTO;
    RecordNavigator rn = (RecordNavigator) request.getAttribute(BaseServlet.RECORD_NAVIGATOR);
    String context = "../../.." + request.getContextPath() + "/";
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
%>


<!-- Start: search control -->
<div class="kt-portlet  kt-portlet--collapse shadow-none border-0" data-ktportlet="true" id="kt_portlet_tools_1"
     style="">
    <div class="kt-portlet__head border-0">
        <div class="kt-portlet__head-label" style="width: 100%;">
            <div class="input-group" style="border: 1px solid #00a1d4; border-left: none; border-radius: 5px;">
                <div class="input-group-prepend">
                    <a href="#" data-ktportlet-tool="toggle" class="btn text-center pl-4 d-flex"
                       aria-describedby="tooltip_p6zf7aqcpv"
                       style="background-color: #00a1d4; border-bottom: 2px solid #00a1d4">
                        <i class="fa fa-caret-down text-light"></i>
                    </a>
                </div>
                <input placeholder=<%=LM.getText(LC.GLOBAL_SEARCH, loginDTO)%> autocomplete='off' type='text'
                       class='form-control border-0'
                       onKeyUp='allfield_changed("",0)' id='anyfield' name='anyfield'
                       value='<%=session.getAttribute("AnyField") == null ? "":(String) session.getAttribute("AnyField")%>'
                >
                <div class="input-group-append mt-4 px-3">
                    <i class="fa fa-search" style="color: #D3D3D3"></i>
                </div>
            </div>
        </div>
        <%--        <div class="kt-portlet__head-toolbar">--%>
        <%--            <div class="kt-portlet__head-group">--%>
        <%--                <div class="tooltip tooltip-portlet tooltip bs-tooltip-top" role="tooltip" id="tooltip_p6zf7aqcpv"--%>
        <%--                     aria-hidden="true" x-placement="top"--%>
        <%--                     style="position: absolute; will-change: transform; visibility: hidden; top: 0; left: 0; transform: translate3d(631px, -39px, 0px);">--%>
        <%--                    <div class="tooltip-arrow arrow" style="left: 34px;"></div>--%>
        <%--                    <div class="tooltip-inner">Collapse</div>--%>
        <%--                </div>--%>
        <%--            </div>--%>
        <%--        </div>--%>
    </div>
    <div class="kt-portlet__body" style="display: none">
        <!-- BEGIN FORM-->
        <div class="ml-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="travel_type_cat">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_SEARCH_TRAVELTYPECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='travel_type_cat' id='travel_type_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("travel_type_cat", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="model_of_travel_cat">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_SEARCH_MODELOFTRAVELCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='model_of_travel_cat' id='model_of_travel_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("model_of_travel", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="travel_purpose_cat">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_SEARCH_TRAVELPURPOSECAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='travel_purpose_cat' id='travel_purpose_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("travel_purpose", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="travel_destination">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_SEARCH_TRAVELDESTINATION, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='travel_destination' id='travel_destination'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=GeoCountryRepository.getInstance().buildOptions(Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="transport_facility_cat">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_SEARCH_TRANSPORTFACILITYCAT, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <select class='form-control' name='transport_facility_cat' id='transport_facility_cat'
                                    onSelect='setSearchChanged()' style="width: 100%">
                                <%=CatRepository.getInstance().buildOptions("transport_facility", Language, null)%>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_SEARCH_DEPARTUREDATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="departure_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden'
                               class='form-control'
                               id='departure_date'
                               name='departure_date'
                               value='' tag='pb_html'/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">
                            <%=LM.getText(LC.EMP_TRAVEL_DETAILS_SEARCH_RETURNDATE, loginDTO)%>
                        </label>
                        <div class="col-md-9">
                            <jsp:include page="/date/date.jsp">
                                <jsp:param name="DATE_ID"
                                           value="return_date_js"></jsp:param>
                                <jsp:param name="LANGUAGE"
                                           value="<%=Language%>"></jsp:param>
                            </jsp:include>
                        </div>
                        <input type='hidden'
                               class='form-control'
                               id='return_date'
                               name='return_date'
                               value='' tag='pb_html'/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right">
                    <input type="hidden" name="search" value="yes"/>
                    <button type="submit"
                            class="btn btn-border-radius text-white shadow green-meadow btn-outline sbold uppercase advanceseach"
                            onclick="allfield_changed('',0)"
                            style="background-color: #00a1d4;">
                        <%=LM.getText(LC.GLOBAL_SEARCH, loginDTO) %>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End: search control -->

<%@include file="../common/pagination_with_go2.jsp" %>
<template id="loader">
    <div class="modal-body">
        <img alt="" class="loading" src="<%=context%>/templates/ViewGrievances_files/loading-spinner-grey.gif">
        <span>Loading...</span>
    </div>
</template>


<script type="text/javascript">

    $(document).ready(() => {
        select2SingleSelector('#travel_type_cat', '<%=Language%>');
        select2SingleSelector('#model_of_travel_cat', '<%=Language%>');
        select2SingleSelector('#travel_purpose_cat', '<%=Language%>');
        select2SingleSelector('#travel_destination', '<%=Language%>');
        select2SingleSelector('#transport_facility_cat', '<%=Language%>');
    });

    function dosubmit(params) {
        document.getElementById('tableForm').innerHTML = document.getElementsByTagName("template")[0].innerHTML;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('tableForm').innerHTML = this.responseText;
                setPageNo();
                searchChanged = 0;
            } else if (this.readyState == 4 && this.status != 200) {
                alert('failed ' + this.status);
            }
        };

        xhttp.open("Get", "<%=action%>&isPermanentTable=true&" + params, true);
        xhttp.send();

    }

    function allfield_changed(go, pagination_number) {
        var params = 'AnyField=' + document.getElementById('anyfield').value;
        $("#departure_date").val(getDateStringById("departure_date_js"));
        $("#return_date").val(getDateStringById("return_date_js"));
        params += '&travel_type_cat=' + document.getElementById('travel_type_cat').value;
        params += '&model_of_travel_cat=' + document.getElementById('model_of_travel_cat').value;
        params += '&travel_purpose_cat=' + document.getElementById('travel_purpose_cat').value;
        params += '&travel_destination=' + document.getElementById('travel_destination').value;
        params += '&transport_facility_cat=' + document.getElementById('transport_facility_cat').value;
        if ($("#departure_date").val()) {
            params += '&departure_date=' + getBDFormattedDate('departure_date');
        }
        if ($("#return_date").val()) {
            params += '&return_date=' + getBDFormattedDate('return_date');
        }
        params += '&search=true&ajax=true';

        var extraParams = document.getElementsByName('extraParam');
        extraParams.forEach((param) => {
                params += "&" + param.getAttribute("tag") + "=" + param.value;
            }
        )

        var pageNo = document.getElementsByName('pageno')[0].value;
        var rpp = document.getElementsByName('RECORDS_PER_PAGE')[0].value;

        var totalRecords = 0;
        var lastSearchTime = 0;
        if (document.getElementById('hidden_totalrecords')) {
            totalRecords = document.getElementById('hidden_totalrecords').value;
            lastSearchTime = document.getElementById('hidden_lastSearchTime').value;
        }


        if (go !== '' && searchChanged == 0) {
            console.log("go found");
            params += '&go=1';
            pageNo = document.getElementsByName('pageno')[pagination_number].value;
            rpp = document.getElementsByName('RECORDS_PER_PAGE')[pagination_number].value;
            setPageNoInAllFields(pageNo);
            setRPPInAllFields(rpp);
        }
        params += '&pageno=' + pageNo;
        params += '&RECORDS_PER_PAGE=' + rpp;
        params += '&TotalRecords=' + totalRecords;
        params += '&lastSearchTime=' + lastSearchTime;
        dosubmit(params);

    }
</script>


