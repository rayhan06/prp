<%@ page import="emp_travel_details.Emp_travel_detailsDtoWithValue" %>
<%@ page import="language.LC" %>
<%@ page import="language.LM" %>
<%@ page import="java.util.List" %>

<input type="hidden" data-ajax-marker="true">

<table class="table table-striped table-bordered" style="font-size: 14px">
    <thead>
    <tr>
        <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELTYPECAT, loginDTO)%>
        </b></th>
        <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELDESTINATION, loginDTO)%>
        </b></th>
        <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_DEPARTUREDATE, loginDTO)%>
        </b></th>
        <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELPURPOSECAT, loginDTO)%>
        </b></th>
        <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_PLANNEDCITIES, loginDTO)%>
        </b></th>
        <th><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_RETURNDATE, loginDTO)%>
        </b></th>
        <%
            if (!actionType.equals("viewSummary")) {
        %>
        <th><b></b></th>
        <%
            }
        %>
    </tr>
    </thead>
    <tbody>
        <%
        List<Emp_travel_detailsDtoWithValue> emp_travel_details = (List<Emp_travel_detailsDtoWithValue>) request.getAttribute("emp_travel_details");
            int travelIndex = 0;
            if (emp_travel_details != null && emp_travel_details.size() > 0) {
                for (Emp_travel_detailsDtoWithValue emp_travel_detail : emp_travel_details) {
                    ++travelIndex;
            %>
    <tr>
        <%@include file="/emp_travel_details/emp_travel_details_info_view_item.jsp" %>
    </tr>
        <%
            }
        }
        %>
</table>


<div class="row">
    <div class="col-12 text-right mt-3">
        <button class="btn btn-gray m-t-10 rounded-pill"
                onclick="location.href='Emp_travel_detailsServlet?actionType=getAddPage&employeeRecordsId=<%=ID%>&tab=5&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
            <i class="fa fa-plus"></i>&nbsp; <%= LM.getText(LC.HR_MANAGEMENT_TRAVEL_AND_CERTIFICATION_ADD_TRAVEL, loginDTO) %>
        </button>
    </div>
</div>
