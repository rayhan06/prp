
<td style="vertical-align: middle;">
    <%=isLanguageEnglish ? emp_travel_detail.travelTypeEng : emp_travel_detail.travelTypeBng%>
</td>

<td style="vertical-align: middle;">
    <%=isLanguageEnglish ? emp_travel_detail.travelDestinationEng : emp_travel_detail.travelDestinationBng%>
</td>

<td style="vertical-align: middle;">
    <%=isLanguageEnglish ? emp_travel_detail.departureDateEng : emp_travel_detail.departureDateBng%>
</td>

<td style="vertical-align: middle;">
    <%=isLanguageEnglish ? emp_travel_detail.travelPurposeEng : emp_travel_detail.travelPurposeBng%>
</td>

<td style="vertical-align: middle;">
    <%=emp_travel_detail.dto.plannedCities%>
</td>

<td style="vertical-align: middle;">
    <%=isLanguageEnglish ? emp_travel_detail.returnDateEng : emp_travel_detail.returnDateBng%>
</td>


<%
    if (!actionName.equals("viewSummary")) {
%>
<td style="text-align: center; vertical-align: middle;">
    <form action="Emp_travel_detailsServlet?isPermanentTable=true&actionType=delete&tab=5&ID=<%=emp_travel_detail.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>"
          method="POST" id="travailDetailsTableForm<%=travelIndex%>" enctype = "multipart/form-data">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn-primary" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_VIEW_DETAILS, loginDTO) %>"
                    onclick="location.href='Emp_travel_detailsServlet?actionType=view&ID=<%=emp_travel_detail.dto.iD%>&empId=<%=empId%>&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-eye"></i>&nbsp;
            </button>&nbsp;
            <button type="button" class="btn-success" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_EDIT, loginDTO) %>"
                    onclick="location.href='Emp_travel_detailsServlet?actionType=getEditPage&ID=<%=emp_travel_detail.dto.iD%>&empId=<%=empId%>&tab=5&userId=<%=request.getParameter("userId")%>'">
                <i class="fa fa-edit"></i>&nbsp;</button>&nbsp;
            <button type="button" class="btn-danger" title="<%= LM.getText(LC.HR_MANAGEMENT_BUTTON_DELETE, loginDTO) %>"
                    onclick="deleteItem('travailDetailsTableForm',<%=travelIndex%>)"><i
                    class="fa fa-trash"></i>&nbsp;</button>
        </div>
    </form>
</td>
<%
    }
%>