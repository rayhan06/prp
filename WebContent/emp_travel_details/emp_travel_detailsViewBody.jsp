<%@page import="language.LC" %>
<%@page import="language.LM" %>
<%@page import="emp_travel_details.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="files.*" %>
<%@ page import="geolocation.GeoCountryRepository" %>
<%@ page import="util.StringUtils" %>
<%@ page import="user.UserDTO" %>
<%@ page import="employee_records.Employee_recordsRepository" %>
<%@ page import="util.HttpRequestUtils" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="geolocation.GeoDistrictRepository" %>
<%@ page import="util.UtilCharacter" %>

<%
    UserDTO userDTO = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().userDTO;
    String Language = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().language;
    boolean isLanguageEnglish = HttpRequestUtils.COMMON_LOGIN_DATA_THREAD_LOCAL.get().isLangEng;
    Emp_travel_detailsDTO emp_travel_detailsDTO = (Emp_travel_detailsDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
%>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding: 0 !important;">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">
                    <i class="fa fa-gift"></i><%=isLanguageEnglish ? "Employee's Travel Details" : "কর্মকর্তার ভ্রমণের বিবরণ"%>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body form-body">
            <h5 class="table-title"><%=isLanguageEnglish ? "Employee's Travel Details" : "কর্মকর্তার ভ্রমণের বিবরণ"%>
            </h5>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-wrap">
                    <tr>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_EMPLOYEERECORDSID, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=Employee_recordsRepository.getInstance().getEmployeeName(emp_travel_detailsDTO.employeeRecordsId, Language)%>
                        </td>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELPLANNUMBER, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=emp_travel_detailsDTO.travelPlanNumber%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELTYPECAT, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=CatRepository.getInstance().getText(Language, "travel_type_cat", emp_travel_detailsDTO.travelTypeCat)%>
                        </td>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_MODELOFTRAVELCAT, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=CatRepository.getInstance().getText(Language, "model_of_travel", emp_travel_detailsDTO.modelOfTravelCat)%>
                        </td>
                    </tr>

                    <%
                        String travelPurpose = CatRepository.getInstance().getText(Language, "travel_purpose", emp_travel_detailsDTO.travelPurposeCat);
                        if (emp_travel_detailsDTO.travelPurposeCat == Emp_travel_detailsServlet.travelPurposeOtherCategory)
                            travelPurpose += ": " + emp_travel_detailsDTO.travelPurposeOther;
                    %>
                    <tr>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELPURPOSECAT, userDTO)%>
                        </b></td>
                        <td style="width: 35%;"><%=travelPurpose%>
                        </td>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRAVELDESTINATION, userDTO)%>
                        </b></td>
                        <%
                            if (emp_travel_detailsDTO.travelTypeCat == 1) {
                        %>
                        <td style="width: 35%"><%=GeoDistrictRepository.getInstance().getTexts(Language, emp_travel_detailsDTO.travelDestination)%>
                        </td>
                        <%
                        } else {
                        %>
                        <td style="width: 35%"><%=GeoCountryRepository.getInstance().getTexts(Language, emp_travel_detailsDTO.travelDestination)%>
                        </td>
                        <%
                            }
                        %>

                    </tr>

                    <tr>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_PLANNEDCITIES, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=emp_travel_detailsDTO.plannedCities%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_TRANSPORTFACILITYCAT, userDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=CatRepository.getInstance().getText(Language, "transport_facility", emp_travel_detailsDTO.transportFacilityCat)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_ISACCOMODATIONREQUIRED, userDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=StringUtils.getYesNo(Language, emp_travel_detailsDTO.isAccomodationRequired)%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_ACCOMODATIONADDRESS, userDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=emp_travel_detailsDTO.accomodationAddress%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_DEPARTUREDATE, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=StringUtils.getFormattedDate(Language, emp_travel_detailsDTO.departureDate)%>
                        </td>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_RETURNDATE, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=StringUtils.getFormattedDate(Language, emp_travel_detailsDTO.returnDate)%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_DETAILEDPURPOSE, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=emp_travel_detailsDTO.detailedPurpose%>
                        </td>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_NUMBEROFWORKINGDAYS, userDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=StringUtils.convertIntToString(Language, emp_travel_detailsDTO.numberOfWorkingDays)%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%">
                            <b><%=isLanguageEnglish ? "Arranged By" : "আয়োজনে" %>
                            </b></td>
                        <td colspan="3"><%=emp_travel_detailsDTO.arrangedBy%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_PLANNEDCOSTBDT, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=StringUtils.convertIntToString(Language, emp_travel_detailsDTO.plannedCostBdt)%>
                        </td>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_ISADVANCEREQUIRED, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=StringUtils.getYesNo(Language, emp_travel_detailsDTO.isAdvanceRequired)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_REQUESTEDADVANCEAMOUNT, userDTO)%>
                            </b></td>
                        <td style="width: 35%"><%=StringUtils.convertIntToString(Language, emp_travel_detailsDTO.requestedAdvanceAmount)%>
                        </td>
                        <td style="width:15%"><b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_NUMBEROFPASSENGERS, userDTO)%>
                        </b></td>
                        <td style="width: 35%"><%=StringUtils.convertIntToString(Language, emp_travel_detailsDTO.numberOfPassengers)%>
                        </td>
                    </tr>


                    <tr>
                        <td style="width:15%">
                            <b><%=LM.getText(LC.EMP_TRAVEL_DETAILS_EDIT_DETAILSOFOTHERPASSENGERS, userDTO)%>
                            </b></td>
                        <td colspan="3"><%=emp_travel_detailsDTO.detailsOfOtherPassengers%>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:15%"><b><%=LM.getText(LC.VM_INCIDENT_ADD_FILESDROPZONE, userDTO)%>
                        </b></td>
                        <td style="width: 35%" colspan="3">
                            <%
                                {
                                    List<FilesDTO> FilesDTOList = new FilesDAO().getMiniDTOsByFileID(emp_travel_detailsDTO.filesDropzone);
                            %>
                            <table>
                                <tr>
                                    <%
                                        if (FilesDTOList != null) {
                                            for (int j = 0; j < FilesDTOList.size(); j++) {
                                                FilesDTO filesDTO = FilesDTOList.get(j);
                                                byte[] encodeBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(filesDTO.thumbnailBlob);
                                    %>
                                    <td style="width: 35%">
                                        <%
                                            if (filesDTO.fileTypes.contains("image") && encodeBase64 != null) {
                                        %>
                                        <img src='data:<%=filesDTO.fileTypes%>;base64,<%=new String(encodeBase64)%>'
                                             style='width:100px'/>
                                        <%
                                            }
                                        %>
                                        <a href='Emp_travel_detailsServlet?actionType=downloadDropzoneFile&id=<%=filesDTO.iD%>'
                                           download><%=filesDTO.fileTitle%>
                                        </a>
                                    </td>
                                    <%
                                            }
                                        }
                                    %>
                                </tr>
                            </table>
                            <%
                                }
                            %>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>