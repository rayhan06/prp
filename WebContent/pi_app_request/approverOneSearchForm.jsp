
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="pi_app_request.*"%>
<%@ page import="util.*"%>

<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@page import = "java.util.Enumeration"%>



<%@ page import="pb.*"%>
<%@page import="sessionmanager.SessionConstants"%>
<%@page import="language.LanguageTextDTO"%>
<%@page import="language.LC"%>
<%@page import="language.LM"%>
<%@page import="login.LoginDTO"%>
<%@page import="user.*"%>
<%@page import="org.apache.commons.codec.binary.*"%>
<%@ page import="office_units.Office_unitsDTO" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="fiscal_year.Fiscal_yearDTO" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>
<%@ page import="javax.rmi.CORBA.Util" %>
<%@page contentType="text/html;charset=utf-8" %>


<%
String navigator2 = "navPI_APP_REQUEST";
String servletName = "Pi_app_requestServlet";
%>
<%@include file="../pb/searchInitializer.jsp"%>				
			
				
				<div class="table-responsive">
					<table id="tableData" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_OFFICEUNITID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_FISCALYEARID, loginDTO)%></th>
								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_STATUS, loginDTO)%></th>
<%--								<th><%=UtilCharacter.getDataByLanguage(Language, "মোট প্যাকেজ ", "Total Package")%></th>--%>
<%--								<th><%=UtilCharacter.getDataByLanguage(Language, "মোট পণ্য ", "Total Item")%></th>--%>
<%--								<th><%=UtilCharacter.getDataByLanguage(Language, "মোট খরচ", "Total Estimated Cost")%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEDDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVEONEDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVETWODATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVETHREEDATE, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTERORGID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEROFFICEID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEROFFICEUNITID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEREMPID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTERPHONENUM, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTERNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTERNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEROFFICENAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEROFFICENAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEROFFICEUNITNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEROFFICEUNITNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_REQUESTEROFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEORGID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEOFFICEID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEOFFICEUNITID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEEMPID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEPHONENUM, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONENAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONENAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEOFFICENAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEOFFICENAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEOFFICEUNITNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEOFFICEUNITNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEOFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERONEOFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOORGID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOOFFICEID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOOFFICEUNITID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOEMPID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOPHONENUM, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWONAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWONAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOOFFICENAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOOFFICENAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOOFFICEUNITNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOOFFICEUNITNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOOFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTWOOFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEORGID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEOFFICEID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEOFFICEUNITID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEEMPID, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEPHONENUM, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREENAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREENAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEOFFICENAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEOFFICENAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEOFFICEUNITNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEOFFICEUNITNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEOFFICEUNITORGNAMEEN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_APPROVERTHREEOFFICEUNITORGNAMEBN, loginDTO)%></th>--%>
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_ADD_MODIFIEDBY, loginDTO)%></th>--%>
								<th><%=LM.getText(LC.HM_VIEW_DETAILS, loginDTO)%></th>								
<%--								<th><%=LM.getText(LC.PI_APP_REQUEST_SEARCH_PI_APP_REQUEST_EDIT_BUTTON, loginDTO)%></th>--%>
<%--								<th class="">--%>
<%--									<div class="text-center">--%>
<%--										<span>All</span>--%>
<%--									</div>--%>
<%--									<div class="d-flex align-items-center justify-content-between mt-3">--%>
<%--										<button type="submit" class="btn d-flex align-items-center" value="" style="color: #ff6a6a">--%>
<%--											<i class="fa fa-trash"></i>--%>
<%--										</button>--%>
<%--										<input type="checkbox" name="delete" id="deleteAll" onclick=""/>--%>
<%--									</div>--%>
<%--								</th>--%>
								
								
							</tr>
						</thead>
						<tbody>
							<%
								ArrayList data = (ArrayList<Pi_app_requestDTO>) rn2.list;

								try
								{

									if (data != null) 
									{
										int size = data.size();										
										System.out.println("data not null and size = " + size + " data = " + data);
										for (int i = 0; i < size; i++) 
										{
											Pi_app_requestDTO pi_app_requestDTO = (Pi_app_requestDTO) data.get(i);
																																
											
											%>
											<tr>
								
		
											<td>
											<%
												value =  "";
												Office_unitsDTO unit = Office_unitsRepository.getInstance().
														getOffice_unitsDTOByID(pi_app_requestDTO.officeUnitId);
												if (unit != null) {
													value = UtilCharacter.getDataByLanguage(Language, unit.unitNameBng, unit.unitNameEng);
												}

											%>
				
											<%=value%>
				
			
											</td>
		
											<td>
											<%
												value =  "";
												Fiscal_yearDTO fiscalYearDTO = Fiscal_yearRepository.getInstance().
														getFiscal_yearDTOByid(pi_app_requestDTO.fiscalYearId);
												if (fiscalYearDTO != null) {
													value = UtilCharacter.getDataByLanguage(Language, fiscalYearDTO.nameBn, fiscalYearDTO.nameEn);
												}
											%>
				
											<%=value%>
				
			
											</td>
		
											<td>
											<%
												value = CommonApprovalStatus.getText(pi_app_requestDTO.status, Language);
											%>
				
											<%=value%>
				
			
<%--											</td>--%>

<%--												<%--%>
<%--													if(pi_app_requestDTO.status == CommonApprovalStatus.PENDING.getValue()){--%>

<%--												%>--%>

<%--														<td>--%>
<%--															<%=Utils.getDigits(pi_app_requestDTO.requested_package_count, Language)%>--%>
<%--														</td>--%>

<%--														<td>--%>
<%--															<%=Utils.getDigits((int)pi_app_requestDTO.requested_item_count, Language)%>--%>
<%--														</td>--%>

<%--														<td>--%>

<%--															<%=Utils.getDigits(String.format("%.2f", pi_app_requestDTO.requested_est_cost),--%>
<%--																	Language)%>--%>
<%--														</td>--%>

<%--												<%--%>

<%--													} else {--%>
<%--												%>--%>
<%--															<td>--%>
<%--																<%=Utils.getDigits(pi_app_requestDTO.approver_one_package_count, Language)%>--%>
<%--															</td>--%>

<%--															<td>--%>
<%--																<%=Utils.getDigits((int)pi_app_requestDTO.approver_one_item_count, Language)%>--%>
<%--															</td>--%>

<%--															<td>--%>

<%--																<%=Utils.getDigits(String.format("%.2f",--%>
<%--																		pi_app_requestDTO.approver_one_est_cost), Language)%>--%>
<%--															</td>--%>
<%--												<%--%>

<%--													}--%>
<%--												%>--%>


		
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requestedDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_requestedDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_requestedDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approveOneDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_approveOneDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_approveOneDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approveTwoDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_approveTwoDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_approveTwoDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approveThreeDate + "";--%>
<%--											%>--%>
<%--											<%--%>
<%--											String formatted_approveThreeDate = simpleDateFormat.format(new Date(Long.parseLong(value))).toString();--%>
<%--											%>--%>
<%--											<%=Utils.getDigits(formatted_approveThreeDate, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOrgId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOfficeId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOfficeUnitId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterEmpId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterPhoneNum + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOfficeNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOfficeNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOfficeUnitNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOfficeUnitNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOfficeUnitOrgNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.requesterOfficeUnitOrgNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOrgId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOfficeId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOfficeUnitId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneEmpId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOnePhoneNum + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOfficeNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOfficeNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOfficeUnitNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOfficeUnitNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOfficeUnitOrgNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverOneOfficeUnitOrgNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOrgId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOfficeId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOfficeUnitId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoEmpId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoPhoneNum + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOfficeNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOfficeNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOfficeUnitNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOfficeUnitNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOfficeUnitOrgNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverTwoOfficeUnitOrgNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOrgId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOfficeId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOfficeUnitId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeEmpId + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreePhoneNum + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOfficeNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOfficeNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOfficeUnitNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOfficeUnitNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOfficeUnitOrgNameEn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.approverThreeOfficeUnitOrgNameBn + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
<%--		--%>
<%--		--%>
<%--		--%>
<%--											<td>--%>
<%--											<%--%>
<%--											value = pi_app_requestDTO.modifiedBy + "";--%>
<%--											%>--%>
<%--				--%>
<%--											<%=Utils.getDigits(value, Language)%>--%>
<%--				--%>
<%--			--%>
<%--											</td>--%>
		
		
		
		
		
	
											<%
											String url ;
											if(pi_app_requestDTO.status == CommonApprovalStatus.PENDING.getValue()){
												url = servletName + "?actionType=addApprovalOne&ID=" + pi_app_requestDTO.iD;
											} else {
												url = servletName + "?actionType=view&ID=" + pi_app_requestDTO.iD;
											}
											%>
												<td>
													<button
															type="button"
															class="btn-sm border-0 shadow bg-light btn-border-radius"
															style="color: #ff6b6b;"
															onclick="location.href='<%=url%>'"
													>
														<i class="fa fa-eye"></i>
													</button>
												</td>

<%--												<td>--%>
<%--													<%--%>
<%--														if (commonDTO.jobCat == SessionConstants.DEFAULT_JOB_CAT) {--%>
<%--													%>--%>
<%--													<button--%>
<%--															type="button"--%>
<%--															class="btn-sm border-0 shadow btn-border-radius text-white"--%>
<%--															style="background-color: #ff6b6b;"--%>
<%--															onclick="location.href='<%=servletName%>?actionType=getEditPage&ID=<%=commonDTO.iD%>'"--%>
<%--													>--%>
<%--														<i class="fa fa-edit"></i>--%>
<%--													</button>--%>
<%--													<%--%>
<%--														}--%>
<%--													%>--%>
<%--												</td>--%>
																						
<%--											<td class="text-right">--%>
<%--												<div class='checker'>--%>
<%--													<span class='chkEdit' ><input type='checkbox' name='ID' value='<%=pi_app_requestDTO.iD%>'/></span>--%>
<%--												</div>--%>
<%--											</td>--%>
																																
											</tr>
											<%
										}
										 
										System.out.println("printing done");
									}
									else
									{
										System.out.println("data  null");
									}
								}
								catch(Exception e)
								{
									System.out.println("JSP exception " + e);
								}
							%>



						</tbody>

					</table>
				</div>

<input type="hidden" id="hidden_pageno" value="<%=pageno2%>" />
<input type="hidden" id="hidden_totalpage" value="<%=totalpage2%>" />
<input type="hidden" id="hidden_totalrecords" value="<%=totalRecords2%>" />
<input type="hidden" id="hidden_lastSearchTime" value="<%=lastSearchTime%>" />
<input type="hidden" id="isPermanentTable" value="<%=isPermanentTable%>" />


			