<%@ page import="util.UtilCharacter" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@page pageEncoding="UTF-8" %>

<div id="drop_down" class="">
    <div class="">
        <div id="ajax-content">
            <div class="mt-4">
                <form class="form-body" id="item-list-modal-form">
                    <h5 class="table-title">

                    </h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="modal-product-view-table">
                            <thead>
                            <tr>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "ক্রমিক নং", "Serial Number")%>
                                </th>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মালামালের বিবরন", "Item Description")%>
                                </th>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "দপ্তর", "Office")%>
                                </th>
                                <th rowspan="1"
                                    colspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "পরিমাণ", "Quantity")%>
                                </th>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "একক দর (টাকায়)", "Unit Price (BDT)")%>
                                </th>
                                <th rowspan="2"
                                    style="vertical-align: middle; text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "মোট দর (টাকায়)", "Total Price (BDT)")%>
                                </th>
                            </tr>
                            <tr>
                                <th style="text-align: center"><%=UtilCharacter.getDataByLanguage(Language, "সংখ্যা", "Number")%>
                                </th>
                                <th style="text-align: center;"><%=UtilCharacter.getDataByLanguage(Language, "একক", "Unit")%>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="item-list-body">
                            <tr class="hiddenTr" id="template-item-list-row">
                                <input type="hidden" id="approveLevel">
                                <input type="hidden" id="iD" value="-1">
                                <td id="modal_item_serial_">
                                </td>
                                <td id="modal_item_description_">
                                </td>
                                <td id="modal_item_office_unit_name_">
                                </td>
                                <td>
                                    <input class='form-control w-auto' type="number" name="quantity" min="0"
                                           max="9999999999" id="modal_item_quantity" value=""
                                           onchange="calculateTotalPrice(this)">
                                </td>
                                <td id="modal_item_unit_name">
                                </td>
                                <td>
                                    <input class='form-control w-auto' type="number" name="unitPrice" min="0"
                                           max="9999999999" id="modal_item_unit_price" value=""
                                           onchange="calculateTotalPrice(this)">
                                </td>
                                <td>
                                    <input class='form-control w-auto' type="number" name="totalPrice" min="0"
                                           max="9999999999" id="modal_item_total_price" value="">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .hiddenTr {
        display: none;
    }
</style>