<%@page import="pi_app_request.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="pb.*" %>
<%@page import="util.*" %>
<%@ page import="office_units.Office_unitsRepository" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDTO" %>
<%@ page import="pi_app_request_details.Pi_app_request_detailsDAO" %>
<%@ page import="common.BaseServlet" %>
<%@ page import="fiscal_year.Fiscal_yearRepository" %>
<%@ page import="pi_package_final.Pi_package_finalDTO" %>
<%@ page import="pi_package_final.PiPackageLotFinalDTO" %>
<%@ page import="pi_package_final.Pi_package_finalRepository" %>
<%@ page import="pi_package_final.PiPackageLotFinalRepository" %>
<%@ page import="pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDTO" %>
<%@ page import="pi_app_request_package_lot_item_list.Pi_app_request_package_lot_item_listDAO" %>
<%@ page import="vm_requisition.CommonApprovalStatus" %>

<%@include file="../pb/viewInitializer.jsp" %>

<%
    String servletName = "Pi_app_requestServlet";
    String ID = request.getParameter("ID");
    long id = Long.parseLong(ID);
    Pi_app_requestDTO pi_app_requestDTO = (Pi_app_requestDTO) request.getAttribute(BaseServlet.DTO_FOR_JSP);
    long appStatus = pi_app_requestDTO.status;

    String officeName = Office_unitsRepository.getInstance().geText(Language, pi_app_requestDTO.officeUnitId);
    String fiscalYearText = Fiscal_yearRepository.getInstance().getText(pi_app_requestDTO.fiscalYearId, "English");

    List<Pi_app_request_detailsDTO> childModels = Pi_app_request_detailsDAO.getInstance().getByAppId(Long.parseLong(ID));

    String allOfficeList = "";
    HashMap<Long, String> offices = new HashMap<>();
    for (Pi_app_request_detailsDTO detailsDTO : childModels) {
        List<Pi_app_request_package_lot_item_listDTO> models = (List<Pi_app_request_package_lot_item_listDTO>)
                Pi_app_request_package_lot_item_listDAO.getInstance()
                        .getDTOsByParent("app_details_id", detailsDTO.iD);
        for (Pi_app_request_package_lot_item_listDTO itemDTO : models) {
            if (offices.containsKey(itemDTO.officeUnitId)) {
                continue;
            } else {
                String office = Office_unitsRepository.getInstance().geText(Language, itemDTO.officeUnitId);
                offices.put(itemDTO.officeUnitId, office);
            }
        }
    }

    int size = offices.size();
    int ptr = 0;
    for (Map.Entry<Long, String> data : offices.entrySet()) {
        if (ptr < size - 1) allOfficeList += data.getValue() + ", ";
        else allOfficeList += data.getValue() + " ";
        ptr++;
    }

    long allPackageTotalSum = 0;
%>


<style>
    .text-color {
        color: #0098bf;
    }

    .form-group label {
        font-weight: 600 !important;
    }

    .form-group {
        margin-bottom: .5rem;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .page-bg {
        background-color: #f9f9fb;
    }

    .fix-fill {
        overflow: hidden;
        white-space: nowrap;
    }

    #to-print-div * {
        font-size: 12px;
    }

    #to-print-div h1 {
        font-size: 16px;
        font-weight: bold;
    }

    #to-print-div h2 {
        font-size: 14px;
        font-weight: bold;
    }

    #to-print-div h3 {
        font-size: 13px;
        font-weight: bold;
    }

    .page[data-size="A4"] {
        width: 210mm;
        height: 297mm;
        padding: .5in;
        background: white;
        margin-bottom: 10px;
    }

    .page[data-size="A4-landscape"] {
        width: 297mm;
        height: 210mm;
        padding: .1in;
        background: white;
        margin-bottom: 10px;
    }

    .foot-note {
        font-size: 11px !important;
    }

    .blank-to-fill {
        display: inline-block;
        border-bottom: 1px dotted black;
        width: inherit;
    }

    .table-bordered > :not(caption) > * {
        border-color: black;
    }

    .table-bordered > :not(caption) > * > * {
        border-color: black;
    }

    .table-bordered td,
    .table-bordered th {
        padding: 5px;
    }

    .align-top {
        vertical-align: top;
    }

    th {
        text-align: center;
    }

    .table-bordered-custom th,
    .table-bordered-custom td {
        border: 1px solid #000;
        padding: 4px;
    }

    .page {
        background: rgba(255, 255, 255, 0.85);
        padding: .5in;
        margin-bottom: 5px;
        page-break-after: always;
        box-shadow: rgba(131, 131, 109, 0.85);
    }

    @media print {
        @page {
            size: landscape;
            margin: .25in;
        }

        .shadow {
            box-shadow: none !important;
        }
    }
</style>

<div class="kt-content p-0" id="kt_content">

    <div class="kt-portlet">

        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title prp-page-title">

                </h3>
            </div>
        </div>
        <div class="kt-portlet__body" id="bill-div">
            <div class="ml-auto m-5">
                <button type="button" class="btn" id='download-pdf'
                        onclick="printDivWithJqueryPrint('to-print-div');">
                    <i class="fa fa-file-pdf fa-2x" style="color: gray" aria-hidden="true"></i>
                </button>
            </div>

            <div style="margin: auto;">
                <div class="container shadow p-4">
                    <div id="to-print-div">
                        <section class="page" data-size="A4-landscape">
                            <div class="">
                                <%------------------------------------------------------------------------------------------------------------------%>
                                <%--DYNAIMICALLY GENERATE TABLE WHICH FITS DATA IN MULTIPLE PAGE--%>
                                <%--TABLE CONFIG--%>
                                <%
                                    boolean isLastPage = false;
                                    final int rowsPerPage = 6;
                                    int index = 0;
                                    while (index < childModels.size()) {
                                        boolean isFirstPage = (index == 0);
                                %>
                                <%if (isFirstPage) {%>
                                <div class="text-center">

                                    <div class="font-weight-bold text-center">
                                        <h3><%=UtilCharacter.getDataByLanguage(Language, "বাংলাদেশ জাতীয় সংসদ সচিবালয়", "Bangladesh National Parliament Secretariat")%>
                                        </h3>
                                        <h4><%=officeName%>
                                        </h4>
                                        <h5>
                                            <%=allOfficeList%> এর জন্য প্রয়োজনীয় পণ্য/দ্রব্য সামগ্রী, ক্রয়, সংগ্রহ ও
                                            মেরামতের জন্য <%=Utils.getDigits(fiscalYearText, Language)%> অর্থবছরের
                                            বার্ষিক ক্রয় পরিকল্পনা
                                        </h5>
                                        <h1>Annual Procurement Plan for Revenue Budgets</h1>
                                    </div>
                                    <div class="text-left pb-4 pt-5">
                                        <h3>Ministry/Division: Bangladesh Parliament Secretariat</h3>
                                        <h3>Head of Procuring Entity name & Code: Honorable Speaker Bangladesh
                                            Parliament or Official Authorized By him.</h3>
                                        <h3>Financial Year: <%=fiscalYearText%>
                                        </h3>
                                    </div>

                                </div>
                                <%}%>

                                <div>
                                    <div>
                                        <table class="table-bordered-custom">
                                            <thead style="background-color:lightgrey">
                                            <tr>
                                                <th><%=(isLanguageEnglish ? "Serial Number" : "ক্রমিক নং")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Package No" : "প্যাকেজ নং")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Description of Procurement Package Goods" : "প্রকিউরমেন্ট প্যাকেজের মালামালের বিবরণ")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Quantity/ Items" : "পরিমান/আইটেম")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Unit Price" : "একক দাম")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Estimated Cost in Tk" : "আনুমানিক খরচ (টাকায়)")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Procurement Method and Type" : "ক্রয় পদ্ধতি এবং প্রকার")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Contract Approving Authority" : "চুক্তি অনুমোদনকারী কর্তৃপক্ষ")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Source of Fund" : "তহবিলের উৎস")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Time Code for Process" : "প্রক্রিয়ার জন্য সময় কোড")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Invite/ Advertisement Tender" : "আমন্ত্রণ/বিজ্ঞাপন দরপত্র")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Tender Opening" : "টেন্ডার ওপেনিং")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Tender Evaluation" : "দরপত্র মূল্যায়ন")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Approval to Award" : "পুরস্কারের অনুমোদন")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Notification of Award" : "পুরস্কারের বিজ্ঞপ্তি")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Signing of Contract" : "চুক্তি স্বাক্ষর")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Total Time to Contract Signature" : "চুক্তি স্বাক্ষরের মোট সময়")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Time for Completion of Contract" : "চুক্তি সমাপ্তির সময়")%>
                                                </th>
                                                <th><%=(isLanguageEnglish ? "Remark" : "রিমার্ক")%>
                                                </th>
                                            </tr>
                                            <tr>
                                                <%
                                                    for (i = 1; i <= 19; i++) {
                                                %>
                                                <td class="text-center"><%=Utils.getDigits(i, Language)%>
                                                </td>
                                                <%}%>
                                            </tr>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <%if (!isFirstPage) {%>
                                            <%}%>

                                            <%
                                                int rowsInThisPage = 0;
                                                int childTableStartingID = 0;
                                                while (index < childModels.size() && rowsInThisPage < rowsPerPage) {
                                                    isLastPage = (index == (childModels.size() - 1));
                                                    rowsInThisPage++;
                                                    Pi_app_request_detailsDTO model = childModels.get(index++);
                                            %>
                                            <%
                                                String newLine = "<br>";
                                                Pi_package_finalDTO packageModel = Pi_package_finalRepository.getInstance()
                                                        .getPi_package_finalDTOByiD(model.packageFinalId);
                                                PiPackageLotFinalDTO lotModel = PiPackageLotFinalRepository.getInstance()
                                                        .getPiPackageLotFinalDTOByiD(model.lotFinalId);
                                                String packageNo = "";
                                                if (packageModel != null)
                                                    packageNo = UtilCharacter.getDataByLanguage(Language, packageModel.packageNumberBn, packageModel.packageNumberEn);
                                                if (lotModel != null) packageNo += " (" +
                                                        UtilCharacter.getDataByLanguage(Language, lotModel.lotNumberBn, lotModel.lotNumberEn) + ")";
                                                String description = "";
                                                if (packageModel != null)
                                                    description = UtilCharacter.getDataByLanguage(Language, packageModel.packageNameBn, packageModel.packageNameEn);
                                                if (lotModel != null)
                                                    description = UtilCharacter.getDataByLanguage(Language, lotModel.lotNameBn, lotModel.lotNameEn);

                                                // QUANTITY / ITEMS CELL DATA GENERATION
                                                List<Pi_app_request_package_lot_item_listDTO> models = (List<Pi_app_request_package_lot_item_listDTO>)
                                                        Pi_app_request_package_lot_item_listDAO.getInstance()
                                                                .getDTOsByParent("app_details_id", model.iD);
                                                HashMap<Long, Long> officeItemCount = new HashMap<>();
                                                for (Pi_app_request_package_lot_item_listDTO quantityModel : models) {
                                                    if (officeItemCount.containsKey(quantityModel.officeUnitId)) {
                                                        long prevCount = officeItemCount.get(quantityModel.officeUnitId);
                                                        officeItemCount.put(quantityModel.officeUnitId, prevCount + 1);
                                                    } else {
                                                        officeItemCount.put(quantityModel.officeUnitId, 1l);
                                                    }
                                                }

                                                StringBuilder quantity = new StringBuilder();

                                                for (Map.Entry<Long, Long> entity : officeItemCount.entrySet()) {
                                                    quantity.append(Office_unitsRepository.getInstance().geText(Language, entity.getKey()))
                                                            .append(newLine)
                                                            .append(Utils.getDigits(entity.getValue(), Language))
                                                            .append(UtilCharacter.getDataByLanguage(Language, " টি", " Piece"))
                                                            .append(newLine);
                                                }
                                                String quantityText = quantity.toString();


                                                // GENERATE DATA OF EST COST CELL
                                                officeItemCount = new HashMap<>();
                                                long grandTotal = 0;
                                                for (Pi_app_request_package_lot_item_listDTO estCostModel : models) {
                                                    long currentCount = Pi_app_request_package_lot_item_listDAO.getInstance()
                                                            .getTotalPriceByAppStatus(estCostModel, appStatus);
                                                    if (officeItemCount.containsKey(estCostModel.officeUnitId)) {
                                                        long prevCount = officeItemCount.get(estCostModel.officeUnitId);
                                                        officeItemCount.put(estCostModel.officeUnitId, prevCount + currentCount);
                                                    } else {
                                                        officeItemCount.put(estCostModel.officeUnitId, currentCount);
                                                    }
                                                    grandTotal += currentCount;
                                                }
                                                allPackageTotalSum += grandTotal;

                                                StringBuilder estCost = new StringBuilder();

                                                for (Map.Entry<Long, Long> entity : officeItemCount.entrySet()) {
                                                    estCost.append(Utils.getDigits(entity.getValue(), Language)).append(newLine);
                                                }
                                                estCost.append("----------------").append(newLine);
                                                estCost.append(Utils.getDigits(grandTotal, Language));
                                                String estCostText = estCost.toString();
                                            %>
                                            <tr class="text-center">
                                                <td id="serial_number_<%=childTableStartingID%>"><%=Utils.getDigits(model.serialNum, Language)%>
                                                </td>
                                                <td id="package_no_<%=childTableStartingID%>"><%=packageNo%>
                                                </td>
                                                <td id='description_<%=childTableStartingID%>'><%=description%>
                                                </td>
                                                <td id='total_quantity_text_<%=childTableStartingID%>'><%=quantityText%>
                                                </td>
                                                <td id="unit_price_<%=childTableStartingID%>">
                                                    <%=model.packageUnitPrice%>
                                                </td>
                                                <td>
                                                    <%=estCostText%>
                                                </td>
                                                <td>
                                                    <%=Pi_app_request_package_lot_item_listDAO.getInstance().getProcurementMethodName(model, appStatus)%>
                                                </td>
                                                <td>
                                                    <%
                                                        String Options;
                                                    %>
                                                    <%=Pi_app_request_package_lot_item_listDAO.getInstance().getApprovalAuthorityName(model, appStatus)%>
                                                </td>
                                                <td>
                                                    <%
                                                        Options = CatRepository.getInstance().buildOptionsWithoutSelectOption("pi_source_of_fund", Language, Math.toIntExact(model.sourceFundId));
                                                    %>
                                                    <%=Options%>
                                                </td>
                                                <td><%=model.timeCodeForProcess%></td>
                                                <td><%=model.inviteAdvertiseTender%></td>
                                                <td><%=model.tenderOpening%></td>
                                                <td><%=model.tenderEvaluation%></td>
                                                <td><%=model.approvalToAward%></td>
                                                <td><%=model.notificationOfAward%></td>
                                                <td><%=model.signingOfContract%></td>
                                                <td><%=model.totalTimeToContractSignature%></td>
                                                <td><%=model.timeForCompletionOfContract%></td>
                                                <td><%=model.remark%></td>
                                            </tr>
                                            <%
                                                    childTableStartingID++;
                                                }
                                            %>
                                            <%
                                                }
                                            %>
                                            </tbody>
                                            <tfoot>
                                            <%if (isLastPage) {%>
                                            <tr>
                                                <td colspan="4" style="text-align: right">
                                                    <%=UtilCharacter.getDataByLanguage(Language, "সর্বমোট ব্যয়: ", "Grand Total: ")%>
                                                    <%
                                                        String allPackageTotalSumText = Utils.getDigits(allPackageTotalSum, Language);
                                                    %>
                                                </td>
                                                <td colspan="2" style="text-align: right">
                                                    <span id="grand_total"><%=allPackageTotalSumText%></span>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <%}%>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <%if (isLastPage) {%>
                                <%}%>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<jsp:include page="../utility/jquery_print.jsp"/>